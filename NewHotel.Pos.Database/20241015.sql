alter table tcfg_npos add npos_atsk varchar2(1) default '0' not null;
comment on column tcfg_npos.npos_atsk is 'flag: Automatically to tables after send all to kitchen';

BEGIN
    MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN
        UPDATE SET PARA.DATE_VERS = '20241015'
    WHEN NOT MATCHED THEN
        INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20241015');
    COMMIT;
END;
/