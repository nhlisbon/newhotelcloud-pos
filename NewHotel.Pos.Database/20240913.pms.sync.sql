CREATE OR REPLACE VIEW SYNC_TNHT_ACOM AS
SELECT ACOM.ROWID AS ROW_ID,
       ACOM.ACOM_PK,
       ACOM.ACOM_ARPA,
       ACOM.ACOM_ARCH
FROM TNHT_ACOM ACOM;
/

CREATE OR REPLACE VIEW SYNC_TNHT_IPOS AS
SELECT IPOS.ROWID AS ROW_ID,
       IPOS.IPOS_PK,
       SECC.HOTE_PK,
       SECC.LITE_ABRE,
       SECC.LITE_DESC,
       IPOS.IPOS_FCTR,
       IPOS.IPOS_TUTR,
       IPOS.TPRG_STDR,
       IPOS.TPRG_CINT,
       IPOS.TPRG_CIPO,
       IPOS.TPRG_PENS,
       IPOS.TPRG_PEPO,
       IPOS.CNFG_TICK,
       IPOS.CNFG_FACT,
       IPOS.CNFG_COMP,
       IPOS.CNFG_BOLE,
       IPOS.IPOS_CCOP,
       IPOS.IPOS_CKAB,
       IPOS.IPOS_KITI,
       IPOS.IPOS_SPTA,
       IPOS.IPOS_COMP,
       IPOS.IPOS_TISE,
       IPOS.IPOS_TIPE,
       IPOS.IPOS_TIVL,
       IPOS.IPOS_TIAT,
       IPOS.IPOS_TION,
       IPOS.MENU_MON,
       IPOS.MENU_TUE,
       IPOS.MENU_WED,
       IPOS.MENU_THU,
       IPOS.MENU_FRI,
       IPOS.MENU_SAT,
       IPOS.MENU_SUN,
       IPOS.CTRL_PK,
       IPOS.IPOS_TIGR,
       IPOS.IPOS_ROSE,
       IPOS.IPOS_PRNO,
       SECC.SECC_INAC AS IPOS_INAC,
       IPOS.ANDR_PAYM,
       IPOS.IPOS_MEAL,
       IPOS.IPOS_HOUSE,
       IPOS.IPOS_ROOM,
       IPOS.IPOS_DEPO,
       IPOS.ASK_PAXS as ASK_PAX,
       IPOS.ASK_DESC,
       IPOS.ASK_ROOM,
       IPOS.TPRG_ACOM
FROM TNHT_IPOS IPOS
         INNER JOIN TNHT_SECC SECC ON SECC.SECC_PK = IPOS.IPOS_PK
         INNER JOIN TCFG_TEMP TEMP ON TEMP.HOTE_PK = SECC.HOTE_PK;
/
CREATE OR REPLACE TRIGGER CHANGE_NOTIFICATION_TNHT_ACOM
    FOR INSERT OR UPDATE OR DELETE
    ON TNHT_ACOM
    COMPOUND TRIGGER
    TYPE t_change_notification IS TABLE OF change_notification%ROWTYPE;
    l_change_notification t_change_notification := t_change_notification();
    v_change_id INTEGER;
AFTER EACH ROW IS
BEGIN
    SELECT change_notification_id.NEXTVAL INTO v_change_id FROM DUAL;
    l_change_notification.EXTEND;
    l_change_notification(l_change_notification.LAST).change_id := v_change_id;
    l_change_notification(l_change_notification.LAST).change_table_name := 'TNHT_ACOM';
    l_change_notification(l_change_notification.LAST).change_row_id := CASE
                                                                           WHEN INSERTING THEN :NEW.ROWID
                                                                           WHEN DELETING THEN :OLD.ROWID
        END;
    l_change_notification(l_change_notification.LAST).change_operation := CASE
                                                                              WHEN INSERTING THEN 'I'
                                                                              WHEN UPDATING THEN 'U'
                                                                              WHEN DELETING THEN 'D'
        END;
    IF DELETING THEN
        l_change_notification(l_change_notification.LAST).change_delete_pk := :OLD.ACOM_PK;
    END IF;
END AFTER EACH ROW;

    AFTER STATEMENT IS
    BEGIN
        FORALL v_index IN l_change_notification.FIRST..l_change_notification.LAST
            INSERT INTO change_notification (change_id, change_table_name, change_operation, change_row_id,
                                             change_delete_pk)
            VALUES (l_change_notification(v_index).change_id, l_change_notification(v_index).change_table_name,
                    l_change_notification(v_index).change_operation, l_change_notification(v_index).change_row_id,
                    l_change_notification(v_index).change_delete_pk);
    END AFTER STATEMENT;
    END;
/


BEGIN
    MERGE INTO TCFG_GENE PARA
    USING DUAL
    ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN
        UPDATE SET PARA.SYNC_VERS = '20240913'
    WHEN NOT MATCHED THEN
        INSERT (PARA.GENE_PK, PARA.SYNC_VERS) VALUES (1, '20240913');
    COMMIT;
END;
/