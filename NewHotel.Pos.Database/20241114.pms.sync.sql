-- tnht_inte trigger

CREATE OR REPLACE TRIGGER change_notification_tnht_inte
FOR INSERT OR UPDATE OR DELETE ON tnht_inte
COMPOUND TRIGGER
  TYPE t_change_notification IS TABLE OF change_notification%ROWTYPE;
  l_change_notification t_change_notification := t_change_notification();
  v_change_id INTEGER;
  AFTER EACH ROW IS
  BEGIN
    SELECT change_notification_id.NEXTVAL INTO v_change_id FROM DUAL;
    l_change_notification.EXTEND;
    l_change_notification(l_change_notification.LAST).change_id := v_change_id;
    l_change_notification(l_change_notification.LAST).change_table_name := 'tnht_inte';
    l_change_notification(l_change_notification.LAST).change_row_id := CASE
                                                                           WHEN INSERTING THEN :NEW.ROWID
                                                                           WHEN UPDATING THEN :OLD.ROWID
                                                                           WHEN DELETING THEN :OLD.ROWID
        END;
    l_change_notification(l_change_notification.LAST).change_operation := CASE
                                                                              WHEN INSERTING THEN 'I'
                                                                              WHEN UPDATING THEN 'U'
                                                                              WHEN DELETING THEN 'D'
        END;
    IF DELETING THEN
        l_change_notification(l_change_notification.LAST).change_delete_pk := :OLD.INTE_PK;
    END IF;
  END AFTER EACH ROW;
  AFTER STATEMENT IS
  BEGIN
     FORALL v_index IN l_change_notification.FIRST..l_change_notification.LAST
       INSERT INTO change_notification (change_id, change_table_name, change_operation, change_row_id, change_delete_pk)
       VALUES (l_change_notification(v_index).change_id, l_change_notification(v_index).change_table_name,
               l_change_notification(v_index).change_operation, l_change_notification(v_index).change_row_id,
               l_change_notification(v_index).change_delete_pk);
     l_change_notification.DELETE;
  END AFTER STATEMENT;
END;
/

-- tnht_inte sync view

CREATE OR REPLACE FORCE VIEW SYNC_TNHT_INTE
(
   ROW_ID,
   INTE_PK,
   INTE_TYPE,
   INTE_ACTI,
   INTE_LMOD,
   INTE_CONFIG
)
   BEQUEATH DEFINER AS
    SELECT INTE.ROWID AS ROW_ID,
            INTE.INTE_PK,
            INTE.INTE_TYPE,
            INTE.INTE_ACTI,
            INTE.INTE_LMOD,
            INTE.INTE_CONFIG
    FROM TNHT_INTE INTE;

-- tnht_arin trigger

CREATE OR REPLACE TRIGGER change_notification_tnht_arin
FOR INSERT OR UPDATE OR DELETE ON tnht_arin
COMPOUND TRIGGER
  TYPE t_change_notification IS TABLE OF change_notification%ROWTYPE;
  l_change_notification t_change_notification := t_change_notification();
  v_change_id INTEGER;
  AFTER EACH ROW IS
  BEGIN
    SELECT change_notification_id.NEXTVAL INTO v_change_id FROM DUAL;
    l_change_notification.EXTEND;
    l_change_notification(l_change_notification.LAST).change_id := v_change_id;
    l_change_notification(l_change_notification.LAST).change_table_name := 'tnht_arin';
    l_change_notification(l_change_notification.LAST).change_row_id := CASE
                                                                           WHEN INSERTING THEN :NEW.ROWID
                                                                           WHEN UPDATING THEN :OLD.ROWID
                                                                           WHEN DELETING THEN :OLD.ROWID
        END;
    l_change_notification(l_change_notification.LAST).change_operation := CASE
                                                                              WHEN INSERTING THEN 'I'
                                                                              WHEN UPDATING THEN 'U'
                                                                              WHEN DELETING THEN 'D'
        END;
    IF DELETING THEN
        l_change_notification(l_change_notification.LAST).change_delete_pk := :OLD.ARIN_PK;
    END IF;
  END AFTER EACH ROW;
  AFTER STATEMENT IS
  BEGIN
     FORALL v_index IN l_change_notification.FIRST..l_change_notification.LAST
       INSERT INTO change_notification (change_id, change_table_name, change_operation, change_row_id, change_delete_pk)
       VALUES (l_change_notification(v_index).change_id, l_change_notification(v_index).change_table_name,
               l_change_notification(v_index).change_operation, l_change_notification(v_index).change_row_id,
               l_change_notification(v_index).change_delete_pk);
     l_change_notification.DELETE;
  END AFTER STATEMENT;
END;
/

-- tnht_arin sync view

CREATE OR REPLACE FORCE VIEW SYNC_TNHT_ARIN
(
   ROW_ID,
   ARIN_PK,
   ARTG_PK,
   INTE_PK,
   ARIN_CODE,
   ARIN_OPCD_ONE,
   ARIN_OPCD_TWO,
   ARIN_LMOD
)
   BEQUEATH DEFINER AS
    SELECT ARIN.ROWID AS ROW_ID,
            ARIN.ARIN_PK,
            ARIN.ARTG_PK,
            ARIN.INTE_PK,
            ARIN.ARIN_CODE,
            ARIN.ARIN_OPCD_ONE,
            ARIN.ARIN_OPCD_TWO,
            ARIN.ARIN_LMOD
    FROM TNHT_ARIN ARIN;


BEGIN    
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
      WHEN MATCHED THEN UPDATE SET PARA.SYNC_VERS = '20241114'
      WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.SYNC_VERS) VALUES (1, '20241114');
  COMMIT;
END;
/