-- This part is for to change the inte_config from VARCHAR2 to CLOB and the inte_type from VARCHAR2 to NUMBER.
DROP TABLE TNHT_INTE CASCADE CONSTRAINTS;

create table TNHT_INTE(
    INTE_PK RAW(16) NOT NULL,
    INTE_TYPE NUMBER DEFAULT 0 NOT NULL,
    INTE_ACTI VARCHAR2(1) DEFAULT 0 NOT NULL,
    INTE_LMOD TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP NOT NULL,
    INTE_CONFIG CLOB NOT NULL,
	constraint nht_pk_inte primary key (inte_pk)
);
comment on column tnht_inte.inte_type is 'Enum for the type of integration';
comment on column tnht_inte.inte_acti is 'If the integration is active or not';
comment on column tnht_inte.inte_config is 'The configuration of the integration in a JSON format';

BEGIN
    MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN
        UPDATE SET PARA.DATE_VERS = '20241114'
    WHEN NOT MATCHED THEN
        INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20241114');
    COMMIT;
END;
/