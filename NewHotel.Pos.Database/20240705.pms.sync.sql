CREATE OR REPLACE TRIGGER CHANGE_NOTIFICATION_TNHT_ARTB
    FOR INSERT OR UPDATE OR DELETE ON TNHT_ARTB
    COMPOUND TRIGGER

    TYPE t_change_notification IS TABLE OF change_notification%ROWTYPE;
    l_change_notification t_change_notification := t_change_notification();
    v_change_id INTEGER;

AFTER EACH ROW IS
BEGIN
    SELECT change_notification_id.NEXTVAL INTO v_change_id FROM DUAL;
    l_change_notification.EXTEND;
    l_change_notification(l_change_notification.LAST).change_id := v_change_id;
    l_change_notification(l_change_notification.LAST).change_table_name := 'TNHT_ARTB';
    l_change_notification(l_change_notification.LAST).change_row_id := CASE
                                                                           WHEN INSERTING THEN :NEW.ROWID
                                                                           WHEN DELETING THEN :OLD.ROWID
        END;
    l_change_notification(l_change_notification.LAST).change_operation := CASE
                                                                              WHEN INSERTING THEN 'I'
                                                                              WHEN UPDATING THEN 'U'
                                                                              WHEN DELETING THEN 'D'
        END;
    IF DELETING THEN
        l_change_notification(l_change_notification.LAST).change_delete_pk := :OLD.artb_pk;
    END IF;
END AFTER EACH ROW;

    AFTER STATEMENT IS
    BEGIN
        FORALL v_index IN l_change_notification.FIRST..l_change_notification.LAST
            INSERT INTO change_notification (change_id, change_table_name, change_operation, change_row_id, change_delete_pk)
            VALUES (l_change_notification(v_index).change_id, l_change_notification(v_index).change_table_name,
                    l_change_notification(v_index).change_operation, l_change_notification(v_index).change_row_id,
                    l_change_notification(v_index).change_delete_pk);
    END AFTER STATEMENT;
END;

BEGIN
    MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.SYNC_VERS = 'yyyymmdd'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.SYNC_VERS) VALUES (1, 'yyyymmdd');
    COMMIT;
END;
/