CREATE OR REPLACE FORCE VIEW SYNC_TCFG_NPOS
AS
SELECT NPOS.ROWID AS ROW_ID,
       NPOS.HOTE_PK,
       NPOS.FAST_PAYM,
       NPOS.PARA_FORE,
       NPOS.PARA_IVIN,
       NPOS.PARA_ESIM,
       NPOS.PARA_TICK,
       NPOS.PARA_MTCO,
       NPOS.PARA_TICO,
       NPOS.PARA_NPAS,
       NPOS.PARA_TISE,
       NPOS.PARA_TIPE,
       NPOS.PARA_TIVL,
       NPOS.PARA_TIAT,
       NPOS.OPEN_PLAN,
       NPOS.COIN_VALO,
       NPOS.DESC_VALO,
       NPOS.TICK_FISC,
       NPOS.DATR_DASY,
       NPOS.NPOS_TIFI,
       NPOS.NPOS_NCHC,
       NPOS.NPOS_FAON,
       NPOS.NPOS_HBOL,
       NPOS.NPOS_HSCO,
       NPOS.NPOS_DEAU,
       NPOS.SIGN_TIPO,
       NPOS.TICK_MAFN,
       FACT.SIGN_VERS,
       CASE WHEN FDES.FDES_TEST IS NULL THEN '1' ELSE FDES.FDES_TEST END AS SIGN_TEST,
       FDES.FDES_USER AS FISC_USER,
       FDES.FDES_PSWD AS FISC_PASW,
       FACT.ANUL_DATR,
       FACT.ANUL_MONTH,
       FACT.ANUL_DAYS,
       NPOS.FAST_PLAN,
       NPOS.PLAN_ROOM,
       NPOS.EMIT_SOTK,
       NPOS.NPOS_TIME,
       NPOS.PROD_FTQT,
       NPOS.NPOS_IBTN,
       NPOS.NPOS_ASGN,
       NPOS.NPOS_ASEP,
       NPOS.NPOS_SPLT,
       NPOS.NPOS_ACAN,
       NPOS.NPOS_FCON,
	   NPOS.NPOS_ATSK
FROM TCFG_NPOS NPOS
         INNER JOIN TCFG_FACT FACT ON FACT.HOTE_PK = NPOS.HOTE_PK
         LEFT JOIN TCFG_FDES FDES ON FACT.HOTE_PK = FDES.HOTE_PK AND FDES.FDES_PK = 3;

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.SYNC_VERS = '20241015'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.SYNC_VERS) VALUES (1, '20241015');
END;
/

COMMIT;