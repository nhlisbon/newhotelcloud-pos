create or replace PROCEDURE pnht_get_changes (
    p_name IN VARCHAR2,
    p_lmod IN TIMESTAMP,
    p_count IN INTEGER,
    p_cursor0 OUT SYS_REFCURSOR)
    IS
BEGIN
    CASE
        WHEN p_name = 'TNHT_HUNMO' THEN
            OPEN p_cursor0 FOR
                SELECT 'M' as change_operation, hunmo.unmo_lmod as change_lastmodified,
                       hunmo.hunmo_pk, hunmo.hote_pk,
                       hunmo.unmo_pk, hunmo.unmo_symb, hunmo.unmo_caja, hunmo.camb_mult,
                       CASE WHEN hote.unmo_base is null THEN '0' ELSE '1' END as unmo_base
                FROM tnht_hunmo hunmo inner JOIN tcfg_hote hote
                                                 ON hote.hote_pk = hunmo.hote_pk AND hote.unmo_base = hunmo.hunmo_pk
                WHERE hunmo.unmo_lmod > p_lmod
                ORDER BY hunmo.unmo_lmod;
        WHEN p_name = 'TNHT_CAMB' THEN
            OPEN p_cursor0 FOR
                SELECT 'M' as change_operation, camb.camb_lmod as change_lastmodified,
                       camb.camb_pk, camb.camb_data as camb_date, camb.camb_vamb as camb_valo, camb.hunmo_pk
                FROM tnht_hunmo hunmo
                         inner JOIN tcfg_hote hote ON hote.hote_pk = hunmo.hote_pk
                         INNER JOIN tnht_camb camb ON camb.hunmo_pk = hunmo.hunmo_pk
                         INNER JOIN tnht_datr datr ON datr.hote_pk = hunmo.hote_pk
                WHERE datr.appl_pk = 2 AND camb.camb_data >= datr.datr_datr
                  AND camb.camb_lmod > p_lmod
                ORDER BY camb.camb_lmod;
        WHEN p_name = 'TNHT_COMU' THEN
            OPEN p_cursor0 FOR
                SELECT 'M' as change_operation, comu.comu_lmod as change_lastmodified,
                       comu.comu_pk, comu.naci_pk, comu.comu_desc, comu.comu_caux
                FROM tnht_comu comu WHERE comu.comu_lmod > p_lmod
                ORDER BY comu.comu_lmod;
        WHEN p_name = 'TNHT_DIST' THEN
            OPEN p_cursor0 FOR
                SELECT 'M' as change_operation, dist.dist_lmod as change_lastmodified,
                       dist.dist_pk, dist.naci_pk, dist.dist_desc, dist.dist_caux, dist.comu_pk
                FROM tnht_dist dist WHERE dist.dist_lmod > p_lmod
                ORDER BY dist.dist_lmod;
        WHEN p_name = 'TNHT_CLIE' THEN
            OPEN p_cursor0 FOR
                'SELECT ''M'' as change_operation, benti.enti_lmod as change_lastmodified,
                benti.benti_pk as clie_pk,
                CASE WHEN enti.enti_pk is not null THEN benti.benti_desc
                     ELSE clie.cont_apel || '', '' || benti.benti_desc END as clie_nome,
                CASE WHEN enti.enti_pk is null THEN 11 ELSE 211 END as clie_type,
                benti.enti_caux as clie_caux, benti.enti_lock as clie_lock,
                benti.fisc_numb, benti.fisc_addr1, benti.fisc_addr2, benti.fisc_loca,
                benti.fisc_copo, benti.fisc_door, benti.fisc_dist, benti.email_addr,
                benti.home_phone, benti.cell_phone, benti.regi_fisc, benti.enti_fino, benti.home_door,
                (SELECT doci.doci_iden FROM tnht_doci doci WHERE doci.doci_pk = benti.docu_iden) as clie_iden,
                (SELECT doci.doci_iden FROM tnht_doci doci WHERE doci.doci_pk = benti.docu_pass) as clie_pass,
                (SELECT doci.doci_iden FROM tnht_doci doci WHERE doci.doci_pk = benti.docu_driv) as clie_driv,
                (SELECT doci.doci_iden FROM tnht_doci doci WHERE doci.doci_pk = benti.docu_resd) as clie_resd,
                CASE WHEN enti.enti_pk is not null THEN enti.naci_pk ELSE clie.naci_pk END as naci_pk,
                ''0'' as clie_deau, benti.fisc_cotr, benti.fisc_corf
              FROM tnht_benti benti
              LEFT JOIN tnht_enti enti ON enti.enti_pk = benti.benti_pk
              LEFT JOIN tnht_clie clie
              ON clie.clie_pk = benti.benti_pk
              WHERE (enti.enti_pk is not null OR clie.clie_pk is not null)     
              AND benti.enti_lmod > :lmod 
              ORDER BY benti.enti_lmod
              FETCH FIRST :nrows ROWS ONLY'
                USING p_lmod, p_count;
        ELSE
            NULL;
        END CASE;
END;
/

BEGIN
    MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.SYNC_VERS = '20240531'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.SYNC_VERS) VALUES (1, '20240531');
END;
/

COMMIT;
