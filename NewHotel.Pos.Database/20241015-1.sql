alter table tnht_ipos add ipos_aivd varchar2(1) default '0' not null;
comment on column tnht_ipos.ipos_aivd is 'flag: Ask Invoice Data';

BEGIN
    MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN
        UPDATE SET PARA.DATE_VERS = '20241015'
    WHEN NOT MATCHED THEN
        INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20241015');
    COMMIT;
END;
/