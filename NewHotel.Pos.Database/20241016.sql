alter table tnht_pvar add pvar_autk varchar2(1) default '0' not null;
comment on column tnht_pvar.pvar_autk is'Flag: Automatically adds the product to the ticket in this stand.';

BEGIN
    MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN
        UPDATE SET PARA.DATE_VERS = '20241016'
    WHEN NOT MATCHED THEN
        INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20241016');
    COMMIT;
END;
/