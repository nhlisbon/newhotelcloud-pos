CREATE OR REPLACE VIEW SYNC_TNHT_HOTE AS
SELECT HOTE.ROWID AS ROW_ID,
       HOTE.HOTE_PK,
       HOTE.HOTE_ABRE,
       HOTE.HOTE_DESC,
       HOTE.FISC_NUMB,
       HOTE.REGI_FISC,
       HOTE.HOME_PHONE,
       HOTE.HOME_ADDR1,
       HOTE.HOME_ADDR2,
       HOTE.HOME_LOCA,
       HOTE.HOME_DOOR,
       HOTE.HOME_COPO,
       HOTE.HOME_DIST,
       HOTE.HOTE_CAUX,
       HOTE.EMAIL_ADDR,
       TCFG.NACI_PK,
       FACT.REGI_CAUX,
       TCFG.PAYS_INTALL
FROM TNHT_HOTE HOTE
         INNER JOIN TCFG_HOTE TCFG ON TCFG.HOTE_PK = HOTE.HOTE_PK
         INNER JOIN TCFG_FACT FACT ON FACT.HOTE_PK = HOTE.HOTE_PK;
/

CREATE OR REPLACE FORCE VIEW SYNC_TNHT_FORE AS
SELECT FORE.ROWID     AS ROW_ID,
       FORE.FORE_PK,
       FORE.HOTE_PK,
       FORE.LITE_ABRE,
       FORE.LITE_DESC,
       FORE.UNMO_PK,
       FORE.FORE_CASH,
       FORE.FORE_CACR,
       FORE.FORE_CRED,
       FORE.FORE_DEBI,
       FORE.REPO_CASH,
       FORI.FORI_PATH AS FORE_PATH,
       FORE.FORE_ORDE,
       FORE.FORE_CAUX,
       FORE.FORE_INAC,
       FORE.FORE_INVO,
       FORE.FORE_TICK,
       FORE.PAYS_ORIGIN
FROM TNHT_FORE FORE
         INNER JOIN TNHT_FOAP FOAP ON FOAP.FORE_PK = FORE.FORE_PK
         INNER JOIN TCFG_TEMP H1 ON H1.HOTE_PK = FORE.HOTE_PK
         INNER JOIN TNHT_FORI FORI ON FORI.FORE_PK = FORE.FORE_PK
WHERE FOAP.APPL_PK = 107;
/

BEGIN
    MERGE INTO TCFG_GENE PARA
    USING DUAL
    ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN
        UPDATE SET PARA.SYNC_VERS = '20240910'
    WHEN NOT MATCHED THEN
        INSERT (PARA.GENE_PK, PARA.SYNC_VERS) VALUES (1, '20240910');
    COMMIT;
END;
/