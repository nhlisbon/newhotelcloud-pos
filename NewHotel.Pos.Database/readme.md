# Instructions

- This is a repository for storing SQL scripts for the database. The scripts are organized by date and type of script. So far, the types of scripts are:
    - Local database
    - Sync / view
    - PMS / Cloud / Backoffice Pos 
- The scripts for sync and pms are only necessary if you want them to be synced with the database in the cloud
- The columns: **DATE_VERS** and **SYNC_VERS** are **Varchar2(8)** so the only valid format is **YYYYMMDD**, in case of the having more than one script for the same day, we combine them both into a single one with the same file name, but do not repeat the **Last Section** part of the script.
- The scripts for **PMS / Cloud / Backoffice Pos** are stored in the git repository [Html5 Database](https://github.com/NewHotel-Software/html5-database) and not in this **project**  and we put them in the **upload** folder.
- The last file name of the scripts should be the same version of **POS**

# Script template for local database

### filename:
yyyymmdd.sql

### last section:

````sql
    BEGIN
        MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
        WHEN MATCHED THEN
            UPDATE SET PARA.DATE_VERS = 'yyyymmdd'
        WHEN NOT MATCHED THEN
            INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, 'yyyymmdd');
        COMMIT;
    END;
    /
````

### examples:

- 20240625.sql
- 20240626.sql

# Script template for sync/view

### filename:
yyyymmdd.pms.sync.sql

### last section:

```sql
    BEGIN    
        MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
            WHEN MATCHED THEN UPDATE SET PARA.SYNC_VERS = 'yyyymmdd'
            WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.SYNC_VERS) VALUES (1, 'yyyymmdd');
        COMMIT;
    END;
    /
```

### examples:

- 20240625.pms.sync.sql
- 20240626.pms.sync.sql

# Script template for PMS

### filename: 			
yyyymmdd.[branch].[schema].[reference].sql

### origin branch:			
develop/release/hotfix

### schema:
ct/ht

### reference:
yyyymmdd version (release/hotfix) caso necesario

### last section:

```sql
begin
    merge into tcfg_gene para using dual on (para.gene_pk = 1) 
        when matched then update set para.date_vers = 'yyyymmdd' 
        when not matched then insert (para.gene_pk, para.date_vers) values (1, 'yyyymmdd'); 
    commit; 
end; 
/
```

 ### examples:

- 20231210.develop.ht.sql
- 20231220.develop.ht.sql
- 20231224.release.ht.20231218.sql
- 20231225.release.ct.20231218.sql
- 20231226.hotfix.ht.20231030.sql
- 20231227.hotfix.ht.20231030.sql
