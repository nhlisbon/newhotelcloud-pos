alter table tnht_tick add tick_pdde char(1) default '0' not null;
comment on column tnht_tick.tick_pdde is'Flag: Print discount description.';

BEGIN
    MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN
        UPDATE SET PARA.DATE_VERS = '20241031'
    WHEN NOT MATCHED THEN
        INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20241031');
    COMMIT;
END;
/