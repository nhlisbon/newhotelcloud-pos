ALTER TABLE TCFG_NPOS ADD NPOS_FCON VARCHAR(1) DEFAULT '0' NOT NULL;
COMMENT ON COLUMN TCFG_NPOS.NPOS_FCON  IS 'Skips the fill of fiscal data step and always puts final consumer in it''s place';

alter table tnht_vend add camb_unmo varchar2(3);
alter table tnht_vend add constraint NHT_FK_UNMO_VEND_02 foreign key (camb_unmo) references tnht_unmo (unmo_pk);

create index nht_ind_vend_camb on tnht_vend (camb_unmo);
alter table tnht_vend add camb_valo number default 1 not null;

comment on column tnht_vend.camb_unmo is 'Moneda a cambiar, ref. tnht_unmo(unmo_pk)';
comment on column tnht_vend.camb_valo is 'Factor de cambio';

alter table tnht_arve add arve_vame number default 0;
comment on column tnht_arve.arve_vame is 'Valor en moneda de cambio del ticket';

alter table tnht_pave add pave_vame number default 0;
comment on column tnht_pave.pave_vame is 'Valor en moneda de cambio del ticket';

BEGIN
    MERGE INTO TCFG_GENE PARA
    USING DUAL
    ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN
        UPDATE SET PARA.DATE_VERS = '20240625'
    WHEN NOT MATCHED THEN
        INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20240625');
END;
/

COMMIT;