ALTER TABLE TNHT_CLIE
    ADD FISC_COTR VARCHAR2(255 CHAR)
    ADD FISC_CORF VARCHAR2(255 CHAR);
COMMENT ON COLUMN TNHT_CLIE.FISC_COTR IS 'Free code for the fiscal regime type';
COMMENT ON COLUMN TNHT_CLIE.FISC_COTR IS 'Free code for the fiscal responsibility';

BEGIN
    MERGE INTO TCFG_GENE PARA
    USING DUAL
    ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN
        UPDATE SET PARA.DATE_VERS = '20240531'
    WHEN NOT MATCHED THEN
        INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20240531');
END;
/

COMMIT;