CREATE OR REPLACE TRIGGER CHANGE_NOTIFICATION_TNHT_ARVE
    FOR INSERT OR UPDATE OR DELETE
    ON TNHT_ARVE
    COMPOUND TRIGGER
    TYPE t_change_notification IS TABLE OF change_notification%ROWTYPE;
    l_change_notification t_change_notification := t_change_notification();
    v_change_id INTEGER;
AFTER EACH ROW IS
BEGIN    
      SELECT change_notification_id.NEXTVAL INTO v_change_id FROM DUAL;
      l_change_notification.EXTEND;
      l_change_notification(l_change_notification.LAST).change_id := v_change_id;
      l_change_notification(l_change_notification.LAST).change_table_name := 'TNHT_ARVE';
      l_change_notification(l_change_notification.LAST).change_operation := 'M';
      l_change_notification(l_change_notification.LAST).change_row_id := :NEW.ROWID;
END AFTER EACH ROW;

    AFTER STATEMENT IS
    BEGIN
        FORALL v_index IN l_change_notification.FIRST..l_change_notification.LAST
            INSERT INTO change_notification (change_id, change_table_name, change_operation, change_row_id,
                                             change_delete_pk)
            VALUES (l_change_notification(v_index).change_id, l_change_notification(v_index).change_table_name,
                    l_change_notification(v_index).change_operation, l_change_notification(v_index).change_row_id,
                    l_change_notification(v_index).change_delete_pk);
    END AFTER STATEMENT;
    END;
/


CREATE OR REPLACE FORCE VIEW SYNC_TNHT_ARVE
(ROW_ID, ARVE_PK, ARVE_ANUL, ANUL_PAID)
BEQUEATH DEFINER
AS 
SELECT ROWID AS ROW_ID,
 ARVE_PK,
 ARVE_ANUL,
 ANUL_PAID
 FROM TNHT_ARVE WHERE ARVE_ANUL = 1;
/


BEGIN
    MERGE INTO TCFG_GENE PARA
    USING DUAL
    ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN
        UPDATE SET PARA.SYNC_VERS = '20241115'
    WHEN NOT MATCHED THEN
        INSERT (PARA.GENE_PK, PARA.SYNC_VERS) VALUES (1, '20241115');
    COMMIT;
END;
/