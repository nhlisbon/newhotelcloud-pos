ALTER TABLE TCFG_NPOS ADD NPOS_SPLT VARCHAR(1) DEFAULT '0';
COMMENT ON COLUMN TCFG_NPOS.NPOS_SPLT IS 'Allows the user to automatically split the ticket if it has a alcohol product';

CREATE OR REPLACE FORCE VIEW SYNC_TCFG_NPOS
AS 
SELECT NPOS.ROWID AS ROW_ID,
       NPOS.HOTE_PK,
       NPOS.FAST_PAYM,
       NPOS.PARA_FORE,
       NPOS.PARA_IVIN,
       NPOS.PARA_ESIM,
       NPOS.PARA_TICK,
       NPOS.PARA_MTCO,
       NPOS.PARA_TICO,
       NPOS.PARA_NPAS,
       NPOS.PARA_TISE,
       NPOS.PARA_TIPE,
       NPOS.PARA_TIVL,
       NPOS.PARA_TIAT,
       NPOS.OPEN_PLAN,
       NPOS.COIN_VALO,
       NPOS.DESC_VALO,
       NPOS.TICK_FISC,
       NPOS.DATR_DASY,
       NPOS.NPOS_TIFI,
       NPOS.NPOS_NCHC,
       NPOS.NPOS_FAON,
       NPOS.NPOS_HBOL,
       NPOS.NPOS_HSCO,
       NPOS.NPOS_DEAU,
       NPOS.SIGN_TIPO,
       NPOS.TICK_MAFN,
       FACT.SIGN_VERS,
       CASE WHEN FDES.FDES_TEST IS NULL THEN '1' ELSE FDES.FDES_TEST END AS SIGN_TEST,
       FDES.FDES_USER AS FISC_USER,
       FDES.FDES_PSWD AS FISC_PASW,
       FACT.ANUL_DATR,
       FACT.ANUL_MONTH,
       FACT.ANUL_DAYS,
       NPOS.FAST_PLAN,
       NPOS.PLAN_ROOM,
       NPOS.EMIT_SOTK,
       NPOS.NPOS_TIME,
       NPOS.PROD_FTQT,
       NPOS.NPOS_IBTN,
       NPOS.NPOS_ASGN,
       NPOS.NPOS_ASEP,
       NPOS.NPOS_SPLT
FROM TCFG_NPOS NPOS
     INNER JOIN TCFG_FACT FACT ON FACT.HOTE_PK = NPOS.HOTE_PK
     LEFT JOIN TCFG_FDES FDES ON FACT.HOTE_PK = FDES.HOTE_PK AND FDES.FDES_PK = 3;

begin
  merge into tcfg_gene para using dual on (para.gene_pk = 1)
    when matched then update set para.date_vers = '20240411-001', para.rele_vers = '20240411-001',para.htfx_vers = '20240411-001'
    when not matched then insert (para.gene_pk, para.date_vers, para.rele_vers, para.htfx_vers) values (1, '20240411-001', '20240411-001', '20240411-001');
  commit;
end;
/