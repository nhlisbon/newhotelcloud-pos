create table tnht_inte (
    inte_pk raw(16),
    inte_name varchar2(255) not null,
    inte_type varchar2(255) not null,
    inte_acti varchar2(1) default '0' not null,
    inte_lmod timestamp default current_timestamp not null,
    INTE_CONFIG CLOB NOT NULL,
    constraint nht_pk_inte primary key (inte_pk)
);

comment on column tnht_inte.inte_pk is 'Primary key for the table';
comment on column tnht_inte.inte_name is 'The name of the integration';
comment on column tnht_inte.inte_type is 'The integration type (Stocks, Reports, Payments, etc)';
comment on column tnht_inte.inte_config is 'Configuration for this integration in a json string';
comment on column tnht_inte.inte_acti is 'If the integration is active or not';
comment on column tnht_inte.inte_lmod is 'Last modification date of the record';

create table tnht_arin (
    arin_pk raw(16),
    artg_pk raw(16) not null,
    inte_pk raw(16) not null,
    arin_code varchar2(255) not null,
    arin_opcd_one varchar2(255),
    arin_opcd_two varchar2(255),
    arin_lmod timestamp default current_timestamp not null,
    constraint nht_pk_arin primary key (arin_pk),
    constraint nht_fk_artg_arin foreign key (artg_pk)
      references tnht_artg (artg_pk)
      on delete cascade,
    constraint nht_fk_inte_arin foreign key (inte_pk)
      references tnht_inte (inte_pk)
      on delete cascade
);

comment on column tnht_arin.arin_pk is 'Primary key for the table';
comment on column tnht_arin.artg_pk is 'Foreign key to the parent table that holds the product in our system';
comment on column tnht_arin.arin_code is 'Unique and required code for the product in the system of the integrator';
comment on column tnht_arin.arin_opcd_one is 'Optional code for the product in the system of the integrator';
comment on column tnht_arin.arin_opcd_two is 'Optional code for the product in the system of the integrator';
comment on column tnht_arin.inte_pk is 'Integration key of the integrator';
comment on column tnht_arin.arin_lmod is 'Last modification date of the record';

BEGIN
    MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN
        UPDATE SET PARA.DATE_VERS = '20240930'
    WHEN NOT MATCHED THEN
        INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20240930');
    COMMIT;
END;
/