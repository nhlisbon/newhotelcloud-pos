﻿MERGE INTO TNHT_LITE USING DUAL ON (LITE_PK = '4CE3F87007624E0180072541F3841E3B')
WHEN NOT MATCHED THEN INSERT (LITE_PK) VALUES ('4CE3F87007624E0180072541F3841E3B');

MERGE INTO TNHT_MULT USING DUAL ON (LITE_PK = '4CE3F87007624E0180072541F3841E3B' AND LANG_PK = 1033)
WHEN NOT MATCHED THEN INSERT (LITE_PK, LANG_PK, MULT_DESC) VALUES ('4CE3F87007624E0180072541F3841E3B', 1033, 'ExFactura Uruguay');
MERGE INTO TNHT_MULT USING DUAL ON (LITE_PK = '4CE3F87007624E0180072541F3841E3B' AND LANG_PK = 3082)
WHEN NOT MATCHED THEN INSERT (LITE_PK, LANG_PK, MULT_DESC) VALUES ('4CE3F87007624E0180072541F3841E3B', 3082, 'ExFactura Uruguay');
MERGE INTO TNHT_MULT USING DUAL ON (LITE_PK = '4CE3F87007624E0180072541F3841E3B' AND LANG_PK = 2070)
WHEN NOT MATCHED THEN INSERT (LITE_PK, LANG_PK, MULT_DESC) VALUES ('4CE3F87007624E0180072541F3841E3B', 2070, 'ExFactura Uruguay');
MERGE INTO TNHT_MULT USING DUAL ON (LITE_PK = '4CE3F87007624E0180072541F3841E3B' AND LANG_PK = 1036)
WHEN NOT MATCHED THEN INSERT (LITE_PK, LANG_PK, MULT_DESC) VALUES ('4CE3F87007624E0180072541F3841E3B', 1036, 'ExFactura Uruguay');
MERGE INTO TNHT_MULT USING DUAL ON (LITE_PK = '4CE3F87007624E0180072541F3841E3B' AND LANG_PK = 1049)
WHEN NOT MATCHED THEN INSERT (LITE_PK, LANG_PK, MULT_DESC) VALUES ('4CE3F87007624E0180072541F3841E3B', 1049, 'ExFactura Uruguay');
MERGE INTO TNHT_MULT USING DUAL ON (LITE_PK = '4CE3F87007624E0180072541F3841E3B' AND LANG_PK = 1046)
WHEN NOT MATCHED THEN INSERT (LITE_PK, LANG_PK, MULT_DESC) VALUES ('4CE3F87007624E0180072541F3841E3B', 1046, 'ExFactura Uruguay');

MERGE INTO TNHT_ENUM USING DUAL ON (ENUM_PK = 2623 AND LITE_PK = '4CE3F87007624E0180072541F3841E3B')
WHEN NOT MATCHED THEN INSERT (ENUM_PK, LITE_PK) VALUES (2623, '4CE3F87007624E0180072541F3841E3B');

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20221114'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20221114');
END;
/

COMMIT;