﻿--changed by Oscar on 12/10/2016

create or replace view vnht_nfce_pdvs
(
    pdv_codigo,
    pdv_abreviatura,
    pdv_descricao
)
as
(
    select      ipos.ipos_pk,
                (select mult_desc from vnht_mult where lite_pk = ipos.lite_abre and lang_pk = 1046) ipos_abre,
                (select mult_desc from vnht_mult where lite_pk = ipos.lite_desc and lang_pk = 1046) ipos_desc
    from        tnht_ipos ipos
)
;

create or replace view vnht_nfce_vendas
(
    venda_chave,
    venda_codigo,
    venda_serie,
    venda_data,
    venda_hora,
    hotel_cnpj,
    hotel_ie,
    hotel_razao,
    hotel_nome,
    hotel_fone,
    hotel_logradouro,
    hotel_numero,
    hotel_complemento,
    hotel_barrio,
    hotel_cep,
    hotel_ibge,
    hotel_cidade,
    hotel_uf,
    cliente_cpf,
    cliente_nome,
    cliente_pais,
	cliente_fone,
    cliente_logradouro,
    cliente_numero,
    cliente_complemento,
    cliente_barrio,
    cliente_cep,
    cliente_ibge,
    cliente_cidade,
    cliente_uf,
    cliente_ie,
    cliente_email
)
as
(
    select      vend.vend_pk, vend.vend_codi, vend.vend_seri, vend.vend_datf, vend.vend_horf, hote.fisc_numb, hote.regi_fisc,
				hote.hote_desc, hote.hote_desc, hote.home_phone, hote.home_addr1, hote.home_door, hote.home_addr2,
				hote.home_loca, hote.home_copo, dist.dist_caux, dist.dist_desc, comu.comu_caux, 
				vend.fact_nuco, vend.fact_guest, naci.naci_pk, clie.home_phone, vend.fact_addr, clie.home_door, clie.fisc_addr2, 
				clie.fisc_loca, clie.fisc_copo, cdist.dist_caux, cdist.dist_desc, ccomu.comu_caux, clie.regi_fisc, clie.email_addr
	from        tnht_vend vend, tnht_hote hote, tnht_dist dist, tnht_comu comu, tnht_clie clie, tnht_dist cdist, tnht_comu ccomu, tnht_naci naci
	where       vend.hote_pk = hote.hote_pk
	and         hote.home_dist = dist.dist_pk (+)
	and         dist.comu_pk = comu.comu_pk (+)
	and         vend.clie_pk = clie.clie_pk (+)
	and         clie.fisc_dist = cdist.dist_pk (+)
	and         cdist.comu_pk = ccomu.comu_pk (+)
	and         clie.naci_pk = naci.naci_pk (+)
)
;
create or replace view vnht_nfce_artigos
(
    venda_chave,
    venda_codigo,
    venda_serie,
    artigo_codigo,
    artigo_descripcao,
    artigo_ncm,
    artigo_cfop,
    artigo_unidade,
    artigo_cantidade,
    artigo_valor_unitario,
    artigo_anulado,
    artigo_anulado_pagado,
    artigo_valor_total,
    artigo_valor_desconto,
    artigo_cest,
    icms_cst,
    icms_base,
    icms_base_reduzida,
    icms_aliquota,
    icms_valor,
    icms_valor_reduzido,
    pis_cst,
    pis_base,
    pis_aliquota,
    pis_valor,
    cofins_cst,
    cofins_base,
    cofins_aliquota,
    cofins_valor
)
as
(
    select       vend.vend_pk, vend.vend_codi, vend.vend_seri, artg.artg_codi, (select mult_desc from vnht_mult where lite_pk = artg.lite_desc and lang_pk = 1046) as artg_desc, artg.artg_cncm,
                 artg.artg_cfop, artg.artg_unme, arve.arve_qtds, case when arve_qtds = 0 then 0 else arve.arve_vsde / arve.arve_qtds end arve_qtds, 
                 case when arve_anul in (0, 2) then 0 else 1 end arve_anul, arve.anul_paid,
                 round(arve.arve_totl, 2) arve_totl, arve_desc, null, artg.artg_csti, 
                 round(arve.arve_totl, 2) arve_totl,
                 round(case when artg.artg_csti like '%00' then arve.arve_totl * 0.4118 else arve.arve_totl end, 2) as base_reduc, 
                 arve.arve_por1, round(arve.arve_ivas, 2) arve_ivas, 
                 round(case when artg.artg_csti like '%00' then arve.arve_ivas * 0.4118 else arve.arve_ivas end, 2) as valor_reduc, 
                 artg.artg_cstp, round(arve.arve_totl, 2) arve_totl, arve.arve_por2, round(arve.arve_iva2, 2) arve_iva2, artg.artg_cstc, round(arve.arve_totl, 2) arve_totl, arve.arve_por3, round(arve_iva3, 2) arve_iva3
    from        tnht_artg artg, tnht_arve arve, tnht_vend vend, tnht_hote hote, tnht_clie clie
    where       artg.artg_pk = arve.artg_pk
    and         arve.vend_pk = vend.vend_pk
    and         vend.hote_pk = hote.hote_pk
    and         vend.clie_pk = clie.clie_pk (+)
)   order by    arve_item asc
;
create or replace view vnht_nfce_pagamentos
(
    venda_chave,
    venda_codigo,
    venda_serie,
    pago_valor,
    pago_tipo,
    pago_nome,
    pago_cartao,
    pago_codigo,
    pago_nota
)
as
(
    select          vend.vend_pk, vend.vend_codi, vend.vend_seri, pave.pave_valo,
                    (select mult_desc from vnht_mult where lite_pk = tire_enum.lite_pk and lang_pk = 1046) as pave_tipo,
                    (select mult_desc from vnht_mult where lite_pk = fore.lite_desc and lang_pk = 1046) as fore_desc,
                    (select mult_desc from vnht_mult where lite_pk = cacr.lite_pk and lang_pk = 1046) as cacr_desc,
                    fore.fore_caux, pave.pave_obsv
    from            tnht_vend vend, tnht_pave pave, tnht_enum tire_enum, tnht_fore fore, tnht_tacr tacr, tnht_cacr cacr
    where           vend.vend_pk = pave.vend_pk
    and             pave.tire_pk = tire_enum.enum_pk
    and             pave.fore_pk = fore.fore_pk (+)
    and             pave.tacr_pk = tacr.tacr_pk (+)
    and             tacr.cacr_pk = cacr.cacr_pk (+)
)
;
CREATE SEQUENCE INTEGRACAONFCE_SEQ START WITH 1 INCREMENT BY 1;
CREATE TABLE INTEGRACAONFCE
(
    ID NUMBER NOT NULL,
    CNPJ CHAR(14) NOT NULL,
	PDVCODIGO RAW(16) NOT NULL,
    PDVNAME VARCHAR2(255) NOT NULL,
    IDREF VARCHAR2(32) NOT NULL,
    DTTIMECREATION DATE DEFAULT SYSDATE,
    FLGSTATUS CHAR NOT NULL,
    RETURNMSG VARCHAR2(500),
    NFCEKEY VARCHAR2(44),
	NFCEPROTOCOL VARCHAR2(20),
	NFCEPROTOCOLCANC VARCHAR2(20),
	NUMNOTA NUMBER,
	SERIE NUMBER,
    FILEJSON LONG RAW,
	NFE CHAR(1) DEFAULT 'N',
	LOCKREG VARCHAR2(255),
    CONSTRAINT XPKINTEGRACAONFCE PRIMARY KEY (ID)
);
CREATE PUBLIC SYNONYM INTEGRACAONFCE FOR INTEGRACAONFCE;
CREATE OR REPLACE TRIGGER TRG_INTNFCE_BFR_INSRT
    BEFORE INSERT ON INTEGRACAONFCE
    FOR EACH ROW
    WHEN (new.ID IS NULL)
    BEGIN
        SELECT INTEGRACAONFCE_SEQ.NEXTVAL
        INTO :new.ID
        FROM dual;
    END;
/
CREATE TABLE SRVCNFCE (IDSRVCNFCE VARCHAR2(15) NOT NULL, TYPESRVC CHAR(1) NOT NULL, CNPJ CHAR(14), PDVNAME VARCHAR2(15), CONSTRAINT XPKSRVCNFCE PRIMARY KEY (IDSRVCNFCE));
CREATE PUBLIC SYNONYM SRVCNFCE FOR SRVCNFCE;


CREATE OR REPLACE TRIGGER TNHT_VEND_NFCE
    AFTER UPDATE ON TNHT_VEND REFERENCING NEW AS NEW OLD AS OLD
        FOR EACH ROW
WHEN (
--JJ sync condition
            ((OLD.VEND_SERI IS NULL AND NEW.VEND_SERI IS NOT NULL AND NEW.VEND_HORF IS NOT NULL) OR
            (OLD.VEND_HORF IS NULL AND NEW.VEND_HORF IS NOT NULL AND NEW.VEND_SERI IS NOT NULL) OR
            (NEW.VEND_SERI IS NOT NULL AND NEW.VEND_HORF IS NOT NULL AND OLD.VEND_ANUL = '0' AND NEW.VEND_ANUL = '1')
            )
      )
DECLARE

        CURSOR CR IS SELECT IDREF, NVL(NFCEKEY, ' ') FROM INTEGRACAONFCE WHERE IDREF = :OLD.VEND_PK;

        CNPJ VARCHAR2(255);
        PDV  VARCHAR2(255);
        PDVCODIGO  RAW(16);
        IDREFERENCIA VARCHAR2(255);
        NFCECHAVE VARCHAR2(255);
        GERANFE CHAR(1);
        TRANSFER CHAR(1);

        BEGIN
            SELECT       NVL(FISC_NUMB, ' ') INTO CNPJ FROM TNHT_HOTE;
            SELECT       CASE WHEN :NEW.CLIE_PK IS NULL THEN 'N' ELSE ( SELECT CASE WHEN CLIE.ENTI_FINO = '0' THEN 'N' ELSE 'S' END FROM TNHT_CLIE CLIE WHERE CLIE_PK = :NEW.CLIE_PK) END INTO GERANFE FROM DUAL;
            SELECT       (SELECT MULT_DESC FROM VNHT_MULT WHERE LITE_PK = IPOS.LITE_DESC AND LANG_PK = 1046), IPOS_PK INTO PDV, PDVCODIGO
            FROM         TNHT_IPOS IPOS
            WHERE        IPOS_PK = :NEW.IPOS_PK;
            --check if was transfer to ignore nfce behaviour
            SELECT  CASE WHEN EXISTS (SELECT 1 FROM TNHT_PAVE WHERE TIRE_PK = 2140 AND VEND_PK = :NEW.VEND_PK) THEN '1' ELSE '0' END INTO TRANSFER FROM DUAL;
            
            IF (TRANSFER = '1')
            THEN
            
                --close ticket standard
                IF (:OLD.VEND_SERI IS NULL AND :NEW.VEND_SERI IS NOT NULL AND :NEW.VEND_ANUL = '0') THEN
                    INSERT INTO INTEGRACAONFCE (CNPJ, PDVCODIGO, PDVNAME, IDREF, FLGSTATUS, NFE) SELECT CNPJ, PDVCODIGO, PDV, :NEW.VEND_PK, 'A', GERANFE FROM DUAL;
                --close ticket already emitted with anul
                ELSIF (:OLD.VEND_SERI IS NOT NULL AND :NEW.VEND_ANUL = '1') THEN
                    OPEN CR;
                    FETCH CR INTO IDREFERENCIA, NFCECHAVE;
                    CLOSE CR;
                    INSERT INTO INTEGRACAONFCE (CNPJ, PDVCODIGO, PDVNAME, IDREF, FLGSTATUS, NFCEKEY, NFE) SELECT CNPJ, PDVCODIGO, PDV, :NEW.VEND_PK, 'C', NFCECHAVE, GERANFE FROM DUAL;
                END IF;
                --close ticket anulled at first (ignore)
            
            END IF;
        END;
/

CREATE OR REPLACE TRIGGER INTEGRACAONFCE_NOTA
AFTER UPDATE OF RETURNMSG ON INTEGRACAONFCE
FOR EACH ROW
    BEGIN
        UPDATE TNHT_VEND SET VEND_NOTA = :NEW.RETURNMSG WHERE VEND_PK = HEXTORAW(:NEW.IDREF);
    END;
/

CREATE OR REPLACE TRIGGER INTEGRACAONFCE_STATUS
AFTER UPDATE OF FLGSTATUS ON INTEGRACAONFCE
FOR EACH ROW
	BEGIN
		UPDATE TNHT_VEND SET FISC_PEND = (CASE WHEN :NEW.FLGSTATUS = 'T' THEN '0' ELSE '1' END) WHERE VEND_PK = HEXTORAW(:NEW.IDREF);
	END;
/
