create or replace view vnht_nfce_pdvs
(
    pdv_codigo,
    pdv_abreviatura,
    pdv_descricao
)
as
(
    select      ipos.ipos_pk, 
                (select mult_desc from vnht_mult where lite_pk = ipos.lite_abre and lang_pk = 1046) ipos_abre,
                (select mult_desc from vnht_mult where lite_pk = ipos.lite_desc and lang_pk = 1046) ipos_desc
    from        tnht_ipos ipos
)
;


alter table integracaonfce rename column pdvname to pdvname_old;
alter table integracaonfce add pdvname varchar(255);
alter table integracaonfce add pdvcodigo raw(16);

update integracaonfce set pdvname = pdvname_old;
update integracaonfce set pdvcodigo = (select pdv_codigo from vnht_nfce_pdvs where pdv_descricao = pdvname_old);

alter table integracaonfce modify (pdvname not null);
alter table integracaonfce modify (pdvcodigo not null);

alter table integracaonfce drop column pdvname_old;  
alter table integracaonfce add nfe char(1) default 'N';
alter table integracaonfce add lockreg varchar2(255);

create or replace trigger tnht_vend_nfce 
    after update on tnht_vend referencing new as new old as old
        for each row
when (
--JJ sync condition
            ((old.vend_seri is null and new.vend_seri is not null and new.vend_horf is not null) or
            (old.vend_horf is null and new.vend_horf is not null and new.vend_seri is not null) or
            (new.vend_seri is not null and new.vend_horf is not null and old.vend_anul = '0' and new.vend_anul = '1'))
      )
declare

        cursor cr is select idref, nvl(nfcekey, ' ') from integracaonfce where idref = :old.vend_pk;
        
        cnpj varchar2(255);
        pdv  varchar2(255);
        pdvcodigo  raw(16);
        idreferencia varchar2(255);
        nfcechave varchar2(255);
        
        begin
            select       nvl(fisc_numb, ' ') into cnpj from tnht_hote;
            select       (select mult_desc from vnht_mult where lite_pk = ipos.lite_desc and lang_pk = 1046), ipos_pk into pdv, pdvcodigo
            from         tnht_ipos ipos 
            where        ipos_pk = :new.ipos_pk;
            
            --close ticket standard
            if (:old.vend_seri is null and :new.vend_seri is not null and :new.vend_anul = '0') then 
                insert into integracaonfce (cnpj, pdvcodigo, pdvname, idref, flgstatus) select cnpj, pdvcodigo, pdv, :new.vend_pk, 'A' from dual;
            --close ticket already emitted with anul
            elsif (:old.vend_seri is not null and :new.vend_anul = '1') then 
                open cr;
                fetch cr into idreferencia, nfcechave;
                close cr;
                insert into integracaonfce (cnpj, pdvname, idref, flgstatus, nfcekey) select cnpj, pdv, :new.vend_pk, 'C', nfcechave from dual;
            end if;    
            --close ticket anulled at first (ignore)            
        end;
/
