﻿alter table tnht_vend add (vend_nota  varchar2(2000));
comment on column tnht_vend.vend_nota is 'Nota asociada al ticket';

create or replace trigger integracaonfce_nota
after update of RETURNMSG on integracaonfce
for each row
    begin
        update tnht_vend set vend_nota = :new.returnmsg where vend_pk = hextoraw(:new.idref);
    end;
/

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20170517'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20170517');
END;
/

COMMIT;