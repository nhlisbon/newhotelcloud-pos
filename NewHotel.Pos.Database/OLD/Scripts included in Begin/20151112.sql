--date_vers 20141201
alter table tnht_arve add (arve_dacr date);
comment on column tnht_arve.arve_dacr is 'Creation date of the line';
update tnht_arve set arve_dacr = to_date('01/01/2015', 'dd/MM/yyyy');
alter table tnht_arve modify(arve_dacr  not null);

begin
  merge into tcfg_gene para using dual on (para.gene_pk = 1)
    when matched then update set para.date_vers = '20151112'
    when not matched then insert (para.gene_pk, para.date_vers) values (1, '20151112');
end;
/

commit;

