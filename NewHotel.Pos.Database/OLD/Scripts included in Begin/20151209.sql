﻿ALTER TABLE TNHT_SEDO ADD (SEDO_CONT  NUMBER(10) DEFAULT 1 NOT NULL);
COMMENT ON COLUMN TNHT_SEDO.SEDO_CONT IS 'Contador auxiliar';
ALTER TABLE TNHT_VEND MODIFY(VEND_SERI NULL);

CREATE TABLE TNHT_LKDE
(
  LKDE_PK    RAW(16)  NOT NULL,
  LKTB_PK    RAW(16)  NOT NULL,
  ARVE_QTDS  NUMBER   NOT NULL,
  ARVE_TOTL  NUMBER   NOT NULL,
  ARVE_VLIQ  NUMBER   NOT NULL,
  ARTG_PK    RAW(16)  NOT NULL
);
COMMENT ON TABLE TNHT_LKDE IS 'Detalle de consulta de mesa';
COMMENT ON COLUMN TNHT_LKDE.LKDE_PK IS 'Llave Primaria';
COMMENT ON COLUMN TNHT_LKDE.LKTB_PK IS 'Detalle de mesa ref tnht_lktb (lktb_pk)';
COMMENT ON COLUMN TNHT_LKDE.ARVE_QTDS IS 'Cantidad de la linea';
COMMENT ON COLUMN TNHT_LKDE.ARVE_TOTL IS 'Total de la linea';
COMMENT ON COLUMN TNHT_LKDE.ARVE_VLIQ IS 'Valor sin impuestos de la linea';
COMMENT ON COLUMN TNHT_LKDE.ARTG_PK IS 'Producto ref tnht_artg (artg_pk)';
ALTER TABLE TNHT_LKDE ADD ( CONSTRAINT NHT_PK_LKDE PRIMARY KEY (LKDE_PK));
ALTER TABLE TNHT_LKDE ADD ( CONSTRAINT NHT_FK_LKTB_LKDE FOREIGN KEY (LKTB_PK) REFERENCES TNHT_LKTB (LKTB_PK) ON DELETE CASCADE);
ALTER TABLE TNHT_LKDE ADD ( CONSTRAINT NHT_FK_ARTG_LKDE FOREIGN KEY (ARTG_PK) REFERENCES TNHT_ARTG (ARTG_PK));

ALTER TABLE TNHT_ARVE MODIFY(ARVE_IMPR NUMBER(4));
COMMENT ON COLUMN TNHT_ARVE.ARVE_IMPR IS 'Contador: Indica si ha sido impreso en la impresora de comanda';
COMMENT ON COLUMN TNHT_ARVE.ARVE_IMAR IS 'flag: Indica si ha sido marchado (1.si, 0.no) ';

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20151209'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20151209');
END;
/

COMMIT;