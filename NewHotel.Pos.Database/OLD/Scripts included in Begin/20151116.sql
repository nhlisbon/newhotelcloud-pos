alter table tnht_arve add (tide_pk raw(16));
comment on column tnht_arve.tide_pk is 'Discount Type Id (ref tnht_tide (tide_pk)';
alter table tnht_arve add constraint nht_fk_tide_arve foreign key (tide_pk) references tnht_tide (tide_pk);


begin
  merge into tcfg_gene para using dual on (para.gene_pk = 1)
    when matched then update set para.date_vers = '20151116'
    when not matched then insert (para.gene_pk, para.date_vers) values (1, '20151116');
end;
/

commit;

