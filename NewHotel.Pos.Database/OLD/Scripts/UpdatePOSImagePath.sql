UPDATE TNHT_ARIM SET ARIM_PATH = REPLACE(ARIM_PATH, 'C:\inetpub\wwwroot\Images\', '');
UPDATE TNHT_SALO SET MESA_FREE = REPLACE(MESA_FREE, 'C:\inetpub\wwwroot\Images\', '');
UPDATE TNHT_SALO SET MESA_OCUP = REPLACE(MESA_OCUP, 'C:\inetpub\wwwroot\Images\', '');
UPDATE TNHT_SALO SET SALO_IMAG = REPLACE(SALO_IMAG, 'C:\inetpub\wwwroot\Images\', '');
UPDATE TNHT_FORI SET FORI_PATH = REPLACE(FORI_PATH, 'C:\inetpub\wwwroot\Images\', '');
COMMIT;
