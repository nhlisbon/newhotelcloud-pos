﻿create table tcfg_gene (gene_pk number(1) not null, date_vers varchar2(8) not null, gene_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_cfg_gene primary key (gene_pk), 
constraint nht_check_cfg_gene check (gene_pk = 1));
comment on table tcfg_gene is 'Configuración general, tabla readonly de 1 solo registro';
comment on column tcfg_gene.gene_pk is 'Código configuración general';
comment on column tcfg_gene.date_vers is 'fecha de la ultima actualizacion a la base de datos en formato (yyyyMMdd)';

create table tnht_lang (lang_pk number(10) not null, lang_prio number(4) not null, constraint nht_pk_lang primary key (lang_pk));
comment on table tnht_lang is 'Idiomas con traducciones ';
comment on column tnht_lang.lang_pk is 'Código del idioma con traducción hace referencia "teorica" a tnht_licl(licl_pk)';
comment on column tnht_lang.lang_prio is 'Prioridad Idioma';

create table tnht_lite (lite_pk raw(16) not null, constraint nht_pk_lite primary key (lite_pk));
comment on table tnht_lite is 'Literales';
comment on column tnht_lite.lite_pk is 'Código Literal';

create table tnht_mult (lite_pk raw(16) not null, lang_pk number(10) not null, mult_desc varchar2(255) not null, constraint nht_pk_mult primary key (lite_pk, lang_pk));
comment on table tnht_mult is 'Tabla general de traducciones';
comment on column tnht_mult.lite_pk is 'Código Literal, ref. tnht_lite(lite_pk), delete cascade';
comment on column tnht_mult.lang_pk is 'Código idioma asociado a la traducción, ref. tnht_lang(lang_pk)';
comment on column tnht_mult.mult_desc is 'Traducción/Descripción en el idioma asociado';
	
create table tnht_enum (enum_pk number(10) not null, lite_pk raw(16) not null, constraint nht_pk_enum primary key (enum_pk));
comment on table tnht_enum is 'Tabla de Enumerativos';
comment on column tnht_enum.enum_pk is 'Código enumerativo';
comment on column tnht_enum.lite_pk is 'Traducción descripción enumerador, ref. tnht_lite(lite_pk)';	

create table tnht_unmo (unmo_pk varchar2(3) not null, lite_pk raw(16) not null, unmo_ndec number(4) default 0 not null, constraint nht_pk_unmo primary key (unmo_pk));
comment on table tnht_unmo is 'Monedas';
comment on column tnht_unmo.unmo_pk is 'Código moneda';
comment on column tnht_unmo.lite_pk is 'Traducción nombre moneda, ref. tnht_lite(lite_pk)';
comment on column tnht_unmo.unmo_ndec is '# decimales ISO';

create table tnht_licl (licl_pk number(10) not null, lite_pk raw(16) not null, licl_prim char(1) default '0' not null, iso_639y1 varchar2(3) not null, iso_639y2 varchar2(3) not null, constraint nht_pk_licl primary key (licl_pk));
comment on table tnht_licl is 'Repository de Idiomas';
comment on column tnht_licl.licl_pk is 'Código idioma';
comment on column tnht_licl.lite_pk is 'Traducción nombre de idioma, ref. tnht_lite(lite_pk)';
comment on column tnht_licl.licl_prim is 'Idioma primario (1. si, 0. no)';
comment on column tnht_licl.iso_639y1 is 'ISO 639-1';
comment on column tnht_licl.iso_639y2 is 'ISO 639-2';

create table tnht_remu (remu_pk number(10) not null, lite_pk raw(16) not null, constraint nht_pk_remu primary key (remu_pk));
comment on table tnht_remu is 'Repository de Regiones del Mundo';
comment on column tnht_remu.remu_pk is 'Código región del mundo';
comment on column tnht_remu.lite_pk is 'Traducción nombre de región del mundo, ref. tnht_lite(lite_pk)';

create table tnht_naci (naci_pk varchar2(3) not null, lite_desc raw(16) not null, remu_pk number(10) not null, licl_pk number(10) not null, naci_caco varchar2(10), iso_3166 varchar2(3) not null, pt_sef varchar2(255) not null, naci_caux varchar(255), constraint nht_pk_naci primary key (naci_pk));
comment on table tnht_naci is 'Repository de Paises';
comment on column tnht_naci.naci_pk is 'Código País - ISO 3166 (alpha 2)';
comment on column tnht_naci.lite_desc is 'Traducción Nombre Pais';
comment on column tnht_naci.remu_pk is 'Código región del mundo a la que pertenece el país, ref. tnht_remu(remu_pk)';
comment on column tnht_naci.licl_pk is 'Código idioma oficial hablado en el país, ref. tnht_licl(licl_pk)';
comment on column tnht_naci.naci_caco is 'Código internacional telefónico (Calling Code)';
comment on column tnht_naci.iso_3166 is 'ISO 3166 (alpha 3)';
comment on column tnht_naci.pt_sef is 'Codigo oficial para SEF-Portugal';
comment on column tnht_naci.naci_caux is 'Código Auxiliar de País';

create table tnht_tope (tope_pk number(10) not null, lite_pk raw(16) not null, constraint nht_pk_tope primary key (tope_pk));
comment on table tnht_tope is 'Clasificador Tipo Operaciones Usuario';
comment on column tnht_tope.tope_pk is 'Código Tipo Operación';
comment on column tnht_tope.lite_pk is 'Traducción nombre de operación usuario, ref. tnht_lite(lite_pk)';

create table tnht_grpr (grpr_pk raw(16) not null, lite_pk raw(16) not null, grpr_orde number(4) default 1 not null, constraint nht_pk_grpr primary key (grpr_pk));
comment on table tnht_grpr is 'Grupo Permisos (Interna)';
comment on column tnht_grpr.grpr_orde is 'Orden para mostrar los grupos de permisos';

create table tnht_perm (perm_pk raw(16) not null, lite_pk raw(16) not null, grpr_pk raw(16) not null, perm_orde number(4) default 1 not null, constraint nht_pk_perm primary key (perm_pk));
comment on table tnht_perm is 'Definición General de Permisos (Interna)';
comment on column tnht_perm.perm_pk is 'Código Permiso';
comment on column tnht_perm.lite_pk is 'Traducción de Permiso';
comment on column tnht_perm.grpr_pk is 'Código grupo permiso, ref. tnht_grpr(grpr_pk)';
comment on column tnht_perm.perm_orde is 'orden del permiso dentro del grupo';

create table tnht_appl (appl_pk number(10) not null, appl_name varchar2(255) not null, constraint nht_pk_appl primary key (appl_pk));
comment on table tnht_appl is 'Aplicaciones/Modulos (Interna)';
comment on column tnht_appl.appl_pk is 'Código Aplicación';
comment on column tnht_appl.appl_name is 'Nombre Aplicación';

create table tnht_peap (peap_pk raw(16) not null, appl_pk number(10) not null, perm_pk raw(16) not null, constraint nht_pk_peap primary key (peap_pk));
comment on table tnht_peap is 'Definición de Permisos por Aplicación (Interna)';

create table tnht_hote (hote_pk raw(16) not null, hote_abre varchar2(255) not null, hote_desc varchar2(255) not null, hote_caux varchar2(255), naci_pk varchar2(3) not null, hote_logo varchar2(255), hote_kali number(4) default 30 not null, hote_expc number(4) default 10 not null, hote_sync timestamp(6), constraint nht_pk_hote primary key (hote_pk));
comment on table tnht_hote is 'Instalaciones Hoteleras/Extrahoteleras';
comment on column tnht_hote.hote_pk is 'Código Instalación Hotelera/Extrahotelera';
comment on column tnht_hote.hote_abre is 'Short Name';
comment on column tnht_hote.hote_desc is 'Description or Long Name';
comment on column tnht_hote.hote_caux is 'Auxiliar Code';
comment on column tnht_hote.naci_pk is 'Pais, ref. tnht_naci(naci_pk)';
comment on column tnht_hote.hote_logo is 'Imagen';
comment on column tnht_hote.hote_kali is 'Intervalo de tiempo en segundos para el chequeo de conexión (keepalive). Por defecto a 30s';
comment on column tnht_hote.hote_expc is 'Contador límite de fallos para considerar conexión expirada. Por defecto a 10';
comment on column tnht_hote.hote_sync is 'Fecha/Hora de última sincronización';

create table tnht_util (util_pk raw(16) not null, hote_pk raw(16) not null, util_login varchar2(255) not null, util_desc varchar2(255) not null, util_pass varchar2(255) not null, util_inte char(1) default '0' not null, util_inac char(1) default '0' not null, util_mgr raw(16) not null, util_code varchar2(255), util_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_util primary key (util_pk));
comment on table tnht_util is 'Usuarios sistema (Logins)';
comment on column tnht_util.util_pk is 'Código Usuario';
comment on column tnht_util.hote_pk is 'Código Hotel';
comment on column tnht_util.util_login is 'Login Usuario';
comment on column tnht_util.util_desc is 'Nombre Usuario';
comment on column tnht_util.util_pass is 'Password Usuario';
comment on column tnht_util.util_inte is 'flag: usuario interno proveniente de MGR (1.si 0.no)';
comment on column tnht_util.util_inac is 'flag: Usuario inactivo para el hotel (1. si, 0.no) ';
comment on column tnht_util.util_mgr is 'Código Usuario Central en MGR';
comment on column tnht_util.util_code is 'Codigo de acceso rapido del usuario';
comment on column tnht_util.util_lmod is 'Ultima modificación: control de concurrencia';

create table tnht_peut (peut_pk raw(16) not null, util_pk raw(16) not null, peap_pk raw(16) not null, secu_code number(10) not null, constraint nht_pk_peut primary key (peut_pk));
comment on table tnht_peut is 'Permisos por Usuario';
comment on column tnht_peut.peut_pk is 'Código Permiso, ref. tnht_perm(perm_pk), delete cascade';
comment on column tnht_peut.util_pk is 'Código Usuario Asociado, ref. tnht_util(util_pk) delete cascade';
comment on column tnht_peut.secu_code is 'código seguridad aplicado (hide, read, read/print, read/write, read/write/print, full access) ref. tmgr_enum(enum_pk)';

create table tnht_loop (loop_pk raw(16) not null, hote_pk raw(16) not null, loop_datr date not null, loop_dare timestamp(6) default current_timestamp not null, loop_hore date not null, tope_pk number(10) default 1 not null, loop_crit number(10) not null, util_pk raw(16) not null, loop_estr varchar2(255) not null, loop_deta varchar2(255), loop_audit varchar2(255), constraint nht_pk_loop primary key (loop_pk));
comment on table tnht_loop is 'Log Operaciones';
comment on column tnht_loop.loop_pk is 'Código Log Operación';
comment on column tnht_loop.hote_pk is 'Código Hotel asociado, ref. tnht_hote(hote_pk)';
comment on column tnht_loop.loop_datr is 'Fecha trabajo al insertar el log';
comment on column tnht_loop.loop_dare is 'Fecha/Hora operación';
comment on column tnht_loop.loop_hore is 'Hora de registro';
comment on column tnht_loop.tope_pk is 'Tipo de Operacion, ref. tnht_tope(tope_pk)';
comment on column tnht_loop.loop_crit is 'Tipo Evento (Critico, Error, Aviso, Información, Auditoria), ref. tnht_enum(enum_pk)';
comment on column tnht_loop.util_pk is 'Código usuario que produce la entrada, ref. tnht_util(util_pk)';
comment on column tnht_loop.loop_estr is 'Estación de Trabajo';
comment on column tnht_loop.loop_deta is 'Detalle Operación';
comment on column tnht_loop.loop_audit is 'Información Auditoria';

create table tnht_mtco (mtco_pk raw(16) not null, hote_pk raw(16) not null, mtco_tica number(10) not null, lite_pk raw(16) not null, mtco_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_mtco primary key (mtco_pk));
comment on table tnht_mtco is 'Clasificador Motivos de Anulación y Corrección';
comment on column tnht_mtco.mtco_pk is 'Código Motivo Anulación/Corrección';
comment on column tnht_mtco.hote_pk is 'Código Instalación Asociada, ref. tnht_hote(hote_pk)';
comment on column tnht_mtco.mtco_tica is 'Tipo Cancelación (Facturas, Movimientos, Reservas), ref. tnht_enum(enum_pk)';
comment on column tnht_mtco.lite_pk is 'Traducción descripción motivo cancelación/corrección, ref. tnht_lite(lite_pk)';
	
create table tnht_fore (fore_pk raw(16) not null, hote_pk raw(16) not null, lite_abre raw(16) not null, lite_desc raw(16) not null, unmo_pk varchar2(3) not null, fore_cash char(1) default '1' not null, fore_cacr char(1) default '0' not null, repo_cash char(1) default '1' not null, fore_path varchar2(255), fore_orde number(4) default 1 not null, fore_inac char(1) default '0' not null, fore_lmod timestamp(6) default CURRENT_TIMESTAMP not null, 	constraint nht_pk_fore primary key (fore_pk));
comment on table tnht_fore is 'Tipos de Cobros';
comment on column tnht_fore.fore_pk is 'Código Tipo Cobro';
comment on column tnht_fore.hote_pk is 'Código Instalación, ref. tnht_hote(hote_pk)';
comment on column tnht_fore.lite_abre is 'Traducción abreviatura tipo cobro, ref. tnht_lite(lite_pk)';
comment on column tnht_fore.lite_desc is 'Traducción descripción tipo cobro, ref. tnht_lite(lite_pk)';
comment on column tnht_fore.unmo_pk is 'Código Moneda Asociada, ref. tnht_unmo(unmo_pk)';
comment on column tnht_fore.fore_cash is 'flag: indica si se considera venta en efectivo';
comment on column tnht_fore.fore_cacr is 'Flag: Tipo Cobro Tarjeta Crédito (1.si, 0.no)';
comment on column tnht_fore.repo_cash is 'flag: Incluir en reporte de caja';
comment on column tnht_fore.fore_orde is 'Orden al mostrar';
comment on column tnht_fore.fore_path is 'Camino relativo al IIS donde se encuentra la imagen';
comment on column tnht_fore.fore_inac is 'Flag: Tipo Cobro Inactivo (1.si, 0.no)';	

create table tnht_regi (regi_pk raw(16) not null, naci_pk varchar2(3) not null, lite_pk raw(16) not null, regi_caux varchar2(255), regi_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_regi primary key (regi_pk));
comment on table tnht_regi is 'Regiones de Impuestos por Paises';
comment on column tnht_regi.regi_pk is 'Código Región Impuesto';
comment on column tnht_regi.naci_pk is 'Código país asociado, ref. tnht_naci(naci_pk), delete cascade';
comment on column tnht_regi.lite_pk is 'Traducción nombre de región o distrito, ref. tnht_lite(lite_pk)';
comment on column tnht_regi.regi_caux is 'Código Auxiliar';

create table tnht_seim (seim_pk raw(16) not null, seim_abre varchar2(20) not null, lite_pk raw(16) not null, seim_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_seim primary key (seim_pk));
comment on table tnht_seim is 'Secuencias de impuestos';
comment on column tnht_seim.seim_pk is 'Código secuencia impuesto';
comment on column tnht_seim.seim_abre is 'Abreviatura Secuencia Impuesto';
comment on column tnht_seim.lite_pk is 'Traducción nombre secuencia impuesto, ref. tnht_lite(lite_pk)';

create table tnht_tiva (tiva_pk raw(16) not null, lite_pk raw(16) not null, tiva_perc number not null, seim_pk raw(16) not null, regi_pk raw(16) not null, tiva_inac char(1) default '0' not null, tiva_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_tiva primary key (tiva_pk));
comment on table tnht_tiva is 'Tasas de Impuestos';
comment on column tnht_tiva.tiva_pk is 'Código tasa impuesto';
comment on column tnht_tiva.lite_pk is 'Traducción descripción de la tasa de impuestos, ref. tnht_lite(lite_pk)';
comment on column tnht_tiva.tiva_perc is 'Porciento impuesto definido';
comment on column tnht_tiva.seim_pk is 'Código secuencia impuesto asociada, ref. tnht_seim(seim_pk)';
comment on column tnht_tiva.regi_pk is 'Código región impuesto asociada, ref. tnht_regi(regi_pk)';
comment on column tnht_tiva.tiva_inac is 'flag: Impuesto Inactivo (0. No, 1.Si)';

create table tnht_esim (esim_pk raw(16) not null, hote_pk raw(16) not null, lite_pk raw(16) not null, esim_defa char(1) not null, esim_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_esim primary key (esim_pk));
comment on table tnht_esim is 'Esquemas Impuestos';
comment on column tnht_esim.esim_pk is 'Código esquema impuesto';
comment on column tnht_esim.hote_pk is 'Código instalación hotelera/extrahotelera, ref. tnht_hote(hote_pk)';
comment on column tnht_esim.lite_pk is 'Traducción nombre de esquema impuesto, ref. tnht_lite(lite_pk)';
comment on column tnht_esim.esim_defa is 'Flag: Esquema impuesto por defecto (1.si, 0.no)';

create table tnht_tide (tide_pk raw(16) not null, lite_pk raw(16) not null, hote_pk raw(16) not null, appl_pk number(10), tide_fixe char(1) not null, tide_vmin number not null, tide_vmax number not null, tide_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_tide primary key (tide_pk));
comment on table tnht_tide is 'Tipos Descuentos';
comment on column tnht_tide.tide_pk is 'Código Tipo Descuento';
comment on column tnht_tide.lite_pk is 'Traducción Descripción Descuento, ref. tnht_lite(lite_pk)';
comment on column tnht_tide.hote_pk is 'Código hotel asociado, ref. tnht_hote(hote_pk)';
comment on column tnht_tide.appl_pk is 'Modulo a usar este descuento (null = usado en todos)';
comment on column tnht_tide.tide_fixe is 'flag: descuento fijo = 1, descuento en rango = 0';
comment on column tnht_tide.tide_vmin is 'valor minimo del rango de impuesto, en caso de impuesto fijo contiene el valor de impuesto';
comment on column tnht_tide.tide_vmax is 'valor maximo del rango de impuesto, en caso de impuesto fijo contiene el valor de impuesto';

create table tnht_cacr (cacr_pk raw(16) not null, hote_pk raw(16) not null, lite_pk raw(16) not null, cacr_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_cacr primary key (cacr_pk));
comment on table tnht_cacr is 'Tipos tarjetas crédito';
comment on column tnht_cacr.cacr_pk is 'Código tipo tarjeta crédito';
comment on column tnht_cacr.hote_pk is 'Código hotel asociado, ref. tnht_hote(hote_pk)';
comment on column tnht_cacr.lite_pk is 'Traducción descripción tipo tarjeta crédito, ref. tnht_lite(lite_pk)';

create table tnht_tido (tido_pk number(10) not null, tido_pare number(10), lite_pk raw(16) not null, tido_seri char(1) default '1' not null, tido_fact char(1) default '0' not null, constraint nht_pk_tido primary key (tido_pk));
comment on table tnht_tido is 'Tipos de Documentos';
comment on column tnht_tido.tido_pk is 'Código tipo documento';
comment on column tnht_tido.tido_pare is 'Dependencia, ref. tnht_tido(tido_pk), delete cascade';
comment on column tnht_tido.lite_pk is 'Traducción descripción tipo documento, ref. tnht_lite(lite_pk)';
comment on column tnht_tido.tido_seri is 'Tipo Documento Serializable (1.Si, 0.No)';
comment on column tnht_tido.tido_fact is 'Tipo Documento Facturación (1.Si, 0.No)';

create table tnht_tacr (tacr_pk raw(16) not null, cacr_pk raw(16) not null, tacr_iden varchar2(255) not null, cacr_vmes number(2), cacr_vano number(4), cacr_code varchar2(3), constraint nht_pk_tacr primary key (tacr_pk));
comment on table tnht_tacr is 'Documentos personales tipo tarjetas de crédito (definiciones)';
comment on column tnht_tacr.cacr_pk is 'Código tipo tarjeta crédito, ref. tnht_cacr(cacr_pk)';
comment on column tnht_tacr.tacr_iden is 'Nº tarjeta credito';
comment on column tnht_tacr.cacr_vmes is 'Mes de validad señalado en la tarjeta de crédito';
comment on column tnht_tacr.cacr_vano is 'Año de validad señalado en la tarjeta de crédito';

create table tnht_clas (clas_pk raw(16) not null, hote_pk raw(16) not null, lite_pk raw(16) not null, clas_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_clas primary key (clas_pk));
comment on table tnht_clas is 'Clasificadores por aplicaciones';
comment on column tnht_clas.clas_pk is 'Id del clasificador';
comment on column tnht_clas.hote_pk is 'Hotel al que pertenece, ref. tnht_hote(hote_pk), delete cascade';
comment on column tnht_clas.lite_pk is 'Traduccion de la descripcion, ref. tnht_lite(lite_pk)';

create table tnht_grup (grup_pk raw(16) not null, grup_cheq char(1) default '1' not null, grup_reca char(1) default '0' not null, constraint nht_pk_grup primary key (grup_pk));
comment on table tnht_grup is 'Grupo de Productos';
comment on column tnht_grup.grup_pk is 'Codigo de grupo de productos, ref. tnht_clas(clas_pk), delete cascade';
comment on column tnht_grup.grup_cheq is 'Flag. Los productos de este grupo se visualizaran en los tickets en la pantalla de venta (1. si, 0. no)';
comment on column tnht_grup.grup_reca is 'Flag. A los productos de este grupo se le podrán aplicar recargos (1. si, 0. no)';

create table tnht_fami (fami_pk raw(16) not null, grup_pk raw(16) not null, constraint nht_pk_fami primary key (fami_pk));
comment on table tnht_fami is 'Familias de Productos';
comment on column tnht_fami.fami_pk is 'Código de Familia de Productos, ref. tnht_clas(clas_pk), delete cascade';
comment on column tnht_fami.grup_pk is 'Grupo al que pertenece esta Familia, ref. tnht_grup(grup_pk)';

create table tnht_sfam (sfam_pk raw(16) not null, fami_pk raw(16) not null, constraint nht_pk_sfam primary key (sfam_pk));
comment on table tnht_sfam is 'SubFamilias de productos';
comment on column tnht_sfam.sfam_pk is 'Código de SubFamilia de Productos, ref. tnht_clas(clas_pk), delete cascade';
comment on column tnht_sfam.fami_pk is 'Codigo de la familia a la que pertenece esta SubFamilia, ref. tnht_fami(fami_pk)';
	
create table tnht_sepa (sepa_pk raw(16) not null, hote_pk raw(16) not null, lite_pk raw(16) not null, sepa_orde number(4) default 1 not null, sepa_maxp number(4) default 1 not null, sepa_acum char(1) default '0' not null, sepa_tick char(1) default '1' not null, sepa_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_sepa primary key (sepa_pk));
comment on table tnht_sepa is 'Tabla de separadores';
comment on column tnht_sepa.sepa_pk is 'Codigo del Separador por defecto del producto';
comment on column tnht_sepa.hote_pk is 'Hotel al que pertence, ref. tnht_hote(hote_pk)';
comment on column tnht_sepa.lite_pk is 'Traduccion del separador, ref. tnht_lite(lite_pk)';
comment on column tnht_sepa.sepa_orde is 'Orden de impresion ';
comment on column tnht_sepa.sepa_maxp is 'Cantidad máxima de platos/paxs de la ronda';
comment on column tnht_sepa.sepa_acum is 'Flag: Ronda acumulativa (1. si, 0. no)';
comment on column tnht_sepa.sepa_tick is 'Flag: Se muestra en el ticket(1. se, 0. no)';	

create table tnht_artg (artg_pk raw(16) not null, hote_pk raw(16) not null, lite_abre raw(16) not null, lite_desc raw(16) not null, artg_codi varchar2(255), fami_pk raw(16) not null, sfam_pk raw(16), sepa_pk raw(16), artg_step number default 1 not null, artg_imag varchar2(255), artg_colo number, artg_tipr number(10) default 3461 not null, artg_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_artg primary key (artg_pk));
comment on table tnht_artg is 'Tabla de productos de venta';
comment on column tnht_artg.artg_pk is 'Primary key';
comment on column tnht_artg.hote_pk is 'Código Instalación, ref. tnht_hote(hote_pk)';
comment on column tnht_artg.lite_abre is 'Traducción abreviatura producto, ref. tnht_lite(lite_pk)';
comment on column tnht_artg.lite_desc is 'Traducción descripción producto, ref. tnht_lite(lite_pk)';
comment on column tnht_artg.artg_codi is 'Codigo visual del producto de venta (PLU)';
comment on column tnht_artg.fami_pk is 'Familia al que pertenece el producto, ref. tnht_fami(fami_pk)';
comment on column tnht_artg.sfam_pk is 'SubFamilia a la que pertenece el producto, ref. tnht_sfam(sfam_pk)';
comment on column tnht_artg.sepa_pk is 'Codigo del separador por defecto del producto, ref . tnht_sepa';
comment on column tnht_artg.artg_step is 'Cantidad aumentada por defecto cuando se postea el producto en el ticket';
comment on column tnht_artg.artg_imag is 'Imagen standard asociada al producto';
comment on column tnht_artg.artg_colo is 'Color adicional para mostrar como el producto en caso de no tener imagen';
comment on column tnht_artg.artg_tipr is 'Tipo precio a aplicar (siempre de tarifa, siempre manual, variable), ref. tnht_enum(enum_pk)';

create table tnht_arim (arim_pk raw(16) not null, artg_pk raw(16) not null, arim_path varchar2(255), constraint nht_pk_arim primary key (arim_pk));
comment on table tnht_arim is 'Tabla imagenes asociadas a cada producto';
comment on column tnht_arim.artg_pk is 'Id del servicio o producto';
comment on column tnht_arim.arim_path is 'Imagen asociada';
	
create table tnht_imse (imse_pk raw(16) not null, artg_pk raw(16) not null, esim_pk raw(16) not null, tiva_cod1 raw(16) not null, tiva_cod2 raw(16), tiva_apl2 number(10), tiva_cod3 raw(16), tiva_apl3 number(10), tiva_ret1 char(1) default '0' not null, tiva_ret2 char(1) default '0' not null, tiva_ret3 char(1) default '0' not null, constraint nht_pk_imse primary key (imse_pk));
comment on table tnht_imse is 'Desglose Impuestos por Servicio';
comment on column tnht_imse.imse_pk is 'Código Impuesto - Servicio';
comment on column tnht_imse.artg_pk is 'Código Producto Seleccionado, ref. tnht_artg(artg_pk) on delete cascade';
comment on column tnht_imse.esim_pk is 'Código Esquema Impuesto Seleccionado, ref. tnht_esim(esim_pk)';
comment on column tnht_imse.tiva_cod1 is 'Código Primera Tasa Impuesto ';
comment on column tnht_imse.tiva_cod2 is 'Código Segunda Tasa Impuesto ';
comment on column tnht_imse.tiva_apl2 is 'Aplicación Impuesto 2 (Sobre Base, Sobre Base + Taxa1), ref. tnht_enum(enum_pk)';
comment on column tnht_imse.tiva_cod3 is 'Código Tercera Tasa Impuesto ';
comment on column tnht_imse.tiva_apl3 is 'Aplicación Impuesto 3 (Sobre Base, Sobre Base + Taxa1, Sobre Base + Taxa2 , Sobre Base + Taxa1 + Taxa2), ref. tnht_enum(enum_pk)';
comment on column tnht_imse.tiva_ret1 is 'flag: Aplicar retención a primera tasa de impuesto?';
comment on column tnht_imse.tiva_ret2 is 'flag: Aplicar retención a segunda tasa de impuesto?';
comment on column tnht_imse.tiva_ret3 is 'flag: Aplicar retención a tercera tasa de impuesto?';	

create table tcfg_npos(hote_pk raw(16) not null, fast_paym char(1) default '1' not null, para_fore raw(16), para_ivin char(1) default '1' not null, para_esim raw(16), para_tick char(1) default '0' not null, para_mtco raw(16), para_tico number(10) default 872 not null, para_npas char(1) default 0 not null, para_tise raw(16), para_tipe number, para_tivl number, para_tiat char(1) default '0' not null, sign_tipo number(10), draw_code varchar2(255), blob_path varchar2(255), npos_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_cfg_npos primary key (hote_pk));
comment on table tcfg_npos is 'Configuraciones POS';
comment on column tcfg_npos.hote_pk is 'Código Hotel, ref. tnht_hote(hote_pk), delete cascade';
comment on column tcfg_npos.fast_paym is 'flag: usar pago rápido';
comment on column tcfg_npos.para_fore is 'Forma pago asociada al pago rápido, ref. tnht_fore(fore_pk)';
comment on column tcfg_npos.para_ivin is 'flag: impuesto incluido en los precios';
comment on column tcfg_npos.para_esim is 'Esquema impuesto utilizado por defecto, ref. tnht_esim(esim_pk)';
comment on column tcfg_npos.para_tick is 'Flag: Si se permite cerrar el día con ticket abiertos';
comment on column tcfg_npos.para_mtco is 'Motivo de Cancelación para cuando se van a mezclar tickets, ref. tnht_mtco(mtco_pk)';
comment on column tcfg_npos.para_tico is 'Tipo Cuenta por defecto (master, extra 1 .. extra 5), ref. tnht_enum(enum_pk)';
comment on column tcfg_npos.para_npas is 'flag: utilizar login numerico para acceder desde los POS locales (1.si, 0. no)';
comment on column tcfg_npos.para_tiat is 'Flag: 1 incluir propina automaticamente';
comment on column tcfg_npos.para_tise is 'Producto utilizado como propina en el hotel';
comment on column tcfg_npos.para_tipe is 'Prociento de propina automatica asociada al hotel';
comment on column tcfg_npos.para_tivl is 'Valor de propina automatica asociada al hotel';
comment on column tcfg_npos.sign_tipo is 'Enumerado (Tipo de Firma a utilizar: SAF-T en Portugal), ref. tnht_enum(enum_pk)';
comment on column tcfg_npos.draw_code is 'Esc Code utilizado para abrir la caja de dinero';
comment on column tcfg_npos.blob_path is 'Camino relativo a la localizacion de las imagenes';
	
create table tnht_duty (duty_pk raw(16) not null, hote_pk raw(16) not null, lite_pk raw(16) not null, duty_hori date not null, duty_horf date not null, duty_etme number(4) default 30 not null, constraint nht_pk_duty primary key (duty_pk));
comment on table tnht_duty is 'Servicios POS (Desayuno, Almuerzo, Cena, etc.)';
comment on column tnht_duty.duty_pk is 'Primary Key';
comment on column tnht_duty.hote_pk is 'Hotel Asociado, ref. tnht_hote(hote_pk), delete cascade';
comment on column tnht_duty.lite_pk is 'Traduccion ';
comment on column tnht_duty.duty_hori is 'Hora Inicio';
comment on column tnht_duty.duty_horf is 'Hora Fin';
comment on column tnht_duty.duty_etme is 'Tiempo estimado de demora de los comensales durante este turno, dado en minutos';

create table tnht_tprg (tprg_pk raw(16) not null, hote_pk raw(16) not null, lite_pk raw(16) not null, unmo_pk varchar2(3) not null, ivas_incl char(1) default '1' not null, tprg_inac  char(1) default '0' not null, tprg_type  number(10) not null, tprg_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_tprg primary key (tprg_pk));
comment on table tnht_tprg is 'Tarifas de precios para productos de venta';
comment on column tnht_tprg.tprg_pk is 'Código Tarifa Venta';
comment on column tnht_tprg.hote_pk is 'Hotel al que pertenece ref. tnht_hote(hote_pk)';
comment on column tnht_tprg.lite_pk is 'Traduccion Nombre Tarifa ref. tnht_lite(lite_pk)';
comment on column tnht_tprg.unmo_pk is 'Moneda de la tarifa precios';
comment on column tnht_tprg.ivas_incl is 'Flag: Impuesto Incluido en los precios (1. si, 0. no)';
comment on column tnht_tprg.tprg_type is 'Tipo Tarifa (Todas, Standard, House use, Meal Plan), ref tnht_enum(enum_pk)';
comment on column tnht_tprg.tprg_inac is 'Flag: Inactiva (1.Si, 0.No)';

create table tnht_tppe (tppe_pk raw(16) not null, tprg_pk raw(16) not null, tppe_dain date not null, tppe_dafi date not null, constraint nht_pk_tppe primary key (tppe_pk));
comment on table tnht_tppe is 'Periodos de Precios';
comment on column tnht_tppe.tprg_pk is 'Codigo de la tarifa, ref. tnht_tprg(tprg_pk), delete cascade';
comment on column tnht_tppe.tppe_dain is 'Fecha inicial del periodo';
comment on column tnht_tppe.tppe_dafi is 'Fecha final del periodo';

create table tnht_tprb (tprb_pk raw(16) not null, tprg_pk raw(16) not null, tprb_dain date not null, tprb_dafi date not null, constraint nht_pk_tprb primary key (tprb_pk));
comment on table tnht_tprb is 'Periodos de bloqueo para una Tarifa de Precios';
comment on column tnht_tprb.tprg_pk is 'Codigo de la tarifa, ref. tnht_tprg(tprg_pk), delete cascade';
comment on column tnht_tprb.tprb_dain is 'Fecha inicial bloqueo';
comment on column tnht_tprb.tprb_dafi is 'Fecha final bloqueo';

create table tnht_tprl (tprl_pk raw(16) not null, tppe_pk raw(16) not null, artg_pk raw(16) not null, tprl_valor number, constraint nht_pk_tprl primary key (tprl_pk));
comment on table tnht_tprl is 'Tarifas de precios de productos (lineas o detalles)';
comment on column tnht_tprl.tprl_pk is 'Primary key';
comment on column tnht_tprl.tppe_pk is 'Periodo asociado, ref. tnht_tppe(tppe_pk), delete cascade';
comment on column tnht_tprl.artg_pk is 'Codigo del producto asociado, ref. tnht_artg(artg_pk), delete cascade';
comment on column tnht_tprl.tprl_valor is 'Precio del producto asociado en el periodo';

create table tnht_caja (caja_pk raw(16) not null, hote_pk raw(16) not null, lite_pk raw(16) not null, caja_veca char(1) default '0' not null, ipos_empc char(1) default '0' not null, caja_lmod timestamp(6) default CURRENT_TIMESTAMP not null, constraint nht_pk_caja primary key (caja_pk));
comment on table tnht_caja is 'Tabla de cajas';
comment on column tnht_caja.caja_pk is 'Id de la caja';
comment on column tnht_caja.caja_veca is 'Flag: Cada cajero solo puede ver los tickets abiertos por el';
comment on column tnht_caja.ipos_empc is 'Flag: Los reportes de caja solo incluye ventas del empleado activo';

create table tnht_menu (menu_pk raw(16) not null, hote_pk raw(16) not null, lite_pk raw(16) not null, menu_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_menu primary key (menu_pk));
comment on table tnht_menu is 'Composicion Menu Puntos de Venta';
comment on column tnht_menu.menu_pk is 'Primary Key';
comment on column tnht_menu.lite_pk is 'Traducción nombre del menu, ref. tnht_lite(lite_pk)';

create table tnht_mede (mede_pk raw(16) not null, menu_pk raw(16) not null, sepa_pk raw(16) not null, artg_pk raw(16) not null, addi_desc varchar2(255), mede_orde number(10) default 1 not null, constraint nht_pk_mede primary key (mede_pk));
comment on table tnht_mede is 'Detalles Menu Puntos de Venta';
comment on column tnht_mede.menu_pk is 'Referencia Menu, ref.tnht_menu(menu_pk) on delete cascade';
comment on column tnht_mede.sepa_pk is 'Referencia Separador, ref. tnht_sepa(sepa_pk)';
comment on column tnht_mede.artg_pk is 'Referencia Producto, ref.tnht_artg(artg_pk) on delete cascade';
comment on column tnht_mede.addi_desc is 'Descripción Adicional'; 
comment on column tnht_mede.mede_orde is 'Orden general dentro de los detalles'; 

create table tnht_tick (tick_pk raw(16) not null, hote_pk raw(16) not null, lite_pk raw(16) not null, tick_ivaf char(1) default '1' not null, tick_hora char(1) default '1' not null, tick_hcie char(1) default '0' not null, tick_sepa char(1) default '1' not null, tick_msg1 varchar2(255), tick_msg2 varchar2(255), tick_sign char(1) default '0' not null, tick_line number(4) default 0 not null, tick_linp number(4), tick_lpro number(1) default '0' not null, tick_alig number(4), tick_logo blob, tick_shlo  char(1) default '1' not null, tick_colu  char(1) default '0' not null, tick_disc  char(1) default '1' not null, tick_reca  char(1) default '1' not null, tick_recv  char(1) default '0' not null, tick_paym  char(1) default '0' not null, tick_tips  char(1) default '0' not null, tick_copr char(1) default '0' not null, tick_coti char(1) default '0' not null, tick_larg char(1) default '0' not null, constraint nht_pk_tick primary key (tick_pk));
comment on table tnht_tick is 'Configuraciones para impresiones locales de un ticket/factura/comprobante/pedido a cocina';
comment on column tnht_tick.tick_pk is 'Código Configuracion del Ticket';
comment on column tnht_tick.hote_pk is 'Hotel al que pertenece, ref. tnht_hote(hote_pk), delete cascade';
comment on column tnht_tick.lite_pk is 'Traducción de la configuracion, ref. tnht_lite(lite_pk)';
comment on column tnht_tick.tick_ivaf is 'Flag: Imprimir los detalles de Impuestos (1. si, 0. no)';
comment on column tnht_tick.tick_hora is 'Flag: Imprimir la hora de apertura del ticket (1. si, 0. no)';
comment on column tnht_tick.tick_hcie is 'Flag: Imprimir fecha y hora de cierre del ticket (1. si, 0. no)';
comment on column tnht_tick.tick_sepa is 'flag: Imprimir los separadores (1. si, 0. no)';
comment on column tnht_tick.tick_msg1 is 'Mensaje 1';
comment on column tnht_tick.tick_msg2 is 'Mensaje 2';
comment on column tnht_tick.tick_sign is 'Flag: Incluir pie de firma (1. si, 0. no)';
comment on column tnht_tick.tick_line is 'Numero de lineas vacias en el encabezado ';
comment on column tnht_tick.tick_linp is 'Minimo número de lineas del ticket';
comment on column tnht_tick.tick_lpro is 'Indica si se incluyen lineas para propinas manual';
comment on column tnht_tick.tick_logo is 'Logo predeterminado para el ticket';
comment on column tnht_tick.tick_alig is 'Tipo de Alineación para el logo, -1.Left 0.Center, 1.Right';
comment on column tnht_tick.tick_shlo is 'Flag: Mostrar el Logo (1. si, 0. no)';
comment on column tnht_tick.tick_colu is 'Flag: Imprimir nombres de columnas (1. si, 0. no)';
comment on column tnht_tick.tick_disc is 'Flag: No mostrar los descuentos cuando son cero (1. si, 0. no)';
comment on column tnht_tick.tick_reca is 'Flag: No mostrar los recargos cuando son cero (1. si, 0. no)';
comment on column tnht_tick.tick_recv is 'Flag: Imprimir las opciones de pago en los recibos (1. si, 0. no)';
comment on column tnht_tick.tick_paym is 'Flag: Imprimir los detalles del pago (1. si, 0. no)';
comment on column tnht_tick.tick_tips is 'Flag: Imprimir las propinas (1. si, 0. no)';
comment on column tnht_tick.tick_copr is 'Flag: Mostrar los comentarios de las lineas de productos (1. si, 0. no)';
comment on column tnht_tick.tick_coti is 'Flag: Mostrar los comentarios del ticket (1. si, 0. no)';
comment on column tnht_tick.tick_larg is 'Flag: 1 imprimir en formato A4';	

create table tnht_ipos (ipos_pk raw(16) not null, hote_pk raw(16) not null, lite_abre raw(16) not null, lite_desc raw(16) not null, ipos_fctr date not null, ipos_tutr number(10) default 1 not null, tprg_stdr raw(16), tprg_cint raw(16), tprg_cipo number, tprg_pens raw(16), tprg_pepo number, cnfg_tick raw(16), cnfg_fact raw(16), cnfg_comp raw(16), ipos_ccop number(4) default 1 not null, ipos_ckab char(1) default 0 not null, ipos_kiti char(1) default 0 not null, ipos_comp char(1) default 1 not null, ipos_tise raw(16), ipos_tipe number, ipos_tivl number, ipos_tiat char(1) default '0' not null, ipos_tion char(1) default 0 not null, menu_mon raw(16), menu_tue raw(16), menu_wed raw(16), menu_thu raw(16), menu_fri raw(16), menu_sat raw(16), menu_sun raw(16), ctrl_pk raw(16), ipos_inac char(1) default '0' not null, ipos_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_ipos primary key (ipos_pk));
comment on table tnht_ipos is 'Puntos de venta';
comment on column tnht_ipos.ipos_pk is 'Primary key';
comment on column tnht_ipos.hote_pk is 'Código Instalación, ref. tnht_hote(hote_pk)';
comment on column tnht_ipos.lite_abre is 'Traducción abreviatura punto venta, ref. tnht_lite(lite_pk)';
comment on column tnht_ipos.lite_desc is 'Traducción descripción punto venta, ref. tnht_lite(lite_pk)';
comment on column tnht_ipos.ipos_fctr is 'Fecha de trabajo de este punto de venta';
comment on column tnht_ipos.ipos_tutr is 'Turno de trabajo';
comment on column tnht_ipos.tprg_stdr is 'Tarifa de precio standard, ref. tnht_tprg(tprg_pk)';
comment on column tnht_ipos.tprg_cint is 'Tarifa de precio de consumo interno, ref. tnht_tprg(tprg_pk)';
comment on column tnht_ipos.tprg_cipo is 'Valor en porciento a aplicar sobre una tarifa existente para preciar los consumos internos';
comment on column tnht_ipos.tprg_pens is 'Tarifa para precios incluidos en pension, ref. tnht_tprg(tprg_pk)';
comment on column tnht_ipos.tprg_pepo is 'Valor en porciento a aplicar sobre una tarifa existente para preciar los incluidos en pensión';
comment on column tnht_ipos.cnfg_tick is 'Configuracion de la impresion del ticket. ';
comment on column tnht_ipos.cnfg_fact is 'Configuracion de la impresion de las facturas';
comment on column tnht_ipos.cnfg_comp is 'Configuracion de la impresion del comprobante(preview)';
comment on column tnht_ipos.ipos_ccop is 'Cantidad de copias a imprimir de la tira del cierre';
comment on column tnht_ipos.ipos_ckab is 'Flag: Cerrar el día con tickets abiertos (1. si, 0. no)';
comment on column tnht_ipos.ipos_kiti is 'Cuando esta activo se imprime una copia del ticket de cocina en la impresora de recibos';
comment on column tnht_ipos.ipos_comp is 'Flag: Se imprimen comprobantes (Preview de la cuenta) (1. si, 0. no)';
comment on column tnht_ipos.ipos_tise is 'Servicio de tipo propina asociado al stand';
comment on column tnht_ipos.ipos_tipe is 'Prociento de propina automatica asociada al stand';
comment on column tnht_ipos.ipos_tivl is 'Valor de propina automatica asociada al stand';
comment on column tnht_ipos.ipos_tiat is 'Flag: 1 incluir propina automaticamente';
comment on column tnht_ipos.ipos_tion is 'Calcular propinas sobre valor neto';
comment on column tnht_ipos.menu_mon is 'Menu para lunes, ref. tnht_menu(menu_pk)';  
comment on column tnht_ipos.menu_tue is 'Menu para martes, ref. tnht_menu(menu_pk)';  
comment on column tnht_ipos.menu_wed is 'Menu para miercoles, ref. tnht_menu(menu_pk)';  
comment on column tnht_ipos.menu_thu is 'Menu para jueves, ref. tnht_menu(menu_pk)';  
comment on column tnht_ipos.menu_fri is 'Menu para viernes, ref. tnht_menu(menu_pk)';  
comment on column tnht_ipos.menu_sat is 'Menu para sabados, ref. tnht_menu(menu_pk)';  
comment on column tnht_ipos.menu_sun is 'Menu para domingos, ref. tnht_menu(menu_pk)';  
comment on column tnht_ipos.ctrl_pk is 'Cuenta control para descargar las ventas de este punto de venta, ref. tnht_ctrl(ctrl_pk)';
comment on column tnht_ipos.ipos_inac is 'flag: punto venta inactivo (1.si, 0.no)';

create table tnht_capv (capv_pk raw(16) not null, caja_pk raw(16) not null, ipos_pk raw(16) not null, capv_bloc char(1) default 0 not null, constraint nht_pk_capv primary key (capv_pk));
comment on table tnht_capv is 'Relacion de Cajas - Puntos de Ventas';
comment on column tnht_capv.capv_pk is 'Primary key';
comment on column tnht_capv.ipos_pk is 'Punto de venta asociado';
comment on column tnht_capv.caja_pk is 'Caja asociada';
comment on column tnht_capv.capv_bloc is 'Flag: Si Stand-Caja está bloqueada y no puede ser utilizada';
	
create table tnht_salo (salo_pk raw(16) not null, lite_pk raw(16) not null, hote_pk raw(16) not null, salo_widt number(4), salo_heig number(4), mesa_free varchar2(4000), mesa_ocup varchar2(4000), salo_imag varchar2(4000), salo_smok char(1) default 0 not null, salo_kids char(1) default 1 not null, constraint nht_pk_salo primary key (salo_pk));
comment on table tnht_salo is 'Salones del punto de venta';
comment on column tnht_salo.salo_pk is 'Identificador';
comment on column tnht_salo.lite_pk is 'Descripcion';
comment on column tnht_salo.hote_pk is 'Hotel';
comment on column tnht_salo.salo_smok is 'flag: Saloon de fumadores o no';
comment on column tnht_salo.salo_kids is 'flag: Saloon que permite ninos o no';
comment on column tnht_salo.mesa_free is 'Path Imagen de las mesas libres';
comment on column tnht_salo.mesa_ocup is 'Path Imagen de las mesas ocupadas';
comment on column tnht_salo.salo_imag is 'Path Imagen Planta del Salon';

create table tnht_ipsl (ipsl_pk raw(16) not null, ipos_pk raw(16) not null, salo_pk raw(16) not null, ipsl_orde number(4) default 1 not null, constraint nht_pk_ipsl primary key (ipsl_pk));
comment on table tnht_ipsl is 'Relación entre salones y puntos de venta';
comment on column tnht_ipsl.ipos_pk is 'Punto de Venta';
comment on column tnht_ipsl.salo_pk is 'Salon';
comment on column tnht_ipsl.ipsl_orde is 'orden visual en que se muestran los salones';

create table tnht_iput (iput_pk raw(16) not null, util_pk raw(16) not null, ipos_pk raw(16) not null, secu_code number(10) not null, constraint nht_pk_iput primary key (iput_pk));
comment on table tnht_iput is 'Permisos de Usuario por puntos de venta';
comment on column tnht_iput.iput_pk is 'Identificador';
comment on column tnht_iput.util_pk is 'Identificador del usuario';
comment on column tnht_iput.ipos_pk is 'Identificador del punto de venta';
comment on column tnht_iput.secu_code is  'Codigo seguridad aplicado (hide, read, read/print, read/write, read/write/print, full access) ref. tmgr_enum(enum_pk)';
	
create table tnht_mesa (mesa_pk raw(16) not null, mesa_desc varchar(255) not null, salo_pk raw(16) not null, mesa_paxs number(4) default 4 not null, mesa_type number(10) not null, mesa_row number(4), mesa_colu number(4), mesa_widt number(4), mesa_heig number(4), constraint nht_pk_mesa primary key (mesa_pk));
comment on table tnht_mesa is 'Mesas del punto de venta';
comment on column tnht_mesa.mesa_desc is 'Descripcion(Numero de mesa)';
comment on column tnht_mesa.salo_pk is 'Salon al que pertenece la mesa (ref tnht_salo)';
comment on column tnht_mesa.mesa_paxs is 'Cantidad maxima de paxs permitidos en esta mesa';
comment on column tnht_mesa.mesa_type is 'Tipo de mesa (ref tnht_enum)';

create table tnht_pvar (pvar_pk raw(16) not null, artg_pk raw(16) not null, ipos_pk raw(16) not null, serv_pk raw(16), favo_orde number(10), pvar_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_pvar primary key (pvar_pk));
comment on table tnht_pvar is 'Productos que pueden ser vendidos en cada punto de venta';
comment on column tnht_pvar.pvar_pk is 'Código Producto - Punto de Venta';
comment on column tnht_pvar.artg_pk is 'Codigo del producto asociado, ref. tnht_artg(artg_pk), delete cascade';
comment on column tnht_pvar.ipos_pk is 'Codigo del punto de venta asociado, ref. tnht_ipos(ipos_pk), delete cascade';
comment on column tnht_pvar.serv_pk is 'Servicio NewHotel usado como Agrupador para pasar los cargos a Newhotel, ref. tnht_serv(serv_pk)';
comment on column tnht_pvar.favo_orde is 'Orden en la lista de productos favoritos, en caso de ser null no aparece en la lista';
	
create table tnht_coin (coin_pk raw(16) not null, coin_invi char(1) default '0' not null, coin_inac char(1) default '0' not null, constraint nht_pk_coin primary key (coin_pk));
comment on table tnht_coin is 'Tabla de consumos internos';
comment on column tnht_coin.coin_pk is 'Codigo de consumo interno, ref. tnht_clas(clas_pk), delete cascade';
comment on column tnht_coin.coin_invi is 'Flag: El titular de consumo interno puede hacer Invitaciones (1. Si, 0. No)';
comment on column tnht_coin.coin_inac is 'Flag: Estado del consumo interno (Inactivo = 1, Activo = 0)';
	
create table tnht_pvci (pvci_pk raw(16) not null, coin_pk raw(16) not null, ipos_pk raw(16) not null, pvci_limi number, pvci_ilim  char(1) default '1' not null, pvci_porl number, constraint nht_pk_pvci primary key (pvci_pk));
comment on table tnht_pvci is 'Relacion de consumos internos por Puntos de venta';
comment on column tnht_pvci.pvci_pk is 'Código relación Pos - Consumo Interno';
comment on column tnht_pvci.coin_pk is 'Consumo Interno asociado, ref. tnht_coin(coin_pk), delete cascade';
comment on column tnht_pvci.ipos_pk is 'Punto de venta asociado, ref. tnht_ipos(ipos_pk), delete cascade';
comment on column tnht_pvci.pvci_limi is 'Limite mensual de consumo interno. Null => ilimitado';
comment on column tnht_pvci.pvci_ilim IS 'Pago ilimitado (1.Si, 2.No)';
comment on column tnht_pvci.pvci_porl IS 'Porciento en caso de no ser ilimitado';	

create table tnht_hour (hour_pk raw(16) not null, hote_pk raw(16) not null, lite_pk raw(16) not null, hour_hori date not null, hour_horf date not null, hour_dias varchar2(7) default '1111111' not null, tprg_pk raw(16) not null, artg_cant number(4) default 1 not null, hour_inac char(1) default '0' not null, hour_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_hour primary key (hour_pk));
comment on table tnht_hour is 'Happy hours - Definiciones';
comment on column tnht_hour.hour_pk is 'Código Happy Hour';
comment on column tnht_hour.hote_pk is 'Hotel al que pertenece, ref. tnht_hote(hote_pk), delete cascade';
comment on column tnht_hour.lite_pk is 'Traduccion de la descripcion, ref. tnht_lite(lite_pk)';
comment on column tnht_hour.hour_hori is 'Hora de inicio';
comment on column tnht_hour.hour_horf is 'Hora Fin';
comment on column tnht_hour.hour_dias is 'Dias de la semana donde se aplica (DLMMJVS)';
comment on column tnht_hour.tprg_pk is 'Tarifa de precios que voy a aplicar, ref. tnht_tprg(tprg_pk)';
comment on column tnht_hour.artg_cant is 'Cantidad de productos por el precio de uno';
comment on column tnht_hour.hour_inac is 'Flag: Happy Hour Inactivo (1. si, 0. no)';

create table tnht_haip (haip_pk raw(16) not null, hour_pk raw(16) not null, ipos_pk raw(16) not null, constraint nht_pk_haip primary key (haip_pk));
comment on table tnht_haip is 'Aplicaciones de Happy Hours por Puntos de Venta';
comment on column tnht_haip.hour_pk is 'Codigo del Happy Hour asociado, ref. tnht_hour(hour_pk), delete cascade';
comment on column tnht_haip.ipos_pk is 'Codigo del Punto de venta asociado, ref. tnht_ipos(ipos_pk), delete cascade';

create table tnht_area (area_pk raw(16) not null, lite_pk raw(16) not null, hote_pk raw(16) not null, area_colo number, area_nuco number(4) default 1 not null, area_inac char(1) default '0' not null, area_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_area primary key (area_pk));
comment on table tnht_area is 'Areas de despacho';
comment on column tnht_area.area_pk is 'Primary key';
comment on column tnht_area.lite_pk is 'Traduccion del nombre del area';
comment on column tnht_area.hote_pk is 'Instalacion';
comment on column tnht_area.area_colo is 'color area despacho';
comment on column tnht_area.area_nuco is 'Cantidad de copias a enviar a la impresora';
comment on column tnht_area.area_inac is 'flag: area despacho inactiva (1.si, 0.no)';

create table tnht_arar(arar_pk raw(16) not null, ipos_pk raw(16) not null, artg_pk raw(16) not null, area_pk raw(16) not null, arar_orde number(4) default 1 not null, constraint nht_pk_arar primary key (arar_pk));
comment on table tnht_arar is 'configuracion productos en areas de despacho';
comment on column tnht_arar.arar_pk is 'Llave Primaria';
comment on column tnht_arar.ipos_pk is 'Punto Venta, ref. tnht_ipos(ipos_pk) delete cascade';
comment on column tnht_arar.artg_pk is 'Producto, ref. tnht_artg(artg_pk) delete cascade';
comment on column tnht_arar.area_pk is 'Area Despacho, ref. tnht_area(area_pk) delete cascade';	
	
create table tnht_prep (prep_pk raw(16) not null, hote_pk raw(16) not null, lite_pk raw(16) not null, prep_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_prep primary key (prep_pk));
comment on table tnht_prep is 'Preparaciones de productos';
comment on column tnht_prep.prep_pk is 'Código de preparacion';
comment on column tnht_prep.hote_pk is 'Código Instalación, ref. tnht_hote(hote_pk)';
comment on column tnht_prep.lite_pk is 'Traduccion de la preparacion, ref. tnht_lite(lite_pk)';
	
create table tnht_grpp (grpp_pk raw(16) not null, prep_pk raw(16) not null, grup_pk raw(16) not null, constraint nht_pk_grpp primary key (grpp_pk));
comment on table tnht_grpp is 'Asociacion de preparaciones a grupos de productos';
comment on column tnht_grpp.prep_pk is 'Código preparacion asociado, ref. tnht_prep(prep_pk), delete cascade';
comment on column tnht_grpp.grup_pk is 'Codigo de grupo asociado, ref. tnht_grup(grup_pk), delete cascade';
	
create table tnht_fapp (fapp_pk raw(16) not null, prep_pk raw(16) not null, fami_pk raw(16) not null, constraint nht_pk_fapp primary key (fapp_pk));
comment on table tnht_fapp is 'Asociacion de preparaciones a familias de productos';
comment on column tnht_fapp.prep_pk is 'Código preparacion asociado, ref. tnht_prep(prep_pk), delete cascade';
comment on column tnht_fapp.fami_pk is 'Codigo de Familia asociada, ref. tnht_fami(fami_pk), delete cascade';	

create table tnht_artp (artp_pk raw(16) not null, prep_pk raw(16) not null, artg_pk raw(16) not null, constraint nht_pk_artp primary key (artp_pk));
comment on table tnht_artp is 'Asociacion de preparaciones y productos';
comment on column tnht_artp.artp_pk is 'Primary key';
comment on column tnht_artp.prep_pk is 'Código preparacion asociado, ref. tnht_prep(prep_pk), delete cascade';
comment on column tnht_artp.artg_pk is 'Código del Producto asociado, ref. tnht_artg(artg_pk), delete cascade';

create table tnht_bcat (bcat_pk raw(16) not null, hote_pk raw(16) not null, lite_pk raw(16) not null, bcat_cate raw(16), bcat_imag varchar2(255), bcat_orde number(4) default 0 not null, bcat_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_bcat primary key (bcat_pk));
comment on table tnht_bcat is 'Categorias y subcategorias de botones de venta';
comment on column tnht_bcat.bcat_pk is 'Id registro';
comment on column tnht_bcat.hote_pk is 'Cod del Hotel';
comment on column tnht_bcat.lite_pk is 'Descripcion de la categoria';
comment on column tnht_bcat.bcat_cate is 'Si es una subcategoria, aqui iria la referencia a la categoria a la que pertenece';
comment on column tnht_bcat.bcat_imag is 'Imagen para el boton de la categoria';
comment on column tnht_bcat.bcat_orde is 'Orden dado en la configuracion de las categorias';

create table tnht_botv (botv_pk raw(16) not null, bcat_pk raw(16) not null, ipos_pk raw(16) not null, artg_pk raw(16) not null, botv_orde number(4) not null, botv_next raw(16), botv_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_botv primary key (botv_pk));
comment on column tnht_botv.bcat_pk is 'Categoria o subcategoria, ref. tnht_bcat';
comment on column tnht_botv.ipos_pk is 'Punto de venta';
comment on column tnht_botv.artg_pk is 'Codigo del producto de venta';
comment on column tnht_botv.botv_orde is 'Orden en la pantalla';
comment on column tnht_botv.botv_next is 'Identificador del proximo producto en su stand-categoria';

create table tnht_vend (vend_pk raw(16) not null, hote_pk raw(16) not null, ipos_pk raw(16) not null, caja_pk raw(16) not null, vend_mesa raw(16), unmo_pk varchar2(3) not null, caja_pave raw(16), vend_seri varchar2(255), vend_codi number(10) not null, vend_dati date not null, vend_hori date not null, vend_datf date, vend_horf date, vend_npax number(4) default 1 not null, vend_comp number(4) default 0 not null, util_pk raw(16) not null, coin_pk raw(16), vend_anul char(1) default 0 not null, mtco_pk raw(16), anul_util raw(16), anul_daan date, anul_dare timestamp(6), anul_obse varchar2(255), ccco_pk raw(16), turn_codi number(4) default 1 not null, vend_desc number default 0 not null, vend_reca number default 0 not null, vend_obse  varchar2(255), duty_pk raw(16), tprg_pk raw(16), vend_ivin char(1) default '1' not null, vend_sign varchar2(255), vend_tota number default 0 not null, fact_ccco raw(16), fact_seri varchar2(255), fact_codi number(10), fact_daem date,    fact_dare date,    fact_guest varchar2(255), fact_addr varchar2(255),fact_naci varchar2(3), fact_nuco varchar2(255), fact_tota number, fact_sign varchar2(255), vend_tipe number, vend_tiva number, vend_proc char(1) default 0 not null, vend_name varchar2(255), vend_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_vend primary key (vend_pk));
comment on table tnht_vend is 'Tickets puntos de venta';
comment on column tnht_vend.hote_pk is 'Codigo del hotel, ref. tnht_hote(hote_pk) delete cascade';
comment on column tnht_vend.ipos_pk is 'Punto Venta, ref. tnht_ipos(ipos_pk)';
comment on column tnht_vend.caja_pk is 'Caja, ref. tnht_caja(caja_pk)';
comment on column tnht_vend.vend_mesa is 'Mesa a que pertenece el ticket ref(tnht_mesa)';
comment on column tnht_vend.unmo_pk is 'Moneda del ticket, ref. tnht_unmo(unmo_pk)';
comment on column tnht_vend.caja_pave is 'Caja donde se cobro el ticket, ref. tnht_caja(caja_pk)';
comment on column tnht_vend.vend_seri is 'Serie del Ticket';
comment on column tnht_vend.vend_codi is 'Consecutivo Serie';
comment on column tnht_vend.vend_dati is 'Fecha de apertura del cheque';
comment on column tnht_vend.vend_hori is 'Hora de apertura del cheque';
comment on column tnht_vend.vend_datf is 'fecha de cierre del ticket';
comment on column tnht_vend.vend_horf is 'Hora de cierre del ticket';
comment on column tnht_vend.vend_npax is 'Numero de pax o personas que consumieron en este ticket';
comment on column tnht_vend.vend_comp is 'Consecutivo de comprobantes emitidos para este ticket';
comment on column tnht_vend.util_pk is 'Usuario del Ticket, ref. tnht_util(util_pk)';
comment on column tnht_vend.coin_pk is 'Concepto de consumo interno, ref tnht_coin(coin_pk)';
comment on column tnht_vend.vend_anul is 'flag: ticket anulado (1.si, 0.no)';
comment on column tnht_vend.mtco_pk is 'Motivo anulacion, ref. tnht_mtco (mtco_pk)';
comment on column tnht_vend.anul_util is 'Usuario que anulo, ref. tnht_util (util_pk)';
comment on column tnht_vend.anul_daan is 'Fecha anulacion';
comment on column tnht_vend.anul_dare is 'Fecha registros anulacion';
comment on column tnht_vend.anul_obse is 'Comentarios anulacion';
comment on column tnht_vend.ccco_pk is 'Cuenta corriente (Habitacion, Huesped, Entidad, Cliente, etc.)';
comment on column tnht_vend.turn_codi is 'Turno en el que se abrio el ticket';
comment on column tnht_vend.vend_desc is 'Valor del descuento general que se le aplica al ticket';
comment on column tnht_vend.vend_reca is 'Valor de recargo que se le aplica al ticket';
comment on column tnht_vend.vend_obse is 'Comentarios del Ticket';
comment on column tnht_vend.duty_pk is 'Servicio POS asociado al ticket, ref. tnht_duty(duty_pk)';
comment on column tnht_vend.tprg_pk is 'Tarifa precios aplicada, ref. tnht_tprg (tprg_pk)';
comment on column tnht_vend.vend_ivin is 'flag: impuesto incluido en los precios';
comment on column tnht_vend.vend_sign is 'Firma digital del ticket';
comment on column tnht_vend.vend_tota is 'Total del Ticket';
comment on column tnht_vend.fact_ccco is 'Cuenta corriente facturada';
comment on column tnht_vend.fact_seri is 'Serie Factura';
comment on column tnht_vend.fact_codi is 'No. Factura';
comment on column tnht_vend.fact_daem is 'Fecha emision factura';
comment on column tnht_vend.fact_dare is 'Fecha registro factura';
comment on column tnht_vend.fact_guest is 'Factura a nombre de...';
comment on column tnht_vend.fact_addr is 'Direccion en la factura';
comment on column tnht_vend.fact_naci is 'Código del País en la factura';
comment on column tnht_vend.fact_nuco is 'No. Fiscal Facturar';
comment on column tnht_vend.fact_tota is 'Total Factura';
comment on column tnht_vend.fact_sign is 'Firma digital de la factura';
comment on column tnht_vend.vend_tipe is 'Tip percent';
comment on column tnht_vend.vend_tiva is 'Tip value';
comment on column tnht_vend.vend_proc is 'flag: ticket procesado o enviado a la cloud (1.si, 0.no)';
comment on column tnht_vend.vend_name is 'Descripcion del Ticket';
	
create table tnht_arve (arve_pk raw(16) not null, vend_pk raw(16) not null, ipos_pk raw(16) not null, caja_pk raw(16) not null, artg_pk raw(16) not null, sepa_pk raw(16), arve_qtds number default 1 not null, arve_totl number not null, arve_vliq number not null, arve_cost number default 0 not null, arve_desc number default 0 not null, arve_pdes number, arve_vsde number not null, arve_reca number default 0 not null, util_pk raw(16) not null, ivas_codi raw(16) not null, arve_por1 number not null, arve_ivas number not null, ivas_cod2 raw(16), arve_por2 number, arve_iva2 number, arve_anul number(4) default 0 not null, arve_impr number(4), arve_imar char(1) default 0 not null, area_pk raw(16), arve_hppy char(1) default 0 not null, arve_adic char(1) default 0 not null, arve_acom raw(16), arve_obsv varchar2(255), arve_obsm varchar2(255), arve_dacr date not null, tide_pk raw(16), ivas_cod3 raw(16), arve_por3 number, arve_iva3 number, sepa_manu char(1) default '0' not null, arve_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_arve primary key (arve_pk));
comment on table tnht_arve is 'Detalles o lineas del ticket';
comment on column tnht_arve.vend_pk is 'Ticket, ref. tnht_vend(vend_pk) delete cascade';
comment on column tnht_arve.ipos_pk is 'Punto de venta que posteo el producto';
comment on column tnht_arve.caja_pk is 'Caja, ref. tnht_caja(caja_pk)';
comment on column tnht_arve.artg_pk is 'Producto asociado, ref. tnht_artg(artg_pk)';
comment on column tnht_arve.sepa_pk is 'Separador de productos en el ticket, ref. tnht_sepa(sepa_Pk)';
comment on column tnht_arve.arve_qtds is 'Cantidad de producto';
comment on column tnht_arve.arve_totl is 'Valor bruto';
comment on column tnht_arve.arve_vliq is 'Valor liquido';
comment on column tnht_arve.arve_cost is 'Costo';
comment on column tnht_arve.arve_desc is 'Valor del descuento';
comment on column tnht_arve.arve_pdes is '% descuento aplicado a la linea';
comment on column tnht_arve.arve_vsde is 'valor de la linea antes de aplicar el descuento';
comment on column tnht_arve.arve_reca is 'Valor de recargo';
comment on column tnht_arve.util_pk is 'Empleado o dependiente que posteo el producto, ref. tnht_util(util_pk)';
comment on column tnht_arve.ivas_codi is 'Codigo del primer Iva aplicado, ref. tnht_tiva(tiva_pk)';
comment on column tnht_arve.arve_por1 is '% impuesto del primer Iva aplicado';
comment on column tnht_arve.arve_ivas is 'Valor primer impuesto';
comment on column tnht_arve.ivas_cod2 is 'Codigo del segundo Iva aplicado, ref. tnht_tiva(tiva_pk)'; 
comment on column tnht_arve.arve_por2 is '% impuesto del segundo Iva aplicado';
comment on column tnht_arve.arve_iva2 is 'Valor segundo impuesto';
comment on column tnht_arve.arve_anul is 'Indica si el producto esta anulado o es una transferencia (0 Activo, 1 Anulado, 2 Transferido)';
comment on column tnht_arve.area_pk is 'Area de despacho a la que fue enviado, ref. tnht_area(area_pk)';
comment on column tnht_arve.arve_hppy is 'flag: producto cargado en happy hour (1.si, 0.no)';
comment on column tnht_arve.arve_adic is 'flag: Indica si el producto fue vendido como una adicion (1.si, 0.no)';
comment on column tnht_arve.arve_acom is 'Si es un producto acompannante de otro, aqui iria la referencia al producto principal del ticket';
comment on column tnht_arve.arve_obsv is 'Observaciones a nivel de linea';
comment on column tnht_arve.arve_obsm is 'Observaciones para la marcha del producto. Este texto sera impreso en la marcha a continuacion de las preparaciones.';
comment on column tnht_arve.arve_dacr is 'Creation date of the line';
comment on column tnht_arve.tide_pk is 'Discount Type Id (ref tnht_tide (tide_pk)';
comment on column tnht_arve.ivas_cod3 is 'Codigo del tercer Iva aplicado, ref. tnht_tiva(tiva_pk)';
comment on column tnht_arve.arve_por3 is '% impuesto del tercer Iva aplicado';
comment on column tnht_arve.arve_iva3 is 'Valor tercer impuesto';
comment on column tnht_arve.sepa_manu is 'TIene separador manual';
comment on column tnht_arve.arve_impr is 'Contador: Indica si ha sido impreso en la impresora de comanda';
comment on column tnht_arve.arve_imar is 'flag: Indica si ha sido marchado (1.si, 0.no) ';

create table tnht_pave (pave_pk raw(16) not null, vend_pk raw(16) not null, tire_pk number(10) not null, fore_pk raw(16), pave_valo number not null, tacr_pk raw(16), ccco_pk raw(16), coin_pk raw(16), pave_obsv varchar2(255), pave_auth varchar2(255), pave_anul number(4) default 0 not null, constraint nht_pk_pave primary key (pave_pk));
comment on table tnht_pave is 'Pagos del Ticket';
comment on column tnht_pave.vend_pk is 'Ticket Ref. tnht_vend(vend_pk), delete cascade';
comment on column tnht_pave.tire_pk is 'Tipo de Pago, ref. tnht_enum(enum_pk)';
comment on column tnht_pave.fore_pk is 'Metodo de Pago, ref. tnht_fore(fore_pk)';
comment on column tnht_pave.pave_valo is 'Valor Pagado';
comment on column tnht_pave.tacr_pk is 'Tarjeta de credito ref. tnht_tacr(tacr_pk)';
comment on column tnht_pave.ccco_pk is 'Cuenta corriente ref. tnht_ccco(ccco_pk)';
comment on column tnht_pave.coin_pk is 'Consumo Interno, ref. tnht_coin(coin_pk)';
comment on column tnht_pave.pave_obsv is 'Observaciones en la linea de pago';
comment on column tnht_pave.pave_auth is 'Código de autorización del pago';
comment on column tnht_pave.pave_anul is 'Motivo de anulacion del pago';

create table tnht_arvp (arvp_pk raw(16) not null, arve_pk raw(16) not null, prep_pk raw(16) not null, constraint nht_pk_arvp primary key (arvp_pk));
comment on table tnht_arvp is 'Preparaciones asociadas a una linea de un ticket ';
comment on column tnht_arvp.arvp_pk is 'Id del registro';
comment on column tnht_arvp.arve_pk is 'Referencia a la linea de un ticket, ref. tnht_arve';
comment on column tnht_arvp.prep_pk is 'Referencia a la preparacion, ref. tnht_prep';

create table tnht_lktb(lktb_pk raw(16) not null, vend_pk raw(16) not null, util_pk raw(16) not null, lktb_seri varchar2(255) not null, lktb_codi number(10) not null, lktb_date date not null, lktb_stim date not null, lktb_tota number not null, lktb_sign varchar2(255), constraint nht_pk_lktb primary key (lktb_pk));
comment on table tnht_lktb is 'Documento Consulta de Mesas';
comment on column tnht_lktb.lktb_pk is 'Primary Key';  
comment on column tnht_lktb.vend_pk is 'Ticket que genera la consulta de mesa';
comment on column tnht_lktb.util_pk is 'Usuario asociado, ref. tnht_util (util_pk)';
comment on column tnht_lktb.lktb_seri is 'Serie';
comment on column tnht_lktb.lktb_codi is 'Numero Documento';
comment on column tnht_lktb.lktb_date is 'Fecha Trabajo';
comment on column tnht_lktb.lktb_stim is 'Fecha Sistema';
comment on column tnht_lktb.lktb_tota is 'Valor';
comment on column tnht_lktb.lktb_sign is 'Firma digital';

create table tnht_loti (loti_pk raw(16) not null, vend_pk raw(16) not null, tope_pk number(10) not null, util_pk raw(16) not null, loti_datr date not null, loti_daho date not null, loti_deta varchar2(4000), loti_lmod  timestamp(6) default current_timestamp not null, constraint nht_pk_loti primary key (loti_pk));
comment on table tnht_loti is 'Log de operaciones del ticket';
comment on column tnht_loti.vend_pk is 'Ticket, ref. tnht_vend(vend_pk)';
comment on column tnht_loti.tope_pk is 'Tipo de operacion, ref. tnht_tope(tope_pk)';
comment on column tnht_loti.util_pk is 'Usuario que realizo la operacion, ref. tnht_util(util_pk)';
comment on column tnht_loti.loti_daho is 'Hora';
comment on column tnht_loti.loti_datr is 'Fecha de trabajo';
comment on column tnht_loti.loti_deta is 'Detalle de la operacion, ej.cuando sea la modificacion de un campo por otro';
	
create table tnht_titr (titr_pk raw(16) not null, ipos_orig raw(16) not null, caja_orig  raw(16) not null, ipos_dest raw(16) not null, caja_dest raw(16), vend_tran raw(16) not null, util_tran raw(16) not null, tran_date date not null, tran_time date not null, vend_acep raw(16), util_acep raw(16), acep_date date, acep_time date, constraint nht_pk_titr primary key (titr_pk));
comment on table tnht_titr is 'Tabla de tickets transferidos';
comment on column tnht_titr.titr_pk is 'Id de la tabla';
comment on column tnht_titr.ipos_orig is 'Punto de venta origen de la transferencia Ref. tnht_ipos(ipos_pk)';
comment on column tnht_titr.ipos_dest is 'Punto de venta destino, ';
comment on column tnht_titr.vend_tran is 'Codigo del ticket original y que se transfiere Ref. tnht_vend(vend_pk)';
comment on column tnht_titr.util_tran is 'Empleado que origina la transferencia del ticket';
comment on column tnht_titr.tran_date is 'Fecha transferencia';
comment on column tnht_titr.tran_time is 'Hora Transferencia';
comment on column tnht_titr.vend_acep is 'Codigo del ticket aceptado es decir en el que se convirtio en el destino, Ref. tnht_vend(vend_pk)';
comment on column tnht_titr.util_acep is 'Usuario que acepto la transferencia';
comment on column tnht_titr.acep_date is 'Fecha aceptar transferencia';
comment on column tnht_titr.acep_time is 'Hora aceptar transferencia';
comment on column tnht_titr.caja_orig is 'caja origen de la transferencia, ref. tnht_caja(caja_pk)';
comment on column tnht_titr.caja_dest is 'caja destino de la transferencia, ref. tnht_caja(caja_pk)';

create table tnht_sedo (sedo_pk raw(16) not null, hote_pk raw(16) not null, sedo_ptex varchar2 (255) not null, sedo_pnum number (10) not null, sedo_orde number (10) default 691 not null, tido_pk number(10) not null, ipos_pk raw(16), caja_pk raw(16), sedo_nufa number(10) not null, sedo_nufi number(10), sedo_dafi date, sedo_seac number(10) not null, sedo_saft varchar2(255), sedo_cont  number(10) default 1 not null, sedo_lmod timestamp(6) default current_timestamp not null, constraint nht_pk_sedo primary key (sedo_pk));
comment on table tnht_sedo is 'Series de Documentos';
comment on column tnht_sedo.sedo_pk is 'Código Serie Documento';
comment on column tnht_sedo.hote_pk is 'Codigo Hotel';
comment on column tnht_sedo.sedo_ptex is 'Prefijo Alfanumerico';
comment on column tnht_sedo.sedo_pnum is 'Prefijo Numerico';
comment on column tnht_sedo.sedo_orde is 'Orden al mostrar la serie de documento';
comment on column tnht_sedo.tido_pk is 'Tipo documento asociado, ref. tnht_tido(tido_pk)';
comment on column tnht_sedo.ipos_pk is 'Punto Venta asociado, ref. tnht_ipos(ipos_pk)';
comment on column tnht_sedo.caja_pk is 'Caja asociada, ref. tnht_caja(caja_pk)';
comment on column tnht_sedo.sedo_nufa is 'Proximo Numero para esta Serie';
comment on column tnht_sedo.sedo_nufi is 'Número Final de esta Serie, null si el final es definido por fecha';
comment on column tnht_sedo.sedo_dafi is 'Fecha Final de esta Serie, null si el final es definido por numero ';
comment on column tnht_sedo.sedo_seac is 'Estado serie documento (activa, inactiva , futura), ref. tnht_enum(enum_pk)';
comment on column tnht_sedo.sedo_saft is 'Firma Digital SAFT-PT';
comment on column tnht_sedo.sedo_cont is 'Contador auxiliar';

create table tnht_lkde (lkde_pk raw(16) not null, lktb_pk raw(16) not null, arve_qtds number not null, arve_totl number not null, arve_vliq number not null, artg_pk raw(16) not null, constraint nht_pk_lkde primary key (lkde_pk));
comment on table tnht_lkde is 'Detalle de consulta de mesa';
comment on column tnht_lkde.lkde_pk is 'Llave Primaria';
comment on column tnht_lkde.lktb_pk is 'Detalle de mesa ref tnht_lktb (lktb_pk)';
comment on column tnht_lkde.arve_qtds is 'Cantidad de la linea';
comment on column tnht_lkde.arve_totl is 'Total de la linea';
comment on column tnht_lkde.arve_vliq is 'Valor sin impuestos de la linea';
comment on column tnht_lkde.artg_pk is 'Producto ref tnht_artg (artg_pk)';

-- triggers

create or replace trigger enum_sync_lite after delete on tnht_enum referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/

create or replace trigger unmo_sync_lite after delete on tnht_unmo referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/

create or replace trigger licl_sync_lite after delete on tnht_licl referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/	

create or replace trigger remu_sync_lite after delete on tnht_remu referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/

create or replace trigger naci_sync_lite after delete on tnht_naci referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_desc;
exception
   when others then raise;
end;
/

create or replace trigger tope_sync_lite after delete on tnht_tope referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/	

create or replace trigger grpr_sync_lite after delete on tnht_grpr referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/

create or replace trigger perm_sync_lite after delete on tnht_perm referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/

create or replace trigger mtco_sync_lite after delete on tnht_mtco referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/

create or replace trigger fore_sync_lite after delete on tnht_fore referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_desc;
   delete from tnht_lite where lite_pk = :old.lite_abre;
exception
   when others then raise;
end;
/

create or replace trigger regi_sync_lite after delete on tnht_regi referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/	

create or replace trigger seim_sync_lite after delete on tnht_seim referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/

create or replace trigger tiva_sync_lite after delete on tnht_tiva referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/	

create or replace trigger esim_sync_lite after delete on tnht_esim referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/

create or replace trigger tide_sync_lite after delete on tnht_tide referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/

create or replace trigger cacr_sync_lite after delete on tnht_cacr referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/	

create or replace trigger tido_sync_lite after delete on tnht_tido referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/

create or replace trigger clas_sync_lite after delete on tnht_clas referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/	

create or replace trigger sepa_sync_lite after delete on tnht_sepa referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/

create or replace trigger artg_sync_lite after delete on tnht_artg referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_abre;
   delete from tnht_lite where lite_pk = :old.lite_desc;
exception
   when others then raise;
end;
/

create or replace trigger duty_sync_lite after delete on tnht_duty referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/

create or replace trigger tprg_sync_lite after delete on tnht_tprg referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/

create or replace trigger caja_sync_lite after delete on tnht_caja referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/	

create or replace trigger menu_sync_lite after delete on tnht_menu referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/

create or replace trigger tick_sync_lite after delete on tnht_tick referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/

create or replace trigger ipos_sync_lite after delete on tnht_ipos referencing new as new old as old
for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_desc;
   delete from tnht_lite where lite_pk = :old.lite_abre;
exception
   when others then raise;
end;
/

create or replace trigger salo_sync_lite after delete on tnht_salo referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/	

create or replace trigger hour_sync_lite after delete on tnht_hour referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/

create or replace trigger area_sync_lite after delete on tnht_area referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;   
exception
   when others then raise;
end;
/	
	
create or replace trigger prep_sync_lite after delete on tnht_prep referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/	
	
create or replace trigger bcat_sync_lite after delete on tnht_bcat referencing new as new old as old for each row
begin
   delete from tnht_lite where lite_pk = :old.lite_pk;
exception
   when others then raise;
end;
/
	
-- contraints 

alter table tnht_mult add (
	constraint nht_fk_lite_mult foreign key (lite_pk) references tnht_lite (lite_pk) on delete cascade);
alter table tnht_enum add (
	constraint nht_fk_lite_enum foreign key (lite_pk) references tnht_lite (lite_pk));	
alter table tnht_unmo add (
	constraint nht_fk_lite_unmo foreign key (lite_pk) references tnht_lite (lite_pk));
alter table tnht_licl add (
	constraint nht_fk_lite_licl foreign key (lite_pk) references tnht_lite (lite_pk));	
alter table tnht_remu add (
	constraint nht_fk_lite_remu foreign key (lite_pk) references tnht_lite (lite_pk));	
alter table tnht_naci add (
	constraint nht_fk_lite_naci_01 foreign key (lite_desc) references tnht_lite (lite_pk),
	constraint nht_fk_licl_naci foreign key (licl_pk) references tnht_licl (licl_pk),
	constraint nht_fk_remu_naci foreign key (remu_pk) references tnht_remu (remu_pk));	
alter table tnht_tope add (
	constraint nht_fk_lite_tope foreign key (lite_pk) references tnht_lite (lite_pk));	
alter table tnht_grpr add (
    constraint nht_fk_lite_grpr foreign key (lite_pk) references tnht_lite (lite_pk));	
alter table tnht_perm add (
	constraint nht_fk_lite_perm foreign key (lite_pk) references tnht_lite (lite_pk),
	constraint nht_fk_grpr_perm foreign key (grpr_pk) references tnht_grpr (grpr_pk));	
alter table tnht_peap add (
	constraint nht_fk_perm_peap foreign key (perm_pk) references tnht_perm (perm_pk) on delete cascade,
	constraint nht_fk_appl_peap foreign key (appl_pk) references tnht_appl (appl_pk) on delete cascade);	
alter table tnht_hote add (
	constraint nht_fk_naci_hote foreign key (naci_pk) references tnht_naci (naci_pk));	
alter table tnht_util add (
	constraint nht_fk_hote_util foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade);
alter table tnht_peut add (
	constraint nht_fk_util_peut foreign key (util_pk) references tnht_util (util_pk) on delete cascade,
	constraint nht_fk_peap_peut foreign key (peap_pk) references tnht_peap (peap_pk) on delete cascade,
	constraint nht_fk_enum_peut foreign key (secu_code) references tnht_enum (enum_pk));
alter table tnht_loop add (
	constraint nht_fk_hote_loop foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade,
	constraint nht_fk_enum_loop foreign key (loop_crit) references tnht_enum (enum_pk),
	constraint nht_fk_tope_loop foreign key (tope_pk) references tnht_tope (tope_pk),
	constraint nht_fk_util_loop foreign key (util_pk) references tnht_util (util_pk));
alter table tnht_mtco add (
	constraint nht_fk_lite_mtco foreign key (lite_pk) references tnht_lite (lite_pk),
	constraint nht_fk_hote_mtco foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade,
	constraint nht_fk_enum_mtco foreign key (mtco_tica) references tnht_enum (enum_pk));
alter table tnht_fore add (
	constraint nht_fk_lite_fore_01 foreign key (lite_desc) references tnht_lite (lite_pk),
	constraint nht_fk_lite_fore_02 foreign key (lite_abre) references tnht_lite (lite_pk),
	constraint nht_fk_unmo_fore foreign key (unmo_pk) references tnht_unmo (unmo_pk),
	constraint nht_fk_hote_fore foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade);
alter table tnht_regi add (
	constraint nht_fk_lite_regi foreign key (lite_pk) references tnht_lite (lite_pk),
	constraint nht_fk_naci_regi foreign key (naci_pk) references tnht_naci (naci_pk) on delete cascade);
alter table tnht_seim add (
	constraint nht_fk_lite_seim foreign key (lite_pk) references tnht_lite (lite_pk));
alter table tnht_tiva add (
	constraint nht_fk_regi_tiva foreign key (regi_pk) references tnht_regi (regi_pk),
	constraint nht_fk_seim_tiva foreign key (seim_pk) references tnht_seim (seim_pk),
	constraint nht_fk_lite_tiva foreign key (lite_pk) references tnht_lite (lite_pk));
alter table tnht_esim add (
    constraint nht_fk_hote_esim foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade,
	constraint nht_fk_lite_esim foreign key (lite_pk) references tnht_lite (lite_pk));
alter table tnht_tide add (
	constraint nht_fk_lite_tide foreign key (lite_pk) references tnht_lite (lite_pk),
	constraint nht_fk_hote_tide foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade,
	constraint nht_fk_appl_tide foreign key (appl_pk) references tnht_appl (appl_pk));	
alter table tnht_cacr add (
	constraint nht_fk_lite_cacr foreign key (lite_pk) references tnht_lite (lite_pk),
	constraint nht_fk_hote_cacr foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade);
alter table tnht_tido add (
	constraint nht_fk_tido_tido foreign key (tido_pare) references tnht_tido (tido_pk),
	constraint nht_fk_lite_tido foreign key (lite_pk) references tnht_lite (lite_pk));
alter table tnht_tacr add (
	constraint nht_fk_cacr_tacr foreign key (cacr_pk) references tnht_cacr (cacr_pk));
alter table tnht_clas add (
	constraint nht_fk_hote_clas foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade,
	constraint nht_fk_lite_clas foreign key (lite_pk) references tnht_lite (lite_pk));
alter table tnht_grup add (
	constraint nht_fk_clas_grup foreign key (grup_pk) references tnht_clas (clas_pk) on delete cascade);	
alter table tnht_fami add (
	constraint nht_fk_clas_fami foreign key (fami_pk) references tnht_clas (clas_pk) on delete cascade,
	constraint nht_fk_grup_fami foreign key (grup_pk) references tnht_grup (grup_pk));
alter table tnht_sfam add (
	constraint nht_fk_clas_sfam foreign key (sfam_pk) references tnht_clas (clas_pk) on delete cascade,
	constraint nht_fk_fami_sfam foreign key (fami_pk) references tnht_fami (fami_pk));
alter table tnht_sepa add (
	constraint nht_fk_hote_sepa foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade,
	constraint nht_fk_lite_sepa foreign key (lite_pk) references tnht_lite (lite_pk));
alter table tnht_artg add (
	constraint nht_fk_hote_artg foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade,
	constraint nht_fk_lite_artg_01 foreign key (lite_abre) references tnht_lite (lite_pk),
	constraint nht_fk_lite_artg_02 foreign key (lite_desc) references tnht_lite (lite_pk),
	constraint nht_fk_fami_artg foreign key (fami_pk) references tnht_fami (fami_pk),
	constraint nht_fk_sfam_artg foreign key (sfam_pk) references tnht_sfam (sfam_pk),
	constraint nht_fk_sepa_artg foreign key (sepa_pk) references tnht_sepa (sepa_pk),
	constraint nht_fk_enum_artg_01 foreign key (artg_tipr) references tnht_enum (enum_pk));
alter table tnht_arim add (	
	constraint nht_fk_artg_arim foreign key (artg_pk) references tnht_artg (artg_pk) on delete cascade);
alter table tnht_imse add (
	constraint nht_fk_artg_imse foreign key (artg_pk) references tnht_artg (artg_pk) on delete cascade,
	constraint nht_fk_esim_imse foreign key (esim_pk) references tnht_esim (esim_pk),
	constraint nht_fk_tiva_imse_01 foreign key (tiva_cod1) references tnht_tiva (tiva_pk),
	constraint nht_fk_tiva_imse_02 foreign key (tiva_cod2) references tnht_tiva (tiva_pk),
	constraint nht_fk_tiva_imse_03 foreign key (tiva_cod3) references tnht_tiva (tiva_pk),
	constraint nht_fk_enum_imse_01 foreign key (tiva_apl2) references tnht_enum (enum_pk),
	constraint nht_fk_enum_imse_02 foreign key (tiva_apl3) references tnht_enum (enum_pk));
alter table tcfg_npos add (
	constraint nht_fk_hote_cfg_npos foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade,
	constraint nht_fk_fore_cfg_npos foreign key (para_fore) references tnht_fore (fore_pk),
	constraint nht_fk_esim_cfg_npos foreign key (para_esim) references tnht_esim (esim_pk),
	constraint nht_fk_mtco_cfg_npos foreign key (para_mtco) references tnht_mtco (mtco_pk),
	constraint nht_fk_enum_cfg_npos_02 foreign key (sign_tipo) references tnht_enum (enum_pk),
	constraint nht_fk_enum_cfg_npos_03 foreign key (para_tico) references tnht_enum (enum_pk),
	constraint nht_fk_artg_npos foreign key (para_tise) references tnht_artg (artg_pk));
alter table tnht_duty add (
	constraint nht_fk_lite_duty foreign key (lite_pk) references tnht_lite (lite_pk),
	constraint nht_fk_hote_duty foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade);
alter table tnht_tprg add (
	constraint nht_fk_hote_tprg foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade,
	constraint nht_fk_lite_tprg foreign key (lite_pk) references tnht_lite (lite_pk),
	constraint nht_fk_enum_tprg_01 foreign key (tprg_type) references tnht_enum (enum_pk),
	constraint nht_fk_unmo_tprg foreign key (unmo_pk) references tnht_unmo (unmo_pk));
alter table tnht_tppe add (
	constraint nht_fk_tprg_tppe foreign key (tprg_pk) references tnht_tprg (tprg_pk) on delete cascade);
alter table tnht_tprb add (
	constraint nht_fk_tprg_tprb foreign key (tprg_pk) references tnht_tprg (tprg_pk) on delete cascade);
alter table tnht_tprl add (
	constraint nht_fk_tppe_tprl foreign key (tppe_pk) references tnht_tppe (tppe_pk) on delete cascade,
	constraint nht_fk_artg_tpri foreign key (artg_pk) references tnht_artg (artg_pk) on delete Cascade);
alter table tnht_caja add (
	constraint nht_fk_hote_caja foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade,
	constraint nht_fk_lite_caja foreign key (lite_pk) references tnht_lite (lite_pk));
alter table tnht_menu add (
	constraint nht_fk_hote_menu foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade,
	constraint nht_fk_lite_menu foreign key (lite_pk) references tnht_lite (lite_pk));
alter table tnht_mede add (
	constraint nht_fk_menu_mede foreign key (menu_pk) references tnht_menu (menu_pk) on delete cascade,
	constraint nht_fk_artg_mede foreign key (artg_pk) references tnht_artg (artg_pk) on delete cascade,
	constraint nht_fk_sepa_mede foreign key (sepa_pk) references tnht_sepa (sepa_pk));	
alter table tnht_tick add (
	constraint nht_fk_hote_tick foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade,	
	constraint nht_fk_lite_tick foreign key (lite_pk) references tnht_lite (lite_pk));
alter table tnht_ipos add (
	constraint nht_fk_hote_ipos foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade,
	constraint nht_fk_lite_ipos_01 foreign key (lite_abre) references tnht_lite (lite_pk),
	constraint nht_fk_lite_ipos_02 foreign key (lite_desc) references tnht_lite (lite_pk),
	constraint nht_fk_tprg_ipos_01 foreign key (tprg_stdr) references tnht_tprg (tprg_pk),
	constraint nht_fk_tprg_ipos_02 foreign key (tprg_cint) references tnht_tprg (tprg_pk), 
	constraint nht_fk_tprg_ipos_03 foreign key (tprg_pens) references tnht_tprg (tprg_pk),
	constraint nht_fk_tick_ipos_01 foreign key (cnfg_tick) references tnht_tick (tick_pk),
	constraint nht_fk_tick_ipos_02 foreign key (cnfg_fact) references tnht_tick (tick_pk),
	constraint nht_fk_tick_ipos_03 foreign key (cnfg_comp) references tnht_tick (tick_pk),
	constraint nht_fk_menu_ipos_01 foreign key (menu_mon) references tnht_menu (menu_pk),
    constraint nht_fk_menu_ipos_02 foreign key (menu_tue) references tnht_menu (menu_pk),
    constraint nht_fk_menu_ipos_03 foreign key (menu_wed) references tnht_menu (menu_pk),
    constraint nht_fk_menu_ipos_04 foreign key (menu_thu) references tnht_menu (menu_pk),
    constraint nht_fk_menu_ipos_05 foreign key (menu_fri) references tnht_menu (menu_pk),
    constraint nht_fk_menu_ipos_06 foreign key (menu_sat) references tnht_menu (menu_pk),
    constraint nht_fk_menu_ipos_07 foreign key (menu_sun) references tnht_menu (menu_pk));
alter table tnht_capv add (
	constraint nht_fk_ipos_capv foreign key (ipos_pk) references tnht_ipos (ipos_pk) on delete cascade,
	constraint nht_fk_caja_capv foreign key (caja_pk) references tnht_caja (caja_pk) on delete cascade);
alter table tnht_salo add (
	constraint nht_fk_lite_salo foreign key (lite_pk) references tnht_lite (lite_pk), 
	constraint nht_fk_hote_salo foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade);	
alter table tnht_ipsl add (
	constraint nht_fk_ipos_ipsl foreign key (ipos_pk) references tnht_ipos (ipos_pk) on delete cascade,
	constraint nht_fk_salo_ipsl foreign key (salo_pk) references tnht_salo (salo_pk) on delete cascade);	
alter table tnht_iput add (
    constraint nht_fk_util_iput foreign key (util_pk) references tnht_util (util_pk) on delete cascade,
    constraint nht_fk_ipos_iput foreign key (ipos_pk) references tnht_ipos (ipos_pk) on delete cascade,
    constraint nht_fk_enum_iput foreign key (secu_code) references tnht_enum (enum_pk));	
alter table tnht_mesa add (
	constraint nht_fk_salo_mesa foreign key (salo_pk) references tnht_salo(salo_pk) on delete cascade,
	constraint nht_fk_enum_mesa foreign key (mesa_type) references tnht_enum(enum_pk));
alter table tnht_pvar add (
	constraint nht_fk_ipos_pvar foreign key (ipos_pk) references tnht_ipos (ipos_pk) on delete cascade,
	constraint nht_fk_artg_pvar foreign key (artg_pk) references tnht_artg (artg_pk) on delete cascade);
alter table tnht_coin add (
	constraint nht_fk_clas_coin foreign key (coin_pk) references tnht_clas (clas_pk) on delete cascade);
alter table tnht_pvci add (
	constraint nht_fk_coin_pvci foreign key (coin_pk) references tnht_coin (coin_pk) on delete cascade,
	constraint nht_fk_ipos_pvci foreign key (ipos_pk) references tnht_ipos (ipos_pk) on delete cascade);
alter table tnht_hour add (
	constraint nht_fk_tprg_hour foreign key (tprg_pk) references tnht_tprg (tprg_pk),
	constraint nht_fk_hote_hour foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade,
	constraint nht_fk_lite_hour foreign key (lite_pk) references tnht_lite (lite_pk));
alter table tnht_haip add (
	constraint nht_fk_ipos_haip foreign key (ipos_pk) references tnht_ipos (ipos_pk) on delete cascade,
	constraint nht_fk_hour_haip foreign key (hour_pk) references tnht_hour (hour_pk) on delete cascade);	
alter table tnht_area add (
	constraint nht_fk_lite_area foreign key (lite_pk) references tnht_lite (lite_pk),
	constraint nht_fk_hote_area foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade);
alter table tnht_arar add (
	constraint nht_fk_ipos_arar foreign key (ipos_pk) references tnht_ipos (ipos_pk) on delete cascade,
	constraint nht_fk_artg_arar foreign key (artg_pk) references tnht_artg (artg_pk) on delete cascade,
	constraint nht_fk_area_arar foreign key (area_pk) references tnht_area (area_pk) on delete cascade); 
alter table tnht_prep add (
	constraint nht_fk_hote_prep foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade,
	constraint nht_fk_lite_prep foreign key (lite_pk) references tnht_lite (lite_pk));
alter table tnht_grpp add (
	constraint nht_fk_prep_grpp foreign key (prep_pk) references tnht_prep (prep_pk) on delete cascade,
	constraint nht_fk_grup_grpp foreign key (grup_pk) references tnht_grup (grup_pk) on delete cascade);
alter table tnht_fapp add (
	constraint nht_fk_prep_fapp foreign key (prep_pk) references tnht_prep (prep_pk) on delete cascade,
	constraint nht_fk_fami_fapp foreign key (fami_pk) references tnht_fami (fami_pk) on delete cascade);
alter table tnht_artp add (
	constraint nht_fk_prep_artp foreign key (prep_pk) references tnht_prep (prep_pk) on delete cascade,
	constraint nht_fk_artg_artp foreign key (artg_pk) references tnht_artg (artg_pk) on delete cascade);
alter table tnht_bcat add (
	constraint nht_fk_lite_bcat foreign key (lite_pk) references tnht_lite (lite_pk),
	constraint nht_fk_hote_bcat foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade,
	constraint nht_fk_bact_bcat foreign key (bcat_cate) references tnht_bcat (bcat_pk));
alter table tnht_botv add (
	constraint nht_fk_bartg_botv foreign key (artg_pk) references tnht_artg (artg_pk) on delete cascade,
	constraint nht_fk_bcat_botv foreign key (bcat_pk) references tnht_bcat (bcat_pk) on delete cascade,
	constraint nht_fk_ipos_botv foreign key (ipos_pk) references tnht_ipos (ipos_pk) on delete cascade);	
alter table tnht_vend add (
	constraint nht_fk_hote_vend foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade,
	constraint nht_fk_caja_vend_01 foreign key (caja_pk) references tnht_caja (caja_pk),
	constraint nht_fk_ipos_vend_01 foreign key (ipos_pk) references tnht_ipos (ipos_pk),
	constraint nht_fk_mesa_vend foreign key (vend_mesa) references tnht_mesa (mesa_pk), 
	constraint nht_fk_unmo_vend_01 foreign key (unmo_pk) references tnht_unmo (unmo_pk),
	constraint nht_fk_naci_vend foreign key (fact_naci) references tnht_naci (naci_pk),
	constraint nht_fk_caja_vend_02 foreign key (caja_pave) references tnht_caja (caja_pk),
	constraint nht_fk_util_vend_01 foreign key (util_pk) references tnht_util (util_pk),
	constraint nht_fk_coin_vend foreign key (coin_pk) references tnht_coin (coin_pk),
	constraint nht_fk_mtco_vend foreign key (mtco_pk) references tnht_mtco (mtco_pk),
	constraint nht_fk_util_vend_02 foreign key (anul_util) references tnht_util (util_pk),
	constraint nht_fk_duty_vend foreign key (duty_pk) references tnht_duty (duty_pk),
	constraint nht_fk_tprg_vend foreign key (tprg_pk) references tnht_tprg (tprg_pk));	
alter table tnht_arve add (
	constraint nht_fk_vend_arve foreign key (vend_pk) references tnht_vend (vend_pk) on delete cascade, 
	constraint nht_fk_caja_arve foreign key (caja_pk) references tnht_caja (caja_pk),
	constraint nht_fk_ipos_arve foreign key (ipos_pk) references tnht_ipos (ipos_pk),
	constraint nht_fk_artg_arve_01 foreign key (artg_pk) references tnht_artg (artg_pk),
	constraint nht_fk_artg_arve_02 foreign key (arve_acom) references tnht_artg (artg_pk),
	constraint nht_fk_sepa_arve foreign key (sepa_pk) references tnht_sepa (sepa_pk),
	constraint nht_fk_util_arve foreign key (util_pk) references tnht_util (util_pk),
	constraint nht_fk_tiva_arve_01 foreign key (ivas_codi) references tnht_tiva (tiva_pk),
	constraint nht_fk_tiva_arve_02 foreign key (ivas_cod2) references tnht_tiva (tiva_pk), 
	constraint nht_fk_tiva_arve_03 foreign key (ivas_cod3) references tnht_tiva (tiva_pk),
	constraint nht_fk_area_arve foreign key (area_pk) references tnht_area (area_pk),
	constraint nht_fk_tide_arve foreign key (tide_pk) references tnht_tide (tide_pk));
alter table tnht_pave add (
	constraint nht_fk_vend_pave foreign key (vend_pk) references tnht_vend (vend_pk) on delete cascade, 
	constraint nht_fk_enum_pave_01 foreign key (tire_pk) references tnht_enum (enum_pk),
	constraint nht_fk_fore_pave foreign key (fore_pk) references tnht_fore (fore_pk),
	constraint nht_fk_tacr_pave foreign key (tacr_pk) references tnht_tacr (tacr_pk),
	constraint nht_fk_coin_pave foreign key (coin_pk) references tnht_coin (coin_pk));
alter table tnht_arvp add (
	constraint nht_fk_arve_arvp foreign key (arve_pk) references tnht_arve (arve_pk) on delete cascade, 
	constraint nht_fk_prep_arvp foreign key (prep_pk) references tnht_prep (prep_pk));
alter table tnht_lktb add (
	constraint nht_fk_vend_lktb foreign key (vend_pk) references tnht_vend (vend_pk) on delete cascade, 
	constraint nht_fk_util_lktb foreign key (util_pk) references tnht_util (util_pk));
alter table tnht_loti add (
	constraint nht_fk_vend_loti foreign key (vend_pk) references tnht_vend (vend_pk) on delete cascade, 
	constraint nht_fk_tope_loti foreign key (tope_pk) references tnht_tope (tope_pk),
	constraint nht_fk_util_loti foreign key (util_pk) references tnht_util (util_pk));	
alter table tnht_sedo add (
	constraint nht_fk_hote_sedo foreign key (hote_pk) references tnht_hote (hote_pk) on delete cascade,
	constraint nht_fk_tido_sedo foreign key (tido_pk) references tnht_tido (tido_pk),
	constraint nht_fk_caja_sedo foreign key (caja_pk) references tnht_caja (caja_pk) on delete cascade,
	constraint nht_fk_ipos_sedo foreign key (ipos_pk) references tnht_ipos (ipos_pk) on delete cascade,
	constraint nht_fk_enum_sedo_01 foreign key (sedo_seac) references tnht_enum (enum_pk),
	constraint nht_fk_enum_sedo_02 foreign key (sedo_orde) references tnht_enum (enum_pk));
alter table tnht_lkde add (
	constraint nht_fk_lktb_lkde foreign key (lktb_pk) references tnht_lktb (lktb_pk) on delete cascade,
	constraint nht_fk_artg_lkde foreign key (artg_pk) references tnht_artg (artg_pk));
	
create index nht_ind_artg_01 on tnht_artg (fami_pk, sfam_pk, sepa_pk);
create index nht_ind_imse_01 on tnht_imse (esim_pk, artg_pk);	
	
-- views

create or replace force view vnht_mult (lang_pk, lite_pk, mult_desc) as
   select qnht_lite.lang_pk, qnht_lite.lite_pk, nvl (tnht_mult.mult_desc, qnht_mult.mult_desc) mult_desc
   from (select tnht_lang.lang_pk, tnht_lite.lite_pk
         from tnht_lang, tnht_lite) qnht_lite left join tnht_mult on qnht_lite.lang_pk = tnht_mult.lang_pk
         and qnht_lite.lite_pk = tnht_mult.lite_pk left join (
			select tnht_mult.lite_pk, tnht_mult.mult_desc
            from tnht_mult, tnht_lang
            where tnht_lang.lang_pk = tnht_mult.lang_pk and (tnht_lang.lang_prio, tnht_mult.lite_pk) in 
                (select min (tnht_lang.lang_prio), tnht_mult.lite_pk
                 from tnht_lang, tnht_mult where tnht_lang.lang_pk = tnht_mult.lang_pk group by tnht_mult.lite_pk)) qnht_mult
    on qnht_lite.lite_pk = qnht_mult.lite_pk;	

create or replace view vnht_coin as
select coin.coin_pk, clas.hote_pk, clas.lite_pk, coin.coin_invi, coin.coin_inac 
from tnht_coin coin, tnht_clas clas
where coin.coin_pk = clas.clas_pk;

create or replace view vnht_grup as
select grup.grup_pk, clas.hote_pk, clas.lite_pk, grup.grup_cheq, grup.grup_reca   
from tnht_grup grup, tnht_clas clas
where grup.grup_pk = clas.clas_pk;

create or replace view vnht_fami as
select fami.fami_pk, clas.hote_pk, clas.lite_pk, fami.grup_pk
from tnht_fami fami, tnht_clas clas
where fami.fami_pk = clas.clas_pk;

create or replace view vnht_sfam as
select sfam.sfam_pk, clas.hote_pk, clas.lite_pk, sfam.fami_pk
from tnht_sfam sfam, tnht_clas clas
where sfam.sfam_pk = clas.clas_pk;

create or replace view vnht_artg as
select  artg.artg_pk, artg.hote_pk, artg.artg_codi, artg.lite_abre, 
        artg.lite_desc, fami.grup_pk, artg.fami_pk, artg.sfam_pk, 
		artg.sepa_pk, artg.artg_step, artg.artg_imag, artg.artg_colo, 
		artg.artg_tipr
from    tnht_artg artg, tnht_fami fami
where   artg.fami_pk = fami.fami_pk;
	
