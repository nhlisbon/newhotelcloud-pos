CREATE OR REPLACE PROCEDURE pnht_get_serie (
  p_pk OUT RAW,
  p_cursor0 OUT SYS_REFCURSOR,
  p_cursor1 OUT SYS_REFCURSOR)
IS
BEGIN
  SELECT sedo_pk INTO p_pk FROM (
    SELECT sedo_pk FROM TNHT_SEDO
    WHERE sedo_sync <= SYSTIMESTAMP AND sedo_sync is not null ORDER BY sedo_sync
  ) WHERE ROWNUM <= 1;

  OPEN p_cursor0 FOR SELECT 'M' AS change_operation,
                     ROWID AS row_id, sedo_pk,
                     hote_pk, sedo_ptex, sedo_pnum, sedo_orde, sedo_nufa,
                     sedo_nufi, sedo_dafi, sedo_seac, sedo_saft, sedo_lmod
                     FROM TNHT_SEDO WHERE sedo_pk = p_pk;

  OPEN p_cursor1 FOR SELECT 'U' AS change_operation,
                     ROWID AS row_id, ipos_pk, caja_pk, tido_pk, sedo_pk
                     FROM TNHT_SEDO WHERE sedo_seac = 721 AND sedo_pk = p_pk;

EXCEPTION
  WHEN NO_DATA_FOUND THEN
    p_pk := null;
END;
/

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.SYNC_VERS = '20170327'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.SYNC_VERS) VALUES (1, '20170327');
END;
/

COMMIT;