﻿ALTER TABLE TNHT_TICK ADD (TICK_SUBT CHAR(1) DEFAULT '1' NOT NULL);
COMMENT ON COLUMN TNHT_TICK.TICK_SUBT IS 'Flag: Mostrar el subtotal en la impresion (1. si 0.no)';

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20181228'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20181228');
END;
/

COMMIT;