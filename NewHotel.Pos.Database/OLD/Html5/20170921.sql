create table tnht_vers ( vers_numb varchar2(255) not null, vers_date date not null, primary key (vers_numb));
comment on table tnht_vers is 'Control de Versiones';
comment on column tnht_vers.vers_numb is 'Version';
comment on column tnht_vers.vers_date is 'Fecha del Registro';

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20170921'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20170921');
END;
/

COMMIT;
