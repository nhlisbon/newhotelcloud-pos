ALTER TABLE TCFG_NPOS ADD NPOS_DEAU CHAR(1 CHAR) DEFAULT '0' NOT NULL;
COMMENT ON COLUMN TCFG_NPOS.NPOS_DEAU IS 'flag: Aplicar descuentos automáticos sobre productos si habilitado para clientes (1.si 0.no)';

ALTER TABLE TNHT_CLIE ADD CLIE_DEAU CHAR(1 CHAR) DEFAULT '0' NOT NULL;
COMMENT ON COLUMN TNHT_CLIE.CLIE_DEAU IS 'flag: Aplicar descuentos automáticos sobre productos si habilitado en la configuración general (1.si 0.no)';

ALTER TABLE TNHT_ARTG ADD ARTG_PDES NUMBER;
COMMENT ON COLUMN TNHT_ARTG.ARTG_PDES IS 'Descuento automático en porciento sobre el precio del producto';

ALTER TABLE TNHT_ARVE ADD ARVE_DEAU CHAR(1 CHAR) DEFAULT '0' NOT NULL;
COMMENT ON COLUMN TNHT_ARVE.ARVE_DEAU IS 'flag: Descuento automático aplicado para esta línea (1.si 0.no)';

CREATE OR REPLACE VIEW VNHT_ARTG AS
SELECT ARTG.ARTG_PK,
       ARTG.HOTE_PK,
       ARTG.ARTG_CODI,
       ARTG.LITE_ABRE,
       ARTG.LITE_DESC,
       FAMI.GRUP_PK,
       ARTG.FAMI_PK,
       ARTG.SFAM_PK,
       ARTG.SEPA_PK,
       ARTG.ARTG_STEP,
       ARTG.ARTG_IMAG,
       ARTG.ARTG_COLO,
       ARTG.ARTG_TIPR,
       ARTG.ARTG_TABL,
       ARTG.ARTG_PDES,
       ARTG.ARTG_INAC
FROM TNHT_ARTG ARTG INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK;

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20210219'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20210219');
END;
/

COMMIT;


DELETE FROM TNHT_APPL;
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (101, 'NewHotel Cloud PMS');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (107, 'NewHotel Cloud POS');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (108, 'NewHotel Cloud POS Mobile');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (109, 'NewHotel Cloud Conta');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (110, 'NewHotel Cloud Condo');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (111, 'NewHotel Cloud SPA');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (112, 'NewHotel Cloud Events');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (113, 'NewHotel Cloud CRM');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (114, 'NewHotel Cloud Stock');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (115, 'NewHotel Cloud Golf');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (116, 'NewHotel Cloud Housekeeping');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (117, 'NewHotel Business Hub');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (118, 'NewHotel Cloud DataManager');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (119, 'NewHotel Cloud Booking');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (120, 'NewHotel Service Portal');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (222, 'NewHotel GES');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (301, 'Cloud Mobile Manager');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (302, 'Cloud Mobile Guest');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (303, 'Mobile HKeeper');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (304, 'NewHotel Mobile Reception');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (305, 'NewHotel Mobile Kiosk');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (306, 'NewHotel Mobile Concierge');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (307, 'NewHotel Online Check In');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (308, 'NewHotel Table Reservation');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (309, 'NewHotel Mobile Waiter');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (310, 'NewHotel Mobile CRM');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (401, 'Integrations - NewHotel ETL');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (402, 'Integration - Climber Revenue');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (403, 'Integrations - Delta Scanner');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (404, 'Integrations - HiJiffy');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (405, 'Integrations - TTLock');
INSERT INTO TNHT_APPL (APPL_PK, APPL_NAME) VALUES (601, 'Interfaces');
COMMIT;