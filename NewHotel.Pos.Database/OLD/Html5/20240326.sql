ALTER TABLE TCFG_NPOS ADD NPOS_IBTN VARCHAR(1) DEFAULT '1';
COMMENT ON COLUMN TCFG_NPOS.NPOS_IBTN IS 'Shows/hides the button to print the invoice';

BEGIN
    MERGE INTO TCFG_GENE PARA
    USING DUAL
    ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN
        UPDATE SET PARA.DATE_VERS = '20240326'
    WHEN NOT MATCHED THEN
        INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20240326');
END;
/

COMMIT;