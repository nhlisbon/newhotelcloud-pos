CREATE OR REPLACE TRIGGER TNHT_VEND_UPDATE_SERI
FOR UPDATE
OF VEND_SERI
  ,FACT_SERI
ON TNHT_VEND
REFERENCING NEW AS NEW OLD AS OLD
compound trigger
   	
    type vend_serie is record (vend_pk raw(16), vend_seri varchar2(255 Byte), vend_codi number(10));
    type array_vend_serie is table of vend_serie;
	
	type fact_serie is record (vend_pk raw(16), fact_seri varchar2(255 Byte), fact_codi number(10));
    type array_fact_serie is table of fact_serie; 
    
    vend_series array_vend_serie := array_vend_serie();
    fact_series array_fact_serie := array_fact_serie();
    
    serie_unique number(1);
    serie_used number(1);
    serie_previo number(1);
after each row is
begin
    if (:new.vend_seri is not null and :old.vend_seri is null) 
    then
        vend_series.EXTEND;
        vend_series(vend_series.LAST).vend_pk := :new.vend_pk;
        vend_series(vend_series.LAST).vend_seri := :new.vend_seri;
        vend_series(vend_series.LAST).vend_codi := :new.vend_codi;
    elsif (:old.vend_seri is not null) 
    then
        RAISE_APPLICATION_ERROR(-20008, 'Ticket serie can not changue');
    end if; 
    
    if (:new.fact_seri is not null and :old.fact_seri is null) 
    then
        fact_series.EXTEND;
        fact_series(fact_series.LAST).vend_pk := :new.vend_pk;
        fact_series(fact_series.LAST).fact_seri := :new.fact_seri;
        fact_series(fact_series.LAST).fact_codi := :new.fact_codi;
    elsif (:old.fact_seri is not null) 
    then
        RAISE_APPLICATION_ERROR(-20009, 'Facture serie can not changue');
    end if; 

end after each row;

after statement is
begin

    --check ticket serie
    for r in 1..vend_series.COUNT
    loop
        --ensure serie is unique
        select  case when exists (select 1 
		                          from tnht_vend 
								  where vend_seri = vend_series(r).vend_seri and 
								        vend_codi = vend_series(r).vend_codi and 
										vend_pk   <> vend_series(r).vend_pk)
					 then 1 else 0 end
                into serie_unique
        from    dual;       
        
        if (serie_unique = 1) 
        then
            RAISE_APPLICATION_ERROR(-20002, 'Serie is not unique');
        end if;
        
        --ensure previous value exists (unless is first one)
        --there is at least one line using this serie already
        select  case when exists (select 1 
		                          from tnht_vend 
								  where vend_seri = vend_series(r).vend_seri and 
								        vend_pk <> vend_series(r).vend_pk) then 1 else 0 end
                into serie_used
        from    dual;
        if (serie_used = 1)
        then
            --needs to have the previous number already in place
            select  case when exists (select 1 
								      from tnht_vend 
									  where vend_seri = vend_series(r).vend_seri and 
									        vend_codi = vend_series(r).vend_codi - 1 and 
											vend_pk <> vend_series(r).vend_pk) then 1 else 0 end
                    into serie_previo
            from    dual;
            
            if (serie_previo = 0) then RAISE_APPLICATION_ERROR(-20003, 'Serie consecutive not valid');
            end if;
        end if; 
    end loop;
    vend_series.DELETE;
    
    --check ticket facture serie
    for r in 1..fact_series.COUNT
    loop
        
        select  case when exists (select 1 
		                          from tnht_vend 
								  where fact_seri = fact_series(r).fact_seri and 
								        fact_codi = fact_series(r).fact_codi and 
										vend_pk   <> fact_series(r).vend_pk)
					 then 1 else 0 end
                into serie_unique
        from    dual;       
        
        if (serie_unique = 1) 
        then
            RAISE_APPLICATION_ERROR(-20004, 'Facture serie is not unique');
        end if;
         
        select  case when exists (select 1 
		                          from tnht_vend 
								  where fact_seri = fact_series(r).fact_seri and 
								        vend_pk <> fact_series(r).vend_pk) then 1 else 0 end
                into serie_used
        from    dual;
        if (serie_used = 1)
        then
           
            select  case when exists (select 1 
								      from tnht_vend 
									  where fact_seri = fact_series(r).fact_seri and 
									        fact_codi = fact_series(r).fact_codi - 1 and 
											vend_pk <> fact_series(r).vend_pk) then 1 else 0 end
                    into serie_previo
            from    dual;
            
            if (serie_previo = 0) then RAISE_APPLICATION_ERROR(-20005, 'Facture serie consecutive not valid');
            end if;
        end if; 
    end loop;
    fact_series.DELETE;
    
end after statement; 
end;
/
 


CREATE OR REPLACE TRIGGER TNHT_VEND_UPDATE_CODI
BEFORE UPDATE
OF VEND_CODI
  ,FACT_CODI
ON TNHT_VEND
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN 
    -- Ticket code can not changued after has serie
    IF (UPDATING('VEND_CODI') AND :OLD.VEND_SERI IS NOT NULL)
    THEN
       RAISE_APPLICATION_ERROR(-20006, 'The code of ticket can not be modified after the series is assigned');
    END IF;
    
    -- Invoice code can not changued after has serie
    IF (UPDATING('FACT_CODI') AND :OLD.FACT_SERI IS NOT NULL)
    THEN
       RAISE_APPLICATION_ERROR(-20007, 'The code of invoice can not be modified after the series is assigned');
    END IF;

END;
/


BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20190228'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20190228');
END;
/

COMMIT;

