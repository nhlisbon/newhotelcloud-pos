﻿ALTER TABLE TNHT_ARVE ADD CONSTRAINT NHT_CK_ARVE_ANUL CHECK (ARVE_ANUL IN (0, 1, 2, 3, 4, 5)); 
COMMENT ON COLUMN TNHT_ARVE.ARVE_ANUL IS 'Indica el estado del producto: 0. Activo, 1. Anulado, 2. Activo por transferencia desde otro ticket, 3. Anulado después de impreso, 4. Anulado por transferencia hacia otro ticket, 5. Anulado por division del ticket';

ALTER TABLE TNHT_LKDE ADD CONSTRAINT NHT_CK_LKDE_ANUL CHECK (ARVE_ANUL IN (0, 1, 2, 3, 4, 5)); 
COMMENT ON COLUMN TNHT_LKDE.ARVE_ANUL IS 'Indica el estado del producto: 0. Activo, 1. Anulado, 2. Activo por transferencia desde otro ticket, 3. Anulado después de impreso, 4. Anulado por transferencia hacia otro ticket, 5. Anulado por division del ticket';

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20220316'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20220316');
END;
/

COMMIT;