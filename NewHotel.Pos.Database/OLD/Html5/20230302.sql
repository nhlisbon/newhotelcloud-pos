﻿ALTER TABLE TNHT_PINF ADD FDES_PK NUMBER(10);
COMMENT ON COLUMN TNHT_PINF.FDES_PK IS 'Tipo de fiscalización';

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20230302'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20230302');
END;
/

COMMIT;