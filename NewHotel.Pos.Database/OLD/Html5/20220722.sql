﻿CREATE OR REPLACE FORCE VIEW VNHT_NFCE_ARTIGOS
(
   VENDA_CHAVE,
   VENDA_CODIGO,
   VENDA_SERIE,
   ARTIGO_CODIGO,
   ARTIGO_DESCRIPCAO,
   ARTIGO_NCM,
   ARTIGO_CFOP,
   ARTIGO_UNIDADE,
   ARTIGO_CANTIDADE,
   ARTIGO_VALOR_UNITARIO,
   ARTIGO_ANULADO,
   ARTIGO_ANULADO_PAGADO,
   ARTIGO_VALOR_TOTAL,
   ARTIGO_VALOR_DESCONTO,
   ARTIGO_CEST,
   ICMS_CST,
   ICMS_BASE,
   ICMS_BASE_REDUZIDA,
   ICMS_ALIQUOTA,
   ICMS_VALOR,
   ICMS_VALOR_REDUZIDO,
   PIS_CST,
   PIS_BASE,
   PIS_ALIQUOTA,
   PIS_VALOR,
   COFINS_CST,
   COFINS_BASE,
   COFINS_ALIQUOTA,
   COFINS_VALOR,
   ICMS_PORC_REDUZIDA,
   ARTIGO_CBENEF
)
AS
    SELECT vend.vend_pk,
           vend.vend_codi,
           vend.vend_seri,
           artg.artg_codi,
           (SELECT mult_desc
              FROM vnht_mult
             WHERE lite_pk = artg.lite_desc AND lang_pk = 1046)
              AS artg_desc,
           artg.artg_cncm,
           artg.artg_cfop,
           artg.artg_unme,
           ROUND (ARVE.arve_qtds, 2) ARVE_QTDS,
           ROUND (
              CASE
                 WHEN arve_qtds = 0 THEN 0
                 ELSE arve.arve_vsde / arve.arve_qtds
              END,
              4)
              AS ARVE_QTDS,
           CASE WHEN arve_anul IN (0, 2) THEN 0 ELSE 1 END arve_anul,
           arve.anul_paid,
           ROUND (arve.arve_totl, 2) arve_totl,
           arve_desc/arve_qtds,
           NULL,
           artg.artg_csti,
           ROUND (arve.arve_totl, 2) arve_totl,
           ROUND (
              CASE
                 WHEN artg.artg_csti LIKE '%20' THEN arve.arve_totl * 0.4118
                 ELSE arve.arve_totl
              END,
              2)
              AS base_reduc,
           arve.arve_por1,
           ROUND (arve.arve_ivas, 2) arve_ivas,
           ROUND (
              CASE
                 WHEN artg.artg_csti LIKE '%20' THEN arve.arve_ivas * 0.4118
                 ELSE arve.arve_ivas
              END,
              2)
              AS valor_reduc,
           artg.artg_cstp,
           ROUND (arve.arve_totl, 2) arve_totl,
           arve.arve_por2,
           ROUND (arve.arve_iva2, 2) arve_iva2,
           artg.artg_cstc,
           ROUND (arve.arve_totl, 2) arve_totl,
           arve.arve_por3,
           ROUND (arve_iva3, 2) arve_iva3,
           '58,82' AS ICMS_PORC_REDUZIDA,
           '              '
      FROM tnht_artg artg,
           tnht_arve arve,
           tnht_vend vend,
           tnht_hote hote,
           tnht_clie clie
     WHERE     artg.artg_pk = arve.artg_pk
           AND arve.vend_pk = vend.vend_pk
           AND vend.hote_pk = hote.hote_pk
           AND vend.clie_pk = clie.clie_pk(+)
   ORDER BY arve_item ASC;

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20220722'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20220722');
END;
/

COMMIT;