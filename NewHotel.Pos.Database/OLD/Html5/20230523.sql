﻿ALTER TABLE TNHT_TABL ADD SEPA_ORDE NUMBER DEFAULT 0;
COMMENT ON COLUMN TNHT_TABL.SEPA_ORDE IS 'Separator order, ref. TNHT_ARTB(TBSP_ORDER)';
ALTER TABLE TNHT_TABL ADD SEPA_DESC RAW(16) DEFAULT NULL;
COMMENT ON COLUMN TNHT_TABL.SEPA_DESC IS 'Separator description PK, ref. TNHT_ARTB(TBSP_DESC)';
ALTER TABLE TNHT_TABL DROP CONSTRAINT NHT_FK_SEPA_TABL;

BEGIN
    MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20230523'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20230523');
END;
/

COMMIT;