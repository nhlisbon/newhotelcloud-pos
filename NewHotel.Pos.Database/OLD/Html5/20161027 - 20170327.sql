﻿CREATE OR REPLACE VIEW SYNC_TNHT_TACR AS
SELECT TACR.ROWID AS ROW_ID,
       TACR.TACR_PK,
       TACR.TACR_NSU,
       TACR.TACR_AUTH,
       TACR.CACR_PK,
       TACR.CACR_VMES,
       TACR.CACR_VANO,
       TACR.CACR_CODE,
       PAVE.VEND_PK
FROM TNHT_TACR TACR
INNER JOIN TNHT_PAVE PAVE ON PAVE.TACR_PK = TACR.TACR_PK;

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20161027'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20161027');
END;
/

COMMIT;

ALTER TABLE TNHT_ARVE ADD (ARVE_ITEM NUMBER(4));
COMMENT ON COLUMN TNHT_ARVE.ARVE_ITEM IS 'Numero secuencial de la linea del ticket';

DECLARE
ITEMNUMBER NUMBER(4);
BEGIN
    FOR TICKET IN (SELECT VEND_PK FROM TNHT_VEND)
    LOOP
        ITEMNUMBER := 0;
        FOR LINE IN 
        (
            SELECT      ARVE.ARVE_PK, SEPA.SEPA_PK, (SELECT MULT_DESC FROM VNHT_MULT WHERE LITE_PK = ARTG.LITE_DESC AND LANG_PK = 1033) ARTG_DESC
            FROM        TNHT_ARVE ARVE, TNHT_SEPA SEPA, TNHT_ARTG ARTG
            WHERE       ARVE.SEPA_PK = SEPA.SEPA_PK (+)
            AND         ARVE.VEND_PK = TICKET.VEND_PK
            AND         ARVE.ARTG_PK = ARTG.ARTG_PK
            ORDER BY    CASE WHEN SEPA.SEPA_ORDE IS NULL THEN 0 ELSE SEPA.SEPA_ORDE END ASC, ARVE_DACR ASC 
        )
        LOOP
            UPDATE TNHT_ARVE SET ARVE_ITEM = ITEMNUMBER WHERE ARVE_PK = LINE.ARVE_PK;
            ITEMNUMBER := ITEMNUMBER + 1;
        END LOOP;
    END LOOP;
END;
/

ALTER TABLE TNHT_ARVE MODIFY (ARVE_ITEM NOT NULL);

create or replace view vnht_nfce_artigos
(
    venda_chave,
    venda_codigo,
    venda_serie,
    artigo_codigo,
    artigo_descripcao,
    artigo_ncm,
    artigo_cfop,
    artigo_unidade,
    artigo_cantidade,
    artigo_valor_unitario,
    artigo_anulado,
    artigo_valor_total,
    artigo_valor_desconto,
    artigo_cest,
    icms_cst,
    icms_base,
    icms_aliquota,
    icms_valor,
    pis_cst,
    pis_base,
    pis_aliquota,
    pis_valor,
    cofins_cst,
    cofins_base,
    cofins_aliquota,
    cofins_valor
)
as
(
    select      vend.vend_pk, vend.vend_codi, vend.vend_seri, artg.artg_codi, (select mult_desc from vnht_mult where lite_pk = artg.lite_desc and lang_pk = 1046) as artg_desc,
                artg.artg_cncm, artg.artg_cfop, artg.artg_unme, arve.arve_qtds, case when arve_qtds = 0 then 0 else arve.arve_vsde / arve.arve_qtds end arve_qtds, case when arve_anul in (0, 2) then 0 else 1 end arve_anul,
                arve.arve_totl, arve_desc, null, artg.artg_csti, arve.arve_totl, arve.arve_por1, arve.arve_ivas, artg.artg_cstp, arve.arve_totl, arve.arve_por2, arve.arve_iva2, artg.artg_cstc, arve.arve_totl, arve.arve_por3, arve_iva3
    from        tnht_artg artg, tnht_arve arve, tnht_vend vend
    where       artg.artg_pk = arve.artg_pk  
    and         arve.vend_pk = vend.vend_pk
    
)   order by    arve_item asc
; 

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20161121'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20161121');
END;
/

COMMIT;

ALTER TABLE TNHT_CLIE ADD (CLIE_TYPE  NUMBER(10));
ALTER TABLE TNHT_CLIE ADD (HOME_DOOR  VARCHAR2(255));
UPDATE TNHT_CLIE SET CLIE_TYPE = 11;
ALTER TABLE TNHT_CLIE MODIFY(CLIE_TYPE NOT NULL);
COMMENT ON COLUMN TNHT_CLIE.CLIE_TYPE IS 'Tipo de Cliente (Cliente Persona o Entitdad, ref tnht_enum)';
COMMENT ON COLUMN TNHT_CLIE.HOME_DOOR IS 'Numero de puerta';

ALTER TABLE TNHT_CLIE ADD (ENTI_FINO CHAR(1) DEFAULT '0' NOT NULL);
COMMENT ON COLUMN TNHT_CLIE.ENTI_FINO IS 'Preferencia Nota Fiscal en el POS';

ALTER TABLE TNHT_VEND ADD (CLIE_PK  RAW(16));
ALTER TABLE TNHT_VEND ADD CONSTRAINT NHT_FK_CLIE_VEND FOREIGN KEY (CLIE_PK) REFERENCES TNHT_CLIE (CLIE_PK);
COMMENT ON COLUMN TNHT_VEND.CLIE_PK IS 'Referencia al cliente / entidad q fue emitido el ticket / factura';

create or replace view vnht_nfce_vendas
(
    venda_chave,
    venda_codigo,
    venda_serie,
    venda_data,
    venda_hora,
    hotel_cnpj,
    hotel_ie,
    hotel_razao,
    hotel_nome,
    hotel_fone,
    hotel_logradouro,
    hotel_numero,
    hotel_complemento,
    hotel_barrio,
    hotel_cep,
    hotel_ibge,
    hotel_cidade,
    hotel_uf,
    cliente_cpf,
    cliente_nome,
    cliente_fone,
    cliente_logradouro,
    cliente_numero,
    cliente_complemento,
    cliente_barrio,
    cliente_cep,
    cliente_ibge,
    cliente_cidade,
    cliente_uf,
    cliente_ie,
    cliente_email
)
as
(
    select      vend.vend_pk, vend.vend_codi, vend.vend_seri, vend.vend_datf, vend.vend_horf, hote.fisc_numb, hote.regi_fisc,
                hote.hote_desc, hote.hote_desc, hote.home_phone, hote.home_addr1, hote.home_door, hote.home_addr2,
                hote.home_loca, hote.home_copo, dist.dist_caux, dist.dist_desc, comu.comu_caux, vend.fact_nuco, 
                vend.fact_guest, clie.home_phone, vend.fact_addr, clie.home_door, clie.fisc_addr2, clie.fisc_loca, clie.fisc_copo, naci.naci_caux, cdist.dist_caux, ccomu.comu_caux, clie.regi_fisc, clie.email_addr
    from        tnht_vend vend, tnht_hote hote, tnht_dist dist, tnht_comu comu, tnht_clie clie, tnht_dist cdist, tnht_comu ccomu, tnht_naci naci
    where       vend.hote_pk = hote.hote_pk
    and         hote.home_dist = dist.dist_pk (+)
    and         dist.comu_pk = comu.comu_pk (+)
    and         vend.clie_pk = clie.clie_pk (+)
    and         clie.fisc_dist = cdist.dist_pk (+)
    and         cdist.comu_pk = ccomu.comu_pk (+)
    and         clie.naci_pk = naci.naci_pk (+)
)
;


CREATE OR REPLACE VIEW SYNC_TNHT_SEDO AS
SELECT ROWID AS ROW_ID,
       SEDO_PK,
       SEDO_NUFA,
       SEDO_SEAC
FROM TNHT_SEDO;

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20161202'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20161202');
END;
/

COMMIT;

CREATE OR REPLACE VIEW SYNC_TNHT_LKDE AS
SELECT ROWID AS ROW_ID,
       LKDE_PK,
       LKTB_PK,
       ARVE_QTDS,
       ARVE_TOTL,
       ARVE_VLIQ,
       ARTG_PK,
       ARVE_ANUL,
       IVAS_CODI,
       ARVE_POR1,
       ARVE_IVAS,
       IVAS_COD2,
       ARVE_POR2,
       ARVE_IVA2,
       IVAS_COD3,
       ARVE_POR3,
       ARVE_IVA3          
FROM TNHT_LKDE;

CREATE OR REPLACE VIEW SYNC_TNHT_TACR AS
SELECT TACR.ROWID AS ROW_ID,
       TACR.TACR_PK,
       TACR.TACR_NSU,
       TACR.TACR_AUTH,
       TACR.CACR_PK,
       TACR.CACR_VMES,
       TACR.CACR_VANO,
       TACR.CACR_CODE,
       PAVE.VEND_PK
FROM TNHT_TACR TACR
INNER JOIN TNHT_PAVE PAVE ON PAVE.TACR_PK = TACR.TACR_PK;

CREATE OR REPLACE VIEW SYNC_TNHT_SEDO AS
SELECT ROWID AS ROW_ID,
       SEDO_PK,
       SEDO_NUFA,
       SEDO_SEAC
FROM TNHT_SEDO;

CREATE OR REPLACE VIEW SYNC_TNHT_VEND AS
SELECT ROWID AS ROW_ID,
       VEND_PK,
       HOTE_PK,
       IPOS_PK,
       CAJA_PK,
       VEND_MESA,
       UNMO_PK,
       CAJA_PAVE,
       VEND_SERI,
       VEND_CODI,
       VEND_DATI,
       VEND_HORI,
       VEND_DATF,
       VEND_HORF,
       VEND_NPAX,
       VEND_COMP,
       UTIL_PK,
       COIN_PK,
       CCCO_PK,
       TURN_CODI,
       VEND_DESC,
       VEND_RECA,
       VEND_OBSE,
       DUTY_PK,
       VEND_IVIN,
       VEND_SIGN,
       FACT_CCCO,
       FACT_SERI,
       FACT_CODI,
       FACT_DAEM,
       FACT_DARE,
       FACT_GUEST AS FACT_TITU,
       FACT_ADDR,
       FACT_NACI,
       FACT_NUCO,
       FACT_TOTA,
       FACT_SIGN,
       VEND_PROC,
       CLIE_PK AS BENTI_PK,
       VEND_LMOD
FROM TNHT_VEND;

COMMIT;

create or replace view vnht_nfce_artigos
(
    venda_chave,
    venda_codigo,
    venda_serie,
    artigo_codigo,
    artigo_descripcao,
    artigo_ncm,
    artigo_cfop,
    artigo_unidade,
    artigo_cantidade,
    artigo_valor_unitario,
    artigo_anulado,
    artigo_valor_total,
    artigo_valor_desconto,
    artigo_cest,
    icms_cst,
    icms_base,
    icms_aliquota,
    icms_valor,
    pis_cst,
    pis_base,
    pis_aliquota,
    pis_valor,
    cofins_cst,
    cofins_base,
    cofins_aliquota,
    cofins_valor
)
as
(
    select       vend.vend_pk, vend.vend_codi, vend.vend_seri, artg.artg_codi, (select mult_desc from vnht_mult where lite_pk = artg.lite_desc and lang_pk = 1046) as artg_desc, artg.artg_cncm,
                 case
                    --no client selected, local sale 
                 when vend.clie_pk is null or length(artg.artg_cfop) <> 4 then artg.artg_cfop 
                 else 
                 (
                     case
                         --foreign
                         when clie.naci_pk <> 'BR' then '7' || substr(artg.artg_cfop, 1, 3)
                         else
                         (
                             case
                                 --inter state
                                 when hote.home_dist <> clie.fisc_dist then '6' || substr(artg.artg_cfop, 1, 3)
                                 --same state
                                 else artg.artg_cfop
                             end
                         )
                     end
                 )
                 end artg_cfop, artg.artg_unme, arve.arve_qtds, case when arve_qtds = 0 then 0 else arve.arve_vsde / arve.arve_qtds end arve_qtds, case when arve_anul in (0, 2) then 0 else 1 end arve_anul,
                 arve.arve_totl, arve_desc, null, artg.artg_csti, arve.arve_totl, arve.arve_por1, arve.arve_ivas, artg.artg_cstp, arve.arve_totl, arve.arve_por2, arve.arve_iva2, artg.artg_cstc, arve.arve_totl, arve.arve_por3, arve_iva3
    from        tnht_artg artg, tnht_arve arve, tnht_vend vend, tnht_hote hote, tnht_clie clie
    where       artg.artg_pk = arve.artg_pk
    and         arve.vend_pk = vend.vend_pk
    and         vend.hote_pk = hote.hote_pk
    and         vend.clie_pk = clie.clie_pk (+)
)   order by    arve_item asc
;

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20161213'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20161213');
END;
/

COMMIT;

DROP TRIGGER SYNC_TNHT_HOTE_UPDATE;

CREATE TABLE TNHT_GRSE (GRSE_PK RAW(16) NOT NULL, HOTE_PK RAW(16) NOT NULL, LITE_PK RAW(16) NOT NULL,
GRSE_INAC CHAR(1) DEFAULT '0' NOT NULL, GRSE_LMOD TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP NOT NULL);
COMMENT ON TABLE TNHT_GRSE IS 'Clasificador Grupo Servicio';
COMMENT ON COLUMN TNHT_GRSE.GRSE_PK IS 'Código Grupo Servicio';
COMMENT ON COLUMN TNHT_GRSE.HOTE_PK IS 'Código Instalación, ref. tnht_hote(hote_pk)';
COMMENT ON COLUMN TNHT_GRSE.LITE_PK IS 'Traducción descripción Grupo Servicio, ref. tnht_lite(lite_pk)';
COMMENT ON COLUMN TNHT_GRSE.GRSE_INAC IS 'flag: indica si esta inactivo el Grupo Servicio';

ALTER TABLE TNHT_GRSE ADD DELETED CHAR(1) DEFAULT '0';
COMMENT ON COLUMN TNHT_GRSE.DELETED IS 'flag: Borrado lógico (1.si, 0.no, null.en proceso)';

ALTER TABLE TNHT_GRSE ADD CONSTRAINT NHT_PK_GRSE PRIMARY KEY (GRSE_PK);
ALTER TABLE TNHT_GRSE ADD CONSTRAINT NHT_FK_HOTE_GRSE FOREIGN KEY (HOTE_PK) REFERENCES TNHT_HOTE (HOTE_PK);

CREATE OR REPLACE TRIGGER GRSE_SYNC_LITE AFTER DELETE ON TNHT_GRSE
FOR EACH ROW
BEGIN
   DELETE FROM TNHT_LITE WHERE LITE_PK = :OLD.LITE_PK;
EXCEPTION
   WHEN OTHERS THEN RAISE;
END;
/

ALTER TABLE TNHT_GRUP ADD (GRSE_PK RAW(16), CATS_PK NUMBER(10));
COMMENT ON COLUMN TNHT_GRUP.GRSE_PK IS 'Grupo de Servicio ref. tnht_grse(grse_pk)';
COMMENT ON COLUMN TNHT_GRUP.CATS_PK IS 'Categoría de Servicio ref. tnht_cats(cats_pk)';

ALTER TABLE TNHT_GRUP ADD CONSTRAINT NHT_FK_GRSE_GRUP FOREIGN KEY (GRSE_PK) REFERENCES TNHT_GRSE (GRSE_PK);

ALTER TABLE TNHT_TIDE ADD TIDE_INAC CHAR(1) DEFAULT '0' NOT NULL;
ALTER TABLE TNHT_MTCO ADD MTCO_INAC CHAR(1) DEFAULT '0' NOT NULL;
COMMENT ON COLUMN TNHT_TIDE.TIDE_INAC IS 'flag: indica si esta inactivo el tipo de descuento';
COMMENT ON COLUMN TNHT_MTCO.MTCO_INAC IS 'flag: indica si esta inactivo el motivo de anulación/corrección';

ALTER TABLE TNHT_VEND ADD ANUL_SYNC TIMESTAMP(6);
COMMENT ON COLUMN TNHT_VEND.ANUL_SYNC IS 'Fecha/Hora del pedido de cancelamiento';

CREATE TABLE TNHT_SYNC (SYNC_PK NUMBER(10) NOT NULL, SYNC_NAME VARCHAR2(255) NOT NULL, SYNC_SIZE NUMBER(10) NOT NULL, SYNC_LMOD TIMESTAMP(6));
COMMENT ON TABLE TNHT_SYNC IS 'Tablas a sincronizar';
COMMENT ON COLUMN TNHT_SYNC.SYNC_PK IS 'Primary key';
COMMENT ON COLUMN TNHT_SYNC.SYNC_NAME IS 'Nombre de la tabla';
COMMENT ON COLUMN TNHT_SYNC.SYNC_SIZE IS 'Cantidad de registros a sincronizar de una vez';
COMMENT ON COLUMN TNHT_SYNC.SYNC_LMOD IS 'Indicador de última modificación sincronizada';

ALTER TABLE TNHT_SYNC ADD CONSTRAINT NHT_PK_SYNC PRIMARY KEY (SYNC_PK);

INSERT INTO TNHT_SYNC (SYNC_PK, SYNC_NAME, SYNC_SIZE) VALUES (1, 'TNHT_CLIE', 100);
COMMIT;

ALTER TABLE TNHT_LITE DROP COLUMN DELETED;
ALTER TABLE TNHT_MULT DROP COLUMN DELETED;
ALTER TABLE TNHT_ENUM DROP COLUMN DELETED;
ALTER TABLE TNHT_CLAS DROP COLUMN DELETED;
ALTER TABLE TNHT_VEND DROP COLUMN DELETED;
ALTER TABLE TNHT_ARVE DROP COLUMN DELETED;
ALTER TABLE TCFG_NPOS DROP COLUMN DELETED;

CREATE OR REPLACE PROCEDURE pnht_get_serie (
  p_pk OUT RAW,
  p_cursor0 OUT SYS_REFCURSOR)
IS
BEGIN
  SELECT sedo_pk INTO p_pk FROM (
    SELECT sedo_pk FROM TNHT_SEDO
    WHERE sedo_sync <= SYSTIMESTAMP AND sedo_sync is not null ORDER BY sedo_sync
  ) WHERE ROWNUM <= 1;

  OPEN p_cursor0 FOR SELECT 'M' AS change_operation,
                     ROWID AS row_id, sedo_pk,
                     hote_pk, sedo_ptex, sedo_pnum, sedo_orde, sedo_nufa,
                     sedo_nufi, sedo_dafi, sedo_seac, sedo_saft, sedo_lmod
                     FROM TNHT_SEDO WHERE sedo_pk = p_pk;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    p_pk := null;
END;
/

CREATE OR REPLACE PROCEDURE pnht_get_table (
  p_pk OUT RAW,
  p_cursor0 OUT SYS_REFCURSOR)
IS
BEGIN
  SELECT mesa_pk INTO p_pk FROM (
    SELECT mesa_pk FROM TNHT_MESA
    WHERE mesa_sync <= SYSTIMESTAMP AND mesa_sync is not null ORDER BY mesa_sync
  ) WHERE ROWNUM <= 1;

  OPEN p_cursor0 FOR SELECT 'U' AS change_operation,
                 ROWID AS row_id, mesa_pk, mesa_colu, mesa_row
                 FROM TNHT_MESA WHERE mesa_pk = p_pk;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    p_pk := null;
END;
/

CREATE OR REPLACE PROCEDURE pnht_get_ticket (
  p_pk OUT RAW,
  p_cursor0  OUT SYS_REFCURSOR,
  p_cursor1  OUT SYS_REFCURSOR,
  p_cursor2  OUT SYS_REFCURSOR,
  p_cursor3  OUT SYS_REFCURSOR,
  p_cursor4  OUT SYS_REFCURSOR,
  p_cursor5  OUT SYS_REFCURSOR,
  p_cursor6  OUT SYS_REFCURSOR,
  p_cursor7  OUT SYS_REFCURSOR,
  p_cursor8  OUT SYS_REFCURSOR,
  p_cursor9  OUT SYS_REFCURSOR,
  p_cursor10 OUT SYS_REFCURSOR,
  p_cursor11 OUT SYS_REFCURSOR,
  p_cursor12 OUT SYS_REFCURSOR,
  p_cursor13 OUT SYS_REFCURSOR,
  p_cursor14 OUT SYS_REFCURSOR,
  p_cursor15 OUT SYS_REFCURSOR,
  p_cursor16 OUT SYS_REFCURSOR,
  p_cursor17 OUT SYS_REFCURSOR,
  p_cursor18 OUT SYS_REFCURSOR,
  p_cursor19 OUT SYS_REFCURSOR,
  p_cursor20 OUT SYS_REFCURSOR,
  p_cursor21 OUT SYS_REFCURSOR,
  p_cursor22 OUT SYS_REFCURSOR,
  p_cursor23 OUT SYS_REFCURSOR,
  p_cursor24 OUT SYS_REFCURSOR,
  p_cursor25 OUT SYS_REFCURSOR)
IS
BEGIN
  SELECT vend_pk INTO p_pk FROM (
    SELECT vend_pk FROM TNHT_VEND
    WHERE vend_sync <= SYSTIMESTAMP AND vend_sync is not null ORDER BY vend_sync
  ) WHERE ROWNUM <= 1;

  -- users
  OPEN p_cursor0 FOR SELECT 'I' AS change_operation,
                     UTIL.ROWID AS ROW_ID, UTIL.UTIL_PK, UTIL.HOTE_PK, UTIL.UTIL_LOGIN,
                     UTIL.UTIL_DESC, UTIL.UTIL_PASS, UTIL.UTIL_CODE, TRUNC(SYSDATE) AS UTIL_DARE,                     
                     UTIL.UTIL_MGR, UTIL.UTIL_INTE, '1' AS UTIL_INAC, UTIL.UTIL_LMOD
                     FROM (
                     SELECT VEND.UTIL_PK
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_UTIL UTIL ON UTIL.UTIL_PK = VEND.UTIL_PK
                     WHERE VEND.VEND_PK = p_pk
                     UNION
                     SELECT VEND.UTIL_OPEN
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_UTIL UTIL ON UTIL.UTIL_PK = VEND.UTIL_OPEN
                     WHERE VEND.VEND_PK = p_pk
                     UNION
                     SELECT VEND.ANUL_UTIL
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_UTIL UTIL ON UTIL.UTIL_PK = VEND.ANUL_UTIL
                     WHERE VEND.ANUL_UTIL IS NOT NULL AND VEND.VEND_PK = p_pk
                     UNION
                     SELECT ARVE.UTIL_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_UTIL UTIL ON UTIL.UTIL_PK = ARVE.UTIL_PK
                     WHERE ARVE.VEND_PK = p_pk
                     ) TEMP INNER JOIN TNHT_UTIL UTIL
                     ON UTIL.UTIL_PK = TEMP.UTIL_PK;

  -- translations
  OPEN p_cursor1 FOR SELECT 'I' AS change_operation,
                     LITE.ROWID AS ROW_ID, LITE.LITE_PK
                     FROM (
                     SELECT GRSE.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_GRUP GRUP ON GRUP.GRUP_PK = FAMI.GRUP_PK
                     INNER JOIN TNHT_GRSE GRSE ON GRSE.GRSE_PK = GRUP.GRSE_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_COIN COIN ON COIN.COIN_PK = VEND.COIN_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = COIN.COIN_PK
                     WHERE VEND.COIN_PK IS NOT NULL AND VEND.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = FAMI.GRUP_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = ARTG.FAMI_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = ARTG.SFAM_PK
                     WHERE ARTG.SFAM_PK IS NOT NULL AND ARVE.VEND_PK = p_pk
                     UNION
                     SELECT ARTG.LITE_ABRE
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT ARTG.LITE_DESC
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT FORE.LITE_ABRE
                     FROM TNHT_FORE FORE 
                     INNER JOIN TNHT_PAVE PAVE ON PAVE.FORE_PK = FORE.FORE_PK
                     WHERE PAVE.VEND_PK = p_pk
                     UNION
                     SELECT FORE.LITE_DESC
                     FROM TNHT_FORE FORE
                     INNER JOIN TNHT_PAVE PAVE ON PAVE.FORE_PK = FORE.FORE_PK
                     WHERE PAVE.VEND_PK = p_pk
                     UNION
                     SELECT TIDE.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_TIDE TIDE ON TIDE.TIDE_PK = ARVE.TIDE_PK
                     WHERE ARVE.TIDE_PK IS NOT NULL AND ARVE.VEND_PK = p_pk
                     UNION
                     SELECT MTCO.LITE_PK
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_MTCO MTCO ON MTCO.MTCO_PK = VEND.MTCO_PK
                     WHERE VEND.MTCO_PK IS NOT NULL AND VEND.VEND_PK = p_pk
                     ) TEMP INNER JOIN TNHT_LITE LITE
                     ON TEMP.LITE_PK = LITE.LITE_PK;

  -- translation descriptions
  OPEN p_cursor2 FOR SELECT 'I' AS change_operation,
                     MULT.ROWID AS ROW_ID, MULT.LITE_PK, MULT.LANG_PK, MULT.MULT_DESC
                     FROM (
                     SELECT GRSE.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_GRUP GRUP ON GRUP.GRUP_PK = FAMI.GRUP_PK
                     INNER JOIN TNHT_GRSE GRSE ON GRSE.GRSE_PK = GRUP.GRSE_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_COIN COIN ON COIN.COIN_PK = VEND.COIN_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = COIN.COIN_PK
                     WHERE VEND.COIN_PK IS NOT NULL AND VEND.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = FAMI.GRUP_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = ARTG.FAMI_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = ARTG.SFAM_PK
                     WHERE ARTG.SFAM_PK IS NOT NULL AND ARVE.VEND_PK = p_pk
                     UNION
                     SELECT ARTG.LITE_ABRE
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT ARTG.LITE_DESC
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT FORE.LITE_ABRE
                     FROM TNHT_FORE FORE 
                     INNER JOIN TNHT_PAVE PAVE ON PAVE.FORE_PK = FORE.FORE_PK
                     WHERE PAVE.VEND_PK = p_pk
                     UNION
                     SELECT FORE.LITE_DESC
                     FROM TNHT_FORE FORE
                     INNER JOIN TNHT_PAVE PAVE ON PAVE.FORE_PK = FORE.FORE_PK
                     WHERE PAVE.VEND_PK = p_pk
                     UNION
                     SELECT TIDE.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_TIDE TIDE ON TIDE.TIDE_PK = ARVE.TIDE_PK
                     WHERE ARVE.TIDE_PK IS NOT NULL AND ARVE.VEND_PK = p_pk
                     UNION
                     SELECT MTCO.LITE_PK
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_MTCO MTCO ON MTCO.MTCO_PK = VEND.MTCO_PK
                     WHERE VEND.MTCO_PK IS NOT NULL AND VEND.VEND_PK = p_pk
                     ) TEMP INNER JOIN TNHT_MULT MULT
                     ON TEMP.LITE_PK = MULT.LITE_PK;

  -- service groups
  OPEN p_cursor3 FOR SELECT 'I' AS change_operation,
                     GRSE.* FROM (
                     SELECT GRSE.ROWID AS ROW_ID, GRSE.GRSE_PK, GRSE.HOTE_PK,
                     GRSE.LITE_PK, '1' AS GRSE_INAC, GRSE.GRSE_LMOD
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_GRUP GRUP ON GRUP.GRUP_PK = FAMI.GRUP_PK
                     INNER JOIN TNHT_GRSE GRSE ON GRSE.GRSE_PK = GRUP.GRSE_PK
                     WHERE ARVE.VEND_PK = p_pk
                     ) GRSE;

  -- grups, families, subfamilies
  OPEN p_cursor4 FOR SELECT 'I' AS change_operation,
                     CLAS.* FROM (
                     SELECT CLAS.ROWID AS ROW_ID, CLAS.CLAS_PK, CLAS.HOTE_PK,
                     CLAS.LITE_PK, CLAS.CLAS_LMOD
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = VEND.COIN_PK
                     WHERE VEND.COIN_PK IS NOT NULL AND VEND.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.ROWID AS ROW_ID, CLAS.CLAS_PK, CLAS.HOTE_PK,
                     CLAS.LITE_PK, CLAS.CLAS_LMOD                    
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = ARTG.FAMI_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.ROWID AS ROW_ID, CLAS.CLAS_PK, CLAS.HOTE_PK,
                     CLAS.LITE_PK, CLAS.CLAS_LMOD
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = FAMI.GRUP_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.ROWID AS ROW_ID, CLAS.CLAS_PK, CLAS.HOTE_PK,
                     CLAS.LITE_PK, CLAS.CLAS_LMOD
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = ARTG.SFAM_PK
                     WHERE ARTG.SFAM_PK IS NOT NULL AND ARVE.VEND_PK = p_pk
                     ) CLAS;
 
  -- internal comsumptions
  OPEN p_cursor5 FOR SELECT 'I' AS change_operation,
                     COIN.ROWID AS ROW_ID, COIN.COIN_PK,
                     COIN.COIN_INVI, '1' AS COIN_INAC
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_COIN COIN ON COIN.COIN_PK = VEND.COIN_PK
                     WHERE VEND.COIN_PK IS NOT NULL AND VEND.VEND_PK = p_pk;

  -- grups
  OPEN p_cursor6 FOR SELECT 'I' AS change_operation,
                     GRUP.ROWID AS ROW_ID, GRUP.GRUP_PK,
                     GRUP.GRUP_CHEQ, GRUP.GRUP_RECA, GRUP.GRSE_PK, GRUP.CATS_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_GRUP GRUP ON GRUP.GRUP_PK = FAMI.GRUP_PK
                     WHERE ARVE.VEND_PK = p_pk;

  -- families
  OPEN p_cursor7 FOR SELECT 'I' AS change_operation,
                     FAMI.ROWID AS ROW_ID, FAMI.FAMI_PK, FAMI.GRUP_PK                    
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     WHERE ARVE.VEND_PK = p_pk;

  -- subfamilies
  OPEN p_cursor8 FOR SELECT 'I' AS change_operation,
                     SFAM.ROWID AS ROW_ID, SFAM.SFAM_PK, SFAM.FAMI_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_SFAM SFAM ON SFAM.SFAM_PK = ARTG.SFAM_PK
                     WHERE ARTG.SFAM_PK IS NOT NULL AND ARVE.VEND_PK = p_pk;

  -- products service
  OPEN p_cursor9 FOR SELECT 'I' AS change_operation,
                     ARTG.ROWID AS ROW_ID, ARTG.ARTG_PK AS SERV_PK, ARTG.HOTE_PK, ARTG.LITE_ABRE, ARTG.LITE_DESC,
                     ARTG.ARTG_ROUN AS SERV_ROUN, ARTG.ARTG_LOCA AS SERV_LOCA, ARTG.ARTG_UNME AS SERV_UNME,
                     ARTG.ARTG_CSTI AS SERV_CSTI, ARTG.ARTG_CNCM AS SERV_CNCM, ARTG.ARTG_CFOP AS SERV_CFOP,
                     ARTG.ARTG_CSTP AS SERV_CSTP, ARTG.ARTG_CSTC AS SERV_CSTC, GRUP.GRSE_PK, GRUP.CATS_PK,
                     '1' AS SERV_INAC, ARTG.ARTG_LMOD AS SERV_LMOD
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_GRUP GRUP ON GRUP.GRUP_PK = FAMI.GRUP_PK
                     WHERE ARVE.VEND_PK = p_pk;

  -- products base
  OPEN p_cursor10 FOR SELECT 'I' AS change_operation,
                      ARTG.ROWID AS ROW_ID, ARTG.ARTG_PK AS BARTG_PK,
                      ARTG.ARTG_CODI, ARTG.FAMI_PK, ARTG.SFAM_PK 
                      FROM TNHT_ARVE ARVE
                      INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                      WHERE ARVE.VEND_PK = p_pk;
 
  -- products
  OPEN p_cursor11 FOR SELECT 'I' AS change_operation,
                      ARTG.ROWID AS ROW_ID, ARTG.ARTG_PK, ARTG.ARTG_STEP, ARTG.ARTG_TIPR, ARTG.ARTG_COLO
                      FROM TNHT_ARVE ARVE
                      INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                      WHERE ARVE.VEND_PK = p_pk;

  -- discount types
  OPEN p_cursor12 FOR SELECT 'I' AS change_operation,
                      TIDE.ROWID AS ROW_ID, TIDE.TIDE_PK, TIDE.LITE_PK, TIDE.HOTE_PK, TIDE.APPL_PK,
                      TIDE.TIDE_FIXE, TIDE.TIDE_VMIN, TIDE.TIDE_VMAX, '1' AS TIDE_INAC, TIDE.TIDE_LMOD                     
                      FROM TNHT_ARVE ARVE
                      INNER JOIN TNHT_TIDE TIDE ON TIDE.TIDE_PK = ARVE.TIDE_PK
                      WHERE ARVE.TIDE_PK IS NOT NULL AND ARVE.VEND_PK = p_pk;

  -- cancellation reason
  OPEN p_cursor13 FOR SELECT 'I' AS change_operation,
                      MTCO.ROWID AS ROW_ID, MTCO.MTCO_PK, MTCO.HOTE_PK, MTCO.LITE_PK,
                      MTCO.MTCO_TICA, '1' AS MTCO_INAC, MTCO.MTCO_LMOD                     
                      FROM TNHT_VEND VEND
                      INNER JOIN TNHT_MTCO MTCO ON MTCO.MTCO_PK = VEND.MTCO_PK
                      WHERE VEND.MTCO_PK IS NOT NULL AND VEND.VEND_PK = p_pk;

  -- payment forms
  OPEN p_cursor14 FOR SELECT 'I' AS CHANGE_OPERATION,
                      FORE.ROWID AS ROW_ID, FORE.FORE_PK, FORE.HOTE_PK,
                      FORE.LITE_ABRE, FORE.LITE_DESC, FORE.UNMO_PK, FORE.FORE_CASH,
                      FORE.FORE_CACR, FORE.REPO_CASH, FORE.FORE_ORDE, FORE.FORE_CAUX,
                      FORE.FORE_CRED, FORE.FORE_DEBI, '1' AS FORE_INAC, FORE.FORE_LMOD
                      FROM TNHT_FORE FORE
                      INNER JOIN TNHT_PAVE PAVE ON PAVE.FORE_PK = FORE.FORE_PK
                      WHERE PAVE.VEND_PK = p_pk;

  -- ticket
  OPEN p_cursor15 FOR SELECT 'M' as change_operation,
                      ROWID AS ROW_ID, VEND_PK, HOTE_PK, IPOS_PK, CAJA_PK, VEND_MESA, UNMO_PK,
                      CAJA_PAVE, VEND_SERI, VEND_CODI, VEND_DATI, VEND_HORI, VEND_DATF, VEND_HORF,
                      VEND_NPAX, VEND_COMP, UTIL_PK, COIN_PK, CCCO_PK, TURN_CODI, VEND_DESC,
                      VEND_RECA, VEND_OBSE, DUTY_PK, VEND_IVIN, VEND_SIGN, FACT_CCCO, FACT_SERI,
                      FACT_CODI, FACT_DAEM, FACT_DARE, FACT_GUEST AS FACT_TITU, FACT_ADDR, FACT_NACI,
                      FACT_NUCO, FACT_TOTA, FACT_SIGN, VEND_PROC, CLIE_PK AS BENTI_PK, VEND_LMOD
                      FROM TNHT_VEND WHERE VEND_PK = p_pk;

  -- ticket lines
  OPEN p_cursor16 FOR SELECT 'M' as change_operation,
                      ROWID AS ROW_ID, ARVE_PK, VEND_PK, IPOS_PK, CAJA_PK, ARTG_PK, SEPA_PK,
                      ARVE_ITEM, ARVE_QTDS, ARVE_TOTL, ARVE_VLIQ, ARVE_COST, ARVE_DESC,
                      ARVE_PDES, ARVE_VSDE, ARVE_RECA, UTIL_PK, IVAS_CODI, ARVE_POR1, ARVE_IVAS,
                      IVAS_COD2, ARVE_POR2, ARVE_IVA2, IVAS_COD3, ARVE_POR3, ARVE_IVA3,
                      ARVE_ANUL, ARVE_HPPY, ARVE_IMAR, ARVE_ADIC, ARVE_ACOM, ARVE_OBSV, ARVE_OBSM,
                      CASE WHEN ARVE_IMPR IS NULL THEN 0 ELSE ARVE_IMPR END AS ARVE_IMPR, ARVE_LMOD
                      FROM TNHT_ARVE WHERE VEND_PK = p_pk;

  -- ticket credit cards document
  OPEN p_cursor17 FOR SELECT 'M' as change_operation,
                      TACR.ROWID AS ROW_ID, TACR.TACR_PK AS DOCU_PK, 1 AS TIDO_PK
                      FROM TNHT_TACR TACR
                      INNER JOIN TNHT_PAVE PAVE ON PAVE.TACR_PK = TACR.TACR_PK
                      WHERE PAVE.VEND_PK = p_pk;

  -- ticket credit cards personal document
  OPEN p_cursor18 FOR SELECT null as change_id, 'M' as change_operation, null as change_delete_pk,
                      TACR.ROWID AS ROW_ID, TACR.TACR_PK AS DOCU_PK, TACR.TACR_IDEN AS DOCP_IDEN
                      FROM TNHT_TACR TACR
                      INNER JOIN TNHT_PAVE PAVE ON PAVE.TACR_PK = TACR.TACR_PK
                      WHERE PAVE.VEND_PK = p_pk;

  -- ticket credit cards
  OPEN p_cursor19 FOR SELECT 'M' as change_operation,
                      TACR.ROWID AS ROW_ID, TACR.TACR_PK, TACR.CACR_PK, TACR.CACR_CODE,
                      TACR.CACR_VMES, TACR.CACR_VANO, TACR.TACR_NSU, TACR.TACR_AUTH
                      FROM TNHT_TACR TACR
                      INNER JOIN TNHT_PAVE PAVE ON PAVE.TACR_PK = TACR.TACR_PK
                      WHERE PAVE.VEND_PK = p_pk;

  -- ticket payments
  OPEN p_cursor20 FOR SELECT 'M' as change_operation,
                      ROWID AS ROW_ID, PAVE_PK, VEND_PK, TIRE_PK, FORE_PK, PAVE_VALO, CCCO_PK,
                      TACR_PK, COIN_PK, PAVE_OBSV, PAVE_AUTH
                      FROM TNHT_PAVE WHERE VEND_PK = p_pk;

  -- ticket lookup tables document
  OPEN p_cursor21 FOR SELECT 'M' as change_operation,
                      ROWID AS ROW_ID, LKTB_PK AS DOCU_PK, 48 AS TIDO_PK
                      FROM TNHT_LKTB WHERE VEND_PK = p_pk;

  -- ticket lookup tables
  OPEN p_cursor22 FOR SELECT 'M' as change_operation,
                      LKTB.ROWID AS ROW_ID, LKTB.LKTB_PK, LKTB.LKTB_DATE, LKTB.LKTB_STIM,
                      LKTB.LKTB_SIGN, LKTB.UTIL_PK, LKTB.LKTB_TOTA, LKTB.LKTB_SERI,
                      LKTB.LKTB_CODI, LKTB.VEND_PK, VEND.IPOS_PK, VEND.CAJA_PK
                      FROM TNHT_LKTB LKTB
                      INNER JOIN TNHT_VEND VEND ON VEND.VEND_PK = LKTB.VEND_PK
                      WHERE VEND.VEND_PK = p_pk;

  -- ticket lookup tables details
  OPEN p_cursor23 FOR SELECT 'M' as change_operation,
                      LKDE.ROWID AS ROW_ID, LKDE.LKDE_PK, LKDE.LKTB_PK, LKDE.ARVE_QTDS, LKDE.ARVE_TOTL,
                      LKDE.ARVE_VLIQ, LKDE.ARTG_PK, LKDE.ARVE_ANUL, LKDE.IVAS_CODI, LKDE.ARVE_POR1,
                      LKDE.ARVE_IVAS, LKDE.IVAS_COD2, LKDE.ARVE_POR2, LKDE.ARVE_IVA2, LKDE.IVAS_COD3,
                      LKDE.ARVE_POR3, LKDE.ARVE_IVA3
                      FROM TNHT_LKDE LKDE
                      INNER JOIN TNHT_LKTB LKTB ON LKTB.LKTB_PK = LKDE.LKTB_PK
                      WHERE LKTB.VEND_PK = p_pk;

  -- ticket invoice
  OPEN p_cursor24 FOR SELECT 'P' as change_operation,
                      ROWID AS row_id, vend_pk AS p_vend_pk, 1033 AS p_lang_pk
                      FROM TNHT_VEND
                      WHERE fact_seri is not null AND vend_pk = p_pk;

  -- ticket table
  OPEN p_cursor25 FOR SELECT 'P' as change_operation,
                      mesa.ROWID AS row_id, vend.vend_pk AS p_vend_pk,
                      vend.vend_mesa AS p_mesa_pk, mesa.mesa_desc AS p_mesa_desc
                      FROM TNHT_VEND vend INNER JOIN TNHT_MESA mesa
                      ON mesa.mesa_pk = vend.vend_mesa
                      WHERE vend.vend_mesa is not null AND vend.vend_pk = p_pk;

EXCEPTION
  WHEN NO_DATA_FOUND THEN
    p_pk := null;
END;
/

CREATE OR REPLACE PROCEDURE pnht_get_ticket_canceled (
  p_pk OUT RAW,
  p_cursor0 OUT SYS_REFCURSOR)
IS
BEGIN
  SELECT vend_pk INTO p_pk FROM (
    SELECT vend_pk FROM TNHT_VEND
    WHERE anul_sync <= SYSTIMESTAMP AND anul_sync IS NOT NULL ORDER BY anul_sync
  ) WHERE ROWNUM <= 1;

  OPEN p_cursor0 FOR SELECT 'P' as change_operation,
                     ROWID AS row_id, vend_pk AS p_vend_pk, mtco_pk AS p_mtco_pk,
                     anul_daan AS p_anul_daan, anul_dare AS p_anul_dare,
                     anul_util AS p_util_pk, anul_obse AS p_anul_obse
                     FROM TNHT_VEND
                     WHERE mtco_pk is not null AND vend_pk = p_pk;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    p_pk := null;
END;
/

CREATE OR REPLACE PROCEDURE pnht_sync_update (
  p_index IN INTEGER,
  p_pk IN RAW,
  p_interval IN INTERVAL DAY TO SECOND := null)
IS
  v_timestamp TIMESTAMP(6) := null;
BEGIN
  IF p_interval is not null THEN
    v_timestamp := SYSTIMESTAMP + p_interval;
  END IF;
 
  CASE
    WHEN p_index = 0 THEN
      UPDATE TNHT_SEDO SET sedo_sync = v_timestamp WHERE sedo_pk = p_pk;
    WHEN p_index = 1 THEN
      UPDATE TNHT_VEND SET vend_sync = v_timestamp WHERE vend_pk = p_pk;
    WHEN p_index = 2 THEN
      UPDATE TNHT_VEND SET anul_sync = v_timestamp WHERE vend_pk = p_pk;
    WHEN p_index = 3 THEN
      UPDATE TNHT_MESA SET mesa_sync = v_timestamp WHERE mesa_pk = p_pk;
  END CASE;
END;
/

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20161227'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20161227');
END;
/

COMMIT;

ALTER TABLE TNHT_ARTG ADD ARTG_CEST VARCHAR2(255);
COMMENT ON COLUMN TNHT_ARTG.ARTG_CEST IS 'Codigo Especificador de Sustitucion Tributaria';

create or replace view vnht_nfce_artigos
(
    venda_chave,
    venda_codigo,
    venda_serie,
    artigo_codigo,
    artigo_descripcao,
    artigo_ncm,
    artigo_cfop,
    artigo_unidade,
    artigo_cantidade,
    artigo_valor_unitario,
    artigo_anulado,
    artigo_valor_total,
    artigo_valor_desconto,
    artigo_cest,
    icms_cst,
    icms_base,
    icms_aliquota,
    icms_valor,
    pis_cst,
    pis_base,
    pis_aliquota,
    pis_valor,
    cofins_cst,
    cofins_base,
    cofins_aliquota,
    cofins_valor
)
as
(
    select       vend.vend_pk, vend.vend_codi, vend.vend_seri, artg.artg_codi, (select mult_desc from vnht_mult where lite_pk = artg.lite_desc and lang_pk = 1046) as artg_desc, artg.artg_cncm,
                 case
                    --no client selected, local sale 
                 when vend.clie_pk is null or length(artg.artg_cfop) <> 4 then artg.artg_cfop 
                 else 
                 (
                     case
                         --foreign
                         when clie.naci_pk <> 'BR' then '7' || substr(artg.artg_cfop, 1, 3)
                         else
                         (
                             case
                                 --inter state
                                 when hote.home_dist <> clie.fisc_dist then '6' || substr(artg.artg_cfop, 1, 3)
                                 --same state
                                 else artg.artg_cfop
                             end
                         )
                     end
                 )
                 end artg_cfop, artg.artg_unme, arve.arve_qtds, case when arve_qtds = 0 then 0 else arve.arve_vsde / arve.arve_qtds end arve_qtds, case when arve_anul in (0, 2) then 0 else 1 end arve_anul,
                 arve.arve_totl, arve_desc, artg.artg_cest, artg.artg_csti, arve.arve_totl, arve.arve_por1, arve.arve_ivas, artg.artg_cstp, arve.arve_totl, arve.arve_por2, arve.arve_iva2, artg.artg_cstc, arve.arve_totl, arve.arve_por3, arve_iva3
    from        tnht_artg artg, tnht_arve arve, tnht_vend vend, tnht_hote hote, tnht_clie clie
    where       artg.artg_pk = arve.artg_pk
    and         arve.vend_pk = vend.vend_pk
    and         vend.hote_pk = hote.hote_pk
    and         vend.clie_pk = clie.clie_pk (+)
)   order by    arve_item asc
;


begin
  merge into tcfg_gene para using dual on (para.gene_pk = 1)
    when matched then update set para.date_vers = '20170126'
    when not matched then insert (para.gene_pk, para.date_vers) values (1, '20170126');
end;
/

commit;

CREATE OR REPLACE TRIGGER SYNC_TNHT_SEDO_INSERT
BEFORE INSERT ON TNHT_SEDO FOR EACH ROW
BEGIN
  :NEW.SEDO_SYNC := SYSTIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER SYNC_TNHT_SEDO_UPDATE
BEFORE UPDATE OF SEDO_NUFA, SEDO_SEAC ON TNHT_SEDO FOR EACH ROW
WHEN (OLD.SEDO_NUFA != NEW.SEDO_NUFA OR OLD.SEDO_SEAC != NEW.SEDO_SEAC)
BEGIN
  IF :NEW.SEDO_NUFA < :OLD.SEDO_NUFA THEN
    :NEW.SEDO_NUFA := :OLD.SEDO_NUFA;
  END IF;
  :NEW.SEDO_SYNC := SYSTIMESTAMP;
END;
/

CREATE OR REPLACE VIEW SYNC_TNHT_VEND AS
SELECT ROWID AS ROW_ID,
       VEND_PK,
       HOTE_PK,
       IPOS_PK,
       CAJA_PK,
       VEND_MESA,
       UNMO_PK,
       CAJA_PAVE,
       VEND_SERI,
       VEND_CODI,
       VEND_DATI,
       VEND_HORI,
       VEND_DATF,
       VEND_HORF,
       VEND_NPAX,
       VEND_COMP,
       UTIL_PK,
       COIN_PK,
       CCCO_PK,
       TURN_CODI,
       VEND_DESC,
       VEND_RECA,
       VEND_OBSE,
       DUTY_PK,
       VEND_IVIN,
       VEND_SIGN,
       FACT_CCCO,
       FACT_SERI,
       FACT_CODI,
       FACT_DAEM,
       FACT_DARE,
       FACT_GUEST AS FACT_TITU,
       FACT_ADDR,
       FACT_NACI,
       FACT_NUCO,
       FACT_TOTA,
       FACT_SIGN,
       CLIE_PK AS BENTI_PK,
       SYSDATE AS VEND_DHRE,
       VEND_LMOD
FROM TNHT_VEND;

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20170222'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20170222');
END;
/

COMMIT;

insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('FE9B0C148F06554884AE4FC21C8E3432', 1046, 'Todos');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2764414B27500540A160B1F44D2253A9', 1046, 'Não requer');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8F83B1A8B6946744AA1D6641DFC1F970', 1046, 'Ambas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9E20714A8FAB3F4CA669189AD0B99505', 1046, 'Nenhum');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('563CEF8234282A428FDC9AAEDFFA5F1C', 1046, 'Nunca');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D7CDB5139840A643AEF2545FFEEDC5E2', 1046, 'Sempre');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('80B4CD1A4A1B4940A78E3DCB1D7B694D', 1046, 'Entrada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('25275BF2E4E9084595D9E7530E9DBC64', 1046, 'Saida');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('1EE7EB06AD245E4CA8015D7032385676', 1046, 'Cliente');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D0C9B8B1E3AB894FA3B4D5AB164AC7F6', 1046, 'Fornecedor');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('DD6019D9C25444469BF202313E9AE882', 1046, 'Outro');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('03AC0F563195F649B76DAB1BE1891C8B', 1046, 'Sim');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A827209654C67849B0C0637DCFEB71AB', 1046, 'Não');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('99D08CD54FD1E24CB3A089EC0868EDBE', 1046, 'Adulto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('023FBA817982A446BB1A91C9D6B56E65', 1046, '1er Adulto Extra');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C30AFA616F10B64CBB629DE12BEFA628', 1046, 'Outro Adulto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5EF265767EF46A479FABB143635E5D78', 1046, 'Adulto Extra');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('72927C9351DA2E4DB2798D48D4980E01', 1046, 'Suplemento Individual');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('60697524749B8A48A49C57F240C71884', 1046, 'Quarto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F2CAB883D535FD4EAB2DA01252D121B0', 1046, 'Pensão');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0DFA7D3D79574C4FBF4147DA26BB9B7A', 1046, 'Adultos');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8C5831162A455A40AB57F001BC76233B', 1046, 'Crianças');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D7BB580CBFEBC4449E3506010BB6FC34', 1046, 'Sobre Base');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('43FBF652FFAC304EB83FCFCFA2825533', 1046, 'Sobre Base + Taxa 1');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('33849013C409BC47ACC11F6C20EE14E6', 1046, 'Sobre Base + Taxa 2');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('09EFF3B3E4806A4791C57C4ECABB8758', 1046, 'Sobre Base + Taxa 1 + Taxa 2');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('34F87DA35D7F5E4CB39B8F5BA899B43F', 1046, 'Faturas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('DE284894A7CE354C875273F022516909', 1046, 'Movimentos');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('44572B2EFF92F8448BC3C538EEDF577C', 1046, 'Reservas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E7ACA4E4529D7349883A0FEA3BFAF08F', 1046, 'Preços');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B38CC2F40B95AF41A0EFC889B2FC4603', 1046, 'Desc. Preço Adulto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0DB4FABF537AA24498857D4206B09143', 1046, 'Desc. Preço Adulto Extra');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('6D52D042F99D8848BDA3A3D112315A22', 1046, 'Preços Fixos');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('BEC748BC841AD9489DB4A4FD5FEEC907', 1046, 'Solteiro');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7C80219B5F73C5468CA067F7D855BB17', 1046, 'Casado');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B4D8E58D10D762478405F9AB17C9E843', 1046, 'Divorciado');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F47D3BF6E5AE1545A24F5B52616F3E2A', 1046, 'Viúvo');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9D73A97219402A418725C6958C8517D2', 1046, 'Empresa');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('11042494B4C5E742BDE59C916D77A75E', 1046, 'Direto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('74125B06774F4042B343B369E7AFD3EF', 1046, 'Reserva');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('38FEF7BF67818642B8F325EFE598598D', 1046, 'Grupo');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7DC317B831FE3045AC427A7D87817B6E', 1046, 'Hóspede');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7FCCBAEBA0A73342BDC9F059822323F3', 1046, 'Conta controle');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('98E6B389B6A90644825D2CFCA0452714', 1046, 'Crédito');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('EDE7F830C131CF408EC583CDFBE07B32', 1046, 'Débito');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5365EBE8C3B81D4AB2A3992801C2771D', 1046, 'Chegada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('62264E5BD30A8D438C8E3F5DB44F96BD', 1046, 'Saida');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('922E1BAE5DD111489E5FF83CDEACE354', 1046, 'Room Nights');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('902024046D10B94FBB715EC4B6DA21A8', 1046, 'Crítica');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('21E01AB3E5297F459B72784AF7D0D3FF', 1046, 'Erro');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('948EE62AB9D8314F8FDAB16DC2871018', 1046, 'Aviso');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('EAA29A0E8DD5594DAB0503F24C88029F', 1046, 'Informação');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9B9DF2888BF1104E89448C7E5A2AFBD0', 1046, 'Auditoria');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('170466C0B8E33641800E41DC15285BBD', 1046, 'Regular');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3754BF7D91435D43A257265AA23DD34B', 1046, 'Charter');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E4405FCACC4C6C4BBE7A407224E8A775', 1046, 'Resumida');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('EE22A0F0F2C74642B786473774006381', 1046, 'Detalhada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('BEE3DF9A2E48B54698F82BE3ACF1EF49', 1046, 'Informações Comerciais');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0753C395304A43489414DB20501FCB53', 1046, 'Informação Fiscal');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A1DA451D66878743ADFBDD9A0D8E25EC', 1046, 'Atacadista');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9363903034C2064DABF315C98C86D6B2', 1046, 'Representante');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('CC042D6A5ED7AE4E81B8B4C085794CA3', 1046, 'Bruto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('AEED8E7DD1A39D41ACCE21C3B0E59B63', 1046, 'Net');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('99C6DD7D74939742B881C374AB86DFFB', 1046, 'Diária');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7E61B1CE765F334E93C1352DBDD63DE4', 1046, 'Intervalos Tempo');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8894F16651FA2E4BBF31F2DC1B71109A', 1046, 'Manual');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('6C1F9FB9F7593942AE1271CDFA93557F', 1046, 'Automático');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('AE82F579FC249C4B8BAD7227D6A58964', 1046, 'Listagem Control');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8C2F728854F15840BBF54A3782B49713', 1046, 'Listagem Final');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9E934B0406C59E4B99341B8C33E0EACA', 1046, 'Baixa');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('62328B644F10EF4D8C992D7FE5D67097', 1046, 'Media');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('091BA3B5D88BE04DBA60CD90A4C98C3B', 1046, 'Alta');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3AD76BD9F8FA754E925D6AE8BE02DB8A', 1046, 'Reservada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('696119608C83E94E82F839BC28584060', 1046, 'Check-In');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2E470DD57CA2504890F23DE7B9FC2D0B', 1046, 'No-Show');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('1C2D7B13F6551B42A9C6E38E2DD5845B', 1046, 'Anulada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('019C23F34E9A2340AA5DEEC332A46873', 1046, 'Check-Out');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('CE4E5DC57CFECE46B0E9C9EBD2DE970C', 1046, 'Casa');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('88503A5F3D70F647AFEDDF9CF2DFC3AE', 1046, 'Sujo');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C70DC8533E22154F98F30BE39BF6ED92', 1046, 'Limpando');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B5175512C99C3145A45DB559B98ECE0A', 1046, 'Verificar');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('253A491969A06B46B2207283513FE56E', 1046, 'Limpo');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('22D33A940ABE7A4CBC75723DC9A0BDC3', 1046, 'Texto + Numérico');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('336E76C3AC9D034CB31F0BC974FC6312', 1046, 'Numérico + Texto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D19358A7F52991438525E5668224FFE7', 1046, 'Ativa');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('384DBA5DCAD4D54AB79659143BB617C7', 1046, 'Inativa');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8718F6ED6ADA1240B8E8916561180803', 1046, 'Futura');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('723C0A564A99A3448436DB7C11BAE845', 1046, 'Masculino');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0C13B3429731924DA1022BEF3DB778ED', 1046, 'Feminino');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A8A4F60E974B0C48978B934FC033DFD7', 1046, 'Imposto por serviço');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('DA071B48D03CD0439ADD709E97C805F1', 1046, 'Modelo 2 taxas (Portugal)');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7800400B91D20F489437575D98DC5C04', 1046, 'Entrada IVA');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('61FFC2FB7A0C834DBBD5916F9B73A226', 1046, 'Saida IVA');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('468E3E9CCB72874EBF290554E8DEF135', 1046, 'Todos os Serviços');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0213430E50335341B837BF8C8BF35DA5', 1046, 'Quarto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F9E8E7E09035DB459947F5713B576137', 1046, 'Pequeno Almoço');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('68CCAB28054DDD40B107AAEB1326F4CE', 1046, 'Comidas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('EDD5D578243EFF41BEEADA6997F8A1B8', 1046, 'Hosp.+CafédaManhã');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('642D4A1814D5034381751DACF78905C1', 1046, 'Hosp.+Alimentação');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2D02C81601D087429BDEC8EBD47E9395', 1046, 'CafédaManhã+Alimentação');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('FD4D2CEDE217554E803A03F092B05646', 1046, 'Hosp.+CafédaManhã+Alimentação');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0E30DD6472D9EE4AB3BF13C64B10D151', 1046, 'master');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E7FC4A3260F3944482098D48FAE38C14', 1046, 'extra 1');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('1C143038C8887B4ABBB124837BB8974F', 1046, 'extra 2');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('1EF46D54DD40B24BB1B55CFF4C055FEC', 1046, 'extra 3');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2857A57CFA062C41B9712263789E9E31', 1046, 'extra 4');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5BC090C7DBCA544E8EF71D2C08592EA4', 1046, 'extra 5');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8020D90A50D78F469050267BECE38D39', 1046, 'Apartamento');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2848BD2FC6FBDE47ADEB7E5DB5368963', 1046, 'Paxs');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('4EA0DF0E51F8694893B730330B216CF0', 1046, 'Apto+ Paxs');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F2E06DC2E38AD64EB742434EA654E5B5', 1046, 'RO');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('257E8C1F84BA8145A262A52AAF13A263', 1046, 'BB');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9B45A7E86AB122478E83813E0DFAB885', 1046, 'HB');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3849A02B7625264A987DD37C97B1EE86', 1046, 'FB');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('87F85E1B9F41284CAC0A79DCBFF53584', 1046, 'AI');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('22421FD753B9B94CAA07642D30EF1549', 1046, 'Proprietário');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('42FF0BC83E5BA74CB060FDC42616CFC9', 1046, 'Time-Share');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C21520CA8FD7A8409E7997CA15C01EA5', 1046, 'Contato');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B6A40E80D919684E8381870652F30633', 1046, 'Vendedor');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('1678D1985DC70F42A4115ECC401F9C56', 1046, 'Conselheiro');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E8A78A0359C32D44ABD08D74B89FCD3F', 1046, 'Cotar');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D07C03420176504D8530E0598E77A1CC', 1046, 'Waiting List');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0C273C4B1675734A9BC1F24D2A3476F7', 1046, 'Confirmada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('EDF0C486B7B6414187717395F52493C7', 1046, 'Tentativa');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7CEFBAC07F5DA74898E37119811A9B35', 1046, 'Carta E-Mail');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9DB9262EC224684BAB5E653600A0D6C5', 1046, 'Confirmação Individual');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7DDAC2F671628B45AE2CE4DC0930F462', 1046, 'Etiquetas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F50B141BB62E2E4489CD8E844135A2DC', 1046, 'Ficha de Cardex');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('127E0F66AE9EDD46A0B2BE5BF30453B0', 1046, 'Confirmação de Grupo');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9AD4C131D158CB4D8FDB4F77DFBB6EA3', 1046, 'Pedido de Adiantamento');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('847E98C0CDE63440A2EA297D041AFDF6', 1046, 'Cupom');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F0B6F6C1A2D8A34A8DD7290A04C38410', 1046, 'Requerem, não enviado');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D67A591A42C8EA47979A5BCE94BEBCB4', 1046, 'Requerem, ja enviado');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('35B327D7F4ED3140907D12B53A2946C3', 1046, 'Autorizado');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('66B316120C318E4ABDAC8CC5AD1365E8', 1046, 'Não autorizado');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B8D58A86951C4843A79C70CE96683666', 1046, 'Pensões');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B38D946DAA0D1F4C81457AB116BC34E4', 1046, 'Standard');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D87901578E39D442817A2636CBFC428D', 1046, 'House-use');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('14E7BCECF60B21489D3ED7487E04B514', 1046, 'Complimentary');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('720AE5BC0CD8F64FB74A824CF22A6A1F', 1046, 'Convite');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9730DB3ED52DEF48802118471CC37B5F', 1046, 'Idade');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C36E4F680D818946AAB31EC906344BB1', 1046, 'Ordem');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('81365F655182444A8FEE928AA2A6545F', 1046, 'Base');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('95B1FBEB16ECC042A32CF4FA1D221594', 1046, 'Extra');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B82B581F7648084798E8C5A9F092D45F', 1046, 'Café da Manhã');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('224D2F7BB6D5164E932D728B66BBA8CA', 1046, 'Almoço');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('EBA340A69198E04DACE3F04FA851FFB5', 1046, 'Jantar');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9D1442E14F12E742AE4CCF2C08C2D7C6', 1046, 'Noites');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('901962004FF1FC4AB35A47F47F479D7B', 1046, 'Fixo');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5F1968EB9274E64CBF0BD1FF51F3E31B', 1046, '% Estadia');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('69C99583B8D8D54A90DDE0D68808D306', 1046, 'Criança');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('458FC8717FEBAC4B81AF0C93EB9C81A9', 1046, 'Bebê');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('89F607FE1777D1428E863D75A534D10B', 1046, 'Ilimitado');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F8ECAA66983FA24995CEEE63AFCB7ACA', 1046, 'Limitado');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('227D2C79C6674D44B6C77793521FE33D', 1046, 'Base');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D8A6723AEA554847BA9621491783BEA6', 1046, 'Suplementar');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('727A2C8165E76E449E838B3D7BEFC266', 1046, 'Almoço (dia chegada)');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E01EB5B17E26F24F81FC381DC7D25F41', 1046, 'Almoço (dia partida)');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('6FAD476971B64D44BC4C5C869B9BF433', 1046, 'Almoço (ambos dias)');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('384853495BD9AE41B0C1B3FD940E011D', 1046, 'Golf');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('1A854B82D249BE4D9AE21A7C882168CC', 1046, 'Cobrada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3405878AA94F52408DBF851937F88A89', 1046, 'Transferida totalmente à contabilidade');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('376C9970AA489844969A299C9CE7877B', 1046, 'Transferida parcialmente à contabilidade');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('DF151544B679E442BE434F741211780B', 1046, 'Pendente de regularizar');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E7AACCA1A56D804FA943ABD120208532', 1046, 'CRM (Pontos)');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3F58A3878B7F1C4EAE2A837984AE763D', 1046, 'Cobrada + Pontos');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('709D8541BE80F744A0FC47E8565CA997', 1046, 'Pre-Visualização');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('38F166BB168E4E449D4085BD2CD1724C', 1046, 'Rejeitado');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('75AA772D793A5D44A9594D1CDD842B16', 1046, 'Autorização Necessária');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('45F23A4EBD9FCE42A43B334BB8599641', 1046, 'Cancelado');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0E3597C1E03FF64D8FF1E5059B9C2334', 1046, 'Inserção');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('45DCB5B46E42AA4EAAA79AF5E6DF11D6', 1046, 'Modificação');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('AADF260918749E469FDE40D1896B941E', 1046, 'Estada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E650F03E87BB4E4B9A1B9D09207883E6', 1046, 'Stop Reservas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('22A00B15EFE2154AB0EB3C970F1E7F89', 1046, 'Stop Check-In');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('332F40DC34B5A0468902FB2AFBC383D8', 1046, 'Pré-Pagamento');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D5CCA4BB8A03154C80C626923520D70D', 1046, 'Remover');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E2293E1AD86B6D4FA205FD1C1D51D08F', 1046, 'Reserva e Check-In');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2B5536CB651DC44F9D0801F91D406A63', 1046, 'Check-In e Fecha/o dia');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7C128D8C1FB57D43876CA64DB8A79FF2', 1046, 'Check-Out e Fecha/o dia');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('97672AA78B9FD940A059DE0B4142F99C', 1046, 'Todos em Reserva');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('EEB6D373FA29F2429384D89C2157ADF3', 1046, 'Todos em Check-In');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8B0C03ABB8F3B64AAC4A4CA43A54E1D8', 1046, 'Só Titular em Reserva');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('ED3754075CBF09469FDE94C36C7088DF', 1046, 'Só Titular em Check-In');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('59F335B1679FF847B69B84462A5E4F77', 1046, 'Sem pré-emissão');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B44740206A92EC40AA51B316B707FF1A', 1046, 'Fecho dia');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('1BF35895DA2C7D42B6DC2342D1E00CBD', 1046, 'Todos em Reserva');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('CC725EBB6490A54D83130853B2BF5BCE', 1046, 'Todos em Check-In');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C7D539DCAACFBC48A69386DE9609A5CE', 1046, 'Só crianças em Reserva');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A66776F14FA9884697C5CE1502AB73D3', 1046, 'Só crianças em Check-In');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('46804F54FC3BE7498ADC44BCC0502E4C', 1046, 'Geral');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C8DB48B12BAE6F409506EC5A87EC25EB', 1046, 'Específico');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F8BF7C463CD50942835D06F0A2DD2219', 1046, 'Sujo/Limpo');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('202EF7B27036D74FA4A20FF1ACD62BE7', 1046, 'Sujo/Em Limpeza/Verificação/Limpo');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E0F224E97E8BC046BD8F6D6C0890D35F', 1046, 'Elevador');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2B9F7FF3E355424FBA87D61469CDFAF9', 1046, 'Escadas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('1B527C44FB068D4D8C60B9657B64BA3F', 1046, 'Governanta');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('CCABA808B9295B4499750D95F0436062', 1046, 'Estada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('55BEE91479B2B04B90DF0325BFEA8045', 1046, 'Adicionar');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('066C3AFFA74FAE4B9D083EF8AAC1362E', 1046, 'Descontar de Diaria');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('405AE5D4470F304BB9023AD9770EC061', 1046, 'Telefones');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('853726AFA7454A45887C514275C322D5', 1046, 'Pay-Tv');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('485E28F974CEAD43985F5C61CAFF9641', 1046, 'Chaves');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2252E64E4AB78D4EB3CC734958AC8396', 1046, 'Internet');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A0D0FDA57AB80C4582F336EBAA47ADDF', 1046, 'Portões');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2AECF9C953DA864C9D03387C429DCF60', 1046, 'Ar Condicionado');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F2C5C452ABEBD547857CD1554A2B73CB', 1046, 'POS Externo');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('33E25E056CE76949B2A06A7594F44E72', 1046, 'Solicitada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('CAFC8BBA4AFDA24C9709802329E050FE', 1046, 'Anulada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A6769C724E939C40A2776613B8DBCA4A', 1046, 'Executada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('137B02A91ABBD444B242E7EAA773C456', 1046, 'Programada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5CF70FA08A1CBD4B9299326D0349D8D6', 1046, 'Sem resposta');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D256235C723DA747A2E7DF399F2FEA48', 1046, 'Solicitação Anulada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('1FC158975F331E45846B46CDFB4141BB', 1046, 'Carta');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('4442DEEEA823F24DAFAEB59190AA2A26', 1046, 'E-Mail');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('DBB18FA623455A4EB53C964171977796', 1046, 'Telefones');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('04908A80E66F6C4C96798FA271DF92C5', 1046, 'Fax');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('751529DA7B67004E907E0FD022ECD0EF', 1046, 'SMS');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E6006AF6D25EF9449DAB7F3B72B9B6AA', 1046, 'Por Serviços');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('AEA03462701E6E41B592B39DC3B02AB9', 1046, 'Português');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('36F4E43A6CEDB045809836A92DBBDCA3', 1046, 'Max. Reservas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3670A41227E6E94C94B98622DDD625B9', 1046, '% Ocupação Crítica');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('CD86A2F7EF6E8744A69BBE1AD3832D68', 1046, 'Ocupadas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('BBDB83872C46654490A946F7CA33E447', 1046, 'Todas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8DF0F4C84F95DF42855BDD538A6F882E', 1046, 'Por defeito');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('11A0CDBFFB955A4093C9524D6CE5C1B3', 1046, 'Incluido');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2BE32CFCC41FE24D87E181281989F93D', 1046, 'Não incluido');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('EDE1D6BCA54C5647A5D99C5D9EF81544', 1046, 'Real -> Fatura');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('4B1DB9DC480AE9419B3512677BE87E47', 1046, 'Fatura -> Real');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8C2B09772B684543B83B6D5629252318', 1046, 'Verificar');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9D0E2AD5DEA9514D925984472CC59B67', 1046, 'Excluir canceladas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('EC7D1BBEFB793D46BEC3480039D843A0', 1046, 'Excluir');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7B1C98E2B9AFDE40B2B756B8787FAEB6', 1046, 'Incluir');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('6937AABC5577E941B44341D79A47D3EF', 1046, 'Só diaria');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('4B8A356AB05D024CBF713AC1E07C38A6', 1046, 'Recebimentos');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('18189A43953564429F06744CE21683F5', 1046, 'Transferido Contabilidade');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D99CBC7F6943DB41AC5651CC94D30DC9', 1046, 'Desconto por Pontos');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2398A9DBCE967F4B9278AC6FD42BA7F0', 1046, 'Desembolso');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3B79D51EEC513445A94BBC010E9B74AF', 1046, 'Devolução');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('472BB15FF602674CB410FE12BDE60396', 1046, 'Fatura No Oficial');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('95A59CA0A356B74DA5456D86A5543500', 1046, 'Desconto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('DCFFBD647374F64DB1FC1F05BC503FD4', 1046, 'Gratuito');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0124A6365F16D2409FB27CE89CEB822C', 1046, 'Room Plan');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('175BCDEC675890419A5B7949D94AABE2', 1046, 'Transferência');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7FBC478D5662AA4BADE24E26D5095B32', 1046, 'Retenção de Imposto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2810E9760C830845B47AA70DF224F6DC', 1046, 'Criança extra');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('DCC3B8A677ECB74AAD8838963B5A1579', 1046, 'Nada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('95CD9D3F19132E4AB6C2422562178C0F', 1046, 'Fatura');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7BD5427F7C8D974F894C8526AE888FE3', 1046, 'Conta corrente');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('4B27BC43DF329C45BB1A2D6C3720B268', 1046, '=');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D2F48070479EE44E9515305E13FA9A03', 1046, '<>');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D936A9B8761652418E76177CEEEDE3C1', 1046, '<');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F6A3EEC56ABE2D4CB1CBB980C3D919CB', 1046, '<=');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('59F134079311464C910B536300790134', 1046, '>');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('6E1DE19D4CF840438F468D85A6272A3B', 1046, '>=');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('BBEC5132782CEA43A303D786599303E3', 1046, 'Conter');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0F0F95B87BEEE64E996F49F757EA2371', 1046, 'Entre');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('57D9B0140ADCB84485636106D7F910D1', 1046, 'Similar');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B0395315E148E74FA1B237D9D6974F33', 1046, 'Excluir');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3FEBA754ABDA4D49A1487F03680D6854', 1046, 'Crédito-Débito');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('45FE92BB33189940B874AC3F355F59D7', 1046, 'Valor');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5C1DAB8D7251A848AE42388F991B0DC1', 1046, 'Porcento');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('BD88EA162E9652478EF1D38451AAD56C', 1046, 'Empresa');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D5D40FCC4ADF3D48B7563A483A868E44', 1046, 'Agente');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7A2DA2419B623E41A3198A43FBE734EA', 1046, 'Contrato');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('664FD40F7425ED4796D6089C5AA338BD', 1046, 'Oferta');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('DE32DB3EF9167A42B127E854889438A8', 1046, 'Early booking');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A968FB78F3FE7B44874440EECC0E1CF0', 1046, 'Futura');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A20F4377638F704ABF6F49ED8F3866DF', 1046, 'Ordem');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B57CF4B28A3B9648AF895CAA0ADDF660', 1046, 'Autogenerar');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8DB874AE92CA7E4191BE58F335730514', 1046, 'Serviço');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('BD08D04B277D1A44A08A8567B4CD2E37', 1046, 'Cobrança');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('57542708E7F5114987B2216195FFEC12', 1046, 'POS');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7D5BCD1F540BA942BD4BBAB4D0980C09', 1046, 'Fora do Funcionamento');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0F155917E4E17E4FAFB966EFE285FB36', 1046, 'Disponível');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2AD6F4EB8B50A84F954154CA5B37504D', 1046, 'Conta Fechada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('BF239AAD27AEE04EA03F9998900B6003', 1046, 'Erro');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('066F826D4FF6A24BA507BC3E056B887F', 1046, 'Extensão não existe');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D44CC7B88C9D7847B89F041225BBF656', 1046, 'Quarto não existe');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8FB950D2B01863439F48921EFC68ED3F', 1046, 'Quarto vago');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('FD669AB14B78BA4781FADA32D05DF914', 1046, 'Diário');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9C27699CBAA980409A702B9B9E3E13E5', 1046, 'Sumário');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('6BFD82B566EBC2459F152F9A263544C3', 1046, 'Período');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2985242463400E4A9DFB3B451D7DDE59', 1046, 'Único Dia');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('598172108C54C44290F23E8CBB2ADC36', 1046, 'Gráfico');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('916288B1DDACB746AE8917552C97B160', 1046, 'Esconder');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('823D4264F9D59B4AB0E0FECCB3C15BDA', 1046, 'Ler');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('63DC5039FA9CBB408BC575B3F48E4641', 1046, 'Ler/Imprimir');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('1AFDA4CFD258374F8DFDA9FCBBA55237', 1046, 'Ler/Escrever');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F425E303B3F90F4E9BF97B02347940ED', 1046, 'Acesso Total');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C189F3F2D06CF243B2C63000C7E0E074', 1046, 'SAF-T Portugal');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('BEE6A9ADB2C9C649BDAC51B2FEAFBDAA', 1046, 'NF-e Brazil');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3A62DCCABBDDEE4791733CF327D0D1D4', 1046, 'Fiscalizaçao Croacia');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('DA2DFB61BAD5B44A9AE869B5D1B39A81', 1046, 'Fiscalizaçao Chile');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('BC8C88B838200F46A26A8A51CEB1BA0B', 1046, 'CMFLEX Brasil');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8E150DB5100A674181DF97010EA59BB2', 1046, 'Menu Fixo');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('68864A8513479D498D9E1B448B2A9C76', 1046, 'Menu Dinâmico');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('249EA9CE1138254BB3CB36FB2BD47131', 1046, 'Menu de Ronda');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('FFDE3179D2A7804493F647D0F66ADF43', 1046, 'Gastronômico');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8E6A56798FED2F449D3A1B869DD8A561', 1046, 'Supermercado');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('15DCC90C0110D94799FC22A38214AA01', 1046, 'Loja');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('244C1095FE5A754282478CECE6B3A277', 1046, 'Pequenas para botões');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B073FDE6AC6659428F2B165839E59B1B', 1046, 'Meio para Apresentações');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B7E91067EB91C64AAFAA2B08FAB725C5', 1046, 'Internas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('BDA5B3A3743B39479E1A802A3129B7DC', 1046, 'Extra');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('034AAD4BA465184B8405940BEAA54D13', 1046, 'EGDS');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('4B5A8BD334057F44B7B8CB9062726A96', 1046, 'Expedia');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('CC6EBCA6BE02A942BA052B1946FC0063', 1046, 'Guestcentric');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('980C4EC202EE4441945332F5A254DA5E', 1046, 'BookAssist');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E927F8807BF0AB45A4EFAA134671B904', 1046, 'Synxis');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A0E9348A11495F4DA4E636275901C7A0', 1046, 'Other');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('99513AD0FFEE6B4099CCB13F8F725E1A', 1046, 'Pegasus');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('83664ECF75560E45B3E18F573ACB510C', 1046, 'Cubilis');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('6E8EDA0218B92C4B8EB994434F92B151', 1046, 'WebBooking');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('EDA4FBBC006CF041A9FDC5D5B9724C23', 1046, 'One Cloud Booking Engine');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C7CF2FAD32712F43BECB5B20E387329A', 1046, 'TravelClick');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9C05D0DEE009BF49AB00F5B86D8A6F21', 1046, 'SiteMinder');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E943982DCE23424B87E343D7BD7AC37B', 1046, 'AvailPro');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('EA4EA2B0CF7CAE4FB7A13BCACE7186A3', 1046, 'TripAdvisor');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9EE552C6D0104D47A6370237BB722471', 1046, 'Booking');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3AAD31B2A740004C87B3445975AD920E', 1046, 'EnzoKiosk');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A1328D21935ABC43BE8BFBCF6A9041D0', 1046, 'EGDSInatel');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('AE150D337A764E47A39830BD4C1B8CDF', 1046, 'DBK');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E02FEC00BC06AA43AF15020E42BEDA1A', 1046, 'RateGain');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('34BEF54FF7DF7E439B4E003EEF5DF30A', 1046, 'Idiso');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2F148F6D31EA0849916A63B0952C92FD', 1046, 'Omnibees');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('17233F9C91A94BB4A563ECD420C16ED4', 1046, 'Fastbooking');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('57DF420093A0BA4ABA6E6AFE6A996E3E', 1046, 'Fixo');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2AB36974A378984E9BC655C6A420438E', 1046, 'À noite');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0B53D4F4C04DFC42AAD17A3E20D3380B', 1046, 'Tipo de quarto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('DC8C2986B4471F43B5A4E9E2F4904D92', 1046, 'Bloque de quarto');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('27A5F619034B6541A9F529BA6F913B0A', 1046, 'Deposito Antecipado');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('211A5C0FC85A83449E767C9E32E59063', 1046, 'Deposito Antecipado Faturado');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('007A59147EA2184D8F20735891A9C635', 1046, 'Notificar Erro');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('4E8B2379E4B104448D371F0D1935A862', 1046, 'Distribuir Diferença');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('6DAD6D1583BFF442AFC40042436A10CF', 1046, 'Roomstay Total');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('364F92E83699E6468D6B795DDB33BB15', 1046, 'Linha aberta');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('EA4DCEDC5F9F7A44B7C8ABBA7451E979', 1046, 'Linha Fechar');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3382BE5662A2A64FB7312C01F9862983', 1046, 'Troca de dados');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8E42A5BDAC399C49B7B794CEB1C84D57', 1046, 'Mudar de quarto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('FAE82624BB13F64AA2DF91EB3434DEEA', 1046, 'Linha aberta sem reservas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('DA8E6946403D2E44A3FB3B1BCEF243E9', 1046, 'Linha fechada sem reservas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C3490F965899C24BBFD10BD67A35E561', 1046, 'Linha fechada sem reservas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E6BA2FBFF7D78945B94AFD876F309EF2', 1046, 'Despertador');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('35115FD85CBF2E4E867CD55F2B1BF71D', 1046, '<NIF>.<Hotel>.<Número>.DAT');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E2F6A8225C273B4085D7C7BA0B9A3710', 1046, '<Hotel>.<Número>');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('711C41B09A3DE2428C3DFA3BC06F6904', 1046, 'Porta Nº');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('6CE969EC9EF3214489281B1CD888ECA7', 1046, 'Tipo Quarto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0278E7DE26105F48A8FA84DD1FB463B9', 1046, 'Bloco Apartamento');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('049BF1AA5976884FA0405D34A6422300', 1046, 'Andar Nº');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0E011827CFC0D54F8668C65852751326', 1046, 'Refuse Reservation');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('39CD62A6F7EFAD4F9EBACAFA0DFC7A7A', 1046, 'Fix Manual Price');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D393CD9B06C1DF4CBC97AC66EFA47F0F', 1046, 'Use Default');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5FE5CD0DA644864DBE829E9B5183CAAD', 1046, 'Default If Not Exist');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E5F945144320AD43AB52BF8FA568EADB', 1046, 'Use Original');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3DE64EEB65F92F4192CC7F1EDBEA9527', 1046, 'EMAILS BÁSICOS');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('CB162DDF20195F4CAACBA59F6EF9B05F', 1046, 'PRE RESERVA / RESERVA');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7A1CE3B1AE82164BA85E88198285BD9D', 1046, 'POST BOOKING / PRE VIAGEM');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E78285F0555CA44990B70A03253D39A1', 1046, 'POST VIAGEM');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B49F612F52E93D4FAD132B4E6068E30D', 1046, 'Não lido');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('AAEB894968092D4A9B29420FCBA4C57F', 1046, 'Ler');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E7395E8E02F37B419467AAEE2F67B9E5', 1046, 'Replied');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D5B2C9B22FD4B34BB875C04BE3F037D2', 1046, 'Bar');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('429D4A8C0729EF448D2460DC35A635EA', 1046, 'Ingressos Gerais');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0B58154BB65AC449923F1429BD0F7262', 1046, 'Pagtos Gerais');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5FFEDF066FEC7B4CBCD5063344FF5420', 1046, 'Desconto Geral');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C238D0D05F80F54E8A8F07716179977C', 1046, 'Geral com antecedência');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C3C16C455106B5498BCAB15949E305A0', 1046, 'Ingressos');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('FB8597D5162A294FAF9AD833747AD7E6', 1046, 'Impostos');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3913FCC2807EE149855296F05ECAE11D', 1046, 'Clientes');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('BDFD07396A59C740976F497753C62E15', 1046, 'Clientes com antecedência');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A47CD1094C8EF543BA586EBF2F27BA87', 1046, 'Tax Account');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('DFA51857A309E848B5C637FE9DC85CD0', 1046, 'Contas a Receber');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2642A655E2B62A499CB68842D23BE4E6', 1046, 'Proposta');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C63C532EFD143F4A9154073380BAB273', 1046, 'Demo');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('4199A16C86163A4FB442071028EED3B1', 1046, 'Subscrição');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('4A1C96CD908EF74890D25844A000E074', 1046, 'Atualização de licença');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0C9D35165365DC46ADC1BA1732453D34', 1046, 'Criação Hotel');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('CF38E6C7C35A2449A64538924F22F0FA', 1046, 'Event');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B8B2172249895C46B6B77BB9C03A9BBD', 1046, 'OfficeBox');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7D02EC79339FCD478DB0A56E4B5E69B9', 1046, 'Pessoas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('83F8E298A5FEFB4B96EE48549467ED18', 1046, 'Pessoas por dia');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A87C3F670F73D1428C1A6F89A12A0FC7', 1046, 'Pessoas por hora');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('36A1F122A8B5894891DECDF06A11A804', 1046, 'Units');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9ED8313C90F5A54BA3C6432C17874913', 1046, 'Tempos');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3D63CD809B9BE14993AFCA6253A18712', 1046, 'Pessoas Tempos');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C1740D48A7EA074EA54EB139E61582F6', 1046, 'Tempos de reservas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('637267EC686D4947BF15685485EC6DAA', 1046, 'Horas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D22D57D07A0B964F94C8D5B6B8C32986', 1046, 'Saldo');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('10C8489A9BFD074AB9B34000043D8920', 1046, 'Entrada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('77E5111CD052C24C9249F2C68551378B', 1046, 'Anual');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5ACA1D3182B5DA4AB2AAF941DC209D85', 1046, 'Semestral');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9245444542CA254C95F0168AD31072E9', 1046, 'Trimestral');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('914FBE7FF7C586448C951543292B153B', 1046, 'Mensal');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('732C9D4F7D74E6478C12EC31AE44AB1D', 1046, 'Semanal');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('749005CAD4778F4196C88A9454FAF220', 1046, 'Inventário');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('70DE2E8E63C0FD4580A2CA3BEF55A062', 1046, 'Disponibilidade');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('4E3EFB7213F7BC4BB7211B956B718087', 1046, 'Preços');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('36D9252868D239499BB9352118CBEDEE', 1046, 'Login');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('6EDD11234C1C014FA9CD2E6C481A70D4', 1046, 'Fechar Turno');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('34D3DFC4C44BC143A389F528F2837961', 1046, 'Fechamento do dia');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('11AC60BA35255240B3D046B346497B33', 1046, 'AM');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('FAAE0808ACB65B488B2C8234B02DFBAA', 1046, 'PM');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('45472BD9E3EF7C4BB01DB0C1A8E4A00F', 1046, 'Sales');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('14B65805B863E545815A6B9FFFEC7122', 1046, 'Anular Vendas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E5605EEF8FA300418F59149DD2F150F6', 1046, 'Anular Devolução');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('4599B58083F71545B9B65EBD4E8A584A', 1046, 'Não Encontrado');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('51DF5D24B67AC74382D19BC78E16E772', 1046, 'Processamento');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('05B88C6AB0198C4C8A2FE0D5B7138A63', 1046, 'Erro');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D91781851AD9754385BD11E2D3CB6E00', 1046, 'Sucesso');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F8326F5959BE41FF9C9D242583AA9BD1', 1046, 'Pendente');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F8F25D9A0AEF8E4ABF4A1CC8331A82C5', 1046, 'Email Enviado');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('DCDCBE8D7A4AAB48B795484358B85842', 1046, 'Shift4');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('09004A2C0D5C324EBAA50D29CA761500', 1046, 'Authorize.Net');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D62967017BD5F542A54EF7809991ECA8', 1046, 'Unicre');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('EFCB7417680227469322AF136034D5A5', 1046, 'ClearOne');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('1FE9958B63150C4B9308A2F4222EE659', 1046, 'SuperPay');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('15B88C06AA35004781ED3E2121353404', 1046, 'Six');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2DB589A7599E224D9298B477677BA6F6', 1046, 'Departamento');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('ED155E575EDBE24589B51175BC1E55CD', 1046, 'Serviços');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('63CF43887BB50746B8C7D245516E823E', 1046, 'Taxa de Imposto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C3FEE03D80D93D4AB804F2F0651C74B2', 1046, 'Tipos de Cartão de Crédito');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('FC6D601B653F894DB4413258D1CDD370', 1046, 'Tipos de desconto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9C74B6ECB7C7A34C8882092303AA5484', 1046, 'Formas de Pagamento');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7A3FBBA77FC2BF47BF894E827C782AC7', 1046, 'External');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0C0C21C303F74F84905AACAA701CC381', 1046, 'Exportação de Reservas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9468AE0C069F5D45B11AD9D7F948BCBF', 1046, 'Sempre Tarifa');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E8500D5E3DE0CB499893EDB9FDBD2403', 1046, 'Sempre Manual');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('43E288A7B6588942B28DAD48C57C056E', 1046, 'Combinado');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('88869B9F8D43A443B84F514FAB27879D', 1046, 'Chaves');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('AC3189182840884F8E104F63A469D634', 1046, 'Leitor de Cartão');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F8002DD68B14764EBC0ADB6D964B0910', 1046, 'Escaner de documentos');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('30B0F68E8F6F4CF199AE2503D8215F83', 1046, 'Terminal de Pagamento');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('AAF01E23E5BDA04B838B637441C3F6F8', 1046, 'Metros Quadrados');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('6EA99A10D89F4B468E11C64B774DF0D3', 1046, 'Pés Quadrados');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('6900DEA3DE91D342B93B73EBCBA81DA0', 1046, 'Quarto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('985CE7CEFB91FE4E95A2B5D0CAD9B53B', 1046, 'Banheiro');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('535CD11497D0A247B76ADF299C124EDA', 1046, 'Sala de Jantar');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E31FB24DA0C0964F9BE16C0093D104F5', 1046, 'Cozinha');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B152FE57CF0EEE488D2EB7D1176136CC', 1046, 'Estudo');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5F45EDECCB3E2C43B1AC6B67F1BA5DFA', 1046, 'Sala de Estar');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B5E19E3D22413D4FB26F2371BA0FB230', 1046, 'Quarto Familiar');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3D9A3E8C04FBDD4B83B11BD402038CC0', 1046, 'Biblioteca');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('57F545624849E14C9223FC600264473F', 1046, 'Escritório');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8E111919290A984786918DA1EACD3058', 1046, 'Varanda');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('DF9470124DD92D439EB01A0E3D835A21', 1046, 'Outro');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A51DD74BF1C09C4E8C10873B7C95B5E5', 1046, 'Norte');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B00414C287C1EB4EA4F1E0FDC074B934', 1046, 'Sul');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E0672EA69075C945939B8A41BC0C4715', 1046, 'Oeste');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('EF35DD7AE15F094EADC95BAD1DB300DE', 1046, 'Leste');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0CEC85E4A1481244ACABBB647680E820', 1046, 'Condomínio');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0C93074DD4D29A47AF639BDD0EEE1840', 1046, 'Residencial');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('823EA7D31D9D034E9B6F1C3DEC93147C', 1046, 'Residencial Misto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9A1F199E4C3E0A4EB3790CEC2F72A63B', 1046, 'Comercial');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7D98992E5F2D3B47811A2F65A186B1B3', 1046, 'Habitação Social');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('BE7EB3ACF1ADB04C88D2E0B20835CD16', 1046, 'Habitação Senior');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('68910084AECA2D43AF44E0E1CA38CAF9', 1046, 'Estacionamento');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D53AF54EC235764C831DED8AB97D1426', 1046, 'Armazenamento');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('30321FF74C77494A98F5F5F8676A0F92', 1046, 'Associação');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0C020E546307D44B9ACC6C19C8D8CA21', 1046, 'Militar');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2C51B94594287C44AA7D7455AF2F7727', 1046, 'Outro');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F4B9A0D2B14E164499A105BB3E25E7B4', 1046, 'Tijolo');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('CB75E8329C9E9246BC94EBA6E75EE635', 1046, 'Madeira');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('BF9248238E3DE54E99651846B657A5C1', 1046, 'Bloco');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A815F1403C160B4D9A74C37246304AFF', 1046, 'Painel');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('090FFD5832FE234C81F5EAAE280B2A6B', 1046, 'Outro');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3DCE1534937F014F9F657BE37BE36A22', 1046, 'Madeira');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5676A34526603148BAC042A94EB7254D', 1046, 'Azulejo');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('6C7356B8AE274F4A9F20370C9DE22DBC', 1046, 'Laminado');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('648A4AFC76F6F341BAE5FC9518A188AA', 1046, 'Tapete');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('64D8E24B0ADD134AB8E9E33B579F2E12', 1046, 'Mista');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B9178EBFB5BB8C4A81DED8349ED17EF5', 1046, 'Outro');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A7CEA0D9033F194B90680E1A594B41F7', 1046, 'Municipal');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3746489F278AB543B294078B7AB5298F', 1046, 'Poço Particular');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('DDA786AEBB9AFA4FA6362C6D44726A80', 1046, 'Poço Comunitário');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('03EA587675D95D4584D77B49057F7F63', 1046, 'Outro');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('65526006C4222E4088F797B0B1EE773F', 1046, 'Gás');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('CE6D30779928304AA7A2F06344A8EAE3', 1046, 'Água');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('AA6D3621D3F64D498EDF7F02299735B4', 1046, 'Hidro');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C23D32BF755F644AADB401A45FB8BC80', 1046, 'Cabo');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('4FF4AA36F6E12A45845D17343126ED98', 1046, 'Internet');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('30E3C5A2F7017F4E8784D81743688EE9', 1046, 'Nenhuma');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('BB1A05893FFFB74F8105ADF7E8E8F0C1', 1046, 'Valores Fixos');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B3BF3E95840F5A4581270A357B6F06DD', 1046, '% Comissão');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('86C119B2F90BC74C8C2FA970B33E9721', 1046, '% Comissão Tarifa');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B40AE04E25BFB346A20B931E0E4A7C4E', 1046, '% Serviço e Tarifa');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D19A120C640CC34EAE4CB63CD2F6B58E', 1046, 'Ocupada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2521EA1A45AC0E4BB2E0365DF7D968BC', 1046, 'Livre');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5EF22B9ADFEF564692C44C34F6DE04F5', 1046, 'Data de Chegada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('00661A2CFDD4284ABF4C9FB19DF0CF83', 1046, 'Data de Saida');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('77C603509BFFA24FB81AC2C0D174DC59', 1046, 'Predefinido');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F2741D59D2B7F249A8BD6343E34607FD', 1046, '=+ Propietario');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('793008C884D2424A937D69464FAE6233', 1046, '=- Propietario');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F0A4F98B21F90E44A9B4DDBC2F6C8A87', 1046, 'Pending Credits');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('473F83F63E9361498BEAFC398C6E94D8', 1046, 'Pending Debits');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0080A085D86A704483EFAA06F7B0BD95', 1046, 'Hotel');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('BDCCF1D0E20EBB41B76AB1A773A9C7D9', 1046, 'Hostel');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('6C2F24D01EE3D4409DF8DC9513055E04', 1046, 'IBE');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('1965A31DFC701842B226AD96D473D5B0', 1046, 'PMS');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('CCD230520F2A0F49AEEBC0AA86B0272E', 1046, 'Chamadas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('CC655497440BF146ACD768468A28FE62', 1046, 'Mensagem');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('44F1F7B56F342D4B860FC7DF3E719913', 1046, 'Código Original em Voucher');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5553DEC9E1262F48A3BC957B554F4EAA', 1046, 'Código Promo em Voucher');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0BB8971363D4F648A03D78F6D11D643F', 1046, 'Total regularizado');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('917AC2E4EECEFB4E85D3E518F024A530', 1046, 'Parcialmente regularizada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F814C5AFDC8A074A9BD87DE1D5B0184B', 1046, 'Não regularizada');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('4B2D58323DBDED4F8F9466B80414C527', 1046, 'Preço por Noite');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2AE5DA03D052514EA63B089049C15F02', 1046, 'Preço Total');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('33CA6337C3691247A446B3CAB540878F', 1046, 'Definido');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7D5B3A53614AED4280B862959FC2F984', 1046, 'Utilizadores');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('FF8D0251D306964D9631EAFA1043EE2B', 1046, 'Clientes');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E6261557BBF7CB4E8C9401FBA24FD6CA', 1046, 'Reservas');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8F084C1D28A0DB4AA8C5845113999CD9', 1046, 'Lazer - Férias ');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('28CE930F0C08834999538E05459A0158', 1046, 'Negócios');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('86F71EDE1171B145BB0AF758E3CA8C78', 1046, 'Convenção - Feira');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C00D9438803A2C44B3A0849DD989FF3E', 1046, 'Parentes - Amigos');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D39DE14DD1DE0B40A17965E2DDB7597F', 1046, 'Estudos - Cursos');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C161FD80E7A00C449094B6959C1C9F27', 1046, 'Religião');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9690B9CEB68D734ABD4243140D287027', 1046, 'Saúde');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('DAEADC3D5678394481D0BC044151F423', 1046, 'Compras');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0647E12A5181114F81EBD552B56A5229', 1046, 'Avião');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A666CAB7B38D8F4DA14ECF9F8857271D', 1046, 'Automóvel');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B3C3D276863B264191248666C6CC227F', 1046, 'Ônibus');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3B18EAC80B5D2E4492B20809C8F7239E', 1046, 'Moto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('BFCF53A3BB3ECA49B394C3EEA31F59A1', 1046, 'Navio - Barco');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5A1D017FCB885D40B4458F53248CB611', 1046, 'Trem');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B3925EBC0E788840B7CA3AF0B7F82EE2', 1046, 'Outro');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7EADCC609D440244B203C6C6D2018AFC', 1046, 'Categoria');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('62D10986395B9F40AD9DCFC202936847', 1046, 'Tarifa');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('75B2C13929707C498C1BEB060F2A2F23', 1046, 'Produto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0FFB5AA3885C2348A8B296F5CE6DFA10', 1046, 'Tipo Quarto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('FDC427B4B9F37B4B8437ED9C42511B44', 1046, 'Parar');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A286D3B6C9B1134A92040A7976C95B08', 1046, 'Alterar o tipo ocupado');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8FACA7CF2518A74FAF12CF722FE7D26D', 1046, 'Remover Quarto');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('EF8BB46BD53EB047863438A6A05EA479', 1046, 'Preço Diaria');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A09D632E40772140857713BA52F19546', 1046, 'Preço Quarto');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5B4A470A1D756E4AA7041683B420B5ED', 1046, 'Apenas Serviços');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('536B19A04C9AA843A19D37F55577936A', 1046, 'Gorjeta');
insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('708A4413CFB66540BAE3189F972CFF60', 1046, 'Taxa de Turismo');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('0B92CB7E038946F8B8F89945AEABA5E2', 1046, 'Release');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('70630F6FB8F1471092533AA00B3AB2A5', 1046, 'Primeira Noite');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D2AF0FF88F024691BC4E19C6E466BBEF', 1046, 'Próxima Noite');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('70BD376321C14DD0ABA5E81740A418DF', 1046, 'Toda a Estadia');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D1F5BE711F52DB4186CC5D7496D9624E', 1046, 'Adicional');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('EE879CC45F1B7C40A7FEE12CD8578CE6', 1046, 'Custo Entrada');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2C60B26C97D5104B9D306C6EAB06D4F7', 1046, 'Reserva Spa');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('DE296A510C60114FAB24AB7DCDD8BB4C', 1046, 'Valor Manual');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5801625B7FDFE140977F90BEDC4611FE', 1046, 'Calculado');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9DDF53638F13394A9B718711B6D21C67', 1046, 'Título');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7970844226054B4AA338572DE5F47955', 1046, 'Separador');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B67646EC95687D43B2267ACC5FC8A2D3', 1046, 'Geral');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('CB81CDD8CACBE240948AA353EF4A5655', 1046, 'Condominio');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('1C7329F951A5CC448DFD2E8704E85A86', 1046, 'Extras');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B74B16A99F0F420D822A74674909F186', 1046, 'Histórico');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('FA42225BEF4B6249AFAA35ABF0AA1515', 1046, 'Cobrança');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('90C16E85A1B2462C85668A7EE3816DBE', 1046, 'Pedido');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5F3F37C1B994485FBFF50F22798D2A86', 1046, 'Lido');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E12ADDAEF123446483717DE310C881F5', 1046, 'completado com sucesso');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5C561EAB22D5471EA81ADDA9E890C617', 1046, 'completado sem sucesso');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A95A7062BFFC4D729C6BECCD9F1017EC', 1046, 'TimeOut');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5AAB90D2BC4480408FD0AFB27F1281FD', 1046, 'Exclusões');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B8EE016B31947D43B52B47C678397118', 1046, 'Isenções');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D211D27C70175541A090C310CB8894C2', 1046, 'Empregada(o) Quarto');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F64C2F98402E0F4590A89994FA1A3695', 1046, 'Cobre turnos');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F18B60D8640F0B4B829A4888C2AA046A', 1046, 'Empregada(o) Quarto - Tarde');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('63B508EB5A3C764B9619151F7F71EF22', 1046, 'Outros');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5AA69113620546CF94C7E936559B34E0', 1046, 'Água');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9BF21002768846AA8F96253DF6382AAB', 1046, 'Energia');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2E14CE748ECB4C72801695C3F468750E', 1046, 'Gás');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('D02BBD58CDBD42B38F59993585AB2C2D', 1046, 'Quartos (Inventário)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('1F5D20912F6C442193DBA56EECF68634', 1046, 'Quartos (Inativo)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('DBA1D9676B37480E823FA7D7F1652423', 1046, 'Quartos (Ocupado)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('5A373F1729A345789C2AD9D4DE892F1C', 1046, 'Quartos (Registado)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('43409EC442124CFB8E7986EB13A61FFA', 1046, 'Quartos (Saída)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8171517ABF604BA9B85BF56A800B7A6F', 1046, 'Quartos (Cortesia)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C02298F3B2E4466EA3BCE5AE5ECAC328', 1046, 'Quartos (Uso da casa)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F8A295C1E5CB4DC987A232DD60A36B96', 1046, 'Quartos (Uso diurno)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('EEE28EDD3918412F802FD645DE9E305F', 1046, 'Quartos (Não comparecimentos)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3EF4A0AF5BD8421487E53D1300398575', 1046, 'Quartos (Cancelados)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('7BCBFABE66574A79808AD7BD79D2F885', 1046, 'Quartos (Visitado)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('4B25A3DE930740FA8837D35D937E259F', 1046, 'Quartos (Lotado)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('2C848955307A40D7A235771E5A982042', 1046, 'Hóspedes (Em casa)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('85E05AFF145E4F1E9BEAF81903353892', 1046, 'Hóspedes (Registados)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('BE0B01A81A1B448D9636EF00C086EDD2', 1046, 'Hóspedes (Saída)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('177922BE863C41A8A289834942F6FD2D', 1046, 'Hóspedes (Cortesia)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F62632F85D1A470EA92760037156F435', 1046, 'Hóspedes (Uso da casa)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8CB3757565F24BF8A6230B4DFEFE320B', 1046, 'Hóspedes (Uso diurno)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8D333E94167143629BECBEC05E9F0A0C', 1046, 'Hóspedes (Não comparecimentos)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('73C9F8811EFF43BDA932917B2712157A', 1046, 'Hóspedes (Cancelados)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('87E3CF54D84041F388E425089E597BDA', 1046, 'Hóspedes (Visitado)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A3886A3093B846AB8CFDB4E2A9EA95E1', 1046, 'Hóspedes (Lotado)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('CCF98B67BCDA47B78C81784255C141BC', 1046, 'Saídas inesperadas');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3DBCA0CB848343EDAC3D416382F777D0', 1046, 'Nº Camas extra (Ocupadas)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('9947F572D3A74B8C89B6A3AFB56C5A38', 1046, 'Nº Berços (Ocupados)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('AB749ED28E06420AB7F3661955D0C37F', 1046, 'Nº Cafés da manhã');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C2431EE742CD472D82D3532DFD58FFD5', 1046, 'Nº Almoços');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('61D73A49A5B44B9192E385054C54D2A1', 1046, 'Nº Jantares');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('60F04DA84A464D2A9D06793E33DD0FFD', 1046, 'Ocupação % (Inventário)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('C549EC0EB64D4A42BFB67F809260E593', 1046, 'Ocupação % (Inventário Ativo)');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('495C322E7887458E8E392BC88D75C751', 1046, 'Taxa média por quarto');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B12B76BB379441C78EE32529BCC18FE6', 1046, 'Total recebido');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('71461C7FD62D4B938342F1592E0E4441', 1046, 'Quartos recebidos');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('A7FDC0EB44584EF5A8B74FD5002F3A0E', 1046, 'Refeições recebidas');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('AECFEC3659F8449187B79FB7F77D20CE', 1046, 'Bebidas recebidas');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F388853CC4C740348D8092571BBD7A32', 1046, 'Outros recebidos');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('8B014A38446B4CC38315F78367C809E0', 1046, 'Total faturado');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('74F5379F3653454C86BF9F4B0035C666', 1046, 'Quartos faturados');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E76F386DD6F54B138EAF50B7C9E83830', 1046, 'Refeições faturadas');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('472B10916AD84951B47759B7289064CE', 1046, 'Bebidas faturadas');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('41BCECAC162F4E55BDC18BFA9641D214', 1046, 'Outros faturados');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('E21EAEA2809A4AA4B9232BB65E75B238', 1046, 'Notificação');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('28EFDF2E6034441AA1A6B4A2076C2693', 1046, 'Circular');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('B7BDF79334AD43F2AA93DA0B2E5E67D2', 1046, 'Aviso');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('F16E7DE396A2CA4ABFA8DF3965D2417B', 1046, 'Gerenciador de Reservas');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('4ECF06DC38DB604E98AE1B89E64EF537', 1046, 'Reservas em Check-in');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('4E00FC0AA7B13A4BA7C63FCA0A59BE12', 1046, 'Reservas em Check-out');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('340F857448EBFE46A26831915298082B', 1046, 'Painel Interativo');
--MD insert into tnht_mult (lite_pk, lang_pk, mult_desc) values ('3A07171DFA4C9B45BC1EB60EB46ACB9F', 1046, 'Mapa de Apartamentos');

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20170223'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20170223');
END;
/

COMMIT;

CREATE OR REPLACE VIEW SYNC_TNHT_SEDO AS
SELECT ROWID AS ROW_ID,
       SEDO_PK,
       SEDO_NUFA,
       SEDO_SEAC
FROM TNHT_SEDO;

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20170302'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20170302');
END;
/

COMMIT;

ALTER TABLE TNHT_ARVE ADD ANUL_PAID CHAR(1) DEFAULT '0';
COMMENT ON COLUMN TNHT_ARVE.ANUL_PAID IS 'Cancelado despues de cerrado el ticket';

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20170310'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20170310');
END;
/

COMMIT;

CREATE OR REPLACE TRIGGER SYNC_TNHT_HOTE_UPDATE
BEFORE UPDATE ON TNHT_HOTE FOR EACH ROW
WHEN (OLD.HOTE_SYNC IS NOT NULL AND NEW.HOTE_SYNC IS NULL) 
BEGIN
  :NEW.HOTE_CONT := 0;
END;
/

ALTER TABLE TCFG_GENE ADD SYNC_VERS VARCHAR2(8);
UPDATE TCFG_GENE SET SYNC_VERS = '20170302';
COMMIT;
ALTER TABLE TCFG_GENE MODIFY SYNC_VERS NOT NULL;

CREATE OR REPLACE VIEW VNHT_SYNC AS
SELECT 'PNHT_GET_SERIE' AS PROC_NAME, 'TNHT_SEDO,TNHT_DOHO' AS PROC_RETURN FROM DUAL
UNION ALL
SELECT 'PNHT_GET_TICKET' AS PROC_NAME, 'TNHT_UTIL,TNHT_LITE,TNHT_MULT,TNHT_GRSE,TNHT_CLAS,TNHT_COIN,TNHT_GRUP,TNHT_FAMI,TNHT_SFAM,TNHT_BSERV,TNHT_BARTG,TNHT_ARTG,TNHT_TIDE,TNHT_MTCO,TNHT_FORE,TNHT_VEND,TNHT_ARVE,TNHT_DOCU,TNHT_DOCP,TNHT_TACR,TNHT_PAVE,TNHT_DOCU,TNHT_LKTB,TNHT_LKDE,PNHT_FACT_FROM_VEND,PNHT_SET_TICKET_TABLE' AS PROC_RETURN FROM DUAL
UNION ALL
SELECT 'PNHT_GET_TICKET_CANCELED' AS PROC_NAME, 'PNHT_CANCEL_TICKET' AS PROC_RETURN FROM DUAL
UNION ALL
SELECT 'PNHT_GET_TABLE' AS PROC_NAME, 'TNHT_MESA' AS PROC_RETURN FROM DUAL;

CREATE OR REPLACE VIEW SYNC_TNHT_ARVE AS
SELECT ROWID AS ROW_ID,
       ARVE_PK,
       VEND_PK,
       IPOS_PK,
       CAJA_PK,
       ARTG_PK,
       SEPA_PK,
       ARVE_QTDS,
       ARVE_TOTL,
       ARVE_VLIQ,
       ARVE_COST,
       ARVE_DESC,
       ARVE_PDES,
       ARVE_VSDE,
       ARVE_RECA,
       UTIL_PK,
       IVAS_CODI,
       ARVE_POR1,
       ARVE_IVAS,
       IVAS_COD2,
       ARVE_POR2,
       ARVE_IVA2,
       IVAS_COD3,
       ARVE_POR3,
       ARVE_IVA3,       
       ARVE_ANUL,
       ANUL_PAID,
       ARVE_IMPR,
       ARVE_IMAR,
       ARVE_HPPY,
       ARVE_ADIC,
       ARVE_ACOM,
       ARVE_OBSV,
       ARVE_OBSM,
       ARVE_LMOD
FROM TNHT_ARVE;

CREATE OR REPLACE PROCEDURE pnht_get_ticket (
  p_pk OUT RAW,
  p_cursor0  OUT SYS_REFCURSOR,
  p_cursor1  OUT SYS_REFCURSOR,
  p_cursor2  OUT SYS_REFCURSOR,
  p_cursor3  OUT SYS_REFCURSOR,
  p_cursor4  OUT SYS_REFCURSOR,
  p_cursor5  OUT SYS_REFCURSOR,
  p_cursor6  OUT SYS_REFCURSOR,
  p_cursor7  OUT SYS_REFCURSOR,
  p_cursor8  OUT SYS_REFCURSOR,
  p_cursor9  OUT SYS_REFCURSOR,
  p_cursor10 OUT SYS_REFCURSOR,
  p_cursor11 OUT SYS_REFCURSOR,
  p_cursor12 OUT SYS_REFCURSOR,
  p_cursor13 OUT SYS_REFCURSOR,
  p_cursor14 OUT SYS_REFCURSOR,
  p_cursor15 OUT SYS_REFCURSOR,
  p_cursor16 OUT SYS_REFCURSOR,
  p_cursor17 OUT SYS_REFCURSOR,
  p_cursor18 OUT SYS_REFCURSOR,
  p_cursor19 OUT SYS_REFCURSOR,
  p_cursor20 OUT SYS_REFCURSOR,
  p_cursor21 OUT SYS_REFCURSOR,
  p_cursor22 OUT SYS_REFCURSOR,
  p_cursor23 OUT SYS_REFCURSOR,
  p_cursor24 OUT SYS_REFCURSOR,
  p_cursor25 OUT SYS_REFCURSOR)
IS
BEGIN
  SELECT vend_pk INTO p_pk FROM (
    SELECT vend_pk FROM TNHT_VEND
    WHERE vend_sync <= SYSTIMESTAMP AND vend_sync is not null ORDER BY vend_sync
  ) WHERE ROWNUM <= 1;

  -- users
  OPEN p_cursor0 FOR SELECT 'I' AS change_operation,
                     UTIL.ROWID AS ROW_ID, UTIL.UTIL_PK, UTIL.HOTE_PK, UTIL.UTIL_LOGIN,
                     UTIL.UTIL_DESC, UTIL.UTIL_PASS, UTIL.UTIL_CODE, TRUNC(SYSDATE) AS UTIL_DARE,                     
                     UTIL.UTIL_MGR, UTIL.UTIL_INTE, '1' AS UTIL_INAC, UTIL.UTIL_LMOD
                     FROM (
                     SELECT VEND.UTIL_PK
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_UTIL UTIL ON UTIL.UTIL_PK = VEND.UTIL_PK
                     WHERE VEND.VEND_PK = p_pk
                     UNION
                     SELECT VEND.UTIL_OPEN
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_UTIL UTIL ON UTIL.UTIL_PK = VEND.UTIL_OPEN
                     WHERE VEND.VEND_PK = p_pk
                     UNION
                     SELECT VEND.ANUL_UTIL
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_UTIL UTIL ON UTIL.UTIL_PK = VEND.ANUL_UTIL
                     WHERE VEND.ANUL_UTIL IS NOT NULL AND VEND.VEND_PK = p_pk
                     UNION
                     SELECT ARVE.UTIL_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_UTIL UTIL ON UTIL.UTIL_PK = ARVE.UTIL_PK
                     WHERE ARVE.VEND_PK = p_pk
                     ) TEMP INNER JOIN TNHT_UTIL UTIL
                     ON UTIL.UTIL_PK = TEMP.UTIL_PK;

  -- translations
  OPEN p_cursor1 FOR SELECT 'I' AS change_operation,
                     LITE.ROWID AS ROW_ID, LITE.LITE_PK
                     FROM (
                     SELECT GRSE.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_GRUP GRUP ON GRUP.GRUP_PK = FAMI.GRUP_PK
                     INNER JOIN TNHT_GRSE GRSE ON GRSE.GRSE_PK = GRUP.GRSE_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_COIN COIN ON COIN.COIN_PK = VEND.COIN_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = COIN.COIN_PK
                     WHERE VEND.COIN_PK IS NOT NULL AND VEND.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = FAMI.GRUP_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = ARTG.FAMI_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = ARTG.SFAM_PK
                     WHERE ARTG.SFAM_PK IS NOT NULL AND ARVE.VEND_PK = p_pk
                     UNION
                     SELECT ARTG.LITE_ABRE
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT ARTG.LITE_DESC
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT FORE.LITE_ABRE
                     FROM TNHT_FORE FORE 
                     INNER JOIN TNHT_PAVE PAVE ON PAVE.FORE_PK = FORE.FORE_PK
                     WHERE PAVE.VEND_PK = p_pk
                     UNION
                     SELECT FORE.LITE_DESC
                     FROM TNHT_FORE FORE
                     INNER JOIN TNHT_PAVE PAVE ON PAVE.FORE_PK = FORE.FORE_PK
                     WHERE PAVE.VEND_PK = p_pk
                     UNION
                     SELECT TIDE.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_TIDE TIDE ON TIDE.TIDE_PK = ARVE.TIDE_PK
                     WHERE ARVE.TIDE_PK IS NOT NULL AND ARVE.VEND_PK = p_pk
                     UNION
                     SELECT MTCO.LITE_PK
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_MTCO MTCO ON MTCO.MTCO_PK = VEND.MTCO_PK
                     WHERE VEND.MTCO_PK IS NOT NULL AND VEND.VEND_PK = p_pk
                     ) TEMP INNER JOIN TNHT_LITE LITE
                     ON TEMP.LITE_PK = LITE.LITE_PK;

  -- translation descriptions
  OPEN p_cursor2 FOR SELECT 'I' AS change_operation,
                     MULT.ROWID AS ROW_ID, MULT.LITE_PK, MULT.LANG_PK, MULT.MULT_DESC
                     FROM (
                     SELECT GRSE.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_GRUP GRUP ON GRUP.GRUP_PK = FAMI.GRUP_PK
                     INNER JOIN TNHT_GRSE GRSE ON GRSE.GRSE_PK = GRUP.GRSE_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_COIN COIN ON COIN.COIN_PK = VEND.COIN_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = COIN.COIN_PK
                     WHERE VEND.COIN_PK IS NOT NULL AND VEND.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = FAMI.GRUP_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = ARTG.FAMI_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = ARTG.SFAM_PK
                     WHERE ARTG.SFAM_PK IS NOT NULL AND ARVE.VEND_PK = p_pk
                     UNION
                     SELECT ARTG.LITE_ABRE
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT ARTG.LITE_DESC
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT FORE.LITE_ABRE
                     FROM TNHT_FORE FORE 
                     INNER JOIN TNHT_PAVE PAVE ON PAVE.FORE_PK = FORE.FORE_PK
                     WHERE PAVE.VEND_PK = p_pk
                     UNION
                     SELECT FORE.LITE_DESC
                     FROM TNHT_FORE FORE
                     INNER JOIN TNHT_PAVE PAVE ON PAVE.FORE_PK = FORE.FORE_PK
                     WHERE PAVE.VEND_PK = p_pk
                     UNION
                     SELECT TIDE.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_TIDE TIDE ON TIDE.TIDE_PK = ARVE.TIDE_PK
                     WHERE ARVE.TIDE_PK IS NOT NULL AND ARVE.VEND_PK = p_pk
                     UNION
                     SELECT MTCO.LITE_PK
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_MTCO MTCO ON MTCO.MTCO_PK = VEND.MTCO_PK
                     WHERE VEND.MTCO_PK IS NOT NULL AND VEND.VEND_PK = p_pk
                     ) TEMP INNER JOIN TNHT_MULT MULT
                     ON TEMP.LITE_PK = MULT.LITE_PK;

  -- service groups
  OPEN p_cursor3 FOR SELECT 'I' AS change_operation,
                     GRSE.* FROM (
                     SELECT GRSE.ROWID AS ROW_ID, GRSE.GRSE_PK, GRSE.HOTE_PK,
                     GRSE.LITE_PK, '1' AS GRSE_INAC, GRSE.GRSE_LMOD
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_GRUP GRUP ON GRUP.GRUP_PK = FAMI.GRUP_PK
                     INNER JOIN TNHT_GRSE GRSE ON GRSE.GRSE_PK = GRUP.GRSE_PK
                     WHERE ARVE.VEND_PK = p_pk
                     ) GRSE;

  -- grups, families, subfamilies
  OPEN p_cursor4 FOR SELECT 'I' AS change_operation,
                     CLAS.* FROM (
                     SELECT CLAS.ROWID AS ROW_ID, CLAS.CLAS_PK, CLAS.HOTE_PK,
                     CLAS.LITE_PK, CLAS.CLAS_LMOD
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = VEND.COIN_PK
                     WHERE VEND.COIN_PK IS NOT NULL AND VEND.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.ROWID AS ROW_ID, CLAS.CLAS_PK, CLAS.HOTE_PK,
                     CLAS.LITE_PK, CLAS.CLAS_LMOD                    
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = ARTG.FAMI_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.ROWID AS ROW_ID, CLAS.CLAS_PK, CLAS.HOTE_PK,
                     CLAS.LITE_PK, CLAS.CLAS_LMOD
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = FAMI.GRUP_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.ROWID AS ROW_ID, CLAS.CLAS_PK, CLAS.HOTE_PK,
                     CLAS.LITE_PK, CLAS.CLAS_LMOD
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = ARTG.SFAM_PK
                     WHERE ARTG.SFAM_PK IS NOT NULL AND ARVE.VEND_PK = p_pk
                     ) CLAS;
 
  -- internal comsumptions
  OPEN p_cursor5 FOR SELECT 'I' AS change_operation,
                     COIN.ROWID AS ROW_ID, COIN.COIN_PK,
                     COIN.COIN_INVI, '1' AS COIN_INAC
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_COIN COIN ON COIN.COIN_PK = VEND.COIN_PK
                     WHERE VEND.COIN_PK IS NOT NULL AND VEND.VEND_PK = p_pk;

  -- grups
  OPEN p_cursor6 FOR SELECT 'I' AS change_operation,
                     GRUP.ROWID AS ROW_ID, GRUP.GRUP_PK,
                     GRUP.GRUP_CHEQ, GRUP.GRUP_RECA, GRUP.GRSE_PK, GRUP.CATS_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_GRUP GRUP ON GRUP.GRUP_PK = FAMI.GRUP_PK
                     WHERE ARVE.VEND_PK = p_pk;

  -- families
  OPEN p_cursor7 FOR SELECT 'I' AS change_operation,
                     FAMI.ROWID AS ROW_ID, FAMI.FAMI_PK, FAMI.GRUP_PK                    
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     WHERE ARVE.VEND_PK = p_pk;

  -- subfamilies
  OPEN p_cursor8 FOR SELECT 'I' AS change_operation,
                     SFAM.ROWID AS ROW_ID, SFAM.SFAM_PK, SFAM.FAMI_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_SFAM SFAM ON SFAM.SFAM_PK = ARTG.SFAM_PK
                     WHERE ARTG.SFAM_PK IS NOT NULL AND ARVE.VEND_PK = p_pk;

  -- products service
  OPEN p_cursor9 FOR SELECT 'I' AS change_operation,
                     ARTG.ROWID AS ROW_ID, ARTG.ARTG_PK AS SERV_PK, ARTG.HOTE_PK, ARTG.LITE_ABRE, ARTG.LITE_DESC,
                     ARTG.ARTG_ROUN AS SERV_ROUN, ARTG.ARTG_LOCA AS SERV_LOCA, ARTG.ARTG_UNME AS SERV_UNME,
                     ARTG.ARTG_CSTI AS SERV_CSTI, ARTG.ARTG_CNCM AS SERV_CNCM, ARTG.ARTG_CFOP AS SERV_CFOP,
                     ARTG.ARTG_CSTP AS SERV_CSTP, ARTG.ARTG_CSTC AS SERV_CSTC, GRUP.GRSE_PK, GRUP.CATS_PK,
                     '1' AS SERV_INAC, ARTG.ARTG_LMOD AS SERV_LMOD
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_GRUP GRUP ON GRUP.GRUP_PK = FAMI.GRUP_PK
                     WHERE ARVE.VEND_PK = p_pk;

  -- products base
  OPEN p_cursor10 FOR SELECT 'I' AS change_operation,
                      ARTG.ROWID AS ROW_ID, ARTG.ARTG_PK AS BARTG_PK,
                      ARTG.ARTG_CODI, ARTG.FAMI_PK, ARTG.SFAM_PK 
                      FROM TNHT_ARVE ARVE
                      INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                      WHERE ARVE.VEND_PK = p_pk;
 
  -- products
  OPEN p_cursor11 FOR SELECT 'I' AS change_operation,
                      ARTG.ROWID AS ROW_ID, ARTG.ARTG_PK, ARTG.ARTG_STEP, ARTG.ARTG_TIPR, ARTG.ARTG_COLO
                      FROM TNHT_ARVE ARVE
                      INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                      WHERE ARVE.VEND_PK = p_pk;

  -- discount types
  OPEN p_cursor12 FOR SELECT 'I' AS change_operation,
                      TIDE.ROWID AS ROW_ID, TIDE.TIDE_PK, TIDE.LITE_PK, TIDE.HOTE_PK, TIDE.APPL_PK,
                      TIDE.TIDE_FIXE, TIDE.TIDE_VMIN, TIDE.TIDE_VMAX, '1' AS TIDE_INAC, TIDE.TIDE_LMOD                     
                      FROM TNHT_ARVE ARVE
                      INNER JOIN TNHT_TIDE TIDE ON TIDE.TIDE_PK = ARVE.TIDE_PK
                      WHERE ARVE.TIDE_PK IS NOT NULL AND ARVE.VEND_PK = p_pk;

  -- cancellation reason
  OPEN p_cursor13 FOR SELECT 'I' AS change_operation,
                      MTCO.ROWID AS ROW_ID, MTCO.MTCO_PK, MTCO.HOTE_PK, MTCO.LITE_PK,
                      MTCO.MTCO_TICA, '1' AS MTCO_INAC, MTCO.MTCO_LMOD                     
                      FROM TNHT_VEND VEND
                      INNER JOIN TNHT_MTCO MTCO ON MTCO.MTCO_PK = VEND.MTCO_PK
                      WHERE VEND.MTCO_PK IS NOT NULL AND VEND.VEND_PK = p_pk;

  -- payment forms
  OPEN p_cursor14 FOR SELECT 'I' AS CHANGE_OPERATION,
                      FORE.ROWID AS ROW_ID, FORE.FORE_PK, FORE.HOTE_PK,
                      FORE.LITE_ABRE, FORE.LITE_DESC, FORE.UNMO_PK, FORE.FORE_CASH,
                      FORE.FORE_CACR, FORE.REPO_CASH, FORE.FORE_ORDE, FORE.FORE_CAUX,
                      FORE.FORE_CRED, FORE.FORE_DEBI, '1' AS FORE_INAC, FORE.FORE_LMOD
                      FROM TNHT_FORE FORE
                      INNER JOIN TNHT_PAVE PAVE ON PAVE.FORE_PK = FORE.FORE_PK
                      WHERE PAVE.VEND_PK = p_pk;

  -- ticket
  OPEN p_cursor15 FOR SELECT 'M' as change_operation,
                      ROWID AS ROW_ID, VEND_PK, HOTE_PK, IPOS_PK, CAJA_PK, VEND_MESA, UNMO_PK,
                      CAJA_PAVE, VEND_SERI, VEND_CODI, VEND_DATI, VEND_HORI, VEND_DATF, VEND_HORF,
                      VEND_NPAX, VEND_COMP, UTIL_PK, COIN_PK, CCCO_PK, TURN_CODI, VEND_DESC,
                      VEND_RECA, VEND_OBSE, DUTY_PK, VEND_IVIN, VEND_SIGN, FACT_CCCO, FACT_SERI,
                      FACT_CODI, FACT_DAEM, FACT_DARE, FACT_GUEST AS FACT_TITU, FACT_ADDR, FACT_NACI,
                      FACT_NUCO, FACT_TOTA, FACT_SIGN, VEND_PROC, CLIE_PK AS BENTI_PK,
                      SYSDATE AS VEND_DHRE, VEND_LMOD
                      FROM TNHT_VEND WHERE VEND_PK = p_pk;

  -- ticket lines
  OPEN p_cursor16 FOR SELECT 'M' as change_operation,
                      ROWID AS ROW_ID, ARVE_PK, VEND_PK, IPOS_PK, CAJA_PK, ARTG_PK, SEPA_PK,
                      ARVE_ITEM, ARVE_QTDS, ARVE_TOTL, ARVE_VLIQ, ARVE_COST, ARVE_DESC,
                      ARVE_PDES, ARVE_VSDE, ARVE_RECA, UTIL_PK, IVAS_CODI, ARVE_POR1, ARVE_IVAS,
                      IVAS_COD2, ARVE_POR2, ARVE_IVA2, IVAS_COD3, ARVE_POR3, ARVE_IVA3, ARVE_ANUL,
                      ANUL_PAID, ARVE_HPPY, ARVE_IMAR, ARVE_ADIC, ARVE_ACOM, ARVE_OBSV, ARVE_OBSM,
                      CASE WHEN ARVE_IMPR IS NULL THEN 0 ELSE ARVE_IMPR END AS ARVE_IMPR, ARVE_LMOD
                      FROM TNHT_ARVE WHERE VEND_PK = p_pk;

  -- ticket credit cards document
  OPEN p_cursor17 FOR SELECT 'M' as change_operation,
                      TACR.ROWID AS ROW_ID, TACR.TACR_PK AS DOCU_PK, 1 AS TIDO_PK
                      FROM TNHT_TACR TACR
                      INNER JOIN TNHT_PAVE PAVE ON PAVE.TACR_PK = TACR.TACR_PK
                      WHERE PAVE.VEND_PK = p_pk;

  -- ticket credit cards personal document
  OPEN p_cursor18 FOR SELECT null as change_id, 'M' as change_operation, null as change_delete_pk,
                      TACR.ROWID AS ROW_ID, TACR.TACR_PK AS DOCU_PK, TACR.TACR_IDEN AS DOCP_IDEN
                      FROM TNHT_TACR TACR
                      INNER JOIN TNHT_PAVE PAVE ON PAVE.TACR_PK = TACR.TACR_PK
                      WHERE PAVE.VEND_PK = p_pk;

  -- ticket credit cards
  OPEN p_cursor19 FOR SELECT 'M' as change_operation,
                      TACR.ROWID AS ROW_ID, TACR.TACR_PK, TACR.CACR_PK, TACR.CACR_CODE,
                      TACR.CACR_VMES, TACR.CACR_VANO, TACR.TACR_NSU, TACR.TACR_AUTH
                      FROM TNHT_TACR TACR
                      INNER JOIN TNHT_PAVE PAVE ON PAVE.TACR_PK = TACR.TACR_PK
                      WHERE PAVE.VEND_PK = p_pk;

  -- ticket payments
  OPEN p_cursor20 FOR SELECT 'M' as change_operation,
                      ROWID AS ROW_ID, PAVE_PK, VEND_PK, TIRE_PK, FORE_PK, PAVE_VALO, CCCO_PK,
                      TACR_PK, COIN_PK, PAVE_OBSV, PAVE_AUTH
                      FROM TNHT_PAVE WHERE VEND_PK = p_pk;

  -- ticket lookup tables document
  OPEN p_cursor21 FOR SELECT 'M' as change_operation,
                      ROWID AS ROW_ID, LKTB_PK AS DOCU_PK, 48 AS TIDO_PK
                      FROM TNHT_LKTB WHERE VEND_PK = p_pk;

  -- ticket lookup tables
  OPEN p_cursor22 FOR SELECT 'M' as change_operation,
                      LKTB.ROWID AS ROW_ID, LKTB.LKTB_PK, LKTB.LKTB_DATE, LKTB.LKTB_STIM,
                      LKTB.LKTB_SIGN, LKTB.UTIL_PK, LKTB.LKTB_TOTA, LKTB.LKTB_SERI,
                      LKTB.LKTB_CODI, LKTB.VEND_PK, VEND.IPOS_PK, VEND.CAJA_PK
                      FROM TNHT_LKTB LKTB
                      INNER JOIN TNHT_VEND VEND ON VEND.VEND_PK = LKTB.VEND_PK
                      WHERE VEND.VEND_PK = p_pk;

  -- ticket lookup tables details
  OPEN p_cursor23 FOR SELECT 'M' as change_operation,
                      LKDE.ROWID AS ROW_ID, LKDE.LKDE_PK, LKDE.LKTB_PK, LKDE.ARVE_QTDS, LKDE.ARVE_TOTL,
                      LKDE.ARVE_VLIQ, LKDE.ARTG_PK, LKDE.ARVE_ANUL, LKDE.IVAS_CODI, LKDE.ARVE_POR1,
                      LKDE.ARVE_IVAS, LKDE.IVAS_COD2, LKDE.ARVE_POR2, LKDE.ARVE_IVA2, LKDE.IVAS_COD3,
                      LKDE.ARVE_POR3, LKDE.ARVE_IVA3
                      FROM TNHT_LKDE LKDE
                      INNER JOIN TNHT_LKTB LKTB ON LKTB.LKTB_PK = LKDE.LKTB_PK
                      WHERE LKTB.VEND_PK = p_pk;

  -- ticket invoice
  OPEN p_cursor24 FOR SELECT 'P' as change_operation,
                      ROWID AS row_id, vend_pk AS p_vend_pk, 1033 AS p_lang_pk
                      FROM TNHT_VEND
                      WHERE fact_seri is not null AND vend_pk = p_pk;

  -- ticket table
  OPEN p_cursor25 FOR SELECT 'P' as change_operation,
                      mesa.ROWID AS row_id, vend.vend_pk AS p_vend_pk,
                      vend.vend_mesa AS p_mesa_pk, mesa.mesa_desc AS p_mesa_desc
                      FROM TNHT_VEND vend INNER JOIN TNHT_MESA mesa
                      ON mesa.mesa_pk = vend.vend_mesa
                      WHERE vend.vend_mesa is not null AND vend.vend_pk = p_pk;

EXCEPTION
  WHEN NO_DATA_FOUND THEN
    p_pk := null;
END;
/

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.SYNC_VERS = '20170313'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.SYNC_VERS) VALUES (1, '20170313');
END;
/

COMMIT;

ALTER TABLE TCFG_NPOS ADD DATR_DASY CHAR(1) DEFAULT '0' NOT NULL;
COMMENT ON COLUMN TCFG_NPOS.DATR_DASY IS 'Prevenir fecha de trabajo al frente de la fecha del sistema';


BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20170323'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20170323');
END;
/

COMMIT;

alter table tnht_cacr add (cacr_inac char(1) default '0' not null);
comment on column tnht_cacr.cacr_inac is 'Inactivo';


BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20170327'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20170327');
END;
/

COMMIT;

CREATE OR REPLACE PROCEDURE pnht_get_serie (
  p_pk OUT RAW,
  p_cursor0 OUT SYS_REFCURSOR,
  p_cursor1 OUT SYS_REFCURSOR)
IS
BEGIN
  SELECT sedo_pk INTO p_pk FROM (
    SELECT sedo_pk FROM TNHT_SEDO
    WHERE sedo_sync <= SYSTIMESTAMP AND sedo_sync is not null ORDER BY sedo_sync
  ) WHERE ROWNUM <= 1;

  OPEN p_cursor0 FOR SELECT 'M' AS change_operation,
                     ROWID AS row_id, sedo_pk,
                     hote_pk, sedo_ptex, sedo_pnum, sedo_orde, sedo_nufa,
                     sedo_nufi, sedo_dafi, sedo_seac, sedo_saft, sedo_lmod
                     FROM TNHT_SEDO WHERE sedo_pk = p_pk;

EXCEPTION
  WHEN NO_DATA_FOUND THEN
    p_pk := null;
END;
/

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.SYNC_VERS = '20170327'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.SYNC_VERS) VALUES (1, '20170327');
END;
/

COMMIT;




