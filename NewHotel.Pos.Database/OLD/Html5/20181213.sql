﻿BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20181213'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20181213');
END;
/

COMMIT;