﻿ALTER TABLE TNHT_VEND ADD VEND_DISI BLOB;
COMMENT ON COLUMN TNHT_VEND.VEND_DISI IS 'Firma digital del cliente';

ALTER TABLE TNHT_TICK ADD TICK_RCDS CHAR(1 CHAR) DEFAULT '0' NOT NULL;
COMMENT ON COLUMN TNHT_TICK.TICK_RCDS IS 'flag: Pedir firma digital para créditos a habitación (1.si 0.no)';

ALTER TABLE TNHT_FORE ADD FORE_PCDS CHAR(1 CHAR) DEFAULT '0' NOT NULL;
COMMENT ON COLUMN TNHT_FORE.FORE_PCDS IS 'flag: Pedir firma digital para esta forma de pago (1.si 0.no)';

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20220720'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20220720');
END;
/

COMMIT;