﻿create table tnht_artb (artb_pk raw(16) not null, artg_pk raw(16) not null, artb_codi raw(16) not null, artb_cant number default 1 not null, sepa_pk raw(16), artb_maxi number default 1  not null, constraint nht_pk_artb primary key (artb_pk));
comment on table tnht_artb is 'Composicion de productos tables';
comment on column tnht_artb.artg_pk is 'Codigo del producto table, ref. tnht_artg(artg_pk), delete cascade';
comment on column tnht_artb.artb_codi is 'Codigo del producto componente, ref. tnht_artg(artg_pk)';
comment on column tnht_artb.artb_cant is 'Cantidad';
alter table tnht_artb add constraint nht_fk_artg_artb_02 foreign key (artb_codi) references tnht_artg;
alter table tnht_artb add constraint nht_fk_artg_artb_01 foreign key (artg_pk) references tnht_artg on delete cascade;
alter table tnht_artb add constraint nht_fk_sepa_artb foreign key (sepa_pk) references tnht_sepa (sepa_pk);

alter table tnht_artg add artg_tabl number(10);
comment on column tnht_artg.artg_tabl is 'Tipo de table (Table Fijo, Table Dinamico, Table de Rondas)';
alter table tnht_artg add constraint nht_fk_enum_artg_02 foreign key (artg_tabl) references tnht_enum (enum_pk);

create table tnht_tabl (tabl_pk raw(16) not null, arve_pk raw(16) not null, artg_pk raw(16) not null, sepa_pk raw(16), tabl_cant number default 1 not null, tabl_cost number default 0 not null, tabl_prec number default 0 not null, tabl_porc number not null, tabl_adic char(1) default '0' not null, tabl_impr char(1) default '0' not null, tabl_imar char(1) default '0' not null, tabl_lmod  timestamp(6) default current_timestamp not null, tabl_obsv varchar2(255), constraint nht_pk_tabl primary key (tabl_pk));
comment on table tnht_tabl is 'Detalles de los productos de tipo Table de ARVE';
comment on column tnht_tabl.tabl_pk is 'Id del registro';
comment on column tnht_tabl.arve_pk is 'Detalle en el ticket, ref. tnht_arve(arve_pk), delete cascade';
comment on column tnht_tabl.artg_pk is 'Codigo del componente del table, el codigo del propio table seria lo que esta en arve ref. tnht_arve';
comment on column tnht_tabl.sepa_pk is 'Codigo del separador dentro de un table';
comment on column tnht_tabl.tabl_cant is 'Cantidad de producto en el table';
comment on column tnht_tabl.tabl_cost is 'Costo del producto en el ticket';
comment on column tnht_tabl.tabl_prec is 'Precio del producto en el ticket';
comment on column tnht_tabl.tabl_porc is 'Porciento que representa el producto en el precio del table';
comment on column tnht_tabl.tabl_adic is 'Indica si el producto es un adicional';
comment on column tnht_tabl.tabl_impr is 'Indica si ha sido marchado';
comment on column tnht_tabl.tabl_imar is 'Indica si ya fue impreso en la impresora de comanda';
comment on column tnht_tabl.tabl_obsv is 'Observaciones para la marcha de ese producto, estas observaciones seran impresas en el pedido a cocima a continuacion de las preparaciones ';
alter table tnht_tabl add constraint nht_fk_arve_tabl foreign key (arve_pk) references tnht_arve (arve_pk) on delete cascade;
alter table tnht_tabl add constraint nht_fk_sepa_tabl foreign key (sepa_pk) references tnht_sepa;
alter table tnht_tabl add constraint nht_fk_artg_tabl foreign key (artg_pk) references tnht_artg;

create table tnht_tabp (tabp_pk raw(16) not null, tabl_pk raw(16) not null, prep_pk raw(16) not null, constraint nht_pk_tabp primary key (tabp_pk));
comment on table tnht_tabp is 'Tabla de preparaciones asociadas a los componentes de un table';
comment on column tnht_tabp.tabp_pk is 'id del registro';
comment on column tnht_tabp.tabl_pk is 'Referencia a la linea de tabl';
comment on column tnht_tabp.prep_pk is 'Codigo de la preparacion, ref. tnht_prep';
alter table tnht_tabp add constraint nht_fk_tabl_tabp foreign key (tabl_pk) references tnht_tabl;
alter table tnht_tabp add constraint nht_fk_prep_tabp foreign key (prep_pk) references tnht_prep;

create or replace force view vnht_artg
(
   artg_pk,
   hote_pk,
   artg_codi,
   lite_abre,
   lite_desc,
   grup_pk,
   fami_pk,
   sfam_pk,
   sepa_pk,
   artg_step,
   artg_imag,
   artg_colo,
   artg_tipr,
   artg_inac,
   artg_tabl
)
as
   select     artg.artg_pk,
              artg.hote_pk,
              artg.artg_codi,
              artg.lite_abre,
              artg.lite_desc,
              fami.grup_pk,
              artg.fami_pk,
              artg.sfam_pk,
              artg.sepa_pk,
              artg.artg_step,
              artg.artg_imag,
              artg.artg_colo,
              artg.artg_tipr,
              artg.artg_inac,
              artg.artg_tabl
    from      tnht_artg artg, tnht_fami fami
    where     artg.fami_pk = fami.fami_pk;

begin
  merge into tcfg_gene para using dual on (para.gene_pk = 1)
    when matched then update set para.date_vers = '20180403'
    when not matched then insert (para.gene_pk, para.date_vers) values (1, '20180403');
end;
/

commit;

