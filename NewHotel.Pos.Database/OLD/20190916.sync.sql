CREATE OR REPLACE PROCEDURE pnht_get_ticket (
  p_pk OUT RAW,
  p_cursor0  OUT SYS_REFCURSOR,
  p_cursor1  OUT SYS_REFCURSOR,
  p_cursor2  OUT SYS_REFCURSOR,
  p_cursor3  OUT SYS_REFCURSOR,
  p_cursor4  OUT SYS_REFCURSOR,
  p_cursor5  OUT SYS_REFCURSOR,
  p_cursor6  OUT SYS_REFCURSOR,
  p_cursor7  OUT SYS_REFCURSOR,
  p_cursor8  OUT SYS_REFCURSOR,
  p_cursor9  OUT SYS_REFCURSOR,
  p_cursor10 OUT SYS_REFCURSOR,
  p_cursor11 OUT SYS_REFCURSOR,
  p_cursor12 OUT SYS_REFCURSOR,
  p_cursor13 OUT SYS_REFCURSOR,
  p_cursor14 OUT SYS_REFCURSOR,
  p_cursor15 OUT SYS_REFCURSOR,
  p_cursor16 OUT SYS_REFCURSOR,
  p_cursor17 OUT SYS_REFCURSOR,
  p_cursor18 OUT SYS_REFCURSOR,
  p_cursor19 OUT SYS_REFCURSOR,
  p_cursor20 OUT SYS_REFCURSOR,
  p_cursor21 OUT SYS_REFCURSOR,
  p_cursor22 OUT SYS_REFCURSOR,
  p_cursor23 OUT SYS_REFCURSOR,
  p_cursor24 OUT SYS_REFCURSOR,
  p_cursor25 OUT SYS_REFCURSOR,
  p_cursor26 OUT SYS_REFCURSOR,
  p_cursor27 OUT SYS_REFCURSOR,
  p_cursor28 OUT SYS_REFCURSOR,
  p_cursor29 OUT SYS_REFCURSOR,
  p_cursor30 OUT SYS_REFCURSOR)
IS
BEGIN
  SELECT vend_pk INTO p_pk FROM (
    SELECT vend_pk FROM TNHT_VEND
    WHERE vend_sync <= SYSTIMESTAMP AND vend_sync is not null ORDER BY vend_sync
  ) WHERE ROWNUM <= 1;

  -- users
  OPEN p_cursor0 FOR SELECT 'I' AS change_operation,
                     UTIL.ROWID AS ROW_ID, UTIL.UTIL_PK, UTIL.HOTE_PK, UTIL.UTIL_LOGIN,
                     UTIL.UTIL_DESC, UTIL.UTIL_PASS, UTIL.UTIL_CODE, TRUNC(SYSDATE) AS UTIL_DARE,                     
                     UTIL.UTIL_MGR, UTIL.UTIL_INTE, '1' AS UTIL_INAC, UTIL.UTIL_LMOD
                     FROM (
                     SELECT VEND.UTIL_PK
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_UTIL UTIL ON UTIL.UTIL_PK = VEND.UTIL_PK
                     WHERE VEND.VEND_PK = p_pk
                     UNION
                     SELECT VEND.UTIL_OPEN
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_UTIL UTIL ON UTIL.UTIL_PK = VEND.UTIL_OPEN
                     WHERE VEND.VEND_PK = p_pk
                     UNION
                     SELECT VEND.ANUL_UTIL
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_UTIL UTIL ON UTIL.UTIL_PK = VEND.ANUL_UTIL
                     WHERE VEND.ANUL_UTIL IS NOT NULL AND VEND.VEND_PK = p_pk
                     UNION
                     SELECT ARVE.UTIL_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_UTIL UTIL ON UTIL.UTIL_PK = ARVE.UTIL_PK
                     WHERE ARVE.VEND_PK = p_pk
                     ) TEMP INNER JOIN TNHT_UTIL UTIL
                     ON UTIL.UTIL_PK = TEMP.UTIL_PK;

  -- translations
  OPEN p_cursor1 FOR SELECT 'I' AS change_operation,
                     LITE.ROWID AS ROW_ID, LITE.LITE_PK
                     FROM (
                     SELECT GRSE.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_GRUP GRUP ON GRUP.GRUP_PK = FAMI.GRUP_PK
                     INNER JOIN TNHT_GRSE GRSE ON GRSE.GRSE_PK = GRUP.GRSE_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_COIN COIN ON COIN.COIN_PK = VEND.COIN_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = COIN.COIN_PK
                     WHERE VEND.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = FAMI.GRUP_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = ARTG.FAMI_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = ARTG.SFAM_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT ARTG.LITE_ABRE
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT ARTG.LITE_DESC
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT FORE.LITE_ABRE
                     FROM TNHT_FORE FORE 
                     INNER JOIN TNHT_PAVE PAVE ON PAVE.FORE_PK = FORE.FORE_PK
                     WHERE PAVE.VEND_PK = p_pk
                     UNION
                     SELECT FORE.LITE_DESC
                     FROM TNHT_FORE FORE
                     INNER JOIN TNHT_PAVE PAVE ON PAVE.FORE_PK = FORE.FORE_PK
                     WHERE PAVE.VEND_PK = p_pk
                     UNION
                     SELECT TIDE.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_TIDE TIDE ON TIDE.TIDE_PK = ARVE.TIDE_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT MTCO.LITE_PK
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_MTCO MTCO ON MTCO.MTCO_PK = VEND.MTCO_PK
                     WHERE VEND.VEND_PK = p_pk
                     UNION
                     SELECT SEPA.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_SEPA SEPA ON SEPA.SEPA_PK = ARVE.SEPA_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT SEPA.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_TABL TABL ON TABL.ARVE_PK = ARVE.ARVE_PK
                     INNER JOIN TNHT_SEPA SEPA ON SEPA.SEPA_PK = TABL.SEPA_PK
                     WHERE ARVE.VEND_PK = p_pk
                     ) TEMP INNER JOIN TNHT_LITE LITE
                     ON LITE.LITE_PK = TEMP.LITE_PK;

  -- translation descriptions
  OPEN p_cursor2 FOR SELECT 'I' AS change_operation,
                     MULT.ROWID AS ROW_ID, MULT.LITE_PK, MULT.LANG_PK, MULT.MULT_DESC
                     FROM (
                     SELECT GRSE.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_GRUP GRUP ON GRUP.GRUP_PK = FAMI.GRUP_PK
                     INNER JOIN TNHT_GRSE GRSE ON GRSE.GRSE_PK = GRUP.GRSE_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_COIN COIN ON COIN.COIN_PK = VEND.COIN_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = COIN.COIN_PK
                     WHERE VEND.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = FAMI.GRUP_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = ARTG.FAMI_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = ARTG.SFAM_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT ARTG.LITE_ABRE
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT ARTG.LITE_DESC
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT FORE.LITE_ABRE
                     FROM TNHT_FORE FORE 
                     INNER JOIN TNHT_PAVE PAVE ON PAVE.FORE_PK = FORE.FORE_PK
                     WHERE PAVE.VEND_PK = p_pk
                     UNION
                     SELECT FORE.LITE_DESC
                     FROM TNHT_FORE FORE
                     INNER JOIN TNHT_PAVE PAVE ON PAVE.FORE_PK = FORE.FORE_PK
                     WHERE PAVE.VEND_PK = p_pk
                     UNION
                     SELECT TIDE.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_TIDE TIDE ON TIDE.TIDE_PK = ARVE.TIDE_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT MTCO.LITE_PK
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_MTCO MTCO ON MTCO.MTCO_PK = VEND.MTCO_PK
                     WHERE VEND.VEND_PK = p_pk
                     UNION
                     SELECT SEPA.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_SEPA SEPA ON SEPA.SEPA_PK = ARVE.SEPA_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT SEPA.LITE_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_TABL TABL ON TABL.ARVE_PK = ARVE.ARVE_PK
                     INNER JOIN TNHT_SEPA SEPA ON SEPA.SEPA_PK = TABL.SEPA_PK
                     WHERE ARVE.VEND_PK = p_pk
                     ) TEMP INNER JOIN TNHT_MULT MULT
                     ON MULT.LITE_PK = TEMP.LITE_PK;

  -- service groups
  OPEN p_cursor3 FOR SELECT 'I' AS change_operation,
                     GRSE.* FROM (
                     SELECT GRSE.ROWID AS ROW_ID, GRSE.GRSE_PK, GRSE.HOTE_PK,
                     GRSE.LITE_PK, '1' AS GRSE_INAC, GRSE.GRSE_LMOD
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_GRUP GRUP ON GRUP.GRUP_PK = FAMI.GRUP_PK
                     INNER JOIN TNHT_GRSE GRSE ON GRSE.GRSE_PK = GRUP.GRSE_PK
                     WHERE ARVE.VEND_PK = p_pk
                     ) GRSE;

  -- grups, families, subfamilies
  OPEN p_cursor4 FOR SELECT 'I' AS change_operation,
                     CLAS.* FROM (
                     SELECT CLAS.ROWID AS ROW_ID, CLAS.CLAS_PK, CLAS.HOTE_PK,
                     CLAS.LITE_PK, CLAS.CLAS_LMOD
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = VEND.COIN_PK
                     WHERE VEND.COIN_PK IS NOT NULL AND VEND.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.ROWID AS ROW_ID, CLAS.CLAS_PK, CLAS.HOTE_PK,
                     CLAS.LITE_PK, CLAS.CLAS_LMOD                    
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = ARTG.FAMI_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.ROWID AS ROW_ID, CLAS.CLAS_PK, CLAS.HOTE_PK,
                     CLAS.LITE_PK, CLAS.CLAS_LMOD
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = FAMI.GRUP_PK
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT CLAS.ROWID AS ROW_ID, CLAS.CLAS_PK, CLAS.HOTE_PK,
                     CLAS.LITE_PK, CLAS.CLAS_LMOD
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = ARTG.SFAM_PK
                     WHERE ARTG.SFAM_PK IS NOT NULL AND ARVE.VEND_PK = p_pk
                     ) CLAS;
 
  -- internal comsumptions
  OPEN p_cursor5 FOR SELECT 'I' AS change_operation,
                     COIN.ROWID AS ROW_ID, COIN.COIN_PK,
                     COIN.COIN_INVI, '1' AS COIN_INAC
                     FROM TNHT_VEND VEND
                     INNER JOIN TNHT_COIN COIN ON COIN.COIN_PK = VEND.COIN_PK
                     WHERE VEND.COIN_PK IS NOT NULL AND VEND.VEND_PK = p_pk;

  -- grups
  OPEN p_cursor6 FOR SELECT 'I' AS change_operation,
                     GRUP.ROWID AS ROW_ID, GRUP.GRUP_PK,
                     GRUP.GRUP_CHEQ, GRUP.GRUP_RECA, GRUP.GRSE_PK, GRUP.CATS_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     INNER JOIN TNHT_GRUP GRUP ON GRUP.GRUP_PK = FAMI.GRUP_PK
                     WHERE ARVE.VEND_PK = p_pk;

  -- families
  OPEN p_cursor7 FOR SELECT 'I' AS change_operation,
                     FAMI.ROWID AS ROW_ID, FAMI.FAMI_PK, FAMI.GRUP_PK                    
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                     WHERE ARVE.VEND_PK = p_pk;

  -- subfamilies
  OPEN p_cursor8 FOR SELECT 'I' AS change_operation,
                     SFAM.ROWID AS ROW_ID, SFAM.SFAM_PK, SFAM.FAMI_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                     INNER JOIN TNHT_SFAM SFAM ON SFAM.SFAM_PK = ARTG.SFAM_PK
                     WHERE ARTG.SFAM_PK IS NOT NULL AND ARVE.VEND_PK = p_pk;

  -- separators
  OPEN p_cursor9 FOR SELECT 'I' AS CHANGE_OPERATION,
                     SEPA.ROWID AS ROW_ID, SEPA.SEPA_PK, SEPA.HOTE_PK,
                     SEPA.LITE_PK, SEPA.SEPA_ORDE, SEPA.SEPA_MAXP,
                     SEPA.SEPA_ACUM, SEPA.SEPA_TICK, SEPA.SEPA_LMOD
                     FROM (
                     SELECT ARVE.SEPA_PK
                     FROM TNHT_ARVE ARVE
                     WHERE ARVE.VEND_PK = p_pk
                     UNION
                     SELECT TABL.SEPA_PK
                     FROM TNHT_ARVE ARVE
                     INNER JOIN TNHT_TABL TABL ON TABL.ARVE_PK = ARVE.ARVE_PK
                     WHERE ARVE.VEND_PK = p_pk
                     ) TEMP INNER JOIN TNHT_SEPA SEPA
                     ON SEPA.SEPA_PK = TEMP.SEPA_PK;

  -- products service
  OPEN p_cursor10 FOR SELECT 'I' AS change_operation,
                      ARTG.ROWID AS ROW_ID, ARTG.ARTG_PK AS SERV_PK, ARTG.HOTE_PK, ARTG.LITE_ABRE, ARTG.LITE_DESC,
                      ARTG.ARTG_ROUN AS SERV_ROUN, ARTG.ARTG_LOCA AS SERV_LOCA, ARTG.ARTG_UNME AS SERV_UNME,
                      ARTG.ARTG_CSTI AS SERV_CSTI, ARTG.ARTG_CNCM AS SERV_CNCM, ARTG.ARTG_CFOP AS SERV_CFOP,
                      ARTG.ARTG_CSTP AS SERV_CSTP, ARTG.ARTG_CSTC AS SERV_CSTC, GRUP.GRSE_PK, GRUP.CATS_PK,
                      '1' AS SERV_INAC, ARTG.ARTG_LMOD AS SERV_LMOD
                      FROM TNHT_ARVE ARVE
                      INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                      INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                      INNER JOIN TNHT_GRUP GRUP ON GRUP.GRUP_PK = FAMI.GRUP_PK
                      WHERE ARVE.VEND_PK = p_pk;

  -- products base
  OPEN p_cursor11 FOR SELECT 'I' AS change_operation,
                      ARTG.ROWID AS ROW_ID, ARTG.ARTG_PK AS BARTG_PK,
                      ARTG.ARTG_CODI, ARTG.FAMI_PK, ARTG.SFAM_PK 
                      FROM TNHT_ARVE ARVE
                      INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                      WHERE ARVE.VEND_PK = p_pk;
 
  -- products
  OPEN p_cursor12 FOR SELECT 'I' AS change_operation,
                      ARTG.ROWID AS ROW_ID, ARTG.ARTG_PK, ARTG.ARTG_STEP,
                      ARTG.ARTG_TIPR, ARTG.ARTG_COLO, ARTG.ARTG_TABL
                      FROM TNHT_ARVE ARVE
                      INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                      WHERE ARVE.VEND_PK = p_pk;

  -- preparations
  OPEN p_cursor13 FOR SELECT 'I' AS change_operation,
                      PREP.ROWID AS ROW_ID, PREP.PREP_PK, PREP.HOTE_PK,
                      PREP.LITE_PK, PREP.PREP_LMOD
                      FROM TNHT_ARVE ARVE
                      INNER JOIN TNHT_TABL TABL ON TABL.ARVE_PK = ARVE.ARVE_PK
                      INNER JOIN TNHT_TABP TABP ON TABP.TABL_PK = TABL.TABL_PK
                      INNER JOIN TNHT_PREP PREP ON PREP.PREP_PK = TABP.PREP_PK
                      WHERE ARVE.VEND_PK = p_pk;

  -- discount types
  OPEN p_cursor14 FOR SELECT 'I' AS change_operation,
                      TIDE.ROWID AS ROW_ID, TIDE.TIDE_PK, TIDE.LITE_PK, TIDE.HOTE_PK, TIDE.APPL_PK,
                      TIDE.TIDE_FIXE, TIDE.TIDE_VMIN, TIDE.TIDE_VMAX, '1' AS TIDE_INAC, TIDE.TIDE_LMOD                     
                      FROM TNHT_ARVE ARVE
                      INNER JOIN TNHT_TIDE TIDE ON TIDE.TIDE_PK = ARVE.TIDE_PK
                      WHERE ARVE.TIDE_PK IS NOT NULL AND ARVE.VEND_PK = p_pk;

  -- cancellation reason
  OPEN p_cursor15 FOR SELECT 'I' AS change_operation,
                      MTCO.ROWID AS ROW_ID, MTCO.MTCO_PK, MTCO.HOTE_PK, MTCO.LITE_PK,
                      MTCO.MTCO_TICA, '1' AS MTCO_INAC, MTCO.MTCO_LMOD                     
                      FROM TNHT_VEND VEND
                      INNER JOIN TNHT_MTCO MTCO ON MTCO.MTCO_PK = VEND.MTCO_PK
                      WHERE VEND.MTCO_PK IS NOT NULL AND VEND.VEND_PK = p_pk;

  -- payment forms
  OPEN p_cursor16 FOR SELECT 'I' AS CHANGE_OPERATION,
                      FORE.ROWID AS ROW_ID, FORE.FORE_PK, FORE.HOTE_PK,
                      FORE.LITE_ABRE, FORE.LITE_DESC, FORE.UNMO_PK, FORE.FORE_CASH,
                      FORE.FORE_CACR, FORE.REPO_CASH, FORE.FORE_ORDE, FORE.FORE_CAUX,
                      FORE.FORE_CRED, FORE.FORE_DEBI, '1' AS FORE_INAC, FORE.FORE_LMOD
                      FROM TNHT_FORE FORE
                      INNER JOIN TNHT_PAVE PAVE ON PAVE.FORE_PK = FORE.FORE_PK
                      WHERE PAVE.VEND_PK = p_pk;

  -- ticket
  OPEN p_cursor17 FOR SELECT 'M' as change_operation,
                      VEND.ROWID AS ROW_ID, VEND.VEND_PK, VEND.HOTE_PK, VEND.IPOS_PK, VEND.CAJA_PK,
                      NULL AS VEND_MESA, VEND.UNMO_PK, VEND.CAJA_PAVE, VEND.VEND_SERI, VEND.VEND_CODI,
                      VEND.VEND_DATI, VEND.VEND_HORI, VEND.VEND_DATF, VEND.VEND_HORF, VEND.ESIM_PK,
                      VEND.VEND_NPAX, VEND.VEND_COMP, VEND.UTIL_PK, VEND.COIN_PK, VEND.CCCO_PK,
                      VEND.TURN_CODI, VEND.VEND_DESC, VEND.VEND_RECA, VEND.VEND_OBSE, VEND.DUTY_PK,
                      VEND.VEND_IVIN, VEND.VEND_SIGN, VEND.FACT_CCCO, VEND.FACT_SERI, VEND.FACT_CODI,
                      VEND.FACT_DAEM, VEND.FACT_DARE, VEND.FACT_GUEST AS FACT_TITU, VEND.FACT_ADDR,
                      VEND.FACT_MAIL, VEND.FACT_NACI, VEND.FACT_NUCO, VEND.FACT_TIDF, VEND.FACT_TOTA,
                      VEND.FACT_SIGN, VEND.VEND_NOTA, VEND.CLIE_PK AS BENTI_PK, VEND.FISC_PEND,
                      VEND.VEND_IFNC, VEND.VEND_IFFA, VEND.VEND_NFFA, VEND.VEND_NFNC,
                      MESA.MESA_DESC, SYSDATE AS VEND_DHRE, VEND.LITR_PK, VEND.VEND_LMOD
                      FROM TNHT_VEND VEND LEFT JOIN TNHT_MESA MESA ON MESA.MESA_PK = VEND.VEND_MESA 
                      WHERE VEND.VEND_PK = p_pk;

  -- ticket lines
  OPEN p_cursor18 FOR SELECT 'M' as change_operation,
                      ROWID AS ROW_ID, ARVE_PK, VEND_PK, IPOS_PK, CAJA_PK, ARTG_PK, SEPA_PK,
                      ARVE_ITEM, ARVE_QTDS, ARVE_TOTL, ARVE_VLIQ, ARVE_COST, ARVE_DESC, TIDE_PK,
                      ARVE_PDES, ARVE_VSDE, ARVE_RECA, UTIL_PK, IVAS_CODI, ARVE_POR1, ARVE_IVAS,
                      IVAS_COD2, ARVE_POR2, ARVE_IVA2, IVAS_COD3, ARVE_POR3, ARVE_IVA3, ARVE_ANUL,
                      ANUL_PAID, ARVE_HPPY, ARVE_IMAR, ARVE_ADIC, ARVE_ACOM, ARVE_OBSV, ARVE_OBSM,
                      CASE WHEN ARVE_IMPR IS NULL THEN 0 ELSE ARVE_IMPR END AS ARVE_IMPR, ARVE_TIPL,
                      ARVE_LMOD
                      FROM TNHT_ARVE WHERE VEND_PK = p_pk;

  -- ticket credit cards document
  OPEN p_cursor19 FOR SELECT 'M' as change_operation,
                      TACR.ROWID AS ROW_ID, TACR.TACR_PK AS DOCU_PK, 1 AS TIDO_PK
                      FROM TNHT_TACR TACR
                      INNER JOIN TNHT_PAVE PAVE ON PAVE.TACR_PK = TACR.TACR_PK
                      WHERE PAVE.VEND_PK = p_pk;

  -- ticket credit cards personal document
  OPEN p_cursor20 FOR SELECT null as change_id, 'M' as change_operation, null as change_delete_pk,
                      TACR.ROWID AS ROW_ID, TACR.TACR_PK AS DOCU_PK, TACR.TACR_IDEN AS DOCP_IDEN
                      FROM TNHT_TACR TACR
                      INNER JOIN TNHT_PAVE PAVE ON PAVE.TACR_PK = TACR.TACR_PK
                      WHERE PAVE.VEND_PK = p_pk;

  -- ticket credit cards
  OPEN p_cursor21 FOR SELECT 'M' as change_operation,
                      TACR.ROWID AS ROW_ID, TACR.TACR_PK, TACR.CACR_PK, TACR.CACR_CODE,
                      TACR.CACR_VMES, TACR.CACR_VANO, TACR.TACR_NSU, TACR.TACR_AUTH
                      FROM TNHT_TACR TACR
                      INNER JOIN TNHT_PAVE PAVE ON PAVE.TACR_PK = TACR.TACR_PK
                      WHERE PAVE.VEND_PK = p_pk;

  -- ticket payments
  OPEN p_cursor22 FOR SELECT 'M' as change_operation,
                      ROWID AS ROW_ID, PAVE_PK, VEND_PK, TIRE_PK, FORE_PK,
                      PAVE_VALO, CCCO_PK, TACR_PK, COIN_PK, PAVE_OBSV,
                      PAVE_AUTH, UNMO_PK, CAMB_MULT, CAMB_ONLINE
                      FROM TNHT_PAVE WHERE VEND_PK = p_pk;

  -- ticket lookup tables document
  OPEN p_cursor23 FOR SELECT 'M' as change_operation,
                      ROWID AS ROW_ID, LKTB_PK AS DOCU_PK, 48 AS TIDO_PK
                      FROM TNHT_LKTB WHERE VEND_PK = p_pk;

  -- ticket lookup tables
  OPEN p_cursor24 FOR SELECT 'M' as change_operation,
                      LKTB.ROWID AS ROW_ID, LKTB.LKTB_PK, LKTB.LKTB_DATE, LKTB.LKTB_STIM,
                      LKTB.LKTB_SIGN, LKTB.UTIL_PK, LKTB.LKTB_TOTA, LKTB.LKTB_SERI,
                      LKTB.LKTB_CODI, LKTB.VEND_PK, VEND.IPOS_PK, VEND.CAJA_PK
                      FROM TNHT_LKTB LKTB
                      INNER JOIN TNHT_VEND VEND ON VEND.VEND_PK = LKTB.VEND_PK
                      WHERE VEND.VEND_PK = p_pk;

  -- ticket lookup tables details
  OPEN p_cursor25 FOR SELECT 'M' as change_operation,
                      LKDE.ROWID AS ROW_ID, LKDE.LKDE_PK, LKDE.LKTB_PK, LKDE.ARVE_QTDS, LKDE.ARVE_TOTL,
                      LKDE.ARVE_VLIQ, LKDE.ARTG_PK, LKDE.ARVE_ANUL, LKDE.IVAS_CODI, LKDE.ARVE_POR1,
                      LKDE.ARVE_IVAS, LKDE.IVAS_COD2, LKDE.ARVE_POR2, LKDE.ARVE_IVA2, LKDE.IVAS_COD3,
                      LKDE.ARVE_POR3, LKDE.ARVE_IVA3
                      FROM TNHT_LKDE LKDE
                      INNER JOIN TNHT_LKTB LKTB ON LKTB.LKTB_PK = LKDE.LKTB_PK
                      WHERE LKTB.VEND_PK = p_pk;

  -- ticket line tables
  OPEN p_cursor26 FOR SELECT 'M' AS CHANGE_OPERATION,
                      TABL.ROWID AS ROW_ID, TABL.TABL_PK,
                      TABL.ARVE_PK, TABL.ARTG_PK, TABL.SEPA_PK,
                      TABL.TABL_CANT, TABL.TABL_COST, TABL.TABL_PREC,
                      TABL.TABL_PORC, TABL.TABL_ADIC, TABL.TABL_IMPR,
                      TABL.TABL_IMAR, TABL.TABL_OBSV, TABL.TABL_LMOD
                      FROM TNHT_ARVE ARVE
                      INNER JOIN TNHT_TABL TABL ON TABL.ARVE_PK = ARVE.ARVE_PK
                      WHERE ARVE.VEND_PK = p_pk;

  -- ticket line table preparations
  OPEN p_cursor27 FOR SELECT 'M' AS CHANGE_OPERATION,
                      TABP.ROWID AS ROW_ID, TABP.TABP_PK,
                      TABP.TABL_PK, TABP.PREP_PK
                      FROM TNHT_ARVE ARVE
                      INNER JOIN TNHT_TABL TABL ON TABL.ARVE_PK = ARVE.ARVE_PK
                      INNER JOIN TNHT_TABP TABP ON TABP.TABL_PK = TABL.TABL_PK
                      WHERE ARVE.VEND_PK = p_pk;

  -- ticket transfer + acepted
  OPEN p_cursor28 FOR SELECT 'P' AS CHANGE_OPERATION,
                      ROWID AS ROW_ID, TITR_PK AS P_TITR_PK,
                      IPOS_ORIG AS P_IPOS_ORIG, CAJA_ORIG AS P_CAJA_ORIG,
                      IPOS_DEST AS P_IPOS_DEST, CAJA_DEST AS P_CAJA_DEST,
                      VEND_TRAN AS P_VEND_TRAN, UTIL_TRAN AS P_UTIL_TRAN,
                      TRAN_DATE AS P_TRAN_DATE, TRAN_TIME AS P_TRAN_TIME,
                      VEND_ACEP AS P_VEND_ACEP, UTIL_ACEP AS P_UTIL_ACEP,
                      ACEP_DATE AS P_ACEP_DATE, ACEP_TIME AS P_ACEP_TIME
                      FROM TNHT_TITR WHERE VEND_ACEP = p_pk;

  -- ticket invoice
  OPEN p_cursor29 FOR SELECT 'P' AS CHANGE_OPERATION,
                      ROWID AS ROW_ID, VEND_PK AS P_VEND_PK, 1033 AS P_LANG_PK
                      FROM TNHT_VEND
                      WHERE FACT_SERI IS NOT NULL AND FACT_CODI IS NOT NULL
                      AND VEND_PK = p_pk;

  -- ticket canceled
  OPEN p_cursor30 FOR SELECT 'P' AS CHANGE_OPERATION,
                      ROWID AS ROW_ID, VEND_PK AS P_VEND_PK, MTCO_PK AS P_MTCO_PK,
                      ANUL_DAAN AS P_ANUL_DAAN, ANUL_DARE AS P_ANUL_DARE,
                      ANUL_UTIL AS P_UTIL_PK, ANUL_OBSE AS P_ANUL_OBSE
                      FROM TNHT_VEND
                      WHERE MTCO_PK IS NOT NULL AND VEND_PK = p_pk;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    p_pk := null;
END;
/

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.SYNC_VERS = '20190916'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.SYNC_VERS) VALUES (1, '20190916');
END;
/

COMMIT;