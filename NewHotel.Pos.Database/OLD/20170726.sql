create or replace trigger tnht_vend_update_seri for update on tnht_vend compound trigger
    type t_vend_row is table of tnht_vend%rowtype;
    list_t_vend_row t_vend_row := t_vend_row();
    serie_unique number(1);
    serie_used number(1);
    serie_previo number(1);
after each row is
begin
    if (:new.vend_seri is not null and :old.vend_seri is null) 
    then
        list_t_vend_row.EXTEND;
        list_t_vend_row(list_t_vend_row.LAST).vend_pk := :new.vend_pk;
        list_t_vend_row(list_t_vend_row.LAST).vend_seri := :new.vend_seri;
        list_t_vend_row(list_t_vend_row.LAST).vend_codi := :new.vend_codi;
    end if;
end after each row;

after statement is
begin
    for r in 1..list_t_vend_row.COUNT
    loop
        --ensure serie is unique
        select  case when exists (select 1 from tnht_vend where vend_seri = list_t_vend_row(r).vend_seri and vend_codi = list_t_vend_row(r).vend_codi and vend_pk <> list_t_vend_row(r).vend_pk) then 1 else 0 end
                into serie_unique
        from    dual;
        if (serie_unique = 1) then RAISE_APPLICATION_ERROR(-20002, 'Serie is not unique');
        end if;
        --ensure previous value exists (unless is first one)
        --there is at least one line using this serie already
        select  case when exists (select 1 from tnht_vend where vend_seri = list_t_vend_row(r).vend_seri and vend_pk <> list_t_vend_row(r).vend_pk) then 1 else 0 end
                into serie_used
        from    dual;
        if (serie_used = 1)
        then
            --needs to have the previous number already in place
            select  case when exists (select 1 from tnht_vend where vend_seri = list_t_vend_row(r).vend_seri and vend_codi = list_t_vend_row(r).vend_codi - 1 and vend_pk <> list_t_vend_row(r).vend_pk) then 1 else 0 end
                    into serie_previo
            from    dual;
            
            if (serie_previo = 0) then RAISE_APPLICATION_ERROR(-20003, 'Serie consecutive not valid');
            end if;
        end if; 
    end loop;
    list_t_vend_row.DELETE;
end after statement; 
end;
/

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20170726'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20170726');
END;
/

COMMIT;