ALTER TABLE TNHT_ARTG ADD ARTG_CEST VARCHAR2(255);
COMMENT ON COLUMN TNHT_ARTG.ARTG_CEST IS 'Codigo Especificador de Sustitucion Tributaria';

create or replace view vnht_nfce_artigos
(
    venda_chave,
    venda_codigo,
    venda_serie,
    artigo_codigo,
    artigo_descripcao,
    artigo_ncm,
    artigo_cfop,
    artigo_unidade,
    artigo_cantidade,
    artigo_valor_unitario,
    artigo_anulado,
    artigo_valor_total,
    artigo_valor_desconto,
    artigo_cest,
    icms_cst,
    icms_base,
    icms_aliquota,
    icms_valor,
    pis_cst,
    pis_base,
    pis_aliquota,
    pis_valor,
    cofins_cst,
    cofins_base,
    cofins_aliquota,
    cofins_valor
)
as
(
    select       vend.vend_pk, vend.vend_codi, vend.vend_seri, artg.artg_codi, (select mult_desc from vnht_mult where lite_pk = artg.lite_desc and lang_pk = 1046) as artg_desc, artg.artg_cncm,
                 case
                    --no client selected, local sale 
                 when vend.clie_pk is null or length(artg.artg_cfop) <> 4 then artg.artg_cfop 
                 else 
                 (
                     case
                         --foreign
                         when clie.naci_pk <> 'BR' then '7' || substr(artg.artg_cfop, 1, 3)
                         else
                         (
                             case
                                 --inter state
                                 when hote.home_dist <> clie.fisc_dist then '6' || substr(artg.artg_cfop, 1, 3)
                                 --same state
                                 else artg.artg_cfop
                             end
                         )
                     end
                 )
                 end artg_cfop, artg.artg_unme, arve.arve_qtds, case when arve_qtds = 0 then 0 else arve.arve_vsde / arve.arve_qtds end arve_qtds, case when arve_anul in (0, 2) then 0 else 1 end arve_anul,
                 arve.arve_totl, arve_desc, artg.artg_cest, artg.artg_csti, arve.arve_totl, arve.arve_por1, arve.arve_ivas, artg.artg_cstp, arve.arve_totl, arve.arve_por2, arve.arve_iva2, artg.artg_cstc, arve.arve_totl, arve.arve_por3, arve_iva3
    from        tnht_artg artg, tnht_arve arve, tnht_vend vend, tnht_hote hote, tnht_clie clie
    where       artg.artg_pk = arve.artg_pk
    and         arve.vend_pk = vend.vend_pk
    and         vend.hote_pk = hote.hote_pk
    and         vend.clie_pk = clie.clie_pk (+)
)   order by    arve_item asc
;


begin
  merge into tcfg_gene para using dual on (para.gene_pk = 1)
    when matched then update set para.date_vers = '20170126'
    when not matched then insert (para.gene_pk, para.date_vers) values (1, '20170126');
end;
/

commit;