﻿CREATE OR REPLACE PROCEDURE PNHT_SYNC_UPDATE (
  p_index IN INTEGER,
  p_pk IN RAW,
  p_interval IN INTERVAL DAY TO SECOND := null)
IS
  v_timestamp TIMESTAMP(6) := null;
BEGIN
  IF p_interval is not null THEN
    v_timestamp := SYSTIMESTAMP + p_interval;
  END IF;
 
  CASE
    WHEN p_index = 0 THEN
      UPDATE TNHT_IPOS SET ipos_sync = v_timestamp WHERE ipos_pk = p_pk;
    WHEN p_index = 1 THEN
      UPDATE TNHT_SEDO SET sedo_sync = v_timestamp WHERE sedo_pk = p_pk;
    WHEN p_index = 2 THEN
      UPDATE TNHT_VEND SET vend_sync = v_timestamp WHERE vend_pk = p_pk;
    WHEN p_index = 3 THEN
      UPDATE TNHT_MESA SET mesa_sync = v_timestamp WHERE mesa_pk = p_pk;
    ELSE
      NULL;
  END CASE;
END;
/

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20220607'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20220607');
END;
/

COMMIT;