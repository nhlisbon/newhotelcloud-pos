﻿ALTER TABLE TNHT_VEND ADD VEND_QRCO BLOB;
COMMENT ON COLUMN TNHT_VEND.VEND_QRCO IS 'Código QR del ticket';

ALTER TABLE TNHT_LKTB ADD LKTB_COVA VARCHAR2(255);
COMMENT ON COLUMN TNHT_LKTB.LKTB_COVA IS 'Código de validación de la consulta de mesa: SAFT-PT ATCUD';
ALTER TABLE TNHT_LKTB ADD LKTB_QRCO BLOB;
COMMENT ON COLUMN TNHT_LKTB.LKTB_QRCO IS 'Código QR de la consulta de mesa';

BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20221216'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20221216');
END;
/

COMMIT;