﻿alter table tnht_cacr add (cacr_inac char(1) default '0' not null);
comment on column tnht_cacr.cacr_inac is 'Inactivo';


BEGIN
  MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.DATE_VERS = '20170327'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20170327');
END;
/

COMMIT;