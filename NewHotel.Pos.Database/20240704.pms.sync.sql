CREATE OR REPLACE TRIGGER change_notification_tnht_hunmo
    FOR INSERT OR UPDATE OR DELETE ON TNHT_HUNMO
    COMPOUND TRIGGER
    TYPE t_change_notification IS TABLE OF change_notification%ROWTYPE;
    l_change_notification t_change_notification := t_change_notification();
    v_change_id INTEGER;
AFTER EACH ROW IS
BEGIN
    SELECT change_notification_id.NEXTVAL INTO v_change_id FROM DUAL;
    l_change_notification.EXTEND;
    l_change_notification(l_change_notification.LAST).change_id := v_change_id;
    l_change_notification(l_change_notification.LAST).change_table_name := 'TNHT_HUNMO';
    IF DELETING THEN
        l_change_notification(l_change_notification.LAST).change_operation := 'D';
        l_change_notification(l_change_notification.LAST).change_row_id := :OLD.ROWID;
        l_change_notification(l_change_notification.LAST).change_delete_pk := :OLD.hunmo_pk;
    ELSE
        l_change_notification(l_change_notification.LAST).change_operation := 'M';
        l_change_notification(l_change_notification.LAST).change_row_id := :NEW.ROWID;
    END IF;
END AFTER EACH ROW;
    AFTER STATEMENT IS
    BEGIN
        FORALL v_index IN l_change_notification.FIRST..l_change_notification.LAST
            INSERT INTO change_notification (change_id, change_table_name, change_operation, change_row_id, change_delete_pk)
            VALUES (l_change_notification(v_index).change_id, l_change_notification(v_index).change_table_name,
                    l_change_notification(v_index).change_operation, l_change_notification(v_index).change_row_id,
                    l_change_notification(v_index).change_delete_pk);
        l_change_notification.DELETE;
    END AFTER STATEMENT;
    END;
/


create or replace view SYNC_TNHT_HUNMO as
SELECT HUNMO.ROWID AS ROW_ID,
       HUNMO.HUNMO_PK,
       HUNMO.HOTE_PK,
       HUNMO.UNMO_PK,
       HUNMO.UNMO_SYMB,
       HUNMO.UNMO_CAJA,
       HUNMO.CAMB_MULT,
       CASE WHEN HOTE.UNMO_BASE = HUNMO.HUNMO_PK THEN  '1' ELSE '0' END AS UNMO_BASE
FROM   TNHT_HUNMO HUNMO
           INNER JOIN TCFG_HOTE HOTE ON HOTE.HOTE_PK = HUNMO.HOTE_PK
/


CREATE OR REPLACE TRIGGER change_notification_tnht_camb
    FOR INSERT OR UPDATE OR DELETE ON TNHT_CAMB
    COMPOUND TRIGGER
    TYPE t_change_notification IS TABLE OF change_notification%ROWTYPE;
    l_change_notification t_change_notification := t_change_notification();
    v_change_id INTEGER;
AFTER EACH ROW IS
BEGIN
    SELECT change_notification_id.NEXTVAL INTO v_change_id FROM DUAL;
    l_change_notification.EXTEND;
    l_change_notification(l_change_notification.LAST).change_id := v_change_id;
    l_change_notification(l_change_notification.LAST).change_table_name := 'TNHT_CAMB';
    IF DELETING THEN
        l_change_notification(l_change_notification.LAST).change_operation := 'D';
        l_change_notification(l_change_notification.LAST).change_row_id := :OLD.ROWID;
        l_change_notification(l_change_notification.LAST).change_delete_pk := :OLD.camb_pk;
    ELSE
        l_change_notification(l_change_notification.LAST).change_operation := 'M';
        l_change_notification(l_change_notification.LAST).change_row_id := :NEW.ROWID;
    END IF;
END AFTER EACH ROW;
    AFTER STATEMENT IS
    BEGIN
        FORALL v_index IN l_change_notification.FIRST..l_change_notification.LAST
            INSERT INTO change_notification (change_id, change_table_name, change_operation, change_row_id, change_delete_pk)
            VALUES (l_change_notification(v_index).change_id, l_change_notification(v_index).change_table_name,
                    l_change_notification(v_index).change_operation, l_change_notification(v_index).change_row_id,
                    l_change_notification(v_index).change_delete_pk);
        l_change_notification.DELETE;
    END AFTER STATEMENT;
    END;
/


create or replace view SYNC_TNHT_CAMB as
SELECT CAMB.ROWID AS ROW_ID,
       CAMB.CAMB_PK,
       CAMB.CAMB_DATA AS CAMB_DATE,
       CAMB.CAMB_VAMB AS CAMB_VALO,
       CAMB.HUNMO_PK
FROM   TNHT_CAMB CAMB
WHERE HUNMO_PK in (SELECT HUNMO_PK FROM TNHT_HUNMO WHERE HOTE_PK = (SELECT HOTE_PK FROM TCFG_HOTE))
/


BEGIN
    MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN UPDATE SET PARA.SYNC_VERS = '20240704'
    WHEN NOT MATCHED THEN INSERT (PARA.GENE_PK, PARA.SYNC_VERS) VALUES (1, '20240704');
END;
/

COMMIT;