ALTER TABLE TNHT_IPOS ADD ASK_DESC CHAR(1) DEFAULT '0' NOT NULL;
ALTER TABLE TNHT_IPOS ADD ASK_ROOM CHAR(1) DEFAULT '0' NOT NULL;

COMMENT ON COLUMN TNHT_IPOS.ASK_DESC IS 'Ask for the description in ticket creation';
COMMENT ON COLUMN TNHT_IPOS.ASK_ROOM IS 'Ask for the room in ticket creation';

BEGIN
    MERGE INTO TCFG_GENE PARA
    USING DUAL
    ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN
        UPDATE SET PARA.DATE_VERS = '20240715'
    WHEN NOT MATCHED THEN
        INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20240715');
END;
/

COMMIT;