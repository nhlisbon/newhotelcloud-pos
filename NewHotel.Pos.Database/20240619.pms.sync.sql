CREATE OR REPLACE VIEW SYNC_TNHT_TICK AS
SELECT TICK.ROWID AS ROW_ID,
       TICK.TICK_PK,
       TICK.HOTE_PK,
       TICK.LITE_PK,
       TICK.TICK_IVAF,
       TICK.TICK_HORA,
       TICK.TICK_HCIE,
       TICK.TICK_SEPA,
       TICK.TICK_MSG1,
       TICK.TICK_MSG2,
       TICK.TICK_SIGN,
       TICK.TICK_LINE,
       TICK.TICK_LINP,
       TICK.TICK_LPRO,
       TICK.TICK_ALIG,
       TICK.TICK_SHLO,
       TICK.TICK_COLU,
       TICK.TICK_DISC,
       TICK.TICK_RECA,
       TICK.TICK_RECV,
       TICK.TICK_PAYM,
       TICK.TICK_TIPS,
       TICK.TICK_COPR,
       TICK.TICK_COTI,
       TICK.TICK_LARG,
       TICK.TICK_TSUG,
       TICK.TICK_LOGO,
       TICK.TICK_TAXD,
       TICK.TICK_SUBT,
       TICK.TICK_TYST,
       TICK.TICK_CCOP,
       TICK.TICK_RCDS,
       TICK.TICK_FISC,
       TICK.TICK_ASGO,
       TICK.TICK_ARNU,
       TICK.TICK_ANOM,
       TICK.TICK_ASIG
FROM TNHT_TICK TICK
         INNER JOIN TCFG_TEMP TEMP ON TEMP.HOTE_PK = TICK.HOTE_PK;

begin
    merge into tcfg_gene para using dual on (para.gene_pk = 1)
    when matched then update set para.date_vers = '20240611-001', para.rele_vers = '20240611-001',para.htfx_vers = '20240611-001'
    when not matched then insert (para.gene_pk, para.date_vers, para.rele_vers, para.htfx_vers) values (1, '20240611-001', '20240611-001', '20240611-001');
    commit;
end;
/