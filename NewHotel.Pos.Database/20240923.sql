create table tnht_arst (
    arst_pk raw(16),
    artg_pk raw(16) not null,
    arst_qtds number not null,
    arst_datr date not null,
    arst_lmod timestamp default current_timestamp not null,
    constraint nht_pk_arst primary key (arst_pk),
    constraint nht_fk_artg_arst foreign key (artg_pk)
      references tnht_artg (artg_pk)
      on delete cascade
);

create index nht_ind_arst_artg on tnht_arst (artg_pk);
create index nht_ind_arst_pk2 on tnht_arst (artg_pk, arst_datr);

comment on table tnht_arst is 'Local product stock';
comment on column tnht_arst.arst_pk is 'Primary key';
comment on column tnht_arst.artg_pk is 'Referenced product';
comment on column tnht_arst.arst_qtds is 'Stock value';
comment on column tnht_arst.arst_datr is 'Stock date';
comment on column tnht_arst.arst_lmod is 'Last modified';

BEGIN
    MERGE INTO TCFG_GENE PARA USING DUAL ON (PARA.GENE_PK = 1)
    WHEN MATCHED THEN
        UPDATE SET PARA.DATE_VERS = '20240923'
    WHEN NOT MATCHED THEN
        INSERT (PARA.GENE_PK, PARA.DATE_VERS) VALUES (1, '20240923');
    COMMIT;
END;
/