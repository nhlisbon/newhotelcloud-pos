﻿using System;
using System.Text;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using System.ServiceModel.Channels;
using Oracle.ManagedDataAccess.Client;
using Serilog;

namespace NewHotel.Sync.Setup
{
    class Program
    {
        private const string ConnectionStringFormat = "User Id={0};Password={1};Data Source={2};Pooling=false;Connection Lifetime=180";

        private static string GetConnectionString(string user, string password, string source, bool asSysDba = false)
        {
            var connectionString = string.Format(ConnectionStringFormat, user, password, source);
            if (asSysDba)
                connectionString += ";DBA Privilege=SYSDBA";
            return connectionString;
        }

        private static IEnumerable<string> GetTables()
        {
            var tables = new List<string>();

            tables.Add("TCFG_NPOS");
            tables.Add("TNHT_LITE,TNHT_MULT");
            tables.Add("TNHT_ENUM");
            tables.Add("TNHT_APPL");
            tables.Add("TNHT_HOTE,TCFG_HOTE");
            tables.Add("TNHT_BSERV,TNHT_BARTG,TNHT_ARTG,TNHT_ARIM");
            tables.Add("TNHT_SEPA");
            tables.Add("TNHT_BCAT");
            tables.Add("TNHT_BOTV");
            tables.Add("TNHT_PREP");
            tables.Add("TNHT_GRPP");
            tables.Add("TNHT_FORE,TNHT_FORI");
            tables.Add("TNHT_CACR");
            tables.Add("TNHT_IPOS,TNHT_SECC");
            tables.Add("TNHT_CAJA");
            tables.Add("TNHT_CAPV");
            tables.Add("TNHT_SALO");
            tables.Add("TNHT_IPSL");
            tables.Add("TNHT_COIN");
            tables.Add("TNHT_CLAS");
            tables.Add("TNHT_DUTY");
            tables.Add("TNHT_ESIM");
            tables.Add("TNHT_IMSE");
            tables.Add("TNHT_SEIM");
            tables.Add("TNHT_TIVA");
            tables.Add("TNHT_HOUR");
            tables.Add("TNHT_HAIP");
            tables.Add("TNHT_GRUP");
            tables.Add("TNHT_FAMI");
            tables.Add("TNHT_SFAM");
            tables.Add("TNHT_FAPP");
            tables.Add("TNHT_MEDE");
            tables.Add("TNHT_MENU");
            tables.Add("TNHT_MESA");
            tables.Add("TNHT_MTCO");
            tables.Add("TNHT_PVAR");
            tables.Add("TNHT_PVCI");
            tables.Add("TNHT_REGI");
            tables.Add("TNHT_REMU");
            tables.Add("TNHT_SEDO,TNHT_DOHO");
            tables.Add("TNHT_TICK");
            tables.Add("TNHT_TIDE");
            tables.Add("TNHT_TPPE");
            tables.Add("TNHT_TPRB");
            tables.Add("TNHT_TPRG");
            tables.Add("TNHT_TPRL");
            tables.Add("TNHT_UNMO");
            tables.Add("TNHT_UTIL");
            tables.Add("TNHT_GRPR");
            tables.Add("TNHT_PERM");
            tables.Add("TNHT_PEAP");
            tables.Add("TNHT_PEUT");
            tables.Add("TNHT_IPUT");

            return tables;
        }

        private static string InternalMetadata(object[] metadata)
        {
            // ColumnName
            var name = (string)metadata[0];
            // ColumnType
            var type = (int)metadata[12];
            // ColumnSize
            var size = (int)metadata[2];
            // NumericPrecision
            object value;
            value = metadata[3];
            short precision = 0;
            if (value != DBNull.Value)
                precision = (short)value;
            // NumericScale
            value = metadata[4];
            short scale = 0;
            if (value != DBNull.Value)
                scale = (short)value;
            // AllowDBNull
            value = metadata[13];

            var isNullable = true;
            if (value != DBNull.Value)
                isNullable = (bool)value;

            return name + type.ToString() + size.ToString() + precision.ToString() + scale.ToString() + (isNullable ? "1" : "0");
        }

        private static void CompareSchemas()
        {
            try
            {
                using (var connection = new OracleConnection(GetConnectionString("POSCLOUD", "POS", "LOCAL", false)))
                {
                    connection.Open();
                    try
                    {
                        foreach (var tables in GetTables())
                        {
                            var tableNames = tables.Split(',');
                            foreach (var tableName in tableNames)
                            {
                                DataTable dt1 = null;
                                using (var cmd = connection.CreateCommand())
                                {
                                    cmd.CommandText = string.Format("SELECT * FROM {0}", tableName);
                                    using (var rdr = cmd.ExecuteReader(CommandBehavior.SchemaOnly))
                                    {
                                        dt1 = rdr.GetSchemaTable();
                                    }
                                }

                                DataTable dt2 = null;
                                try
                                {
                                    using (var cn2 = new OracleConnection(GetConnectionString("POSCLOUD", "POS", "REMOTE", false)))
                                    {
                                        cn2.Open();
                                        try
                                        {
                                            using (var cmd = cn2.CreateCommand())
                                            {
                                                cmd.CommandText = string.Format("SELECT * FROM {0}", tableName);
                                                using (var rdr = cmd.ExecuteReader(CommandBehavior.SchemaOnly))
                                                {
                                                    dt2 = rdr.GetSchemaTable();
                                                }
                                            }
                                        }
                                        finally
                                        {
                                            cn2.Close();
                                        }
                                    }
                                }
                                catch
                                {

                                }

                                if (dt1 != null && dt2 != null)
                                {
                                    var metadata2 = new Dictionary<string, string>();
                                    for (int index = 0; index < dt2.Rows.Count; index++)
                                    {
                                        var items = dt2.Rows[index].ItemArray;
                                        var fieldName = (string)items[0];
                                        if (!fieldName.EndsWith("LMOD"))
                                            metadata2.Add(fieldName, InternalMetadata(items));
                                    }

                                    var metadata1 = new Dictionary<string, string>();
                                    for (int index = 0; index < dt1.Rows.Count; index++)
                                    {
                                        var items = dt1.Rows[index].ItemArray;
                                        var fieldName = (string)items[0];
                                        if (metadata2.ContainsKey(fieldName))
                                            metadata1.Add(fieldName, InternalMetadata(items));
                                    }

                                    if (!metadata1.OrderBy(x => x.Key).Select(x => x.Value)
                                        .SequenceEqual(metadata2.OrderBy(x => x.Key).Select(x => x.Value)))
                                    {
                                        //Console.WriteLine("--------------------------------");
                                        Console.WriteLine("{0} not equal", tableName);
                                        //Console.WriteLine("--------------------------------");
                                        //foreach (var md in metadata1.OrderBy(x => x.Key))
                                        //    Console.WriteLine(md);
                                        //Console.WriteLine("--------------------------------");
                                        //foreach (var md in metadata2.OrderBy(x => x.Key))
                                        //    Console.WriteLine(md);
                                        //Console.WriteLine("--------------------------------");
                                        //Console.ReadLine();
                                    }
                                }
                                else
                                    Console.WriteLine("{0} not found", tableName);
                            }
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            catch (OracleException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public class SyncBodyWriter : BodyWriter
        {
            private readonly MemoryStream _header = new MemoryStream();

            public SyncBodyWriter()
                : base(false)
            {
                using (var writer = System.Xml.XmlDictionaryWriter.Create(_header))
                {
                    WriteFields(writer);
                }
            }

            private void WriteFields(XmlWriter writer)
            {
                writer.WriteStartElement("Rd");
                writer.WriteAttributeString("Name", "El nombre");
                writer.WriteAttributeString("Pk", "La Pk");
                writer.WriteEndElement();
            }

            protected override void OnWriteBodyContents(XmlDictionaryWriter writer)
            {
                _header.Seek(0, SeekOrigin.Begin);
                var dr = XmlDictionaryReader.Create(_header);
                dr.MoveToContent();
                writer.WriteNode(dr, false);
                writer.WriteStartElement("Rs");
                writer.WriteStartElement("Rw");
                writer.WriteAttributeString("Op", "La Op");
                int i = 1;
                writer.WriteStartElement(i.ToString());
                writer.WriteValue(100M);
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
        }

        private static Oracle.ManagedDataAccess.Client.OracleDataReader GetMultipleRefCursors(Oracle.ManagedDataAccess.Client.OracleConnection connection, params string[] sql)
        {
            using (var command = connection.CreateCommand())
            {
                var sb = new StringBuilder();
                sb.AppendLine("BEGIN");
                for (int index = 0; index < sql.Length; index++)
                {
                    sb.AppendLine(string.Format("  OPEN :cursor{0} FOR  \r\n{1};", index + 1, sql[index]));
                    var param = command.CreateParameter();
                    param.OracleDbType = Oracle.ManagedDataAccess.Client.OracleDbType.RefCursor;
                    param.Direction = System.Data.ParameterDirection.ReturnValue;
                    command.Parameters.Add(param);
                }
                sb.AppendLine("END;");
                command.CommandText = sb.ToString().Replace("\r\n", "\n");

                //command.ExecuteNonQuery();
                //var readers = new List<Oracle.ManagedDataAccess.Client.OracleDataReader>(command.Parameters.Count);
                //for (int index = 0; index < command.Parameters.Count; index++)
                //{
                //    using (var param = command.Parameters[index])
                //    {
                //        if (param.OracleDbType == Oracle.ManagedDataAccess.Client.OracleDbType.RefCursor)
                //        {
                //            var refCursor = param.Value as Oracle.ManagedDataAccess.Types.OracleRefCursor;
                //            if (refCursor != null)
                //                readers.Add(refCursor.GetDataReader());

                //            refCursor.Dispose();
                //        }
                //    }
                //}

                //return readers.ToArray();
                return command.ExecuteReader();
            }
        }

        private static string ChangeNotificationTest(OracleConnection connection)
        {
            try
            {
                var dep = new OracleDependency();


                dep.OnChange += new OnChangeEventHandler(OnNotificaton);
                Console.WriteLine("Dependency created: {0}", dep.Id);

                using (var command = new OracleCommand("select 1 from TNHT_LITE", connection))
                {
                    dep.AddCommandDependency(command);
                    command.Notification.IsNotifiedOnce = false;
                    command.Notification.IsPersistent = false;
                    command.Notification.Timeout = 600;
                    command.AddRowid = true;
                    command.ExecuteNonQuery();
                }

                using (var command = new OracleCommand("select 1 from TNHT_MULT", connection))
                {
                    dep.AddCommandDependency(command);
                    command.Notification.IsNotifiedOnce = false;
                    command.Notification.IsPersistent = false;
                    command.Notification.Timeout = 600;
                    command.AddRowid = true;
                    command.ExecuteNonQuery();
                }

                using (var command = new OracleCommand("select 1 from TNHT_CLAS", connection))
                {
                    dep.AddCommandDependency(command);
                    command.Notification.IsNotifiedOnce = false;
                    command.Notification.IsPersistent = false;
                    command.Notification.Timeout = 600;
                    command.AddRowid = true;
                    command.ExecuteNonQuery();
                }

                using (var command = new OracleCommand("select 1 from TNHT_GRUP", connection))
                {
                    dep.AddCommandDependency(command);
                    command.Notification.IsNotifiedOnce = false;
                    command.Notification.IsPersistent = false;
                    command.Notification.Timeout = 600;
                    command.AddRowid = true;
                    command.ExecuteNonQuery();
                }

                using (var command = new OracleCommand("select 1 from TNHT_GRPP", connection))
                {
                    dep.AddCommandDependency(command);
                    command.Notification.IsNotifiedOnce = false;
                    command.Notification.IsPersistent = false;
                    command.Notification.Timeout = 600;
                    command.AddRowid = true;
                    command.ExecuteNonQuery();
                }

                return dep.Id;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return null;
        }

        private static void OnNotificaton(object source, OracleNotificationEventArgs arg)
        {
            Console.WriteLine("Notification Received: {0}", ((OracleDependency)source).Id);
            for (int index = 0; index < arg.ResourceNames.Length; index++)
                Console.WriteLine("Resource: {0}", arg.ResourceNames[index]);
            for (int index = 0; index < arg.Details.Rows.Count; index++)
                Console.WriteLine("RowId: {0} -> {1}", arg.Details.Rows[index]["ResourceName"], arg.Details.Rows[index]["RowId"]);
        }

        private static IEnumerable<string> GetTableNames()
        {
            var names = new List<string>();

            names.Add("TNHT_LITE");
            names.Add("TNHT_MULT");
            names.Add("TNHT_ENUM");
            names.Add("TNHT_CLAS");
            names.Add("TNHT_GRSE");
            names.Add("TNHT_GRUP");
            names.Add("TNHT_FAMI");
            names.Add("TNHT_SFAM");
            names.Add("TNHT_COIN");
            names.Add("TNHT_SEPA");
            names.Add("TNHT_ARTG");
            names.Add("TNHT_ARTB");
            names.Add("TNHT_ARIM");
            names.Add("TNHT_UNMO");
            names.Add("TNHT_REGI");
            names.Add("TNHT_SEIM");
            names.Add("TNHT_TIVA");
            names.Add("TNHT_HOTE");
            names.Add("TNHT_AREA");
            names.Add("TNHT_BCAT");
            names.Add("TNHT_CACR");
            names.Add("TNHT_MENU");
            names.Add("TNHT_MEDE");
            names.Add("TNHT_ESIM");
            names.Add("TNHT_IMSE");
            names.Add("TNHT_TIDE");
            names.Add("TNHT_UTIL");
            names.Add("TNHT_GRPR");
            names.Add("TNHT_PERM");
            names.Add("TNHT_PEAP");
            names.Add("TNHT_PEUT");
            names.Add("TNHT_PREP");
            names.Add("TNHT_ARTP");
            names.Add("TNHT_FAPP");
            names.Add("TNHT_GRPP");
            names.Add("TNHT_CAJA");
            names.Add("TNHT_SALO");
            names.Add("TNHT_MESA");
            names.Add("TNHT_DUTY");
            names.Add("TNHT_TPRG");
            names.Add("TNHT_TPRB");
            names.Add("TNHT_HOUR");
            names.Add("TNHT_TPPE");
            names.Add("TNHT_TPRL");
            names.Add("TNHT_MTCO");
            names.Add("TNHT_TICK");
            names.Add("TNHT_TISU");
            names.Add("TNHT_IPOS");
            names.Add("TNHT_SEDO");
            names.Add("TNHT_ARAR");
            names.Add("TNHT_PVCI");
            names.Add("TNHT_PVAR");
            names.Add("TNHT_HAIP");
            names.Add("TNHT_IPSL");
            names.Add("TNHT_BOTV");
            names.Add("TNHT_CAPV");
            names.Add("TNHT_IPUT");
            names.Add("TCFG_NPOS");
            names.Add("TNHT_COMU");
            names.Add("TNHT_DIST");

            return names.Select(o => o.ToLowerInvariant());
        }

        private static bool ExcludeFromDelete(string name)
        {
            switch (name.ToUpperInvariant())
            {
                case "TNHT_LITE":
                case "TNHT_MULT":
                case "TNHT_ENUM":
                case "TNHT_CLAS":
                case "TNHT_HOTE":
                case "TNHT_SEDO":
                    return true;
            }

            return false;
        }

        private static void TestLog()
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.ColoredConsole()
                .WriteTo.Seq("http://localhost:5341")
                .CreateLogger();

            Log.Information("Hello, {Name}!", Environment.UserName);

            // Important to call at exit so that batched events are flushed.
            Log.CloseAndFlush();
        }

        private static void CompoundTrigger()
        {
            using (var connection = new OracleConnection(GetConnectionString("PT2014022001", "CLOUD", "ORCL1", false)))
            {
                connection.Open();
                try
                {
                    var sql = new StringBuilder();
                    sql.AppendLine("select ucc.column_name from user_constraints uc inner join user_cons_columns ucc");
                    sql.AppendLine("on ucc.table_name = uc.table_name and ucc.constraint_name = uc.constraint_name");
                    sql.Append("where ucc.position = 1 and uc.constraint_type = 'P' and uc.table_name = :table_name");

                    using (var command = connection.CreateCommand())
                    {
                        command.BindByName = true;
                        command.CommandText = sql.Replace("\r\n", "\n").ToString();

                        var param = command.CreateParameter();
                        param.ParameterName = "table_name";
                        command.Parameters.Add(param);

                        var path = string.Format(".\\{0}.sql", "ChangeNotificationTriggers");

                        var names = GetTableNames();
                        foreach (var name in names)
                        {
                            param.Value = name.ToUpperInvariant();
                            var pk = Convert.ToString(command.ExecuteScalar()).ToLowerInvariant();

                            sql = new StringBuilder();
                            sql.AppendLine("CREATE OR REPLACE TRIGGER change_notification_{0}");
                            sql.Append("FOR INSERT OR UPDATE ");
                            if (!ExcludeFromDelete(name))
                                sql.Append("OR DELETE ");
                            sql.AppendLine("ON {0} DISABLE");
                            sql.AppendLine("COMPOUND TRIGGER");
                            sql.AppendLine("  TYPE t_change_notification IS TABLE OF change_notification%ROWTYPE;");
                            sql.AppendLine("  l_change_notification t_change_notification := t_change_notification();");
                            sql.AppendLine("  v_change_id INTEGER;");
                            sql.AppendLine("  AFTER EACH ROW IS");
                            sql.AppendLine("  BEGIN");
                            sql.AppendLine("    SELECT change_notification_id.NEXTVAL INTO v_change_id FROM DUAL;");
                            sql.AppendLine("    l_change_notification.EXTEND;");
                            sql.AppendLine("    l_change_notification(l_change_notification.LAST).change_id := v_change_id;");
                            sql.AppendLine("    l_change_notification(l_change_notification.LAST).change_table_name := '{1}';");
                            if (ExcludeFromDelete(name))
                            {
                                sql.AppendLine("    l_change_notification(l_change_notification.LAST).change_operation := 'M';");
                                sql.AppendLine("    l_change_notification(l_change_notification.LAST).change_row_id := :NEW.ROWID;");
                            }
                            else
                            {
                                sql.AppendLine("    IF DELETING THEN");
                                sql.AppendLine("      l_change_notification(l_change_notification.LAST).change_operation := 'D';");
                                sql.AppendLine("      l_change_notification(l_change_notification.LAST).change_row_id := :OLD.ROWID;");
                                sql.AppendLine("      l_change_notification(l_change_notification.LAST).change_delete_pk := :OLD.{2};");
                                sql.AppendLine("    ELSE");
                                sql.AppendLine("      l_change_notification(l_change_notification.LAST).change_operation := 'M';");
                                sql.AppendLine("      l_change_notification(l_change_notification.LAST).change_row_id := :NEW.ROWID;");
                                sql.AppendLine("    END IF;");
                            }
                            sql.AppendLine("  END AFTER EACH ROW;");
                            sql.AppendLine("  AFTER STATEMENT IS");
                            sql.AppendLine("  BEGIN");
                            sql.AppendLine("     FORALL v_index IN l_change_notification.FIRST..l_change_notification.LAST");
                            sql.AppendLine("       INSERT INTO change_notification (change_id, change_table_name, change_operation, change_row_id, change_delete_pk)");
                            sql.AppendLine("       VALUES (l_change_notification(v_index).change_id, l_change_notification(v_index).change_table_name,");
                            sql.AppendLine("               l_change_notification(v_index).change_operation, l_change_notification(v_index).change_row_id,");
                            sql.AppendLine("               l_change_notification(v_index).change_delete_pk);");
                            sql.AppendLine("     l_change_notification.DELETE;");
                            sql.AppendLine("  END AFTER STATEMENT;");
                            sql.AppendLine("END;");
                            sql.AppendLine("/");

                            File.AppendAllText(path, Environment.NewLine);
                            File.AppendAllText(path, string.Format(sql.ToString(), name, name.ToUpperInvariant(), pk));
                        }
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        static void Main(string[] args)
        {
            //TestLog();

            Console.WriteLine();
            Console.WriteLine("Press any key to exit ...");
            Console.ReadKey(false);
        }
    }
}