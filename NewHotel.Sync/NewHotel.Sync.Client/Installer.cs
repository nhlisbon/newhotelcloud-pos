﻿using System.ComponentModel;
using System.ServiceProcess;
using System.Configuration.Install;

namespace NewHotel.Sync.Client
{
    [RunInstaller(true)]
    public class ProjectInstaller : Installer
    {
        private ServiceProcessInstaller process;
        private ServiceInstaller service;

        public ProjectInstaller()
        {
            process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.LocalSystem;
            service = new ServiceInstaller();
            service.ServiceName = SyncServiceHost.SyncServiceName;
            service.DisplayName = service.ServiceName.Replace('.', ' ');
            Installers.Add(process);
            Installers.Add(service);
        }
    }
}