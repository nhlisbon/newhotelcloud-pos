﻿using System;
using System.Collections.Generic;
using System.IO;

namespace NewHotel.Sync.Data
{
    public abstract class SyncProvider
    {
        private readonly static List<string> _tableNames;

        static SyncProvider()
        {
            _tableNames = new List<string>();
        }

        public abstract Stream GetImageStream(Guid hotelId, string path);
        public abstract Tuple<string, string, string, string> GetConnection(Guid hotelId);

        protected static void AddTableName(string tableName)
        {
            _tableNames.Add(tableName);
        }

        public IEnumerable<string> GetTableNames()
        {
            return _tableNames;
        }
    }
}