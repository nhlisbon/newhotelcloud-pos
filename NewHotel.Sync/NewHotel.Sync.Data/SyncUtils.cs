﻿using System;
using System.Text;
using System.Linq;
using System.Data;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Globalization;
using System.Diagnostics;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using NewHotel.Diagnostics;
using RestSharp;
using Newtonsoft.Json;

namespace NewHotel.Sync.Data
{
    public static class SyncUtil
    {
        #region Constants

        private const string ConnectionStringFormat = "User Id={0};Password={1};Data Source={2};Min Pool Size={3};Max Pool Size={4};Pooling={5};Connection Lifetime=300;Connection Timeout=5";
        public const int MinPoolSize = 2;
        public const int MaxPoolSize = 5;

        public const string SyncChangesAction = "SyncChanges";
        public const string SyncChangesAckAction = "SyncChangesAck";
        public const string SyncChangesNakAction = "SyncChangesNak";

        private const string MergeTag = "M";
        private const string DeleteTag = "D";
        private const string InsertTag = "I";
        private const string UpdateTag = "U";
        private const string ProcedureTag = "P";
        private const string IdTag = "Id";
        private const string SyncBeginTag = "Begin";
        private const string SyncEndTag = "End";
        private const string SyncFieldTag = "Fld";
        private const string SyncMarkTag = "Counter";
        private const string OperationTag = "Cmd";
        private const string LastModifiedTag = "Modified";
        private const string SourceTag = "Source";
        private const string TargetTag = "Target";
        private const string PrimaryKeyTag = "Pk";
        private const string IndexTag = "Index";
        private const string DataTypeTag = "DataType";
        private const string DataOpTag = "DataOp";
        private const string RowDefTag = "RowDef";
        private const string ColDefTag = "ColDef";
        private const string RowSetTag = "RowSet";
        private const string RowTag = "Row";
        private const string ColumnTag = "Col";

        private const string RawDbTypeTag = "R";
        private const string StringDbTypeTag = "C";
        private const string FloatDbTypeTag = "F";
        private const string DateTimeDbTypeTag = "D";
        private const string LongDbTypeTag = "L";
        private const string IntDbTypeTag = "I";
        private const string ShortDbTypeTag = "S";
        private const string ByteDbTypeTag = "B";
        private const string BlobDbTypeTag = "O";
        private const string ClobDbTypeTag = "M";
        private const string IntervalDSDbTypeTag = "N";
        private const string UnknownTypeTag = "U";

        private const string DeleteParamName = "DELETED";
        private const string TrueValue = "1";
        private const string FalseValue = "0";

        private const string ChangeIdColumn = "CHANGE_ID";
        private const string ChangeOperationColumn = "CHANGE_OPERATION";
        private const string ChangeDeletePkColumn = "CHANGE_DELETE_PK";
        private const string ChangeLastModifiedColumn = "CHANGE_LASTMODIFIED";
        private const string ChangeRowIdColumn = "ROW_ID";

        private const int CommandTimeout = 25;

        #region Manager
        private const string RouteAuthCredentialContext = "auth/com";
        private const string RouteHotelGetById = "manager/hotel/get";
        #endregion Manager

        #endregion
        #region Members

        private static readonly ConcurrentDictionary<Guid, Tuple<string, string, string, string>> _connections =
            new ConcurrentDictionary<Guid, Tuple<string, string, string, string>>();
        private static readonly ConcurrentDictionary<string, string> _commands =
            new ConcurrentDictionary<string, string>();
        private static readonly ConcurrentDictionary<string, Func<string, string, StringBuilder, HashSet<string>, IEnumerable<string>, IEnumerable<string>, bool>> _builders =
            new ConcurrentDictionary<string, Func<string, string, StringBuilder, HashSet<string>, IEnumerable<string>, IEnumerable<string>, bool>>();

        private static readonly string ConnectionCommandText;
        private static readonly string UpdateSyncMarkCommandText;
        private static readonly string UpdateSyncMarkTimestampCommandText;
        private static readonly string UpdateSyncBeginCommandText;
        private static readonly string UpdateSyncEndCommandText;
        private static readonly string DeleteSyncEndCommandText;
        private static readonly string UpdateSyncLastModifiedCommandText;

        private static MessageBuffer _ackMessageBuffer;
        private static MessageBuffer _nakMessageBuffer;

        private static string Service;
        private static string Schema;
        private static string Password;

        #region Manager
        private static string BaseUrl;
        private static string ApiKey;
        private static bool KeyNeeded = false;
        private static string Username;
        private static IRestClient restClient;
        #endregion Manager

        #endregion
        #region Constructor

        static SyncUtil()
        {
            ConnectionCommandText = "SELECT hote_user, hote_pwrd, hote_serv, hote_desc FROM tnht_hote WHERE hote_pk = :hote_pk";
            UpdateSyncMarkCommandText = "UPDATE tnht_hote SET hote_cont = :hote_cont";
            UpdateSyncMarkTimestampCommandText = "UPDATE tnht_hote SET hote_cont = :hote_cont, hote_sync = :hote_sync";
            UpdateSyncBeginCommandText = "UPDATE {0} SET deleted = null";
            UpdateSyncEndCommandText = "UPDATE {0} SET deleted = '1' WHERE deleted is null";
            UpdateSyncLastModifiedCommandText = "UPDATE tnht_sync SET sync_lmod = :sync_lmod WHERE sync_name = :sync_name";

            var sql = new StringBuilder();
            sql.AppendLine("DECLARE");
            sql.AppendLine("  dml_errors EXCEPTION;");
            sql.AppendLine("  PRAGMA exception_init(dml_errors, -24381);");
            sql.AppendLine("  CURSOR c_rowid IS SELECT rowid as row_id FROM {0} WHERE deleted = '1';");
            sql.AppendLine("  TYPE t_tab_rowid IS TABLE OF UROWID;");
            sql.AppendLine("  l_tab_rowid t_tab_rowid;");
            sql.AppendLine("  l_tab_limit CONSTANT NUMBER := 1000;");
            sql.AppendLine("BEGIN");
            sql.AppendLine("  OPEN c_rowid;");
            sql.AppendLine("  LOOP");
            sql.AppendLine("    FETCH c_rowid BULK COLLECT INTO l_tab_rowid LIMIT l_tab_limit;");
            sql.AppendLine("    BEGIN");
            sql.AppendLine("      FORALL i IN l_tab_rowid.FIRST..l_tab_rowid.LAST SAVE EXCEPTIONS");
            sql.AppendLine("        DELETE FROM {0} WHERE rowid = l_tab_rowid(i);");
            sql.AppendLine("    EXCEPTION");
            sql.AppendLine("      WHEN dml_errors THEN");
            sql.AppendLine("        NULL;");
            sql.AppendLine("    END;");
            sql.AppendLine("    EXIT WHEN l_tab_rowid.COUNT < l_tab_limit;");
            sql.AppendLine("  END LOOP;");
            sql.AppendLine("  CLOSE c_rowid;");
            sql.Append("END;");
            DeleteSyncEndCommandText = sql.Replace("\r\n", "\n").ToString();

            _ackMessageBuffer = GetSyncResponseMessage(SyncChangesAckAction);
            _nakMessageBuffer = GetSyncResponseMessage(SyncChangesNakAction);
        }

        #endregion
        #region Private Clases

        private abstract class SyncBodyWriter : BodyWriter
        {
            public SyncBodyWriter()
                : base(false)
            {
            }
        }

        private sealed class SyncTableBodyWriter : SyncBodyWriter
        {
            private readonly Guid? _id;
            private readonly string _source;
            private readonly string[] _names;
            private OracleDataReader _reader;
            private long _size;
            private MemoryStream _rowDef;
            private int _index;
            private string _pkName;
            private Tuple<string, string>[] _fields;
            private bool _endOfReader;
            private bool _nextResult;

            public SyncTableBodyWriter(OracleDataReader reader, long size, Guid? id, string source, params string[] names)
                : base()
            {
                _source = source;
                _reader = reader;
                _size = size;
                _id = id;
                _names = names;
                Initialize(_reader);
            }

            private void Initialize(OracleDataReader reader)
            {
                if (_size <= 0L)
                {
                    if (reader.FetchSize > 0 && reader.RowSize > 0)
                        _size = (long)Math.Truncate((decimal)(reader.FetchSize / reader.RowSize));
                    else
                        _size = long.MaxValue;
                }

                _rowDef = new MemoryStream();
                using (var writer = XmlDictionaryWriter.Create(_rowDef, new XmlWriterSettings() { OmitXmlDeclaration = true }))
                {
                    writer.WriteStartElement(RowDefTag);
                    var name = _names[_index].ToUpperInvariant();
                    _index++;
                    writer.WriteAttributeString(SourceTag, string.IsNullOrEmpty(_source) ? name : _source);
                    writer.WriteAttributeString(TargetTag, name);
                    _pkName = GetPk(name, reader);
                    writer.WriteAttributeString(PrimaryKeyTag, _pkName);

                    _fields = new Tuple<string, string>[reader.FieldCount];

                    var i = 0;
                    for (int index = 0; index < reader.FieldCount; index++)
                    {
                        var fieldName = reader.GetName(index).ToUpperInvariant();
                        var fieldTypeName = GetDataTypeName(reader.GetDataTypeName(index).ToUpper());
                        _fields[index] = Tuple.Create(fieldName, fieldTypeName);

                        if (Included(name, fieldName))
                        {
                            writer.WriteStartElement(ColDefTag);
                            writer.WriteAttributeString(IndexTag, (i++).ToString());
                            writer.WriteAttributeString(DataTypeTag, fieldTypeName);
                            var dataOp = GetOp(name, fieldName);
                            if (!string.IsNullOrEmpty(dataOp))
                                writer.WriteAttributeString(DataOpTag, dataOp);
                            writer.WriteString(fieldName);
                            writer.WriteEndElement();
                        }
                    }
                }
            }

            private bool Included(string tableName, string fieldName)
            {
                switch (fieldName)
                {
                    case ChangeIdColumn:
                    case ChangeOperationColumn:
                    case ChangeDeletePkColumn:
                    case ChangeLastModifiedColumn:
                    case ChangeRowIdColumn:
                        return false;
                }

                return true;
            }

            private string GetOp(string tableName, string fieldName)
            {
                switch (tableName)
                {
                    case "TNHT_SEDO":
                        if (fieldName == "SEDO_NUFA")
                            return "I";
                        break;
                }

                return string.Empty;
            }

            private string GetPk(string tableName, IDataReader reader)
            {
                switch (tableName)
                {
                    case "TNHT_MULT":
                        return "LITE_PK,LANG_PK";
                    case "TNHT_DOHO":
                        return "IPOS_PK,CAJA_PK,TIDO_PK";
                }

                int index = 0;
                while (index++ < reader.FieldCount)
                {
                    var fieldName = reader.GetName(index);
                    switch (fieldName.ToUpperInvariant())
                    {
                        case ChangeIdColumn:
                        case ChangeOperationColumn:
                        case ChangeDeletePkColumn:
                        case ChangeLastModifiedColumn:
                        case ChangeRowIdColumn:
                            break;
                        default:
                            return reader.GetName(index);
                    }
                }

                return string.Empty;
            }

            private IEnumerable<object[]> GetRows(OracleDataReader reader)
            {
                var rows = new List<object[]>();

                long i = 0;
                while (++i <= _size)
                {
                    _endOfReader = !reader.Read();
                    if (_endOfReader)
                    {
                        _nextResult = reader.NextResult();
                        if (_nextResult)
                            _endOfReader = false;

                        break;
                    }
                    else
                    {
                        var values = new object[reader.FieldCount];
                        reader.GetValues(values);
                        rows.Add(values);
                    }
                }

                return rows;
            }

            private static string GetDataTypeName(string typeName)
            {
                // gets tag name from type name
                switch (typeName)
                {
                    case "RAW":
                        return RawDbTypeTag;
                    case "VARCHAR2":
                    case "CHAR":
                        return StringDbTypeTag;
                    case "DECIMAL":
                        return FloatDbTypeTag;
                    case "DATE":
                    case "TIMESTAMP":
                        return DateTimeDbTypeTag;
                    case "INT16":
                        return ShortDbTypeTag;
                    case "INT32":
                        return IntDbTypeTag;
                    case "INT64":
                        return LongDbTypeTag;
                    case "BYTE":
                        return ByteDbTypeTag;
                    case "BLOB":
                        return BlobDbTypeTag;
                    case "CLOB":
                        return ClobDbTypeTag;
                    case "INTERVALDS":
                        return IntervalDSDbTypeTag;
                }

                return UnknownTypeTag;
            }

            private static void WriteValue(XmlDictionaryWriter writer, string typeName, object value)
            {
                // writes value according to type tag
                switch (typeName)
                {
                    case RawDbTypeTag:
                    case BlobDbTypeTag:
                        var bytes = value as byte[];
                        writer.WriteString(bytes.ToHex());
                        break;
                    case ByteDbTypeTag:
                    case ShortDbTypeTag:
                        writer.WriteValue(Convert.ToInt32(value));
                        break;
                    case IntDbTypeTag:
                        writer.WriteValue((int)value);
                        break;
                    case LongDbTypeTag:
                        writer.WriteValue((long)value);
                        break;
                    case DateTimeDbTypeTag:
                        writer.WriteValue(DateTime.SpecifyKind((DateTime)value, DateTimeKind.Utc).Ticks);
                        break;
                    case StringDbTypeTag:
                    case ClobDbTypeTag:
                        writer.WriteValue((string)value);
                        break;
                    case FloatDbTypeTag:
                        writer.WriteValue((decimal)value);
                        break;
                    case IntervalDSDbTypeTag:
                        writer.WriteValue((TimeSpan) value);
                        break;
                    default:
                        writer.WriteValue(value);
                        break;
                }
            }

            protected override void OnWriteBodyContents(XmlDictionaryWriter writer)
            {
                try
                {
                    if (_id.HasValue)
                    {
                        // writes id tag if present
                        writer.WriteStartElement(IdTag);
                        writer.WriteString(_id.Value.ToHex());
                        writer.WriteEndElement();
                    }

                    do
                    {
                        // gets rows from reader
                        var rows = GetRows(_reader);

                        if (rows.Any())
                        {
                            _rowDef.Seek(0, SeekOrigin.Begin);
                            using (var reader = XmlDictionaryReader.Create(_rowDef))
                            {
                                reader.MoveToContent();
                                writer.WriteNode(reader, false);
                            }

                            // // writes row set tag start
                            writer.WriteStartElement(RowSetTag);
                            foreach (var row in rows)
                            {
                                // writes row tag start
                                writer.WriteStartElement(RowTag);

                                var i = 0;
                                int pkDeleteIndex = -1;
                                for (int index = 0; index < _fields.Length; index++)
                                {
                                    var field = _fields[index];
                                    var name = field.Item1;
                                    var value = pkDeleteIndex >= 0 && name == _pkName ? row[pkDeleteIndex] : row[index];

                                    switch (name)
                                    {
                                        case ChangeIdColumn:
                                            // writes sync mark if is not null
                                            if (value != DBNull.Value)
                                                writer.WriteAttributeString(SyncMarkTag, Convert.ToInt64(value).ToString());
                                            break;
                                        case ChangeOperationColumn:
                                            // writes operation identifier if is not null
                                            if (value != DBNull.Value)
                                                writer.WriteAttributeString(OperationTag, value.ToString());
                                            break;
                                        case ChangeDeletePkColumn:
                                            // gets column index of delete primary key if especified
                                            if (value != DBNull.Value)
                                                pkDeleteIndex = index;
                                            break;
                                        case ChangeLastModifiedColumn:
                                            // writes last modified value if is not null
                                            if (value != DBNull.Value)
                                                writer.WriteAttributeString(LastModifiedTag, ((DateTime)value).Ticks.ToString());
                                            break;
                                        case ChangeRowIdColumn:
                                            // if row id is null, skip this row
                                            if (value == DBNull.Value)
                                                continue;
                                            break;
                                        default:
                                            // writes column data
                                            if (value != DBNull.Value)
                                            {
                                                writer.WriteStartElement(ColumnTag);
                                                writer.WriteAttributeString(IndexTag, i.ToString());
                                                WriteValue(writer, field.Item2, value);
                                                writer.WriteEndElement();
                                            }
                                            i++;
                                            break;
                                    }
                                }

                                // writes row tag end
                                writer.WriteEndElement();
                            }
                            // // writes row set tag end
                            writer.WriteEndElement();
                        }

                        // next result set or exit
                        if (_nextResult)
                        {
                            _nextResult = false;
                            Initialize(_reader);
                        }
                        else
                            break;
                    } while (true);

                    if (_endOfReader)
                    {
                        _reader.Close();
                        _reader = null;
                    }
                }
                catch
                {
                    if (!_reader.IsClosed)
                        _reader.Close();
                    _reader = null;
                    throw;
                }
            }
        }

        private sealed class SyncIdBodyWriter : SyncBodyWriter
        {
            #region Members

            private readonly long _syncId;

            #endregion
            #region Public Methods

            public SyncIdBodyWriter(long syncId)
                : base()
            {
                _syncId = syncId;
            }

            #endregion
            #region Protected Methods

            protected override void OnWriteBodyContents(XmlDictionaryWriter writer)
            {
                writer.WriteStartElement(SyncMarkTag);
                writer.WriteValue(_syncId);
                writer.WriteEndElement();
            }

            #endregion
        }

        private sealed class SyncCommandBodyWriter : SyncBodyWriter
        {
            #region Members

            private readonly string _syncCommand;
            private readonly string _syncName;

            #endregion
            #region Public Methods

            public SyncCommandBodyWriter(string syncCommand, string syncName)
                : base()
            {
                _syncCommand = syncCommand;
                _syncName = syncName;
            }

            #endregion
            #region Protected Methods

            protected override void OnWriteBodyContents(XmlDictionaryWriter writer)
            {
                writer.WriteStartElement(_syncCommand);
                writer.WriteValue(_syncName);
                writer.WriteEndElement();
            }

            #endregion
        }

        #region Manager

        public sealed class NHWManagerSettings
        {
            public string Url { get; set; }
            public string ApiKey { get; set; }
        }

        public class CredentialsBaseModel
        {
            public long LanguageId { get; set; } = 1033;
            public long ApplicationId { get; set; } = 101;
        }

        public class CredentialsModel : CredentialsBaseModel
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }

        public class ContextCredentialsModel : CredentialsModel
        {
            public string HotelId { get; set; }
        }

        public class ManagerBaseResponse
        {
            public List<string> Errors { get; set; } = new List<string>();
            public List<string> Warnings { get; set; } = new List<string>();

            public bool HasErrors => Errors.Count > 0;
            public bool HasWarnings => Warnings.Count > 0;
            public bool IsEmpty => (Warnings.Count == 0 && Errors.Count == 0);
        }

        public class BaseTokenResponse : ManagerBaseResponse
        {
            public string Token { get; set; }
        }

        public class HotelModelResponse : ManagerBaseResponse
        {
            public HotelModel Model { get; set; }
        }

        public class HotelModel
        {
            public string HotelFullName { get; set; }

            public string HotelStringConnection { get; set; }
        }

        #endregion Manager

        #endregion
        #region Private Methods

        private static TraceSource Logger
        {
            get { return Log.Source["NewHotel.Sync"]; }
        }

        private static MessageBuffer GetSyncResponseMessage(string action)
        {
            var message = Message.CreateMessage(MessageVersion.Soap12WSAddressing10, action);
            return message.CreateBufferedCopy(int.MaxValue);
        }

        private static Type GetDbType(string typeName)
        {
            switch (typeName)
            {
                case RawDbTypeTag:
                    return typeof(byte[]);
                case StringDbTypeTag:
                    return typeof(string);
                case FloatDbTypeTag:
                    return typeof(decimal);
                case DateTimeDbTypeTag:
                    return typeof(DateTime);
                case LongDbTypeTag:
                    return typeof(long);
                case IntDbTypeTag:
                    return typeof(int);
                case ShortDbTypeTag:
                    return typeof(short);
                case ByteDbTypeTag:
                    return typeof(byte);
                case BlobDbTypeTag:
                    return typeof(OracleBlob);
                case ClobDbTypeTag:
                    return typeof(OracleClob);
                case IntervalDSDbTypeTag:
                    return typeof(OracleIntervalDS);
            }

            return typeof(object);
        }

        private static Type GetDbTypeByValue(object value)
        {
            return value is TimeSpan ? typeof(OracleIntervalDS) : value.GetType();
        }

        private static object GetDbValue(string typeName, object value)
        {
            switch (typeName)
            {
                case StringDbTypeTag:
                    return value.ToString();
                case DateTimeDbTypeTag:
                    return DateTime.SpecifyKind(new DateTime(Convert.ToInt64(value)), DateTimeKind.Utc);
                case ShortDbTypeTag:
                    return Convert.ToInt16(value);
                case RawDbTypeTag:
                case BlobDbTypeTag:
                    var binHex = (string)value;
                    return binHex.ToBytes();
                case ClobDbTypeTag:
                    return value.ToString().ToCharArray();
                case IntervalDSDbTypeTag:
                    return new OracleIntervalDS((TimeSpan) value);
                default:
                    if (value is TimeSpan span)
                        return new OracleIntervalDS(span);
                    break;
            }

            return value;
        }

        private static void GetSyncCommandParameters(OracleCommand command,
            IEnumerable<Tuple<string, string, string, object[]>> parameters, int arrayLength)
        {
            if (arrayLength <= 0) return;

            command.ArrayBindCount = arrayLength;

            foreach (var parameter in parameters)
            {
                var paramName = parameter.Item1;
                var paramTypeName = parameter.Item2;
                var dbValueType = GetDbType(paramTypeName);
                if (dbValueType == typeof(object))
                    // Previously was verified if the array has values
                    dbValueType = GetDbTypeByValue(parameter.Item4[0]);

                var arrayBindStatus = new OracleParameterStatus[arrayLength];
                var valueArray = Array.CreateInstance(dbValueType ?? typeof(object), arrayLength);
                valueArray.Initialize();

                for (var index = 0; index < arrayLength; index++)
                {
                    var dbValue = parameter.Item4[index];
                    if (dbValue == null)
                        arrayBindStatus[index] = OracleParameterStatus.NullInsert;
                    else
                    {
                        dbValue = GetDbValue(paramTypeName, dbValue);

                        if (dbValueType == typeof(OracleBlob))
                        {
                            var blob = new OracleBlob(command.Connection);
                            blob.BeginChunkWrite();
                            var bytes = (byte[])dbValue;
                            blob.Write(bytes, 0, bytes.Length);
                            blob.EndChunkWrite();
                            valueArray.SetValue(blob, index);
                        }
                        else if (dbValueType == typeof(OracleClob))
                        {
                            var clob = new OracleClob(command.Connection);
                            clob.BeginChunkWrite();
                            var array = (char[])dbValue;
                            clob.Write(array, 0, array.Length);
                            clob.EndChunkWrite();
                            valueArray.SetValue(clob, index);
                        }
                        else
                            valueArray.SetValue(dbValue, index);
                    }
                }

                var param = command.CreateParameter();
                param.ParameterName = paramName;
                param.Direction = ParameterDirection.Input;
                param.ArrayBindStatus = arrayBindStatus;
                param.Value = valueArray;

                command.Parameters.Add(param);
            }
        }

        private static bool UpdateDeletedOnMerge(string syncName)
        {
            switch (syncName)
            {
                case "TNHT_GRSE":
                case "TNHT_GRUP":
                case "TNHT_FAMI":
                case "TNHT_SFAM":
                case "TNHT_COIN":
                case "TNHT_SEPA":
                case "TNHT_ARTG":
                case "TNHT_UNMO":
                case "TNHT_FORE":
                case "TNHT_REGI":
                case "TNHT_SEIM":
                case "TNHT_TIVA":
                case "TNHT_HOTE":
                case "TNHT_AREA":
                case "TNHT_BCAT":
                case "TNHT_CACR":
                case "TNHT_MENU":
                case "TNHT_MEDE":
                case "TNHT_ESIM":
                case "TNHT_IMSE":
                case "TNHT_TIDE":
                case "TNHT_UTIL":
                case "TNHT_GRPR":
                case "TNHT_PERM":
                case "TNHT_PEAP":
                case "TNHT_PEUT":
                case "TNHT_PREP":
                case "TNHT_ARTP":
                case "TNHT_FAPP":
                case "TNHT_GRPP":
                case "TNHT_CAJA":
                case "TNHT_SALO":
                case "TNHT_MESA":
                case "TNHT_DUTY":
                case "TNHT_TPRG":
                case "TNHT_TPRB":
                case "TNHT_HOUR":
                case "TNHT_TPPE":
                case "TNHT_TPRL":
                case "TNHT_MTCO":
                case "TNHT_TICK":
                case "TNHT_TISU":
                case "TNHT_IPOS":
                case "TNHT_SEDO":
                case "TNHT_ARAR":
                case "TNHT_PVCI":
                case "TNHT_PVAR":
                case "TNHT_HAIP":
                case "TNHT_IPSL":
                case "TNHT_BOTV":
                case "TNHT_CAPV":
                case "TNHT_IPUT":
                case "TNHT_COMU":
                case "TNHT_DIST":
                case "TNHT_CLIE":
                    return true;
            }

            return false;
        }

        private static bool DeleteOnMerge(string syncName)
        {
            switch (syncName)
            {
                case "TNHT_PEUT":
                case "TNHT_FAPP":
                case "TNHT_GRPP":
                case "TNHT_TPRB":
                case "TNHT_TISU":
                case "TNHT_TPPE":
                case "TNHT_TPRL":
                case "TNHT_ARAR":
                case "TNHT_ARIM":
                case "TNHT_PVCI":
                case "TNHT_PVAR":
                case "TNHT_HAIP":
                case "TNHT_IPSL":
                case "TNHT_BOTV":
                case "TNHT_CAPV":
                case "TNHT_IPUT":
                case "TNHT_ARTB":
                    return true;
            }

            return false;
        }

        private static OracleCommand BuildSyncCommand(OracleConnection connection, string schema,
            string syncSource, string syncTarget, string syncOperation, HashSet<string> syncPkColumns,
            IEnumerable<Tuple<string, string, string, object[]>> parameters, int arrayLength, bool deleted)
        {
            switch (syncOperation)
            {
                case DeleteTag:
                case MergeTag:
                    {
                        var commandParams = parameters;

                        var updateDeletedOnMerge = false;
                        var deleteOnMerge = false;
                        if (deleted)
                        {
                            updateDeletedOnMerge = UpdateDeletedOnMerge(syncTarget);
                            deleteOnMerge = DeleteOnMerge(syncTarget);
                        }

                        var inserting = commandParams.Where(p => string.IsNullOrEmpty(p.Item3) || p.Item3 == "I").Select(p => p.Item1).ToList();
                        var updating = commandParams.Where(p => string.IsNullOrEmpty(p.Item3) || p.Item3 == "U").Select(p => p.Item1).Except(syncPkColumns).ToList();
                        if (!updateDeletedOnMerge && !deleteOnMerge)
                        {
                            inserting.Remove(DeleteParamName);
                            updating.Remove(DeleteParamName);
                        }

                        if (syncOperation == DeleteTag && !updating.Any())
                            return null;

                        var commandSql = _commands.GetOrAdd(schema + "." + syncSource + ":" + MergeTag, (id) =>
                        {
                            var sql = new StringBuilder();

                            Func<string, string, StringBuilder, HashSet<string>, IEnumerable<string>, IEnumerable<string>, bool> builder;
                            if (!_builders.TryGetValue(syncTarget, out builder) ||
                                !builder(syncOperation, syncTarget, sql, syncPkColumns, inserting, updating))
                            {
                                sql.AppendFormat("MERGE INTO {0} TEMP USING DUAL ON (", syncTarget);
                                sql.AppendLine();
                                sql.Append(string.Join(" AND ", syncPkColumns.Select(p => "TEMP." + p + " = :" + p)));
                                sql.AppendLine(")");

                                if (updating.Any())
                                {
                                    sql.Append("WHEN MATCHED THEN UPDATE SET");
                                    sql.AppendLine();
                                    if (deleted)
                                        sql.AppendLine(string.Join("," + Environment.NewLine,
                                            updating.Select(p => "TEMP." + p + " = " +
                                            (p == DeleteParamName ? ":" + p
                                            : string.Format("CASE WHEN :{0} = '{1}' THEN TEMP.{2} ELSE :{2} END", DeleteParamName, TrueValue, p)))));
                                    else
                                        sql.AppendLine(string.Join("," + Environment.NewLine,
                                            updating.Select(p => "TEMP." + p + " = :" + p)));
                                    if (deleted && deleteOnMerge)
                                    {
                                        sql.AppendFormat("DELETE WHERE :{0} = '{1}'", DeleteParamName, TrueValue);
                                        sql.AppendLine();
                                    }
                                }

                                sql.AppendLine("WHEN NOT MATCHED THEN INSERT (");
                                sql.Append(string.Join("," + Environment.NewLine, inserting));
                                sql.AppendLine(")");
                                sql.AppendLine("VALUES (");
                                sql.Append(string.Join("," + Environment.NewLine, inserting.Select(p => ":" + p)));
                                sql.AppendLine(")");
                                if (deleted)
                                    sql.AppendFormat("WHERE :{0} != '{1}'", DeleteParamName, TrueValue);
                            }

                            var commandText = sql.ToString(); 
                            Logger.TraceVerbose(0, "Sync command {CommandId} builded" + Environment.NewLine + "{CommandText}", id, commandText);

                            return commandText;
                        });

                        var command = connection.CreateCommand();
                        command.BindByName = true;
                        command.CommandText = commandSql.Replace("\r\n", "\n").ToString();
                        command.CommandTimeout = CommandTimeout;
                        GetSyncCommandParameters(command, commandParams, arrayLength);

                        return command;
                    }
                case InsertTag:
                    {
                        var commandParams = parameters.Skip(1);
                        var inserting = commandParams.Select(p => p.Item1);

                        var commandSql = _commands.GetOrAdd(schema + "." + syncSource + ":" + syncOperation, (id) =>
                        {
                            var sql = new StringBuilder();

                            Func<string, string, StringBuilder, HashSet<string>, IEnumerable<string>, IEnumerable<string>, bool> builder;
                            if (!_builders.TryGetValue(syncTarget, out builder) ||
                                !builder(syncOperation, syncTarget, sql, syncPkColumns, inserting, null))
                            {
                                sql.AppendFormat("MERGE INTO {0} TEMP USING DUAL ON (", syncTarget);
                                sql.AppendLine();
                                sql.Append(string.Join(" AND ", syncPkColumns.Select(p => "TEMP." + p + " = :" + p)));
                                sql.AppendLine(")");
                                sql.AppendLine("WHEN NOT MATCHED THEN INSERT (");
                                sql.Append(string.Join("," + Environment.NewLine, inserting));
                                sql.AppendLine(")");
                                sql.AppendLine("VALUES (");
                                sql.Append(string.Join("," + Environment.NewLine, inserting.Select(p => ":" + p)));
                                sql.Append(")");
                            }

                            var commandText = sql.ToString();
                            Logger.TraceVerbose(0, "Sync command {CommandId} builded" + Environment.NewLine + "{CommandText}", id, commandText);

                            return commandText;
                        });

                        var command = connection.CreateCommand();
                        command.BindByName = true;
                        command.CommandText = commandSql.Replace("\r\n", "\n").ToString();
                        command.CommandTimeout = CommandTimeout;
                        GetSyncCommandParameters(command, commandParams, arrayLength);

                        return command;
                    }
                case UpdateTag:
                    {
                        var commandParams = parameters.Skip(1);

                        var updating = commandParams.Select(p => p.Item1).Where(p => !syncPkColumns.Contains(p));
                        if (!updating.Any())
                            return null;

                        var commandSql = _commands.GetOrAdd(schema + "." + syncSource + ":" + syncOperation, (id) =>
                        {
                            var sql = new StringBuilder();

                            Func<string, string, StringBuilder, HashSet<string>, IEnumerable<string>, IEnumerable<string>, bool> builder;
                            if (!_builders.TryGetValue(syncTarget, out builder) ||
                                !builder(syncOperation, syncTarget, sql, syncPkColumns, null, updating))
                            {
                                sql.AppendFormat("UPDATE {0} SET", syncTarget);
                                sql.AppendLine();
                                sql.Append(string.Join("," + Environment.NewLine,
                                    updating.Select(p => p + " = :" + p)));
                                sql.AppendLine();
                                sql.Append("WHERE ");
                                sql.AppendLine(string.Join(" AND ", syncPkColumns.Select(p => p + " = :" + p)));
                            }

                            var commandText = sql.ToString();
                            Logger.TraceVerbose(0, "Sync command {CommandId} builded" + Environment.NewLine + "{CommandText}", id, commandText);

                            return commandText;
                        });

                        var command = connection.CreateCommand();
                        command.BindByName = true;
                        command.CommandText = commandSql.Replace("\r\n", "\n").ToString();
                        command.CommandTimeout = CommandTimeout;
                        GetSyncCommandParameters(command, commandParams, arrayLength);

                        return command;
                    }
                case ProcedureTag:
                    {
                        var command = connection.CreateCommand();
                        command.BindByName = true;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = syncTarget;
                        command.CommandTimeout = CommandTimeout;
                        GetSyncCommandParameters(command,
                            parameters.Where(p => !p.Item1.StartsWith(DeleteParamName)),
                            arrayLength);

                        return command;
                    }
            }

            return null;
        }

        private static IEnumerable<OracleCommand> BuildSyncBeginCommand(OracleConnection connection, string tableName)
        {
            var commands = new List<OracleCommand>();
            if (UpdateDeletedOnMerge(tableName))
            {
                var command = connection.CreateCommand();
                command.BindByName = true;
                command.CommandText = string.Format(UpdateSyncBeginCommandText, tableName);
                command.CommandTimeout = CommandTimeout;
                commands.Add(command);
            }

            return commands;
        }

        private static IEnumerable<OracleCommand> BuildSyncEndCommand(OracleConnection connection, string tableName)
        {
            var commands = new List<OracleCommand>();
            if (UpdateDeletedOnMerge(tableName))
            {
                var command = connection.CreateCommand();
                command.BindByName = true;
                command.CommandText = string.Format(UpdateSyncEndCommandText, tableName);
                command.CommandTimeout = CommandTimeout;
                commands.Add(command);

                command = connection.CreateCommand();
                command.BindByName = true;
                command.CommandText = string.Format(DeleteSyncEndCommandText, tableName);
                command.CommandTimeout = CommandTimeout;
                commands.Add(command);
            }

            return commands;
        }

        private static OracleCommand BuildSyncMarkCommand(OracleConnection connection, long syncId, DateTime? syncTimestamp = null)
        {
            var command = connection.CreateCommand();
            command.BindByName = true;
            command.CommandType = CommandType.Text;
            command.CommandText = syncTimestamp.HasValue ? UpdateSyncMarkTimestampCommandText : UpdateSyncMarkCommandText;
            command.CommandTimeout = CommandTimeout;

            var param = command.CreateParameter();
            param.Direction = ParameterDirection.Input;
            param.OracleDbType = OracleDbType.Int64;
            param.ParameterName = "hote_cont";
            param.Value = syncId;
            command.Parameters.Add(param);

            if (syncTimestamp.HasValue)
            {
                param = command.CreateParameter();
                param.Direction = ParameterDirection.Input;
                param.OracleDbType = OracleDbType.TimeStamp;
                param.ParameterName = "hote_sync";
                param.Value = syncTimestamp.Value;
                command.Parameters.Add(param);
            }

            return command;
        }

        private static OracleCommand BuildLastModifiedCommand(OracleConnection connection, string syncName, long syncLastModified)
        {
            var command = connection.CreateCommand();
            command.BindByName = true;
            command.CommandType = CommandType.Text;
            command.CommandText = UpdateSyncLastModifiedCommandText;
            command.CommandTimeout = CommandTimeout;

            var param = command.CreateParameter();
            param.Direction = ParameterDirection.Input;
            param.OracleDbType = OracleDbType.Varchar2;
            param.ParameterName = "sync_name";
            param.Value = syncName;
            command.Parameters.Add(param);

            param = command.CreateParameter();
            param.Direction = ParameterDirection.Input;
            param.OracleDbType = OracleDbType.TimeStamp;
            param.ParameterName = "sync_lmod";
            param.Value = new DateTime(syncLastModified, DateTimeKind.Utc);
            command.Parameters.Add(param);

            return command;
        }

        private static IEnumerable<Tuple<string, string, OracleCommand>> GetSyncCommands(OracleConnection connection,
            string schema, XmlDictionaryReader reader, bool deleted)
        {
            var commands = new List<Tuple<string, string, OracleCommand>>();

            if (!reader.EOF)
            {
                switch (reader.LocalName)
                {
                    case SyncMarkTag:
                        {
                            reader.ReadStartElement(SyncMarkTag);
                            var syncId = Convert.ToInt64(reader.ReadContentAsObject());
                            reader.ReadEndElement();

                            commands.Add(Tuple.Create("TNHT_HOTE", "U", BuildSyncMarkCommand(connection, syncId, DateTime.Now)));
                        }
                        break;
                    case SyncBeginTag:
                        {
                            reader.ReadStartElement(SyncBeginTag);
                            var syncTableName = reader.ReadContentAsString();
                            reader.ReadEndElement();

                            foreach (var command in BuildSyncBeginCommand(connection, syncTableName))
                                commands.Add(Tuple.Create(syncTableName, "B", command));
                        }
                        break;
                    case SyncEndTag:
                        {
                            reader.ReadStartElement(SyncEndTag);
                            var syncTableName = reader.ReadContentAsString();
                            reader.ReadEndElement();

                            foreach (var command in BuildSyncEndCommand(connection, syncTableName))
                                commands.Add(Tuple.Create(syncTableName, "E", command));
                        }
                        break;
                    case RowDefTag:
                        {
                            while (!reader.EOF && reader.LocalName == RowDefTag)
                            {
                                long? syncId = null;
                                var syncSource = reader.GetAttribute(SourceTag);
                                var syncTableName = reader.GetAttribute(TargetTag);
                                var syncPkColumns = reader.GetAttribute(PrimaryKeyTag);
                                var syncOperation = reader.GetAttribute(OperationTag) ?? MergeTag;

                                long syncLastModified = 0;

                                var fields = new Dictionary<string, Tuple<string, string>>();
                                fields.Add(DeleteParamName, Tuple.Create<string, string>(StringDbTypeTag, null));

                                reader.ReadStartElement(RowDefTag);
                                while (!reader.EOF && reader.LocalName != RowDefTag)
                                {
                                    var syncColumnDataType = reader.GetAttribute(DataTypeTag);
                                    var syncColumnDataOp = reader.GetAttribute(DataOpTag);
                                    var syncColumnData = Tuple.Create(syncColumnDataType, syncColumnDataOp);
                                    reader.ReadStartElement();
                                    fields.Add(reader.ReadContentAsString(), syncColumnData);
                                    reader.ReadEndElement();
                                }
                                reader.ReadEndElement();

                                var records = new List<object[]>();

                                reader.ReadStartElement(RowSetTag);
                                while (!reader.EOF && reader.LocalName != RowSetTag)
                                {
                                    var attr = reader.GetAttribute(SyncMarkTag);
                                    if (attr != null)
                                        syncId = long.Parse(attr);
                                    syncOperation = reader.GetAttribute(OperationTag) ?? MergeTag;

                                    long lastModified;
                                    if (long.TryParse(reader.GetAttribute(LastModifiedTag), out lastModified))
                                    {
                                        if (lastModified > syncLastModified)
                                            syncLastModified = lastModified;
                                    }

                                    var values = new object[fields.Count + 1];
                                    values[0] = syncOperation == DeleteTag ? TrueValue : FalseValue;

                                    var fieldCount = 0;
                                    reader.ReadStartElement(RowTag);
                                    while (!reader.EOF && reader.LocalName != RowTag)
                                    {
                                        var index = int.Parse(reader.GetAttribute(IndexTag), NumberStyles.Integer) + 1;
                                        reader.ReadStartElement();
                                        values[index] = reader.ReadContentAsObject();
                                        reader.ReadEndElement();
                                        fieldCount++;
                                    }
                                    reader.ReadEndElement();

                                    if (fieldCount > 0)
                                        records.Add(values);
                                }
                                reader.ReadEndElement();

                                if (records.Count > 0)
                                {
                                    var parameters = fields.Select(f =>
                                        Tuple.Create(f.Key, f.Value.Item1, f.Value.Item2, new object[records.Count]))
                                        .ToArray();

                                    int j = 0;
                                    foreach (var record in records)
                                    {
                                        for (var i = 0; i < parameters.Length; i++)
                                            parameters[i].Item4[j] = record[i];
                                        j++;
                                    }
        
                                    var command = BuildSyncCommand(connection, schema, syncSource, syncTableName, syncOperation,
                                        new HashSet<string>(syncPkColumns.Split(',')), parameters, records.Count, deleted);

                                    if (command != null)
                                        commands.Add(Tuple.Create(syncTableName, syncOperation, command));

                                    if (syncLastModified > 0L)
                                        commands.Add(Tuple.Create("TNHT_SYNC", "U", BuildLastModifiedCommand(connection, syncTableName, syncLastModified)));
                                }

                                if (syncId.HasValue)
                                    commands.Add(Tuple.Create("TNHT_HOTE", "U", BuildSyncMarkCommand(connection, syncId.Value)));
                            }
                        }
                        break;
                }
            }

            return commands;
        }

        #region Manager

        public static void ManagerInit()
        {
            KeyNeeded = !string.IsNullOrEmpty(ApiKey);
            if (!string.IsNullOrEmpty(BaseUrl))
                restClient = new RestClient(BaseUrl) { Timeout = -1 };
        }

        public static BaseTokenResponse AuthenticateContextCredential(ContextCredentialsModel model)
        {
            var request = new RestRequest(RouteAuthCredentialContext, Method.POST);
            request.AddHeader("Content-Type", "application/json");
            if (KeyNeeded)
            {
                request.AddHeader("Ocp-Apim-Subscription-Key", ApiKey);
                request.AddHeader("Ocp-Apim-Trace", "true");
            }
            request.AddJsonBody(model);

            try
            {
                IRestResponse response = restClient.Execute(request);
                var responseData = JsonConvert.DeserializeObject<BaseTokenResponse>(response.Content, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });
                return responseData;
            }
            catch (Exception e)
            {
                Logger.TraceError(0, "Error trying to authenticate context in manager: {0}.", e.Message);
                return null;
            }
        }

        public static HotelModelResponse GetHotelById(string id, string accessToken)
        {
            var request = new RestRequest(RouteHotelGetById, Method.GET);
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            request.AddHeader("Content-Type", "application/json");
            if (KeyNeeded)
            {
                request.AddHeader("Ocp-Apim-Subscription-Key", ApiKey);
                request.AddHeader("Ocp-Apim-Trace", "true");
            }
            request.AddParameter("id", id);

            try
            {
                IRestResponse response = restClient.Execute(request);
                var responseData = JsonConvert.DeserializeObject<HotelModelResponse>(response.Content, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });
                return responseData;
            }
            catch (Exception e)
            {
                Logger.TraceError(0, "Error trying to get hotel by id in manager: {0}.", e.Message);
                return null;
            }
        }

        #endregion Manager

        #endregion
        #region Public Methods

        public static void SetConnection(string service, string schema, string password)
        {
            Service = service;
            Schema = schema;
            Password = password;
        }

        public static void SetManagerConnection(string baseUrl, string apiKey, string username, string password)
        {
            BaseUrl = baseUrl;
            ApiKey = apiKey;
            Username = username;
            Password = password;
        }

        public static string GetConnectionString(string schema, string password, string service, bool pooling = true)
        {
            return string.Format(ConnectionStringFormat, schema, password, service, MinPoolSize, MaxPoolSize, pooling ? "true" : "false");
        }

        public static string ConnectionString
        {
            get { return GetConnectionString(Schema, Password, Service, false); }
        }

        public static bool TryGetConnection(Guid connectionId, SyncProvider provider,
            out string connectionSchema, out string connectionName, out string connectionString)
        {
            var ci = Tuple.Create(string.Empty, string.Empty, string.Empty, string.Empty);
            connectionSchema = ci.Item1;
            connectionName = ci.Item4;
            connectionString = string.Empty;

            ci = _connections.GetOrAdd(connectionId, (id) =>
            {
                var tuple = provider.GetConnection(id);
                if (string.IsNullOrEmpty(tuple.Item1))
                    Logger.TraceWarning(0, "Hotel {Id} not found", id.ToHex());
                else
                    Logger.TraceInformation(0, "Hotel {Id} found: {Service}.{Schema}", id.ToHex(), tuple.Item3, tuple.Item1);

                return tuple;
            });

            if (!string.IsNullOrEmpty(ci.Item1))
            {
                connectionSchema = ci.Item1;
                connectionName = ci.Item4;
                connectionString = GetConnectionString(ci.Item1, ci.Item2, ci.Item3);

                return true;
            }

            return false;
        }

        public static string GetConnectionString(Guid id, SyncProvider provider)
        {
            string connectionSchema;
            string connectionName;
            string connectionString;
            if (!TryGetConnection(id, provider, out connectionSchema, out connectionName, out connectionString))
                throw new ApplicationException(string.Format("Connection not found: {0}", id.ToHex()));

            return connectionString;
        }

        public static bool AddCommandBuilder(string name,
            Func<string, string, StringBuilder, HashSet<string>, IEnumerable<string>, IEnumerable<string>, bool> builder)
        {
            return _builders.TryAdd(name, builder);
        }

        public static void CreateCommandCursors(OracleCommand command, int count)
        {
            for (int index = 0; index < count; index++)
            {
                var param = command.CreateParameter();
                param.ParameterName = "p_cursor" + index.ToString();
                param.OracleDbType = OracleDbType.RefCursor;
                param.Direction = ParameterDirection.Output;
                command.Parameters.Add(param);
            }
        }

        public static IEnumerable<Tuple<string, string, OracleCommand>> GetSyncCommands(OracleConnection connection,
            string schema, Message message, bool deleted)
        {
            using (var reader = message.GetReaderAtBodyContents())
            {
                return GetSyncCommands(connection, schema, reader, deleted);
            }
        }

        private static void ApplySyncCommands(OracleConnection connection, string schema,
            XmlDictionaryReader reader, Message message, TraceSource logger, bool deleted)
        {
            using (var transaction = connection.BeginTransaction())
            {
                var commands = GetSyncCommands(connection, schema, reader, deleted);

                if (commands.Any())
                {
                    try
                    {
                        var traceVerbose = logger.Switch.ShouldTrace(TraceEventType.Verbose);
                        foreach (var command in commands)
                        {
                            logger.TraceInformation(0, "Sync applying changes to {Schema}.{TableName}", schema, command.Item1);

                            var rowsAffected = command.Item3.ExecuteNonQuery();
                            if (traceVerbose)
                            {
                                var commandId = schema + "." + command.Item1 + ":" + command.Item2;
                                if (rowsAffected < 0)
                                    logger.TraceVerbose(0, "Sync command {CommandId} executed.", commandId);
                                else
                                    logger.TraceVerbose(0, "Sync command {CommandId} executed. " + rowsAffected.ToString() + " rows affected", commandId);
                            }
                        }

                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                    finally
                    {
                        foreach (var command in commands)
                        {
                            foreach (OracleParameter parameter in command.Item3.Parameters)
                                parameter.Dispose();

                            command.Item3.Dispose();
                        }
                    }
                }
            }
        }

        public static void ApplySyncCommands(OracleConnection connection, string schema,
            Message message, TraceSource logger, bool deleted = false)
        {
            using (var reader = message.GetReaderAtBodyContents())
            {
                ApplySyncCommands(connection, schema, reader, message, logger, deleted);
            }
        }

        public static Guid ApplySyncCommands(SyncProvider provider, Message message, bool verbose, bool deleted = false)
        {
            MessageBuffer messageBuffer = null;
            if (verbose)
            {
                // buffered copy to trace
                messageBuffer = message.CreateBufferedCopy(int.MaxValue);
                message = messageBuffer.CreateMessage();
            }

            using (var reader = message.GetReaderAtBodyContents())
            {
                var id = Guid.Empty;
                if (!reader.EOF && reader.LocalName == IdTag)
                {
                    reader.ReadStartElement(IdTag);
                    id = Convert.ToString(reader.ReadContentAsObject()).ToGuid();
                    reader.ReadEndElement();
                }

                if (id != Guid.Empty)
                {
                    var logger = Logger;
                    using (logger.TraceScope(id))
                    {
                        if (messageBuffer != null)
                            logger.TraceData(0, "Sync message:", messageBuffer.CreateMessage());

                        string connectionSchema;
                        string connectionName;
                        string connectionString;
                        if (TryGetConnection(id, provider, out connectionSchema, out connectionName, out connectionString))
                        {
                            using (var connection = new OracleConnection(connectionString))
                            {
                                connection.Open();
                                connection.ActionName = "ApplySyncCommands";
                                try
                                {
                                    ApplySyncCommands(connection, connectionSchema, reader, message, logger, deleted);
                                }
                                finally
                                {
                                    connection.ActionName = null;
                                    connection.Close();
                                }
                            }
                        }
                    }
                }

                return id;
            }
        }

        #region Messages

        public static Message GetFaultMessage(Exception exception, string action)
        {
            return Message.CreateMessage(MessageVersion.Soap12WSAddressing10,
                new FaultException(exception.Message).CreateMessageFault(), action);
        }

        public static Message GetEmptyMessage(string action)
        {
            return Message.CreateMessage(MessageVersion.Soap12WSAddressing10, action);
        }

        public static Message GetEmptyMessage()
        {
            return GetEmptyMessage(SyncChangesAction);
        }
        
        public static Message GetSyncMessage(OracleDataReader reader, long size, Guid? id, string source, params string[] syncNames)
        {
            return Message.CreateMessage(MessageVersion.Soap12WSAddressing10,
                SyncChangesAction, new SyncTableBodyWriter(reader, size, id, source, syncNames));
        }

        public static Message GetSyncMessage(OracleDataReader reader, Guid? id, string source, params string[] syncNames)
        {
            return Message.CreateMessage(MessageVersion.Soap12WSAddressing10,
                SyncChangesAction, new SyncTableBodyWriter(reader, 0L, id, source, syncNames));
        }

        public static Message GetSyncMessage(long syncId)
        {
            return Message.CreateMessage(MessageVersion.Soap12WSAddressing10,
                SyncChangesAction, new SyncIdBodyWriter(syncId));
        }

        public static Message GetAckSyncMessage()
        {
            return _ackMessageBuffer.CreateMessage();
        }

        public static Message GetNakSyncMessage()
        {
            return _nakMessageBuffer.CreateMessage();
        }

        public static Message GetStartSyncTableMessage(string name)
        {
            return Message.CreateMessage(MessageVersion.Soap12WSAddressing10,
                SyncChangesAction, new SyncCommandBodyWriter(SyncBeginTag, name));
        }

        public static Message GetEndSyncTableMessage(string name)
        {
            return Message.CreateMessage(MessageVersion.Soap12WSAddressing10,
                SyncChangesAction, new SyncCommandBodyWriter(SyncEndTag, name));
        }

        #endregion

        #endregion
    }
}