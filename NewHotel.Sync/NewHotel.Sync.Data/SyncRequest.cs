﻿using System;
using System.ServiceModel.Channels;
using System.Runtime.Serialization;

namespace NewHotel.Sync.Data
{
    [DataContract]
    public class SyncRequest
    {
        #region Constants

        //public const string SyncRequestAction = "http://tempuri.org/ISyncService/GetSyncChanges";
        public const string SyncRequestAction = "GetSyncChanges";

        #endregion
        #region Members

        private readonly static DataContractSerializer _serializer = new DataContractSerializer(typeof(SyncRequest));

        [DataMember]
        public readonly Guid Id;
        [DataMember]
        public readonly string Name;
        [DataMember]
        public readonly long LastModified;
        [DataMember]
        public readonly long Count;

        #endregion
        #region Public Methods

        public SyncRequest(Guid id, string name, long lastModified, long count)
        {
            Id = id;
            Name = name;
            LastModified = lastModified;
            Count = count;
        }

        public Message CreateMessage()
        {
            return Message.CreateMessage(MessageVersion.Soap12WSAddressing10, SyncRequestAction, this, _serializer);
        }

        public static SyncRequest GetBody(Message message)
        {
            return message.GetBody<SyncRequest>(_serializer);
        }

        #endregion
    }
}