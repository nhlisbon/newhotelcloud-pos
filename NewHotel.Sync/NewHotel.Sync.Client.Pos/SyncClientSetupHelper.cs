﻿using System.IO;
using System.Data;
using System.Reflection;
using System.ComponentModel.Composition;
using Oracle.ManagedDataAccess.Client;
using NewHotel.Sync.Core;

namespace NewHotel.Sync.Server
{
    [Export(typeof(ISyncSetupHelper)), PartCreationPolicy(CreationPolicy.Shared)]
    public class SyncClientSetupHelper : SyncSetupHelper
    {
        protected override Stream GetManifestResourceStream(string name)
        {
            return Assembly.GetExecutingAssembly().GetManifestResourceStream(name);
        }

        protected override IDbConnection GetConnection(string connectionString)
        {
            return new OracleConnection(connectionString);
        }

        protected override void Uninstall(string serviceName, string schemaName, string schemaPassword, string sysPassword)
        {
            RevokeRights(serviceName, sysPassword, schemaName);
        }

        protected override void Install(string serviceName, string schemaName, string schemaPassword, string sysPassword)
        {
            GrantRights(serviceName, sysPassword, schemaName);
        }
    }
}