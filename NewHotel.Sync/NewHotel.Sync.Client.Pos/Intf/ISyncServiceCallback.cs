﻿using System.ServiceModel;
using System.ServiceModel.Channels;

namespace NewHotel.Sync.Client
{
    public interface ISyncServiceCallBack
    {
        [OperationContract(IsOneWay = true, Action = "SyncChanges")]
        void OnSyncChanges(Message message);
    }
}