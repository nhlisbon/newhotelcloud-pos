﻿using System;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace NewHotel.Sync.Client
{
    [ServiceContract(CallbackContract = typeof(ISyncServiceCallBack), SessionMode = SessionMode.Required)]
    public interface ISyncService
    {
        [OperationContract(IsOneWay = true)]
        void StartSync(Guid id, long syncCounter);

        [OperationContract(IsOneWay = true)]
        void StopSync(Guid id);

        [OperationContract(IsOneWay = true)]
        void UpdateSync(Guid id, long syncCounter);

        [OperationContract(Action = "SyncChanges", ReplyAction = "*")]
        Message SyncChanges(Message message);

        [OperationContract(Action = "GetSyncChanges", ReplyAction = "*")]
        Message GetSyncChanges(Message request);

        [OperationContract()]
        Stream GetImageFile(Guid id, string path);

        #region Versioning Methods

        [OperationContract()]
        int GetVersion();

        #endregion
    }
}