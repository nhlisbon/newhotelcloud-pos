﻿#define dcn
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Data;
using System.Net;
using System.Reflection;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ComponentModel.Composition;
using System.Threading;
using System.Diagnostics;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using NewHotel.Sync.Core;
using NewHotel.Sync.Data;
using NewHotel.Diagnostics;

namespace NewHotel.Sync.Client
{
    [Export(typeof(ISyncClientProvider)), PartCreationPolicy(CreationPolicy.NonShared)]
    public class SyncClientProvider : ISyncClientProvider, ISyncServiceCallBack, IDisposable
    {
        private const string url = "net.tcp://{0}:{1}/NewHotel.Sync.Server/SyncService/";
        private const string ConnectionStringFormat = "User Id={0};Password={1};Data Source={2};Min Pool Size={3};Max Pool Size={4};Pooling={5};Connection Lifetime=180";
        private const int MinPoolSize = 1;
        private const int MaxPoolSize = 10;
        private const string ImageDirectory = "Images";
        private const string PosEventName = "NHCLOUDPOS";

        private Guid _id;
        private TraceSource _logger;
        private ClientSyncTask _syncTask;
        private DateTime? _disconnectedSince = DateTime.Now;       
        private Proxy<ISyncService> _proxy;

        private static TimeSpan  _failRetry = TimeSpan.FromMinutes(5);
        private static readonly TimeSpan _syncDelay = TimeSpan.FromMinutes(1);
        private static object _syncObject = new object();
        private static OracleDependency _dependency = null;

        private static readonly string GetIdCommandText;
        private static readonly string GetDbVersionsCommandText;
        private static readonly string GetSyncCounterCommandText;
        private static readonly string GetImagesChangeLogCommandText;
        private static readonly string GetSyncTablesCommandText;
        private static readonly string GetSyncProcsCommandText;
        private static readonly string UpdateImagesChangeLogCommandText;
        private static readonly string UpdateSyncTimestampCommandText;

        private static string Service;
        private static string Schema;
        private static string Password;
        private static string Server;
        private static string Port;
        private static string ImageRootPath;

        static SyncClientProvider()
        {
            ServicePointManager.SetTcpKeepAlive(true, 180000, 60000);

            GetIdCommandText = "SELECT hote_pk FROM tnht_hote";
            GetDbVersionsCommandText = "SELECT date_vers, sync_vers FROM tcfg_gene";
            GetSyncCounterCommandText = "SELECT CASE WHEN hote_sync is null THEN 0 ELSE hote_cont END FROM tnht_hote";
            GetSyncTablesCommandText = "SELECT sync_name, sync_lmod, sync_size FROM tnht_sync ORDER BY sync_pk";
            GetSyncProcsCommandText = "SELECT proc_name, proc_return FROM vnht_sync";

            var sql = new StringBuilder();
            sql.AppendLine("SELECT ROWIDTOCHAR(rowid) as row_id, imag_path FROM tnht_imag");
            sql.AppendLine("WHERE imag_path is not null AND imag_sync is not null AND rownum <= 100");
            sql.Append("ORDER BY imag_sync");
            GetImagesChangeLogCommandText = sql.Replace("\r\n", "\n").ToString();

            UpdateSyncTimestampCommandText = "UPDATE tnht_hote SET hote_sync = SYSTIMESTAMP WHERE hote_sync is null";
            UpdateImagesChangeLogCommandText = "UPDATE tnht_imag SET imag_sync = null WHERE rowid = :row_id";
        }

        public static void SetConnection(string service, string schema, string password)
        {
            Service = service;
            Schema = schema;
            Password = password;
        }

        private void ReadConfig()
        {
            var service = SyncClientConfig.Instance.Service;
            var schema = SyncClientConfig.Instance.Schema;
            var password = SyncClientConfig.Instance.Password;

            SetConnection(service, schema, password);
            Logger.TraceInformation(0, "Connection config: {@Connection}", new { Service = service, Schema = schema, Password = password });

            Server = SyncClientConfig.Instance.Server;
            Port = SyncClientConfig.Instance.Port;
            Logger.TraceInformation(0, "Endpoint config: " + string.Format(url, Server, Port));

            ImageRootPath = GetImageRootPath(SyncClientConfig.Instance.ImageRootPath);
            Logger.TraceInformation(0, "Image root path: {ImageRootPath}", ImageRootPath);
        }

        private static string GetImageRootPath(string imageRootPath)
        {
            if (string.IsNullOrEmpty(imageRootPath))
            {
                var currPath = Directory.GetCurrentDirectory();
                if (!Directory.Exists(Path.Combine(currPath, ImageDirectory)))
                    Directory.CreateDirectory(ImageDirectory);

                imageRootPath = Path.Combine(currPath, ImageDirectory);
            }

            return imageRootPath;
        }

        private static string ConnectionString
        {
            get { return string.Format(ConnectionStringFormat, Schema, Password, Service, MinPoolSize, MaxPoolSize, "true"); }
        }

        private static Uri Uri
        {
            get { return new Uri(string.Format(url, Server, Port)); }
        }

        protected Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }

        protected TraceSource Logger
        {
            get
            {
                if (_logger == null)
                    _logger = Log.Source["NewHotel.Sync"];

                return _logger;
            }
        }

        private static Guid? GetId(OracleConnection connection)
        {
            connection.ActionName = "GetId";
            using (var command = connection.CreateCommand())
            {
                command.CommandText = GetIdCommandText;

                using (var reader = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    try
                    {
                        if (reader.Read())
                        {
                            var bytes = new byte[16];
                            reader.GetBytes(0, 0, bytes, 0, 16);
                            return new Guid(bytes);
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }

            return null;
        }

        private static bool TryGetDbVersions(OracleConnection connection, out int dbVersion, out int dbSyncVersion)
        {
            dbVersion = 0;
            dbSyncVersion = 0;

            connection.ActionName = "GetDbVersions";
            using (var command = connection.CreateCommand())
            {
                command.CommandText = GetDbVersionsCommandText;

                using (var reader = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    try
                    {
                        if (reader.Read())
                        {
                            int.TryParse(reader.GetString(0), out dbVersion);
                            int.TryParse(reader.GetString(1), out dbSyncVersion);
                            return true;
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }

                return false;
            }
        }

        private static bool TryGetSyncCounter(OracleConnection connection, out long syncCounter)
        {
            syncCounter = -1L;

            connection.ActionName = "GetSyncCounter";
            using (var command = connection.CreateCommand())
            {
                command.CommandText = GetSyncCounterCommandText;

                using (var reader = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    try
                    {
                        if (reader.Read())
                        {
                            syncCounter = reader.GetInt64(0);
                            return true;
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }

            return false;
        }

        private static IDictionary<string, string> GetImagesChangeLog(OracleConnection connection)
        {
            var changeLog = new Dictionary<string, string>();

            using (var command = connection.CreateCommand())
            {
                command.CommandType = CommandType.Text;
                command.CommandText = GetImagesChangeLogCommandText;

                using (var reader = command.ExecuteReader(CommandBehavior.SingleResult))
                {
                    try
                    {
                        while (reader.Read())
                            changeLog.Add(reader.GetString(0), reader.GetString(1));
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }

            return changeLog;
        }

        private static IList<Tuple<string, long, long>> GetTablesToSync(OracleConnection connection)
        {
            var tableLog = new List<Tuple<string, long, long>>();

            using (var command = connection.CreateCommand())
            {
                command.CommandType = CommandType.Text;
                command.CommandText = GetSyncTablesCommandText;

                connection.ActionName = "GetTablesSync";
                using (var reader = command.ExecuteReader(CommandBehavior.SingleResult))
                {
                    try
                    {
                        while (reader.Read())
                            tableLog.Add(Tuple.Create(reader.GetString(0),
                                reader.IsDBNull(1) ? 0L : reader.GetDateTime(1).Ticks, reader.GetInt64(2)));
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }

            return tableLog;
        }

        private bool UpdateImageChangeLog(OracleConnection connection, string rowId)
        {
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.BindByName = true;
                    command.CommandType = CommandType.Text;
                    command.CommandText = UpdateImagesChangeLogCommandText;

                    var param = command.CreateParameter();
                    param.ParameterName = "row_id";
                    param.Direction = ParameterDirection.Input;
                    param.OracleDbType = OracleDbType.Varchar2;
                    param.Value = rowId;
                    command.Parameters.Add(param);

                    return command.ExecuteNonQuery() == 1;
                }
            }
            catch (OracleException ex)
            {
                Logger.TraceError(0, "Database error: {@Exception}", new { Code = ex.ErrorCode, Message = ex.Message, Connection = ConnectionString });
                return false;
            }
        }

        private void UpdateSyncTimestamp(OracleConnection connection)
        {
            var transaction = connection.BeginTransaction();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = UpdateSyncTimestampCommandText;
                    command.ExecuteNonQuery();
                }

                transaction.Commit();
            }
            catch (OracleException ex)
            {
                transaction.Rollback();
                Logger.TraceError(0, "Database error: {@Exception}", new { Code = ex.ErrorCode, Message = ex.Message, Connection = ConnectionString });
            }
        }

        private void OnProxyOpening(object sender, EventArgs e)
        {
            Debug.WriteLine("Proxy opening ...");
        }

        private void OnProxyOpened(object sender, EventArgs e)
        {
            _disconnectedSince = null;

            Debug.WriteLine("Proxy opened");
        }

        private void OnProxyClosing(object sender, EventArgs e)
        {
            Debug.WriteLine("Proxy closing ...");
        }

        private void OnProxyClosed(object sender, EventArgs e)
        {
            if (!_disconnectedSince.HasValue)
                _disconnectedSince = DateTime.Now;

            Debug.WriteLine("Proxy closed");
        }

        private void OnProxyFaulted(object sender, EventArgs e)
        {
            if (!_disconnectedSince.HasValue)
                _disconnectedSince = DateTime.Now;
            Debug.WriteLine("Proxy faulted");

            if (_syncTask != null)
                _syncTask.Set(PosEventName);
        }

        private static void UpdateSync(OracleConnection connection, long index, object pk, TimeSpan timespan)
        {
            var transaction = connection.BeginTransaction();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.BindByName = true;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "PNHT_SYNC_UPDATE";

                    using (var indexParam = command.CreateParameter())
                    {
                        indexParam.ParameterName = "p_index";
                        indexParam.Direction = ParameterDirection.Input;
                        indexParam.OracleDbType = OracleDbType.Int64;
                        indexParam.Value = index;
                        command.Parameters.Add(indexParam);

                        using (var pkParam = command.CreateParameter())
                        {
                            pkParam.ParameterName = "p_pk";
                            pkParam.Direction = ParameterDirection.Input;
                            pkParam.OracleDbType = OracleDbType.Raw;
                            pkParam.Size = 16;
                            pkParam.Value = pk;
                            command.Parameters.Add(pkParam);

                            using (var intervalParam = command.CreateParameter())
                            {
                                intervalParam.ParameterName = "p_interval";
                                intervalParam.Direction = ParameterDirection.Input;
                                intervalParam.OracleDbType = OracleDbType.IntervalDS;
                                if (timespan != TimeSpan.Zero)
                                    intervalParam.Value = timespan;
                                else
                                    intervalParam.Value = DBNull.Value;
                                command.Parameters.Add(intervalParam);

                                command.ExecuteNonQuery();
                            }
                        }
                    }

                    transaction.Commit();
                }
            }
            catch
            {
                transaction.Rollback();
                throw;
            }
        }

        private static IEnumerable<Tuple<string, string>> GetSyncProcs(OracleConnection connection)
        {
            var syncProcs = new List<Tuple<string, string>>();

            using (var command = connection.CreateCommand())
            {
                command.CommandText = GetSyncProcsCommandText;

                connection.ActionName = "GetTablesSync";
                using (var reader = command.ExecuteReader(CommandBehavior.SingleResult))
                {
                    try
                    {
                        while (reader.Read())
                            syncProcs.Add(Tuple.Create(reader.GetString(0), reader.GetString(1)));
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }

            return syncProcs;
        }

        private void OnChangeNotificaton(object source, OracleNotificationEventArgs arg)
        {
            var dependency = (OracleDependency)source;
            if (_syncTask != null)
            {
                _syncTask.Set(PosEventName);
                if (_syncTask.Logger.Switch.ShouldTrace(TraceEventType.Verbose))
                {
                    for (int index = 0; index < arg.ResourceNames.Length; index++)
                        Logger.TraceVerbose(0, "Change notification for {TableName}", arg.ResourceNames[index]);
                }
            }
        }

        private Proxy<ISyncService> CreateProxy()
        {
            var netTcpBinding = new NetTcpBinding(SecurityMode.None);
            netTcpBinding.Security.Message.ClientCredentialType = MessageCredentialType.None;
            //netTcpBinding.Security.Transport.ClientCredentialType = TcpClientCredentialType.Certificate;
            netTcpBinding.Security.Transport.ClientCredentialType = TcpClientCredentialType.None;
            //netTcpBinding.Security.Transport.ProtectionLevel = System.Net.Security.ProtectionLevel.EncryptAndSign;
            netTcpBinding.Security.Transport.ProtectionLevel = System.Net.Security.ProtectionLevel.None;
            //var netTcpEndpoint = new EndpointAddress(Uri, EndpointIdentity.CreateDnsIdentity("localhost"));
            var netTcpEndpoint = new EndpointAddress(Uri);
            netTcpBinding.ReceiveTimeout = TimeSpan.FromHours(1);
            netTcpBinding.SendTimeout = TimeSpan.FromMinutes(3);
            netTcpBinding.OpenTimeout = TimeSpan.FromSeconds(5);
            netTcpBinding.CloseTimeout = TimeSpan.FromSeconds(5);
            //netTcpBinding.MaxBufferPoolSize = int.MaxValue;
            //netTcpBinding.MaxBufferSize = int.MaxValue;
            //netTcpBinding.MaxConnections = int.MaxValue;
            netTcpBinding.MaxReceivedMessageSize = int.MaxValue;
            netTcpBinding.ReaderQuotas.MaxArrayLength = int.MaxValue;
            //netTcpBinding.ListenBacklog = int.MaxValue;
            //netTcpBinding.PortSharingEnabled = false;
            //netTcpBinding.TransactionFlow = false;

            var channelFactory = new DuplexChannelFactory<ISyncService>(
                new InstanceContext(this), netTcpBinding, netTcpEndpoint);
            //channelFactory.Credentials.ClientCertificate.SetCertificate(StoreLocation.LocalMachine, StoreName.My,
            //    X509FindType.FindBySerialNumber, "‎675053c2fa59279e4d1e3be9d041623f");
            //channelFactory.Credentials.ServiceCertificate.Authentication.RevocationMode = X509RevocationMode.NoCheck;

            var proxy = new Proxy<ISyncService>(channelFactory.CreateChannel(), TimeSpan.FromHours(1));

            proxy.Opening += new EventHandler(OnProxyOpening);
            proxy.Opened += new EventHandler(OnProxyOpened);
            proxy.Closed += new EventHandler(OnProxyClosed);
            proxy.Closing += new EventHandler(OnProxyClosing);
            proxy.Faulted += new EventHandler(OnProxyFaulted);

            return proxy;
        }

        private static void RegisterNotification(OracleConnection connection, OracleDependency dependency, string commandText)
        {
            using (var command = connection.CreateCommand())
            {
                command.CommandText = commandText;
                dependency.AddCommandDependency(command);
                command.Notification.IsNotifiedOnce = false;
                command.Notification.IsPersistent = false;
                command.Notification.Timeout = 0;
                command.ExecuteNonQuery();
            }
        }

        private OracleDependency RegisterNotifications()
        {
            using (var connection = new OracleConnection(ConnectionString))
            {
                connection.Open();
                connection.ActionName = "RegisterNotification";
                try
                {
                    var dependency = new OracleDependency();
                    dependency.OnChange += OnChangeNotificaton;
                    dependency.QueryBasedNotification = true;

                    RegisterNotification(connection, dependency, "SELECT hote_sync FROM TNHT_HOTE");
                    RegisterNotification(connection, dependency, "SELECT vend_sync FROM TNHT_VEND");
                    RegisterNotification(connection, dependency, "SELECT mesa_sync FROM TNHT_MESA");
                    RegisterNotification(connection, dependency, "SELECT sedo_sync FROM TNHT_SEDO");

                    return dependency;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        private bool UnregisterNotifications(OracleDependency dependency)
        {
            using (var connection = new OracleConnection(ConnectionString))
            {
                connection.Open();
                connection.ActionName = "UnregisterNotification";
                try
                {
                    dependency.RemoveRegistration(connection);
                    return true;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        private bool DoSyncToServer(OracleConnection connection, CancellationToken cancellationToken, Guid id, string schema)
        {
            var hasRows = false;
            var syncProcs = GetSyncProcs(connection).ToArray();
            
            for (int index = 0; index < syncProcs.Length; index++)
            {
                try
                {
                    var syncProc = syncProcs[index];
                    var procName = syncProc.Item1;
                    var cursorNames = syncProc.Item2.Split(',').ToArray();

                    connection.ActionName = "GetChangedData: " + procName;

                    using (var command = connection.CreateCommand())
                    {
                        command.BindByName = true;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = procName;

                        using (var pkParam = command.CreateParameter())
                        {
                            pkParam.ParameterName = "p_pk";
                            pkParam.OracleDbType = OracleDbType.Raw;
                            pkParam.Size = 16;
                            pkParam.Direction = ParameterDirection.Output;
                            command.Parameters.Add(pkParam);

                            SyncUtil.CreateCommandCursors(command, cursorNames.Length);

                            using (var reader = command.ExecuteReader())
                            {
                                try
                                {
                                    var pkValue = (OracleBinary)pkParam.Value;
                                    if (!pkValue.IsNull)
                                    {
                                        hasRows = true;
                                        var message = SyncUtil.GetSyncMessage(reader, int.MaxValue, id, null, cursorNames);

                                        Logger.TraceInformation(0, "Sending changes from {Schema}.{TableName}", schema, procName);
                                        var response = _proxy.Channel.SyncChanges(message);

                                        if (response.IsFault)
                                        {
                                            Logger.TraceWarning(0, "Changes from {Schema}.{TableName} failed", schema, procName);

                                            connection.ActionName = "UpdateFaultSync";
                                            UpdateSync(connection, index, pkValue.Value, _failRetry);
                                        }
                                        else
                                        {
                                            Logger.TraceInformation(0, "Changes from {Schema}.{TableName} applied", schema, procName);

                                            connection.ActionName = "UpdateSucceedSync";
                                            UpdateSync(connection, index, pkValue.Value, TimeSpan.Zero);
                                        }
                                    }
                                }
                                finally
                                {
                                    if (!reader.IsClosed)
                                        reader.Close();
                                }
                            }
                        }
                    }
                }
                catch (OracleException ex)
                {
                    Logger.TraceError(0, "Database error: {@Exception}", new { Code = ex.ErrorCode, Message = ex.Message, Connection = ConnectionString });
                }

                if (cancellationToken.IsCancellationRequested)
                    break;
            }

            return hasRows;
        }

        private bool DoSyncFromServer(OracleConnection connection, CancellationToken cancellationToken, Guid id, string schema)
        {
            try
            {
                foreach (var tableSync in GetTablesToSync(connection))
                {
                    var request = new SyncRequest(id, tableSync.Item1, tableSync.Item2, tableSync.Item3);
                    var message = _proxy.Channel.GetSyncChanges(request.CreateMessage());

                    if (message.IsFault)
                        Logger.TraceWarning(0, "Sync {Schema}.{TableName} failed", schema, tableSync.Item1);
                    else if (!message.IsEmpty)
                    {
                        MessageBuffer messageBuffer = null;
                        if (Logger.Switch.ShouldTrace(TraceEventType.Verbose))
                        {
                            // buffered copy to trace
                            messageBuffer = message.CreateBufferedCopy(int.MaxValue);
                            Logger.TraceData(0, "Sync message:", messageBuffer.CreateMessage());
                        }

                        SyncUtil.ApplySyncCommands(connection, schema, messageBuffer != null
                            ? messageBuffer.CreateMessage() : message, Logger, true);
                        Logger.TraceInformation(0, "Sync {Schema}.{TableName} applied", schema, tableSync.Item1);

                        return true;
                    }

                    if (cancellationToken.IsCancellationRequested)
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.TraceError(0, "Error: {@Exception}", new { Message = ex.Message, Stack = ex.StackTrace });
            }

            return false;
        }

        private bool DoImageSync(OracleConnection connection, CancellationToken cancellationToken, Guid id, string schema)
        {
            connection.ActionName = "GetImagesChangeLog";
            var imagesChangeLog = GetImagesChangeLog(connection);

            foreach (var imageChangeLog in imagesChangeLog)
            {
                var imageFilePath = imageChangeLog.Value;
                Logger.TraceInformation(0, "Reading image {ImagePath} for {Schema}", imageFilePath, schema);

                var imageStream = _proxy.Channel.GetImageFile(id, imageFilePath);
                if (imageStream != null)
                {
                    try
                    {
                        var imageFile = Path.Combine(ImageRootPath, imageFilePath);
                        var imagePath = imageFile.Substring(0, imageFile.LastIndexOf('\\'));

                        if (!Directory.Exists(imagePath))
                            Directory.CreateDirectory(imagePath);

                        var isImageEmpty = false;
                        using (var fileStream = File.Create(imageFile))
                        {
                            imageStream.CopyTo(fileStream);
                            isImageEmpty = fileStream.Length == 0;
                            fileStream.Close();
                        }

                        if (isImageEmpty)
                        {
                            File.Delete(imageFile);
                            Logger.TraceInformation(0, "Image {ImagePath} empty for {Schema}", imageFilePath, schema);
                        }
                        else
                            Logger.TraceInformation(0, "Image {ImagePath} updated for {Schema}", imageFilePath, schema);

                        if (UpdateImageChangeLog(connection, imageChangeLog.Key))
                            Logger.TraceVerbose(0, "Image {ImagePath} commit for {Schema}", imageFilePath, schema);
                    }
                    catch (Exception ex)
                    {
                        Logger.TraceError(0, "Error: {@Exception}", new { Message = ex.Message, Stack = ex.StackTrace });
                    }
                }
                else
                    Logger.TraceInformation(0, "Image {ImagePath} not found for {Schema}", imageFilePath, schema);

                if (cancellationToken.IsCancellationRequested)
                    break;
            }

            return imagesChangeLog.Count > 0;
        }

        private int GetAsmVersion()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var version = assembly.GetName().Version;

            int asmVersion;
            if (!int.TryParse(version.Major.ToString("0000") + version.Minor.ToString("00") + version.Build.ToString("00"), out asmVersion))
                return 0;

            return asmVersion;
        }

        private bool CheckDBVersion(int dbVersion, int asmVersion)
        {
            if (dbVersion < asmVersion)
            {
                Logger.TraceError(0, "Sync database version mismatch {DbVersion}/{AsmVersion}", dbVersion, asmVersion);
                return false;
            }
            else
                Logger.TraceInformation(0, "Sync database version: {DbVersion}", dbVersion);

            return true;
        }

        private bool CheckVersion(ISyncService proxy, int localAsmVersion)
        {
            var remoteAsmVersion = proxy.GetVersion();
            if (remoteAsmVersion != localAsmVersion)
            {
                Logger.TraceError(0, "Sync version mismatch {RemoteVersion}/{LocalVersion}", remoteAsmVersion, localAsmVersion);
                return false;
            }
            else
                Logger.TraceInformation(0, "Sync version: {AsmVersion}", localAsmVersion);

            return true;
        }

        private void DoClientSync(SyncTask sender, CancellationToken cancellationToken)
        {
            // leer el identificador del hotel
            using (var connection = new OracleConnection(ConnectionString))
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    try
                    {
                        connection.Open();
                        try
                        {
                            var id = GetId(connection);
                            if (id.HasValue)
                            {
                                _id = id.Value;
                                break;
                            }
                            else
                                Logger.TraceError(0, "Installation not defined. TNHT_HOTE is empty.");
                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                    catch (OracleException ex)
                    {
                        Logger.TraceWarning(0, "Error: {@Exception}", new { Message = ex.Message, Stack = ex.StackTrace });
                        if (ex.ErrorCode == 12514)
                            Thread.Sleep(TimeSpan.FromSeconds(1));
                    }
                }
            }

            if (!cancellationToken.IsCancellationRequested && _id != Guid.Empty)
            {
                var syncTask = (ClientSyncTask)sender;
                using (Logger.TraceScope(_id))
                {
                    Logger.TraceInformation(0, "Started sync on {Schema}", syncTask.ToString());

                    try
                    {
                        #if dcn
                        lock (this)
                        {
                            if (_dependency == null)
                            {
                                using (Logger.TraceScope(syncTask.Id))
                                {
                                    try
                                    {
                                        // register change notification
                                        _dependency = RegisterNotifications();
                                        Logger.TraceVerbose(0, "Notification registered for {Schema}", _dependency.UserName);
                                    }
                                    catch (Exception ex)
                                    {
                                        // if error log warning
                                        Logger.TraceError(0, "Notification registration failed for {Schema}: {@Exception}",
                                            syncTask.ToString(), new { Message = ex.Message });
                                    }
                                }
                            }
                        }
                        #endif

                        while (!cancellationToken.IsCancellationRequested)
                        {
                            Logger.TraceInformation("Sync {Schema} is running ...", syncTask.ToString());

                            var delay = TimeSpan.Zero;
                            try
                            {
                                if (cancellationToken.IsCancellationRequested)
                                    continue;

                                if (_proxy == null)
                                {
                                    _proxy = CreateProxy();
                                    Logger.TraceVerbose(0, "Proxy created for {Schema}", syncTask.ToString());
                                }

                                var hasRows = false;
                                using (var connection = new OracleConnection(ConnectionString))
                                {
                                    connection.Open();
                                    try
                                    {
                                        if (cancellationToken.IsCancellationRequested)
                                            continue;

                                        // if version changes
                                        var syncAsmVersion = GetAsmVersion();
                                        Logger.TraceInformation(0, "Sync database compatible app version {Version}", syncAsmVersion);

                                        if (!TryGetDbVersions(connection, out int dbVersion, out int dbSyncVersion))
                                        {
                                            dbVersion = 0;
                                            dbSyncVersion = 0;
                                        }
                                        
                                        if (!CheckDBVersion(dbVersion, syncAsmVersion))
                                            break;
                                        else if (!CheckVersion(_proxy.Channel, syncAsmVersion))
                                            break;

                                        if (TryGetSyncCounter(connection, out long syncCounter))
                                        {
                                            _proxy.Channel.StartSync(_id, syncCounter);
                                            Logger.TraceVerbose(0, "Sync counter {Counter} sended for {Schema}", syncCounter, syncTask.ToString());
                                            Logger.TraceVerbose(0, "Sync version {SyncAsmVersion}, Database version {DbVersion}, Database sync version {DbSyncVersion}", syncAsmVersion, dbVersion, dbSyncVersion);

                                            if (cancellationToken.IsCancellationRequested)
                                                continue;

                                            Logger.TraceVerbose(0, "DoSyncToServer");
                                            if (DoSyncToServer(connection, cancellationToken, _id, syncTask.ToString()))
                                                hasRows = true;

                                            if (cancellationToken.IsCancellationRequested)
                                                continue;

                                            Logger.TraceVerbose(0, "DoSyncFromServer");
                                            if (DoSyncFromServer(connection, cancellationToken, _id, syncTask.ToString()))
                                                hasRows = true;

                                            if (cancellationToken.IsCancellationRequested)
                                                continue;

                                            Logger.TraceVerbose(0, "DoImageSync");
                                            if (DoImageSync(connection, cancellationToken, _id, syncTask.ToString()))
                                                hasRows = true;

                                            if (cancellationToken.IsCancellationRequested)
                                                continue;
                                        }
                                    }
                                    finally
                                    {
                                        connection.ActionName = null;
                                        connection.Close();
                                    }
                                }

                                if (!hasRows)
                                {
                                    if (!cancellationToken.IsCancellationRequested)
                                        syncTask.Wait(PosEventName);
                                }
                            }
                            catch (OperationCanceledException)
                            {
                                throw;
                            }
                            catch (TimeoutException ex)
                            {
                                delay = TimeSpan.FromSeconds(30);
                                if (_proxy != null)
                                {
                                    _proxy.Dispose();
                                    _proxy = null;
                                }
                                Logger.TraceError(0, "Timeout error: {@Exception}", new { Message = ex.Message });
                            }
                            catch (CommunicationException ex)
                            {
                                if (_disconnectedSince.HasValue && (DateTime.Now - _disconnectedSince.Value) > TimeSpan.FromMinutes(10))
                                {
                                    Logger.TraceWarning(0, "Sync {Schema} disconnected", syncTask.ToString());
                                    _disconnectedSince = DateTime.Now;
                                }

                                delay = TimeSpan.FromMinutes(1);
                                if (_proxy != null)
                                {
                                    _proxy.Dispose();
                                    _proxy = null;
                                }
                                Logger.TraceError(0, "Communication error: {@Exception}", new { Message = ex.Message });
                            }
                            catch (OracleException ex)
                            {
                                delay = TimeSpan.FromMinutes(3);
                                Logger.TraceError(0, "Database error: {@Exception}", new { Code = ex.ErrorCode, Message = ex.Message, Connection = ConnectionString });
                            }
                            catch (Exception ex)
                            {
                                delay = TimeSpan.FromMinutes(3);
                                Logger.TraceError(0, "Error: {@Exception}", new { Message = ex.Message, Stack = ex.StackTrace });
                            }

                            if (delay > TimeSpan.Zero)
                            {
                                Logger.TraceVerbose(0, "Waiting {Delay} to retry on {Schema}", delay, syncTask.ToString());
                                syncTask.Delay(delay, cancellationToken);
                            }
                        }

                        try
                        {
                            if (_proxy != null && _proxy.State == CommunicationState.Opened)
                            {
                                try
                                {
                                    Logger.TraceInformation(0, "Stopping sync on {Schema}", syncTask.ToString());
                                    _proxy.Channel.StopSync(syncTask.Id);
                                    Logger.TraceInformation(0, "Stopped sync on {Schema}", syncTask.ToString());
                                }
                                catch (TimeoutException ex)
                                {
                                    Logger.TraceError(0, "Timeout error: {@Exception}", new { Message = ex.Message });
                                }
                                catch (CommunicationException ex)
                                {
                                    Logger.TraceError(0, "Communication error: {@Exception}", new { Message = ex.Message });
                                }
                                catch (Exception ex)
                                {
                                    Logger.TraceError(0, "Error: {@Exception}", new { Message = ex.Message });
                                }
                            }
                        }
                        finally
                        {
                            if (_proxy != null)
                            {
                                _proxy.Dispose();
                                _proxy = null;
                            }
                        }
                    }
                    catch (OperationCanceledException ex)
                    {
                        Logger.TraceError(0, "Canceled error: {@Exception}", new { Message = ex.Message, Stack = ex.StackTrace });
                        throw;
                    }
                    catch (Exception ex)
                    {
                        Logger.TraceError(0, "Error: {@Exception}", new { Message = ex.Message, Stack = ex.StackTrace });
                    }
                    finally
                    {
                        #if dcn
                        lock (this)
                        {
                            if (_dependency != null)
                            {
                                using (Logger.TraceScope(syncTask.Id))
                                {
                                    try
                                    {
                                        // unregister change notification
                                        if (UnregisterNotifications(_dependency))
                                        {
                                            Logger.TraceVerbose(0, "Notification unregistered for {Schema}", _dependency.UserName);
                                            _dependency = null;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        // if error log warning
                                        Logger.TraceError(0, "Notification unregistration failed for {Schema}: {@Exception}",
                                            _dependency.UserName, new { Message = ex.Message });
                                    }
                                }
                            }
                        }
                        #endif
                    }
                }
            }
        }

        public void Start()
        {
            try
            {
                lock (_syncObject)
                {
                    if (_syncTask == null)
                    {
                        ReadConfig();
                        _syncTask = new ClientSyncTask(_id, Schema, _syncDelay);
                        if (_syncTask != null)
                            _syncTask.Run(DoClientSync);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.TraceError(0, "Error: {@Exception}", new { Message = ex.Message, Stack = ex.StackTrace });
            }
        }

        public void Stop()
        {
            try
            {
                lock (_syncObject)
                {
                    if (_syncTask != null)
                    {
                        if (_syncTask.IsRunning)
                        {
                            _syncTask.Cancel();
                            _syncTask.Task.Wait();
                        }

                        _syncTask.Dispose();
                        _syncTask = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.TraceError(0, "Error: {@Exception}", new { Message = ex.Message, Stack = ex.StackTrace });
            }
        }

        public bool IsRunning
        {
            get { return _syncTask != null && _syncTask.IsRunning; }
        }

        public void OnSyncChanges(Message message)
        {
            if (_syncTask != null)
            {
                try
                {
                    using (Logger.TraceScope(_id))
                    {
                        if (message.IsFault)
                            Logger.TraceData(0, "Sync fault message:", message);
                        else
                        {
                            if (Logger.Switch.ShouldTrace(TraceEventType.Verbose))
                                Logger.TraceData(0, "Sync message:", message);

                            using (var connection = new OracleConnection(ConnectionString))
                            {
                                var syncCounter = -1L;

                                connection.Open();
                                try
                                {
                                    if (!message.IsEmpty)
                                    {
                                        var transaction = connection.BeginTransaction();
                                        try
                                        {
                                            IEnumerable<Tuple<string, string, OracleCommand>> commands = null;
                                            commands = SyncUtil.GetSyncCommands(connection, _syncTask.ToString(), message, true);

                                            try
                                            {
                                                var traceVerbose = Logger.Switch.ShouldTrace(TraceEventType.Verbose);
                                                foreach (var command in commands)
                                                {
                                                    Logger.TraceInformation("Sync applying changes to {Schema}.{TableName}", _syncTask.ToString(), command.Item1);
                                                    var rowsAffected = command.Item3.ExecuteNonQuery();
                                                    if (traceVerbose)
                                                    {
                                                        var commandId = _syncTask.ToString() + "." + command.Item1 + ":" + command.Item2;
                                                        if (rowsAffected < 0)
                                                            Logger.TraceVerbose(0, "Sync command {CommandId} executed.", commandId);
                                                        else
                                                            Logger.TraceVerbose(0, "Sync command {CommandId} executed. " + rowsAffected.ToString() + " rows affected.", commandId);
                                                    }
                                                }

                                                transaction.Commit();
                                            }
                                            finally
                                            {
                                                if (commands != null)
                                                {
                                                    foreach (var command in commands)
                                                    {
                                                        foreach (OracleParameter parameter in command.Item3.Parameters)
                                                            parameter.Dispose();

                                                        command.Item3.Dispose();
                                                    }
                                                }
                                            }

                                            TryGetSyncCounter(connection, out syncCounter);
                                        }
                                        catch (OracleException ex)
                                        {
                                            transaction.Rollback();
                                            Logger.TraceError(0, "Database error: {@Exception}", new { Code = ex.ErrorCode, Message = ex.Message, Connection = ConnectionString });
                                        }
                                        catch
                                        {
                                            transaction.Rollback();
                                            throw;
                                        }
                                    }
                                    else
                                        TryGetSyncCounter(connection, out syncCounter);
                                }
                                finally
                                {
                                    connection.Close();

                                    _proxy.Channel.UpdateSync(_syncTask.Id, syncCounter);
                                    Logger.TraceVerbose(0, "Sync counter {Counter} sended for {Schema}", syncCounter, _syncTask.ToString());
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.TraceError(0, "Error: {@Exception}", new { Message = ex.Message, Stack = ex.StackTrace });
                }
            }
        }

        public override string ToString()
        {
            return string.Format(url, Server, Port);
        }

        public void Dispose()
        {
            if (_proxy != null)
            {
                _proxy.Dispose();
                _proxy = null;
            }
        }
    }
}