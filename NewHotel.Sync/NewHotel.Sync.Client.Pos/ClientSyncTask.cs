﻿using System;
using NewHotel.Sync.Core;

namespace NewHotel.Sync.Client
{
    public sealed class ClientSyncTask : SyncTask
    {
        #region Constants

        public const string WaitName = "NHCLOUDPOS";

        #endregion
        #region Constructor

        public ClientSyncTask(Guid id, string name, TimeSpan timeout)
            : base(id, name, string.Empty, timeout, WaitName)
        {
        }

        #endregion
        #region Protected Methods

        // signal to wakeup task
        protected override void Signal()
        {
            Set(WaitName);
        }

        #endregion
    }
}