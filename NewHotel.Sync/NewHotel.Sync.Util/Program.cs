﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Data.OracleClient;
using NewHotel.Sync.Core;

namespace NewHotel.Sync.Util
{
    class Program
    {
        private const string ConnectionStringFormat = "User Id={0};Password={1};Data Source={2};Pooling=false;Connection Lifetime=180";

        private static string GetConnectionString(string user, string password, string source)
        {
            return string.Format(ConnectionStringFormat, user, password, source);
        }

        private static string GetFieldValue(object value)
        {
            var type = value.GetType();
            var typeCode = Type.GetTypeCode(type);
            switch (typeCode)
            {
                case TypeCode.DateTime:
                    var dateValue = (DateTime)Convert.ChangeType(value, type);
                    if (dateValue.TimeOfDay == TimeSpan.Zero)
                        return string.Format("TO_DATE('{0}-{1}-{2}', 'DD-MM-YYYY')",
                            dateValue.Day.ToString("D2"), dateValue.Month.ToString("D2"), dateValue.Year.ToString("D4"));
                    else
                        return string.Format("TO_DATE('{0}-{1}-{2} {3}:{4}:{5}', 'DD-MM-YYYY HH24:MI:SS')",
                            dateValue.Day.ToString("D2"), dateValue.Month.ToString("D2"), dateValue.Year.ToString("D4"),
                            dateValue.Hour.ToString("D2"), dateValue.Minute.ToString("D2"), dateValue.Second.ToString("D2"));
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                    var longValue = (long)Convert.ChangeType(value, typeof(long));
                    return longValue.ToString();
                case TypeCode.String:
                    var strValue = (string)Convert.ChangeType(value, type);
                    strValue = strValue.Replace("'", "''");
                    return string.IsNullOrEmpty(strValue) ? "''" : "'" + strValue + "'";
                case TypeCode.Boolean:
                    var boolValue = (bool)Convert.ChangeType(value, type);
                    return boolValue ? "'1'" : "'0'";
                case TypeCode.Decimal:
                    var numberFormat = (NumberFormatInfo)NumberFormatInfo.InvariantInfo.Clone();
                    numberFormat.NumberDecimalSeparator = ".";
                    numberFormat.NumberDecimalDigits = 2;
                    var decimalValue = (decimal)Convert.ChangeType(value, type);
                    return decimalValue.ToString("0.###############", numberFormat);
                case TypeCode.Object:
                    if (type.Equals(typeof(Guid)))                    {
                        var guidValue = (Guid)value;
                        return "'" + guidValue.ToHex() + "'";
                    }
                    else if (type.Equals(typeof(byte[])))
                    {
                        var bytesValue = (byte[])value;
                        return "'" + bytesValue.ToHex() + "'";
                    }
                    break;
            }

            return "''";
        }

        private static void GetMergeCommand(OracleConnection connection, StringBuilder sql,
            string commandTableName, string commandText, params string[] keys)
        {
            GetMergeCommand(connection, sql, commandTableName, commandText, new string[0], keys);
        }

        private static void GetMergeCommand(OracleConnection connection, StringBuilder sql,
            string commandTableName, string commandText, string[] excludedFields, params string[] keys)
        {
            using (var command = connection.CreateCommand())
            {
                command.CommandText = commandText;
                using (var reader = command.ExecuteReader(CommandBehavior.SingleResult))
                {
                    try
                    {
                        sql.AppendLine("-- " + commandTableName);
                        sql.AppendLine();

                        while (reader.Read())
                        {
                            if (commandTableName == "TNHT_BSERV")
                            {
                                var fieldValue = GetFieldValue(reader.GetValue(reader.GetOrdinal("FAMI_PK")));
                                sql.AppendFormat("MERGE INTO {0} TEMP1 USING (SELECT GRUP.GRSE_PK, GRUP.CATS_PK FROM TNHT_FAMI FAMI INNER JOIN TNHT_GRUP GRUP ON GRUP.GRUP_PK = FAMI.GRUP_PK WHERE FAMI.FAMI_PK = {1}) TEMP2 ON (", commandTableName, fieldValue);
                                sql.AppendLine();
                                for (int index = 0; index < keys.Length; index++)
                                {
                                    if (index > 0)
                                        sql.Append(" AND ");
                                    sql.AppendFormat("TEMP1.{0} = {1}", keys[index], GetFieldValue(reader.GetValue(index)));
                                }
                            }
                            else
                            {
                                sql.AppendFormat("MERGE INTO {0} TEMP1 USING DUAL ON (", commandTableName);
                                sql.AppendLine();
                                for (int index = 0; index < keys.Length; index++)
                                {
                                    if (index > 0)
                                        sql.Append(" AND ");
                                    sql.AppendFormat("TEMP1.{0} = {1}", keys[index], GetFieldValue(reader.GetValue(index)));
                                }
                            }
                            sql.AppendLine(")");
                            sql.AppendLine("WHEN NOT MATCHED THEN INSERT (");
                            for (int index = 0; index < reader.FieldCount; index++)
                            {
                                var name = reader.GetName(index);
                                if (!excludedFields.Contains(name))
                                {
                                    if (index > 0)
                                        sql.Append(", ");
                                    sql.Append(name);
                                }
                            }
                            sql.AppendLine(") VALUES (");
                            for (int index = 0; index < reader.FieldCount; index++)
                            {
                                var name = reader.GetName(index);
                                if (!excludedFields.Contains(name))
                                {
                                    if (index > 0)
                                        sql.Append(", ");
                                    if (reader.IsDBNull(index))
                                    {
                                        switch (name)
                                        {
                                            case "GRSE_PK":
                                            case "CATS_PK":
                                                sql.Append("TEMP2." + name);
                                                break;
                                            default:
                                                sql.Append("NULL");
                                                break;
                                        }
                                    }
                                    else
                                        sql.Append(GetFieldValue(reader.GetValue(index)));
                                }
                            }
                            sql.AppendLine(");");
                        }

                        sql.AppendLine("COMMIT;");
                        sql.AppendLine();
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            var filePath = string.Empty;
            if (args.Length > 0)
                filePath = args[0];
            else
            {
                using (var connection = new OracleConnection(GetConnectionString("POSCLOUD", "CLOUD", "ORCL1")))
                {
                    connection.Open();
                    try
                    {
                        Console.Write("File: ");
                        filePath = Console.ReadLine();
                        if (!string.IsNullOrEmpty(filePath))
                        {
                            var sql = new StringBuilder();

                            sql.AppendLine("SET DEFINE OFF;");
                            sql.AppendLine();
                            /*
                            // TNHT_UTIL
                            GetMergeCommand(connection, sql, "TNHT_UTIL", "SELECT UTIL_PK, HOTE_PK, UTIL_LOGIN, UTIL_DESC, UTIL_PASS, UTIL_CODE, UTIL_DARE, UTIL_MGR, UTIL_INTE, '1' AS UTIL_INAC, UTIL_LMOD FROM TNHT_UTIL", "UTIL_PK");

                            // TNHT_CLAS
                            GetMergeCommand(connection, sql, "TNHT_LITE", "SELECT LITE_PK FROM TNHT_CLAS", "LITE_PK");
                            GetMergeCommand(connection, sql, "TNHT_MULT", "SELECT MULT.LITE_PK, MULT.LANG_PK, MULT.MULT_DESC FROM TNHT_MULT MULT INNER JOIN TNHT_CLAS CLAS ON CLAS.LITE_PK = MULT.LITE_PK", "LITE_PK", "LANG_PK");

                            //GetMergeCommand(connection, sql, "TNHT_CLAS", "SELECT CLAS_PK, LITE_PK, HOTE_PK, '1' AS CLAS_INAC, CLAS_LMOD FROM TNHT_CLAS", "CLAS_PK");
                            GetMergeCommand(connection, sql, "TNHT_CLAS", "SELECT CLAS_PK, LITE_PK, HOTE_PK, CLAS_LMOD FROM TNHT_CLAS", "CLAS_PK");

                            // TNHT_GRSE
                            GetMergeCommand(connection, sql, "TNHT_LITE", "SELECT 'DE77573836694BA2B62187C4C0118523' AS LITE_PK FROM DUAL", "LITE_PK");
                            GetMergeCommand(connection, sql, "TNHT_MULT", "SELECT 'DE77573836694BA2B62187C4C0118523' AS LITE_PK, 1033 AS LANG_PK, 'Unknown' AS MULT_DESC FROM DUAL", "LITE_PK", "LANG_PK");
                            GetMergeCommand(connection, sql, "TNHT_MULT", "SELECT 'DE77573836694BA2B62187C4C0118523' AS LITE_PK, 3082 AS LANG_PK, 'Desconocido' AS MULT_DESC FROM DUAL", "LITE_PK", "LANG_PK");
                            GetMergeCommand(connection, sql, "TNHT_MULT", "SELECT 'DE77573836694BA2B62187C4C0118523' AS LITE_PK, 2070 AS LANG_PK, 'Desconhecido' AS MULT_DESC FROM DUAL", "LITE_PK", "LANG_PK");

                            GetMergeCommand(connection, sql, "TNHT_GRSE", "SELECT '016F2857224648E09250ECD1C2E0C14A' AS GRSE_PK, 'DE77573836694BA2B62187C4C0118523' AS LITE_PK, HOTE_PK FROM TNHT_HOTE", "GRSE_PK");

                            // TNHT_GRUP
                            GetMergeCommand(connection, sql, "TNHT_GRUP", "SELECT GRUP_PK, GRUP_CHEQ, GRUP_RECA, '016F2857224648E09250ECD1C2E0C14A' AS GRSE_PK, 3 AS CATS_PK FROM TNHT_GRUP", "GRUP_PK");

                            // TNHT_FAMI
                            GetMergeCommand(connection, sql, "TNHT_FAMI", "SELECT FAMI_PK, GRUP_PK FROM TNHT_FAMI", "FAMI_PK");

                            // TNHT_SFAM
                            GetMergeCommand(connection, sql, "TNHT_SFAM", "SELECT SFAM_PK, FAMI_PK FROM TNHT_SFAM", "SFAM_PK");

                            // TNHT_COIN
                            GetMergeCommand(connection, sql, "TNHT_COIN", "SELECT COIN_PK, COIN_INVI FROM TNHT_COIN", "COIN_PK");
                            */
                            // TNHT_ARTG
                            GetMergeCommand(connection, sql, "TNHT_LITE", "SELECT LITE_ABRE AS LITE_PK FROM TNHT_ARTG", "LITE_PK");
                            GetMergeCommand(connection, sql, "TNHT_MULT", "SELECT MULT.LITE_PK, MULT.LANG_PK, MULT.MULT_DESC FROM TNHT_MULT MULT INNER JOIN TNHT_ARTG ARTG ON ARTG.LITE_ABRE = MULT.LITE_PK", "LITE_PK", "LANG_PK");

                            GetMergeCommand(connection, sql, "TNHT_LITE", "SELECT LITE_DESC AS LITE_PK FROM TNHT_ARTG", "LITE_PK");
                            GetMergeCommand(connection, sql, "TNHT_MULT", "SELECT MULT.LITE_PK, MULT.LANG_PK, MULT.MULT_DESC FROM TNHT_MULT MULT INNER JOIN TNHT_ARTG ARTG ON ARTG.LITE_DESC = MULT.LITE_PK", "LITE_PK", "LANG_PK");

                            GetMergeCommand(connection, sql, "TNHT_BSERV", "SELECT ARTG.ARTG_PK AS SERV_PK, ARTG.HOTE_PK, ARTG.LITE_ABRE, ARTG.LITE_DESC, NULL AS GRSE_PK, NULL AS CATS_PK, ARTG.ARTG_ROUN AS SERV_ROUN, ARTG.ARTG_LOCA AS SERV_LOCA, ARTG.ARTG_UNME AS SERV_UNME, ARTG.ARTG_CSTI AS SERV_CSTI, ARTG.ARTG_CNCM AS SERV_CNCM, ARTG.ARTG_CFOP AS SERV_CFOP, ARTG.ARTG_CSTP AS SERV_CSTP, ARTG.ARTG_CSTC AS SERV_CSTC, '1' AS SERV_INAC, '0' AS SERV_INMA, FAMI.FAMI_PK FROM TNHT_ARTG ARTG INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK INNER JOIN TNHT_GRUP GRUP ON GRUP.GRUP_PK = FAMI.GRUP_PK", new string[] { "FAMI_PK" }, "SERV_PK");
                            GetMergeCommand(connection, sql, "TNHT_BARTG", "SELECT ARTG_PK AS BARTG_PK, ARTG_CODI, FAMI_PK, SFAM_PK FROM TNHT_ARTG", "BARTG_PK");
                            GetMergeCommand(connection, sql, "TNHT_ARTG", "SELECT ARTG_PK, ARTG_STEP, ARTG_TIPR, ARTG_COLO FROM TNHT_ARTG", "ARTG_PK");
                            /*
                            // TNHT_FORE
                            GetMergeCommand(connection, sql, "TNHT_LITE", "SELECT LITE_ABRE AS LITE_PK FROM TNHT_FORE", "LITE_PK");
                            GetMergeCommand(connection, sql, "TNHT_MULT", "SELECT MULT.LITE_PK, MULT.LANG_PK, MULT.MULT_DESC FROM TNHT_MULT MULT INNER JOIN TNHT_FORE FORE ON FORE.LITE_ABRE = MULT.LITE_PK", "LITE_PK", "LANG_PK");

                            GetMergeCommand(connection, sql, "TNHT_LITE", "SELECT LITE_DESC AS LITE_PK FROM TNHT_FORE", "LITE_PK");
                            GetMergeCommand(connection, sql, "TNHT_MULT", "SELECT MULT.LITE_PK, MULT.LANG_PK, MULT.MULT_DESC FROM TNHT_MULT MULT INNER JOIN TNHT_FORE FORE ON FORE.LITE_DESC = MULT.LITE_PK", "LITE_PK", "LANG_PK");

                            GetMergeCommand(connection, sql, "TNHT_FORE", "SELECT FORE_PK, HOTE_PK, LITE_ABRE, LITE_DESC, UNMO_PK, FORE_CASH, FORE_CACR, REPO_CASH, FORE_ORDE, FORE_PCRM, FORE_IMPF, FORE_BANC, FORE_TEFP, FORE_TEND, FORE_EVIF, FORE_CRED, FORE_DEBI, FORE_CAUX, '1' AS FORE_INAC, FORE_LMOD FROM TNHT_FORE", "FORE_PK");

                            // TNHT_TIDE
                            GetMergeCommand(connection, sql, "TNHT_LITE", "SELECT LITE_PK FROM TNHT_TIDE", "LITE_PK");
                            GetMergeCommand(connection, sql, "TNHT_MULT", "SELECT MULT.LITE_PK, MULT.LANG_PK, MULT.MULT_DESC FROM TNHT_MULT MULT INNER JOIN TNHT_TIDE TIDE ON TIDE.LITE_PK = MULT.LITE_PK", "LITE_PK", "LANG_PK");

                            //GetMergeCommand(connection, sql, "TNHT_TIDE", "SELECT TIDE_PK, LITE_PK, HOTE_PK, TIDE_FIXE, TIDE_VMIN, TIDE_VMAX, TIDE_TIDE, APPL_PK, '1' AS TIDE_INAC, TIDE_LMOD FROM TNHT_TIDE", "TIDE_PK");
                            GetMergeCommand(connection, sql, "TNHT_TIDE", "SELECT TIDE_PK, LITE_PK, HOTE_PK, TIDE_FIXE, TIDE_VMIN, TIDE_VMAX, TIDE_TIDE, APPL_PK, 1 AS TIDE_APPL, TIDE_LMOD FROM TNHT_TIDE", "TIDE_PK");

                            // TNHT_MTCO
                            GetMergeCommand(connection, sql, "TNHT_LITE", "SELECT LITE_PK FROM TNHT_MTCO", "LITE_PK");
                            GetMergeCommand(connection, sql, "TNHT_MULT", "SELECT MULT.LITE_PK, MULT.LANG_PK, MULT.MULT_DESC FROM TNHT_MULT MULT INNER JOIN TNHT_MTCO MTCO ON MTCO.LITE_PK = MULT.LITE_PK", "LITE_PK", "LANG_PK");

                            //GetMergeCommand(connection, sql, "TNHT_MTCO", "SELECT MTCO_PK, LITE_PK, HOTE_PK, MTCO_TICA, '1' AS MTCO_INAC, MTCO_LMOD FROM TNHT_MTCO", "MTCO_PK");
                            GetMergeCommand(connection, sql, "TNHT_MTCO", "SELECT MTCO_PK, LITE_PK, HOTE_PK, MTCO_TICA, MTCO_LMOD FROM TNHT_MTCO", "MTCO_PK");

                            // TNHT_CACR
                            GetMergeCommand(connection, sql, "TNHT_LITE", "SELECT LITE_PK FROM TNHT_CACR", "LITE_PK");
                            GetMergeCommand(connection, sql, "TNHT_MULT", "SELECT MULT.LITE_PK, MULT.LANG_PK, MULT.MULT_DESC FROM TNHT_MULT MULT INNER JOIN TNHT_CACR CACR ON CACR.LITE_PK = MULT.LITE_PK", "LITE_PK", "LANG_PK");

                            GetMergeCommand(connection, sql, "TNHT_CACR", "SELECT CACR_PK, LITE_PK, HOTE_PK, CACR_CACR, CACR_CADE, CACR_LMOD FROM TNHT_CACR", "CACR_PK");

                            // TNHT_SALO
                            GetMergeCommand(connection, sql, "TNHT_LITE", "SELECT LITE_PK FROM TNHT_SALO", "LITE_PK");
                            GetMergeCommand(connection, sql, "TNHT_MULT", "SELECT MULT.LITE_PK, MULT.LANG_PK, MULT.MULT_DESC FROM TNHT_MULT MULT INNER JOIN TNHT_SALO SALO ON SALO.LITE_PK = MULT.LITE_PK", "LITE_PK", "LANG_PK");

                            GetMergeCommand(connection, sql, "TNHT_SALO", "SELECT SALO_PK, LITE_PK, HOTE_PK, SALO_WIDT, SALO_HEIG, MESA_FREE, MESA_OCUP, SALO_IMAG, SALO_SMOK, SALO_KIDS FROM TNHT_SALO", "SALO_PK");

                            // TNHT_MESA
                            GetMergeCommand(connection, sql, "TNHT_MESA", "SELECT MESA_PK, MESA_DESC, SALO_PK, MESA_PAXS, MESA_TYPE, MESA_ROW, MESA_COLU, MESA_WIDT, MESA_HEIG FROM TNHT_MESA", "MESA_PK");
                            */
                            filePath = Path.Combine(Directory.GetCurrentDirectory(), filePath);
                            if (File.Exists(filePath))
                                File.Delete(filePath);
                            File.AppendAllText(filePath, sql.ToString(), Encoding.UTF8);
                            Console.Write("--> ");
                            Console.WriteLine(filePath);
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }
    }
}
