﻿using System;

namespace NewHotel.Sync.Core
{
    public static class GuidExtension
    {
        private static readonly long _baseTicks = 599266080000000000;

        private static TimeSpan ToTimeOfDay(byte[] guidArray)
        {
            var msecsArray = new byte[8];
            Array.Copy(guidArray, 12, msecsArray, 4, 4);
            Array.Reverse(msecsArray);
            return TimeSpan.FromMilliseconds(BitConverter.ToInt32(msecsArray, 0));
        }

        public static Guid GenerateGuid(DateTime dateTime)
        {
            var guidArray = Guid.NewGuid().ToByteArray();

            var days = new TimeSpan(dateTime.Ticks - _baseTicks).Days;
            var msecs = dateTime.TimeOfDay.TotalMilliseconds;
            var daysArray = BitConverter.GetBytes(days);
            var msecsArray = BitConverter.GetBytes((long)msecs);

            Array.Reverse(daysArray);
            Array.Reverse(msecsArray);

            Array.Copy(daysArray, daysArray.Length - 2, guidArray, guidArray.Length - 6, 2);
            Array.Copy(msecsArray, msecsArray.Length - 4, guidArray, guidArray.Length - 4, 4);

            return new Guid(guidArray);
        }

        public static Guid GenerateGuid()
        {
            return GenerateGuid(DateTime.Now.ToUniversalTime());
        }

        public static Guid NewGuid(this DateTime dateTime)
        {
            return GenerateGuid(dateTime.ToUniversalTime());
        }

        public static DateTime ToDateTime(this Guid guid)
        {
            var guidArray = guid.ToByteArray();

            var daysArray = new byte[4];
            Array.Copy(guidArray, 10, daysArray, 2, 2);
            Array.Reverse(daysArray);

            var dateTime = new DateTime(_baseTicks);
            dateTime = dateTime.AddDays(BitConverter.ToInt32(daysArray, 0));
            dateTime = dateTime.Add(ToTimeOfDay(guidArray));
            return dateTime;
        }

        public static TimeSpan ToTimeOfDay(this Guid guid)
        {
            return ToTimeOfDay(guid.ToByteArray());
        }

        public static byte[] ToBytes(this string str)
        {
            var length = str.Length;
            var bytes = new byte[length / 2];
            var index = 0;
            for (int i = 0; i < length; i += 2)
                bytes[index++] = Convert.ToByte(str.Substring(i, 2), 16);

            return bytes;
        }

        public static Guid ToGuid(this string str)
        {
            return new Guid(ToBytes(str));
        }

        public static bool TryToGuid(this string str, out Guid guid)
        {
            try
            {
                guid = ToGuid(str);
            }
            catch (FormatException)
            {
                guid = Guid.Empty;
                return false;
            }

            return true;
        }

        public static string ToHex(this byte[] bytes)
        {
            var hex = new char[bytes.Length * 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                var hexArray = bytes[i].ToString("X2").ToCharArray();
                Array.Copy(hexArray, 0, hex, i * 2, 2);
            }

            return new string(hex);
        }

        public static string ToHex(this Guid guid)
        {
            var bytes = guid.ToByteArray();
            return bytes.ToHex();
        }
    }
}