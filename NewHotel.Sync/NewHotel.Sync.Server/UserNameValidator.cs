﻿using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;
using System.Diagnostics;

namespace NewHotel.Sync.Server
{
    public class UserNameValidator : UserNamePasswordValidator
    {
        public override void Validate(string userName, string password)
        {
            Trace.TraceInformation("UserName: {0}", userName);
            Trace.TraceInformation("Password: {0}", password);
            if (userName != "NHT")
                throw new SecurityTokenException("Invalid Username");
            if (password != "1234")
                throw new SecurityTokenException("Invalid Password");
        }
    }
}
