﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Configuration.Install;
using System.Reflection;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Diagnostics;
using NewHotel.Sync.Core;
using NewHotel.Diagnostics;

namespace NewHotel.Sync.Server
{
    public class SyncServiceHost : ServiceBase
    {
        private const string LogName = "Application";

        public static string SyncServiceName;

        [Import(typeof(ISyncSetupHelper))]
        private ISyncSetupHelper _setupHelper = null;

        [ImportMany(typeof(ISyncServerProvider))]
        private List<ISyncServerProvider> _providers = null;

        private static TraceSource _logger;

        static SyncServiceHost()
        {
            SyncServiceName = Path.GetFileNameWithoutExtension(Assembly.GetEntryAssembly().Location);
            _logger = Log.Source[SyncServiceName];
        }

        public SyncServiceHost()
        {
            CanPauseAndContinue = true;
            ServiceName = SyncServiceName;
            Compose();
        }

        private void Compose()
        {
            var container = new CompositionContainer(new DirectoryCatalog(".\\", "*.dll"));
            container.SatisfyImportsOnce(this);
        }

        private bool IsRunning
        {
            get { return _providers != null && _providers.Any(x => x.IsRunning); }
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                foreach (var provider in _providers)
                    provider.Start();
            }
            catch (Exception ex)
            {
                Debug.Fail(ex.Message, ex.StackTrace);
            }
        }

        protected override void OnStop()
        {
            if (_providers != null)
            {
                try
                {
                    foreach (var provider in _providers)
                    {
                        if (provider.IsRunning)
                            provider.Stop();
                    }
                }
                catch (Exception ex)
                {
                    Debug.Fail(ex.Message, ex.StackTrace);
                }
            }
        }

        private void SetupRun(bool install)
        {
            if (install)
                _setupHelper.Install();
            else
                _setupHelper.Uninstall();

            Console.WriteLine("Press any key to exit ...");
            Console.WriteLine();
            Console.ReadKey(false);
        }

        private void ConsoleRun()
        {
            Console.WriteLine("Press any key to stop server ...");
            Console.WriteLine();

            OnStart(null);
            if (IsRunning)
            {
                try
                {
                    Console.ReadKey(false);
                }
                finally
                {
                    OnStop();
                }
            }
            else
                Console.WriteLine("Not running ...");

            Console.WriteLine();
            Console.WriteLine("Press any key to exit ...");
            Console.WriteLine();
            Console.ReadKey(false);
        }

        private static void InstallService()
        {
            ManagedInstallerClass.InstallHelper(new string[] { Assembly.GetExecutingAssembly().Location });
        }

        private static void UninstallService()
        {
            ManagedInstallerClass.InstallHelper(new string[] { "/u", Assembly.GetExecutingAssembly().Location });
        }

        public static void Main(string[] args)
        {
            var debugMode = false;
            bool? setupInstallMode = null;

            if (args.Length > 0)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    switch (args[i].ToUpperInvariant())
                    {
                        case "/H":
                            Console.WriteLine("Command line switches");
                            Console.WriteLine("/H   Help");
                            Console.WriteLine("/I   Install service");
                            Console.WriteLine("/U   Uninstall service");
                            Console.WriteLine("/D   Console debug mode");
                            Console.ReadKey(true);
                            return;
                        case "/I":
                            InstallService();
                            return;
                        case "/U":
                            UninstallService();
                            return;
                        case "/D":
                            debugMode = true;
                            break;
                        default:
                            break;
                    }
                }
            }

            try
            {
                if (setupInstallMode.HasValue)
                {
                    using (var host = new SyncServiceHost())
                    {
                        host.SetupRun(setupInstallMode.Value);
                    }
                }
                else if (debugMode)
                {
                    using (var host = new SyncServiceHost())
                    {
                        host.ConsoleRun();
                    }
                }
                else
                    ServiceBase.Run(new SyncServiceHost());
            }
            catch (Exception ex)
            {
                Debug.Fail(ex.Message, ex.StackTrace);

                if (setupInstallMode.HasValue || debugMode)
                    Console.ReadKey(true);
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing && _providers != null)
            {
                foreach (var provider in _providers)
                    provider.Dispose();
            }
        }
    }
}