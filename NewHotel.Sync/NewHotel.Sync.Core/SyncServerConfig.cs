﻿using System.Configuration;

namespace NewHotel.Sync.Core
{
    public sealed class SyncServerConfig : ConfigurationSection
    {
        #region Constants

        private const string SectionName = "newhotel.sync.server.config";
        private const string ServiceName = "service";
        private const string SchemaName = "schema";
        private const string SchemaPassword = "password";
        private const string ImageRootPathName = "images";

        #endregion
        #region Properties

        [ConfigurationProperty(ServiceName, IsRequired = false, IsKey = false, DefaultValue = "ORCL")]
        public string Service
        {
            get { return (string)this[ServiceName]; }
            set { this[ServiceName] = value; }
        }

        [ConfigurationProperty(SchemaName, IsRequired = false, IsKey = true, DefaultValue = "MGR")]
        public string Schema
        {
            get { return (string)this[SchemaName]; }
            set { this[SchemaName] = value; }
        }

        [ConfigurationProperty(SchemaPassword, IsRequired = true, IsKey = false, DefaultValue = "")]
        public string Password
        {
            get { return CryptUtils.Decrypt((string)this[SchemaPassword]); }
            set { this[SchemaPassword] = CryptUtils.Encrypt(value); }
        }

        [ConfigurationProperty(ImageRootPathName, IsRequired = false, IsKey = false, DefaultValue = "C:\\inetpub\\wwwroot\\Images")]
        public string ImageRootPath
        {
            get { return (string)this[ImageRootPathName]; }
            set { this[ImageRootPathName] = value; }
        }

        #endregion
        #region Methods

        public static SyncServerConfig Instance
        {
            get { return ConfigurationManager.GetSection(SectionName) as SyncServerConfig; }
        }

        #endregion
    }
}