﻿using System;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace NewHotel.Sync.Core
{
    public static class CryptUtils
    {
        private const string _passw = "1C756173-402D-4958-AA76-F2B3511955D7";

        public static string Encrypt(string input, string password)
        {
            // Test data
            var data = input;
            var utfdata = UTF8Encoding.UTF8.GetBytes(data);
            var saltBytes = UTF8Encoding.UTF8.GetBytes("saltIsGoodForYou");

            // Our symmetric encryption algorithm
            var aes = new AesManaged();

            // We're using the PBKDF2 standard for password-based key generation
            var rfc = new Rfc2898DeriveBytes(password, saltBytes);

            // Setting our parameters
            aes.BlockSize = aes.LegalBlockSizes[0].MaxSize;
            aes.KeySize = aes.LegalKeySizes[0].MaxSize;

            aes.Key = rfc.GetBytes(aes.KeySize / 8);
            aes.IV = rfc.GetBytes(aes.BlockSize / 8);

            // Encryption
            var encryptTransf = aes.CreateEncryptor();

            // Output stream, can be also a FileStream
            byte[] encryptBytes;
            using (var encryptStream = new MemoryStream())
            {
                using (var encryptor = new CryptoStream(encryptStream, encryptTransf, CryptoStreamMode.Write))
                {
                    encryptor.Write(utfdata, 0, utfdata.Length);
                    encryptor.Flush();
                    encryptor.Close();
                }

                // Showing our encrypted content
                encryptBytes = encryptStream.ToArray();
            }

            return Convert.ToBase64String(encryptBytes);
        }

        public static string Encrypt(string input)
        {
            return Encrypt(input, _passw);
        }

        public static string Decrypt(string base64Input, string password)
        {
            var encryptBytes = Convert.FromBase64String(base64Input);
            var saltBytes = Encoding.UTF8.GetBytes("saltIsGoodForYou");

            // Our symmetric encryption algorithm
            var aes = new AesManaged();

            // We're using the PBKDF2 standard for password-based key generation
            var rfc = new Rfc2898DeriveBytes(password, saltBytes);

            // Setting our parameters
            aes.BlockSize = aes.LegalBlockSizes[0].MaxSize;
            aes.KeySize = aes.LegalKeySizes[0].MaxSize;

            aes.Key = rfc.GetBytes(aes.KeySize / 8);
            aes.IV = rfc.GetBytes(aes.BlockSize / 8);

            // Now, decryption
            var decryptTrans = aes.CreateDecryptor();

            // Output stream, can be also a FileStream
            byte[] decryptBytes;
            using (var decryptStream = new MemoryStream())
            {
                using (var decryptor = new CryptoStream(decryptStream, decryptTrans, CryptoStreamMode.Write))
                {
                    decryptor.Write(encryptBytes, 0, encryptBytes.Length);
                    decryptor.Flush();
                    decryptor.Close();
                }

                // Showing our decrypted content
                decryptBytes = decryptStream.ToArray();
            }

            return UTF8Encoding.UTF8.GetString(decryptBytes, 0, decryptBytes.Length);
        }

        public static string Decrypt(string base64Input)
        {
            return Decrypt(base64Input, _passw);
        }
    }
}