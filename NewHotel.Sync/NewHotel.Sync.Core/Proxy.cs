﻿using System;
using System.ServiceModel;

namespace NewHotel.Sync.Core
{
    public sealed class Proxy<T> : IDisposable
    {
        private T _obj;
        private bool _disposed = false;

        public event EventHandler Disposed;

        public Proxy(T obj, TimeSpan timeout)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");

            _obj = obj;
            InnerChannel.OperationTimeout = timeout;
        }

        public event EventHandler Opening
        {
            add { CommunicationObject.Opening += value; }
            remove { CommunicationObject.Opening -= value; }
        }

        public event EventHandler Opened
        {
            add { CommunicationObject.Opened += value; }
            remove { CommunicationObject.Opened -= value; }
        }

        public event EventHandler Closing
        {
            add { CommunicationObject.Closing += value; }
            remove { CommunicationObject.Closing -= value; }
        }

        public event EventHandler Closed
        {
            add { CommunicationObject.Closed += value; }
            remove { CommunicationObject.Closed -= value; }
        }

        public event EventHandler Faulted
        {
            add { CommunicationObject.Faulted += value; }
            remove { CommunicationObject.Faulted -= value; }
        }

        private ICommunicationObject CommunicationObject
        {
            get { return _obj as ICommunicationObject; }
        }

        public CommunicationState State
        {
            get { return CommunicationObject.State; }
        }

        public T Channel
        {
            get { return _obj; }
        }

        public IClientChannel InnerChannel
        {
            get { return _obj as IClientChannel; }
        }

        private void Dispose(bool abort)
        {
            if (!_disposed)
            {
                _disposed = true;
                try
                {
                    if (CommunicationObject != null)
                    {
                        try
                        {
                            switch (CommunicationObject.State)
                            {
                                case CommunicationState.Faulted:
                                    CommunicationObject.Abort();
                                    break;
                                case CommunicationState.Opened:
                                    if (abort)
                                        CommunicationObject.Abort();
                                    else
                                    {
                                        try
                                        {
                                            CommunicationObject.Close(TimeSpan.FromSeconds(3));
                                        }
                                        catch
                                        {
                                            CommunicationObject.Abort();
                                        }
                                    }
                                    break;
                            }
                        }
                        finally
                        {
                            var disposable = _obj as IDisposable;
                            if (disposable != null)
                            {
                                disposable.Dispose();
                                if (Disposed != null)
                                    Disposed(null, EventArgs.Empty);
                            }
                        }
                    }
                }
                finally
                {
                    GC.SuppressFinalize(this);
                }
            }
        }

        public void Dispose()
        {
            Dispose(false);
        }

        ~Proxy()
        {
            Dispose(true);
        }
    }
}