﻿using System.Linq;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace NewHotel.Diagnostics
{
	public sealed class Log
	{
		#region Members

		private sealed class TraceSourceCollection : KeyedCollection<string, TraceSource>
		{
			protected override string GetKeyForItem(TraceSource item)
			{
				return item.Name.ToUpperInvariant();
			}
		}

		private readonly TraceSourceCollection _sources = new TraceSourceCollection();
		public static readonly Log Source = new Log();

		#endregion
		#region Public Properties

		public TraceSource this[string name]
		{
			get
			{
				lock (_sources)
				{
					TraceSource source;
					if (!_sources.Contains(name.ToUpperInvariant()))
					{
						source = new TraceSource(name);
						_sources.Add(source);
					}
					else
						source = _sources[name.ToUpperInvariant()];

					return source;
				}
			}
		}

		public override string ToString()
		{
			lock (_sources)
			{
				return string.Join(", ", _sources.Select(x => string.Format("{0}={1}", x.Name, x.Listeners.Count > 0)));
			}
		}

		#endregion
	}
}