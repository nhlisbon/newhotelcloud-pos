﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using NewHotel.Diagnostics;

namespace NewHotel.Sync.Core
{
    public abstract class SyncTask : IDisposable
    {
        #region Members

        private readonly TraceSource _logger;

        private bool _isDisposed = false;
        private CancellationTokenSource _cancellationTokenSource;
        private Task _task;
        private readonly IDictionary<string, EventWaitHandle> _waitHandles;

        public readonly Guid Id;
        public readonly string Name;
        public readonly string Description;
        public readonly TimeSpan DefaultTimeout;

        #endregion
        #region Constructor

        protected SyncTask(Guid id, string name, string description, TimeSpan timeout, params string[] waitHandleNames)
            : base()
        {
            Id = id;
            Name = name;
            Description = description;
            DefaultTimeout = timeout;

            _waitHandles = new Dictionary<string, EventWaitHandle>(waitHandleNames.Length);
            foreach (var waitHandleName in waitHandleNames)
                _waitHandles.Add(waitHandleName, new EventWaitHandle(false, EventResetMode.AutoReset,
                    string.Format("{0}:{1}", Name, waitHandleName)));

            _logger = CreateLogger();
        }

        #endregion
        #region Properties

        // return a trace source logger
        public TraceSource Logger
        {
            get { return _logger; }
        }

        // return the task
        public Task Task
        {
            get { return _task; }
        }

        #endregion
        #region Protected Methods

        // creates a trace source logger
        protected virtual TraceSource CreateLogger()
        {
            return Log.Source["NewHotel.Sync"];
        }

        //// signal to wakeup task
        protected abstract void Signal();

        #endregion
        #region Private Methods

        // event fired when task is canceled
        private void OnTaskCancelled(object obj)
        {
            Debug.WriteLine("Task {0} terminated", obj);
        }

        // gets cancelation token to cancel task and register the cancel event
        private CancellationToken GetCancellationToken()
        {
            if (_cancellationTokenSource == null)
            {
                _cancellationTokenSource = new CancellationTokenSource();
                _cancellationTokenSource.Token.Register(OnTaskCancelled, this);
            }

            return _cancellationTokenSource.Token;
        }

        private EventWaitHandle WaitHandle(string name)
        {
            lock (_waitHandles)
            {
                EventWaitHandle waitHandle;
                if (_waitHandles.TryGetValue(name, out waitHandle))
                    return waitHandle;
            }

            return null;
        }

        #endregion
        #region Public Methods

        // delay task an specific time
        public bool Delay(TimeSpan delay, CancellationToken cancellationToken)
        {
            try
            {
                Task.Delay(delay, cancellationToken).Wait();
            }
            catch (AggregateException)
            {
                return true;
            }

            return false;
        }

        // sleep task an specific timeout
        public bool Wait(string waitName, TimeSpan timeout)
        {
            var signaled = false;
            var waitHandle = WaitHandle(waitName);
            if (waitHandle != null)
            {
                try
                {
                    Debug.WriteLine("Task {0}:{1} waiting {2} ...", Name, waitName, timeout);
                    signaled = waitHandle.WaitOne(DefaultTimeout);
                }
                finally
                {
                    waitHandle.Reset();
                    if (signaled)
                        Debug.WriteLine("Task {0}:{1} released", Name, waitName);
                    else
                        Debug.WriteLine("Task {0}:{1} timeout {2}", Name, waitName, timeout);
                }
            }

            return signaled;
        }

        // sleep task
        public bool Wait(string waitName)
        {
            return Wait(waitName, DefaultTimeout);
        }

        // wakeup task
        public bool Set(string waitName)
        {
            var waitHandle = WaitHandle(waitName);
            if (waitHandle != null)
            {
                try
                {
                    return waitHandle.Set();
                }
                finally
                {
                    Debug.WriteLine("Task {0}:{1} signaled", Name, waitName);
                }
            }

            return false;
        }

        // checks if task is running or running pending
        public bool IsRunning
        {
            get { return _task != null && (_task.Status == TaskStatus.Running || _task.Status == TaskStatus.WaitingToRun); }
        }

        // start running task
        public void Run(Action<SyncTask, CancellationToken> action)
        {
            lock (this)
            {
                if (_task != null)
                {
                    if (_task.IsFaulted)
                    {
                        Debug.WriteLine("Task {0} faulted", ToString());
                        if (_cancellationTokenSource != null)
                        {
                            _cancellationTokenSource.Dispose();
                            _cancellationTokenSource = null;
                        }

                        _task.Dispose();
                        _task = null;
                    }
                    else if (_task.IsCanceled)
                        Debug.WriteLine("Task {0} canceled", ToString());
                    else if (_task.IsCompleted)
                        Debug.WriteLine("Task {0} completed", ToString());
                }

                if (_task == null)
                {
                    var token = GetCancellationToken();
                    _task = Task.Run(() => action(this, token), token);
                    Debug.WriteLine("Task {0} {1}", ToString(), _task.Status);
                }
            }
        }

        // cancel task and release if sleeping
        public void Cancel()
        {
            if (_cancellationTokenSource != null)
                _cancellationTokenSource.Cancel();

            Signal();
        }

        #region IDisposable Members

        // clean resources
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_task != null)
                {
                    _task.Dispose();
                    _task = null;
                }

                if (_cancellationTokenSource != null)
                {
                    _cancellationTokenSource.Dispose();
                    _cancellationTokenSource = null;
                }

                lock (_waitHandles)
                {
                    foreach (var waitHandle in _waitHandles.Values)
                    {
                        if (waitHandle != null)
                            waitHandle.Dispose();
                    }
                    _waitHandles.Clear();
                }
            }
        }

        // cleaup resources and references on dispose call
        public void Dispose()
        {
            if (!_isDisposed)
            {
                _isDisposed = true;
                try
                {
                    Dispose(true);
                }
                finally
                {
                    GC.SuppressFinalize(this);
                }
            }
        }

        #endregion

        // finalizer
        ~SyncTask()
        {
            Dispose(false);
        }

        // gets sync task name and status
        public override string ToString()
        {
            return Name;
        }

        #endregion
    }
}