﻿using System;
using System.Diagnostics;

namespace NewHotel.Diagnostics
{
	public static class TraceDebug
	{
		static TraceDebug()
		{
			System.Diagnostics.Trace.IndentSize = 3;
		}

		public static void TraceData(this System.Diagnostics.TraceSource source, int id, object data)
		{
			source.TraceData(System.Diagnostics.TraceEventType.Verbose, id, data);
		}

		public static void TraceData(this System.Diagnostics.TraceSource source, int id, params object[] data)
		{
			source.TraceData(System.Diagnostics.TraceEventType.Verbose, id, data);
		}

		public static void TraceSuspend(this System.Diagnostics.TraceSource source, int id, string message, params object[] args)
		{
			source.TraceEvent(System.Diagnostics.TraceEventType.Suspend, id, message, args);
		}

		public static void TraceResume(this System.Diagnostics.TraceSource source, int id, string message, params object[] args)
		{
			source.TraceEvent(System.Diagnostics.TraceEventType.Resume, id, message, args);
		}

		public static void TraceInformation(this System.Diagnostics.TraceSource source, int id, string message, params object[] args)
		{
			source.TraceEvent(System.Diagnostics.TraceEventType.Information, id, message, args);
		}

		public static void TraceVerbose(this System.Diagnostics.TraceSource source, int id, string message, params object[] args)
		{
			source.TraceEvent(System.Diagnostics.TraceEventType.Verbose, id, message, args);
		}

		public static void TraceWarning(this System.Diagnostics.TraceSource source, int id, string warning, params object[] args)
		{
			source.TraceEvent(System.Diagnostics.TraceEventType.Warning, id, warning, args);
		}

		public static void TraceError(this System.Diagnostics.TraceSource source, int id, string error, params object[] args)
		{
			source.TraceEvent(System.Diagnostics.TraceEventType.Error, id, error, args);
		}

		public static void TraceException(this System.Diagnostics.TraceSource source, int id, Exception ex)
		{
			source.TraceEvent(System.Diagnostics.TraceEventType.Error, id, ex.Message + Environment.NewLine + ex.StackTrace);
			if (ex.InnerException != null)
				source.TraceEvent(System.Diagnostics.TraceEventType.Error, id,
					ex.InnerException.Message + Environment.NewLine + ex.InnerException.StackTrace);
		}

		public static void TraceException(this System.Diagnostics.TraceSource source, Exception ex)
		{
			source.TraceException(0, ex);
		}

		public static void TraceWriteLine(string message, params object[] args)
		{
			System.Diagnostics.Debug.WriteLine(message, args);
		}

		public static void TraceWrite(string message)
		{
			System.Diagnostics.Debug.Write(message);
		}

		public static bool StartOperation(object operation)
		{
			if (operation != null)
			{
				object currOperation = null;
				if (System.Diagnostics.Trace.CorrelationManager.LogicalOperationStack.Count > 0)
					currOperation = System.Diagnostics.Trace.CorrelationManager.LogicalOperationStack.Peek();

				if (!operation.Equals(currOperation))
				{
					System.Diagnostics.Trace.CorrelationManager.StartLogicalOperation(operation.ToString());
					System.Diagnostics.Trace.Indent();
					return true;
				}
			}

			return false;
		}

		public static bool StopOperation()
		{
			//object currOperation = null;
			if (System.Diagnostics.Trace.CorrelationManager.LogicalOperationStack.Count > 0)
			{
				//currOperation = System.Diagnostics.Trace.CorrelationManager.LogicalOperationStack.Peek();
				System.Diagnostics.Trace.Unindent();
				System.Diagnostics.Trace.CorrelationManager.StopLogicalOperation();
				return true;
			}

			return false;
		}

		public static bool StartActivity(Guid newActivityId, out Guid currActivityId)
		{
			currActivityId = System.Diagnostics.Trace.CorrelationManager.ActivityId;
			if (newActivityId != currActivityId)
			{
				System.Diagnostics.Trace.CorrelationManager.ActivityId = newActivityId;
				return true;
			}

			return false;
		}

		public static bool StopActivity(Guid oldActivityId)
		{
			var currActivityId = System.Diagnostics.Trace.CorrelationManager.ActivityId;
			if (oldActivityId != currActivityId)
			{
				System.Diagnostics.Trace.CorrelationManager.ActivityId = oldActivityId;
				return true;
			}

			return false;
		}

		private class TraceActivityScope : IDisposable
		{
			protected readonly int _id;
			protected object _operation;
			protected Guid _activityId;
			private bool _disposed;

			protected virtual void TraceStart(int id, object operation)
			{
			}

			protected virtual void TraceStop(int id, object operation)
			{
			}

			protected virtual void TraceTransfer(int id, object operation, Guid activityId)
			{
			}

			protected void BeginActivityScope(object operation, Guid newActivityId)
			{
				Guid currActivityId;
				//if (StartActivity(newActivityId, out currActivityId))
				//    TraceTransfer(_id, operation, newActivityId);
				StartActivity(newActivityId, out currActivityId);
				_activityId = currActivityId;

				if (operation != null)
				{
					_operation = operation;
					if (StartOperation(_operation))
						TraceStart(_id, _operation);
				}
			}

			private void EndActivityScope()
			{
				if (_operation != null)
				{
					if (StopOperation())
						TraceStop(_id, _operation);
				}

				//if (StopActivity(_activityId))
				//    TraceTransfer(_id, currOperation, _activityId);
				StopActivity(_activityId);
			}

			protected TraceActivityScope(int id)
			{
				_id = id;
			}

			public TraceActivityScope(int id, string newOperationName, Guid newActivityId)
				: this(id)
			{
				BeginActivityScope(newOperationName, newActivityId);
			}

			public void Dispose()
			{
				if (!_disposed)
				{
					_disposed = true;
					EndActivityScope();
				}
			}
		}

		public static IDisposable TraceScope(int id, string name, Guid activityId)
		{
			return new TraceActivityScope(id, name, activityId);
		}

		public static IDisposable TraceScope(int id, string name, string activityId)
		{
			Guid activityGuidId;
			if (Guid.TryParse(activityId, out activityGuidId))
				return new TraceActivityScope(id, name, activityGuidId);

			return new TraceActivityScope(id, name, Guid.Empty);
		}

		public static IDisposable TraceScope(int id, string name)
		{
			return new TraceActivityScope(id, name, Guid.Empty);
		}

		public static IDisposable TraceScope(string name, Guid activityId)
		{
			return TraceScope(0, name, activityId);
		}

		public static IDisposable TraceScope(string name, string activityId)
		{
			return TraceScope(0, name, activityId);
		}

		public static IDisposable TraceScope(string name)
		{
			return TraceScope(0, name, null);
		}

		private sealed class TraceSourceActivityScope : TraceActivityScope
		{
			private readonly System.Diagnostics.TraceSource _source;

			protected override void TraceStart(int id, object operation)
			{
				if (operation != null)
					_source.TraceEvent(System.Diagnostics.TraceEventType.Start, id, operation.ToString());
				else
					_source.TraceEvent(System.Diagnostics.TraceEventType.Start, id);
			}

			protected override void TraceStop(int id, object operation)
			{
				if (operation != null)
					_source.TraceEvent(System.Diagnostics.TraceEventType.Stop, id, operation.ToString());
				else
					_source.TraceEvent(System.Diagnostics.TraceEventType.Stop, id);
			}

			protected override void TraceTransfer(int id, object operation, Guid activityId)
			{
				if (operation != null)
					_source.TraceTransfer(id, operation.ToString(), activityId);
				else
					_source.TraceTransfer(id, string.Empty, activityId);
			}

			public TraceSourceActivityScope(System.Diagnostics.TraceSource source, int id, string newOperationName, Guid newActivityId)
				: base(id)
			{
				_source = source;
				BeginActivityScope(newOperationName, newActivityId);
			}
		}

		public static IDisposable TraceScope(this System.Diagnostics.TraceSource source, int id, string name, Guid activityId)
		{
			return new TraceSourceActivityScope(source, id, name, activityId);
		}

		public static IDisposable TraceScope(this System.Diagnostics.TraceSource source, int id, string name)
		{
			return new TraceSourceActivityScope(source, id, name, Guid.Empty);
		}

		public static IDisposable TraceScope(this System.Diagnostics.TraceSource source, string name)
		{
			return TraceScope(source, 0, name, Guid.Empty);
		}

		public static IDisposable TraceScope(this System.Diagnostics.TraceSource source, int id, string name, string activityId)
		{
			Guid activityGuidId;
			if (Guid.TryParse(activityId, out activityGuidId))
				return new TraceSourceActivityScope(source, id, name, activityGuidId);

			return new TraceSourceActivityScope(source, id, name, Guid.Empty);
		}

		public static IDisposable TraceScope(this System.Diagnostics.TraceSource source, string name, Guid activityId)
		{
			return TraceScope(source, 0, name, activityId);
		}

		public static IDisposable TraceScope(this System.Diagnostics.TraceSource source, Guid activityId)
		{
			return TraceScope(source, 0, null, activityId);
		}

		public static IDisposable TraceScope(this System.Diagnostics.TraceSource source, string name, string activityId)
		{
			return TraceScope(source, 0, name, activityId);
		}

		public static bool ShouldTraceVerbose(this System.Diagnostics.TraceSource source)
		{
			return source.Switch.ShouldTrace(System.Diagnostics.TraceEventType.Verbose);
		}

		public static void RemoveListener(this System.Diagnostics.TraceSource source, string name)
		{
			source.Listeners.Remove(name);
		}

		public static void RemoveListener(string name)
		{
			System.Diagnostics.Trace.Listeners.Remove(name);
		}


		private class ColorConsoleTraceListener : System.Diagnostics.ConsoleTraceListener
		{
			private readonly ConsoleColor _errorColor;
			private readonly ConsoleColor _warningColor;
			private readonly ConsoleColor _verboseColor;

			public ColorConsoleTraceListener(ConsoleColor errorColor, ConsoleColor warningColor, ConsoleColor verboseColor)
			{
				_errorColor = errorColor;
				_warningColor = warningColor;
				_verboseColor = verboseColor;
			}

			private void TraceColor(TraceEventType eventType)
			{
				switch (eventType)
				{
					case TraceEventType.Critical:
					case TraceEventType.Error:
						Console.ForegroundColor = _errorColor;
						break;
					case TraceEventType.Warning:
						Console.ForegroundColor = _warningColor;
						break;
					case TraceEventType.Verbose:
						Console.ForegroundColor = _verboseColor;
						break;
				}
			}

			private static void TraceResetColor(TraceEventType eventType)
			{
				switch (eventType)
				{
					case TraceEventType.Critical:
					case TraceEventType.Error:
					case TraceEventType.Warning:
					case TraceEventType.Verbose:
						Console.ResetColor();
						break;
				}
			}

			public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id)
			{
				TraceColor(eventType);
				try
				{
					base.TraceEvent(eventCache, source, eventType, id);
				}
				finally
				{
					TraceResetColor(eventType);
				}
			}

			public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string message)
			{
				TraceColor(eventType);
				try
				{
					base.TraceEvent(eventCache, source, eventType, id, message);
				}
				finally
				{
					TraceResetColor(eventType);
				}
			}

			public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string format, params object[] args)
			{
				TraceColor(eventType);
				try
				{
					base.TraceEvent(eventCache, source, eventType, id, format, args);
				}
				finally
				{
					TraceResetColor(eventType);
				}
			}
		}

		public static void AppendConsoleTraceListener(this System.Diagnostics.TraceSource source,
			System.Diagnostics.SourceLevels sourceLevels = System.Diagnostics.SourceLevels.All,
			ConsoleColor errorColor = ConsoleColor.Red, ConsoleColor warningColor = ConsoleColor.Yellow, ConsoleColor verboseColor = ConsoleColor.Gray)
		{
			//var listener = new ColorConsoleTraceListener(errorColor, warningColor, verboseColor);
			var listener = new ConsoleTraceListener();
			if (sourceLevels != System.Diagnostics.SourceLevels.All)
				listener.Filter = new System.Diagnostics.EventTypeFilter(sourceLevels);

			source.Listeners.Add(listener);
		}

		public static void AppendEventLogTraceListener(this System.Diagnostics.TraceSource source,
			string logName, string logSourceName, System.Diagnostics.SourceLevels? sourceLevels = null)
		{
			var listener = new System.Diagnostics.EventLogTraceListener(new System.Diagnostics.EventLog(logName) { Source = logSourceName });
			if (sourceLevels.HasValue)
				listener.Filter = new System.Diagnostics.EventTypeFilter(sourceLevels.Value);

			source.Listeners.Add(listener);
		}
	}
}