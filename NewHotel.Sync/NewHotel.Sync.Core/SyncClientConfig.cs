﻿using System;
using System.Configuration;

namespace NewHotel.Sync.Core
{
    public sealed class SyncClientConfig : ConfigurationSection
    {
        #region Constants

        private const string SectionName = "newhotel.sync.client.config";
        private const string ServiceName = "service";
        private const string SchemaName = "schema";
        private const string SchemaPassword = "password";
        private const string ServerAddress = "server";
        private const string PortNumber = "port";
        private const string ImageRootPathName = "images";

        #endregion
        #region Properties

        [ConfigurationProperty(ServiceName, IsRequired = true, IsKey = false, DefaultValue = "ORCL")]
        public string Service
        {
            get { return (string)this[ServiceName]; }
            set { this[ServiceName] = value; }
        }

        [ConfigurationProperty(SchemaName, IsRequired = true, IsKey = true, DefaultValue = "")]
        public string Schema
        {
            get { return (string)this[SchemaName]; }
            set { this[SchemaName] = value; }
        }

        [ConfigurationProperty(SchemaPassword, IsRequired = true, IsKey = false, DefaultValue = "")]
        public string Password
        {
            get { return CryptUtils.Decrypt((string)this[SchemaPassword]); }
            set { this[SchemaPassword] = CryptUtils.Encrypt(value); }
        }

        [ConfigurationProperty(ServerAddress, IsRequired = false, IsKey = false, DefaultValue = "localhost")]
        public string Server
        {
            get { return (string)this[ServerAddress]; }
            set { this[ServerAddress] = value; }
        }

        [ConfigurationProperty(PortNumber, IsRequired = false, IsKey = false, DefaultValue = "8000")]
        public string Port
        {
            get { return (string)this[PortNumber]; }
            set { this[PortNumber] = value; }
        }

        [ConfigurationProperty(ImageRootPathName, IsRequired = false, IsKey = false, DefaultValue = "")]
        public string ImageRootPath
        {
            get { return (string)this[ImageRootPathName]; }
            set { this[ImageRootPathName] = value; }
        }

        #endregion
        #region Methods

        public static SyncClientConfig Instance
        {
            get { return ConfigurationManager.GetSection(SectionName) as SyncClientConfig; }
        }

        #endregion
    }
}