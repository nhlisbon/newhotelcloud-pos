﻿using System;
using System.Text;
using System.IO;
using System.Data;
using NewHotel.Sync.Core;

namespace NewHotel.Sync.Server
{
    public abstract class SyncSetupHelper : ISyncSetupHelper
    {
        private const string ConnectionStringFormat = "User Id={0};Password={1};Data Source={2};Pooling=false;Connection Lifetime=180";

        protected virtual bool GetSchemaCredentials
        {
            get { return false; }
        }

        protected void GrantRights(string serviceName, string sysPassword, string schemaName)
        {
            using (var connection = GetConnection(GetConnectionString("SYS", sysPassword, serviceName, true)))
            {
                connection.Open();
                try
                {
                    Console.WriteLine("Granting rights ...");

                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = string.Format("GRANT CHANGE NOTIFICATION TO {0}", schemaName);
                        command.ExecuteNonQuery();

                        command.CommandText = "ALTER SYSTEM SET job_queue_processes = 1";
                        command.ExecuteNonQuery();
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        protected void RevokeRights(string serviceName, string sysPassword, string schemaName)
        {
            using (var connection = GetConnection(GetConnectionString("SYS", sysPassword, serviceName, true)))
            {
                connection.Open();
                try
                {
                    Console.WriteLine("Revoking rights ...");

                    using (var command = connection.CreateCommand())
                    {
                        try
                        {
                            command.CommandText = string.Format("REVOKE CHANGE NOTIFICATION FROM {0}", schemaName);
                            command.ExecuteNonQuery();
                        }
                        catch
                        {
                        }

                        try
                        {
                            command.CommandText = "ALTER SYSTEM SET job_queue_processes = 0";
                            command.ExecuteNonQuery();
                        }
                        catch
                        {
                        }
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void Install()
        {
            Console.WriteLine();
            Console.Write("Service Name: ");
            var serviceName = Console.ReadLine();
            if (!string.IsNullOrEmpty(serviceName))
            {
                Console.Write("Sys Password: ");
                var sysPassword = Console.ReadLine();
                if (!string.IsNullOrEmpty(sysPassword))
                {
                    Console.Write("Schema Name: ");
                    var schemaName = Console.ReadLine();
                    if (!string.IsNullOrEmpty(schemaName))
                    {
                        var schemaPassword = string.Empty;
                        if (GetSchemaCredentials)
                        {
                            Console.Write("Schema Password: ");
                            schemaPassword = Console.ReadLine();
                        }

                        Console.WriteLine();
                        try
                        {
                            Install(serviceName, schemaName, schemaPassword, sysPassword);
                            Console.WriteLine("Install succeeded.");
                        }
                        catch
                        {
                            Console.WriteLine("Install failed.");
                            throw;
                        }
                    }
                }
            }
        }

        public void Uninstall()
        {
            Console.WriteLine();
            Console.Write("Service Name: ");
            var serviceName = Console.ReadLine();
            if (!string.IsNullOrEmpty(serviceName))
            {
                Console.Write("Sys Password: ");
                var sysPassword = Console.ReadLine();
                if (!string.IsNullOrEmpty(sysPassword))
                {
                    Console.Write("Schema Name: ");
                    var schemaName = Console.ReadLine();
                    if (!string.IsNullOrEmpty(schemaName))
                    {
                        var schemaPassword = string.Empty;
                        if (GetSchemaCredentials)
                        {
                            Console.Write("Schema Password: ");
                            schemaPassword = Console.ReadLine();
                        }

                        Console.WriteLine();
                        try
                        {
                            Uninstall(serviceName, schemaName, schemaPassword, sysPassword);
                            Console.WriteLine("Unstall succeeded.");
                        }
                        catch
                        {
                            Console.WriteLine("Uninstall failed.");
                            throw;
                        }
                    }
                }
            }
        }

        protected static string GetConnectionString(string user, string password, string source, bool asSysDba)
        {
            var connectionString = string.Format(ConnectionStringFormat, user, password, source);
            if (asSysDba)
                connectionString += ";DBA Privilege=SYSDBA";
            return connectionString;
        }

        protected string GetFromManifest(string resourcePath, string name)
        {
            using (var reader = new StreamReader(GetManifestResourceStream(resourcePath + "." + name)))
            {
                return reader.ReadToEnd();
            }
        }

        protected abstract Stream GetManifestResourceStream(string name);
        protected abstract IDbConnection GetConnection(string connectionString);

        protected static void ExecuteScript(IDbConnection connection, params string[] sqls)
        {
            for (var index = 0; index < sqls.Length; index++)
            {
                using (var command = connection.CreateCommand())
                {
                    var commandText = sqls[index].Replace("\r\n", "\n");
                    if (commandText.EndsWith(";"))
                        commandText = commandText.Substring(0, commandText.Length - 1);
                    command.CommandText = commandText;
                    command.ExecuteNonQuery();
                }
            }
        }

        protected static void ExecuteScript(IDbConnection connection, StringBuilder sql)
        {
            ExecuteScript(connection, sql.ToString());
        }

        protected static void WriteScriptToFile(string fileName, string sql)
        {
            var path = string.Format(".\\{0}.sql", fileName);
            File.AppendAllText(path, Environment.NewLine);
            File.AppendAllText(path, sql.ToString());
        }

        protected static void WriteScriptToFile(string fileName, StringBuilder sql)
        {
            WriteScriptToFile(fileName, sql.ToString());
        }

        protected abstract void Uninstall(string serviceName, string schemaName, string schemaPassword, string sysPassword);
        protected abstract void Install(string serviceName, string schemaName, string schemaPassword, string sysPassword);
    }
}
