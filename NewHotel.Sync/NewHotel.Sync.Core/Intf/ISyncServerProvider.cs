﻿using System;

namespace NewHotel.Sync.Server
{
    public interface ISyncServerProvider : IDisposable
    {
        void Start();
        void Stop();
        bool IsRunning { get; }
    }
}