﻿using System;

namespace NewHotel.Sync.Client
{
    public interface ISyncClientProvider : IDisposable
    {
        void Start();
        void Stop();
        bool IsRunning { get; }
    }
}