﻿namespace NewHotel.Sync.Core
{
    public interface ISyncSetupHelper
    {
        void Install();
        void Uninstall();
    }
}
