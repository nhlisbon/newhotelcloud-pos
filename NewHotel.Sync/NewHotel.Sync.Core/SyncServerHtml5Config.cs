﻿using System.Configuration;

namespace NewHotel.Sync.Core
{
    public sealed class SyncServerHtml5Config : ConfigurationSection
    {
        #region Constants

        private const string SectionName = "newhotel.sync.server.html5.config";
        private const string UrlName = "url";
        private const string ApiKeyName = "apikey";
        private const string UsernameName = "username";
        private const string PasswordName = "password";

        #endregion
        #region Properties

        [ConfigurationProperty(UrlName, IsRequired = false, IsKey = false, DefaultValue = "")]
        public string Url
        {
            get { return (string)this[UrlName]; }
            set { this[UrlName] = value; }
        }

        [ConfigurationProperty(ApiKeyName, IsRequired = false, IsKey = false, DefaultValue = "")]
        public string ApiKey
        {
            get { return (string)this[ApiKeyName]; }
            set { this[ApiKeyName] = value; }
        }

        [ConfigurationProperty(UsernameName, IsRequired = false, IsKey = false, DefaultValue = "")]
        public string Username
        {
            get { return (string)this[UsernameName]; }
            set { this[UsernameName] = value; }
        }

        [ConfigurationProperty(PasswordName, IsRequired = false, IsKey = false, DefaultValue = "")]
        public string Password
        {
            get { return (string)this[PasswordName]; }
            set { this[PasswordName] = value; }
        }

        #endregion
        #region Methods

        public static SyncServerHtml5Config Instance
        {
            get { return ConfigurationManager.GetSection(SectionName) as SyncServerHtml5Config; }
        }

        #endregion
    }
}