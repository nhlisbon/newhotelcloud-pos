﻿using System;
using System.Net;
using System.IO;
using System.Collections.Generic;
using RestSharp;
using NewHotel.Sync.Core;
using NewHotel.Sync.Data;

namespace NewHotel.Sync.Server.Pos
{
    public sealed class Html5Provider : SyncProvider
    {
        private readonly string _url;
        private readonly string _username;
        private readonly string _password;
        private readonly string _apikey;
        private readonly IDictionary<Guid, string> _tokens = new Dictionary<Guid, string>();

        class ContentResponse<T>
            where T : class
        {
            public T Content { get; set; }
        }

        class HotelData
        {
            public string HotelFullName { get; set; }
            public string HotelStringConnection { get; set; }
        }

        class TokenData
        {
            public string Token { get; set; }
            public HotelData HotelContext { get; set; }
        }

        class AuthenticateResponse : ContentResponse<TokenData>
        {
        }

        class ImageData
        {
            public string FileDataUri { get; set; }
        }

        class ImageResponse : ContentResponse<ImageData>
        {
        }

        static Html5Provider()
        {
            // traducciones
            AddTableName("TNHT_LITE");
            AddTableName("TNHT_MULT");
            // enumerados
            AddTableName("TNHT_ENUM");
            // productos
            AddTableName("TNHT_GRSE");
            AddTableName("TNHT_CLAS");
            AddTableName("TNHT_GRUP");
            AddTableName("TNHT_FAMI");
            AddTableName("TNHT_SFAM");
            AddTableName("TNHT_COIN");
            AddTableName("TNHT_SEPA");
            AddTableName("TNHT_ARTG");
            AddTableName("TNHT_ARTB");
            // monedas
            AddTableName("TNHT_UNMO");
            // pagos
            AddTableName("TNHT_FORE");
            // impuestos
            AddTableName("TNHT_TIVA");
            // configuraciones
            AddTableName("TNHT_HOTE");
            AddTableName("TNHT_AREA");
            // categorias de botones
            AddTableName("TNHT_BCAT");
            // tarjetas de credito
            AddTableName("TNHT_CACR");
            // menus
            AddTableName("TNHT_MENU");
            AddTableName("TNHT_MEDE");
            // impuestos por servicio
            AddTableName("TNHT_ESIM");
            AddTableName("TNHT_IMSE");
            // descuentos
            AddTableName("TNHT_TIDE");
            // usuarios
            AddTableName("TNHT_UTIL");
            AddTableName("TNHT_GRPR");
            AddTableName("TNHT_PERM");
            AddTableName("TNHT_PEAP");
            AddTableName("TNHT_PEUT");
            AddTableName("TNHT_PREP");
            AddTableName("TNHT_ARTP");
            AddTableName("TNHT_FAPP");
            AddTableName("TNHT_GRPP");
            AddTableName("TNHT_CAJA");
            AddTableName("TNHT_SALO");
            AddTableName("TNHT_MESA");
            AddTableName("TNHT_DUTY");
            AddTableName("TNHT_TPRG");
            AddTableName("TNHT_TPRB");
            AddTableName("TNHT_HOUR");
            AddTableName("TNHT_TPPE");
            AddTableName("TNHT_TPRL");
            AddTableName("TNHT_MTCO");
            AddTableName("TNHT_TICK");
            AddTableName("TNHT_TISU");
            AddTableName("TNHT_IPOS");
            AddTableName("TNHT_SEDO");
            AddTableName("TNHT_ARAR");
            AddTableName("TNHT_PVCI");
            AddTableName("TNHT_PVAR");
            AddTableName("TNHT_HAIP");
            AddTableName("TNHT_IPSL");
            AddTableName("TNHT_BOTV");
            AddTableName("TNHT_CAPV");
            AddTableName("TNHT_IPUT");
            AddTableName("TCFG_NPOS");
        }

        public Html5Provider(string url, string username, string password, string apikey = null)
        {
            _url = url;
            _username = username;
            _password = password;
            _apikey = apikey;
        }

        private RestClient GetRestClient()
        {
            var client = new RestClient(_url);
            client.ThrowOnAnyError = true;

            return client;
        }

        private TokenData Authenticate(RestClient client, Guid hotelId)
        {
            var request = new RestRequest("authenticate", DataFormat.Json)
                .AddJsonBody(new
                {
                    HotelId = hotelId.ToString(),
                    Username = _username,
                    Password = _password,
                    ApplicationId = 101,
                    LanguageId = 1033,
                    Encrypted = true
                });

            var response = client.Post<AuthenticateResponse>(request);
            if (!response.IsSuccessful)
                throw new ApplicationException($"Error trying to authenticate on hotel {hotelId} ({response.StatusCode})");

            if (response.Data.Content != null)
            {
                if (!string.IsNullOrEmpty(response.Data.Content.Token))
                {
                    lock (_tokens)
                    {
                        _tokens[hotelId] = response.Data.Content.Token;
                    }
                }

                return response.Data.Content;
            }

            return null;
        }

        private IRestResponse<ImageResponse> GetImage(RestClient client, Guid hotelId, string path)
        {
            string token;
            lock (_tokens)
            {
                if (!_tokens.TryGetValue(hotelId, out token))
                    token = null;
            }

            if (string.IsNullOrEmpty(token))
            {
                var tokenData = Authenticate(client, hotelId);
                if (tokenData != null && !string.IsNullOrEmpty(tokenData.Token))
                    token = tokenData.Token;
            }

            var request = new RestRequest("posintegration/getimages", DataFormat.Json)
                .AddHeader("Authorization", $"Bearer {token}")
                .AddParameter("path", path);

            return client.Get<ImageResponse>(request);
        }

        public override Stream GetImageStream(Guid hotelId, string path)
        {
            var client = GetRestClient();

            var response = GetImage(client, hotelId, path);
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                _tokens.Remove(hotelId);
                response = GetImage(client, hotelId, path);
            }

            if (!response.IsSuccessful)
                throw new ApplicationException($"Error {response.StatusCode} retrieving image {path}");

            if (response.Data != null && response.Data.Content != null)
            {
                var imageUri = response.Data.Content.FileDataUri;
                if (!string.IsNullOrEmpty(imageUri))
                {
                    const string base64Pattern = ";base64,";

                    var index = imageUri.IndexOf(base64Pattern);
                    if (index < 0)
                        throw new ApplicationException($"Invalid uri format decoding image {path}");

                    var base64Image = imageUri.Substring(index + base64Pattern.Length);

                    try
                    {
                        return new MemoryStream(Convert.FromBase64String(base64Image));
                    }
                    catch (FormatException)
                    {
                        throw new ApplicationException($"Invalid base64 format converting image {path}");
                    }
                }
                else
                    throw new ApplicationException($"Image uri empty for image {path}");
            }

            throw new ApplicationException($"Image {path} doesn't exist");
        }

        public override Tuple<string, string, string, string> GetConnection(Guid hotelId)
        {
            var name = string.Empty;
            var schema = string.Empty;
            var password = string.Empty;
            var service = string.Empty;

            var client = GetRestClient();

            var tokenData = Authenticate(client, hotelId);
            if (tokenData != null && tokenData.HotelContext != null)
            {
                var hotelContext = tokenData.HotelContext;
                if (!string.IsNullOrEmpty(hotelContext.HotelFullName))
                {
                    name = hotelContext.HotelFullName;
                    if (!string.IsNullOrEmpty(hotelContext.HotelStringConnection))
                    {
                        var splitHotelStringConnection = hotelContext.HotelStringConnection.Split(';');
                        if (splitHotelStringConnection.Length == 3)
                        {
                            schema = splitHotelStringConnection[0];
                            password = CryptUtils.Decrypt(splitHotelStringConnection[1]);
                            service = splitHotelStringConnection[2];
                        }
                        else
                            throw new ApplicationException($"Invalid hotel '{name}' string connection: {splitHotelStringConnection}");
                    }
                }
            }

            return Tuple.Create(schema, password, service, name);
        }
    }
}