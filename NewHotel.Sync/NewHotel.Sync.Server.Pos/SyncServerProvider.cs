﻿using System;
using System.ServiceModel;
using System.ComponentModel.Composition;
using System.Diagnostics;

namespace NewHotel.Sync.Server
{
    [Export(typeof(ISyncServerProvider)), PartCreationPolicy(CreationPolicy.NonShared)]
    public class SyncServerProvider : ISyncServerProvider, IDisposable
    {
        private ServiceHost _host;

        private static void OnFaulted(object sender, EventArgs args)
        {
            Trace.TraceError("Service faulted");
        }

        private static void OnOpening(object sender, EventArgs args)
        {
            Trace.TraceInformation("Service opening ...");
        }

        private static void OnOpened(object sender, EventArgs args)
        {
            Trace.TraceInformation("Service opened");
        }

        private static void OnClosing(object sender, EventArgs args)
        {
            Trace.TraceInformation("Service closing ...");
        }

        private static void OnClosed(object sender, EventArgs args)
        {
            Trace.TraceInformation("Service closed");
        }

        public void Start()
        {
            if (_host == null)
            {
                _host = new ServiceHost(typeof(SyncService));
                _host.Faulted += OnFaulted;
                _host.Opening += OnOpening;
                _host.Opened += OnOpened;
                _host.Closing += OnClosing;
                _host.Closed += OnClosed;
                _host.Open();
            }
        }

        public void Stop()
        {
            if (_host != null)
            {
                try
                {
                    SyncService.CancellTasks();
                }
                finally
                {
                    _host.Close();
                    Dispose(true);
                }
            }
        }

        public bool IsRunning
        {
           get { return _host != null && _host.State == CommunicationState.Opened; }
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                var disposable = _host as IDisposable;
                if (disposable != null)
                    disposable.Dispose();

                _host = null;
            }
        }

        public void Dispose()
        {
            Dispose(false);
        }
    }
}
