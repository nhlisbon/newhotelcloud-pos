﻿using System;
using NewHotel.Sync.Core;

namespace NewHotel.Sync.Server
{
    public sealed class ServerSyncTask<T> : SyncTask where T : class
    {
        #region Constants

        public static readonly string[] WaitNames = new string[] { "NHCLOUDPOS", "NHCLOUDHUB" };

        #endregion
        #region Members

        // the callback channel to client
        private T _callbackChannel;
        // sync sequence id counter
        private long _syncCounter;

        #endregion
        #region Constructor

        public ServerSyncTask(Guid id, string name, string description, TimeSpan timeout, T callbackChannel)
            : base(id, name, description, timeout, WaitNames)
        {
            // store callback channel
            _callbackChannel = callbackChannel;
        }

        #endregion
        #region Public Properties

        // sync counter from change sequence
        public long SyncCounter
        {
            get
            {
                lock (this)
                {
                    return _syncCounter;
                }
            }
            set
            {
                lock (this)
                {
                    if (value != _syncCounter)
                        _syncCounter = value;
                }
            }
        }

        // get callback channel
        public T CallbackChannel
        {
            get { return _callbackChannel; }
        }

        #endregion
        #region Public Methods

        #endregion
        #region Protected Methods

        //protected override TraceSource CreateLogger()
        //{
        //    var logger = Log.Source["NewHotel.Sync." + Id.ToHex()];
        //    if (logger.Listeners.Count > 0)
        //        return logger;

        //    return base.CreateLogger();
        //}

        // wakeup from any wait
        protected override void Signal()
        {
            Set(WaitNames[1]);
            Set(WaitNames[0]);
        }

        #endregion
        #region IDisposable Members

        // clean resource usage
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                // remove reference to channel
                _callbackChannel = null;
            }
        }

        #endregion
    }
}