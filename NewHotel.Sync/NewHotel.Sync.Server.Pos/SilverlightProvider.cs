﻿using System;
using System.IO;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using NewHotel.Sync.Core;
using NewHotel.Sync.Data;

namespace NewHotel.Sync.Server.Pos
{
    public sealed class SilverlightProvider : SyncProvider
    {
        private const string ConnectionStringFormat = "User Id={0};Password={1};Data Source={2};Pooling=false;Connection Lifetime=300;Connection Timeout=5";

        private readonly string _rootPath;
        private readonly string _service;
        private readonly string _schema;
        private readonly string _password;

        private readonly static string _connectionCommandText;

        static SilverlightProvider()
        {
            // traducciones
            AddTableName("TNHT_LITE");
            AddTableName("TNHT_MULT");
            // enumerados
            AddTableName("TNHT_ENUM");
            // productos
            AddTableName("TNHT_GRSE");
            AddTableName("TNHT_CLAS");
            AddTableName("TNHT_GRUP");
            AddTableName("TNHT_FAMI");
            AddTableName("TNHT_SFAM");
            AddTableName("TNHT_COIN");
            AddTableName("TNHT_SEPA");
            AddTableName("TNHT_ARTG");
            AddTableName("TNHT_ARTB");
            // monedas
            AddTableName("TNHT_UNMO");
            // pagos
            AddTableName("TNHT_FORE");
            // impuestos
            AddTableName("TNHT_REGI");
            AddTableName("TNHT_SEIM");
            AddTableName("TNHT_TIVA");
            // configuraciones
            AddTableName("TNHT_HOTE");
            AddTableName("TNHT_AREA");
            // categorias de botones
            AddTableName("TNHT_BCAT");
            // tarjetas de credito
            AddTableName("TNHT_CACR");
            // menus
            AddTableName("TNHT_MENU");
            AddTableName("TNHT_MEDE");
            // impuestos por servicio
            AddTableName("TNHT_ESIM");
            AddTableName("TNHT_IMSE");
            // descuentos
            AddTableName("TNHT_TIDE");
            // usuarios
            AddTableName("TNHT_UTIL");
            AddTableName("TNHT_GRPR");
            AddTableName("TNHT_PERM");
            AddTableName("TNHT_PEAP");
            AddTableName("TNHT_PEUT");
            AddTableName("TNHT_PREP");
            AddTableName("TNHT_ARTP");
            AddTableName("TNHT_FAPP");
            AddTableName("TNHT_GRPP");
            AddTableName("TNHT_CAJA");
            AddTableName("TNHT_SALO");
            AddTableName("TNHT_MESA");
            AddTableName("TNHT_DUTY");
            AddTableName("TNHT_TPRG");
            AddTableName("TNHT_TPRB");
            AddTableName("TNHT_HOUR");
            AddTableName("TNHT_TPPE");
            AddTableName("TNHT_TPRL");
            AddTableName("TNHT_MTCO");
            AddTableName("TNHT_TICK");
            AddTableName("TNHT_TISU");
            AddTableName("TNHT_IPOS");
            AddTableName("TNHT_SEDO");
            AddTableName("TNHT_ARAR");
            AddTableName("TNHT_PVCI");
            AddTableName("TNHT_PVAR");
            AddTableName("TNHT_HAIP");
            AddTableName("TNHT_IPSL");
            AddTableName("TNHT_BOTV");
            AddTableName("TNHT_CAPV");
            AddTableName("TNHT_IPUT");
            AddTableName("TCFG_NPOS");

            _connectionCommandText = "SELECT hote_user, hote_pwrd, hote_serv, hote_desc FROM tnht_hote WHERE hote_pk = :hote_pk";
        }

        public static string GetConnectionString(string schema, string password, string service)
        {
            return string.Format(ConnectionStringFormat, schema, password, service);
        }

        public SilverlightProvider(string rootPath, string service, string schema, string password)
        {
            _rootPath = rootPath;
            _service = service;
            _schema = schema;
            _password = password;
        }

        public override Stream GetImageStream(Guid hotelId, string path)
        {
            var filePath = Path.Combine(_rootPath, path);

            try
            {
                return File.OpenRead(filePath);
            }
            catch (UnauthorizedAccessException)
            {
                throw new ApplicationException($"Image {filePath} access error");
            }
            catch (DirectoryNotFoundException)
            {
                throw new ApplicationException($"Image {filePath} directory not found");
            }
            catch (FileNotFoundException)
            {
                throw new ApplicationException($"Image {filePath} not found");
            }
        }

        public override Tuple<string, string, string, string> GetConnection(Guid hotelId)
        {
            using (var connection = new OracleConnection(GetConnectionString(_schema, _password, _service)))
            {
                connection.Open();
                connection.ActionName = "GetConnectionString";
                try
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = _connectionCommandText;

                        using (var param = command.CreateParameter())
                        {
                            param.ParameterName = "hote_pk";
                            param.OracleDbType = OracleDbType.Raw;
                            param.Direction = ParameterDirection.Input;
                            param.Value = hotelId.ToByteArray();
                            command.Parameters.Add(param);

                            using (var reader = command.ExecuteReader(CommandBehavior.SingleRow))
                            {
                                try
                                {
                                    if (reader.Read())
                                    {
                                        var schema = reader.GetString(0);
                                        var password = CryptUtils.Decrypt(reader.GetString(1));
                                        var service = reader.GetString(2);
                                        var name = reader.GetString(3);

                                        return Tuple.Create(schema, password, service, name);
                                    }
                                }
                                finally
                                {
                                    reader.Close();
                                }
                            }
                        }
                    }
                }
                finally
                {
                    connection.ActionName = null;
                    connection.Close();
                }

                return Tuple.Create(string.Empty, string.Empty, string.Empty, string.Empty);
            }
        }
    }
}