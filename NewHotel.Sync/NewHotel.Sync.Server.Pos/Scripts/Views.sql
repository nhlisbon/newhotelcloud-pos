﻿CREATE OR REPLACE VIEW sync_tcfg_npos AS
SELECT npos.rowid AS row_id, npos.hote_pk,
npos.fast_paym, npos.para_fore, npos.para_ivin, npos.para_esim, npos.para_tick,
npos.para_mtco, npos.para_tico, npos.para_npas, npos.para_tise, npos.para_tipe,
npos.para_tivl, npos.para_tiat, npos.open_plan, npos.coin_valo, npos.desc_valo,
npos.tick_fisc, npos.datr_dasy, fact.sign_tipo
FROM tcfg_npos npos
inner join tcfg_fact fact on fact.hote_pk = npos.hote_pk;
CREATE OR REPLACE VIEW sync_tnht_hote AS
SELECT hote.rowid AS row_id, hote.hote_pk, hote.hote_abre, hote.hote_desc, hote.hote_caux, tcfg.naci_pk
FROM tnht_hote hote
inner join tcfg_hote tcfg on tcfg.hote_pk = hote.hote_pk;
CREATE OR REPLACE VIEW sync_tcfg_hote AS
SELECT tcfg.rowid AS row_id, hote.hote_pk, hote.hote_abre, hote.hote_desc, hote.hote_caux, tcfg.naci_pk
FROM tnht_hote hote
inner join tcfg_hote tcfg on tcfg.hote_pk = hote.hote_pk;
CREATE OR REPLACE VIEW sync_tnht_tide AS
SELECT rowid AS row_id, tide_pk, lite_pk, hote_pk, appl_pk, tide_fixe, tide_vmin, tide_vmax
FROM tnht_tide;
CREATE OR REPLACE VIEW sync_tnht_esim AS
SELECT rowid AS row_id, esim_pk, hote_pk, lite_pk, esim_defa
FROM tnht_esim;
CREATE OR REPLACE VIEW sync_tnht_cacr AS
SELECT rowid AS row_id, cacr_pk, hote_pk, lite_pk
FROM tnht_cacr;
CREATE OR REPLACE VIEW sync_tnht_grup AS
SELECT rowid AS row_id, grup_pk, grup_cheq, grup_reca
FROM tnht_grup;
CREATE OR REPLACE VIEW sync_tnht_fami AS
SELECT rowid AS row_id, fami_pk, grup_pk
FROM tnht_fami;
CREATE OR REPLACE VIEW sync_tnht_bserv AS
SELECT bserv.rowid AS row_id, artg.artg_pk, bserv.hote_pk, bserv.lite_abre, bserv.lite_desc,
bartg.artg_codi, bartg.fami_pk, bartg.sfam_pk,
artg.sepa_pk, artg.artg_step, artg.artg_colo, artg.artg_tipr,
bserv.serv_roun AS artg_roun, bserv.serv_unme AS artg_unme, bserv.serv_loca AS artg_loca,
bserv.serv_csti AS artg_csti, bserv.serv_cncm AS artg_cncm, bserv.serv_cfop AS artg_cfop,
bserv.serv_inac AS artg_inac
FROM tnht_artg artg
inner join tnht_bserv bserv on bserv.serv_pk = artg.artg_pk
inner join tnht_bartg bartg on bartg.bartg_pk = artg.artg_pk;
CREATE OR REPLACE VIEW sync_tnht_bartg AS
SELECT bartg.rowid AS row_id, artg.artg_pk, bserv.hote_pk, bserv.lite_abre, bserv.lite_desc,
bartg.artg_codi, bartg.fami_pk, bartg.sfam_pk,
artg.sepa_pk, artg.artg_step, artg.artg_colo, artg.artg_tipr
FROM tnht_artg artg
inner join tnht_bserv bserv on bserv.serv_pk = artg.artg_pk
inner join tnht_bartg bartg on bartg.bartg_pk = artg.artg_pk;
CREATE OR REPLACE VIEW sync_tnht_arim AS
SELECT rowid AS row_id, arim_pk, bartg_pk AS artg_pk, arim_path
FROM tnht_arim WHERE arim_tipo = 2702;
CREATE OR REPLACE VIEW sync_tnht_artg AS
SELECT artg.rowid AS row_id, artg.artg_pk,
artg.sepa_pk, artg.artg_step, artg.artg_colo, artg.artg_tipr
FROM tnht_artg artg;
CREATE OR REPLACE VIEW sync_tnht_bcat AS
SELECT rowid AS row_id, bcat_pk, hote_pk, lite_pk, null AS bcat_cate, bcat_orde
FROM tnht_bcat
union all
SELECT rowid AS row_id, bcat_pk, hote_pk, lite_pk, bcat_cate, bcat_orde
FROM tnht_bcat WHERE bcat_cate is not null;
CREATE OR REPLACE VIEW sync_tnht_botv AS
SELECT rowid AS row_id, botv_pk, bcat_pk, ipos_pk, bartg_pk AS artg_pk, botv_orde, null AS botv_next
FROM tnht_botv
union all
SELECT rowid AS row_id, botv_pk, bcat_pk, ipos_pk, bartg_pk AS artg_pk, botv_orde, botv_next
FROM tnht_botv WHERE botv_next is not null;
CREATE OR REPLACE VIEW sync_tnht_foap AS
SELECT fore.rowid AS row_id, fore.fore_pk, fore.hote_pk, fore.lite_abre, fore.lite_desc, fore.unmo_pk, fore.fore_cash, fore.fore_cacr, fore.repo_cash, fori.fori_path AS  fore_path, fore.fore_orde, fore.fore_inac
FROM tnht_fore fore
inner join tnht_foap foap on foap.fore_pk = fore.fore_pk and foap.appl_pk = 2
left join tnht_fori fori on fori.fore_pk = fore.fore_pk;
CREATE OR REPLACE VIEW sync_tnht_fori AS
SELECT fore.rowid AS row_id, fore.fore_pk, fore.hote_pk, fore.lite_abre, fore.lite_desc,
fore.unmo_pk, fore.fore_cash, fore.fore_cacr, fore.repo_cash, fori.fori_path AS fore_path, fore.fore_orde, fore.fore_inac
FROM tnht_fore fore
inner join tnht_foap foap on foap.fore_pk = fore.fore_pk and foap.appl_pk = 2
left join tnht_fori fori on fori.fore_pk = fore.fore_pk;
CREATE OR REPLACE VIEW sync_tnht_ipos AS
SELECT ipos.rowid AS row_id, ipos.ipos_pk, secc.hote_pk, secc.lite_abre, secc.lite_desc, ipos.ipos_fctr, ipos.ipos_tutr, ipos.tprg_stdr, ipos.tprg_cint, ipos.tprg_cipo, ipos.tprg_pens, ipos.tprg_pepo,
ipos.cnfg_tick, ipos.cnfg_fact, ipos.cnfg_comp, ipos.ipos_ccop, ipos.ipos_ckab, ipos.ipos_kiti, ipos.ipos_comp, ipos.ipos_tise, ipos.ipos_tipe,
ipos.ipos_tivl, ipos.ipos_tiat, ipos.ipos_tion,
ipos.menu_mon, ipos.menu_tue, ipos.menu_wed, ipos.menu_thu, ipos.menu_fri, ipos.menu_sat, ipos.menu_sun, ipos.ctrl_pk, secc.secc_inac AS ipos_inac
FROM tnht_ipos ipos
inner join tnht_secc secc on secc.secc_pk = ipos.ipos_pk;
CREATE OR REPLACE VIEW sync_tnht_secc AS
SELECT secc.rowid AS row_id, ipos.ipos_pk, secc.hote_pk, secc.lite_abre, secc.lite_desc, ipos.ipos_fctr, ipos.ipos_tutr, ipos.tprg_stdr, ipos.tprg_cint, ipos.tprg_cipo, ipos.tprg_pens, ipos.tprg_pepo,
ipos.cnfg_tick, ipos.cnfg_fact, ipos.cnfg_comp, ipos.ipos_ccop, ipos.ipos_ckab, ipos.ipos_kiti, ipos.ipos_comp, ipos.ipos_tise, ipos.ipos_tipe,
ipos.ipos_tivl, ipos.ipos_tiat, ipos.ipos_tion,
ipos.menu_mon, ipos.menu_tue, ipos.menu_wed, ipos.menu_thu, ipos.menu_fri, ipos.menu_sat, ipos.menu_sun, ipos.ctrl_pk, secc.secc_inac AS ipos_inac
FROM tnht_ipos ipos
inner join tnht_secc secc on secc.secc_pk = ipos.ipos_pk;
CREATE OR REPLACE VIEW sync_tnht_imse AS
SELECT imse.rowid AS row_id, imse.imse_pk, imse.serv_pk AS artg_pk, imse.esim_pk, imse.tiva_cod1, imse.tiva_cod2, imse.tiva_cod3,
imse.tiva_apl2, imse.tiva_apl3, imse.tiva_ret1, imse.tiva_ret2, imse.tiva_ret3
FROM tnht_imse imse inner join tnht_artg artg on artg.artg_pk = imse.serv_pk;
CREATE OR REPLACE VIEW sync_tnht_sedo AS
SELECT sedo.rowid AS row_id, sedo.sedo_pk, sedo.hote_pk, doho.tido_pk, doho.ipos_pk, doho.caja_pk, sedo.sedo_nufa, sedo.sedo_nufi, sedo.sedo_dafi,
sedo.sedo_seac, sedo.sedo_ptex, sedo.sedo_pnum, sedo.sedo_orde, sedo.sedo_saft
FROM tnht_sedo sedo
inner join tnht_doho doho on doho.sedo_pk = sedo.sedo_pk and doho.hote_pk = sedo.hote_pk
where doho.ipos_pk is not null and doho.caja_pk is not null;
CREATE OR REPLACE VIEW sync_tnht_doho AS
SELECT doho.rowid AS row_id, sedo.sedo_pk, sedo.hote_pk, doho.tido_pk, doho.ipos_pk, doho.caja_pk, sedo.sedo_nufa, sedo.sedo_nufi, sedo.sedo_dafi,
sedo.sedo_seac, sedo.sedo_ptex, sedo.sedo_pnum, sedo.sedo_orde, sedo.sedo_saft
FROM tnht_sedo sedo
inner join tnht_doho doho on doho.sedo_pk = sedo.sedo_pk and doho.hote_pk = sedo.hote_pk
where doho.ipos_pk is not null and doho.caja_pk is not null;
CREATE OR REPLACE VIEW sync_tnht_util AS
SELECT rowid AS row_id, util_pk, hote_pk, util_login, util_code, util_desc, util_pass, util_inte, util_inac, util_mgr
FROM tnht_util;
CREATE OR REPLACE VIEW sync_tnht_caja AS
SELECT rowid AS row_id, caja_pk, hote_pk, lite_pk, caja_veca, ipos_empc
FROM tnht_caja;
CREATE OR REPLACE VIEW sync_tnht_mesa AS
SELECT rowid AS row_id, mesa_pk, mesa_desc, salo_pk, mesa_paxs, mesa_type, mesa_row, mesa_colu, mesa_widt, mesa_heig
FROM tnht_mesa;
CREATE OR REPLACE VIEW sync_tnht_tprg AS
SELECT rowid AS row_id, tprg_pk, hote_pk, lite_pk, unmo_pk, ivas_incl, tprg_inac, tprg_type
FROM tnht_tprg;
CREATE OR REPLACE VIEW sync_tnht_tppe AS
SELECT rowid AS row_id, tppe_pk, tprg_pk, tppe_dain, tppe_dafi
FROM tnht_tppe;
CREATE OR REPLACE VIEW sync_tnht_area AS
SELECT rowid AS row_id, area_pk, lite_pk, hote_pk, area_colo, area_nuco, area_inac
FROM tnht_area;
CREATE OR REPLACE VIEW sync_tnht_mtco AS
SELECT rowid AS row_id, mtco_pk, lite_pk, hote_pk, mtco_tica
FROM tnht_mtco;
CREATE OR REPLACE VIEW sync_tnht_peap AS
SELECT rowid AS row_id, peap_pk, perm_pk, appl_pk
FROM tnht_peap WHERE appl_pk = 2;
CREATE OR REPLACE VIEW sync_tnht_peut AS
SELECT peut.rowid AS row_id, peut.peut_pk, peut.util_pk, peut.peap_pk, peut.secu_code
FROM tnht_peut peut inner join tnht_peap peap on peap.peap_pk = peut.peap_pk
where peap.appl_pk = 2;
CREATE OR REPLACE VIEW sync_tnht_tiva AS
SELECT rowid AS row_id, tiva_pk,
lite_pk, seim_pk, regi_pk, tiva_perc, tiva_inac, tiva_lmod
FROM tnht_tiva;
CREATE OR REPLACE VIEW sync_tnht_lite AS SELECT lite.rowid AS row_id, lite.* FROM tnht_lite lite;
CREATE OR REPLACE VIEW sync_tnht_mult AS SELECT mult.rowid AS row_id, mult.* FROM tnht_mult mult;
CREATE OR REPLACE VIEW sync_tnht_enum AS SELECT enum.rowid AS row_id, enum.* FROM tnht_enum enum;
CREATE OR REPLACE VIEW sync_tnht_clas AS SELECT clas.rowid AS row_id, clas.* FROM tnht_clas clas;
CREATE OR REPLACE VIEW sync_tnht_sfam AS SELECT sfam.rowid AS row_id, sfam.* FROM tnht_sfam sfam;
CREATE OR REPLACE VIEW sync_tnht_coin AS SELECT coin.rowid AS row_id, coin.* FROM tnht_coin coin;
CREATE OR REPLACE VIEW sync_tnht_sepa AS SELECT sepa.rowid AS row_id, sepa.* FROM tnht_sepa sepa;
CREATE OR REPLACE VIEW sync_tnht_unmo AS SELECT unmo.rowid AS row_id, unmo.* FROM tnht_unmo unmo;
CREATE OR REPLACE VIEW sync_tnht_regi AS SELECT regi.rowid AS row_id, regi.* FROM tnht_regi regi;
CREATE OR REPLACE VIEW sync_tnht_seim AS SELECT seim.rowid AS row_id, seim.* FROM tnht_seim seim;
CREATE OR REPLACE VIEW sync_tnht_menu AS SELECT menu.rowid AS row_id, menu.* FROM tnht_menu menu;
CREATE OR REPLACE VIEW sync_tnht_mede AS SELECT mede.rowid AS row_id, mede.* FROM tnht_mede mede;
CREATE OR REPLACE VIEW sync_tnht_grpr AS SELECT grpr.rowid AS row_id, grpr.* FROM tnht_grpr grpr;
CREATE OR REPLACE VIEW sync_tnht_perm AS SELECT perm.rowid AS row_id, perm.* FROM tnht_perm perm;
CREATE OR REPLACE VIEW sync_tnht_prep AS SELECT prep.rowid AS row_id, prep.* FROM tnht_prep prep;
CREATE OR REPLACE VIEW sync_tnht_artp AS SELECT artp.rowid AS row_id, artp.* FROM tnht_artp artp;
CREATE OR REPLACE VIEW sync_tnht_fapp AS SELECT fapp.rowid AS row_id, fapp.* FROM tnht_fapp fapp;
CREATE OR REPLACE VIEW sync_tnht_grpp AS SELECT grpp.rowid AS row_id, grpp.* FROM tnht_grpp grpp;
CREATE OR REPLACE VIEW sync_tnht_salo AS SELECT salo.rowid AS row_id, salo.* FROM tnht_salo salo;
CREATE OR REPLACE VIEW sync_tnht_duty AS SELECT duty.rowid AS row_id, duty.* FROM tnht_duty duty;
CREATE OR REPLACE VIEW sync_tnht_tprb AS SELECT tprb.rowid AS row_id, tprb.* FROM tnht_tprb tprb;
CREATE OR REPLACE VIEW sync_tnht_hour AS SELECT hour.rowid AS row_id, hour.* FROM tnht_hour hour;
CREATE OR REPLACE VIEW sync_tnht_tprl AS SELECT tprl.rowid AS row_id, tprl.* FROM tnht_tprl tprl;
CREATE OR REPLACE VIEW sync_tnht_tick AS SELECT tick.rowid AS row_id, tick.* FROM tnht_tick tick;
CREATE OR REPLACE VIEW sync_tnht_arar AS SELECT arar.rowid AS row_id, arar.* FROM tnht_arar arar;
CREATE OR REPLACE VIEW sync_tnht_pvci AS SELECT pvci.rowid AS row_id, pvci.* FROM tnht_pvci pvci;
CREATE OR REPLACE VIEW sync_tnht_pvar AS SELECT pvar.rowid AS row_id, pvar.* FROM tnht_pvar pvar;
CREATE OR REPLACE VIEW sync_tnht_haip AS SELECT haip.rowid AS row_id, haip.* FROM tnht_haip haip;
CREATE OR REPLACE VIEW sync_tnht_ipsl AS SELECT ipsl.rowid AS row_id, ipsl.* FROM tnht_ipsl ipsl;
CREATE OR REPLACE VIEW sync_tnht_capv AS SELECT capv.rowid AS row_id, capv.* FROM tnht_capv capv;
CREATE OR REPLACE VIEW sync_tnht_iput AS SELECT iput.rowid AS row_id, iput.* FROM tnht_iput iput;
CREATE OR REPLACE VIEW sync_tnht_comu AS SELECT comu.rowid AS row_id, comu.* FROM tnht_comu comu;
CREATE OR REPLACE VIEW sync_tnht_dist AS SELECT dist.rowid AS row_id, dist.* FROM tnht_dist dist;
CREATE OR REPLACE VIEW sync_tnht_benti AS
SELECT benti.rowid AS row_id, benti.benti_pk AS clie_pk,
cont.cont_apel || ', ' || benti.benti_desc AS clie_nome,
benti.enti_caux AS clie_caux, benti.enti_lock AS clie_lock,
benti.fisc_numb, benti.fisc_addr1, benti.fisc_addr2, benti.fisc_loca, benti.fisc_copo,
benti.home_phone, benti.email_addr, benti.regi_fisc, benti.fisc_dist, cont.naci_pk
FROM tnht_clie clie inner join tnht_cont cont on cont.cont_pk = clie.clie_pk
inner join tnht_benti benti on benti.benti_pk = cont.cont_pk; 
CREATE OR REPLACE VIEW sync_tnht_cont AS
SELECT cont.rowid AS row_id, benti.benti_pk AS clie_pk,
cont.cont_apel || ', ' || benti.benti_desc AS clie_nome, cont.naci_pk
FROM tnht_clie clie inner join tnht_cont cont on cont.cont_pk = clie.clie_pk
inner join tnht_benti benti on benti.benti_pk = cont.cont_pk;
CREATE OR REPLACE VIEW sync_tnht_vend AS
SELECT vend.rowid AS row_id, vend.vend_pk,
'1' AS vend_anul, anul.mtco_pk, anul.anul_daan, anul.anul_hore AS anul_dare, anul.anul_obse, anul.util_pk AS anul_util
FROM tnht_vend vend inner join tnht_anul anul on anul.anul_pk = vend.anul_pk;
CREATE OR REPLACE VIEW sync_tnht_arve AS
SELECT rowid AS row_id, arve_pk, arve_anul
FROM tnht_arve WHERE arve_anul = 1;