﻿using System.ServiceModel;
using System.ServiceModel.Channels;

namespace NewHotel.Sync.Server
{
    public interface ISyncServiceCallBack
    {
        [OperationContract(IsOneWay = true, Action = SyncService.OnSyncChangesAction)]
        void OnSyncChanges(Message message);
    }
}