﻿using System;
using System.IO;
using System.Data;
using System.Reflection;
using System.ComponentModel.Composition;
using Oracle.ManagedDataAccess.Client;
using NewHotel.Sync.Core;

namespace NewHotel.Sync.Server
{
    [Export(typeof(ISyncSetupHelper)), PartCreationPolicy(CreationPolicy.Shared)]
    public class SyncServerSetupHelper : SyncSetupHelper
    {
        protected override Stream GetManifestResourceStream(string name)
        {
            return Assembly.GetExecutingAssembly().GetManifestResourceStream(name);
        }

        protected override IDbConnection GetConnection(string connectionString)
        {
            return new OracleConnection(connectionString);
        }

        protected override bool GetSchemaCredentials
        {
            get { return true; }
        }

        protected override void Install(string serviceName, string schemaName, string schemaPassword, string sysPassword)
        {
            GrantRights(serviceName, sysPassword, schemaName);
            /*
            using (var connection = new OracleConnection(GetConnectionString(schemaName, schemaPassword, serviceName, false)))
            {
                connection.Open();
                try
                {
                    Console.WriteLine("Enabling triggers ...");

                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "select trigger_name from user_triggers where trigger_name like 'CHANGE_NOTIFICATION_%' and status = 'DISABLED'";

                        using (var reader = command.ExecuteReader(CommandBehavior.SingleResult))
                        {
                            try
                            {
                                while (reader.Read())
                                {
                                    var triggerName = reader.GetString(0);
                                    ExecuteScript(connection, string.Format("alter trigger {0} enable", triggerName));
                                    Console.WriteLine("Trigger {0} enabled.", triggerName);
                                }
                            }
                            finally
                            {
                                reader.Close();
                            }
                        }
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
            */
        }

        protected override void Uninstall(string serviceName, string schemaName, string schemaPassword, string sysPassword)
        {
            /*
            using (var connection = new OracleConnection(GetConnectionString(schemaName, schemaPassword, serviceName, false)))
            {
                connection.Open();
                try
                {
                    Console.WriteLine("Disabling triggers ...");

                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "select trigger_name from user_triggers where trigger_name like 'CHANGE_NOTIFICATION_%' and status = 'ENABLED'";

                        using (var reader = command.ExecuteReader(CommandBehavior.SingleResult))
                        {
                            try
                            {
                                while (reader.Read())
                                {
                                    var triggerName = reader.GetString(0);
                                    ExecuteScript(connection, string.Format("alter trigger {0} disable", triggerName));
                                    Console.WriteLine("Trigger {0} disabled.", triggerName);
                                }
                            }
                            finally
                            {
                                reader.Close();
                            }
                        }
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
            */
            RevokeRights(serviceName, sysPassword, schemaName);
        }
    }
}
