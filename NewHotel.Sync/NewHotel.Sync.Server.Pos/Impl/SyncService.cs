﻿//#define dcn
using System;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading;
using System.Threading.Tasks;
using System.Security.AccessControl;
using System.Diagnostics;
using Oracle.ManagedDataAccess.Client;
using NewHotel.Sync.Core;
using NewHotel.Sync.Data;
using NewHotel.Diagnostics;
using NewHotel.Sync.Server.Pos;

namespace NewHotel.Sync.Server
{
    [ServiceBehavior(ValidateMustUnderstand = false, UseSynchronizationContext = false, InstanceContextMode = InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Multiple)]
    [CallbackBehavior(ValidateMustUnderstand = false, UseSynchronizationContext = false, ConcurrencyMode = ConcurrencyMode.Single)]
    public class SyncService : ISyncService
    {
        public const string OnSyncChangesAction = "SyncChanges";

        private const long FetchRows = 100;
        private const string SyncQueryTemplate = "SELECT * FROM sync_{0}";
        private const string HubEventName = "NHCLOUDHUB";
        private const string PosEventName = "NHCLOUDPOS";

        //private static string ChangeFullLogCommandText;
        private static string ChangeLogCommandText;
        private static string ChangedDataTableCommandText;
        private static string ChangedDataRowCommandText;
        private static string ClearLogCommandText;

        private static readonly IDictionary<ISyncServiceCallBack, ServerSyncTask<ISyncServiceCallBack>> _channels =
            new Dictionary<ISyncServiceCallBack, ServerSyncTask<ISyncServiceCallBack>>();
        private static readonly IDictionary<Guid, string> _connections =
            new Dictionary<Guid, string>();
        private static readonly IDictionary<Guid, OracleDependency> _notifications =
            new Dictionary<Guid, OracleDependency>();

        private static readonly TimeSpan _syncDelay = TimeSpan.FromMinutes(1);
        private static readonly TimeSpan _syncResponseDelay = TimeSpan.FromSeconds(5);

        private static TraceSource _logger;
        private static SyncProvider _syncProvider;

        private struct ChangeLog : IEquatable<ChangeLog>
        {
            public readonly static ChangeLog Empty = new ChangeLog(0L, string.Empty, string.Empty, 'M');

            public long FromId;
            public long ToId;
            public string LocalName;
            public string RemoteName;
            public char Operation;

            public ChangeLog(long id, string localName, string remoteName, char operation)
            {
                FromId = id;
                ToId = id;
                LocalName = localName;
                RemoteName = remoteName;
                Operation = ResolveOperation(localName, operation);
            }

            private static char ResolveOperation(string name, char operation)
            {
                switch (name)
                {
                    case "TNHT_VEND":
                    case "TNHT_ARVE":
                        return 'U';
                }

                return operation;
            }

            public bool Equals(ChangeLog other)
            {
                return other.LocalName == LocalName && other.Operation == Operation;
            }
        }

        static SyncService()
        {
            _logger = Log.Source["NewHotel.Sync"];

            // init database commands
            var sql = new StringBuilder();
            /*
            ChangeFullLogCommandText = "SELECT local_name, remote_name FROM change_notification_tabs ORDER BY id";

            sql = new StringBuilder();
            sql.AppendLine("SELECT cn.change_id, cn.change_table_name as change_local_name, cnt.remote_name as change_remote_name, cn.change_operation");
            sql.AppendLine("FROM change_notification cn INNER JOIN change_notification_tabs cnt ON cnt.local_name = cn.change_table_name");
            sql.AppendLine("WHERE cn.change_id > :change_id");
            sql.Append("ORDER BY cn.change_id");
            */
            sql.AppendLine("SELECT change_id, change_table_name, change_operation");
            sql.AppendLine("FROM change_notification");
            sql.AppendLine("WHERE change_id > :change_id");
            sql.Append("ORDER BY change_id");
            ChangeLogCommandText = sql.Replace("\r\n", "\n").ToString();

            sql = new StringBuilder();
            sql.AppendLine("SELECT null as change_id, :change_operation as change_operation,");
            sql.AppendLine("null as change_delete_pk, tmp.*");
            sql.Append("FROM ({0}) tmp");
            ChangedDataTableCommandText = sql.Replace("\r\n", "\n").ToString();

            sql = new StringBuilder();
            sql.AppendLine("SELECT cn.change_id, cn.change_operation,");
            sql.AppendLine("cn.change_delete_pk, tmp.*");
            sql.AppendLine("FROM change_notification cn");
            sql.AppendLine("LEFT JOIN ({0}) tmp");
            sql.AppendLine("ON tmp.row_id = cn.change_row_id");
            sql.AppendLine("WHERE cn.change_operation != :delete_change_operation");
            sql.AppendLine("AND cn.change_id between :change_id_min and :change_id_max");
            sql.AppendLine("UNION ALL");
            sql.AppendLine("SELECT cn.change_id, cn.change_operation,");
            sql.AppendLine("cn.change_delete_pk, tmp.*");
            sql.AppendLine("FROM change_notification cn");
            sql.AppendLine("LEFT JOIN ({0}) tmp");
            sql.AppendLine("ON tmp.row_id = cn.change_row_id");
            sql.AppendLine("WHERE cn.change_operation = :delete_change_operation");
            sql.AppendLine("AND cn.change_id between :change_id_min and :change_id_max");
            sql.Append("ORDER BY 1");
            ChangedDataRowCommandText = sql.Replace("\r\n", "\n").ToString();

            sql = new StringBuilder();
            ClearLogCommandText = "DELETE FROM change_notification WHERE change_id < :change_id";

            //SyncUtil.AddCommandBuilder("TNHT_BENTI", GetBaseEntityInsertCommand);

            ReadConfig();
        }

        // read configuration file
        public static void ReadConfig()
        {
            try
            {
                if (SyncServerHtml5Config.Instance == null)
                {
                    // init database connection info
                    var instance = SyncServerConfig.Instance;

                    var imageRootPath = instance.ImageRootPath;
                    Logger.TraceInformation(0, "Image root path: {ImageRootPath}", imageRootPath);

                    var service = instance.Service;
                    var schema = instance.Schema;
                    var password = instance.Password;
                    Logger.TraceInformation(0, "Connection config: {@Connection}",
                        new { Service = instance.Service, Schema = instance.Schema, Password = instance.Password });

                    SyncUtil.SetConnection(service, schema, password);

                    _syncProvider = new SilverlightProvider(imageRootPath, service, schema, password);
                }
                else
                {
                    // init database connection info
                    var instance = SyncServerHtml5Config.Instance;

                    var url = instance.Url;
                    var apiKey = instance.ApiKey;
                    var username = instance.Username;
                    var password = instance.Password;

                    Logger.TraceInformation(0, "Connection config: {@Connection}",
                        new { Url = url, ApiKey = apiKey, Username = username, Password = password });

                    SyncUtil.SetManagerConnection(url, apiKey, username, password);

                    _syncProvider = new Html5Provider(url, username, password);
                }
            }
            catch (Exception ex)
            {
                Logger.TraceError(0, ex.Message);
                throw;
            }
        }

        //get trace logger
        private static TraceSource Logger
        {
            get { return _logger; }
        }

        // Get logger based on task id
        private static TraceSource GetLogger(Guid id)
        {
            return _logger;
        }

        private static bool GetBaseEntityInsertCommand(string operation, string name, StringBuilder sql,
            HashSet<string> pks, IEnumerable<string> inserting, IEnumerable<string> updating)
        {
            switch (operation)
            {
                case "I":
                    sql.AppendFormat("MERGE INTO {0} TEMP USING (", name);
                    sql.AppendLine();
                    sql.AppendLine("SELECT GRUP.GRSE_PK, GRUP.CATS_PK FROM TNHT_GRUP GRUP");
                    sql.AppendLine("INNER JOIN TNHT_FAMI FAMI ON FAMI.GRUP_PK = GRUP.GRUP_PK");
                    sql.AppendLine("WHERE FAMI.FAMI_PK = :FAMI_PK");
                    sql.Append(") GRSE ON (");
                    sql.Append(string.Join(" AND ", pks.Select(p => "TEMP." + p + " = :" + p)));
                    sql.AppendLine(")");
                    sql.AppendLine("WHEN NOT MATCHED THEN INSERT (");
                    sql.Append(string.Join("," + Environment.NewLine,
                        inserting.Where(p => p != "FAMI_PK")));
                    sql.AppendLine(")");
                    sql.AppendLine("VALUES (");
                    sql.Append(string.Join("," + Environment.NewLine,
                        inserting.Where(p => p != "FAMI_PK").Select(p => (p == "GRSE_PK" || p == "CATS_PK" ? "GRSE." : ":") + p)));
                    sql.Append(")");
                    return true;
            }

            return false;
        }

        // request all sync task cancellation
        public static void CancellTasks()
        {
            lock (_channels)
            {
                var tasks = new List<Task>(_channels.Count);
                foreach (var syncTask in _channels.Values)
                {
                    tasks.Add(syncTask.Task);
                    if (syncTask.IsRunning)
                        syncTask.Cancel();
                }

                if (tasks.Count > 0)
                {
                    Debug.WriteLine("Waiting for tasks completion ...");
                    try
                    {
                        Task.WaitAll(tasks.ToArray());
                    }
                    catch (AggregateException)
                    {
                    }

                    foreach (var syncTask in _channels.Values)
                        Debug.WriteLine("Task: {0}", syncTask);
                }

                _channels.Clear();
            }
        }

        // register callback
        private static void RegisterCallback(ServerSyncTask<ISyncServiceCallBack> syncTask)
        {
            _channels.Add(syncTask.CallbackChannel, syncTask);
            Debug.WriteLine("Sync callback registered: {0}", _channels.Count);
        }

        // unregister callback
        private static void UnregisterCallback(ISyncServiceCallBack callback, bool faulted)
        {
            if (callback != null)
            {
                lock (_channels)
                {
                    ServerSyncTask<ISyncServiceCallBack> syncTask;
                    if (_channels.TryGetValue(callback, out syncTask))
                    {
                        _channels.Remove(callback);
                        if (faulted)
                            Debug.WriteLine("Sync callback unregistered due to fault: {0}", _channels.Count);
                        else
                            Debug.WriteLine("Sync callback unregistered due to close: {0}", _channels.Count);
                        if (syncTask.IsRunning)
                            syncTask.Cancel();
                    }
                }
            }
        }

        private static void OnChannelFaulted(object sender, EventArgs e)
        {
            try
            {
                UnregisterCallback(sender as ISyncServiceCallBack, true);
            }
            catch (Exception ex)
            {
                Debug.Fail(ex.Message, ex.StackTrace);
            }
        }

        private static void OnChannelClosed(object sender, EventArgs e)
        {
            try
            {
                UnregisterCallback(sender as ISyncServiceCallBack, false);
            }
            catch (Exception ex)
            {
                Debug.Fail(ex.Message, ex.StackTrace);
            }
        }

        // reset change log
        private static void ClearChangeLog(Guid id, long syncId)
        {
            var connectionString = SyncUtil.GetConnectionString(id, _syncProvider);
            using (var connection = new OracleConnection(connectionString))
            {
                connection.Open();
                connection.ActionName = "ClearChangeLog";
                try
                {
                    var transaction = connection.BeginTransaction();
                    try
                    {
                        using (var command = connection.CreateCommand())
                        {
                            command.CommandText = ClearLogCommandText;

                            using (var param = command.CreateParameter())
                            {
                                param.Direction = ParameterDirection.Input;
                                param.ParameterName = "change_id";
                                param.OracleDbType = OracleDbType.Int64;
                                param.Value = syncId;
                                command.Parameters.Add(param);

                                command.ExecuteNonQuery();
                            }

                            transaction.Commit();
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
                finally
                {
                    connection.ActionName = null;
                    connection.Close();
                }
            }
        }

        // get changed data from change log
        private static IEnumerable<ChangeLog> GetChangeLogs(Guid id, long syncId)
        {
            var changeLogs = new List<ChangeLog>();
            var connectionString = SyncUtil.GetConnectionString(id, _syncProvider);
            using (var connection = new OracleConnection(connectionString))
            {
                connection.Open();
                connection.ActionName = "GetChangeLogs";
                try
                {
                    if (syncId > 0L)
                    {
                        using (var command = connection.CreateCommand())
                        {
                            command.BindByName = true;
                            command.CommandType = CommandType.Text;
                            command.CommandText = ChangeLogCommandText;

                            using (var param = command.CreateParameter())
                            {
                                param.Direction = ParameterDirection.Input;
                                param.ParameterName = "change_id";
                                param.OracleDbType = OracleDbType.Int64;
                                param.Value = syncId;
                                command.Parameters.Add(param);

                                var lastChangeLog = ChangeLog.Empty;
                                using (var reader = command.ExecuteReader(CommandBehavior.SingleResult))
                                {
                                    try
                                    {
                                        reader.FetchSize = reader.RowSize * 10;
                                        while (reader.Read())
                                        {
                                            var changeLog = new ChangeLog(reader.GetInt64(0),
                                                //reader.GetString(1), reader.IsDBNull(2) ? null : reader.GetString(2), reader.GetString(3)[0]);
                                                reader.GetString(1), null, reader.GetString(2)[0]);

                                            if (changeLog.Equals(lastChangeLog))
                                                lastChangeLog.ToId = changeLog.FromId;
                                            else
                                            {
                                                if (!lastChangeLog.Equals(ChangeLog.Empty))
                                                    changeLogs.Add(lastChangeLog);
                                                lastChangeLog = changeLog;
                                            }
                                        }

                                        if (!lastChangeLog.Equals(ChangeLog.Empty))
                                            changeLogs.Add(lastChangeLog);
                                    }
                                    finally
                                    {
                                        reader.Close();
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        /*
                        connection.ActionName = "GetFullChangeLogs";
                        using (var command = connection.CreateCommand())
                        {
                            command.CommandType = CommandType.Text;
                            command.CommandText = ChangeFullLogCommandText;

                            using (var reader = command.ExecuteReader(CommandBehavior.SingleResult))
                            {
                                try
                                {
                                    while (reader.Read())
                                        changeLogs.Add(new ChangeLog(0L, reader.GetString(0), reader.IsDBNull(1) ? null : reader.GetString(1), 'M'));
                                }
                                finally
                                {
                                    reader.Close();
                                }
                            }
                        }
                        */
                        foreach (var syncTable in _syncProvider.GetTableNames())
                            changeLogs.Add(new ChangeLog(0L, syncTable, null, 'M'));
                    }
                }
                finally
                {
                    connection.ActionName = null;
                    connection.Close();
                }
            }

            return changeLogs;
        }

        // retrieve changed log
        private static OracleDataReader GetChangeData(Guid id, long syncFromId, long syncToId, char syncOperation, string syncQuery)
        {
            var connection = new OracleConnection(SyncUtil.GetConnectionString(id, _syncProvider));
            connection.Open();
            connection.ActionName = "GetChangeData";

            using (var command = connection.CreateCommand())
            {
                command.BindByName = true;
                command.CommandType = CommandType.Text;

                try
                {
                    if (syncFromId > 0L && syncFromId <= syncToId)
                    {
                        command.CommandText = string.Format(ChangedDataRowCommandText, syncQuery);

                        var param = command.CreateParameter();
                        param.Direction = ParameterDirection.Input;
                        param.ParameterName = "delete_change_operation";
                        param.OracleDbType = OracleDbType.Char;
                        param.Value = "D";
                        command.Parameters.Add(param);

                        param = command.CreateParameter();
                        param.Direction = ParameterDirection.Input;
                        param.ParameterName = "change_id_min";
                        param.OracleDbType = OracleDbType.Int64;
                        param.Value = syncFromId;
                        command.Parameters.Add(param);

                        param = command.CreateParameter();
                        param.Direction = ParameterDirection.Input;
                        param.ParameterName = "change_id_max";
                        param.OracleDbType = OracleDbType.Int64;
                        param.Value = syncToId;
                        command.Parameters.Add(param);
                    }
                    else
                    {
                        command.CommandText = string.Format(ChangedDataTableCommandText, syncQuery);

                        var param = command.CreateParameter();
                        param.Direction = ParameterDirection.Input;
                        param.ParameterName = "change_operation";
                        param.OracleDbType = OracleDbType.Char;
                        param.Value = syncOperation;
                        command.Parameters.Add(param);
                    }

                    try
                    {
                        var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                        //reader.FetchSize = (long)(Math.Truncate(MessageSize / reader.RowSize) * reader.RowSize);
                        reader.FetchSize = FetchRows * reader.RowSize;
                        return reader;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                finally
                {
                    foreach (OracleParameter parameter in command.Parameters)
                        parameter.Dispose();
                }
            }
        }

        // get current change id from sequence
        private static long GetChangeId(Guid id)
        {
            using (var connection = new OracleConnection(SyncUtil.GetConnectionString(id, _syncProvider)))
            {
                connection.Open();
                connection.ActionName = "GetChangeId";
                try
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "SELECT change_notification_id.NEXTVAL FROM DUAL";
                        return Convert.ToInt64(command.ExecuteScalar());
                    }
                }
                finally
                {
                    connection.ActionName = null;
                    connection.Close();
                }
            }
        }

        // release sync task on table change notification
        private static void OnChangeNotificaton(object source, OracleNotificationEventArgs arg)
        {
            var dependency = (OracleDependency)source;
            EventWaitHandle eventWait;
            if (EventWaitHandle.TryOpenExisting(string.Format("{0}:{1}", dependency.UserName, PosEventName), EventWaitHandleRights.Modify, out eventWait))
            {
                eventWait.Set();
                for (int index = 0; index < arg.ResourceNames.Length; index++)
                    Logger.TraceVerbose(0, "Change notification for {TableName}", arg.ResourceNames[index]);
            }
        }

        // register database change notification
        private static OracleDependency RegisterNotification(string connectionString)
        {
            using (var connection = new OracleConnection(connectionString))
            {
                connection.Open();
                connection.ActionName = "RegisterNotification";
                try
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "SELECT 1 FROM change_notification";
                        var dependency = new OracleDependency(command, false, 0, false);
                        dependency.QueryBasedNotification = false;
                        dependency.OnChange += OnChangeNotificaton;
                        command.ExecuteNonQuery();
                        return dependency;
                    }
                }
                finally
                {
                    connection.ActionName = null;
                    connection.Close();
                }
            }
        }

        // unregister database change notification
        private static void UnregisterNotification(string connectionString, OracleDependency dependency)
        {
            using (var connection = new OracleConnection(connectionString))
            {
                connection.Open();
                connection.ActionName = "UnregisterNotification";
                try
                {
                    dependency.RemoveRegistration(connection);
                }
                finally
                {
                    connection.ActionName = null;
                    connection.Close();
                }
            }
        }

        // update last upload time
        private static void UpdateUploadTime(Guid id)
        {
            var connectionString = SyncUtil.GetConnectionString(id, _syncProvider);
            using (var connection = new OracleConnection(connectionString))
            {
                connection.Open();
                connection.ActionName = "UpdateUploadTime";
                try
                {
                    var transaction = connection.BeginTransaction();
                    try
                    {
                        using (var command = connection.CreateCommand())
                        {
                            command.CommandText = "update tcfg_npos set last_upld = systimestamp where hote_pk = :hote_pk";

                            using (var param = command.CreateParameter())
                            {
                                param.Direction = ParameterDirection.Input;
                                param.ParameterName = "hote_pk";
                                param.OracleDbType = OracleDbType.Raw;
                                param.Value = id.ToByteArray();
                                command.Parameters.Add(param);

                                command.ExecuteNonQuery();
                            }

                            transaction.Commit();
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
                finally
                {
                    connection.ActionName = null;
                    connection.Close();
                }
            }
        }

        // update last download time
        private static void UpdateDownloadTime(Guid id)
        {
            var connectionString = SyncUtil.GetConnectionString(id, _syncProvider);
            using (var connection = new OracleConnection(connectionString))
            {
                connection.Open();
                connection.ActionName = "UpdateDownloadTime";
                try
                {
                    var transaction = connection.BeginTransaction();
                    try
                    {
                        using (var command = connection.CreateCommand())
                        {
                            command.CommandText = "update tcfg_npos set last_dwld = systimestamp where hote_pk = :hote_pk";

                            using (var param = command.CreateParameter())
                            {
                                param.Direction = ParameterDirection.Input;
                                param.ParameterName = "hote_pk";
                                param.OracleDbType = OracleDbType.Raw;
                                param.Value = id.ToByteArray();
                                command.Parameters.Add(param);

                                command.ExecuteNonQuery();
                            }

                            transaction.Commit();
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
                finally
                {
                    connection.ActionName = null;
                    connection.Close();
                }
            }
        }

        // update last ping time
        private static void UpdateStartInfo(Guid id)
        {
            var connectionString = SyncUtil.GetConnectionString(id, _syncProvider);
            using (var connection = new OracleConnection(connectionString))
            {
                connection.Open();
                connection.ActionName = "UpdateStartInfo";
                try
                {
                    var transaction = connection.BeginTransaction();
                    try
                    {
                        using (var command = connection.CreateCommand())
                        {
                            command.BindByName = true;
                            command.CommandType = CommandType.Text;
                            command.CommandText = "update tcfg_npos set last_ping = systimestamp where hote_pk = :hote_pk";

                            using (var param = command.CreateParameter())
                            {
                                param.Direction = ParameterDirection.Input;
                                param.ParameterName = "hote_pk";
                                param.OracleDbType = OracleDbType.Raw;
                                param.Value = id.ToByteArray();
                                command.Parameters.Add(param);

                                command.ExecuteNonQuery();
                            }
                        }

                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
                finally
                {
                    connection.ActionName = null;
                    connection.Close();
                }
            }
        }

        // main processing loop
        private static void DoServerSync(SyncTask sender, CancellationToken cancellationToken)
        {
            var syncTask = (ServerSyncTask<ISyncServiceCallBack>)sender;
            var connectionString = string.Empty;

            try
            {
                using (syncTask.Logger.TraceScope(syncTask.Id))
                {
                    syncTask.Logger.TraceInformation(0, "Sync {Schema} started", syncTask.ToString());

                    // get connection info based on id
                    connectionString = SyncUtil.GetConnectionString(syncTask.Id, _syncProvider);

                    #if dcn
                    lock (_notifications)
                    {
                        OracleDependency dependency;
                        if (!_notifications.TryGetValue(syncTask.Id, out dependency))
                        {
                            try
                            {
                                // register change notification
                                dependency = RegisterNotification(connectionString);
                                _notifications.Add(syncTask.Id, dependency);
                                syncTask.Logger.TraceVerbose(0, "Notification registered for {Schema}", syncTask.ToString());
                            }
                            catch (Exception ex)
                            {
                                // if error log warning
                                syncTask.Logger.TraceError(0, "Notification registration error for {Schema}: {Exception}",
                                    syncTask.ToString(), ex.Message);
                            }
                        }
                    }
                    #endif
                }

                // do main loop while not canceled
                while (!cancellationToken.IsCancellationRequested)
                {
                    syncTask.Logger.TraceInformation("Sync {Schema} is running ...", syncTask.ToString());
                    using (syncTask.Logger.TraceScope(syncTask.Id))
                    {
                        // read change log
                        using (var enumerator = GetChangeLogs(syncTask.Id, syncTask.SyncCounter).GetEnumerator())
                        {
                            var currSyncId = 0L;

                            // if full sync requested, retrive current sync counter from database
                            if (syncTask.SyncCounter == 0L)
                            {
                                currSyncId = GetChangeId(syncTask.Id);
                                syncTask.Logger.TraceInformation(0, "Sync initiated on {Schema}", syncTask.ToString());
                            }

                            var isError = false;
                            if (enumerator.MoveNext())
                            {
                                // do while not canceled
                                while (!cancellationToken.IsCancellationRequested)
                                {
                                    var changeLog = enumerator.Current;
                                    var localSyncName = changeLog.LocalName.ToUpperInvariant();
                                    var remoteSyncName = localSyncName;
                                    var syncOperation = changeLog.Operation;
                                    var syncFromId = changeLog.FromId;
                                    var syncToId = changeLog.ToId;
                                    var syncQuery = string.Format(SyncQueryTemplate, localSyncName.ToLowerInvariant());

                                    var isTimeout = false;
                                    if (!string.IsNullOrEmpty(syncQuery))
                                    {
                                        // read change data to send in chunks
                                        syncTask.Logger.TraceInformation("Sync changes from {Schema}.{TableName}", syncTask.ToString(), localSyncName);
                                        using (var reader = GetChangeData(syncTask.Id, syncFromId, syncToId, syncOperation, syncQuery))
                                        {
                                            try
                                            {
                                                // if has something to send
                                                if (reader.HasRows)
                                                {
                                                    // if full sync requested, send start table sync message
                                                    if (syncTask.SyncCounter == 0L)
                                                    {
                                                        syncTask.CallbackChannel.OnSyncChanges(SyncUtil.GetStartSyncTableMessage(remoteSyncName));

                                                        // wait for response
                                                        if (!syncTask.Wait(HubEventName, _syncResponseDelay))
                                                        {
                                                            isTimeout = true;
                                                            break;
                                                        }

                                                        if (syncTask.SyncCounter < 0L)
                                                        {
                                                            isError = true;
                                                            break;
                                                        }
                                                    }

                                                    syncTask.Logger.TraceVerbose(0, "Sync reading changes on {Schema}.{TableName} ...", syncTask.ToString(), localSyncName);

                                                    // if not reach the end
                                                    while (!reader.IsClosed)
                                                    {
                                                        // if canceled exit
                                                        if (cancellationToken.IsCancellationRequested)
                                                            break;

                                                        // build change message                                                            
                                                        var message = SyncUtil.GetSyncMessage(reader, null, localSyncName, remoteSyncName);

                                                        MessageBuffer messageBuffer = null;
                                                        if (syncTask.Logger.Switch.ShouldTrace(TraceEventType.Verbose))
                                                        {
                                                            // buffered copy to trace
                                                            messageBuffer = message.CreateBufferedCopy(int.MaxValue);
                                                            syncTask.Logger.TraceData(0, "Sync message:", messageBuffer.CreateMessage());
                                                        }
                                                        else
                                                            syncTask.Logger.TraceInformation(0, "Sending changes from {Schema}.{TableName}", syncTask.ToString(), localSyncName);

                                                        // send message
                                                        syncTask.CallbackChannel.OnSyncChanges(messageBuffer != null ? messageBuffer.CreateMessage() : message);

                                                        // if canceled exit
                                                        if (cancellationToken.IsCancellationRequested)
                                                            break;

                                                        // wait for response
                                                        if (!syncTask.Wait(HubEventName, _syncResponseDelay))
                                                        {
                                                            isTimeout = true;
                                                            break;
                                                        }

                                                        if (syncTask.SyncCounter < 0L)
                                                        {
                                                            isError = true;
                                                            break;
                                                        }

                                                        // if canceled exit
                                                        if (cancellationToken.IsCancellationRequested)
                                                            break;
                                                    }

                                                    if (syncTask.SyncCounter < 0L)
                                                        break;

                                                    UpdateUploadTime(syncTask.Id);

                                                    // if not cancelled and all data sended
                                                    if (!cancellationToken.IsCancellationRequested && reader.IsClosed)
                                                        syncTask.Logger.TraceVerbose(0, "Changes from {Schema}.{TableName} sended", syncTask.ToString(), localSyncName);

                                                    // if full sync requested, send end table sync message
                                                    if (syncTask.SyncCounter == 0L)
                                                    {
                                                        syncTask.CallbackChannel.OnSyncChanges(SyncUtil.GetEndSyncTableMessage(remoteSyncName));

                                                        // wait for response
                                                        if (!syncTask.Wait(HubEventName, _syncResponseDelay))
                                                        {
                                                            isTimeout = true;
                                                            break;
                                                        }

                                                        if (syncTask.SyncCounter < 0L)
                                                        {
                                                            isError = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            finally
                                            {
                                                if (!reader.IsClosed)
                                                    reader.Close();
                                            }
                                        }
                                    }

                                    // if response error, log trace and out
                                    if (isError)
                                    {
                                        syncTask.Logger.TraceError(0, "Response error for {Schema}.{TableName}", syncTask.ToString(), localSyncName);
                                        break;
                                    }

                                    // if response timeout, log trace and out
                                    if (isTimeout)
                                    {
                                        syncTask.Logger.TraceError(0, "Response timeout for {Schema}.{TableName}", syncTask.ToString(), localSyncName);
                                        break;
                                    }

                                    // if at the end go out
                                    if (!enumerator.MoveNext())
                                        break;
                                }

                                // out of the loop
                                var syncCounter = syncTask.SyncCounter;

                                // full finished
                                if (syncCounter <= 0L)
                                {
                                    // send end of sync
                                    var message = SyncUtil.GetSyncMessage(currSyncId);
                                    syncTask.CallbackChannel.OnSyncChanges(message);
                                    syncTask.Logger.TraceVerbose(0, "Sync mark {Counter} sended for {Schema}", currSyncId, syncTask.ToString());

                                    // full sync aborted
                                    if (syncCounter < 0L)
                                        syncTask.Logger.TraceInformation(0, "Sync aborted on {Schema}", syncTask.ToString());
                                    else
                                    {
                                        syncTask.Logger.TraceInformation(0, "Sync succeeded on {Schema}", syncTask.ToString());

                                        // clear change log
                                        ClearChangeLog(syncTask.Id, currSyncId);
                                        syncTask.Logger.TraceVerbose(3, "Sync log cleaned on {Schema}", syncTask.ToString());
                                    }

                                    // wait for response
                                    syncTask.Wait(HubEventName, _syncResponseDelay);
                                }
                            }
                        }
                    }

                    // if not canceled then wait for next database notification
                    if (!cancellationToken.IsCancellationRequested)
                    {
                        syncTask.Logger.TraceInformation("Sync waiting changes on {Schema}", syncTask.ToString());
                        syncTask.Wait(PosEventName);
                    }
                };
            }
            // canceled exception, re-throws exception
            catch (OperationCanceledException)
            {
                throw;
            }
            // disposed error, go out of loop to invalidate channel
            catch (ObjectDisposedException ex)
            {
                syncTask.Logger.TraceError(0, "Disposed error: {@Exception}", new { Message = ex.Message });
                throw;
            }
            // timeout error, go out of loop to invalidate channel
            catch (TimeoutException ex)
            {
                syncTask.Logger.TraceError(0, "Timeout error: {@Exception}", new { Message = ex.Message });
                throw;
            }
            // communication error, go out of loop to invalidate channel
            catch (CommunicationException ex)
            {
                syncTask.Logger.TraceError(0, "Communication error: {@Exception}", new { Message = ex.Message });
                throw;
            }
            // if a database exception, log trace error and database connection info
            catch (OracleException ex)
            {
                syncTask.Logger.TraceError(0, "Database error: {@Exception}", new { Code = ex.ErrorCode, Message = ex.Message, Connection = connectionString });
                throw;
            }
            // other exception, just log trace error
            catch (Exception ex)
            {
                syncTask.Logger.TraceError(0, "Error: {@Exception}", new { Message = ex.Message, Stack = ex.StackTrace });
                throw;
            }
            finally
            {
                using (syncTask.Logger.TraceScope(syncTask.Id))
                {
                    syncTask.Logger.TraceInformation(0, "Sync {Schema} terminated", syncTask.ToString());

                    #if dcn
                    lock (_notifications)
                    {
                        OracleDependency dependency;
                        if (_notifications.TryGetValue(syncTask.Id, out dependency))
                        {
                            try
                            {
                                // unregister change notification
                                UnregisterNotification(connectionString, dependency);
                                _notifications.Remove(syncTask.Id);
                                syncTask.Logger.TraceVerbose(0, "Notification unregistered for {Schema}", syncTask.ToString());
                            }
                            catch (Exception ex)
                            {
                                // if error log warning
                                syncTask.Logger.TraceError(0, "Notification unregistration error for {Schema}: {Exception}",
                                    syncTask.ToString(), ex.Message);
                            }
                        }
                    }
                    #endif
                }
            }
        }

        // running if any task is running
        public bool IsRunning
        {
            get
            {
                lock (_channels)
                {
                    return _channels.Values.Any(x => x.IsRunning);
                }
            }
        }

        // start sync loop
        public void StartSync(Guid id, long syncCounter)
        {
            using (Logger.TraceScope(id))
            {
                try
                {
                    ServerSyncTask<ISyncServiceCallBack> syncTask;

                    string connectionName = null;
                    lock (_channels)
                    {
                        var callback = OperationContext.Current.GetCallbackChannel<ISyncServiceCallBack>();
                        if (!_channels.TryGetValue(callback, out syncTask))
                        {
                            var connectionString = SyncUtil.GetConnectionString(id, _syncProvider);

                            var schemaName = connectionString.Substring(8, connectionString.IndexOf(';') - 8);

                            syncTask = new ServerSyncTask<ISyncServiceCallBack>(id, schemaName, connectionName, _syncDelay, callback);

                            OperationContext.Current.Channel.Faulted += OnChannelFaulted;
                            OperationContext.Current.Channel.Closed += OnChannelClosed;

                            RegisterCallback(syncTask);
                        }
                    }

                    var oldSyncCounter = syncTask.SyncCounter;
                    syncTask.SyncCounter = syncCounter;
                    syncTask.Logger.TraceVerbose(0, "Sync counter {Counter} received", syncCounter);

                    // update info
                    UpdateStartInfo(id);

                    if (oldSyncCounter != syncCounter)
                        syncTask.Set(PosEventName);

                    // try to run if not already running
                    syncTask.Run(DoServerSync);
                }
                catch (Exception ex)
                {
                    Logger.TraceError(0, "Error: {@Exception}", new { Message = ex.Message, Stack = ex.StackTrace });
                }
            }
        }

        // stop sync loop
        public void StopSync(Guid id)
        {
            using (Logger.TraceScope(id))
            {
                try
                {
                    lock (_channels)
                    {
                        var callback = OperationContext.Current.GetCallbackChannel<ISyncServiceCallBack>();

                        ServerSyncTask<ISyncServiceCallBack> syncTask;
                        if (_channels.TryGetValue(callback, out syncTask) && syncTask.IsRunning)
                        {
                            _channels.Remove(callback);
                            if (syncTask.IsRunning)
                                syncTask.Cancel();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.TraceError(0, "Error: {@Exception}", new { Message = ex.Message, Stack = ex.StackTrace });
                }
            }
        }

        // update current change id from remote and release sync loop
        public void UpdateSync(Guid id, long syncCounter)
        {
            using (Logger.TraceScope(id))
            {
                try
                {
                    lock (_channels)
                    {
                        var callback = OperationContext.Current.GetCallbackChannel<ISyncServiceCallBack>();

                        ServerSyncTask<ISyncServiceCallBack> syncTask;
                        if (_channels.TryGetValue(callback, out syncTask))
                        {
                            if (syncCounter <= 0L || syncCounter > syncTask.SyncCounter)
                            {
                                syncTask.SyncCounter = syncCounter;
                                syncTask.Logger.TraceVerbose(0, "Sync counter {Counter} received", syncCounter);
                            }

                            syncTask.Set(HubEventName);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.TraceError(0, "Error: {@Exception}", new { Message = ex.Message, Stack = ex.StackTrace });
                }
            }
        }

        // apply remote changes
        public Message SyncChanges(Message message)
        {
            try
            {
                if (!message.IsEmpty)
                {
                    var id = SyncUtil.ApplySyncCommands(_syncProvider, message, Logger.Switch.ShouldTrace(TraceEventType.Verbose));
                    if (id != Guid.Empty)
                        UpdateUploadTime(id);
                }

                return SyncUtil.GetEmptyMessage("SyncChanges");
            }
            catch (OracleException ex)
            {
                if (ex.Number >= 20000 && ex.Number <= 20999)
                    Logger.TraceError(0, "Error: {@Exception}", new { Message = ex.Message });
                else
                    Logger.TraceError(0, "Error: {@Exception}", new { Message = ex.Message, Stack = ex.StackTrace });

                return SyncUtil.GetFaultMessage(ex, "SyncChanges");
            }
            catch (Exception ex)
            {
                Logger.TraceError(0, "Error: {@Exception}", new { Message = ex.Message, Stack = ex.StackTrace });

                return SyncUtil.GetFaultMessage(ex, "SyncChanges");
            }
        }

        // get table changes
        public Message GetSyncChanges(Message message)
        {
            try
            {
                var request = SyncRequest.GetBody(message);

                if (request != null)
                {
                    using (Logger.TraceScope(request.Id))
                    {
                        using (var connection = new OracleConnection(SyncUtil.GetConnectionString(request.Id, _syncProvider)))
                        {
                            connection.Open();
                            connection.ActionName = "GetSyncChanges: " + request.Name;
                            try
                            {
                                using (var command = connection.CreateCommand())
                                {
                                    command.BindByName = true;
                                    command.CommandType = CommandType.StoredProcedure;
                                    command.CommandText = "pnht_get_changes";

                                    try
                                    {
                                        var param = command.CreateParameter();
                                        param.ParameterName = "p_name";
                                        param.OracleDbType = OracleDbType.Varchar2;
                                        param.Direction = ParameterDirection.Input;
                                        param.Value = request.Name.ToUpperInvariant();
                                        command.Parameters.Add(param);

                                        param = command.CreateParameter();
                                        param.ParameterName = "p_lmod";
                                        param.OracleDbType = OracleDbType.TimeStamp;
                                        param.Direction = ParameterDirection.Input;
                                        param.Value = new DateTime(request.LastModified, DateTimeKind.Utc);
                                        command.Parameters.Add(param);

                                        param = command.CreateParameter();
                                        param.ParameterName = "p_count";
                                        param.OracleDbType = OracleDbType.Int64;
                                        param.Direction = ParameterDirection.Input;
                                        param.Value = request.Count > 0 ? request.Count : long.MaxValue;
                                        command.Parameters.Add(param);

                                        SyncUtil.CreateCommandCursors(command, 1);

                                        using (var reader = command.ExecuteReader(CommandBehavior.SingleResult))
                                        {
                                            if (reader.HasRows)
                                            {
                                                UpdateDownloadTime(request.Id);

                                                using (var syncMessage = SyncUtil.GetSyncMessage(reader, request.Count, null, null, request.Name))
                                                {
                                                    using (var messageBuffer = syncMessage.CreateBufferedCopy(int.MaxValue))
                                                    {
                                                        return messageBuffer.CreateMessage();
                                                    }
                                                }
                                            }
                                            else
                                                reader.Close();
                                        }
                                    }
                                    finally
                                    {
                                        foreach (OracleParameter parameter in command.Parameters)
                                            parameter.Dispose();
                                    }
                                }
                            }
                            finally
                            {
                                connection.ActionName = null;
                                connection.Close();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.TraceError(0, "Error: {@Exception}", new { Message = ex.Message, Stack = ex.StackTrace });

                return SyncUtil.GetFaultMessage(ex, "SyncChangesAction");
            }

            return SyncUtil.GetEmptyMessage();
        }

        // get image from disk or an empty stream upon error
        public Stream GetImageFile(Guid hotelId, string path)
        {
            using (Logger.TraceScope(hotelId))
            {
                try
                {
                    var stream = _syncProvider.GetImageStream(hotelId, path);
                    Logger.TraceInformation(0, "Image file {ImageFilePath} retrived: {ImageFileSize} bytes", path, stream.Length);
                    return stream;
                }
                catch (ApplicationException ex)
                {
                    Logger.TraceError(0, ex.Message);
                }
                catch (Exception ex)
                {
                    Logger.TraceError(0, "Error retriving {ImageFilePath}: {@Exception}", path, new { Message = ex.Message, Stack = ex.StackTrace });
                }
            }

            return new MemoryStream();
        }

        #region Versioning Methods

        // get sync assembly version
        public int GetVersion()
        {
            var asmVersion = 0;

            try
            {
                var assembly = Assembly.GetExecutingAssembly();
                var version = assembly.GetName().Version;
                int.TryParse(version.Major.ToString("0000") + version.Minor.ToString("00") + version.Build.ToString("00"), out asmVersion);
            }
            catch (Exception ex)
            {
                Logger.TraceError(0, "Error: {@Exception}", new { Message = ex.Message, Stack = ex.StackTrace });
            }

            return asmVersion;
        }

        #endregion
    }
}