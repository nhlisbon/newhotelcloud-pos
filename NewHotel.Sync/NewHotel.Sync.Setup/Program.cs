﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;

namespace NewHotel.Sync.Setup
{
    class Program
    {
        private const string ConnectionStringFormat = "User Id={0};Password={1};Data Source={2};Pooling=false;Connection Lifetime=180";

        private static string GetConnectionString(string user, string password, string source, bool asSysDba)
        {
            var connectionString = string.Format(ConnectionStringFormat, user, password, source);
            if (asSysDba)
                connectionString += ";DBA Privilege=SYSDBA";
            return connectionString;
        }

        private static IEnumerable<string> GetTables()
        {
            var tables = new List<string>();

            tables.Add("TCFG_NPOS");
            tables.Add("TNHT_LITE,TNHT_MULT");
            tables.Add("TNHT_ENUM");
            tables.Add("TNHT_APPL");
            tables.Add("TNHT_HOTE,TCFG_HOTE");
            tables.Add("TNHT_BSERV,TNHT_BARTG,TNHT_ARTG,TNHT_ARIM");
            tables.Add("TNHT_SEPA");
            tables.Add("TNHT_BCAT");
            tables.Add("TNHT_BOTV");
            tables.Add("TNHT_PREP");
            tables.Add("TNHT_GRPP");
            tables.Add("TNHT_FORE,TNHT_FORI");
            tables.Add("TNHT_CACR");
            tables.Add("TNHT_IPOS,TNHT_SECC");
            tables.Add("TNHT_CAJA");
            tables.Add("TNHT_CAPV");
            tables.Add("TNHT_SALO");
            tables.Add("TNHT_IPSL");
            tables.Add("TNHT_COIN");
            tables.Add("TNHT_CLAS");
            tables.Add("TNHT_DUTY");
            tables.Add("TNHT_ESIM");
            tables.Add("TNHT_IMSE");
            tables.Add("TNHT_SEIM");
            tables.Add("TNHT_TIVA");
            tables.Add("TNHT_HOUR");
            tables.Add("TNHT_HAIP");
            tables.Add("TNHT_GRUP");
            tables.Add("TNHT_FAMI");
            tables.Add("TNHT_SFAM");
            tables.Add("TNHT_FAPP");
            tables.Add("TNHT_MEDE");
            tables.Add("TNHT_MENU");
            tables.Add("TNHT_MESA");
            tables.Add("TNHT_MTCO");
            tables.Add("TNHT_PVAR");
            tables.Add("TNHT_PVCI");
            tables.Add("TNHT_REGI");
            tables.Add("TNHT_REMU");
            tables.Add("TNHT_SEDO,TNHT_DOHO");
            tables.Add("TNHT_TICK");
            tables.Add("TNHT_TIDE");
            tables.Add("TNHT_TPPE");
            tables.Add("TNHT_TPRB");
            tables.Add("TNHT_TPRG");
            tables.Add("TNHT_TPRL");
            tables.Add("TNHT_UNMO");
            tables.Add("TNHT_UTIL");
            tables.Add("TNHT_GRPR");
            tables.Add("TNHT_PERM");
            tables.Add("TNHT_PEAP");
            tables.Add("TNHT_PEUT");
            tables.Add("TNHT_IPUT");

            return tables;
        }

        private static string InternalMetadata(object[] metadata)
        {
            // ColumnName
            var name = (string)metadata[0];
            // ColumnType
            var type = (int)metadata[12];
            // ColumnSize
            var size = (int)metadata[2];
            // NumericPrecision
            object value;
            value = metadata[3];
            short precision = 0;
            if (value != DBNull.Value)
                precision = (short)value;
            // NumericScale
            value = metadata[4];
            short scale = 0;
            if (value != DBNull.Value)
                scale = (short)value;
            // AllowDBNull
            value = metadata[13];

            var isNullable = true;
            if (value != DBNull.Value)
                isNullable = (bool)value;

            return name + type.ToString() + size.ToString() + precision.ToString() + scale.ToString() + (isNullable ? "1" : "0");
        }

        private static void CompareSchemas()
        {
            try
            {
                using (var connection = new OracleConnection(GetConnectionString("POSCLOUD", "POS", "LOCAL", false)))
                {
                    connection.Open();
                    try
                    {
                        foreach (var tables in GetTables())
                        {
                            var tableNames = tables.Split(',');
                            foreach (var tableName in tableNames)
                            {
                                DataTable dt1 = null;
                                using (var cmd = connection.CreateCommand())
                                {
                                    cmd.CommandText = string.Format("SELECT * FROM {0}", tableName);
                                    using (var rdr = cmd.ExecuteReader(CommandBehavior.SchemaOnly))
                                    {
                                        dt1 = rdr.GetSchemaTable();
                                    }
                                }

                                DataTable dt2 = null;
                                try
                                {
                                    using (var cn2 = new OracleConnection(GetConnectionString("POSCLOUD", "POS", "REMOTE", false)))
                                    {
                                        cn2.Open();
                                        try
                                        {
                                            using (var cmd = cn2.CreateCommand())
                                            {
                                                cmd.CommandText = string.Format("SELECT * FROM {0}", tableName);
                                                using (var rdr = cmd.ExecuteReader(CommandBehavior.SchemaOnly))
                                                {
                                                    dt2 = rdr.GetSchemaTable();
                                                }
                                            }
                                        }
                                        finally
                                        {
                                            cn2.Close();
                                        }
                                    }
                                }
                                catch
                                {

                                }

                                if (dt1 != null && dt2 != null)
                                {
                                    var metadata2 = new Dictionary<string, string>();
                                    for (int index = 0; index < dt2.Rows.Count; index++)
                                    {
                                        var items = dt2.Rows[index].ItemArray;
                                        var fieldName = (string)items[0];
                                        if (!fieldName.EndsWith("LMOD"))
                                            metadata2.Add(fieldName, InternalMetadata(items));
                                    }

                                    var metadata1 = new Dictionary<string, string>();
                                    for (int index = 0; index < dt1.Rows.Count; index++)
                                    {
                                        var items = dt1.Rows[index].ItemArray;
                                        var fieldName = (string)items[0];
                                        if (metadata2.ContainsKey(fieldName))
                                            metadata1.Add(fieldName, InternalMetadata(items));
                                    }

                                    if (!metadata1.OrderBy(x => x.Key).Select(x => x.Value)
                                        .SequenceEqual(metadata2.OrderBy(x => x.Key).Select(x => x.Value)))
                                    {
                                        //Console.WriteLine("--------------------------------");
                                        Console.WriteLine("{0} not equal", tableName);
                                        //Console.WriteLine("--------------------------------");
                                        //foreach (var md in metadata1.OrderBy(x => x.Key))
                                        //    Console.WriteLine(md);
                                        //Console.WriteLine("--------------------------------");
                                        //foreach (var md in metadata2.OrderBy(x => x.Key))
                                        //    Console.WriteLine(md);
                                        //Console.WriteLine("--------------------------------");
                                        //Console.ReadLine();
                                    }
                                }
                                else
                                    Console.WriteLine("{0} not found", tableName);
                            }
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            catch (OracleException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void Main(string[] args)
        {
            CompareSchemas();
            Console.WriteLine();
            Console.WriteLine("Any key to exit ...");
            Console.ReadKey(false);
        }
    }
}
