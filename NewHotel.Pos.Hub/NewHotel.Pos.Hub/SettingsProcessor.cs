﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using NewHotel.Pos.IoC;
using System;
using System.IO;
using System.Reflection;

namespace NewHotel.Pos.Hub
{
	public class LocalOptions<T> : IOptions<T> where T: class
	{
		private readonly T _value;

		public LocalOptions(T value)
		{
			_value = value;
		}

		public T Value => _value;
	}

	public static class SettingsProcessor
    {
		const string ConfigFileName = "poshub.settings.json";
		public static IConfiguration Configuration { get; private set; }

		public static IServiceConfigurator BuildConfiguration(this IServiceConfigurator configurator, Action<IServiceConfigurator,IConfiguration> action)
		{
			var path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
			path = Path.Combine(path, ConfigFileName);
			if (!File.Exists(path))
			{
				File.WriteAllText(path,
                @"{
					""PaySystemConfig"": {
						""Url"": ""http://localhost:7073""
					},
					// TODO: Remove this section when the real configuration is available in the backoffice
					""MxmTaskConfig"": {
						""MxmTaskLogsPath"": ""C:\\*\\MxmTask.txt"",
						""MaxRetries"": 1,
						""MxmTaskIntervalInSeconds"": 10,
						""MxmUrl"": ""https://sescmshom.mxmwebmanager.com.br/webmanager/"",
						""EnableMxmBackgroundTask"": ""true"",
					}
				}");
			}

			IConfiguration cfg = new ConfigurationBuilder()
				.AddJsonFile(path)
				//.AddJsonFile("poshub.settings.json")
				.Build()
				;
			configurator.RegisterSingletonUsingFactory<IConfiguration>(() => cfg);
			Configuration = cfg;
			action(configurator, cfg);

			return configurator;
		}

		public static IServiceConfigurator Configure<T>(this IServiceConfigurator configurator, IConfiguration configuration) where T: class
		{
			return configurator.AddTransient<IOptions<T>, IConfiguration>(cfg => new LocalOptions<T>(cfg.GetSection(typeof(T).Name).Get<T>()));
		}

		/*
		public static void Load()
        {
			var path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
			path = Path.Combine(path, "poshub.settings.json");
			if (File.Exists(path))
			{
				IConfiguration cfg = new ConfigurationBuilder()
					.AddJsonFile(path)
					//.AddJsonFile("poshub.settings.json")
					.Build()
					;

				//cfg.GetSection("PayConfig").Bind(new PayConfig());
				var ss = cfg.GetSection("PayConfig");
				var kk = cfg.GetSection(nameof(PayConfig)).Get<PayConfig>();
				var pp = cfg.Get<PayConfig>();
				var pp1 = cfg.GetValue<PayConfig>(nameof(PayConfig));

				var nnoo = new ConfigureNamedOptions<PayConfig>(nameof(PayConfig), null);
				OptionsFactory<PayConfig> factory = new OptionsFactory<PayConfig>(null, null);
				Options.Create(cfg.GetSection(nameof(PayConfig)).Get<PayConfig>());

				OptionsBuilder<PayConfig> builder = new OptionsBuilder<PayConfig>(null, "");
				// Services.AddSingleton((IConfigureOptions<TOptions>)new ConfigureNamedOptions<TOptions>(Name, configureOptions));
			}
			else
			{
			}
		}
		*/
	}
}
