﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("NewHotel POS Hub")]
[assembly: AssemblyDescription("Hub Server")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("NewHotel Software S.A.")]
[assembly: AssemblyProduct("NewHotel POS Cloud")]
[assembly: AssemblyCopyright("Copyright © NewHotel Software S.A. 2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("12fb460a-0fb5-41b0-a1c7-d5562a5f6ce3")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2024.10.14.1518")]
[assembly: AssemblyFileVersion("2024.10.14.1518")]

