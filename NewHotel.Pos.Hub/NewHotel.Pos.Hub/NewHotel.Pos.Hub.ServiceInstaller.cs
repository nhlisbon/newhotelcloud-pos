﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace NewHotel.Pos.Hub
{
    [RunInstaller(true)]
    public partial class NewHotelPosHubServiceInstaller : Installer
    {
        private ServiceProcessInstaller process;
        private ServiceInstaller service;

        public NewHotelPosHubServiceInstaller()
        {
            process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.LocalSystem;
            service = new ServiceInstaller();
            service.ServiceName = NewHotelPosHubService.Name;
            service.DisplayName = NewHotelPosHubService.DisplayName;
            Installers.Add(process);
            Installers.Add(service);
        }
    }
}
