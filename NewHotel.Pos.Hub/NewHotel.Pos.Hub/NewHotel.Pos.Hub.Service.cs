﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Configuration;
using System.Configuration.Install;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Globalization;
using Microsoft.Win32;
using NewHotel.Pos.Hub.Core;
using NewHotel.Pos.Hub.Core.Logs;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Hub.Common;
using NewHotel.Pos.Hub.Business;
using NewHotel.Pos.PrinterDocument;
using System.IO;
using NewHotel.Pos.Hub.Business.Business;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Scheduler;
using NewHotel.Pos.Hub.Business.BackgroundTasks;
using NewHotel.Pos.Hub.Business.IntegrationConfigs;
using Microsoft.Extensions.Options;

namespace NewHotel.Pos.Hub
{
	public class NewHotelPosHubService : ServiceBase
	{
		#region Variable + Constructor

		private const string LogName = "Application";
		public const string DisplayName = "NewHotel Pos Hub";
		public const string Name = "NewHotel.Pos.Hub";

		private static bool _debugMode = false;

		private Task[] _tasks;
		private CancellationTokenSource _cts;
		private EventWaitHandle _waitHandle;
		private readonly TimeSpan _waitTime = TimeSpan.FromSeconds(30);

		[ImportMany(typeof(IHubHost))]
		private readonly List<IHubHost> _hosts = new List<IHubHost>();

		IHubLogs log;
		IHubLogs Log => log ?? (log = HubLogsFactory.DefaultFactory.CreateHubLogs(LogName));

		void ClearLog()
		{
			log?.Dispose();
			log = null;
		}

		public NewHotelPosHubService()
		{
			AutoLog = true;
			ServiceName = Name;
			Compose();
		}

		#endregion

		#region Self Host Api
		Hosting.WebSelfHost _webSelfHost;
		IDisposable _webApiInstance;

		void BuildWebSelfHost()
		{
			var ss = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			var sg = ss.GetSectionGroup("system.serviceModel");//System.ServiceModel.Configuration.ServiceModelSectionGroup
			var services = sg.Sections["services"] as System.ServiceModel.Configuration.ServicesSection;
			var service = services.Services.Cast<System.ServiceModel.Configuration.ServiceElement>().FirstOrDefault(x => x.Name == "NewHotel.Pos.Hub.Cashier.Service");
			var address = service.Host.BaseAddresses.Cast<System.ServiceModel.Configuration.BaseAddressElement>().Select(x => new Uri(x.BaseAddress)).FirstOrDefault(x => x.Scheme == "net.tcp");


			string url = $"http://*:{address.Port + 1}";//ConfigurationManager.AppSettings.Get("WebSelfHostUrl");
			_webSelfHost = new Hosting.WebSelfHost(url);
			var resolver = CwFactory.Instance.GetDependencyResolver();
			_webApiInstance = _webSelfHost.Start(resolver);
			Log.Information($"REST Api Server running at {url}");
		}

		void StopWebSelfHost()
		{
			_webApiInstance?.Dispose();
			_webApiInstance = null;
			_webSelfHost = null;
		}
		#endregion

		#region Properties

		public static string CloudLocation { get; set; }

		#endregion

		#region Methods

		#region Public

		public static void Main(string[] args)
		{
			if (!Environment.UserInteractive)
			{
				Console.SetOut(TextWriter.Null);
				Console.SetError(TextWriter.Null);
				Console.SetIn(TextReader.Null);
			}
			_debugMode = false;
			if (args.Length > 0)
			{
				for (int i = 0; i < args.Length; i++)
				{
					switch (args[i].ToUpperInvariant())
					{
						case "/I":
							InstallService();
							return;
						case "/U":
							UninstallService();
							return;
						case "/D":
							_debugMode = true;
							break;
						default:
							break;
					}
				}
			}

			try
			{
				CwFactory.Instance.BuildConfiguration((services, config) =>
				{
					services
					.Configure<PaySystemConfig>(config)
					.Configure<MxmTaskConfig>(config)
					;
				});

				CommonConfiguration.Configure(CwFactory.Instance);
				BusinessConfiguration.Configure(CwFactory.Instance);
				PrinterDocumentConfiguration.Configure(CwFactory.Instance);
				ServicesConfiguration.Configure(CwFactory.Instance);

				CwFactory.Instance.RegisterSingletonUsingFactory(() =>
				{
					IActionScheduler scheduler = new ActionScheduler(async actionScoped =>
					{
						using (CwFactory.Instance.RequireScope())
							await actionScoped(CwFactory.Instance.Resolve);
					});

					return scheduler;
				});

				CwFactory.Instance.InitializeForWcfServices();
				CwFactory.Instance.RegisterWcfService<Cashier.IService, Cashier.Service>();
				CwFactory.Instance.RegisterWcfService<Handheld.IServiceHandheld, Handheld.ServiceHandheld>();
				CwFactory.Instance.RegisterWcfService<Kitchen.IService, Kitchen.Service>();

                SharedServices.RegisterCommonDependencies();

                // TODO: Remove this var and if from here and put it in the correct place, probably in wherever the next file this goes to or maybe a new file that handles all the tasks that need to be launched, like a task manager 
                var mxmTaskConfig = CwFactory.Resolve<IOptions<MxmTaskConfig>>();
				if (mxmTaskConfig?.Value?.EnableMxmBackgroundTask ?? false)
				{
					var actionScheduler = CwFactory.Resolve<IActionScheduler>();
					actionScheduler.RepeatUntil<MxmTask>(new MxmState { Counter = 0, MaxRetries = mxmTaskConfig.Value.MaxRetries }, TimeSpan.FromSeconds(mxmTaskConfig.Value.MxmTaskIntervalInSeconds), true);
				}

                SystemEvents.TimeChanged += (s, a) =>
				{
					CultureInfo.CurrentCulture.ClearCachedData();
					Console.WriteLine("Cleared culture cached data");
				};

				if (_debugMode)
				{
					var listener = new ConsoleTraceListener();
					Trace.Listeners.Add(listener);

					using (var host = new NewHotelPosHubService())
					{
						host.ConsoleRun(args);
					}
				}
				else
					ServiceBase.Run(new NewHotelPosHubService());
			}
			catch (Exception ex)
			{
				if (ex.InnerException != null)
					Console.WriteLine(ex.InnerException.Message + Environment.NewLine + ex.InnerException.StackTrace);
				else
					Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
			}
		}

		#endregion

		#region Aux

		protected override void OnStart(string[] args)
		{
			Start(args);
		}

		private void Compose()
		{
			_hosts.Add(new Cashier.HubCashierServiceHost());
			_hosts.Add(new Handheld.HubHandheldServiceHost());
			_hosts.Add(new Kitchen.HubKitchenServiceHost());


			/*
            try
            {
                var container = new CompositionContainer(new DirectoryCatalog(".\\", "*.dll"));
                container.SatisfyImportsOnce(this);
            }
            catch (ReflectionTypeLoadException ex)
            {
                var exceptions = new StringBuilder();
                foreach (var loaderEx in ex.LoaderExceptions)
                    exceptions.AppendLine(loaderEx.Message);
                WriteException(new Exception(exceptions.ToString()));
            }
            */
		}

		private void WriteException(Exception ex)
		{
			Log.Error(ex);
			if (_debugMode)
			{
				Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
				if (ex.InnerException != null)
					Console.WriteLine(ex.InnerException.Message + Environment.NewLine + ex.InnerException.StackTrace);
				Console.ReadKey();
			}
			else
			{
				EventLog.WriteEntry(ex.Message + Environment.NewLine + ex.StackTrace, EventLogEntryType.Error);
				if (ex.InnerException != null)
					EventLog.WriteEntry(ex.InnerException.Message + Environment.NewLine + ex.InnerException.StackTrace, EventLogEntryType.Error);
			}
		}

		private void Start(string[] args)
		{
			Log.Information("Main Start");
			try
			{
				_waitHandle = new EventWaitHandle(false, EventResetMode.ManualReset, Name);
				CloudLocation = ConfigurationManager.AppSettings.Get("CloudLocation");

				var tasks = new List<Task>();

				_cts = new CancellationTokenSource();
				foreach (var host in _hosts)
				{
					Log.Information($"Start service: {host.GetType().Name}");
					var task = new Task((arg) =>
					{
						int exitCode = 0;
						try
						{
							_cts.Token.ThrowIfCancellationRequested();
							var serviceHost = (IHubHost)arg;
							try
							{
								exitCode = serviceHost.Run();
							}
							catch (Exception ex)
							{
								WriteException(ex);
								Log.Error(ex);
							}
							finally
							{
								host.Dispose();
							}
						}
						catch (OperationCanceledException ex)
						{
							_waitHandle?.Set();
							WriteException(ex);
							Log.Error(ex);
						}
						catch (Exception ex)
						{
							WriteException(ex);
							Log.Error(ex);
						}
					}, host, _cts.Token);

					tasks.Add(task);
					task.Start();
				}

				_tasks = tasks.ToArray();
				BuildWebSelfHost();
			}
			catch (Exception ex)
			{
				WriteException(ex);
			}
		}


		private new void Stop()
		{
			Log.Information("Main Stop");
			try
			{
				StopWebSelfHost();

				if (_waitHandle != null)
					_waitHandle.Set();
				try
				{
					if (_tasks != null)
					{
						// Trace.TraceInformation("Wait for task completion ...");
						Log.Information("Wait for task completion ...");
						if (Task.WaitAll(_tasks, _waitTime))
						{
							for (int index = 0; index < _tasks.Length; index++)
								_tasks[index].Dispose();
						}
						else
							_cts.Cancel();

						_tasks = null;
						_cts = null;
					}
				}
				finally
				{
					_waitHandle.Dispose();
					_waitHandle = null;
				}
			}
			catch (Exception ex)
			{
				WriteException(ex);
			}

			ClearLog();
		}

		protected override void OnStop()
		{
			Stop();
		}

		private bool IsRunning
		{
			get { return _tasks != null && _tasks.Any(x => x.Status == TaskStatus.Running || x.Status == TaskStatus.WaitingToRun); }
		}

		private void ConsoleRun(string[] args)
		{
			Start(args);
			if (IsRunning)
			{
				try
				{
					Console.WriteLine("{0} running. Press any key to exit ...", Name);
					Console.WriteLine();
					Console.ReadKey(false);
				}
				finally
				{
					Stop();
				}
			}
		}

		private static void InstallService()
		{
			ManagedInstallerClass.InstallHelper(new string[] { Assembly.GetExecutingAssembly().Location });
		}

		private static void UninstallService()
		{
			ManagedInstallerClass.InstallHelper(new string[] { "/u", Assembly.GetExecutingAssembly().Location });
		}

		#endregion

		#endregion
	}
}