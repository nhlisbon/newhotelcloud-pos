﻿using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;

namespace NewHotel.Pos.Hub
{
    public class UserNameValidator : UserNamePasswordValidator
    {
        public override void Validate(string userName, string password)
        {
            if (userName != "NHT")
                throw new SecurityTokenException("Invalid Username");
            if (password != "1234")
                throw new SecurityTokenException("Invalid Password");
        }
    }
}
