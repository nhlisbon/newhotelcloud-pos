﻿using NewHotel.Pos.Hub.Business.Business;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Hub.Business.PersistentObjects.Tickets;
using NewHotel.Pos.Hub.Business.Validations;
using NewHotel.Pos.IoC;

namespace NewHotel.Pos.Hub
{
    public class SharedServices
    {
        static SharedServices()
        {
        }

        public static void RegisterCommonDependencies()
        {
            CwFactory.RegisterWithFactoryMethodSingleton<IValidatorFactory, ValidatorFactory>(() =>
                {
                    ValidatorFactory factory = new ValidatorFactory();
                    factory.Register<Ticket>((ticket, validatorFactory) => new TicketValidator(ticket, validatorFactory));
                    factory.Register<ProductLine>((productLine, validatorFactory) => new ProductLineValidator(productLine, validatorFactory));
                    return factory;
                }
            );
            CwFactory.Register<IStocksManager, StocksManager>();
            CwFactory.Register<IMxmBusiness, MxmBusiness>();
            CwFactory.Register<IIntegrationBusiness, IntegrationBusiness>();
        }
    }
}
