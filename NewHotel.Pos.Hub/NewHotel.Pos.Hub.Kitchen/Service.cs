﻿using System;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.Manager.Oracle;
using NewHotel.Pos.Hub.Business.Business;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Common;
using NewHotel.Pos.Hub.Contracts.Common.Records.Stands;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.IoC;

namespace NewHotel.Pos.Hub.Kitchen
{
    public class Service : CommonService, IService
    {
        #region IoC

        static Service()
        {
            CwFactory.Register<IProductBusiness, ProductBusiness>();
            //CwFactory.RegisterWithFactoryMethod<IDatabaseManager, IDatabaseManager>(GetManager);
        }

        #endregion

        public ValidationStringResult CheckServiceAlive()
        {
            var result = new ValidationStringResult
            {
                Description = BusinessContext.SocketIOUri
            };
            return result;
        }

        public ValidationItemSource<StandAreaRecord> GetStandAreas()
        {
            var result = new ValidationItemSource<StandAreaRecord>();

            var business = GetBusiness<IProductBusiness>();
            var areas = business.GetStandAreas(1033);
            result.ItemSource = areas;
            return result;
        }

        public ValidationItemSource<AreasRecord> GetAreas()
        {
            var result = new ValidationItemSource<AreasRecord>();

            try
            {
                var business = GetBusiness<IProductBusiness>();
                var areas = business.GetAreasByStand(Guid.Empty, 1033);
                result.ItemSource = areas;
                return result;
            }
            catch (Exception ex)
            {
                return new ValidationItemSource<AreasRecord>(ex);
            }
        }

        public ValidationItemSource<AreasRecord> GetMarches()
        {
            throw new NotImplementedException();
        }
    }
}