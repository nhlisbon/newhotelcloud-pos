﻿using NewHotel.Pos.Hub.Core;
using System.ComponentModel.Composition;

namespace NewHotel.Pos.Hub.Kitchen
{
	[Export(typeof(IHubHost)), PartCreationPolicy(CreationPolicy.NonShared)]
    public sealed class HubKitchenServiceHost : HubServiceHost
    {
        #region Constructor

        public HubKitchenServiceHost()
            : base("NewHotel.Pos.Hub", typeof(Service))
        {
        }

        #endregion
    }
}