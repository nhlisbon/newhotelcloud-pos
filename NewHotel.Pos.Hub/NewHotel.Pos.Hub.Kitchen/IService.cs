﻿using System;
using NewHotel.Pos.Hub.Common;
using System.ServiceModel;
using System.ServiceModel.Web;
using NewHotel.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.Records.Stands;
using NewHotel.Pos.Hub.Model;

namespace NewHotel.Pos.Hub.Kitchen
{
    [ServiceContract]
    public interface IService : ICommonService
    {
        /// <summary>
        /// Util url for clients can check if server is alive
        /// </summary>
        /// <returns>true</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/CheckServiceAlive", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationStringResult CheckServiceAlive();

        /// <summary>
        /// Get Dispatch Areas for each stand
        /// </summary>
        /// <returns>List of areas</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetStandAreas", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<StandAreaRecord> GetStandAreas();
        
        /// <summary>
        /// Get Dispatch Areas
        /// </summary>
        /// <returns>List of areas</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetAreas", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<AreasRecord> GetAreas();
        
        /// <summary>
        /// Get Marches
        /// </summary>
        /// <returns>List of marches</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetMarches", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<AreasRecord> GetMarches();
    }
}