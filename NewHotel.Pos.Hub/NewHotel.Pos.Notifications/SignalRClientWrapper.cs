﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using NewHotel.Pos.Core;
using NewHotel.SignalR.Client;

namespace NewHotel.Pos.Notifications
{
    public class SignalRClientWrapper : IDisposable
    {
        #region Members

        private SignalRClient _signalRClient;

        #endregion
        #region Properties

        public string HubName { get; private set; }
        public string SignalRServer { get; private set; }
        public bool IsConnected => _signalRClient.State == Microsoft.AspNet.SignalR.Client.ConnectionState.Connected;

        #endregion
        #region Events

        public event Action<Exception> Error;
        public event Action<StateChange> StateChanged;

        #endregion
        #region Constructors

        public SignalRClientWrapper(string hubName, string signalRServer)
        {
            HubName = hubName;
            SignalRServer = signalRServer;
        } 

        #endregion
        #region Methods

        public Task Initialize(Action<string, string> callback = null)
        {
            if (_signalRClient == null)
            {
                _signalRClient = new SignalRClient(HubName, SignalRServer);
                _signalRClient.Error += OnError;
                _signalRClient.StateChanged += OnStateChanged;

                return _signalRClient.Start(callback);
            }

            return Task.CompletedTask;
        }

        public Task<bool> Subscribe<T>(string eventName, Action<string, T> callback)
        {
            return _signalRClient.Subscribe(eventName, callback);
        }

        public void Publish<T>(string eventName, T data) where T : class
        {
            try
            {
                if (_signalRClient != null && IsConnected)
                    _signalRClient.Publish(eventName, data);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public void Publish<T>(string eventName, NotificationContract<T> contract)
        {
            try
            {
                if (_signalRClient != null && IsConnected)
                    _signalRClient.Publish(eventName, Serializer.SerializeJson(contract, typeof(NotificationContract<T>)));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void OnError(Exception ex)
        {
            Error?.Invoke(ex);
        }

        private void OnStateChanged(Microsoft.AspNet.SignalR.Client.StateChange state)
        {
            StateChanged?.Invoke(new StateChange((ConnectionState)state.OldState, (ConnectionState)state.NewState));
        }

        #endregion 
        #region IDisposable Members

        public void Dispose()
        {
            _signalRClient.Dispose();
        }

        #endregion
    }
}