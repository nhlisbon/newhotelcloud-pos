﻿using System;
using System.Threading.Tasks;
using NewHotel.Pos.Core;
using SocketIOClient;

namespace NewHotel.Pos.Notifications
{
    public class SocketIOClientWrapper : IDisposable
    {
        #region Members

        private SocketIO _socketIOClient;
        private ConnectionState _state = ConnectionState.Disconnected;
        private Action<string, string> _callback;

        public static SocketIOClientWrapper Instance;

        #endregion
        #region Properties

        public string Uri { get; private set; }
        public bool IsConnected => _socketIOClient?.Connected ?? false;

        #endregion
        #region Events

        public event Action<Exception> Error;
        public event Action<StateChange> StateChanged;

        #endregion
        #region Constructors

        public SocketIOClientWrapper(string uri)
        {
            Uri = uri;
        } 

        #endregion
        #region Public Methods

        public Task Initialize(Action<string, string> callback = null)
        {
            if (_socketIOClient == null)
            {
                _callback = callback;
                _socketIOClient = new SocketIO(Uri);
                _socketIOClient.OnError += OnError;
                _socketIOClient.OnConnected += OnConnected;
                _socketIOClient.OnDisconnected += OnDisconnected;
                _socketIOClient.OnReconnectAttempt += OnReconnectAttempt;

                Instance = this;

                return _socketIOClient.ConnectAsync();
            }

            return Task.CompletedTask;
        }

        public Task<bool> Subscribe(Action<string, string> callback)
        {
            try
            {
                _socketIOClient.OnAny((name, resp) =>
                {
                    callback(name, resp.GetValue<string>());
                });

               return Task.FromResult(true);
            }
            catch (Exception ex)
            {
                Error?.Invoke(ex);
            }

            return Task.FromResult(false);
        }
        
        public Task<bool> Unsuscribe(string eventName)
        {
            try
            {
                _socketIOClient.Off(eventName);
                return Task.FromResult(true);
            }
            catch (Exception ex)
            {
                Error?.Invoke(ex);
            }

            return Task.FromResult(false);
        }

        public Task<bool> Subscribe<T>(string eventName, Action<T> callback)
        {
            try
            {
                if (_socketIOClient != null)
                {
                    _socketIOClient.On(eventName, resp =>
                    {
                        var a = Serializer.Deserialize<T>(resp.GetValue<string>());
                        callback(a);
                    });

                    return Task.FromResult(true);
                }
            }
            catch (Exception ex)
            {
                Error?.Invoke(ex);
            }

            return Task.FromResult(false);
        }

        public void Publish<T>(string eventName, T contract)
        {
            try
            {
                if (_socketIOClient != null && IsConnected)
                {
                    var data = Serializer.SerializeJson(contract, typeof(T));
                    _socketIOClient.EmitAsync(eventName, (resp) =>
                    {
                        _callback?.Invoke(eventName, data);
                    }, data);
                }
            }
            catch (Exception ex)
            {
                Error?.Invoke(ex);
            }
        }

        #endregion
        #region Private Methods

        private void OnError(object sender, string message)
        {
            Error?.Invoke(new Exception(message));
        }

        private void OnReconnectAttempt(object sender, int attempt)
        {
            if (attempt == 1)
                StateChanged?.Invoke(new StateChange(_state, _state = ConnectionState.Reconnecting));
        }

        private void OnConnected(object sender, EventArgs e)
        {
            StateChanged?.Invoke(new StateChange(_state, _state = ConnectionState.Connected));
        }

        private void OnDisconnected(object sender, string message)
        {
            StateChanged?.Invoke(new StateChange(_state, _state = ConnectionState.Disconnected));
        }

        #endregion
        #region IDisposable Members

        public async void Dispose()
        {
            if (_socketIOClient != null)
            {
                if (_socketIOClient.Connected)
                    await _socketIOClient.DisconnectAsync();
                _socketIOClient.Dispose();
            }
        }

        #endregion
    }
}