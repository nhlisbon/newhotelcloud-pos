﻿using System;

namespace NewHotel.Pos.Notifications
{
    public sealed class VoidOrder
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
    }

    public sealed class VoidDispatchAreaOrders
    {
        public long LanguageId { get; set; }
        public Guid AreaId { get; set; }
        public string Area { get; set; }
        public string Saloon { get; set; }
        public string Table { get; set; }
        public Guid TicketId { get; set; }
        public VoidOrder[] Orders { get; set; }
    }
}