﻿using System;

namespace NewHotel.Pos.Notifications
{
    public sealed class DispatchAreaStatusChanged
    {
        public long LanguageId { get; set; }
        public string Area { get; set; }
        public Guid TicketId { get; set; }
        public Guid OrderId { get; set; }
        public short OrderStatus { get; set; }
    }
}