﻿using System;

namespace NewHotel.Pos.Notifications
{
    public sealed class SendOrder
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public string Separator { get; set; }
        public string Preparations { get; set; }
        public string Observations { get; set; }
        public short? PaxNumber { get; set; }
    }

    public sealed class SendDispatchAreaOrders
    {
        public long LanguageId { get; set; }
        public Guid AreaId { get; set; }
        public string Area { get; set; }
        public string Saloon { get; set; }
        public string Table { get; set; }
        public Guid TicketId { get; set; }
        public SendOrder[] Orders { get; set; }
    }
}