﻿namespace NewHotel.Pos.Notifications
{
    public sealed class StateChange
    {
        public readonly ConnectionState OldState;
        public readonly ConnectionState NewState;

        public StateChange(ConnectionState oldState, ConnectionState newState)
        {
            OldState = oldState;
            NewState = newState;
        }
    }
}