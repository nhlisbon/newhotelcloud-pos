﻿namespace NewHotel.Pos.Notifications
{
    public enum ConnectionState
    {
        Connecting,
        Connected,
        Reconnecting,
        Disconnected,
    }
}