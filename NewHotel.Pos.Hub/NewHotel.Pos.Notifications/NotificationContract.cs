﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Globalization;

namespace NewHotel.Pos.Notifications
{
    public class CustomJsonDateTimeConverter : DateTimeConverterBase
    {
        private const string Format = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'";

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var value = reader.Value;
            if (value != null)
            {
                if (DateTime.TryParseExact(value.ToString(), Format, CultureInfo.InvariantCulture, DateTimeStyles.None, out var result))
                    return result;
            }

            return null;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((DateTime)value).ToUniversalTime().ToString(Format));
        }
    }

    public class NotificationContract<T>
    {
        public Guid TokenID { get; set; }
        [JsonConverter(typeof(CustomJsonDateTimeConverter))]
        public DateTime Fired { get; set; }
        public Guid StandId { get; set; }
        public Guid CashierId { get; set; }
        public Guid HotelId { get; set; }

        public T DataContext { get; set; }

        public NotificationContract()
        {
            Fired = DateTime.UtcNow;
        }
    } 
}