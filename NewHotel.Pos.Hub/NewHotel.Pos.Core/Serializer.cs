﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;
using NewHotel.Contracts;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace NewHotel.Pos.Core
{
    public static class Serializer
    {
        private static readonly List<Type> _knowTypes;
        private static readonly JsonSerializerSettings _jsonSerializerSettings;

        static Serializer()
        {
            _knowTypes = new List<Type>();
            _knowTypes.Add(typeof(POSTicketContract));
            _knowTypes.Add(typeof(StandTurnDateContract));
            _knowTypes.Add(typeof(CancellationControlContract));
            _knowTypes.Add(typeof(Guid));
            _knowTypes.Add(typeof(DateTime));
            _knowTypes.Add(typeof(QueryRequest));
            _knowTypes.Add(typeof(TransferContract));
            _knowTypes.Add(typeof(TablesGroupContract));
            _knowTypes.Add(typeof(MesaContract));
            _knowTypes.Add(typeof(LiteTablesReservationByTableRecord));

            var contractResolver = new DefaultContractResolver
            {
                NamingStrategy = new CamelCaseNamingStrategy()
            };

            _jsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = contractResolver,
                Formatting = Formatting.Indented
            };
        }

        public static string Serialize(object obj)
        {
            if (obj == null)
                return "null";

            return SerializeJson(obj, obj.GetType());
        }

        public static string Serialize<T>(object obj)
        {
            return SerializeJson(obj, typeof(T));
        }

        public static string SerializeJson(object obj, Type type)
        {
            var result = string.Empty;

            using (var ms = new MemoryStream())
            {
                var serializer = new DataContractJsonSerializer(type, _knowTypes);

                serializer.WriteObject(ms, obj);
                ms.Position = 0;
                using (var sr = new StreamReader(ms))
                {
                    result = sr.ReadToEnd();
                    sr.Close();
                }
            }

            return result;
        }

        private static object Deserialize(string obj, Type type)
        {
            object result = null;

            using (var ms = new MemoryStream())
            {
                using (var sw = new StreamWriter(ms))
                {
                    sw.Write(obj);
                    sw.Flush();
                    ms.Position = 0;
                    var serializer = new DataContractJsonSerializer(type, _knowTypes);
                    result = serializer.ReadObject(ms);
                }
            }

            return result;
        }

        public static T Deserialize<T>(string obj)
        {
            return (T)Deserialize(obj, typeof(T));
        }

        public static string ConvertToJson(object obj)
        {
            return JsonConvert.SerializeObject(obj, _jsonSerializerSettings);
        }

        public static T ConvertFromJson<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json, _jsonSerializerSettings);
        }
    }
}