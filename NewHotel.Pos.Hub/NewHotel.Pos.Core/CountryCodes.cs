﻿namespace NewHotel.Pos.Core
{
    public static class CountryCodes
    {
        public const string Portugal = "PT";
        public const string Angola = "AO";
        public const string Brazil = "BR";
        public const string Peru = "PE";
        public const string Ecuador = "EC";
        public const string Mexico = "MX";
        public const string Chile = "CL";
        public const string Colombia = "CO";
    }
}