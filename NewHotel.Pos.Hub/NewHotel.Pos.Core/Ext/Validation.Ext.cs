﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Pos.Core.Ext
{
    public static class Validation
    {
        public static ValidationResult IfEmptyAddValidations(this ValidationResult validation, Func<ValidationResult> func)
        {
            if (validation.IsEmpty)
                validation.AddValidations(func());

            return validation;
        }

        public static T IfEmptyAddValidationsAndGetContractResult<T>(this ValidationResult validation, Func<ValidationContractResult> func) where T : BaseContract
        {
            T result = default;
            if (validation.IsEmpty)
            {
                var funcResult = func();
                if (validation.AddValidations(funcResult).IsEmpty)
                    result = (T)funcResult.Contract;
            }

            return result;
        }

        public static T IfEmptyAddValidationsAndGetResult<T>(this ValidationResult validation, Func<T> func) where T : ValidationResult
        {
            T result = default;
            if (validation.IsEmpty)
            {
                var funcResult = func();
                if (validation.AddValidations(funcResult).IsEmpty)
                    result = funcResult;
            }

            return result;
        }
    }
}