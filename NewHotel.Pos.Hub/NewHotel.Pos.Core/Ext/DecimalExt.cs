﻿using System;
using System.Linq;

namespace NewHotel.Pos.Core.Ext
{
    public static class DecimalExt
    {
        struct DecimalPow
        {
            public int Places { get; set; }
            public decimal Pow { get; set; }
            public decimal Fraq { get; set; }
        }
        static readonly DecimalPow[] pows;
        static DecimalExt()
        {
            const int max = 20;
            pows = new DecimalPow[max];

            for (int i = 0; i < max; i++)
            {
                decimal pow = (decimal)Math.Pow(10, i);

				pows[i] = new DecimalPow
				{
					Places = i,
					Pow = pow,
					Fraq = 1M / pow
				};
            }
        }

		public static int DecimalPlaces(this decimal d)
        {
            return BitConverter.GetBytes(Decimal.GetBits(d)[3])[2];
        }

        public static decimal Truncate(this decimal value, int places)
        {
            var aux = Math.Pow(10f, places);
            return (decimal)(Math.Truncate(aux * (double)value) / aux);
        }

        public static decimal Truncate2(this decimal value)
        {
            return Truncate(value, 2);
        }

        public static decimal Round(this decimal value, int decimals)
        {
            return Math.Round(value, decimals);
        }

        public static decimal Round2(this decimal value)
        {
            return Round(value, 2);
        }

        public static decimal CommercialRound(this decimal value, int places = 2)
		{
			decimal result = Math.Round(value, places, MidpointRounding.AwayFromZero);
   //         if (result < value)
			//{
			//	result += pows[places].Fraq;
			//}
			return result;
		}

        public static bool InRange(this decimal value, decimal compareWith, int places = 2)
        {
            return Math.Abs(value - compareWith) <= pows[places].Fraq;
        }

        /// <summary>
        /// P = T * % / 100
        /// </summary>
        public static decimal GetDiscountValue(this decimal total, decimal percent)
        {
            return (total * percent / 100).Round2();
        }

        /// <summary>
        /// % = P / T * 100 
        /// </summary>
        public static decimal GetPercent(this decimal total, decimal percentValue)
        {
            return (percentValue / total * 100);
        }

        public static int GetAmountOfDecimals(this decimal value)
        {
            int digs = 0;
            while (decimal.Truncate(value) != value)
            {
                digs++;
                value *= 10;
            }

            return digs;
        }

        public static bool HasDecimals(this decimal value)
        {
            return Decimal.Truncate(value) != value;
        }

        public static decimal RoundTo(this decimal value, short decimals)
        {
            return Decimal.Round(value, decimals, MidpointRounding.AwayFromZero);
        }

        public static decimal RoundToLower(this decimal value, int precision)
        {
            decimal factor = (decimal)Math.Pow(10, precision);             
            decimal unround = decimal.Floor(value * factor);             
            decimal fraction = value * factor - unround;            
            return fraction <= 0.5m 
                ? unround / factor 
                : (unround + 1m) / factor
                ;
        }
    }
}
