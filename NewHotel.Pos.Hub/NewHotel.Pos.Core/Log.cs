﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using NewHotel.Contracts;
using Newtonsoft.Json;

namespace NewHotel.Pos.Core
{
    public enum Event
    {
        PrintBill, PrintTicket, PrintInvoice, PrintCreditNote, PrintBallot
    };

    public interface ILogger
    {
        void TraceError(string message);
        void TraceError(IEnumerable<Validation> validations);
        void TraceInformation(string message);
        void TraceWarning(string message);

        void TraceInformation(string message, object obj);
        void TraceEvent(Event e, string message);
    }

    public class TraceLog : ILogger
    {
        private readonly string _prefix = "NEWHOTEL.POS... ";

        public TraceLog()
        {
            
        }

        public TraceLog(string prefix)
        {
            _prefix = prefix;
        }

        public void TraceEvent(Event e, string message)
        {
            TraceInformation($"Event: {e} \nMessage: {message}");
        }

        public void TraceInformation(string message, object obj)
        {
            string sObj = null;
            try
            {
                sObj = JsonConvert.SerializeObject(obj, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            }
            finally
            {
                TraceInformation($"{message} => \n{sObj}");
            }
        }

        public void TraceInformation(string message)
        {
            try
            {
                Trace.TraceInformation(_prefix + $"Time: {DateTime.Now} \n{message}");
            }
            catch
            {
            }
        }

        public void TraceWarning(string message)
        {
            try
            {
                Trace.TraceWarning(_prefix + message);
            }
            catch
            {
            }
        }

        public void TraceError(string message)
        {
            try
            {
                Trace.TraceError(_prefix + message);
            }
            catch
            {
            }
        }

        public void TraceError(IEnumerable<Validation> validations)
        {
            try
            {
                foreach (var validation in validations)
                    TraceError(validation.Message);
            }
            catch
            {
            }
        }
    }
}
