﻿using System.Text;

namespace NewHotel.Core
{
    public sealed class SqlPart
    {
        private StringBuilder sql;

        public SqlPart()
        {
            Clear();
        }

        public void Add(string part)
        {
            sql.AppendLine(part);
        }

        public void Clear()
        {
            sql = new StringBuilder();
        }

        public bool IsEmpty
        {
            get { return sql.Length == 0; }
        }

        public void CopyFrom(SqlPart sqlPart)
        {
            Clear();
            sql.Append(sqlPart.sql.ToString());
        }

        public override string ToString()
        {
            return IsEmpty ? string.Empty : sql.ToString();
        }
    }
}