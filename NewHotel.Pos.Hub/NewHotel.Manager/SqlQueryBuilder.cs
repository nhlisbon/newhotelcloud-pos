﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;

namespace NewHotel.Manager
{
    public abstract class SqlQueryBuilder : ISqlQueryBuilder
    {
        private StringBuilder query = new StringBuilder();

        public const string SelectToken = "SELECT";
        public const string FromToken = "FROM";
        public const string WhereToken = "WHERE";
        public const string UnionToken = "UNION";

        private static char[] spaceDelimiters;
        private static char[] colDelimiters;
        private static char[] paramDelimiters;
        private static char[] parentesis;

        protected const string ROWNUM = "ROWNUM_";
        protected const string TABLEALIAS = "TABLE_";
        protected const string STARTROW = "STARTROW_";
        protected const string ENDROW = "ENDROW_";

        static SqlQueryBuilder()
        {
            spaceDelimiters = new char[] { ' ', '\r', '\n' };
            colDelimiters = new char[] { ' ', '.', '\r', '\n' };
            paramDelimiters = new char[] { ' ', ',', ')', '\r', '\n' };
            parentesis = new char[] { '(', ')' };
        }

        protected void Invalidate()
        {
            _columns = null;
            _parameters = null;
            _containsWhere = null;
        }

        private static int IndexOf(string str, char[] chars, int start, char leftskip, char rightskip)
        {
            int index;
            do
            {
                IList<char> delimiters = new List<char>(chars);
                delimiters.Add(leftskip);
                index = str.IndexOfAny(delimiters.ToArray(), start);
                if (index < 0 || str[index] != leftskip)
                    break;
                else
                {
                    start = index + 1;
                    int count = 1;
                    while (count > 0)
                    {
                        index = str.IndexOfAny(new char[] { leftskip, rightskip }, start);
                        if (index < 0)
                            break;
                        if (str[index] == rightskip)
                            count--;
                        else if (str[index] == leftskip)
                            count++;
                        start = index + 1;
                    }
                }
            } while (true);

            return index;
        }

        private string[] _columns;
        public string[] Columns
        {
            get
            {
                if (_columns == null)
                    _columns = GetColumns(query.ToString());

                return _columns;
            }
        }

        private string[] GetColumns(string sql)
        {
            HashSet<string> names = new HashSet<string>();

            int begin = sql.IndexOf(SelectToken, StringComparison.InvariantCultureIgnoreCase) + SelectToken.Length;
            int end = sql.LastIndexOf(UnionToken, StringComparison.InvariantCultureIgnoreCase);
            if (end < 0)
                end = sql.LastIndexOf(FromToken, StringComparison.InvariantCultureIgnoreCase) + FromToken.Length;
            else
            {
                end = Math.Min(end, sql.LastIndexOf(FromToken, StringComparison.InvariantCultureIgnoreCase) + FromToken.Length);
            }
            string columns = sql.Substring(SelectToken.Length, end - begin);

            int start = 0;
            int i = 0;
            do
            {
                i = IndexOf(columns, new char[] { ',' }, start, '(', ')');
                if (i < 0)
                {
                    i = columns.LastIndexOf(FromToken);
                    if (i >= 0)
                    {
                        i--;
                        while (spaceDelimiters.Contains(columns[i]))
                        {
                            i--;
                        }
                        end = i;
                        begin = columns.LastIndexOfAny(colDelimiters, end);
                        if (begin >= 0)
                            names.Add(columns.Substring(begin + 1, end - begin).Trim());
                    }
                    break;
                }

                start = i + 1;
                end = i - 1;
                begin = columns.LastIndexOfAny(colDelimiters, end);
                if (begin >= 0)
                    names.Add(columns.Substring(begin + 1, end - begin).Trim());
            } while (true);

            return names.ToArray();
        }

        private string[] _parameters;
        public string[] Parameters
        {
            get
            {
                if (_parameters == null)
                {
                    _parameters = GetParameters(ToString());
                }

                return _parameters;
            }
        }

        private string[] GetParameters(string sql)
        {
            HashSet<string> names = new HashSet<string>();

            int i = 0;
            while (true)
            {
                int begin = IndexOf(sql, new char[] { ':' }, i, '\'', '\'');
                if (begin < 0)
                    break;
                begin++;
                int end = sql.IndexOfAny(paramDelimiters, begin);
                if (end >= 0)
                    names.Add(sql.Substring(begin, end - begin).Trim().ToUpper());
                i = end;
            }

            return names.ToArray();
        }

        private bool? _containsWhere;
        public bool ContainsWhere
        {
            get
            {
                if (!_containsWhere.HasValue)
                    _containsWhere = GetContainsWhere(query.ToString());

                return _containsWhere.Value;
            }
        }

        private bool GetContainsWhere(string sql)
        {
            int index = sql.LastIndexOf(FromToken);
            return sql.IndexOf(WhereToken, index + FromToken.Length) >= 0;
        }

        public virtual string Parse(string sql)
        {
            return sql;
        }

        private bool _paged = false;
        public bool Paged
        {
            get { return _paged; }
            set
            {
                if (_paged != value)
                {
                    _paged = value;
                    _parameters = null;
                }
            }
        }

        protected abstract string GetPagedStatement(string query);

        public ISqlQueryBuilder Append(string str)
        {
            query.Append(Parse(str.ToUpper()));
            Invalidate();
            return this;
        }

        public ISqlQueryBuilder AppendLine(string str)
        {
            query.AppendLine(Parse(str.ToUpper()));
            Invalidate();
            return this;
        }

        public override string ToString()
        {
            return _paged ? GetPagedStatement(query.ToString()) : query.ToString();
        }
    }
}
