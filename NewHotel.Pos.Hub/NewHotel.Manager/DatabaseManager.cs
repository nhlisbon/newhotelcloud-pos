﻿//#define PERF
using System;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using NewHotel.Core;
using NewHotel.DataAnnotations;

namespace NewHotel.Manager
{
    public abstract class DatabaseManager : IDatabaseManager
    {
        #if (PERF)
        #region Private Struct

        public sealed class CommandParameterLogger
        {
            private string _parameterName;
            private string _parameterValue;

            [XmlAttribute("Name")]
            public string Name
            {
                get { return _parameterName; }
                set { _parameterName = value; }
            }

            [XmlText]
            public string Value
            {
                get { return _parameterValue; }
                set { _parameterValue = value; }
            }

            public CommandParameterLogger(string parameterName, string parameterValue)
            {
                _parameterName = parameterName;
                _parameterValue = parameterValue;
            }

            public CommandParameterLogger()
                : this(string.Empty, null)
            {
            }
        }

        [XmlRoot(ElementName = "Command", Namespace = "http://tempuri.org/Command.xsd")]
        public sealed class CommandLogger : ICommandLogger, IComparable<ICommandLogger>
        {
            private string _commandName;
            private string _commandText;
            private CommandParameterLogger[] _commandParams;

            private TimeSpan _commandExecTime;
            private TimeSpan _commandFetchTime;
            private Stopwatch _commandStopWatch;

            [XmlAttribute("Name")]
            public string CommandName
            {
                get { return _commandName; }
                set { _commandName = value; }
            }

            [XmlElement(ElementName = "ExecTime", DataType = "duration", IsNullable = false)]
            public string ExecTime
            {
                get { return _commandExecTime == TimeSpan.Zero ? null : XmlConvert.ToString(_commandExecTime); }
                set { _commandExecTime = string.IsNullOrEmpty(value) ? TimeSpan.Zero : XmlConvert.ToTimeSpan(value); }
            }

            [XmlElement(ElementName = "FetchTime", DataType = "duration", IsNullable = false)]
            public string FetchTime
            {
                get { return _commandFetchTime == TimeSpan.Zero ? null : XmlConvert.ToString(_commandFetchTime); }
                set { _commandFetchTime = string.IsNullOrEmpty(value) ? TimeSpan.Zero : XmlConvert.ToTimeSpan(value); }
            }

            [XmlText]
            public string CommandText
            {
                get { return _commandText; }
                set { _commandText = value; }
            }

            [XmlArray("Parameters")]
            [XmlArrayItem("Parameter", IsNullable = false)]
            public CommandParameterLogger[] CommandParams
            {
                get { return _commandParams != null && _commandParams.Length > 0 ? _commandParams : null; }
                set { _commandParams = value; }
            }

            public TimeSpan ElapsedTime
            {
                get { return _commandExecTime.Add(_commandFetchTime); }
            }

            private CommandLogger(string commandName, string commandText,
                CommandParameterLogger[] commandParams, TimeSpan execTime, TimeSpan fetchTime)
            {
                _commandName = commandName;
                _commandText = commandText;
                _commandParams = commandParams;
                _commandExecTime = execTime;
                _commandFetchTime = fetchTime;
            }

            public CommandLogger(string commandName, string commandText, CommandParameterLogger[] commandParams)
                : this(commandName, commandText, commandParams, TimeSpan.Zero, TimeSpan.Zero)
            {
                _commandStopWatch = Stopwatch.StartNew();
            }

            public CommandLogger(string commandName)
                : this(commandName, string.Empty, null)
            {
            }

            public CommandLogger()
                : this(string.Empty)
            {
            }

            public void Executed()
            {
                if (_commandStopWatch != null && _commandStopWatch.IsRunning)
                {
                    _commandExecTime = _commandStopWatch.Elapsed;
                    _commandFetchTime = TimeSpan.Zero;
                }
            }

            public void Fetched()
            {
                if (_commandStopWatch != null && _commandStopWatch.IsRunning)
                    _commandFetchTime = _commandStopWatch.Elapsed - _commandExecTime;
            }

            public int CompareTo(ICommandLogger other)
            {
                return ElapsedTime.CompareTo(other.ElapsedTime);
            }

            public override string ToString()
            {
                return _commandName;
            } 

            public void Dispose()
            {
                if (_commandStopWatch != null)
                {
                    if (_commandStopWatch.IsRunning)
                        _commandStopWatch.Stop();
                    _commandStopWatch = null;
                }
            }
        }

        #endregion
        #endif
        #region Constants

        protected const string MANAGERLOGKEY = "NewHotel.Manager";

        #if (PERF)
        private const string MANAGERCOMMANDLOGKEY = "NewHotel.Manager.Command";
        #endif

        protected const string TABLEALIAS = "TABLE_";
        protected const string ROWOFFSET = "ROWOFFSET_";
        protected const string ROWCOUNT = "ROWCOUNT_";
        protected const string CURSOR = "CURSOR_";

        protected const string ROWNUM = "ROWNUM_";
        protected const string STARTROW = "STARTROW_";
        protected const string ENDROW = "ENDROW_";

        protected const char TrueValue = '1';
        protected const char FalseValue = '0';

        #endregion
        #region Members

        protected string _connectionString;
        protected readonly string _moduleName;
        protected string _user;
        protected string _password;
        protected string _dataSource;

        private long openCounter = 0;

        #endregion
        #region Constructors

        protected DatabaseManager(string connectionString, string module, string user, string password, string dataSource)
        {
            _connectionString = connectionString;
            _moduleName = module;
            _user = user;
            _password = password;
            _dataSource = dataSource;
        }

        ~DatabaseManager()
        {
            Dispose();
        }

        #endregion
        #region Properties

        public virtual string TableAliasParamName
        {
            get { return TABLEALIAS; }
        }

        public virtual string RowOffsetParamName
        {
            get { return ROWOFFSET; }
        }

        public virtual string RowCountParamName
        {
            get { return ROWCOUNT; }
        }

        public virtual string RowNumParamName
        {
            get { return ROWNUM; }
        }

        public virtual string StartRowParamName
        {
            get { return STARTROW; }
        }

        public virtual string EndRowParamName
        {
            get { return ENDROW; }
        }

        protected static void LogException(int hashCode, Exception ex)
        {
            StackFrame stackFrame = new StackFrame(2);
            Log.Source[MANAGERLOGKEY].TraceError("Exception on connection {0}.\n File: {1}, Line: {2}, Caller: {3}.\n{4}",
                hashCode, stackFrame.GetFileName(), stackFrame.GetFileLineNumber(), stackFrame.GetMethod(), ex.Message);
        }

        public string ConnectionString
        {
            get { return _connectionString; }
        }

        public string User
        {
            get { return _user; }
        }

        public string Password
        {
            get { return _password; }
        }

        public string DataSource
        {
            get { return _dataSource; }
        }

        protected abstract IDbConnection Connection { get; }
        public abstract IDbTransaction Transaction { get; }

        #endregion
        #region Public Methods

        public abstract Type GetTypeFromMetadata(Type type, string dataTypeName, object[] metadata, out bool isNullable);

        public virtual object ConvertValueType(object value)
        {
            if (value != null)
            {
                var type = value.GetType();
                if (type == typeof(bool))
                    return (bool)value ? TrueValue.ToString() : FalseValue.ToString();
                else if (type.IsEnum)
                    return Convert.ChangeType(value, Enum.GetUnderlyingType(type));
                else if (type == typeof(DateTime))
                    return ((DateTime)value).ToUtcDateTime();
                else if (type == typeof(ARGBColor))
                {
                    var argbColor = (ARGBColor)value;
                    int color = argbColor;
                    return new decimal(color);
                }
            }

            return value;
        }

        public abstract bool IsConnected { get; }
        protected abstract void AfterOpenConnection();
        protected abstract void ClearConnection(bool dispose);

        protected virtual void ClearTransaction(bool dispose)
        {
            _transactionLevel = 0;
        }

        public void Open()
        {
            if (openCounter == 0)
            {
                if (!IsConnected)
                {
                    Connection.Open();
                    AfterOpenConnection();
                    Log.Source[MANAGERLOGKEY].TraceInfo("Connection opened.\n{0}", ToString());
                }
            }

            openCounter++;
        }

        public void Close()
        {
            if (openCounter > 0)
            {
                openCounter--;

                if (openCounter == 0)
                {
                    if (IsConnected)
                    {
                        Connection.Close();
                        Log.Source[MANAGERLOGKEY].TraceInfo("Connection closed.\n{0}", ToString());
                    }
                    ClearConnection(true);
                }
            }
        }

        private int _transactionLevel = 0;
        public int TransactionLevel
        {
            get { return _transactionLevel; }
        }

        protected abstract void CreateTransaction();

        public void BeginTransaction()
        {
            if (_transactionLevel++ == 0 && Transaction == null)
            {
                if (IsConnected)
                    CreateTransaction();
                else
                    throw new InvalidOperationException("Cannot start transaction. Connection is closed");
            }
        }

        public void CommitTransaction()
        {
            if (_transactionLevel > 0)
                _transactionLevel--;

            if (Transaction != null && _transactionLevel == 0)
            {
                if (IsConnected)
                {
                    Transaction.Commit();
                    ClearTransaction(true);
                }
                else
                    throw new InvalidOperationException("Cannot commit transaction. Connection is closed");
            }
        }

        public void RollbackTransaction()
        {
            if (Transaction != null)
            {
                if (IsConnected)
                {
                    Transaction.Rollback();
                    ClearTransaction(true);
                }
                else
                    throw new InvalidOperationException("Cannot rollback transaction. Connection is closed");
            }
        }

        private bool _isDisposed = false;
        public void Dispose()
        {
            if (!_isDisposed)
            {
                _isDisposed = true;
                try
                {
                    ClearConnection(false);
                }
                finally
                {
                    GC.SuppressFinalize(this);
                }
            }
        }

        #region Query Statement

        protected abstract class QueryStatement : IQueryStatement
        {
            public readonly SqlPart Select = new SqlPart();
            public readonly SqlPart From = new SqlPart();
            public readonly SqlPart On = new SqlPart();
            public readonly SqlPart Where = new SqlPart();
            public readonly SqlPart GroupBy = new SqlPart();
            public readonly SqlPart OrderBy = new SqlPart();

            #region IQueryStatement Members

            public void ClearWhere()
            {
                Where.Clear();
            }

            public void AppendWhere(string where)
            {
                Where.Add(where);
            }

            public abstract IQueryStatement Clone();

            #endregion
        }

        #endregion

        protected abstract QueryStatement GetQueryStatement();

        #region Merge Statement

        protected abstract class MergeStatement : IMergeStatement
        {
            private string timestampName;
            public readonly SqlPart MergeInto = new SqlPart();
            public readonly SqlPart UsingOn = new SqlPart();
            public readonly SqlPart UpdateSet = new SqlPart();
            public readonly SqlPart UpdateWhere = new SqlPart();
            public readonly SqlPart Insert = new SqlPart();
            public readonly SqlPart Values = new SqlPart();

            #region IMergeStatement Members

            public string TimestampName
            {
                get { return timestampName; }
                set { timestampName = value.ToUpperInvariant(); }
            }

            public abstract string FormatTimestamp(DateTime date);
            public abstract string GetTimestampFormatExpression(string paramName, string colName);

            public abstract IMergeStatement Clone();

            #endregion
        }

        #endregion

        protected abstract MergeStatement GetMergeStatement();

        #region Delete Statement

        protected abstract class DeleteStatement : IDeleteStatement
        {
            public readonly SqlPart DeleteFrom = new SqlPart();
            public readonly SqlPart Where = new SqlPart();

            #region IDeleteStatement Members

            public abstract IDeleteStatement Clone();

            #endregion
        }

        #endregion

        protected abstract DeleteStatement GetDeleteStatement();

        #region Lock Statement

        protected abstract class LockStatement : ILockStatement
        {
            public readonly SqlPart Table = new SqlPart();

            #region ILockStatement Members

            public abstract ILockStatement Clone();

            #endregion
        }

        #endregion

        protected abstract LockStatement GetLockStatement();

        private static string GetName(string name)
        {
            int len = name.Length - 1;
            if (len >= 0)
            {
                switch (name[len])
                {
                    case '+':
                    case '-':
                        return name.Substring(0, len);
                }
            }

            return name;
        }

        private int GetOrder(string name)
        {
            int len = name.Length - 1;
            if (len >= 0)
            {
                switch (name[len])
                {
                    case '+': return 1;
                    case '-': return -1;
                }
            }

            return 0;
        }

        private static string GetAliasName(string name)
        {
            return GetName(name).Replace('.', '$');
        }

        private static string GetFullName(string name)
        {
            return GetName(name).Replace('$', '.');
        }

        private static string GetParamName(string name)
        {
            return ":" + GetAliasName(name);
        }

        private static string GetTableName(string name)
        {
            name = GetName(name);
            return name.Substring(0, name.IndexOf('.'));
        }

        private static string GetColName(string name)
        {
            name = GetName(name);
            return name.Substring(name.IndexOf('.') + 1);
        }

        public abstract string TableLock { get; }
        public abstract string StatementLock { get; }

        protected static readonly IDictionary<string, IQueryStatement> queryCmdMap = new Dictionary<string, IQueryStatement>();
        public virtual IQueryStatement GetQueryCommandText(Type type, LambdaExpression[] exprs, int count = 0)
        {
            var key = new StringBuilder(type.Name);
            if (exprs != null && exprs.Length > 0)
                key.AppendFormat("({0})", string.Join(",", exprs.Select(x => x.Body.ToString().Replace(x.Parameters[0].Name + ".", string.Empty))));
            key.AppendFormat("[{0}]", count);

            IQueryStatement sql;
            lock (queryCmdMap)
            {
                if (!queryCmdMap.TryGetValue(key.ToString(), out sql))
                {
                    sql = GetLoadCommandText(type);//.Clone();
                    sql.ClearWhere();

                    var filters = new List<string>();
                    if (exprs != null && exprs.Length > 0)
                    {
                        for (var i = 0; i < exprs.Length; i++)
                        {
                            var expr = exprs[i].Body;
                            if (expr.NodeType == ExpressionType.Convert)
                                expr = (expr as UnaryExpression).Operand;
                            var member = expr as MemberExpression;

                            if (member != null)
                            {
                                if (member.Member.DeclaringType.IsAssignableFrom(type))
                                {
                                    var pair = type.PersistentProps()
                                        .FirstOrDefault(x => x.Value.Name.Equals(member.Member.Name));
                                    if (pair.Key == null)
                                        throw new ArgumentException("Persistence attribute not defined for this member.");

                                    if (count > 0)
                                    {
                                        var parameters = new List<string>();
                                        for (int j = 0; j < count; j++)
                                            parameters.Add(":ARG" + j.ToString());
                                        filters.Add(pair.Key + " IN (" + string.Join(", ", parameters) + ")");
                                    }
                                    else
                                        filters.Add(pair.Key + " = :ARG" + i.ToString());
                                }
                                else
                                    throw new ArgumentException(string.Format("Member expression must belongs to {0}.", type.FullName));
                            }
                            else
                                throw new ArgumentException(string.Format("{0} is not a member expression.", expr.ToString()));
                        }
                    }

                    if (filters.Count > 0)
                        sql.AppendWhere(string.Join(" AND ", filters));

                    queryCmdMap.Add(key.ToString(), sql);
                }
            }

            return sql;
        }

        protected static readonly IDictionary<Type, IQueryStatement> loadCmdMap =
            new Dictionary<Type, IQueryStatement>();
        public virtual IQueryStatement GetLoadCommandText(Type type)
        {
            IQueryStatement statement;
            lock (loadCmdMap)
            {
                if (!loadCmdMap.TryGetValue(type, out statement))
                {
                    var tables = type.PersistentTables()
                        .Values.SelectMany(x => x);
                    var props = type.PersistentProps();
                    var exprs = type.ReferenceExprs();
                    var refs = type.ReferenceProps();

                    var tableNames = tables.Select(x => GetTableName(x));

                    var sql = GetQueryStatement();
                    var cols = new List<string>();
                    var sorts = new List<int>();
                    int i = 0;
                    foreach (var col in props.Keys.OrderBy(c => c).Concat(refs.Keys)
                        .Where(c => tableNames.Contains(GetTableName(c)) && !exprs.ContainsKey(c)))
                    {
                        i++;
                        var colName = GetName(col);
                        var order = i * GetOrder(col);
                        if (order != 0)
                            sorts.Add(order);

                        cols.Add(colName + " AS " + GetAliasName(colName));
                    }
                    foreach (var col in exprs.Values)
                        cols.Add(col);
                    sql.Select.Add(string.Join(",\r\n", cols));
                    var mainTable = tables.First();
                    var mainTableName = GetTableName(mainTable);
                    sql.From.Add(mainTableName + " {0} " + mainTableName);
                    foreach (var table in tables.Skip(1))
                    {
                        var tableName = GetTableName(table);
                        sql.From.Add("INNER JOIN " + tableName + "{0} " + tableName + " ON " + table + " = " + mainTable);
                    }
                    sql.Where.Add(mainTable + " = :ID_PK{1}");
                    var sort = string.Join(",", sorts.Select(x => Math.Abs(x).ToString() + (x < 0 ? " DESC" : " ASC")));
                    if (sort != string.Empty)
                        sql.OrderBy.Add(sort);
                    loadCmdMap.Add(type, sql);
                    statement = sql;
                }
            }

            return statement.Clone();
        }

        public void RegisterCustomLoadCommandText(Type type, IQueryStatement query)
        {
            lock (loadCmdMap)
            {
                loadCmdMap[type] = query;
            }
        }

        private static string GetUpdateTimestampExpression(IMergeStatement statement, string col, string timestamp)
        {
            string colName = GetColName(col);
            string paramName = GetParamName(timestamp);

            return statement.GetTimestampFormatExpression(paramName, colName);
        }

        protected static readonly IDictionary<Type, IDictionary<string, IMergeStatement>> mergeCmdMap =
            new Dictionary<Type, IDictionary<string, IMergeStatement>>();
        public virtual IDictionary<string, IMergeStatement> GetMergeCommandText(Type type)
        {
            const string TimestampDBName = "{0}.TIMESTAMP";

            IDictionary<string, IMergeStatement> statements;
            lock (mergeCmdMap)
            {
                if (!mergeCmdMap.TryGetValue(type, out statements))
                {
                    statements = new Dictionary<string, IMergeStatement>();

                    var tables = type.PersistentTables().Values.SelectMany(x => x);
                    var props = type.PersistentProps();
                    var exprs = type.ReferenceExprs();
                    var refs = type.ReferenceProps();

                    foreach (var table in tables)
                    {
                        var sql = GetMergeStatement();
                        var tableName = GetTableName(table);

                        var tableCols = props.Concat(refs)
                            .Where(c => c.Key.StartsWith(tableName) && !exprs.ContainsKey(c.Key));
                        var lastModifiedCol = tableCols.FirstOrDefault(x => x.Value.IsLastModified);

                        var keys = tableCols.Select(c => c.Key).ToArray();

                        sql.MergeInto.Add(tableName + " " + tableName);

                        var key = table;
                        var cols = new List<string>();
                        sql.UsingOn.Add("(" + key + " = " + GetParamName(key) + ")");
                        if (keys.Length > 1)
                        {
                            for (int i = 1; i < keys.Length; i++)
                                cols.Add(keys[i]);
                            sql.UpdateSet.Add(string.Join(",\r\n", cols.Select(x => GetColName(x) + " = " + GetParamName(x))));
                            if (lastModifiedCol.Value != null)
                            {
                                sql.TimestampName = string.Format(TimestampDBName, tableName);
                                sql.UpdateWhere.Add(GetUpdateTimestampExpression(sql, lastModifiedCol.Key, sql.TimestampName));
                            }
                        }

                        cols.Clear();
                        foreach (var col in keys)
                            cols.Add(GetName(col));
                        sql.Insert.Add("(" + string.Join(",", cols.Select(x => GetColName(x))) + ")");
                        sql.Values.Add("(" + string.Join(",", cols.Select(x => GetParamName(x))) + ")");

                        statements.Add(tableName, sql);
                    }

                    mergeCmdMap.Add(type, statements);
                }
            }

            return statements;
        }

        protected static readonly IDictionary<Type, IDictionary<string, IDeleteStatement>> deleteCmdMap =
            new Dictionary<Type, IDictionary<string, IDeleteStatement>>();
        public virtual IDictionary<string, IDeleteStatement> GetDeleteCommandText(Type type)
        {
            IDictionary<string, IDeleteStatement> statements;
            lock (deleteCmdMap)
            {
                if (!deleteCmdMap.TryGetValue(type, out statements))
                {
                    statements = new Dictionary<string, IDeleteStatement>();

                    var tables = type.PersistentTables()
                        .Values.SelectMany(x => x);
                    foreach (string table in tables)
                    {
                        var sql = GetDeleteStatement();
                        var tableName = GetTableName(table);
                        sql.DeleteFrom.Add(tableName);
                        var key = table;
                        sql.Where.Add(key + " = " + GetParamName(key));

                        statements.Add(tableName, sql);
                    }

                    deleteCmdMap.Add(type, statements);
                }
            }

            return statements;
        }

        protected static readonly IDictionary<Type, IDictionary<string, ILockStatement>> lockCmdMap =
            new Dictionary<Type, IDictionary<string, ILockStatement>>();
        public virtual IDictionary<string, ILockStatement> GetLockCommandText(Type type)
        {
            IDictionary<string, ILockStatement> statements;
            lock (lockCmdMap)
            {
                if (!lockCmdMap.TryGetValue(type, out statements))
                {
                    var tableNames = type.PersistentTables()
                        .Values.SelectMany(x => x)
                        .Select(x => GetTableName(x))
                        .ToArray();

                    statements = new Dictionary<string, ILockStatement>();

                    foreach (var tableName in tableNames)
                    {
                        var sql = GetLockStatement();
                        sql.Table.Add(tableName);

                        statements.Add(tableName, sql);
                    }

                    lockCmdMap.Add(type, statements);
                }
            }

            return statements;
        }

        public string GetParameters(IEnumerable<KeyValuePair<string, object>> parameters)
        {
            if (parameters == null)
                return string.Empty;
            else
            {
                var values = new List<string>();
                foreach (var param in parameters)
                    values.Add(param.Key + " = " + (param.Value == null ? "NULL" : (param.Value.Equals(DBNull.Value) ? "DBNULL" : ParameterToString(param.Value))));

                return string.Join(", ", values);
            }
        }

        public override string ToString()
        {
            if (IsConnected)
                return string.Format("Connection {0}: Params: {1}, Connected: {2}, Transaction level: {3}",
                    Connection.GetHashCode(), ConnectionString, true, TransactionLevel);
            else
                return string.Format("Params: {0}, Connected: {1}, Transaction level: {2}",
                    ConnectionString, false, TransactionLevel);
        }

        #endregion
        #region Protected Abstract Methods

        protected abstract string ParameterToString(object value);

        #endregion
        #region Public Abstract Methods

        public abstract IDbCommand CreateCommand(string commandName, int pageNumber, short pageSize, string commandLabel = null);
        public abstract IDbCommand CreateCommand(string commandName, int arrayBindCount, string commandLabel = null);
        public abstract IDbCommand CreateCommand(string commandName, string commandLabel = null);

        public abstract IDbParameter CreateParameter(IDbCommand command);
        public abstract IDbParameter CreateParameter(IDbCommand command, string paramName, object paramValue, DbType dbType, ParameterDirection direction);
        public abstract IDbParameter CreateParameter(IDbCommand command, string name, object value);
        public abstract void CreateParameter(IDbCommand command, string name, int count, object[] value);

        public abstract QueryReader GetQueryReader(IDataReader reader);
        public abstract IDataReader ExecuteReader(IDbCommand command, CommandBehavior behavior);
        public abstract IDataReader ExecuteReader(string commandText, string commandName,
            IEnumerable<KeyValuePair<string, object>> parameters, int pageNumber, short pageSize, out long recordCount);
        public abstract object ExecuteScalar(IDbCommand command);
        public abstract int ExecuteNonQuery(IDbCommand command);
        public abstract void ExecuteDataSet(IDbCommand command, DataSet dataSet, string tableName);
        public abstract long GetSequenceNextValue(string name);
        public abstract long GetSequenceCurrentValue(string name);

        public abstract string GetCommandText(string commandText, string commandCols);
        public abstract string GetCountCommandText(string commandText);
        public abstract string GetPagedCommandText(string commandText, string commandCols, int pageNumber, short pageSize);
        public abstract string GetUnionCommandText(string commandText1, string commandText2);

        public abstract string GetSqlExpression(object value, Type type);
        public abstract string GetSqlConcatOperator();

        public abstract ISqlQueryBuilder GetSqlQueryBuilder();

        #endregion
        #region Public Methods

        #if (PERF)
        public ICommandLogger StartCommand(IDbCommand command)
        {
            if (!Debugger.IsAttached)
            {
                var commandParams = command.Parameters.Cast<IDataParameter>()
                    .Select(p => new CommandParameterLogger(p.ParameterName,
                        p.Value == null ? "NULL" : (p.Value.Equals(DBNull.Value) ? "DBNULL" : ParameterToString(p.Value))));

                return new CommandLogger(command.ToString(), command.CommandText, commandParams.ToArray());
            }

            return null;
        }

        public ICommandLogger StartCommand(string commandName)
        {
            if (!Debugger.IsAttached)
                return new CommandLogger(commandName);

            return null;
        }

        public void EndCommand(ICommandLogger commandLogger)
        {
            if (!Debugger.IsAttached && commandLogger != null)
            { 
                Log.Source[MANAGERCOMMANDLOGKEY].TraceData(commandLogger);
                commandLogger.Dispose();
            }
        }
        #else
        public ICommandLogger StartCommand(IDbCommand command)
        {
            return null;
        }

        public ICommandLogger StartCommand(string commandName)
        {
            return null;
        }

        public void EndCommand(ICommandLogger commandLogger)
        {
        }
        #endif

        public IDataReader ExecuteReader(IDbCommand command)
        {
            return ExecuteReader(command, CommandBehavior.SingleResult);
        }

        public int ExecuteNonQuery(string commandText, string commandName)
        {
            var command = CreateCommand(commandName);
            command.CommandText = commandText;
            return ExecuteNonQuery(command);
        }

        public abstract object Clone();

        #endregion
    }
}