﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Configuration;
using System.Configuration.Install;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NewHotel.Pos.Hub.Core;

namespace NewHotel.Pos.ServiceWindow
{
    public class PosServiceWindow : ServiceBase
    {
        #region Variables

        [ImportMany(typeof(IHubHost))]
        private readonly List<IHubHost> _hosts = new List<IHubHost>();
        private Task[] _tasks;
        private CancellationTokenSource _cts;
        private EventWaitHandle _waitHandle;
        private readonly TimeSpan _waitTime = TimeSpan.FromSeconds(30);

        private bool _debugMode;

        #endregion
        #region Constructors

        protected PosServiceWindow(string name)
        {
            AutoLog = true;
            ServiceName = name;
            Compose();
        }

        public PosServiceWindow()
        {
        }

        #endregion
        #region Properties

        public static string CloudLocation { get; set; }

        public bool IsRunning
        {
            get { return _tasks != null && _tasks.Any(x => x.Status == TaskStatus.Running || x.Status == TaskStatus.WaitingToRun); }
        }

        #endregion
        #region Methods

        public void Execute(string[] args)
        {
            _debugMode = false;
            if (args.Length > 0)
            {
                foreach (var arg in args)
                {
                    switch (arg.ToUpperInvariant())
                    {
                        case "/I":
                            InstallService();
                            return;
                        case "/U":
                            UninstallService();
                            return;
                        case "/D":
                            _debugMode = true;
                            break;
                    }
                }
            }

            try
            {
                if (_debugMode)
                {
                    var listener = new ConsoleTraceListener();
                    Trace.Listeners.Add(listener);
                    ConsoleRun(args);
                }
                else Run(this);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Console.WriteLine(ex.InnerException.Message + Environment.NewLine + ex.InnerException.StackTrace);
                else Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
            }
        }

        public void Start(string[] args)
        {
            try
            {
                _waitHandle = new EventWaitHandle(false, EventResetMode.ManualReset, ServiceName);
                CloudLocation = ConfigurationManager.AppSettings.Get("CloudLocation");

                var tasks = new List<Task>();

                _cts = new CancellationTokenSource();
                foreach (var host in _hosts)
                {
                    var task = new Task((arg) =>
                    {
                        int exitCode = 0;
                        try
                        {
                            _cts.Token.ThrowIfCancellationRequested();
                            var serviceHost = (IHubHost)arg;
                            try
                            {
                                exitCode = serviceHost.Run();
                            }
                            finally
                            {
                                host.Dispose();
                            }
                        }
                        catch (OperationCanceledException)
                        {
                            _waitHandle?.Set();
                        }
                        catch (Exception ex)
                        {
                            WriteException(ex);
                        }
                    }, host, _cts.Token);

                    tasks.Add(task);
                    task.Start();
                }

                _tasks = tasks.ToArray();
            }
            catch (Exception ex)
            {
                WriteException(ex);
            }
        }

        public new void Stop()
        {
            try
            {
                _waitHandle?.Set();
                try
                {
                    if (_tasks != null)
                    {
                        Trace.TraceInformation("Wait for task completion ...");
                        if (Task.WaitAll(_tasks, _waitTime))
                        {
                            for (int index = 0; index < _tasks.Length; index++)
                                _tasks[index].Dispose();
                        }
                        else
                            _cts.Cancel();

                        _tasks = null;
                        _cts = null;
                    }
                }
                finally
                {
                    _waitHandle?.Dispose();
                    _waitHandle = null;
                }
            }
            catch (Exception ex)
            {
                WriteException(ex);
            }
        }

        public void InstallService()
        {
            ManagedInstallerClass.InstallHelper(new string[] { Assembly.GetExecutingAssembly().Location });
        }

        public void UninstallService()
        {
            ManagedInstallerClass.InstallHelper(new string[] { "/u", Assembly.GetExecutingAssembly().Location });
        }

        protected void WriteException(Exception ex)
        {
            if (_debugMode)
            {
                Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
                if (ex.InnerException != null)
                    Console.WriteLine(ex.InnerException.Message + Environment.NewLine + ex.InnerException.StackTrace);
            }
            else
            {
                EventLog.WriteEntry(ex.Message + Environment.NewLine + ex.StackTrace, EventLogEntryType.Error);
                if (ex.InnerException != null)
                    EventLog.WriteEntry(ex.InnerException.Message + Environment.NewLine + ex.InnerException.StackTrace, EventLogEntryType.Error);
            }
        }

        private void ConsoleRun(string[] args)
        {
            Start(args);
            if (IsRunning)
            {
                try
                {
                    Console.WriteLine("{0} running. Press any key to exit ...", ServiceName);
                    Console.WriteLine();
                    Console.ReadKey(false);
                }
                finally
                {
                    Stop();
                }
            }
        }

        protected void Compose()
        {
            try
            {
                var container = new CompositionContainer(new DirectoryCatalog(".\\", "*.dll"));
                container.SatisfyImportsOnce(this);
            }
            catch (ReflectionTypeLoadException ex)
            {
                var exceptions = new StringBuilder();
                foreach (var loaderEx in ex.LoaderExceptions)
                    exceptions.AppendLine(loaderEx.Message);
                WriteException(new Exception(exceptions.ToString()));
            }
        }

        #endregion
    }
}
