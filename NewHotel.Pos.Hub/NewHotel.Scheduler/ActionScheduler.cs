﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Scheduler
{
	public interface IActionJob
	{
		Task<object> Execute(object state);

		Task<bool> MustRepeat(object state);
	}

	public interface IActionScheduler
	{
		void RepeatUntil<T>(object state, TimeSpan interval, bool startNow)
			where T : IActionJob;
	}

	public class ActionScheduler : IActionScheduler
	{
		private readonly Action<Func<Func<Type, object>, Task>> scopedAction;

		public ActionScheduler(Action<Func<Func<Type, object>, Task>> scopedAction)
		{
			this.scopedAction = scopedAction;
		}

		public void RepeatUntil<T>(object state, TimeSpan interval, bool startNow)
			where T : IActionJob
		{
			var job = new SchedulterJob(scopedAction)
			{
				StartNow = startNow,
				Interval = interval
			};

			Task.Run(async () =>
			{
				await job.Execute<T>(state);
			});
		}
	}

	public class SchedulterJob
	{
		private readonly Action<Func< Func<Type, object>, Task>> scopedAction;

		public SchedulterJob(Action<Func<Func<Type, object>, Task>> scopedAction)
		{
			this.scopedAction = scopedAction;
		}

		public bool StartNow { get; set; }
		public TimeSpan Interval { get; set; }

		async Task<(object, bool)> InternalExecute(Type type, object state)
		{
			object newState = null;
			bool repeat = false;

			scopedAction(async resolver =>
			{
				try
				{
					var ctors = type.GetConstructors();
					var ctor = ctors[0];
					System.Reflection.ParameterInfo[] pars = ctor.GetParameters();
					object[] parameters = new object[pars.Length];
					for (int i = 0; i < pars.Length; i++)
					{
						var par = pars[i];
						var parType = par.ParameterType;
						parameters[i] = resolver(parType);
					}
					var job = (IActionJob)Activator.CreateInstance(type, parameters);
					newState = await job.Execute(state);
					repeat = await job.MustRepeat(newState);
				}
				catch (Exception)
				{
					// No errors allowed here
				}
			});

			if (Interval != TimeSpan.Zero && repeat)
				await Task.Delay(Interval);

			return (newState, repeat);
		}

		public async Task Execute<T>(object state) where T : IActionJob
		{
			var type = typeof(T);
			bool repeat = true;
			object newState = state;

			if (!StartNow)
			{
				await Task.Delay(Interval);
			}

			while (repeat)
			{
				(newState, repeat) = await InternalExecute(type, newState);
			}
		}
	}
}
