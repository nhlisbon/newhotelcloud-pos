﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PEPE_PK")]
    public class ProfilePermissionsRecord : BaseRecord
    {
        [MappingColumn("PERF_PK", Nullable = true)]
        public Guid ProfileId { get; set; }
        [MappingColumn("PERM_DESC", Nullable = true)]
        public string PermissioDescription { get; set; }
        [MappingColumn("GRPR_DESC", Nullable = true)]
        public string GroupDescription { get; set; }
        [MappingColumn("SECU_CODE", Nullable = true)]
        public long SecurityCode { get; set; }
        [MappingColumn("PERM_PK", Nullable = true)]
        public Guid PermissionId { get; set; }
        [MappingColumn("APPL_PK", Nullable = true)]
        public long ApplicationId { get; set; }
    }
}