﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("LFOU_PK")]
    public class LostandFoundRecord : BaseRecord
    {
        [MappingColumn("LFOU_DARE", Nullable = true)]
        public DateTime Date { get; set; }
        [MappingColumn("LFOU_ITEM", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("LFOU_WHERE", Nullable = true)]
        public string Where { get; set; }
        [MappingColumn("LFOU_WHO", Nullable = true)]
        public string Who { get; set; }
        [MappingColumn("LFOU_RETU", Nullable = true)]
        public bool IsReturned { get; set; }
        [MappingColumn("LFOU_DISC", Nullable = true)]
        public bool IsDiscarded { get; set; }
    }
}