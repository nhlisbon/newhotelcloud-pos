﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("DOCF_PK")]
    public class OfficialDocExtAccountRecord : BaseRecord
    {
        // 0. Facturas, 1. Notas Crédito, 2. Facturas Pro-Forma, 3. Recibos, 4. Boletas
        [MappingColumn("DOCU_TIDO")]
        public short DocumentType { get; set; }
        [MappingColumn("TACO_PK")]
        [DataMember]
        public Guid ExtAccountId { get; set; }
        [DataMember]
        [MappingColumn("DOCU_FULL_SERIE")]
        public string DocumentFullSerie { get; set; }
        [MappingColumn("DOCF_DAEM")]
        [DataMember]
        public DateTime EmisionDateDate { get; set; }
        [MappingColumn("DOCF_HOEM")]
        [DataMember]
        public DateTime EmisionDateTime { get; set; }
        [MappingColumn("TACO_DARE")]
        [DataMember]
        public DateTime? RegularizationDateExtAccount { get; set; }
        [MappingColumn("UTIL_LOGIN")]
        [DataMember]
        public string UserDescription { get; set; }
        [MappingColumn("TACO_REGU")]
        [DataMember]
        public bool RegularizationExtAccount { get; set; }
        [MappingColumn("LIRE_DOCU")]
        [DataMember]
        public string ReseNumber { get; set; }
        [MappingColumn("LIRE_GUEST")]
        [DataMember]
        public string GuestName { get; set; }
        [MappingColumn("LIOC_VOUC")]
        [DataMember]
        public string Voucher { get; set; }
        [MappingColumn("DOFI_CONT")]
        [DataMember]
        public Decimal Total { get; set; }
        [MappingColumn("DOFI_COEX")]
        [DataMember]
        public Decimal ForeignTotal { get; set; }
        [MappingColumn("TACO_TPDF")]
        [DataMember]
        public short DaysOfExpired { get; set; }

        public DateTime PaymentDate
        {
            get { return EmisionDateDate.AddDays(DaysOfExpired); }
        }

        public bool DisableRegularization
        {
            get { return false; }
        }

        public string EmissionDate
        {
            get { return EmisionDateDate.ToMonthAndDay() + " - " + EmisionDateTime.ToHourAndMinute(); }
        }
    }
}