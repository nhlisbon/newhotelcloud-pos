﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("AVAR_PK")]
    public class MalfunctionRecord : BaseRecord
    {
        [MappingColumn("AVAR_DATA")]
        public DateTime RegistrationDate { get; set; }
        [MappingColumn("TIAV_DESC")]
        public string FailureType { get; set; }
        [MappingColumn("TIAV_PK")]
        public Guid FailureTypeId { get; set; }
        [MappingColumn("SUAV_DESC")]
        public string SubTypesFailure { get; set; }
        [MappingColumn("SUAV_PK", Nullable = true)]
        public Guid? SubTypesFailureId { get; set; }
        [MappingColumn("PROC_DESC")]
        public string FailureOrigin { get; set; }
        [MappingColumn("PROC_PK")]
        public Guid FailureOriginId { get; set; }
        [MappingColumn("ZONA_DESC")]
        public string Zone { get; set; }
        [MappingColumn("ZOCO_PK", Nullable = true)]
        public Guid? ZoneId { get; set; }
        [MappingColumn("AVAR_PRIO")]
        public PriorityType Priority { get; set; }
        [MappingColumn("AVAR_DESC")]
        public string Description { get; set; }
        [MappingColumn("AVAR_RESO")]
        public bool Solved { get; set; }
        [MappingColumn("AVAR_DARE", Nullable = true)]
        public DateTime? SolvedDate { get; set; }
        [MappingColumn("AVAR_SOLU")]
        public string DescriptionSolved { get; set; }
        [MappingColumn("AVAR_ANUL")]
        public bool Cancelled { get; set; }
        [MappingColumn("AVAR_EMPL")]
        public string Employee { get; set; }
        [MappingColumn("DAREFORMAT")]
        public string SolvedDateFormat { get; set; }
        [MappingColumn("ALOJ_ROOM")]
        public string RoomNumber { get; set; }
        [MappingColumn("ALOJ_PK", Nullable = true)]
        public Guid? RoomId { get; set; }
        

        
    }
}
