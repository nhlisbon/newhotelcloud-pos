﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TREC_PK")]
    public class AssignedRoomReservationRecord : BaseRecord
    {
        [MappingColumn("TREC_DESC", Nullable = true)]
        public string RoomTypeDescription { get; set; }
        [MappingColumn("ALOJ_PK", Nullable = true)]
        public Guid? RoomId { get; set; }
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        public string RoomNumber { get; set; }
        [MappingColumn("ALOJ_DESC", Nullable = true)]
        public string RoomDescription { get; set; }
        [MappingColumn("ALOJ_ROST")]
        public RoomStatus RoomStatus { get; set; }
        [MappingColumn("ROST_DESC")]
        public string RoomStatusDescription { get; set; }
        [MappingColumn("LIRE_DAEN", Nullable = true)]
        public DateTime? Arrival { get; set; }
        [MappingColumn("LIRE_DASA", Nullable = true)]
        public DateTime? Departure { get; set; }
        [MappingColumn("LIES_DAEN", Nullable = true)]
        public DateTime? InitialDate { get; set; }
        [MappingColumn("LIES_DASA", Nullable = true)]
        public DateTime? FinalDate { get; set; }
        [MappingColumn("LIRE_ESTA", Nullable = true)]
        public ReservationState? State { get; set; }
        [MappingColumn("ESTA_DESC", Nullable = true)]
        public string StateDescription { get; set; }
        [MappingColumn("LIRE_PK", Nullable = true)]
        public Guid? ReservationId { get; set; }
        [MappingColumn("LIRE_DESC", Nullable = true)]
        public string Reservation { get; set; }
        [MappingColumn("RESE_NUMBER", Nullable = true)]
        public string ReservationNumber { get; set; }
        [MappingColumn("RESE_ENTI", Nullable = true)]
        public string Entity { get; set; }
        [MappingColumn("RESE_HOLDER", Nullable = true)]
        public string Holder { get; set; }
        [MappingColumn("RESE_GROUP", Nullable = true)]
        public string Group { get; set; }
        [MappingColumn("RESE_COLO", Nullable = true)]
        public ARGBColor BackgroundColor { get; set; }
        [MappingColumn("LIRE_COLO", Nullable = true)]
        public ARGBColor? ResourceColor { get; set; }
        [MappingColumn("LIOC_OVER", Nullable = true)]
        public bool? Overbooking { get; set; }
        [MappingColumn("LIOC_PRCI", Nullable = true)]
        public bool? PreCheckin { get; set; }
        [MappingColumn("LIOC_DFGA", Nullable = true)]
        public DateTime? DaysGuaranteedForNoShow { get; set; }
        [MappingColumn("CCCO_PK", Nullable = true)]
        public Guid? AccountId { get; set; }
        [MappingColumn("RESE_PEND", Nullable = false)]
        public bool? PendingPayments { get; set; }
        [MappingColumn("LIOC_VOUC", Nullable = true)]
        public string Voucher { get; set; }
        [MappingColumn("ALLO_INCL", Nullable = true)]
        public bool AllotmentIncluded { get; set; }
        [MappingColumn("ALLO_PK", Nullable = true)]
        public Guid? AllotmentId { get; set; }
        [MappingColumn("RESE_DACR", Nullable = true)]
		public DateTime? CreationDate { get; set; }
        [MappingColumn("GRPR_COLO", Nullable = true)]
        public ARGBColor? GroupColor { get; set; }
        [MappingColumn("LIOC_ALBL", Nullable = true)]
        public bool? LockRoom { get; set; }
    }
}
