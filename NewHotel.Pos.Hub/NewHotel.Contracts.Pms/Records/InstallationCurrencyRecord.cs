﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("HUNMO_PK")]
    public class InstallationCurrencyRecord : BaseRecord
    {
        [MappingColumn("UNMO_PK", Nullable = true)]
        public string Code { get; set; }
        [MappingColumn("UNMO_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("BASE_CURR", Nullable = true)]
        public bool Default { get; set; }
        [MappingColumn("UNMO_NDEC", Nullable = true)]
        public int DecimalNumber { get; set; }
        [MappingColumn("UNMO_SYMB", Nullable = true)]
        public string CurrencySymbol { get; set; }
        [MappingColumn("CAMB_VAMB", Nullable = true)]
        public decimal? ExchangeRate { get; set; }
        [MappingColumn("CAMB_MULT", Nullable = true)]
        public bool ExchangeMultiOperator { get; set; }
        [MappingColumn("GETD_PREC", Nullable = true)]
        public int DecimalPlaces { get; set; }
        [MappingColumn("UNMO_CAJA")]
        public bool UsedInCashFlow { get; set; }
        [MappingColumn("UNMO_DEVO")]
        public bool UsedInRefunds { get; set; }
    }
}
