﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("COPO_PK")]
    public class PostalZoneRecord:BaseRecord
    {
        [MappingColumn("COPO_INIT",Nullable=true)]
        public string InitialCode{get;set;}
        [MappingColumn("COPO_FINA",Nullable=true)]
        public string FinalCode{get;set;}
        [MappingColumn("COPO_LOCA",Nullable=true)]
        public string Location{get;set;}
    }
}