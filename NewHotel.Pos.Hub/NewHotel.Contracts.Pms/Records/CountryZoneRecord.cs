﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("COMU_PK")]
    public class CountryZoneRecord:BaseRecord
    {
        [DataMember]
        [MappingColumn("COMU_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}