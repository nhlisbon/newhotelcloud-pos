﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TPDI_PK")]
    public class PriceRateRecord : BaseRecord
    {
        [MappingColumn("TPDI_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("TIPO_DESC", Nullable = true)]
        public string PriceType { get; set; }
        [MappingColumn("UNMO_PK", Nullable = true)]
        public string Currency { get; set; }
        [MappingColumn("CATP_DESC", Nullable = true)]
        public string Category { get; set; }
        [MappingColumn("SEME_DESC", Nullable = true)]
        public string Segment { get; set; }
        [MappingColumn("SEME_PK", Nullable = true)]
        public Guid? SegmentId { get; set; }
        [MappingColumn("PARE_DESC", Nullable = true)]
        public string Parent { get; set; }
        [MappingColumn("TPDI_DERV", Nullable = false)]
        public bool HasDerived { get; set; }
    }
}
