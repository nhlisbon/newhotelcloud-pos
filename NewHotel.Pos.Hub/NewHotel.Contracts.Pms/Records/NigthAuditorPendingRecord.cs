﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("LIRE_PK")]
    public class NigthAuditorPendingRecord : BaseRecord
    {
        [MappingColumn("RESE_NUMBER", Nullable = true)]
        public string ReseNumber { get; set; }
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        public string Room { get; set; }
        [MappingColumn("ABRE_CODO", Nullable = true)]
        public string RoomType { get; set; }
        [MappingColumn("RESE_PAXS", Nullable = true)]
        public string Paxs { get; set; }
        [MappingColumn("RESE_ESTA", Nullable = true)]
        public string Status { get; set; }
        [MappingColumn("LIOC_ESTA", Nullable = true)]
        public ReservationState StatusId { get; set; }
        [MappingColumn("RESE_ANPH", Nullable = true)]
        public string Guest { get; set; }
        [MappingColumn("RESE_DAEN", Nullable = true)]
        public string Arrival { get; set; }
        [MappingColumn("RESE_DASA", Nullable = true)]
        public string Departure { get; set; }
        [MappingColumn("ENTI_CODI", Nullable = true)]
        public string Company { get; set; }
        [MappingColumn("NACI_PK", Nullable = true)]
        public string Country { get; set; }
        [MappingColumn("RESE_OPER", Nullable = true)]
        public long? Operations { get; set; }
        [MappingColumn("RESE_ARRIVAL", Nullable = true)]
        public DateTime? ArrivalDate { get; set; }
        [MappingColumn("RESE_DEPARTURE", Nullable = true)]
        public DateTime? DepartureDate { get; set; }
        [MappingColumn("ARRIVAL_TIME", Nullable = true)]
        public DateTime? ArrivalTime { get; set; }
        [MappingColumn("DEPARTURE_TIME", Nullable = true)]
        public DateTime? DepartureTime { get; set; }

        public string ArrivalDateF { get { return (ArrivalDate.HasValue) ? ArrivalDate.Value.ToMonthAndDay() + " - " + ArrivalDate.Value.ToHourAndMinute() : ""; } }
        public string DepartureDateF { get { return (DepartureDate.HasValue) ? DepartureDate.Value.ToMonthAndDay() + " - " + DepartureDate.Value.ToHourAndMinute() : ""; } }
    }
}
