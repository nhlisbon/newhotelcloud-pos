﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [MappingQuery("PERM_PK")]
    public class GroupPermissionsRecord : BaseRecord
    {
         [DataMember]
         [MappingColumn("PERM_DESC", Nullable = true)]
         public string PermissionDescription { get; set; }
         [DataMember]
         [MappingColumn("GRPR_DESC", Nullable = true)]
         public string GroupDescription { get; set; }
         [DataMember]
         [MappingColumn("PERM_ORDE", Nullable = true)]
         public long PermissionOrder { get; set; }
         [DataMember]
         [MappingColumn("GRPR_ORDE", Nullable = true)]
         public long GroupOrder { get; set; }
         [DataMember]
         [MappingColumn("PEAP_PK", Nullable = true)]
         public Guid PermissionApplicationId { get; set; }
         [DataMember]
         public bool Active { get; set; }
    }
}