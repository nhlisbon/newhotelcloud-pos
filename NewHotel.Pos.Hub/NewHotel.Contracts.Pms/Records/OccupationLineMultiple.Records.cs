﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [MappingQuery("LIRE_PK")]
    public class OccupationLineMultipleRecord : BaseRecord
    {
        #region Members

        [DataMember]
        public string _room;
        [DataMember]
        public Guid? _roomId;
        [DataMember]
        public string _roomTypeOccupied;
        [DataMember]
        public Guid? _roomType;


        #endregion

        [MappingColumn("RESE_NUMBER", Nullable = false)]
        [DataMember]
        public string ReservationNumber { get; set; }
        [MappingColumn("RESE_ANPH", Nullable = true)]
        [DataMember]
        public string Guest { get; set; }
        [MappingColumn("NACI_PK", Nullable = true)]
        [DataMember]
        public string CountryCode { get; set; }
        [MappingColumn("RESE_DAEN", Nullable = true)]
        [DataMember]
        public string Arrival { get; set; }
        [MappingColumn("RESE_DASA", Nullable = true)]
        [DataMember]
        public string Departure { get; set; }
        [MappingColumn("ABRE_CODR", Nullable = true)]
        [DataMember]
        public string RoomTypeReserved { get; set; }
        [MappingColumn("ABRE_CODO", Nullable = true)]
        [IgnoreDataMember]
        public string RoomTypeOccupied { get { return _roomTypeOccupied; } set { Set<string>(ref _roomTypeOccupied, value, "RoomTypeOccupied"); } }
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        [IgnoreDataMember]
        public string Room { get { return _room; } set { Set<string>(ref _room, value, "Room"); } }
        [MappingColumn("MOPE_CODI", Nullable = true)]
        [DataMember]
        public string Pension { get; set; }
        [MappingColumn("RESE_PAXS", Nullable = true)]
        [DataMember]
        public string Paxs { get; set; }
        [MappingColumn("RESE_ESTA", Nullable = true)]
        [DataMember]
        public string StateDescription { get; set; }
        [MappingColumn("LIOC_ESTA", Nullable = true)]
        [DataMember]
        public ReservationState State { get; set; }
        [MappingColumn("ENTI_CODI", Nullable = true)]
        [DataMember]
        public string Company { get; set; }
        [MappingColumn("RESE_PREC", Nullable = true)]
        [DataMember]
        public string PriceRate { get; set; }
        [MappingColumn("GRUP_NAME", Nullable = true)]
        [DataMember]
        public string GroupName { get; set; }
        [MappingColumn("RESE_VOUC", Nullable = true)]
        [DataMember]
        public string Voucher { get; set; }
        [MappingColumn("DEPARTURE", Nullable = true)]
        [DataMember]
        public string DepartureFlight { get; set; }
        [MappingColumn("ARRIVAL", Nullable = true)]
        [DataMember]
        public string ArrivalFlight { get; set; }
        [MappingColumn("ROST_OCUP", Nullable = true)]
        [DataMember]
        public string RoomStatus { get; set; }
        [MappingColumn("RESE_DACR", Nullable = true)]
        [DataMember]
        public string CreationDate { get; set; }
        [MappingColumn("RESE_OBSE", Nullable = true)]
        [DataMember]
        public string Comments { get; set; }
        [MappingColumn("CCCO_PK", Nullable = true)]
        [DataMember]
        public Guid AccountId { get; set; }
        [MappingColumn("RESE_OPER", Nullable = true)]
        [DataMember]
        public long Operations { get; set; }
        [MappingColumn("LIRE_DAEN", Nullable = true)]
        [DataMember]
        public DateTime ArrivalDate { get; set; }
        [MappingColumn("LIRE_DASA", Nullable = true)]
        [DataMember]
        public DateTime DepartureDate { get; set; }
        [MappingColumn("TIAL_CODO", Nullable = true)]
        [DataMember]
        [IgnoreDataMember]
        public Guid? RoomType { get { return _roomType; } set { Set<Guid?>(ref _roomType, value, "RoomType"); } }
        [MappingColumn("RESE_GRUP", Nullable = false)]
        [DataMember]
        public bool IsGroup { get; set; }
        [MappingColumn("ALOJ_PK", Nullable = true)]
        [IgnoreDataMember]
        public Guid? RoomId { get { return _roomId; } set { Set<Guid?>(ref _roomId, value, "RoomId"); } }
        [MappingColumn("RESE_PEND")]
        [DataMember]
        public bool Balance { get; set; }

        [MappingColumn("TIAL_CODR")]
        [DataMember]
        public Guid RoomTypeReservedId { get; set; }
        [MappingColumn("MOPE_ID")]
        [DataMember]
        public short PensionMode { get; set; }
        [MappingColumn("LIRE_TRES")]
        [DataMember]
        public ReservationType ReservationType { get; set; }
        [MappingColumn("ENTI_PK", Nullable = true)]
        [DataMember]
        public Guid? CompanyId { get; set; }
        [MappingColumn("OPER_PK", Nullable = true)]
        [DataMember]
        public Guid? ContractId { get; set; }
        [MappingColumn("OPER_DESC", Nullable = true)]
        [DataMember]
        public string ContractDescription { get; set; }
        [MappingColumn("ALLO_PK", Nullable = true)]
        [DataMember]
        public Guid? AllotmentId { get; set; }
        [MappingColumn("ALLO_DESC", Nullable = true)]
        [DataMember]
        public string AllotmentDescription { get; set; }
        [MappingColumn("LIOC_LIFO", Nullable = true)]
        [DataMember]
        public bool FixRules { get; set; }
        [MappingColumn("ALLO_INCL", Nullable = true)]
        [DataMember]
        public bool AllotmentIncluded { get; set; }
        [MappingColumn("LIOC_TITA", Nullable = true)]
        [DataMember]
        public ReservationPaymentType? ReservationPaymentType { get; set; }
        [MappingColumn("LIOC_CRIT", Nullable = true)]
        [DataMember]
        public ReservationPriceCalculation? ReservationPriceCalculation { get; set; }
        [MappingColumn("TPDI_PK", Nullable = true)]
        [DataMember]
        public Guid? PriceRateId { get; set; }
        [MappingColumn("TPDI_DESC", Nullable = true)]
        [DataMember]
        public string PriceRateDescription { get; set; }
        [MappingColumn("UNMO_PK", Nullable = true)]
        [DataMember]
        public string Currency { get; set; }
        [MappingColumn("HOPP_VDME", Nullable = true)]
        [DataMember]
        public decimal? CurrencyValue { get; set; }
        [MappingColumn("HOPP_VADI", Nullable = true)]
        [DataMember]
        public decimal? CurrencyBaseValue { get; set; }
        [MappingColumn("TIDE_PK", Nullable = true)]
        [DataMember]
        public Guid? DiscountId { get; set; }
        [MappingColumn("HOPP_PDES", Nullable = true)]
        [DataMember]
        public decimal? DiscountPercent { get; set; }
        [MappingColumn("INDE_CODI", Nullable = true)]
        [DataMember]
        public DiscountMethod? ApplyTo { get; set; }
        [MappingColumn("INDE_CODA", Nullable = true)]
        [DataMember]
        public DiscountMethod? CalculatedFrom { get; set; }


        [MappingColumn("DAEN_DATE", Nullable = true)]
        [DataMember]
        public DateTime ArrivalDateDate { get; set; }
        [MappingColumn("HOEN_DATE", Nullable = true)]
        [DataMember]
        public DateTime ArrivalTime { get; set; }
        [MappingColumn("DASA_DATE", Nullable = true)]
        [DataMember]
        public DateTime DepartureDateDate { get; set; }
        [MappingColumn("HOSA_DATE", Nullable = true)]
        [DataMember]
        public DateTime DepartureTime { get; set; }

        public string ArrivalF { get { return ArrivalDateDate.ToMonthAndDay() + " - " + ArrivalTime.ToHourAndMinute(); } }
        public string DepartureF { get { return DepartureDateDate.ToMonthAndDay() + " - " + DepartureTime.ToHourAndMinute(); } }

    }
}
