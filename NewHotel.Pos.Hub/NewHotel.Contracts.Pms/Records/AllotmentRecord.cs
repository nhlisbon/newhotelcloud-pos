﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ALLO_PK")]
    public class AllotmentRecord : BaseRecord
    {
        [MappingColumn("ALLO_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [MappingColumn("ALLO_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("ALLO_ALAR", Nullable = true)]
        public object Alarm { get; set; }
        [MappingColumn("ALLO_GARA", Nullable = true)]
        public bool AllotmentInGuarantee { get; set; }
        [MappingColumn("ALLO_AFCH", Nullable = true)]
        public bool AffectBookingChannels { get; set; }
        [MappingColumn("TIPO_DESC", Nullable = true)]
        public string AllotmentType { get; set; }
        [MappingColumn("ALLO_PALA", Nullable = true)]
        public object AlarmPercent { get; set; }
    }
}
