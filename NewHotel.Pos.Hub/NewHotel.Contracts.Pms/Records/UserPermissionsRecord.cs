﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PEUT_PK")]
    public class UserPermissionsRecord : BaseRecord
    {
        [MappingColumn("PERF_PK", Nullable = true)]
        public Guid ProfileId { get; set; }
        [MappingColumn("PERM_DESC", Nullable = true)]
        public string PermissionDescription { get; set; }
        [MappingColumn("GRPR_DESC", Nullable = true)]
        public string GroupDescription { get; set; }
        [MappingColumn("GRPR_ORDE", Nullable = true)]
        public string GroupOrder { get; set; }
        [MappingColumn("PERM_ORDE", Nullable = true)]
        public string PermissionOrder { get; set; }
        [MappingColumn("SECU_DESC", Nullable = true)]
        public string SecurityCodeDescription { get; set; }
        [MappingColumn("SECU_CODE", Nullable = true)]
        public long SecurityCode { get; set; }
    }
}