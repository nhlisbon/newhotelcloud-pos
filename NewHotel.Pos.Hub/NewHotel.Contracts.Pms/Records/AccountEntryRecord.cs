﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class AccountEntryRecord : BaseEntryRecord
    {
        [MappingColumn("TICO_DESC", Nullable = true)]
        public string Account { get; set; }
        [MappingColumn("MOVI_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("TOTAL", Nullable = true)]
        public decimal? Amount { get; set; }
        [MappingColumn("VALOR", Nullable = true)]
        public decimal? Gross { get; set; }
        [MappingColumn("DISCOUNT", Nullable = true)]
        public string Discount { get; set; }
        [MappingColumn("UNMO_PK", Nullable = true)]
        public string Currency { get; set; }
        [MappingColumn("MOVI_CCCO", Nullable = true)]
        public string TransferFrom { get; set; }
        [MappingColumn("MOVI_GRUP", Nullable = false)]
        public bool Totalized { get; set; }
    }
}
