﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("LRST_PK")]
    public class RoomStatusLogRecord: BaseRecord
    {
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        public string Room { get; set; }
        [MappingColumn("LRST_DATA", Nullable = true)]
        public DateTime Date { get; set; }
        [MappingColumn("LRST_HORA", Nullable = true)]
        public string Time { get; set; }
        [MappingColumn("ALOJ_ROST", Nullable = true)]
        public RoomStatus Status { get; set; }
        [MappingColumn("ROST_DESC", Nullable = true)]
        public string StatusDescription { get; set; }
        [MappingColumn("ALOJ_ORDE")]
        public long RoomOrder { get; set; }
        [MappingColumn("OLD_ROST_DESC", Nullable = true)]
        public string OldStatus { get; set; }
        [MappingColumn("NEW_ROST_DESC", Nullable = true)]
        public string NewStatus { get; set; }
        [MappingColumn("UTIL_DESC", Nullable = true)]
        public string User { get; set; }
    }
}