﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("DORE_PK")]
    public class ReservationDocumentRecord : BaseRecord
    {
        [MappingColumn("LIRE_PK", Nullable = true)]
        public Guid ReservationId { get; set; }
        [MappingColumn("LIRE_DESC", Nullable = true)]
        public string ReservationNumber { get; set; }
        [MappingColumn("FILE_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("FILE_NAME", Nullable = true)]
        public string Filename { get; set; }
        [MappingColumn("FILE_LMOD", Nullable = true)]
        public DateTime Date { get; set; }
    }
}
