﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("KCFG_PK")]
    public class KeyPermissionsRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("KCFG_DESC")]
        public string Description { get; set; }
        [MappingColumn("KCFG_INCL")]
        [IgnoreDataMember]
        public bool IncludeDefault { get; set; }
        [DataMember]
        [MappingColumn("HOTE_PK")]
        public Guid Installation { get; set; }
    }
}
