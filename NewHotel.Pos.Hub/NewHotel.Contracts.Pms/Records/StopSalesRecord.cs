﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("BEOT_PK")]
    public class StopSalesRecord : BaseRecord
    {

        [MappingColumn("BEOT_REST", Nullable = true)]
        public int? RestrictionId { get; set; }
        [MappingColumn("BEOT_REST_DESC", Nullable = true)]
        public string Restriction { get; set; }
        [MappingColumn("BEOT_TIPO", Nullable = true)]
        public int RestrictionTypeId { get; set; }
        [MappingColumn("BEOT_TIPO_DESC", Nullable = true)]
        public string RestrictionType { get; set; }
        [MappingColumn("ENTI_PK", Nullable = true)]
        public Guid? EntityId { get; set; }
        [MappingColumn("ENTI_DESC", Nullable = true)]
        public string Entity { get; set; }
        [MappingColumn("OPER_DESC", Nullable = true)]
        public string Operator { get; set; }
        [MappingColumn("OPER_ABRE", Nullable = true)]
        public string OperatorAbre { get; set; }
        [MappingColumn("TIAL_PK", Nullable = true)]
        public Guid? RoomTypeId { get; set; }
        [MappingColumn("TIAL_ABRE", Nullable = true)]
        public string RoomType { get; set; }
        [MappingColumn("MOPE_PK", Nullable = true)]
        public int? PensionModeId { get; set; }
        [MappingColumn("MOPE_DESC", Nullable = true)]
        public string PensionMode { get; set; }
        [MappingColumn("ORME_PK", Nullable = true)]
        public Guid? MarketOriginId { get; set; }
        [MappingColumn("ORME_DESC", Nullable = true)]
        public string MarketOrigin { get; set; }
        [MappingColumn("BEOT_DAIN", Nullable = true)]
        public DateTime InitialDate { get; set; }
        [MappingColumn("BEOT_DAFI", Nullable = true)]
        public DateTime FinalDate { get; set; }
        [MappingColumn("BEOT_NMAX", Nullable = true)]
        public int? Max { get; set; }
        [MappingColumn("BEOT_NMIN", Nullable = true)]
        public int? Min { get; set; }
    }
}
