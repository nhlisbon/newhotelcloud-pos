﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("DIFE_PK")]
    public class HolidaysRecord:BaseRecord
    {
        [MappingColumn("DIFE_DATE", Nullable = true)]
        public string Date { get; set; }
        [MappingColumn("DIFE_MOTI", Nullable = true)]
        public string Event { get; set; }
        [MappingColumn("DIFE_COLO", Nullable = true)]
        public uint Color { get; set; }
    }
}