﻿using System;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
	[MappingQuery("DOCF_PK")]
	public class RpsInvoiceRecord : BaseRecord
	{
		[MappingColumn("DOCF_FACT")]
		public string Invoice { get; set; }
		[MappingColumn("DOCF_DESC")]
		public string Holder { get; set; }
		[MappingColumn("DOCF_NACI")]
		public string Country { get; set; }
		[MappingColumn("DOCF_DATA")]
		public DateTime EmissionDate { get; set; }
		[MappingColumn("DOCF_VALO")]
		public decimal? Value { get; set; }
		[MappingColumn("UTIL_DESC")]
		public string UserName { get; set; }
		[MappingColumn("DOCF_ESTA")]
		public ServiceFiscalNoteLoteStatus? Status { get; set; }
		[MappingColumn("DOCF_DAES")]
		public DateTime? StatusDate { get; set; }
        [MappingColumn("DOCF_NFST")]
        public ServiceFiscalNoteLoteStatus? FiscalNoteStatus { get; set; }
        [MappingColumn("DOCF_NFDR")]
        public DateTime? FiscalNoteStatusDate { get; set; }
        [MappingColumn("DOCF_MESG")]
        public string ServiceFiscalNoteStatusMessage { get; set; }
        [MappingColumn("DOCF_NFME")]
        public string FiscalNoteStatusMessage { get; set; }
        [MappingColumn("EMAIL_ADDR")]
        public string HolderEmail { get; set; }
        [MappingColumn("RPSE_NSRQ")]
        public string Xml { get; set; }
		[MappingColumn("DOCF_NRPS")]
		public long? Number { get; set; }
        [MappingColumn("DOCF_NNFE")]
        public long? FiscalNoteNumber { get; set; }
		[MappingColumn("DOCF_NFSE", Nullable = true)]
		public long? NFSe { get; set; }

        public string StatusMessage
        {
            get
            {
                var sb = new StringBuilder();

                if (Status.HasValue)
                {
                    sb.Append("NFS-e:");
                    if (StatusDate.HasValue)
                        sb.Append(" " + StatusDate.Value.ToString());

                    if (Status.Value == ServiceFiscalNoteLoteStatus.Canceled)
                        sb.Append(" (Cancelada)");

                    if (!string.IsNullOrEmpty(ServiceFiscalNoteStatusMessage))
                        sb.Append(" " + ServiceFiscalNoteStatusMessage);
                }

                if (FiscalNoteStatus.HasValue)
                {
                    if (sb.Length > 0)
                        sb.AppendLine();

                    sb.Append("NF-e:");
                    if (FiscalNoteStatusDate.HasValue)
                        sb.Append(" " + FiscalNoteStatusDate.Value.ToString());

                    if (FiscalNoteStatus.Value == ServiceFiscalNoteLoteStatus.Canceled)
                        sb.Append(" (Cancelada)");

                    if (!string.IsNullOrEmpty(FiscalNoteStatusMessage))
                        sb.Append(" " + FiscalNoteStatusMessage);
                }

                return sb.ToString();
            }
        }
    }
}