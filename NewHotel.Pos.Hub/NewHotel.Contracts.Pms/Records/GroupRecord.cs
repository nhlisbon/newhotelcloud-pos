﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("GRPR_PK")]
    public class GroupRecord : BaseRecord
    {
        [MappingColumn("GRPR_DESC", Nullable = true)]
        public string GroupDescription { get; set; }
        [MappingColumn("GRPR_ORDE", Nullable = true)]
        public string GroupOrder { get; set; }
    }
}