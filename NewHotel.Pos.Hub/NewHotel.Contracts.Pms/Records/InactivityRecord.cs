﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("INAL_PK")]
    public class InactivityRecord : BaseRecord
    {
        [MappingColumn("INAL_DAIN", Nullable = true)]
        public DateTime Initial { get; set; }
        [MappingColumn("INAL_DAFI", Nullable = true)]
        public DateTime Final { get; set; }
        [MappingColumn("INAL_TIPO", Nullable = true)]
        public object Type { get; set; }
        [MappingColumn("INAL_AFBO", Nullable = true)]
        public object AffectsBooking { get; set; }
    }
}