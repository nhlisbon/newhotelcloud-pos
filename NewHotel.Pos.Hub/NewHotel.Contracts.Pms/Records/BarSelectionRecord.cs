﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("BARES_PK")]
    public class BarSelectionRecord : BaseRecord
    {
        [MappingColumn("BARES_ORDE", Nullable = false)]
        public short Orden { get; set; }
        [MappingColumn("TPDI_PK", Nullable = false)]
        public Guid TaxRate { get; set; }
        [MappingColumn("TPDI_DESC", Nullable = false)]
        public string TaxRateTranslation { get; set; }
        [MappingColumn("BARES_OVER", Nullable = true)]
        public int? BarPercentToApply { get; set; }
        [MappingColumn("BARES_USING", Nullable = false)]
        public string BarPriceCalculationMethod { get; set; }
    }
}