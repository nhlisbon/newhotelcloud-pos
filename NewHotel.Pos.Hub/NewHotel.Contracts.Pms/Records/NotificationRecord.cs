﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TPTP_PK")]
    public class NotificationRecord : BaseRecord<short>
    {
        [MappingColumn("TPTP_TYPE", Nullable = false)]
        public string Type { get; set; }
        [MappingColumn("TPTP_DESC", Nullable = false)]
        public string GroupDescription { get; set; }
        [MappingColumn("TPLT_PK", Nullable = false)]
        public Guid TemplateId { get; set; }
        [MappingColumn("TPLT_NAME", Nullable = true)]
        public string Name { get; set; }
        [MappingColumn("TPLT_LANG", Nullable = true)]
        public string Language { get; set; }
        [MappingColumn("LICL_DESC", Nullable = true)]
        public string LanguageDescription { get; set; }
    }

    [MappingQuery("TPTP_PK")]
    public class NotificationExtendedRecord : BaseRecord
    {
        [MappingColumn("TPTP_GROUP", Nullable = false)]
        public string Group { get; set; }
        [MappingColumn("TPTP_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("TPLT_SUBJ", Nullable = true)]
        public string Subject { get; set; }
        [MappingColumn("TPLT_NAME", Nullable = true)]
        public string Name { get; set; }
        [MappingColumn("TPLT_PK", Nullable = false)]
        public Guid TemplateId { get; set; }
        [MappingColumn("TPLT_LANG", Nullable = true)]
        public long Language { get; set; }
        [MappingColumn("LICL_DESC", Nullable = true)]
        public string LanguageDescription { get; set; }
        [MappingColumn("TPLT_HTML", Nullable = true)]
        public bool IsHtml { get; set; }
    }
}