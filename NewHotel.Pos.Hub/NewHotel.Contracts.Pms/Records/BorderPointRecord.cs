﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("POFR_PK")]
    public class BorderPointRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("POFR_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}