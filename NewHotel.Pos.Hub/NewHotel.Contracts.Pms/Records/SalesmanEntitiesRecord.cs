﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("SAEN_PK")]
    public class SalesmanEntitiesRecord : BaseRecord
    {
        [MappingColumn("SALE_NAME", Nullable = true)]
        public String Salesman { get; set; }
        [MappingColumn("ENTI_DESC", Nullable = true)]
        public string CompanyName { get; set; }
    }
}
