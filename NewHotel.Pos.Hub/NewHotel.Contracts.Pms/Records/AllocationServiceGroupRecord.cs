﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("GASS_PK")]
    public class AllocationServiceGroupRecord : BaseRecord
    {
        [MappingColumn("GASS_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}