﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("EMPR_PK")]
    public class EnterpriseRecord : BaseRecord
    {
        [MappingColumn("BENTI_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("BENTI_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [MappingColumn("LICL_PK", Nullable = true)]
        public string Language { get; set; }
    }
}