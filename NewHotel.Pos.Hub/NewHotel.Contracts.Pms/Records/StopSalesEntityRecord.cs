﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ENTI_PK")]
    public class StopSalesEntityRecord : BaseRecord
    {
        [MappingColumn("OPER_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [MappingColumn("ENTI_NOCO", Nullable = true)]
        public string CompanyName { get; set; }
        [MappingColumn("OPER_DESC", Nullable = true)]
        public string FiscalName { get; set; }
        [MappingColumn("OPER_PK", Nullable = true)]
        public Guid? FiscalNameId { get; set; }
    }

    [MappingQuery("REST_PK")]
    public class RestrictionRecord : BaseRecord
    {
        [MappingColumn("REST_DATE", Nullable = false)]
        public DateTime Date { get; set; }
        [MappingColumn("TYPE_DESC", Nullable = false)]
        public string Level { get; set; }
    }
}
