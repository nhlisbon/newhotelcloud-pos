﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class DailyPriceRecord : BaseRecord
    {
        [DataMember]
        public PriceRateInfoContract Day0 { get; set; }
        [DataMember]
        public PriceRateInfoContract Day1 { get; set; }
        [DataMember]
        public PriceRateInfoContract Day2 { get; set; }
        [DataMember]
        public PriceRateInfoContract Day3 { get; set; }
        [DataMember]
        public PriceRateInfoContract Day4 { get; set; }
        [DataMember]
        public PriceRateInfoContract Day5 { get; set; }
        [DataMember]
        public PriceRateInfoContract Day6 { get; set; }
    }
}
