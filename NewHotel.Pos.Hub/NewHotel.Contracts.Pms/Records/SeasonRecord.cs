﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("SEAS_PK")]
    public class SeasonRecord : BaseRecord
    {
        [MappingColumn("SEAS_MIN", Nullable = true)]
        public int Min { get; set; }
        [MappingColumn("SEAS_MAX", Nullable = true)]
        public int Max { get; set; }
        [MappingColumn("SEAS_AVG", Nullable = true)]
        public decimal Average { get; set; }
        [MappingColumn("SEAS_COLO", Nullable = true)]
        public ARGBColor Color { get; set; }
        [MappingColumn("SEAS_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
