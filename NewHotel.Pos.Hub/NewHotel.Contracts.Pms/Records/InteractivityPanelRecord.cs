﻿using System;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("LIRE_PK")]
    public class InteractivityPanelRecord : BaseRecord
    {
        #region Members

        [DataMember]
        public string _room;
        [DataMember]
        public Guid? _roomId;
        [DataMember]
        public string _roomTypeOccupied;
        [DataMember]
        public Guid? _roomType;

        #endregion

        [MappingColumn("RESE_NUMBER", Nullable = false)]
        [DataMember]
        public string ReservationNumber { get; set; }
        [MappingColumn("RESE_ANPH", Nullable = true)]
        [DataMember]
        public string Guest { get; set; }
        [MappingColumn("NACI_PK", Nullable = true)]
        [DataMember]
        public string CountryCode { get; set; }
        [MappingColumn("RESE_DAEN", Nullable = true)]
        [DataMember]
        public string Arrival { get; set; }
        [MappingColumn("RESE_DASA", Nullable = true)]
        [DataMember]
        public string Departure { get; set; }
        [MappingColumn("ABRE_CODR", Nullable = true)]
        [DataMember]
        public string RoomTypeReserved { get; set; }
        [MappingColumn("ABRE_CODO", Nullable = true)]
        [IgnoreDataMember]
        public string RoomTypeOccupied { get { return _roomTypeOccupied; } set { Set(ref _roomTypeOccupied, value, "RoomTypeOccupied"); } }
        [MappingColumn("ALOJ_VIRT", Nullable = true)]
        public bool? VirtualRoom { get; set; }
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        [IgnoreDataMember]
        public string Room { get { return _room; } set { Set(ref _room, value, "Room"); } }
        [MappingColumn("RESE_ESTA", Nullable = true)]
        [DataMember]
        public string StateDescription { get; set; }
        [MappingColumn("LIOC_ESTA", Nullable = true)]
        [DataMember]
        public ReservationState State { get; set; }
        [MappingColumn("RESE_VOUC", Nullable = true)]
        [DataMember]
        public string Voucher { get; set; }
        [MappingColumn("ROST_OCUP", Nullable = true)]
        [DataMember]
        public string RoomStatus { get; set; }
        [MappingColumn("RESE_DACR", Nullable = true)]
        [DataMember]
        public string CreationDate { get; set; }
        [MappingColumn("RESE_OBSE", Nullable = true)]
        [DataMember]
        public string Comments { get; set; }
        [MappingColumn("CCCO_PK", Nullable = true)]
        [DataMember]
        public Guid AccountId { get; set; }
        [MappingColumn("CCCO_BLOC", Nullable = true)]
        [DataMember]
        public bool IsLock { get; set; }
        [MappingColumn("RESE_OPER", Nullable = true)]
        [DataMember]
        public long Operations { get; set; }
        [MappingColumn("LIRE_DAEN", Nullable = true)]
        [DataMember]
        public DateTime ArrivalDate { get; set; }
        [MappingColumn("LIRE_DASA", Nullable = true)]
        [DataMember]
        public DateTime DepartureDate { get; set; }
        [MappingColumn("TIAL_CODO", Nullable = true)]
        [DataMember]
        [IgnoreDataMember]
        public Guid? RoomType { get { return _roomType; } set { Set(ref _roomType, value, "RoomType"); } }
        [MappingColumn("RESE_GRUP", Nullable = false)]
        [DataMember]
        public bool IsGroup { get; set; }
        [MappingColumn("ALOJ_PK", Nullable = true)]
        [IgnoreDataMember]
        public Guid? RoomId { get { return _roomId; } set { Set(ref _roomId, value, "RoomId"); } }
        [MappingColumn("GUEST_LIST")]
        [DataMember]
        public string GuestList { get; set; }
        [MappingColumn("BIRTH_DATE")]
        [DataMember]
        public bool HasBirthDay { get; set; }
        [MappingColumn("DAEN_DATE")]
        [DataMember]
        public DateTime ArrivalDateDate { get; set; }
        [MappingColumn("HOEN_DATE")]
        [DataMember]
        public DateTime ArrivalDateTime { get; set; }
        [MappingColumn("DASA_DATE")]
        [DataMember]
        public DateTime DepartureDateDate { get; set; }
        [MappingColumn("HOSA_DATE")]
        [DataMember]
        public DateTime DepartureDateTime { get; set; }
        [MappingColumn("ALOJ_ROST", Nullable = true)]
        [DataMember]
        public RoomStatus? RoomState { get; set; }
        [MappingColumn("LIRE_HAPP")]
        [DataMember]
        public short GuestHappiness { get; set; }

        public bool HasRoomStatus { get { return RoomState.HasValue; } }
        public string ArrivalF { get { return ArrivalDateDate.ToMonthAndDay() + " - " + ArrivalDateTime.ToHourAndMinute(); } }
        public string DepartureF { get { return DepartureDateDate.ToMonthAndDay() + " - " + DepartureDateTime.ToHourAndMinute(); } }

        public bool Selected { get; set; }
    }
}