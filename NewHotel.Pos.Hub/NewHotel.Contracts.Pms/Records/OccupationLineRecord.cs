﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [MappingQuery("LIRE_PK")]
    public class OccupationLineRecord : BaseRecord
    {
        #region Members

        [DataMember]
        public string _room;
        [DataMember]
        public Guid? _roomId;
        [DataMember]
        public string _roomTypeOccupied;
        [DataMember]
        public Guid? _roomType;
        

        #endregion

        [MappingColumn("RESE_NUMBER", Nullable = false)]
        [DataMember]
        public string ReservationNumber { get; set; }
        [MappingColumn("RESE_ANPH", Nullable = true)]
        [DataMember]
        public string Guest { get; set; }
        [MappingColumn("NACI_PK", Nullable = true)]
        [DataMember]
        public string CountryCode { get; set; }
        [MappingColumn("RESE_DAEN", Nullable = true)]
        [DataMember]
        public string Arrival { get; set; }
        [MappingColumn("RESE_DASA", Nullable = true)]
        [DataMember]
        public string Departure { get; set; }
        [MappingColumn("ABRE_CODR", Nullable = true)]
        [DataMember]
        public string RoomTypeReserved { get; set; }
        [MappingColumn("ABRE_CODO", Nullable = true)]
        [IgnoreDataMember]
        public string RoomTypeOccupied { get { return _roomTypeOccupied; } set { Set<string>(ref _roomTypeOccupied, value, "RoomTypeOccupied"); } }
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        [IgnoreDataMember]
        public string Room { get { return _room; } set { Set<string>(ref _room, value, "Room"); } }
        [MappingColumn("MOPE_CODI", Nullable = true)]
        [DataMember]
        public string Pension { get; set; }
        [MappingColumn("RESE_PAXS", Nullable = true)]
        [DataMember]
        public string Paxs { get; set; }
        [MappingColumn("RESE_ESTA", Nullable = true)]
        [DataMember]
        public string StateDescription { get; set; }
        [MappingColumn("LIOC_ESTA", Nullable = true)]
        [DataMember]
        public ReservationState State { get; set; }
        [MappingColumn("ENTI_CODI", Nullable = true)]
        [DataMember]
        public string Company { get; set; }
        [MappingColumn("RESE_PREC", Nullable = true)]
        [DataMember]
        public string PriceRate { get; set; }
        [MappingColumn("GRUP_NAME", Nullable = true)]
        [DataMember]
        public string GroupName { get; set; }
        [MappingColumn("RESE_VOUC", Nullable = true)]
        [DataMember]
        public string Voucher { get; set; }
        [MappingColumn("DEPARTURE", Nullable = true)]
        [DataMember]
        public string DepartureFlight { get; set; }
        [MappingColumn("ARRIVAL", Nullable = true)]
        [DataMember]
        public string ArrivalFlight { get; set; }
        [MappingColumn("ROST_OCUP", Nullable = true)]
        [DataMember]
        public string RoomStatus { get; set; }
        [MappingColumn("RESE_DACR", Nullable = true)]
        [DataMember]
        public string CreationDate { get; set; }
        [MappingColumn("RESE_OBSE", Nullable = true)]
        [DataMember]
        public string Comments { get; set; }
        [MappingColumn("CCCO_PK", Nullable = true)]
        [DataMember]
        public Guid? AccountId { get; set; }
        [MappingColumn("CCCO_BLOC", Nullable = true)]
        [DataMember]
        public bool IsLock { get; set; }
        [MappingColumn("RESE_OPER", Nullable = true)]
        [DataMember]
        public long Operations { get; set; }
        [MappingColumn("LIRE_DAEN", Nullable = true)]
        [DataMember]
        public DateTime ArrivalDate { get; set; }
        [MappingColumn("LIRE_DASA", Nullable = true)]
        [DataMember]
        public DateTime DepartureDate { get; set; }
        [MappingColumn("TIAL_CODO", Nullable = true)]
        [DataMember]
        [IgnoreDataMember]
        public Guid? RoomType { get { return _roomType; } set { Set<Guid?>(ref _roomType, value, "RoomType"); } }
        [MappingColumn("RESE_GRUP", Nullable = false)]
        [DataMember]
        public bool IsGroup { get; set; }
        [MappingColumn("ALOJ_PK", Nullable = true)]
        [IgnoreDataMember]
        public Guid? RoomId { get { return _roomId; } set { Set<Guid?>(ref _roomId, value, "RoomId"); } }
        [MappingColumn("SALDO", Nullable = true)]
        [DataMember]
        public decimal? Balance { get; set; }
    }
}
