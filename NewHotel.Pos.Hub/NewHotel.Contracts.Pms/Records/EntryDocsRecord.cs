﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("DOCU_PK")]
    public class EntryDocsRecord : BaseRecord
    {
        [MappingColumn("DOCU_SERIE")]
        public string DocumentNumber { get; set; }
        [MappingColumn("DOCU_DAEM")]
        public string EmitionDate { get; set; }
        [MappingColumn("DOCU_TIDO")]
        public string DocumentTypeDescription { get; set; }
        [MappingColumn("UNMO_PK")]
        public string Currency { get; set; }
        [MappingColumn("DOCU_VALO")]
        public decimal Total { get; set; }
        [MappingColumn("DOCU_ANUL")]
        public bool Cancelled { get; set; }
        [MappingColumn("DOCF_DAEM")]
        public DateTime EmisionDateDate { get; set; }
        [MappingColumn("DOCF_HOEM")]
        public DateTime EmisionDateTime { get; set; }

        public string EmitionDateF { get { return EmisionDateDate.ToMonthAndDay() + " - " + EmisionDateTime.ToHourAndMinute(); } }
    }
}