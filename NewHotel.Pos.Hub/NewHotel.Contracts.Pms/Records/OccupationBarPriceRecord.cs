﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("OBPR_PK")]
    public class OccupationBarPriceRecord : BaseRecord
    {
        [MappingColumn("PREC_DATE", Nullable = false)]
        public DateTime Date { get; set; }

        [MappingColumn("BAR_PRICE", Nullable = false)]
        public decimal Price { get; set; }

        [MappingColumn("ROOMSAVAILABLE", Nullable = false)]
        public short RoomsAvailables { get; set; }

        [MappingColumn("INVENTORYTOTAL", Nullable = false)]
        public short TotalInventory { get; set; }

        [MappingColumn("TREC_PK", Nullable = false)]
        public Guid RoomTypeId { get; set; }
    }
}
