﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("HEMPL_PK")]
    public class TechnicianRecord : BaseRecord
    {
        [MappingColumn("EMPL_ROLE", Nullable = true)]
        public string Role { get; set; }
        [MappingColumn("CONT_TITU", Nullable = true)]
        public string Title { get; set; }
        [MappingColumn("FULL_NAME", Nullable = true)]
        public string FullName { get; set; }
        [MappingColumn("CONT_SEXO", Nullable = true)]
        public string Gender { get; set; }
        [MappingColumn("CONT_NAME", Nullable = true)]
        public string Name { get; set; }
        [MappingColumn("CONT_APEL", Nullable = true)]
        public string LastName { get; set; }
        [MappingColumn("CONT_APE2", Nullable = true)]
        public string MiddleName { get; set; }
        [MappingColumn("CONT_NACI", Nullable = true)]
        public string Country { get; set; }
        [MappingColumn("CONT_DANA", Nullable = true)]
        public DateTime? Birthday { get; set; }
        [MappingColumn("CONT_CVIL", Nullable = true)]
        public string MaritalStatus { get; set; }
        [MappingColumn("CONT_PROF", Nullable = true)]
        public string Profession { get; set; }
    }
}