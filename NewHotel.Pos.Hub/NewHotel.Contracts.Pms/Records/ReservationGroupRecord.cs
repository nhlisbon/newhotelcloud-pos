﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("RESE_PK")]
    public class ReservationGroupRecord : BaseRecord
    {
        [MappingColumn("RESE_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("RESE_IDEN", Nullable = true)]
        public string Identifier { get; set; }
        [MappingColumn("TIGR_PK", Nullable = true)]
        public Guid? GroupTypeId { get; set; }
        [MappingColumn("CCCO_PK", Nullable = true)]
        public Guid? AccountId { get; set; }
        [MappingColumn("TIGR_DESC", Nullable = true)]
        public string ReservationGroupType { get; set; }
        [MappingColumn("RESE_DACR", Nullable = true)]
        public DateTime Created { get; set; }
        [MappingColumn("RESE_DAEN", Nullable = true)]
        public DateTime Arrival { get; set; }
        [MappingColumn("RESE_DASA", Nullable = true)]
        public DateTime Departure { get; set; }
        [MappingColumn("RESE_NHAB", Nullable = true)]
        public decimal Rooms { get; set; }
        [MappingColumn("RESE_REAL", Nullable = true)]
        public decimal ActualRooms { get; set; }
        [MappingColumn("RESE_ANUL", Nullable = true)]
        public decimal AnulRooms { get; set; }
        [MappingColumn("RESE_ATTE", Nullable = true)]
        public decimal AttemptRooms { get; set; }
        [MappingColumn("RESE_NACI", Nullable = true)]
        public string Countries { get; set; }
        [MappingColumn("RESE_ENTI", Nullable = true)]
        public string Entities { get; set; }
        [MappingColumn("RESE_OPER", Nullable = true)]
        public long Operations { get; set; }
        [MappingColumn("RESE_DELI", Nullable = true)]
        public DateTime? DeadLineDate { get; set; }
        [MappingColumn("RESE_GRUP", Nullable = false)]
        public bool IsGroup { get; set; }
        [MappingColumn("AGEN_CODI", Nullable = true)]
        public string Agency { get; set; }
        [MappingColumn("RESE_OBSE", Nullable = true)]
        public string Comments { get; set; }

        /// <summary>
        /// Formatted comment
        /// </summary>
        public string CommentsFormatted
        {
            get
            {
                if (string.IsNullOrEmpty(Comments))
                    return string.Empty;

                if (Comments.Length < 60)
                    return Comments.Replace(Environment.NewLine, string.Empty);

                return Comments.Substring(0, 59).Replace(Environment.NewLine, string.Empty) + "...";
            }
        }
    }
}