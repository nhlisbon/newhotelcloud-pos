﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("MOPE_PK")]
    public class PensionModeRecord : BaseRecord
    {
        [MappingColumn("MOPE_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("MOPE_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
    }
}