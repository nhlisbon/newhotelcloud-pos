﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PERF_PK")]
    public class ProfileRecord : BaseRecord
    {
        [MappingColumn("PERF_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}