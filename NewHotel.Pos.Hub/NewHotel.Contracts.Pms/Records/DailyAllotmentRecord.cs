﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class DailyAllomentRecord : BaseRecord
    {
        [DataMember]
        public AllotmentInfoContract Day0 { get; set; }
        [DataMember]
        public AllotmentInfoContract Day1 { get; set; }
        [DataMember]
        public AllotmentInfoContract Day2 { get; set; }
        [DataMember]
        public AllotmentInfoContract Day3 { get; set; }
        [DataMember]
        public AllotmentInfoContract Day4 { get; set; }
        [DataMember]
        public AllotmentInfoContract Day5 { get; set; }
        [DataMember]
        public AllotmentInfoContract Day6 { get; set; }
    }
}
