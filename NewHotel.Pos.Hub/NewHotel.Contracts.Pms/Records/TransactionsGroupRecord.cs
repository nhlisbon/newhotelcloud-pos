﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("key")]
    class TransactionsGroupRecord : BaseRecord
    {
        [MappingColumn("description")]
        public string Description { get; set; }
    }
}
