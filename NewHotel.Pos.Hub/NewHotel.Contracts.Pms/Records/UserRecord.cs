﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("UTIL_PK")]
    public class UserRecord : BaseRecord
    {
        [MappingColumn("UTIL_LOGIN", Nullable = true)]
        public string Login { get; set; }
        [MappingColumn("UTIL_NOME", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("UTIL_INAC", Nullable = true)]
        public bool Inactive { get; set; }
        [MappingColumn("PERF_PK", Nullable = true)]
        public Guid? ProfileId { get; set; }
        [MappingColumn("PERF_DESC", Nullable = true)]
        public string ProfileDesc{ get; set; }
        
    }
}
