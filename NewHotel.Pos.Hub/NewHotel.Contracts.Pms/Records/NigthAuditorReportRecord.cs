﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("RNAU_PK")]
    public class NigthAuditorReportRecord : BaseRecord
    {
        [MappingColumn("RELA_PK", Nullable = true)]
        public long ReportId { get; set; }
        [MappingColumn("RELA_NAME", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("RNAU_FRNA", Nullable = true)]
        public long Type { get; set; }
    }
}