﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("VMOD_PK")]
    public class ModelRecord:BaseRecord
    {
        [MappingColumn("VMOD_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}