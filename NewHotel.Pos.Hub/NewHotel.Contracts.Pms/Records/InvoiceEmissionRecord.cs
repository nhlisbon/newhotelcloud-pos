﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("MOVI_PK")]
    public class InvoiceEmissionRecord : BaseRecord, IDataFill
    {
        [MappingColumn("MOVI_RESE", Nullable = true)]
        public string ReservationNumber { get; set; }

        [MappingColumn("RESE_PK", Nullable = true)]
        public Guid? ReservationId { get; set; }

        [MappingColumn("TACR_PK", Nullable = true)]
        public Guid? ReservationCreditCardId { get; set; }

        [MappingColumn("MOVI_DATR", Nullable = true)]
        public string WorkDate { get; set; }

        [MappingColumn("MOVI_DAVA", Nullable = true)]
        public string DateValue { get; set; }

        [MappingColumn("MOVI_ADES", Nullable = true)]
        public string AddDescription { get; set; }

        [MappingColumn("MOVI_CANT", Nullable = true)]
        public short? ServiceQty { get; set; }

        [MappingColumn("MOVI_VALO", Nullable = true)]
        public decimal? MovementValue { get; set; }

        [MappingColumn("MOVI_VLIQ", Nullable = true)]
        public decimal? MovementLiquidValue { get; set; }

        [MappingColumn("MOVI_DCTO", Nullable = true)]
        public decimal? DiscountValue { get; set; }

        [MappingColumn("MOVI_TIMO")]
        public EntrieType Type { get; set; }

        [MappingColumn("DISCOUNT", Nullable = true)]
        public string Discount { get; set; }

        [MappingColumn("SERV_DESC", Nullable = true)]
        public string ServiceDescription { get; set; }

        [MappingColumn("SECC_DESC", Nullable = true)]
        public string DepartmentDescription { get; set; }

        [MappingColumn("FORE_DESC", Nullable = true)]
        public string PaymentDescription { get; set; }

        [MappingColumn("TPDI_DESC", Nullable = true)]
        public string PensioModeDescription { get; set; }

        [MappingColumn("MOVI_NUDO", Nullable = true)]
        public string DocumentNumber { get; set; }

        [MappingColumn("MOVI_DIAR", Nullable = true)]
        public bool? FromPension { get; set; }

        [MappingColumn("MOVI_TELF", Nullable = true)]
        public bool? Telf { get; set; }

        [MappingColumn("MOVI_DESC", Nullable = true)]
        public string Description { get; set; }

        [MappingColumn("UNMO_MOEX", Nullable = true)]
        public string ForeingCurrencyCode { get; set; }

        [MappingColumn("MOVI_VAME", Nullable = true)]
        public decimal? ForeingCurrencyValue { get; set; }

        [MappingColumn("TIVA1_PK", Nullable = true)]
        public Guid? TaxValueId1 { get; set; }

        [MappingColumn("TIVA1_PIMP", Nullable = true)]
        public decimal? TaxValuePercent1 { get; set; }

        [MappingColumn("TIVA1_VIMP", Nullable = true)]
        public decimal? TaxValueValue1 { get; set; }

        [MappingColumn("TIVA2_PK", Nullable = true)]
        public Guid? TaxValueId2 { get; set; }

        [MappingColumn("TIVA2_PIMP", Nullable = true)]
        public decimal? TaxValuePercent2 { get; set; }

        [MappingColumn("TIVA2_VIMP", Nullable = true)]
        public decimal? TaxValueValue2 { get; set; }

        [MappingColumn("TIVA3_PK", Nullable = true)]
        public Guid? TaxValueId3 { get; set; }

        [MappingColumn("TIVA3_PIMP", Nullable = true)]
        public decimal? TaxValuePercent3 { get; set; }

        [MappingColumn("TIVA3_VIMP", Nullable = true)]
        public decimal? TaxValueValue3 { get; set; }

        [MappingColumn("TIVA4_PK", Nullable = true)]
        public Guid? TaxValueId4 { get; set; }

        [MappingColumn("TIVA4_PIMP", Nullable = true)]
        public decimal? TaxValuePercent4 { get; set; }

        [MappingColumn("TIVA4_VIMP", Nullable = true)]
        public decimal? TaxValueValue4 { get; set; }

        [MappingColumn("MOVI_TAXS", Nullable = true)]
        public string Taxes { get; set; }

        public decimal? Value
        {
            get { return MovementValue.HasValue && Type == EntrieType.Debit ? -MovementValue.Value : MovementValue; }
        }

        public decimal? NetValue
        {
            get { return MovementValue.HasValue && Type == EntrieType.Debit ? -MovementLiquidValue.Value : MovementLiquidValue; }
        }

        public object MovementIds { get; set; }

        public void FillData(IRecord record)
        {
            var value = record.ValueAs(typeof(object), "MOVI_PKS");
			if (value is byte[])
				value = new Guid((byte[])value);
            else if (value is IEnumerable<object>)
                value = ((IEnumerable<object>)value).ToArray();

            MovementIds = value;
        }
    }
}
