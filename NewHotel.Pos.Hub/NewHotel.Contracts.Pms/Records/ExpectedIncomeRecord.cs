﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("RESE_CODI")]
    public class ExpectedIncomeRecord : BaseRecord
    {
        [MappingColumn("RESE_ALOJ", Nullable = true)]
        public string Room { get; set; }
        [MappingColumn("RESE_ESTA", Nullable = true)]
        public string State { get; set; }
        [MappingColumn("RESE_ANPH", Nullable = true)]
        public string Holder { get; set; }
        [MappingColumn("TIAL_ABRE", Nullable = true)]
        public string RoomType { get; set; }
        [MappingColumn("RESE_DAEN", Nullable = true)]
        public string Arrival { get; set; }
        [MappingColumn("RESE_DASA", Nullable = true)]
        public string Departure { get; set; }
        [MappingColumn("RESE_PAXS", Nullable = true)]
        public string Paxs { get; set; }
        [MappingColumn("LIRE_PK")]
        public Guid  OccupationLineId { get; set; }
        [MappingColumn("UNMO_PK")]
        public string Currency { get; set; }
        [MappingColumn("MOVI_VALO", Nullable = true)]
        public decimal? Rate { get; set; }
        [MappingColumn("DAEN_DATE")]
        public DateTime ArrivalDate { get; set; }
        [MappingColumn("DASA_DATE")]
        public DateTime DepartureDate { get; set; }
        [MappingColumn("HOEN_DATE")]
        public DateTime ArrivalTime { get; set; }
        [MappingColumn("HOSA_DATE")]
        public DateTime DepartureTime { get; set; }
        [MappingColumn("CTRL_PRICE")]
        public string PriceType { get; set; }

        public string ArrivalF { get { return ArrivalDate.ToMonthAndDay() + " - " + ArrivalTime.ToHourAndMinute(); } }
        public string DepartureF { get { return DepartureDate.ToMonthAndDay() + " - " + DepartureTime.ToHourAndMinute(); } }
    }
}