﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("HOTE_PK")]
    public class DailyArrivalReservationRecord : BaseRecord
    {
        [MappingColumn("ROOM_EXP_ARRIVAL", Nullable = false)]
        public long ExpectedArrived { get; set; }
        [MappingColumn("ROOM_CURRENT_ARRIVAL", Nullable = false)]
        public long CurrentArrived { get; set; }
        [MappingColumn("TOTAL_ARRIVAL", Nullable = false)]
        public long TotalArrive { get; set; }
        [MappingColumn("ROOM_EXP_DEPARTURE", Nullable = false)]
        public long ExpectedDeparture { get; set; }
        [MappingColumn("ROOM_CURRENT_DEPARTURE", Nullable = false)]
        public long CurrentDeparture { get; set; }
        [MappingColumn("TOTAL_DEPARTURE", Nullable = false)]
        public long TotalDeparture { get; set; }

        public long MajorArrivalInterval
        {
            get { return TotalArrive < 10 ? TotalArrive : 10; }
        }

        public long MajorDepartureInterval
        {
            get { return TotalDeparture < 10 ? TotalDeparture : 10; }
        }
    }
}