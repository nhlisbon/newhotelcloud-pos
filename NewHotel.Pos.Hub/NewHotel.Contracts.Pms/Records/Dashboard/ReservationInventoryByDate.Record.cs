﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("")]
    public class ReservationInventoryByDateRecord : BaseRecord
    {
        [MappingColumn("EDTA_DATA")]
        public DateTime X { get; set; }
        [MappingColumn("DAYOFWEEK")]
        public string DOW { get; set; }
        [MappingColumn("DAYOFWEEKPLUSDAY")]
        public string DOWD { get; set; }
        [MappingColumn("ROOMS")]
        public decimal R { get; set; }
        [MappingColumn("OUTOFORDER")]
        public decimal O { get; set; }
        [MappingColumn("TOTALRESERVAS")]
        public decimal TR { get; set; }
        [MappingColumn("ROOMUSED")]
        public decimal RU { get; set; }
        [MappingColumn("GUARANTEED_ALLOTMENT")] 
        public decimal AG { get; set; }

        public decimal V
        {
            get
            {
                var vacants = R - O - RU - AG;
                if (vacants < decimal.Zero)
                    return decimal.Zero;

                return vacants;
            }
        }
    }
}