﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("")]
    public class NationalityAnalysisRecord : BaseRecord
    {
        [MappingColumn("NACI_DESC")]
        public string X { get; set; }
        [MappingColumn("RESE_COUNT", Nullable = true)]
        public decimal? Y { get; set; }
    }
}