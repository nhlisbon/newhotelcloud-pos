﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("DIAMES")]
    public class AverageDailyRateRecord : BaseRecord
    {
        [MappingColumn("EDTA_DATA")]
        public DateTime X { get; set; }
        [MappingColumn("ADR")]
        public decimal Y { get; set; }
        [MappingColumn("REVPAR")]
        public decimal R { get; set; }
    }
}