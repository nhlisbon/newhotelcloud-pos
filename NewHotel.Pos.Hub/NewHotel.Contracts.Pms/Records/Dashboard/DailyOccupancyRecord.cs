﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PK")]
    public class DailyOccupancyRecord : BaseRecord
    {
        [MappingColumn("OCCUPANCY", Nullable = true)]
        public long? Occupancy { get; set; }
        [MappingColumn("TOTAL", Nullable = true)]
        public long? Total { get; set; }
        [MappingColumn("INACTIVE", Nullable = true)]
        public long? Inactive { get; set; }
        [MappingColumn("GUARANTEED_ALLOTMENTS", Nullable = true)]
        public long? GuaranteedAllotments { get; set; }
        [MappingColumn("PERCACTIVE", Nullable = true)]
        public decimal? PercentActive { get; set; }
        [MappingColumn("PERCTOTAL", Nullable = true)]
        public decimal? PercentTotal { get; set; }
        [MappingColumn("PARA_PEOC", Nullable = true)]
        public decimal? PercentCritical { get; set; }
        [MappingColumn("DASH_SMRT", Nullable = false)]
        public bool OccupancySmart { get; set; }

        public string PercentActiveText
        {
            get
            {
                if (PercentActive.HasValue)
                    return PercentActive.Value.ToString("F02") + "%";

                return string.Empty;
            }
        }

        public string PercentTotalText
        {
            get
            {
                if (PercentTotal.HasValue)
                    return PercentTotal.Value.ToString("F02") + "%";

                return string.Empty;
            }
        }

        public string PercentText
        {
            get { return OccupancySmart ? PercentTotalText : PercentActiveText; }
        }
    }
}