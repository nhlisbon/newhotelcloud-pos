﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("DIAMES")]
    public class OccupancyRecord : BaseRecord
    {
        [MappingColumn("EDTA_DATA")]
        public DateTime X { get; set; }
        [MappingColumn("OCCUPANCY_SMART_INDIV")]
        public decimal I { get; set; }
        [MappingColumn("OCCUPANCY_SMART_GROUP")]
        public decimal G { get; set; }
    }
}