﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("EDTA_DATA")]
    public class MarketSegmentABCAnalysisRecord : BaseRecord
    {
        [MappingColumn("SEME_DESC")]
        public string X { get; set; }
        [MappingColumn("RESE_COUNT", Nullable = true)]
        public decimal? Y { get; set; }
    }
}