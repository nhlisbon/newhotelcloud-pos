﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("")]
    public class MarketSourceABCAnalysisRecord : BaseRecord
    {
        [MappingColumn("ORME_DESC")]
        public string X { get; set; }
        [MappingColumn("RESE_COUNT", Nullable = true)]
        public decimal? Y { get; set; }
    }
}