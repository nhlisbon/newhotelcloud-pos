﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("")]
    public class OccupancyRevParByDatesRecord : BaseRecord
    {
        [MappingColumn("EDTA_DATA")]
        public DateTime X { get; set; }
        [MappingColumn("DAYOFWEEK")]
        public string DOW { get; set; }
        [MappingColumn("DAYOFWEEKPLUSDAY")]
        public string DOWD { get; set; }
        [MappingColumn("ROOMS")]
        public decimal R { get; set; }
        [MappingColumn("TOTALRESERVAS")]
        public decimal TR { get; set; }
        [MappingColumn("R_GROUP")]
        public decimal RG { get; set; }
        [MappingColumn("R_INDIV")]
        public decimal RI { get; set; }
        [MappingColumn("FORECASTINCOME")]
        public decimal FI { get; set; }
        [MappingColumn("OUTOFORDER")]
        public decimal OO { get; set; }
        [MappingColumn("GUARANTEED_ALLOTMENTS")]
        public decimal AG { get; set; }
        [MappingColumn("ADR")]
        public decimal ADR { get; set; }
        [MappingColumn("REVPAR")]
        public decimal RPAR { get; set; }
        [MappingColumn("OCCUPANCY")]
        public decimal O { get; set; }
        [MappingColumn("OCCUPANCY_SMART")]
        public decimal OS { get; set; }
        [MappingColumn("OCCUPANCY_INDIV")]
        public decimal OI { get; set; }
        [MappingColumn("OCCUPANCY_SMART_INDIV")]
        public decimal OIS { get; set; }
        [MappingColumn("OCCUPANCY_GROUP")]
        public decimal OG { get; set; }
        [MappingColumn("OCCUPANCY_SMART_GROUP")]
        public decimal OGS { get; set; }
        [MappingColumn("OCCUPANCY_SMART_ALLOTMENT")]
        public decimal OAS { get; set; }
    }
}