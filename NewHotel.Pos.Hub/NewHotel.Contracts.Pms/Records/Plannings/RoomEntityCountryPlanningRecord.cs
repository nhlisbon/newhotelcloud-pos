﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class RoomEntityCountryPlanningRecord : PlanningBaseRecord
    {
        [MappingColumn("TREC_PK", Nullable = true)]
        public Guid RoomTypeId { get; set; }
        [MappingColumn("TREC_ABRE", Nullable = true)]
        public string RoomTypeDescription { get; set; }
        [MappingColumn("ENTI_PK", Nullable = true)]
        public Guid EntityId { get; set; }
        [MappingColumn("ENTI_DESC", Nullable = true)]
        public string EntityDescription { get; set; }
        [MappingColumn("NACI_PK", Nullable = true)]
        public string CountryId { get; set; }
        [MappingColumn("NACI_DESC", Nullable = true)]
        public string CountryDescription { get; set; }
        [MappingColumn("TOTL_ROOM", Nullable = true)]
        public int? TotalRooms { get; set; }
    }
}
