﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class AvailabilityExtrasRecord : PlanningBaseRecord
    {
        [MappingColumn("ADIC_PK", Nullable = true)]
        public Guid IncidentalId { get; set; }
        [MappingColumn("ADIC_DESC", Nullable = true)]
        public string IncidentalDescription { get; set; }
        [MappingColumn("TOTL_ADIC", Nullable = true)]
        public int? TotalIncidental { get; set; }
    }
}