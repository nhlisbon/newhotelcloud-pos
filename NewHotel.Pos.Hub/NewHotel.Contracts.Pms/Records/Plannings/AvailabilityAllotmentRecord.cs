﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class AvailabilityAllotmentRecord : PlanningBaseRecord
    {
        [MappingColumn("ALLO_TOTL", Nullable = true)]
        public int Contrated { get; set; }
        [MappingColumn("ALLO_USED", Nullable = true)]
        public int Used{ get; set; }
    }
}
