﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class AvailabilityInventoryRecord : PlanningBaseRecord
    {
        [MappingColumn("TOTL_ROOM", Nullable = false)]
        public int Inventory { get; set; }
        [MappingColumn("TOTL_ACTI", Nullable = false)]
        public int Active { get; set; }
        [MappingColumn("TOTL_INAC", Nullable = false)]
        public int Inactive { get; set; }
    }
}
