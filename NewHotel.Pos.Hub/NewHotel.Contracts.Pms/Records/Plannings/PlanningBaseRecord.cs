﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("EDTA_DATA")]
    public class PlanningBaseRecord : BaseRecord
    {
        [MappingColumn("HOTE_PK", Nullable = false)]
        public Guid HotelId { get; set; }
        [MappingColumn("HOTE_DESC", Nullable = false)]
        public string HotelDescription { get; set; }
    }
}