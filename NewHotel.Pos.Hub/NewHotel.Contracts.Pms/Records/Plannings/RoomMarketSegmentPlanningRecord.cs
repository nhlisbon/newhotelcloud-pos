﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class RoomMarketSegmentPlanningRecord : PlanningBaseRecord
    {
        [MappingColumn("TREC_PK", Nullable = true)]
        public Guid RoomTypeId { get; set; }
        [MappingColumn("TREC_ABRE", Nullable = true)]
        public string RoomTypeDescription { get; set; }
        [MappingColumn("SEME_PK", Nullable = true)]
        public Guid SegmentId { get; set; }
        [MappingColumn("SEME_DESC", Nullable = true)]
        public string SegmentDescription { get; set; }
        [MappingColumn("GRME_PK", Nullable = true)]
        public Guid GroupId { get; set; }
        [MappingColumn("GRME_DESC", Nullable = true)]
        public string GroupDescription { get; set; }
        [MappingColumn("TOTL_ROOM", Nullable = true)]
        public int? TotalRooms { get; set; }
    }
}
