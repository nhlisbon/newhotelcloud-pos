﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class PaxsMealsPlanningRecord : PlanningBaseRecord
    {
        [MappingColumn("DESAYUNO", Nullable = true)]
        public long? Breakfast { get; set; }
        [MappingColumn("ALMUERZO", Nullable = true)]
        public long? Lunch { get; set; }
        [MappingColumn("CENA", Nullable = true)]
        public long? Dinner { get; set; }     
    }
}