﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class RoomEntityOriginPlanningRecord : PlanningBaseRecord
    {
        [MappingColumn("TREC_PK", Nullable = true)]
        public Guid RoomTypeId { get; set; }
        [MappingColumn("TREC_ABRE", Nullable = true)]
        public string RoomTypeDescription { get; set; }
        [MappingColumn("ENTI_PK", Nullable = true)]
        public Guid EntityId { get; set; }
        [MappingColumn("ENTI_DESC", Nullable = true)]
        public string EntityDescription { get; set; }
        [MappingColumn("ORME_PK", Nullable = true)]
        public Guid OriginId { get; set; }
        [MappingColumn("ORME_DESC", Nullable = true)]
        public string OriginDescription { get; set; }
        [MappingColumn("TOTL_ROOM", Nullable = true)]
        public int? TotalRooms { get; set; }
    }
}
