﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PTRA_PK")]
    public class WorkstationRecord : BaseRecord
    {
        [MappingColumn("PTRA_TYPE")]
        public WorkstationTypes WorkstationType { get; set; }
        [MappingColumn("TYPE_DESC")]
        public string TypeDescription { get; set; }
        [MappingColumn("PTRA_CODE")]
        public string Code { get; set; }
        [MappingColumn("PTRA_DESC")]
        public string Description { get; set; }
    }
}
