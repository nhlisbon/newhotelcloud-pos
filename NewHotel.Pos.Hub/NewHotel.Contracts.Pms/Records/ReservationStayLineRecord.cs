﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("LIES_PK")]
    public class ReservationStayLineRecord : BaseRecord
    {
        [MappingColumn("LIES_DAEN", Nullable = true)]
        public DateTime FromDate { get; set; }
        [MappingColumn("LIES_DASA", Nullable = true)]
        public DateTime ToDate { get; set; }
        [MappingColumn("ALOJ_NUMB", Nullable = true)]
        public string Room { get; set; }
        [MappingColumn("LIES_OVER", Nullable = true)]
        public bool Overbooking { get; set; }
        [MappingColumn("EXTE_HOTE", Nullable = true)]
        public string ExternalHotel { get; set; }
        [MappingColumn("EXTE_ALOJ", Nullable = true)]
        public string ExternalRoom { get; set; }
        [MappingColumn("INTE_HOTE", Nullable = true)]
        public string InternalHotel { get; set; }
    }
}