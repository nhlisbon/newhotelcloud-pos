﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("ALOJ_PK")]
    public class AvailableRoomsRecord: BaseRecord
    {
        [DataMember]
        [MappingColumn("ALOJ_ROOM", Nullable = false)]
        public string Room { get; set; }
        [DataMember]
        [MappingColumn("TREC_PK", Nullable = false)]
        public Guid TypeId { get; set; }
        [DataMember]
        [MappingColumn("TREC_ABRE", Nullable = false)]
        public string Type { get; set; }
        [DataMember]
        [MappingColumn("ALOJ_VIRT", Nullable = false)]
        public bool VirtualRoom { get; set; }
        [DataMember]
        [MappingColumn("ALOJ_OCUP", Nullable = false)]
        public bool OccupiedNow { get; set; }
        [DataMember]
        [MappingColumn("ALOJ_ROST", Nullable = false)]
        public RoomStatus Status { get; set; }
        [DataMember]
        [MappingColumn("ALOJ_LUSE", Nullable = true)]
        public DateTime? LastDateOccupancy { get; set; }
    }
}