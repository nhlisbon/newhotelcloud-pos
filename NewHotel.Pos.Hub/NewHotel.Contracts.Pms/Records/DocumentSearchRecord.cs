﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("DOCU_PK")]
    public class DocumentSearchRecord : BaseRecord
    {
        // 0. Facturas, 1. Notas Crédito, 2. Facturas Pro-Forma, 3. Recibos, 4. Boletas, 6. Debit Notes
        [MappingColumn("DOCU_TIDO")]
        public short DocumentType { get; set; }
        [MappingColumn("TIDO_DESC")]
        public string DocumentTypeDescription { get; set; }
        [MappingColumn("DOCU_FULL_SERIE")]
        public string DocumentFullSerie { get; set; }
        [MappingColumn("DOCU_SEDO")]
        public string DocumentSerie { get; set; }
        [MappingColumn("DOCU_CODI")]
        public long? DocumentNumber { get; set; }
        [MappingColumn("DOCU_TITU")]
        public string Holder { get; set; }
        [MappingColumn("DOCU_FNUM")]
        public string FiscalNumber { get; set; }
        [MappingColumn("DOCU_ANUL")]
        public bool Cancelled { get; set; }
        [MappingColumn("DOCU_DEAN")]
        public bool? AdvancedInvoice { get; set; }
        [MappingColumn("DOCU_REGU")]
        public bool Regularized { get; set; }
        [MappingColumn("DOCU_OBSE")]
        public string Comments { get; set; }
        [MappingColumn("ENTI_DESC")]
        public string EntityDescription { get; set; }
        [MappingColumn("DOCU_VALO")]
        public decimal DocumentValue { get; set; }
        [MappingColumn("UNMO_BASE")]
        public string BaseCurrency { get; set; }
        [MappingColumn("DOCU_VAME")]
        public decimal? ForeingValue { get; set; }
        [MappingColumn("DOCU_MOEX")]
        public string DocumentCurrency { get; set; }
        [MappingColumn("DOCU_CAMB")]
        public decimal? ForeingExchange { get; set; }
        [MappingColumn("DOCU_REFE")]
        public string DocumentReference { get; set; }
        [MappingColumn("NOTE_REFE")]
        public string NoteReference { get; set; }
        [MappingColumn("DOCU_USER")]
        public string User { get; set; }
        [MappingColumn("DOCU_OPER", Nullable = true)]
        public long? Operations { get; set; }
        [MappingColumn("TACO_DESC")]
        public string ExtAccountDesc { get; set; }
        [MappingColumn("DOCF_DAEM")]
        public DateTime EmisionDate { get; set; }
        [MappingColumn("DOCF_HOEM")]
        public DateTime EmisionTime { get; set; }
        [MappingColumn("DEFA_VOUC")]
        public string Voucher { get; set; }
        [MappingColumn("DOCF_FIES")]
        public DocumentFiscalStatus? FiscalStatus { get; set; }
        [MappingColumn("DOCF_FIME")]
        public string FiscalStatusMessage { get; set; }
        [MappingColumn("EMAIL_ADDR")]
        public string HolderEmailAddress { get; set; }
        [MappingColumn("DOCF_FREE")]
        public string FreeCode { get; set; }
        [MappingColumn("DOCF_NRPS")]
        public string ExternalInvoiceNumber { get; set; }
        [MappingColumn("DOCU_CONS")]
        public long? Consecutive { get; set; }
        [MappingColumn("DOCU_FISC")]
        public bool Fiscal { get; set; }

        public string EmitionDateF
        {
            get { return EmisionDate.ToMonthAndDay() + " - " + EmisionTime.ToHourAndMinute(); }
        }
    }
}