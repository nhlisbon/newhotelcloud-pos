﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("EDTA_DATA")]
    public class MontlyOccupancyProfileRecord : BaseRecord
    {
        [MappingColumn("MESANO")]
        public string X { get; set; }
        [MappingColumn("OCCUPANCY")]
        public decimal Y { get; set; }
    }
}