﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
	[MappingQuery("RPSD_PK")]
	public class RpsInvoiceDetailsRecord : BaseRecord
	{
		[MappingColumn("SERV_DESC")]
		public string Service { get; set; }
		[MappingColumn("SERV_DAEM")]
		public DateTime ServiceDate { get; set; }
		[MappingColumn("SERV_VALO")]
		public decimal GrossValue { get; set; }
		[MappingColumn("SERV_VLIQ")]
		public decimal NetValue { get; set; }
		[MappingColumn("SERV_TAXA")]
		public decimal TaxPercent { get; set; }
		[MappingColumn("SERV_VIMP")]
		public decimal TaxValue { get; set; }
        [MappingColumn("SERV_VRET")]
		public decimal RetentionValue { get; set; }
	}
}