﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("FEDI_PK")]
    public class DailyClosingRecord : BaseRecord
    {
        [MappingColumn("FEDI_DATA", Nullable = false)]
        public DateTime Day { get; set; }
        [MappingColumn("FEDI_COMP", Nullable = false)]
        public bool Completed { get; set; }
        [MappingColumn("FEDI_SUCE", Nullable = false)]
        public bool Success { get; set; }
        [MappingColumn("FEDI_ESTD", Nullable = false)]
        public bool Statistics { get; set; }
        [MappingColumn("FEDI_CONF", Nullable = false)]
        public bool Confirmed { get; set; }
        [MappingColumn("UTIL_LOGIN", Nullable = false)]
        public string User { get; set; }
        [MappingColumn("FEDI_DAOP", Nullable = false)]
        public string Date { get; set; }
        [MappingColumn("FEDI_HOOP", Nullable = false)]
        public string Time { get; set; }
        [MappingColumn("FEDI_DAAN", Nullable = true)]
        public bool? PrevDateCompletedSuccess { get; set; }

        public bool CanConfirm
        {
            get { return !Confirmed && Completed && Success && (PrevDateCompletedSuccess ?? true); }
        }
    }
}