﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [MappingQuery("ALOJ_PK")]
    public class RoomStatusRecord: BaseRecord
    {
       
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        [DataMember]
        public string Room { get; set; }
        [MappingColumn("TIAL_ABRE", Nullable = true)]
        [DataMember]
        public string Type { get; set; }
        [MappingColumn("BLAL_DESC", Nullable = true)]
        [DataMember]
        public string Block { get; set; }
        [MappingColumn("ALOJ_PISO", Nullable = true)]
        [DataMember]
        public short Floor { get; set; }
        [MappingColumn("ALOJ_OCUP", Nullable = true)]
        [DataMember]
        public bool Occupied { get; set; }
        [MappingColumn("ALOJ_ACTI", Nullable = true)]
        [DataMember]
        public bool Active { get; set; }
        [MappingColumn("ALOJ_PREA", Nullable = true)]
        [DataMember]
        public bool PreAssigned { get; set; }
        [MappingColumn("ALOJ_ROST", Nullable = true)]
        [DataMember]
        public RoomStatus Status { get; set; }
        [MappingColumn("ROST_DESC", Nullable = true)]
        [DataMember]
        public string StatusDescription { get; set; }
        [MappingColumn("ALOJ_PRSA")]
        [DataMember]
        public bool PreUnassigned { get; set; }
        [DataMember]
        public int ColumnId { get; set; }
        [DataMember]
        public int RowId { get; set; }

        [MappingColumn("COLO_DIRT")]
        [DataMember]
        public ARGBColor RoomColorDirty { get; set; }
        [MappingColumn("COLO_CLEAN")]
        [DataMember]
        public ARGBColor RoomColorClean { get; set; }

    }
}
