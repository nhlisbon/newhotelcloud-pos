﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("CHBS_PK")]
    public class BookingRecord : BaseRecord
    {
        [MappingColumn("CHBS_NAME")]
        public string BookingServiceDescription { get; set; }
        [MappingColumn("TYPE_DESC")]
        public string Type { get; set; }
        [MappingColumn("CHBS_TYPE")]
        public BookingServiceType TypeId { get; set; }
        [MappingColumn("CHBS_EXCL")]
        public bool ExportClassifiers { get; set; }
        [MappingColumn("CHBS_EXPR")]
        public bool ExportPrices { get; set; }
        [MappingColumn("CHBS_EXRE")]
        public bool ExportRestrictions { get; set; }
        [MappingColumn("EXPM_DESC")]
        public string ExportAvailabilityMode { get; set; }
        [MappingColumn("CHBS_SCAN")]
        public bool SubChannel { get; set; }
        [MappingColumn("CHBS_INAC")]
        public bool Inactive { get; set; }
    }
}