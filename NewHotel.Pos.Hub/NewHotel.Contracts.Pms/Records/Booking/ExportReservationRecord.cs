﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("EXRE_PK")]
    public class ExportReservationRecord : BaseRecord
    {
        [MappingColumn("LIRE_RESE")]
        public string ReservationNumber  { get; set; }
        [MappingColumn("EXRE_DATE")]
        public DateTime ReservationUpdate { get; set; }
    }
}