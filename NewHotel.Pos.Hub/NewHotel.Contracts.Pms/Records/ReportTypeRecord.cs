﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("RELA_PK")]
    public class ReportTypeRecord : BaseRecord
    {
        [MappingColumn("RELA_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("TIPO_DESC", Nullable = true)]
        public string Type { get; set; }
        public string Name { get; set; }
    }
}