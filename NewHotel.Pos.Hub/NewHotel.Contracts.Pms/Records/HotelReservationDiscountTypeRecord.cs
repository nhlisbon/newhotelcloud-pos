﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TIDE_PK")]
    public class HotelReservationDiscountTypeRecord : BaseRecord
    {
        [MappingColumn("TIDE_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("TIDE_FIXE")]
        public bool Fixed { get; set; }
        [MappingColumn("TIDE_VMIN")]
        public decimal MinValue { get; set; }
        [MappingColumn("TIDE_VMAX")]
        public decimal MaxValue { get; set; }
    }
}