﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("RELA_PK")]
    public class NigthAuditorAllReportRecord : BaseRecord
    {
        [MappingColumn("RELA_FRNA", Nullable = true)]
        public string Type { get; set; }
         [MappingColumn("RELA_DESC", Nullable = true)]
        public string Description { get; set; }
       
        [MappingColumn("TIPO_DESC", Nullable = true)]
        public string TypeDescription { get; set; }
    }
}