﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("DESP_PK")]
    public class WakeUpRecord : BaseRecord
    {
        private object _imagenUri;
        [MappingColumn("DESP_DATR", Nullable = true)]
        public DateTime WorkDate { get; set; }
        [MappingColumn("DESP_HORE", Nullable = true)]
        public string RegistrationTime { get; set; }
        [MappingColumn("DESP_DATA", Nullable = true)]
        public DateTime ExecutionDate { get; set; }
        [MappingColumn("DESP_HORA", Nullable = true)]
        public string ExecutionTime { get; set; }
        [MappingColumn("DESP_STAT", Nullable = true)]
        public WakeUpCallState Status { get; set; }
        [MappingColumn("DESP_DETE", Nullable = true)]
        public WakeUpCallTypeCall WakeUpType { get; set; }
        [MappingColumn("EXTE_DESC", Nullable = true)]
        public string ExtensionDescription { get; set; }
        [MappingColumn("RESE_NUMB", Nullable = true)]
        public string ReservationNumber { get; set; }
        [MappingColumn("LIRE_ANPH", Nullable = true)]
        public string Holder { get; set; }
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        public string RoomNumber { get; set; }



        [DataMember]
        public object ImagenUri { get { return _imagenUri; } set { Set<object>(ref _imagenUri, value, "ImagenUri"); } }
    }
}
