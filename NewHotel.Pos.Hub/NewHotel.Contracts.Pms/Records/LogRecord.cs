﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("LORE_PK")]
    public class ReservationLogRecord : BaseRecord
    {
        [MappingColumn("TOPE_DESC", Nullable = true)]
        public string Operation { get; set; }
        [MappingColumn("LORE_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("LORE_DARE", Nullable = true)]
        public DateTime Date { get; set; }
        [MappingColumn("LORE_DATR", Nullable = true)]
        public DateTime WorkDate { get; set; }
        [MappingColumn("USER_LOGIN", Nullable = true)]
        public string User { get; set; }
    }

    [MappingQuery("NLOG_PK")]
    public class NotificationLogRecord : BaseRecord
    {
        [MappingColumn("LIRE_PK", Nullable = false)]
        public Guid ReservationLineId { get; set; }
        [MappingColumn("TPLT_PK", Nullable = false)]
        public Guid NotificationId { get; set; }
        [MappingColumn("TPLT_BODY", Nullable = false)]
        public Clob Template { get; set; }
        [MappingColumn("NLOG_ADDR", Nullable = false)]
        public string Email { get; set; }
        [MappingColumn("NLOG_EMAI", Nullable = false)]
        public bool NotifybyEmail { get; set; }
        [MappingColumn("NLOG_DATE", Nullable = false)]
        public DateTime NotificationDate { get; set; }
        [MappingColumn("NLOG_TIME", Nullable = false)]
        public DateTime NotificationTime { get; set; }
    }
}