﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace XMLGenerator
{
    [Serializable]
    public class CloudData
    {
        public CloudData()
        {
            PriceRates = new List<PriceRate>();
            Entities = new List<Entity>();
            Contracts = new List<Contract>();
            Clients = new List<Client>();
            Reservations = new List<Reservation>();
            Seasons = new List<Season>();
            Rooms = new List<Room>();
        }
        
        public List<PriceRate> PriceRates { get; set; }
        public List<Entity> Entities { get; set; }
        public List<Contract> Contracts { get; set; }
        public List<Client> Clients { get; set; }
        public List<Reservation> Reservations { get; set; }
        public List<Season> Seasons { get; set; }
        public List<Room> Rooms { get; set; }

        public static CloudData GenerateRandomSet()
        {
            CloudData data = new CloudData();

            #region Price Rate
            data.PriceRates.Add(new PriceRate() { ID = 1, Months = 12, Name = "Rack", Price = 50, AdditionalPerPax = 25 });
            data.PriceRates.Add(new PriceRate() { ID = 2, Months = 12, Name = "Bar1", Price = 100, AdditionalPerPax = 50 });
            data.PriceRates.Add(new PriceRate() { ID = 3, Months = 12, Name = "Bar2", Price = 75, AdditionalPerPax = 30 });
            data.PriceRates.Add(new PriceRate() { ID = 4, Months = 12, Name = "Bar3", Price = 40, AdditionalPerPax = 10 });
            data.PriceRates.Add(new PriceRate() { ID = 5, Months = 12, Name = "BAR", IsBar = true, Bar1ID = 2, Bar2ID = 3, Bar3ID = 4 });
            #endregion
            #region Entity
            data.Entities.Add(new Entity() { ID = 1, Abbreviation = "TC", ComercialName = "Travel City", Description = "Travel City", PlanningColor = "25,230,140,68" });
            #endregion
            #region Contract
            data.Contracts.Add(new Contract() { ID = 1, Abbreviation = "TCC", Description = "TC Contract", EntityID = 1, PriceRateID = 1 });
            #endregion
            #region Cardex
            data.Clients.Add(new Client() { ID = 1, Country = "ES", FirstName = "David", LastName = "Garcia" });
            data.Clients.Add(new Client() { ID = 2, Country = "ES", FirstName = "Javier", LastName = "Fernandez" });
            data.Clients.Add(new Client() { ID = 3, Country = "ES", FirstName = "Sergio", LastName = "Gonzalez" });
            data.Clients.Add(new Client() { ID = 4, Country = "ES", FirstName = "Adrian", LastName = "Rodriguez" });
            data.Clients.Add(new Client() { ID = 5, Country = "ES", FirstName = "Carlos", LastName = "Lopez" });
            data.Clients.Add(new Client() { ID = 6, Country = "ES", FirstName = "Pablo", LastName = "Martinez" });
            data.Clients.Add(new Client() { ID = 7, Country = "ES", FirstName = "Alvaro", LastName = "Sanchez" });
            data.Clients.Add(new Client() { ID = 8, Country = "ES", FirstName = "Ivan", LastName = "Perez" });
            data.Clients.Add(new Client() { ID = 9, Country = "ES", FirstName = "Jorge", LastName = "Martin" });
            data.Clients.Add(new Client() { ID = 10, Country = "ES", FirstName = "Alejandro", LastName = "Gomez" });
            data.Clients.Add(new Client() { ID = 11, Country = "ES", FirstName = "Antonio", LastName = "Ruiz" });
            data.Clients.Add(new Client() { ID = 12, Country = "ES", FirstName = "Jose", LastName = "Hernandez" });
            data.Clients.Add(new Client() { ID = 13, Country = "ES", FirstName = "Manuel", LastName = "Jimenez" });
            data.Clients.Add(new Client() { ID = 14, Country = "ES", FirstName = "Francisco", LastName = "Diez" });
            data.Clients.Add(new Client() { ID = 15, Country = "ES", FirstName = "Juan", LastName = "Alvarez" });

            data.Clients.Add(new Client() { ID = 16, Country = "US", FirstName = "James", LastName = "Smith" });
            data.Clients.Add(new Client() { ID = 17, Country = "US", FirstName = "John", LastName = "Johnson" });
            data.Clients.Add(new Client() { ID = 18, Country = "US", FirstName = "Robert", LastName = "Williams" });
            data.Clients.Add(new Client() { ID = 19, Country = "US", FirstName = "Michael", LastName = "Jones" });
            data.Clients.Add(new Client() { ID = 20, Country = "US", FirstName = "Williams", LastName = "Brown" });
            data.Clients.Add(new Client() { ID = 21, Country = "US", FirstName = "David", LastName = "Davis" });
            data.Clients.Add(new Client() { ID = 22, Country = "US", FirstName = "Richard", LastName = "Miller" });
            data.Clients.Add(new Client() { ID = 23, Country = "US", FirstName = "Charles", LastName = "Wilson" });
            data.Clients.Add(new Client() { ID = 24, Country = "US", FirstName = "Joseph", LastName = "Moore" });
            data.Clients.Add(new Client() { ID = 25, Country = "US", FirstName = "Thomas", LastName = "Taylor" });

            data.Clients.Add(new Client() { ID = 26, Country = "PT", FirstName = "Rodrigo", LastName = "Silva" });
            data.Clients.Add(new Client() { ID = 27, Country = "PT", FirstName = "João", LastName = "Santos" });
            data.Clients.Add(new Client() { ID = 28, Country = "PT", FirstName = "Afonso", LastName = "Ferreira" });
            data.Clients.Add(new Client() { ID = 29, Country = "PT", FirstName = "Tomas", LastName = "Pereira" });
            data.Clients.Add(new Client() { ID = 30, Country = "PT", FirstName = "Tiago", LastName = "Rodriguez" });
            data.Clients.Add(new Client() { ID = 31, Country = "PT", FirstName = "Diogo", LastName = "Costa" });
            data.Clients.Add(new Client() { ID = 32, Country = "PT", FirstName = "Francisco", LastName = "Oliveira" });
            data.Clients.Add(new Client() { ID = 33, Country = "PT", FirstName = "Gabriel", LastName = "Martins" });
            data.Clients.Add(new Client() { ID = 34, Country = "PT", FirstName = "Rafael", LastName = "Sousa" });
            data.Clients.Add(new Client() { ID = 35, Country = "PT", FirstName = "Simão", LastName = "Gonçalves" });

            data.Clients.Add(new Client() { ID = 36, Country = "FR", FirstName = "Philippe", LastName = "Caillaux" });
            data.Clients.Add(new Client() { ID = 37, Country = "FR", FirstName = "Jean Pierre", LastName = "Crochet" });
            data.Clients.Add(new Client() { ID = 38, Country = "FR", FirstName = "Michel", LastName = "Chale" });
            data.Clients.Add(new Client() { ID = 39, Country = "FR", FirstName = "Nicolas", LastName = "Deville" });
            data.Clients.Add(new Client() { ID = 40, Country = "FR", FirstName = "Pierre", LastName = "Dubois" });
            data.Clients.Add(new Client() { ID = 41, Country = "FR", FirstName = "David", LastName = "Dupont" });
            data.Clients.Add(new Client() { ID = 42, Country = "FR", FirstName = "Christophe", LastName = "Dufour" });
            data.Clients.Add(new Client() { ID = 43, Country = "FR", FirstName = "Jean", LastName = "Le Blanc" });
            data.Clients.Add(new Client() { ID = 44, Country = "FR", FirstName = "Stephane", LastName = "Millet" });
            data.Clients.Add(new Client() { ID = 45, Country = "FR", FirstName = "Alain", LastName = "Petit" });
            #endregion
            #region Reservations
            data.Reservations.Add(new Reservation() { ID = 1, Arrival = 0, Nights = 1, Guests = new List<int>(new int[] { 1 }), ContractID = 1, EntityID = 0, HouseUse = true, ManualPrice = 50, PriceRateID = 0, UseManualPrice = true });
            data.Reservations.Add(new Reservation() { ID = 2, Arrival = 3, Nights = 3, Guests = new List<int>(new int[] { 2 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 3, Arrival = 1, Nights = 4, Guests = new List<int>(new int[] { 3 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 4, Arrival = 5, Nights = 7, Guests = new List<int>(new int[] { 4 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 5, Arrival = 2, Nights = 3, Guests = new List<int>(new int[] { 5 }), ContractID = 0, EntityID = 0, HouseUse = false, ManualPrice = 70, PriceRateID = 0, UseManualPrice = true });
            data.Reservations.Add(new Reservation() { ID = 6, Arrival = 8, Nights = 9, Guests = new List<int>(new int[] { 6 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 7, Arrival = 0, Nights = 12, Guests = new List<int>(new int[] { 7 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 8, Arrival = 12, Nights = 5, Guests = new List<int>(new int[] { 8 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 9, Arrival = 3, Nights = 4, Guests = new List<int>(new int[] { 9 }), ContractID = 0, EntityID = 0, HouseUse = false, ManualPrice = 90, PriceRateID = 0, UseManualPrice = true });
            data.Reservations.Add(new Reservation() { ID = 10, Arrival = 8, Nights = 2, Guests = new List<int>(new int[] { 10 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 11, Arrival = 5, Nights = 4, Guests = new List<int>(new int[] { 11 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 12, Arrival = 2, Nights = 5, Guests = new List<int>(new int[] { 12 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 13, Arrival = 5, Nights = 4, Guests = new List<int>(new int[] { 13 }), ContractID = 0, EntityID = 0, HouseUse = true, ManualPrice = 60, PriceRateID = 0, UseManualPrice = true });
            data.Reservations.Add(new Reservation() { ID = 14, Arrival = 8, Nights = 7, Guests = new List<int>(new int[] { 14 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 15, Arrival = 0, Nights = 8, Guests = new List<int>(new int[] { 15 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 16, Arrival = 3, Nights = 3, Guests = new List<int>(new int[] { 16 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 17, Arrival = 1, Nights = 2, Guests = new List<int>(new int[] { 17 }), ContractID = 0, EntityID = 0, HouseUse = false, ManualPrice = 45, PriceRateID = 0, UseManualPrice = true });
            data.Reservations.Add(new Reservation() { ID = 18, Arrival = 3, Nights = 6, Guests = new List<int>(new int[] { 18 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 19, Arrival = 9, Nights = 9, Guests = new List<int>(new int[] { 19 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 20, Arrival = 2, Nights = 3, Guests = new List<int>(new int[] { 20 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 21, Arrival = 0, Nights = 4, Guests = new List<int>(new int[] { 21 }), ContractID = 0, EntityID = 0, HouseUse = false, ManualPrice = 50, PriceRateID = 0, UseManualPrice = true });
            data.Reservations.Add(new Reservation() { ID = 22, Arrival = 5, Nights = 5, Guests = new List<int>(new int[] { 22 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 23, Arrival = 0, Nights = 10, Guests = new List<int>(new int[] { 23 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 24, Arrival = 4, Nights = 1, Guests = new List<int>(new int[] { 24 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 25, Arrival = 3, Nights = 2, Guests = new List<int>(new int[] { 25 }), ContractID = 0, EntityID = 0, HouseUse = false, ManualPrice = 65, PriceRateID = 0, UseManualPrice = true });
            data.Reservations.Add(new Reservation() { ID = 26, Arrival = 7, Nights = 4, Guests = new List<int>(new int[] { 26 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 27, Arrival = 4, Nights = 5, Guests = new List<int>(new int[] { 27 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 28, Arrival = 9, Nights = 7, Guests = new List<int>(new int[] { 28 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 29, Arrival = 2, Nights = 9, Guests = new List<int>(new int[] { 29 }), ContractID = 0, EntityID = 0, HouseUse = false, ManualPrice = 60, PriceRateID = 0, UseManualPrice = true });
            data.Reservations.Add(new Reservation() { ID = 30, Arrival = 0, Nights = 6, Guests = new List<int>(new int[] { 30 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 31, Arrival = 1, Nights = 7, Guests = new List<int>(new int[] { 31 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 32, Arrival = 6, Nights = 3, Guests = new List<int>(new int[] { 32 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 33, Arrival = 3, Nights = 6, Guests = new List<int>(new int[] { 33 }), ContractID = 0, EntityID = 0, HouseUse = false, ManualPrice = 50, PriceRateID = 0, UseManualPrice = true });
            data.Reservations.Add(new Reservation() { ID = 34, Arrival = 3, Nights = 7, Guests = new List<int>(new int[] { 34 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 35, Arrival = 0, Nights = 7, Guests = new List<int>(new int[] { 35 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 36, Arrival = 11, Nights = 8, Guests = new List<int>(new int[] { 36 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 37, Arrival = 2, Nights = 2, Guests = new List<int>(new int[] { 37 }), ContractID = 0, EntityID = 0, HouseUse = false, ManualPrice = 75, PriceRateID = 0, UseManualPrice = true });
            data.Reservations.Add(new Reservation() { ID = 38, Arrival = 7, Nights = 4, Guests = new List<int>(new int[] { 38 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 39, Arrival = 1, Nights = 5, Guests = new List<int>(new int[] { 39 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 40, Arrival = 3, Nights = 12, Guests = new List<int>(new int[] { 40 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 41, Arrival = 0, Nights = 4, Guests = new List<int>(new int[] { 41 }), ContractID = 0, EntityID = 0, HouseUse = false, ManualPrice = 80, PriceRateID = 0, UseManualPrice = true });
            data.Reservations.Add(new Reservation() { ID = 42, Arrival = 2, Nights = 7, Guests = new List<int>(new int[] { 42 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 43, Arrival = 4, Nights = 2, Guests = new List<int>(new int[] { 43 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 44, Arrival = 6, Nights = 3, Guests = new List<int>(new int[] { 44 }), ContractID = 1, EntityID = 1, HouseUse = false, ManualPrice = 0, PriceRateID = 1, UseManualPrice = false });
            data.Reservations.Add(new Reservation() { ID = 45, Arrival = 8, Nights = 7, Guests = new List<int>(new int[] { 45 }), ContractID = 0, EntityID = 0, HouseUse = false, ManualPrice = 50, PriceRateID = 0, UseManualPrice = true });
            #endregion
            #region Seasons
            data.Seasons.Add(new Season() { ID = 1, Description = "High", MinValue = 90, MaxValue = 100, Average = 95, Color = "25,25,25,25" });
            data.Seasons.Add(new Season() { ID = 2, Description = "Medium", MinValue = 50, MaxValue = 89, Average = 60, Color = "25,120,120,120" });
            data.Seasons.Add(new Season() { ID = 3, Description = "Low", MinValue = 0, MaxValue = 49, Average = 30, Color = "25,200,200,200" });
            #endregion

            return data;
        }
    }
}
