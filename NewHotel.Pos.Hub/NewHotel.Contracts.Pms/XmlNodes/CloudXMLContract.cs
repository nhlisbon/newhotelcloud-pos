﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class CloudXMLContract : BaseContract
    {
        [DataMember]
        public string FileContent { get; set; }
    }
}
