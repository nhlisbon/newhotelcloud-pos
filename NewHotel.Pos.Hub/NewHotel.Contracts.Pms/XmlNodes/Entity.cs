﻿using System;

namespace XMLGenerator
{
    [Serializable]
    public class Entity : BaseNode
    {
        public string ComercialName { get; set; }
        public string Country { get; set; }
        public string Abbreviation { get; set; }
        public string Description { get; set; }
        public string PlanningColor { get; set; }
        public long? Language { get; set; }

        public string HomeAddress1 { get; set; }
        public string HomeAddress2 { get; set; }
        public string HomeCountry { get; set; }
        public string HomeLocation { get; set; }
        public string HomePostalCode { get; set; }

        public string FiscalAddress1 { get; set; }
        public string FiscalAddress2 { get; set; }
        public string FiscalCountry { get; set; }
        public string FiscalLocation { get; set; }
        public string FiscalPostalCode { get; set; }

        public string HomePhone { get; set; }
        public string BusinessPhone { get; set; }
        public string MobilePhone { get; set; }
        public string FaxNumber { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string SkypeName { get; set; }
        public string FiscalNumber { get; set; }
    }
}