﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XMLGenerator
{
    [Serializable]
    public class PriceRate : BaseNode
    {
        /// <summary>
        /// Name (In Hotel Default Language)
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Number of Months starting from Workdate with Price
        /// </summary>
        public int Months { get; set; }
        /// <summary>
        /// Base Price
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// Additional Value Per Pax (>1)
        /// </summary>
        public decimal AdditionalPerPax { get; set; }
        /// <summary>
        /// Indicates if is a Bar Price Rate
        /// </summary>
        public bool IsBar { get; set; }
        /// <summary>
        /// ID of the Bar1 Rate
        /// </summary>
        public int Bar1ID { get; set; }
        /// <summary>
        /// ID of the Bar2 Rate
        /// </summary>
        public int Bar2ID { get; set; }
        /// <summary>
        /// ID of the Bar3 Rate
        /// </summary>
        public int Bar3ID { get; set; }
    }
}
