﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Contracts;

namespace XMLGenerator
{
    [Serializable]
    public class Movement : BaseNode
    {
        /// <summary>
        /// Value
        /// </summary>
        public decimal Value { get; set; }
        /// <summary>
        /// Quantity
        /// </summary>
        public short Quantity { get; set; }
        /// <summary>
        /// Folder
        /// </summary>
        public DailyAccountType Folder { get; set; }
    }
}
