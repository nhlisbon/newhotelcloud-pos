﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XMLGenerator
{
    [Serializable]
    public class Season : BaseNode
    {
        /// <summary>
        /// Season Description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Minimun Value
        /// </summary>
        public short MinValue { get; set; }
        /// <summary>
        /// Maximun Value
        /// </summary>
        public short MaxValue { get; set; }
        /// <summary>
        /// Average Value
        /// </summary>
        public decimal Average { get; set; }
        /// <summary>
        /// Season Color
        /// </summary>
        public string Color { get; set; }
    }
}
