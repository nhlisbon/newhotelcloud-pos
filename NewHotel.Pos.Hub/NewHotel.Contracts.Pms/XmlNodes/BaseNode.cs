﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XMLGenerator
{
    [Serializable]
    public class BaseNode
    {
        /// <summary>
        /// Identifier
        /// </summary>
        public int ID { get; set; }
    }
}
