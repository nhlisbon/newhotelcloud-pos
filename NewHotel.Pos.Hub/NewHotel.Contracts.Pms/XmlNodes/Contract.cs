﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XMLGenerator
{
    [Serializable]
    public class Contract : BaseNode
    {
        /// <summary>
        /// Contract Abbreviation
        /// </summary>
        public string Abbreviation { get; set; }
        /// <summary>
        /// Contract Description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Entity related to Contract
        /// </summary>
        public int EntityID { get; set; }
        /// <summary>
        /// Price Rate related to Contract
        /// </summary>
        public int PriceRateID { get; set; }
    }
}
