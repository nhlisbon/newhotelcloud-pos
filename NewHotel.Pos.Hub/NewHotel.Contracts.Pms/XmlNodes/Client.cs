﻿using System;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace XMLGenerator
{
    [Serializable]
    public class Client : BaseNode
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LastName2 { get; set; }
        public GenderType Gender { get; set; }
        public CivilState CivilState { get; set; }
        public DateTime? Birthdate { get; set; }
        public short ChildrenNumber { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }

        public string HomeAddress1 { get; set; }
        public string HomeAddress2 { get; set; }
        public string HomeCountry { get; set; }
        public string HomeLocation { get; set; }
        public string HomePostalCode { get; set; }

        public string FiscalAddress1 { get; set; }
        public string FiscalAddress2 { get; set; }
        public string FiscalCountry { get; set; }
        public string FiscalLocation { get; set; }
        public string FiscalPostalCode { get; set; }

        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string FaxNumber { get; set; }
        public string Email { get; set; }

        public string FiscalNumber { get; set; }
        public string FiscalRegistry { get; set; }

        public string Country { get; set; }

        public Blob? Image { get; set; }
    }
}