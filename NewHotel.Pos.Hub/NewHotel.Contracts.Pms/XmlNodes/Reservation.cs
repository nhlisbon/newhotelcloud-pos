﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XMLGenerator
{
    [Serializable]
    public class Reservation : BaseNode
    {
        public Reservation()
        {
            Guests = new List<int>();
            RoomCharges = new List<Movement>();
            DrinkCharges = new List<Movement>();
            FoodCharges = new List<Movement>();
        }
        /// <summary>
        /// Days from Workdate
        /// </summary>
        public int Arrival { get; set; }
        /// <summary>
        /// Number of Nights
        /// </summary>
        public int Nights { get; set; }
        /// <summary>
        /// List of Guests (ID)
        /// </summary>
        public List<int> Guests { get; set; }
        /// <summary>
        /// ID of the Room
        /// </summary>
        public int RoomID { get; set; }
        /// <summary>
        /// Indicates de Index in Guests of the Holder
        /// </summary>
        public int HolderIndex { get; set; }
        /// <summary>
        /// Use Manual Price or Rate Price
        /// </summary>
        public bool UseManualPrice { get; set; }
        /// <summary>
        /// Price Rate. In case UseManualPrice is False
        /// </summary>
        public int PriceRateID { get; set; }
        /// <summary>
        /// Manual Price. In case UseManualPrice is True
        /// </summary>
        public decimal ManualPrice { get; set; }
        /// <summary>
        /// Entity. Leave 0 for default.
        /// </summary>
        public int EntityID { get; set; }
        /// <summary>
        /// Contract. Leave 0 for default.
        /// </summary>
        public int ContractID { get; set; }
        /// <summary>
        /// Will Ignore all Prices if set to True
        /// </summary>
        public bool HouseUse { get; set; }
        
        //Movements
        public List<Movement> RoomCharges { get; set; }
        public List<Movement> DrinkCharges { get; set; }
        public List<Movement> FoodCharges { get; set; }
    }
}
