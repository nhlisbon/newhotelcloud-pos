﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace XMLGenerator
{
    [Serializable]
    public class Room : BaseNode
    {
        /// <summary>
        /// Door Number
        /// </summary>
        public string Door { get; set; }
        /// <summary>
        /// Status
        /// </summary>
        public RoomStatus Status { get; set; }
        /// <summary>
        /// Orden (To sort the Rooms)
        /// </summary>
        public short Orden { get; set; }
    }
}
