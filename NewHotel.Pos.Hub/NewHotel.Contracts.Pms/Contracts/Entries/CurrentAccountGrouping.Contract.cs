﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class CurrentAccountGroupingContract : BaseContract
    {
        [DataMember]
        public CurrentAccountGroupingFlags DefaultGrouping { get; internal set; }

        public CurrentAccountGroupingContract(CurrentAccountGroupingFlags defaultGrouping)
        {
            DefaultGrouping = defaultGrouping;
        }
    }
}