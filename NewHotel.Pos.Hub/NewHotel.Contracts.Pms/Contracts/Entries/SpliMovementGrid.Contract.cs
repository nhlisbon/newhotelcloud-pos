﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	public class SplitMovementGridContract : BaseContract
    {
        #region Members

		[DataMember]
		internal Guid _movementContractId;
		[DataMember]
		internal string _description;

        private decimal _initialAmount;
        private decimal _firstAmount;

        #endregion
        #region Constructor

        public SplitMovementGridContract(MovementContract movementContract)
        {
			_movementContractId = (Guid)movementContract.Id;
            _description = movementContract.ServiceDesc;
			InitialAmount = (movementContract.TaxIncluded ?? true) ? movementContract.GrossValue : movementContract.NetValue;
        }

        #endregion
        #region Properties

		public Guid MovementContractId { get { return _movementContractId; } }
        public string Description { get { return _description; } }

        [DataMember]
        public decimal InitialAmount
		{ 
			get { return _initialAmount; }
			set
			{
				if (Set(ref _initialAmount, value, "InitialAmount"))
					FirstAmount = _initialAmount;
			}
		}

		[DataMember]
		public decimal FirstAmount
		{
			get { return _firstAmount; }
			set
			{
				if (Set(ref _firstAmount, value, "FirstAmount"))
					NotifyPropertyChanged("SecondAmount");
			}
		}

        public decimal SecondAmount
		{
			get { return InitialAmount - FirstAmount; }
		}

        #endregion
	}
}
