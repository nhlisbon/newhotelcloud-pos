﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class MovementBaseContract : BaseContract
    {
        #region Members

        private Guid? _departmentId;
        private Guid? _serviceByDepartmentId;
        private ReceivableType? _receivableType;
        private short? _quantity;
        private string _currency;
        private DailyAccountType _accountFolder;
        private Guid? _currentAccountId;
        private string _currentAccountDescription;
        private CurrentAccountType? _currentAccountType;
        private CreditCardContract _creditCard;
        private CreditCardContract _reservationCreditCard;
        internal decimal? _discountValue;
        private Guid? _withdrawType;
        private Guid? _entityId;
        private EntrieType _type;
        private Guid? _taxSchemaId;
        private bool? _taxIncluded;
        [DataMember]
        internal CurrencyInstallationContract _baseCurrency;

        #endregion
        #region Persistent Properties

        [DataMember]
        public DateTime RegistrationDateTime { get; set; }
        [DataMember]
        public DateTime WorkDate { get; set; }
        [DataMember]
        public short WorkShift { get; set; }
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Value date required.")]
        public DateTime ValueDate { get; set; }
        [DataMember]
        public EntrieType Type
        {
            get { return _type; }
            set { Set(ref _type, value, "Type"); }
        }
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Department required.")]
        public Guid? DepartmentId
        {
            get { return _departmentId; }
            set { Set(ref _departmentId, value, "DepartmentId"); }
        }
        [DataMember]
        public Guid? ServiceByDepartmentId
        {
            get { return _serviceByDepartmentId; }
            set { Set(ref _serviceByDepartmentId, value, "ServiceByDepartmentId"); }
        }
        [DataMember]
        public short? Quantity
        {
            get { return _quantity; }
            set { Set(ref _quantity, value, "Quantity"); }
        }
        [DataMember]
        public string Currency
        {
            get { return _currency; }
            set { Set<string>(ref _currency, value, "Currency"); }
        }
        [DataMember]
        public decimal GrossValue { get; set; }
        [DataMember]
        public decimal NetValue { get; set; }
        [DataMember]
        public decimal? CommissionValue { get; set; }
        [DataMember]
        public decimal? ForeignCurrencyValue { get; set; }
        [DataMember]
        public decimal? ExchangeRateValue { get; set; }
        [DataMember]
        public bool ExchangeMultiplySymbol { get; set; }
        [DataMember]
        public string ForeignCurrency { get; set; }
        [DataMember]
        public decimal? UndiscountValue { get; set; }
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Current account required.")]
        public Guid? CurrentAccountId
        {
            get { return _currentAccountId; }
            set { Set<Guid?>(ref _currentAccountId, value, "CurrentAccountId"); }
        }
        [DataMember]
        public CurrentAccountType? CurrentAccountType
        {
            get { return _currentAccountType; }
            set { Set(ref _currentAccountType, value, "CurrentAccountType"); }
        }
        [DataMember]
        public string CurrentAccountDescription
        {
            get { return _currentAccountDescription; }
            set { Set(ref _currentAccountDescription, value, "CurrentAccountDescription"); }
        }
        [DataMember]
        public DailyAccountType AccountFolder
        {
            get { return _accountFolder; }
            set { Set(ref _accountFolder, value, "AccountFolder"); }
        }
        [DataMember]
        public bool Cancelled { get; set; }
        [DataMember]
        public bool Corrected { get; set; }
        [DataMember]
        public bool IsAuto { get; set; }
        [DataMember]
        public bool IsDaily { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string DocNumber { get; set; }
        [DataMember]
        public string PhoneNumber { get; set; }
        [DataMember]
        public short? PensionMode { get; set; }
        [DataMember]
        public Guid? PriceRateId { get; set; }
        [DataMember]
        public ReceivableType? Receivable
        {
            get { return _receivableType; }
            set { Set(ref _receivableType, value, "Receivable"); }
        }
        [DataMember]
        public bool InAdvance { get; set; }
        [DataMember]
        public Guid? WithdrawType
        {
            get { return _withdrawType; }
            set { Set(ref _withdrawType, value, "WithdrawType"); }
        }
        [DataMember]
        public Guid? ReceiptDocumentId { get; set; }

        [DataMember]
        public Guid? OfficialDocumentId { get; set; }

        [DataMember]
        public OfficialDocContract OfficialDocument { get; set; }

        [DataMember(Name = "DiscountPercent")]
        internal decimal? _discountPercent;
        public decimal? DiscountPercent
        {
            get { return _discountPercent; }
            set
            {
                Set(ref _discountPercent, value, "DiscountPercent");
                if (_discountPercent.HasValue)
                    _discountValue = null;
            }
        }

        [DataMember(Name = "DiscountValue")]
        public decimal? DiscountValue
        {
            get { return _discountValue; }
            set
            {
                Set<decimal?>(ref _discountValue, value, "DiscountValue");
                if (_discountValue.HasValue)
                    _discountPercent = null;
            }
        }

        [ReflectionExclude]
        public DiscountType DiscountType
        {
            get { return _discountPercent.HasValue ? DiscountType.Percent : DiscountType.Value; }
        }

        [DataMember]
        public Guid? EntityId
        {
            get { return _entityId; }
            set { Set(ref _entityId, value, "EntityId"); }
        }

        [DataMember]
        public Guid? TaxSchemaId
        {
            get { return _taxSchemaId; }
            set { Set(ref _taxSchemaId, value, "TaxSchemaId"); }
        }

        [DataMember]
        public bool? TaxIncluded
        {
            get { return _taxIncluded; }
            set { Set(ref _taxIncluded, value, "TaxIncluded"); }
        }
        [DataMember]
        public long ApplicationId { get; set; }

        #endregion

        #region Parameters
        public CurrencyInstallationContract BaseCurrency { get { return _baseCurrency; } }
        [DataMember]
        public bool PaymentMethodVisibility { get; set; }
        #endregion
        #region Contracts

        [ReflectionExclude]
        public TypedList<MovementTaxDetailContract> TaxDetailMovements { get; private set; }

        [ReflectionExclude]
        public TypedList<MovementCashFlowContract> CashFlows { get; private set; }

        [ReflectionExclude]
        [DataMember]
        public CreditCardContract CreditCard
        {
            get { return _creditCard; }
            set { Set(ref _creditCard, value, "CreditCard"); }
        }

        [ReflectionExclude]
        [DataMember]
        public CreditCardContract ReservationCreditCard
        {
            get { return _reservationCreditCard; }
            set { Set(ref _reservationCreditCard, value, "ReservationCreditCard"); }
        }

        #endregion
        #region Constructors

        public MovementBaseContract(CurrencyInstallationContract baseCurrency)
            : base()
        {
            TaxDetailMovements = new TypedList<MovementTaxDetailContract>();
            CashFlows = new TypedList<MovementCashFlowContract>();
            _baseCurrency = baseCurrency;
        }

        public MovementBaseContract(DateTime workDate, DateTime valueDate, Guid? service, DailyAccountType accountType,
            short? pensionMode, Guid? priceRate, short? quantity, string movementCurrency,
            decimal? exchangeRateValue, bool exchangeRateMult,
            decimal? commissionValue, decimal? disscountPercent, CurrencyInstallationContract baseCurrency)
            : this(baseCurrency)
        {
            WorkDate = workDate;
            ValueDate = valueDate;
            ServiceByDepartmentId = service;
            AccountFolder = accountType;
            PensionMode = pensionMode;
            PriceRateId = priceRate;
            Quantity = quantity;
            Currency = movementCurrency;
            ExchangeRateValue = exchangeRateValue;
            ExchangeMultiplySymbol = exchangeRateMult;
            CommissionValue = commissionValue;
            DiscountPercent = disscountPercent;
        }

        #endregion

        public override object Clone()
        {
            var contract = (BaseContract)Activator.CreateInstance(GetType(), BaseCurrency);
            this.AssignTo(contract);
            return contract;
        }
    }
}
