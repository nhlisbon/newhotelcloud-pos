﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [CustomValidation(typeof(MovementContract), "ValidateMovement")]
    public class MovementContract : MovementBaseContract
    {
        #region Members

        private decimal? _movementValue;
        private decimal? _movementBaseValue;
        private string _movementCurrency;
        private bool _taxIncluded;

        #endregion
        #region Constructors

        public MovementContract(CancellationControlContract cancellationControl, CurrencyInstallationContract baseCurrency)
            : base(baseCurrency)
        {
            CancellationControl = cancellationControl;
            GreatherThanZero = true;
            EqualZero = true;
            LessThanZero = true;
           
        }

        public MovementContract(CurrencyInstallationContract baseCurrency)
            : this(null, baseCurrency) { GreatherThanZero = true; EqualZero = true; LessThanZero = true; }

        #endregion
        #region Persistent Properties

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Currency required.")]
        public string MovementCurrency
        {
            get { return _movementCurrency; }
            set { Set<string>(ref _movementCurrency, value, "MovementCurrency"); }
        }
        [DataMember]
        public decimal? MovementValue
        {
            get { return _movementValue; }
            set { Set<decimal?>(ref _movementValue, value, "MovementValue"); }
        }
        [DataMember]
        public decimal? MovementBaseValue
        {
            get { return _movementBaseValue; }
            set { Set<decimal?>(ref _movementBaseValue, value, "MovementBaseValue"); }
        }

        [DataMember]
        public Guid? UserId { get; set; }
        [DataMember]
        public string UserLogin { get; set; }

        #endregion

        [ReflectionExclude]
        public string ServiceDesc { get; set; }

        [DataMember]
        public bool LessThanZero { get; set; }
        [DataMember]
        public bool GreatherThanZero { get; set; }
        [DataMember]
        public bool EqualZero { get; set; }

        [ReflectionExclude]
        [DataMember]
        public CancellationControlContract CancellationControl { get; set; }

        [DataMember]
        public Guid CurrentAccountOriginId { get; set; }
        [DataMember]
        public string TransferedDescription { get; set; }

        public bool IsTransfered { get { return (CurrentAccountId.HasValue && !Cancelled && !CurrentAccountId.Value.Equals(CurrentAccountOriginId)); } }

        [DataMember]
        public bool ForceInsert { get; set; }

        [DataMember]
        public PaymentServiceProvider PaymentProvider { get; set; }

        [DataMember]
        public Guid? PaymentMethodDefaultId { get; set; }

		[DataMember]
		public bool InAdvanceInvoice { get; set; }

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateMovement(MovementContract obj)
        {
            //if(obj.MovementCurrency != obj.BaseCurrency.CurrencyId && !obj.ExchangeRateValue.HasValue)
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Foreing Currency Exchange Missing");

            if (obj.MovementValue.HasValue && obj.MovementValue.Value > 0 && !obj.GreatherThanZero) return new System.ComponentModel.DataAnnotations.ValidationResult("Value not allowed");
            if (obj.MovementValue.HasValue && obj.MovementValue.Value < 0 && !obj.LessThanZero) return new System.ComponentModel.DataAnnotations.ValidationResult("Value not allowed");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }


    [DataContract]
    [CustomValidation(typeof(MovementReservationContract), "ValidateMovementReservationContract")]
    public class MovementReservationContract : MovementContract
    {
        private bool _selectedAdvancedDeposit;
        private bool _selectedGroup;
        private bool _selectedReservations;
        private bool _isGroupAdvanced;

        public MovementReservationContract( CurrencyInstallationContract baseCurrency)
            : base(baseCurrency)
        {
            AccountFolder = DailyAccountType.Master;
            Type = EntrieType.Debit;
            Receivable = ReceivableType.Payment;
            MovementValue = 0;
            PaymentProvider = PaymentServiceProvider.Manual;

        }
        [ReflectionExclude]
        [DataMember]
        public ReservationHolderInfoContract ReservationHolderInfoContract { get; set; }
        [DataMember]
        public bool SelectedAdvancedDeposit { get { return _selectedAdvancedDeposit; } set { Set<bool>(ref _selectedAdvancedDeposit, value, "SelectedAdvancedDeposit"); } }
        [DataMember]
        public bool SelectedGroup { get { return _selectedGroup; } set { Set<bool>(ref _selectedGroup, value, "SelectedGroup"); } }
        [DataMember]
        public bool SelectedReservations { get { return _selectedReservations; } set { Set<bool>(ref _selectedReservations, value, "SelectedReservations"); } }
        [DataMember]
        public bool IsGroupAdvanced { get { return _isGroupAdvanced; } set { Set<bool>(ref _isGroupAdvanced, value, "IsGroupAdvanced"); } }

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateMovementReservationContract(MovementReservationContract obj)
        {
            if (obj.SelectedAdvancedDeposit &&  !obj.SelectedGroup && !obj.SelectedReservations)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Select advanced deposit type: Group or n-Reservations");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

    }
}
