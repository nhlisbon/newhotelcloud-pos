﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Contracts
{
    public class MultiContractItem
    {
        public Guid? CurrentAccountId { get; set; }
        public string DocumentNum { get; set; }
        public short? Quantity { get; set; }
        public decimal? EntryValue { get; set; }
        
        public MultiContractItem() { }
    }

    public class MovementMultiContract : BaseContract
    {
        public MovementMultiContract()
            : base() 
        {
            Folder = DailyAccountType.Master;
            _items = new TypedList<MultiContractItem>();
        }

        public DateTime ValueDate { get; set; }
        public string EntryCurrency;
        public decimal? ExchangeValue { get; set; }
        public DailyAccountType Folder { get; set; }
        public Guid? ServiceByDepartmentId { get; set; }
        public string Description { get; set; }

        private TypedList<MultiContractItem> _items;
        [ReflectionExclude()]
        public TypedList<MultiContractItem> Items { get; private set; }
    }
}
