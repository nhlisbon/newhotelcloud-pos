﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Globalization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class MovementPriceSummarizedContract : BaseContract
    {
        #region Members

        private decimal _movementValue;
        private decimal _movementNetValue;
        private bool _taxIncluded;
        [DataMember]
        internal bool _invalidPrice;

        #endregion
        #region Properties

        [DataMember]
        public DailyAccountType AccountFolder { get; internal set; }
        [DataMember]
        public string AccountFolderDesc { get; internal set; }
        [DataMember]
        public DateTime MovementDate { get; internal set; }
        [DataMember]
        public Guid? DepartmentId { get; internal set; }
        [DataMember]
        public string DepartmentDesc { get; internal set; }
        [DataMember]
        public Guid? ServiceId { get; internal set; }
        [DataMember]
        public Guid? ServiceByDepartmentId { get; internal set; }
        [DataMember]
        public string ServiceDesc { get; internal set; }
        [DataMember]
        public decimal? DiscountPercent { get; internal set; }
        [DataMember]
        public decimal Discount { get; internal set; }
        [DataMember]
        public string MovementCurrency { get; internal set; }
        [DataMember]
        public decimal MovementValue
        {
            get { return _movementValue; }
            set { Set(ref _movementValue, value, "MovementValue"); }
        }
        [DataMember]
        public decimal MovementNetValue
        {
            get { return _movementNetValue; }
            set { Set(ref _movementNetValue, value, "MovementNetValue"); }
        }
        [DataMember]
        public bool TaxIncluded
        {
            get { return _taxIncluded; }
            set { Set<bool>(ref _taxIncluded, value, "TaxIncluded"); }
        }
        public bool InvalidPrice
        {
            get { return _invalidPrice; }
        }

        #endregion

        #region Constructors

        public MovementPriceSummarizedContract(DateTime movementDate,
            DailyAccountType accountFolder, string accountFolderDesc,
            Guid? departmentId, string departmentDesc, Guid? serviceId, string serviceDesc,
            decimal? discountPerc, decimal discount, decimal movementValue, decimal movementNetValue, bool invalidPrice,
            Guid? servicebyDepartament, string currency)
            : base() 
        {
            MovementDate = movementDate;
            AccountFolder = accountFolder;
            AccountFolderDesc = accountFolderDesc;
            DepartmentId = departmentId;
            DepartmentDesc = departmentDesc;
            ServiceId = serviceId;
            ServiceDesc = serviceDesc;
            DiscountPercent = discountPerc;
            Discount = discount;
            _movementValue = movementValue;
            _movementNetValue = movementNetValue;
            _invalidPrice = invalidPrice;
            ServiceByDepartmentId = servicebyDepartament;
            MovementCurrency = currency;
        }

        #endregion
    }
}
