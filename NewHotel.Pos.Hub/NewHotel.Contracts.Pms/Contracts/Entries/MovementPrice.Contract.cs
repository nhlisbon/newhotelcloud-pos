﻿using System;
using System.Collections.ObjectModel;
using NewHotel.Contracts;

namespace NewHotel.Contracts
{
    public class MovementPriceContract : MovementBaseContract, IMovementPrice
    {
        #region Properties

        public MovementSource? MovementSource { get; private set; }
        public PersonType? TypeLine { get; private set; }
        public MovementType MovementType { get; private set; }
        public Guid? RoomTypeId { get; private set; }
        public HalfBoardEatMode? HalfPensionMode { get; private set; }
        public decimal MovementValue { get; set; }

        #endregion
        #region Extended Properties

        public bool IsBoard
        {
            get { return PensionMode.HasValue && MovementType != MovementType.Room && MovementType != MovementType.Other; }
        }

        public decimal Discount
        {
            get
            {
                if (DiscountPercent.HasValue)
                    return (UndiscountValue ?? decimal.Zero) * DiscountPercent.Value / 100;

                return DiscountValue ?? decimal.Zero;
            }
        }

        #endregion
        #region Methods

        public void SetCurrentAccountId(Guid? currentAccountId)
        {
            CurrentAccountId = currentAccountId;
        }

        #endregion
        #region Constructors

        public MovementPriceContract(CurrencyInstallationContract baseCurrency)
            : base(baseCurrency) { }

        public MovementPriceContract(DateTime workDate, DateTime valueDate, Guid? department, Guid? service,
            MovementType movementType, MovementSource? movementSource, DailyAccountType accountType,
            short? pensionMode, Guid? priceRate, short? quantity, string movementCurrency,
            decimal movementValue, decimal? exchangeRateValue, bool exchangeRateMult, decimal? commissionValue,
            decimal? disscountPercent, PersonType? typeLine,
            Guid? roomTypeId, HalfBoardEatMode? halfPensionMode,
            CurrencyInstallationContract baseCurrency)
            : base(workDate, valueDate, service, accountType, pensionMode, priceRate, quantity,
            movementCurrency, exchangeRateValue, exchangeRateMult, commissionValue, disscountPercent, baseCurrency)
        {
            Type = EntrieType.Credit;
            DepartmentId = department;
            MovementType = movementType;
            MovementSource = movementSource;
            MovementValue = movementValue;
            //GrossValue = movementValue;
            //NetValue = movementValue;
            TypeLine = typeLine;
            RoomTypeId = roomTypeId;
            HalfPensionMode = halfPensionMode;

            var undiscountValue = movementValue;
            var currencyId = baseCurrency.CurrencyId;
            if (movementCurrency != currencyId)
            {
                Currency = currencyId;
                ForeignCurrency = movementCurrency;
                ForeignCurrencyValue = movementValue;
                undiscountValue = exchangeRateMult
                    ? movementValue * (exchangeRateValue ?? decimal.One)
                    : movementValue / (exchangeRateValue ?? decimal.One);
            }

            UndiscountValue = undiscountValue;
            GrossValue = undiscountValue;
            NetValue = undiscountValue;
        }

        #endregion
    }
}
