﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class HotelInitDefaultContract : BaseContract
    {
        private long _initType;
        private long? _initExtraType;
        private bool _included;

        public HotelInitDefaultContract()
        {
            Description = new LanguageTranslationContract();
            Included = true;
        }

        [ReflectionExclude]
        [DataMember]
        public LanguageTranslationContract Description { get; internal set; }
        [DataMember]
        public long InitType { get { return _initType; } set { Set(ref _initType, value, "InitType"); } }
        [DataMember]
        public long? InitExtraType { get { return _initExtraType; } set { Set(ref _initExtraType, value, "InitExtraType"); } }
        [DataMember]
        public bool Included { get { return _included; } set { Set(ref _included, value, "Included"); } }

        public string DescriptionTranslated { get { return (string)Description; } }

        //Just to be used on Person Titles
        public string MaleDescription { get { return ((string)Description).Split(',')[0]; } }
        public string FemaleDescription { get { return ((string)Description).Split(',')[1]; } }
    }
}
