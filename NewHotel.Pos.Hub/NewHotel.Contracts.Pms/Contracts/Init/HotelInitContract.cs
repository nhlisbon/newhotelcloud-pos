﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(HotelInitContract), "ValidateHotelInit")]
    public class HotelInitContract : BaseContract
    {
        #region Members

        private DateTime _minWorkDate;
        private DateTime _workDate;
        private long _allowedRooms;

        private bool _ro;
        private bool _bb;
        private bool _hb;
        private bool _fb;
        private bool _ai;

        private bool _sampleData;

        private bool _hasWelcomeMail;
        private bool _hasFeedbackMail;
        private bool _hasConfirmationMail;

        private string _welcomeMail;
        private string _feedbackMail;
        private string _confirmationMail;

        #endregion
        #region Constructor

        public HotelInitContract(DateTime workDate)
        {
            MinWorkDate = workDate;
            WorkDate = workDate;

            PersonTitles = new TypedList<HotelInitDefaultContract>();
            OriginsDefault = new TypedList<HotelInitDefaultDualContract>();
            SegmentsDefault = new TypedList<HotelInitDefaultDualContract>();
            ClientTypesDefault = new TypedList<HotelInitDefaultContract>();
            AttentionsDefault = new TypedList<HotelInitDefaultContract>();
            GuestCategoryDefault = new TypedList<HotelInitDefaultContract>();
            EntityTypesDefault = new TypedList<HotelInitDefaultContract>();
            DepartmentsDefault = new TypedList<HotelInitDefaultContract>();
            PaymentMethodsDefault = new TypedList<HotelInitDefaultContract>();
            CancellationReasonsDefault = new TypedList<HotelInitDefaultContract>();
            GuarantiesDefault = new TypedList<HotelInitDefaultContract>();
            UnexpectedDeparturesDefault = new TypedList<HotelInitDefaultContract>();
            GroupTypesDefault = new TypedList<HotelInitDefaultContract>();
            RoomFeaturesDefault = new TypedList<HotelInitDefaultContract>();
            BedsCotsDefault = new TypedList<HotelInitDefaultContract>();

            RO = true;
            BB = true;
            HB = true;
            FB = true;
            AI = true;

            #region  Rate BreakDown

            Room = new HotelInitBreakdownContract();

            BreakfastDrink = new HotelInitBreakdownContract()
            {
                AdultVisible = true,
                ChildVisible = true,
                AdultPrice = 2,
                ChildPrice = 2
            };

            BreakfastFood = new HotelInitBreakdownContract()
            {
                AdultVisible = true,
                ChildVisible = true,
                AdultPrice = 3,
                ChildPrice = 3
            };

            LunchDrink = new HotelInitBreakdownContract()
            {
                AdultVisible = true,
                ChildVisible = true,
                AdultPrice = 5,
                ChildPrice = 5
            };

            LunchFood = new HotelInitBreakdownContract()
            {
                AdultVisible = true,
                ChildVisible = true,
                AdultPrice = 5,
                ChildPrice = 5
            };

            DinnerDrink = new HotelInitBreakdownContract()
            {
                AdultVisible = true,
                ChildVisible = true,
                AdultPrice = 5,
                ChildPrice = 5
            };

            DinnerFood = new HotelInitBreakdownContract()
            {
                AdultVisible = true,
                ChildVisible = true,
                AdultPrice = 5,
                ChildPrice = 5
            };

            Phone = new HotelInitBreakdownContract();

            PayTV = new HotelInitBreakdownContract();

            Internet = new HotelInitBreakdownContract();

            #endregion

            TaxesContract = new HotelInitTaxesContract();
        }

        #endregion
        #region Properties

        [DataMember]
        public DateTime MinWorkDate { get { return _minWorkDate; } set { Set(ref _minWorkDate, value, "MinWorkDate"); } }
        [DataMember]
        public DateTime WorkDate { get { return _workDate; } set { Set(ref _workDate, value, "WorkDate"); } }
        [DataMember]
        public long AllowedRooms
        {
            get { return _allowedRooms; }
            set
            {
                if (Set(ref _allowedRooms, value, "AllowedRooms"))
                    InitializeRooms(_allowedRooms);
            }
        }

        [DataMember]
        public bool RO { get { return _ro; } set { Set(ref _ro, value, "RO"); } }
        [DataMember]
        public bool BB { get { return _bb; } set { Set(ref _bb, value, "BB"); } }
        [DataMember]
        public bool HB { get { return _hb; } set { Set(ref _hb, value, "HB"); } }
        [DataMember]
        public bool FB { get { return _fb; } set { Set(ref _fb, value, "FB"); } }
        [DataMember]
        public bool AI { get { return _ai; } set { Set(ref _ai, value, "AI"); } }

        [DataMember]
        public bool SampleData { get { return _sampleData; } set { Set(ref _sampleData, value, "SampleData"); } }

        [DataMember]
        public HotelInitRoomTypeContract FirstRoomType { get; set; }
        [DataMember]
        public HotelInitRoomTypeContract SecondRoomType { get; set; }
        [DataMember]
        public HotelInitRoomTypeContract ThirdRoomType { get; set; }
        [DataMember]
        public HotelInitTaxesContract TaxesContract { get; set; }

        [DataMember]
        public HotelInitBreakdownContract Room { get; set; }
        [DataMember]
        public HotelInitBreakdownContract BreakfastFood { get; set; }
        [DataMember]
        public HotelInitBreakdownContract BreakfastDrink { get; set; }
        [DataMember]
        public HotelInitBreakdownContract LunchFood { get; set; }
        [DataMember]
        public HotelInitBreakdownContract LunchDrink { get; set; }
        [DataMember]
        public HotelInitBreakdownContract DinnerFood { get; set; }
        [DataMember]
        public HotelInitBreakdownContract DinnerDrink { get; set; }
        [DataMember]
        public HotelInitBreakdownContract Phone{ get; set; }
        [DataMember]
        public HotelInitBreakdownContract PayTV { get; set; }
        [DataMember]
        public HotelInitBreakdownContract Internet { get; set; }

        [DataMember]
        public bool HasWelcomeMail { get { return _hasWelcomeMail; } set { Set(ref _hasWelcomeMail, value, "HasWelcomeMail"); } }
        [DataMember]
        public bool HasFeedbackMail { get { return _hasFeedbackMail; } set { Set(ref _hasFeedbackMail, value, "HasFeedbackMail"); } }
        [DataMember]
        public bool HasConfirmationMail { get { return _hasConfirmationMail; } set { Set(ref _hasConfirmationMail, value, "HasConfirmationMail"); } }

        [DataMember]
        public string WelcomeMail { get { return _welcomeMail; } set { Set(ref _welcomeMail, value, "WelcomeMail"); } }
        [DataMember]
        public string FeedbackMail { get { return _feedbackMail; } set { Set(ref _feedbackMail, value, "FeedbackMail"); } }
        [DataMember]
        public string ConfirmationMail { get { return _confirmationMail; } set { Set(ref _confirmationMail, value, "ConfirmationMail"); } }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public List<HotelInitBreakdownContract> Breakdowns
        { 
            get 
            {
                var result = new List<HotelInitBreakdownContract>();
                result.Add(Room);

                if (BB || HB || FB || AI)
                {
                    result.Add(BreakfastFood);
                    result.Add(BreakfastDrink);
                }

                if (HB || FB || AI)
                {
                    result.Add(LunchFood);
                    result.Add(LunchDrink);
                    result.Add(DinnerFood);
                    result.Add(DinnerDrink);
                }

                result.Add(Phone);
                return result;
            }
        }

        #endregion
        #region Lists

        [DataMember]
        public TypedList<HotelInitDefaultContract> PersonTitles { get; set; }
        [DataMember]
        public TypedList<HotelInitDefaultDualContract> OriginsDefault { get; set; }
        [DataMember]
        public TypedList<HotelInitDefaultDualContract> SegmentsDefault { get; set; }
        [DataMember]
        public TypedList<HotelInitDefaultContract> ClientTypesDefault { get; set; }
        [DataMember]
        public TypedList<HotelInitDefaultContract> AttentionsDefault { get; set; }
        [DataMember]
        public TypedList<HotelInitDefaultContract> GuestCategoryDefault { get; set; }
        [DataMember]
        public TypedList<HotelInitDefaultContract> EntityTypesDefault { get; set; }
        [DataMember]
        public TypedList<HotelInitDefaultContract> DepartmentsDefault { get; set; }
        [DataMember]
        public TypedList<HotelInitDefaultContract> PaymentMethodsDefault { get; set; }
        [DataMember]
        public TypedList<HotelInitDefaultContract> CancellationReasonsDefault { get; set; }
        [DataMember]
        public TypedList<HotelInitDefaultContract> GuarantiesDefault { get; set; }
        [DataMember]
        public TypedList<HotelInitDefaultContract> UnexpectedDeparturesDefault { get; set; }
        [DataMember]
        public TypedList<HotelInitDefaultContract> GroupTypesDefault { get; set; }
        [DataMember]
        public TypedList<HotelInitDefaultContract> RoomFeaturesDefault { get; set; }
        [DataMember]
        public TypedList<HotelInitDefaultContract> BedsCotsDefault { get; set; }

        #endregion
        #region Initialize Methods

        private void InitializeRooms(long allowedRooms)
        {
            var roomsPerType = allowedRooms / 3;
            var remainingRooms = allowedRooms - roomsPerType * 3;

            FirstRoomType = new HotelInitRoomTypeContract()
            {
                Active = true,
                Abbreviation = "SLG",
                Description = "Single",
                Min = 1,
                Max = 1,
                Rooms = new TypedList<HotelInitRoomTypeRoomContract>()
            };

            for (int i = 0; i < roomsPerType + remainingRooms; i++)
            {
                FirstRoomType.Rooms.Add(new HotelInitRoomTypeRoomContract()
                {
                    DoorNo = "1" + (i + 1).ToString("00")
                });
            }

            SecondRoomType = new HotelInitRoomTypeContract()
            {
                Active = true,
                Abbreviation = "DBL",
                Description = "Double",
                Min = 1,
                Max = 2,
                Rooms = new TypedList<HotelInitRoomTypeRoomContract>()
            };

            for (int i = 0; i < roomsPerType; i++)
            {
                SecondRoomType.Rooms.Add(new HotelInitRoomTypeRoomContract()
                {
                    DoorNo = "2" + (i + 1).ToString("00")
                });
            }

            ThirdRoomType = new HotelInitRoomTypeContract()
            {
                Active = true,
                Abbreviation = "SUI",
                Description = "Suite",
                Min = 1,
                Max = 2,
                Rooms = new TypedList<HotelInitRoomTypeRoomContract>()
            };

            for (int i = 0; i < roomsPerType; i++)
            {
                ThirdRoomType.Rooms.Add(new HotelInitRoomTypeRoomContract()
                {
                    DoorNo = "3" + (i + 1).ToString("00")
                });
            }
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateHotelInit(HotelInitContract obj)
        {
            if (!obj.RO && !obj.BB && !obj.HB && !obj.FB && !obj.AI)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Select at least one Meal Plan.");

            foreach (var breakDownContract in obj.Breakdowns)
            {
                if (string.IsNullOrEmpty(breakDownContract.Department) ||
                    string.IsNullOrEmpty(breakDownContract.Service))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Department or Service empty in BreakDowns.");
            }

            if (string.IsNullOrEmpty(obj.TaxesContract.Schema))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Schema cannot be empty.");

            if (!obj.TaxesContract.TaxRateId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Tax Rate cannot be empty.");

            if (!obj.RoomFeaturesDefault.Any(x => x.Included))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Select at least one Room Feature.");
            
            if (!obj.GuarantiesDefault.Any(x => x.Included))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Select at least one Warranty.");

            if (!obj.UnexpectedDeparturesDefault.Any(x => x.Included))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Select at least one Unexpected Departure.");

            if (!obj.FirstRoomType.Active &&
                !obj.SecondRoomType.Active &&
                !obj.ThirdRoomType.Active)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Select at least one Room Type.");

            var firstType = obj.FirstRoomType.Active && obj.FirstRoomType.Rooms.Count > 0;
            var secondType = obj.SecondRoomType.Active && obj.SecondRoomType.Rooms.Count > 0;
            var thirdType = obj.ThirdRoomType.Active && obj.ThirdRoomType.Rooms.Count > 0;

            if (!firstType && !secondType && !thirdType)
                return new System.ComponentModel.DataAnnotations.ValidationResult("At least one room must be created in any active Room Type.");

            if (obj.FirstRoomType.Active)
            {
                if (string.IsNullOrEmpty(obj.FirstRoomType.Abbreviation))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Room Type Abbreviation cannot be empty.");
                if (string.IsNullOrEmpty(obj.FirstRoomType.Description))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Room Type Description cannot be empty.");
                if (obj.FirstRoomType.Min > obj.FirstRoomType.Max)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Min Paxs should be smaller than Max Paxs.");
            }

            if (obj.SecondRoomType.Active)
            {
                if (string.IsNullOrEmpty(obj.SecondRoomType.Abbreviation))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Room Type Abbreviation cannot be empty.");
                if (string.IsNullOrEmpty(obj.SecondRoomType.Description))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Room Type Description cannot be empty.");
                if (obj.SecondRoomType.Min > obj.SecondRoomType.Max)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Min Paxs should be smaller than Max Paxs.");
            }

            if (obj.ThirdRoomType.Active)
            {
                if (string.IsNullOrEmpty(obj.ThirdRoomType.Abbreviation))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Room Type Abbreviation cannot be empty.");
                if (string.IsNullOrEmpty(obj.ThirdRoomType.Description))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Room Type Description cannot be empty.");
                if (obj.ThirdRoomType.Min > obj.ThirdRoomType.Max)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Min Paxs should be smaller than Max Paxs.");
            }

            if (!obj.ClientTypesDefault.Any(x => x.Included))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Select at least one Client Type.");

            if (!obj.GroupTypesDefault.Any(x => x.Included))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Select at least one Group Type.");

            if (!obj.EntityTypesDefault.Any(x => x.Included))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Select at least one Entity Type.");

            if (!obj.CancellationReasonsDefault.Any(x => x.Included))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Select at least one Cancellation Reason.");

            if (!obj.BedsCotsDefault.Any(x => x.Included))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Select at least one Extra beds and cots.");

            if (!obj.OriginsDefault.Any(x => x.Included))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Select at least one Source.");

            if (!obj.SegmentsDefault.Any(x => x.Included))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Select at least one Segment.");

            if (!obj.PersonTitles.Any(x => x.Included))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Select at least one Person Title.");

            if (!obj.AttentionsDefault.Any(x => x.Included))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Select at least one Attention Type.");

            if (!obj.GuestCategoryDefault.Any(x => x.Included))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Select at least one Guest Category.");

            if (obj.HasConfirmationMail && string.IsNullOrEmpty(obj.ConfirmationMail))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Missing Confirmation Mail Body");
            if (obj.HasWelcomeMail && string.IsNullOrEmpty(obj.WelcomeMail))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Missing Welcome Mail Body");
            if (obj.HasFeedbackMail && string.IsNullOrEmpty(obj.FeedbackMail))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Missing Feedback Mail Body");

            long rooms = 0;
            if (obj.FirstRoomType.Active)
                rooms += obj.FirstRoomType.Rooms.Count();
            if (obj.SecondRoomType.Active)
                rooms += obj.SecondRoomType.Rooms.Count();
            if (obj.ThirdRoomType.Active)
                rooms += obj.ThirdRoomType.Rooms.Count();
            if (rooms > obj.AllowedRooms)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Rooms created over license limit. Please remove at least " + (rooms - obj.AllowedRooms) + " room(s)");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}