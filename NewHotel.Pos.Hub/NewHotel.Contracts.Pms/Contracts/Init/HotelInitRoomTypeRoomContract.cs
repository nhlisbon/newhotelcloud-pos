﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class HotelInitRoomTypeRoomContract : BaseContract
    {
        #region Members
        private string _doorNo;
        #endregion
        #region Constructor
        public HotelInitRoomTypeRoomContract()
        {

        }
        #endregion
        #region Properties
        [DataMember]
        public string DoorNo { get { return _doorNo; } set { Set(ref _doorNo, value, "DoorNo"); } }
        #endregion
    }
}
