﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class HotelInitRoomTypeContract : BaseContract
    {
        #region Members

        private string _abbreviation;
        private string _description;
        private short _min;
        private short _max;
        private bool _active;

        #endregion
        #region Properties

        [DataMember]
        public string Abbreviation { get { return _abbreviation; } set { Set(ref _abbreviation, value, "Abbreviation"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public short Min { get { return _min; } set { Set(ref _min, value, "Min"); } }
        [DataMember]
        public short Max { get { return _max; } set { Set(ref _max, value, "Max"); } }
        [DataMember]
        public bool Active { get { return _active; } set { Set(ref _active, value, "Active"); } }

        #endregion
        #region Lists

        [DataMember]
        public TypedList<HotelInitRoomTypeRoomContract> Rooms { get; set; }

        #endregion
    }
}