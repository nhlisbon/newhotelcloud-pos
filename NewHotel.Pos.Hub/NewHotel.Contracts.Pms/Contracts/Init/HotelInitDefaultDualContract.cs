﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class HotelInitDefaultDualContract : BaseContract
    {
        private long _initType;
        private long _initExtraType;
        private bool _included;
        private long _groupId;

        public HotelInitDefaultDualContract()
        {
            Description = new LanguageTranslationContract();
            ExtraDescription = new LanguageTranslationContract();
            Included = true;
        }

        [ReflectionExclude]
        [DataMember]
        public LanguageTranslationContract Description { get; internal set; }
        [ReflectionExclude]
        [DataMember]
        public LanguageTranslationContract ExtraDescription { get; internal set; }
        [DataMember]
        public long InitType { get { return _initType; } set { Set(ref _initType, value, "InitType"); } }
        [DataMember]
        public long InitExtraType { get { return _initExtraType; } set { Set(ref _initExtraType, value, "InitExtraType"); } }
        [DataMember]
        public bool Included { get { return _included; } set { Set(ref _included, value, "Included"); } }
        [DataMember]
        public long GroupId { get { return _groupId; } set { Set(ref _groupId, value, "GroupId"); } }

        public string DescriptionTranslated { get { return (string)Description; } }
        public string ExtraDescriptionTranslated { get { return (string)ExtraDescription; } }
    }
}
