﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class HotelInitBreakdownContract : BaseContract
    {
        private bool _active;

        private decimal _childPrice;
        private decimal _adultPrice;
        private bool _childVisible;
        private bool _adultVisible;

        public HotelInitBreakdownContract() 
        {
            Active = true;
            Resource = new LanguageTranslationContract();
            Department = new LanguageTranslationContract();
            Service = new LanguageTranslationContract();
        }

        [DataMember]
        public bool Active { get { return _active; } set { Set(ref _active, value, "Active"); } }
        [DataMember]
        public LanguageTranslationContract Resource { get; internal set; }
        [DataMember]
        public LanguageTranslationContract Department { get; internal set; }
        [DataMember]
        public LanguageTranslationContract Service { get; internal set; }
        [DataMember]
        public decimal ChildPrice { get { return _childPrice; } set { Set(ref _childPrice, value, "ChildPrice"); } }
        [DataMember]
        public decimal AdultPrice { get { return _adultPrice; } set { Set(ref _adultPrice, value, "AdultPrice"); } }
        [DataMember]
        public bool ChildVisible { get { return _childVisible; } set { Set(ref _childVisible, value, "ChildVisible"); } }
        [DataMember]
        public bool AdultVisible { get { return _adultVisible; } set { Set(ref _adultVisible, value, "AdultVisible"); } }

        public string ResourceTranslated { get { return (string)Resource; } }
    }
}
