﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class HotelInitMarketSegmentContract : BaseContract
    {
        private bool _included;
        private string _description;
        private string _groupDescription;
        private long _groupId;

        [DataMember]
        public bool Included { get { return _included; } set { Set(ref _included, value, "Included"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public string GroupDescription { get { return _groupDescription; } set { Set(ref _groupDescription, value, "GroupDescription"); } }
        [DataMember]
        public long GroupId { get { return _groupId; } set { Set(ref _groupId, value, "GroupId"); } }
    }
}
