﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class HotelInitTaxesContract : BaseContract
    {
        private string _country;
        private string _baseCurrency;
        private string _schema;
        private Guid? _regionId;
        private Guid? _taxRateId;
        private Guid? _sequenceId;
        private decimal _manualTax;

        public HotelInitTaxesContract() { }

        [DataMember]
        public string Country { get { return _country; } set { Set(ref _country, value, "Country"); } }
        [DataMember]
        public string BaseCurrency { get { return _baseCurrency; } set { Set(ref _baseCurrency, value, "BaseCurrency"); } }
        [DataMember]
        public string Schema { get { return _schema; } set { Set(ref _schema, value, "Schema"); } }
        [DataMember]
        public Guid? RegionId { get { return _regionId; } set { Set(ref _regionId, value, "RegionId"); } }
        [DataMember]
        public Guid? TaxRateId { get { return _taxRateId; } set { Set(ref _taxRateId, value, "TaxRateId"); } }
        [DataMember]
        public Guid? SequenceId { get { return _sequenceId; } set { Set(ref _sequenceId, value, "SequenceId"); } }
        [DataMember]
        public decimal ManualTax { get { return _manualTax; } set { Set(ref _manualTax, value, "ManualTax"); } }

        [DataMember]
        public bool DefaultRegion { get; set; }
        [DataMember]
        public bool DefaultSequence { get; set; }
        [DataMember]
        public string SequenceAbbreviation { get; set; }
    }
}