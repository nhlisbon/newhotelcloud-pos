﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    public class BudgetContract : BaseContract
    {
        #region Private Members
        private DateTime _date;
        private Guid? _seasonId;
        private bool _enabled;
        private ARGBColor _color;

        //Forecasted Occupancy
        private short _occupancy;
        //Inventory
        private short _inventory;
        //Price for selected ResourceType
        //private decimal _price;
        private Dictionary<Guid, decimal> _prices;
        //Real Occupancy
        private short _realOccupancy;
        //Real Availability
        private short _realAvailability;
        //Important Day
        private ARGBColor? _holidayColor;

        #endregion
        #region Constructor
        public BudgetContract()
        {
            Color = ARGBColor.White;
            _prices = new Dictionary<Guid, decimal>();
        }
        #endregion
        #region Public Properties
        [DataMember]
        public DateTime Date { get { return _date; } set { Set(ref _date, value, "Date"); } }
        [DataMember]
        public Guid? SeasonId { get { return _seasonId; } set { Set(ref _seasonId, value, "SeasonId"); } }
        [DataMember]
        public short Occupancy { get { return _occupancy; } set { Set(ref _occupancy, value, "Occupancy"); } }
        [DataMember]
        public short RealOccupancy { get { return _realOccupancy; } set { Set(ref _realOccupancy, value, "RealOccupancy"); } }
        [DataMember]
        public short RealAvailability { get { return _realAvailability; } set { Set(ref _realAvailability, value, "RealAvailability"); } }
        [DataMember]
        public bool Enabled { get { return _enabled; } set { Set(ref _enabled, value, "Enabled"); } }
        [DataMember]
        public ARGBColor Color { get { return _color; } set { Set(ref _color, value, "Color"); } }
        [DataMember]
        public short Inventory { get { return _inventory; } set { Set(ref _inventory, value, "Inventory"); } }
        [DataMember]
        public Dictionary<Guid, decimal> Prices { get { return _prices; } set { Set(ref _prices, value, "Prices"); } }
        [DataMember]
        public ARGBColor? HolidayColor { get { return _holidayColor; } set { Set(ref _holidayColor, value, "HolidayColor"); } }
        #endregion
        #region Visual Memmbers
        public object CellRectangle;
        public object CellHoliday;
        public object CellText;
        #endregion
    }
}
