﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class BarSelectionContract : BaseContract
    {
        #region Private Members
        private Guid? _taxRateId;
        private string _taxRateTranslation;
        private short _orden;
        #endregion
        #region Constructor
        public BarSelectionContract() { }
        #endregion
        #region Public Properties
        [DataMember]
        public Guid? TaxRateId { get { return _taxRateId; } set { Set(ref _taxRateId, value, "TaxRateId"); } }
        [DataMember]
        public string TaxRateTranslation { get { return _taxRateTranslation; } set { Set(ref _taxRateTranslation, value, "TaxRateTranslation"); } }
        [DataMember]
        public short Orden { get { return _orden; } set { Set(ref _orden, value, "Orden"); } }
        #endregion
    }
}
