﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    public class SpecialDayContract : BaseContract
    {
        #region Members

        private DateTime _date;
        private string _description;
        private ARGBColor _color;
        private bool _repetitive;

        #endregion
        #region Properties

        [DataMember]
        public DateTime Date { get { return _date; } set { Set(ref _date, value, "Date"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public ARGBColor Color { get { return _color; } set { Set(ref _color, value, "Color"); } }
        [DataMember]
        public bool Repetitive { get { return _repetitive; } set { Set(ref _repetitive, value, "Repetitive"); } }

        #endregion
    }
}
