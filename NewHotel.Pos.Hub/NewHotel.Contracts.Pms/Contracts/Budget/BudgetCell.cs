﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public abstract class BudgetCell : BaseContract
    {
        #region Private Members
        private bool _isValid;
        private string _headerText;
        #endregion
        #region Public Properties
        [DataMember]
        public bool IsValid { get { return _isValid; } set { Set(ref _isValid, value, "IsValid"); } }
        [DataMember]
        public string HeaderText { get { return _headerText; } set { Set(ref _headerText, value, "HeaderText"); } }
        #endregion
    }
}
