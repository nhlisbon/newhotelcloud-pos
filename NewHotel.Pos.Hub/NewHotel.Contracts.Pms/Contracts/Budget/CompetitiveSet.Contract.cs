﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class CompetitiveSetContract : BaseContract
    {
        #region Constructor
        public CompetitiveSetContract()
        {
            Hotels = new List<HotelPricesContract>();
        } 
        #endregion
        #region Public Properties
        [DataMember]
        public List<HotelPricesContract> Hotels { get; set; }
        [DataMember]
        public string HotelDescription { get; set; }
        [DataMember]
        public double HotelMinPrice { get; set; }
        [DataMember]
        public double HotelMaxPrice { get; set; }
        [DataMember]
        public double HotelStars { get; set; }
        [DataMember]
        public double TripAdvisorRate { get; set; }

        public double HotelRating
        {
            get { return HotelStars * 10 + TripAdvisorRate; }
        }
        #endregion
    }
}
