﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    public class SeasonContract : BaseContract
    {
        #region Members
        private short _min;
        private short _max;
        private decimal _average;
        private ARGBColor _color;
        private string _description;
        #endregion
        #region Properties
        [DataMember]
        public short Min { get { return _min; } set { Set(ref _min, value, "Min"); } }
        [DataMember]
        public short Max { get { return _max; } set { Set(ref _max, value, "Max"); } }
        [DataMember]
        public decimal Average { get { return _average; } set { Set(ref _average, value, "Average"); } }
        [DataMember]
        public ARGBColor Color { get { return _color; } set { Set(ref _color, value, "Color"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        #endregion
    }
}
