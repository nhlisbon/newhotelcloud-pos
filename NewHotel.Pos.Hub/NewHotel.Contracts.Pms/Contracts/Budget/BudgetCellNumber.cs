﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    public class BudgetCellNumber : BudgetCell
    {
        #region Private Members
        private int _value;
        #endregion
        #region Constructor
        public BudgetCellNumber()
        {
            HeaderText = "";
            Value = 0;
        }
        #endregion
        #region Public Properties
        public int Value { get { return _value; } set { Set(ref _value, value, "Value"); } }
        public object VisualRectangle { get; set; }
        #endregion
    }
}
