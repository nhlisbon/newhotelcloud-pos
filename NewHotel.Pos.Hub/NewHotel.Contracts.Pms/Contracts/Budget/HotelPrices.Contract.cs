﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class HotelPricesContract : BaseContract
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string FullAddress { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Featured { get; set; }
        [DataMember]
        public int Stars { get; set; }
        [DataMember]
        public string Rating { get; set; }
        [DataMember]
        public string TripAdvisorRating { get; set; }
        [DataMember]
        public string Longitude { get; set; }
        [DataMember]
        public string Latitude { get; set; }
        [DataMember]
        public string Distance { get; set; }
        [DataMember]
        public double LowRate { get; set; }
        [DataMember]
        public double HighRate { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string HotelBaseUrl { get; set; }
        [DataMember]
        public string HotelType { get; set; }
        [DataMember]
        public long HotelId { get; set; }

        private bool _isCompetitiveSet;
        [DataMember]
        public bool IsCompetitiveSet { get { return _isCompetitiveSet; } set { Set(ref _isCompetitiveSet, value, "IsCompetitiveSet"); } }

        public double VisualCategory
        {
            get
            {
                return (Stars * 2.0 / 10);
            }
        }

        public double ClientRanking
        {
            get
            {
                try
                {
                    return Stars * 10 + double.Parse(TripAdvisorRating);
                }
                catch (Exception)
                {
                    return 0;
                }
                
            }
        }
    }
}
