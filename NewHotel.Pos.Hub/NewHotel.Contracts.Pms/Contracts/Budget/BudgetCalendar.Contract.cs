﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    public class BudgetCalendarContract : BaseContract
    {
        #region Private Members
        private DateTime _initialDate;
        private DateTime _finalDate;
        private Guid _resourceTypeId;
        #endregion
        #region Constructor
        public BudgetCalendarContract(DateTime initialDate, DateTime finalDate)
        {
            Data = new List<BudgetContract>();
            SpecialDays = new List<SpecialDayRecord>();
            _initialDate = initialDate;
            _finalDate = finalDate;
        }
        #endregion
        #region Public Properties
        [DataMember]
        public List<BudgetContract> Data { get; set; }
        [DataMember]
        public List<SpecialDayRecord> SpecialDays { get; set; }
        [DataMember]
        public DateTime InitialDate { get { return _initialDate; } set { _initialDate = value; } }
        [DataMember]
        public DateTime FinalDate { get { return _finalDate; } set { _finalDate = value; } }
        [DataMember]
        public Guid ResourceTypeId { get { return _resourceTypeId; } set { Set(ref _resourceTypeId, value, "ResourceTypeId"); } }
        #endregion
        #region Public Methods
        public string GetDateCaption(int row, int column)
        {
            DateTime date = new DateTime(_initialDate.Year, _initialDate.Month, 1);
            date = date.AddMonths(column);
            if (DateTime.DaysInMonth(date.Year, date.Month) > row)
            {
                date = date.AddDays(row);
                return date.DayOfWeek.ToString().ToLower().Substring(0, 1) + " " + date.Day.ToString("00");
            }
            else return "";
        }
        public ARGBColor GetDateColor(int row, int column)
        {
            DateTime date = new DateTime(_initialDate.Year, _initialDate.Month, 1);
            date = date.AddMonths(column);
            if (DateTime.DaysInMonth(date.Year, date.Month) > row)
            {
                date = date.AddDays(row);
                return (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday) ? ARGBColor.LightBlue : ARGBColor.WhiteSmoke;
            }
            else return ARGBColor.WhiteSmoke;
        }
        public void UpdateValues(List<OccupationBarPriceRecord> records)
        {
            if (records.Count == 0) return;

            int index = 0;
            //move index in Data to match first date in records
            while (Data[index].Date != records[0].Date) index++;
            DateTime currentDate = DateTime.MinValue;

            foreach (OccupationBarPriceRecord record in records)
            {
                //First Time Only
                if (currentDate == DateTime.MinValue)
                {
                    currentDate = record.Date;
                }
                //Record Changed Date, then move one cell in calendar
                if (currentDate != record.Date)
                {
                    index++;
                    currentDate = record.Date;
                }
                BudgetContract cell = Data[index];
                cell.Inventory = record.TotalInventory;
                if (cell.Prices.ContainsKey(record.RoomTypeId)) cell.Prices[record.RoomTypeId] = Math.Round(record.Price, 2);
                else cell.Prices.Add(record.RoomTypeId, Math.Round(record.Price, 2));
                //cell.Price = record.Price;
                cell.RealOccupancy = (short)(record.TotalInventory - record.RoomsAvailables);
                cell.RealAvailability = record.RoomsAvailables;
            }
        }
        public void UpdateSpecialDays(List<SpecialDayRecord> specialDays)
        {
            this.SpecialDays = specialDays;

            if (specialDays.Count == 0) return;
            
            foreach (BudgetContract cell in Data)
            {
                bool found = false;

                foreach (SpecialDayRecord record in specialDays)
                {
                    if (cell.Date.Day == record.Date.Day && cell.Date.Month == record.Date.Month)
                    {
                        cell.HolidayColor = record.Color;
                        found = true;
                    }
                }

                if (!found) cell.HolidayColor = null;
            }
        }
        #endregion
        #region Indexer
        public BudgetContract this[int row, int column]
        {
            get
            {
                DateTime date = new DateTime(_initialDate.Year, _initialDate.Month, 1);
                date = date.AddMonths(column);
                if (DateTime.DaysInMonth(date.Year, date.Month) > row)
                {
                    date = date.AddDays(row);
                    for (int i = 0; i < Data.Count; i++)
                    {
                        if (Data[i].Date == date) return Data[i];
                    }
                }
                return null;
            }
            set
            {
                DateTime date = new DateTime(_initialDate.Year, _initialDate.Month, 1);
                date = date.AddMonths(column);
                if (DateTime.DaysInMonth(date.Year, date.Month) > row)
                {
                    date = date.AddDays(row);
                    for (int i = 0; i < Data.Count; i++)
                    {
                        if (Data[i].Date == date) Data[i] = value;
                    }
                }
            }
        }
        #endregion
    }
}
