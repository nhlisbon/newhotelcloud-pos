﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    public class BudgetCellColor : BudgetCell
    {
        #region Private Members
        private ARGBColor _backgroundColor;
        #endregion
        #region Constructor
        public BudgetCellColor()
        {
            HeaderText = "";
            BackgroundColor = ARGBColor.White;
        }
        #endregion
        #region Public Properties
        public ARGBColor BackgroundColor { get { return _backgroundColor; } set { Set(ref _backgroundColor, value, "BackgroundColor"); } }
        public object VisualRectangle { get; set; }
        #endregion
    }
}
