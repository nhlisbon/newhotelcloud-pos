﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace NewHotel.Contracts
{
    public partial class LanguageTranslation : ITypedList
    {
        private PropertyDescriptorCollection GetTypeProperties(Type type)
        {
            TypeDescriptionProvider provider = TypeDescriptor.GetProvider(type);
            return provider.GetTypeDescriptor(type).GetProperties();
        }

        #region ITypedList Members

        public PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors)
        {
            if (listAccessors == null)
                return GetTypeProperties(typeof(TranslationInfoContract));

            return null;
        }

        public string GetListName(PropertyDescriptor[] listAccessors)
        {
            return string.Format("LanguageTranslation", typeof(TranslationInfoContract).Name);
        }

        #endregion
    }
}
