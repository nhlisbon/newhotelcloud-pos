﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class RoomStatusContract : BaseContract
    {
        #region Properties

        [DataMember]
        public Guid RoomId { get; set; }
        [DataMember]
        public string RoomNum { get;  set; }

        [DataMember]
        public RoomStatus? Status { get; set; }
        [DataMember]
        public int Column { get; set; }
       

        #endregion

        #region Constructors

        public RoomStatusContract()
            : base() { }


        public RoomStatusContract(Guid? roomStatusLogId,
            Guid roomId, string roomNum,
            RoomStatus? status )
            : base() 
        {
            Id = roomStatusLogId;
            RoomId = roomId;
            RoomNum = roomNum;
            Status = status;
        }

        #endregion
    }


    [DataContract]
    public class RoomStatusContractDetailed : BaseRecord
    {
        [DataMember]
        public RoomStatusRecord Col0 { get; set; }
        [DataMember]
        public RoomStatusRecord Col1 { get; set; }
        [DataMember]
        public RoomStatusRecord Col2 { get; set; }
        [DataMember]
        public RoomStatusRecord Col3 { get; set; }
        [DataMember]
        public RoomStatusRecord Col4 { get; set; }
        [DataMember]
        public RoomStatusRecord Col5 { get; set; }
        [DataMember]
        public RoomStatusRecord Col6 { get; set; }
        [DataMember]
        public RoomStatusRecord Col7 { get; set; }
        [DataMember]
        public RoomStatusRecord Col8 { get; set; }
        [DataMember]
        public RoomStatusRecord Col9 { get; set; }
        [DataMember]
        public RoomStatusRecord Col10 { get; set; }
        [DataMember]
        public RoomStatusRecord Col11 { get; set; }
        [DataMember]
        public RoomStatusRecord Col12 { get; set; }
        [DataMember]
        public RoomStatusRecord Col13 { get; set; }
    }
}
