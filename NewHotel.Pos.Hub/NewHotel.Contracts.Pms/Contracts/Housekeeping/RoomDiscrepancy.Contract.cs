﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class RoomDiscrepancyContract : BaseContract
    {
        #region Members
        private short? _declaredAdults;
        private short? _declaredChildren;
        private short? _declaredBabies;
        private bool? _declaredActive;
        private bool? _declaredOccupied;
        private RoomStatus? _declaredRoomStatus;
        private string _comment;
        #endregion

        #region Properties

        [DataMember]
        public Guid RoomId { get; internal set; }
        [DataMember]
        public string RoomNum { get; internal set; }
        [DataMember]
        public string RoomTypeDesc { get; internal set; }
        [DataMember]
        public DateTime DiscrepancyDate { get; internal set; }
        [DataMember]
        public DateTime DiscrepancyTime { get; internal set; }
        [DataMember]
        public bool? Occupied { get; internal set; }
        [DataMember]
        public bool? DeclaredOccupied { get { return _declaredOccupied; } set { Set(ref _declaredOccupied, value, "DeclaredOccupied"); } }
        [DataMember]
        public RoomStatus? RoomStatus { get; internal set; }
        [DataMember]
        public string RoomStatusDesc { get; internal set; }
        [DataMember]
        public RoomStatus? DeclaredRoomStatus { get { return _declaredRoomStatus; } set { Set(ref _declaredRoomStatus, value, "DeclaredRoomStatus"); } }
        [DataMember]
        public string DeclaredRoomStatusDesc { get; set; }
        [DataMember]
        public bool? Active { get; internal set; }
        [DataMember]
        public bool? DeclaredActive { get { return _declaredActive; } set { Set(ref _declaredActive, value, "DeclaredActive"); } }
        [DataMember]
        public short? Adults { get; internal set; }
        [DataMember]
        public short? DeclaredAdults { get { return _declaredAdults; } set { Set(ref _declaredAdults, value, "DeclaredAdults"); } }
        [DataMember]
        public short? Children { get; internal set; }
        [DataMember]
        public short? DeclaredChildren { get { return _declaredChildren; } set { Set(ref _declaredChildren, value, "DeclaredChildren"); } }
        [DataMember]
        public short? Babies { get; internal set; }
        [DataMember]
        public short? DeclaredBabies { get { return _declaredBabies; } set { Set(ref _declaredBabies, value, "DeclaredBabies"); } }
        [DataMember]
        public string Comment { get { return _comment; } set { Set(ref _comment, value, "Comment"); } }

        #endregion
        #region Private Methods

        private string FormatPaxs(short? adults, short? children, short? babies)
        {
            if (adults.HasValue || children.HasValue || babies.HasValue)
                return string.Format("{0} {1} {2}", adults ?? 0, children ?? 0, babies ?? 0);
            else
                return string.Empty;
        }

        #endregion
        #region Extended Properties

        public string Paxs
        {
            get { return FormatPaxs(Adults, Children, Babies); }
        }

        public string DeclaredPaxs
        {
            get { return FormatPaxs(DeclaredAdults, DeclaredChildren, DeclaredBabies); }
        }

        public bool? Free
        {
            get { return Occupied.HasValue ? !Occupied : null; }
            set 
            {
                Occupied = value.HasValue ? !value : null;
                NotifyPropertyChanged("Free");
            }
        }

        public bool? DeclaredFree
        {
            get { return DeclaredOccupied.HasValue ? !DeclaredOccupied : null; }
            set 
            { 
                DeclaredOccupied = value.HasValue ? !value : null;
                NotifyPropertyChanged("DeclaredFree");
            }
        }

        #endregion
        #region Constructors

        public RoomDiscrepancyContract(Guid? roomDiscrepancyId,
            DateTime? discrepancyDate, DateTime? discrepancyTime,
            Guid roomId, string roomNum, string roomTypeDesc,
            bool? occupied, bool? declaredOccupied,
            RoomStatus? roomStatus, string roomStatusDesc,
            RoomStatus? declaredRoomStatus, string declaredRoomStatusDesc,
            bool? active, bool? declaredActive,
            short? adults, short? declaredAdults, short? children, short? declaredChildren,
            short? babies, short? declaredBabies, string comment,
            DateTime lastModified, DateTime date)
            : base() 
        {
            Id = roomDiscrepancyId;
            DiscrepancyDate = discrepancyDate ?? date;
            DiscrepancyTime = discrepancyTime ?? date;
            RoomId = roomId;
            RoomNum = roomNum;
            RoomTypeDesc = roomTypeDesc;
            Occupied = occupied;
            DeclaredOccupied = declaredOccupied ?? occupied;
            RoomStatus = roomStatus;
            RoomStatusDesc = roomStatusDesc;
            DeclaredRoomStatus = declaredRoomStatus ?? roomStatus;
            DeclaredRoomStatusDesc = declaredRoomStatusDesc ?? roomStatusDesc;
            Active = active;
            DeclaredActive = declaredActive ?? active;
            Adults = adults;
            DeclaredAdults = declaredAdults ?? adults;
            Children = children;
            DeclaredChildren = declaredChildren ?? children;
            Babies = babies;
            DeclaredBabies = declaredBabies ?? babies;
            Comment = comment;
            LastModified = lastModified;
        }

        #endregion
    }
}
