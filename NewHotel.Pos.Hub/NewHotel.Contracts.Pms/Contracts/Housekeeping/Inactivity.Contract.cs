﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [CustomValidation(typeof(InactivityContract), "ValidateInactivity")]
    public class InactivityContract : BaseContract
    {
        #region Members

        [DataMember]
        internal DateTime _workDate;
        private DateTime _initialDate;
        private DateTime _finalDate;
        private Guid _inactivityTypeId;
        private bool _affectBooking;

        #endregion
        #region Properties

        [DataMember]
        public Guid ResourceId { get; set; }
        public DateTime WorkDate { get { return _workDate; } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Initial date required.")]
        public DateTime InitialDate { get { return _initialDate; } set { Set(ref _initialDate, value, "InitialDate"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Final date required.")]
        public DateTime FinalDate { get { return _finalDate; } set { Set(ref _finalDate, value, "FinalDate"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Inactivity type required.")]
        public Guid InactivityTypeId
        {
            get { return _inactivityTypeId; }
            set { Set(ref _inactivityTypeId, value, "InactivityTypeId"); }
        }
        [DataMember]
        public string InactivityTranslation { get; set; }
        [DataMember]
        public bool AffectBooking { get { return _affectBooking; } set { Set(ref _affectBooking, value, "AffectBooking"); } }
        [DataMember]
        public string Comments { get; set; }
        #endregion
        #region Extension Properties

        public bool IsInitialDateEnabled
        {
            get { return InitialDate.Date >= WorkDate.Date; } 
        }

        public bool IsFinalDateEnabled
        {
            get { return FinalDate.Date >= WorkDate.Date; }
        }

        public DateTime InitialMinDate
        {
            get { return InitialDate.Date >= WorkDate.Date ? WorkDate.Date : InitialDate.Date; }
        }

        public DateTime FinalMinDate
        {
            get { return FinalDate.Date >= WorkDate.Date ? WorkDate.Date : FinalDate.Date; }
        }

        #endregion
        #region Constructors

        public InactivityContract(DateTime workDate)
            : base()
        {
            _workDate = workDate;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateInactivity(InactivityContract obj)
        {
            if (obj.InitialDate == DateTime.MinValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Initial date required.");
            if (obj.FinalDate == DateTime.MinValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Final date required.");
            if (obj.FinalDate < obj.InitialDate)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Initial date greater than final Date.");
            if (obj.InactivityTypeId == Guid.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Inactivity type required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
