﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class WebExpressDetailInfoItemContract : BaseContract
    {
        [DataMember]
        public short Pension { get; set; }
        [DataMember]
        public string PensionDescription { get; set; }
        [DataMember]
        public Guid RoomType { get; set; }
        [DataMember]
        public string RoomTypeAbbreviation { get; set; }
        [DataMember]
        public string RoomTypeDescription { get; set; }
        [DataMember]
        public string ImageLink { get; set; }
        [DataMember]
        public List<WebExpressDailyInfoContract> DailyValues { get; set; }
        [DataMember]
        public bool RestrictedItem { get; set; }
        [DataMember]
        public string RestrictionDescription { get; set; }


        public WebExpressDetailInfoItemContract()
        {
            DailyValues = new List<WebExpressDailyInfoContract>();
        }
        
    }
}
