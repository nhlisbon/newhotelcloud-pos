﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class WebExpressReservationContract : BaseContract
    {
        public WebExpressReservationContract()
        {
            Guests = new List<WebExpressReservationContactContract>();
            Prices = new Dictionary<DateTime, decimal>(); 
        }

        /// <summary>
        /// Room Type
        /// </summary>
        [DataMember]
        public Guid? RoomTypeId { get; set; }
        /// <summary>
        /// Pension Mode
        /// </summary>
        [DataMember]
        public short? PensionModeId { get; set; }
        /// <summary>
        /// Arrival Date
        /// </summary>
        [DataMember]
        public DateTime Arrival { get; set; }
        /// <summary>
        /// Departure Date
        /// </summary>
        [DataMember]
        public DateTime Departure { get; set; }
        /// <summary>
        /// Quantity of Adults
        /// </summary>
        [DataMember]
        public short Adults { get; set; }
        /// <summary>
        /// Quantity of Children
        /// </summary>
        [DataMember]
        public short Children { get; set; }
        /// <summary>
        /// Quantity of Babbies
        /// </summary>
        [DataMember]
        public short Babbies { get; set; }
        /// <summary>
        /// Reservation Comments
        /// </summary>
        [DataMember]
        public string Comments { get; set; }
        /// <summary>
        /// Credit Card Number
        /// </summary>
        [DataMember]
        public string CreditCardNumber { get; set; }
        /// <summary>
        /// Credit Card Holder
        /// </summary>
        [DataMember]
        public string CreditCardHolder { get; set; }
        /// <summary>
        /// Validation Month
        /// </summary>
        [DataMember]
        public int? CreditCardMonth { get; set; }
        /// <summary>
        /// Validation Year (YYYY)
        /// </summary>
        [DataMember]
        public int? CreditCardYear { get; set; }
        /// <summary>
        /// Credit Card Type
        /// </summary>
        [DataMember]
        public Guid? CreditCardType { get; set; }
        /// <summary>
        /// Prices Per Day
        /// </summary>
        public Dictionary<DateTime, decimal> Prices { get; set; }
        /// <summary>
        /// Guest List
        /// </summary>
        [DataMember]
        public List<WebExpressReservationContactContract> Guests { get; set; }
    }
}
