﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class WebExpressPaymentOriginContract : BaseContract
    {
        public WebExpressPaymentOriginContract()
        {
            Settings = new Dictionary<string, string>();
        }

        [DataMember]
        public int SystemType { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public IDictionary<string, string> Settings { get; set; }
        [DataMember]
        public bool IsTestMode { get; set; }
    }
}