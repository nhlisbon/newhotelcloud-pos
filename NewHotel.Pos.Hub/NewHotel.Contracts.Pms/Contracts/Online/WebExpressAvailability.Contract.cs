﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class WebExpressAvailabilityContract : BaseContract
    {
        [DataMember]
        public DateTime From { get; set; }
        [DataMember]
        public DateTime To { get; set; }
        [DataMember]
        public SerializableDictionary<DateTime, decimal> BestPrices { get; set; }
        [DataMember]
        public SerializableDictionary<DateTime, long> Vacants { get; set; }
        [DataMember]
        public string HotelName { get; set; }
        [DataMember]
        public string Currency { get; set; }
        [DataMember]
        public string CurrencySymbol { get; set; }
        [DataMember]
        public byte[] HotelLogo { get; set; }
        [DataMember]
        public bool IsHostel { get; set; }
        [DataMember]
        public bool CreditCardRequired { get; set; }
        [DataMember]
        public ShowPriceType DisplayPriceType { get; set; }
        

        public WebExpressAvailabilityContract()
        {
            BestPrices = new SerializableDictionary<DateTime, decimal>();
            Vacants = new SerializableDictionary<DateTime, long>();
            DisplayPriceType = ShowPriceType.Both;
        }
    }
}
