﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class WebExpressDailyInfoContract : BaseContract
    {
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public decimal NetValue { get; set; }
        [DataMember]
        public int Availability { get; set; }

        public decimal TaxValue { get { return Price - NetValue; } }
    }
}
