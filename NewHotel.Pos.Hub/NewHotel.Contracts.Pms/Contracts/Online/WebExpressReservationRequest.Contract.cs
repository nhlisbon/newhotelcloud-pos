﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class WebExpressReservationRequestContract : BaseContract
    {
        public WebExpressReservationRequestContract()
        {
            Reservations = new List<WebExpressReservationContract>();
        }

        /// <summary>
        /// List of Reservations
        /// </summary>
        [DataMember]
        public List<WebExpressReservationContract> Reservations { get; set; }
    }
}
