﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class WebExpressPaymentRequestContract : BaseContract
    {
        public WebExpressPaymentRequestContract()
        {
        }

        [DataMember]
        public Guid OriginId { get; set; }
        [DataMember]
        public DateTime? Date { get; set; }
        [DataMember]
        public decimal AmountValue { get; set; }
        [DataMember]
        public string Currency { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Reference { get; set; }
        [DataMember]
        public string Token { get; set; }
        [DataMember]
        public string ResultCode { get; set; }
    }
}