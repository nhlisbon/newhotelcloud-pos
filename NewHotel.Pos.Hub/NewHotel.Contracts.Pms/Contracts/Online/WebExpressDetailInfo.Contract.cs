﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class WebExpressDetailInfoContract : BaseContract
    {
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public DateTime From { get; set; }
        [DataMember]
        public DateTime To { get; set; }
        [DataMember]
        public Guid RateId { get; set; }
        [DataMember]
        public short Adults { get; set; }
        [DataMember]
        public short Childs { get; set; }
        [DataMember]
        public string RateDescription { get; set; }
        [DataMember]
        public string Currency { get; set; }
        [DataMember]
        public string CurrencySymbol { get; set; }
        [DataMember]
        public ShowPriceType DisplayPriceType { get; set; }
        [DataMember]
        public List<WebExpressDetailInfoItemContract> Items { get; set; }

        public WebExpressDetailInfoContract()
        {
            DisplayPriceType = ShowPriceType.Both;
        }

        public WebExpressDetailInfoContract(DateTime from, DateTime to, Guid rateId, string rateDescription, string currency, short adults, short childs, ShowPriceType showPriceType)
        {
            From = from;
            To = to;
            RateId = rateId;
            RateDescription = rateDescription;
            Adults = adults;
            Childs = childs;
            Currency = currency;
            DisplayPriceType = showPriceType;
            Items = new List<WebExpressDetailInfoItemContract>();
        }
    }
}
