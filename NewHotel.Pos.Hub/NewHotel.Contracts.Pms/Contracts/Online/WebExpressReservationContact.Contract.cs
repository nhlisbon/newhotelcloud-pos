﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class WebExpressReservationContactContract : BaseContract
    {
        /// <summary>
        /// First Name of the Holder
        /// </summary>
        [DataMember]
        public string FirstName { get; set; }
        /// <summary>
        /// Last name of the Holder
        /// </summary>
        [DataMember]
        public string LastName { get; set; }
        /// <summary>
        /// Address of the Holder
        /// </summary>
        [DataMember]
        public string Address { get; set; }
        /// <summary>
        /// Phone Number of the Holder
        /// </summary>
        [DataMember]
        public string PhoneNumber { get; set; }
        /// <summary>
        /// City of the Holder
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// Email Address
        /// </summary>
        [DataMember]
        public string EmailAddress { get; set; }
        /// <summary>
        /// Country of the Holder
        /// </summary>
        [DataMember]
        public string Country { get; set; }
        /// <summary>
        /// Postal Code of the Holder
        /// </summary>
        [DataMember]
        public string PostalCode { get; set; }
        /// <summary>
        /// Guest Type
        /// </summary>
        [DataMember]
        public GuestType GuestType { get; set; }
        /// <summary>
        /// Birthdate
        /// </summary>
        [DataMember]
        public DateTime? BirthDate { get; set; }
        /// <summary>
        /// Passport Number
        /// </summary>
        [DataMember]
        public string PassportNumber { get; set; }
        /// <summary>
        /// Country of Emission of Passport
        /// </summary>
        [DataMember]
        public string PassportCountry { get; set; }
        /// <summary>
        /// Nationality of the Guest
        /// </summary>
        [DataMember]
        public string Nationality { get; set; }
    }
}
