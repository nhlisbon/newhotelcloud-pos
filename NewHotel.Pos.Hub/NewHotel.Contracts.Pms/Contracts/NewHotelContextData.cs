﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Runtime.Serialization;
//using System.Globalization;

//namespace NewHotel.Contracts
//{
//    [DataContract]
//    public class NewHotelContextData : BaseContract
//    {
//        #region Members

//        [DataMember]
//        internal int _maxTabCount;
//        [DataMember]
//        internal Guid _userId;
//        [DataMember]
//        internal string _name;
//        [DataMember]
//        internal long _applicationId;
//        [DataMember]
//        internal Guid _installationId;
//        [DataMember]
//        internal string _installationName;
//        [DataMember]
//        internal long _languageId;
//        [DataMember]
//        internal string _cultureName;
//        [DataMember]
//        internal Dictionary<string, KeyDescRecord[]> _enums;
//        [DataMember]
//        internal KeyDescRecord[] _languages;
//        [DataMember]
//        internal DateTime _sysDate;
//        [DataMember]
//        internal DateTime _workDate;
//        [DataMember]
//        internal short _workshift;
//        [DataMember]
//        internal DateTime? _firstOpenedDate;
//        [DataMember]
//        internal int? _lastOpenedYear;
//        [DataMember]
//        internal string _currencyId;
//        [DataMember]
//        internal string _currencySymbol;
//        [DataMember]
//        internal short? _currencyDecInPercents;
//        [DataMember]
//        internal short? _currencyDecInPrices;
//        [DataMember]
//        internal short? _currencyDecInExchange;

//        private CultureInfo _culture = null;

//        #endregion
//        #region Constructors

//        public NewHotelContextData(Guid installationId, long languageId)
//        {
//            _installationId = installationId;
//            _languageId = languageId;
//        }

//        public NewHotelContextData(int maxTabCount,
//            Guid userId, string name, long applicationId,
//            Guid installationId, string installationName, long languageId, 
//            string cultureName, short workshift, DateTime workDate, DateTime sysDate,
//            DateTime? firstOpenedDate, int? lastOpenedYear,
//            string currencyId, string currencySymbol,
//            short? currencyDecInPercents, short? currencyDecInPrices, short? currencyDecInExchange,
//            Dictionary<string, KeyDescRecord[]> enums, KeyDescRecord[] languages)
//        {
//            _maxTabCount = maxTabCount;
//            _userId = userId;
//            _name = name;
//            _applicationId = applicationId;
//            _installationId = installationId;
//            _installationName = installationName;
//            _languageId = languageId;
//            _cultureName = cultureName;
//            _enums = enums;
//            _languages = languages;
//            _workDate = workDate;
//            _workshift = workshift;
//            _firstOpenedDate = firstOpenedDate;
//            _lastOpenedYear = lastOpenedYear;
//            _sysDate = sysDate;
//            _currencyId = currencyId;
//            _currencySymbol = currencySymbol;
//            _currencyDecInPercents = currencyDecInPercents;
//            _currencyDecInPrices = currencyDecInPrices;
//            _currencyDecInExchange = currencyDecInExchange;
//        }

//        #endregion
//        #region Properties

//        public int MaxTabCount
//        {
//            get { return _maxTabCount; }
//        }

//        public Guid UserId
//        {
//            get { return _userId; }
//        }

//        public string Name
//        {
//            get { return _name; }
//        }

//        public long ApplicationId
//        {
//            get { return _applicationId; }
//        }

//        public Guid InstallationId 
//        {
//            get { return _installationId; } 
//        }

//        public string InstallationName
//        {
//            get { return _installationName; }
//        }

//        public long LanguageId
//        {
//            get { return _languageId; }
//        }

//        public CultureInfo CultureInfo
//        {
//            get 
//            {
//                if (_culture == null && !string.IsNullOrEmpty(_cultureName))
//                    _culture = new CultureInfo(_cultureName);
//                return _culture; 
//            }
//        }

//        public DateTime SysDate
//        {
//            get { return _sysDate; }
//            set { Set(ref _sysDate, value, "SysDate"); }
//        }

//        public DateTime WorkDate
//        {
//            get { return _workDate; }
//            set { Set(ref _workDate, value, "WorkDate"); }
//        }

//        public short WorkShift
//        {
//            get { return _workshift; }
//            set { Set(ref _workshift, value, "WorkShift"); }
//        }

//        public int? FirstOpenedYear
//        {
//            get 
//            {
//                if (_firstOpenedDate.HasValue)
//                    return _firstOpenedDate.Value.Year;

//                return null;
//            }
//        }

//        public DateTime? FirstOpenedDate
//        {
//            get { return _firstOpenedDate; }
//        }

//        public int? LastOpenedYear
//        {
//            get { return _lastOpenedYear; }
//            set { Set(ref _lastOpenedYear, value, "LastOpenedYear"); }
//        }

//        public DateTime? LastOpenedDate
//        {
//            get
//            {
//                if (_lastOpenedYear.HasValue)
//                    return new DateTime(_lastOpenedYear.Value, 12, 31);

//                return null;
//            }
//        }

//        public KeyDescRecord[] LanguageData
//        {
//            get { return _languages; }
//        }

//        public Dictionary<string, KeyDescRecord[]> EnumData
//        {
//            get { return _enums; }
//        }

//        public string CurrencyId
//        {
//            get { return _currencyId; }
//        }

//        public string CurrencySymbol
//        {
//            get { return _currencySymbol; }
//        }

//        public short? CurrencyDecInPercents
//        {
//            get { return _currencyDecInPercents; }
//        }

//        public short? CurrencyDecInPrices
//        {
//            get { return _currencyDecInPrices; }
//        }

//        public short? CurrencyDecInExchange
//        {
//            get { return _currencyDecInExchange; }
//        }

//        #endregion
//    }
//}
