﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class SupplementByBookingContract : BaseContract
    {
        #region Members

        private bool _included;

        #endregion
        #region Properties

        [DataMember]
        public Guid ServiceId { get; set; }
        [DataMember]
        public string ServiceDescription { get; set; }
        [DataMember]
        public bool Included { get { return _included; } set { Set(ref _included, value, "Included"); } }

        #endregion
        #region Constructors

        public SupplementByBookingContract()
            : base()
        {
        }

        public SupplementByBookingContract(Guid serviceId, string serviceDescription, bool included)
            : this()
        {
            ServiceId = serviceId;
            ServiceDescription = serviceDescription;
            Included = included;
        }

        public SupplementByBookingContract(Guid id, Guid serviceId, string serviceDescription, bool included)
            : this(serviceId, serviceDescription, included)
        {
            Id = id;
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    [CustomValidation(typeof(BookingServiceContract), "ValidateBookingService")]
    public class BookingServiceContract : BaseContract
    {
        #region Members

        private Guid? _entityId;
        private Guid? _agencyId;
        private Guid? _contractId;
        private Guid? _departamentPayment;
        private string _entityDesc;
        private string _agencyDesc;
        private string _contractDesc;
        private Guid? _userCancellation;
        private string _userCreationDesc;
        private string _userCancellationDesc;
        private Guid? _userCreation;
        private Guid? _departamentAdditional;
        private Guid? _serviceAdditional;
        private  string _channelUser;
        private  string _channelPassword;
        private  string _channelHotelCode;
        private  string _channelHotelName;
        private  string _emailConfirmation;
        private string _channelChainName;
        private string _channelChainCode;
        private  Guid? _allotmentId;
        private string _allotmentDesc;
        private  BookingServiceType? _bookingServiceType;
        private Guid? _templateId;
        private Guid? _hotelTemplateId;
        private bool _creditCardRequired;
        private bool _notifyReviewExpress;
        private bool _allowPMSDateModification;
        private bool _allowPMSOccupiedRoomTypeModification;
        private bool _assignRoomInCheckIn;
        private bool _allowPMSReservedRoomTypeModification;
        private EBookingTravelAgencyIntegrationType _travelAgencyIntegrationMode;
        private EBookingCompanyIntegrationType _entityIntegrationMode;
        private EActionWhenPriceError _actionWhenPriceReservationError;
        private EActionWhenTotalPriceError _actionWhenReservationTotalPriceError;
        private EBookingChannelPaymentsIntegrationType _paymentsIntegrationType;
        private DailyAccountType _reservationPaymentFolder;
        private bool _reservationSyncPush;
        private bool _reservationAsyncPush;
        private string _entityCountry;
        private string _agencyCountry;
        private DateTime? _activationDate;
        private EActionWhenModifyWithoutInsert _actionWhenModifyWithoutInsert;
        private bool _creditCardMapingDefaultIfNoExist;
        private bool _roomRateMapingDefaultIfNoExist;
        private bool _roomRateMapingGeneralIfNoExist;
        private EConfirmationState _reservationConfirmationState;
        private EAttentionMappingType _attentionMappingType;
        private Guid? _reservationDefaultAttentionCode;
        private EBookingGroupIntegrationType _groupIntegrationType;
        private ESynchronizationType _syncType;
        private EExpediaExportLOSType _expediaExportLOSType;
        private EExpediaExportPriceType _exportPriceType;
        private EFastbookingExportType _fastbookingExportType;
        private EUseAdditionals _useAdditionals;
        private EOriginMappingType _originMappingType;
        private ESegmentMappingType _segmentMappingType;
        private EActionWhenNoPricesInformed _actionWhenNoPricesInformed;
        private EActionWhenInvalidTaxInformed _actionWhenInvalidTaxInformed;
        private EActionWhenPMSDataValidationError _actionWhenPMSDataValidationError;
        private EBookingGuestEmailIntegrationType _emailIntegrationType;
        private bool _unassignRoomBookingChannel;
        private bool _allowChangeOccupiedRoomTypeWithAssignedRoom;
        private bool _inactive;

        #endregion
        #region Properties

        [DataMember]
        public string BookingServiceDescription { get; set; }
        [DataMember]
        public BookingServiceType? BookingServiceType
        {
            get { return _bookingServiceType; }
            set
            {
                if (Set(ref _bookingServiceType, value, "BookingServiceType"))
                    NotifyPropertyChanged("Login", "CloudOneBookingService", "PullEnabled", "PushSyncEnabled", "PushAsyncEnabled");
            }
        }
        [DataMember]
        public bool Subcanal { get; set; }
        [DataMember]
        public Guid? EntityId { get { return _entityId; } set { Set(ref _entityId, value, "EntityId"); } }
        [DataMember]
        public Guid? AgencyId { get { return _agencyId; } set { Set(ref _agencyId, value, "AgencyId"); } }
        [DataMember]
        public Guid? ContractId { get { return _contractId; } set { Set(ref _contractId, value, "ContractId"); } }
        [DataMember]
        public string EntityDesc { get { return _entityId.HasValue ? _entityDesc : null; } set { Set(ref _entityDesc, value, "EntityDesc"); } }
        [DataMember]
        public string AgencyDesc { get { return _agencyId.HasValue ? _agencyDesc : null; } set { Set(ref _agencyDesc, value, "AgencyDesc"); } }
        [DataMember]
        public string ContractDesc { get { return _contractId.HasValue ? _contractDesc : null; } set { Set(ref _contractDesc, value, "ContractDesc"); } }
        [DataMember]
        public Guid? SegmentId { get; set; }
        [DataMember]
        public Guid? OriginId { get; set; }
        [DataMember]
        public Guid? UserCreation { get { return _userCreation; } set { Set(ref _userCreation, value, "UserCreation"); } }
        [DataMember]
        public Guid? UserCancellation { get { return _userCancellation; } set { Set(ref _userCancellation, value, "UserCancellation"); } }
        [DataMember]
        public string UserCreationDesc { get { return _userCreationDesc; } set { Set(ref _userCreationDesc, value, "UserCreationDesc"); } }
        [DataMember]
        public string UserCancellationDesc { get { return _userCancellationDesc; } set { Set(ref _userCancellationDesc, value, "UserCancellationDesc"); } }
        [DataMember]
        public Guid? DepartamentPayment { get { return _departamentPayment; } set { Set(ref _departamentPayment, value, "DepartamentPayment"); } }
        [DataMember]
        public Guid? ReservationPaymentId { get; set; }
        [DataMember]
        public Guid? DepartamentAdditional { get { return _departamentAdditional; } set { Set(ref _departamentAdditional, value, "DepartamentAdditional"); } }
        [DataMember]
        public Guid? ServiceAdditional { get { return _serviceAdditional; } set { Set(ref _serviceAdditional, value, "ServiceAdditional"); } }
        [DataMember]
        public bool IsDaily { get; set; }
        [DataMember]
        public AdditionalEntryType? MealTypeAdditional { get; set; }
        [DataMember]
        public bool UserCreationCancellation { get; set; }
        [DataMember]
        public bool Overallotment { get; set; }
        [DataMember]
        public bool Overbooking { get; set; }
        [DataMember]
        public decimal? OverbookingPercent { get; set; }
        [DataMember]
        public bool Availability { get; set; }
        [DataMember]
        public bool NeverConfirmReservation { get; set; }
        [DataMember]
        public bool MaxPersonsRoom { get; set; }
        [DataMember]
        public Guid? WarrantyTypeId { get; set; }
        [DataMember]
        public Guid? CancellationReasonId { get; set; }
        [DataMember]
        public Guid? PaymentCancellationReasonId { get; set; }
        [DataMember]
        public Guid? TaxSchemaId { get; set; }
        [DataMember]
        public DepositInfoType? DepositType { get; set; }
        [DataMember]
        public decimal? DepositValue { get; set; }
        [DataMember]
        public bool CheckPrice { get; set; }
        [DataMember]
        public InvoiceInstructionType InvoiceInstruction { get; set; }
        [DataMember]
        public bool ImportReservations { get; set; }
        [DataMember]
        public bool ExportPrices { get; set; }
        [DataMember]
        public ExternalChannelExportMode ExportMode { get; set; }
        [DataMember]
        public bool ExportRestrictions { get; set; }
        [DataMember]
        public bool ExportClassifiers { get; set; }
        [DataMember]
        public bool AllowCustomReservationEntity { get; set; }
        [DataMember]
        public Guid? ReservationGroupTypeId{ get; set; }
        [DataMember]
        public Guid? ClientTypeId { get; set; }
        [DataMember]
        public DailyAccountType? AccountType { get; set; }
        [DataMember]
        public bool FixPrice { get; set; }
        [DataMember]
        public CheckPriceActionType? CheckPriceAction { get; set; }
        [DataMember]
        public IList<BookingServiceType> BookingServiceTypeList { get;  set; }
        [DataMember]
        public string ChannelUser { get { return _channelUser; } set { Set(ref _channelUser, value, "ChannelUser"); } }
        [DataMember]
        public string ChannelPassword {get { return _channelPassword; } set { Set(ref _channelPassword, value, "ChannelPassword"); }}
        [DataMember]
        public string ChannelHotelCode { get { return _channelHotelCode; } set { Set(ref _channelHotelCode, value, "ChannelHotelCode");} }
        [DataMember]
        public string ChannelHotelName { get { return _channelHotelName; } set { Set(ref _channelHotelName, value, "ChannelHotelName"); } }
        [DataMember]
        public string EmailConfirmation { get { return _emailConfirmation; } set { Set(ref _emailConfirmation, value, "EmailConfirmation"); } }
        [DataMember]
        public string ChannelChainName { get { return _channelChainName; } set { Set(ref _channelChainName, value, "ChannelChainName"); } }
        [DataMember]
        public string ChannelChainCode { get { return _channelChainCode; } set { Set(ref _channelChainCode, value, "ChannelChainCode"); } }
        [DataMember]
        public int? ExportDaysCount { get; set; }
        [DataMember]
        public UseReservationMarketChannel? UseReservationMarketSegment { get; set; }
        [DataMember]
        public UseReservationMarketChannel? UseReservationMarketOrigin  { get; set; }
        [DataMember]
        public string CountryHotel { get; set; }
        [DataMember]
        public string CountryGuest { get; set; }
        [DataMember]
        public ReservationPaymentType? ResePaymentType { get; set; }
        [DataMember]
        public long? LanguageId { get; set; }
        [DataMember]
        public Guid? HotelChannelId { get; set; }
        [DataMember]
        public bool ApplyTaxGuest { get; set; }
        [DataMember]
        public bool AmountAfterTax { get; set; }
        [DataMember]
        public Guid? AllotmentId { get { return _allotmentId; } set { Set(ref _allotmentId, value, "AllotmentId"); } }
        [DataMember]
        public string AllotmentDesc {  get { return _allotmentId.HasValue ? _allotmentDesc : null; } set { Set(ref _allotmentDesc, value, "AllotmentDesc"); } }
        [DataMember]
        public Guid? TemplateId { get { return _templateId; } set { Set(ref _templateId, value, "TemplateId"); } }
        [DataMember]
        public Guid? HotelTemplateId { get { return _hotelTemplateId; } set { Set(ref _hotelTemplateId, value, "HotelTemplateId"); } }
        [DataMember]
        public ReservationPriceCalculation? ChildPriceType { get; set; }
        [DataMember]
        public Guid? BookingIdReferenced { get; set; }
        [DataMember]
        public bool CreditCardRequired { get { return _creditCardRequired; } set { Set(ref _creditCardRequired, value, "CreditCardRequired"); } }
        [DataMember]
        public ReviewExpressType? ReviewExpressType { get; set; }
        [DataMember]
        public bool NotifyReviewExpress { get { return _notifyReviewExpress; } set { Set(ref _notifyReviewExpress, value, "NotifyReviewExpress"); } }
        [DataMember]
        public BookingVoucherType? BookingVoucherType { get; set; }
        [DataMember]
        public bool IgnoreNotifyExternalChannel { get; set; }
        [DataMember]
        public ShowPriceType ShowPriceType { get; set; }
        [DataMember]
        public bool AssignRoomInCheckIn { get { return _assignRoomInCheckIn; } set { Set(ref _assignRoomInCheckIn, value, "AssignRoomInCheckIn"); } }
        [DataMember]
        public bool AllowPMSDateModification { get { return _allowPMSDateModification; } set { Set(ref _allowPMSDateModification, value, "AllowPMSDateModification"); } }
        [DataMember]
        public bool AllowPMSOccupiedRoomTypeModification { get { return _allowPMSOccupiedRoomTypeModification; } set { Set(ref _allowPMSOccupiedRoomTypeModification, value, "AllowPMSOccupiedRoomTypeModification"); } }
        [DataMember]
        public bool AllowPMSReservedRoomTypeModification { get { return _allowPMSReservedRoomTypeModification; } set { Set(ref _allowPMSReservedRoomTypeModification, value, "AllowPMSReservedRoomTypeModification"); } }
        [DataMember]
        public EBookingTravelAgencyIntegrationType TravelAgencyIntegrationMode { get { return _travelAgencyIntegrationMode; } set { Set(ref _travelAgencyIntegrationMode, value, "TravelAgencyIntegrationMode"); } }
        [DataMember]
        public EBookingCompanyIntegrationType EntityIntegrationMode { get { return _entityIntegrationMode; } set { Set(ref _entityIntegrationMode, value, "EntityIntegrationMode"); } }
        [DataMember]
        public EActionWhenPriceError ActionWhenPriceReservationError { get { return _actionWhenPriceReservationError; } set { Set(ref _actionWhenPriceReservationError, value, "ActionWhenPriceReservationError"); } }
        [DataMember]
        public EActionWhenTotalPriceError ActionWhenReservationTotalPriceError { get { return _actionWhenReservationTotalPriceError; } set { Set(ref _actionWhenReservationTotalPriceError, value, "ActionWhenReservationTotalPriceError"); } }
        [DataMember]
        public EBookingChannelPaymentsIntegrationType PaymentsIntegrationType { get { return _paymentsIntegrationType; } set { Set(ref _paymentsIntegrationType, value, "PaymentsIntegrationType"); } }
        [DataMember]
        public bool ReservationSyncPush { get { return _reservationSyncPush; } set { Set(ref _reservationSyncPush, value, "ReservationSyncPush"); } }
        [DataMember]
        public bool ReservationAsyncPush { get { return _reservationAsyncPush; } set { Set(ref _reservationAsyncPush, value, "ReservationAsyncPush"); } }
        [DataMember]
        public DailyAccountType ReservationPaymentFolder { get { return _reservationPaymentFolder; } set { Set(ref _reservationPaymentFolder, value, "ReservationPaymentFolder"); } }
        [DataMember]
        public string EntityCountry { get { return _entityCountry; } set { Set(ref _entityCountry, value, "EntityCountry"); } }
        [DataMember]
        public string AgencyCountry { get { return _agencyCountry; } set { Set(ref _agencyCountry, value, "AgencyCountry"); } }
        [DataMember]
        public DateTime? ActivationDate { get { return _activationDate; } set { Set(ref _activationDate, value, "ActivationDate"); } }
        [DataMember]
        public string Identifier { get; set; }
        [DataMember]
        public EActionWhenModifyWithoutInsert ActionWhenModifyWithoutInsert { get { return _actionWhenModifyWithoutInsert; } set { Set(ref _actionWhenModifyWithoutInsert, value, "ActionWhenModifyWithoutInsert"); } }
        [DataMember]
        public EConfirmationState ReservationConfirmationState { get { return _reservationConfirmationState; } set { Set(ref _reservationConfirmationState, value, "ReservationConfirmationState"); } }
        [DataMember]
        public EAttentionMappingType AttentionMappingType { get { return _attentionMappingType; } set { Set(ref _attentionMappingType, value, "AttentionMappingType"); } }
        [DataMember]
        public Guid? ReservationDefaultAttentionCode { get { return _reservationDefaultAttentionCode; } set { Set(ref _reservationDefaultAttentionCode, value, "ReservationDefaultAttentionCode"); } }
        [DataMember]
        public bool CreditCardMapingDefaultIfNoExist { get { return _creditCardMapingDefaultIfNoExist; } set { Set(ref _creditCardMapingDefaultIfNoExist, value, "CreditCardMapingDefaultIfNoExist"); } }
        [DataMember]
        public bool RoomRateMapingDefaultIfNoExist { get { return _roomRateMapingDefaultIfNoExist; } set { Set(ref _roomRateMapingDefaultIfNoExist, value, "RoomRateMapingDefaultIfNoExist"); } }
        [DataMember]
        public bool RoomRateMapingGeneralIfNoExist { get { return _roomRateMapingGeneralIfNoExist; } set { Set(ref _roomRateMapingGeneralIfNoExist, value, "RoomRateMapingGeneralIfNoExist"); } }
        [DataMember]
        public EBookingGroupIntegrationType GroupIntegrationType { get { return _groupIntegrationType; } set { Set(ref _groupIntegrationType, value, "GroupIntegrationType"); } }
        [DataMember]
        public ESynchronizationType SyncType { get { return _syncType; } set { Set(ref _syncType, value, "SyncType"); } }
        [DataMember]
        public EExpediaExportLOSType ExpediaExportLOSType { get { return _expediaExportLOSType; } set { Set(ref _expediaExportLOSType, value, "ExpediaExportLOSType"); } }
        [DataMember]
        public EExpediaExportPriceType ExportPriceType { get { return _exportPriceType; } set { Set(ref _exportPriceType, value, "ExportPriceType"); } }
        [DataMember]
        public EFastbookingExportType FastbookingExportType { get { return _fastbookingExportType; } set { Set(ref _fastbookingExportType, value, "FastbookingExportType"); } }
        [DataMember]
        public EUseAdditionals UseAdditionals { get { return _useAdditionals; } set { Set(ref _useAdditionals, value, "UseAdditionals"); } }
        [DataMember]
        public EOriginMappingType OriginMappingType { get { return _originMappingType; } set { Set(ref _originMappingType, value, "OriginMappingType"); } }
        [DataMember]
        public ESegmentMappingType SegmentMappingType { get { return _segmentMappingType; } set { Set(ref _segmentMappingType, value, "SegmentMappingType"); } }
        [DataMember]
        public EActionWhenNoPricesInformed ActionWhenNoPricesInformed { get { return _actionWhenNoPricesInformed; } set { Set(ref _actionWhenNoPricesInformed, value, "ActionWhenNoPricesInformed"); } }
        [DataMember]
        public EActionWhenInvalidTaxInformed ActionWhenInvalidTaxInformed { get { return _actionWhenInvalidTaxInformed; } set { Set(ref _actionWhenInvalidTaxInformed, value, "ActionWhenInvalidTaxInformed"); } }
        [DataMember]
        public EActionWhenPMSDataValidationError ActionWhenPMSDataValidationError { get { return _actionWhenPMSDataValidationError; } set { Set(ref _actionWhenPMSDataValidationError, value, "ActionWhenPMSDataValidationError"); } }
        [DataMember]
        public EBookingGuestEmailIntegrationType EmailIntegrationType { get { return _emailIntegrationType; } set { Set(ref _emailIntegrationType, value, "EmailIntegrationType"); } }
        [DataMember]
        public bool UnassignRoomBookingChannel { get { return _unassignRoomBookingChannel; } set { Set(ref _unassignRoomBookingChannel, value, "UnassignRoomBookingChannel"); } }
        [DataMember]
        public bool AllowChangeOccupiedRoomTypeWithAssignedRoom { get { return _allowChangeOccupiedRoomTypeWithAssignedRoom; } set { Set(ref _allowChangeOccupiedRoomTypeWithAssignedRoom, value, "AllowChangeOccupiedRoomTypeWithAssignedRoom"); } }
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public string Login
        {
            get
            {
                if (_bookingServiceType.HasValue)
                    return BookingServiceType.Value.ToString().ToUpperInvariant();

                return string.Empty;
            }
        }

        [ReflectionExclude]
        public Guid? PriceRateId
        {
            get
            {
                if (PriceRatesByBooking != null && PriceRatesByBooking.Count > 0)
                {
                    var priceRateByBooking = PriceRatesByBooking.FirstOrDefault(x => x.IsDefault);
                    if (priceRateByBooking != null)
                        return priceRateByBooking.PriceRateId;
                }

                return null;
            }
        }

        [ReflectionExclude]
        public string PriceRateDesc
        {
            get
            {
                if (PriceRatesByBooking != null && PriceRatesByBooking.Count > 0)
                {
                    var priceRateByBooking = PriceRatesByBooking.FirstOrDefault(x => x.IsDefault);
                    if (priceRateByBooking != null)
                        return priceRateByBooking.PriceRateDesc;
                }

                return null;
            }
        }

        [ReflectionExclude]
        public bool PullEnabled
        {
            get
            {
                if (BookingServiceType.HasValue)
                {
                    switch (BookingServiceType.Value)
                    {
                        case Contracts.BookingServiceType.GenericOTA:
                        case Contracts.BookingServiceType.EGDS:
                        case Contracts.BookingServiceType.Expedia:
                        case Contracts.BookingServiceType.GuestCentric:
                        case Contracts.BookingServiceType.BookAssist:
                        case Contracts.BookingServiceType.SynXis:
                        case Contracts.BookingServiceType.Other:
                        case Contracts.BookingServiceType.Pegasus:
                        case Contracts.BookingServiceType.Cubilis:
                        case Contracts.BookingServiceType.WebBooking:
                        case Contracts.BookingServiceType.SiteMinder:
                        case Contracts.BookingServiceType.AvailPro:
                        case Contracts.BookingServiceType.TripAdvisor:
                        case Contracts.BookingServiceType.Booking:
                        case Contracts.BookingServiceType.NeoBookings:
                        case Contracts.BookingServiceType.EnzoKiosk:
                        case Contracts.BookingServiceType.EGDSInatel:
                        case Contracts.BookingServiceType.DBK:
                        case Contracts.BookingServiceType.RateGain:
                        case Contracts.BookingServiceType.Omnibees:
                        case Contracts.BookingServiceType.ParityRate:
                        case Contracts.BookingServiceType.Fastbooking:
                        case Contracts.BookingServiceType.TravelClickGMS:
                        case Contracts.BookingServiceType.Airbnb:
                            return true;
                    }
                }

                return false;
            }
        }

        [ReflectionExclude]
        public bool PushSyncEnabled
        {
            get
            {
                if (BookingServiceType.HasValue)
                {
                    switch (BookingServiceType.Value)
                    {
                        case Contracts.BookingServiceType.GenericOTA:
                        case Contracts.BookingServiceType.EGDS:
                        case Contracts.BookingServiceType.Idiso:
                        case Contracts.BookingServiceType.Dingus:
                            return true;
                    }
                }

                return false;
            }
        }

        [ReflectionExclude]
        public bool PushAsyncEnabled
        {
            get
            {
                if (BookingServiceType.HasValue)
                {
                    switch (BookingServiceType.Value)
                    {
                        case Contracts.BookingServiceType.TravelClick:
                        case Contracts.BookingServiceType.SynxisHTNG:
                            return true;
                    }
                }

                return false;
            }
        }

        [ReflectionExclude]
        public bool CloudOneBookingService
        {
            get { return !BookingServiceType.HasValue || BookingServiceType.Value == Contracts.BookingServiceType.CloudOne; }
        }

        #endregion
        #region Contracts

        [DataMember]
        public TypedList<PensionModeByBookingContract> PensionModesByBooking { get; internal set; }
        [DataMember]
        public TypedList<PriceRatesByBookingContract> PriceRatesByBooking { get; internal set; }
        [DataMember]
        public TypedList<SupplementByBookingContract> SupplementsByBooking { get; internal set; }

        #endregion
        #region Constructors

        public BookingServiceContract()
            : base()
        {
            PensionModesByBooking = new TypedList<PensionModeByBookingContract>();
            PriceRatesByBooking = new TypedList<PriceRatesByBookingContract>();
            SupplementsByBooking = new TypedList<SupplementByBookingContract>();

            BookingServiceTypeList = new List<BookingServiceType>();
            ExportMode = ExternalChannelExportMode.None;
            InvoiceInstruction = InvoiceInstructionType.None;
            ShowPriceType = Contracts.ShowPriceType.Both;
            ReservationPaymentFolder = DailyAccountType.Master;
            GroupIntegrationType = EBookingGroupIntegrationType.None;
            SyncType = ESynchronizationType.SyncByGroup;
            ExpediaExportLOSType = EExpediaExportLOSType.Arrival;
            ExportPriceType = EExpediaExportPriceType.Occupancy;
            FastbookingExportType = EFastbookingExportType.RoomRateBasedAllotment;
            UseAdditionals = EUseAdditionals.None;
            OriginMappingType = EOriginMappingType.None;
            SegmentMappingType = ESegmentMappingType.None;
            ActionWhenInvalidTaxInformed = EActionWhenInvalidTaxInformed.NotifyError;
            ActionWhenNoPricesInformed = EActionWhenNoPricesInformed.NotifyError;
            ActionWhenPMSDataValidationError = EActionWhenPMSDataValidationError.NotifyError;
            EmailIntegrationType = EBookingGuestEmailIntegrationType.AsEmail;

            PriceRatesByBooking.CollectionChanged += (s, ev) =>
            {
                NotifyPropertyChanged("PriceRateDesc");
            };
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateBookingService(BookingServiceContract obj)
        {
            if (string.IsNullOrEmpty(obj.BookingServiceDescription))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description required.");

            if (!obj.BookingServiceType.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Booking service type required.");
            else
            {
                var bookingServiceTypeName = Enum.GetName(typeof(BookingServiceType), obj.BookingServiceType.Value);

                if (!obj.PushAsyncEnabled && obj.ReservationAsyncPush)
                    return new System.ComponentModel.DataAnnotations.ValidationResult(string.Format("Push async not allowed on {0}.", bookingServiceTypeName));
                if (!obj.PushSyncEnabled && obj.ReservationSyncPush)
                    return new System.ComponentModel.DataAnnotations.ValidationResult(string.Format("Push sync not allowed on {0}.", bookingServiceTypeName));
                if (!obj.PullEnabled && obj.ImportReservations)
                    return new System.ComponentModel.DataAnnotations.ValidationResult(string.Format("Import reservations not allowed on {0}.", bookingServiceTypeName));

                if (string.IsNullOrEmpty(obj.ChannelUser))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Channel user required.");
                if (string.IsNullOrEmpty(obj.ChannelHotelCode))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Channel hotel code required.");
                if (string.IsNullOrEmpty(obj.ChannelHotelName))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Channel hotel name required.");
                if (string.IsNullOrEmpty(obj.EmailConfirmation))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Email confirmation required.");
                if (!obj.ExportMode.Equals(ExternalChannelExportMode.None) && ((obj.ExportDaysCount ?? 0) <= 0))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Count days to export required.");
                if (!obj.ExportMode.Equals(ExternalChannelExportMode.None) && ((obj.ExportDaysCount ?? 0) > 731))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Count days to export over two years not allowed.");
            }

            if (!obj.ClientTypeId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Client type required.");
            if (!obj.UserCreation.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("User creation required.");
            if (!obj.UserCancellation.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("User cancellation required.");
            //if (!obj.CancellationReasonId.HasValue)
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Cancellation reason required.");
            //if (!obj.WarrantyTypeId.HasValue)
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Warranty type required.");
            if (obj.CheckPrice == true && !obj.CheckPriceAction.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Check price action type required.");
            if (!obj.UseReservationMarketOrigin.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Use reservation market origin required.");
            if (!obj.UseReservationMarketSegment.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Use reservation market segment required.");
            if (!obj.TaxSchemaId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Tax schema required.");
            if (!obj.CheckPriceAction.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Check break down action required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class GroupBookingContract : BaseContract
    {
        #region Members

        private string _webServExternalUser;
        private string _webServExternalPassword;
        private bool _webServExternalActive;

        #endregion
        #region Properties

        [DataMember]
        public string WebServExternalUser { get { return _webServExternalUser; } set { Set(ref _webServExternalUser, value, "WebServExternalUser"); } }
        [DataMember]
        public string WebServExternalPassword { get { return _webServExternalPassword; } set { Set(ref _webServExternalPassword, value, "WebServExternalPassword"); } }
        [DataMember]
        public bool WebServExternalActive { get { return _webServExternalActive; } set { Set(ref _webServExternalActive, value, "WebServExternalActive"); } }

        #endregion
        #region Constructor

        public GroupBookingContract()
            : base() { }

        #endregion
    }
}