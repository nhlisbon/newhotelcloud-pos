﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PriceRatesByBookingContract: BaseContract
    {
        [DataMember]
        public Guid BookingServiceId { get; set; }
        [DataMember]
        public Guid PriceRateId { get; set; }
        [DataMember]
        public string PriceRateDesc { get; set; }
        [DataMember]
        public bool IsDefault { get; set; }

        public PriceRatesByBookingContract(Guid bookingServiceId, Guid priceRateId, string priceRateDesc, bool isDefault)
        {
            BookingServiceId = bookingServiceId;
            PriceRateId = priceRateId;
            PriceRateDesc = priceRateDesc;
            IsDefault = isDefault;
        }

        public PriceRatesByBookingContract(Guid id, Guid bookingServiceId, Guid priceRateId, string priceRateDesc, bool isDefault)
            : this(bookingServiceId, priceRateId, priceRateDesc, isDefault)
        {
            Id = id;
        }
    }
}