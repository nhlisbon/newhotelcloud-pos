﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PensionModeByBookingContract: BaseContract
    {
        [DataMember]
        public Guid BookingServiceId { get; set; }
        [DataMember]
        public Guid PensionModeId { get; set; }
        [DataMember]
        public string PensionModeDesc { get; set; }
        [DataMember]
        public short PensionMode { get; set; }
        [DataMember]
        public bool IsDefault { get; set; }

        public PensionModeByBookingContract(Guid bookingServiceId, Guid pensionModeId, string pensionModeDesc, short pensionMode, bool isDefault)
        {
            BookingServiceId = bookingServiceId;
            PensionModeId = pensionModeId;
            PensionModeDesc = pensionModeDesc;
            PensionMode = pensionMode;
            IsDefault = isDefault;
        }

        public PensionModeByBookingContract(Guid id, Guid bookingServiceId, Guid pensionModeId, string pensionModeDesc, short pensionMode, bool isDefault)
            : this(bookingServiceId, pensionModeId, pensionModeDesc, pensionMode, isDefault)
        {
            Id = id;
        }
    }
}