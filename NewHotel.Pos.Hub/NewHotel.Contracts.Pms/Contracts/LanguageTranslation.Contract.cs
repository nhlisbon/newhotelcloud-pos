﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.ComponentModel;
//using System.Runtime.Serialization;

//namespace NewHotel.Contracts
//{
//    public class TranslationInfoContract
//    {
//        public readonly long LanguageId;
//        public readonly string LanguageName;
//        public readonly string Translation;

//        public TranslationInfoContract(long languageId, string languageName, string translation)
//        {
//            LanguageId = languageId;
//            LanguageName = languageName;
//            Translation = translation;
//        }

//        private TranslationInfoContract() { }
//    }

//    [CollectionDataContract]
//    [KnownType(typeof(TranslationInfoContract))]
//    public partial class LanguageTranslationContract : BaseContract, IEnumerable<TranslationInfoContract>
//    {
//        #region Contract properties

//        private readonly List<TranslationInfoContract> _translations;

//        #endregion

//        public LanguageTranslationContract() 
//            : base()
//        {
//            _translations = new List<TranslationInfoContract>();
//        }

//        public LanguageTranslationContract(object id, IEnumerable<TranslationInfoContract> translations)
//            : base()
//        {
//            Id = id;
//            _translations = new List<TranslationInfoContract>(translations);
//        }

//        private TranslationInfoContract Find(long language)
//        {
//            return _translations.FirstOrDefault(x => x.LanguageId == language);
//        }

//        [IgnoreDataMember]
//        [ReflectionExclude]
//        public string this[long language]
//        {
//            get
//            {
//                TranslationInfoContract info = Find(language);

//                if (info == null)
//                    throw new ArgumentOutOfRangeException(string.Format("language ({0})", language));

//                return info.Translation;
//            }
//            set
//            {
//                TranslationInfoContract info = Find(language);

//                if (info == null)
//                    throw new ArgumentOutOfRangeException(string.Format("language ({0})", language));

//                if (info.Translation != value)
//                {
//                    int index = _translations.IndexOf(info);
//                    _translations[index] = new TranslationInfoContract(language,
//                        info.LanguageName, value ?? string.Empty);
//                }
//            }
//        }

//        public static implicit operator string(LanguageTranslationContract languageTranslationContract)
//        {
//            string translation = languageTranslationContract[languageTranslationContract.Language];

//            if (string.IsNullOrEmpty(translation))
//            {
//                TranslationInfoContract info = languageTranslationContract
//                    .FirstOrDefault(x => !string.IsNullOrEmpty(x.Translation));

//                translation = info != null ? info.Translation : string.Empty;
//            }

//            return translation;
//        }

//        public void Clear()
//        {
//            _translations.Clear();
//        }

//        public void AddRange(IEnumerable<TranslationInfoContract> translationInfos)
//        {
//            _translations.AddRange(translationInfos);
//        }

//        public void Add(TranslationInfoContract translationInfos)
//        {
//            _translations.Add(translationInfos);
//        }

//        [IgnoreDataMember]
//        [ReflectionExclude(Hide = true)]
//        public bool IsEmpty
//        {
//            get { return !_translations.Any(x => !string.IsNullOrEmpty(x.Translation)); }
//        }

//        [IgnoreDataMember]
//        [ReflectionExclude]
//        public int Count
//        {
//            get { return _translations.Count; }
//        }

//        [IgnoreDataMember]
//        [ReflectionExclude]
//        public long Language { get; set; }

//        #region IEnumerable<TranslationInfoContract> Members

//        public IEnumerator<TranslationInfoContract> GetEnumerator()
//        {
//            return _translations.GetEnumerator();
//        }

//        #endregion

//        #region IEnumerable Members

//        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
//        {
//            return _translations.GetEnumerator();
//        }

//        #endregion
//    }
//}
