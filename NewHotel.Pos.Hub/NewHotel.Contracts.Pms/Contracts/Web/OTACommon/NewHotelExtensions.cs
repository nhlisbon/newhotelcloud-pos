﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NewHotel.Contracts.OTA
{
    #region NewHotel class

    #region NewHotel Mapping class

    //[System.SerializableAttribute()]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = false)]

    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GeneralMapping
    {
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string value { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name { get; set; }
    }

    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class CreditCardMapping : GeneralMapping { }

    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class GuaranteeMapping : GeneralMapping { }

    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class SourceOfBusinessMapping : GeneralMapping { }

    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class MarketSegmentMapping : GeneralMapping { }

    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class CustomerMapping : GeneralMapping
    {
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CustomerTitle { get; set; }
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CustomerTitleName { get; set; }
    }

    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class EntityMapping : GeneralMapping { }


    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class CurrencyMapping : GeneralMapping { }

    public enum EAccountType { Master, Extra1, Extra2, Extra3, Extra4, Extra5 };

    public enum PMSRatePlans { RoomOnly, BedBreakfast, HalfBoard, FullBoard, AllInclusive };

   [System.Xml.Serialization.XmlType(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum AdditionalEntryType
    {

        /// <remarks/>
        Standard,

        /// <remarks/>
        Breakfast,

        /// <remarks/>
        Lunch,

        /// <remarks/>
        Dinner,
    }

    //public enum PMSRatePlans { RoomOnly = '1', BedBreakfast = '2', HalfBoard = '3', FullBoard = '4', AllInclusive = '5' };

    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class ServiceMapping : GeneralMapping
    {
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SectionCode { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ServiceCode { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SectionName { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ServiceName { get; set; }

        [XmlIgnore]
        public bool AccountTypeSpecified { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public EAccountType AccountType { get; set; }

        [XmlIgnore]
        public bool TypeSpecified { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public AdditionalEntryType Type { get; set; }

        //[System.Xml.Serialization.XmlAttributeAttribute()]
        //public PMSRatePlans Type { get; set; }



        [XmlIgnore]
        public bool DailySpecified { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Daily { get; set; }
    }

    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class SpecialRequestMapping : GeneralMapping
    {
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string SpecialRequestRPH { get; set; }
    }

    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class RoomRateMapping
    {
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string RoomRateRPH { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string OperatorCode { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string OperatorName { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AllotmentCode { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AllotmentName { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PriceRateCode { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PriceRateName { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PackageCode { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PackageName { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RoomTypeCode { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RoomTypeName { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PMSRatePlans RatePlanCode { get; set; }
    }

    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class RatePlanCandidateMapping
    {
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string RatePlanCandidateRPH { get; set; }

        [XmlIgnore]
        public bool RatePlanCandidateRPHSpecified { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string OperatorCode { get; set; }
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AllotmentCode { get; set; }
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PMSRatePlans MealPlanCode { get; set; }
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MealPlanCodeSpecified { get; set; }
    }


    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class PMSStatusApplication
    {
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string EntityCode { get; set; }
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string OperatorCode { get; set; }
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AllotmentCode { get; set; }
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PMSRatePlans MealPlanCode { get; set; }

        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MealPlanCodeSpecified { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PackageCode { get; set; }
    }

  

    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class TPAMapping
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ID { get; set; }

        private object[] itemsField;

        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("RoomRateMappings", typeof(RoomRateMapping))]
        [System.Xml.Serialization.XmlElementAttribute("EntityMapping", typeof(EntityMapping))]
        [System.Xml.Serialization.XmlElementAttribute("CurrencyMapping", typeof(CurrencyMapping))]
        [System.Xml.Serialization.XmlElementAttribute("ServiceMapping", typeof(ServiceMapping))]
        [System.Xml.Serialization.XmlElementAttribute("CreditCardMapping", typeof(CreditCardMapping))]
        [System.Xml.Serialization.XmlElementAttribute("CustomerMapping", typeof(CustomerMapping))]
        [System.Xml.Serialization.XmlElementAttribute("GuaranteeMapping", typeof(GuaranteeMapping))]
        [System.Xml.Serialization.XmlElementAttribute("MarketSegmentMapping", typeof(MarketSegmentMapping))]
        [System.Xml.Serialization.XmlElementAttribute("SourceOfBusinessMapping", typeof(SourceOfBusinessMapping))]
        [System.Xml.Serialization.XmlElementAttribute("SpecialRequestMapping", typeof(SpecialRequestMapping))]
        [System.Xml.Serialization.XmlElementAttribute("PMSStatusApplication", typeof(PMSStatusApplication))]
        [System.Xml.Serialization.XmlElementAttribute("RatePlanCandidateMapping", typeof(RatePlanCandidateMapping))]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
    }
    #endregion

    #region NewHotel Classifiers
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class Classifiers
    {
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public virtual string value { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public virtual string name { get; set; }
    }

    #region EntitiesClassifiers
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class EntitiesClassifiers
    {
        private EntityClassifier[] entitiesField;

        [System.Xml.Serialization.XmlElementAttribute("Entity")]
        public EntityClassifier[] Entities
        {
            get
            {
                return this.entitiesField;
            }
            set
            {
                this.entitiesField = value;
            }
        }
    }


    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class EntityClassifier : Classifiers
    {
        private Classifiers[] operatorsField;

        [System.Xml.Serialization.XmlElementAttribute("Operator")]
        public Classifiers[] Operators
        {
            get
            {
                return this.operatorsField;
            }
            set
            {
                this.operatorsField = value;
            }
        }
    }
    #endregion

    #region SectionsClassifier
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class SectionsClassifier
    {
        private SectionClassifier[] sectionsField;

        [System.Xml.Serialization.XmlElementAttribute("Section")]
        public SectionClassifier[] Sections
        {
            get
            {
                return this.sectionsField;
            }
            set
            {
                this.sectionsField = value;
            }
        }
    }

    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class SectionClassifier : Classifiers
    {
        private Classifiers[] servicessField;

        [System.Xml.Serialization.XmlElementAttribute("Service")]
        public Classifiers[] Services
        {
            get
            {
                return this.servicessField;
            }
            set
            {
                this.servicessField = value;
            }
        }
    }
    #endregion

    #region RoomTypesClassifier
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class RoomTypesClassifier
    {
        private Classifiers[] roomTypesField;

        [System.Xml.Serialization.XmlElementAttribute("RoomType")]
        public Classifiers[] RoomTypes
        {
            get
            {
                return this.roomTypesField;
            }
            set
            {
                this.roomTypesField = value;
            }
        }
    }
    #endregion


    #region CreditCardsClassifier
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class CreditCardsClassifier
    {
        private Classifiers[] creditCardsField;

        [System.Xml.Serialization.XmlElementAttribute("CreditCard")]
        public Classifiers[] CreditCards
        {
            get
            {
                return this.creditCardsField;
            }
            set
            {
                this.creditCardsField = value;
            }
        }
    }
    #endregion


    #region MarketSegmentsClassifier
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class MarketSegmentsClassifier
    {
        private Classifiers[] marketSegmentsField;

        [System.Xml.Serialization.XmlElementAttribute("MarketSegment")]
        public Classifiers[] MarketSegments
        {
            get
            {
                return this.marketSegmentsField;
            }
            set
            {
                this.marketSegmentsField = value;
            }
        }
    }
    #endregion

    #region SourcesOfBusinessClassifier
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class SourcesOfBusinessClassifier
    {
        private Classifiers[] sourcesOfBusinessField;

        [System.Xml.Serialization.XmlElementAttribute("SourceOfBusiness")]
        public Classifiers[] SourcesOfBusiness
        {
            get
            {
                return this.sourcesOfBusinessField;
            }
            set
            {
                this.sourcesOfBusinessField = value;
            }
        }
    }
    #endregion

    #region GuarantiesClassifier
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class GuarantiesClassifier
    {
        private Classifiers[] guarantiesField;

        [System.Xml.Serialization.XmlElementAttribute("Guarantee")]
        public Classifiers[] Guaranties
        {
            get
            {
                return this.guarantiesField;
            }
            set
            {
                this.guarantiesField = value;
            }
        }
    }
    #endregion


    #region TitlesClassifier
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class TitlesClassifier
    {
        private Classifiers[] titlesField;

        [System.Xml.Serialization.XmlElementAttribute("Title")]
        public Classifiers[] Titles
        {
            get
            {
                return this.titlesField;
            }
            set
            {
                this.titlesField = value;
            }
        }
    }

    #endregion

    #region PriceRatesClassifier
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class PriceRatesClassifier
    {
        private Classifiers[] priceRatesField;

        [System.Xml.Serialization.XmlElementAttribute("PriceRate")]
        public Classifiers[] PriceRates
        {
            get
            {
                return this.priceRatesField;
            }
            set
            {
                this.priceRatesField = value;
            }
        }
    }
    #endregion

    #region CurrenciesClassifier

    public class CurrenciesClassifier
    {
        private Classifiers[] currenciesField;

        [System.Xml.Serialization.XmlElementAttribute("Currency")]
        public Classifiers[] Currencies
        {
            get
            {
                return this.currenciesField;
            }
            set
            {
                this.currenciesField = value;
            }
        }
    }
    #endregion

    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(IncludeInSchema = false)]
    public enum TPAClassifiersItemsType
    {
        Entities,
        Sections,
        RoomTypes,
        CreditCardTypes,
        Guaranties,
        SourcesOfBusiness,
        MarketSegments,
        Titles,
        PriceRates,
        Currencies,
    }

    [System.Xml.Serialization.XmlType("TPAClassifiers")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class TPAClassifiers
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ID { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemsElementName")]
        [System.Xml.Serialization.XmlElement("Entities", typeof(EntitiesClassifiers))]
        [System.Xml.Serialization.XmlElementAttribute("Sections", typeof(SectionsClassifier))]
        [System.Xml.Serialization.XmlElementAttribute("RoomTypes", typeof(RoomTypesClassifier))]
        [System.Xml.Serialization.XmlElementAttribute("CreditCardTypes", typeof(CreditCardsClassifier))]
        [System.Xml.Serialization.XmlElementAttribute("Guaranties", typeof(GuarantiesClassifier))]
        [System.Xml.Serialization.XmlElementAttribute("SourcesOfBusiness", typeof(SourcesOfBusinessClassifier))]
        [System.Xml.Serialization.XmlElementAttribute("MarketSegments", typeof(MarketSegmentsClassifier))]
        [System.Xml.Serialization.XmlElementAttribute("Titles", typeof(TitlesClassifier))]
        [System.Xml.Serialization.XmlElementAttribute("PriceRates", typeof(PriceRatesClassifier))]
        [System.Xml.Serialization.XmlElementAttribute("Currencies", typeof(CurrenciesClassifier))]

        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
        private object[] itemsField;




        private TPAClassifiersItemsType[] itemsElementNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ItemsElementName")]
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public TPAClassifiersItemsType[] ItemsElementName
        {
            get
            {
                return this.itemsElementNameField;
            }
            set
            {
                this.itemsElementNameField = value;
            }
        }
    }


    //[System.Xml.Serialization.XmlType("DescriptiveContentType")]
    //public class DescriptiveContentType
    //{
    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlElementAttribute("BasicPropertyInfo", typeof(BasicPropertyInfoType))]

    //    [System.Xml.Serialization.XmlElementAttribute("Entities", typeof(EntitiesClassifiers))]
    //    [System.Xml.Serialization.XmlElementAttribute("Sections", typeof(SectionsClassifier))]
    //    [System.Xml.Serialization.XmlElementAttribute("RoomTypes", typeof(RoomTypesClassifier))]
    //    [System.Xml.Serialization.XmlElementAttribute("CreditCardTypes", typeof(CreditCardsClassifier))]
    //    [System.Xml.Serialization.XmlElementAttribute("Guaranties", typeof(GuarantiesClassifier))]
    //    [System.Xml.Serialization.XmlElementAttribute("SourceOfBusiness", typeof(SourcesOfBusinessClassifier))]
    //    [System.Xml.Serialization.XmlElementAttribute("MarketSegments", typeof(MarketSegmentsClassifier))]
    //    [System.Xml.Serialization.XmlElementAttribute("Titles", typeof(TitlesClassifier))]
    //    [System.Xml.Serialization.XmlElementAttribute("PriceRates", typeof(PriceRatesClassifier))]
    //    [System.Xml.Serialization.XmlElementAttribute("Currencies", typeof(CurrenciesClassifier))]

    //    public object[] Items
    //    {
    //        get
    //        {
    //            return this.itemsField;
    //        }
    //        set
    //        {
    //            this.itemsField = value;
    //        }
    //    }
    //    private object[] itemsField;
    //}

    //[System.Xml.Serialization.XmlType("DescriptiveContentsType")]
    //public class DescriptiveContentsType
    //{
    //    private DescriptiveContentType[] descriptiveContentField;

    //    [System.Xml.Serialization.XmlElementAttribute("DescriptiveContent")]
    //    public DescriptiveContentType[] DescriptiveContent
    //    {
    //        get
    //        {
    //            return this.descriptiveContentField;
    //        }
    //        set
    //        {
    //            this.descriptiveContentField = value;
    //        }
    //    }
    //}

    #endregion
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class SourceOfBusiness
    {

        private string primaryCodeField;

        private string primaryIDField;

        private string secondaryIDField;

        private string secondaryCodeField;

        private string subSourceIDField;

        private string subSourceCodeField;

        private string marketSourceCodeField;

        private string croCodeField;

        private string valueField;

        private string sourceOfBusinessCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PrimaryCode
        {
            get
            {
                return this.primaryCodeField;
            }
            set
            {
                this.primaryCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PrimaryID
        {
            get
            {
                return this.primaryIDField;
            }
            set
            {
                this.primaryIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SecondaryID
        {
            get
            {
                return this.secondaryIDField;
            }
            set
            {
                this.secondaryIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SecondaryCode
        {
            get
            {
                return this.secondaryCodeField;
            }
            set
            {
                this.secondaryCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SubSourceID
        {
            get
            {
                return this.subSourceIDField;
            }
            set
            {
                this.subSourceIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SubSourceCode
        {
            get
            {
                return this.subSourceCodeField;
            }
            set
            {
                this.subSourceCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MarketSourceCode
        {
            get
            {
                return this.marketSourceCodeField;
            }
            set
            {
                this.marketSourceCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CroCode
        {
            get
            {
                return this.croCodeField;
            }
            set
            {
                this.croCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SourceOfBusinessCode
        {
            get
            {
                return this.sourceOfBusinessCodeField;
            }
            set
            {
                this.sourceOfBusinessCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class MarketSegment
    {

        private string marketSegmentCodeField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MarketSegmentCode
        {
            get
            {
                return this.marketSegmentCodeField;
            }
            set
            {
                this.marketSegmentCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    #endregion

    #region OTA
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class GuestCountTypeGuestCount
    {

        private string ageQualifyingCodeField;

        private int ageField;

        private int countField;

        private string ageBucketField;

        private string resGuestRPHField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AgeQualifyingCode
        {
            get
            {
                return this.ageQualifyingCodeField;
            }
            set
            {
                this.ageQualifyingCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "int")]
        public int Age
        {
            get
            {
                return this.ageField;
            }
            set
            {
                this.ageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "int")]
        public int Count
        {
            get
            {
                return this.countField;
            }
            set
            {
                this.countField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AgeBucket
        {
            get
            {
                return this.ageBucketField;
            }
            set
            {
                this.ageBucketField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ResGuestRPH
        {
            get
            {
                return this.resGuestRPHField;
            }
            set
            {
                this.resGuestRPHField = value;
            }
        }
    }

    public partial class HotelReservationIDsTypeHotelReservationID
    {
        private string resIDField;
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ResID
        {
            get
            {
                return this.resIDField;
            }
            set
            {
                this.resIDField = value;
            }
        }
    }

    public partial class StatusApplicationControlType
    {
        private TPA_ExtensionsType tPA_ExtensionsField;
        /// <remarks/>
        public TPA_ExtensionsType TPA_Extensions
        {
            get
            {
                return this.tPA_ExtensionsField;
            }
            set
            {
                this.tPA_ExtensionsField = value;
            }
        }
    }

    public partial class HotelRatePlanType
    {
        private DateTime startField;
        private DateTime endField;
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public DateTime Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public DateTime End
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }

        private TPA_ExtensionsType tPA_ExtensionsField;
        /// <remarks/>
        public TPA_ExtensionsType TPA_Extensions
        {
            get
            {
                return this.tPA_ExtensionsField;
            }
            set
            {
                this.tPA_ExtensionsField = value;
            }
        }
    }

    public partial class HotelRatePlanTypeRate
    {
        private TPA_ExtensionsType tPA_ExtensionsField;
        /// <remarks/>
        public TPA_ExtensionsType TPA_Extensions
        {
            get
            {
                return this.tPA_ExtensionsField;
            }
            set
            {
                this.tPA_ExtensionsField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class HotelReservationsIDsType
    {
        private TransactionActionType resStatusField;
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public TransactionActionType ResStatus
        {
            get
            {
                return this.resStatusField;
            }
            set
            {
                this.resStatusField = value;
            }
        }

        private bool resStatusFieldSpecified;
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ResStatusSpecified
        {
            get
            {
                return this.resStatusFieldSpecified;
            }
            set
            {
                this.resStatusFieldSpecified = value;
            }
        }



        private HotelReservationIDsTypeHotelReservationID[] hotelReservationIDField;

        [System.Xml.Serialization.XmlElementAttribute("HotelReservationID")]
        public HotelReservationIDsTypeHotelReservationID[] HotelReservationID
        {
            get
            {
                return this.hotelReservationIDField;
            }
            set
            {
                this.hotelReservationIDField = value;
            }
        }
    }

    public enum EChannelType { Expedia = 1, Booking = 2, SynXis = 3, BookAssist = 4, WebBooking = 5, EGDS = 6, GuestCentric = 7, Pegasus = 8, Cubilis = 9, TravelClick = 10, SiteMinder = 11, AvailPro = 12 };

    public enum EStayType { Stay = 1 };

    public partial class TPAChannelType
    {
        public EChannelType ChannelType { get; set; }
        public string ChannelIdentifier { get; set; }
    }

    public partial class AvailRequestType
    {
        public EStayType Type { get; set; }
    }

    public partial class TaxesType
    {
        [System.Xml.Serialization.XmlAttributeAttribute("TaxInclusive")]
        public bool TaxInclusive { get; set; }

        [XmlIgnore]
        public bool TaxInclusiveSpecified { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute("FixedTaxInclusive")]
        public bool FixedTaxInclusive { get; set; }

        [XmlIgnore]
        public bool FixedTaxInclusiveSpecified { get; set; }
    }

    [System.Xml.Serialization.XmlType("TPA_ExtensionsType")]
    public partial class TPA_ExtensionsType
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Warnings", typeof(WarningsType))]
        [System.Xml.Serialization.XmlElementAttribute("TPAMapping", typeof(TPAMapping))]
        [System.Xml.Serialization.XmlElementAttribute("MarketSegment", typeof(MarketSegment))]
        [System.Xml.Serialization.XmlElementAttribute("SourceOfBusiness", typeof(SourceOfBusiness))]
        [System.Xml.Serialization.XmlElementAttribute("HotelReservationsIDs", typeof(HotelReservationsIDsType))]
        [System.Xml.Serialization.XmlElementAttribute("TPAClassifiers", typeof(TPAClassifiers))]
        [System.Xml.Serialization.XmlElementAttribute("TPAChannelType", typeof(TPAChannelType))]
        [System.Xml.Serialization.XmlElementAttribute("AvailRequest", typeof(AvailRequestType))]
        [System.Xml.Serialization.XmlElementAttribute("Taxes", typeof(TaxesType))]
        [System.Xml.Serialization.XmlElementAttribute("RoomTypeCandidates", typeof(OTA_HotelRatePlanRQRoomTypeCandidatesType))]
        [System.Xml.Serialization.XmlElementAttribute("GuestCounts", typeof(GuestCountType))]

        //[System.Xml.Serialization.XmlElementAttribute("DescriptiveContents", typeof(DescriptiveContentsType))]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
        private object[] itemsField;
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_HotelRatePlanRQRoomTypeCandidate : RoomStayCandidateType
    {


    }

    public class OTA_HotelRatePlanRQRoomTypeCandidatesType
    {
        private OTA_HotelRatePlanRQRoomTypeCandidate[] roomTypeCandidateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("RoomTypeCandidate")]
        public OTA_HotelRatePlanRQRoomTypeCandidate[] RoomTypeCandidate
        {
            get
            {
                return this.roomTypeCandidateField;
            }
            set
            {
                this.roomTypeCandidateField = value;
            }
        }
    }

    /// <remarks>muda a forma de serializar o campo RPH para attribute</remarks>/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class ResGuestRPHType
    {
        private string rPHField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RPH
        {
            get
            {
                return this.rPHField;
            }
            set
            {
                this.rPHField = value;
            }
        }
    }

    /// <remarks>
    /// Segundo o xsd e a documentação de OTA isto tem uma colection de ResGuestRPH
    /// Isto esta definido como class para quando seja generada a mensagem de OTA não apague as mudanças</remarks>/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public class ResGuestRPHsType
    {

        private string[] textField;
        private ResGuestRPHType[] ResGuestRPHField;

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string[] Text
        {
            get
            {
                return this.textField;
            }
            set
            {
                this.textField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("ResGuestRPH", IsNullable = false)]
        public ResGuestRPHType[] ResGuestRPH
        {
            get
            {
                return this.ResGuestRPHField;
            }
            set
            {
                this.ResGuestRPHField = value;
            }
        }

    }

    #region Telephone

    [System.Xml.Serialization.XmlRootAttribute("OTA_HotelResModifyNotifRS", Namespace = "http://www.opentravel.org/OTA/2003/05", IsNullable = false)]
    public partial class HotelResModifyResponseType
    { }



    public partial class PaymentCardType
    {
        private string cardNumberField;

        /// <remarks/>
        public string CardNumber
        {
            get
            {
                return this.cardNumberField;
            }
            set
            {
                this.cardNumberField = value;
            }
        }
    }

    public partial class DateTimeSpanType
    {
        private DateTime startField;
        private DateTime endField;
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public DateTime Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public DateTime End
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }
    }



    public partial class OTA_HotelAvailGetRQHotelAvailRequestBestAvailableRateCandidate
    {
        private TPA_ExtensionsType tPA_ExtensionsField;
        /// <remarks/>
        public TPA_ExtensionsType TPA_Extensions
        {
            get
            {
                return this.tPA_ExtensionsField;
            }
            set
            {
                this.tPA_ExtensionsField = value;
            }
        }
    }

    public partial class OTA_HotelAvailGetRQHotelAvailRequest
    {

        [System.Xml.Serialization.XmlAttributeAttribute("HotelAvailRequestIdentifier")]
        public string HotelAvailRequestIdentifier { get; set; }

        [XmlIgnore]
        public bool HotelAvailRequestIdentifierSpecified { get; set; }
    }


    public partial class OTA_HotelRatePlanRQRatePlan
    {

        [System.Xml.Serialization.XmlAttributeAttribute("HotelRatePlanRequestIdentifier")]
        public string HotelRatePlanRequestIdentifier { get; set; }

        [XmlIgnore]
        public bool HotelRatePlanRequestIdentifierSpecified { get; set; }
    }

    #endregion

    #endregion

}