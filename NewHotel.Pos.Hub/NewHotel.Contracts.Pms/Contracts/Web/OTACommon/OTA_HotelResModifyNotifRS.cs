﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.5448
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Xml.Serialization;

// 
// This source code was auto-generated by xsd, Version=2.0.50727.3038.
// 

namespace NewHotel.Contracts.OTA
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class HotelResModifyResponseType
    {

        private SourceType[] pOSField;

        private object[] itemsField;

        private string echoTokenField;

        private System.DateTime timeStampField;

        private bool timeStampFieldSpecified;

        private MessageAcknowledgementTypeTarget targetField;

        private bool targetFieldSpecified;

        private string targetNameField;

        private decimal versionField;

        private string transactionIdentifierField;

        private string sequenceNmbrField;

        private MessageAcknowledgementTypeTransactionStatusCode transactionStatusCodeField;

        private bool transactionStatusCodeFieldSpecified;

        private bool retransmissionIndicatorField;

        private bool retransmissionIndicatorFieldSpecified;

        private string correlationIDField;

        private string primaryLangIDField;

        private string altLangIDField;

        private TransactionStatusType resResponseTypeField;

        private bool resResponseTypeFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Source", IsNullable = false)]
        public SourceType[] POS
        {
            get
            {
                return this.pOSField;
            }
            set
            {
                this.pOSField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Errors", typeof(ErrorsType))]
        [System.Xml.Serialization.XmlElementAttribute("HotelResModifies", typeof(HotelResModifyType))]
        [System.Xml.Serialization.XmlElementAttribute("Success", typeof(SuccessType))]
        [System.Xml.Serialization.XmlElementAttribute("Warnings", typeof(WarningsType))]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string EchoToken
        {
            get
            {
                return this.echoTokenField;
            }
            set
            {
                this.echoTokenField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime TimeStamp
        {
            get
            {
                return this.timeStampField;
            }
            set
            {
                this.timeStampField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TimeStampSpecified
        {
            get
            {
                return this.timeStampFieldSpecified;
            }
            set
            {
                this.timeStampFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public MessageAcknowledgementTypeTarget Target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TargetSpecified
        {
            get
            {
                return this.targetFieldSpecified;
            }
            set
            {
                this.targetFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TargetName
        {
            get
            {
                return this.targetNameField;
            }
            set
            {
                this.targetNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TransactionIdentifier
        {
            get
            {
                return this.transactionIdentifierField;
            }
            set
            {
                this.transactionIdentifierField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string SequenceNmbr
        {
            get
            {
                return this.sequenceNmbrField;
            }
            set
            {
                this.sequenceNmbrField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public MessageAcknowledgementTypeTransactionStatusCode TransactionStatusCode
        {
            get
            {
                return this.transactionStatusCodeField;
            }
            set
            {
                this.transactionStatusCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TransactionStatusCodeSpecified
        {
            get
            {
                return this.transactionStatusCodeFieldSpecified;
            }
            set
            {
                this.transactionStatusCodeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool RetransmissionIndicator
        {
            get
            {
                return this.retransmissionIndicatorField;
            }
            set
            {
                this.retransmissionIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RetransmissionIndicatorSpecified
        {
            get
            {
                return this.retransmissionIndicatorFieldSpecified;
            }
            set
            {
                this.retransmissionIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CorrelationID
        {
            get
            {
                return this.correlationIDField;
            }
            set
            {
                this.correlationIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "language")]
        public string PrimaryLangID
        {
            get
            {
                return this.primaryLangIDField;
            }
            set
            {
                this.primaryLangIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "language")]
        public string AltLangID
        {
            get
            {
                return this.altLangIDField;
            }
            set
            {
                this.altLangIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public TransactionStatusType ResResponseType
        {
            get
            {
                return this.resResponseTypeField;
            }
            set
            {
                this.resResponseTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ResResponseTypeSpecified
        {
            get
            {
                return this.resResponseTypeFieldSpecified;
            }
            set
            {
                this.resResponseTypeFieldSpecified = value;
            }
        }
    }


    ///// <remarks/>
    //[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    //[System.SerializableAttribute()]
    //[System.Diagnostics.DebuggerStepThroughAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    //[System.Xml.Serialization.XmlRootAttribute("OTA_HotelResModifyNotifRS", Namespace = "http://www.opentravel.org/OTA/2003/05", IsNullable = false)]
    //public partial class HotelResModifyResponseType
    //{

    //    private SourceType[] pOSField;

    //    private object[] itemsField;

    //    private string echoTokenField;

    //    private System.DateTime timeStampField;

    //    private bool timeStampFieldSpecified;

    //    private HotelResModifyResponseTypeTarget targetField;

    //    private bool targetFieldSpecified;

    //    private string targetNameField;

    //    private decimal versionField;

    //    private string transactionIdentifierField;

    //    private string sequenceNmbrField;

    //    private HotelResModifyResponseTypeTransactionStatusCode transactionStatusCodeField;

    //    private bool transactionStatusCodeFieldSpecified;

    //    private bool retransmissionIndicatorField;

    //    private bool retransmissionIndicatorFieldSpecified;

    //    private string correlationIDField;

    //    private TransactionStatusType resResponseTypeField;

    //    private bool resResponseTypeFieldSpecified;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlArrayItemAttribute("Source", IsNullable = false)]
    //    public SourceType[] POS
    //    {
    //        get
    //        {
    //            return this.pOSField;
    //        }
    //        set
    //        {
    //            this.pOSField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlElementAttribute("Errors", typeof(ErrorsType))]
    //    [System.Xml.Serialization.XmlElementAttribute("HotelResModifies", typeof(HotelResModifyType))]
    //    [System.Xml.Serialization.XmlElementAttribute("Success", typeof(SuccessType))]
    //    [System.Xml.Serialization.XmlElementAttribute("Warnings", typeof(WarningsType))]
    //    public object[] Items
    //    {
    //        get
    //        {
    //            return this.itemsField;
    //        }
    //        set
    //        {
    //            this.itemsField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string EchoToken
    //    {
    //        get
    //        {
    //            return this.echoTokenField;
    //        }
    //        set
    //        {
    //            this.echoTokenField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public System.DateTime TimeStamp
    //    {
    //        get
    //        {
    //            return this.timeStampField;
    //        }
    //        set
    //        {
    //            this.timeStampField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlIgnoreAttribute()]
    //    public bool TimeStampSpecified
    //    {
    //        get
    //        {
    //            return this.timeStampFieldSpecified;
    //        }
    //        set
    //        {
    //            this.timeStampFieldSpecified = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public HotelResModifyResponseTypeTarget Target
    //    {
    //        get
    //        {
    //            return this.targetField;
    //        }
    //        set
    //        {
    //            this.targetField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlIgnoreAttribute()]
    //    public bool TargetSpecified
    //    {
    //        get
    //        {
    //            return this.targetFieldSpecified;
    //        }
    //        set
    //        {
    //            this.targetFieldSpecified = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string TargetName
    //    {
    //        get
    //        {
    //            return this.targetNameField;
    //        }
    //        set
    //        {
    //            this.targetNameField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public decimal Version
    //    {
    //        get
    //        {
    //            return this.versionField;
    //        }
    //        set
    //        {
    //            this.versionField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string TransactionIdentifier
    //    {
    //        get
    //        {
    //            return this.transactionIdentifierField;
    //        }
    //        set
    //        {
    //            this.transactionIdentifierField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
    //    public string SequenceNmbr
    //    {
    //        get
    //        {
    //            return this.sequenceNmbrField;
    //        }
    //        set
    //        {
    //            this.sequenceNmbrField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public HotelResModifyResponseTypeTransactionStatusCode TransactionStatusCode
    //    {
    //        get
    //        {
    //            return this.transactionStatusCodeField;
    //        }
    //        set
    //        {
    //            this.transactionStatusCodeField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlIgnoreAttribute()]
    //    public bool TransactionStatusCodeSpecified
    //    {
    //        get
    //        {
    //            return this.transactionStatusCodeFieldSpecified;
    //        }
    //        set
    //        {
    //            this.transactionStatusCodeFieldSpecified = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public bool RetransmissionIndicator
    //    {
    //        get
    //        {
    //            return this.retransmissionIndicatorField;
    //        }
    //        set
    //        {
    //            this.retransmissionIndicatorField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlIgnoreAttribute()]
    //    public bool RetransmissionIndicatorSpecified
    //    {
    //        get
    //        {
    //            return this.retransmissionIndicatorFieldSpecified;
    //        }
    //        set
    //        {
    //            this.retransmissionIndicatorFieldSpecified = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string CorrelationID
    //    {
    //        get
    //        {
    //            return this.correlationIDField;
    //        }
    //        set
    //        {
    //            this.correlationIDField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public TransactionStatusType ResResponseType
    //    {
    //        get
    //        {
    //            return this.resResponseTypeField;
    //        }
    //        set
    //        {
    //            this.resResponseTypeField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlIgnoreAttribute()]
    //    public bool ResResponseTypeSpecified
    //    {
    //        get
    //        {
    //            return this.resResponseTypeFieldSpecified;
    //        }
    //        set
    //        {
    //            this.resResponseTypeFieldSpecified = value;
    //        }
    //    }
    //}
}