﻿using System;
using System.Xml.Serialization;
using NewHotel.Contracts;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    [XmlRoot("DumpExportReservation")]
    public class DumpExportReservationResponse : BaseResponse
    {
        public List<DumpReservationResponse> Reservations;
        public DateTime AccessTime { get; set; }
    }

    public class DumpReservationResponse
    {

        public Guid ReservationId { get; set; }
        public string ReservationNumber { get; set; }
        public string Guest { get; set; }
        public Guid? GuestId { get; set; }
        public string Country { get; set; }
        public DateTime Created { get; set; }
        public DateTime Arrival { get; set; }
        public DateTime Departure { get; set; }
        public int Nights { get; set; }
        public string Reserved { get; set; }
        public string Occuped { get; set; }
        public string Room { get; set; }
        public string Meal { get; set; }
        public string MealCode { get; set; }
        public int Adults { get; set; }
        public int Child { get; set; }
        public int Baby { get; set; }
        public ReservationState Status { get; set; }
        public string GroupName { get; set; }
        public string GroupNumber { get; set; }
        public string Company { get; set; }
        public Guid? CompanyId { get; set; }
        public string Agency { get; set; }
        public Guid? AgencyId { get; set; }
        public string Allotment { get; set; }
        public string Segment { get; set; }
        public string Source { get; set; }
        public string CancellationPolicy { get; set; }
        public string Guarantee { get; set; }
        public string PriceType { get; set; }
        public string PriceRate { get; set; }
        public Guid? ReferencePriceRate { get; set; }
        public decimal Price { get; set; }
        public decimal Invoiced { get; set; }
        public decimal Balance { get; set; }
        public string External { get; set; }
        public string Voucher { get; set; }
        public string Comments { get; set; }
        public string Createdby { get; set; }
        public string Confirmation { get; set; }
        public string Cancellation { get; set; }
        public DateTime? CancellationDate { get; set; }
        public string Master { get; set; }
        public string Extra1 { get; set; }
        public string Extra2 { get; set; }
        public string Extra3 { get; set; }
        public string Extra4 { get; set; }
        public string Extra5 { get; set; }
        public DateTime LastModifiedDate { get; set; }

    }
}
