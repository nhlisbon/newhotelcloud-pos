﻿using System;
using System.Xml.Serialization;
using NewHotel.Contracts;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    [XmlRoot("DumpExportStatisticsReservation")]
    public class DumpExportStatisticsReservationResponse : BaseResponse
    {
        public List<DumpStatisticsReservationItem> Items;
        public DateTime AccessTime { get; set; }
    }

    public class DumpStatisticsReservationItem
    {
        public Guid Id { get; set; }
        public Guid HotelId { get; set; }
        public DateTime Date { get; set; }
        public Guid ReservationId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }
        public Guid RoomTypeId { get; set; }
        public string Nationality { get; set; }
        public Guid? EntityId { get; set; }
        public Guid MarketSourceId { get; set; }
        public Guid MarketSegmentId { get; set; }
        public int Sleepovers { get; set; }
        public int SleepoversGroups { get; set; }
        public int SleepoversEntities { get; set; }
        public int SleepoversDirects { get; set; }
        public int SleepoversHouseUse { get; set; }
        public int DayUse { get; set; }
        public int Overbooking { get; set; }
        public int Walkin { get; set; }
        public int Adults { get; set; }
        public int Children { get; set; }
        public int Babbies { get; set; }
        public int TotalGuests { get; set; }
        public int TotalGuestsRO { get; set; }
        public int TotalGuestsBB { get; set; }
        public int TotalGuestsHB { get; set; }
        public int TotalGuestsFB { get; set; }
        public int TotalGuestsAI { get; set; }
        public decimal TotalIncomePlusTax { get; set; }
        public decimal TotalIncome { get; set; }
        public decimal TotalIncomeDailyPlusTax { get; set; }
        public decimal TotalIncomeDaily { get; set; }
        public decimal TotalIncomeUSAHRoomPlusTax { get; set; }
        public decimal TotalIncomeUSAHRoom { get; set; }
        public decimal TotalIncomeUSAHFoodPlusTax { get; set; }
        public decimal TotalIncomeUSAHFood { get; set; }
        public decimal TotalIncomeUSAHBeveragePlusTax { get; set; }
        public decimal TotalIncomeUSAHBeverage { get; set; }
        public decimal TotalIncomeUSAHComsPlusTax { get; set; }
        public decimal TotalIncomeUSAHComs { get; set; }
        public decimal TotalIncomeUSAHEventsPlusTax { get; set; }
        public decimal TotalIncomeUSAHEvents { get; set; }
        public decimal TotalIncomeUSAHHealthPlusTax { get; set; }
        public decimal TotalIncomeUSAHHealth { get; set; }
        public decimal TotalIncomeUSAHBusinessPlusTax { get; set; }
        public decimal TotalIncomeUSAHBusiness { get; set; }
        public decimal TotalIncomeUSAHOThersPlusTax { get; set; }
        public decimal TotalIncomeUSAHOThers { get; set; }
        public decimal TotalIncomeDirectPlusTax { get; set; }
        public decimal TotalIncomeDirect { get; set; }
        public decimal TotalIncomeEntitiesPlusTax { get; set; }
        public decimal TotalIncomeEntities { get; set; }
        public decimal TotalIncomeROPlusTax { get; set; }
        public decimal TotalIncomeRO { get; set; }
        public decimal TotalIncomeBBPlusTax { get; set; }
        public decimal TotalIncomeBB { get; set; }
        public decimal TotalIncomeHBPlusTax { get; set; }
        public decimal TotalIncomeHB { get; set; }
        public decimal TotalIncomeFBPlusTax { get; set; }
        public decimal TotalIncomeFB { get; set; }
        public decimal TotalIncomeAIPlusTax { get; set; }
        public decimal TotalIncomeAI { get; set; }
        public decimal TotalPaid { get; set; }
        public decimal TotalPendingAccounts { get; set; }
        public decimal TotalCommissions { get; set; }
    }

   
}
