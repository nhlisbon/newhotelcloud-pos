﻿using System;
using System.Xml.Serialization;
using NewHotel.Contracts;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    [XmlRoot("DumpExportInactivity")]
    public class DumpExportInactivityResponse : BaseResponse
    {
        public List<DumpInactivityResponse> Inactivities;
        public DateTime AccessTime { get; set; }
    }

    public class DumpInactivityResponse
    {
        public Guid Id { get; set; }
        public DateTime InitialDate { get; set; }
        public DateTime FinalDate { get; set; }
        public bool AffectBooking { get; set; }
        public string Observations { get; set; }
        public Guid RoomTypeId { get; set; }
        public string RoomTypeAbbreviation { get; set; }
        public string RoomTypeDescription { get; set; }
        public string InactiviyType { get; set; }
    }

   
}
