﻿using System;
using System.Xml.Serialization;
using NewHotel.Contracts;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    [XmlRoot("DumpExportStatisticsProfile")]
    public class DumpExportStatisticsProfileResponse : BaseResponse
    {
        public List<DumpStatisticsProfileItem> Items;
        public DateTime AccessTime { get; set; }
    }

    public class DumpStatisticsProfileItem
    {
        public Guid Id { get; set; }
        public Guid ProfileId { get; set; }
        public DateTime Date { get; set; }
        public Guid? ReservationId { get; set; }
        public int TotalArrivals { get; set; }
        public int TotalNights { get; set; }
        public int TotalCancellations { get; set; }
        public int TotalNoshows { get; set; }
        public int TotalGroupReservations { get; set; }
        public int TotalEarlyDepartures { get; set; }
        public decimal TotalIncomePlusTax { get; set; }
        public decimal TotalIncome { get; set; }
        public decimal TotalIncomeDailyPlusTax { get; set; }
        public decimal TotalIncomeDaily { get; set; }
        public decimal TotalIncomeRoomPriceRatePlusTax { get; set; }
        public decimal TotalIncomeRoomPriceRate { get; set; }
        public decimal TotalIncomeNonGuestsPlusTax { get; set; }
        public decimal TotalIncomeNonGuests { get; set; }
        public decimal TotalIncomeUSAHRoomPlusTax { get; set; }
        public decimal TotalIncomeUSAHRoom { get; set; }
        public decimal TotalIncomeUSAHFoodPlusTax { get; set; }
        public decimal TotalIncomeUSAHFood { get; set; }
        public decimal TotalIncomeUSAHBeveragePlusTax { get; set; }
        public decimal TotalIncomeUSAHBeverage { get; set; }
        public decimal TotalIncomeUSAHComsPlusTax { get; set; }
        public decimal TotalIncomeUSAHComs { get; set; }
        public decimal TotalIncomeUSAHEventsPlusTax { get; set; }
        public decimal TotalIncomeUSAHEvents { get; set; }
        public decimal TotalIncomeUSAHHealthPlusTax { get; set; }
        public decimal TotalIncomeUSAHHealth { get; set; }
        public decimal TotalIncomeUSAHBusinessPlusTax { get; set; }
        public decimal TotalIncomeUSAHBusiness { get; set; }
        public decimal TotalIncomeUSAHOThersPlusTax { get; set; }
        public decimal TotalIncomeUSAHOThers { get; set; }
    }

   
}
