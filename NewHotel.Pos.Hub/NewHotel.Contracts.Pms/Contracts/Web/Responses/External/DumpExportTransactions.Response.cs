﻿using System;
using System.Xml.Serialization;
using NewHotel.Contracts;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    [XmlRoot("DumpExportTransaction")]
    public class DumpExportTransactionResponse : BaseResponse
    {
        public List<DumpTransactionResponse> Transactions;
        public DateTime AccessTime { get; set; }
    }

    public class DumpTransactionResponse
    {
        public Guid Id { get; set; }
        public DateTime WorkDate { get; set; }
        public DateTime ValueDate { get; set; }
        public EntrieType EntrieType { get; set; }
        public Guid DepartmentId { get; set; }
        public string DepartmentDescription { get; set; }
        public Guid? ServiceId { get; set; }
        public string ServiceDescription { get; set; }
        public string ServiceGroupAbbreviation { get; set; }
        public string ServiceGroupDescription { get; set; }
        public Guid CurrentAccountId { get; set; }
        public string CurrentAccountDescription{ get; set; }
        public Guid OriginAccountId { get; set; }
        public string OriginAccountDescription { get; set; }
        public Guid? ReservationId { get; set; }
        public DailyAccountType Folder { get; set; }
        public string Currency { get; set; }
        public bool Cancelled { get; set; }
        public bool Corrected { get; set; }
        public string CancellationReason { get; set; }
        public bool Automatic { get; set; }
        public bool Daily { get; set; }
        public string PriceRateDescription { get; set; }
        public int Quantity { get; set; }
        public decimal GrossValue { get; set; }
        public decimal NetValue { get; set; }
        public Guid? ReceivableId { get; set; }
        public string ReceivableDescription { get; set; }
        public bool Invoiced { get; set; }
        public int ApplicationId { get; set; }
        public bool TaxIncluded { get; set; }
        public string RoomNumber { get; set; }
        public decimal CityTaxValue { get; set; }
        public string PaymentTransactionID { get; set; }
        public Guid? UserId { get; set; }
        public string UserDescription { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string MovimentDescription { get; set; }
        public string MovimentDocument { get; set; }
    }


}
