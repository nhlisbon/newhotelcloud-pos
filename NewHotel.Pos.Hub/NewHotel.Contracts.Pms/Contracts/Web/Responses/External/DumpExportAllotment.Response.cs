﻿using System;
using System.Xml.Serialization;
using NewHotel.Contracts;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    [XmlRoot("DumpExportAllotmentResponse")]
    public class DumpExportAllotmentResponse : BaseResponse
    {
        public List<DumpAllotmentResponse> Allotments;
        public DateTime AccessTime { get; set; }
    }

    public class DumpAllotmentResponse
    {
        public Guid AllotmentId { get; set; }
        public string Abbreviation { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public bool Guaranteed { get; set; }
        public bool AffectBooking { get; set; }
        public DateTime Date { get; set; }
        public long Contracted { get; set; }
        public long? ReleaseDates { get; set; }
        public Guid RoomTypeId { get; set; }
        public string RoomTypeDescription { get; set; }
    }

   
}
