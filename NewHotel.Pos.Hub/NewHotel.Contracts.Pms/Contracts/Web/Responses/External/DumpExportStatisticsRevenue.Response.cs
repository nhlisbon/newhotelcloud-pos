﻿using System;
using System.Xml.Serialization;
using NewHotel.Contracts;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    [XmlRoot("DumpExportStatisticsRevenue")]
    public class DumpExportStatisticsRevenueResponse : BaseResponse
    {
        public List<DumpStatisticsRevenueItem> Items;
        public DateTime AccessTime { get; set; }
    }

    public class DumpStatisticsRevenueItem
    {
        public Guid Id { get; set; }
        public Guid HotelId { get; set; }
        public DateTime Date { get; set; }
        public decimal TotalIncomePlusTax { get; set; }
        public decimal TotalIncome { get; set; }
        public decimal TotalIncomeGuestsPlusTax { get; set; }
        public decimal TotalIncomeGuests { get; set; }
        public decimal TotalIncomeGuestsGroupsPlusTax { get; set; }
        public decimal TotalIncomeGuestsGroups { get; set; }
        public decimal TotalIncomeGuestsEntitiesPlusTax { get; set; }
        public decimal TotalIncomeGuestsEntities { get; set; }
        public decimal TotalIncomeGuestsDirectPlusTax { get; set; }
        public decimal TotalIncomeGuestsDirect { get; set; }
        public decimal TotalIncomePriceratePlusTax { get; set; }
        public decimal TotalIncomePricerate { get; set; }
        public decimal TotalIncomePriceRateLodgingPlusTax { get; set; }
        public decimal TotalIncomePriceRateLodging { get; set; }
        public decimal TotalIncomeNonGuestsPlusTax { get; set; }
        public decimal TotalIncomeNonGuests { get; set; }
        public decimal TotalIncomeUSAHRoomPlusTax { get; set; }
        public decimal TotalIncomeUSAHRoom { get; set; }
        public decimal TotalIncomeUSAHFoodPlusTax { get; set; }
        public decimal TotalIncomeUSAHFood { get; set; }
        public decimal TotalIncomeUSAHBeveragePlusTax { get; set; }
        public decimal TotalIncomeUSAHBeverage { get; set; }
        public decimal TotalIncomeUSAHComsPlusTax { get; set; }
        public decimal TotalIncomeUSAHComs { get; set; }
        public decimal TotalIncomeUSAHEventsPlusTax { get; set; }
        public decimal TotalIncomeUSAHEvents { get; set; }
        public decimal TotalIncomeUSAHHealthPlusTax { get; set; }
        public decimal TotalIncomeUSAHHealth { get; set; }
        public decimal TotalIncomeUSAHBusinessPlusTax { get; set; }
        public decimal TotalIncomeUSAHBusiness { get; set; }
        public decimal TotalIncomeUSAHOThersPlusTax { get; set; }
        public decimal TotalIncomeUSAHOThers { get; set; }
        public decimal TotalPayments { get; set; }
        public decimal TotalCreditCards { get; set; }
        public decimal TotalSendToAccounting { get; set; }
        public decimal TotalPaymentsInAccounting { get; set; }
        public decimal TotalDepositReceived { get; set; }
        public decimal TotalDepositHonoured { get; set; }
    }

   
}
