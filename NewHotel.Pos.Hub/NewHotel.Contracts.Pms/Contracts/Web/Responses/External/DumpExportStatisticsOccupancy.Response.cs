﻿using System;
using System.Xml.Serialization;
using NewHotel.Contracts;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    [XmlRoot("DumpExportStatisticsOccupancy")]
    public class DumpExportStatisticsOccupancyResponse : BaseResponse
    {
        public List<DumpStatisticsOccupancyItem> Items;
        public DateTime AccessTime { get; set; }
    }

    public class DumpStatisticsOccupancyItem
    {
        public Guid Id { get; set; }
        public Guid HotelId { get; set; }
        public DateTime Date { get; set; }
        public int TotalIntentoryRooms { get; set; }
        public int TotalInactiveRooms { get; set; }
        public int TotalOutRentalRooms { get; set; }
        public int TotalAttemps { get; set; }
        public int TotalWaitingList { get; set; }
        public int TotalConfirmed { get; set; }
        public int TotalCancelled { get; set; }
        public int TotalNoShow { get; set; }
        public int TotalDayUse { get; set; }
        public int TotalWalkin { get; set; }
        public int TotalEarlyDeparture { get; set; }
        public int TotalLateDeparture { get; set; }
        public int TotalOverbooked { get; set; }
        public int TotalCreated { get; set; }
        public int TotalCheckIn { get; set; }
        public int TotalStayovers { get; set; }
        public int TotalCheckOut { get; set; }
        public int TotalIndividual { get; set; }
        public int TotalGroup { get; set; }
        public int TotalDirect { get; set; }
        public int TotalCompany { get; set; }
        public int TotalOwners { get; set; }
        public int TotalTimeShare { get; set; }
        public int TotalStandardPrice { get; set; }
        public int TotalHouseUsePrice { get; set; }
        public int TotalCumplimentaryPrice { get; set; }
        public int TotalInvitationPrice { get; set; }
        public int TotalAdults{ get; set; }
        public int TotalChildren { get; set; }
        public int TotalBabbies { get; set; }
        public int TotalFreeAdults { get; set; }
        public int TotalFreeChildren { get; set; }
        public decimal OccupancyReservationsVsInventory { get; set; }
        public decimal OccupancyReservationsVSActiveRooms { get; set; }
        public decimal OccupancyRoomsSoldVsInventory { get; set; }
        public decimal OccupancyRoomsSoldVsActiveRooms { get; set; }
    }

   
}
