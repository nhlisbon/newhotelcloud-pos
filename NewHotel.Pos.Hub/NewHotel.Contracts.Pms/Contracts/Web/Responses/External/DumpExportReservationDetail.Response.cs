﻿using System;
using System.Xml.Serialization;
using NewHotel.Contracts;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    [XmlRoot("DumpExportReservationDetail")]
    public class DumpExportReservationDetailResponse : BaseResponse
    {
        public List<DumpReservationDetailResponse> Reservations;
        public DateTime AccessTime { get; set; }
    }
    [XmlRoot("DumpReservationDetail")]
    public class DumpReservationDetailResponse
    {
        public Guid ReservationId { get; set; }
        public List<DumpReservationDailyPriceResponse> DailyPrices { get; set; }
        public List<Guid> GuestList { get; set; }
    }

    public class DumpReservationDailyPriceResponse
    {
        public DateTime ValueDate { get; set; }
        public decimal Value { get; set; }
        public Guid ServiceId { get; set; }
        public string ServiceDescription { get; set; }
        public Guid DepartmentId { get; set; }
        public string DeparmentDescription { get; set; }
        public string ServiceGroupAbbreviation { get; set; }
        public string ServiceGroupDescription { get; set; }
        public decimal CityTaxValue { get; set; }
    }
}
