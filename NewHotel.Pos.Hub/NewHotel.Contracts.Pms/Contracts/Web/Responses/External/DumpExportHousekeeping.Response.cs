﻿using System;
using System.Xml.Serialization;
using NewHotel.Contracts;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    [XmlRoot("DumpExportHousekeepingResponse")]
    public class DumpExportHousekeepingResponse : BaseResponse
    {
        public List<DumpHousekeepingResponse> Logs;
        public DateTime AccessTime { get; set; }
    }

    public class DumpHousekeepingResponse
    {
        public Guid RoomId { get; set; }
        public string RoomNumber { get; set; }
        public DateTime Date { get; set; }
        public RoomStatus OldStatus { get; set; }
        public RoomStatus NewStatus { get; set; }
        public string OldStatusDescription { get; set; }
        public string NewStatusDesccription { get; set; }
        public string UserName { get; set; }

   
    }

   
}
