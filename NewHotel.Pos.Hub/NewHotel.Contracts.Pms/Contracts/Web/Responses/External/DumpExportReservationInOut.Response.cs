﻿using System;
using System.Xml.Serialization;
using NewHotel.Contracts;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    [XmlRoot("DumpExportReservationInOut")]
    public class DumpExportReservationInOutResponse : BaseResponse
    {
        public List<DumpReservationInOutResponse> Reservations;
        public DateTime AccessTime { get; set; }
    }

    public class DumpReservationInOutResponse
    {
        public Guid Id { get; set; }
        public string ReservationNumber { get; set; }
        public DateTime Date { get; set; }
   
    }

   
}
