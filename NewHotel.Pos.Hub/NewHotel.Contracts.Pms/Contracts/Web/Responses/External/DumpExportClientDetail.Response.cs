﻿using System;
using System.Xml.Serialization;
using NewHotel.Contracts;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    [XmlRoot("DumpExportClientDetail")]
    public class DumpExportClientDetailResponse : BaseResponse
    {
        public List<DumpClientDetailResponse> Clients;
        public DateTime AccessTime { get; set; }
    }

    public class DumpClientDetailResponse
    {
        public Guid Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public string Locality { get; set; }
        public string City { get; set; }
        public string CityCode { get; set; }
        public string State { get; set; }
        public string StateCode { get; set; }
        public string FreeCode { get; set; }
        public string PostalCode { get; set; }
        public Guid? AccountId { get; set; }
        public string HomePhone { get; set; }
        public string BusinessPhone { get; set; }
        public string CellPhone { get; set; }
        public string FaxPhone { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Country { get; set; }
        public bool Inactive { get; set; }
        public string FiscalNumber { get; set; }
        public string Nationality { get; set; }
        public string Gender { get; set; }
        public string Title { get; set; }
        public string LanguageDescription { get; set; }
        public string LanguageISO { get; set; }
        public string Birthdate { get; set; }
        public string Profession { get; set; }
        public string Company { get; set; }

        public List<DumpClientDetailClientsEntitysResponse> EntitysRelated { get; set; }

    }

    public class DumpClientDetailClientsEntitysResponse {
        public Guid EntityRelatedId { get; set; }
        public String ComercialName { get; set; }
        public String EntityType { get; set; }
    }

}
