﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class ClientsResponse : BaseResponse
    {
        public List<ClientRecordResponse> Clients { get; set; }
        public DateTime AccessTime { get; set; }
    }
}