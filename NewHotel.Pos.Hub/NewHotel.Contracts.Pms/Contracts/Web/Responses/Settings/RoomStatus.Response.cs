﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class RoomStatusResponse : BaseResponse
    {
        public List<RoomStatusRecordResponse> RoomStatus { get; set; }
        public DateTime AccessTime { get; set; }
    }
}