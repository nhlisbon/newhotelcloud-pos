﻿using NewHotel.Contracts;
using NewHotel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;

namespace NewHotel.Web.Services
{
    public class RoomRecordResponse
    {
        public Guid Id { get; set; }
        public Guid RoomTypeId { get; set; }
        public string RoomTypeDesc { get; set; }
        public string RoomNumber { get; set; }
        public bool Occuppied { get; set; }
    }
}
