﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class EntitiesResponse : BaseResponse
    {
        public List<EntityRecordResponse> Entities { get; set; }
        public DateTime AccessTime { get; set; }
    }
}