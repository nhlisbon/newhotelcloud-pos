﻿using NewHotel.Contracts;
using NewHotel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;

namespace NewHotel.Web.Services
{
    public class RoomStatusRecordResponse
    {
        public Guid Id { get; set; }
        public string RoomNumber { get; set; }
        public DateTime? CheckInDate { get; set; }
        public Guid? ReservationId { get; set; }
        public string ReservationNumber { get; set; }
        public RoomStatus Status { get; set; }
        public List<GuestRecordResponse> Guests { get; set; }
    }

    public class GuestRecordResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsHolder { get; set; }
    }
}
