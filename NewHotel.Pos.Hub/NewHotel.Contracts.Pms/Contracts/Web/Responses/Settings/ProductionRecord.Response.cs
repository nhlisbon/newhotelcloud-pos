﻿using NewHotel.Contracts;
using NewHotel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;

namespace NewHotel.Web.Services
{
    public class ProductionRecordResponse
    {
        public Guid Id { get; set; }
        public Guid HotelId { get; set; }
        public string Description { get; set; }
        public int RoomNights { get; set; }
        public decimal NetValue { get; set; }
    }
}
