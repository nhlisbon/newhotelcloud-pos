﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class ProductionResponse : BaseResponse
    {
        public List<ProductionRecordResponse> Production { get; set; }
        public DateTime AccessTime { get; set; }
    }   
}