﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class RoomsResponse : BaseResponse
    {
        public List<RoomRecordResponse> Rooms { get; set; }
        public DateTime AccessTime { get; set; }
    }
}