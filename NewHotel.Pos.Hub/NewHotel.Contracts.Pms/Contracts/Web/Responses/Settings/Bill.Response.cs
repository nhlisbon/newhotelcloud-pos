﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class BillResponse : BaseResponse
    {
        public List<BillRecordResponse> Lines { get; set; }
        public DateTime AccessTime { get; set; }
    }
}