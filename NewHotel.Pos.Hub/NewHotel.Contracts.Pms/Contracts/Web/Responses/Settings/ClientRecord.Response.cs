﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class ClientRecordResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string LastName2 { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }
        public string Locality { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string FiscalNumber { get; set; }
        public DateTime? BirthDate { get; set; }
        public string HomePhone { get; set; }
        public string BusinessPhone { get; set; }
        public string CellPhone { get; set; }
        public string FaxPhone { get; set; }
        public string Email { get; set; }
        public string ClientType { get; set; }
        public bool ReceiveNotifications { get; set; }
        public string Identity { get; set; }
        public string Passport { get; set; }
        public string DriverLicense { get; set; }
        public string Residence { get; set; }
        public GenderType Sexo { get; set; }
        public CivilState CivilStatus { get; set; }
        public bool Unwanted { get; set; }
        public bool Regular { get; set; }
        public bool Owner { get; set; }
    }
}