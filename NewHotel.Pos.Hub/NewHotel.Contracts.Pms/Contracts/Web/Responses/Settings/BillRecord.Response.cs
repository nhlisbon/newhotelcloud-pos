﻿using NewHotel.Contracts;
using NewHotel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class BillRecordResponse
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public decimal Value { get; set; }
        public string Currency { get; set; }
        public EntrieType Type { get; set; }
    }
}
