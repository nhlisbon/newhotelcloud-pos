﻿namespace NewHotel.Web.Services
{
    public class KioskReservationAddress
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Location { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
    }
}