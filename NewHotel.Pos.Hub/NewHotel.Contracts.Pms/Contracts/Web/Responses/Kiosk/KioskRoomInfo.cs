﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskRoomInfo
    {
        public Guid RoomId { get; set; }
        public Guid RoomTypeId { get; set; }
        public string RoomTypeDescription { get; set; }
        public string RoomName { get; set; }
        public RoomStatus Status { get; set; }
    }
}
