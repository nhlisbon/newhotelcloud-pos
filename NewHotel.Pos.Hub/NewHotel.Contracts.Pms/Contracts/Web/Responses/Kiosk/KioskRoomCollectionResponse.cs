﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskRoomCollectionResponse : KioskBaseResponse
    {
        public List<KioskRoomInfo> Rooms { get; set; }
    }
}
