﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskReservationAccountLineInfo
    {
        public Guid Identifier { get; set; }
        public EntrieType Type { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string Department { get; set; }
        public decimal Value { get; set; }
        public short CategoryId { get; set; }
        public string CategoryDescription { get; set; }
        public bool PriceRateCharge { get; set; }
    }
}
