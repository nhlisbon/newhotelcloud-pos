﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskReservationPriceInfo
    {
        public DateTime Date { get; set; }
        public decimal Price { get; set; }
    }
}
