﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskRoomTypePriceInfo
    {
        public short? PensionMode { get; set; }
        public string PensionModeDescription { get; set; }
        public Guid? PriceRateId { get; set; }
        public string PriceRateDescription { get; set; }
        public KioskRoomTypeInfo RoomType { get; set; }
        public List<KioskReservationPriceInfo> Prices { get; set; }

        public bool HasPrices
        {
            get
            {
                if (Prices == null) return false;
                if (Prices.Count() == 0) return false;
                return true;
            }
        }
        public bool Equatable(KioskRoomTypePriceInfo priceDefinition)
        {
            if ((PriceRateId ?? Guid.Empty) != (priceDefinition.PriceRateId ?? Guid.Empty)) return false;
            if (RoomType.RoomTypeId != priceDefinition.RoomType.RoomTypeId) return false;
            if (PensionMode != priceDefinition.PensionMode) return false;

            return true;
        }

        public decimal TotalPrice
        {
            get
            {
                if (Prices == null) return 0;
                if (Prices.Count() == 0) return 0;
                return Prices.Sum(x => x.Price);
            }
        }
    }
}
