﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskReservationDetailResponse : KioskBaseResponse
    {
        public string GroupReservationName { get; set; }
        public bool IsGroup { get; set; }
        public Guid GroupReservationId { get; set; }
        public List<KioskReservationDetail> Reservations { get; set; }
    }
}
