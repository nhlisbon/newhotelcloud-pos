﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskReservationDetail
    {
        public string ReservationName { get; set; }
        public ReservationState Status { get; set; }
        public DateTime Arrival { get; set; }
        public DateTime Departure { get; set; }
        public string ExternalId { get; set; }
        public string Voucher { get; set; }
        public int Paxs { get; set; }
        public string sPaxs { get; set; }
        public bool HideRate { get; set; }
        public Guid ReservationId { get; set; }
        public KioskRoomTypeInfo ReservedRoomType { get; set; }
        public KioskRoomTypeInfo OccupedRoomType { get; set; }
        public List<KioskRoomTypePriceInfo> Prices { get; set; }
        public List<KioskReservationGuest> Guests { get; set; }
        public KioskRoomInfo Room { get; set; }
        public KioskReservationAccountInfo Account { get; set; }
        public List<KioskReservationUpsellItem> UpsellItems { get; set; }
        public List<KioskReservationUpsellItem> UsedUpsellItems { get; set; }
        public List<KioskReservationInvoice> Invoices { get; set; }
        public CheckInAuthorize CheckInAuthorization { get; set; }
        public bool NoRoomsAvailable { get; set; }
    }
}
