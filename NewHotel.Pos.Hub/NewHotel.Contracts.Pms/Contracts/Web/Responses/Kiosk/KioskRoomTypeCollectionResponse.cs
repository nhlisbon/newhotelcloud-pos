﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskRoomTypeCollectionResponse : KioskBaseResponse
    {
        public List<KioskRoomTypeInfo> RoomTypes { get; set; }
    }
}
