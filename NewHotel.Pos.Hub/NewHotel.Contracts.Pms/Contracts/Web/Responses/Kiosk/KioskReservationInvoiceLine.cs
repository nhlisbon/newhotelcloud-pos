﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskReservationInvoiceLine
    {
        public DateTime Date { get; set; }
        public DocumentDetail Type { get; set; }
        public string Description { get; set; }
        public decimal Value { get; set; }
    }
}
