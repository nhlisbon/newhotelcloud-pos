﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class KioskReservationGuest
    {
        public Guid GuestId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LastName2 { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Nationality { get; set; }
        public DateTime? Birthday { get; set; }
        public GenderType? Gender { get; set; }
        public KioskReservationAddress Address { get; set; }
        public bool Holder { get; set; }
        public string PassportNumber { get; set; }
        public string PassportCountry { get; set; }
        public DateTime? PassportIssue { get; set; }
        public DateTime? PassportExpiration { get; set; }
        public string IdentityNumber { get; set; }
        public string IdentityCountry { get; set; }
        public DateTime? IdentityIssue { get; set; }
        public DateTime? IdentityExpiration { get; set; }
        public string LicenseNumber { get; set; }
        public string LicenseCountry { get; set; }
        public DateTime? LicenseIssue { get; set; }
        public DateTime? LicenseExpiration { get; set; }
        public string FiscalNumber { get; set; }
        public string GuestDocumentImage { get; set; }
        public string GuestAge { get; set; }
        public KioskReservationGuestCompany Company { get; set; }
        public List<KioskReservationGuestConsentItem> Consents { get; set; }
    }

    public class KioskReservationGuestCompany
    {
        public Guid CompanyId { get; set; }
        public string Name { get; set; }
        public string TaxNumber { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Notes { get; set; }
        public KioskReservationAddress Address { get; set; }
    }

    public class KioskReservationGuestConsentItem
    {
        public Guid TypeID { get; set; }
        public long TypeCode { get; set; }
        public string ConsentValue { get; set; }
        public string Description { get; set; }
        public string HowCollected { get; set; }
        public string WhyCollected { get; set; }
        public string HowShared { get; set; }
        public string WhyShared { get; set; }
    }


}
