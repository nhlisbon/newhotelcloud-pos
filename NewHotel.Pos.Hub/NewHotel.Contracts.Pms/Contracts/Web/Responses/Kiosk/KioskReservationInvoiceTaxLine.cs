﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskReservationInvoiceTaxLine
    {
        public decimal TaxPercent { get; set; }
        public string Description { get; set; }
        public decimal Value { get; set; }
        public decimal Incidence { get; set; }
        public decimal BaseImp { get; set; }
    }
}
