﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskReservationAccountInfo
    {
        public List<KioskReservationAccountLineInfo> Movements { get; set; }
        public decimal TotalCredits{ get; set; }
        public decimal TotalDebits { get; set; }
        public decimal PendingValue { get; set; }
    }
}
