﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Web.Services
{
    public class KioskReservationUpsellItem
    {
        public Guid UpsellItemId { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public string Description { get; set; }
        public Guid? ServiceByDepartmentId { get; set; }
        public decimal Price { get; set; }
        public decimal? PriceChild { get; set; }
        public decimal? PriceInfant { get; set; }
        public string PriceOption { get; set; }
        public string ImageId { get; set; }
        public string Image { get; set; }
    }
}
