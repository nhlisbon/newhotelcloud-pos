﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskReservationInfoResponse : KioskBaseResponse
    {
        public List<KioskReservationInfo> Reservations { get; set; }
    }
}
