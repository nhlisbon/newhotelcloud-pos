﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskImageCollectionResponse : KioskBaseResponse
    {
        public List<KioskImageResponse> Images { get; set; }
    }
}
