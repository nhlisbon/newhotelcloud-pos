﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskReservationInvoice
    {
        public DateTime CreationDate { get; set; }
        public Guid InvoiceId { get; set; }
        public string InvoiceSerial { get; set; }
        public List<KioskReservationInvoiceLine> Lines { get; set; }
        public List<KioskReservationInvoiceTaxLine> Taxes { get; set; }
        public decimal TotalGross { get; set; }
        public decimal TotalNet { get; set; }
        public decimal CityTax { get; set; }
        public string Signature { get; set; }
        public byte[] QrCode { get; set; }
        public string FiscalizationCode { get; set; }
    }
}
