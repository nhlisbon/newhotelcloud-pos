﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskRoomTypeInfo
    {
        public Guid RoomTypeId { get; set; }
        public string RoomTypeAbreviation { get; set; }
        public string RoomTypeDescription { get; set; }
        public string RoomTypeComments { get; set; }
        public short MinPersons { get; set; }
        public short MaxPersons { get; set; }
        public string ImageId { get; set; }
        public string Image { get; set; }
    }
}
