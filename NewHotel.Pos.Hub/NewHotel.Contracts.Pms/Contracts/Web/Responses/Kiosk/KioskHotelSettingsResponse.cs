﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskHotelSettingsResponse : KioskBaseResponse
    {
        public DateTime? CheckInTime { get; set; }
        public DateTime? CheckOutTime { get; set; }
        public bool ReadyForInvoiceVersion { get; set; }
        public DateTime? WorkingDate { get; set; }
        public string InstalationName { get; set; }
        public Guid InstalationId { get; set; }
        public int DefaultMealPlan { get; set; }
    }
}
