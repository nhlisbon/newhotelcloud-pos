﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class KioskReservationInfo
    {
        public string ReservationGroupName { get; set; }
        public string ReservationName { get; set; }
        public Guid ReservationId { get; set; }
        public ReservationState Status { get; set; }
        public string HolderName { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }
        public int Paxs { get; set; }
        public string ReservedRoomType { get; set; }
    }
}
