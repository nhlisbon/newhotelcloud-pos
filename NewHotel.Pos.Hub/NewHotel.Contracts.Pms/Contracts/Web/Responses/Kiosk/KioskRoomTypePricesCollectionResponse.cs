﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskRoomTypePricesCollectionResponse : KioskBaseResponse
    {
        public List<KioskRoomTypePriceInfo> RoomTypePrices { get; set; }
    }
}
