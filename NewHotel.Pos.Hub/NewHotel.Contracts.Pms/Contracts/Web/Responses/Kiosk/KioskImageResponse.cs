﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskImageResponse : KioskBaseResponse
    {
        public string ImageId { get; set; }
        public string ImageHash { get; set; }
        public long ImageSize { get; set; }
        public string ImageData { get; set; }
    }
}
