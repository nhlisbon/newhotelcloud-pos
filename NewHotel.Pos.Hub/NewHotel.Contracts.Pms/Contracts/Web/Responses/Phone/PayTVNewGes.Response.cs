﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class PayTVNewGesResponse : BaseResponse
    {
        public PayTVNewGesItemResponse[] PayTVNewGesItems;
    }

    public class PayTVNewGesItemResponse
    {
        public string PayTVId;
        public ExtensionResponse Extension;

        /// <summary>
        /// HOTE_PK
        /// Hotel
        /// </summary>
        public string InstallationId;

        /// <summary>
        /// PKIO_COMA
        /// Command JOBTYPE_CREATE = 1, JOBTYPE_DUPLICATE = 2, JOBTYPE_CANCEL = 3
        /// </summary>
        public int CommandId;

        /// <summary>
        /// PKIO_DATR
        /// Fecha de emisión del pedido
        /// </summary>
        public DateTime IssueDate;

        /// <summary>
        /// PKIO_TIME
        /// Hora de emisión del pedido
        /// </summary>
        public DateTime IssueTime;

        /// <summary>
        /// PKIO_NUTE
        /// Numero de intentos
        /// </summary>
        public short Attempts;

        /// <summary>
        /// UTIL_PK
        /// Usuario que emite el pedido
        /// </summary>
        public string UserId;


        /// <summary>
        /// LIRE_PK
        /// Reserva, ref. tnht_lire(lire_pk)
        /// </summary>
        public string ReservationId;

        /// <summary>
        /// LIRE_INFO
        /// N¿/Serie Reserva para envio
        /// </summary>
        public string SerieReservation;

        /// <summary>
        /// LIRE_DAEN
        /// Fecha entrada reserva
        /// </summary>
        public DateTime? ArrivalDate;

        /// <summary>
        /// LIRE_DAEN
        /// Fecha entrada reserva
        /// </summary>
        public DateTime? DepartureDate;
        /// <summary>
        /// LIRE_NAME
        /// Nombre titular reserva
        /// </summary>
        public string HolderName;
        /// <summary>
        /// LIRE_APEL
        /// Apellido titular reserva
        /// </summary>
        public string HolderApe;

        /// <summary>
        /// LIRE_LICL
        /// Idioma reserva, ref. tnht_licl(licl_pk)
        /// </summary>
        public long Language;

        /// <summary>
        /// ALOJ_PK
        /// habitacion reserva, ref. tnht_aloj(aloj_pk)
        /// </summary>
        public string RoomId;

        /// <summary>
        /// PKIO_ATTE
        /// flag: pedido atendido (1. si, 0. no)
        /// </summary>
        public bool AttemtpTreated;

        /// <summary>
        /// ATTE_DATR
        /// Fecha en que fue atendido el pedido
        /// </summary>
        public DateTime? DateAttemtpTreated;

        /// <summary>
        /// ATTE_TIME
        /// Hora en que fue atendido el pedido
        /// </summary>
        public DateTime? TimeAttemtpTreated;

        /// <summary>
        /// OLD_ALOJ
        /// habitacion reserva, ref. tnht_aloj(aloj_pk)
        /// </summary>
        public string OldRoomId;


    }

    public class ExtensionResponse
    {
        /// <summary>
        /// EXTE_PK
        /// Extension, ref. tnht_exte(exte_pk)
        /// </summary>
        public string ExtensionId;
        /// <summary>
        /// EXTE_DESC
        /// Prefijo extensión utilizado como descripción
        /// </summary>
        public string Description;

        /// <summary>
        /// EXTE_TIPO
        /// Tipo Extension (Telefono, PayTv, Internet, etc), ref. tnht_enum(enum_pk)
        /// </summary>
        public ExtensionType Type;
        /// <summary>
        /// ALOJ_PK
        /// Código Habitacion (Recurso) asociado a la extension
        /// </summary>
        public string Room;

        /// <summary>
        /// EXTE_GRUP
        /// Código alfanumerico que representa un grupo de extensiones, ejs: Administración
        /// </summary>
        public string Group;

        /// <summary>
        /// CCCO_PK
        /// Cuenta corriente asociada a la extension
        /// ref. tnht_ccco(ccco_pk)
        /// </summary>
        public string CurrentAccountId;

        /// <summary>
        /// EXTE_PREC
        /// Tipo tarifa asociada (Habitacion, Interna, Extra)
        /// </summary>
        public PhonePrice? ExtensionPrice;
    }
}