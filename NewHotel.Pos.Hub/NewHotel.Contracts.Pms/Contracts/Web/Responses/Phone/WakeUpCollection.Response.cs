﻿using System;

namespace NewHotel.Web.Services
{
    public class WakeUpCollectionResponse : BaseResponse
    {
        #region Members

        public WakeUpResponse[] WakeUps;

        #endregion
    }
}