﻿using System;

namespace NewHotel.Web.Services
{
    public class JobCollectionResponse : BaseResponse
    {
        #region Constants

        public const int JOBTYPE_OPENLINE = 1;
        public const int JOBTYPE_CLOSELINE = 2;
        public const int JOBTYPE_OPENLINEMANUAL = 3;
        public const int JOBTYPE_CLOSELINEMANUAL = 4;
        public const int JOBTYPE_MODIFYDATA = 5;
        public const int JOBTYPE_ROOMCHANGE = 6;
        public const int JOBTYPE_TOGGLEDISTURBMODE = 7;
        public const int JOBTYPE_WAKEUP = 8;
        public const int JOBTYPE_CREATEKEY = 10;
        public const int JOBTYPE_DUPLICATEKEY = 11;
        public const int JOBTYPE_CANCELKEY = 12;
        public const int JOBTYPE_OPENPAYTV = 31;
        public const int JOBTYPE_CLOSEPAYTV = 32;
        public const int JOBTYPE_OPENPAYTVMANUAL = 33;
        public const int JOBTYPE_CLOSEPAYTVMANUAL = 34;

        #endregion
        #region Members

        public JobResponse[] Jobs;

        #endregion
    }
}