﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class ExtensionCollectionResponse : BaseResponse
    {
        public ExtensionItemResponse[] ExtensionItems;
    }

    public class ExtensionItemResponse
    {
        public ExtensionResponse Extension;
        public ReservationDetailsResponse ReservationDetails;
    }

    public class ReservationDetailsResponse
    {
        /// <summary>
        /// LIRE_PK
        /// Reserva, ref. tnht_lire(lire_pk)
        /// </summary>
        public string ReservationId;
        /// <summary>
        /// LIRE_INFO
        /// N¿/Serie Reserva para envio
        /// </summary>
        public string SerieReservation;
        /// <summary>
        /// LIRE_DAEN
        /// Fecha entrada reserva
        /// </summary>
        public DateTime ArrivalDate;
        /// <summary>
        /// LIRE_DAEN
        /// Fecha entrada reserva
        /// </summary>
        public DateTime DepartureDate;
        /// <summary>
        /// LIRE_NAME
        /// Nombre titular reserva
        /// </summary>
        public string HolderName;
        /// <summary>
        /// LIRE_APEL
        /// Apellido titular reserva
        /// </summary>
        public string HolderApe;
        /// <summary>
        /// ALOJ_PK
        /// habitacion reserva, ref. tnht_aloj(aloj_pk)
        /// </summary>
        public string RoomId;
    }

    public class ExtensionResponse
    {
        /// <summary>
        /// EXTE_PK
        /// Extension, ref. tnht_exte(exte_pk)
        /// </summary>
        public string ExtensionId;
        /// <summary>
        /// EXTE_DESC
        /// Prefijo extensión utilizado como descripción
        /// </summary>
        public string Description;

        /// <summary>
        /// EXTE_TIPO
        /// Tipo Extension (Telefono, PayTv, Internet, etc), ref. tnht_enum(enum_pk)
        /// </summary>
        public ExtensionType Type;
        /// <summary>
        /// ALOJ_PK
        /// Código Habitacion (Recurso) asociado a la extension
        /// </summary>
        public string Room;

        /// <summary>
        /// EXTE_GRUP
        /// Código alfanumerico que representa un grupo de extensiones, ejs: Administración
        /// </summary>
        public string Group;

        /// <summary>
        /// CCCO_PK
        /// Cuenta corriente asociada a la extension
        /// ref. tnht_ccco(ccco_pk)
        /// </summary>
        public string CurrentAccountId;

        /// <summary>
        /// EXTE_PREC
        /// Tipo tarifa asociada (Habitacion, Interna, Extra)
        /// </summary>
        public PhonePrice? ExtensionPrice;
    }
}