﻿using System;
using NewHotel.Contracts;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class ExtensionSyncResponse : BaseResponse
    {
        public ExtensionSyncItemResponse[] ExtensionItems;
    }

    public class ExtensionSyncItemResponse
    {
        public ExtensionResponse Extension;
        public ReservationDetailsResponse ReservationDetails;
        public List<ReservationDetailsResponse> OtherGuest;
    }

    public class ReservationDetailsResponse
    {
        /// <summary>
        /// LIRE_PK
        /// Reserva, ref. tnht_lire(lire_pk)
        /// </summary>
        public string ReservationId;
        /// <summary>
        /// LIRE_INFO
        /// N¿/Serie Reserva para envio
        /// </summary>
        public string SerieReservation;
        /// <summary>
        /// LIRE_DAEN
        /// Fecha entrada reserva
        /// </summary>
        public DateTime ArrivalDate;
        /// <summary>
        /// LIRE_DAEN
        /// Fecha entrada reserva
        /// </summary>
        public DateTime DepartureDate;
        /// <summary>
        /// FirstName Holder reserva
        /// </summary>
        public string HolderFirstName;
        /// <summary>
        /// Last name Holder reserva
        /// </summary>
        public string HolderLastName;
        /// <summary>
        /// lire_coex
        /// </summary>
        public string HolderExternalCode;
        /// <summary>
        /// MiddleName Holder reserva
        /// </summary>
        public string HolderMiddleName;
        /// <summary>
        /// Holder Name
        /// </summary>
        public string Holder;
        /// <summary>
        /// Title Id
        /// </summary>
        public string TitleId;
        /// <summary>
        /// Title
        /// </summary>
        public string Title;
        /// <summary>
        /// ALOJ_PK
        /// habitacion reserva, ref. tnht_aloj(aloj_pk)
        /// </summary>
        public string RoomId;

        /// <summary>
        /// ALOJ
        /// habitacion reserva, ref. tnht_aloj(aloj_pk)
        /// </summary>
        public string Room;
        /// <summary>
        /// Idioma reserva, ref. tnht_licl(licl_pk)
        /// </summary>
        public string Language;
    }

    public class ExtensionResponse
    {
        /// <summary>
        /// Extension, ref. tnht_exte(exte_pk)
        /// </summary>
        public string ExtensionId;
        /// <summary>
        /// Prefijo extensión utilizado como descripción
        /// </summary>
        public string Description;

        /// <summary>
        /// Tipo Extensión (Phone = 1831, PayTv = 1832, Keys = 1833, Internet = 1834, Gates = 1835)
        /// </summary>
        public ExtensionType Type;
        /// <summary>
        /// Código Habitacion (Recurso) asociado a la extension
        /// </summary>
        public string Room;

        /// <summary>
        /// Código alfanumerico que representa un grupo de extensiones, ejs: Administración
        /// </summary>
        public string Group;

        /// <summary>
        /// Cuenta corriente asociada a la extension
        /// ref. tnht_ccco(ccco_pk)
        /// </summary>
        public string CurrentAccountId;

        /// <summary>
        /// Tipo tarifa asociada (Habitacion, Interna, Extra)
        /// </summary>
        public PhonePrice? ExtensionPrice;
    }
}