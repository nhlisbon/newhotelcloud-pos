﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Web.Services;

namespace NewHotel.Contracts
{
    public class MovementCollectionResponse : BaseResponse
    {
        public MovementResponse[] Movements;
    }
}
