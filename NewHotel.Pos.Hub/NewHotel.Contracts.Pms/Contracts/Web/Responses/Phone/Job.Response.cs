﻿using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class JobResponse
    {
        #region Members

        public string JobID;
        public DateTime DateTime;
        public int JobType;
        public bool Done;
        public string Extension;

        public string Reservation;
        public DateTime? ReservationArrivalDate;
        public DateTime? ReservationDepartureDate;
        public string ReservationLanguage;
        public string HolderFirstName;
        public string HolderLastName;
        public string HolderExternalCode;

        public string OldRoom;
        public string NewRoom;

        public string Room;
        public int KeyAmount;
        public string KeyPermissions;

        public string Workstation;

        public TransferCCResponse TransferCCInfoDetails;
        public List<ReservationDetailsResponse> OtherGuest;
        #endregion 
    }
}