﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{

    public class TransferCCResponse : BaseResponse
    {
        public string OperatorUser;
        public string TicketId;
        public long OperationType;
        public decimal TransferCCValue;
        public DevolutionDetailsResponse DevolutionDetails;
        public ReplieDetailsResponse ReplieDetails;
    }

    public class DevolutionDetailsResponse
    {
        public string NumberTransationCO;
        public string NumberTransationBCO;
        public string AutorizationCode;
        public DateTime IssueDate;
    }

    public class ReplieDetailsResponse
    {
        public string CodeResult;
        public string DescriptionReceived;
        public string Terminal;
        public string ApplicationChip;
        public string ApplicationChipDesc;
        public string CardNumber;
        public string TradeNumber;
        public string CardMark;
        public string HolderCard;
        public string BankName;
        public string ARCDesc;
        public bool SignatureRequired;
        public string NumberRefTP;
    }
}