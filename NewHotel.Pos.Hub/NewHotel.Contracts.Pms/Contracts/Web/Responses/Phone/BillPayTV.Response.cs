﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{

    public class BillPayTVResponse : BaseResponse
    {
        public DateTime ArrivalDate;
        public DateTime DepartureDate;
        public string SerieReservation;
        public string ReservationHolder;
        public MovementsDetailsResponse[] Movements;
    }

    public class MovementsDetailsResponse
    {
        public string Id;
        public DateTime IssueDate;
        public string DepartamentCode;
        public string DepartamentDesc;
        public string ServiceCode;
        public string ServiceDesc;
        public string Description;
        public string DocNumber;
        public decimal Value;
        public bool InvoicedMovement;
        public EntrieType Type;

    }

}