﻿using NewHotel.Contracts;
using System;

namespace NewHotel.Web.Services
{
    public class MessageNewGesResponse
    {
        #region Members

        public string JobID;
        public DateTime DateTime;
        public int JobType;
        public bool Done;
        public string Extension;

        #endregion

        public string Reservation;
        public DateTime? ReservationArrivalDate;
        public DateTime? ReservationDepartureDate;
        public string ReservationLanguage;
        public string HolderFirstName;
        public string HolderLastName;
        public string MessageCode;
        public string MessageDecription;
        public string Room;
        public bool CallBackRequest;
        public bool CallRequest;
        public bool MessageReceived;
        public string User;
        public long DeviceType;
        public long MessageType;
    }

    public class MessageNewGesCollectionResponse : BaseResponse
    {
        #region Members

        public MessageNewGesResponse[] MessageColletion;

        #endregion
    }
}