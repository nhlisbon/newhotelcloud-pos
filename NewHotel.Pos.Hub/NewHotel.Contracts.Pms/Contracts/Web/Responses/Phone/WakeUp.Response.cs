﻿using NewHotel.Contracts;
using System;

namespace NewHotel.Web.Services
{
    public class WakeUpResponse : BaseResponse
    {
        public string Id { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        public WakeUpCallTypeCall ExtType { get; set; }
        public WakeUpCallState State { get; set; }
        public string Extension { get; set; }

        //NewHotel Prime Compatibility Properties
        public DateTime RegDate { get; set; }
        public DateTime RegTime { get; set; }
    }
}