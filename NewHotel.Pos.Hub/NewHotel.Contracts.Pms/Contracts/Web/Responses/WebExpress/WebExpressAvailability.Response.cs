﻿using NewHotel.Contracts;
using System;

namespace NewHotel.Web.Services
{
    public class WebExpressAvailabilityResponse : BaseResponse
    {
        public WebExpressAvailabilityContract Contract { get; set; }
        
    }
}