﻿using NewHotel.Contracts;
using System;

namespace NewHotel.Web.Services
{
    public class WebExpressDetailedResponse : BaseResponse
    {
        public WebExpressDetailInfoContract Contract { get; set; }
    }
}