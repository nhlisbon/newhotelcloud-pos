﻿using System;

namespace NewHotel.Web.Services
{
    public class CancelEntrieResponse : BaseResponse
    {
        public string[] Entries;
    }
}

