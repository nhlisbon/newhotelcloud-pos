﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
	/// <summary>
	/// List of GuestInfoResponse
	/// </summary>
    public class GuestInfoListResponse : BaseResponse
    {
        public GuestInfoListResponse()
        {
            Guests = new GuestInfoResponse[] { };
        }
		/// <summary>
		/// List of Guests
		/// </summary>
        public GuestInfoResponse[] Guests { get; set; }
    }

	/// <summary>
	/// Guest Information
	/// </summary>
    public class GuestInfoResponse
    {
		/// <summary>
		/// Identifier
		/// </summary>
        public Guid Id { get; set; }
		/// <summary>
		/// Full Name
		/// </summary>
        public string Name { get; set; }
		/// <summary>
		/// BirthDate
		/// </summary>
        public DateTime? BirthDate { get; set; }
		/// <summary>
		/// Document Type
		/// </summary>
        public PersonalDocType? DocType { get; set; }
		/// <summary>
		/// Document Number
		/// </summary>
        public string DocNumber { get; set; }
		/// <summary>
		/// Pension Mode (Room Only, Bed & Breakfast, Half Pension, Full Pension, All Inclusive)
		/// </summary>
        public string Board { get; set; }
		/// <summary>
		/// Gender (Male, Female)
		/// </summary>
        public GenderType Sex { get; set; }
		/// <summary>
		/// Nactionality (ISO Code)
		/// </summary>
        public string Nationality { get; set; }
		/// <summary>
		/// Country (ISO Code)
		/// </summary>
        public string Country { get; set; }
		/// <summary>
		/// Zip Code
		/// </summary>
        public string ZipCode { get; set; }
		/// <summary>
		/// Email Address
		/// </summary>
        public string Email { get; set; }
		/// <summary>
		/// Preferred Language
		/// </summary>
        public string Language { get; set; }
    }
}
