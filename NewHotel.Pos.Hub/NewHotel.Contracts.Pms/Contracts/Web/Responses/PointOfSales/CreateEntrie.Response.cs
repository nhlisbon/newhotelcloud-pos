﻿using System;

namespace NewHotel.Web.Services
{
    public enum EntrieID { Entries = 0 }

    public class CreateEntrieResponse : BaseResponse
    {
        public QueryDataReponse<EntrieID> Entries;
    }
}
