﻿using System;

namespace NewHotel.Web.Services
{
    public enum NonReservationID
    {
        Accounts = 0,
        AccountFolders = 1
    };

    public class NonReservationAccountResponse : QueryDataReponse<NonReservationID>
    {
        public NonReservationAccountResponse()
        {
            ID = NonReservationID.Accounts;
        }
    }
}
