﻿using System;

namespace NewHotel.Web.Services
{
    public enum ReservationID
    {
        Reservations = 0, AccountFolders = 1, Guests = 2
    };

    public class ReservationResponse : QueryDataReponse<ReservationID>
    {
        public ReservationResponse()
        {
            ID = ReservationID.Reservations;
        }
    }
}
