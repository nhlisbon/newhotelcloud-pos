﻿using System;
using System.Xml.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    [XmlRoot("ReservationExport")]
    public class ExportReservationResponse : BaseResponse
    {
        [XmlArray("Reservations")]
        [XmlArrayItem("Reservation")]
        public OccupationResponse[] Reservations;
    }

    public class OccupationGuestResponse
    {
        public string Id;
        public string Title;
        public string FullName;
        public bool IsHolder;
        public string Address;
        public string Email;
        public string FixedPhone;
        public string MobilePhone;
        public string Skype;
        public string Nacionality;
        public GenderType Gender;
    }

    public class OccupationResponse
    {
        public string Id;
        public string Number;
        [XmlElement(DataType = "date")]
        public DateTime Creation;
        [XmlElement(DataType = "date")]
        public DateTime Arrival;
        [XmlElement(DataType = "date")]
        public DateTime Departure;
        public string Room;
        public string RoomBlock;
        public string RoomTypology;
        public ReservationState Status;
        public string KeyCode;
        public string Origin;
        public string Paxs;
        public string Comments;
        public decimal TotalPrice;
        public string Currency;
        public string Segment;
        public string GroupName;
        public string Company;
        public string Agency;
        public string RateCode;
        public string MealPlan;

        [XmlArray("Guests")]
        [XmlArrayItem("Guest")]
        public OccupationGuestResponse[] Guests;
    }
}
