﻿
using NewHotel.Contracts;
using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class ProductCollectionResponse : BaseResponse
    {
        public List<ProductResponse> Products { get; set; }
    }
}