﻿
using NewHotel.Contracts;
using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class SaleCollectionResponse : BaseResponse
    {
        public List<SaleResponse> Sales { get; set; }
    }
}