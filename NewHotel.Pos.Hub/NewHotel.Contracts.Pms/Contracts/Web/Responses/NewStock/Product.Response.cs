﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class ProductResponse : BaseResponse
    {
        public string ProductID { get; set; }
        public string GroupID { get; set; }
        public string FamilyID { get; set; }
        public string SubfamilyID { get; set; }
        public string Abbrevation { get; set; }
        public string Description { get; set; }
        public string AuxiliarCode { get; set; }
        public bool Disable { get; set; }
        public bool IsTable { get; set; }
    }
}