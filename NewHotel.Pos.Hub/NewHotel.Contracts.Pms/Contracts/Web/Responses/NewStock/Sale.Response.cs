﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class SaleResponse : BaseResponse
    {
        public string SaleID { get; set; }
        public string ProductID { get; set; }
        /// <summary>
        /// Close Date
        /// </summary>
        public DateTime Date { get; set; }
        public string PointOfSalesID { get; set; }
        public short Turn { get; set; }
        public decimal Quantity { get; set; }
        public decimal GrossValue { get; set; }
        public decimal NetValue { get; set; }
        public string CurrencyID { get; set; }
        public bool IsBaseCurrency { get; set; }
        public string TicketSerial { get; set; }
        public string TicketNumber { get; set; }
        public string HotelID { get; set; }
    }
}