﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class CancelledItemCollectionResponse : BaseResponse
    {
        public List<CancelledItemResponse> CancelledItems { get; set; }
    }
}