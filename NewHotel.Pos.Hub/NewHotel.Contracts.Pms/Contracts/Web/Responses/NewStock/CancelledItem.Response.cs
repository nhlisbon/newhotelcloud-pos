﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class CancelledItemResponse : BaseResponse
    {
        public string SalesID { get; set; }
        public DateTime Date { get; set; }
        public short Turn { get; set; }
        public string HotelID { get; set; }
        public string TicketSerial { get; set; }
        public string TicketNumber { get; set; }
    }
}