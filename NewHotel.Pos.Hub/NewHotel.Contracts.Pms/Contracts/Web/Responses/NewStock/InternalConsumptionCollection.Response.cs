﻿
using NewHotel.Contracts;
using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class InternalConsumptionCollectionResponse : BaseResponse
    {
        public List<InternalConsumptionResponse> InternalConsumptions { get; set; }
    }
}