﻿using System;

namespace NewHotel.Web.Services
{
    public enum NomenclatureID
    {
        Hotels = 0, RoomTypes = 1, MarketOrigins = 2, MarketSegments = 3, TaxSchemas = 4, GuestTypes = 5,
        PensionBoards = 6, Entities = 7, Countries = 8, Departments = 9, Services = 10, ServicesByDepartment = 11, 
        Currencies = 12, WithdrawTypes = 13, CreditCardTypes = 14,
        ResourceCharacteristicTypes = 15, Contracts = 16, WarrantyTypes = 17, Titles = 18, MaleTitles = 19, 
        FemaleTitles = 20, PriceRate = 21, ClientTypes =  22, Users =  23,
        CancellationReasons = 24, ExternalAccounts = 25, Allotments = 26, Company = 27, Distributor = 28,
        PointOfSales = 30, Groups = 31, Families = 32, SubFamilies = 33, InternalConsumptions = 34, 
        PointOfSalesProducts = 35, AttentionTypes = 36, ServiceSupplements = 37, Agencies = 38, EntityTypes = 39,
        Suppliers = 40, MovementTypes = 41, FinantialCategories = 42, CostCenter = 43, AccountGroups = 44,
        Regions = 45, Banks = 46
    };

    public class NomenclatureResponse : BaseResponse
    {
        public DateTime WorkDate;
        public KeyValueIDCollectionResponse<NomenclatureID>[] Nomenclatures;
    }
}