﻿using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class AvailablePaymentCard
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public class AvailableInvoicePaymentCardsResponse : BaseResponse
    {
        public List<AvailablePaymentCard> PaymentCards;

        public AvailableInvoicePaymentCardsResponse()
        {
            PaymentCards = new List<AvailablePaymentCard>();
        }
    }
}