﻿using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class ServiceAndDepartmentResponse : BaseResponse
    {
        public ServiceAndDepartmentItem[] Items { get; set; }
    }

    public class ServiceAndDepartmentItem
    {
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        public string ServiceCode { get; set; }
        public string ServiceName { get; set; }
        public decimal TotalNet { get; set; }
        public decimal TotalGross { get; set; }
    }
}