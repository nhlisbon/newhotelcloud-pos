﻿using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class AvailableCtrlAccount
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public class AvailableInvoiceCtrlAccountsResponse : BaseResponse
    {
        public List<AvailableCtrlAccount> CtrlAccounts;

        public AvailableInvoiceCtrlAccountsResponse()
        {
            CtrlAccounts = new List<AvailableCtrlAccount>();
        }
    }
}