﻿using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class InvoicePendingItem
    {
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
    }

    public class ReservationInvoicePendingsResponse : BaseResponse
    {
        public decimal PendingInvoiceValue { get; set; }
        public List<InvoicePendingItem> Items { get; set; }

        public ReservationInvoicePendingsResponse()
        {
            Items = new List<InvoicePendingItem>();
        }
    }
}