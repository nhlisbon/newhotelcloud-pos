﻿using System;

namespace NewHotel.Web.Services
{
    public class InvoiceResponse : BaseResponse
    {
        public string InvoiceSerie { get; set; }
        public long InvoiceNumber { get; set; }
        public string InvoiceUrl { get; set; }
    }
}