﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class CashReportResponse : BaseResponse
    {
        public CashReportResponseItem[] Items { get; set; }
    }
    
    public class CashReportResponseItem
    {
        public DateTime CurrentDate { get; set; }
        public DateTime Time { get; set; }
        public short WorkShift { get; set; }
        public long ApplicationId { get; set; }
        public string Description { get; set; }
        public string TransactionDescription { get; set; }
        public string TransactionDocNumber { get; set; }
        public bool AdvancedCharge { get; set; } = false;
        public string CreditCardType { get; set; }
        public string Account { get; set; }
        public string MainGuest { get; set; }
        public string Room { get; set; }
        public string CurrencyCode { get; set; }
        public decimal Total { get; set; }
        public string CurrencyForeignCode { get; set; }
        public decimal? TotalForeign { get; set; }
        public string ReceiptNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public string UserLogin { get; set; }
    }
}