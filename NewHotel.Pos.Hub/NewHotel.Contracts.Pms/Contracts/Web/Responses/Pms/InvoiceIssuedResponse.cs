﻿using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class InvoiceIssuedResponse : BaseResponse
    {
        public InvoiceIssuedHeader[] Documents { get; set; }
    }
    
    public class InvoiceIssuedHeader
    {
        public string DocumentId { get; set; }
        public string DocumentType { get; set; }
        public int DocumentNo { get; set; }
        public string SerieId { get; set; }
        public string SerieDescription { get; set; }
        public DateTime IssueDate { get; set; }
        public string IssueTime { get; set; }
        public string ClientName { get; set; }
        public string ClientAddress { get; set; }
        public string ClientFiscalNo { get; set; }
        public decimal? NetTotal { get; set; }
        public decimal? TaxTotal { get; set; }
        public decimal? GrossTotal { get; set; }
        public string UserIssued { get; set; }
        public bool Cancelled { get; set; }
        public string UserCancelled { get; set; }

        public List<InvoiceIssuedLine> Lines { get; set; } = new List<InvoiceIssuedLine>();
        public List<InvoicePaymentLine> Payments { get; set; } = new List<InvoicePaymentLine>();
    }

    public class InvoiceIssuedLine
    {
        public int LineNumber { get; set; }
        public DateTime? LineDate { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string IncomeGroup { get; set; }

        public int? Quantity { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? NetTotal { get; set; }
        public decimal? TaxTotal { get; set; }
        public decimal? GrossTotal { get; set; }

        public decimal? TaxPercent { get; set; }
        public string TaxName { get; set; }
    }

    public class InvoicePaymentLine
    {
        public int LineNumber { get; set; }
        public DateTime? LineDate { get; set; }
        public string Description { get; set; }
        public decimal? Total { get; set; }
    }
}