﻿using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class AvailablePaymentType
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public bool Cash { get; set; }
        public bool CreditCard { get; set; }
        public bool DebitCard { get; set; }
    }

    public class AvailableInvoicePaymentTypesResponse : BaseResponse
    {
        public List<AvailablePaymentType> PaymentTypes;

        public AvailableInvoicePaymentTypesResponse()
        {
            PaymentTypes = new List<AvailablePaymentType>();
        }
    }
}