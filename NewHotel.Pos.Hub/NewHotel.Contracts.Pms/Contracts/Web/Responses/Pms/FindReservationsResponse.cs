﻿using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class ReservationIdent
    {
        public string Serie { get; set; }
        public long Number { get; set; }
    }

    public class FindReservationsResponse : BaseResponse
    {
        public List<ReservationIdent> Reservations { get; set; }
    }
}