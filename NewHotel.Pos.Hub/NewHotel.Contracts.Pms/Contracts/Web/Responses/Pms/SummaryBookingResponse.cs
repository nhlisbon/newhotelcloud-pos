﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class SummaryBookingResponse : BaseResponse
    {
        public DateTime CurrentDate { get; set; }
        public int RoomTotal { get; set; }
        public int RoomOutOfOrder { get; set; }
        public int BookingCancelled { get; set; }
        public int BookingNoShow { get; set; }
        public int BookingDayUse { get; set; }
        public int BookingNews { get; set; }
        public int BookingCheckIn { get; set; }
        public int BookingStays { get; set; }
        public int BookingCheckOut { get; set; }
        public decimal OccupancyStayBookingInventory { get; set; }
        public decimal OccupancyStayBookingActive { get; set; }
        public decimal OccupancySoldBookingInventory { get; set; }
        public decimal OccupancySoldBookingActive { get; set; }
    }

    public class SummaryBookingItem
    {

    }
}
