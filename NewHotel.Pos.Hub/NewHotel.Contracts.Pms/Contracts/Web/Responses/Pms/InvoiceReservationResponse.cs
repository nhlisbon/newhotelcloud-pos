﻿using System;

namespace NewHotel.Web.Services
{
    public class InvoiceReservationResponse : BaseResponse
    {
        public string Serie { get; set; }
        public long Number { get; set; }
        public string Url { get; set; }
    }
}