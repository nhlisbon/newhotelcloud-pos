﻿using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class AvailableService
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public class AvailableInvoiceServicesResponse : BaseResponse
    {
        public List<AvailableService> Services;

        public AvailableInvoiceServicesResponse()
        {
            Services = new List<AvailableService>();
        }
    }
}