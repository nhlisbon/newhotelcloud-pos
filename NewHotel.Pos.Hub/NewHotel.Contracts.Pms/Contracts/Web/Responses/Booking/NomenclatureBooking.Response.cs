﻿using System;

namespace NewHotel.Web.Services
{
    public class NomenclatureBookingResponse
    {
        public int ID;
        public NomenclatureBookingCollectionResponse[] KeyValues;
    }
}
