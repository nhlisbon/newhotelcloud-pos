﻿using System;

namespace NewHotel.Web.Services
{
    public class NomenclatureBookingCollectionDetailsResponse
    {
        public string ID;
        public string Value;
        public string IDDetail;
        public string ValueDetail;
    }
}
