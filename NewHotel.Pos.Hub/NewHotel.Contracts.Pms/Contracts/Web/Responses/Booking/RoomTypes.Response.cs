﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace NewHotel.Web.Services
{
    public class RoomTypesBookingInfo : KioskRoomTypeInfo
    {
        public short BasePersons { get; set; }
        public short ValidatePersons { get; set; }
        public bool Active { get; set; }
        public string Key { get; set; }
    }

    public class RoomTypesBookingResponse : BaseResponse
    {
        public List<RoomTypesBookingInfo> Rooms { get; set; }
    }

}
