﻿using NewHotel.Contracts;
using System;

namespace NewHotel.Web.Services
{
    public class BookingOneCloudPriceResponse : BaseResponse
    {
        public WebExpressDetailInfoContract Contract;
    }
}