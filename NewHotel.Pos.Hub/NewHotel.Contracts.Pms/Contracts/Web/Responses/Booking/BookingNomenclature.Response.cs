﻿using System;

namespace NewHotel.Web.Services
{
    public class BookingExternalNomenclatureResponse : BaseResponse
    {
        public DateTime WorkDate;
        public NomenclatureBookingResponse[] Nomenclatures;
    }
}