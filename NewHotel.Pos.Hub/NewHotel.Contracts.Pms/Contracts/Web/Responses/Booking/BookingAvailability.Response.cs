﻿using System;

namespace NewHotel.Web.Services
{
    public class BookingAvailabilityResponse : BaseResponse
    {
        public bool IsAvailable;
        public string RoomTypeDescription;
        public AvailabilityByDateResponse[] AvailabilityByDate;
        public RoomTypeAvailabilityResponse[] OtherRoomTypesAvailability;
    }
}