﻿using System;

namespace NewHotel.Web.Services
{
    public class NomenclatureBookingCollectionResponse
    {
        public KeyValueResponse Nomenclator;
        public NomenclatureBookingCollectionDetailsResponse[] KeyValuesDetails;
    }
}
