﻿using System;

namespace NewHotel.Web.Services
{
    public class RoomTypeAvailabilityResponse
    {
        public string RoomType;
        public string RoomTypeDescription;
    }
}