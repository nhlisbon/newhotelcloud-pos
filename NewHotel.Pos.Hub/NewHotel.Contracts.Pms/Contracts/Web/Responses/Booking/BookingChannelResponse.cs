﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class BookingChannelResponse : BaseResponse
    {
        public string BookingServiceDescription;
        public BookingServiceType? BookingServiceType;
        public bool Subcanal;
        public bool IsDaily;
        public bool Overallotment;
        public decimal? OverbookingPercent;
        public bool Allowoverbooking;
        public bool MaxPersonsRoom;
        public string ClientTypeId;
        public DailyAccountType? AccountType;
        public bool CheckPrice;
        public decimal? DepositValue;
        public DepositInfoType? DepositType;
        public Guid? BookingId { get; set; }
        public Guid? HotelChannelId { get; set; }
        public string ChannelUser { get; set; }
        public string ChannelPassword { get; set; }
        public string ChannelHotelCode { get; set; }
        public string ChannelHotelName { get; set; }
        public string EmailConfirmation { get; set; }
        public bool Inactive { get; set; }
        public string PriceRate { get; set; }
        public string Segment { get; set; }
        public string Origin { get; set; }
        public string UserCreation { get; set; }
        public string UserCancellation { get; set; }
        public string DepartamentPayment { get; set; }
        public string PaymentType { get; set; }
        public bool CheckUserInCancelation { get; set; }
        bool AllowOverAllotment { get; set; }
        public bool AllowOverBooking { get; set; }
        public decimal? OverBookingPercent { get; set; }
        public bool CheckReservationAvailability { get; set; }
        /// <summary>
        /// este es el AllowWaitingList del cloud
        /// </summary>
        public bool NeverConfirmReservation { get; set; }
        public bool MaxPersonsAdvise { get; set; }
        public string Warranty { get; set; }
        public string CancellationReason { get; set; }
        public string PaymentCancellationReason { get; set; }
        public InvoiceInstructionType? InvoiceInstruction { get; set; }
        /// <summary>
        /// Permitir hacer reservas de una entidad que no sea la configurada por defecto
        /// </summary>
        public bool AllowCustomReservationEntity { get; set; }
        public string TaxSchema { get; set; }
        public string ClientType { get; set; }
        public bool FixPrice { get; set; }
        public CheckPriceActionType? CheckPriceAction { get; set; }
        public string ServiceAdditional { get; set; }
        public string DepartamentAdditional { get; set; }
        public bool IsDailyAdditional { get; set; }
        public short[] RatePlans { get; set; }
        public bool Availability { get; set; }
        public bool ImportReservations { get; set; }
        public bool ExportPrices { get; set; }
        public ExternalChannelExportMode? ExportMode { get; set; }
        public bool ExportRestrictions { get; set; }
        public bool ExportClassifiers { get; set; }
        public int? ExportDaysCount { get; set; }
        public string EntityId { get; set; }
        public string AgencyId { get; set; }
        public string Contract { get; set; }
        public NewHotel.Contracts.OTA.AdditionalEntryType? MealTypeAdditional { get; set; }
        public long UseReservationMarketSegment { get; set; }
        public long UseReservationMarketOrigin { get; set; }
        public long ChildPriceType { get; set; }
        public string AllotmentId { get; set; }
        public bool ApplyTaxGuest { get; set; }
        public bool AmountAfterTax { get; set; }
        public BookingVoucherType? BookingVoucherType { get; set; }
        public string ReservationCountry { get; set; }
        public string GuestCountry { get; set; }
        public string EntityCountry { get; set; }
        public string AgencyCountry { get; set; }
        public EBookingTravelAgencyIntegrationType TravelAgencyIntegrationMode { get; set; }
        public EBookingCompanyIntegrationType EntityIntegrationMode { get; set; }
        public EActionWhenPriceError ActionWhenPriceReservationError { get; set; }
        public EActionWhenTotalPriceError ActionWhenReservationTotalPriceError { get; set; }
        public EBookingChannelPaymentsIntegrationType PaymentsIntegrationType { get; set; }
        public bool ReservationSyncPush { get; set; }
        public bool ReservationAsyncPush { get; set; }
        public EActionWhenModifyWithoutInsert ActionWhenModifyWithoutInsert { get; set; }
        public bool CreditCardMapingDefaultIfNoExist { get; set; }
        public bool RoomRateMapingDefaultIfNoExist { get; set; }
        public bool RoomRateMapingGeneralIfNoExist { get; set; }
        public EConfirmationState ReservationConfirmationState { get; set; }
        public EAttentionMappingType AttentionMappingType { get; set; }
        public Guid? ReservationDefaultAttentionCode { get; set; }
        public EBookingGroupIntegrationType GroupIntegrationType { get; set; }
        public bool? AllowChangeOccupiedRoomTypeWithAssignedRoom { get; set; }
        public bool? AllowPMSOccupiedRoomTypeModification { get; set; }
        public bool? UnassignRoomBookingChannel { get; set; }
        public bool? AllowPMSReservedRoomTypeModification { get; set; }
        public bool? AssignRoomInCheckIn { get; set; }
        public DateTime? ActivationDate { get; set; }
        public ESynchronizationType? SyncType { get; set; }
        public EExpediaExportLOSType? ExpediaExportLOSType { get; set; }
        public EExpediaExportPriceType? ExportPriceType { get; set; }
        public EFastbookingExportType? FastbookingExportType { get; set; }
        public EUseAdditionals? UseAdditionals { get; set; }
        public EOriginMappingType? OriginMappingType { get; set; }
        public ESegmentMappingType? SegmentMappingType { get; set; }
        public EActionWhenNoPricesInformed? ActionWhenNoPricesInformed { get; set; }
        public EActionWhenInvalidTaxInformed? ActionWhenInvalidTaxInformed { get; set; }
        public EBookingGuestEmailIntegrationType? EmailIntegrationType { get; set; }
        public EActionWhenPMSDataValidationError? ActionWhenPMSDataValidationError { get; set; }

        public BookingChannelResponse()
        {
            TravelAgencyIntegrationMode = EBookingTravelAgencyIntegrationType.None;
            EntityIntegrationMode = EBookingCompanyIntegrationType.None;
            ActionWhenPriceReservationError = EActionWhenPriceError.NotifyError;
            ActionWhenReservationTotalPriceError = EActionWhenTotalPriceError.NotifyError;
            PaymentsIntegrationType = EBookingChannelPaymentsIntegrationType.None;
            ActionWhenModifyWithoutInsert = EActionWhenModifyWithoutInsert.NotifyError;
            ReservationConfirmationState = EConfirmationState.Confirmed;
            AttentionMappingType = EAttentionMappingType.None;
            GroupIntegrationType = EBookingGroupIntegrationType.None;
        }
    }

    public class BookingChannelManagementResponse : BaseResponse
    {
        public Guid BookingId { get; set; }
        public Guid HotelChannelId { get; set; }
    }

    public enum EHotelTypeResponse
    {
        /// <remarks/>
        NewHotelPMS,
        /// <remarks/>
        NewHotelCloudPMS,
    }

    public class HotelDataResponse : BaseResponse
    {        
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public string Address { get; set; }
        public string ContactPerson{ get; set; }
        public string Phone{ get; set; }
        public string Fax{ get; set; }
        public string Email { get; set; }
        public string Url { get; set; }
        public string DocumentLanguage { get; set; }
        public EHotelTypeResponse Type { get; set; }
        public Guid? HotelDistributorId { get; set; }
        public Guid? CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string DistribuidorName { get; set; }
        public string CountryIso2 { get; set; }
        public string CountryIso3 { get; set; }
		public string Currency { get; set; }
        public string UrlBookingChannel { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string AddressCountryIso2 { get; set; }
        public string AddressCountryIso3 { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Latitude { get; set; }
    }
}