﻿using System;

namespace NewHotel.Web.Services
{
    public class AvailabilityByDateResponse
    {
        public DateTime Date;
        public bool IsAvailable;
    }
}