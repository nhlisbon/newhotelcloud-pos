﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NewHotel.Web.Services
{
    [XmlRoot("ClickViewEntity")]
    public class ClickViewEntityResponse : BaseResponse
    {
        public List<ClickViewEntity> Entities;
        public DateTime AccessTime { get; set; }
    }

    public class ClickViewEntity
    {
        public Guid Id { get; set; }
        public string Abbreviation { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string Locality { get; set; }
        public string City { get; set; }
        public string CityCode { get; set; }
        public string State { get; set; }
        public string StateCode { get; set; }
        public string FreeCode { get; set; }
        public string CommercialName { get; set; }
        public string PostalCode { get; set; }
        public Guid? AccountId { get; set; }
        public string HomePhone { get; set; }
        public string BusinessPhone { get; set; }
        public string CellPhone { get; set; }
        public string FaxPhone { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Country { get; set; }
        public bool Inactive { get; set; }
        public string CompanyType { get; set; }
        public string CompanyCategory { get; set; }
        public string Origen { get; set; }
        public string Segment { get; set; }
        public Guid CompanyTypeId { get; set; }
        public EntityCategory CompanyCategoryId { get; set; }
        public Guid OrigenId { get; set; }
        public Guid SegmentId { get; set; }
        public string FiscalNumber { get; set; }

    }
}
