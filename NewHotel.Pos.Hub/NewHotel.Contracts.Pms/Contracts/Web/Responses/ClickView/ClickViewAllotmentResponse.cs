﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NewHotel.Web.Services
{
    [XmlRoot("ClickViewAllotment")]
    public class ClickViewAllotmentResponse : BaseResponse
    {
        public List<ClickViewAllotment> Allotments;
        public DateTime AccessTime { get; set; }
    }

    public class ClickViewAllotment
    {
        public Guid AllotmentId { get; set; }
        public string AllotmentDescription { get; set; }
        public bool Garantee { get; set; }
        public Guid HotelId { get; set; }
        public string HotelDescription { get; set; }
        public DateTime Date { get; set; }
        public Guid RoomTypeId { get; set; }
        public int ReleaseDays { get; set; }
        public string RoomTypeAbbreviation { get; set; }
        public int Contracted { get; set; }
    }
}
