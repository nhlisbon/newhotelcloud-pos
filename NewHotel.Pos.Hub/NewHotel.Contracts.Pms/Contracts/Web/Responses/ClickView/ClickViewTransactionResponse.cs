﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NewHotel.Web.Services
{
    [XmlRoot("ClickViewTransaction")]
    public class ClickViewTransactionResponse : BaseResponse
    {
        public List<ClickViewTransaction> Transactions;
        public DateTime AccessTime { get; set; }
    }

    public class ClickViewTransaction
    {
        public Guid Id { get; set; }
        public Guid HotelId { get; set; }
        public string HotelDescription { get; set; }
        public DateTime ValueDate { get; set; }
        public decimal GrossValue { get; set; }
        public decimal NetValue { get; set; }
        public Guid? ReservationId { get; set; }
        public string ReservationNumber { get; set; }
        public DateTime RegistrationDate { get; set; }
        public Guid UserId { get; set; }
        public string UserDescription { get; set; }
        public Guid DepartmentId { get; set; }
        public string DepartmentDescription { get; set; }
        public Guid? ServiceId { get; set; }
        public string ServiceDescription { get; set; }
        public bool IsPayment { get; set; }
    }
}
