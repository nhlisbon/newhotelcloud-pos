﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NewHotel.Web.Services
{
    [XmlRoot("ClickViewStatistics")]
    public class ClickViewStatisticsResponse : BaseResponse
    {
        public List<ClickViewStatistics> Statistics;
        public DateTime AccessTime { get; set; }
    }

    public class ClickViewStatistics
    {
        public Guid HotelId { get; set; }
        public string HotelDescription { get; set; }
        public DateTime Date { get; set; }
        public Guid ReservationId { get; set; }
        public string ReservationNumber { get; set; }
        public Guid? EntityId { get; set; }
        public int Adults { get; set; }
        public int Children { get; set; }
        public int Inventory { get; set; }
        public int Inactives { get; set; }
        public int OutOfRental { get; set; }
        public int Reservations { get; set; }
        public decimal RoomRevenue { get; set; }
        public decimal FoodBeverageRevenue { get; set; }
        public decimal OthersRevenue { get; set; }

    }
}
