﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NewHotel.Web.Services
{
    [XmlRoot("ClickViewReservation")]
    public class ClickViewReservationResponse : BaseResponse
    {
        public List<ClickViewReservation> Reservations;
        public DateTime AccessTime { get; set; }
    }

    public class ClickViewReservation
    {
        public Guid ReservationId { get; set; }
        public string ReservationNumber { get; set; }
        public Guid? EntityId { get; set; }
        public Guid? AgencyId { get; set; }
        public Guid GroudId { get; set; }
        public string GroupName { get; set; }
        public Guid? AllotmentId { get; set; }
        public string AllotmentDescription { get; set; }
        public Guid HotelId { get; set; }
        public string HotelDescription { get; set; }
        public ReservationState Status { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }
        public Guid ReservedRoomTypeId { get; set; }
        public string ReservedRoomTypeAbbreviation { get; set; }
        public Guid OccupiedRoomTypeId { get; set; }
        public string OccupiedRoomTypeAbbreviation { get; set; }
        public long PensionId { get; set; }
        public string PensionDescription { get; set; }
        public int Adults { get; set; }
        public int Children { get; set; }
        public Guid UserId { get; set; }
        public string UserDescription { get; set; }
        public decimal RoomRevenueIncome { get; set; }
        public decimal FoodAndBeverageIncome { get; set; }
        public decimal OtherIncome { get; set; }
    }
}
