﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NewHotel.Web.Services
{
    [XmlRoot("ClickViewClient")]
    public class ClickViewClientResponse : BaseResponse
    {
        public List<ClickViewClient> Clients;
        public DateTime AccessTime { get; set; }
    }

    public class ClickViewClient
    {
        public Guid Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public string Locality { get; set; }
        public string City { get; set; }
        public string CityCode { get; set; }
        public string State { get; set; }
        public string StateCode { get; set; }
        public string FreeCode { get; set; }
        public string PostalCode { get; set; }
        public Guid? AccountId { get; set; }
        public string HomePhone { get; set; }
        public string BusinessPhone { get; set; }
        public string CellPhone { get; set; }
        public string FaxPhone { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Country { get; set; }
        public bool Inactive { get; set; }
        public string FiscalNumber { get; set; }

    }
}
