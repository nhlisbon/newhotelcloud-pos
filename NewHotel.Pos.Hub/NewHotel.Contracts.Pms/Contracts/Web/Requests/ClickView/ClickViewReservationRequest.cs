﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class ClickViewReservationRequest : BaseRequest
    {
        public DateTime? Arrival { get; set; }
        public DateTime? Departure { get; set; }
        public DateTime? ModifyAfter { get; set; }
        public DateTime? ModifyBefore { get; set; }
        public DateTime? CreatedAfter { get; set; }
        public DateTime? CreatedBefore { get; set; }
    }
}
