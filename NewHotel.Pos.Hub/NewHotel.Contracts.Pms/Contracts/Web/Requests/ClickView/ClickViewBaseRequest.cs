﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class ClickViewBaseRequest : BaseRequest
    {
        public DateTime? TimeStamp { get; set; }
    }
}
