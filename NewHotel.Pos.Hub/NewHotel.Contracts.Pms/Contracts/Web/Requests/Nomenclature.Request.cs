﻿using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class NomenclatureFilter
    {
        public NomenclatureID Nomenclature;
        public List<FilterArgumentRequest> Filters;
    }

    public class NomenclatureRequest : BaseRequest
    {
        public NomenclatureID[] Nomenclatures;
        public List<NomenclatureFilter> NomenclatureFilters;
    }
}