﻿using NewHotel.Contracts;
using System;

namespace NewHotel.Web.Services
{
    /// <summary>
    /// Request for Guest Information per Room
    /// </summary>
    public class GuestInfoRequest : BaseRequest
    {
        /// <summary>
        /// Room Numnber
        /// </summary>
        public string RoomNumber { get; set; }
    }
}
