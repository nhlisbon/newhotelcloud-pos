﻿using System;

namespace NewHotel.Web.Services
{
    public class CancelEntrieRequest : BaseRequest
    {
        public long ApplicationId;
        public string CancellationReasonId;
        public string Document;
        public string Comment;
        public string[] Entries;
    }
}
