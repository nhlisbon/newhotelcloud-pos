﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class EntrieRequest
    {
        public string ID;
        public string DepartmentId;
        public string Currency;
        public DateTime Date;
        public decimal Value;
        public decimal? ForeignValue;
        public decimal? ExchangeRate;
        public string AccountId;
        public DailyAccountType AccountFolder;
        public string Document;
        public string Description;
        public string InvoiceSerie;
        public long? InvoiceNumber;
    }

    public class CreditRequest : EntrieRequest
    {
        public string ServiceId;
        public short? Quantity;
        public decimal? DiscountPercent;
        public decimal? DiscountValue;
        public decimal? CommissionValue;
        public bool TaxIncluded;
    }

    public class DebitRequest : EntrieRequest
    {
        public string WithdrawTypeId;
        public string CreditCardTypeId;
        public string CreditCardNumber;
        public short? ValidationMonth;
        public short? ValidationYear;
        public string ValidationCode;
    }

    public class CreateEntrieRequest : BaseRequest
    {
        public long ApplicationId;
        public CreditRequest[] Credits;
        public DebitRequest[] Debits;        
    }
}
