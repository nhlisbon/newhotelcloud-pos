﻿using NewHotel.Contracts;
using System;

namespace NewHotel.Web.Services
{
    /// <summary>
    /// Request for a Charge to a Room
    /// </summary>
    public class RoomChargeRequest : BaseRequest
    {
        /// <summary>
        /// Room Numnber
        /// </summary>
        public string RoomNumber { get; set; }
        /// <summary>
        /// Service Identifier
        /// </summary>
        public Guid ServiceId { get; set; }
        /// <summary>
        /// Quantity
        /// </summary>
        public short Quantity { get; set; }
        /// <summary>
        /// Total Value
        /// </summary>
        public decimal Value { get; set; }
    }
}
