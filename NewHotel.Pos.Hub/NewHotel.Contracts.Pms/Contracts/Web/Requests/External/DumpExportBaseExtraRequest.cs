﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class DumpExportBaseExtraRequest : DumpExportBaseRequest
    {
        public DateTime? LastModifiedAfter { get; set; }
        public DateTime? LastModifiedBefore { get; set; }
    }
}
