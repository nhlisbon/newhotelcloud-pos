﻿using System;

namespace NewHotel.Web.Services
{
    public class PeriodRequest : BaseRequest
    {
        public DateTime? InitialDate { get; set; }
        public DateTime? FinalDate { get; set; }
    }
}
