﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class TextFileRequest : DateBaseRequest
    {
        public ExternalDeviceType DeviceType { get; set; }
    }
}
