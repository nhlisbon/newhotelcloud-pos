﻿using System;

namespace NewHotel.Web.Services
{
    public class DumpReservationRequest : BaseRequest
    {
        public Guid[] ReservationIds { get; set; }
    }
}
