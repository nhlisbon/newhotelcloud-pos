﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class BavelRequest : BaseRequest
    {
        public Guid ExportId { get; set; }
    }
}
