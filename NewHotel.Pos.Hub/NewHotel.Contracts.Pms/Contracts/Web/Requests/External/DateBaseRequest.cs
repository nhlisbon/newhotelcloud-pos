﻿using System;

namespace NewHotel.Web.Services
{
    public class DateBaseRequest : BaseRequest
    {
        public DateTime ExportDate { get; set; }
    }
}
