﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class DumpExportBaseRequest : BaseRequest
    {
        public DateTime? TimeStamp { get; set; }
        public bool Occupancy { get; set; }
        public DateTime? WorkDateAfter { get; set; }
        public DateTime? WorkDateBefore { get; set; }
    }
}
