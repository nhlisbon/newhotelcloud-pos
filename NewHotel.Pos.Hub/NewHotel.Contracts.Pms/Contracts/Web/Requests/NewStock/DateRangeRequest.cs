﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class DateRangeRequest : BaseRequest
    {
        public DateTime InitialDate { get; set; }
        public DateTime FinalDate { get; set; }
    }
}