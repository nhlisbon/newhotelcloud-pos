﻿using System;
using NewHotel.Contracts;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{

    public class AccEntrieDetail
    {
        public string ID;
        public string FinnatialCategoryId;
        public string CostCenterId;
        public decimal Percent;
        public decimal Value;
    }

    public class AccEntrieBaseRequest
    {
        public string ID;
        public string AccountId;
        public DateTime Date;
        public string Document;
        public string Description;
        public DateTime? ExpireDate;
        public string DocumentReference;
        public string Currency;
        public decimal Value;
        public decimal? ForeignValue;
        public decimal? ExchangeRate;
        public string ForeingCurrency;
        public AccEntrieDetail[] Details;
    }

    public class CreateAccEntrieBaseRequest : BaseRequest
    {
        public long ApplicationId;
        public AccEntrieBaseRequest[] Movements;
    }


    public class AccEntrieRequest : AccEntrieBaseRequest
    {
        public long MovementType;
        public int OperationType; //MOCO_CLIE => 0-Compras; 1-Ventas

        public AccEntrieRequest(AccEntrieBaseRequest parent)
        {
            ID = parent.ID;
            AccountId = parent.AccountId;
            Date = parent.Date;
            Document = parent.Document;
            Description = parent.Description;
            ExpireDate = parent.ExpireDate;
            DocumentReference = parent.DocumentReference;
            Currency = parent.Currency;
            Value = parent.Value;
            ForeignValue = parent.ForeignValue;
            ExchangeRate = parent.ExchangeRate;
            ForeingCurrency = parent.ForeingCurrency;

            if (parent.Details != null)
            {
                List<AccEntrieDetail> lstDetails = new List<AccEntrieDetail>();
                foreach (AccEntrieDetail det in parent.Details)
                    lstDetails.Add(det);
                Details = lstDetails.ToArray();
            }
        }
}

    public class CreateAccEntrieRequest : BaseRequest
    {
        public long ApplicationId;
        public AccEntrieRequest[] Movements;
    }

    public class CancelAccEntrieRequest : BaseRequest
    {
        public long ApplicationId;
        public string CancellationReasonId;
        public string Comment;
        public string[] Entries;
    }

}
