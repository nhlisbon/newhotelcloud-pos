﻿using NewHotel.Contracts;
using System;

namespace NewHotel.Web.Services
{
    public class WebExpressAvailabilityRequest : BaseRequest
    {
        public Guid? ChannelId { get; set; }
        public DateTime? InitialDate { get; set; }
        public Guid? PriceRateId { get; set; }
    }
}
