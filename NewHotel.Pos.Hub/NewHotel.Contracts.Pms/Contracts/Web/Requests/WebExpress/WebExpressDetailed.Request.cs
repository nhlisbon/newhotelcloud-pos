﻿using NewHotel.Contracts;
using System;

namespace NewHotel.Web.Services
{
    public class WebExpressDetailedRequest : BaseRequest
    {
        public Guid? ExternalChannelId{get;set;}
        public DateTime InitialDate { get; set; } 
        public DateTime FinalDate { get; set; }
        public short Adults { get; set; }
        public short Childs { get; set; }
        public Guid? PriceRateId { get; set; }
    }
}
