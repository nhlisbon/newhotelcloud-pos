﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class PhoneCallRequest : BasePhoneRequest
    {
        public string CallNumber;
        public DateTime CallDateTime;
        public long CallPulse;
        public string CallLenght;
        public PhonePriceCollection[] PriceIDs;
    }
}