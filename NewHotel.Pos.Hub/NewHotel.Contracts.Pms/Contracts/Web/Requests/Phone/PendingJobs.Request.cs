﻿using System;

namespace NewHotel.Web.Services
{
    public class PendingJobsRequest : BaseRequest
    {
        #region Constants

        public const int DEVICE_PHONE = 1;
        public const int DEVICE_KEYS = 2;
        public const int DEVICE_PAYTV = 3;
        public const int DEVICE_TRANSFERCC = 4;
        public const int DEVICE_SCANDOC = 5;
        public const int DEVICE_INTERNET = 6;
        public const int DEVICE_TEF = 7;
        public const int DEVICE_AC = 8;
        public const int DEVICE_FILE = 11;
        public const int DEFAULTBATCHSIZE = 25;

        #endregion
        #region Members

        public int BatchSize = 0;
        public int DeviceType = DEVICE_PHONE;
        public string WorkStation = null;

        #endregion
    }
}