﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class PayTVCallRequest : BasePhoneRequest
    {
        public string Code;
        public string Description;
        public decimal Value;
        public short Quantity;
        public DateTime CallDateTime;
    }

    public class InternetCallRequest : PayTVCallRequest
    {
    }
}