﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class BillPayTVCallRequest : BasePhoneRequest
    {
        public bool IsMaster;
        public bool? Invoiced;
        public int DeviceType = 3;
        public bool Payment;
    }
}