﻿using System;

namespace NewHotel.Web.Services
{
    public class PhoneDeviceRequest : BasePhoneRequest 
    {
        public PhoneDeviceCollection[] DeviceIDs;
    }
}