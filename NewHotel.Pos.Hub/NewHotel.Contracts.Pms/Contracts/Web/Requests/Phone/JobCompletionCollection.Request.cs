﻿using System;

namespace NewHotel.Web.Services
{
    public class JobCompletionCollectionRequest : BaseRequest
    {
        public const int DEVICE_PHONE = 1;
        public const int DEVICE_KEYS = 2;
        public const int DEVICE_PAYTV = 3;
        public const int DEVICE_TRANSFERCC = 4;
        public const int DEVICE_INTERNET = 6;
        public const int DEVICE_TEF = 7;
        public const int DEVICE_AC = 8;
        public const int DEVICE_EXTERNAL = 11;
        #region Members

        public JobCompletionRequest[] Completions;
        public int DeviceType = DEVICE_PHONE;

        #endregion
    }
}