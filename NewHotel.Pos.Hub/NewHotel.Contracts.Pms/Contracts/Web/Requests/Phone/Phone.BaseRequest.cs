﻿using System;

namespace NewHotel.Web.Services
{
    public class BasePhoneRequest : BaseRequest
    {
        public string ExtensionID;
    }

    public class BaseExtensionRequest : BaseRequest
    {
        /// <summary>
        ///  Tipo Extensión (Phone = 1831, PayTv = 1832, Keys = 1833, Internet = 1834, Gates = 1835)
        /// </summary>
        public long ExtensionType;
    }
}