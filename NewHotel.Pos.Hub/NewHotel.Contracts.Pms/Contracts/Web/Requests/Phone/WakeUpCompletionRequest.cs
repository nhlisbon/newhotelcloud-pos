﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class WakeUpCompletionRequest : BaseRequest
    {
        public Guid ExtensionId { get; set; }
        public WakeUpCallState Status { get; set; }
    }
}