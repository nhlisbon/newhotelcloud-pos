﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class TransferCCRequest : BaseRequest
    {
        public TransferCCDataRequest TransferCCData;
        public DevolutionDetailsRequest DevolutionDetails;
        public ReplieDetailsRequest ReplieDetails;
    }

    public class TransferCCDataRequest : BaseRequest
    {
        public string TransferCCId;
        public string OperatorUser;
        public string TicketId;
        public long OperationType;
        public decimal TransferCCValue;
    }

    public class DevolutionDetailsRequest
    {
        public string NumberTransationCO;
        public string NumberTransationBCO;
        public string AutorizationCode;
        public DateTime IssueDate;
    }

    public class ReplieDetailsRequest
    {
        public string CodeResult;
        public string DescriptionReceived;
        public string Terminal;
        public string ApplicationChip;
        public string ApplicationChipDesc;
        public string CardNumber;
        public string TradeNumber;
        public string CardMark;
        public string HolderCard;
        public string BankName;
        public string ARCDesc;
        public bool SignatureRequired;
        public string NumberRefTP;
    }

}