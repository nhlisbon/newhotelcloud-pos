﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Web.Services;

namespace NewHotel.Contracts
{
    public class PendingMovementsRequest : BaseRequest
    {
        #region Constants
        public const int DEFAULTBATCHSIZE = 25;
        #endregion
        #region Members
        public int BatchSize = 0;
        #endregion
    }
}
