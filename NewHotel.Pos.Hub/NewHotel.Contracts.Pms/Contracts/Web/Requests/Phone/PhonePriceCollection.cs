﻿using System;

namespace NewHotel.Web.Services
{
    public class PhonePriceCollection
    {
        public int ID;
        public string PriceDefinition;
        public decimal PriceValue;
    }
}