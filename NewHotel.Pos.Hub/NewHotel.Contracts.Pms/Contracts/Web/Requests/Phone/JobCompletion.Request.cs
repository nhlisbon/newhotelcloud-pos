﻿using System;

namespace NewHotel.Web.Services
{
    public class JobCompletionRequest
    {
        #region Constants

        public const int STATUS_NONE = 0;
        public const int STATUS_PROGRAMED = 1;
        public const int STATUS_UNATTENDED = 2;
        public const int STATUS_ATTENDED = 3;
        public const int STATUS_EXECUTED = 4;
        public const int STATUS_CANCELLED = 5;
        public const int STATUS_ABORTED = 6;

        #endregion
        #region Members

        public string JobID;
        public DateTime? CompletionDateTime;
        public int CompletionStatus = STATUS_NONE;
        public TransferCCRequest TransferCCInfoDetails;
        public string DigitCode;
        public bool MessageReceived; //solo para los mensajes

        #endregion
    }
}