﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Web.Services;

namespace NewHotel.Contracts
{
    public class MovementCompletionCollectionRequest : BaseRequest
    {
        #region Members

        public MovementCompletionRequest[] Completions;

        #endregion
    }
}
