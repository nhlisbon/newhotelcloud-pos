﻿using System;

namespace NewHotel.Web.Services
{
    public class PhoneNomenclatureRequest : BaseRequest
    {
        public int[] NomenclatureIDs;
    }
}