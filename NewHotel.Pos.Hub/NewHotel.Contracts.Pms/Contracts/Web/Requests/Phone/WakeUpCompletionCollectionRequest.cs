﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class WakeUpCompletionCollectionRequest : BaseRequest
    {
        #region Members

        public WakeUpCompletionRequest[] Requests;

        #endregion
    }
}