﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Contracts
{
    public class MovementCompletionRequest
    {
        #region Constants

        public const int STATUS_NONE = 0;
        public const int STATUS_EXECUTED = 1;

        #endregion
        #region Members

        public string MovementID;
        public DateTime? CompletionDateTime;
        public int CompletionStatus = STATUS_NONE;

        #endregion
    }
}
