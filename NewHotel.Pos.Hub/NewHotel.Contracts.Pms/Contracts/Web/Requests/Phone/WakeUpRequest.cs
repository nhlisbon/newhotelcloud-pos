﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class WakeUpRequest : BaseRequest
    {
        public int BatchSize { get; set; }
        public WakeUpCallTypeCall DeviceType { get; set; }

    }
}