﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class RoomStatusRequest : BasePhoneRequest
    {
        public RoomStatus Status;
    }
}