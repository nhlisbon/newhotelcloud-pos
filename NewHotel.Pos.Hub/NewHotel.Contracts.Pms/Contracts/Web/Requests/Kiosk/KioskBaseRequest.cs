﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskBaseRequest : BaseRequest
    {
        public Guid ExternalChannelID { get; set; }
    }
}
