﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskAddReservationPaymentRequest : KioskBaseRequest
    {
        public Guid PaymentId { get; set; }
        public string ReservationName { get; set; }
        public Guid? ReservationId { get; set; }
        public decimal Value { get; set; }
        public string Description { get; set; }
        public DateTime? Date { get; set; }
        public Guid? PaymentMethod { get; set; }
        public bool Invoice { get; set; }
        public DailyAccountType? PaymentFolder { get; set; }
    }
}
