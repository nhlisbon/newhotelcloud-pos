﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskGetRoomTypePricesRequest : KioskBaseRequest
    {
        public string ReservationName { get; set; }
        public Guid? PriceRateId { get; set; }
        public short? PensionMode { get; set; }
        public Guid? RoomTypeId { get; set; }
        public DateTime ArrivalDate { get; set; }
        public int Nights { get; set; }
        public int Paxs { get; set; }
        public string sPaxs { get; set; }
        public bool IncludeRoomsWithoutPrices { get; set; }
        public bool IncludeDowngrades { get; set; }
        public bool NewReservationFromKiosk { get; set; }
    }
}
