﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskGetReservationRequest : KioskBaseRequest
    {
        public Guid? GroupReservationId { get; set; }
        public string GroupReservationName { get; set; }
        public Guid? ReservationId { get; set; }
        public string ReservationName { get; set; }
        public Guid[] ExternalReservationsIds { get; set; }
    }
}
