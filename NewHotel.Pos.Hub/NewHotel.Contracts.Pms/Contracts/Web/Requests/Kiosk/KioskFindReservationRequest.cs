﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskFindReservationRequest : KioskBaseRequest
    {
        public List<KioskFilterArgument> Arguments { get; set; }
    }
}
