﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskGetRoomsRequest : KioskBaseRequest
    {
        public string RoomNumber { get; set; }
        public Guid? RoomTypeId { get; set; }
        public RoomStatus? Status { get; set; }
    }
}
