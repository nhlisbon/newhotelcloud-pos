﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskImageRequest : KioskBaseRequest
    {
        public string ImageId { get; set; }
    }
}
