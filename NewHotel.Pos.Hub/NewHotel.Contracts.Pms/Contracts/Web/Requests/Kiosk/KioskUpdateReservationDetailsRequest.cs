﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskUpdateReservationDetailsRequest : KioskBaseRequest
    {
        public string ReservationName { get; set; }
        public Guid? ReservationId { get; set; }
        public DateTime ArrivalDate { get; set; }
        public int Nights { get; set; }
        public int Paxs { get; set; }
        public string sPaxs { get; set; }
        public Guid? RoomId { get; set; }
        public short? PensionMode { get; set; }
        public Guid? PriceRateId { get; set; }
        public KioskRoomTypeInfo RoomTypeInfo { get; set; }
        public string[] KeyIds { get; set; }
        public bool SendInvoice { get; set; }
        public List<string> EmailAddresses { get; set; }
        public bool GenerateReservationsEntries { get; set; }
    }
}
