﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskUpdateReservationStatusRequest : KioskBaseRequest
    {
        public string ReservationName { get; set; }
        public ReservationState Status { get; set; }
        public bool JustCheck { get; set; }
    }
}
