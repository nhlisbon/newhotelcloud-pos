﻿using NewHotel.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskFilterArgument
    {
        public KioskFilterFields Name { get; set; }
        public object Value { get; set; }
        public string Expression { get; set; }
        public string Group { get; set; }
        public FilterTypes FilterType { get; set; }
    }

    public enum KioskFilterFields : long { ReservationNumber, ReservationStatus, ReservationCheckIn, ReservationCheckOut, ReservationVoucher, GroupName, GuestName, GuestEmail, GuestPhone, ReservationArriveToday, ReservationDepartureToday, RoomNumber, ExternalChannelNumber, RoomKey, ConfirmationNumber }
}
