﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskCreateReservationDetailsRequest : KioskBaseRequest
    {
        public DateTime ArrivalDate { get; set; }
        public int Nights { get; set; }
        public int Paxs { get; set; }
        public string sPaxs { get; set; }
        public Guid? RoomId { get; set; }
        public short? PensionMode { get; set; }
        public Guid? PriceRateId { get; set; }
        public KioskRoomTypeInfo RoomTypeInfo { get; set; }
    }
}
