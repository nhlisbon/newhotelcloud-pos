﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskAssignRoomRequest : KioskBaseRequest
    {
        public Guid? RoomId { get; set; }
        public Guid? ReservationId { get; set; }
        public string ReservationName { get; set; }
    }
}
