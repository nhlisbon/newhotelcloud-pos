﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskUpdateReservationGuestsRequest : KioskBaseRequest
    {
        public string ReservationName { get; set; }
        public List<KioskReservationGuest> Guests { get; set; }
    }
}
