﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Web.Services
{
    public class KioskUpdateGroupStatusRequest : KioskBaseRequest
    {
        public string GroupName { get; set; }
        public ReservationState Status { get; set; }
    }
}
