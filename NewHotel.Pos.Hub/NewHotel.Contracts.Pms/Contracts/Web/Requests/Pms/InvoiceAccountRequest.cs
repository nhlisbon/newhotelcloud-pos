﻿using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class InvoiceService
    {
        public string ServiceCode { get; set; }
        public short Quantity { get; set; }
        public decimal Amount { get; set; }
    }

    public class InvoiceAccountRequest : BaseRequest
    {
        public string AccountCode { get; set; }
        public HolderData Holder { get; set; }
        public List<InvoiceService> Services { get; set; }
        public List<InvoicePayment> Payments { get; set; }
    }
}