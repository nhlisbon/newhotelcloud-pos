﻿using System;
using System.Collections.Generic;

namespace NewHotel.Web.Services
{
    public class HolderData
    {
        public string Name { get; set; }
        public string Nationality { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string FiscalNumber { get; set; }
        public string Email { get; set; }
    }

    public class InvoicePayment
    {
        public string PaymentTypeCode { get; set; }
        public decimal Amount { get; set; }
        public string CreditCardTypeCode { get; set; }
        public string CreditCardNumber { get; set; }
        public string CreditCardTransactionNumber { get; set; }
        public string CreditCardAuthorizationCode { get; set; }
        public string CreditCardTerminalId { get; set; }
    }

    public class InvoiceReservationRequest : BaseRequest
    {
        public string ReservationSerie { get; set; }
        public long ReservationNumber { get; set; }
        public HolderData Holder { get; set; }
        public List<InvoicePayment> Payments { get; set; }
    }
}