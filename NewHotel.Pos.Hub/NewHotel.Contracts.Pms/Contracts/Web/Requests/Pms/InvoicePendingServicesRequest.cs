﻿using System;

namespace NewHotel.Web.Services
{
    public class ReservationInvoicePendingsRequest : BaseRequest
    {
        public string ReservationSerie { get; set; }
        public long ReservationNumber { get; set; }
    }
}