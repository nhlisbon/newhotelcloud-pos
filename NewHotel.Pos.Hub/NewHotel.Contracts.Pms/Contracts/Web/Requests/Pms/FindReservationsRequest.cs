﻿using System;

namespace NewHotel.Web.Services
{
    public class FindReservationsRequest : BaseRequest
    {
        public string ExternalId { get; set; }
    }
}