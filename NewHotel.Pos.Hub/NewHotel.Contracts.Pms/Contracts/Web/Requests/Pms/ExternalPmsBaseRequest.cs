﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class ExternalPmsBaseRequest : BaseRequest
    {
        public DateTime InitialDate { get; set; }
        public DateTime? FinalDate { get; set; }
    }
}
