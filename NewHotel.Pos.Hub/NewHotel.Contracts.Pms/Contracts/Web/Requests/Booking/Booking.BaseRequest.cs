﻿using System;

namespace NewHotel.Web.Services
{
    public class BaseBookingRequest : BaseRequest
    {
        public string Channel;
        public string SubChannel;
    }
}