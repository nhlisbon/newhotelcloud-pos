﻿using System.Collections.Generic;

namespace NewHotel.Web.Services
{    
    public class ExternalChannelNotificationData
    {
        public string EntityCode { get; set; }
        public string AgencyCode { get; set; }
        public string RoomTypeCode { get; set; }
        public string OperatorCode { get; set; }
        public string PriceRateCode { get; set; }
        public string PackageCode { get; set; }
        public string AllotmentCode { get; set; }
        public string RatePlanCode { get; set; }
    }

    public class ExternalChannelNotificationRequest : BaseRequest
    {
        public List<ExternalChannelNotificationData> NotificationData { get; set; }
    }
}