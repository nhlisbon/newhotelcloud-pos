﻿using System;

namespace NewHotel.Web.Services
{
    public class BookingNomenclatureRequest : BaseBookingRequest
    {
        public int[] NomenclatureIDs;
    }
}