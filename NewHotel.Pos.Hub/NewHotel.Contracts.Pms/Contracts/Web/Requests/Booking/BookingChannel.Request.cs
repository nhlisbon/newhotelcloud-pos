﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class BookingChannelRequest : BaseRequest
    {
        public Guid? BookingChannelId { get; set; }
        public Guid? HotelChannelId { get; set; }
        public string BookingServiceDescription { get; set; }
        public BookingServiceType? BookingServiceType { get; set; }
        public string ChannelUser { get; set; }
        public string ChannelPassword { get; set; }
        public string ChannelHotelCode { get; set; }
        public string ChannelHotelName { get; set; }
        public string EmailConfirmation { get; set; }
        public bool Inactive { get; set; }
        public bool Subcanal { get; set; }
        public string EntityId { get; set; }
        public string AgencyId { get; set; }
        public string ContractId { get; set; }
        public string PriceRateDesc { get; set; }
        public string SegmentId { get; set; }
        public string OriginId { get; set; }
        public string UserCreation { get; set; }
        public string UserCancellation { get; set; }
        public string DepartamentPayment { get; set; }
        public string ReservationPaymentId { get; set; }
        public DailyAccountType? ReservationPaymentFolder { get; set; }
        public string DepartamentAdditional { get; set; }
        public string  ServiceAdditional { get; set; }
        public bool IsDaily { get; set; }
        public bool UserCreationCancellation { get; set; }
        /// <summary>
        /// Decir si las reservas q vienen de este canal pueden permitir overallotment, si tengo q chequear disponibilidad
        /// </summary>
        public bool Overallotment { get; set; }
        public decimal? OverbookingPercent { get; set; }
        public bool Availability { get; set; }
        public bool Allowoverbooking { get; set; }
        public bool NeverConfirmReservation { get; set; }
        public bool MaxPersonsRoom { get; set; }
        public string WarrantyTypeId { get; set; }
        public string CancellationReasonId { get; set; }
        public string PaymentCancellationReasonId { get; set; }
        public string TaxSchemaId { get; set; }
        public DepositInfoType? DepositType { get; set; }
        public decimal? DepositValue { get; set; }
        public bool CheckPrice { get; set; }
        public InvoiceInstructionType InvoiceInstruction { get; set; }
        public bool ImportReservations { get; set; }
        public bool ExportPrices { get; set; }
        public ExternalChannelExportMode ExportMode { get; set; }
        public bool ExportRestrictions { get; set; }
        public bool ExportClassifiers { get; set; }
        public int? ExportDaysCount { get; set; }
        public bool AllowCustomReservationEntity { get; set; }
        public string Login { get; set; }
        public string ReservationGroupTypeId { get; set; }
        public string ClientTypeId { get; set; }
        public DailyAccountType? AccountType { get; set; }
        public bool FixPrice { get; set; }
        public CheckPriceActionType? CheckPriceAction { get; set; }
        public long ChildPriceType { get; set; }
        public short[] PensionModebyBooking { get; set; }
        public NewHotel.Contracts.OTA.AdditionalEntryType? MealTypeAdditional { get; set; }
        public long UseReservationMarketSegment { get; set; }
        public long UseReservationMarketOrigin { get; set; }
        public string AllotmentId { get; set; }
        public bool ApplyTaxGuest { get; set; }
        public bool AmountAfterTax { get; set; }
        public BookingVoucherType? BookingVoucherType { get; set; }
        public string ReservationCountry { get; set; }
        public string GuestCountry { get; set; }
        public string EntityCountry { get; set; }
        public string AgencyCountry { get; set; }
        public EBookingTravelAgencyIntegrationType TravelAgencyIntegrationMode { get; set; }
        public EBookingCompanyIntegrationType EntityIntegrationMode { get; set; }
        public EActionWhenPriceError ActionWhenPriceReservationError { get; set; }
        public EActionWhenTotalPriceError ActionWhenReservationTotalPriceError { get; set; }
        public EBookingChannelPaymentsIntegrationType PaymentsIntegrationType { get; set; }
        public bool ReservationSyncPush { get; set; }
        public bool ReservationAsyncPush { get; set; }
        public EActionWhenModifyWithoutInsert? ActionWhenModifyWithoutInsert { get; set; }
        public bool? CreditCardMapingDefaultIfNoExist { get; set; }
        public bool? RoomRateMapingDefaultIfNoExist { get; set; }
        public bool? RoomRateMapingGeneralIfNoExist { get; set; }
        public EConfirmationState? ReservationConfirmationState { get; set; }
        public EAttentionMappingType? AttentionMappingType { get; set; }
        public string ReservationDefaultAttentionCode { get; set; }
        public EBookingGroupIntegrationType? GroupIntegrationType { get; set; }
        public bool? AllowChangeOccupiedRoomTypeWithAssignedRoom { get; set; }
        public bool? AllowPMSOccupiedRoomTypeModification { get; set; }
        public bool? UnassignRoomBookingChannel { get; set; }
        public bool? AllowPMSReservedRoomTypeModification { get; set; }
        public bool? AssignRoomInCheckIn { get; set; }
        public DateTime? ActivationDate { get; set; }
        public ESynchronizationType? SyncType { get; set; }
        public EExpediaExportLOSType? ExpediaExportLOSType { get; set; }
        public EExpediaExportPriceType? ExportPriceType { get; set; }
        public EFastbookingExportType? FastbookingExportType { get; set; }
        public EUseAdditionals? UseAdditionals { get; set; }
        public EOriginMappingType? OriginMappingType { get; set; }
        public ESegmentMappingType? SegmentMappingType { get; set; }
        public EActionWhenNoPricesInformed? ActionWhenNoPricesInformed { get; set; }
        public EActionWhenInvalidTaxInformed? ActionWhenInvalidTaxInformed { get; set; }
        public string[] ServiceSupplements { get; set; }
        /// <summary>
        /// No tratado del lado del PMS, solo en los canales
        /// </summary>
        public EBookingGuestEmailIntegrationType? EmailIntegrationType { get; set; }
        public EActionWhenPMSDataValidationError? ActionWhenPMSDataValidationError { get; set; }
    }
}