﻿using System;

namespace NewHotel.Web.Services
{
    public class BookingAvailabilityRequest : BaseBookingRequest
    {
        public DateTime ArrivalDate;
        public DateTime DepartureDate;
        public string RoomType;
        public short Adults;
        public short Children;
    }
}