﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Web.Services
{

    public class RoomTypesBookingRequest : BaseRequest
    {
        public string RoomTypeId { get; set; }
        public int? Adults { get; set; }
        public int? Children { get; set; }
    }
}
