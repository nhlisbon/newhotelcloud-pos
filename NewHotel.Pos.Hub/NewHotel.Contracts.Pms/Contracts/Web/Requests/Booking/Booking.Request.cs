﻿using System;

namespace NewHotel.Web.Services
{
    public class BookingRequest : BaseBookingRequest
    {
        public DateTime ArrivalDate;
        public DateTime DepartureDate;
        public string RoomType;
        public string PensionBoard;
        public string Currency;
        public decimal Price;
        public string MarketOrigin;
        public string MarketSegment;
        public string GuestFirstName;
        public string GuestLastName;
        public string GuestCountry;
        public string GuestType;
        public string GuestGender;
        public short Adults;
        public short Children;
        public bool TaxIncluded;
        public string TaxSchema;
        public string Company;
        public string Comments;
    }
}