﻿using System;

namespace NewHotel.Web.Services
{
    public class ClientsRequest : BaseRequest
    {
        public DateTime? LastModification;
        public int PageNumber;
        public int RowPerPage;
    }
}