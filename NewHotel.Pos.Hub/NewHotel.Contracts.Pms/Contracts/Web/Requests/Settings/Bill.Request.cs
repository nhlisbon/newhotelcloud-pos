﻿using System;

namespace NewHotel.Web.Services
{
    public class BillRequest : BaseRequest
    {
        public Guid? GuestId;
        public Guid? RoomId;
        public Guid? ReservationId;
    }
}