﻿using System;

namespace NewHotel.Web.Services
{
    public class EntitiesRequest : BaseRequest
    {
        public DateTime? LastModification;
        public int PageNumber;
        public int RowPerPage;
    }
}