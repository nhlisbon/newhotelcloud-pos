﻿using System;

namespace NewHotel.Web.Services
{
    public class ProductionRequest : BaseRequest
    {
        public DateTime Date;
        public int PageNumber;
        public int RowPerPage;
    }
}