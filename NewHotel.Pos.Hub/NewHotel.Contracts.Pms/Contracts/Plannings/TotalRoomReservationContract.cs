﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class TotalRoomReservationContract : BaseContract
    {
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public int RoomInventory { get; set; }
        [DataMember]
        public int Availability { get; set; }
    }
}
