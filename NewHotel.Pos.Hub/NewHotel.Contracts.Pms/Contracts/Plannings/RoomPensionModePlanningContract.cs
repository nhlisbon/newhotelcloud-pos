﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    public class RoomPensionModePlanningContract : PlanningContract
    {
        public string BreakfastTraslate;
        public string LunchTraslate;
        public string DinnerTraslate;
        public TypedList<PlanningBaseRecord> RecordsDetails { get; set; }

        public override void BuildPlanning()
        {
            List<RoomPensionModePlanningRecord> records = Records.Cast<RoomPensionModePlanningRecord>().ToList().OrderBy(x=>x.PensionModeId).ToList();
            //Default Mode
            if (DetailColumns == null || DetailColumns.Count == 0) BuildPlanningInTotalGrouping(records);
           
            AddTotalRow();
            AddPaxsMeals();
        }

        public void AddPaxsMeals ()
        {
            List<string> hotels = new List<string>();

            foreach (PaxsMealsPlanningRecord record in RecordsDetails)
            {
                string key = record.HotelId.ToString();
                string key_breakfast = record.HotelId.ToString() + "&" + "Breakfast";
                string key_lunch = record.HotelId.ToString() + "&" + "Lunch";
                string key_dinner = record.HotelId.ToString() + "&" + "Dinner";

                if (!hotels.Contains(key)) hotels.Add(key);

                #region Breakfast
                if (this.ContainsRow(key_breakfast))
                {
                    PlanningRowContract rowContract = this.GetRowByKey(key_breakfast);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Breakfast.HasValue) ? record.Breakfast.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");
                    rowContract.Id = key_breakfast;
                    rowContract.Title = BreakfastTraslate;

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Breakfast.HasValue) ? record.Breakfast.Value : 0)).ToString();

                    //Insert in correct place
                    int parentIndex = this.GetLastRowIndexByParentKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                } 
                #endregion
                #region Lunch
                if (this.ContainsRow(key_lunch))
                {
                    PlanningRowContract rowContract = this.GetRowByKey(key_lunch);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Lunch.HasValue) ? record.Lunch.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");
                    rowContract.Id = key_lunch;
                    rowContract.Title = LunchTraslate;

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Lunch.HasValue) ? record.Lunch.Value : 0)).ToString();

                    //Insert in correct place
                    int parentIndex = this.GetLastRowIndexByParentKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }
                #endregion
                #region Dinner
                if (this.ContainsRow(key_dinner))
                {
                    PlanningRowContract rowContract = this.GetRowByKey(key_dinner);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Dinner.HasValue) ? record.Dinner.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");
                    rowContract.Id = key_dinner;
                    rowContract.Title = DinnerTraslate;

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Dinner.HasValue) ? record.Dinner.Value : 0)).ToString();

                    //Insert in correct place
                    int parentIndex = this.GetLastRowIndexByParentKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }
                #endregion
            }
            
            Separators = new List<NHPlanningSeparator>();

            if (hotels.Count > 1)
            {
                foreach (string hotel_key in hotels)
                {
                    int index = this.GetLastRowIndexByParentKey(hotel_key);
                    Separators.Add(new NHPlanningSeparator() { RowPosition = index - 2, Color = ARGBColor.Black });
                }
            }
        }
        private void BuildPlanningInTotalGrouping(IList<RoomPensionModePlanningRecord> records)
        {
            Rows = new List<PlanningRowContract>();
            foreach (RoomPensionModePlanningRecord record in records)
            {
                #region Hotel Group
                string key = record.HotelId.ToString();

                if (this.ContainsRow(key))
                {
                    PlanningRowContract rowContract = this.GetRowByKey(key);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.ReseCount.HasValue) ? record.ReseCount.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Title = record.HotelDescription;
                    rowContract.Id = key;
                    rowContract.SortKeys = new List<string>();
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(String.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.ReseCount.HasValue) ? record.ReseCount.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }
                #endregion
                #region Pension Group
                string key_pension = record.HotelId.ToString() + "&" + record.PensionModeId.ToString();
                if (this.ContainsRow(key_pension))
                {
                    PlanningRowContract rowContract = this.GetRowByKey(key_pension);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.ReseCount.HasValue) ? record.ReseCount.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Title = record.PensionModeDescription;
                    rowContract.Id = key_pension;
                    rowContract.SortKeys = new List<string>();
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.PensionModeDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238); ;

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.ReseCount.HasValue) ? record.ReseCount.Value : 0)).ToString();

                    //Insert in correct place
                    int parentIndex = this.GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                } 
                #endregion 
            }
        }
    }
}
