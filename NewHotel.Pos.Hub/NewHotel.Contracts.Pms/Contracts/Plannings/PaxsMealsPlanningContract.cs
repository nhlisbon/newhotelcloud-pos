﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    public class PaxsMealsPlanningContract : PlanningContract
    {
        public string BreakfastTraslate;
        public string LunchTraslate;
        public string DinnerTraslate;

        public override void BuildPlanning()
        {
            List<PaxsMealsPlanningRecord> records = Records.Cast<PaxsMealsPlanningRecord>().ToList();

            //Default Mode
            if (DetailColumns == null || DetailColumns.Count == 0) BuildPlanningInTotalGrouping(records);
        }

        private void BuildPlanningInTotalGrouping(IList<PaxsMealsPlanningRecord> records)
        {
            Rows = new List<PlanningRowContract>();

            foreach (PaxsMealsPlanningRecord record in records)
            {

                if (this.ContainsRow("Breakfast"))
                {
                    PlanningRowContract rowContractB = this.GetRowByKey("Breakfast");
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContractB.Id = "Breakfast";
                    rowContractB.Title = BreakfastTraslate;
                    rowContractB.Content[indexInContext] = (int.Parse(rowContractB.Content[indexInContext]) + ((record.Breakfast.HasValue) ? record.Breakfast.Value : 0)).ToString();
                }
                if (this.ContainsRow("Lunch"))
                {
                    PlanningRowContract rowContractL = this.GetRowByKey("Lunch");
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContractL.Id = "Lunch";
                    rowContractL.Title = LunchTraslate;
                    rowContractL.Content[indexInContext] = (int.Parse(rowContractL.Content[indexInContext]) + ((record.Lunch.HasValue) ? record.Lunch.Value : 0)).ToString();
                }
                if (this.ContainsRow("Dinner"))
                {
                    PlanningRowContract rowContractD = this.GetRowByKey("Dinner");
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContractD.Id = "Dinner";
                    rowContractD.Title = DinnerTraslate;
                    rowContractD.Content[indexInContext] = (int.Parse(rowContractD.Content[indexInContext]) + ((record.Dinner.HasValue) ? record.Dinner.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContractB = new PlanningRowContract();
                    rowContractB.InitializeList((To - From).Days + 1, "0");
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContractB.Id = "Breakfast";
                    rowContractB.Content[indexInContext] = (int.Parse(rowContractB.Content[indexInContext]) + ((record.Breakfast.HasValue) ? record.Breakfast.Value : 0)).ToString();

                    PlanningRowContract rowContractL = new PlanningRowContract();
                    rowContractL.InitializeList((To - From).Days + 1, "0");
                    int indexInContextL = ((DateTime)record.Id - From).Days;
                    rowContractL.Id = "Lunch";
                    rowContractL.Content[indexInContextL] = (int.Parse(rowContractL.Content[indexInContextL]) + ((record.Lunch.HasValue) ? record.Lunch.Value : 0)).ToString();

                    PlanningRowContract rowContractD = new PlanningRowContract();
                    rowContractD.InitializeList((To - From).Days + 1, "0");
                    int indexInContextD = ((DateTime)record.Id - From).Days;
                    rowContractD.Id = "Dinner";
                    rowContractD.Content[indexInContextD] = (int.Parse(rowContractD.Content[indexInContextD]) + ((record.Dinner.HasValue) ? record.Dinner.Value : 0)).ToString();

                    //Styles
                    rowContractB.FontBold = rowContractL.FontBold = rowContractD.FontBold =false;
                    rowContractB.FontItalic = rowContractL.FontItalic = rowContractD.FontItalic = false;
                    rowContractB.LeftMargin = rowContractL.LeftMargin = rowContractD.LeftMargin = 5;
                    rowContractB.CustomBackground = rowContractL.CustomBackground = rowContractD.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238); ;

                    //Values
                    indexInContext = ((DateTime)record.Id - From).Days;
                    rowContractB.Content[indexInContext] = (int.Parse(rowContractB.Content[indexInContext]) + ((record.Breakfast.HasValue) ? record.Breakfast.Value : 0)).ToString();
                    Rows.Add(rowContractB);
                    rowContractL.Content[indexInContext] = (int.Parse(rowContractL.Content[indexInContext]) + ((record.Lunch.HasValue) ? record.Lunch.Value : 0)).ToString();
                    Rows.Add(rowContractL);
                    rowContractD.Content[indexInContext] = (int.Parse(rowContractD.Content[indexInContext]) + ((record.Dinner.HasValue) ? record.Dinner.Value : 0)).ToString();
                    Rows.Add(rowContractD);
                } 
               
            }
        }

    }
}
