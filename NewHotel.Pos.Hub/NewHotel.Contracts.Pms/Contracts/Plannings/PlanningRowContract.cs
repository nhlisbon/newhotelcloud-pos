﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    public class PlanningRowContract : BaseContract
    {
        #region Data's Properties
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public object Id { get; set; }
        [DataMember]
        public List<string> SortKeys { get; set; }
        #endregion
        #region Style's Properties
        [DataMember]
        public bool FontBold { get; set; }
        [DataMember]
        public bool FontItalic { get; set; }
        [DataMember]
        public int LeftMargin { get; set; }
        [DataMember]
        public ARGBColor? CustomBackground { get; set; }
        [DataMember]
        public bool HotelDescriptionRow { get; set; }
        #endregion
        #region Day's Properties
        [DataMember]
        public string[] Content { get; set; }
        #endregion
        #region Public Methods
        public void InitializeList(int length, string value)
        {
            Content = new string[length];
            for (int i = 0; i < Content.Length; i++)
            {
                Content[i] = value;
            }
        }
        public int Total()
        {
            int result = 0;
            foreach (string s in Content)
            {
                int value = 0;
                int.TryParse(s, out value);
                result += value;
            }
            return result;
        }
        #endregion
    }
}
