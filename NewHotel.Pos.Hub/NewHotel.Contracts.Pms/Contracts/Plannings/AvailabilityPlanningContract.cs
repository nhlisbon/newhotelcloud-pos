﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    public class AvailabilityPlanningContract : PlanningContract
    {
        #region Public Properties
        public string RoomsAvailablesTranslation { get; set; }
        public string ExtraBedsTranslation { get; set; }
        public string CotsTranslation { get; set; }
        public string ReservedTranslation { get; set; }
        public string InWaitingListTranslation { get; set; }
        public string CancelledTranslation { get; set; }
        public string NoShowTranslation { get; set; }
        public string OverbookedTranslation { get; set; }
        public string AttemptsTranslation { get; set; }
        public string AllotmentsTranslation { get; set; }
        public string UsedTranslation { get; set; }
        public string RoomInventoryTranslation { get; set; }
        public string ActiveTranslation { get; set; }
        public string OutOfOrderTranslation { get; set; }

        public bool Allotments { get; set; }
        public bool Attempts { get; set; }
        public bool Summary { get; set; }
        public bool WaitingList { get; set; }
        public bool Extras { get; set; }
        public bool Reservations { get; set; }

        public bool IsMultiHotel { get; set; }
        #endregion
        #region Lists
        public List<AvailabilityRoomTypeRecord> ListRoomType { get; set; }
        public List<AvailabilityExtrasRecord> ListExtras { get; set; }
        public List<AvailabilityReservationsRecord> ListReservations { get; set; }
        public List<AvailabilityAllotmentRecord> ListAllotments { get; set; }
        public List<AvailabilityInventoryRecord> ListInventory { get; set; }
        #endregion
        #region Constructor
        public AvailabilityPlanningContract()
            : base()
        {
            ListRoomType = new List<AvailabilityRoomTypeRecord>();
            ListExtras = new List<AvailabilityExtrasRecord>();
            ListReservations = new List<AvailabilityReservationsRecord>();
            ListAllotments = new List<AvailabilityAllotmentRecord>();
            ListInventory = new List<AvailabilityInventoryRecord>();
        }
        #endregion
        public override void BuildPlanning()
        {
            Rows = new List<PlanningRowContract>();
            Separators = new List<NHPlanningSeparator>();
            string extraBedFirstItemKey = String.Empty;

            #region Extras
            if (Extras && ListExtras.Count > 0)
            {
                foreach (AvailabilityExtrasRecord record in ListExtras)
                {
                    #region Hotel Group
                    string key = record.HotelId.ToString();

                    if (ContainsRow(key))
                    {
                        var rowContract = GetRowByKey(key);
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalIncidental.HasValue) ? record.TotalIncidental.Value : 0)).ToString();
                    }
                    else
                    {
                        var rowContract = new PlanningRowContract();
                        rowContract.InitializeList((To - From).Days + 1, "0");

                        //Details
                        rowContract.Title = record.HotelDescription;
                        rowContract.Id = key;
                        rowContract.SortKeys = new List<string>();
                        rowContract.SortKeys.Add(record.HotelDescription);

                        //Styles
                        rowContract.HotelDescriptionRow = true;

                        //Values
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalIncidental.HasValue) ? record.TotalIncidental.Value : 0)).ToString();
                        Rows.Add(rowContract);
                    }
                    #endregion
                    #region Extras Group
                    string key_extraType = record.HotelId.ToString() + "&" + record.IncidentalId.ToString();
                    if (this.ContainsRow(key_extraType))
                    {
                        PlanningRowContract rowContract = this.GetRowByKey(key_extraType);
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalIncidental.HasValue) ? record.TotalIncidental.Value : 0)).ToString();
                    }
                    else
                    {
                        extraBedFirstItemKey = key_extraType;

                        PlanningRowContract rowContract = new PlanningRowContract();
                        rowContract.InitializeList((To - From).Days + 1, "0");

                        //Details
                        rowContract.Title = record.IncidentalDescription;
                        rowContract.Id = key_extraType;
                        rowContract.SortKeys = new List<string>();
                        rowContract.SortKeys.Add(record.HotelDescription);

                        //Styles
                        rowContract.FontBold = false;
                        rowContract.FontItalic = false;
                        rowContract.LeftMargin = 10;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238); ;

                        //Values
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalIncidental.HasValue) ? record.TotalIncidental.Value : 0)).ToString();

                        //Insert in correct place
                        int parentIndex = this.GetRowIndexByKey(key);
                        if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(parentIndex + 1, rowContract);
                    }
                    #endregion
                }
            } 
            #endregion
            #region RoomTypes
            foreach (AvailabilityRoomTypeRecord record in ListRoomType)
            {
                #region Hotel Group
                string key = record.HotelId.ToString();

                if (this.ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Title = record.HotelDescription;
                    rowContract.Id = key;
                    rowContract.SortKeys = new List<string>();
                    rowContract.SortKeys.Add(record.HotelDescription);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }
                #endregion
                #region RoomType Total
                string key_roomTotal = record.HotelId.ToString() + "&" + RoomsAvailablesTranslation;
                if (this.ContainsRow(key_roomTotal))
                {
                    PlanningRowContract rowContract = this.GetRowByKey(key_roomTotal);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Title = RoomsAvailablesTranslation;
                    rowContract.Id = key_roomTotal;
                    rowContract.SortKeys = new List<string>();
                    rowContract.SortKeys.Add(record.HotelDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238); ;

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    int parentIndex = this.GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }
                #endregion
                #region RoomType Group
                string key_roomType = record.HotelId.ToString() + "&" + record.RoomTypeId.ToString();
                if (this.ContainsRow(key_roomType))
                {
                    PlanningRowContract rowContract = this.GetRowByKey(key_roomType);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Title = record.RoomTypeDescription;
                    rowContract.Id = key_roomType;
                    rowContract.SortKeys = new List<string>();
                    rowContract.SortKeys.Add(record.HotelDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 10;
                    rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238); ;

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    int parentIndex = this.GetRowIndexByKey(key_roomTotal);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }
                #endregion
            }
            #endregion
            #region Reservations
            if (Reservations && ListReservations.Count > 0)
            {
                foreach (var record in ListReservations)
                {
                    string key = record.HotelId.ToString();
                    string key_rese = key + "&" + ReservedTranslation;
                    string key_wait = key + "&" + InWaitingListTranslation;
                    string key_canc = key + "&" + CancelledTranslation;
                    string key_nshw = key + "&" + NoShowTranslation;
                    string key_over = key + "&" + OverbookedTranslation;

                    #region Reservations
                    if (ContainsRow(key_rese))
                    {
                        var rowContract = GetRowByKey(key_rese);
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Reservations).ToString();
                    }
                    else
                    {
                        var rowContract = new PlanningRowContract() { Title = ReservedTranslation, Id = key + "&" + ReservedTranslation, SortKeys = new List<string>(), FontBold = false, FontItalic = false, LeftMargin = 5, CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238) };
                        rowContract.InitializeList((To - From).Days + 1, "0");
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Reservations).ToString();

                        int index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }
                    #endregion
                    #region WaitingList
                    if (WaitingList)
                    {
                        if (ContainsRow(key_wait))
                        {
                            var rowContract = GetRowByKey(key_wait);
                            int indexInContext = ((DateTime)record.Id - From).Days;
                            rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.WaitingList).ToString();
                        }
                        else
                        {
                            var rowContract = new PlanningRowContract() { Title = InWaitingListTranslation, Id = key + "&" + InWaitingListTranslation, SortKeys = new List<string>(), FontBold = false, FontItalic = false, LeftMargin = 10, CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238) };
                            rowContract.InitializeList((To - From).Days + 1, "0");
                            int indexInContext = ((DateTime)record.Id - From).Days;
                            rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.WaitingList).ToString();

                            int index = GetLastRowIndexByParentKey(key);
                            if (index + 1 == Rows.Count) Rows.Add(rowContract);
                            else Rows.Insert(index + 1, rowContract);
                        }
                    }
                    #endregion
                    #region Cancelled
                    if (ContainsRow(key_canc))
                    {
                        var rowContract = GetRowByKey(key_canc);
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Cancelled).ToString();
                    }
                    else
                    {
                        var rowContract = new PlanningRowContract() { Title = CancelledTranslation, Id = key + "&" + CancelledTranslation, SortKeys = new List<string>(), FontBold = false, FontItalic = false, LeftMargin = 5, CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238) };
                        rowContract.InitializeList((To - From).Days + 1, "0");
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Cancelled).ToString();

                        int index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }
                    #endregion
                    #region NoShow
                    if (ContainsRow(key_nshw))
                    {
                        var rowContract = GetRowByKey(key_nshw);
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.NoShows).ToString();
                    }
                    else
                    {
                        var rowContract = new PlanningRowContract() { Title = NoShowTranslation, Id = key + "&" + NoShowTranslation, SortKeys = new List<string>(), FontBold = false, FontItalic = false, LeftMargin = 5, CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238) };
                        rowContract.InitializeList((To - From).Days + 1, "0");
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.NoShows).ToString();

                        int index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }
                    #endregion
                    #region Overbooked
                    if (ContainsRow(key_over))
                    {
                        var rowContract = GetRowByKey(key_over);
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Overbooked).ToString();
                    }
                    else
                    {
                        var rowContract = new PlanningRowContract() { Title = OverbookedTranslation, Id = key + "&" + OverbookedTranslation, SortKeys = new List<string>(), FontBold = false, FontItalic = false, LeftMargin = 5, CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238) };
                        rowContract.InitializeList((To - From).Days + 1, "0");
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Overbooked).ToString();

                        int index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }
                    #endregion
                    #region Attempts

                    if (Attempts)
                    {
                        string key_attempt = key + "&" + AttemptsTranslation;

                        if (ContainsRow(key_attempt))
                        {
                            var rowContract = GetRowByKey(key_attempt);
                            int indexInContext = ((DateTime)record.Id - From).Days;
                            rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Attempts).ToString();
                        }
                        else
                        {
                            var rowContract = new PlanningRowContract() { Title = AttemptsTranslation, Id = key + "&" + AttemptsTranslation, SortKeys = new List<string>(), FontBold = false, FontItalic = false, LeftMargin = 5, CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238) };
                            rowContract.InitializeList((To - From).Days + 1, "0");
                            int indexInContext = ((DateTime)record.Id - From).Days;
                            rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Attempts).ToString();

                            int index = GetLastRowIndexByParentKey(key);
                            if (index + 1 == Rows.Count) Rows.Add(rowContract);
                            else Rows.Insert(index + 1, rowContract);
                        }
                    }

                    #endregion
                }
            }
            #endregion
            #region Allotment
            if (Allotments && ListAllotments.Count > 0)
            {
                foreach (var record in ListAllotments)
                {
                    string key = record.HotelId.ToString();
                    string key_cont = key + "&" + AllotmentsTranslation;
                    string key_used = key + "&" + UsedTranslation;

                    #region Contracted
                    if (this.ContainsRow(key_cont))
                    {
                        var rowContract = GetRowByKey(key_cont);
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Contrated).ToString();
                    }
                    else
                    {
                        var rowContract = new PlanningRowContract() { Title = AllotmentsTranslation, Id = key + "&" + AllotmentsTranslation, SortKeys = new List<string>(), FontBold = false, FontItalic = false, LeftMargin = 5, CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238) };
                        rowContract.InitializeList((To - From).Days + 1, "0");
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Contrated).ToString();

                        int index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }
                    #endregion
                    #region Used
                    if (this.ContainsRow(key_used))
                    {
                        var rowContract = GetRowByKey(key_used);
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Used).ToString();
                    }
                    else
                    {
                        var rowContract = new PlanningRowContract() { Title = UsedTranslation, Id = key + "&" + UsedTranslation, SortKeys = new List<string>(), FontBold = false, FontItalic = false, LeftMargin = 5, CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238) };
                        rowContract.InitializeList((To - From).Days + 1, "0");
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Used).ToString();

                        int index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }
                    #endregion
                }
            }
            #endregion
            #region Inventory
            if (Summary && ListInventory.Count > 0)
            {
                foreach (var record in ListInventory)
                {
                    string key = record.HotelId.ToString();
                    string key_inve = key + "&" + RoomInventoryTranslation;
                    string key_avai = key + "&" + ActiveTranslation;
                    string key_inac = key + "&" + OutOfOrderTranslation;

                    #region Contracted
                    if (this.ContainsRow(key_inve))
                    {
                        var rowContract = GetRowByKey(key_inve);
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Inventory).ToString();
                    }
                    else
                    {
                        var rowContract = new PlanningRowContract() { Title = RoomInventoryTranslation, Id = key + "&" + RoomInventoryTranslation, SortKeys = new List<string>(), FontBold = false, FontItalic = false, LeftMargin = 5, CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238) };
                        rowContract.InitializeList((To - From).Days + 1, "0");
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Inventory).ToString();

                        int index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }
                    #endregion
                    #region Used
                    if (this.ContainsRow(key_avai))
                    {
                        var rowContract = GetRowByKey(key_avai);
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Active).ToString();
                    }
                    else
                    {
                        var rowContract = new PlanningRowContract() { Title = ActiveTranslation, Id = key + "&" + ActiveTranslation, SortKeys = new List<string>(), FontBold = false, FontItalic = false, LeftMargin = 10, CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238) };
                        rowContract.InitializeList((To - From).Days + 1, "0");
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Active).ToString();

                        int index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }
                    #endregion
                    #region Inactive
                    if (this.ContainsRow(key_inac))
                    {
                        var rowContract = GetRowByKey(key_inac);
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Inactive).ToString();
                    }
                    else
                    {
                        var rowContract = new PlanningRowContract() { Title = OutOfOrderTranslation, Id = key + "&" + OutOfOrderTranslation, SortKeys = new List<string>(), FontBold = false, FontItalic = false, LeftMargin = 10, CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238) };
                        rowContract.InitializeList((To - From).Days + 1, "0");
                        int indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Inactive).ToString();

                        int index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }
                    #endregion
                }
            }
            #endregion

            for (int i = 0; i < Rows.Count; i++)
            {
                var rowContract = Rows[i];
                if (rowContract.Id.ToString().IndexOf(ReservedTranslation) != -1) Separators.Add(new NHPlanningSeparator() { RowPosition = (IsMultiHotel) ? i : i - 1, Color = ARGBColor.Black });
                if (rowContract.Id.ToString().IndexOf(AttemptsTranslation) != -1) Separators.Add(new NHPlanningSeparator() { RowPosition = (IsMultiHotel) ? i : i - 1, Color = ARGBColor.Black });
                if (rowContract.Id.ToString().IndexOf(AllotmentsTranslation) != -1) Separators.Add(new NHPlanningSeparator() { RowPosition = (IsMultiHotel) ? i : i - 1, Color = ARGBColor.Black });
                if (rowContract.Id.ToString().IndexOf(RoomInventoryTranslation) != -1) Separators.Add(new NHPlanningSeparator() { RowPosition = (IsMultiHotel) ? i : i - 1, Color = ARGBColor.Black });
            }

            if (ListRoomType.Count > 0 && !String.IsNullOrEmpty(extraBedFirstItemKey))
            {
                int index = GetRowIndexByKey(extraBedFirstItemKey);
                Separators.Add(new NHPlanningSeparator() { RowPosition = (IsMultiHotel) ? index : index - 1, Color = ARGBColor.Black });
            }
        }
    }
}
