﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    public abstract class PlanningContract : BaseContract
    {
        #region Members

        private const int RecordCount = 50;
        private List<string> _detailsColumns;

        #endregion
        #region Constructor

        public PlanningContract()
        {
            // Set Colors

            BackgroundColorFriday = ARGBColor.White;
            BackgroundColorMonday = ARGBColor.White;
            BackgroundColorSaturday = ARGBColor.Yellow;
            BackgroundColorSunday = ARGBColor.Yellow;
            BackgroundColorThursday = ARGBColor.White;
            BackgroundColorTuesday = ARGBColor.White;
            BackgroundColorWednesday = ARGBColor.White;

            //BackgroundColor = ARGBColor.White;
            //WeekendColor = ARGBColor.Yellow;
            WorkDaydColor = ARGBColor.Cyan;
            NegativeColor = ARGBColor.LightPink;
            PositiveColor = ARGBColor.LightGreen;
            TotalColor = ARGBColor.LightGreen;

            SpecialDays = new Dictionary<DateTime, ValuePair<ARGBColor, string>>();
        }

        #endregion
        #region Local Properties

        public TypedList<PlanningBaseRecord> Records { get; set; }

        public List<string> DetailColumns
        {
            get
            {
                if (_detailsColumns == null)
                    _detailsColumns = new List<string>();

                return _detailsColumns;
            }
            set
            {
                _detailsColumns = value;
            }
        }

        #endregion
        #region Public Properties

        [DataMember]
        public List<PlanningRowContract> Rows { get; set; }
        [DataMember]
        public List<NHPlanningSeparator> Separators { get; set; }
        [DataMember]
        public short? DaysShowedBeforeWorkingDateInPlannings { get; set; }
        [DataMember]
        public DateTime From { get; set; }
        [DataMember]
        public DateTime To { get; set; }
        [DataMember]
        public DateTime WorkDate { get; set; }
        [DataMember]
        public int CurrentPage { get; set; }
        [DataMember]
        public int TotalPages { get; set; }
        [DataMember]
        public bool Multiproperty { get; set; }
        [DataMember]
        public IDictionary<DateTime, ValuePair<ARGBColor, string>> SpecialDays { get; set; }

        // Colors
        //[DataMember]
        //public ARGBColor BackgroundColor { get; set; }
        //[DataMember]
        //public ARGBColor WeekendColor { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorMonday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorTuesday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorWednesday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorThursday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorFriday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorSaturday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorSunday { get; set; }



        [DataMember]
        public ARGBColor WorkDaydColor { get; set; }
        [DataMember]
        public ARGBColor NegativeColor { get; set; }
        [DataMember]
        public ARGBColor PositiveColor { get; set; }
        [DataMember]
        public ARGBColor TotalColor { get; set; }

        #endregion
        #region Methods

        public abstract void BuildPlanning();

        public void SortPlanning()
        {
            if (Rows.Count != 0)
            {
                var contract = Rows[0];
                if (contract.SortKeys.Count > 0)
                {
                    if (contract.SortKeys.Count == 1) SortPlanningOneColumn();
                    if (contract.SortKeys.Count == 2) SortPlanningTwoColumn();
                    if (contract.SortKeys.Count == 3) SortPlanningThreeColumn();
                    if (contract.SortKeys.Count == 4) SortPlanningFourColumn();
                }
            }
        }

        public void AddTotalRow()
        {
            if (Rows.Count != 0)
            {
                var totalRow = new PlanningRowContract();
                totalRow.InitializeList((To - From).Days + 1, "0");

                // Details
                totalRow.Title = "TOTAL";
                totalRow.Id = Guid.NewGuid().ToString("N");
                totalRow.SortKeys = new List<string>();

                // Styles
                totalRow.FontBold = true;
                totalRow.FontItalic = false;
                totalRow.LeftMargin = 5;
                totalRow.CustomBackground = null;

                // Values
                foreach (PlanningRowContract row in Rows)
                {
                    for (int i = 0; i < row.Content.Length; i++)
                    {
                        if (!row.FontBold && !row.FontItalic && !row.HotelDescriptionRow)
                            totalRow.Content[i] = (int.Parse(totalRow.Content[i]) + int.Parse(row.Content[i])).ToString();
                    }
                }
                
                Rows.Add(totalRow);
            }
        }

        public bool ContainsRow(object id)
        {
            foreach (PlanningRowContract rowContract in Rows)
            {
                if (rowContract.Id.Equals(id))
                    return true;
            }

            return false;
        }

        public PlanningRowContract GetRowByKey(object id)
        {
            foreach (PlanningRowContract rowContract in Rows)
            {
                if (rowContract.Id.Equals(id))
                    return rowContract;
            }

            return null;
        }

        public int GetRowIndexByKey(object id)
        {
            for (int i = 0; i < Rows.Count; i++)
            {
                if (Rows[i].Id.Equals(id))
                    return i;
            }

            return -1;
        }

        public int GetLastRowIndexByParentKey(object id)
        {
            var firstMatch = false;
            for (int i = 0; i < Rows.Count; i++)
            {
                if (Rows[i].Id.ToString().Contains(id.ToString()))
                    firstMatch = true;

                if (firstMatch && !Rows[i].Id.ToString().Contains(id.ToString()))
                    return i - 1;
            }

            if (firstMatch)
                return Rows.Count - 1;

            return -1;
        }

        #endregion
        #region Paging Methods

        //Gets the number of groups
        private int GetGroupNumber()
        {
            int result = 0;
            foreach (PlanningRowContract row in Rows)
            {
                if (!row.Id.ToString().Contains("&"))
                    result++;
            }

            return result;
        }

        //Get the average size of all groups
        private int GetGroupSize()
        {
            var groups = new List<int>();
            var firstItem = true;
            int groupCount = 0;

            foreach (PlanningRowContract row in Rows)
            {
                if (!row.Id.ToString().Contains("&"))
                {
                    if (firstItem)
                        firstItem = false;
                    else
                    {
                        groups.Add(groupCount);
                        groupCount = 0;
                    }
                }
                else
                    groupCount++;
            }

            int sum = 0;
            foreach (int value in groups) { sum += value; }
            if (sum == 0) return 0;
            double average = sum / groups.Count;
            int result = int.Parse(Math.Round(average, 0).ToString());

            return result;
        }

        //Gets the number of groups per page, with an average of 50 items per request
        private int GetGroupPerPage()
        {
            int groupSize = GetGroupSize();
            if (groupSize == 0) return RecordCount;
            double groupAmount = RecordCount / groupSize;
            int result = int.Parse(Math.Round(groupAmount, 0).ToString());
            if (result == 0) return Rows.Count;

            return result;
        }

        //Gets the number of pages
        public int PageNumber()
        {
            var groupNumber = GetGroupNumber();
            var groupPerPage = GetGroupPerPage();
            if (groupNumber == 0 || groupNumber < groupPerPage)
                return 1;

            if (groupNumber % groupPerPage == 0)
                return (int)(groupNumber / groupPerPage);
            else
            {
                decimal division = groupNumber / groupPerPage;
                return decimal.ToInt32(Math.Round(division, 0)) + 1;
            }
        }

        // Leave in rows the selected groups, according to provided page
        public void CropRows()
        {
            var groupPerPage = GetGroupPerPage();
            var page = CurrentPage;

            //index of first group to look for
            int groupIndex = (page - 1) * groupPerPage;

            //if index is greather than available groups return
            if (groupIndex > GetGroupNumber())
                return;

            //move looking for first group
            int currentIndex = -1;
            int groupsInserted = 0;
            bool insertMode = false;
            var resultRows = new List<PlanningRowContract>();
            foreach (var row in Rows)
            {
                //increment index if is group
                if (!row.Id.ToString().Contains("&")) currentIndex++;
                //if reach the goal index, start inserting
                if (currentIndex == groupIndex) insertMode = true;
                if (insertMode)
                {
                    //increment number of groups inserted
                    if (!row.Id.ToString().Contains("&")) groupsInserted++;
                    //if current group index is greather than page, end
                    if (groupsInserted > groupPerPage) break;
                    //else add row
                    else resultRows.Add(row);
                }
            }

            Rows.Clear();
            foreach (var row in resultRows)
                Rows.Add(row);
        }

        #endregion
        #region Private Methods

        private void SortPlanningOneColumn()
        {
            var sorted = from PlanningRowContract rowContract in Rows
                         orderby rowContract.SortKeys[0] ascending
                         select rowContract;
            Rows = sorted.ToList();
        }

        private void SortPlanningTwoColumn()
        {
            var sorted = from PlanningRowContract rowContract in Rows
                         orderby rowContract.SortKeys[0] ascending, rowContract.SortKeys[1] ascending
                         select rowContract;
            Rows = sorted.ToList();
        }

        private void SortPlanningThreeColumn()
        {
            var sorted = from PlanningRowContract rowContract in Rows
                         orderby rowContract.SortKeys[0] ascending, rowContract.SortKeys[1] ascending, rowContract.SortKeys[2] ascending
                         select rowContract;
            Rows = sorted.ToList();
        }

        private void SortPlanningFourColumn()
        {
            var sorted = from PlanningRowContract rowContract in Rows
                         orderby rowContract.SortKeys[0] ascending, rowContract.SortKeys[1] ascending, rowContract.SortKeys[2] ascending, rowContract.SortKeys[3] ascending
                         select rowContract;
            Rows = sorted.ToList();
        }

        #endregion
    }

    public class NHPlanningSeparator
    {
        public int RowPosition { get; set; }
        public ARGBColor Color { get; set; }
    }
}
