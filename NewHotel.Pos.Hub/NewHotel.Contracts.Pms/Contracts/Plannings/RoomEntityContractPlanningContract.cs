﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    public class RoomEntityContractPlanningContract : PlanningContract
    {
        public override void BuildPlanning()
        {
            //Details possible Columns 
            //("OPER_PK", Level = 1)
            //("TREC_PK", Level = 2)
            List<RoomEntityContractPlanningRecord> records = Records.Cast<RoomEntityContractPlanningRecord>().ToList();

            //Default Mode
            if (DetailColumns == null || DetailColumns.Count == 0) BuildPlanningInTotalGrouping(records);
            //Entity & Contract
            else if (DetailColumns.Count == 1 && DetailColumns.Contains("OPER_PK")) BuildPlanningInOperGrouping(records);
            //Entity & Room
            else if (DetailColumns.Count == 1 && DetailColumns.Contains("TREC_PK")) BuildPlanningInRoomGrouping(records);
            //Entity, Contract & Room
            else if (DetailColumns.Count == 2 && DetailColumns.Contains("OPER_PK") && DetailColumns.Contains("TREC_PK")) BuildPlanningInFullDetails(records);

            SortPlanning();
            AddTotalRow();
        }

        private void BuildPlanningInTotalGrouping(IList<RoomEntityContractPlanningRecord> records)
        {
            Rows = new List<PlanningRowContract>();

            foreach (RoomEntityContractPlanningRecord record in records)
            {
                #region Hotel Group
                string key = record.HotelId.ToString();

                if (this.ContainsRow(key))
                {
                    PlanningRowContract rowContract = this.GetRowByKey(key);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Title = record.HotelDescription;
                    rowContract.Id = key;
                    rowContract.SortKeys = new List<string>();
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(String.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }
                #endregion
                #region Entity Group
                string key_entity = record.HotelId.ToString() + "&" + record.EntityId.ToString();

                if (this.ContainsRow(key_entity))
                {
                    PlanningRowContract rowContract = this.GetRowByKey(key_entity);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Title = record.EntityDescription;
                    rowContract.Id = key_entity;
                    rowContract.SortKeys = new List<string>();
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.EntityDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = null;

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    int parentIndex = this.GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                    
                } 
                #endregion
            }
        }
        private void BuildPlanningInOperGrouping(IList<RoomEntityContractPlanningRecord> records)
        {
            Rows = new List<PlanningRowContract>();

            foreach (RoomEntityContractPlanningRecord record in records)
            {
                #region Hotel Group
                string key = record.HotelId.ToString();

                if (this.ContainsRow(key))
                {
                    PlanningRowContract rowContract = this.GetRowByKey(key);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Title = record.HotelDescription;
                    rowContract.Id = key;
                    rowContract.SortKeys = new List<string>();
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(String.Empty);
                    rowContract.SortKeys.Add(String.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }
                #endregion
                #region Entity Group
                string key_entity = record.HotelId.ToString() + "&" + record.EntityId.ToString();
                if (this.ContainsRow(key_entity))
                {
                    PlanningRowContract rowContract = this.GetRowByKey(key_entity);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Title = record.EntityDescription;
                    rowContract.Id = key_entity;
                    rowContract.SortKeys = new List<string>();
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.EntityDescription);
                    rowContract.SortKeys.Add(String.Empty);

                    //Styles
                    rowContract.FontBold = true;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238); ;

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    int parentIndex = this.GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                } 
                #endregion
                #region Contract Group
                string key_oper = record.HotelId.ToString() + "&" + record.EntityId.ToString() + "&" + record.ContractId.ToString();
                if (this.ContainsRow(key_oper))
                {
                    PlanningRowContract rowContract = this.GetRowByKey(key_oper);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Title = record.ContractDescription;
                    rowContract.Id = key_oper;
                    rowContract.SortKeys = new List<string>();
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.EntityDescription);
                    rowContract.SortKeys.Add(record.ContractDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 10;
                    rowContract.CustomBackground = null;

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    int parentIndex = this.GetRowIndexByKey(key_entity);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                } 
                #endregion
            }
        }
        private void BuildPlanningInRoomGrouping(IList<RoomEntityContractPlanningRecord> records)
        {
            Rows = new List<PlanningRowContract>(); ;

            foreach (RoomEntityContractPlanningRecord record in records)
            {
                #region Hotel Group
                string key = record.HotelId.ToString();

                if (this.ContainsRow(key))
                {
                    PlanningRowContract rowContract = this.GetRowByKey(key);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Title = record.HotelDescription;
                    rowContract.Id = key;
                    rowContract.SortKeys = new List<string>();
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(String.Empty);
                    rowContract.SortKeys.Add(String.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }
                #endregion
                #region Entity Group
                string key_entity = record.HotelId.ToString() + "&" + record.EntityId.ToString();
                if (this.ContainsRow(key_entity))
                {
                    PlanningRowContract rowContract = this.GetRowByKey(key_entity);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Title = record.EntityDescription;
                    rowContract.Id = key_entity;
                    rowContract.SortKeys = new List<string>();
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.EntityDescription);
                    rowContract.SortKeys.Add(String.Empty);

                    //Styles
                    rowContract.FontBold = true;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238); ;

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    int parentIndex = this.GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }
                #endregion
                #region Room Group
                string key_room = record.HotelId.ToString() + "&" + record.EntityId.ToString() + "&" + record.RoomTypeId.ToString();
                if (this.ContainsRow(key_room))
                {
                    PlanningRowContract rowContract = this.GetRowByKey(key_room);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Title = record.RoomTypeDescription;
                    rowContract.Id = key_room;
                    rowContract.SortKeys = new List<string>();
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.EntityDescription);
                    rowContract.SortKeys.Add(record.RoomTypeDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 10;
                    rowContract.CustomBackground = null;

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    int parentIndex = this.GetRowIndexByKey(key_entity);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }
                #endregion
            }
        }
        private void BuildPlanningInFullDetails(IList<RoomEntityContractPlanningRecord> records)
        {
            Rows = new List<PlanningRowContract>(); ;

            foreach (RoomEntityContractPlanningRecord record in records)
            {
                #region Hotel Group
                string key = record.HotelId.ToString();

                if (this.ContainsRow(key))
                {
                    PlanningRowContract rowContract = this.GetRowByKey(key);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Title = record.HotelDescription;
                    rowContract.Id = key;
                    rowContract.SortKeys = new List<string>();
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(String.Empty);
                    rowContract.SortKeys.Add(String.Empty);
                    rowContract.SortKeys.Add(String.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }
                #endregion
                #region Entity Group
                string key_entity = record.HotelId.ToString() + "&" + record.EntityId.ToString();
                if (this.ContainsRow(key_entity))
                {
                    PlanningRowContract rowContract = this.GetRowByKey(key_entity);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Title = record.EntityDescription;
                    rowContract.Id = key_entity;
                    rowContract.SortKeys = new List<string>();
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.EntityDescription);
                    rowContract.SortKeys.Add(String.Empty);
                    rowContract.SortKeys.Add(String.Empty);

                    //Styles
                    rowContract.FontBold = true;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.LightGray;

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    int parentIndex = this.GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }
                #endregion
                #region Contract Group
                string key_oper = record.HotelId.ToString() + "&" + record.EntityId.ToString() + "&" + record.ContractId.ToString();
                if (this.ContainsRow(key_oper))
                {
                    PlanningRowContract rowContract = this.GetRowByKey(key_oper);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Title = record.ContractDescription;
                    rowContract.Id = key_oper;
                    rowContract.SortKeys = new List<string>();
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.EntityDescription);
                    rowContract.SortKeys.Add(record.ContractDescription);
                    rowContract.SortKeys.Add(String.Empty);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = true;
                    rowContract.LeftMargin = 10;
                    rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    int parentIndex = this.GetRowIndexByKey(key_entity);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }
                #endregion
                #region Room Group
                string key_room = record.HotelId.ToString() + "&" + record.EntityId.ToString() + "&" + record.ContractId.ToString() + "&" + record.RoomTypeId.ToString();
                if (this.ContainsRow(key_room))
                {
                    PlanningRowContract rowContract = this.GetRowByKey(key_room);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    PlanningRowContract rowContract = new PlanningRowContract();
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Title = record.RoomTypeDescription;
                    rowContract.Id = key_room;
                    rowContract.SortKeys = new List<string>();
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.EntityDescription);
                    rowContract.SortKeys.Add(record.ContractDescription);
                    rowContract.SortKeys.Add(record.RoomTypeDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 15;
                    rowContract.CustomBackground = null;

                    //Values
                    int indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    int parentIndex = this.GetRowIndexByKey(key_oper);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }
                #endregion
            }
        }

    }
}
