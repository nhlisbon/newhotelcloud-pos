﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class AvailabilityRequest : BaseContract
    {
        [DataMember]
        public DateTime FromDate { get; internal set; }
        [DataMember]
        public DateTime ToDate { get; set; }
        [DataMember]
        public Guid ResourceTypeId { get; internal set; }
        [DataMember]
        public int ResourceTypeCount { get; internal set; }

        public AvailabilityRequest(DateTime fromDate, DateTime toDate,
            Guid resourceTypeId, int resourceTypeCount)
        {
            FromDate = fromDate;
            ToDate = toDate;
            ResourceTypeId = resourceTypeId;
            ResourceTypeCount = resourceTypeCount;
        }

        public AvailabilityRequest() : base() { }
    }
}
