﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class AvailabilityInfoContract : BaseContract
    {
        #region Properties

        [DataMember]
        public DateTime AvailabilityDate { get; set; }
        [DataMember]
        public string ResourceTypeAbrev { get; set; }
        [DataMember]
        public long TotalRooms { get; set; }
        [DataMember]
        public long InactiveRooms { get; set; }
        [DataMember]
        public long OccupedRooms { get; set; }
        [DataMember]
        public ResourceOccupationControlType CriticalOcupationType { get; set; }
        [ReflectionExclude]
        public string CriticalOcupationTypeTranslation { get; set; }
        [DataMember]
        public decimal CriticalOcupation { get; set; }
        [DataMember]
        public bool InsertionLock { get; set; }

        public long ActiveRooms
        {
            get { return (TotalRooms - InactiveRooms); }
        }
        public long AvailableRooms
        {
            get { return (ActiveRooms - OccupedRooms); }
        }
        public decimal OccupationPercent
        {
            get 
            {
                if (CriticalOcupationType == ResourceOccupationControlType.MaxReservation)
                    return (CriticalOcupationRooms ?? 0) == 0 ? 0 : Math.Round(OccupedRooms * 100.0M / CriticalOcupationRooms.Value, 2);
                else
                    return ActiveRooms == 0 ? 0 : Math.Round(OccupedRooms * 100.0M / ActiveRooms, 2); 
            }
        }
        public long? CriticalOcupationRooms
        {
            get { return CriticalOcupationType == ResourceOccupationControlType.MaxReservation ? new Nullable<long>(decimal.ToInt64(CriticalOcupation)) : null; }
        }
        public decimal? CriticalOcupationPercent
        {
            get { return (CriticalOcupationType == ResourceOccupationControlType.CriticalOccupation ? new Nullable<decimal>(CriticalOcupation) : null); }
        }

        [ReflectionExclude]
        public bool HasCriticalOccupation
        {
            get
            {
                switch (CriticalOcupationType)
                {
                    case ResourceOccupationControlType.CriticalOccupation:
                        if (OccupationPercent > CriticalOcupationPercent)
                            return true;
                        break;
                    case ResourceOccupationControlType.MaxReservation:
                        if (OccupedRooms > CriticalOcupationRooms)
                            return true;
                        break;
                }

                return false;
            }
        }

        #endregion
        #region Constructors

        public AvailabilityInfoContract()
            : base()
        {
            ResourceTypeAbrev = string.Empty;
        }

        #endregion
    }
}
