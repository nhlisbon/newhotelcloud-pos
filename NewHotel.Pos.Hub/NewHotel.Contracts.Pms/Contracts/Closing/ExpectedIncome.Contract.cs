﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ExpectedIncomeContract : BaseContract
    {
        #region Properties

        [DataMember]
        public string ReservationNum { get; internal set; }
        [DataMember]
        public string ReservationType { get; internal set; }
        [DataMember]
        public string RoomNum { get; internal set; }
        [DataMember]
        public DateTime ArrivalDate { get; internal set; }
        [DataMember]
        public DateTime DepartureDate { get; internal set; }
        [DataMember]
        public string ReservationState { get; internal set; }
        [DataMember]
        public string Holder { get; internal set; }
        [DataMember]
        public string RoomTypeDesc { get; internal set; }
        [DataMember]
        public string CompanyName { get; internal set; }
        [DataMember]
        public bool Group { get; internal set; }
        [DataMember]
        public string PensionMode { get; internal set; }
        [DataMember]
        public short Paxs { get; internal set; }
        [DataMember]
        public decimal? DiscountPercent { get; internal set; }
        [DataMember]
        public decimal DailyValue { get; internal set; }

        #endregion
        #region Constructors

        public ExpectedIncomeContract(string reservationNum, string reservationType, string roomNum,
            DateTime arrivalDate, DateTime departureDate, string reservationState, string holder,
            string roomTypeDesc, string companyName, bool group, string pensionMode,
            short paxs, decimal dailyValue, decimal? discountPercent)
            : base()
        {
            ReservationNum = reservationNum;
            ReservationType = reservationType;
            RoomNum = roomNum;
            ArrivalDate = arrivalDate;
            DepartureDate = departureDate;
            ReservationState = reservationState;
            Holder = holder;
            RoomTypeDesc = roomTypeDesc;
            CompanyName = companyName;
            Group = group;
            PensionMode = pensionMode;
            Paxs = paxs;
            DailyValue = dailyValue;
            DiscountPercent = discountPercent;
        }

        #endregion
    }
}
