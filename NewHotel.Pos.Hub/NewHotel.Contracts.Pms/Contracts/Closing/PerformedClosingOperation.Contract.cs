﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PerformedClosingOperationContract : BaseContract
    {
        #region Properties

        [DataMember]
        public long ClosingOperationId { get; internal set; }
        [DataMember]
        public string ClosingOperationName { get; internal set; }
        [DataMember]
        public bool Completed { get; set; }
        [DataMember]
        public bool Successfull { get; set; }

        #endregion
        #region Constructors

        public PerformedClosingOperationContract(long closingOperationId, string closingOperationName)
            : base()
        {
            ClosingOperationId = closingOperationId;
            ClosingOperationName = closingOperationName;
        }

        #endregion
    }
}
