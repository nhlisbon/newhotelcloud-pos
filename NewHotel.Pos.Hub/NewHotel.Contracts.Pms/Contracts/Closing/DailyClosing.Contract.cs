﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class DailyClosingContract : BaseContract
    {
        #region Properties

        [DataMember]
        public DateTime ClosingDate { get; set; }
        [DataMember]
        public DateTime RegistrationDate { get; internal set; }
        [DataMember]
        public DateTime RegistrationTime { get; internal set; }
        [DataMember]
        public string UserName { get; internal set; }
        [DataMember]
        public bool Completed { get; set; }
        [DataMember]
        public bool Successfull { get; set; }
        [DataMember]
        public bool Confirmed { get; set; }
        [DataMember]
        public bool Statistics { get; set; }

        [DataMember]
        public bool ExpDailyClosing { get; set; }
        [DataMember]
        public short? NextCode { get; set; }
        [DataMember]
        public ExportTypeOfficialEntity? ExportType { get; set; }
        [DataMember]
        public string FiscalNumerHotel { get; set; }
        [DataMember]
        public string EntityOfficialCode { get; set; }
        [ReflectionExclude]
        public string FileNameExpOff
        {
            get
            {
                string fileName =  string.Empty;
                if (ExpDailyClosing)
                {
                    if (ExportType == ExportTypeOfficialEntity.HotelNumero)
                        fileName = string.Format("{0}{1}.txt", EntityOfficialCode, NextCode??1);
                    else
                        fileName = string.Format("{0}{1}{2}.Dat", FiscalNumerHotel, EntityOfficialCode, NextCode??1);
                }
                return fileName;
            }
        }

        [ReflectionExclude]
        public bool UseWebServiceSef => ExportType == ExportTypeOfficialEntity.WebService;

        #endregion
        #region Contracts

        [ReflectionExclude]
        [DataMember]
        public TypedList<PerformedClosingOperationContract> Operations { get; internal set; }

        [DataMember]
        public TypedList<ConfirmationNightAuditorReportsContract> Reports { get; set; }

        [DataMember]
        public bool ExportReports { get; set; } = false;

        #endregion
        #region Constructors

        public DailyClosingContract(DateTime date, DateTime registrationTime,
            string userName, PerformedClosingOperationContract[] operations) : base() 
        {
            ClosingDate = date;
            UserName = userName;
            RegistrationTime = registrationTime;
            RegistrationDate = RegistrationTime.Date;
            Operations = new TypedList<PerformedClosingOperationContract>(operations);
            Reports = new TypedList<ConfirmationNightAuditorReportsContract>();
        }

        #endregion
    }
}
