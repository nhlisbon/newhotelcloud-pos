﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class FiscalDocContract : DocumentContract
	{
		#region Constants

		public const string FinalCustomerFiscalNumber = "999999990";
		public const string FinalCustomerFiscalName = "Consumidor final";

		#endregion
		#region Members

		private long? _documentNumber;
        private string _documentSerie;
        private decimal _documentValue;
        private decimal _pendingValue;
        private string _holder;
        private string _address;
        private string _fiscalNumber;
        private string _fiscalRegister;
        private short _printerCount;
        private short _printerForeignCount;
        private short _copies;
        private string _comments;
		private Guid? _baseEntityId;
		private bool _finalCustomer;
		private string _baseEntityName;
		private string _baseEntityAddress;
		private string _baseEntityFiscalNumber;
		private string _baseEntityFiscalRegister;
		private bool _applyRetention;
		private Guid? _creditCardRefundId;
	
		[DataMember(Order = 1)]
		internal TypedList<CreditCardRefundContract> _creditCards;

        #endregion
        #region Constructors

        public FiscalDocContract()
            : base()
        {
            DocDetails = new TypedList<FiscalDocDetailsContract>();
			_creditCards = new TypedList<CreditCardRefundContract>();
        }

        #endregion
        #region Properties

        [DataMember]
        public string UserDescription { get; set; }
        [DataMember]
        public Guid? OccupationLineId { get; set; }
        [DataMember]
		public Guid? BaseEntityId
		{
			get { return _baseEntityId; }
			set
			{
				if (Set(ref _baseEntityId, value, "BaseEntityId"))
				{
					NotifyPropertyChanged("CanModifyHolder");
					NotifyPropertyChanged("CanModifyFiscalNumber");
					NotifyPropertyChanged("CanModifyAddress");
				}
			}
		}
        [DataMember]
        public string BaseEntityDescription { get; set; }
        [DataMember]
        public Guid? ContractId { get; set; }
        [DataMember]
        public string ContractDescription { get; set; }
        [DataMember]
        public DateTime EmissionDate { get; set; }
        [DataMember]
        public string DocumentSerie
        {
            get { return _documentSerie; }
            set
            {
                if (Set(ref _documentSerie, value, "DocumentSerie"))
                {
                    NotifyPropertyChanged("SerieName");
                    NotifyPropertyChanged("IsEnabled");
                    NotifyPropertyChanged("CanModifyDetails");
                    NotifyPropertyChanged("CanModifyPayments");
                }
            }
        }
        [DataMember]
        public long? DocumentNumber
        {
            get { return _documentNumber; }
            set
            {
                if (Set(ref _documentNumber, value, "DocumentNumber"))
                {
                    NotifyPropertyChanged("SerieName");
                    NotifyPropertyChanged("IsEnabled");
                    NotifyPropertyChanged("CanModifyDetails");
                    NotifyPropertyChanged("CanModifyPayments");
                }
            }
        }
        [DataMember]
        public decimal DocumentValue
		{ 
			get { return _documentValue; }
			set { Set(ref _documentValue, value, "DocumentValue"); }
		}
        [DataMember]
        public virtual decimal PendingValue { get { return _pendingValue; } set { Set(ref _pendingValue, value, "PendingValue"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Holder required.", AllowEmptyStrings = false)]
        public string Holder { get { return _holder; } set { Set(ref _holder, value, "Holder"); } }
        [DataMember]
        public string Address { get { return _address; } set { Set(ref _address, value, "Address"); } }
        [DataMember]
        public string FiscalNumber 
		{ 
			get { return _fiscalNumber; }
			set
			{
				if (value != null)
					Set(ref _fiscalNumber, value.Trim(), "FiscalNumber");
				else
					Set(ref _fiscalNumber, value, "FiscalNumber");
			}
		}
        [DataMember]
        public string FiscalRegister
		{ 
			get { return _fiscalRegister; }
			set
			{
				if (value != null)
					Set(ref _fiscalRegister, value.Trim(), "FiscalRegister");
				else
					Set(ref _fiscalRegister, value, "FiscalRegister");
			}
		}
        [DataMember]
        public virtual string DocumentCurrencyId { get; set; }
        [DataMember]
        public string DocumentForeignCurrencyId { get; set; }
        [DataMember]
        public decimal? ForeignValue { get; set; }
        [DataMember]
        public decimal? ExchangeValue { get; set; }
        [DataMember]
        public bool DetailedDocument { get; set; }
        [DataMember]
        public short PrinterCount
        {
            get { return _printerCount; }
            set
            {
                if (Set(ref _printerCount, value, "PrinterCount"))
                    NotifyPropertyChanged("PrinterCopyCount");
            }
        }
        [DataMember]
        public short PrinterForeignCount
        {
            get { return _printerForeignCount; }
            set
            {
                if (Set(ref _printerForeignCount, value, "PrinterForeignCount"))
                    NotifyPropertyChanged("PrinterCopyCount");
            }
        }
        [DataMember]
        public short? PensionMode { get; set; }
        [DataMember]
        public string RoomNumber { get; set; }
        [DataMember]
        public string ReservationNumber { get; set; }
        [DataMember]
        public Guid? CancellationControlId { get; set; }
        [DataMember]
        public string ExtAccountDescription { get; set; }
        [DataMember]
        public bool VerifyFiscalNumber { get; set; }
        [DataMember]
        public short Copies { get { return _copies; } set { Set(ref _copies, value, "Copies"); } }
        [DataMember]
        public string Comments { get { return _comments; } set { Set(ref _comments, value, "Comments"); } }
        [DataMember]
        public long ApplicationId { get; set; }
		[DataMember]
		public DocumentSign? SignatureType { get; set; }
		[DataMember]
		public bool FinalCustomer
		{ 
			get { return _finalCustomer; }
			set
			{
				if (Set(ref _finalCustomer, value, "FinalCustomer"))
				{
					NotifyPropertyChanged("CanModifyHolder");
					NotifyPropertyChanged("CanModifyFiscalNumber");
					NotifyPropertyChanged("CanModifyAddress");
				}

				if (!_finalCustomer && _baseEntityId.HasValue)
				{
					if (_baseEntityName != null)
						Holder = _baseEntityName;
					if (_baseEntityFiscalNumber != null)
						FiscalNumber = _baseEntityFiscalNumber;
					if (_baseEntityAddress != null)
						Address = _baseEntityAddress;
				}
				else if (ShowFinalCustomer && _finalCustomer)
				{
					Holder = FinalCustomerFiscalName;
					FiscalNumber = FinalCustomerFiscalNumber;
					Address = string.Empty;
				}
			}
		}
        [DataMember]
        public CancellationControlContract CancellationControl { get; internal set; }

		[DataMember]
		public virtual bool ApplyRetention
		{
			get { return _applyRetention; }
			set { Set(ref _applyRetention, value, "ApplyRetention"); }
		}

		[DataMember(Order = 2)]
		public virtual Guid? CreditCardRefundId
		{ 
			get { return _creditCardRefundId; }
			set { Set(ref _creditCardRefundId, value, "CreditCardRefundId"); }
		}

        #endregion
        #region Extended Properties

		[ReflectionExclude]
		public virtual bool ShowRefundCreditCards
		{
			get { return false; }
		}

		[ReflectionExclude]
		public bool ShowFinalCustomer
		{
			get { return SignatureType.HasValue && SignatureType.Value == DocumentSign.FiscalizationPortugal; }
		}

        [ReflectionExclude]
        public bool CancellationControlVisibility
        {
            get { return CancellationControlId.HasValue; }
        }

        [ReflectionExclude]
        public bool IsEnabled
        {
            get { return !DocumentNumber.HasValue; }
        }

        [ReflectionExclude]
        public short PrinterCopyCount
        {
            get { return string.IsNullOrEmpty(DocumentForeignCurrencyId) ? PrinterCount : PrinterForeignCount; }
        }

        [ReflectionExclude]
        public string Currency
        {
            get { return string.IsNullOrEmpty(DocumentForeignCurrencyId) ? DocumentCurrencyId : DocumentForeignCurrencyId; }
        }

        [ReflectionExclude]
        public string SerieName
        {
            get
            {
                var serieName = string.Empty;
                if (DocumentNumber.HasValue)
                    serieName += DocumentNumber.Value.ToString() + "/";
                serieName += DocumentSerie;

                return serieName;
            }
        }

		[ReflectionExclude]
		public bool CanModifyHolder
		{
			get
			{
				if (SignatureType.HasValue)
					return false;

				return IsEnabled;
			}
		}

		[ReflectionExclude]
		public bool CanModifyFiscalNumber
		{
			get
			{
				if (SignatureType.HasValue)
				{
					switch (SignatureType.Value)
					{
						case DocumentSign.FiscalizationBrazil:
							return false;
						case DocumentSign.FiscalizationPortugal:
							return IsEnabled && _baseEntityId.HasValue && !_finalCustomer &&
                                string.IsNullOrEmpty(_baseEntityFiscalNumber);
					}
				}

				return IsEnabled;
			}
		}

		[ReflectionExclude]
		public bool CanModifyAddress
		{
			get
			{
				if (SignatureType.HasValue)
				{
					switch (SignatureType.Value)
					{
						case DocumentSign.FiscalizationBrazil:
							return false;
						case DocumentSign.FiscalizationPortugal:
							return IsEnabled && _baseEntityId.HasValue && !_finalCustomer && string.IsNullOrEmpty(_baseEntityAddress);
					}
				}

				return IsEnabled;
			}
		}

        [ReflectionExclude]
        public bool IsValidFiscalNumber
        {
            get
            {
                if (SignatureType.HasValue && !_finalCustomer)
                {
                    switch (SignatureType.Value)
                    {
                        case DocumentSign.FiscalizationPortugal:
                            return !string.IsNullOrEmpty(FiscalNumber) && FiscalNumber.Length <= 20;
                        case DocumentSign.FiscalizationCroatia:
                            return !string.IsNullOrEmpty(FiscalNumber) &&
                                (FiscalNumber.Length == 11 || (FiscalNumber.StartsWith("HR") && FiscalNumber.Length == 13));
                    }
                }

                return VerifyFiscalNumber ? !string.IsNullOrEmpty(FiscalNumber) : true;
            }
        }

        [ReflectionExclude]
        public bool IsValidAddress
        {
            get
            {
                return true;
            }
        }

		public string BaseEntityName
		{
			get { return _baseEntityName; }
			set
			{
                _baseEntityName = value;
				Holder = _baseEntityName;
			}
		}

		public string BaseEntityAddress
		{
			get { return _baseEntityAddress; }
			set
			{
				_baseEntityAddress = value;
				Address = _baseEntityAddress;
			}
		}

		public string BaseEntityFiscalNumber
		{
			get { return _baseEntityFiscalNumber; }
			set
			{
				_baseEntityFiscalNumber = value;
				if (_baseEntityFiscalNumber != null)
					_baseEntityFiscalNumber = _baseEntityFiscalNumber.Trim();
				FiscalNumber = _baseEntityFiscalNumber;
				NotifyPropertyChanged("CanModifyFiscalNumber");
			}
		}

		public string BaseEntityFiscalRegister
		{
			get { return _baseEntityFiscalRegister; }
			set
			{
				_baseEntityFiscalRegister = value;
				if (_baseEntityFiscalRegister != null)
					_baseEntityFiscalRegister = _baseEntityFiscalRegister.Trim();
				FiscalRegister = _baseEntityFiscalRegister;
			}
		}

        #endregion
        #region Contracts

        [DataMember]
        public TypedList<FiscalDocDetailsContract> DocDetails { get; internal set; }

		[ReflectionExclude]
		public TypedList<CreditCardRefundContract> CreditCards
		{
			get { return _creditCards; }
			protected set { _creditCards = value; }
		}

        #endregion
    }
}
