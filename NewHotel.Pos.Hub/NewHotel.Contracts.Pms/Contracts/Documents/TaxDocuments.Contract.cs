﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class TaxDocumentsContract : BaseContract
    {
        #region Constructors

        public TaxDocumentsContract()
            : base() { }

        #endregion
        [DataMember]
        public decimal Tax1Value{ get; set; }
        [DataMember]
        public decimal Tax2Value { get; set; }
        [DataMember]
        public decimal Tax3Value { get; set; }
        [DataMember]
        public decimal Tax4Value { get; set; }
    }
}
