﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    public class PaymentContract : FiscalDocDetailsContract
    {
        #region Constants

        private const string creditCardFormat = "{0}-{1}-****-***{2}";

        #endregion
        #region CreditCard

        [DataMember]
        public Guid? WithdrawTypeId { get; set; }
        [DataMember]
        public string WithdrawTypeDesc { get; set; }
        [DataMember]
        public string CreditCardNumber { get; set; }
        [DataMember]
        public Guid? CreditCardTypeId { get; set; }
        [DataMember]
        public string CreditCardTypeDesc { get; set; }
        [DataMember]
        public Guid? CreditCardId { get; set; }
        [DataMember]
        public short? Month { get; set; }
        [DataMember]
        public short? Year { get; set; }
        [DataMember]
        public string ValidationCode { get; set; }
		[DataMember]
		public string HolderName { get; set; }
		[DataMember]
		public string Address { get; set; }
		[DataMember]
		public string ZipCode { get; set; }
		[DataMember]
		public string CardData { get; set; }

        [ReflectionExclude]
        public string CreditCard
        {
            get
            {
                if (string.IsNullOrEmpty(CreditCardNumber) || CreditCardNumber.Length != 16)
                    return string.Empty;

                return string.Format(creditCardFormat,
                    CreditCardNumber.Substring(0, 4),
                    CreditCardNumber.Substring(4, 4),
                    CreditCardNumber.Substring(14, 2));
            }
        }

        [ReflectionExclude]
        public string CreditCardColumnDescription
        {
            get
            {
                if (string.IsNullOrEmpty(CreditCardTypeDesc))
                    return null;

                return CreditCardTypeDesc + " -> " + CreditCard;
            }
        }

        #endregion
        #region Constructors

        public PaymentContract()
            : base() { }

        public PaymentContract(decimal? accountExchangeRateValue, bool accountExchangeRateMult, string accountExchangeRateCurrency, bool taxIncluded)
            : base(accountExchangeRateValue, accountExchangeRateMult, accountExchangeRateCurrency, taxIncluded)
        {
        }

        #endregion
    }
}
