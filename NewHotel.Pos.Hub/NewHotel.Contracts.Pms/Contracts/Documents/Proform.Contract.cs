﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ProformContract : FiscalDocContract
    {
        #region Constructors

        public ProformContract()
            : base() { }

        #endregion
    }
}
