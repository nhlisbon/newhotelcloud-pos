﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class BavelExportContract : BaseContract
    {
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string Content { get; set; }
    }
}
