﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class FiscalDocDetailsContract : BaseContract
    {
        #region Mmembers

        protected decimal _totalValue;
        private List<Guid> _movementIds;

        #endregion
        #region Properties

        [DataMember]
        public DocumentDetail DetailType { get; set; }
        [DataMember]
        public DateTime DetailDate { get; set; }
        [DataMember]
        public Guid? CurrentAccountId { get; set; }
        [DataMember]
        public short? PensionModeId { get; set; }
        [DataMember]
        public Guid? ResourceId { get; set; }
        [DataMember]
        public string Voucher { get; set; }
        [DataMember]
        public short? Adults { get; set; }
        [DataMember]
        public short? Childrens { get; set; }
        [DataMember]
        public DateTime? InitialDate { get; set; }
        [DataMember]
        public DateTime? FinalDate { get; set; }
        [DataMember]
        public DateTime? InitialPrintedDate { get; set; }
        [DataMember]
        public DateTime? FinalPrintedDate { get; set; }
        [DataMember]
        public short? Quantity { get; set; }
        [DataMember]
        public decimal? UnitValue { get; set; }
        [DataMember]
        public virtual decimal TotalValue
        {
            get { return _totalValue; }
            set { Set(ref _totalValue, value, "TotalValue"); }
        }
        [DataMember]
        public decimal? LiquidValue { get; set; }
        [DataMember]
        public string ForeignCurrency { get; set; }
        [DataMember]
        public decimal? ForeignUnitValue { get; set; }
        [DataMember]
        public decimal? ForeignTotalValue { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string DescriptionAdditional { get; set; }
        [DataMember]
        public decimal? DiscountValue { get; set; }
        [DataMember]
        public decimal? ForeignDiscountValue { get; set; }
        [DataMember]
        public Guid? TaxRate1 { get; set; }
        [DataMember]
        public decimal? TaxPercent1 { get; set; }
        [DataMember]
        public decimal? TaxValue1 { get; set; }
        [DataMember]
        public Guid? TaxRate2 { get; set; }
        [DataMember]
        public decimal? TaxPercent2 { get; set; }
        [DataMember]
        public decimal? TaxValue2 { get; set; }
        [DataMember]
        public Guid? TaxRate3 { get; set; }
        [DataMember]
        public decimal? TaxPercent3 { get; set; }
        [DataMember]
        public decimal? TaxValue3 { get; set; }
        [DataMember]
        public Guid? TaxRate4 { get; set; }
        [DataMember]
        public decimal? TaxPercent4 { get; set; }
        [DataMember]
        public decimal? TaxValue4 { get; set; }
        [DataMember]
        public List<Guid> MovementIds
        {
            get { return _movementIds; }
            set { Set(ref _movementIds, value, "MovementIds"); }
        }

        [DataMember]
        public Guid? DepartmentId { get; set; }
        [DataMember]
        public Guid? ServiceDepartmentId { get; set; }

        [DataMember]
        public decimal? AccountExchangeRateValue { get; set; }
        [DataMember]
        public bool AccountExchangeRateMult { get; set; }
        [DataMember]
        public string AccountExchangeRateCurrency { get; set; }

        [DataMember]
        public bool TaxIncluded { get; set; }

        #endregion
        #region Extended Properties

        public bool IsManual
        {
            get { return MovementIds == null || MovementIds.Count == 0; }
        }

        #endregion
        #region Constructors

        public FiscalDocDetailsContract()
            : base() { }

        public FiscalDocDetailsContract(decimal? accountExchangeRateValue, bool accountExchangeRateMult, string accountExchangeRateCurrency, bool taxIncluded)
            : this()
        {
            AccountExchangeRateValue = accountExchangeRateValue;
            AccountExchangeRateCurrency = accountExchangeRateCurrency;
            AccountExchangeRateMult = accountExchangeRateMult;
            TaxIncluded = taxIncluded;
        }

        #endregion
    }
}
