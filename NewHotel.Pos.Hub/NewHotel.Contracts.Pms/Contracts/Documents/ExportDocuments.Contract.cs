﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ExportDocumentsContract : BaseContract
    {
        #region Members

        private DateTime _dateIni;
        private DateTime _dateFin;
        private long _documentType;
        private ExportDocumentType _exportType;
        private bool _priceDetails;
        private bool? _hideInfo;

        #endregion
        #region Properties

        [DataMember]
        public string ReporPath { get; set; }
        [DataMember]
        public DateTime DateIni { get { return _dateIni; } set { Set(ref _dateIni, value, "DateIni"); } }
        [DataMember]
        public DateTime DateFin { get { return _dateFin; } set { Set(ref _dateFin, value, "DateFin"); } }
        [DataMember]
        public long DocumentType { get { return _documentType; } set { Set(ref _documentType, value, "DocumentType"); } }
        [DataMember]
        public ExportDocumentType ExportType { get { return _exportType; } set { Set(ref _exportType, value, "ExportType"); } }
        [DataMember]
        public bool PriceDetails { get { return _priceDetails; } set { Set(ref _priceDetails, value, "PriceDetails"); } }
        [DataMember]
        public bool? HideInfo { get { return _hideInfo; } set { Set(ref _hideInfo, value, "HideInfo"); } }

        #endregion
        #region Constructor

        public ExportDocumentsContract()
            : base() { }

        #endregion
    }
}