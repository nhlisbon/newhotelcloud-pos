﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    public class CurrencyContract : BaseContract<string>
    {
        #region Properties

        [DataMember]
        public string Description { get; internal set; }
        [DataMember]
        public decimal ExchangeRateValue { get; internal set; }
        [DataMember]
        public bool ExchangeRateMult { get; internal set; }
        [DataMember]
        public short ShowDecimals { get; internal set; }
        [DataMember]
        public short EditDecimals { get; internal set; }

        #endregion
        #region Constructor

        public CurrencyContract(string id, string description, decimal exchangeRateValue, bool exchangeRateMult, short showDecimals, short editDecimals)
            : base()
        {
            Id = id;
            Description = description;
            ExchangeRateValue = exchangeRateValue;
            ExchangeRateMult = exchangeRateMult;
            ShowDecimals = showDecimals;
            EditDecimals = editDecimals;
        }

        #endregion
    }

    [DataContract]
    public class OfficialDocContract : FiscalDocContract
    {
        #region Members

        private DateTime? _expiredDate;
        private string _currencyId;
        protected decimal _exchangeRateValue = decimal.One;
        protected bool _exchangeRateMult = true;

        #endregion
        #region Constructors

        public OfficialDocContract()
            : base()
        {
			PaymentProvider = PaymentServiceProvider.Manual;
            DocumentState = OficialDocumentState.Pending;
            DocTaxes = new TypedList<OfficialDocTaxesContract>();
            Discounts = new TypedList<InvoiceDiscountsContract>();
            Currencies = new TypedList<CurrencyContract>();
        }

        #endregion
        #region Properties

		[DataMember(Order = 0)]
		public PaymentServiceProvider PaymentProvider { get; set; }

        // para usar si la cuenta tiene un cambio fijado
        [DataMember]
        public decimal? AccountExchangeRateValue { get; set; }
        [DataMember]
        public bool AccountExchangeRateMult { get; set; }
        [DataMember]
        public string AccountExchangeRateCurrency { get; set; }

        [DataMember]
        public Guid? ExtAccountId { get; set; }
        [DataMember]
        public Guid? CommissionTax { get; set; }
        [DataMember]
        public decimal? CommissionPercent { get; set; }
        [DataMember]
        public decimal? CommissionValue { get; set; }
        [DataMember]
        public OficialDocumentState DocumentState { get; set; }
        [DataMember]
        public short ExportCount { get; set; }
        [DataMember]
        public short ExportForeignCount { get; set; }
        [DataMember]
        public DateTime? ExpiredDate { get { return _expiredDate; } set { Set(ref _expiredDate, value, "ExpiredDate"); } }
        [DataMember]
        public bool OffshoreDocument { get; set; }
        [DataMember]
        public Guid? OffshoreReference { get; set; }
        [DataMember]
        public decimal? TotalNet { get; set; }
        [DataMember]
        public DateTime? RegularizationDate { get; set; }
        [DataMember]
        public decimal? TotalAccountTransfer { get; set; }
        [DataMember]
        public decimal? TotalTaxes { get; set; }
        [DataMember]
        public decimal? TotalCreditWithTaxes { get; set; }
        [DataMember]
        public decimal? TotalCreditNoTaxes { get; set; }
        [DataMember]
        public decimal? TotalInAdvanceDeposits { get; set; }
        [DataMember]
        public decimal? TotalPayments { get; set; }
        [DataMember]
        public decimal? TotalDiscount { get; set; }
        [DataMember]
        public decimal? TotalCashAdvance { get; set; }
        [DataMember]
        public decimal? TotalRefund { get; set; }
        [DataMember]
        public decimal? FinancialDiscount { get; set; }
        [DataMember]
        public Guid? PaymentMethodDefaultId { get; set; }
        [DataMember]
        public DateTime? DocDate { get; set; }
        [DataMember]
        public DateTime? DocSysDateTime { get; set; }
        [DataMember]
        public string SignDocument { get; set; }

        [DataMember]
        public bool UseDocumentAccount { get; set; }
        [DataMember]
        public CurrentAccountType? CurrentAccountType { get; set; }
        [DataMember]
        public Guid? CurrentAccountId { get; set; }
        [DataMember]
        public string CurrentAccountDescription { get; set; }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        protected decimal CurrencyExchangeRateValue
        {
            get
            {
                if (!string.IsNullOrEmpty(AccountExchangeRateCurrency) && CurrencyId == AccountExchangeRateCurrency && AccountExchangeRateValue.HasValue)
                    return AccountExchangeRateValue.Value;

                return _exchangeRateValue;
            }
        }

        [ReflectionExclude]
        protected bool CurrencyExchangeRateMult
        {
            get
            {
                if (!string.IsNullOrEmpty(AccountExchangeRateCurrency) && CurrencyId == AccountExchangeRateCurrency && AccountExchangeRateValue.HasValue)
                    return AccountExchangeRateMult;

                return _exchangeRateMult;
            }
        }

        [ReflectionExclude]
        public virtual string CurrencyId
        {
            get { return _currencyId; }
            set
            {
                if (_currencyId != value)
                {
                    _currencyId = value;
                    _exchangeRateValue = decimal.One;
                    _exchangeRateMult = true;

                    if (!string.IsNullOrEmpty(_currencyId) && Currencies != null)
                    {
                        var currency = Currencies.FirstOrDefault(x => x.Id.Equals(_currencyId));
                        if (currency != null)
                        {
                            _exchangeRateValue = currency.ExchangeRateValue;
                            _exchangeRateMult = currency.ExchangeRateMult;
                        }
                    }

                    NotifyPropertyChanged("CurrencyId");
                    NotifyPropertyChanged("CurrencyDebtValue");
                }
            }
        }

        [ReflectionExclude]
        public virtual decimal CurrencyDebtValue
        {
            get { return decimal.Zero; }
        }

        [ReflectionExclude]
        public virtual bool ShowCurrency
        {
            get
            {
                if (!string.IsNullOrEmpty(DocumentCurrencyId) && Currencies != null &&
                    Currencies.Where(x => !x.Id.Equals(DocumentCurrencyId)).Count() > 0)
                    return true;
                
                return false;
            }
        }

		[ReflectionExclude]
		public virtual bool HideCreditPaymentMethods
		{
			get { return false; }
		}

        [ReflectionExclude]
        public virtual decimal? CreditsMaxValue
        {
            get { return null; }
        }

        #endregion
        #region Contracts

        [ReflectionExclude]
        [DataMember]
        public TypedList<OfficialDocTaxesContract> DocTaxes { get; internal set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<InvoiceDiscountsContract> Discounts { get; internal set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<CurrencyContract> Currencies { get; internal set; }

        #endregion
        #region Protected Methods

        protected static decimal GetCurrencyValue(decimal grossValue, decimal? foreingnCurrencyValue, string foreignCurrency)
        {
            if (!string.IsNullOrEmpty(foreignCurrency) && foreingnCurrencyValue.HasValue)
                return foreingnCurrencyValue.Value;

            return grossValue;
        }

        protected decimal GetExchangeRateValue(decimal value, decimal? exchangeRateValue, bool exchangeRateMult)
        {
            if (exchangeRateValue.HasValue)
                return exchangeRateMult ? value * exchangeRateValue.Value : value / exchangeRateValue.Value;

            return CurrencyExchangeRateMult ? value * CurrencyExchangeRateValue : value / CurrencyExchangeRateValue;
        }

        #endregion
        #region Payment Methods

        public virtual void AddDetail(Guid departmentId, Guid serviceDepartmentId,
            string description, string addDescription, Guid currentAccountId, short? pensionModeId,
            short quantity, decimal value, string currency)
        {
        }

        public void AddDetail(Guid departmentId, Guid serviceDepartmentId,
            string description, string addDescription, Guid currentAccountId, short? pensionModeId,
            short quantity, decimal value)
        {
            AddDetail(departmentId, serviceDepartmentId, description, addDescription,
                currentAccountId, pensionModeId, quantity, value, string.Empty);
        }

        public virtual void RemoveDetails(Guid[] ids)
        {
            DocDetails.Remove(x => ids.Contains((Guid)x.Id));
        }

        public virtual void AddPayment(Guid? withdrawTypeId, string withdrawTypeDesc, Guid? creditCardTypeId, string creditCardTypeDesc,
            Guid? creditCardId, string creditCardNumber, short? month, short? year, string validationCode,
			string holderName, string address, string zipCode, decimal value, string currency, string cardData)
        { 
        }

        public void AddPayment(Guid? withdrawTypeId, string withdrawTypeDesc, Guid? creditCardTypeId, string creditCardTypeDesc,
			Guid? creditCardId, string creditCardNumber, short? month, short? year, string validationCode,
			string holderName, string address, string zipCode, decimal value, string cardData)
        {
            AddPayment(withdrawTypeId, withdrawTypeDesc, creditCardTypeId, creditCardTypeDesc,
				creditCardId, creditCardNumber, month, year, validationCode,
				holderName, address, zipCode, value, string.Empty, cardData);
        }

        public virtual void RemovePayments(Guid[] ids)
        {
            DocDetails.Remove(x => ids.Contains((Guid)x.Id));
        }

        public virtual void RemovePayments()
        {
        }

        #endregion
        #region Discount Methods

        public virtual bool AddDiscount(Guid? discountTypeId, string discountTypeDescription, decimal? discountPercent, decimal? discountValue)
        {
            return false;
        }

        public virtual void RemoveDiscounts(Guid[] ids)
        {
            Discounts.Remove(x => ids.Contains((Guid)x.Id));
        }

        #endregion
    }
}
