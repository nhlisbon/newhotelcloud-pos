﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class PreInvoiceSelectedContract : BaseContract
    {
        #region Constructors

        public PreInvoiceSelectedContract()
            : base() 
        {
            Details = new TypedList<InvoiceReservationRecord>();
            InDesk = true;
        }

        #endregion
        #region Properties

        public bool InDesk { get; set; }
        public Guid? EntityId { get; set; }
        public Guid? ContractId { get; set; }

        #endregion
        #region Contracts
        [ReflectionExclude]
        public TypedList<InvoiceReservationRecord> Details { get; set; }

        #endregion
    }
}
