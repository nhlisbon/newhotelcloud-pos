﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Contracts
{
    [DataContract]
    public class PreInvoiceContract : BaseContract
    {
        #region Constants

        private const string creditCardFormat = "{0}-{1}-****-***{2}";

        #endregion
        #region Members

        public decimal? ExchangeRateValue;
        public bool ExchangeRateMult;

        #endregion
        #region Properties

        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string AdditionalDescription { get; set; }
        [DataMember]
        public DateTime WorkDate { get; set; }
        [DataMember]
        public DateTime ValueDate { get; set; }
        [DataMember]
        public short? Quantity { get; set; }
        [DataMember]
        public decimal GrossValue { get; set; }
        [DataMember]
        public decimal NetValue { get; set; }
        [DataMember]
        public decimal? DiscountValue { get; set; }
        [DataMember]
        public decimal? ForeignDiscountValue { get; set; }
        [DataMember]
        public string ForeignCurrency { get; set; }
        [DataMember]
        public decimal? ForeignCurrencyValue { get; set; }

        // para usar si la cuenta tiene un cambio fijado
        [DataMember]
        public decimal? AccountExchangeRateValue { get; set; }
        [DataMember]
        public bool AccountExchangeRateMult { get; set; }
        [DataMember]
        public string AccountExchangeRateCurrency { get; set; }

        [DataMember]
        public Guid? TaxRateId1 { get; set; }
        [DataMember]
        public decimal? TaxPercent1 { get; set; }
        [DataMember]
        public decimal? TaxValue1 { get; set; }
        [DataMember]
        public Guid? TaxRateId2 { get; set; }
        [DataMember]
        public decimal? TaxPercent2 { get; set; }
        [DataMember]
        public decimal? TaxValue2 { get; set; }
        [DataMember]
        public Guid? TaxRateId3 { get; set; }
        [DataMember]
        public decimal? TaxPercent3 { get; set; }
        [DataMember]
        public decimal? TaxValue3 { get; set; }
        [DataMember]
        public Guid? TaxRateId4 { get; set; }
        [DataMember]
        public decimal? TaxPercent4 { get; set; }
        [DataMember]
        public decimal? TaxValue4 { get; set; }
        [DataMember]
        public bool IsCredit { get; set; }
        [DataMember]
        public Guid[] MovementIds { get; set; }

        #endregion
        #region CreditCard Properties

        [DataMember]
        public Guid? WithdrawTypeId { get; set; }
        [DataMember]
        public string WithdrawTypeDesc { get; set; }
        [DataMember]
        public string CreditCardNumber { get; set; }
        [DataMember]
        public Guid? CreditCardTypeId { get; set; }
        [DataMember]
        public string CreditCardTypeDesc { get; set; }
        [DataMember]
        public Guid? CreditCardId { get; set; }
        [DataMember]
        public short? Month { get; set; }
        [DataMember]
        public short? Year { get; set; }
        [DataMember]
        public string ValidationCode { get; set; }
        [DataMember]
        public string CreditCardAuthCode { get; set; }
		[DataMember]
		public string HolderName { get; set; }
		[DataMember]
		public string Address { get; set; }
		[DataMember]
		public string ZipCode { get; set; }
		[DataMember]
		public string CardData { get; set; }
        [DataMember]
        public string Reference { get; set; }

        [ReflectionExclude]
        public string CreditCard
        {
            get
            {
				if (!string.IsNullOrEmpty(CreditCardNumber) && CreditCardNumber.Length > 8)
				{
					var temp = CreditCardNumber.ToCharArray();
					for (int i = 8; i < (temp.Length - 1); i++)
						temp[i] = '*';
					var identNumber = new string(temp);

					return string.Format("{0}-{1}-{2}-{3}",
						identNumber.Substring(0, 4),
						identNumber.Substring(4, 4),
						identNumber.Substring(8, 4),
						identNumber.Substring(12));
				}

				return string.Empty;
			}
        }

        [ReflectionExclude]
        public string CreditCardColumnDescription 
        { 
            get 
            { 
                if (string.IsNullOrEmpty(CreditCardTypeDesc))
                    return null;

                return string.Format("{0} -> {1}", CreditCardTypeDesc, CreditCard);
            } 
        }

        #endregion
        #region Extension Properties

        public bool IsManual
        {
            get { return MovementIds == null || MovementIds.Length == 0; }
        }

        #endregion
        #region Constructors

        public PreInvoiceContract()
            : base() { }

        public PreInvoiceContract(decimal? accountExchangeRateValue, bool accountExchangeRateMult, string accountExchangeRateCurrency)
            : this()
        {
            AccountExchangeRateValue = accountExchangeRateValue;
            AccountExchangeRateCurrency = accountExchangeRateCurrency;
            AccountExchangeRateMult = accountExchangeRateMult;
        }

        #endregion
    }
}
