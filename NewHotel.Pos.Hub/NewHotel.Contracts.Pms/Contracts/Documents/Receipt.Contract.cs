﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class ReceiptContract : FiscalDocContract
    {
        #region Constructors

        public ReceiptContract()
            : base() { }

        #endregion
    }
}
