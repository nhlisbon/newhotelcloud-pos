﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    public class InvoicePreviewContract : BaseContract
    {
    }

    [DataContract]
    public class InvoiceContract : OfficialDocContract
    {
        #region Members

        private PaymentMode _paymentMode;

        #endregion
        #region Properties

        [DataMember]
        public bool IsTicket { get; internal set; }
        [DataMember]
        public bool IsInAdvance { get; set; }
        [DataMember]
        public bool IsCredit { get; set; }
        [DataMember]
        public bool IsAnnulment { get; set; }
        [DataMember]
        public bool ShowPaxsCount { get; set; }
        [DataMember]
        public bool ShowGuestName { get; set; }
        [DataMember]
        public bool ShowHotelInfo { get; set; }
        [DataMember]
        public string Voucher { get; set; }
        [DataMember]
        public PaymentMode PaymentMode
        {
            get { return _paymentMode; }
            set { Set(ref _paymentMode, value, "PaymentMode"); }
        }
        [DataMember]
        public DateTime WorkDate { get; internal set; }
        [DataMember]
        public Guid? CreditReferenceId { get; internal set; }
        [DataMember]
        public Guid? ProformReferenceId { get; internal set; }
        [DataMember]
        public Guid? AnnulmentReferenceId { get; internal set; }

        [DataMember]
        public string CreditReferenceSerie { get; internal set; }
        [DataMember]
        public string ProformReferenceSerie { get; internal set; }
        [DataMember]
        public string AnnulmentReferenceSerie { get; internal set; }

        [DataMember]
        public bool AllowPaymentPending { get; internal set; }
        [DataMember]
        public Guid? TaxSchemaId { get; internal set; }

        [DataMember]
        public Guid? ReservationId { get; set; }
        [DataMember]
        public bool PaymentMethodVisibility { get; set; }
        [DataMember]
        public bool FreeOfCharge { get; set; }
        [DataMember]
        public bool TransferOnlinePayment { get; set; }

		[DataMember]
		public decimal TotalRetained { get; set; }
		[DataMember]
		public decimal ForeignCurrencyTotalRetained { get; set; }

        [DataMember]
        public override string DocumentCurrencyId
        {
            get { return base.DocumentCurrencyId; }
            set
            {
                if (base.DocumentCurrencyId != value)
                {
                    base.DocumentCurrencyId = value;
                    if (string.IsNullOrEmpty(CurrencyId))
                        CurrencyId = value;
                }
            }
        }

        [DataMember]
        public CreditCardContract CreditCard { get; set; }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public bool CreditReferenceVisibility 
        {
            get { return CreditReferenceId.HasValue; }
        }

        [ReflectionExclude]
        public bool ProformaReferenceVisibility
        {
            get { return ProformReferenceId.HasValue; }
        }

        [ReflectionExclude]
        public bool AnnulmentReferenceVisibility
        {
            get { return AnnulmentReferenceId.HasValue; }
        }

        [ReflectionExclude]
        public override string CurrencyId
        {
            get { return base.CurrencyId; }
            set
            {
                if (base.CurrencyId != value)
                {
                    base.CurrencyId = value;
                    NotifyPropertyChanged("CurrencyTotalRetained");
                    NotifyPropertyChanged("CurrencyTotalPaid");
                }
            }
        }

        [ReflectionExclude]
        public decimal TotalPaid
        {
			get { return PreInvoices.Where(x => !x.IsCredit && x.IsManual).Sum(x => x.GrossValue) +
				(ApplyRetention ? TotalRetained : decimal.Zero); }
        }

        [ReflectionExclude]
        public decimal DebtValue
        {
            get { return PendingValue - TotalPaid - TotalFinancialDiscount; }
        }

        [ReflectionExclude]
        public bool IsPartialPayed
        {
            get { return DebtValue > 0 && (PreInvoices.FirstOrDefault(x => !x.IsCredit) != null); }
        }

        [ReflectionExclude]
        public string ForeignCurrency
        {
            get 
            {
                var currencies = Details.Select(x => x.ForeignCurrency ?? string.Empty);
                if (currencies.Count() == 1 && currencies.First() == string.Empty)
                {
                    currencies = Payments.Select(x => x.ForeignCurrency ?? string.Empty).Where(x => x != string.Empty).Distinct();
                    if (currencies.Count() == 1)
                    {
                        var currency = currencies.First();
                        if (currency == currencies.First())
                            return currency;
                    }
                }
                else
                {
                    currencies = Details.Select(x => x.ForeignCurrency ?? string.Empty).Where(x => x != string.Empty).Distinct();
                    if (currencies.Count() == 1)
                    {
                        var currency = currencies.First();
                        if (Payments.Count == 0)
                            return currency;
                        else
                        {
                            currencies = Payments.Select(x => x.ForeignCurrency ?? string.Empty).Where(x => x != string.Empty).Distinct();
                            if (currencies.Count() == 1)
                            {
                                if (currency == currencies.First())
                                    return currency;
                            }
                        }
                    }
                }

                return null;
            }
        }

        [ReflectionExclude]
        public decimal TotalInForeignCurrency
        {
            get
            {
                if (string.IsNullOrEmpty(ForeignCurrency))
                    return decimal.Zero;

                return Details.Where(x => x.IsCredit).Sum(x => !string.IsNullOrEmpty(x.ForeignCurrency) && x.ForeignCurrencyValue.HasValue
                         ? x.ForeignCurrencyValue.Value
                         : (CurrencyExchangeRateMult ? x.GrossValue / CurrencyExchangeRateValue : x.GrossValue * CurrencyExchangeRateValue));
            }
        }

        [ReflectionExclude]
        public decimal TotalFinancialDiscount
        {
            get { return Discounts.Sum(x => x.DiscountValue); }
        }

        [ReflectionExclude]
        public decimal CurrencyTotalRetained
        {
			get
			{
                return !string.IsNullOrEmpty(ForeignCurrency) && ForeignCurrencyTotalRetained > decimal.Zero
                    ? ForeignCurrencyTotalRetained
                    : (CurrencyExchangeRateMult ? TotalRetained / CurrencyExchangeRateValue : TotalRetained * CurrencyExchangeRateValue);
			}
        }

        [ReflectionExclude]
        public decimal CurrencyTotalPaid
        {
			get
			{ 
				return PreInvoices.Where(x => x.IsManual && !x.IsCredit).Sum(x =>
                    !string.IsNullOrEmpty(x.ForeignCurrency) && x.ForeignCurrencyValue.HasValue
                    ? x.ForeignCurrencyValue.Value
                    : (CurrencyExchangeRateMult ? x.GrossValue / CurrencyExchangeRateValue : x.GrossValue * CurrencyExchangeRateValue))
                    + CurrencyTotalRetained;
                    //.Sum(x => GetCurrencyValue(x.GrossValue, x.ForeignCurrencyValue, x.ForeignCurrency))
                    //+ (ApplyRetention ? GetCurrencyValue(TotalRetained, ForeignCurrencyTotalRetained, ForeignCurrency) : decimal.Zero);
			}
        }

        [ReflectionExclude]
        public decimal ForeignCurrencyPendingValue
        {
            get
            { 
                return PreInvoices.Where(x => !x.IsManual).Sum(x => (x.IsCredit ? decimal.One : decimal.MinusOne) *
                    (!string.IsNullOrEmpty(x.ForeignCurrency) && x.ForeignCurrencyValue.HasValue
                         ? x.ForeignCurrencyValue.Value
                         : (CurrencyExchangeRateMult ? x.GrossValue / CurrencyExchangeRateValue : x.GrossValue * CurrencyExchangeRateValue)));
            }
        }

        [ReflectionExclude]
        public override decimal CurrencyDebtValue
        {
            get
            {
                var pendingValue = decimal.Zero;
                if (!string.IsNullOrEmpty(ForeignCurrency) && CurrencyId == ForeignCurrency)
                    pendingValue = ForeignCurrencyPendingValue;
                else
                    pendingValue = GetExchangeRateValue(PendingValue, CurrencyExchangeRateValue, !CurrencyExchangeRateMult);
                return pendingValue - CurrencyTotalPaid
                    - GetExchangeRateValue(TotalFinancialDiscount, CurrencyExchangeRateValue, !CurrencyExchangeRateMult);
            }
        }

        [ReflectionExclude]
        public override bool ShowCurrency
        {
            get { return true; }
        }

		public override bool ApplyRetention
		{
			get	{ return base.ApplyRetention; }
			set
			{
				if (base.ApplyRetention != value)
				{
					base.ApplyRetention = value;
					NotifyPropertyChanged("TotalPaid");
					NotifyPropertyChanged("DebtValue");
				}
			}
		}
        public bool AllowInsertfromExternal { get; set; }

        #endregion
        #region Contracts

        [ReflectionExclude]
        [DataMember]
        public TypedList<PreInvoiceContract> Details { get; internal set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<PreInvoiceContract> Payments { get; internal set; }

        [ReflectionExclude]
        public IEnumerable<PreInvoiceContract> PreInvoices
        {
            get { return Details.Concat(Payments); }
        }

        #endregion
        #region Constructors

        public InvoiceContract(bool isTicket, DateTime workDate,
            bool allowPaymentPending, Guid?  taxSchemaId)
            : base()
        {
            IsTicket = isTicket;
            WorkDate = workDate;
            AllowPaymentPending = allowPaymentPending;
            PaymentMode = PaymentMode.Payment;
            DocumentState = OficialDocumentState.Payed;
            DetailedDocument = true;
            Details = new TypedList<PreInvoiceContract>();
            Payments = new TypedList<PreInvoiceContract>();
            TaxSchemaId = taxSchemaId;
        }

        public InvoiceContract(bool isTicket, DateTime workDate,
            bool allowPaymentPending, Guid?  taxSchemaId,
            CancellationControlContract cancellationControl,
            Guid? annulmentReferenceId, string annulmentReferenceSerie,
            Guid? creditReferenceId, string creditReferenceSerie,
            Guid? proformReferenceId, string proformReferenceSerie)
            : this(isTicket, workDate, allowPaymentPending, taxSchemaId)
        {
            CancellationControl = cancellationControl;
            AnnulmentReferenceId = annulmentReferenceId;
            AnnulmentReferenceSerie = annulmentReferenceSerie;
            CreditReferenceId = creditReferenceId;
            CreditReferenceSerie = creditReferenceSerie;
            ProformReferenceId = proformReferenceId;
            ProformReferenceSerie = proformReferenceSerie;
        }

        #endregion
        #region Public Methods

        public override void AddPayment(Guid? withdrawTypeId, string withdrawTypeDesc, Guid? creditCardTypeId,
            string creditCardTypeDesc, Guid? creditCardId, string creditCardNumber, short? month, short? year, string validationCode,
            string holderName, string address, string zipCode, decimal value, string currency, string cardData)
        {
            var contract = new PreInvoiceContract(AccountExchangeRateValue, AccountExchangeRateMult, AccountExchangeRateCurrency);
            contract.NewId();
            contract.IsCredit = false;
            contract.WithdrawTypeId = withdrawTypeId;
            contract.WithdrawTypeDesc = withdrawTypeDesc;
            contract.CreditCardTypeId = creditCardTypeId;
            contract.CreditCardTypeDesc = creditCardTypeDesc;
            if (creditCardId.HasValue)
                contract.CreditCardId = creditCardId.Value;
            else if (creditCardTypeId.HasValue)
                contract.CreditCardId = BaseContract.NewGuid();
            contract.CreditCardNumber = creditCardNumber;
            contract.Month = month;
            contract.Year = year;
            contract.ValidationCode = validationCode;
			contract.HolderName = holderName;
			contract.Address = address;
			contract.ZipCode = zipCode;
			contract.CardData = cardData;

            if (string.IsNullOrEmpty(currency) || currency == DocumentCurrencyId)
            {
                contract.ForeignCurrency = null;
                contract.ForeignCurrencyValue = null;
                contract.ExchangeRateMult = true;
                contract.ExchangeRateValue = null;
                contract.GrossValue = value;
            }
            else
            {
                contract.ForeignCurrency = currency;
                contract.ForeignCurrencyValue = value;
                contract.ExchangeRateMult = CurrencyExchangeRateMult;
                if (value == CurrencyDebtValue)
                {
                    contract.ExchangeRateValue = CurrencyExchangeRateMult
                        ? contract.GrossValue / contract.ForeignCurrencyValue.Value
                        : contract.GrossValue * contract.ForeignCurrencyValue.Value;
                    contract.GrossValue = DebtValue;
                }
                else
                {
                    contract.ExchangeRateValue = CurrencyExchangeRateValue;
                    contract.GrossValue = GetExchangeRateValue(value, CurrencyExchangeRateValue, CurrencyExchangeRateMult);
                }
            }

            contract.MovementIds = new Guid[0];
            Payments.Add(contract);

            NotifyPropertyChanged("TotalPaid");
            NotifyPropertyChanged("DebtValue");
            NotifyPropertyChanged("ForeignCurrency");
            NotifyPropertyChanged("TotalInForeignCurrency");
        }

        public override void RemovePayments(Guid[] ids)
        {
            Payments.Remove(x => ids.Contains((Guid)x.Id));
            NotifyPropertyChanged("TotalPaid");
            NotifyPropertyChanged("DebtValue");
            NotifyPropertyChanged("ForeignCurrency");
            NotifyPropertyChanged("TotalInForeignCurrency");
        }

        public override void RemovePayments()
        {
            Payments.Clear();
            NotifyPropertyChanged("TotalPaid");
            NotifyPropertyChanged("DebtValue");
            NotifyPropertyChanged("ForeignCurrency");
            NotifyPropertyChanged("TotalInForeignCurrency");
        }

		public override bool AddDiscount(Guid? discountTypeId, string discountTypeDescription, decimal? discountPercent, decimal? discountValue)
        {
			if (!discountValue.HasValue && discountPercent.HasValue)
				discountValue = discountPercent.Value == 100M ? DebtValue : (DebtValue * discountPercent.Value / 100M);

            if (discountValue > 0 && discountValue <= DebtValue)
            {
                var invoiceDiscount = new InvoiceDiscountsContract();
                invoiceDiscount.DiscountTypeId = discountTypeId;
                invoiceDiscount.DiscountTypeDescription = discountTypeDescription;
                invoiceDiscount.DiscountPercent = discountPercent;
                invoiceDiscount.DiscountValue = discountValue.Value;

                Discounts.Add(invoiceDiscount);
                NotifyPropertyChanged("TotalFinancialDiscount");
                NotifyPropertyChanged("DebtValue");
                NotifyPropertyChanged("ForeignCurrency");

                return true;
            }

            return false;
        }

        public override void RemoveDiscounts(Guid[] ids)
        {
            base.RemoveDiscounts(ids);
            NotifyPropertyChanged("TotalFinancialDiscount");
            NotifyPropertyChanged("DebtValue");
            NotifyPropertyChanged("ForeignCurrency");
        }

        #endregion

    }
}
