﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class OfficialDocTaxesContract : BaseContract
    {
        #region Constructors

        public OfficialDocTaxesContract()
            : base() { }

        #endregion
        #region Properties

        //[DataMember]
        //public Guid OfficialDocumentId { get; set; }
        [DataMember]
        public Guid TaxRateId { get; set; }
        [DataMember]
        public string TaxRateDescription { get; set; }
        [DataMember]
        public decimal TaxPercent { get; set; }
        [DataMember]
        public decimal TaxIncidence { get; set; }
        [DataMember]
        public decimal TaxBase { get; set; }
        [DataMember]
        public decimal TaxValue { get; set; }
        [DataMember]
        public bool AdvancedTax { get; set; }

        #endregion
    }
}
