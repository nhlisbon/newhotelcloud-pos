﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class CreditCardDetailsContract : FiscalDocDetailsContract
    {
        #region Constructors

        public CreditCardDetailsContract()
            : base() { }

        #endregion
        #region Properties

        [DataMember]
        public string DocNumber { get; set; }
        [DataMember]
        public DailyAccountType AccountFolder { get; set; }
        [DataMember]
        public bool TaxIncluded { get; set; }
        [DataMember]
        public Guid? DepartmentId { get; set; }
        [DataMember]
        public Guid? ServiceByDepartmentId { get; set; }

        #endregion
    }
}
