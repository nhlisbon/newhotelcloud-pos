﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class InvoiceDiscountsContract : BaseContract
    {
        #region Properties

        [DataMember]
        public Guid? DiscountTypeId { get; set; }
        [DataMember]
        public string DiscountTypeDescription { get; set; }
        [DataMember]
        public decimal? DiscountPercent { get; set; }
        [DataMember]
        public decimal DiscountValue { get; set; }

        #endregion
        #region Constructors

        public InvoiceDiscountsContract()
            : base() { }

        #endregion
    }
}
