﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    public class CreditNotePreviewContract : BaseContract
    {
    }

    [DataContract]
    [KnownType(typeof(PaymentContract))]
    public class CreditNoteContract : OfficialDocContract
    {
        #region Members

		[DataMember(Order = 3)]
		public CreditCardContract _creditCard;

        private PaymentMode _paymentMode;
		private decimal _totalPaid;
        private Guid? _invoiceReferenceId;
        private bool _reinsertInvoiceServices;

        #endregion
        #region Properties

        [DataMember]
        public PaymentMode PaymentMode
        {
            get { return _paymentMode; }
            set { Set(ref _paymentMode, value, "PaymentMode"); }
        }

        [DataMember]
        public Guid? InvoiceReferenceId 
        {
            get { return _invoiceReferenceId; }
            internal set { Set(ref _invoiceReferenceId, value, "InvoiceReferenceId"); }
        }
        [DataMember]
        public string InvoiceReferenceSerie { get; internal set; }
        [DataMember]
        public decimal InvoiceTotalCredits { get; set; }
        [DataMember]
        public bool ReinsertInvoiceServices
        {
            get { return _reinsertInvoiceServices; }
            set { Set(ref _reinsertInvoiceServices, value, "ReinsertInvoiceServices"); }
        }
        [DataMember]
        public bool OpenOrUnlockAccounts { get; set; }

        [DataMember]
        public override string DocumentCurrencyId
        {
            get { return base.DocumentCurrencyId; }
            set
            {
                if (base.DocumentCurrencyId != value)
                {
                    base.DocumentCurrencyId = value;
                    if (string.IsNullOrEmpty(CurrencyId))
                        CurrencyId = value;
                }
            }
        }

		[ReflectionExclude]
		[DataMember(Order = 2)]
		public override Guid? CreditCardRefundId
		{
			get { return base.CreditCardRefundId; }
			set
			{
				if (base.CreditCardRefundId != value)
				{
					base.CreditCardRefundId = value;
					if (base.CreditCardRefundId.HasValue && _creditCards != null)
					{
						if (CreditCard != null)
							FiilCreditCard();
						else
						{
							CreditCard = new CreditCardContract(true);
							CreditCard.PaymentProvider = PaymentProvider;
						}
					}
				}
			}
		}

        [DataMember]
        public bool TaxIncluded { get; set; }

        public decimal TotalPaidReal { get; set; }
              
        #endregion
        #region Extended Properties

		[ReflectionExclude]
		public override bool ShowRefundCreditCards
		{
			get { return _creditCards != null && _creditCards.Count > 0; }
		}

        [ReflectionExclude]
        public decimal TotalPaid
        {
			set
			{
				Set(ref _totalPaid, value, "TotalPaid");
				NotifyPropertyChanged("DebtValue");
			}
            get { return DocDetails.Where(x => x.DetailType == DocumentDetail.Payment).Sum(x => x.TotalValue); }
        }

        [ReflectionExclude]
        public decimal DebtValue
        {
            get { return PendingValue - TotalPaid - TotalFinancialDiscount; }
        }

        [ReflectionExclude]
        public bool IsPartialPayed
        {
            get { return DebtValue > decimal.Zero && (DocDetails.FirstOrDefault(x => x.DetailType == DocumentDetail.Payment) != null); }
        }

        [ReflectionExclude]
        public string ForeignCurrency
        {
            get
            {
                var currencies = DocDetails.Select(x => x.ForeignCurrency ?? string.Empty).Where(x => x != string.Empty).Distinct();
                return currencies.Count() == 1 ? currencies.First() : null;
            }
        }

        [ReflectionExclude]
        public decimal DocumentForeignValue
        {
            get
            {
                if (string.IsNullOrEmpty(ForeignCurrency))
                    return decimal.Zero;
                else
                    return Math.Round(DocDetails.Where(x => x.DetailType != DocumentDetail.Payment).Sum(x =>
                        !string.IsNullOrEmpty(x.ForeignCurrency) && x.ForeignTotalValue.HasValue
                             ? x.ForeignTotalValue.Value
                             : (CurrencyExchangeRateMult ? x.TotalValue / CurrencyExchangeRateValue : x.TotalValue * CurrencyExchangeRateValue)), 2);
            }
        }

        [ReflectionExclude]
        public decimal TotalFinancialDiscount
        {
            get { return Math.Round(Discounts.Sum(x => x.DiscountValue), 2); }
        }

        [ReflectionExclude]
        public decimal CurrencyTotalPaid
        {
            get { return Math.Round(DocDetails.Where(x => x.IsManual && x.DetailType == DocumentDetail.Payment).Sum(x => GetCurrencyValue(x.TotalValue, x.ForeignTotalValue, x.ForeignCurrency)), 2); }
        }

        [ReflectionExclude]
        public decimal ForeignCurrencyPendingValue
        {
            get
            {
                return Math.Round(DocDetails.Where(x => x.IsManual).Sum(x => (x.DetailType != DocumentDetail.Payment ? decimal.One : decimal.MinusOne) *
                    (!string.IsNullOrEmpty(x.ForeignCurrency) && x.ForeignTotalValue.HasValue
                         ? x.ForeignTotalValue.Value
                         : (CurrencyExchangeRateMult ? x.TotalValue / CurrencyExchangeRateValue : x.TotalValue * CurrencyExchangeRateValue))), 2);
            }
        }

        [ReflectionExclude]
        public override decimal CurrencyDebtValue
        {
            get
            {
                var pendingValue = decimal.Zero;
                if (!string.IsNullOrEmpty(ForeignCurrency) && CurrencyId == ForeignCurrency)
                    pendingValue = DocumentForeignValue;
                else
                    pendingValue = Math.Round(GetExchangeRateValue(DocumentValue, CurrencyExchangeRateValue, !CurrencyExchangeRateMult), 2);
                
                return pendingValue - CurrencyTotalPaid
                    - Math.Round(GetExchangeRateValue(TotalFinancialDiscount, CurrencyExchangeRateValue, !CurrencyExchangeRateMult), 2);
            }
        }

        [ReflectionExclude]
        public bool CanModifyDetails
        {
            get { return string.IsNullOrEmpty(InvoiceReferenceSerie); }
        }

        [ReflectionExclude]
        public bool CanModifyPayments
        {
            get { return IsEnabled && string.IsNullOrEmpty(DocumentSerie); }
        }

        [ReflectionExclude]
        public override bool ShowCurrency
        {
            get { return true; }
        }

		[ReflectionExclude]
		public override bool HideCreditPaymentMethods
		{
			get
			{
				return !InvoiceReferenceId.HasValue &&
					PaymentProvider != PaymentServiceProvider.Manual &&
					PaymentProvider != PaymentServiceProvider.ClearOne;
			}
		}

        [ReflectionExclude]
        public override decimal? CreditsMaxValue
        {
            get { return InvoiceTotalCredits - PendingValue; }
        }

        #endregion
        #region Contracts

        public CreditCardContract CreditCard
		{
			get { return _creditCard; }
			set
			{
				_creditCard = value;
				if (CreditCardRefundId.HasValue && _creditCard != null && CreditCards != null)
					FiilCreditCard();
			}
		}

        [ReflectionExclude]
        public TypedList<FiscalDocDetailsContract> Services
        {
            get { return new TypedList<FiscalDocDetailsContract>(DocDetails.OfType<FiscalDocDetailsContract>()
                .Where(x => x.DetailType == DocumentDetail.Service)); }
        }

        [ReflectionExclude]
        public TypedList<PaymentContract> Payments
        {
            get { return new TypedList<PaymentContract>(DocDetails.OfType<PaymentContract>()); }
        }

        #endregion
        #region Constructors

        public CreditNoteContract()
            : base() 
        {
            _paymentMode = PaymentMode.Payment;
        }

        public CreditNoteContract(Guid? invoiceReferenceId, string invoiceReferenceSerie,
			PaymentServiceProvider paymentProvider, CreditCardRefundContract[] creditCards)
            : this()
        {
            InvoiceReferenceId = invoiceReferenceId;
            InvoiceReferenceSerie = invoiceReferenceSerie;
			PaymentProvider = paymentProvider;
			CreditCards = new TypedList<CreditCardRefundContract>(creditCards);
			if (creditCards.Length > 0)
				CreditCardRefundId = (Guid)creditCards[0].Id;
        }

        #endregion
		#region Private Methods

		private void FiilCreditCard()
		{
			var creditCardRefund = _creditCards.FirstOrDefault(x => x.Id.Equals(CreditCardRefundId.Value));
			if (creditCardRefund != null)
			{
				CreditCard.CreditCardTypeId = creditCardRefund.CreditCardTypeId;
				CreditCard.IdentNumber = creditCardRefund.IdentNumber;
				CreditCard.ValidationMonth = creditCardRefund.ValidationMonth;
				CreditCard.ValidationYear = creditCardRefund.ValidationYear;
			}
		}

        #endregion
		#region Public Methods

		public override void AddDetail(Guid departmentId, Guid serviceDepartmentId,
            string description, string addDescription, Guid currentAccountId, short? pensionModeId,
            short quantity, decimal value, string currency)
        {
            var contract = new FiscalDocDetailsContract(AccountExchangeRateValue, AccountExchangeRateMult, AccountExchangeRateCurrency, TaxIncluded);
            contract.DetailType = DocumentDetail.Service;
            contract.Quantity = quantity;
            contract.Description = description;
            contract.DescriptionAdditional = addDescription;
            contract.DepartmentId = departmentId;
            contract.ServiceDepartmentId = serviceDepartmentId;
            contract.CurrentAccountId = currentAccountId;
            contract.PensionModeId = pensionModeId;

            if (string.IsNullOrEmpty(currency) || currency == DocumentCurrencyId)
            {
                contract.ForeignCurrency = null;
                contract.TotalValue = value;
                contract.ForeignTotalValue = null;
            }
            else
            {
                contract.ForeignCurrency = currency;
                if (value == CurrencyDebtValue)
                    contract.TotalValue = DebtValue;
                else
                    contract.TotalValue = GetExchangeRateValue(value, CurrencyExchangeRateValue, CurrencyExchangeRateMult);
                contract.ForeignTotalValue = value;
            }
            contract.UnitValue = quantity > 0 ? contract.TotalValue / quantity : contract.TotalValue;

            DocDetails.Add(contract);

            PendingValue = DocDetails.Where(x => x.DetailType == DocumentDetail.Service).Sum(x => x.TotalValue);
            DocumentValue = PendingValue;
            NotifyPropertyChanged("DocumentValue");
            NotifyPropertyChanged("DocumentForeignValue");
            NotifyPropertyChanged("DebtValue");
        }

        public override void RemoveDetails(Guid[] ids)
        {
            var totalValue = DocDetails.Where(x => ids.Contains((Guid)x.Id)).Sum(x => x.TotalValue);
            PendingValue -= totalValue;
            DocumentValue = PendingValue;
            base.RemoveDetails(ids);

            PendingValue = DocDetails.Where(x => x.DetailType == DocumentDetail.Service).Sum(x => x.TotalValue);
            DocumentValue = PendingValue;
            NotifyPropertyChanged("DocumentValue");
            NotifyPropertyChanged("DocumentForeignValue");
            NotifyPropertyChanged("DebtValue");
        }

        public  void UpdateDetails(Guid id)
        {
            var totalValue = DocDetails.Where(x => x.Id.Equals(id)).Sum(x => x.TotalValue);
            PendingValue -= totalValue;
            DocumentValue = PendingValue;
            PendingValue = DocDetails.Where(x => x.DetailType == DocumentDetail.Service).Sum(x => x.TotalValue);
            DocumentValue = PendingValue;
            NotifyPropertyChanged("DocumentValue");
            NotifyPropertyChanged("DocumentForeignValue");
            NotifyPropertyChanged("DebtValue");
        }

        public override void AddPayment(Guid? withdrawTypeId, string withdrawTypeDesc, Guid? creditCardTypeId,
            string creditCardTypeDesc, Guid? creditCardId, string creditCardNumber, short? month, short? year, string validationCode,
            string holderName, string address, string zipCode, decimal value, string currency, string cardData)
        {
            var contract = new PaymentContract(AccountExchangeRateValue, AccountExchangeRateMult, AccountExchangeRateCurrency, TaxIncluded);
            contract.NewId();
            contract.DetailType = DocumentDetail.Payment;
            contract.WithdrawTypeId = withdrawTypeId;
            contract.WithdrawTypeDesc = withdrawTypeDesc;
            contract.CreditCardTypeId = creditCardTypeId;
            contract.CreditCardTypeDesc = creditCardTypeDesc;
            if (creditCardId.HasValue)
                contract.CreditCardId = creditCardId.Value;
            else if (creditCardTypeId.HasValue)
                contract.CreditCardId = BaseContract.NewGuid();
            contract.CreditCardNumber = creditCardNumber;
            contract.Month = month;
            contract.Year = year;
            contract.ValidationCode = validationCode;
			contract.HolderName = holderName;
			contract.Address = address;
			contract.ZipCode = zipCode;
			contract.CardData = cardData;

            if (string.IsNullOrEmpty(currency) || currency == DocumentCurrencyId)
            {
                contract.ForeignCurrency = null;
                contract.TotalValue = value;
                contract.ForeignTotalValue = null;
            }
            else
            {
                contract.ForeignCurrency = currency;
                if (value == CurrencyDebtValue)
                    contract.TotalValue = DebtValue;
                else
                    contract.TotalValue = GetExchangeRateValue(value, CurrencyExchangeRateValue, CurrencyExchangeRateMult);
                contract.ForeignTotalValue = value;
            }

            DocDetails.Add(contract);

            var debtValue = DebtValue;
            if (debtValue != decimal.Zero && Math.Abs(debtValue) < 0.01M)
                contract.TotalValue += debtValue;
            var currencyDebtValue = CurrencyDebtValue;
            if (currencyDebtValue != decimal.Zero && Math.Abs(currencyDebtValue) < 0.01M)
                contract.ForeignTotalValue += currencyDebtValue;

            NotifyPropertyChanged("TotalPaid");
            NotifyPropertyChanged("DebtValue");
        }

        public override void RemovePayments(Guid[] ids)
        {
            base.RemovePayments(ids);

            NotifyPropertyChanged("TotalPaid");
            NotifyPropertyChanged("DebtValue");
        }

        public override void RemovePayments()
        {
            DocDetails.Remove(x => x.DetailType == DocumentDetail.Payment);
            NotifyPropertyChanged("TotalPaid");
            NotifyPropertyChanged("DebtValue");
        }

        #endregion
    }

	[DataContract]
	public class CreditCardRefundContract : BaseContract
	{
		#region Members

		[DataMember]
		internal Guid _creditCardTypeId;
		[DataMember]
		internal string _identNumber;
		[DataMember]
		internal short? _validationMonth;
		[DataMember]
		internal short? _validationYear;

		#endregion
		#region Constructor

		public CreditCardRefundContract(Guid id, Guid creditCardTypeId, string identNumber, short? validationMonth, short? validationYear)
		{
			Id = id;
			_creditCardTypeId = creditCardTypeId;
			_identNumber = identNumber;
			_validationMonth = validationMonth;
			_validationYear = validationYear;
		}

		#endregion
		#region Properties

		[ReflectionExclude]
		public Guid CreditCardTypeId { get { return _creditCardTypeId; } }
		[ReflectionExclude]
		public string IdentNumber { get { return _identNumber; } }
		[ReflectionExclude]
		public short? ValidationMonth { get { return _validationMonth; } }
		[ReflectionExclude]
		public short? ValidationYear { get { return _validationYear; } }

		[ReflectionExclude]
		public string CreditCard
		{
			get
			{
				if (!string.IsNullOrEmpty(IdentNumber) && IdentNumber.Length > 8)
				{
					var temp = IdentNumber.ToCharArray();
					for (int i = 8; i < (temp.Length - 1); i++)
						temp[i] = '*';
					var identNumber = new string(temp);

					return string.Format("{0}-{1}-{2}-{3}",
						identNumber.Substring(0, 4),
						identNumber.Substring(4, 4),
						identNumber.Substring(8, 4),
						identNumber.Substring(12));
				}

				return null;
			}
		}

		#endregion
	}
}
