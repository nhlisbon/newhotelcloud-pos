﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class ClearOneContract : BaseContract
    {
        public Guid InstallationId {get; set;}
        public int CommandId {get; set;}
        public DateTime IssueDate {get; set;}
        public DateTime IssueTime {get; set;}
        public short Attempts {get; set;}
        public Guid UserId {get; set;}
        public string TicketId {get; set;}
        public string OperatorUser {get; set;}
        public ClearOneOperation OperationType {get; set;}
        public decimal ClearOneValue {get; set;}
        public int NumberTransationCancel {get; set;}
        public int NumberCancellation {get; set;}
        public string AutorizationCode {get; set;}
        public int CodeResult {get; set;}
        public string DescriptionReceived {get; set;}
        public decimal NumberReceived {get; set;}
        public string ApplicationChip {get; set;}
        public string ApplicationChipDesc {get; set;}
        public string CardNumber {get; set;}
        public string TradeNumber {get; set;}
        public string CardMark {get; set;}
        public string HolderCard {get; set;}
        public string BankName {get; set;}
        public string ARCDesc {get; set;}
        public bool SignatureRequired  {get; set;}
        public string LocalRef {get; set;}
        public string OperatorRef {get; set;}
        public bool AttemtpTreated {get; set;}
        public DateTime? DateAttemtpTreated {get; set;}
        public DateTime? TimeAttemtpTreated {get; set;}
    }
}