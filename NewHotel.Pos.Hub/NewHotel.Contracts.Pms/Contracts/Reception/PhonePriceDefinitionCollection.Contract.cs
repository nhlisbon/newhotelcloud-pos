﻿using System;

namespace NewHotel.Contracts
{
    public class PhonePriceDefinitionCollectionContract
    {
        public int Id { get; set; }
        public string PriceDefinition { get; set; }
        public decimal PriceValue { get; set; }
    }
}