﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Contracts
{
    public class BasePhoneRequest
    {
        public string HotelID { get; set; }
        public string ExtensionID { get; set; }
    }


    public class PhoneCallRequestContract : BasePhoneRequest
    {
        public string CallNumber { get; set; }
        public DateTime CallDate { get; set; }
        public DateTime CallTime { get; set; }
        public TimeSymbol TimeSymbol { get; set; }
        public long CallPulse { get; set; }
        public string CallLenght { get; set; }
        public PhonePriceDefinitionCollectionContract[] PriceIDs { get; set; }
    }

    public class PhonePriceDefinitionCollectionContract
    {
        public int ID { get; set; }
        public string PriceDefinition { get; set; }
        public decimal PriceValue { get; set; }
    }
}
