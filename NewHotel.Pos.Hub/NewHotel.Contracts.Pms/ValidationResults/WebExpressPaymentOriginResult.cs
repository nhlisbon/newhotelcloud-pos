﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class WebExpressPaymentOriginResult : ValidationResult
    {
        [DataMember]
        public TypedList<WebExpressPaymentOriginContract> Origins { get; internal set; }

        public WebExpressPaymentOriginResult()
            : base()
        {
            Origins = new TypedList<WebExpressPaymentOriginContract>();
        }
    }
}