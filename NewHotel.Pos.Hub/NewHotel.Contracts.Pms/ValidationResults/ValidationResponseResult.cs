﻿using NewHotel.Web.Services;
using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ValidationResponseResult : ValidationResult
    {
        [DataMember]
        public BaseResponse Response{get;set;}

        public ValidationResponseResult()
            : base()
        {
            
        }
    }
}