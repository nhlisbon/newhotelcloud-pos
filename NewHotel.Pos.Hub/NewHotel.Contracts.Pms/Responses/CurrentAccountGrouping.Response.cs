﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class CurrentAccountGroupingResponse : QueryResponse
    {
        #region Properties

        [DataMember]
        public CurrentAccountGroupingFlags DefaultFlags { get; internal set; }

        #endregion
        #region Constructor

        public CurrentAccountGroupingResponse(QueryResponse qr, CurrentAccountGroupingFlags flags)
            : base(qr.Type, qr)
        {
            DefaultFlags = flags;
        }

        #endregion
    }
}
