﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PensionModesByInstallationResponse : QueryResponse
    {
        #region Properties

        [DataMember]
        public short? ReservationPensionModeId { get; internal set; }

        #endregion
        #region Constructor

        public PensionModesByInstallationResponse(QueryResponse qr, short? reservationPensionModeId)
            : base(qr.Type, qr)
        {
            ReservationPensionModeId = reservationPensionModeId;
        }

        #endregion
    }
}
