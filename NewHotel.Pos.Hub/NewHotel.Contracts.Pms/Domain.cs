﻿using System;

namespace NewHotel.Contracts
{
    #region Guest Operations

    public enum GuestOperation : long
    {
        CheckIn,
        CheckOut,
        Registration
    };

    #endregion
    #region CurrentAccount Status

    public enum CurrentAccountStatus : long
    {
        Created = 2,
        Opened = 11,
        Closed = 6,
        Locked = 5
    }

    #endregion
    
   
    #region Navigator Options

    public enum NavigatorOptions : long
    {
        All = 1,
        Select = 2,
        SelectPrint = 3,
        AddModifyDelete = 4,
        AddModifyDeletePrint = 5
    }

    #endregion
    #region No-Show Status

    public enum ENoShowStatus : long
    {
        NoShow,
        UndoNoShow,
        ModifyNoShow
    }

    #endregion
    #region ReportFilter

    public enum ForecastType : long
    {
        ForecastDaily = 1,
        ForecastWeekly = 2,
        ForecastMonthly = 3,
        GuestAgePiramide = 4,
        ExpectedIncome = 5,
        IncomeMonthly = 56
    }

    #endregion

    /// <summary>
    /// Filtros Fecha (= <> >= <= between)
    /// </summary>
    public enum FilterDate : long { Equal = 2221, NotEqual = 2222, LessThan = 2223, LessThanOrEqual = 2224, GreaterThan = 2225, GreaterThanOrEqual = 2226, Between = 2228 };

    /// <summary>
    /// TNHT_CHBS.CHBS_ATYP = Tipo Disponibilidad (Room Type, Room Block)
    /// </summary>
    public enum AvailabilityType : long { RoomType = 2820, RoomBlock = 2821 };

    /// <summary>
    /// LCRE.LCRE_UNMO = Moneda Base/Suplementaria
    /// </summary>
    public enum CurrencyMode : long { Base = 1381, Suplementary = 1382 };

    /// <summary>
    /// TPDI.TPDI_TIPO = Tipo Tarifa (Diaria, Intervalo tiempo)
    /// </summary>
    public enum MethodPrice : long { Daily = 481, TimeIntervals = 482 };

    /// <summary>
    /// CFG_RESE.VOUC_DUPL = Verificación de los Voucher
    /// </summary>
    public enum VoucherDuplication : long { Verify = 2071, VerifyExcludingCancellations = 2072 };

    /// <summary>
    /// NO FIELD = Tipo Proximidad Habitaciones (Todas, Elevador, Escaleras, Ama LLaves)
    /// </summary>
    public enum ProximityRoomFilter : long { All = 1, Elevator = 1741, Stairs = 1742, Housekeeping = 1743 };

    /// <summary>
    /// NO FIELD = Tipo Descuento (Valor, Porciento)
    /// </summary>
    public enum EDiscountType : long { Value = 2291, Percent = 2292 };

    /// <summary>
    /// NO FIELD = Status Inactividades (Activa, Fuera Servicio)
    /// </summary>
    public enum EInactivityStatus : long { Active = 721, OutOfOrder = 2461 };

    /// <summary>
    /// NO FIELD = Ocupacion Recurso (Ocupado, Disponible)
    /// </summary>
    public enum EResourceOccupancy : long { Occupied = 1981, Available = 2491 };
   
    /// <summary>
    /// DDOT.DDOT_TIPO = Tipo Template
    /// </summary>
    public enum TemplateType : long { MailLetter = 1051, SingleConfirmation = 1052, Labels = 1053, Cardex = 1054, GroupConfirmation = 1055, DepositRequest = 1056, Ticket = 1057 };

    /// <summary>
    /// Filtro Reporte GuestForeigners (Arrivals, Departures, SleepNights)
    /// </summary>
    public enum GuestForeignersFilter : long { Arrivals = 9, Departures = 10, SleepNights = 303 };

    /// <summary>
    /// TNHT_CKIO.CKIO_COMA  = Interaccion con NewGes (Open Line , Close Line, ChangeData, ChangeRoom, Open Line without Reservation, Close ine without Resrvation, Do Not Disturb, Alarm clock)
    /// </summary>
    public enum NewGesInteractionType : long { OpenLine = 2851, CloseLine = 2852, ChangeData = 2853, ChangeRoom = 2854, OpenLinewithoutReservation = 2855, CloseLinewithoutReservation = 2856, DoNotDisturb = 2857, Alarmclock = 2858 };

    /// <summary>
    /// TNHT_TEMP.TEMP_TIPO = Tipos de Templates (Reserved, CheckIn, CheckOut)
    /// </summary>
    public enum NotificationType : long { Reserved = 5001, CheckIn = 5002, CheckOut = 5003 };

}