﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewHotel.Pos.Integration.Http.Models
{
    public class HttpConfig
    {
        public string Url { get; set; }
        public KeyValuePair<string, string>? Headers { get; set; }
    }
}
