﻿using NewHotel.Pos.Integration.Http.Models;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Text.Json;

namespace NewHotel.Pos.Integration.Http.Http
{
    public class Client : IDisposable
    {
        static readonly Lazy<HttpClientHandler> handler = new Lazy<HttpClientHandler>(() => new HttpClientHandler());
        private readonly HttpClient _httpClient;
        private readonly HttpConfig _config;
        public HttpClient HttpClient => _httpClient ?? GetClient();
        
        public Client(HttpConfig config)
        {
            _config = config;
            _httpClient = GetClient();
        }

        private HttpClient GetClient()
        {
            var client = new HttpClient(handler.Value)
            {
                BaseAddress = new Uri(_config.Url)
            };
            client.DefaultRequestHeaders.Add(_config.Headers.Value.Key, _config.Headers.Value.Value);
            return client;
        }

        public void Dispose()
        {
            HttpClient?.Dispose();
        }
    }

    public static class ClientExtension
    {
        static readonly JsonSerializerOptions options = new JsonSerializerOptions
        {
            DefaultIgnoreCondition = System.Text.Json.Serialization.JsonIgnoreCondition.WhenWritingNull
        };

        public static string Serialize<T>(this T obj)
		{
			return JsonSerializer.Serialize(obj, options);
		}

        public static T Deserialize<T>(string value)
        {
			return JsonSerializer.Deserialize<T>(value);
		}

		public static async Task<T> GetAsync<T>(this Client client, string url, CancellationToken cancellationToken = default)
        {
            var response = await client.HttpClient.GetAsync(url, cancellationToken);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            return Deserialize<T>(content);
        }

        public static async Task<TRes> PostAsync<TReq, TRes>(this Client client, string url, TReq data, CancellationToken cancellationToken = default)
        {
            var json = Serialize(data);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.HttpClient.PostAsync(url, content, cancellationToken);
            response.EnsureSuccessStatusCode();
            var responseContent = await response.Content.ReadAsStringAsync();
            //return Deserialize<TRes>(responseContent);
            try
            {
                var res = Deserialize<TRes>(responseContent);
                return res;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return Deserialize<TRes>(responseContent);
            }
        }

        public static async Task<T> PutAsync<T>(this Client client, string url, object data, CancellationToken cancellationToken = default)
        {
            var json = Serialize(data);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.HttpClient.PutAsync(url, content, cancellationToken);
            response.EnsureSuccessStatusCode();
            var responseContent = await response.Content.ReadAsStringAsync();
            return Deserialize<T>(responseContent);
        }

        public static async Task<T> DeleteAsync<T>(this Client client, string url, CancellationToken cancellationToken = default)
        {
            var response = await client.HttpClient.DeleteAsync(url, cancellationToken);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            return Deserialize<T>(content);
        }
    }
}
