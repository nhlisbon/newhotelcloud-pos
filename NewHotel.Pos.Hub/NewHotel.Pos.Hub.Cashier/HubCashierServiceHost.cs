﻿using NewHotel.Pos.Hub.Core;
using System.ComponentModel.Composition;

namespace NewHotel.Pos.Hub.Cashier
{
	[Export(typeof(IHubHost)), PartCreationPolicy(CreationPolicy.NonShared)]
    public sealed class HubCashierServiceHost : HubServiceHost, IHubHost
    {
        #region Constructor

        public HubCashierServiceHost()
            : base("NewHotel.Pos.Hub", typeof(Service))
        {
        }

        #endregion
    }
}