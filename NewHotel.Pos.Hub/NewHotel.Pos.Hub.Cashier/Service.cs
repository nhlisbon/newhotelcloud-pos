﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using NewHotel.Core;
using NewHotel.Pos.Communication.Behavior;
using NewHotel.Pos.Core;
using NewHotel.Pos.Hub.Business.Business;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Common;
using NewHotel.Pos.Hub.Contracts.Common.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.Records;
using NewHotel.Pos.Hub.Contracts.Common.Records.Tickets;
using NewHotel.Pos.Hub.Core;
using NewHotel.Pos.Hub.Core.Logs;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Hub.Model.Cashier.Contracts.Settings;
using NewHotel.Pos.Hub.Model.WaitingListStand.Contract;
using NewHotel.Pos.Hub.Model.WaitingListStand.Record;
using NewHotel.Pos.IoC;
using ClosedTicketRecord = NewHotel.Pos.Hub.Model.ClosedTicketRecord;
using DocumentSerieRecord = NewHotel.Pos.Hub.Model.DocumentSerieRecord;
using HotelRecord = NewHotel.Pos.Hub.Model.HotelRecord;
using ProductRecord = NewHotel.Pos.Hub.Contracts.Common.Records.Products.ProductRecord;
using SaloonRecord = NewHotel.Pos.Hub.Model.SaloonRecord;
using UserRecord = NewHotel.Pos.Hub.Model.UserRecord;
using NewHotel.Pos.Hub.Contracts.Common.Records.Stands;
using System.IO.Compression;
using NewHotel.Pos.Hub.Contracts.Common.Records.CurrentAccount;
using NewHotel.Pos.Hub.Contracts.Handheld.Contracts;
using NewHotel.Pos.Hub.Contracts.PaySystem;
using System.Threading.Tasks;
using NewHotel.Pos.Integration.Http.Http;

namespace NewHotel.Pos.Hub.Cashier
{
    public class Service : CommonService, IService
    {
		#region DocumentSerie

		public SerialNumberResult GetNextSerialNumber(string cashierId, string standId, string justSerie, string docType, string workDate)
        {
            using IDocumentSerieBusiness business = GetBusiness<IDocumentSerieBusiness>();
            return business.GetNextSerialNumber(cashierId.AsGuid(), standId.AsGuid(), justSerie.AsBoolean(), docType.AsLong(), true);
        }

        #endregion
        #region Stands

        public List<CashierRecord> GetCashiers(string hotelId, string userId, string language)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetCashiers(hotelId.AsGuid(), userId.AsGuid(), language.AsInteger());
        }

        public List<POSStandRecord> GetStands(string cajaId, string language)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetStands(cajaId.AsGuid(), language.AsInteger());
        }

        public List<LiteStandRecord> GetStandsForTransfer(string language)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetStandsForTransfer(language.AsInteger());
        }

        public ValidationResult TransferTicket(string ticket, string transfer)
        {
            using var business = GetBusiness<IStandBusiness>();
            var ticketContract = Serializer.Deserialize<POSTicketContract>(ticket);
            var transferContract = Serializer.Deserialize<TransferContract>(transfer);
            var result = business.TransferTicket(ticketContract, transferContract);
            if (result.IsEmpty)
                BusinessContext.Publish("TransferTicket", transferContract);
            return result;
        }

        public ValidationContractResult VerifyTransferToAccept(string standId, string language)
        {
            using var business = GetBusiness<IStandBusiness>();
            HubSessionData hubSessionData = GetHubSessionData(HubContextCache.Current.Token);
            return business.VerifyTransferToAccept(standId.AsGuid(), hubSessionData.InstallationId, int.Parse(language));
        }

        public ValidationContractResult VerifyTransferToReturn(string standId, string language)
        {
            using var business = GetBusiness<IStandBusiness>();
            HubSessionData hubSessionData = GetHubSessionData(HubContextCache.Current.Token);
            return business.VerifyTransferToReturn(standId.AsGuid(), hubSessionData.InstallationId, int.Parse(language));
        }

        public ValidationTicketsResult AcceptTransferedTickets(string transfers)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var lTransfers = Serializer.Deserialize<List<TransferContract>>(transfers);
            return business.AcceptTransferedTickets(lTransfers);
        }

        public ValidationTicketsResult ReturnTransferedTickets(string transfers)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.ReturnTransferedTickets(Serializer.Deserialize<List<TransferContract>>(transfers));
        }

        public ValidationItemResult<StandEnvironment> GetStandEnvironment(string standId, string language)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetStandEnvironment(standId.AsGuid(), language.AsInteger());
        }

        public List<POSStandRecord> GetStandsFromCloud(string language)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetStandsFromCloud(language.AsInteger());
        }

        public ValidationResult CloseDayValidations(string standId, string cashierId, string closeDay)
        {
            using var business = GetBusiness<IStandBusiness>();
            var stand = Serializer.Deserialize<Guid>(standId);
            var cashier = Serializer.Deserialize<Guid>(cashierId);
            var date = Serializer.Deserialize<DateTime>(closeDay);
            return business.CloseDayValidations(stand, cashier, date);
        }

        public ValidationContractResult CloseDay(string standId, string cajaId, string closeDay)
        {
            using var business = GetBusiness<IStandBusiness>();
            var stand = Serializer.Deserialize<Guid>(standId);
            var caja = Serializer.Deserialize<Guid>(cajaId);
            var date = Serializer.Deserialize<DateTime>(closeDay);

            var result = business.CloseDay(stand, caja, date);
            if (result.IsEmpty)
                InvalidateAllSessionDataAndResetIt();
            
            return result;
        }

        public ValidationDateResult UndoCloseDay(string standId, string cajaId)
        {
            using var business = GetBusiness<IStandBusiness>();
            var stand = Serializer.Deserialize<Guid>(standId);
            var caja = Serializer.Deserialize<Guid>(cajaId);

            var result = business.UndoCloseDay(stand, caja);
            if (result.IsEmpty)
                InvalidateAllSessionDataAndResetIt();
            
            return result;
        }

        public ValidationContractResult CloseTurn(string standId, string cajaId)
        {
            using var business = GetBusiness<IStandBusiness>();
            Guid stand = standId.AsGuid();
            Guid caja = cajaId.AsGuid();

            return business.CloseTurn(stand, caja);
        }

        public ValidationItemResult<UserRecord> GetUserByCode(string userCode)
        {
            //using var scope = CwFactory.Instance.BeginScope();
            using var business = GetBusiness<IStandBusiness>();
            return business.GetUserByCode(userCode);
        }

        public ValidationItemResult<UserRecord> GetUserByName(string loginUser)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetUserByName(loginUser);
        }

        public ValidationBooleanResult CheckUserPermission(string userId, string permission)
        {
            using var business = GetBusiness<IStandBusiness>();
            var userIdObj = Serializer.Deserialize<Guid>(userId);
            var permissionObj = Serializer.Deserialize<Permissions>(permission);
            return business.CheckUserPermission(userIdObj, permissionObj);
        }

        public List<SaloonRecord> GetSaloonsByStand(string standId, string language)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetSaloonsByStand(standId.AsGuid(), int.Parse(language));
        }

        public List<TableRecord> GetTablesBySaloon(string saloonId)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetTablesBySaloon(saloonId.AsGuid());
        }

        public ValidationItemSource<ReservationRecord> GetReservations(ReservationFilterContract filter)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetReservations(filter);
        }

        public ValidationItemSource<EventReservationSearchFromExternalRecord> GetEventReservations(string installationId)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetEventReservations(installationId.AsGuidX());
        }

        public ValidationItemSource<SpaReservationSearchFromExternalRecord> GetSpaReservations(string installationId)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetSpaReservations(new SpaSearchReservationFilterModel
            {
                HotelId = installationId.AsGuidX()
            });
        }

        public ValidationItemSource<SpaReservationSearchFromExternalRecord> GetSpaReservationsV2(SpaSearchReservationFilterModel filter)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetSpaReservations(filter);
        }

        public ValidationItemResult<SpaServiceRecord> GetSpaServices(string installationId, string spaId)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetSpaServices(installationId.AsGuidX(), spaId.AsGuid());
        }

        public ValidationItemSource<PmsCompanyRecord> GetEntityInstallations(PmsCompanyFilterModel filter)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetEntityInstallations(filter);
        }

        public ValidationItemSource<ReservationsGroupsRecord> GetReservationsGroups( string installationId)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetReservationsGroups(installationId.AsGuidX());
        }

        public ValidationItemSource<PmsControlAccountRecord> GetControlAccounts(PmsControlAccountFilterModel filter)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetControlAccounts(filter);
        }

        public ValidationItemSource<ControlAccountMovementDto> GetControlAccountMovements(ControlAccountMovementsFilterModel filter)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetControlAccountMovements(filter);
        }

        public ValidationItemSource<ClientInstallationRecord> GetClientInstallations(string openAccounts, string clientId, string room, string fullName, string installationId)
        {
            using var business = GetBusiness<IStandBusiness>();
            if (bool.TryParse(openAccounts, out bool openAcc))
                return business.GetClientInstallations(openAcc, clientId.AsGuidX(), room, fullName, installationId.AsGuidX());
            
            return business.GetClientInstallations();
        }

        public ValidationItemSource<ClientRecord> GetClientEntities(string queryRequest)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetClientEntities(Serializer.Deserialize<QueryRequest>(queryRequest));
        }

        public ValidationResult UpdateMesa(string contract)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.UpdateMesa(Serializer.Deserialize<MesaContract>(contract));
        }

        public ValidationItemSource<ClosedTicketRecord> GetOldTickets(string standId, string cashierId, string date)
        {
            using var business = GetBusiness<IStandBusiness>();
            var stand = Serializer.Deserialize<Guid?>(standId);
            var cashier = Serializer.Deserialize<Guid?>(cashierId);
            var closeDate = Serializer.Deserialize<DateTime?>(date);
            return business.GetOldTickets(stand, cashier, closeDate);
        }

        public List<SalesCashierRecord> GetSalesReport(string queryRequest, string isTurnReport)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetSalesReport(Serializer.Deserialize<QueryRequest>(queryRequest), Serializer.Deserialize<bool>(isTurnReport));
        }

        public ValidationItemSource<SalePaymentRecord> GetPaymentsReport(string queryRequest)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetPaymentsReport(Serializer.Deserialize<QueryRequest>(queryRequest));
        }

        public TicketRContract LoadTicketReal(string ticketId)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.LoadTicketReal(ticketId.AsGuid());
        }

        #region Table Reservation

        public ValidationItemSource<LiteTablesReservationRecord> GetTableReservations(string filter)
        {
            using var business = GetBusiness<IReservationTableBusinessCloud>();
            var filterContract = Serializer.Deserialize<TablesReservationFilterContract>(filter);
            return business.GetTableReservations(filterContract);
        }

        public ValidationResult CancelTableReservation(string id, string cancellationReasonId, string comments)
        {
            using var business = GetBusiness<IReservationTableBusinessCloud>();
            var tableReservationId = Serializer.Deserialize<Guid>(id);
            var cancellationReason = Serializer.Deserialize<Guid>(cancellationReasonId);
            return business.CancelBookingTable(tableReservationId, cancellationReason, comments);
        }

        public ValidationItemSource<StandTimeSlotRecord> GetStandTimeSlots(string standId, string date)
        {
            using var business = GetBusiness<IReservationTableBusinessCloud>();
            var gStandId = Serializer.Deserialize<Guid>(standId);
            var dDate = Serializer.Deserialize<DateTime>(date);
            return business.GetStandTimeSlots(gStandId, dDate);
        }

        #endregion

        #region Waiting List

        public ValidationItemSource<PaxWaitingListRecord> GetWaitingList(string waitingListFilterContract)
        {
            using var business = GetBusiness<IPaxWaitingListBusiness>();
            var filter = Serializer.Deserialize<WaitingListFilterContract>(waitingListFilterContract);
            return business.GetWaitingList(filter);
        }

        public ValidationContractResult NewPaxWaitingList()
        {
            using var business = GetBusiness<IPaxWaitingListBusiness>();
            return business.NewPaxWaitingList();
        }

        public ValidationContractResult LoadPaxWaitingList(string id)
        {
            using var business = GetBusiness<IPaxWaitingListBusiness>();
            return business.LoadPaxWaitingList(Serializer.Deserialize<Guid>(id));
        }

        public ValidationContractResult PersistPaxWaitingList(string contract)
        {
            using var business = GetBusiness<IPaxWaitingListBusiness>();
            return business.PersistPaxWaitingList(Serializer.Deserialize<PaxWaitingListContract>(contract));
        }

        public ValidationResult DeletePaxWaitingList(string id)
        {
            using var business = GetBusiness<IPaxWaitingListBusiness>();
            return business.DeletePaxWaitingList(Serializer.Deserialize<Guid>(id));
        }

        #endregion

        #region Meals Control

        public ValidationItemSource<MealsControlRecord> GetMealsControl(MealsFilterModel model)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetMealsControl(model);
        }

        public ValidationResult UpdateMealsControl(string guestId, string breakfast, string lunch, string dinner, string installationId)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.UpdateMealsControl(guestId.AsGuid(), breakfast.AsBoolean(), lunch.AsBoolean(), dinner.AsBoolean(), installationId.AsGuidX());
        }

        public ValidationItemResult<ClientInfoContract> GetClientInfo(string clientId)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetClientInfo(clientId.AsGuid());
        }

        #endregion

        #endregion
        #region Multi Hotel

        public ValidationItemSource<ChargeHotelRecord> GetChargeHotels()
        {
            using var business = GetBusiness<ICloudBusiness>();
            return business.GetChargeHotels();
        }

        #endregion
        #region Products

        public ValidationItemResult<ImageRecord> GetImage(string id)
        {
            if (id == null)
                return null;
            using var business = GetBusiness<IProductBusiness>();
            return business.GetImage(id);
        }

        public List<ImageRecord> GetProductsImages(string hotelId, string standId, string rateId, string taxSchema, string language)
        {
            using var business = GetBusiness<IProductBusiness>();
            List<ImageRecord> result = new List<ImageRecord>();
            List<ProductRecord> products = business.GetProductsByStandRate(hotelId.AsGuid(), standId.AsGuid(), language.AsInteger());
            foreach (var product in products)
            {
                ValidationItemResult<ImageRecord> imageResult = GetImage(product.Image);
                if (imageResult.IsEmpty) result.Add(imageResult.Item);
            }
            return result;
        }

        public ValidationItemSource<ProductPriceRecord> GetProductPrices(string standId, string rateId)
        {
            using var business = GetBusiness<IProductBusiness>();
            ValidationItemSource<ProductPriceRecord> result = business.GetProductPrices(standId.AsGuid(), rateId.AsGuid());
            return result;
        }

        public List<ProductRecord> GetProductsByStandandRate(string hotelId, string standId, string rateId, string taxSchema, string language)
        {
            using var business = GetBusiness<IProductBusiness>();
            // TODO: No rate, no taxSchema are used
            return business.GetProductsByStandRate(hotelId.AsGuid(), standId.AsGuid(), language.AsInteger());
        }

        public List<GroupRecord> GetProductGroups(string hotelId, string standId, string language)
        {
            using var business = GetBusiness<IProductBusiness>();
            return business.GetProductGroups(hotelId.AsGuid(), standId.AsGuid(), language.AsInteger());
        }

        public List<FamilyRecord> GetProductFamilies(string hotelId, string standId, string language)
        {
            using var business = GetBusiness<IProductBusiness>();
            return business.GetProductFamilies(hotelId.AsGuid(), standId.AsGuid(), language.AsInteger());
        }

        public List<SubFamilyRecord> GetProductSubFamilies(string hotelId, string standId, string language)
        {
            using var business = GetBusiness<IProductBusiness>();
            return business.GetProductSubFamilies(hotelId.AsGuid(), standId.AsGuid(), language.AsInteger());
        }

        public List<PreparationRecord> GetPreparationsByProducts(string productId, string language)
        {
            using var business = GetBusiness<IProductBusiness>();
            return business.GetPreparationsByProducts(productId.AsGuid(), language.AsInteger());
        }

        public List<AreasRecord> GetAreasByProducts(string standId, string productId, string language)
        {
            using var business = GetBusiness<IProductBusiness>();
            return business.GetAreasByProducts(standId.AsGuid(), productId.AsGuid(), language.AsInteger());
        }

        public List<AreasRecord> GetAreasByStand(string standId, string language)
        {
            using var business = GetBusiness<IProductBusiness>();
            return business.GetAreasByStand(standId.AsGuid(), language.AsInteger());
        }

        public decimal? GetProductPrice(string standId, string productId, string rateId)
        {
            using var business = GetBusiness<IProductBusiness>();
            return business.GetProductPrice(standId.AsGuid(), productId.AsGuid(), rateId.AsGuid());
        }

        #endregion
        #region Tickets

        #region Print

        public POSTicketPrintConfigurationRecord GetTicketPrintConfiguration(string configurationId, string language)
        {
            using var business = GetBusiness<ITicketBusiness>();
            Guid gConfigurationId = configurationId.AsGuid();
            int iLanguage = language.AsInteger();
            return business.GetTicketPrintConfiguration(gConfigurationId, iLanguage);
        }

        public ExportOfficialDocumentResult GetPdfInvoiceReport(string ticketId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var gTicketId = Serializer.Deserialize<Guid>(ticketId);
            return business.GetPdfInvoiceReport(gTicketId);
        }

        #endregion

        public List<DocumentSerieRecord> GetTicketSerieByStandCashier(string standId, string cashierId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.GetTicketSerieByStandCashier(standId.AsGuid(), cashierId.AsGuid());
        }

        public ValidationContractResult NewTicket(string hotelId, string cajaId, string standId, string tableId, string utilId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.NewTicket(hotelId.AsGuid(), standId.AsGuid(), cajaId.AsGuid(), tableId.AsGuidX(), utilId.AsGuid());
        }

        public ValidationContractResult CreateEmptyTicketToReservationTable(string tableIds, string reservationTableId, string name)
        {
            using var business = GetBusiness<ITicketBusiness>();
            Guid[] table = Serializer.Deserialize<Guid[]>(tableIds);
            Guid reservationTable = Serializer.Deserialize<Guid>(reservationTableId);
            return business.CreateEmptyTicketToReservationTable(table, reservationTable, name);
        }

        public ValidationContractResult CreateEmptyTicketToPaxWaitingList(string tableId, string paxWaitingListId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var gTableId = Serializer.Deserialize<Guid>(tableId);
            var gPaxWaitingListId = Serializer.Deserialize<Guid>(paxWaitingListId);
            return business.CreateEmptyTicketToPaxWaitingList(gTableId, gPaxWaitingListId);
        }

        public ValidationContractResult LoadTicketForEdition(string ticketId, bool forceUnlock)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var gTicketId = Serializer.Deserialize<Guid>(ticketId);
            return business.LoadTicketForEdition(gTicketId, forceUnlock);
        }

        public ValidationContractResult SaveTicketFromEdition(string ticketId, string paxs)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var gTicketId = Serializer.Deserialize<Guid>(ticketId);
            var sPaxs = Serializer.Deserialize<short>(paxs);
            return business.SaveTicketFromEdition(gTicketId, sPaxs);
        }

        public ValidationContractResult MoveTicketToTableById(string ticketId, string tableId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.MoveTicketToTable(ticketId.AsGuid(), tableId.AsGuidX());
        }

        public ValidationContractResult MoveTicketToTable(string ticketContract, string tableId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var cTicket = Serializer.Deserialize<POSTicketContract>(ticketContract);
            var gTableId = Serializer.Deserialize<Guid?>(tableId);
            return business.MoveTicketToTable(cTicket, gTableId);
        }

        public ValidationContractResult PersistentProductLine(string contract, string hotelId, string schemaId, bool included)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var productLine = Serializer.Deserialize<POSProductLineContract>(contract);
            return business.PersistProductLine(productLine, hotelId.AsGuid(), schemaId.AsGuid(), included);
        }

        public ValidationContractResult MergeTickets(string originalTicket, string voidTicket)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var cOrignalTicket = Serializer.Deserialize<POSTicketContract>(originalTicket);
            var cVoidTicket = Serializer.Deserialize<POSTicketContract>(voidTicket);
            return business.MergeTickets(cOrignalTicket, cVoidTicket);
        }

        public ValidationContractResult MergeTicketsById(string originalTicketId, string voidTicketId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var gOrignalTicketId = Serializer.Deserialize<Guid>(originalTicketId);
            var gVoidTicketId = Serializer.Deserialize<Guid>(voidTicketId);
            return business.MergeTicketsById(gOrignalTicketId, gVoidTicketId);
        }

        public ValidationContractResult SplitTicketProduct(string ticket, string productLineId, string quantity)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var cTicket = Serializer.Deserialize<POSTicketContract>(ticket);
            var gProductLineId = Serializer.Deserialize<Guid>(productLineId);
            var iQuantity = Serializer.Deserialize<int>(quantity);
            return business.SplitTicketProduct(cTicket, gProductLineId, iQuantity);
        }

        public ValidationContractResult ChangeTicketProductDetails(string ticket, string productLineId, string editedLine)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var cTicket = Serializer.Deserialize<POSTicketContract>(ticket);
            var gProductLineId = Serializer.Deserialize<Guid>(productLineId);
            var cEditedLine = Serializer.Deserialize<POSProductLineContract>(editedLine);
            return business.ChangeTicketProductDetails(cTicket, gProductLineId, cEditedLine);
        }

        public ValidationContractResult DispatchTicketOrders(string ticket, string productLineIds)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var cTicket = Serializer.Deserialize<POSTicketContract>(ticket);
            var lProductLineIds = Serializer.Deserialize<List<Guid>>(productLineIds);
            return business.DispatchTicketOrders(cTicket, lProductLineIds);
        }

        public ValidationContractResult VoidDispatchTicketOrders(string ticket, string productLineIds)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var cTicket = Serializer.Deserialize<POSTicketContract>(ticket);
            var lProductLineIds = Serializer.Deserialize<List<Guid>>(productLineIds);
            return business.VoidDispatchTicketOrders(cTicket, lProductLineIds);
        }

        public ValidationContractResult AwayDispatchTicketOrders(string ticket, string productLineIds)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var cTicket = Serializer.Deserialize<POSTicketContract>(ticket);
            var lProductLineIds = Serializer.Deserialize<List<DispatchContract>>(productLineIds);
            return business.AwayDispatchTicketOrders(cTicket, lProductLineIds);
        }

        public SplitTicketResult SplitTicketManual(string ticketId, string ordersToLeave)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var gTicketId = Serializer.Deserialize<Guid>(ticketId);
            var mOrdersToLeave = Serializer.Deserialize<KeyValuePair<Guid, decimal>[]>(ordersToLeave);
            return business.SplitTicketManual(gTicketId, mOrdersToLeave);
        }

        public SplitTicketResult SplitTicketAuto(string ticketId, string quantity)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var gTicketId = Serializer.Deserialize<Guid>(ticketId);
            int iQuantity = Serializer.Deserialize<int>(quantity);
            return business.SplitTicketAuto(gTicketId, iQuantity);
        }

        public SplitTicketResult SplitTicketByAlcoholicGroup(string ticketId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var gTicketId = Serializer.Deserialize<Guid>(ticketId);
            return business.SplitTicketByAlcoholicGroup(gTicketId);
        }

        public ValidationResult PersistentPaymentLine(string contract)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var cPaymentLine = Serializer.Deserialize<PaymentLineContract>(contract);
            return business.PersistentPaymentLine(cPaymentLine);
        }

        public ValidationContractResult AddProductLineToTicket(string ticket, string productLine)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var cTicket = Serializer.Deserialize<POSTicketContract>(ticket);
            var cProductLine = Serializer.Deserialize<POSProductLineContract>(productLine);
            return business.AddProductLineToTicket(cTicket, cProductLine);
        }

        public ValidationContractResult AddProductToTicket(string ticket, string product, string quantity, string manualPrice, string manualPriceDesc, string observations)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var cTicket = Serializer.Deserialize<POSTicketContract>(ticket);
            var cProduct = Serializer.Deserialize<ProductRecord>(product);
            var iQuantity = Serializer.Deserialize<int>(quantity);
            var dManualPrice = Serializer.Deserialize<decimal?>(manualPrice);
            return business.AddProductToTicket(cTicket, cProduct, iQuantity, dManualPrice, manualPriceDesc, null, false, true, observations);
        }

        public ValidationContractResult CreateLookupTable(string ticket)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var cTicket = Serializer.Deserialize<POSTicketContract>(ticket);
            return business.CreateLookupTable(cTicket);
        }

        public ValidationResult DeleteProductLine(string productLineId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.DeleteProductLine(productLineId);
        }

        public ValidationResult CancelProductLine(string productLineId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.CancelProductLine(productLineId);
        }

        public ValidationResult DeletePaymentLine(string paymentLineId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.DeletePaymentLine(paymentLineId);
        }

        public ValidationResult CancelPaymentLine(string paymentLineId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.CancelPaymentLine(paymentLineId);
        }

        public ValidationResult ValidateTicket(string contract)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var cTicket = Serializer.Deserialize<POSTicketContract>(contract);
            return business.ValidateTicket(cTicket);
        }

        public ValidationContractResult PersistTicket(string contract)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var cTicket = Serializer.Deserialize<POSTicketContract>(contract);
            return business.PersistTicket(cTicket);
        }

        public ValidationContractResult CloseTicket(string contract, string tipProduct, string saveClient)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var cTicket = Serializer.Deserialize<POSTicketContract>(contract);
            var tipProductRecord = string.IsNullOrEmpty(tipProduct) ? null : Serializer.Deserialize<ProductRecord>(tipProduct);
            var saveClientBool = !string.IsNullOrEmpty(tipProduct) && Serializer.Deserialize<bool>(saveClient);
            return business.CloseTicket(cTicket, tipProductRecord, saveClientBool);
        }

        public ValidationResult SyncronizeTicket(string ticketId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var gTicketId = Serializer.Deserialize<Guid>(ticketId);
            return business.Sync(gTicketId);
        }

        public ValidationResult UpdateFiscalPrinterData(string ticketId, string fpSerialInvoice, string fpInvoiceNumber, string fpSerialCreditNote, string fpCreditNoteNumber, bool sync)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var gTicketId = Serializer.Deserialize<Guid>(ticketId);
            var lfpInvoiceNumber = Serializer.Deserialize<long?>(fpInvoiceNumber);
            var lfpCreditNoteNumber = Serializer.Deserialize<long?>(fpCreditNoteNumber);
            return business.UpdateFiscalPrinterDataAsync(gTicketId, fpSerialInvoice, lfpInvoiceNumber, fpSerialCreditNote, lfpCreditNoteNumber, sync);
        }

        public ValidationContractResult LoadClosedTicket(string ticketId, string local)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var gTicketId = Serializer.Deserialize<Guid>(ticketId);
            var bLocal = Serializer.Deserialize<bool>(local);
            return business.LoadCloseTicket(gTicketId, bLocal);
        }

        public ValidationContractResult CancelTicket(string contract, string cancellationReasonId, string comment, string createQuickTicket)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var cTicket = Serializer.Deserialize<POSTicketContract>(contract);
            var gCancellationReasonId = Serializer.Deserialize<Guid>(cancellationReasonId);
            var cCreateQuickTicket = Serializer.Deserialize<bool>(createQuickTicket);
            return business.CancelTicket(cTicket, gCancellationReasonId, comment, false, createQuickTicket: cCreateQuickTicket);
        }

        public ValidationContractResult CancelTicketById(string ticketId, string cancellationReasonId, string comment, string createQuickTicket)
        {
            using var business = GetBusiness<ITicketBusiness>();
			var gTicketId = Serializer.Deserialize<Guid>(ticketId);
			var gCancellationReasonId = Serializer.Deserialize<Guid>(cancellationReasonId);
            var cCreateQuickTicket = Serializer.Deserialize<bool>(createQuickTicket);
            return business.CancelTicketById(gTicketId, gCancellationReasonId, comment, false, createQuickTicket: cCreateQuickTicket);
        }
        
        public ValidationContractResult CreateQuickTicketFromClosedTicket(string closedTicketRecordId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var closedTicketId = closedTicketRecordId.AsGuid();
            return business.CreateQuickTicketFromClosedTicket(closedTicketId);
        }

        public ValidationContractResult CancelClosedTicket(string ticketId, string cancellationReasonId, string comments, string voidTicket, string local, string createQuickTicket)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var gTicketId = Serializer.Deserialize<Guid>(ticketId);
            var gCancellationReasonId = Serializer.Deserialize<Guid>(cancellationReasonId);
            var bLocal = Serializer.Deserialize<bool>(local);
            var bVoidTicket = Serializer.Deserialize<bool>(voidTicket);
            var bCreateQuickTicket = Serializer.Deserialize<bool>(createQuickTicket);
            return business.CancelClosedTicket(gTicketId, gCancellationReasonId, comments, bVoidTicket, bLocal, bCreateQuickTicket);
        }

        public ValidationContractResult CancelTicketProduct(string ticketContract, string productLineId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var cTicket = Serializer.Deserialize<POSTicketContract>(ticketContract);
            var gProductLineId = Serializer.Deserialize<Guid>(productLineId);
            return business.CancelTicketProduct(cTicket, gProductLineId);
        }

        public List<POSTicketContract> GetTicketByStandSaloon(string hotelId, string cashierId, string standId, string saloonId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.GetTicketByStandSaloon(hotelId.AsGuid(), cashierId.AsGuid(), standId.AsGuid(), saloonId.AsGuid());
        }

        public List<POSTicketContract> GetTicketByStandCashier(string standId, string cashierId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            HubSessionData hubSessionData = GetHubSessionData(HubContextCache.Current.Token);
            return business.GetTicketByStandCashier(hubSessionData.InstallationId, standId.AsGuid(), cashierId.AsGuid());
        }

        public List<TicketInfo> GetTicketInfoByStandCashier(string standId, string cashierId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            HubSessionData hubSessionData = GetHubSessionData(HubContextCache.Current.Token);
            return business.GetTicketInfoByStandCashier(hubSessionData.InstallationId, standId.AsGuid(), cashierId.AsGuid());
        }

        public ValidationResult<string> NotifyRoomServiceMonitor(string contract)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var cTicket = Serializer.Deserialize<POSTicketContract>(contract);
            return business.NotifyRoomServiceMonitor(cTicket);
        }

        public ValidationContractResult IncrementPrints(string ticketId, string title)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.IncrementPrints(ticketId.AsGuid(), title.AsInteger());
        }

        public ValidationContractResult CheckOpenTicket(string ticketId, string standId, string cashierId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.CheckOpenTicket(ticketId.AsGuid(), standId.AsGuid(), cashierId.AsGuid());
        }

        public ValidationContractResult ChangeProductLinesSeparator(string ticketId, string[] productLineIds, string separatorId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.ChangeProductLinesSeparator(ticketId.AsGuid(), productLineIds.Select(productLineId => productLineId.AsGuid()), separatorId.AsGuidX());
        }
        
        public ValidationContractResult ChangeProductLinesSeat(string ticketId, string[] productLineIds, short seat)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.ChangeProductLinesSeat(ticketId.AsGuid(),
                productLineIds.Select(productLineId => productLineId.AsGuid()), seat);
        }
        
        #region Signatures

        /// <summary>
        /// Gets all the signatures.
        /// </summary>
        /// <returns>A list of all signatures in the database</returns>
        public List<SignatureRecord> GetAllSignatures()
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.GetAllSignatures();
        }
        
        /// <summary>
        /// Gets one signature by its ticket id.
        /// </summary>
        /// <param name="ticketId">The ticket id to be searched for</param>
        /// <returns>A single record with the given ticketId</returns>
        public SignatureRecord GetSignature(string ticketId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.GetSignature(ticketId.AsGuid());
        }
        
        /// <summary>
        /// Adds a new signature to the database.
        /// </summary>
        /// <param name="signatureRecord">The record to be added</param>
        /// <returns>An object with the errors or success of the operation</returns>
        public ValidationContractResult AddSignature(SignatureRecord signatureRecord)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.AddSignature(signatureRecord);
        }
        
        /// <summary>
        /// Updates a signature in the database.
        /// </summary>
        /// <param name="ticketId">The ticket Id of the record to be updated</param>
        /// <param name="signatureRecord">The new record</param>
        /// <returns>An object with the errors or success of the operation</returns>
        public ValidationContractResult UpdateSignature(string ticketId, SignatureRecord signatureRecord)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.UpdateSignature(ticketId.AsGuid(), signatureRecord);
        }
        
        /// <summary>
        /// Deletes a signature from the database.
        /// </summary>
        /// <param name="ticketId">The ticket Id of the record to be deleted</param>
        /// <returns>An object with the errors or success of the operation</returns>
        public ValidationContractResult DeleteSignature(string ticketId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.DeleteSignature(ticketId.AsGuid());
        }

        public ValidationItemResult<Guid> RequestNHSignatureCapture(string ticketId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.RequestNHSignatureCapture(ticketId.AsGuid());
        }

        public ValidationContractResult RetrieveNHSignatureCapture(string ticketId, string signatureId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.RetrieveNHSignatureCapture(ticketId.AsGuid() ,signatureId.AsGuid());
        }

        public ValidationResult RemoveNHSignatureCapture(string signatureId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.RemoveNHSignatureCapture(signatureId.AsGuid());
        }

        #endregion

        #endregion

        #region Settings

        public ValidationItemSource<DownloadedImageContact> DownloadImages(KeyValuePair<string, DateTime>[] paths)
        {
            using var business = GetBusiness<ISettingBusiness>();
            return business.DownloadImages(paths);
        }

        public async Task<ValidationResult<string>> SyncLocalStock()
        {
            using var business = GetBusiness<ISettingBusiness>();
            return await business.SyncLocalStock();
        }

        public ValidationResult ResetSyncStatus()
        {
            using var business = GetBusiness<ISettingBusiness>();
            return business.ResetSyncStatus();
        }

        public List<HotelRecord> GetHotels(string userId)
        {
            using var business = GetBusiness<ISettingBusiness>();
            return business.GetHotels(userId.AsGuid());
        }

        public HotelEnvironment GetHotelEnvironment(string userId, string language)
        {
            int lcid = Int32.Parse(language);

            using var manager = GetManager();
            using var settingBusiness = BusinessContext.GetBusiness<ISettingBusiness>(manager);
            var hotel = settingBusiness.GetHotels(userId.AsGuid()).FirstOrDefault();

            if (hotel == null)
                throw new OperationCanceledException(String.Format("No hotel found for user '{0}'.", userId));

            var environments = settingBusiness.GetHotels(userId.AsGuid())
                .Select(s => new HotelEnvironment { Hotel = s });

            using var standBusiness = BusinessContext.GetBusiness<IStandBusiness>(manager);
            var environment = new HotelEnvironment
            {
                Hotel = hotel,
                Cashiers = standBusiness.GetCashiers(hotel.Id, userId.AsGuid(), lcid),
                Stands = new List<POSStandRecord>()
            };

            foreach (var cashier in environment.Cashiers)
            {
                foreach (var stand in standBusiness.GetStands(cashier.Id, lcid))
                    environment.Stands.Add(stand);
            }

            return environment;
        }

        public POSGeneralSettingsRecord GetGeneralSettings(bool fromCloud)
        {
            using var business = GetBusiness<ISettingBusiness>();
            return business.GetGeneralSettings(BusinessContext.InstallationId, fromCloud);
        }

        public List<CreditCardTypesRecord> GetCreditCardTypes(string hotelId, string language)
        {
            using var business = GetBusiness<ISettingBusiness>();
            return business.GetCreditCardTypes(hotelId.AsGuid(), language.AsInteger());
        }

        public List<HouseUseRecord> GetHouseUses(string standId, string language)
        {
            using var business = GetBusiness<ISettingBusiness>();
            return business.GetHouseUses(standId.AsGuid(), language.AsInteger());
        }

        public List<PaymentMethodsRecord> GetPaymentMethods(string hotelId, string language)
        {
            using var business = GetBusiness<ISettingBusiness>();
            return business.GetPaymentMethods(hotelId.AsGuid(), language.AsInteger());
        }

        public List<SeparatorsRecord> GetSeparators(string hotelId, string language)
        {
            using var business = GetBusiness<ISettingBusiness>();
            return business.GetSeparators(hotelId.AsGuid(), language.AsInteger());
        }

        public List<TicketDutyRecord> GetTicketDuty(string hotelId, string language)
        {
            using var business = GetBusiness<ISettingBusiness>();
            return business.GetTicketDuty(hotelId.AsGuid(), language.AsInteger());
        }

        public List<CategoriesRecord> GetButtonCategories(string hotelId, string language)
        {
            using var business = GetBusiness<ISettingBusiness>();
            return business.GetButtonCategories(hotelId.AsGuid(), language.AsInteger());
        }

        public List<SubCategoriesRecord> GetButtonSubCategories(string categoryId, string language)
        {
            using var business = GetBusiness<ISettingBusiness>();
            return business.GetButtonSubCategories(categoryId.AsGuid(), int.Parse(language));
        }

        public List<ColumnsPlusProductsRecord> GetColumnsPlusProducts(string standId, string subCategoryId, string language)
        {
            using var business = GetBusiness<ISettingBusiness>();
            return business.GetColumnsPlusProducts(standId.AsGuid(), subCategoryId.AsGuid(), int.Parse(language));
        }

        public string GetStandardCurrencyByStand(string standId)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetStandardCurrencyByStand(standId.AsGuid());
        }

        public ValidationItemResult<PmsReservationRecord> GetPmsReservation(PmsReservationFilterModel filter)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetPmsReservation(filter);
        }

        #endregion
        
        #region POS Cashier File

        private static readonly object _key = new object();
        public Stream GetCashierFiles()
        {
            
            lock (_key)
            {
                //path to folder. Set in App.config
                string path = ConfigurationManager.AppSettings.Get("CashierPkgPath");

                /*code to manage CashierPkgPath not set in App.config. 
                
                if (string.IsNullOrEmpty(path))
                    path = Path.Combine(Environment.CurrentDirectory, "Cashier\\pos.bin");
                */

                //path to zip
                string pathToZip = Directory.GetParent(path).FullName + "\\POS.zip";
                //create zip if it doesn't already exist
                if (!File.Exists(pathToZip))
                    ZipFile.CreateFromDirectory(path, pathToZip);

                FileStream stream = File.OpenRead(pathToZip);
                return stream;
            }
        }
        #endregion

        #region External Payments

        //      public Task<string> GenericCall(string functionName, string data)
        //{
        //          Serialization.FromJSON<ExternalPaymentRequestContract>(data);
        //          var method = this.GetType().GetMethod(functionName);
        //          var paramterType = method.GetParameters()[0].ParameterType;
        //	var parameter = Serialization.FromJSON(paramterType, data);

        //          var result = method.Invoke(data, [parameter]);

        //          return result.ToJson(true);

        //	using var business = GetBusiness<IPaySystemBusiness>();
        //	return business.GenerciCall(functionName, data);
        //}  

        private async Task<string> GenericCall<Rq, Rs>(string requestString, Func<Rq, Task<ValidationResult<Rs>>> func)
        {
            Rq request = ClientExtension.Deserialize<Rq>(requestString);
			ValidationResult<Rs> response = await func(request);
            return response.Serialize();
        }

		public async Task<ValidationResult<RequestTerminalsContract>> GetPayTerminals(string originId)
		{
			var result = new ValidationResult<RequestTerminalsContract>();
			if (result.ValidateGuid(originId, out var originIdValue).HasErrors)
				return result;

			using var business = GetBusiness<IPaySystemBusiness>();
			return await business.SafeRunAsync(b => b.GetPayTerminals(originIdValue));
		}

		public async Task<string> RequestExternalPayment(string requestString)
		{
			using var business = GetBusiness<IPaySystemBusiness>();
			return await GenericCall<ExternalPaymentRequestContract, ExternalPaymentResponseContract>(requestString, request => business.SafeRunAsync(b => b.RequestPayment(request)));
		}

        public Task<string> UpdateProcessedExternalPayment(string requestString)
        {
            throw new NotImplementedException();
		}

		public async Task<ValidationResult<ExternalPaymentEntryContract>> UpdatePendingExternalPayment(string entryId)
		{
			var result = new ValidationResult<ExternalPaymentEntryContract>();
			if (result.ValidateGuid(entryId, out var entryIdValue).HasErrors)
				return result;

			using var business = GetBusiness<IPaySystemBusiness>();
			return await business.SafeRunAsync(b => b.UpdatePendingPayment(entryIdValue));
		}

		#endregion
	}

	public static class ServiceEx
	{
		public static T ValidateGuid<T>(this T result, string guidText, out Guid guid)
			where T : ValidationResult
		{
			if (result.HasErrors)
			{
				guid = default;
				return result;
			}

			if (!Guid.TryParse(guidText, out guid))
				result.AddError($"Invalid GUID: {guidText}");

			return result;
		}

		public static async Task<T> SafeRunAsync<T, S>(this S service, Func<S, Task<T>> func)
			where T : ValidationResult, new()
		{
			try
			{
				return await func(service);
			}
			catch (Exception ex)
			{
				var result = new T();
				result.AddError(ex.Message);
				return result;
			}
		}
	}
}