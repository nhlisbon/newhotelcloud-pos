﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Threading.Tasks;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using NewHotel.Pos.Hub.Common;
using NewHotel.Pos.Hub.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.Records;
using NewHotel.Pos.Hub.Contracts.Common.Records.CurrentAccount;
using NewHotel.Pos.Hub.Contracts.Common.Records.Stands;
using NewHotel.Pos.Hub.Contracts.Common.Records.Tickets;
using NewHotel.Pos.Hub.Contracts.PaySystem;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Hub.Model.Cashier.Contracts.Settings;
using NewHotel.Pos.Hub.Model.WaitingListStand.Contract;
using NewHotel.Pos.Hub.Model.WaitingListStand.Record;
using ClosedTicketRecord = NewHotel.Pos.Hub.Model.ClosedTicketRecord;
using DocumentSerieRecord = NewHotel.Pos.Hub.Model.DocumentSerieRecord;
using HotelRecord = NewHotel.Pos.Hub.Model.HotelRecord;
using ProductRecord = NewHotel.Pos.Hub.Contracts.Common.Records.Products.ProductRecord;
using SaloonRecord = NewHotel.Pos.Hub.Model.SaloonRecord;
using UserRecord = NewHotel.Pos.Hub.Model.UserRecord;


namespace NewHotel.Pos.Hub.Cashier
{
    [ServiceKnownType(typeof(BaseContract))]
    [ServiceKnownType(typeof(POSTicketContract))]
    [ServiceKnownType(typeof(POSProductLineContract))]
    [ServiceKnownType(typeof(ContainerTransferContract))]
    [ServiceKnownType(typeof(StandTurnDateContract))]
    [ServiceKnownType(typeof(PaxWaitingListContract))]
    [ServiceKnownType(typeof(DownloadedImageContact))]
    [ServiceKnownType(typeof(TablesReservationLineContract))]
    [ServiceKnownType(typeof(POSDocumentType))]
    [ServiceKnownType(typeof(HotelContract))]
    [ServiceContract]
    public interface IService : ICommonService
    {
        #region Stands

        [OperationContract]
        [WebInvoke(UriTemplate = "/DownloadImages", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<DownloadedImageContact> DownloadImages(KeyValuePair<string, DateTime>[] paths);

        [OperationContract]
        [WebInvoke(UriTemplate = "/SyncLocalStock", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Task<ValidationResult<string>> SyncLocalStock();

        [OperationContract]
        [WebInvoke(UriTemplate = "/Cashiers/{hotelId}&{userId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<CashierRecord> GetCashiers(string hotelId, string userId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/Stands/{cajaId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<POSStandRecord> GetStands(string cajaId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/StandsForTransfer/{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<LiteStandRecord> GetStandsForTransfer(string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/TransferTicket/{ticket}&{transfer}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationResult TransferTicket(string ticket, string transfer);

        [OperationContract]
        [WebInvoke(UriTemplate = "/VerifyTransferToAccept/{standId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult VerifyTransferToAccept(string standId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/AcceptTransferedTickets/{transfers}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationTicketsResult AcceptTransferedTickets(string transfers);

        [OperationContract]
        [WebInvoke(UriTemplate = "/VerifyTransferToReturn/{standId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult VerifyTransferToReturn(string standId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ReturnTransferedTickets/{transfers}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationTicketsResult ReturnTransferedTickets(string transfers);

        [OperationContract]
        [WebInvoke(UriTemplate = "/UserByCode/{userCode}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<UserRecord> GetUserByCode(string userCode);

        [OperationContract]
        [WebInvoke(UriTemplate = "/GetUserByName/{loginUser}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<UserRecord> GetUserByName(string loginUser);

        [OperationContract]
        [WebInvoke(UriTemplate = "/CheckUserPermission/{userId}&{permission}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationBooleanResult CheckUserPermission(string userId, string permission);

        [OperationContract]
        [WebInvoke(UriTemplate = "/Saloons/{standId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<SaloonRecord> GetSaloonsByStand(string standId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/Tables/{saloonId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<TableRecord> GetTablesBySaloon(string saloonId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/StandTimeSlots/{standId}&{date}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<StandTimeSlotRecord> GetStandTimeSlots(string standId, string date);

        [OperationContract]
        [WebInvoke(UriTemplate = "/Reservations", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<ReservationRecord> GetReservations(ReservationFilterContract filter);

        [OperationContract]
        [WebInvoke(UriTemplate = "/SpaReservations/{installationId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Obsolete]
        ValidationItemSource<SpaReservationSearchFromExternalRecord> GetSpaReservations(string installationId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/SpaReservations", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<SpaReservationSearchFromExternalRecord> GetSpaReservationsV2(SpaSearchReservationFilterModel filter);

        [OperationContract]
        [WebInvoke(UriTemplate = "/SpaServices/{installationId}&{spaId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<SpaServiceRecord> GetSpaServices(string installationId, string spaId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/EventReservations/{installationId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<EventReservationSearchFromExternalRecord> GetEventReservations(string installationId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/EntityInstallations", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<PmsCompanyRecord> GetEntityInstallations(PmsCompanyFilterModel filter);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ReservationsGroups/{installationId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<ReservationsGroupsRecord> GetReservationsGroups(string installationId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ControlAccounts", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<PmsControlAccountRecord> GetControlAccounts(PmsControlAccountFilterModel filter);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/ControlAccountMovements", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<ControlAccountMovementDto> GetControlAccountMovements(ControlAccountMovementsFilterModel filter);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ClientInstallations/{openAccounts}&{clientId}&{room}&{fullName}&{installationId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<ClientInstallationRecord> GetClientInstallations(string openAccounts, string clientId, string room, string fullName, string installationId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/GetClientEntities/{queryRequest}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<ClientRecord> GetClientEntities(string queryRequest);

        [OperationContract]
        [WebInvoke(UriTemplate = "/UpdateMesa/{contract}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationResult UpdateMesa(string contract);

        [OperationContract]
        [WebInvoke(UriTemplate = "/OldTickets/{standId}&{cashierId}&{date}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<ClosedTicketRecord> GetOldTickets(string standId, string cashierId, string date);

        [OperationContract]
        [WebInvoke(UriTemplate = "/SalesReport/{queryRequest}&{isTurnReport}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<SalesCashierRecord> GetSalesReport(string queryRequest, string isTurnReport);

        [OperationContract]
        [WebInvoke(UriTemplate = "/PaymentsReport/{queryRequest}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<SalePaymentRecord> GetPaymentsReport(string queryRequest);

        [OperationContract]
        [WebInvoke(UriTemplate = "/OldTicket/{ticketId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        TicketRContract LoadTicketReal(string ticketId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/GetStandardCurrencyByStand/{standId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetStandardCurrencyByStand(string standId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/GetPmsReservation", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<PmsReservationRecord> GetPmsReservation(PmsReservationFilterModel filter);

        #region Table Reservation

        [OperationContract]
        [WebInvoke(UriTemplate = "/GetTableReservations/{filter}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<LiteTablesReservationRecord> GetTableReservations(string filter);

        [OperationContract]
        [WebInvoke(UriTemplate = "/CancelTableReservation/{id}&{cancellationReasonId}&{comments}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationResult CancelTableReservation(string id, string cancellationReasonId, string comments);

        #endregion

        #region WaitingList


        [OperationContract]
        [WebInvoke(UriTemplate = "/GetWaitingList/{waitingListFilterContract}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<PaxWaitingListRecord> GetWaitingList(string waitingListFilterContract);

        [OperationContract]
        [WebInvoke(UriTemplate = "/NewPaxWaitingList/", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult NewPaxWaitingList();

        [OperationContract]
        [WebInvoke(UriTemplate = "/LoadPaxWaitingList/{id}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult LoadPaxWaitingList(string id);

        [OperationContract]
        [WebInvoke(UriTemplate = "/PersistPaxWaitingList/{contract}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult PersistPaxWaitingList(string contract);

        [OperationContract]
        [WebInvoke(UriTemplate = "/DeletePaxWaitingList/{id}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationResult DeletePaxWaitingList(string id);


        #endregion

        #region Meals Control

        [OperationContract]
        [WebInvoke(UriTemplate = "/GetMealsControl", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<MealsControlRecord> GetMealsControl(MealsFilterModel model);

        [OperationContract]
        [WebInvoke(UriTemplate = "/UpdateMealsControl/{guestid}&{breakfast}&{lunch}&{dinner}&{installationId}", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationResult UpdateMealsControl(string guestid, string breakfast, string lunch, string dinner, string installationId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/GetClientInfo/{clientid}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<ClientInfoContract> GetClientInfo(string clientid);

        #endregion

        #endregion

        #region Multi Hotel

        [OperationContract]
        [WebInvoke(UriTemplate = "/GetChargeHotels/", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<ChargeHotelRecord> GetChargeHotels();

        #endregion

        #region Products

        [OperationContract]
        [WebInvoke(UriTemplate = "/ProductByStand/{hotelId}&{standId}&{rateId}&{taxSchema}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<ProductRecord> GetProductsByStandandRate(string hotelId, string standId, string rateId, string taxSchema, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ProductsImages/{hotelId}&{standId}&{rateId}&{taxSchema}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<ImageRecord> GetProductsImages(string hotelId, string standId, string rateId, string taxSchema, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/GetProductPrices/{standId}&{rateId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<ProductPriceRecord> GetProductPrices(string standId, string rateId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ProductGroup/{hotelId}&{standId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<GroupRecord> GetProductGroups(string hotelId, string standId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ProductFamily/{hotelId}&{standId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<FamilyRecord> GetProductFamilies(string hotelId, string standId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ProductSubFamily/{hotelId}&{standId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<SubFamilyRecord> GetProductSubFamilies(string hotelId, string standId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/Preparations/{productId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<PreparationRecord> GetPreparationsByProducts(string productId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/Areas/{standId}&{productId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<AreasRecord> GetAreasByProducts(string standId, string productId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/AreasByStand/{standId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<AreasRecord> GetAreasByStand(string standId, string language);


        [OperationContract]
        [WebInvoke(UriTemplate = "/ProductPrice/{standId}&{productId}&{rateId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        decimal? GetProductPrice(string standId, string productId, string rateId);

        #endregion

        #region Tickets

        [OperationContract]
        [WebInvoke(UriTemplate = "/Documents/{standId}&{cashierId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<DocumentSerieRecord> GetTicketSerieByStandCashier(string standId, string cashierId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/NextSerialNumber/{cashierId}&{standId}&{justSerie}&{docType}&{workDate}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        SerialNumberResult GetNextSerialNumber(string cashierId, string standId, string justSerie, string docType, string workDate);

        [OperationContract]
        [WebInvoke(UriTemplate = "/NewTicket/{hotelId}&{cajaId}&{standId}&{tableId}&{utilId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult NewTicket(string hotelId, string cajaId, string standId, string tableId, string utilId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/CreateEmptyTicketToReservationTable/{tableIds}&{reservationTableId}&{name}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult CreateEmptyTicketToReservationTable(string tableIds, string reservationTableId, string name);

        [OperationContract]
        [WebInvoke(UriTemplate = "/CreateEmptyTicketToPaxWaitingList/{tableId}&{paxWaitingListId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult CreateEmptyTicketToPaxWaitingList(string tableId, string paxWaitingListId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/LoadTicketForEdition/?ticketId={ticketId}&forceUnlock={forceUnlock}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult LoadTicketForEdition(string ticketId, bool forceUnlock);

        [OperationContract]
        [WebInvoke(UriTemplate = "/SaveTicketFromEdition/{ticketId}&{paxs}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult SaveTicketFromEdition(string ticketId, string paxs);

        [OperationContract]
        [WebInvoke(UriTemplate = "/MoveTicketToTable/{ticketContract}&{tableId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult MoveTicketToTable(string ticketContract, string tableId);

		[OperationContract]
		[WebInvoke(UriTemplate = "/MoveTicketToTableById/{ticketId}&{tableId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
		ValidationContractResult MoveTicketToTableById(string ticketId, string tableId);


		[OperationContract]
        [WebInvoke(UriTemplate = "/PersistentProductLine/?contract={contract}&schemaId={schemaId}&hotelId={hotelId}&included={included}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult PersistentProductLine(string contract, string schemaId, string hotelId, bool included);

        [OperationContract]
        [WebInvoke(UriTemplate = "/PersistentProductLine/{originalTicket}&{voidTicket}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult MergeTickets(string originalTicket, string voidTicket);

		[OperationContract]
		[WebInvoke(UriTemplate = "/MergeTicketsById/{originalTicketId}&{voidTicketId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
		ValidationContractResult MergeTicketsById(string originalTicketId, string voidTicketId);

		[OperationContract]
        [WebInvoke(UriTemplate = "/PersistentProductLine/{ticket}&{productLineId}&{quantity}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult SplitTicketProduct(string ticket, string productLineId, string quantity);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ChangeTicketProductDetails/{ticket}&{productLineId}&{editedLine}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult ChangeTicketProductDetails(string ticket, string productLineId, string editedLine);

        [OperationContract]
        [WebInvoke(UriTemplate = "/DispatchTicketOrders/{ticket}&{productLineIds}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult DispatchTicketOrders(string ticket, string productLineIds);

        [OperationContract]
        [WebInvoke(UriTemplate = "/VoidDispatchTicketOrders/{ticket}&{productLineIds}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult VoidDispatchTicketOrders(string ticket, string productLineIds);

        [OperationContract]
        [WebInvoke(UriTemplate = "/AwayDispatchTicketOrders/{ticket}&{productLineIds}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult AwayDispatchTicketOrders(string ticket, string productLineIds);

        [OperationContract]
        [WebInvoke(UriTemplate = "/SplitTicketManual/{ticketId}&{ordersToLeave}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        SplitTicketResult SplitTicketManual(string ticketId, string ordersToLeave);

        [OperationContract]
        [WebInvoke(UriTemplate = "/SplitTicketAuto/{ticketId}&{quantity}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        SplitTicketResult SplitTicketAuto(string ticketId, string quantity);

        [OperationContract]
        [WebInvoke(UriTemplate = "/SplitTicketByAlcoholicGroup/{ticketId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        SplitTicketResult SplitTicketByAlcoholicGroup(string ticketId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/PersistentPaymentLine/?contract={contract}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationResult PersistentPaymentLine(string contract);

        [OperationContract]
        [WebInvoke(UriTemplate = "/AddProductLineToTicket/{ticket}&{productLine}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult AddProductLineToTicket(string ticket, string productLine);

        [OperationContract]
        [WebInvoke(UriTemplate = "/AddProductToTicket/{ticket}&{product}&{quantity}&{manualPrice}&{manualPriceDesc}&{observations}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult AddProductToTicket(string ticket, string product, string quantity, string manualPrice, string manualPriceDesc, string observations);

        [OperationContract]
        [WebInvoke(UriTemplate = "/CreateLookupTable/{ticket}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult CreateLookupTable(string ticket);

        [OperationContract]
        [WebInvoke(UriTemplate = "/PersistTicket/?contract={contract}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult PersistTicket(string contract);

        [OperationContract]
        [WebInvoke(UriTemplate = "/CloseTicket/{contract}&{tipProduct}&{saveClient}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult CloseTicket(string contract, string tipProduct, string saveClient);

        [OperationContract]
        [WebInvoke(UriTemplate = "/UpdateFiscalPrinterData/?tickedId={tickedId}&fpSerialInvoice={fpSerialInvoice}&fpInvoiceNumber={fpInvoiceNumber}&fpSerialCreditNote={fpSerialCreditNote}&fpCreditNoteNumber={fpCreditNoteNumber}&sync={sync}", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationResult UpdateFiscalPrinterData(string tickedId, string fpSerialInvoice, string fpInvoiceNumber, string fpSerialCreditNote, string fpCreditNoteNumber, bool sync);

        [OperationContract]
        [WebInvoke(UriTemplate = "/LoadClosedTicket/{tickedId}&{local}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult LoadClosedTicket(string tickedId, string local);

        [OperationContract]
        [WebInvoke(UriTemplate = "/DeleteProductLine/{productLineId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationResult DeleteProductLine(string productLineId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/CancelProductLine/{productLineId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationResult CancelProductLine(string productLineId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/DeletePaymentLine/{paymentLineId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationResult DeletePaymentLine(string paymentLineId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/CancelPaymentLine/{paymentLineId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationResult CancelPaymentLine(string paymentLineId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/CancelTicket/{contract}&{cancellationReasonId}&{comment}&{createQuickTicket}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
		ValidationContractResult CancelTicket(string contract, string cancellationReasonId, string comment, string createQuickTicket);

		[OperationContract]
		[WebInvoke(UriTemplate = "/CancelTicketById/{ticketId}&{cancellationReasonId}&{comment}&{createQuickTicket}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
		ValidationContractResult CancelTicketById(string ticketId, string cancellationReasonId, string comment, string createQuickTicket);
		
		[OperationContract]
        [WebInvoke(UriTemplate = "/CancelClosedTicket/{ticketId}&{cancellationReasonId}&{comments}&{voidTicket}&{local}&{createQuickTicket}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult CancelClosedTicket(string ticketId, string cancellationReasonId, string comments, string voidTicket, string local, string createQuickTicket);

        [OperationContract]
        [WebInvoke(UriTemplate = "/CancelTicketProduct/{ticketContract}&{productLineId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult CancelTicketProduct(string ticketContract, string productLineId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/TicketByStandSaloon/{hotelId}&{cashierId}&{standId}&{saloonId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<POSTicketContract> GetTicketByStandSaloon(string hotelId, string cashierId, string standId, string saloonId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/TicketByStandCashier/{standId}&{cashierId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<POSTicketContract> GetTicketByStandCashier(string standId, string cashierId);

		[OperationContract]
		[WebInvoke(UriTemplate = "/TicketInfoByStandCashier/{standId}&{cashierId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
		List<TicketInfo> GetTicketInfoByStandCashier(string standId, string cashierId);


		[OperationContract]
        [WebInvoke(UriTemplate = "/TicketPrintConfiguration/{configurationId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        POSTicketPrintConfigurationRecord GetTicketPrintConfiguration(string configurationId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/GetPdfInvoiceReport/{ticketId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ExportOfficialDocumentResult GetPdfInvoiceReport(string ticketId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/NotifyRoomServiceMonitor/{contract}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationResult<string> NotifyRoomServiceMonitor(string contract);

        [OperationContract]
        [WebInvoke(UriTemplate = "/IncrementPrints/{ticketId}&{title}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult IncrementPrints(string ticketId, string title);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/ValidateRealTicket/{ticketId}&{standId}&{cashierId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult CheckOpenTicket(string ticketId, string standId, string cashierId);
        
        /// <summary>
        /// Change the separator to a list of products lines
        /// </summary>
        /// <param name="ticketId">The thicket id</param>
        /// <param name="productLineIds">The ids of the product lines</param>
        /// <param name="separatorId">The separator id</param>
        /// <returns>A Contract Ticket</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/ChangeProductLinesSeparator", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult ChangeProductLinesSeparator(string ticketId, string[] productLineIds, string separatorId);
        
        /// <summary>
        /// Change the seat to a list of products lines
        /// </summary>
        /// <param name="ticketId">The thicket id</param>
        /// <param name="productLineIds">The ids of the product lines</param>
        /// <param name="seat">The seat number</param>
        /// <returns>A Contract Ticket</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/ChangeProductLinesSeat", Method = "POST",
	        BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
	        ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult ChangeProductLinesSeat(string ticketId, string[] productLineIds, short seat);

        [OperationContract]
        [WebInvoke(UriTemplate = "/CreateQuickTicketFromClosedTicket", Method = "POST",
	        BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
	        ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult CreateQuickTicketFromClosedTicket(string closedTicketRecordId);
        
        #endregion

        #region Settings

        [OperationContract]
        [WebInvoke(UriTemplate = "/Hotel/{userId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<HotelRecord> GetHotels(string userId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/HotelEnvironment/{userId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        HotelEnvironment GetHotelEnvironment(string userId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/GetSettings/?fromCloud={fromCloud}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        POSGeneralSettingsRecord GetGeneralSettings(bool fromCloud);

        [OperationContract]
        [WebInvoke(UriTemplate = "/StandEnvironment/{standId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<StandEnvironment> GetStandEnvironment(string standId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/GetStandsFromCloud/{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<POSStandRecord> GetStandsFromCloud(string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/CloseDayValidations/{standId}&{cashierId}&{closeDay}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationResult CloseDayValidations(string standId, string cashierId, string closeDay);

        [OperationContract]
        [WebInvoke(UriTemplate = "/CloseDay/{standId}&{cajaId}&{closeDay}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult CloseDay(string standId, string cajaId, string closeDay);

        [OperationContract]
        [WebInvoke(UriTemplate = "/UndoCloseDay/{standId}&{cajaId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationDateResult UndoCloseDay(string standId, string cajaId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/CloseTurn/{standId}&{cajaId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult CloseTurn(string standId, string cajaId);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/CreditCardTypes/{hotelId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<CreditCardTypesRecord> GetCreditCardTypes(string hotelId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/HouseUses/{standId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<HouseUseRecord> GetHouseUses(string standId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/PaymentMethods/{hotelId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<PaymentMethodsRecord> GetPaymentMethods(string hotelId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/Separators/{hotelId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<SeparatorsRecord> GetSeparators(string hotelId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/TicketDuty/{hotelId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<TicketDutyRecord> GetTicketDuty(string hotelId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/Categories/{hotelId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<CategoriesRecord> GetButtonCategories(string hotelId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/SubCategories/{categoryId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<SubCategoriesRecord> GetButtonSubCategories(string categoryId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/SubCategoriesProducts/{standId}&{subCategoryId}&{language}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<ColumnsPlusProductsRecord> GetColumnsPlusProducts(string standId, string subCategoryId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ResetSyncStatus/", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationResult ResetSyncStatus();
        
        #region Digital Signatures
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetAllSignatures", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<SignatureRecord> GetAllSignatures();
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetSignature/{ticketId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        SignatureRecord GetSignature(string ticketId);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/AddSignature", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult AddSignature(SignatureRecord signatureRecord);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/UpdateSignature/{ticketId}", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
		ValidationContractResult UpdateSignature(string ticketId, SignatureRecord signatureRecord);
        
		[OperationContract]
		[WebInvoke(UriTemplate = "/DeleteSignature/{ticketId}", Method = "DELETE", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
		ValidationContractResult DeleteSignature(string ticketId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/RequestNHSignatureCapture/{ticketId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<Guid> RequestNHSignatureCapture(string ticketId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/RetrieveNHSignatureCapture/{ticketId}&{signatureId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult RetrieveNHSignatureCapture(string ticketId, string signatureId);

        #endregion


        #endregion

        #region Images

        [OperationContract]
        [WebInvoke(UriTemplate = "/Image/{id}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<ImageRecord> GetImage(string id);

        #endregion

        #region POS Cashier File

        [OperationContract]
        Stream GetCashierFiles();

		#endregion

		#region External Payments
		[OperationContract]
		[WebInvoke(UriTemplate = "/PayTerminals/{originId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
		Task<ValidationResult<RequestTerminalsContract>> GetPayTerminals(string originId);

		[OperationContract]
		[WebInvoke(UriTemplate = "/RequestExternalPayment", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
		Task<string> RequestExternalPayment(string requestString);

		[OperationContract]
		[WebInvoke(UriTemplate = "/UpdateProcessedExternalPayment", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
		Task<string> UpdateProcessedExternalPayment(string requestString);

		[OperationContract]
		[WebInvoke(UriTemplate = "/ExternalPayment/{entryId}/Refresh", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
		Task<ValidationResult<ExternalPaymentEntryContract>> UpdatePendingExternalPayment(string entryId);
		#endregion

	}
}