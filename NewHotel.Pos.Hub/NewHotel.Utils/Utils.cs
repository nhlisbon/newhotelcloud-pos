﻿using System;
using System.Text;
using System.Net;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Globalization;
using System.Text.RegularExpressions;

namespace NewHotel.Utils
{
    public static class Utils
    {
        private const string EmailRegEx = @"^(?("")(""[^""]+?""@)|(([0-9a-zA-Z_]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z_])@))" +
                                          @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z]{2,6}))$";

        private static HashSet<string> _twoLetterISOCountries;

        public static string GetUniqueAlphaNumeric(int maxSize)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            var crypto = new RNGCryptoServiceProvider();
            var data = new byte[maxSize];
            crypto.GetNonZeroBytes(data);
            var sb = new StringBuilder(maxSize);
            foreach (byte b in data)
                sb.Append(chars[b % (chars.Length - 1)]);

            return sb.ToString();
        }

        private static IPHostEntry GetIpHostEntry(string hostName)
        {
            return Dns.GetHostEntry(hostName);
        }

        public static string GetHostIpAddress(string hostName)
        {
            var addrs = GetIpHostEntry(hostName).AddressList;
            if (addrs.Length > 0)
                return addrs[addrs.Length - 1].ToString();

            return string.Empty;
        }

        public static string HostIpAddress
        {
            get { return GetHostIpAddress(Dns.GetHostName()); }
        }

        public static string HostName
        {
            get { return GetIpHostEntry(Dns.GetHostName()).HostName; }
        }

        public static HashSet<string> GetTwoLetterISOCountries()
        {
            if (_twoLetterISOCountries == null)
            {
                _twoLetterISOCountries = new HashSet<string>();
                foreach (var cultureInfo in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
                {
                    var regionInfo = new RegionInfo(cultureInfo.LCID);
                    _twoLetterISOCountries.Add(regionInfo.TwoLetterISORegionName);
                }
            }

            return _twoLetterISOCountries;
        }

        public static IEnumerable<string> SplitEmailAddress(string address)
        {
            return address.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static bool IsValidEmailAddress(string address)
        {
            if (string.IsNullOrEmpty(address)) return false;

            foreach (var addr in SplitEmailAddress(address))
            {
                //Net Validation
                try
                {
                    new System.Net.Mail.MailAddress(addr);
                }
                catch
                {
                    return false;
                }

                //Regex Validation
                if (!Regex.IsMatch(addr, EmailRegEx))
                    return false;
            }

            return true;
        }
    }
}