﻿using System.IO;
using System.Drawing.Imaging;

namespace NewHotel.Business
{
    public class QRCode
    {
        #region Members

        private readonly byte[] _buffer;

        #endregion
        #region Constructor

        public QRCode(string content, int height = 354, int width = 354, int margin = 1)
        {
            var writer = new ZXing.BarcodeWriter();
            writer.Format = ZXing.BarcodeFormat.QR_CODE;
            writer.Options = new ZXing.Common.EncodingOptions() { Height = height, Width = width, Margin = margin };
            writer.Options.Hints.Add(ZXing.EncodeHintType.ERROR_CORRECTION, "M");

            using (var stream = new MemoryStream())
            {
                try
                {
                    var bmp = writer.Write(content);
                    bmp.Save(stream, ImageFormat.Bmp);

                    _buffer = stream.GetBuffer();
                }
                finally
                {
                    stream.Close();
                }
            }
        }

        #endregion
        #region Public Methods

        public static implicit operator byte[] (QRCode qrCode)
        {
            return qrCode._buffer;
        }

        #endregion
    }
}