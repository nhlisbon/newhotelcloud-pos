﻿using System;

namespace NewHotel.Utils
{
    public static class StringExt
    {
        public static string Crop(this string str, int size)
        {
            if (str.Length > size)
                return str.Substring(0, size);

            return str;
        }
    }
}
