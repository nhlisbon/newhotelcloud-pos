﻿using System;
using System.Web;

namespace NewHotel.Utils
{
    public static class HttpResquestExt
    {
        public static string ClientIpAddress(this HttpRequest req)
        {
            var ip = req.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
                ip = req.ServerVariables["REMOTE_ADDR"];

            return ip;
        }
    }
}