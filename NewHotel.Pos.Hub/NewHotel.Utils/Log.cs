﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;

namespace NewHotel.Utils
{
    public sealed class Log
    {
        #region Members

        private IDictionary<string, TraceSource> _sources;

        #endregion
        #region Public Properties

        public TraceSource this[string name]
        {
            get
            {
                TraceSource source = null;
                lock (this)
                {
                    if (_sources == null)
                        _sources = new Dictionary<string, TraceSource>();

                    if (!_sources.TryGetValue(name.ToUpperInvariant(), out source))
                    {
                        source = new TraceSource(name);
                        _sources.Add(name.ToUpperInvariant(), source);
                    }
                }

                return source;
            }
        }

        public override string ToString()
        {
            lock (this)
            {
                if (_sources == null)
                    return string.Empty;

                return string.Join(", ", _sources.Select(x => string.Format("{0}={1}", x.Key, x.Value.Listeners.Count > 0)).ToArray());
            }
        }

        public static readonly Log Source = new Log();

        #endregion
    }
}
