﻿using NewHotel.Contracts;
using System;
using System.Linq;
using System.Reflection;

namespace NewHotel.WPF.Common.FiscalPrinters
{
    public abstract class FiscalPrinter
    {
        public static FiscalPrinter CreateFiscalPrinter(string fiscalPrinterType)
        {
            Type type = Assembly.GetExecutingAssembly().GetTypes().Where(x => x.Name == fiscalPrinterType).FirstOrDefault();
            return type != null ? Activator.CreateInstance(type) as FiscalPrinter : null;
        }

        public abstract bool PrintTicket(TicketRContract ticketContract);

    }
}
