﻿using NewHotel.Contracts;

namespace NewHotel.WPF.Common.FiscalPrinters
{
    public class BematechFiscalPrinter : FiscalPrinter
    {
        public override bool PrintTicket(TicketRContract ticketContract)
        {
            if (ticketContract == null)
            {// No tenemos ticket
                return false;
            }// No tenemos ticket
            else
            {// Tenemos ticket, hasta ahora valido
                int result = 0;
                if (ticketContract.ProductLines != null)
                    foreach (var product in ticketContract.ProductLines)
                    {
                        // Falta: permitir poner cantidades fraccinoarias
                        // Falta: ver que es FF
                        // Falta: Revisar que el codigo y la description lleguen adecuadamente a este metodo
                        // Falta: Los impuestos de los productos

                        //var result = BemaFI32.Bematech_FI_VendeItem(product.ProductCode, product.ProductDesc, "FF", "I", product.ProductQtd.ToString("F0"), 2, (product.ValueBeforeDiscount / product.ProductQtd).ToString("F2"), "%", (product.Discount ?? 0).ToString("F2"));
                        result = BemaFI32.Bematech_FI_VendeItem("123", "UnProductoCualquiera", "FF", "I", product.ProductQtd.ToString("F0"), 2, (product.Importe / product.ProductQtd).ToString("F2"), "%", (product.Discount ?? 0).ToString("F2"));
                        if (result <= 0)
                        {
                            return false;
                        }

                        if (product.IsAnul != 0 && product.IsAnul != 2)
                        {// El producto fue anulado
                            result = BemaFI32.Bematech_FI_CancelaItemAnterior();
                            if (result <= 0)
                            {
                                return false;
                            }
                        }// El producto fue anulado
                    }

                if (ticketContract.Anul.HasValue)
                {// El ticket fue anulado, no tiene forma de pagos, solo cancelamos el ticket
                    result = BemaFI32.Bematech_FI_CancelaCupom();
                    if (result <= 0)
                    {
                        return false;
                    }
                }// El ticket fue anulado, no tiene forma de pagos, solo cancelamos el ticket
                else
                {// No fue cancelado, imprimamos los pagos
                    if (ticketContract.PaymentLines != null)
                    {
                        foreach (var payment in ticketContract.PaymentLines)
                        {
                            result = BemaFI32.Bematech_FI_EfetuaFormaPagamentoDescricaoForma("Dinheiro", payment.Valo.ToString("F2"), payment.Observations);
                            if (result <= 0)
                            {
                                return false;
                            }
                        }
                        result = BemaFI32.Bematech_FI_TerminaFechamentoCupom("Gracias por su compra vuelva pronto, lo queremos... Paco y familia");
                        if (result <= 0)
                        {
                            return false;
                        }
                    }
                }// No fue cancelado, imprimamos los pagos

            }// Tenemos ticket, hasta ahora valido
            return true;
        }
    }
}
