﻿using System;
using System.Windows;
using System.Windows.Media.Animation;
using System.Runtime.CompilerServices;
using NewHotel.WPF.Common;
using NewHotel.DataAnnotations;
using NewHotel.Contracts;

namespace NewHotel.WPF.App.Pos.Dialogs
{
    /// <summary>
    /// Interaction logic for ErrorsWindow.xaml
    /// </summary>
    public partial class ErrorsWindow : Window
    {
        public bool TwoOptAvailable;

        public static bool NoShow { get; set; }

        public static void ShowErrorsWindow(string message, string header = "Error", Window parent = null, int count = 0, [CallerMemberName] string callerName = null)
        {
            if (!NoShow)
            {
                UIThread.Invoke(() =>
                {
                    var window = new ErrorsWindow("Something went wrong", message, callerName);
                    window.Owner = parent == null ? Application.Current.MainWindow : parent;
                    window.ShowDialog();
                });
            }
        }

        public static void ShowInfoWindow(string message, string header, Window parent = null, int count = 0, [CallerMemberName] string callerName = null)
        {
            if (!NoShow)
            {
                UIThread.Invoke(() =>
                {
                    var window = new ErrorsWindow(header, message, callerName);
                    window.Owner = parent == null ? Application.Current.MainWindow : parent;
                    window.ShowDialog();
                });
            }
        }

        public static void ShowErrorWindows(Exception ex)
        {
            if (!NoShow)
            {
                UIThread.Invoke(() =>
                {
                    var window = new ErrorsWindow("Something went wrong", ex != null ? ex.InnerMessage() : String.Empty, ex != null ? ex.ToLogString() : String.Empty);
                    window.Owner = Application.Current.MainWindow;
                    window.ShowDialog();
                });
            }
        }

        public static void ShowErrorWindows(Exception ex, Action<bool?> callback)
        {
            if (!NoShow)
            {
                UIThread.Invoke(() =>
                {
                    var window = new ErrorsWindow("Something went wrong", ex != null ? ex.InnerMessage() : String.Empty, ex != null ? ex.ToLogString() : String.Empty);
                    window.Owner = Application.Current.MainWindow;
                    var result = window.ShowDialog();
                    callback(result);
                });
            }
        }

        public static void ShowErrorWindows(string message, Action<bool?> callback)
        {
            if (!NoShow)
            {
                UIThread.Invoke(() =>
                {
                    var window = new ErrorsWindow("Something went wrong", message, callback);
                    window.TwoOptAvailable = true;
                    window.Owner = Application.Current.MainWindow;
                    var result = window.ShowDialog();
                    callback?.Invoke(result);
                });
            }
        }

        public static void ShowErrorWindows(ValidationResult result)
        {
            if (!NoShow)
            {
                UIThread.Invoke(() =>
                {
                    var window = new ErrorsWindow("Something went wrong", result != null ? result.Message : String.Empty, result != null ? result.TraceMessage : String.Empty);
                    window.Owner = Application.Current.MainWindow;
                    window.ShowDialog();
                });
            }
        }

        public ErrorsWindow()
        {
            InitializeComponent();
        }

        public ErrorsWindow(string header, string message, int count)
            : this()
        {
            Header = header;
            Message = message;
        }

        public ErrorsWindow(string header, string message, string traceMessage)
            : this()
        {
            Header = header;
            Message = message;
            TraceMessage = traceMessage;
            txtClipboard.Visibility = string.IsNullOrEmpty(traceMessage) ? Visibility.Collapsed : Visibility.Visible;
        }

        public ErrorsWindow(string header, string message, Action<bool?> callback)
            : this()
        {
            Header = header;
            Message = message;
            txtClipboard.Visibility = Visibility.Collapsed;
            
            if (callback != null)
            {
                TwoOptAvailable = true;
                btnCancel.Visibility = Visibility.Visible;
            }
        }

        public string Header
        {
            get => (string)GetValue(HeaderProperty);
            private set => SetValue(HeaderProperty, value);
        }

        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register("Header", typeof(string), typeof(ErrorsWindow));

        public string Message
        {
            get => (string)GetValue(MessageProperty);
            private set => SetValue(MessageProperty, value);
        }

        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register("Message", typeof(string), typeof(ErrorsWindow));

        public string TraceMessage
        {
            get => (string)GetValue(TraceMessageProperty);
            private set => SetValue(TraceMessageProperty, value);
        }

        public static readonly DependencyProperty TraceMessageProperty =
            DependencyProperty.Register("TraceMessage", typeof(string), typeof(ErrorsWindow));

        private void ActionButton_Click(object sender, RoutedEventArgs e)
        {
            (Resources["hide"] as Storyboard).Begin();
        }

        private void errorWindow_Loaded(object sender, RoutedEventArgs e)
        {
            (Resources["show"] as Storyboard).Begin();
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            Close();
        }

        private void TextBlock_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Clipboard.SetText(Message + Environment.NewLine + TraceMessage);
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            (Resources["hide"] as Storyboard).Begin();
            if (TwoOptAvailable)
                DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            (Resources["hide"] as Storyboard).Begin();
            DialogResult = false;
        }
    }
}