﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace NewHotel.WPF.Common.Notifications
{
    /// <summary>
    /// Interaction logic for ErrorsWindow.xaml
    /// </summary>
    public partial class NotificationWindow : Window
    {
        private int count = 3;
        public static bool NoShow { get; set; }
        public static void ShowNotificationWindow(string message, Window parent = null, int count = 3)
        {
            
            if (!NoShow)
            {
                UIThread.Invoke(() =>
                    {
                        var window = new NotificationWindow(message, count);
                        window.Owner = parent == null ? Application.Current.MainWindow : parent;
                        window.Show();
                    });
            }
        }

        public NotificationWindow()
        {
            InitializeComponent();
        }

        public NotificationWindow(string message, int count)
            :this()
        {
            Message = message;
            this.count = count;
        }


        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            private set { SetValue(MessageProperty, value); }
        }

        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register("Message", typeof(string), typeof(NotificationWindow));

        private void window_Loaded(object sender, RoutedEventArgs e)
        {
            (Resources["show"] as Storyboard).Begin();
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            Owner.Activate();
            this.Close();
        }

        private void Storyboard_Completed_1(object sender, EventArgs e)
        {
            (Resources["hide"] as Storyboard).BeginTime = new TimeSpan(0, 0, 0, count);
            (Resources["hide"] as Storyboard).Begin();
        }

    }
}
