﻿namespace NewHotel.WPF.Common.Models
{
    public class CollectionViewItem<T>
    {
        public CollectionViewItem(object item, T id, string description)
        {
            Item = item;
            Id = id;
            Description = description;
        }

        public T Id { get; private set; }
        public string Description { get; private set; }
        public object Item { get; private set; }
    }
}