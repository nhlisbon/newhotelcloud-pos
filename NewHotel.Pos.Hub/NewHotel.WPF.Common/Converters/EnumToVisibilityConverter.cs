﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Globalization;

namespace NewHotel.WPF.Common.Converters
{
    public class EnumToVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                if (parameter != null && parameter.ToString().Equals("!"))
                    return !value.Equals(parameter) ? Visibility.Visible : Visibility.Collapsed;
                else
                    return value.Equals(parameter) ? Visibility.Visible : Visibility.Collapsed;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}