﻿using System;
using System.Windows.Data;

namespace NewHotel.WPF.Common.Converters
{
	public sealed class UriConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (value is Uri)
				return value;

			Uri resultUri;
			return (value is string) && Uri.TryCreate((string)value, UriKind.RelativeOrAbsolute, out resultUri) ?
				resultUri : new Uri(String.Empty, UriKind.RelativeOrAbsolute);
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
