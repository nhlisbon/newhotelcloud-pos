﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace NewHotel.WPF.Common.Converters
{
    public class DateTimeFormatConverter : IValueConverter
    {
        public string Format { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return "";

            var format = !string.IsNullOrEmpty(Format) ? Format : parameter != null ? parameter.ToString(): CultureInfo.CurrentUICulture.DateTimeFormat.ShortDatePattern;
            return ((DateTime)value).ToString(format, CultureInfo.CurrentUICulture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
