﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Globalization;

namespace NewHotel.WPF.Common.Converters
{
    // Convertidor de bool a visibilidad
    [ValueConversion(typeof(object), typeof(Visibility))]
    public class NullToInvisibility : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || string.IsNullOrEmpty(value.ToString())) 
                return Visibility.Collapsed;
            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}