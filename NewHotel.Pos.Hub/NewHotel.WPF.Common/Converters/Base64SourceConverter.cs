﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace NewHotel.WPF.Common.Converters
{
    public class Base64SourceConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string)
            {
                try
                {
                    #region Resource Image
                    if (value.ToString().IndexOf("pack") != -1)
                    {
                        BitmapImage image = new BitmapImage();
                        image.BeginInit();
                        image.UriSource = new Uri((string)value);
                        image.EndInit();
                        return image;
                    }
                    #endregion
                    #region Base64 Image
                    //Is a Base64 Image
                    else
                    {
                        byte[] binaryData = System.Convert.FromBase64String((string)value);
                        BitmapImage image = new BitmapImage();
                        image.BeginInit();
                        image.StreamSource = new MemoryStream(binaryData);
                        image.EndInit();
                        return image;
                    }
                    #endregion
                }
                catch
                {
                    return new BitmapImage();
                }
            }
            return new BitmapImage();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
