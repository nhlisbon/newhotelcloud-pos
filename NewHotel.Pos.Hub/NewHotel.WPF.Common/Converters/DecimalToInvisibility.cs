﻿using System;
using System.Windows.Data;

namespace NewHotel.WPF.Common.Converters
{
    [ValueConversion(typeof(decimal), typeof(System.Windows.Visibility))]
    public class DecimalToInvisibility : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((decimal)value == 0) ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
