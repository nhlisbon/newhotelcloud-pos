﻿using System;
using System.Windows.Data;
using System.Globalization;
using NewHotel.Pos.Localization;
using LocalizedSuffix = NewHotel.Pos.Localization.LocalizedSuffix;

namespace NewHotel.WPF.Common.Converters
{
    public class LocalizedTextConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var newLine = string.Empty;

            if (value != null)
            {
                var list = value.ToString().Split('~');
                var isKey = list.Length == 1;

                for (int i = 0; i < list.Length; i++)
                {
                    if (isKey)
                    {
                        string id = list[i];
                        if (!string.IsNullOrEmpty(id))
                        {
                            var suffix = LocalizedSuffix.Label;
                            if (parameter != null)
                            {
                                if (parameter is LocalizedSuffix)
                                    suffix = (LocalizedSuffix)parameter;
                                else if (!Enum.TryParse<LocalizedSuffix>(parameter.ToString(), out suffix))
                                    newLine += LocalizationMgr.Translation(id, LocalizedSuffix.Label, parameter.ToString());
                            }

                            newLine += LocalizationMgr.Translation(id, suffix);
                        }
                        else
                            newLine += id;
                    }
                    else
                        newLine += list[i];

                    isKey = !isKey;
                }
            }

            return newLine;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}