﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace NewHotel.WPF.Common.Converters
{
    public class ImageSourceConverter : IValueConverter
    {
        private readonly Dictionary<string, byte[]> _localImage = new();
        
        public object? Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            switch (value)
            {
                case string path:
                {
                    if (Path.GetExtension(path).ToLower() == ".webp")
                        return null;
                    
                    if (path.IndexOf("pack", StringComparison.Ordinal) != -1)
                        return CreateBitmapImage(new Uri(path)); // Resource Image

                    if (!File.Exists(path))
                        return null;

                    if (!_localImage.ContainsKey(path))
                        _localImage.Add(path, File.ReadAllBytes(path));

                    return CreateBitmapImage(new MemoryStream(_localImage[path])); // Path to local image

                }
                
                case Stream stream:
                    return CreateBitmapImage(stream);
                
                case byte[] bytes:
                    return CreateBitmapImage(new MemoryStream(bytes));
                
                default:
                    return null;
            }
        }

        public object ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        
        private static BitmapImage CreateBitmapImage(Stream stream)
        {
            var image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = stream;
            image.EndInit();
            return image;
        }

        private static BitmapImage CreateBitmapImage(Uri uri)
        {
            var image = new BitmapImage();
            image.BeginInit();
            image.UriSource = uri;
            image.EndInit();
            return image;
        }
    }
}
