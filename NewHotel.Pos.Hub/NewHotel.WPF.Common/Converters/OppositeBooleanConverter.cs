﻿using System;
using System.Windows.Data;
using System.Globalization;

namespace NewHotel.WPF.Common.Converters
{
    public class OppositeBooleanConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
                return !((bool?)value).GetValueOrDefault();

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
                return !((bool?)value).GetValueOrDefault();

            return null;
        }

        #endregion
    }
}