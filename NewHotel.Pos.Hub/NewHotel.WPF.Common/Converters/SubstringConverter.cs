﻿using System;
using System.Windows.Data;

namespace NewHotel.WPF.App.Pos.Converters
{
    [ValueConversion(typeof(string), typeof(string))]
    public class SubstringConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value.ToString().Substring(0, int.Parse(parameter.ToString()));
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
