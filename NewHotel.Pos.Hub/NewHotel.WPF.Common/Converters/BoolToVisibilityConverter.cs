﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace NewHotel.WPF.App.Pos.Converters
{
    public class BoolToVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var bValue = value as bool?;

            if (!bValue.HasValue)
                return Visibility.Collapsed;

            if (parameter as string == "!")
                bValue = !bValue;

            if (bValue.Value)
                return Visibility.Visible;
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}