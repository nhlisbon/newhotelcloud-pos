﻿using System;
using System.Windows.Data;

namespace NewHotel.WPF.Common.Converters
{
    [ValueConversion(typeof(string), typeof(string))]
    public class TruncStringConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int len = 10;
            try
            {
                len = int.Parse(parameter.ToString());
            }catch{}

            if (value.ToString().Length > len + 3)
            {
                return value.ToString().Substring(0, len) + "...";
            }
            else return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
