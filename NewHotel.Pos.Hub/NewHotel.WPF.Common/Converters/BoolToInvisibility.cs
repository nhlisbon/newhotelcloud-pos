﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Globalization;

namespace NewHotel.WPF.Common.Converters
{
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BoolToInvisibility : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
                return ((bool)value) ? Visibility.Collapsed : Visibility.Visible;

            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}