﻿using NewHotel.WPF.Common.Converters;
using System.Globalization;
using System.Threading;
using System.Windows.Data;

namespace NewHotel.WPF.Common
{
    public class LocalizedMultiBinding : MultiBinding
    {
        public LocalizedMultiBinding()
        {
            ConverterCulture = (CultureInfo)Thread.CurrentThread.CurrentUICulture.Clone();
            ConverterCulture.NumberFormat.CurrencySymbol = string.Empty;
        }
    }

    public class LocalizedBinding : Binding
    {
        public LocalizedBinding()
            : base()
        {
            ConverterCulture = (CultureInfo)Thread.CurrentThread.CurrentUICulture.Clone();
            ConverterCulture.NumberFormat.CurrencySymbol = string.Empty;
        }

        public LocalizedBinding(string path)
            : base(path)
        {
            ConverterCulture = (CultureInfo)Thread.CurrentThread.CurrentUICulture.Clone();
            ConverterCulture.NumberFormat.CurrencySymbol = string.Empty;
        }
    }

    public class BoolToInvisibilityBinding : LocalizedBinding
    {
        public BoolToInvisibilityBinding()
            : base()
        {
            Converter = new BoolToInvisibility();
        }

        public BoolToInvisibilityBinding(string path)
            : base(path)
        {
            Converter = new BoolToInvisibility();
        }
    }

    public class BoolToVisibilityBinding : LocalizedBinding
    {
        public BoolToVisibilityBinding()
            : base()
        {
            Converter = new BoolToVisibility();
        }

        public BoolToVisibilityBinding(string path)
            : base(path)
        {
            Converter = new BoolToVisibility();
        }
    }
}