﻿namespace NewHotel.WPF.Common.Controls
{
    public interface ITSKControlsKey
    {
        void RunCommandEnter(object sender, object control);
        void RunCommandTab(object sender, object control);
    }
}
