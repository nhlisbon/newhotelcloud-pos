﻿using System;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace NewHotel.WPF.Common.Controls
{
    /// <summary>
    /// Interaction logic for Joystick.xaml
    /// </summary>
    public partial class Joystick : UserControl
    {

        public event Action<Joystick> Movement;
        Timer timer = null;

        private Ellipse control = null;

        public Joystick()
        {
            InitializeComponent();
            timer = new Timer(Interval);
            timer.Enabled = false;
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            control = Resources["control"] as Ellipse;

            Color = Colors.Black;
            LinesColor = Colors.Black;
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (Movement != null)
                Movement(this);
        }

        long _interval = 100;
        public long Interval
        {
            get
            {
                return _interval;
            }
            set
            {
                timer.Interval = _interval = value;
            }
        }


        public Color LinesColor
        {
            get { return (Color)GetValue(LinesColorProperty); }
            set { SetValue(LinesColorProperty, value); }
        }

        public static readonly DependencyProperty LinesColorProperty =
            DependencyProperty.Register("LinesColor", typeof(Color), typeof(Joystick));


        public Color Color
        {
            get { return (Color)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

        public static readonly DependencyProperty ColorProperty =
            DependencyProperty.Register("Color", typeof(Color), typeof(Joystick));


        public double Offset { get; set; }
        public double OffsetX { get; set; }
        public double OffsetY { get; set; }
        public int Direction { get; set; }


        private void canvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            timer.Enabled = true;
            ellipse.Visibility = System.Windows.Visibility.Collapsed;

            var currentPointInCanvas = e.GetPosition(canvas);

            control.Width = ellipse.ActualWidth;
            control.Height = ellipse.ActualHeight;
            canvas.Children.Add(control);
            control.UpdateLayout();

            Canvas.SetLeft(control, currentPointInCanvas.X - control.ActualWidth / 2);
            Canvas.SetTop(control, currentPointInCanvas.Y - control.ActualHeight / 2);

            //calculos
            OffsetX = (currentPointInCanvas.X - canvas.ActualWidth / 2) / canvas.ActualWidth;
            OffsetY = (currentPointInCanvas.Y - canvas.ActualHeight / 2) / canvas.ActualHeight;
            Offset = Math.Sqrt(OffsetX * OffsetX + OffsetY * OffsetY);
        }

        private void canvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (timer.Enabled)
            {
                canvas.Children.Clear();
                timer.Enabled = false;
                ellipse.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void canvas_MouseLeave(object sender, MouseEventArgs e)
        {
            if (timer.Enabled)
            {
                canvas.Children.Clear();
                timer.Enabled = false;
                ellipse.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (timer.Enabled)
            {
                var currentPointInCanvas = e.GetPosition(canvas);
                Canvas.SetLeft(control, currentPointInCanvas.X - control.ActualWidth / 2);
                Canvas.SetTop(control, currentPointInCanvas.Y - control.ActualHeight / 2);

                //calculos
                OffsetX = (currentPointInCanvas.X - canvas.ActualWidth / 2) / canvas.ActualWidth;
                OffsetY = (currentPointInCanvas.Y - canvas.ActualHeight / 2) / canvas.ActualHeight;
                Offset = Math.Sqrt(OffsetX * OffsetX + OffsetY * OffsetY);

            }
        }
    }
}
