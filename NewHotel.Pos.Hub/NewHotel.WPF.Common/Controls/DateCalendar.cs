﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;

namespace NewHotel.WPF.Common.Controls
{
    public class DateCalendar : DatePicker
    {
        public static readonly DependencyProperty EditableProperty =
            DependencyProperty.Register("Editable", typeof(bool),
            typeof(DateCalendar), new PropertyMetadata(false));

        public bool Editable
        {
            get { return (bool)GetValue(EditableProperty); }
            set { SetValue(EditableProperty, value); }
        }
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            var textBox = (DatePickerTextBox)GetTemplateChild("PART_TextBox");
            var binding = new Binding { Source = this, Path = new PropertyPath(DateCalendar.EditableProperty) };
            textBox.SetBinding(DatePickerTextBox.FocusableProperty, binding);
        }
    }
}
