﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace NewHotel.WPF.Common.Controls.Busy
{
    /// <summary>
    /// Interaction logic for BusyControl.xaml
    /// </summary>
    public partial class BusyControl : UserControl
    {
        private static AutoResetEvent _event = new AutoResetEvent(false);
        private static Thread _thread;
        private readonly Storyboard _storyboard;

        public static BusyControl Instance { get; set; }

        public BusyControl()
        {
            InitializeComponent();
            _storyboard = Resources["Loading"] as Storyboard;
        }

        public void Show(string message = "Busy")
        {
            Dispatcher.Invoke(() =>
            {
                titleTextBlock.Tag = message;
                _storyboard.Begin();
            });
        }

        public void Close()
        {
            Dispatcher.Invoke(() =>
            {
                _storyboard.Stop();
            });
        }

        public static HostVisual CreateElementOnWorkerThread()
        {
            // Create the HostVisual that will "contain" the VisualTarget
            // on the worker thread.
            var hostVisual = new HostVisual();

            // Spin up a worker thread, and pass it the HostVisual that it
            // should be part of.
            _thread = new Thread(new ParameterizedThreadStart(WorkerThread));
            _thread.SetApartmentState(ApartmentState.STA);
            _thread.IsBackground = true;
            _thread.Start(hostVisual);

            // Wait for the worker thread to spin up and create the VisualTarget.
            _event.WaitOne();

            return hostVisual;
        }

        private static void WorkerThread(object arg)
        {
            // Create the VisualTargetPresentationSource and then signal the
            // calling thread, so that it can continue without waiting for us.
            var hostVisual = (HostVisual)arg;

            var visualTargetPS = new VisualTargetPresentationSource(hostVisual);

            // Create a MediaElement and use it as the root visual for the
            // VisualTarget.
            Instance = new BusyControl();
            visualTargetPS.RootVisual = Instance;

            _event.Set();

            // Run a dispatcher for this worker thread.  This is the central
            // processing loop for WPF.
            Dispatcher.Run();
        }

    }

    [ContentProperty("Child")]
    public class VisualWrapper : FrameworkElement
    {
        private Visual _child;

        public Visual Child
        {
            get
            {
                return _child;
            }

            set
            {
                if (_child != null)
                    RemoveVisualChild(_child);

                _child = value;

                AddVisualChild(_child);
            }
        }

        protected override Visual GetVisualChild(int index)
        {
            if (_child != null && index == 0)
                return _child;
            
            throw new ArgumentOutOfRangeException("index");
        }

        protected override int VisualChildrenCount
        {
            get { return _child != null ? 1 : 0; }
        }
    }

    public class VisualTargetPresentationSource : PresentationSource
    {
        private VisualTarget _visualTarget;

        public VisualTargetPresentationSource(HostVisual hostVisual)
        {
            _visualTarget = new VisualTarget(hostVisual);
        }

        public override Visual RootVisual
        {
            get { return _visualTarget.RootVisual; }
            set
            {
                var oldRoot = _visualTarget.RootVisual;

                // Set the root visual of the VisualTarget.  This visual will
                // now be used to visually compose the scene.
                _visualTarget.RootVisual = value;

                // Tell the PresentationSource that the root visual has
                // changed.  This kicks off a bunch of stuff like the
                // Loaded event.
                RootChanged(oldRoot, value);

                // Kickoff layout...
                var rootElement = value as UIElement;
                if (rootElement != null)
                {
                    rootElement.Measure(new Size(Double.PositiveInfinity, Double.PositiveInfinity));
                    rootElement.Arrange(new Rect(rootElement.DesiredSize));
                }
            }
        }

        protected override CompositionTarget GetCompositionTargetCore()
        {
            return _visualTarget;
        }

        public override bool IsDisposed
        {
            get { return false; }
        }
    }
}