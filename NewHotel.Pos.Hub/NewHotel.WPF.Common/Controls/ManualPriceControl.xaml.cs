﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewHotel.WPF.Common.Controls
{
    /// <summary>
    /// Interaction logic for ManualValue.xaml
    /// </summary>
    public partial class ManualPriceControl : UserControl
    {
        public ManualPriceControl()
        {
            InitializeComponent();
        }

        public event Action<decimal>? ValueChange;
        public event Action<string>? DescriptionChange;

        public decimal Value
        {
            get => (decimal)GetValue(ValueProperty);
            set => SetValue(ValueProperty, value);
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register(nameof(Value), typeof(decimal), typeof(ManualPriceControl), new PropertyMetadata(ValueChanged));
        
        private static void ValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = (ManualPriceControl)d;
            obj.ValueChange?.Invoke((decimal)e.NewValue);
        }

        public string Description
        {
            get => (string)GetValue(DescriptionProperty);
            set => SetValue(DescriptionProperty, value);
        }
        
        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register(nameof(Description), typeof(string), typeof(ManualPriceControl), new PropertyMetadata(DescriptionChanged));
        
        private static void DescriptionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = (ManualPriceControl)d;
            obj.DescriptionChange?.Invoke((string)e.NewValue);
        }
    }
}
