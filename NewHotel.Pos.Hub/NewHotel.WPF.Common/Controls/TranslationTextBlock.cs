﻿using System.Windows;
using System.Windows.Controls;
using NewHotel.Localization;

namespace NewHotel.WPF.Common.Controls
{
    public class TranslationTextBlock : TextBlock
    {
        #region Dependency Properties

        public static DependencyProperty TranslationIdProperty =
            DependencyProperty.Register(nameof(TranslationId), typeof(string), typeof(TranslationTextBlock), new PropertyMetadata(null, TranslationIdPropertyChanged));

        public string TranslationId
        {
            get => (string)GetValue(TranslationIdProperty);
            set => SetValue(TranslationIdProperty, value);
        }

        public static DependencyProperty SuffixProperty =
            DependencyProperty.Register(nameof(Suffix), typeof(LocalizedSuffix), typeof(TranslationTextBlock), new PropertyMetadata(LocalizedSuffix.Label, SuffixPropertyChanged));

        public LocalizedSuffix Suffix
        {
            get => (LocalizedSuffix)GetValue(SuffixProperty);
            set => SetValue(SuffixProperty, value);
        }

        #endregion
        #region Constructor

        public TranslationTextBlock()
        {
        }

        #endregion
        #region Private Methods

        private static void TranslationIdPropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var source = (TranslationTextBlock)sender;
            source.Refresh((string)e.NewValue, source.Suffix);
        }

        private static void SuffixPropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var source = (TranslationTextBlock)sender;
            source.Refresh(source.TranslationId, (LocalizedSuffix)e.NewValue);
        }

        private void Refresh(string translationId, LocalizedSuffix suffix)
        {
            if (!string.IsNullOrEmpty(translationId))
                Text = new LocalizedText(translationId, suffix);
        }

        #endregion
        #region Public Methods

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            Refresh(TranslationId, Suffix);
        }

        #endregion
    }
}