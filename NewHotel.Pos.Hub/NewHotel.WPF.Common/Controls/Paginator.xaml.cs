﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using NewHotel.WPF.Common.Controls.Buttons;

namespace NewHotel.WPF.Common.Controls
{
    /// <summary>
    /// Interaction logic for Paginator.xaml
    /// </summary>
    public partial class Paginator : ScrollViewer, INotifyPropertyChanged
    {
        #region Variables + Constructor

        private bool _isSelected;
        private int _currentPage;
        private bool _isScroll;
        private bool _isCustom;
        private double _newHorizontalOffset;
        private double _newVerticalOffset;
        private ControlTemplate _iniTemplate;
        private bool _hasPaginate;
        private int _countPages;

        public Paginator()
        {
            InitializeComponent();
        }

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Properties

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                if (IsSelected) BeginSelectAnimation();
                else BeginDeSelectAnimation();
            }
        }

        public int CountPages
        {
            get { return _countPages; }
            set
            {
                _countPages = value;
                HasPaginate = CountPages > 1;
            }
        }

        public int CurrentPage
        {
            get { return _currentPage; }
            set
            {
                SetCurrentPageValue(value);
                MoveScrollToCurrentPage();
            }
        }

        private void SetCurrentPageValue(int value)
        {
            if (value < 1)
            {
                value = 1;
            }
            else if (value > _countPages)
            {
                value = _countPages;
            }

            _currentPage = value;
        }

        private void ValidateCurrentPage()
        {
            SetCurrentPageValue(_currentPage);
        }

        public bool IsScroll
        {
            get { return _isScroll; }
            set
            {
                _isCustom = true;
                _isScroll = value;
                if (_isScroll)
                {
                    Template = Orientation == Orientation.Vertical
                        ? ScrollVerticalTemplate
                        : ScrollHorizontalTemplate;
                }
                else Template = PaginatorTemplate;
            }
        }

        public bool IsCustom
        {
            get { return _isCustom; }
            set
            {
                if (_isCustom != value)
                {
                    _isCustom = value;
                    if (_isCustom)
                    {
                        Template = _isScroll
                            ? ScrollTemplate
                            : PaginatorTemplate;
                    }
                    else Template = _iniTemplate;
                }
            }
        }

        public bool HasPaginate
        {
            get { return _hasPaginate; }
            set
            {
                _hasPaginate = value;
                OnPropertyChanged(nameof(HasPaginate));
            }
        }

        #region ShowCustomScrollButtons

        public bool ShowCustomScrollButtons
        {
            get { return (bool)GetValue(ShowCustomScrollButtonsProperty); }
            set { SetValue(ShowCustomScrollButtonsProperty, value); }
        }

        public static readonly DependencyProperty ShowCustomScrollButtonsProperty =
            DependencyProperty.Register(nameof(ShowCustomScrollButtons), typeof(bool), typeof(Paginator));

        #endregion

        #region Orientation

        public Orientation Orientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register(nameof(Orientation), typeof(Orientation), typeof(Paginator), new PropertyMetadata(Orientation.Vertical));

        #endregion

        #region LastPageFocusMode

        public bool LastPageFocusMode
        {
            get { return (bool)GetValue(LastPageFocusModeProperty); }
            set { SetValue(LastPageFocusModeProperty, value); }
        }

        public static readonly DependencyProperty LastPageFocusModeProperty =
            DependencyProperty.Register(nameof(LastPageFocusMode), typeof(bool), typeof(Paginator));

        #endregion

        #region ContentOpacity

        public double ContentOpacity
        {
            get { return (double)GetValue(ContentOpacityProperty); }
            set { SetValue(ContentOpacityProperty, value); }
        }

        public static readonly DependencyProperty ContentOpacityProperty =
            DependencyProperty.Register(nameof(ContentOpacity), typeof(double), typeof(Paginator), new PropertyMetadata((double)1));

        #endregion

        #region SelectionOpacity

        public double SelectionOpacity
        {
            get { return (double)GetValue(SelectionOpacityProperty); }
            set { SetValue(SelectionOpacityProperty, value); }
        }

        public static readonly DependencyProperty SelectionOpacityProperty =
            DependencyProperty.Register(nameof(SelectionOpacity), typeof(double), typeof(Paginator));

        #endregion

        #region PageButtonColor

        public Brush PageButtonBrush
        {
            get { return (Brush)GetValue(PageButtonBrushProperty); }
            set { SetValue(PageButtonBrushProperty, value); }
        }

        public static readonly DependencyProperty PageButtonBrushProperty =
            DependencyProperty.Register(nameof(PageButtonBrush), typeof(Brush), typeof(Paginator), new PropertyMetadata(Brushes.DarkOrange));

        #endregion

        #region PageButtonWidth

        public double PageButtonWidth
        {
            get { return (double)GetValue(PageButtonWidthProperty); }
            set { SetValue(PageButtonWidthProperty, value); }
        }

        public static readonly DependencyProperty PageButtonWidthProperty =
            DependencyProperty.Register(nameof(PageButtonWidth), typeof(double), typeof(Paginator), new PropertyMetadata((double)16));

        #endregion

        #region PageButtonHeigth

        public double PageButtonHeigth
        {
            get { return (double)GetValue(PageButtonHeigthProperty); }
            set { SetValue(PageButtonHeigthProperty, value); }
        }

        public static readonly DependencyProperty PageButtonHeigthProperty =
            DependencyProperty.Register(nameof(PageButtonHeigth), typeof(double), typeof(Paginator), new PropertyMetadata((double)32));

        #endregion

        #region Private

        private ControlTemplate ScrollTemplate
        {
            get { return (ControlTemplate)Resources["scrollTemplate"]; }
        }

        private ControlTemplate ScrollHorizontalTemplate
        {
            get { return (ControlTemplate)Resources["scrollHorizontalTemplate"]; }
        }

        private FrameworkElement PagesNavegatorContainter
        {
            get { return (FrameworkElement)Resources["PagesNavegatorContainter"]; }
        }

        private ControlTemplate ScrollVerticalTemplate
        {
            get { return (ControlTemplate)Resources["scrollVerticalTemplate"]; }
        }

        private ControlTemplate PaginatorHorizontalScrollTemplate
        {
            get { return (ControlTemplate)Resources["paginatorHorizontalScrollTemplate"]; }
        }

        private ControlTemplate PaginatorTemplate
        {
            get { return (ControlTemplate)Resources["paginatorTemplate"]; }
        }

        #endregion

        #endregion

        #region Methods

        #region Events Handlers

        private void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            ValidateCurrentPage();
            int newCountPages = GetPagesCount();

            if (IsCustom && (!IsScroll || Orientation == Orientation.Horizontal))
            {
                if (newCountPages != CountPages)
                {
                    CountPages = newCountPages;
                    if (LastPageFocusMode)
                    {
                        CurrentPage = CountPages;
                        // ScrollToEnd();
                    }


                    // if (newCountPages > CountPages)
                    // {
                    //     CountPages = newCountPages;
                    //     if (CurrentPage == 1 && LastPageFocusMode)
                    //         CurrentPage = CountPages;
                    // }
                    // else
                    // {
                    //     CountPages = newCountPages;
                    //     if (CurrentPage > CountPages && LastPageFocusMode)
                    //         CurrentPage = CountPages;
                    // }
                }
            }

            if (_isCustom && _isScroll)
                ShowCustomScrollButtons = HasPaginate;
        }

        protected override void OnTemplateChanged(ControlTemplate oldTemplate, ControlTemplate newTemplate)
        {
            if (_iniTemplate == null)
                _iniTemplate = oldTemplate;
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            if (Orientation == Orientation.Vertical)
                ScrollToVerticalOffset(_newVerticalOffset);
            else ScrollToHorizontalOffset(_newHorizontalOffset);
            BeginShowContentAnimation();
        }

        private void me_Loaded(object sender, RoutedEventArgs e)
        {
            if (IsCustom)
            {
                if (IsScroll)
                {
                    Template = Orientation == Orientation.Vertical
                        ? ScrollVerticalTemplate
                        : PaginatorHorizontalScrollTemplate;
                }
                else Template = PaginatorTemplate;
            }
        }
        
        

        private void NextPageButtonDown_OnClick(object sender, RoutedEventArgs e)
        {
            GoToNextPage();
        }

        private void NextPageButtonDownContainer(object sender, MouseButtonEventArgs e)
        {
            SetIsPressedToActionButtonChildrens(sender, true);
            GoToNextPage();
        }

        private void NextPageButtonUpContainer(object sender, MouseButtonEventArgs e)
        {
            SetIsPressedToActionButtonChildrens(sender, false);
        }

        private void PreviousPageButtonDown_OnClick(object sender, RoutedEventArgs e)
        {
            GoToPreviousPage();
        }

        private void PreviousPageButtonDownContainer(object sender, MouseButtonEventArgs e)
        {
            SetIsPressedToActionButtonChildrens(sender, true);
            GoToPreviousPage();
        }

        private void PreviousPageButtonUpContainer(object sender, MouseButtonEventArgs e)
        {
            SetIsPressedToActionButtonChildrens(sender, false);
        }

        private void GoToNextPage()
        {
            ValidateCurrentPage();
            if (CurrentPage < CountPages)
                CurrentPage++;
        }

        private void GoToPreviousPage()
        {
            ValidateCurrentPage();
            if (CurrentPage > 1)
                CurrentPage--;
        }


        private void SetIsPressedToActionButtonChildrens(object obj, bool value)
        {
            var panel = obj as Panel;
            if (panel != null)
            {
                foreach (var child in panel.Children)
                {
                    var button = child as ActionButton;
                    if (button != null)
                        button.IsPressed = value;
                }
            }
        }

        #endregion

        #region Aux

        private int GetPagesCount()
        {
            if (Orientation == Orientation.Vertical)
            {
                if (ViewportHeight == 0) return 0;
                return (int)Math.Ceiling(ExtentHeight / ViewportHeight);
            }

            if (ViewportWidth == 0) return 0;
            return (int)Math.Ceiling(ExtentWidth / ViewportWidth);
        }

        private void MoveScrollToCurrentPage()
        {
            if (Orientation == Orientation.Vertical)
            {
                _newVerticalOffset = (CurrentPage - 1) * ViewportHeight;

                // if (VerticalOffset != _newVerticalOffset)
                    BeginHideContentAnimamtion();
            }
            else
            {
                _newHorizontalOffset = (CurrentPage - 1) * ViewportWidth;

                // if (HorizontalOffset != _newHorizontalOffset)
                    BeginHideContentAnimamtion();
            }
        }

        private void BeginHideContentAnimamtion()
        {
            ((Storyboard)Resources["HideContent"]).Begin();
        }

        private void BeginShowContentAnimation()
        {
            ((Storyboard)Resources["ShowContent"]).Begin();
        }

        private void BeginDeSelectAnimation()
        {
            ((Storyboard)Resources["Deselect"]).Begin();
        }

        private void BeginSelectAnimation()
        {
            ((Storyboard)Resources["Select"]).Begin();
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #endregion

        private void Paginator_OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            CurrentPage = 1;
            CountPages = GetPagesCount();
            if (!LastPageFocusMode) return;
            CurrentPage = CountPages;
            // ScrollToEnd();
        }

        public new void ScrollToEnd()
        {
            CountPages = GetPagesCount();
            CurrentPage = CountPages;
        }
    }
}