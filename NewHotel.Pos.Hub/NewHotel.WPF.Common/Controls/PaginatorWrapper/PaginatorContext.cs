﻿using System.Collections.Generic;

namespace NewHotel.WPF.Common.Models
{
    public interface IPaginatorContext
    {
        int CurrentPage { get; set; }
        int ItemsPerPage { get; }
        int ItemsCount { get; }
        int PagesCount { get; }
        IEnumerable<object> GetItemsFromPage(int page);
    }
}
