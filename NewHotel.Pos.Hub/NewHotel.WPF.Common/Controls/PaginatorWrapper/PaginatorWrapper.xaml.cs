﻿using NewHotel.WPF.Common.Models;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace NewHotel.WPF.Common.Controls
{
    /// <summary>
    /// Interaction logic for PaginatorWrapper.xaml
    /// </summary>
    public partial class PaginatorWrapper : UserControl
    {
        public PaginatorWrapper()
        {
            InitializeComponent();
        }

        public ItemsControl ItemsControl
        {
            get { return (ItemsControl)GetValue(ItemsControlProperty); }
            set { SetValue(ItemsControlProperty, value); }
        }

        public static readonly DependencyProperty ItemsControlProperty =
            DependencyProperty.Register("ItemsControl", typeof(ItemsControl), typeof(Paginator), new UIPropertyMetadata(0));



        public IPaginatorContext PaginatorContext
        {
            get
            {
                return DataContext as IPaginatorContext;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (PaginatorContext.CurrentPage <= 0) return;

            var list = PaginatorContext.GetItemsFromPage(PaginatorContext.CurrentPage - 1);
            if (list != null && list.Count() > 0)
            {
                PaginatorContext.CurrentPage--;
                ItemsControl.ItemsSource = list;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var list = PaginatorContext.GetItemsFromPage(PaginatorContext.CurrentPage + 1);
            if (list != null && list.Count() > 0)
            {
                PaginatorContext.CurrentPage++;
                ItemsControl.ItemsSource = list;
            }
        }
    }
}
