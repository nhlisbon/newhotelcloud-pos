﻿using System.Windows;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Editors;
using NewHotel.Pos.Core.Ext;

namespace NewHotel.WPF.Common.Controls
{
    public class NumericEditControl : SpinEdit
    {
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            ThemeManager.SetTheme(this, Theme.FindTheme(Theme.Office2013Name));
        }
    }

    public class IntEditControl : NumericEditControl
    {
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            EditValueType = typeof(int);
        }
    }

    public class DecimalEditControl : NumericEditControl
    {
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            EditValueType = typeof(decimal);
            EditValueChanging += OnEditValueChanging;
        }

        public decimal? EditVal => (decimal?)EditValue;

        public int? DecimalPlaces
        {
            get => (int?)GetValue(DecimalPlacesProperty);
            set => SetValue(DecimalPlacesProperty, value);
        }

        public static readonly DependencyProperty DecimalPlacesProperty =
            DependencyProperty.Register(nameof(DecimalPlaces), typeof(int?), typeof(DecimalEditControl));

        private void OnEditValueChanging(object sender, EditValueChangingEventArgs e)
        {
            if (DecimalPlaces.HasValue && e.NewValue != null && ((decimal)e.NewValue).DecimalPlaces() > DecimalPlaces)
            {
                e.IsCancel = true;
                e.Handled = true;
            }
        }
    }
}