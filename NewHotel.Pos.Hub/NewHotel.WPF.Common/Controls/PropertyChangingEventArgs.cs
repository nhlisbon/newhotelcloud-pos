﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace NewHotel.WPF.Common.Controls
{
	public class NHPropertyChangingEventArgs : PropertyChangingEventArgs
	{
		public object OldValue { get; }
		public object NewValue { get; set; }

		public NHPropertyChangingEventArgs(string propertyName, object oldValue, object newValue) : base(propertyName)
		{
			OldValue = oldValue;
			NewValue = newValue;
		}
	}
}
