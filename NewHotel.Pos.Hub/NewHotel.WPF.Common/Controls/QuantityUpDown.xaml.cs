﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using NewHotel.Pos.Core.Ext;

namespace NewHotel.WPF.Common.Controls
{
    /// <summary>
    /// Interaction logic for QuantityUpDown.xaml
    /// </summary>
    public partial class QuantityUpDown : UserControl
    {
        #region Variables + Constructor

        private Point _firstPoint, _prevPoint;

        public QuantityUpDown()
        {
            InitializeComponent();
        }

        #endregion
        #region Events

        public event Action<object, NHPropertyChangedEventArgs> ValueChanged;
        public event Func<object, NHPropertyChangingEventArgs, bool> ValueChanging;

        #endregion
        #region Properties

        #region Value

        public decimal Value
        {
            get { return (decimal)GetValue(ValueProperty); }
            set
            {
                if (MinValue.HasValue && value < MinValue.Value)
                    return;
                if (MaxValue.HasValue && value > MaxValue.Value)
                    return;

                SetValue(ValueProperty, value);
            }
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register(nameof(Value), typeof(decimal), typeof(QuantityUpDown), new PropertyMetadata(decimal.Zero, OnValuePropertyChanged, OnValuePropertyChanging));

        private static void OnValuePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (QuantityUpDown)d;
            control.OnValueChanged(control, new NHPropertyChangedEventArgs(nameof(Value)) { NewValue = e.NewValue, OldValue = e.OldValue });
            SetQuantityDisplayFormat(control);
        }

        private static object OnValuePropertyChanging(DependencyObject d, object value)
        {
            var control = (QuantityUpDown)d;
            return control.OnValueChanging((decimal)value);
        }

        public bool ShowEditor
        {
            get { return (bool)GetValue(ShowEditorProperty); }
            set { SetValue(ShowEditorProperty, value); }
        }

        public static readonly DependencyProperty ShowEditorProperty =
            DependencyProperty.Register(nameof(ShowEditor), typeof(bool), typeof(QuantityUpDown), new PropertyMetadata(false));

        #endregion
        #region MinValue

        public decimal? MinValue
        {
            get { return (decimal?)GetValue(MinValueProperty); }
            set { SetValue(MinValueProperty, value); }
        }

        public static readonly DependencyProperty MinValueProperty =
            DependencyProperty.Register(nameof(MinValue), typeof(decimal?), typeof(QuantityUpDown));

        #endregion
        #region MaxValue

        public decimal? MaxValue
        {
            get { return (decimal?)GetValue(MaxValueProperty); }
            set { SetValue(MaxValueProperty, value); }
        }

        public static readonly DependencyProperty MaxValueProperty =
            DependencyProperty.Register(nameof(MaxValue), typeof(decimal?), typeof(QuantityUpDown));

        #endregion
        #region CanDecrementValue

        public bool CanDecrementValue
        {
            get { return (bool)GetValue(CanDecrementValueProperty); }
            set { SetValue(CanDecrementValueProperty, value); }
        }

        public static readonly DependencyProperty CanDecrementValueProperty =
            DependencyProperty.Register(nameof(CanDecrementValue), typeof(bool), typeof(QuantityUpDown), new PropertyMetadata(true));

        #endregion
        #region CanIncrementValue

        public bool CanIncrementValue
        {
            get { return (bool)GetValue(CanIncrementValueProperty); }
            set { SetValue(CanIncrementValueProperty, value); }
        }

        public static readonly DependencyProperty CanIncrementValueProperty =
            DependencyProperty.Register(nameof(CanIncrementValue), typeof(bool), typeof(QuantityUpDown), new PropertyMetadata(true));

        #endregion
        #region IsReadonly

        public bool IsReadonly
        {
            get { return (bool)GetValue(IsReadonlyProperty); }
            set { SetValue(IsReadonlyProperty, value); }
        }

        public static readonly DependencyProperty IsReadonlyProperty =
            DependencyProperty.Register(nameof(IsReadonly), typeof(bool), typeof(QuantityUpDown), new PropertyMetadata(false));

        #endregion
        #region Increment

        public decimal Increment
        {
            get { return (decimal)GetValue(IncrementProperty); }
            set { SetValue(IncrementProperty, value); }
        }

        public static readonly DependencyProperty IncrementProperty =
            DependencyProperty.Register(nameof(Increment), typeof(decimal), typeof(QuantityUpDown), new PropertyMetadata(OnIncrementPropertyChangued));

        private static void OnIncrementPropertyChangued(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SetQuantityDisplayFormat((QuantityUpDown)d);
        }

        #endregion
        #region IsPressed

        public bool IsPressed
        {
            get { return (bool)GetValue(IsPressedProperty); }
            set { SetValue(IsPressedProperty, value); }
        }

        public static readonly DependencyProperty IsPressedProperty =
            DependencyProperty.Register(nameof(IsPressed), typeof(bool), typeof(QuantityUpDown));

        #endregion

        #endregion
        #region Methods

        private void SubButton_Click(object sender, RoutedEventArgs e)
        {
            if(Value > Increment)
                Value -= Increment;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            Value += Increment;
        }

        private void UserControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            IsPressed = true;
            _firstPoint = e.GetPosition(this);
            _prevPoint = _firstPoint;
        }

        private void UserControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (IsPressed)
                IsPressed = false;
        }

        private void UserControl_MouseLeave(object sender, MouseEventArgs e)
        {
            if (IsPressed)
                IsPressed = false;
        }

        private void UserControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (IsPressed)
            {
                var newPoint = e.GetPosition(this);
                var dist = (int)ActualWidth / 11;

                for (int i = 1; i < 11; i++)
                {
                    if (_prevPoint.X <= i * dist && i * dist <= newPoint.X)
                        Value += Increment;
                    if (_prevPoint.X >= i * dist && i * dist >= newPoint.X)
                        Value -= Increment;
                }

                _prevPoint = newPoint;
            }
        }

        private static void SetQuantityDisplayFormat(QuantityUpDown ctrl)
        {
            var digits = Math.Max(ctrl.Increment.GetAmountOfDecimals(), ctrl.Value.GetAmountOfDecimals());
            var bind = new Binding
            {
                Path = new PropertyPath(nameof(Value)),
                ElementName = "userControl",
                StringFormat = "F" + digits.ToString()
            };

            BindingOperations.SetBinding(ctrl.tbQuantity, TextBlock.TextProperty, bind);
        }
       
        protected virtual void OnValueChanged(object arg1, NHPropertyChangedEventArgs arg2)
        {
            ValueChanged?.Invoke(arg1, arg2);
        }

        protected virtual object OnValueChanging(decimal newValue)
        {
            if (ValueChanging != null)
            {
                var args = new NHPropertyChangingEventArgs(nameof(Value), Value, newValue);
                return ValueChanging(this, args) ? args.NewValue : args.OldValue;
            }

            return newValue;
        }

        public void Clear()
        {
            edtQuantity.Clear();
        }

        #endregion
    }
}