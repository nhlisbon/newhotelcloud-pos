﻿using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace NewHotel.WPF.Common.Controls.Buttons
{
    //F111026:ped025:02:logica del boton para acciones alternadas
    /// <summary>
    /// Interaction logic for ActionButton.xaml
    /// </summary>
    public partial class AltActionButton : ButtonBase
    {
        public event RoutedEventHandler ClickOnFirst;
        public event RoutedEventHandler ClickOnSecond;

        public AltActionButton()
            :this(true, true)
        { }

        public AltActionButton(bool isFirst = true, bool isAlt = true)
        {
            Click += new RoutedEventHandler(AltActionButton_Click);
            //IsFirst = isFirst;
            IsAlt = isAlt;
        }

        void AltActionButton_Click(object sender, RoutedEventArgs e)
        {
            if (IsFirst)
            {
                if (ClickOnFirst != null)
                    ClickOnFirst(sender, e);
            }
            else if (ClickOnSecond != null)
                ClickOnSecond(sender, e);
            if (IsAlt)
                IsFirst = !IsFirst;
        }

        public bool IsFirst
        {
            get { return (bool)GetValue(IsFirstProperty); }
            set { SetValue(IsFirstProperty, value); }
        }
        public static readonly DependencyProperty IsFirstProperty =
            DependencyProperty.Register("IsFirst", typeof(bool), typeof(AltActionButton), new PropertyMetadata(true));

        public bool IsAlt { get; set; }

        public bool ShowText
        {
            get { return (bool)GetValue(ShowTextProperty); }
            set { SetValue(ShowTextProperty, value); }
        }

        public static readonly DependencyProperty ShowTextProperty =
            DependencyProperty.Register("ShowText", typeof(bool), typeof(AltActionButton));

        public bool ShowImage
        {
            get { return (bool)GetValue(ShowImageProperty); }
            set { SetValue(ShowImageProperty, value); }
        }

        public static readonly DependencyProperty ShowImageProperty =
            DependencyProperty.Register("ShowImage", typeof(bool), typeof(AltActionButton));

        public ImageSource FirstImageSource
        {
            get { return (ImageSource)GetValue(FirstImageSourceProperty); }
            set { SetValue(FirstImageSourceProperty, value); }
        }

        public string FirstText
        {
            get { return (string)GetValue(FirstTextProperty); }
            set { SetValue(FirstTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FirstTextProperty =
            DependencyProperty.Register("FirstText", typeof(string), typeof(AltActionButton));


        public static readonly DependencyProperty FirstImageSourceProperty =
            DependencyProperty.Register("FirstImageSource", typeof(ImageSource), typeof(AltActionButton));

        public ImageSource SecondImageSource
        {
            get { return (ImageSource)GetValue(SecondImageSourceProperty); }
            set { SetValue(SecondImageSourceProperty, value); }
        }

        public string SecondText
        {
            get { return (string)GetValue(SecondTextProperty); }
            set { SetValue(SecondTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SecondTextProperty =
            DependencyProperty.Register("SecondText", typeof(string), typeof(AltActionButton));


        public static readonly DependencyProperty SecondImageSourceProperty =
            DependencyProperty.Register("SecondImageSource", typeof(ImageSource), typeof(AltActionButton));

    }
}
