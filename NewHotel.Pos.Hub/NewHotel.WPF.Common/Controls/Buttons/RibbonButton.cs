﻿using System.Windows;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Windows.Controls.Primitives;

namespace NewHotel.WPF.Common.Controls.Buttons
{
    /// <summary>
    /// Interaction logic for RibbonButton.xaml
    /// </summary>
    public partial class RibbonButton : CheckButton
    {
        public RibbonButton()
        {
            Items = new ObservableCollection<ButtonBase>();
        }

        private void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (ButtonBase item in e.NewItems)
                        item.Click += Item_Click;
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (ButtonBase item in e.OldItems)
                        item.Click -= Item_Click;
                    break;
            }
        }

        protected virtual void Item_Click(object sender, RoutedEventArgs e)
        {
            IsChecked = false;
        }

        public ObservableCollection<ButtonBase> Items
        {
            get { return (ObservableCollection<ButtonBase>)GetValue(ItemsProperty); }
            set
            { 
                if (Items != null)
                    Items.CollectionChanged -= Items_CollectionChanged;
                SetValue(ItemsProperty, value); 
                if (value != null)
                    Items.CollectionChanged += Items_CollectionChanged;
            }
        }

        // Using a DependencyProperty as the backing store for Items. This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items", typeof(ObservableCollection<ButtonBase>), typeof(RibbonButton));

        public int NotificationsCount
        {
            get { return (int)GetValue(NotificationsCountProperty); }
            set { SetValue(NotificationsCountProperty, value); }
        }

        public static readonly DependencyProperty NotificationsCountProperty =
            DependencyProperty.Register("NotificationsCount", typeof(int), typeof(RibbonButton), new PropertyMetadata(NotificationsCountChanged));

        private static void NotificationsCountChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            var rb = (RibbonButton)sender;
            rb.ShowNotificationsCount = (int)args.NewValue > 0;
        }

        public bool ShowNotificationsCount
        {
            get { return (bool)GetValue(ShowNotificationsCountProperty); }
            set { SetValue(ShowNotificationsCountProperty, value); }
        }

        public static readonly DependencyProperty ShowNotificationsCountProperty =
            DependencyProperty.Register("ShowNotificationsCount", typeof(bool), typeof(RibbonButton));
    }
}