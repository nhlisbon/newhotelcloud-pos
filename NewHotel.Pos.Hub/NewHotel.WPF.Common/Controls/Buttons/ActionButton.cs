﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Controls.Primitives;

namespace NewHotel.WPF.Common.Controls.Buttons
{
    /// <summary>
    /// Interaction logic for ActionButton.xaml
    /// </summary>
    public partial class ActionButton : ButtonBase
    {
        #region Variables + Enums

        private int _mouseDownCounter;

        #endregion
        #region Properties

        #region ImageHorizontalAlignment

        public HorizontalAlignment ImageHorizontalAlignment
        {
            get => (HorizontalAlignment)GetValue(ImageHorizontalAlignmentProperty);
            set => SetValue(ImageHorizontalAlignmentProperty, value);
        }

        public static readonly DependencyProperty ImageHorizontalAlignmentProperty =
            DependencyProperty.Register(nameof(ImageHorizontalAlignment), typeof(HorizontalAlignment), typeof(ActionButton), new PropertyMetadata(HorizontalAlignment.Stretch));

        #endregion
        #region ImageVerticalAlignment

        public VerticalAlignment ImageVerticalAlignment
        {
            get => (VerticalAlignment)GetValue(ImageVerticalAlignmentProperty);
            set => SetValue(ImageVerticalAlignmentProperty, value);
        }

        public static readonly DependencyProperty ImageVerticalAlignmentProperty =
            DependencyProperty.Register(nameof(ImageVerticalAlignment), typeof(VerticalAlignment), typeof(ActionButton), new PropertyMetadata(VerticalAlignment.Stretch));

        #endregion
        #region ShowText

        public bool ShowText
        {
            get { return (bool)GetValue(ShowTextProperty); }
            set { SetValue(ShowTextProperty, value); }
        }

        public static readonly DependencyProperty ShowTextProperty =
            DependencyProperty.Register(nameof(ShowText), typeof(bool), typeof(ActionButton));

        #endregion
        #region ShowImage

        public bool ShowImage
        {
            get { return (bool)GetValue(ShowImageProperty); }
            set { SetValue(ShowImageProperty, value); }
        }

        public static readonly DependencyProperty ShowImageProperty =
            DependencyProperty.Register(nameof(ShowImage), typeof(bool), typeof(ActionButton));

        #endregion
        #region ImageSource

        public ImageSource ImageSource
        {
            get { return (ImageSource)GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        }

        public static readonly DependencyProperty ImageSourceProperty =
            DependencyProperty.Register(nameof(ImageSource), typeof(ImageSource), typeof(ActionButton));

        #endregion
        #region Text

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register(nameof(Text), typeof(string), typeof(ActionButton));

		#endregion

		#region TextFontSize
		public double TextFontSize
		{
			get { return (double)GetValue(TextFontSizeProperty); }
			set { SetValue(TextFontSizeProperty, value); }
		}

        public static readonly DependencyProperty TextFontSizeProperty =
			DependencyProperty.Register(nameof(TextFontSize), typeof(double), typeof(ActionButton));
		#endregion

		#region Notifications

		public int NotificationsCount
        {
            get { return (int)GetValue(NotificationsCountProperty); }
            set { SetValue(NotificationsCountProperty, value); }
        }

        public static readonly DependencyProperty NotificationsCountProperty =
            DependencyProperty.Register(nameof(NotificationsCount), typeof(int), typeof(ActionButton), new PropertyMetadata(NotificationsCountChanged));

        private static void NotificationsCountChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            var ab = (ActionButton)sender;
            ab.ShowNotificationsCount = (int)args.NewValue > 0;
        }

        #endregion
        #region ShowNotificationsCount

        public bool ShowNotificationsCount
        {
            get { return (bool)GetValue(ShowNotificationsCountProperty); }
            set { SetValue(ShowNotificationsCountProperty, value); }
        }

        public static readonly DependencyProperty ShowNotificationsCountProperty =
            DependencyProperty.Register(nameof(ShowNotificationsCount), typeof(bool), typeof(ActionButton));

        #endregion
        #region TextWrapping

        public TextWrapping TextWrapping
        {
            get { return (TextWrapping)GetValue(TextWrappingProperty); }
            set { SetValue(TextWrappingProperty, value); }
        }

        public static readonly DependencyProperty TextWrappingProperty =
            DependencyProperty.Register(nameof(TextWrapping), typeof(TextWrapping), typeof(ActionButton));

        #endregion
        #region TextTrimming

        public TextTrimming TextTrimming
        {
            get { return (TextTrimming)GetValue(TextTrimmingProperty); }
            set { SetValue(TextTrimmingProperty, value); }
        }

        public static readonly DependencyProperty TextTrimmingProperty =
            DependencyProperty.Register(nameof(TextTrimming), typeof(TextTrimming), typeof(ActionButton),
                new PropertyMetadata(TextTrimming.CharacterEllipsis));

        #endregion
        #region DisableClick

        public bool DisableClick
        {
            get { return (bool)GetValue(DisableClickProperty); }
            set { SetValue(DisableClickProperty, value); }
        }

        public static readonly DependencyProperty DisableClickProperty =
            DependencyProperty.Register(nameof(DisableClick), typeof(bool), typeof(ActionButton),
                new PropertyMetadata(false));

        #endregion

        public new bool IsPressed
        {
            get { return base.IsPressed; }
            set { base.IsPressed = value; }
        }

        #endregion
        #region Methods

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            if (e.ClickCount >= 2)
                e.Handled = true;
            base.OnPreviewMouseDown(e);
        }

        protected override void OnClick()
        {
            _mouseDownCounter++;
            try
            {
                base.OnClick();
            }
            finally
            {
                _mouseDownCounter--;
            }
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (_mouseDownCounter > 0)
                e.Handled = true;
            else if (DisableClick)
                _mouseDownCounter++;
            base.OnMouseDown(e);
        }

        public void EnableClick()
        {
            _mouseDownCounter = 0;
        }

        #endregion
        #region Constructor

        public ActionButton()
        {
            InputBindings.Clear();
            InputBindings.Add(new MouseBinding() { Gesture = new MouseGesture(), MouseAction = MouseAction.LeftClick });
        }

        #endregion
    }
}