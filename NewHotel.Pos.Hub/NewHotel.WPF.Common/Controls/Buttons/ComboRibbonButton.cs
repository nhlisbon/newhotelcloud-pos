﻿using System.Windows;

namespace NewHotel.WPF.Common.Controls.Buttons
{
    /// <summary>
    /// Interaction logic for ComboRibbonButton.xaml
    /// </summary>
    public partial class ComboRibbonButton : RibbonButton
    {
        public ComboRibbonButton() : base() { }

        protected override void Item_Click(object sender, RoutedEventArgs e)
        {
            IsChecked = false;
        }
    }
}