﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace NewHotel.WPF.Common.Controls.TouchScreen
{
    /// <summary>
    /// Interaction logic for TouchKeyboard.xaml
    /// </summary>
    public partial class TouchScreenKeyboard : Window
    {
        #region Property

        private static double _WidthTouchKeyboard = 830;
        private static Dictionary<Key, RoutedUICommand> commandsByKey = new Dictionary<Key, RoutedUICommand>();
        private static Window? _InstanceObject;
        private static Brush _PreviousTextBoxBackgroundBrush = null;
        private static Brush _PreviousTextBoxBorderBrush = null;
        private static Thickness _PreviousTextBoxBorderThickness;
        private static Control _CurrentControl;
        private Point touchStart;
        private static bool _isDragging;
        private static bool _wasManuallyPositioned;
        private static object lastFocus = null;
        private bool useExternalRunCommand;

        #endregion

        #region Constructor

        public TouchScreenKeyboard()
        {
            InitializeComponent();
            //this.Width = WidthTouchKeyboard;
            
            TouchMove += Window_TouchMove;
            TouchUp += Window_TouchUp;
            MouseMove += Window_MouseMove;
            MouseUp += Window_MouseUp;
            SizeToContent = SizeToContent.WidthAndHeight;
        }

        static TouchScreenKeyboard()
        {
            //DefaultStyleKeyProperty.OverrideMetadata(typeof(TouchScreenKeyboard), new FrameworkPropertyMetadata(typeof(TouchScreenKeyboard)));

            SetCommandBinding();
        }

        #endregion

        #region Variables

        public bool IsNumeric
        {
            get { return (bool)GetValue(IsNumericProperty); }
            set
            {
                SetValue(IsNumericProperty, value);
                if (value)
                {
                    numpadContainer.Visibility = System.Windows.Visibility.Visible;
                    (Resources["showNumeric"] as Storyboard).Begin();
                    (Resources["hideKeyboard"] as Storyboard).Begin();
                }
                else
                {
                    keyboardContainer.Visibility = System.Windows.Visibility.Visible;
                    (Resources["hideNumeric"] as Storyboard).Begin();
                    (Resources["showKeyboard"] as Storyboard).Begin();
                }
            }
        }

        public static readonly DependencyProperty IsNumericProperty =
            DependencyProperty.Register("IsNumeric", typeof(bool), typeof(TouchScreenKeyboard));

        public static double WidthTouchKeyboard
        {
            get { return _WidthTouchKeyboard; }
            set { _WidthTouchKeyboard = value; }

        }

        public bool ShiftFlag
        {
            get { return (bool)GetValue(ShiftFlagProperty); }
            set { SetValue(ShiftFlagProperty, value); }
        }

        public static readonly DependencyProperty ShiftFlagProperty =
            DependencyProperty.Register("ShiftFlag", typeof(bool), typeof(TouchScreenKeyboard));

        public bool CapsLockFlag
        {
            get { return (bool)GetValue(CapsLockFlagProperty); }
            set { SetValue(CapsLockFlagProperty, value); }
        }

        public static readonly DependencyProperty CapsLockFlagProperty =
            DependencyProperty.Register("CapsLockFlag", typeof(bool), typeof(TouchScreenKeyboard));

        public static string TouchScreenText
        {
            get
            {
                switch (_CurrentControl)
                {
                    case TextBox textBox:
                        return textBox.Text;
                    case ComboBox comboBox:
                        return comboBox.Text;
                    case PasswordBox passwordBox:
                        return passwordBox.Password;
                    case DecimalEditControl decimalEditControl:
                        return decimalEditControl.EditValue.ToString();
                    default:
                        return "";
                }
            }
            set
            {
                switch (_CurrentControl)
                {
                    case TextBox textBox:
                        textBox.Text = value;
                        break;
                    case ComboBox comboBox:
                        comboBox.Text = value;
                        break;
                    case PasswordBox passwordBox:
                        passwordBox.Password = value;
                        break;
                    case DecimalEditControl decimalEditControl:
                        decimalEditControl.EditValue = value;
                        break;
                }
            }
        }

        #region Init UI Commands

        // NumPad
        // 1ra fila
        public static RoutedUICommand CmdExc = new RoutedUICommand();
        public static RoutedUICommand CmdDoubleInvertedComma = new RoutedUICommand();
        public static RoutedUICommand CmdNumber = new RoutedUICommand();

        public static RoutedUICommand CmdBackspace = new RoutedUICommand();
        public static RoutedUICommand CmdDiv = new RoutedUICommand();
        public static RoutedUICommand CmdMul = new RoutedUICommand();
        public static RoutedUICommand CmdMinus = new RoutedUICommand();

        // 2da fila
        public static RoutedUICommand CmdMoney = new RoutedUICommand();
        public static RoutedUICommand CmdPercent = new RoutedUICommand();
        public static RoutedUICommand CmdAmp = new RoutedUICommand();

        public static RoutedUICommand Cmd7 = new RoutedUICommand();
        public static RoutedUICommand Cmd8 = new RoutedUICommand();
        public static RoutedUICommand Cmd9 = new RoutedUICommand();
        public static RoutedUICommand CmdPlus = new RoutedUICommand();

        // 3ra fila
        public static RoutedUICommand CmdOpenPar = new RoutedUICommand();
        public static RoutedUICommand CmdClosePar = new RoutedUICommand();
        public static RoutedUICommand CmdTilde = new RoutedUICommand();

        public static RoutedUICommand Cmd4 = new RoutedUICommand();
        public static RoutedUICommand Cmd5 = new RoutedUICommand();
        public static RoutedUICommand Cmd6 = new RoutedUICommand();

        // 4ta fila
        public static RoutedUICommand CmdQuestion = new RoutedUICommand();
        public static RoutedUICommand CmdSemiColon = new RoutedUICommand();
        public static RoutedUICommand CmdColon = new RoutedUICommand();

        public static RoutedUICommand Cmd1 = new RoutedUICommand();
        public static RoutedUICommand Cmd2 = new RoutedUICommand();
        public static RoutedUICommand Cmd3 = new RoutedUICommand();
        public static RoutedUICommand CmdClear = new RoutedUICommand();

        // 5ta fila
        public static RoutedUICommand CmdEmail = new RoutedUICommand();
        public static RoutedUICommand Cmd00 = new RoutedUICommand();
        public static RoutedUICommand Cmd0 = new RoutedUICommand();
        public static RoutedUICommand CmdDot = new RoutedUICommand();

        // KeyBoard
        // 1ra fila
        public static RoutedUICommand CmdQ = new RoutedUICommand();
        public static RoutedUICommand Cmdw = new RoutedUICommand();
        public static RoutedUICommand CmdE = new RoutedUICommand();
        public static RoutedUICommand CmdR = new RoutedUICommand();
        public static RoutedUICommand CmdT = new RoutedUICommand();
        public static RoutedUICommand CmdY = new RoutedUICommand();
        public static RoutedUICommand CmdU = new RoutedUICommand();
        public static RoutedUICommand CmdI = new RoutedUICommand();
        public static RoutedUICommand CmdO = new RoutedUICommand();
        public static RoutedUICommand CmdP = new RoutedUICommand();

        // 2da fila
        public static RoutedUICommand CmdCaps = new RoutedUICommand();
        public static RoutedUICommand CmdA = new RoutedUICommand();
        public static RoutedUICommand CmdS = new RoutedUICommand();
        public static RoutedUICommand CmdD = new RoutedUICommand();
        public static RoutedUICommand CmdF = new RoutedUICommand();
        public static RoutedUICommand CmdG = new RoutedUICommand();
        public static RoutedUICommand CmdH = new RoutedUICommand();
        public static RoutedUICommand CmdJ = new RoutedUICommand();
        public static RoutedUICommand CmdK = new RoutedUICommand();
        public static RoutedUICommand CmdL = new RoutedUICommand();
        public static RoutedUICommand CmdEnter = new RoutedUICommand();

        // 3ra fila
        public static RoutedUICommand CmdShift = new RoutedUICommand();
        public static RoutedUICommand CmdZ = new RoutedUICommand();
        public static RoutedUICommand CmdX = new RoutedUICommand();
        public static RoutedUICommand CmdC = new RoutedUICommand();
        public static RoutedUICommand CmdV = new RoutedUICommand();
        public static RoutedUICommand CmdB = new RoutedUICommand();
        public static RoutedUICommand CmdN = new RoutedUICommand();
        public static RoutedUICommand CmdM = new RoutedUICommand();
        public static RoutedUICommand CmdComma = new RoutedUICommand();

        // 4ta fila
        public static RoutedUICommand CmdSpaceBar = new RoutedUICommand();

        #endregion

        #endregion

        #region CommandRelatedCode
        private static void SetCommandBinding()
        {
            #region LinkKeyWithCommand
            // NUM PAD
            // 1ra fila
            commandsByKey.Add(Key.Back, CmdBackspace);
            commandsByKey.Add(Key.Divide, CmdDiv);
            commandsByKey.Add(Key.Multiply, CmdMul);
            commandsByKey.Add(Key.Subtract, CmdMinus);

            // 2da fila
            commandsByKey.Add(Key.D7, Cmd7); commandsByKey.Add(Key.NumPad7, Cmd7);
            commandsByKey.Add(Key.D8, Cmd8); commandsByKey.Add(Key.NumPad8, Cmd8);
            commandsByKey.Add(Key.D9, Cmd9); commandsByKey.Add(Key.NumPad9, Cmd9);

            // 3ra fila
            commandsByKey.Add(Key.D4, Cmd4); commandsByKey.Add(Key.NumPad4, Cmd4);
            commandsByKey.Add(Key.D5, Cmd5); commandsByKey.Add(Key.NumPad5, Cmd5);
            commandsByKey.Add(Key.D6, Cmd6); commandsByKey.Add(Key.NumPad6, Cmd6);

            // 4ta fila
            commandsByKey.Add(Key.D1, Cmd1); commandsByKey.Add(Key.NumPad1, Cmd1);
            commandsByKey.Add(Key.D2, Cmd2); commandsByKey.Add(Key.NumPad2, Cmd2);
            commandsByKey.Add(Key.D3, Cmd3); commandsByKey.Add(Key.NumPad3, Cmd3);

            // 5ta fila
            commandsByKey.Add(Key.D0, Cmd0); commandsByKey.Add(Key.NumPad0, Cmd0);
            commandsByKey.Add(Key.OemPeriod, CmdDot);

            // KeyBoard
            // 1ra fila
            commandsByKey.Add(Key.Q, CmdQ);
            commandsByKey.Add(Key.W, Cmdw);
            commandsByKey.Add(Key.E, CmdE);
            commandsByKey.Add(Key.R, CmdR);
            commandsByKey.Add(Key.T, CmdT);
            commandsByKey.Add(Key.Y, CmdY);
            commandsByKey.Add(Key.U, CmdU);
            commandsByKey.Add(Key.I, CmdI);
            commandsByKey.Add(Key.O, CmdO);
            commandsByKey.Add(Key.P, CmdP);

            // 2da fila
            commandsByKey.Add(Key.A, CmdA);
            commandsByKey.Add(Key.S, CmdS);
            commandsByKey.Add(Key.D, CmdD);
            commandsByKey.Add(Key.F, CmdF);
            commandsByKey.Add(Key.G, CmdG);
            commandsByKey.Add(Key.H, CmdH);
            commandsByKey.Add(Key.J, CmdJ);
            commandsByKey.Add(Key.K, CmdK);
            commandsByKey.Add(Key.L, CmdL);

            // 3ra fila
            commandsByKey.Add(Key.Z, CmdZ);
            commandsByKey.Add(Key.X, CmdX);
            commandsByKey.Add(Key.C, CmdC);
            commandsByKey.Add(Key.V, CmdV);
            commandsByKey.Add(Key.B, CmdB);
            commandsByKey.Add(Key.N, CmdN);
            commandsByKey.Add(Key.M, CmdM);
            commandsByKey.Add(Key.OemComma, CmdComma);

            // 4ta fila
            commandsByKey.Add(Key.Space, CmdSpaceBar);
            #endregion

            #region CommandBindings

            // NumPad
            // 1ra fila
            CommandBinding CbExc = new CommandBinding(CmdExc, RunCommand);
            CommandBinding CbDoubleInvertedComma = new CommandBinding(CmdDoubleInvertedComma, RunCommand);
            CommandBinding CbNumber = new CommandBinding(CmdNumber, RunCommand);

            CommandBinding CbBackspace = new CommandBinding(CmdBackspace, RunCommand);
            CommandBinding CbDiv = new CommandBinding(CmdDiv, RunCommand);
            CommandBinding CbMul = new CommandBinding(CmdMul, RunCommand);
            CommandBinding CbMinus = new CommandBinding(CmdMinus, RunCommand);

            // 2da fila
            CommandBinding CbMoney = new CommandBinding(CmdMoney, RunCommand);
            CommandBinding CbPercent = new CommandBinding(CmdPercent, RunCommand);
            CommandBinding CbAmp = new CommandBinding(CmdAmp, RunCommand);

            CommandBinding Cb7 = new CommandBinding(Cmd7, RunCommand);
            CommandBinding Cb8 = new CommandBinding(Cmd8, RunCommand);
            CommandBinding Cb9 = new CommandBinding(Cmd9, RunCommand);
            CommandBinding CbPlus = new CommandBinding(CmdPlus, RunCommand);

            // 3ra fila
            CommandBinding CbOpenPar = new CommandBinding(CmdOpenPar, RunCommand);
            CommandBinding CbClosePar = new CommandBinding(CmdClosePar, RunCommand);
            CommandBinding CbTilde = new CommandBinding(CmdTilde, RunCommand);

            CommandBinding Cb4 = new CommandBinding(Cmd4, RunCommand);
            CommandBinding Cb5 = new CommandBinding(Cmd5, RunCommand);
            CommandBinding Cb6 = new CommandBinding(Cmd6, RunCommand);

            // 4ta fila
            CommandBinding CbQuestion = new CommandBinding(CmdQuestion, RunCommand);
            CommandBinding CbSemiColon = new CommandBinding(CmdSemiColon, RunCommand);
            CommandBinding CbColon = new CommandBinding(CmdColon, RunCommand);

            CommandBinding Cb1 = new CommandBinding(Cmd1, RunCommand);
            CommandBinding Cb2 = new CommandBinding(Cmd2, RunCommand);
            CommandBinding Cb3 = new CommandBinding(Cmd3, RunCommand);
            CommandBinding CbClear = new CommandBinding(CmdClear, RunCommand);

            // 5ta fila
            CommandBinding CbEmail = new CommandBinding(CmdEmail, RunCommand);
            CommandBinding Cb00 = new CommandBinding(Cmd00, RunCommand);
            CommandBinding Cb0 = new CommandBinding(Cmd0, RunCommand);
            CommandBinding CbDot = new CommandBinding(CmdDot, RunCommand);



            // KeyBoard
            // 1ra fila
            CommandBinding CbQ = new CommandBinding(CmdQ, RunCommand);
            CommandBinding Cbw = new CommandBinding(Cmdw, RunCommand);
            CommandBinding CbE = new CommandBinding(CmdE, RunCommand);
            CommandBinding CbR = new CommandBinding(CmdR, RunCommand);
            CommandBinding CbT = new CommandBinding(CmdT, RunCommand);
            CommandBinding CbY = new CommandBinding(CmdY, RunCommand);
            CommandBinding CbU = new CommandBinding(CmdU, RunCommand);
            CommandBinding CbI = new CommandBinding(CmdI, RunCommand);
            CommandBinding CbO = new CommandBinding(CmdO, RunCommand);
            CommandBinding CbP = new CommandBinding(CmdP, RunCommand);

            // 2da fila
            CommandBinding CbCaps = new CommandBinding(CmdCaps, RunCommand);
            CommandBinding CbA = new CommandBinding(CmdA, RunCommand);
            CommandBinding CbS = new CommandBinding(CmdS, RunCommand);
            CommandBinding CbD = new CommandBinding(CmdD, RunCommand);
            CommandBinding CbF = new CommandBinding(CmdF, RunCommand);
            CommandBinding CbG = new CommandBinding(CmdG, RunCommand);
            CommandBinding CbH = new CommandBinding(CmdH, RunCommand);
            CommandBinding CbJ = new CommandBinding(CmdJ, RunCommand);
            CommandBinding CbK = new CommandBinding(CmdK, RunCommand);
            CommandBinding CbL = new CommandBinding(CmdL, RunCommand);
            CommandBinding CbEnter = new CommandBinding(CmdEnter, RunCommand);

            // 3ra fila
            CommandBinding CbShift = new CommandBinding(CmdShift, RunCommand);
            CommandBinding CbZ = new CommandBinding(CmdZ, RunCommand);
            CommandBinding CbX = new CommandBinding(CmdX, RunCommand);
            CommandBinding CbC = new CommandBinding(CmdC, RunCommand);
            CommandBinding CbV = new CommandBinding(CmdV, RunCommand);
            CommandBinding CbB = new CommandBinding(CmdB, RunCommand);
            CommandBinding CbN = new CommandBinding(CmdN, RunCommand);
            CommandBinding CbM = new CommandBinding(CmdM, RunCommand);
            CommandBinding CbComma = new CommandBinding(CmdComma, RunCommand);

            // 4ta fila
            CommandBinding CbSpaceBar = new CommandBinding(CmdSpaceBar, RunCommand);

            #endregion

            #region CommandManagers

            // NumPad
            // 1ra fila
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbExc);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbDoubleInvertedComma);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbNumber);

            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbBackspace);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbDiv);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbMul);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbMinus);

            // 2da fila
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbMoney);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbPercent);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbAmp);

            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), Cb7);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), Cb8);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), Cb9);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbPlus);

            // 3ra fila
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbOpenPar);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbClosePar);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbTilde);

            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), Cb4);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), Cb5);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), Cb6);

            // 4ta fila
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbQuestion);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbSemiColon);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbColon);

            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), Cb1);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), Cb2);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), Cb3);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbClear);

            // 5ta fila
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbEmail);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), Cb00);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), Cb0);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbDot);



            // KeyBoard
            // 1ra fila
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbQ);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), Cbw);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbE);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbR);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbT);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbY);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbU);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbI);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbO);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbP);

            // 2da fila
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbCaps);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbA);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbS);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbD);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbF);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbG);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbH);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbJ);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbK);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbL);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbEnter);

            // 3ra fila
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbShift);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbZ);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbX);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbC);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbV);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbB);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbN);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbM);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbComma);

            // 4ta fila
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenKeyboard), CbSpaceBar);

            #endregion


        }

        static void MyRunCommand(object sender, ICommand command)
        {
            var keyboard = sender as TouchScreenKeyboard;
            if (keyboard.resetNext && command != CmdEnter)
            {
                TouchScreenKeyboard.TouchScreenText = "";
                keyboard.resetNext = false;
            }
            if (command == CmdExc)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "!";
            }
            else if (command == CmdDoubleInvertedComma)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "\"";
            }
            else if (command == CmdNumber)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "#";
            }
            else if (command == CmdBackspace)
            {
                if (!string.IsNullOrEmpty(TouchScreenKeyboard.TouchScreenText))
                {
                    TouchScreenKeyboard.TouchScreenText = TouchScreenKeyboard.TouchScreenText.Substring(0, TouchScreenKeyboard.TouchScreenText.Length - 1);
                }
            }
            else if (command == CmdDiv)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "/";
            }
            else if (command == CmdMul)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "*";
            }
            else if (command == CmdMinus)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "-";
            }
            else if (command == CmdMoney)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "$";
            }
            else if (command == CmdPercent)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "%";
            }
            else if (command == CmdAmp)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "&";
            }
            else if (command == Cmd7)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "7";
            }
            else if (command == Cmd8)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "8";
            }
            else if (command == Cmd9)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "9";
            }
            else if (command == CmdPlus)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "+";
            }
            else if (command == CmdOpenPar)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "(";
            }
            else if (command == CmdClosePar)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += ")";
            }
            else if (command == CmdTilde)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "'";
            }
            else if (command == Cmd4)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "4";
            }
            else if (command == Cmd5)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "5";
            }
            else if (command == Cmd6)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "6";
            }
            else if (command == CmdQuestion)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "?";
            }
            else if (command == CmdSemiColon)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += ";";
            }
            else if (command == CmdColon)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += ":";
            }
            else if (command == Cmd1)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "1";
            }
            else if (command == Cmd2)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "2";
            }
            else if (command == Cmd3)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "3";
            }
            else if (command == CmdClear)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText = "";
            }
            else if (command == CmdEmail)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "@";
            }
            else if (command == Cmd00)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "00";
            }
            else if (command == Cmd0)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += "0";
            }
            else if (command == CmdDot)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += Thread.CurrentThread.CurrentUICulture.NumberFormat.CurrencyDecimalSeparator; ;
            }
            else if (command == CmdQ)
            {
                AddKeyBoardINput('Q');
            }
            else if (command == Cmdw)
            {
                AddKeyBoardINput('W');
            }
            else if (command == CmdE)
            {
                AddKeyBoardINput('E');
            }
            else if (command == CmdR)
            {
                AddKeyBoardINput('R');
            }
            else if (command == CmdT)
            {
                AddKeyBoardINput('T');
            }
            else if (command == CmdY)
            {
                AddKeyBoardINput('Y');
            }
            else if (command == CmdU)
            {
                AddKeyBoardINput('U');
            }
            else if (command == CmdI)
            {
                AddKeyBoardINput('I');
            }
            else if (command == CmdO)
            {
                AddKeyBoardINput('O');
            }
            else if (command == CmdP)
            {
                AddKeyBoardINput('P');
            }
            else if (command == CmdCaps)
            {
                //(_InstanceObject as TouchScreenKeyboard).CapsLockFlag = !(_InstanceObject as TouchScreenKeyboard).CapsLockFlag;
            }
            else if (command == CmdA)
            {
                AddKeyBoardINput('A');
            }
            else if (command == CmdS)
            {
                AddKeyBoardINput('S');
            }
            else if (command == CmdD)
            {
                AddKeyBoardINput('D');
            }
            else if (command == CmdF)
            {
                AddKeyBoardINput('F');
            }
            else if (command == CmdG)
            {
                AddKeyBoardINput('G');
            }
            else if (command == CmdH)
            {
                AddKeyBoardINput('H');
            }
            else if (command == CmdJ)
            {
                AddKeyBoardINput('J');
            }
            else if (command == CmdK)
            {
                AddKeyBoardINput('K');
            }
            else if (command == CmdL)
            {
                AddKeyBoardINput('L');
            }
            else if (command == CmdEnter)
            {
                if (_CurrentControl is TextBox && (_CurrentControl as TextBox).AcceptsReturn)
                {
                    if (IsAllowedToAddChar)
                        TouchScreenKeyboard.TouchScreenText += Environment.NewLine;
                }
                else
                {
                    if (_InstanceObject != null)
                    {
                        if ((_InstanceObject as TouchScreenKeyboard).useExternalRunCommand)
                        {
                            DependencyObject parent = _CurrentControl;
                            while (parent != null)
                            {
                                if (parent is ITSKControlsKey)
                                {
                                    (parent as ITSKControlsKey).RunCommandEnter(_InstanceObject, _CurrentControl);
                                    break;
                                }
                                parent = VisualTreeHelper.GetParent(parent);
                            }
                        }
                        _InstanceObject.Close();
                        _InstanceObject = null;
                    }
                    _CurrentControl.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                    if (_CurrentControl is ComboBox)
                        _CurrentControl.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
                }
            }
            else if (command == CmdShift)
            {
                //(_InstanceObject as TouchScreenKeyboard).ShiftFlag = !(_InstanceObject as TouchScreenKeyboard).ShiftFlag;
            }
            else if (command == CmdZ)
            {
                AddKeyBoardINput('Z');
            }
            else if (command == CmdX)
            {
                AddKeyBoardINput('X');
            }
            else if (command == CmdC)
            {
                AddKeyBoardINput('C');
            }
            else if (command == CmdV)
            {
                AddKeyBoardINput('V');
            }
            else if (command == CmdB)
            {
                AddKeyBoardINput('B');
            }
            else if (command == CmdN)
            {
                AddKeyBoardINput('N');
            }
            else if (command == CmdM)
            {
                AddKeyBoardINput('M');
            }
            else if (command == CmdComma)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += ",";
            }
            else if (command == CmdSpaceBar)
            {
                if (IsAllowedToAddChar)
                    TouchScreenKeyboard.TouchScreenText += " ";
            }
        }

        static void RunCommand(object sender, ExecutedRoutedEventArgs e)
        {
            MyRunCommand(sender, e.Command);
        }

        #endregion

        #region Main Functionality
        private static void AddKeyBoardINput(char input)
        {
            if (!IsAllowedToAddChar) return;
            if ((((_InstanceObject as TouchScreenKeyboard).CapsLockFlag ? 1 : 0) + ((_InstanceObject as TouchScreenKeyboard).ShiftFlag ? 1 : 0)) % 2 == 0)
                TouchScreenKeyboard.TouchScreenText += char.ToLower(input).ToString();
            else TouchScreenKeyboard.TouchScreenText += char.ToUpper(input).ToString();
            (_InstanceObject as TouchScreenKeyboard).ShiftFlag = false;
        }

        private static bool IsAllowedToAddChar
        {
            get
            {
                return !(_CurrentControl is TextBox) ||
                    (_CurrentControl as TextBox).MaxLength <= 0 ||
                    (_CurrentControl as TextBox).MaxLength > TouchScreenKeyboard.TouchScreenText.Length;
            }
        }

        private static void SyncChild()
        {
            if (_CurrentControl == null || _InstanceObject == null || _isDragging || _wasManuallyPositioned) 
                return;

            var virtualpoint = new Point(0, _CurrentControl.ActualHeight + 3);
            var Actualpoint = _CurrentControl.PointToScreen(virtualpoint);

            if (_InstanceObject.Width + Actualpoint.X > SystemParameters.VirtualScreenWidth)
            {
                var difference = _InstanceObject.Width + Actualpoint.X - SystemParameters.VirtualScreenWidth;
                _InstanceObject.Left = Actualpoint.X - difference;
            }
            else if (!(Actualpoint.X > 1))
            {
                _InstanceObject.Left = 1;
            }
            else
                _InstanceObject.Left = Actualpoint.X;


            if (_InstanceObject.Height + Actualpoint.Y > SystemParameters.VirtualScreenHeight)
            {
                virtualpoint = new Point(0, -_InstanceObject.Height - 3);
                Actualpoint = _CurrentControl.PointToScreen(virtualpoint);
                _InstanceObject.Top = Actualpoint.Y;
            }
            else if (!(Actualpoint.Y > 1))
            {
                _InstanceObject.Left = 1;
            }
            else
                _InstanceObject.Top = Actualpoint.Y;
        }

        public static bool GetTouchScreenKeyboard(DependencyObject obj)
        {
            return (bool)obj.GetValue(TouchScreenKeyboardProperty);
        }

        public static void SetTouchScreenKeyboard(DependencyObject obj, bool value)
        {
            obj.SetValue(TouchScreenKeyboardProperty, value);
        }

        public static readonly DependencyProperty TouchScreenKeyboardProperty =
            DependencyProperty.RegisterAttached("TouchScreenKeyboard", typeof(bool), typeof(TouchScreenKeyboard), new UIPropertyMetadata(default(bool), TouchScreenKeyboardPropertyChanged));
        private bool resetNext;


        static void TouchScreenKeyboardPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement host = sender as FrameworkElement;
            if (host != null)
            {
                host.GotKeyboardFocus += new KeyboardFocusChangedEventHandler(OnGotFocus);
                host.LostFocus += new RoutedEventHandler(OnLostFocus);
            }
        }

        static void BuildKeyboard(Control host)
        {
            FrameworkElement ct = host;
            while (ct != null)
            {
                if (ct is Window window)
                {
                    window.LocationChanged += new EventHandler(TouchScreenKeyboard_LocationChanged);
                    window.Activated += new EventHandler(TouchScreenKeyboard_Activated);
                    window.Deactivated += new EventHandler(TouchScreenKeyboard_Deactivated);
                    break;
                }
                ct = (FrameworkElement)ct.Parent;
            }

            _InstanceObject = new TouchScreenKeyboard();
            _InstanceObject.AllowsTransparency = true;
            _InstanceObject.Background = Brushes.Transparent;
            _InstanceObject.WindowStyle = WindowStyle.None;
            _InstanceObject.ShowInTaskbar = false;
            _InstanceObject.Topmost = true;

            host.LayoutUpdated += new EventHandler(tb_LayoutUpdated);
            _InstanceObject.Show();

            if (Keyboard.IsKeyToggled(Key.CapsLock))
                (_InstanceObject as TouchScreenKeyboard).CapsLockFlag = true;
        }

        #endregion

        #region Events

        static void OnGotFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            var host = sender as Control;
            _PreviousTextBoxBackgroundBrush = host.Background;
            _PreviousTextBoxBorderBrush = host.BorderBrush;
            _PreviousTextBoxBorderThickness = host.BorderThickness;

            // host.Background = Brushes.Yellow;
            host.BorderBrush = Brushes.Black;
            host.BorderThickness = new Thickness(2);

            if (host.Equals(lastFocus))
                if (_InstanceObject != null)
                    return;
                else
                    BuildKeyboard(host);    

            lastFocus = host;

            _CurrentControl = host;
            if (_CurrentControl is ComboBox)
                (_CurrentControl as ComboBox).DropDownOpened += TouchScreenKeyboard_DropDownOpened;
            
            if (_InstanceObject == null)
                BuildKeyboard(host);

            //F111102:ped046:02: Acceder a la informacion de teclado flotante en el Tag del control
            if (host.Tag is not TouchScreenKeyboardInfo info) 
                return;

            (_InstanceObject as TouchScreenKeyboard).IsNumeric = info.IsNumeric;
            (_InstanceObject as TouchScreenKeyboard).resetNext = info.ResetFirst;
            (_InstanceObject as TouchScreenKeyboard).useExternalRunCommand = info.UseExternalRunCommand;
        }

        static void TouchScreenKeyboard_DropDownOpened(object sender, EventArgs e)
        {
            if (_InstanceObject != null)
            {
                (_InstanceObject as TouchScreenKeyboard).hide_Click(null, null);
            }
        }

        static void TouchScreenKeyboard_Deactivated(object sender, EventArgs e)
        {
            if (_InstanceObject != null)
            {
                _InstanceObject.Topmost = false;
            }
        }

        static void TouchScreenKeyboard_Activated(object sender, EventArgs e)
        {
            if (_InstanceObject != null)
            {
                _InstanceObject.Topmost = true;
            }
        }

        static void TouchScreenKeyboard_LocationChanged(object sender, EventArgs e)
        {
            SyncChild();
        }

        static void tb_LayoutUpdated(object sender, EventArgs e)
        {
            SyncChild();
        }

        static void OnLostFocus(object sender, RoutedEventArgs e)
        {
            lastFocus = null;
            _isDragging = false;
            Control host = sender as Control;
            host.Background = _PreviousTextBoxBackgroundBrush;
            host.BorderBrush = _PreviousTextBoxBorderBrush;
            host.BorderThickness = _PreviousTextBoxBorderThickness;

            if (sender is ComboBox)
            {// Es un combobox
                (sender as ComboBox).DropDownOpened -= TouchScreenKeyboard_DropDownOpened;
            }// Es un combobox

            if (_InstanceObject != null)
            {
                _InstanceObject.Close();
                _InstanceObject = null;
            }

        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.MouseDevice.Capture(this);
            touchStart = e.GetPosition(this);
            _isDragging = false;
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_isDragging)
            {
                var mouseCurrent = e.GetPosition(this);
                if (Math.Abs(mouseCurrent.X - touchStart.X) >= SystemParameters.MinimumHorizontalDragDistance ||
                    Math.Abs(mouseCurrent.Y - touchStart.Y) >= SystemParameters.MinimumVerticalDragDistance
                   )
                    _isDragging = true;
            }

            if (_isDragging && e.LeftButton == MouseButtonState.Pressed)
                DragMove();
        }

        private void Window_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ReleaseMouseCapture();
            _isDragging = false;
            _wasManuallyPositioned = true;
        }

        private void Window_TouchDown(object sender, TouchEventArgs e)
        {
            e.TouchDevice.Capture(this);
            touchStart = e.GetTouchPoint(this).Position;
            _isDragging = false;
        }

        private void Window_TouchMove(object sender, TouchEventArgs e)
        {
            if (!_isDragging)
            {
                var touchCurrent = e.GetTouchPoint(this).Position;
                if (Math.Abs(touchCurrent.X - touchStart.X) >= SystemParameters.MinimumHorizontalDragDistance ||
                    Math.Abs(touchCurrent.Y - touchStart.Y) >= SystemParameters.MinimumVerticalDragDistance
                )
                    _isDragging = true;
            }

            if (_isDragging)
                DragMove();
        }

        private void Window_TouchUp(object sender, TouchEventArgs e)
        {
            ReleaseTouchCapture(e.TouchDevice);
            _isDragging = false;
            _wasManuallyPositioned = true;
        }

        private void showNumeric_Click(object sender, RoutedEventArgs e)
        {
            IsNumeric = true;
        }

        private void hideNumeric_Click(object sender, RoutedEventArgs e)
        {
            IsNumeric = false;
        }

        private void hide_Click(object sender, RoutedEventArgs e)
        {
            if (_InstanceObject == null)
                return;

            _isDragging = false;
            _wasManuallyPositioned = false;
            _InstanceObject.Close();
            _InstanceObject = null;
            
        }

        private void hideNumeric_Completed(object sender, EventArgs e)
        {
            numpadContainer.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void hideKeyboard_Completed(object sender, EventArgs e)
        {
            keyboardContainer.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void userControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (IsNumeric)
            {
                numpadContainer.Visibility = System.Windows.Visibility.Visible;
                (Resources["showNumeric"] as Storyboard).Begin();
            }
            else
            {
                keyboardContainer.Visibility = System.Windows.Visibility.Visible;
                (Resources["showKeyboard"] as Storyboard).Begin();
            }
        }

        private void userControl_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    {
                        hide_Click(_InstanceObject, null);
                        e.Handled = true;
                        break;
                    }
                case Key.Enter:
                    {
                        MyRunCommand(_InstanceObject, CmdEnter);
                        break;
                    }
                case Key.CapsLock:
                    {
                        (_InstanceObject as TouchScreenKeyboard).CapsLockFlag = !(_InstanceObject as TouchScreenKeyboard).CapsLockFlag;
                        break;
                    }
                case Key.LeftShift:
                case Key.RightShift:
                    {
                        (_InstanceObject as TouchScreenKeyboard).ShiftFlag = !(_InstanceObject as TouchScreenKeyboard).ShiftFlag;
                        break;
                    }
                default:
                    {
                        if (commandsByKey.ContainsKey(e.Key))
                            MyRunCommand(_InstanceObject, commandsByKey[e.Key]);
                        break;
                    }
            }
        }
        
        #endregion
    }

    //F111102:ped046:01:Clase que permite almacenar la informacion necesaria para personalizar el teclado flotante
    public class TouchScreenKeyboardInfo
    {

        public TouchScreenKeyboardInfo()
        {
            ResetFirst = true;
        }

        public bool IsNumeric { get; set; }
        public bool ResetFirst { get; set; }
        public bool UseExternalRunCommand { get; set; }
    }
}
