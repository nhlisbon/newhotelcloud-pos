﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace NewHotel.WPF.Common.Controls.TouchScreen
{
    public class TouchScreenNumPad : Window
    {
        #region Variables

        public static RoutedUICommand Cmd00 = new RoutedUICommand();
        public static RoutedUICommand Cmd1 = new RoutedUICommand();
        public static RoutedUICommand Cmd2 = new RoutedUICommand();
        public static RoutedUICommand Cmd3 = new RoutedUICommand();
        public static RoutedUICommand Cmd4 = new RoutedUICommand();
        public static RoutedUICommand Cmd5 = new RoutedUICommand();
        public static RoutedUICommand Cmd6 = new RoutedUICommand();
        public static RoutedUICommand Cmd7 = new RoutedUICommand();
        public static RoutedUICommand Cmd8 = new RoutedUICommand();
        public static RoutedUICommand Cmd9 = new RoutedUICommand();
        public static RoutedUICommand Cmd0 = new RoutedUICommand();
        public static RoutedUICommand CmdMinus = new RoutedUICommand();
        public static RoutedUICommand CmdPlus = new RoutedUICommand();
        public static RoutedUICommand CmdPLU = new RoutedUICommand();
        public static RoutedUICommand CmdBackspace = new RoutedUICommand();
        public static RoutedUICommand CmdDot = new RoutedUICommand();
        public static RoutedUICommand CmdClear = new RoutedUICommand();

        private static Window _instanceObject;
        private static Brush _previousTextBoxBackgroundBrush;
        private static Brush _previousTextBoxBorderBrush;
        private static Thickness _previousTextBoxBorderThickness;
        private static Control _currentControl;

        #endregion
        #region Properties

        public static string TouchScreenText
        {
            get
            {
                if (_currentControl is TextBox box)
                    return box.Text;
                if (_currentControl is PasswordBox passwordBox)
                    return passwordBox.Password;
                return "";
            }
            set
            {
                if (_currentControl is TextBox box)
                    box.Text = value;
                else if (_currentControl is PasswordBox passwordBox)
                    passwordBox.Password = value;
            }
        }

        #endregion
        #region Constructors

        public TouchScreenNumPad()
        {
        }

        static TouchScreenNumPad()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TouchScreenNumPad), new FrameworkPropertyMetadata(typeof(TouchScreenNumPad)));

            SetCommandBinding();
        }

        #endregion
        #region Methods

        #region CommandRelatedCode

        private static void SetCommandBinding()
        {
            CommandBinding Cb00 = new CommandBinding(Cmd00, RunCommand);
            CommandBinding Cb1 = new CommandBinding(Cmd1, RunCommand);
            CommandBinding Cb2 = new CommandBinding(Cmd2, RunCommand);
            CommandBinding Cb3 = new CommandBinding(Cmd3, RunCommand);
            CommandBinding Cb4 = new CommandBinding(Cmd4, RunCommand);
            CommandBinding Cb5 = new CommandBinding(Cmd5, RunCommand);
            CommandBinding Cb6 = new CommandBinding(Cmd6, RunCommand);
            CommandBinding Cb7 = new CommandBinding(Cmd7, RunCommand);
            CommandBinding Cb8 = new CommandBinding(Cmd8, RunCommand);
            CommandBinding Cb9 = new CommandBinding(Cmd9, RunCommand);
            CommandBinding Cb0 = new CommandBinding(Cmd0, RunCommand);
            CommandBinding CbMinus = new CommandBinding(CmdMinus, RunCommand);
            CommandBinding CbPlus = new CommandBinding(CmdPlus, RunCommand);
            CommandBinding CbPLU = new CommandBinding(CmdPLU, RunCommand);
            CommandBinding CbDot = new CommandBinding(CmdDot, RunCommand);
            CommandBinding CbBackspace = new CommandBinding(CmdBackspace, RunCommand);
            CommandBinding CbClear = new CommandBinding(CmdClear, RunCommand);

            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenNumPad), Cb00);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenNumPad), Cb1);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenNumPad), Cb2);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenNumPad), Cb3);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenNumPad), Cb4);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenNumPad), Cb5);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenNumPad), Cb6);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenNumPad), Cb7);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenNumPad), Cb8);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenNumPad), Cb9);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenNumPad), Cb0);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenNumPad), CbMinus);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenNumPad), CbPlus);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenNumPad), CbPLU);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenNumPad), CbDot);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenNumPad), CbBackspace);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenNumPad), CbClear);

        }
        private static void RunCommand(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Command == Cmd00)  //First Row
            {
                TouchScreenText += "00";
            }
            else if (e.Command == Cmd1)
            {
                TouchScreenText += "1";
            }
            else if (e.Command == Cmd2)
            {
                TouchScreenText += "2";
            }
            else if (e.Command == Cmd3)
            {
                TouchScreenText += "3";
            }
            else if (e.Command == Cmd4)
            {
                TouchScreenText += "4";
            }
            else if (e.Command == Cmd5)
            {
                TouchScreenText += "5";
            }
            else if (e.Command == Cmd6)
            {
                TouchScreenText += "6";
            }
            else if (e.Command == Cmd7)
            {
                TouchScreenText += "7";
            }
            else if (e.Command == Cmd8)
            {
                TouchScreenText += "8";
            }
            else if (e.Command == Cmd9)
            {
                TouchScreenText += "9";
            }
            else if (e.Command == Cmd0)
            {
                TouchScreenText += "0";
            }
            else if (e.Command == CmdMinus)
            {
                TouchScreenText += "-";
            }
            else if (e.Command == CmdPlus)
            {
                TouchScreenText += "+";
            }
            else if (e.Command == CmdPLU)
            {
                TouchScreenText += " PLU ";
            }
            else if (e.Command == CmdDot)
            {
                TouchScreenText += Thread.CurrentThread.CurrentUICulture.NumberFormat.CurrencyDecimalSeparator;
            }
            else if (e.Command == CmdBackspace)
            {
                if (!string.IsNullOrEmpty(TouchScreenText))
                {
                    TouchScreenText = TouchScreenText.Substring(0, TouchScreenText.Length - 1);
                }
            }
            else if (e.Command == CmdClear)//Last row
            {
                TouchScreenText = "";
            }
        }

        #endregion
        #region Main Functionality

        public static bool GetTouchScreenNumPad(DependencyObject obj)
        {
            return (bool)obj.GetValue(TouchScreenNumPadProperty);
        }

        public static void SetTouchScreenNumPad(DependencyObject obj, bool value)
        {
            obj.SetValue(TouchScreenNumPadProperty, value);
        }

        public static readonly DependencyProperty TouchScreenNumPadProperty =
            DependencyProperty.RegisterAttached(nameof(TouchScreenNumPad), typeof(bool), typeof(TouchScreenNumPad), new UIPropertyMetadata(default(bool), TouchScreenNumPadPropertyChanged));

        private static void Syncchild()
        {
            if (_currentControl != null && _instanceObject != null)
            {
                Point virtualpoint = new Point(0, _currentControl.ActualHeight + 3);
                Point actualpoint = _currentControl.PointToScreen(virtualpoint);

                if (_instanceObject.ActualWidth + actualpoint.X > SystemParameters.VirtualScreenWidth)
                {
                    double difference = _instanceObject.ActualWidth + actualpoint.X - SystemParameters.VirtualScreenWidth;
                    _instanceObject.Left = actualpoint.X - difference;
                }
                else if (!(actualpoint.X > 1))
                {
                    _instanceObject.Left = 1;
                }
                else
                    _instanceObject.Left = actualpoint.X;

                _instanceObject.Top = actualpoint.Y;
                _instanceObject.Show();
            }
        }

        private static void TouchScreenNumPadPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (sender is FrameworkElement host)
            {
                host.GotFocus += OnGotFocus;
                host.LostFocus += OnLostFocus;
            }
        }

        private static void OnGotFocus(object sender, RoutedEventArgs e)
        {
            Control host = sender as Control;

            _previousTextBoxBackgroundBrush = host.Background;
            _previousTextBoxBorderBrush = host.BorderBrush;
            _previousTextBoxBorderThickness = host.BorderThickness;

            host.Background = Brushes.Yellow;
            host.BorderBrush = Brushes.Red;
            host.BorderThickness = new Thickness(4);


            _currentControl = host;

            if (_instanceObject == null)
            {
                FrameworkElement ct = host;
                while (true)
                {
                    if (ct is Window window)
                    {
                        window.LocationChanged += TouchScreenNumPad_LocationChanged;
                        window.Activated += TouchScreenNumPad_Activated;
                        window.Deactivated += TouchScreenNumPad_Deactivated;
                        break;
                    }
                    ct = (FrameworkElement)ct.Parent;
                }

                _instanceObject = new TouchScreenNumPad();
                _instanceObject.AllowsTransparency = true;
                _instanceObject.WindowStyle = WindowStyle.None;
                _instanceObject.ShowInTaskbar = false;
                _instanceObject.ShowInTaskbar = false;
                _instanceObject.Topmost = true;

                host.LayoutUpdated += tb_LayoutUpdated;
            }
        }

        private static void TouchScreenNumPad_Deactivated(object sender, EventArgs e)
        {
            if (_instanceObject != null)
            {
                _instanceObject.Topmost = false;
            }
        }

        private static void TouchScreenNumPad_Activated(object sender, EventArgs e)
        {
            if (_instanceObject != null)
            {
                _instanceObject.Topmost = true;
            }
        }

        private static void TouchScreenNumPad_LocationChanged(object sender, EventArgs e)
        {
            Syncchild();
        }

        private static void tb_LayoutUpdated(object sender, EventArgs e)
        {
            Syncchild();
        }

        private static void OnLostFocus(object sender, RoutedEventArgs e)
        {
            var host = sender as Control;
            host.Background = _previousTextBoxBackgroundBrush;
            host.BorderBrush = _previousTextBoxBorderBrush;
            host.BorderThickness = _previousTextBoxBorderThickness;

            if (_instanceObject != null)
            {
                _instanceObject.Close();
                _instanceObject = null;
            }
        }

        #endregion

        #endregion
    }
}
