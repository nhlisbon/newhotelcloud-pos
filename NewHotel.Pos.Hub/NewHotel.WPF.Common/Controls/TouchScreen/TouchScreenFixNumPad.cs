﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Input;

namespace NewHotel.WPF.Common.Controls.TouchScreen
{
    public class TouchScreenFixNumPad : UserControl
    {
        #region Members
     
        public static Dictionary<string, TouchScreenFixNumPad> Instance = new Dictionary<string, TouchScreenFixNumPad>();

        public static RoutedUICommand Cmd00 = new RoutedUICommand();
        public static RoutedUICommand Cmd1 = new RoutedUICommand();
        public static RoutedUICommand Cmd2 = new RoutedUICommand();
        public static RoutedUICommand Cmd3 = new RoutedUICommand();
        public static RoutedUICommand Cmd4 = new RoutedUICommand();
        public static RoutedUICommand Cmd5 = new RoutedUICommand();
        public static RoutedUICommand Cmd6 = new RoutedUICommand();
        public static RoutedUICommand Cmd7 = new RoutedUICommand();
        public static RoutedUICommand Cmd8 = new RoutedUICommand();
        public static RoutedUICommand Cmd9 = new RoutedUICommand();
        public static RoutedUICommand Cmd0 = new RoutedUICommand();
        public static RoutedUICommand CmdMinus = new RoutedUICommand();
        public static RoutedUICommand CmdPlus = new RoutedUICommand();
        public static RoutedUICommand CmdPLU = new RoutedUICommand();
        public static RoutedUICommand CmdBackspace = new RoutedUICommand();
        public static RoutedUICommand CmdDot = new RoutedUICommand();
        public static RoutedUICommand CmdClear = new RoutedUICommand();
        public static RoutedUICommand CmdEnter = new RoutedUICommand();
        
        private Control _currentControl;
        private string _touchScreenText;
        private string _instanceName;
        private bool _resetNext;

        #endregion
        #region Constructor

        public TouchScreenFixNumPad()
        {
            SetCommandBinding();
        }

        #endregion
        #region Events

        public static event Action<TouchScreenFixNumPad> Enter_Click;
        public static event Action<TouchScreenFixNumPad> CurrentControlChanged;

        #endregion
        #region Properties

        public Control CurrentControl
        {
            get { return _currentControl; }
            set
            {
                _currentControl = value;
                CurrentControlChanged?.Invoke(this);
            }
        }

        public string InstanceName
        {
            get { return _instanceName; }
            set
            {
                _instanceName = value;
                if (Instance.ContainsKey(value))
                    Instance[value] = this;
                else
                    Instance.Add(value, this);
            }
        }

        public string TouchScreenText
        {
            get { return _touchScreenText; }
            set
            {
                _touchScreenText = value;
                switch (CurrentControl)
                {
                    case TextBox textBox:
                        textBox.Text = _touchScreenText;
                        break;
                    case PasswordBox passwordBox:
                        passwordBox.Password = _touchScreenText;
                        break;
                    case DecimalEditControl decimalEditControl:
                        if (string.IsNullOrEmpty(value))
                        {
                            if (decimalEditControl.AllowNullInput)
                                decimalEditControl.EditValue = null;
                            else
                                decimalEditControl.Value = 0;
                        }
                        else
                        {
                            decimal decimalValue;
                            if (decimal.TryParse(_touchScreenText, out decimalValue))
                                decimalEditControl.Value = decimalValue;
                        }
                        break;
                    case QuantityUpDown quantityControl:
                        if (string.IsNullOrEmpty(value))
                            quantityControl.Clear();
                        else
                        {
                            decimal decimalValue;
                            if (decimal.TryParse(_touchScreenText, out decimalValue))
                                quantityControl.Value = decimalValue;
                        }
                        break;
                }
            }
        }

        #endregion
        #region Private Methods

        private static void SetCommandBinding()
        {
            var cb00 = new CommandBinding(Cmd00, RunCommand);
            var cb1 = new CommandBinding(Cmd1, RunCommand);
            var cb2 = new CommandBinding(Cmd2, RunCommand);
            var cb3 = new CommandBinding(Cmd3, RunCommand);
            var cb4 = new CommandBinding(Cmd4, RunCommand);
            var cb5 = new CommandBinding(Cmd5, RunCommand);
            var cb6 = new CommandBinding(Cmd6, RunCommand);
            var cb7 = new CommandBinding(Cmd7, RunCommand);
            var cb8 = new CommandBinding(Cmd8, RunCommand);
            var cb9 = new CommandBinding(Cmd9, RunCommand);
            var cb0 = new CommandBinding(Cmd0, RunCommand);
            var cbMinus = new CommandBinding(CmdMinus, RunCommand);
            var cbPlus = new CommandBinding(CmdPlus, RunCommand);
            var cbPLU = new CommandBinding(CmdPLU, RunCommand);
            var cbDot = new CommandBinding(CmdDot, RunCommand);
            var cbBackspace = new CommandBinding(CmdBackspace, RunCommand);
            var cbClear = new CommandBinding(CmdClear, RunCommand);
            var cbEnter = new CommandBinding(CmdEnter, RunCommand);

            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenFixNumPad), cb00);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenFixNumPad), cb1);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenFixNumPad), cb2);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenFixNumPad), cb3);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenFixNumPad), cb4);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenFixNumPad), cb5);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenFixNumPad), cb6);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenFixNumPad), cb7);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenFixNumPad), cb8);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenFixNumPad), cb9);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenFixNumPad), cb0);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenFixNumPad), cbMinus);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenFixNumPad), cbPlus);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenFixNumPad), cbPLU);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenFixNumPad), cbDot);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenFixNumPad), cbBackspace);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenFixNumPad), cbClear);
            CommandManager.RegisterClassCommandBinding(typeof(TouchScreenFixNumPad), cbEnter);
        }

        private static void RunCommand(object sender, ExecutedRoutedEventArgs e)
        {
            var numPad = (TouchScreenFixNumPad)sender;

            if (numPad._resetNext)
            {
                numPad.TouchScreenText = string.Empty;
                numPad._resetNext = false;
            }

            if (e.Command == Cmd00)
                numPad.TouchScreenText += "00";
            else if (e.Command == Cmd1)
                numPad.TouchScreenText += "1";
            else if (e.Command == Cmd2)
                numPad.TouchScreenText += "2";
            else if (e.Command == Cmd3)
                numPad.TouchScreenText += "3";
            else if (e.Command == Cmd4)
                numPad.TouchScreenText += "4";
            else if (e.Command == Cmd5)
                numPad.TouchScreenText += "5";
            else if (e.Command == Cmd6)
                numPad.TouchScreenText += "6";
            else if (e.Command == Cmd7)
                numPad.TouchScreenText += "7";
            else if (e.Command == Cmd8)
                numPad.TouchScreenText += "8";
            else if (e.Command == Cmd9)
                numPad.TouchScreenText += "9";
            else if (e.Command == Cmd0)
                numPad.TouchScreenText += "0";
            else if (e.Command == CmdMinus)
                numPad.TouchScreenText += "-";
            else if (e.Command == CmdPlus)
                numPad.TouchScreenText += "+";
            else if (e.Command == CmdPLU)
                numPad.TouchScreenText += " X ";
            else if (e.Command == CmdDot)
                numPad.TouchScreenText += Thread.CurrentThread.CurrentUICulture.NumberFormat.CurrencyDecimalSeparator;
            else if (e.Command == CmdBackspace)
            { 
                if (!string.IsNullOrEmpty(numPad.TouchScreenText))
                    numPad.TouchScreenText = numPad.TouchScreenText.Substring(0, numPad.TouchScreenText.Length - 1);
            }
            else if (e.Command == CmdClear)
                numPad.TouchScreenText = string.Empty;
            else if (e.Command == CmdEnter)
                Enter_Click?.Invoke(numPad);
        }

        #endregion
        #region Public Methods

        public void ResetNext()
        {
            _resetNext = true;
        }

        #endregion
    }
}