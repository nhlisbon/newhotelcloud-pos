﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewHotel.WPF.Common.Controls
{
    /// <summary>
    /// Interaction logic for RangePicker.xaml
    /// </summary>
    public partial class RangePicker : UserControl
    {
        #region Constructors

        public RangePicker()
        {
            InitializeComponent();
        }

        #endregion
        #region Properties

        #region Mode

        public RpMode Mode
        {
            get => (RpMode)GetValue(ModeProperty);
            set => SetValue(ModeProperty, value);
        }

        public static readonly DependencyProperty ModeProperty =
            DependencyProperty.Register(nameof(Mode), typeof(RpMode), typeof(RangePicker), new PropertyMetadata(ModePropertyChanged));

        private static void ModePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = (RangePicker)d;
            switch ((RpMode)e.NewValue)
            {
                case RpMode.None:
                    obj.Visibility = Visibility.Collapsed;
                    break;
                case RpMode.DateRange:
                    obj.RdbByDate.IsChecked = true;
                    break;
                case RpMode.NumberRange:
                    obj.RdbByNumber.IsChecked = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        #endregion
        #region StartDate

        public DateTime StartDate
        {
            get => (DateTime)GetValue(StartDateProperty);
            set => SetValue(StartDateProperty, value);
        }

        public static readonly DependencyProperty StartDateProperty =
            DependencyProperty.Register(nameof(StartDate), typeof(DateTime), typeof(RangePicker));

        #endregion
        #region EndDate

        public DateTime EndDate
        {
            get => (DateTime)GetValue(EndDateProperty);
            set => SetValue(EndDateProperty, value);
        }

        public static readonly DependencyProperty EndDateProperty =
            DependencyProperty.Register(nameof(EndDate), typeof(DateTime), typeof(RangePicker));

        #endregion
        #region StartNumber

        public long StartNumber
        {
            get => (long)GetValue(StartNumberProperty);
            set => SetValue(StartNumberProperty, value);
        }

        public static readonly DependencyProperty StartNumberProperty =
            DependencyProperty.Register(nameof(StartNumber), typeof(long), typeof(RangePicker));

        #endregion
        #region EndNumber

        public long EndNumber
        {
            get => (long)GetValue(EndNumberProperty);
            set => SetValue(EndNumberProperty, value);
        }

        public static readonly DependencyProperty EndNumberProperty =
            DependencyProperty.Register(nameof(EndNumber), typeof(long), typeof(RangePicker));

        #endregion

        #endregion
        #region Methods

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Mode = RpMode.DateRange;
            StartDate = DateTime.Today;
            EndDate = DateTime.Today;
            StartNumber = 1;
            EndNumber = 1;
        }

        private void RdbByDate_OnChecked(object sender, RoutedEventArgs e)
        {
            Mode = RpMode.DateRange;
        }

        private void RdbByNumber_OnChecked(object sender, RoutedEventArgs e)
        {
            Mode = RpMode.NumberRange;
        }

        #endregion
        #region Enums

        public enum RpMode
        {
            None, DateRange, NumberRange
        }

        #endregion
    }
}
