﻿using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace NewHotel.WPF.Common.Controls
{
    /// <summary>
    /// Interaction logic for PickerControl.xaml
    /// </summary>
    public partial class PickerControl : UserControl
    {
        #region Constructors

        public PickerControl()
        {
            InitializeComponent();
        }

        #endregion
        #region Properties

        #region ShowFilter

        public bool ShowFilter
        {
            get => (bool)GetValue(ShowFilterProperty);
            set => SetValue(ShowFilterProperty, value);
        }

        public static readonly DependencyProperty ShowFilterProperty =
            DependencyProperty.Register(nameof(ShowFilter), typeof(bool), typeof(PickerControl), new PropertyMetadata(true));

        #endregion
        #region FilterText

        public string FilterText
        {
            get => (string)GetValue(FilterTextProperty);
            set => SetValue(FilterTextProperty, value);
        }

        public static readonly DependencyProperty FilterTextProperty =
            DependencyProperty.Register(nameof(FilterText), typeof(string), typeof(PickerControl), new PropertyMetadata(FilterTextChanged));

        private static void FilterTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = (PickerControl)d;
            obj.ItemsSourceView.View.Refresh();
        }

        #endregion
        #region ItemContainerStyle

        public Style ItemContainerStyle
        {
            get => (Style)this.GetValue(ItemContainerStyleProperty);
            set => SetValue(ItemContainerStyleProperty, value);
        }

        public static readonly DependencyProperty ItemContainerStyleProperty =
            DependencyProperty.Register(nameof(ItemContainerStyle), typeof(Style), typeof(PickerControl));

        #endregion
        #region SelectedItem

        public object SelectedItem
        {
            get => GetValue(SelectedItemProperty);
            set => SetValue(SelectedItemProperty, value);
        }

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register(nameof(SelectedItem), typeof(object), typeof(PickerControl));

        #endregion
        #region ItemsSource

        public IEnumerable ItemsSource
        {
            get => (IEnumerable)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register(nameof(ItemsSource), typeof(IEnumerable), typeof(PickerControl), new PropertyMetadata(ItemsSourceChanged));

        private static void ItemsSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = (PickerControl)d;
            obj.ItemsSourceView.Source = obj.ItemsSource;
            obj.ItemsSourceView.View.Filter = item => obj.Filter?.Invoke(item, obj.FilterText) ?? true;
        }

        #endregion
        #region ItemTemplate

        public DataTemplate ItemTemplate
        {
            get => (DataTemplate)GetValue(ItemTemplateProperty);
            set => SetValue(ItemTemplateProperty, value);
        }

        public static readonly DependencyProperty ItemTemplateProperty =
            DependencyProperty.Register(nameof(ItemTemplate), typeof(DataTemplate), typeof(PickerControl));

        #endregion

        public Func<object, string, bool> Filter { get; set; }
        public CollectionViewSource ItemsSourceView => (CollectionViewSource)Resources["ItemsSourceView"];

        #endregion
        #region Methods

        public void Release()
        {
            ClearValue(FilterTextProperty);
        }

        #endregion
    }
}