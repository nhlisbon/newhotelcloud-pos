﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using NewHotel.Localization;

namespace NewHotel.WPF.Common.Controls
{
    public class TranslationLabel : Label
    {
        #region Dependency Properties

        public static DependencyProperty TranslationIdProperty =
            DependencyProperty.Register(nameof(TranslationId), typeof(string), typeof(TranslationLabel), new PropertyMetadata(null, TranslationIdPropertyChanged));

        public string TranslationId
        {
            get => (string)GetValue(TranslationIdProperty);
            set => SetValue(TranslationIdProperty, value);
        }

        public static DependencyProperty SuffixProperty =
            DependencyProperty.Register(nameof(Suffix), typeof(LocalizedSuffix), typeof(TranslationLabel), new PropertyMetadata(LocalizedSuffix.Label, SuffixPropertyChanged));

        public LocalizedSuffix Suffix
        {
            get => (LocalizedSuffix)GetValue(SuffixProperty);
            set => SetValue(SuffixProperty, value);
        }

        #endregion
        #region Constructor

        public TranslationLabel()
        {
        }

        #endregion
        #region Private Methods

        private static void TranslationIdPropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var source = (TranslationLabel)sender;
            source.Refresh((string)e.NewValue, source.Suffix);
        }

        private static void SuffixPropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var source = (TranslationLabel)sender;
            source.Refresh(source.TranslationId, (LocalizedSuffix)e.NewValue);
        }

        private void Refresh(string translationId, LocalizedSuffix suffix)
        {
            if (!string.IsNullOrEmpty(translationId))
                Content = new LocalizedText(translationId, suffix);
        }

        #endregion
        #region Public Methods

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            Refresh(TranslationId, Suffix);
        }

        #endregion
    }
}
