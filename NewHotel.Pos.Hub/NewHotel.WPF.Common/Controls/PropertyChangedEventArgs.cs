﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.WPF.Common.Controls
{
    public class NHPropertyChangedEventArgs : PropertyChangedEventArgs
    {
        public object OldValue { get; set; }
        public object NewValue { get; set; }

        public NHPropertyChangedEventArgs(string propertyName) : base(propertyName)
        {
        }
    }
}
