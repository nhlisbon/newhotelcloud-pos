﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using NewHotel.Pos.Core.Ext;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Localization;
using NewHotel.Pos.Printer.Interfaces;
using NewHotel.WPF.App.Pos.Controls;

namespace NewHotel.Pos.PrinterDocument.Common
{
    internal static class Utils
    {
        public static IEnumerable<IPrinteableMovementTaxDetail> TaxGroups(IPrintableDoc ticket)
        {
            var taxes = new List<IPrinteableMovementTaxDetail>();

            IEnumerable<IPrintableOrder> productLines;
            if (ticket.IsAnul)
                productLines = ticket.ProductLines.Where(o => o.AnulAfterClosed);
            else
                productLines = ticket.ProductLines.Where(o => o.IsVisible);

            foreach (var order in productLines)
            {
                foreach (var item in GetTaxList(order))
                {
                    var auxTax = taxes.FirstOrDefault(x => x.TaxRateId == item.TaxRateId);
                    if (auxTax != null)
                    {
                        // Ya tenemos un impuesto con ese porciento
                        auxTax.TaxBase += item.TaxBase;
                        auxTax.TaxValue += item.TaxValue;
                    }
                    else
                        // Este porciento de impuesto es nuevo
                        taxes.Add(item);
                }
            }

            return taxes.OrderBy(x => x.TaxPercent);
        }

        public static ObservableCollection<OrderLite> OrderLites(IPrintableDoc ticket, IPrintDocSetting settings)
        {
            var lites = new ObservableCollection<OrderLite>();

            foreach (var order in ticket.ProductLines)
            {
                if (!order.IsVisible && (!ticket.IsAnul || order.IsTip || (!order.AnulAfterClosed && !order.IsActive))) continue;
                // Diferentes lineas con el mismo producto, descuento y sin notas(commentarios) se muestran como una en el ticket
                var lite = lites.FirstOrDefault(x => x.ProductId == order.ProductId &&
                                                     string.IsNullOrEmpty(order.Notes) &&
                                                     string.IsNullOrEmpty(x.Comments) &&
                                                     order.DiscountPercent == x.DiscountPercent &&
                                                     order.AutomaticDiscount == x.AutomaticDiscount &&
                                                     order.ProductDescription == x.ProductDescription);
                if (lite == null)
                {
                    lite = new OrderLite
                    {
                        ItemNumber = order.ItemNumber,
                        Separator = order.Separator,
                        ProductId = order.ProductId,
                        ProductDescription = order.ProductDescription,
                        DiscountPercent = order.DiscountPercent,
                        AutomaticDiscount = order.AutomaticDiscount,
                        Comments = settings.ShowCommentsProducts ? order.Notes : string.Empty,
                        ShowDiscountInfo = order.ShowDiscountInfo,
                        FirstIvaDescription = order.FirstIvaDescription,
                        SecondIvaDescription = order.SecondIvaDescription,
                        ThirdIvaDescription = order.ThirdIvaDescription,
                        PaxNumber = order.PaxNumber,
                        ClientInfo = ticket.Clients.FirstOrDefault(c => c.Pax == (order.PaxNumber ?? 0))?.ClientInfo,
                        ShowQuantityDecimalCase = order.ShowQuantityDecimalCase,
                        DiscountTypeDescription = order.DiscountTypeDescription,
                    };

                    lites.Add(lite);
                }

                lite.Quantity += order.Quantity;
                lite.ProductPriceBeforeDiscount += order.ProductPriceBeforeDiscount;
                lite.CostBeforeDiscount += order.CostBeforeDiscount;
                lite.DiscountValue += order.DiscountValue;
                lite.Cost += order.Cost;
                lite.FirstIvas = lite.FirstIvas.GetValueOrDefault() + order.FirstIvas;
                lite.SecondIvas = lite.SecondIvas.GetValueOrDefault() + order.SecondIvas;
                lite.ThirdIvas = lite.ThirdIvas.GetValueOrDefault() + order.ThirdIvas;
                lite.CodeIsentoIva = order.CodeIsentoIva;
                lite.DescIsentoIva = order.DescIsentoIva;
                lite.DiscountTypeDescription = order.DiscountTypeDescription;
            }

            return lites;
        }

        public static ObservableCollection<TipSuggestionLite> ProposalSuggestions(IPrintDocSetting settings, IPrintableDoc ticket)
        {
            var suggestions = new ObservableCollection<TipSuggestionLite>();

            foreach (var suggestion in settings.Suggestions)
            {
                suggestions.Add(new TipSuggestionLite
                {
                    Description = suggestion.Description,
                    Autogenerate = suggestion.Autogenerate,
                    Proposal = (suggestion.OverNet ? ticket.NetSubTotalAmount : ticket.GrossSubTotalAmount).GetDiscountValue(suggestion.Percent)
                });
            }

            return suggestions;
        }

        public static TitleData GetTitleData(TicketTitles title, IPrintableDoc doc, string country)
        {
            var titleDescription = string.Empty;
            var isCopy = false;

            switch (title)
            {
                case TicketTitles.Invoice:
                    titleDescription = country == "CO" ? "FACTURA DE VENTA" : "POSInvoiceDefaultName".Translate();
                    isCopy = doc.FiscalDocPrints > 1;
                    break;
                case TicketTitles.Ballot:
                    titleDescription = "POSBallotDefaultName".Translate();
                    isCopy = doc.FiscalDocPrints > 1;
                    break;
                case TicketTitles.CreditNote:
                    titleDescription = "POSCreditNoteDefaultName".Translate();
                    isCopy = doc.CreditNotePrints > 1;
                    break;
                case TicketTitles.Ticket:
                    titleDescription = "POSTicketDefaultName".Translate();
                    isCopy = doc.Prints > 1;
                    break;
                case TicketTitles.Receipt:
                    titleDescription = "POSReceiptDefaultName".Translate();
                    break;
            }

            return new TitleData
            {
                TitleDescription = titleDescription.ToUpper(),
                IsCopy = isCopy,
                IsInvoice = title == TicketTitles.Invoice,
                IsCreditNote = title == TicketTitles.CreditNote,
                IsReceipt = title == TicketTitles.Receipt,
                IsTicket = title == TicketTitles.Ticket,
                IsBallot = title == TicketTitles.Ballot
            };
        }

        private static IEnumerable<IPrinteableMovementTaxDetail> GetTaxList(IPrintableOrder order)
        {
            yield return new MovementTaxDetail
            {
                TaxRateId = order.FirstIvaCode,
                TaxPercent = order.FirstIvaPercent,
                TaxRateDescription = order.FirstIvaDescription,
                TaxValue = order.FirstIvas,
                TaxBase = order.FirstIvaBase
            };

            if (order.SecondIvaCode.HasValue && order.SecondIvaPercent.HasValue)
            {
                yield return new MovementTaxDetail
                {
                    TaxRateId = order.SecondIvaCode.Value,
                    TaxPercent = order.SecondIvaPercent.Value,
                    TaxRateDescription = order.SecondIvaDescription,
                    TaxValue = order.SecondIvas.Value,
                    TaxBase = order.SecondIvaBase.Value
                };
            }

            if (order.ThirdIvaCode.HasValue && order.ThirdIvaPercent.HasValue)
            {
                yield return new MovementTaxDetail
                {
                    TaxRateId = order.ThirdIvaCode.Value,
                    TaxPercent = order.ThirdIvaPercent.Value,
                    TaxRateDescription = order.ThirdIvaDescription,
                    TaxValue = order.ThirdIvas.Value,
                    TaxBase = order.ThirdIvaBase.Value
                };
            }
        }
    }

    internal class TitleData
    {
        public string TitleDescription { get; set; }
        public bool IsInvoice { get; set; }
        public bool IsCreditNote { get; set; }
        public bool IsReceipt { get; set; }
        public bool IsTicket { get; set; }
        public bool IsBallot { get; set; }
        public bool IsCopy { get; set; }
    }

    public class OrderLite
    {      
        public IPrinteableSeparator Separator { get; set; }
        public int ItemNumber { get; set; }
        public Guid ProductId { get; set; }
        public string ProductDescription { get; set; }
        public decimal Quantity { get; set; }
        public string Comments { get; set; }
        public bool ShowDiscountInfo { get; set; }
        public decimal ProductPriceBeforeDiscount { get; set; }
        public decimal DiscountPercent { get; set; }
        public decimal DiscountValue { get; set; }
        public decimal CostBeforeDiscount { get; set; }
        public decimal Cost { get; set; }
        public string FirstIvaDescription { get; set; }
        public string SecondIvaDescription { get; set; }
        public string ThirdIvaDescription { get; set; }
        public decimal? FirstIvas { get; set; }
        public decimal? SecondIvas { get; set; }
        public decimal? ThirdIvas { get; set; }
        public bool AutomaticDiscount { get; set; }
        public short? PaxNumber { get; set; }
        public string ClientInfo { get; set; }

        public bool ShowQuantityDecimalCase { get; set; }
        public string CodeIsentoIva { get; set; }
        public string DescIsentoIva { get; set; }

        public bool HasFirstTax => !string.IsNullOrEmpty(FirstIvaDescription) && FirstIvas.HasValue;
        public bool HasSecondTax => !string.IsNullOrEmpty(SecondIvaDescription) && SecondIvas.HasValue;
        public bool HasThirdITax => !string.IsNullOrEmpty(ThirdIvaDescription) && ThirdIvas.HasValue;
        public string DiscountLabel => AutomaticDiscount ? "a" : "m";
        public string DiscountTypeDescription { get; set; }
        public bool ShowComments => !string.IsNullOrEmpty(Comments);
        public string PaxName
        {
            get
            {
                return PaxNumber switch
                {
                    null => string.Empty,
                    0 => LocalizationMgr.Translation("Shared"),
                    _ => $" {PaxNumber.ToString()} {ClientInfo}"
                };
            }
        }
    }

    public class TipSuggestionLite
    {
        public string Description { get; set; }
        public decimal? Proposal { get; set; }
        public bool Autogenerate { get; set; }
    }

    public class MovementTaxDetail : IPrinteableMovementTaxDetail
    {
        public Guid TaxRateId { get; set; }
        public decimal TaxPercent { get; set; }
        public string TaxRateDescription { get; set; }
        public decimal TaxValue { get; set; }
        public decimal TaxBase { get; set; }
    }
}