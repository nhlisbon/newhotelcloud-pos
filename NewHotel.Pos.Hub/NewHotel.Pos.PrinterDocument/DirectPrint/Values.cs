﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.PrinterDocument.DirectPrint
{
	public struct PrintableSize
	{
		private readonly float width;
		private readonly float height;

		public PrintableSize(float width, float height)
		{
			this.width = width;
			this.height = height;
		}

		public float Width => width;
		public float Height => height;
		public bool IsEmpty => width == default && height == default;
	}

	public enum LengthType
	{
		/// <summary>The length of the area is the needed to accomodate its content.</summary>
		Content = 0,
		/// <summary>
		/// The length of the area is the result of distributing the available free space of the container after subtracting all non proportional items.
		/// </summary>
		Proportional = 1,
		/// <summary>The length of the area is a percent of the available in its container.</summary>
		Percent = 2,
		/// <summary>The length is specified by a value of units.</summary>
		Units = 3,
	}

	public enum PrintableHorizontalAlign
	{
		Left = 0,
		Center = 1,
		Right = 2,
	}

	public struct PrntLength
	{
		private readonly float value;
		private readonly LengthType type;

		public static readonly PrntLength Content = new PrntLength(0, LengthType.Content);
		public static readonly PrntLength Proportional = new PrntLength(0, LengthType.Proportional);

		public PrntLength(float value, LengthType type)
		{
			this.value = value;
			this.type = type;
		}

		public float Value => value;

		public LengthType Type => type;
	}

	public struct PrintMargin
	{
		public PrintMargin(float left, float top, float right, float bottom)
		{
			Left = left;
			Top = top;
			Right = right;
			Bottom = bottom;
		}

		public PrintMargin(float allMargins)
		{
			Left = Top = Right = Bottom = allMargins;
		}

		public float Left { get; }
		public float Top { get; }
		public float Right { get; }
		public float Bottom { get; }
	}
}
