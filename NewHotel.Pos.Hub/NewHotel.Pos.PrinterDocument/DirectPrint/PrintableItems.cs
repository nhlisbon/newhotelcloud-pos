﻿using System.Collections.Generic;

namespace NewHotel.Pos.PrinterDocument.DirectPrint
{
	public class PrintableItem
	{
		private PrntLength width;

		public PrntLength Width { get => width; set => width = value; }
		public PrintableHorizontalAlign HorizontalAlign { get; set; }
		public PrintMargin Margin { get; set; }
	}

	public class PrintNewLine : PrintableItem
	{
	}

	public class PrintText : PrintableItem
	{
		public PrintFont Font { get; set; }
		public IEnumerable<string> Text { get; set; }
	}

	public class PrintRectangle : PrintableItem
	{
		public PrntLength Height { get; set; }
	}
}
