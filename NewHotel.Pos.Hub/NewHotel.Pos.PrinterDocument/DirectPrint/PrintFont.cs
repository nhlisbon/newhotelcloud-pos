﻿namespace NewHotel.Pos.PrinterDocument.DirectPrint
{
	public class PrintFont
	{
		public string Name { get; set; }
		public float Size { get; set; }
		public bool Bold { get; set; }
		public bool Italic { get; set; }
		public bool Strikeout { get; set; }
		public bool Underline { get; set; }
		// public System.Drawing.StringUnit FontUnit { get; set; } = System.Drawing.StringUnit.Point;
		public System.Drawing.GraphicsUnit? FontUnit { get; set; } = System.Drawing.GraphicsUnit.Point;


		public string Id => $"{Name}|{Size}{FontUnit}|b{Bold}|i{Italic}|s{Strikeout}|u{Underline}";
	}
}
