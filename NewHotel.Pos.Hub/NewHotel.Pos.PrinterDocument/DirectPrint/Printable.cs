﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.PrinterDocument.DirectPrint
{
	public interface IPrintableValues
	{
		void AddItem(PrintableItem item);
		PrintFont Font { get; set; }
	}

	public class Printable : IPrintableValues
	{
		private readonly List<PrintableItem> items;

		public Printable()
		{
			items = new List<PrintableItem>();
		}

		public void AddItem(PrintableItem item)
		{
			items.Add(item);
		}

		public IReadOnlyList<PrintableItem> Items => items;
		public PrintFont Font { get; set; }
		public PrintableSize PageSize { get; set; }
		public PrintMargin Margin { get; set; }
	}

	public static class PrintableActions
	{
		public static T SetFont<T>(this T printable, string name, float size)
			where T : IPrintableValues
		{
			printable.Font = new PrintFont { Name = name, Size = size };
			return printable;
		}

		public static T SetFont<T>(this T printable, PrintFont font)
			where T : IPrintableValues
		{
			printable.Font = font;
			return printable;
		}

		public static T AddText<T>(this T printable, string text, PrintFont font = default, PrntLength width = default, PrintableHorizontalAlign horizontalAlign = default, PrintMargin margin = default)
			where T : IPrintableValues
		{
			var ptext = new PrintText { Text = new string[] { text }, Font = font ?? printable.Font, Width = width, HorizontalAlign = horizontalAlign, Margin = margin };
			printable.AddItem(ptext);
			return printable;
		}

		public static T AddFullText<T>(this T printable, string text, PrintFont font = default, PrintableHorizontalAlign horizontalAlign = default, PrintMargin margin = default)
			where T : IPrintableValues
		{
			var ptext = new PrintText { Text = new string[] { text }, Font = font ?? printable.Font, Width = PrntLength.Proportional, HorizontalAlign = horizontalAlign, Margin = margin };
			printable.AddItem(ptext);
			return printable;
		}

		public static T AddNewLine<T>(this T printable)
			where T : IPrintableValues
		{
			var nl = new PrintNewLine();
			printable.AddItem(nl);
			return printable;
		}

		public static T AddRectangle<T>(this T printable, PrntLength? width = default, PrntLength? height = default)
			where T : IPrintableValues
		{
			var re = new PrintRectangle { Width = width ?? PrntLength.Proportional, Height = height ?? new PrntLength(0, LengthType.Units) };
			printable.AddItem(re);
			return printable;
		}
	}
}
