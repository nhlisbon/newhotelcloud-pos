﻿using NewHotel.Pos.IoC;
using NewHotel.Pos.PrinterDocument.Interfaces;
using NewHotel.Pos.PrinterDocument.Simple;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.PrinterDocument
{
	public static class PrinterDocumentConfiguration
	{
		public static void Configure(IServiceConfigurator configurator)
		{
			configurator
				.AddTransient<ITicketPrinter, SimplePrinterDocument>()
				;
		}
	}
}
