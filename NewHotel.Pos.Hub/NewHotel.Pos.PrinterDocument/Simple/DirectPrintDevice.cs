﻿using NewHotel.Pos.PrinterDocument.DirectPrint;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.PrinterDocument.Simple
{
	public class DirectPrintDevice
	{
		string printer;

		public DirectPrintDevice(string printer)
		{
			this.printer = printer;
		}

		public void Print(Printable printable)
		{
			var context = new PrintContext(printable);

			PrintDocument pd = new PrintDocument();
			pd.PrinterSettings.PrinterName = printer;
			pd.PrintPage += (s, e) =>
			{
				PrintPage(context, e);
			};
			pd.Print();
		}

		private static void PrintPage(PrintContext context, PrintPageEventArgs args)
		{
			args.Graphics.PageUnit = GraphicsUnit.Point;

			var fonts = new Dictionary<string, Font>();
			var black_pen = new Pen(Color.Black, 0.5f);

			PrintingLineItem maxItem = null;
			RectangleF clip = context.Printable.PageSize.IsEmpty ? args.Graphics.VisibleClipBounds : new RectangleF(0, 0, context.Printable.PageSize.Width, context.Printable.PageSize.Height);

			clip.Offset(context.Printable.Margin.Left, context.Printable.Margin.Top);
			clip.Width -= context.Printable.Margin.Left + context.Printable.Margin.Right;

			args.Graphics.SetClip(clip);

			var line = new List<PrintingLineItem>();

			float baseX = 0;
			float currentX = 0;
			float currentY = clip.Y; // 0;
			float maxHeight = 0;
			int proportions = 0;

			float GetFirstX() => clip.X; // baseX;
			float GetReminderWidth() => clip.Width - (currentX - GetFirstX());

			StringAlignment GetFormat(PrintableHorizontalAlign align)
			{
				return align switch
				{
					PrintableHorizontalAlign.Left => StringAlignment.Near,
					PrintableHorizontalAlign.Center => StringAlignment.Center,
					PrintableHorizontalAlign.Right => StringAlignment.Far,
					_ => throw new NotImplementedException(),
				};
			}

			void RenderLine()
			{
				float allwidth = clip.Width;

				foreach (var lineItem in line)
				{
					var rect = lineItem.Rectangle;
					// rect.Offset(0, maxItem.BaseLine - lineItem.BaseLine);
					// lineItem.Rectangle = rect;
					if (lineItem.Item.Width.Type != LengthType.Proportional)
						allwidth -= rect.Width;
				}

				if (proportions > 0)
				{
					float propWidth = allwidth / proportions;
					float nextX = GetFirstX();
					foreach (var lineItem in line)
					{
						var rect = lineItem.Rectangle;
						float delta = nextX - rect.X;
						if (lineItem.Item.Width.Type == LengthType.Proportional)
							rect.Width = propWidth;
						rect.Offset(delta, 0f);
						nextX = rect.Right;
						lineItem.Rectangle = rect;
					}
				}

				foreach (var lineItem in line)
				{
					RectangleF rect = lineItem.Rectangle;
					rect.Offset(lineItem.Item.Margin.Left, lineItem.Item.Margin.Top);
					rect.Width -= lineItem.Item.Margin.Right;
					rect.Height -= lineItem.Item.Margin.Bottom;
					var sf = new StringFormat { Alignment = GetFormat(lineItem.Item.HorizontalAlign), LineAlignment = StringAlignment.Far };
					if (lineItem.Item is PrintText printText)
					{
						args.Graphics.DrawString(lineItem.Text, lineItem.Font, Brushes.Black, rect, sf);
					}
					else if (lineItem.Item is PrintRectangle printRectangle)
					{
						//--
						float y = rect.Y + (rect.Height / 2) - (printRectangle.Height.Value / 2);
						if (printRectangle.Height.Value > 0)
						{
							args.Graphics.DrawRectangle(Pens.Black, rect.X, y, rect.Width, printRectangle.Height.Value);
						}
						else
						{
							args.Graphics.DrawLine(black_pen, rect.X, y, rect.Right, y);
						}
					}
					// args.Graphics.DrawRectangle(Pens.LightGray, Rectangle.Round(rect));
				}
			}

			void NewLine()
			{
				currentX = GetFirstX();
				currentY += maxHeight;
				maxHeight = 0f;
				proportions = 0;
				maxItem = null;
				line.Clear();
			}

			Font GetFont(PrintFont pfont = default)
			{
				if (pfont == null)
				{
					pfont = context.Printable.Font;
				}

				if (!fonts.TryGetValue(pfont.Id, out var font))
				{
					FontStyle style = FontStyle.Regular
						| (pfont.Bold ? FontStyle.Bold : 0)
						| (pfont.Italic ? FontStyle.Italic : 0)
						| (pfont.Strikeout ? FontStyle.Strikeout : 0)
						| (pfont.Underline ? FontStyle.Underline : 0)
						;
					FontFamily fontFamily = string.IsNullOrEmpty(pfont.Name) ? FontFamily.GenericSansSerif : new FontFamily(pfont.Name);
					GraphicsUnit unit = pfont.FontUnit ?? GraphicsUnit.Point;
					float size = pfont.FontUnit.HasValue ? pfont.Size : (pfont.Size * 72f / 96f);
					font = new Font(fontFamily, size, style, unit);
					// font = new Font(pfont.Name, pfont.Size, style, GraphicsUnit.Point);
					fonts.Add(pfont.Id, font);
				}

				return font;
			}

			void Step(PrintingLineItem lineItem)
			{
				if (lineItem.Item.Width.Type == LengthType.Percent)
				{
					var width = clip.Width * lineItem.Item.Width.Value / 100f;
					if (width > 0)
					{
						var rect = lineItem.Rectangle;
						lineItem.Rectangle = new RectangleF(rect.Location, new SizeF(width, rect.Height));
					}
				}
				if (lineItem.Item.Width.Type == LengthType.Proportional)
				{
					proportions++;
				}
				currentX += lineItem.Rectangle.Size.Width;
				if (maxItem == null || lineItem.Rectangle.Height > maxItem.Rectangle.Height)
				{
					maxItem = lineItem;
					maxHeight = lineItem.Rectangle.Height;
				}
			}

			try
			{
				NewLine();
				while (context.GetNextItem(out PrintableItem item))
				{
					if (item is PrintText printText)
					{
						var txt = printText.Text.First();
						var font = GetFont(printText.Font);
						var restWidth = GetReminderWidth();
						var sizeF = args.Graphics.MeasureString(txt, font, (int)restWidth);
						var rect = new RectangleF(new PointF(currentX, currentY), sizeF);
						rect.Width += item.Margin.Left + item.Margin.Right;
						rect.Height += item.Margin.Top + item.Margin.Bottom;
						var pitem = new PrintingLineItem
						{
							Rectangle = rect,
							Font = font,
							Item = printText,
							Text = txt,
							BaseLine = rect.Height * font.FontFamily.GetCellAscent(font.Style) / font.FontFamily.GetEmHeight(font.Style),
						};
						line.Add(pitem);

						Step(pitem);
					}
					if (item is PrintRectangle rectangle)
					{
						var font = GetFont();
						var height = GetFont().GetHeight(args.Graphics);
						var rect = new RectangleF(currentX, currentY, rectangle.Width.Value, height);
						var pitem = new PrintingLineItem
						{
							Rectangle = rect,
							Font = font,
							Item = rectangle,
							Text = string.Empty,
						};
						line.Add(pitem);

						Step(pitem);
					}
					// --
					if (item is PrintNewLine printNewLine)
					{
						if (maxHeight == 0)
						{
							maxHeight = GetFont().GetHeight(args.Graphics);
						}
						RenderLine();
						NewLine();
					}
				}
				RenderLine();
			}
			catch // (Exception ex)
			{
				// You can't fail
			}

			foreach (var font in fonts.Values)
			{
				font.Dispose();
			}
			black_pen.Dispose();
			args.HasMorePages = false;
		}

	}

	class PrintContext
	{
		private readonly Printable printable;
		private int currentItemIndex;

		public PrintContext(Printable printable)
		{
			this.printable = printable;
			currentItemIndex = -1;
		}

		public Printable Printable => printable;
		public PrintableItem CurentItem => printable.Items[currentItemIndex];

		public bool GetNextItem(out PrintableItem item)
		{
			if ((currentItemIndex + 1) < printable.Items.Count)
			{
				++currentItemIndex;
				item = printable.Items[currentItemIndex];
				return true;
			}
			item = null;
			return false;
		}
	}

	class PrintingLineItem
	{
		public RectangleF Rectangle { get; set; }
		public Font Font { get; set; }
		public string Text { get; set; }
		public PrintableItem Item { get; set; }
		public float BaseLine { get; set; }
	}


}
