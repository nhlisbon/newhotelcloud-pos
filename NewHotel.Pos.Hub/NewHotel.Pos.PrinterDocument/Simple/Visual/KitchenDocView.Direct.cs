﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using NewHotel.Pos.Localization;
using NewHotel.Contracts;
using NewHotel.Pos.PrinterDocument.DirectPrint;
using NewHotel.Pos.PrinterDocument.Fiscal;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Documents;
using NewHotel.Pos.Printer.Interfaces;

namespace NewHotel.Pos.PrinterDocument.Simple.Visual
{
	public partial class KitchenDocView
	{
		Printable IPreparer<Printable>.PreparePrint(PrinterContext context)
		{
			if (string.IsNullOrEmpty(KitchenDoc.AreaDescription))
				throw new ArgumentException("Spooler: Area description is missing in the printers.xml file");

			const int defaultTopMargin = 2;
			const int defaultBottonMargin = 2;
			const int defaultLeftMargin = 12;
			const int defaultRightMargin = 2;

			const string DefaultFont = "Segoe UI";

			var setting = context.Setting;
			context.PrintableAreaWidth.ToString();

			// var font = new PrintFont { Name = "Segoe UI", Size = 7.5f };
			var font = new PrintFont { Name = "Arial", Size = 7.5f };
			var fontAction = new PrintFont { Name = "Arial", Size = context.Setting.ActionFontSize, Bold = true };
			var font_i15 = new PrintFont { Name = "Arial", Size = 12, Italic = true };
			var font_b15 = new PrintFont { Name = "Arial", Size = 15, Bold = true };
			var font_b12 = new PrintFont { Name = "Arial", Size = 12, Bold = true };
			var font_r09 = new PrintFont { Name = "Arial", Size = 9 };
			var font_b09 = new PrintFont { Name = "Arial", Size = 9 , Bold = true };
			var font_line = new PrintFont { Name = "Arial", Size = setting.LineFontSize, FontUnit = null };
			var font_note = new PrintFont { Name = "Arial", Size = setting.NoteFontSize, FontUnit = null, Italic = true };
			var font_seatSeparator = new PrintFont { Name = "Arial", Size = setting.SeatSeparatorFontSize, FontUnit = null, Bold = true };

			var now = DateTime.Now;
			Printable printable = new Printable
			{
				Margin = new PrintMargin(
					setting.LeftMargin + defaultLeftMargin,
					setting.TopMargin + defaultTopMargin,
					setting.RigthMargin + defaultRightMargin,
					setting.BottomMargin + defaultBottonMargin)
			}
				.SetFont(font)
			#region Date and sale number
				.AddText($"{"Date".Translate()}: {now.ToShortDateString()}  {now:HH:mm}", width: PrntLength.Proportional)
				.AddText($"{"SaleNo".Translate()}: {Doc.OpeningNumber}", width: PrntLength.Proportional, horizontalAlign: PrintableHorizontalAlign.Right)
			#endregion
			#region User who dispatched
				.AddNewLine()
				.AddText($"{"User".Translate()}: {Doc.LastUserDispatched}", width: PrntLength.Proportional)
			#endregion
			#region Action
				.AddNewLine()
				.AddFullText(KitchenDoc.ActionDescription, font: fontAction, horizontalAlign: PrintableHorizontalAlign.Center)
			#endregion
			#region Area
				.AddNewLine()
				.AddFullText(KitchenDoc.AreaDescription, font: font_i15, horizontalAlign: PrintableHorizontalAlign.Center)
			#endregion
				;

			#region Saloon & Table
			if (!string.IsNullOrEmpty(Doc.SaloonDescription) || !string.IsNullOrEmpty(Doc.TableDescription))
			{
				var saloonTable = $"{(string.IsNullOrEmpty(Doc.SaloonDescription) ? "[NoSaloon]" : Doc.SaloonDescription)} / {(string.IsNullOrEmpty(Doc.TableDescription) ? "[NoTable]" : Doc.TableDescription)}";
				printable
					.AddNewLine()
					.AddFullText(saloonTable, font: font_b12, horizontalAlign: PrintableHorizontalAlign.Center);
			}
			#endregion

			#region Account

			if (!string.IsNullOrEmpty(Doc.AccountDescription))
			{
				printable
					.AddNewLine()
					.AddNewLine()
					.AddFullText(Doc.AccountDescription, font: font_b09, horizontalAlign: PrintableHorizontalAlign.Center)
					.AddNewLine()
					.AddFullText(Doc.AllowRoomCredit ? "POS".Translate() : "NoPOS".Translate(), font: font_b09, horizontalAlign: PrintableHorizontalAlign.Center)
					.AddNewLine();
			}

			#endregion

			#region Header
			printable
				.AddNewLine().SetFont(font_r09)
				.AddText($"{"Stand".Translate()}:   {Doc.StandDescription}")
				.AddNewLine()
				.AddText($"{"Cashier".Translate()}: {Doc.CashierDescription}");

			if (!string.IsNullOrEmpty(Doc.Room) && string.IsNullOrEmpty(Doc.AccountDescription))
				printable
					.AddNewLine()
					.AddText($"{"Room".Translate()}:    {Doc.Room}");

			if (!string.IsNullOrEmpty(Doc.Name) && string.IsNullOrEmpty(Doc.AccountDescription))
				printable
					.AddNewLine()
					.AddText($"{"Description".Translate()}:    {Doc.Name}");

			printable
				.AddNewLine()
				.AddText($"{"Paxs".Translate()}:    {Doc.Paxs}  User: {Doc.UserDescription}\n")
				.AddNewLine()
				.AddText($"{"Date".Translate()}:    {now.ToShortDateString()}  {now.ToString("HH:mm")}");
			#endregion

			if (Doc.Clients.Any())
			{
				// Separador aqui
				printable
					.AddNewLine()
					.AddRectangle();

				#region Client Info

				foreach (var client in Doc.Clients)
				{
					printable
						.AddNewLine()
						.AddText($"{client.Pax}", font: font_b09)
						.AddRectangle()
						.AddText($"{client.ClientInfo}", font: font_b09, horizontalAlign: PrintableHorizontalAlign.Center)
						.AddRectangle()
						;

					var hasPreferences = !string.IsNullOrEmpty(client.Preferences);
					var hasDiets = !string.IsNullOrEmpty(client.Diets);
					var hasAttentions = !string.IsNullOrEmpty(client.Attentions);
					var hasAllergies = !string.IsNullOrEmpty(client.Allergies);
					var hasSegmentOperations = !string.IsNullOrEmpty(client.SegmentOperations);
					var hasUncommonAllergies = !string.IsNullOrEmpty(client.UncommonAllergies);
					var hasClientInfo = hasPreferences || hasDiets || hasAttentions || hasAllergies || hasSegmentOperations || hasUncommonAllergies;

					if (!hasClientInfo) continue;

					#region Preferences

					if (hasPreferences)
					{
						printable
							.AddNewLine()
							.AddText($"{"Preferences".Translate()}: ", font: font_b09)
							.AddFullText($"{client.Preferences}", font: font_r09)
							;
					}

					#endregion
					#region Diets

					if (hasDiets)
					{
						printable
							.AddNewLine()
							.AddText($"{"Diets".Translate()}: ", font: font_b09)
							.AddFullText($"{client.Diets}", font: font_r09)
							;
					}

					#endregion

					#region Attentions

					if (hasAttentions)
					{
						printable
							.AddNewLine()
							.AddText($"{"Attentions".Translate()}: ", font: font_b09)
							.AddFullText($"{client.Attentions}", font: font_r09)
							;
					}

					#endregion

					#region Allergies

					if (hasAllergies)
					{
						printable
							.AddNewLine()
							.AddText($"{"Allergies".Translate()}: ", font: font_b09)
							.AddFullText($"{client.Allergies}", font: font_r09)
							;
					}

					#endregion
					
					#region Segment Operations

					if (hasSegmentOperations)
					{
						printable
							.AddNewLine()
							.AddText($"{"OperationSegments".Translate()}: ", font: font_b09)
							.AddFullText($"{client.SegmentOperations}", font: font_r09)
							;
					}

					#endregion

					#region Uncommon Allergies

					if (hasUncommonAllergies)
					{
						printable
							.AddNewLine()
							.AddText($"{"UncommonAllergies".Translate()}: ", font: font_b09)
							.AddFullText($"{client.UncommonAllergies}", font: font_r09)
							;
					}

					#endregion
				}

				#endregion
			}

			#region Separator

			printable
				.AddNewLine()
				.AddRectangle();

			#endregion


			#region Group Orders

			if (string.IsNullOrEmpty(Doc.SeriePrex))
				Trace.TraceInformation($"Kitchen doc view Num:{Doc.OpeningNumber} Ticket:{Doc.Serie ?? 0}");
			else
				Trace.TraceInformation($"Kitchen doc view Num:{Doc.OpeningNumber} Ticket:{Doc.Serie ?? 0}/{Doc.SeriePrex}");
			foreach (var productLine in Doc.ProductLines)
				Trace.TraceInformation($"ProductLine Id:{productLine.Id}");
			foreach (var orderId in KitchenDoc.OrderIds)
				Trace.TraceInformation($"Order Id:{orderId}");

			var orders = new List<KitchenOrder>();
			var productLines = Doc.ProductLines.Where(x => KitchenDoc.OrderIds.Keys.Contains(x.Id));
			Trace.TraceInformation($"{productLines.Count()} orders found in product lines");

			foreach (var productLine in productLines)
			{
				var similar = orders.FirstOrDefault(x => x.Line.ProductId == productLine.ProductId &&
														 (string.IsNullOrEmpty(productLine.Notes) && string.IsNullOrEmpty(x.Line.Notes) || productLine.Notes == x.Line.Notes) &&
														  !productLine.HasManualPrice && !x.Line.HasManualPrice &&
														  productLine.Separator?.Description == x.Line.Separator?.Description &&
														  (productLine.PaxNumber ?? short.MaxValue) == (x.Line.PaxNumber ?? short.MaxValue) &&
														  productLine.TableLines.Count == 0 && x.Line.TableLines.Count == 0 &&
														 ((productLine.Preparations.Count == 0 && x.Line.Preparations.Count == 0) ||
														  productLine.Preparations.OrderBy(y => y.Id).SequenceEqual(x.Line.Preparations.OrderBy(y => y.Id), new PreparationComparer())));

				if (similar != null)
					similar.Quantity += productLine.Quantity;
				else
					orders.Add(new KitchenOrder(productLine));
			}

			#endregion

			#region Orders

			switch (KitchenDoc.ProductGrouping)
			{
				case TicketProductGrouping.SeatsSeparators:
					{
						Trace.TraceInformation($"Grouping by seats {orders.Count} orders");
						var seats = orders.GroupBy(x => x.Line.PaxName)
										  .OrderBy(x => x.First().Line.PaxNumber ?? short.MaxValue);

						Trace.TraceInformation($"{seats.Count()} seats");
						foreach (var seat in seats)
						{
							Trace.TraceInformation($"Seat: {seat.Key} Items:{seat.Count()}");

							#region Seat

							{
								printable
									.AddNewLine()
									.AddText($"{seat.Key}", font: font_seatSeparator)
									.AddNewLine()
									.AddRectangle()
									;
							}

							#endregion

							Trace.TraceInformation($"Grouping by separator {orders.Count} orders");
							var separators = seat.GroupBy(x => x.Line.Separator.Description)
												 .OrderBy(x => x.First().Line.Separator.ImpresionOrden);

							Trace.TraceInformation($"{separators.Count()} separators");
							foreach (var separator in separators)
							{
								Trace.TraceInformation($"Separator Name:{separator.Key} Items:{separator.Count()}");

								#region Separator

								{
									printable
										.AddNewLine()
										.AddRectangle()
										.AddText($"{separator.Key}")
										.AddText($"({separator.Count()})")
										.AddRectangle()
										;
								}

								#endregion

								#region Separator Orders

								foreach (var order in separator)
								{
									#region Order Manual Separator

									//if (order.Line.HasManualSeparator)
									//{
									//    var line = GetRectangle(Brushes.Black);
									//    document.AddBlock(GetBlockUiContainer(line, margin: new Thickness(2, 2, 2, 0)));
									//}

									#endregion

									#region Order Quantity + Product

									Trace.TraceInformation($"Print separator item Qty:{order.Quantity} Product:{order.Line.ProductDescription}");
									printable
										.AddNewLine()
										.AddText($"{order.Quantity}  {order.Line.ProductDescription}", font: font_line)
										;

									#endregion

									#region Order Notes

									if (!string.IsNullOrEmpty(order.Line.Notes))
									{
										// var tbNotes = GetTextBlock(order.Line.Notes, wrapping: TextWrapping.Wrap);
										printable
											.AddNewLine()
											.AddText(order.Line.Notes, font: font_note)
											;
									}

									#endregion

									#region Order Table Menu

									if (order.Line.TableLines is { Count: > 0 })
									{
										var tables = order.Line.TableLines
											.Where(x => KitchenDoc.OrderIds[order.Line.Id].Contains(x.Id))
											.GroupBy(x => x.Separator?.Description)
											.OrderBy(x => x.First().Separator?.ImpresionOrden ?? -1);

										foreach (var table in tables)
										{
											#region Table Separator

											printable
												.AddNewLine()
												.AddRectangle()
												.AddText($"{table.Key}  ({table.Count()})")
												.AddRectangle()
												;
											#endregion

											#region Table Products Separator

											foreach (var sepTab in table)
											{
												printable
													.AddNewLine()
													.AddText(sepTab.ProductDescription, font: font_line)
													;

												if (sepTab.Preparations != null && sepTab.Preparations.Count > 0)
												{
													foreach (var p in sepTab.Preparations)
													{
														printable
															.AddNewLine()
															.AddText($"    - {p.Description}")
															;
													}
												}

												#region Observations

												if (string.IsNullOrEmpty(sepTab.Notes)) continue;
												printable
													.AddNewLine()
													.AddText($"    - {sepTab.Notes}", font: font_note)
													;

												#endregion
											}

											#endregion
										}
									}

									#endregion

									#region Order Preparations

									if (order.Line.Preparations != null && order.Line.Preparations.Count > 0)
									{
										foreach (var p in order.Line.Preparations)
										{
											printable
												.AddNewLine()
												.AddText($"    - {p.Description}")
												;
										}

									}

									#endregion
								}

								#endregion
							}
						}
					}
					break;
				case TicketProductGrouping.SeparatorsSeats:
					{
						Trace.TraceInformation($"Grouping by separator {orders.Count} orders");
						var separators = orders.GroupBy(x => x.Line.Separator.Description)
											   .OrderBy(x => x.First().Line.Separator.ImpresionOrden);

						Trace.TraceInformation($"{separators.Count()} separators");
						foreach (var separator in separators)
						{
							Trace.TraceInformation($"Separator Name:{separator.Key} Items:{separator.Count()}");

							#region Separator

							{
								printable
									.AddNewLine()
									.AddText($"{separator.Key}", font: font_seatSeparator)
									.AddRectangle(height: new PrntLength(1, LengthType.Units))
									;
							}

							#endregion

							Trace.TraceInformation($"Grouping by seats {orders.Count} orders");
							var seats = separator.GroupBy(x => x.Line.PaxName)
												 .OrderBy(x => x.First().Line.PaxNumber ?? short.MaxValue);

							Trace.TraceInformation($"{seats.Count()} seats");
							foreach (var seat in seats)
							{
								Trace.TraceInformation($"Seat: {seat.Key} Items:{seat.Count()}");

								#region Seat

								{
									printable
										.AddNewLine()
										.AddRectangle()
										.AddText($"{seat.Key}", font: font_seatSeparator)
										;
								}

								#endregion

								#region Seat Orders

								foreach (var order in seat)
								{
									#region Order Quantity + Product

									Trace.TraceInformation($"Print separator item Qty:{order.Quantity} Product:{order.Line.ProductDescription}");
									printable
										.AddNewLine()
										.AddText($"{order.Quantity}  {order.Line.ProductDescription}", font: font_line)
										;

									#endregion

									#region Order Notes

									if (!string.IsNullOrEmpty(order.Line.Notes))
									{
										printable
											.AddNewLine()
											.AddText(order.Line.Notes, font: font_note)
											;
									}

									#endregion

									#region Order Table Menu

									if (order.Line.TableLines is { Count: > 0 })
									{
										var tables = order.Line.TableLines
											.Where(x => KitchenDoc.OrderIds[order.Line.Id].Contains(x.Id))
											.GroupBy(x => x.Separator?.Description)
											.OrderBy(x => x.First().Separator?.ImpresionOrden ?? -1);

										foreach (var table in tables)
										{
											#region Table Separator

											printable
												.AddNewLine()
												.AddRectangle()
												.AddText($"{table.Key}  ({table.Count()})")
												.AddRectangle()
												;

											#endregion

											#region Table Products Separator

											foreach (var tab in table)
											{
												#region Product

												printable
													.AddNewLine()
													.AddFullText(tab.ProductDescription, horizontalAlign: PrintableHorizontalAlign.Center, font: font_i15)
													;

												#endregion

												#region Product Preparations

												if (tab.Preparations != null && tab.Preparations.Count > 0)
												{
													foreach (var p in tab.Preparations)
													{
														printable
															.AddNewLine()
															.AddText($"    - {p.Description}", font: font_note)
															;
													}

												}

												#endregion

												#region Observations

												if (string.IsNullOrEmpty(tab.Notes)) continue;
												printable
													.AddNewLine()
													.AddText($"    - {tab.Notes}", font: font_note)
													;

												#endregion
											}

											#endregion
										}
									}

									#endregion

									#region Order Preparations

									if (order.Line.Preparations != null && order.Line.Preparations.Count > 0)
									{
										foreach (var p in order.Line.Preparations)
										{
											printable
												.AddNewLine()
												.AddText($"    - {p.Description}")
												;

										}

									}

									#endregion
								}

								#endregion
							}
						}
					}
					break;
			}

			#endregion

			#region Footer

			{
				printable
					.AddNewLine()
					.AddRectangle()
					.AddText($" {now.ToShortDateString()}  {now.ToShortTimeString()} ")
					.AddRectangle()
					;

			}

			#endregion

			return printable;
		}
	}
}
