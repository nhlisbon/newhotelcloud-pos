﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Controls;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Core;
using NewHotel.Contracts;
using NewHotel.Contracts.DataProvider;
using NewHotel.DataAnnotations;
using NewHotel.Pos.Localization;
using NewHotel.Pos.Printer.Interfaces;
using NewHotel.Pos.PrinterDocument.Common;
using NewHotel.Pos.PrinterDocument.Simple;
using NewHotel.Pos.PrinterDocument.Simple.Visual;
using System.Text.RegularExpressions;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for PrintDocControl.xaml
    /// </summary>
    public partial class DocViewControl : UserControl, IPreparer<Visual>
    {
        #region Variables

        private readonly Dictionary<TextBlock, CultureInfo> _textBlocksTranslation = new Dictionary<TextBlock, CultureInfo>();

        public static string RoomNumber => "RoomNº".Translate();
        public static string ClientName => "Name".Translate();
        public static string Signature => "Signature".Translate();
        
        #endregion
        #region Constructor

        public DocViewControl(TicketTitles title, IPrintableDoc doc, string country, string fiscalDescription = "")
        {
            InitializeComponent();
            InitializeDependencys();

            Title = title;

            var data = Utils.GetTitleData(Title, doc, country);
            TitleDescription = (fiscalDescription != "") ? fiscalDescription : data.TitleDescription;
            ShowCopy = data.IsCopy;
            IsReceipt = data.IsReceipt;
            IsTicket = data.IsTicket;
            IsBallot = data.IsBallot;
            IsInvoice = data.IsInvoice;
            IsCreditNote = data.IsCreditNote;
            ShowPayments = !data.IsReceipt;
        }

        #endregion
        #region Properties

        #region Title

        public TicketTitles Title
        {
            get => (TicketTitles)GetValue(TitleProperty);
            set => SetValue(TitleProperty, value);
        }

        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register(nameof(Title), typeof(TicketTitles), typeof(DocViewControl));

        #endregion
        #region TitleDescription

        public string TitleDescription
        {
            get => (string)GetValue(TitleDescriptionProperty);
            set => SetValue(TitleDescriptionProperty, value);
        }

        public static readonly DependencyProperty TitleDescriptionProperty =
            DependencyProperty.Register(nameof(TitleDescription), typeof(string), typeof(DocViewControl));

        #endregion
        #region LookupTable

        public IPrinteableLookupTable? LookupTable
        {
            get => (IPrinteableLookupTable)GetValue(LookupTableProperty);
            set => SetValue(LookupTableProperty, value);
        }

        public static readonly DependencyProperty LookupTableProperty =
            DependencyProperty.Register(nameof(LookupTable), typeof(IPrinteableLookupTable), typeof(DocViewControl));

        #endregion
        #region ShowCopy

        public bool ShowCopy
        {
            get => (bool)GetValue(ShowCopyProperty);
            set => SetValue(ShowCopyProperty, value);
        }

        public static readonly DependencyProperty ShowCopyProperty =
            DependencyProperty.Register(nameof(ShowCopy), typeof(bool), typeof(DocViewControl));

        #endregion
        #region ShowPayments

        public bool ShowPayments
        {
            get => (bool)GetValue(ShowPaymentsProperty);
            set => SetValue(ShowPaymentsProperty, value);
        }

        public static readonly DependencyProperty ShowPaymentsProperty =
            DependencyProperty.Register(nameof(ShowPayments), typeof(bool), typeof(DocViewControl));

        #endregion
        #region ShowFiscalNumber

        public bool ShowFiscalNumber
        {
            get => (bool)GetValue(ShowFiscalNumberProperty);
            set => SetValue(ShowFiscalNumberProperty, value);
        }

        public static readonly DependencyProperty ShowFiscalNumberProperty =
            DependencyProperty.Register(nameof(ShowFiscalNumber), typeof(bool), typeof(DocViewControl), new PropertyMetadata(true));

        #endregion
        #region ShowBuyerInfo


        public bool ShowBuyerInfo
        {
            get => (bool)GetValue(ShowBuyerInfoProperty);
            set => SetValue(ShowBuyerInfoProperty, value);
        }

        public static readonly DependencyProperty ShowBuyerInfoProperty =
            DependencyProperty.Register(nameof(ShowBuyerInfo), typeof(bool), typeof(DocViewControl));

        #endregion
        #region IsInvoice

        public bool IsInvoice
        {
            get => (bool)GetValue(IsInvoiceProperty);
            set => SetValue(IsInvoiceProperty, value);
        }

        public static readonly DependencyProperty IsInvoiceProperty =
            DependencyProperty.Register(nameof(IsInvoice), typeof(bool), typeof(DocViewControl));

        #endregion
        #region IsCreditNote

        public bool IsCreditNote
        {
            get => (bool)GetValue(IsCreditNoteProperty);
            set => SetValue(IsCreditNoteProperty, value);
        }

        public static readonly DependencyProperty IsCreditNoteProperty =
            DependencyProperty.Register(nameof(IsCreditNote), typeof(bool), typeof(DocViewControl));

        #endregion
        #region IsBallot

        public bool IsBallot
        {
            get => (bool)GetValue(IsBallotProperty);
            set => SetValue(IsBallotProperty, value);
        }

        public static readonly DependencyProperty IsBallotProperty =
            DependencyProperty.Register(nameof(IsBallot), typeof(bool), typeof(DocViewControl));

        #endregion
        #region IsReceipt

        public bool IsReceipt
        {
            get => (bool)GetValue(IsReceiptProperty);
            set => SetValue(IsReceiptProperty, value);
        }
        public static readonly DependencyProperty IsReceiptProperty =
            DependencyProperty.Register(nameof(IsReceipt), typeof(bool), typeof(DocViewControl));

        #endregion
        #region IsTicket

        public bool IsTicket
        {
            get => (bool)GetValue(IsTicketProperty);
            set => SetValue(IsTicketProperty, value);
        }

        public static readonly DependencyProperty IsTicketProperty =
            DependencyProperty.Register(nameof(IsTicket), typeof(bool), typeof(DocViewControl));

        #endregion
        #region ShowLookupTableSignature

        public bool ShowLookupTableSignature
        {
            get => (bool)GetValue(ShowLookupTableSignatureProperty);
            set => SetValue(ShowLookupTableSignatureProperty, value);
        }

        public static readonly DependencyProperty ShowLookupTableSignatureProperty =
            DependencyProperty.Register(nameof(ShowLookupTableSignature), typeof(bool), typeof(DocViewControl));

        #endregion
        #region ShowTicketSignature

        public bool ShowTicketSignature
        {
            get => (bool)GetValue(ShowTicketSignatureProperty);
            set => SetValue(ShowTicketSignatureProperty, value);
        }

        public static readonly DependencyProperty ShowTicketSignatureProperty =
            DependencyProperty.Register(nameof(ShowTicketSignature), typeof(bool), typeof(DocViewControl));

        #endregion
        #region ReceiptValidationCode + ReceipQrCode

        public string ReceiptValidationCode
        {
            get => (string)GetValue(ReceiptValidationCodeProperty);
            set => SetValue(ReceiptValidationCodeProperty, value);
        }

        public static readonly DependencyProperty ReceiptValidationCodeProperty =
            DependencyProperty.Register(nameof(ReceiptValidationCode), typeof(string), typeof(DocViewControl));

        public Blob? ReceiptQrCode
        {
            get => (Blob?)GetValue(ReceiptQrCodeProperty);
            set => SetValue(ReceiptQrCodeProperty, value);
        }

        public static readonly DependencyProperty ReceiptQrCodeProperty =
            DependencyProperty.Register(nameof(ReceiptQrCode), typeof(Blob?), typeof(DocViewControl));

        #endregion
        #region TicketValidationCode + TicketQrCode

        public string TicketValidationCode
        {
            get => (string)GetValue(TicketValidationCodeProperty);
            set => SetValue(TicketValidationCodeProperty, value);
        }

        public static readonly DependencyProperty TicketValidationCodeProperty =
            DependencyProperty.Register(nameof(TicketValidationCode), typeof(string), typeof(DocViewControl));

        public Blob? TicketQrCode
        {
            get => (Blob?)GetValue(TicketQrCodeProperty);
            set => SetValue(TicketQrCodeProperty, value);
        }

        public static readonly DependencyProperty TicketQrCodeProperty =
            DependencyProperty.Register(nameof(TicketQrCode), typeof(Blob?), typeof(DocViewControl));

        #endregion
        #region InvoiceValidationCode + InvoiceQrCode

        public string InvoiceValidationCode
        {
            get => (string)GetValue(InvoiceValidationCodeProperty);
            set => SetValue(InvoiceValidationCodeProperty, value);
        }

        public static readonly DependencyProperty InvoiceValidationCodeProperty =
            DependencyProperty.Register(nameof(InvoiceValidationCode), typeof(string), typeof(DocViewControl));

        public Blob? InvoiceQrCode
        {
            get => (Blob?)GetValue(InvoiceQrCodeProperty);
            set => SetValue(InvoiceQrCodeProperty, value);
        }

        public static readonly DependencyProperty InvoiceQrCodeProperty =
            DependencyProperty.Register(nameof(InvoiceQrCode), typeof(Blob?), typeof(DocViewControl));

        #endregion
        #region CreditNoteValidationCode + CreditNoteQrCode

        public string CreditNoteValidationCode
        {
            get => (string)GetValue(CreditNoteValidationCodeProperty);
            set => SetValue(CreditNoteValidationCodeProperty, value);
        }

        public static readonly DependencyProperty CreditNoteValidationCodeProperty =
            DependencyProperty.Register(nameof(CreditNoteValidationCode), typeof(string), typeof(DocViewControl));

        public Blob? CreditNoteQrCode
        {
            get => (Blob?)GetValue(CreditNoteQrCodeProperty);
            set => SetValue(CreditNoteQrCodeProperty, value);
        }

        public static readonly DependencyProperty CreditNoteQrCodeProperty =
            DependencyProperty.Register(nameof(CreditNoteQrCode), typeof(Blob?), typeof(DocViewControl));

        #endregion
        #region ShowPaymentOptionsAndSignature

        public bool ShowPaymentOptionsAndSignature
        {
            get => (bool)GetValue(ShowPaymentOptionsAndSignatureProperty);
            set => SetValue(ShowPaymentOptionsAndSignatureProperty, value);
        }

        public static readonly DependencyProperty ShowPaymentOptionsAndSignatureProperty =
            DependencyProperty.Register(nameof(ShowPaymentOptionsAndSignature), typeof(bool), typeof(DocViewControl));

        #endregion
        #region ShowDiscounts

        public bool ShowDiscounts
        {
            get => (bool)GetValue(ShowDiscountsProperty);
            set => SetValue(ShowDiscountsProperty, value);
        }

        public static readonly DependencyProperty ShowDiscountsProperty =
            DependencyProperty.Register(nameof(ShowDiscounts), typeof(bool), typeof(DocViewControl));

        #endregion
        #region ShowRecharges

        public bool ShowRecharges
        {
            get => (bool)GetValue(ShowRechargesProperty);
            set => SetValue(ShowRechargesProperty, value);
        }

        public static readonly DependencyProperty ShowRechargesProperty =
            DependencyProperty.Register(nameof(ShowRecharges), typeof(bool), typeof(DocViewControl));

        #endregion
        #region PrintSubtotal

        public bool PrintSubTotal
        {
            get => (bool)GetValue(PrintSubTotalProperty);
            set => SetValue(PrintSubTotalProperty, value);
        }

        public static readonly DependencyProperty PrintSubTotalProperty =
            DependencyProperty.Register(nameof(PrintSubTotal), typeof(bool), typeof(DocViewControl));

        #endregion
        #region SubTotalToPrint

        /// <summary>
        /// Get/Set the kind of subtotal to print, net o brute
        /// </summary>
        public KindSubTotal SubTotalToPrint
        {
            get => (KindSubTotal)GetValue(SubTotalToPrintProperty);
            set => SetValue(SubTotalToPrintProperty, value);
        }

        public static readonly DependencyProperty SubTotalToPrintProperty =
            DependencyProperty.Register(nameof(SubTotalToPrint), typeof(KindSubTotal), typeof(DocViewControl));

        #endregion
        #region ShowTips

        public bool ShowTips
        {
            get => (bool)GetValue(ShowTipsProperty);
            set => SetValue(ShowTipsProperty, value);
        }

        public static readonly DependencyProperty ShowTipsProperty =
            DependencyProperty.Register(nameof(ShowTips), typeof(bool), typeof(DocViewControl));


        #endregion
        #region PrintReceiptQrCode

        public bool PrintReceiptQrCode
        {
            get => (bool)GetValue(PrintReceiptQrCodeProperty);
            set => SetValue(PrintReceiptQrCodeProperty, value);
        }

        public static readonly DependencyProperty PrintReceiptQrCodeProperty =
            DependencyProperty.Register(nameof(PrintReceiptQrCode), typeof(bool), typeof(DocViewControl));

        #endregion
        #region PrintTicketQrCode

        public bool PrintTicketQrCode
        {
            get => (bool)GetValue(PrintTicketQrCodeProperty);
            set => SetValue(PrintTicketQrCodeProperty, value);
        }

        public static readonly DependencyProperty PrintTicketQrCodeProperty =
            DependencyProperty.Register(nameof(PrintTicketQrCode), typeof(bool), typeof(DocViewControl));

        #endregion
        #region PrintInvoiceQrCode

        public bool PrintInvoiceQrCode
        {
            get => (bool)GetValue(PrintInvoiceQrCodeProperty);
            set => SetValue(PrintInvoiceQrCodeProperty, value);
        }

        public static readonly DependencyProperty PrintInvoiceQrCodeProperty =
            DependencyProperty.Register(nameof(PrintInvoiceQrCode), typeof(bool), typeof(DocViewControl));

        #endregion
        #region PrintCreditNoteQrCode

        public bool PrintCreditNoteQrCode
        {
            get => (bool)GetValue(PrintCreditNoteQrCodeProperty);
            set => SetValue(PrintCreditNoteQrCodeProperty, value);
        }

        public static readonly DependencyProperty PrintCreditNoteQrCodeProperty =
            DependencyProperty.Register(nameof(PrintCreditNoteQrCode), typeof(bool), typeof(DocViewControl));

        #endregion
        #region PrintNormalTax

        public bool PrintNormalTax
        {
            get => (bool)GetValue(PrintNormalTaxProperty);
            set => SetValue(PrintNormalTaxProperty, value);
        }

        public static readonly DependencyProperty PrintNormalTaxProperty =
            DependencyProperty.Register(nameof(PrintNormalTax), typeof(bool), typeof(DocViewControl));

        #endregion
        #region PrintTaxDetailsPerLine

        public bool PrintTaxDetailsPerLine
        {
            get => (bool)GetValue(PrintTaxDetailsPerLineProperty);
            set => SetValue(PrintTaxDetailsPerLineProperty, value);
        }

        public static readonly DependencyProperty PrintTaxDetailsPerLineProperty =
            DependencyProperty.Register(nameof(PrintTaxDetailsPerLine), typeof(bool), typeof(DocViewControl));

        #endregion
        #region PrintCloseDateTime

        public bool PrintCloseDateTime
        {
            get => (bool)GetValue(PrintCloseDateTimeProperty);
            set => SetValue(PrintCloseDateTimeProperty, value);
        }

        public static readonly DependencyProperty PrintCloseDateTimeProperty =
            DependencyProperty.Register(nameof(PrintCloseDateTime), typeof(bool), typeof(DocViewControl));

        #endregion
        #region PrintCurrentTime

        public bool PrintCurrentTime
        {
            get => (bool)GetValue(PrintCurrentTimeProperty);
            set => SetValue(PrintCurrentTimeProperty, value);
        }

        public static readonly DependencyProperty PrintCurrentTimeProperty =
            DependencyProperty.Register(nameof(PrintCurrentTime), typeof(bool), typeof(DocViewControl));

        #endregion
        #region PrintPaymentOptions

        public bool PrintPaymentOptions
        {
            get => (bool)GetValue(PrintPaymentOptionsProperty);
            set => SetValue(PrintPaymentOptionsProperty, value);
        }

        public static readonly DependencyProperty PrintPaymentOptionsProperty =
            DependencyProperty.Register(nameof(PrintPaymentOptions), typeof(bool), typeof(DocViewControl));

        #endregion
        #region PrintPaymentDetails

        public bool PrintPaymentDetails
        {
            get => (bool)GetValue(PrintPaymentDetailsProperty);
            set => SetValue(PrintPaymentDetailsProperty, value);
        }

        public static readonly DependencyProperty PrintPaymentDetailsProperty =
            DependencyProperty.Register(nameof(PrintPaymentDetails), typeof(bool), typeof(DocViewControl));


        #endregion
        #region PrintLogo

        public bool PrintLogo
        {
            get => (bool)GetValue(PrintLogoProperty);
            set => SetValue(PrintLogoProperty, value);
        }

        public static readonly DependencyProperty PrintLogoProperty =
            DependencyProperty.Register(nameof(PrintLogo), typeof(bool), typeof(DocViewControl));


        #endregion
        #region ShowAnul

        public bool ShowAnul
        {
            get => (bool)GetValue(ShowAnulProperty);
            set => SetValue(ShowAnulProperty, value);
        }

        public static readonly DependencyProperty ShowAnulProperty =
            DependencyProperty.Register(nameof(ShowAnul), typeof(bool), typeof(DocViewControl));

        #endregion
        #region ShowMessage1

        public bool ShowMessage1
        {
            get => (bool)GetValue(ShowMessage1Property);
            set => SetValue(ShowMessage1Property, value);
        }

        public static readonly DependencyProperty ShowMessage1Property =
            DependencyProperty.Register(nameof(ShowMessage1), typeof(bool), typeof(DocViewControl));

        #endregion
        #region ShowMessage2

        public bool ShowMessage2
        {
            get => (bool)GetValue(ShowMessage2Property);
            set => SetValue(ShowMessage2Property, value);
        }

        public static readonly DependencyProperty ShowMessage2Property =
            DependencyProperty.Register(nameof(ShowMessage2), typeof(bool), typeof(DocViewControl));

        #endregion
        #region ShowCommentsTicket

        public bool ShowCommentsTicket
        {
            get => (bool)GetValue(ShowCommentsTicketProperty);
            set => SetValue(ShowCommentsTicketProperty, value);
        }

        public static readonly DependencyProperty ShowCommentsTicketProperty =
            DependencyProperty.Register(nameof(ShowCommentsTicket), typeof(bool), typeof(DocViewControl));


        #endregion
        #region ShowTipSuggestions

        public bool ShowTipSuggestions
        {
            get => (bool)GetValue(ShowTipSuggestionsProperty);
            set => SetValue(ShowTipSuggestionsProperty, value);
        }

        public static readonly DependencyProperty ShowTipSuggestionsProperty =
            DependencyProperty.Register(nameof(ShowTipSuggestions), typeof(bool), typeof(DocViewControl));

        #endregion
        #region ShowInvoiceSignature

        public bool ShowInvoiceSignature
        {
            get => (bool)GetValue(ShowInvoiceSignatureProperty);
            set => SetValue(ShowInvoiceSignatureProperty, value);
        }

        public static readonly DependencyProperty ShowInvoiceSignatureProperty =
            DependencyProperty.Register(nameof(ShowInvoiceSignature), typeof(bool), typeof(DocViewControl));

        #endregion
        #region ShowCreditNoteSignature

        public bool ShowCreditNoteSignature
        {
            get => (bool)GetValue(ShowCreditNoteSignatureProperty);
            set => SetValue(ShowCreditNoteSignatureProperty, value);
        }

        public static readonly DependencyProperty ShowCreditNoteSignatureProperty =
            DependencyProperty.Register(nameof(ShowCreditNoteSignature), typeof(bool), typeof(DocViewControl));

        #endregion
        #region ShowGuestSignature

        public bool ShowGuestSignature
        {
            get => (bool)GetValue(ShowGuestSignatureProperty);
            set => SetValue(ShowGuestSignatureProperty, value);
        }

        public static readonly DependencyProperty ShowGuestSignatureProperty =
            DependencyProperty.Register(nameof(ShowGuestSignature), typeof(bool), typeof(DocViewControl));


        #endregion
        #region ShowDigitalSignature

        public bool ShowDigitalSignature
        {
            get => (bool)GetValue(ShowDigitalSignatureProperty);
            set => SetValue(ShowDigitalSignatureProperty, value);
        }

        public static readonly DependencyProperty ShowDigitalSignatureProperty =
            DependencyProperty.Register(nameof(ShowDigitalSignature), typeof(bool), typeof(DocViewControl));


        #endregion
        #region Ticket

        public IPrintableDoc Ticket
        {
            get => (IPrintableDoc)GetValue(TicketProperty);
            set => SetValue(TicketProperty, value);
        }

        public static readonly DependencyProperty TicketProperty =
            DependencyProperty.Register(nameof(Ticket), typeof(IPrintableDoc), typeof(DocViewControl));


        #endregion
        #region Records

        public ObservableCollection<OrderLite> Orders
        {
            get => (ObservableCollection<OrderLite>)GetValue(OrdersProperty);
            set => SetValue(OrdersProperty, value);
        }

        public static readonly DependencyProperty OrdersProperty =
            DependencyProperty.Register(nameof(Orders), typeof(ObservableCollection<OrderLite>), typeof(DocViewControl));

        #endregion
        #region Taxes

        public ObservableCollection<IPrinteableMovementTaxDetail> Taxes
        {
            get => (ObservableCollection<IPrinteableMovementTaxDetail>)GetValue(TaxesProperty);
            set => SetValue(TaxesProperty, value);
        }

        public static readonly DependencyProperty TaxesProperty =
            DependencyProperty.Register(nameof(Taxes), typeof(ObservableCollection<IPrinteableMovementTaxDetail>), typeof(DocViewControl));


        #endregion
        #region UserName

        public string UserName
        {
            get => (string)GetValue(UserNameProperty);
            set => SetValue(UserNameProperty, value);
        }

        public static readonly DependencyProperty UserNameProperty = DependencyProperty.Register(nameof(UserName), typeof(string), typeof(DocViewControl));

        #endregion
        #region Series

        #region LookupTableSerie

        public string LookupTableSerie
        {
            get => (string)GetValue(LookupTableSerieProperty);
            set => SetValue(LookupTableSerieProperty, value);
        }
        public static readonly DependencyProperty LookupTableSerieProperty = DependencyProperty.Register(nameof(LookupTableSerie), typeof(string), typeof(DocViewControl));


        #endregion
        #region TicketOpeningNumber

        public string TicketOpeningNumber
        {
            get => (string)GetValue(TicketOpeningNumberProperty);
            set => SetValue(TicketOpeningNumberProperty, value);
        }

        public static readonly DependencyProperty TicketOpeningNumberProperty = DependencyProperty.Register(nameof(TicketOpeningNumber), typeof(string), typeof(DocViewControl));

        #endregion
        #region TicketSerie

        public string TicketSerie
        {
            get => (string)GetValue(TicketSerieProperty);
            set => SetValue(TicketSerieProperty, value);
        }
        public static readonly DependencyProperty TicketSerieProperty = DependencyProperty.Register(nameof(TicketSerie), typeof(string), typeof(DocViewControl));


        #endregion
        #region FiscalDocSerie

        public string FiscalDocSerie
        {
            get => (string)GetValue(FiscalDocSerieProperty);
            set => SetValue(FiscalDocSerieProperty, value);
        }

        public static readonly DependencyProperty FiscalDocSerieProperty = DependencyProperty.Register(nameof(FiscalDocSerie), typeof(string), typeof(DocViewControl));

        #endregion
        #region CreditNoteSerie

        public string CreditNoteSerie
        {
            get => (string)GetValue(CreditNoteSerieProperty);
            set => SetValue(CreditNoteSerieProperty, value);
        }

        public static readonly DependencyProperty CreditNoteSerieProperty = DependencyProperty.Register(nameof(CreditNoteSerie), typeof(string), typeof(DocViewControl));

        #endregion
        #region TipSuggestions

        public ObservableCollection<TipSuggestionLite> TipSuggestions
        {
            get => (ObservableCollection<TipSuggestionLite>)GetValue(TipSuggestionsProperty);
            set => SetValue(TipSuggestionsProperty, value);
        }

        public static readonly DependencyProperty TipSuggestionsProperty = DependencyProperty.Register(nameof(TipSuggestions), typeof(ObservableCollection<TipSuggestionLite>), typeof(DocViewControl));

        #endregion
        #endregion
        #region InvoiceSignature

        public string InvoiceSignature
        {
            get => (string)GetValue(InvoiceSignatureProperty);
            set => SetValue(InvoiceSignatureProperty, value);
        }

        public static readonly DependencyProperty InvoiceSignatureProperty =
            DependencyProperty.Register(nameof(InvoiceSignature), typeof(string), typeof(DocViewControl));

        #endregion
        #region CreditNoteSignature

        public string CreditNoteSignature
        {
            get => (string)GetValue(CreditNoteSignatureProperty);
            set => SetValue(CreditNoteSignatureProperty, value);
        }

        public static readonly DependencyProperty CreditNoteSignatureProperty =
            DependencyProperty.Register(nameof(CreditNoteSignature), typeof(string), typeof(DocViewControl));

        #endregion
        #region SignedLabel

        public string SignedLabel
        {
            get => (string)GetValue(SignedLabelProperty);
            set => SetValue(SignedLabelProperty, value);
        }

        public static readonly DependencyProperty SignedLabelProperty =
            DependencyProperty.Register(nameof(SignedLabel), typeof(string), typeof(DocViewControl));

        #endregion
        #region TicketSignature

        public string TicketSignature
        {
            get => (string)GetValue(TicketSignatureProperty);
            set => SetValue(TicketSignatureProperty, value);
        }

        public static readonly DependencyProperty TicketSignatureProperty =
            DependencyProperty.Register(nameof(TicketSignature), typeof(string), typeof(DocViewControl));

        #endregion

        #region ShowMessageAllowRoomCredit

        public bool ShowAllowRoomCredit
        {
            get => (bool)GetValue(ShowAllowRoomCreditProperty);
            set => SetValue(ShowAllowRoomCreditProperty, value);
        }

        public static readonly DependencyProperty ShowAllowRoomCreditProperty =
            DependencyProperty.Register(nameof(ShowAllowRoomCredit), typeof(bool), typeof(DocViewControl));


        #endregion

        #region Cufe
        public string Cufe
        {
            get => (string)GetValue(CufeProperty);
            set => SetValue(CufeProperty, value);
        }

        public static readonly DependencyProperty CufeProperty =
            DependencyProperty.Register(nameof(Cufe), typeof(string), typeof(DocViewControl));
        #endregion

        #region Fiscal QR
        public Blob? FiscalDocQRCode
        {
            get => (Blob?)GetValue(FiscalDocQRCodeProperty);
            set => SetValue(FiscalDocQRCodeProperty, value);
        }

        public static readonly DependencyProperty FiscalDocQRCodeProperty =
            DependencyProperty.Register(nameof(FiscalDocQRCode), typeof(Blob?), typeof(DocViewControl));
        #endregion
        
        #region ShowSignatureOptions

        public bool ShowSignatureOptions
        {
            get => (bool)GetValue(ShowSignatureOptionsProperty);
            set => SetValue(ShowSignatureOptionsProperty, value);
        }

        public static readonly DependencyProperty ShowSignatureOptionsProperty =
            DependencyProperty.Register(nameof(ShowSignatureOptions), typeof(bool), typeof(DocViewControl));
        
        #endregion
        
        #region AlternateRoomNumber

        public string AlternateRoomNumber
        {
            get => (string)GetValue(AlternateRoomNumberProperty);
            set => SetValue(AlternateRoomNumberProperty, value);
        }

        public static readonly DependencyProperty AlternateRoomNumberProperty =
            DependencyProperty.Register(nameof(AlternateRoomNumber), typeof(string), typeof(DocViewControl));

        #endregion
        
        #region AlternateName

        public string AlternateName
        {
            get => (string)GetValue(AlternateNameProperty);
            set => SetValue(AlternateNameProperty, value);
        }

        public static readonly DependencyProperty AlternateNameProperty =
            DependencyProperty.Register(nameof(AlternateName), typeof(string), typeof(DocViewControl));

        #endregion
        
        #region AlternateSignature

        public string AlternateSignature
        {
            get => (string)GetValue(AlternateSignatureProperty);
            set => SetValue(AlternateSignatureProperty, value);
        }

        public static readonly DependencyProperty AlternateSignatureProperty =
            DependencyProperty.Register(nameof(AlternateSignature), typeof(string), typeof(DocViewControl));

        #endregion
        
        #endregion
        
        public string Pension
        {
            get => (string)GetValue(AlternatePensionProperty);
            set => SetValue(AlternatePensionProperty, value);
        }

        public static readonly DependencyProperty AlternatePensionProperty =
            DependencyProperty.Register(nameof(Pension), typeof(string), typeof(DocViewControl));

        public string RoomAndPension
        {
            get => (string)GetValue(AlternateRoomAndPensionProperty);
            set => SetValue(AlternateRoomAndPensionProperty, value);
        }

        public static readonly DependencyProperty AlternateRoomAndPensionProperty =
            DependencyProperty.Register(nameof(RoomAndPension), typeof(string), typeof(DocViewControl));

        #region Methods

        public Visual PreparePrint(PrinterContext context)
        {
            LogPrintOperation(context);
            return Scroll.PreparePrintScroll(context);
        }

        public void Initialize(IPrintableDoc doc, IPrintDocSetting settings)
        {
            #region General
            
            DataContext = settings;
            Ticket = doc;
            UserName = Title == TicketTitles.CreditNote ? doc.CreditNoteUserDescription : doc.UserDescription;
            ShowBuyerInfo = Title is TicketTitles.Ballot or TicketTitles.Invoice or TicketTitles.CreditNote;
            if (Title == TicketTitles.Receipt)
                LookupTable = doc.LookupTables.First(x => x.Number == doc.LookupTables.Max(y => y.Number));

            FiscalizationSignature(settings);
            FiscalizationFormat(settings);

            Orders = Utils.OrderLites(doc, settings);

            InitializeSeparators(settings);

            PrintSubTotal = settings.PrintSubtotal;
            SubTotalToPrint = settings.SubTotalToPrint ?? KindSubTotal.Net;

            PrintCloseDateTime = settings.PrintCloseDateTime && doc is { CloseDate: not null, CloseTime: not null };
            ShowDiscounts = !settings.SuppressDiscountWhenCero || doc.GeneralDiscount > decimal.Zero;
            ShowRecharges = !settings.SuppressRechargeWhenCero || doc.Recharge > decimal.Zero;
            ShowTips = settings.PrintTips && doc.TipAmount > decimal.Zero;
            ShowTipSuggestions = settings.TipSuggestion;
            TipSuggestions = Utils.ProposalSuggestions(settings, doc);

            PrintTaxDetailsPerLine = settings.PrintTaxDetailsPerLine;
            PrintNormalTax = !(IsReceipt && Ticket.TaxIncluded) && !settings.PrintTaxDetails;
            PrintLogo = settings is { PrintLogo: true, LogoImage: not null };
            ShowAnul = Ticket.IsAnul && string.IsNullOrEmpty(doc.CreditNoteSerie);
            ShowMessage1 = !string.IsNullOrEmpty(settings.PrintMessage1);
            ShowMessage2 = !string.IsNullOrEmpty(settings.PrintMessage2);
            ShowCommentsTicket = settings.ShowCommentsTicket && !string.IsNullOrEmpty(doc.Observations);

            if (settings.PrintTaxDetails)
            {
                var taxes = Utils.TaxGroups(doc);
                Taxes = new ObservableCollection<IPrinteableMovementTaxDetail>(taxes);
            }

            ShowPayments = doc is { HasMealPlanPayment: false, HasHouseUsePayment: false, HasCreditRoomPayment: false };
            ShowPaymentOptionsAndSignature = settings.PrintCustomerSignature || IsReceipt &&
                (settings.PrintCustomerSignature || settings.PrintPaymentOptions);
            PrintPaymentOptions = IsReceipt && settings.PrintPaymentOptions;
            PrintPaymentDetails = !IsReceipt && settings.PrintPaymentDetails;
            ShowInvoiceSignature = IsInvoice && !string.IsNullOrEmpty(doc.FiscalDocSignature);
            ShowCreditNoteSignature = IsCreditNote && !string.IsNullOrEmpty(doc.CreditNoteSignature);
            ShowLookupTableSignature = IsReceipt && LookupTable != null && !string.IsNullOrEmpty(LookupTable.SignedLabel);
            ShowTicketSignature = IsTicket && !string.IsNullOrEmpty(doc.TicketSignature);
            ShowGuestSignature = doc is { Account: not null, HasAccountDepositPayment: false };
            ShowDigitalSignature = doc.DigitalSignature.HasValue;

            Pension = doc.Pension;
            RoomAndPension = string.IsNullOrEmpty(Pension) ? doc.Room : $"{doc.Room} / {Pension}";
            
            #endregion
            
            #region QR Codes
            
            switch (Title)
            {
                case TicketTitles.Receipt:
                    if (LookupTable != null && !string.IsNullOrEmpty(LookupTable.ValidationCode) && LookupTable.Number.HasValue)
                        ReceiptValidationCode = $"ATCUD-{LookupTable.ValidationCode}-{LookupTable.Number.ToString()}";
                    ReceiptQrCode = LookupTable?.QrCode;
                    ShowAllowRoomCredit = !doc.AllowRoomCredit && !string.IsNullOrEmpty(doc.AccountDescription);
                    break;
                case TicketTitles.Ticket:
                    if (!string.IsNullOrEmpty(doc.ValidationCode) && doc.Serie.HasValue)
                        TicketValidationCode = $"ATCUD-{doc.ValidationCode}-{doc.Serie.Value.ToString()}";
                    TicketQrCode = doc.QrCode;
                    ShowTipSuggestions = false;
                    break;
                case TicketTitles.Ballot:
                    ShowTipSuggestions = false;
                    break;
                case TicketTitles.Invoice:
                    if (!string.IsNullOrEmpty(doc.FiscalDocValidationCode) && doc.FiscalDocNumber.HasValue)
                        InvoiceValidationCode = $"ATCUD-{doc.FiscalDocValidationCode}-{doc.FiscalDocNumber.Value.ToString()}";
                    InvoiceQrCode = doc.FiscalDocQrCode;
                    ShowTipSuggestions = false;
                    break;
                case TicketTitles.CreditNote:
                    if (!string.IsNullOrEmpty(doc.CreditNoteValidationCode) && doc.CreditNoteNumber.HasValue)
                        CreditNoteValidationCode = $"ATCUD-{doc.CreditNoteValidationCode}-{doc.CreditNoteNumber.Value.ToString()}";
                    CreditNoteQrCode = doc.CreditNoteQrCode;
                    ShowTipSuggestions = false;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            PrintReceiptQrCode = IsReceipt && LookupTable is { QrCode: not null };
            PrintTicketQrCode = TicketQrCode.HasValue;
            PrintInvoiceQrCode = InvoiceQrCode.HasValue;
            PrintCreditNoteQrCode = CreditNoteQrCode.HasValue;
            Cufe = doc.Cufe;
            
            #endregion

            #region Alternate Labels

            ShowSignatureOptions = settings.ShowSignatureOptions;
            if (!ShowSignatureOptions) 
                return;
            
            AlternateRoomNumber = settings.AlternateRoomNumber;
            AlternateName = settings.AlternateName;
            AlternateSignature = settings.AlternateSignature;
            
            #endregion
        }

        /// <summary>
        /// Formats the ticket series according to the country
        /// </summary>
        /// <param name="signType">The value of the enum DocumentSign which represents the country</param>
        /// <param name="ticketSeries">The series of the current ticket</param>
        /// <param name="number">The number of the ticket</param>
        /// <returns>A formatted string to be used on the receipt/invoice</returns>
        private static string FormatTicketSeries(DocumentSign signType, string ticketSeries, long? number)
        {
            if (string.IsNullOrEmpty(ticketSeries)) return string.Empty;
            // In the future, just add a new case for the new country
            return signType switch
            {
                DocumentSign.FiscalizationEcuador => $"{ticketSeries}-{(number ?? 0).ToString().PadLeft(9, '0')}",
                DocumentSign.FiscalizationColombiaBtw => $"{ticketSeries} {(number ?? 0)}",
                _ => $"{number}/{ticketSeries}"
            };
        }

        private static string FormatOpeningNumber(DocumentSign signType, long number)
        {
            switch (signType)
            {
                case DocumentSign.FiscalizationEcuador:
                    return number.ToString().PadLeft(9, '0');
                default:
                    return number.ToString();
            }
        }

        private void FiscalizationFormat(IPrintDocSetting settings)
        {
            TicketOpeningNumber = FormatOpeningNumber(settings.SignType, Ticket.OpeningNumber);
            if (LookupTable != null)
                LookupTableSerie = FormatTicketSeries(settings.SignType, LookupTable.Serie, LookupTable.Number);
            TicketSerie = FormatTicketSeries(settings.SignType, Ticket.SeriePrex, Ticket.Serie);
            if (!string.IsNullOrEmpty(Ticket.FiscalDocSerie))
                FiscalDocSerie = FormatTicketSeries(settings.SignType, Ticket.FiscalDocSeriePrex, Ticket.FiscalDocNumber);
            if (!string.IsNullOrEmpty(Ticket.CreditNoteSerie))
                CreditNoteSerie = FormatTicketSeries(settings.SignType, Ticket.CreditNoteSeriePrex, Ticket.CreditNoteNumber);
        }

        private void FiscalizationSignature(IPrintDocSetting settings)
        {
            const string certificateNumber = "1437";

            switch (settings.SignType)
            {
                case DocumentSign.FiscalizationPortugal:
                case DocumentSign.FiscalizationAngola:
                    ShowFiscalNumber = FiscalDocContract.GetFinalCustomerNumber(settings.SignType) != Ticket.FiscalDocFiscalNumber;

                    var certification = @$"-Processado por programa certificado no. {certificateNumber}/AT";
                    string BuildSignature(string s, string c) => s.Substring(0, 1) + s.Substring(10, 1) + s.Substring(20, 1) + s.Substring(30, 1) + c;

                    if (!string.IsNullOrEmpty(Ticket.FiscalDocSignature))
                        InvoiceSignature = BuildSignature(Ticket.FiscalDocSignature, certification);

                    if (!string.IsNullOrEmpty(Ticket.CreditNoteSignature))
                        CreditNoteSignature = BuildSignature(Ticket.CreditNoteSignature, certification);

                    if (!string.IsNullOrEmpty(Ticket.SignedLabel))
                        SignedLabel = BuildSignature(Ticket.SignedLabel, certification);

                    if (!string.IsNullOrEmpty(Ticket.TicketSignature))
                        TicketSignature = BuildSignature(Ticket.TicketSignature, certification);

                    break;
            }
        }

        #region Aux

        private void LogPrintOperation(PrinterContext context)
        {
            var logger = CwFactory.Resolve<ILogger>();
            Event e = Title switch
            {
                TicketTitles.Ballot => Event.PrintBallot,
                TicketTitles.Invoice => Event.PrintInvoice,
                TicketTitles.CreditNote => Event.PrintCreditNote,
                TicketTitles.Receipt => Event.PrintBill,
                TicketTitles.Ticket => Event.PrintTicket,
            };

            logger.TraceEvent(e,
                    $@"Preparing to print -> {context.ToLog()} -> 
                              Ticket: {Ticket.SeriePrex}/{Ticket.Serie + " - " + Ticket.TicketSignature};
                              Receipt: {(LookupTable != null ? LookupTable.Serie + " - " + LookupTable.Signature : "")}/{(LookupTable != null ? LookupTable.Number.ToString() : "")}; 
                              Invoice: {(Ticket.FiscalDocNumber.HasValue ? Ticket.FiscalDocNumber + " - " + Ticket.FiscalDocSerie : "")}; 
                              CreditNote: {(Ticket.FiscalDocNumber.HasValue ? Ticket.FiscalDocNumber + " - " + Ticket.FiscalDocSerie : "")}; 
                              ProductLineCount: {Ticket.VisibleOrdersCount}; Total: {Ticket.TotalAmount};
                              OpenTime: {Ticket.CheckOpeningDate.Date + Ticket.CheckOpeningTime.TimeOfDay}; 
                              CloseTime: {(Ticket.CloseDate != null ? (Ticket.CloseDate.Value.Date + (Ticket.CloseTime?.TimeOfDay ?? new TimeSpan(0))).ToString(CultureInfo.InvariantCulture) : "")};");
        }

        private void userControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            ProductsItemControl.GroupStyle.Clear();
            //if (DataContext is IPrintDocSetting _ && ((IPrintDocSetting)DataContext).PrintSeparators)
            //    ProductsItemControl.GroupStyle.Add(Resources["GroupStyle"] as GroupStyle);
        }

        private void InitializeSeparators(IPrintDocSetting settings)
        {
            if (settings != null && settings.PrintSeparators)
            {
                var view = (CollectionView)CollectionViewSource.GetDefaultView(Orders);
                view.GroupDescriptions?.Clear();
                view.SortDescriptions.Clear();
                view.Filter = null;

                switch (settings.ProductGrouping)
                {
                    case TicketProductGrouping.SeatsSeparators:
                        view.GroupDescriptions?.Add(new PropertyGroupDescription(nameof(OrderLite.PaxName)));
                        view.GroupDescriptions?.Add(new PropertyGroupDescription(nameof(OrderLite.Separator)));
                        view.SortDescriptions.Add(new SortDescription(nameof(OrderLite.PaxNumber), ListSortDirection.Ascending));
                        view.SortDescriptions.Add(new SortDescription(nameof(OrderLite.Separator), ListSortDirection.Ascending));
                        view.SortDescriptions.Add(new SortDescription(nameof(OrderLite.ItemNumber), ListSortDirection.Ascending));
                        break;
                    case TicketProductGrouping.SeparatorsSeats:
                        view.GroupDescriptions?.Add(new PropertyGroupDescription(nameof(OrderLite.Separator)));
                        view.GroupDescriptions?.Add(new PropertyGroupDescription(nameof(OrderLite.PaxName)));
                        view.SortDescriptions.Add(new SortDescription(nameof(OrderLite.Separator), ListSortDirection.Ascending));
                        view.SortDescriptions.Add(new SortDescription(nameof(OrderLite.PaxNumber), ListSortDirection.Ascending));
                        view.SortDescriptions.Add(new SortDescription(nameof(OrderLite.ItemNumber), ListSortDirection.Ascending));
                        break;
                }

                ProductsItemControl.GroupStyle.Add(Resources["PaxGroupStyle"] as GroupStyle);
                ProductsItemControl.GroupStyle.Add(Resources["SeparatorGroupStyle"] as GroupStyle);
            }
        }

        private void TextBlockTranslationLoaded(object sender, RoutedEventArgs e)
        {
            if (!_textBlocksTranslation.ContainsKey((TextBlock)sender))
            {
                var item = (TextBlock)sender;
                _textBlocksTranslation.Add(item, Thread.CurrentThread.CurrentUICulture);

                // Refresh
                var binding = BindingOperations.GetBindingExpressionBase(item, TextBlock.TextProperty);
                binding?.UpdateTarget();
                _textBlocksTranslation[item] = Thread.CurrentThread.CurrentUICulture;
            }
        }

        private void TextBlockTranslationUnloaded(object sender, RoutedEventArgs e)
        {
            if (_textBlocksTranslation.ContainsKey((TextBlock)sender))
                _textBlocksTranslation.Remove((TextBlock)sender);
        }

        #endregion

        private static void InitializeDependencys()
        {
            CwFactory.Register<ILogger, TraceLog>();
        }

        #endregion
    }

    public enum TicketTitles { Receipt, Ticket, Invoice, CreditNote, Ballot };
}