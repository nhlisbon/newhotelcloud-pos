﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Diagnostics;
using NewHotel.Pos.Localization;
using NewHotel.Pos.Printer.Interfaces;
using NewHotel.Contracts;
using NewHotel.Pos.PrinterDocument.DirectPrint;
using NewHotel.Pos.PrinterDocument.Fiscal;
using System.Runtime.Remoting.Contexts;

namespace NewHotel.Pos.PrinterDocument.Simple.Visual
{
	public partial class KitchenDocView : IPreparer<DocumentPaginator>, IPreparer<Printable>
	{
		public KitchenDoc KitchenDoc { get; set; }

		public IPrintableDoc Doc => KitchenDoc.Ticket;

		public void Initialize(KitchenDoc kitchenDoc)
		{
			KitchenDoc = kitchenDoc;
		}

		public DocumentPaginator PreparePrint(PrinterContext context)
		{
			const int defaultTopMargin = 2;
			const int defaultBottonMargin = 2;
			const int defaultLeftMargin = 12;
			const int defaultRightMargin = 2;

			var setting = context.Setting;
			var now = DateTime.Now;

			var document = new FlowDocument
			{
				ColumnGap = 0,
				ColumnWidth = context.PrintableAreaWidth - (setting.LeftMargin + defaultLeftMargin + setting.RigthMargin + defaultRightMargin),
				PagePadding = new Thickness(setting.LeftMargin + defaultLeftMargin, setting.TopMargin + defaultTopMargin, setting.RigthMargin + defaultRightMargin, setting.BottomMargin + defaultBottonMargin),
				PageWidth = context.PrintableAreaWidth - (setting.LeftMargin + defaultLeftMargin + setting.RigthMargin + defaultRightMargin),
			};

			try
			{
				Paragraph paragraph;

				#region Date and sale number

				{
					var grid = new Grid();
					grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
					grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

					var date = GetTextBlock($"{"Date".Translate()}: {now.ToShortDateString()}  {now:HH:mm}", horizontalAlignment: HorizontalAlignment.Left);
					var sale = GetTextBlock($"{"SaleNo".Translate()}: {Doc.OpeningNumber}", horizontalAlignment: HorizontalAlignment.Right);

					grid.Children.Add(date);
					grid.Children.Add(sale);

					Grid.SetColumn(date, 0);
					Grid.SetColumn(sale, 1);
					document.AddBlock(GetBlockUiContainer(grid));
				}

				#endregion

				#region Action

				paragraph = GetParagraph(KitchenDoc.ActionDescription, fontSize: context.Setting.ActionFontSize, margin: new Thickness(0, 0, 0, 0));
				paragraph.FontWeight = FontWeights.Bold;
				document.AddBlock(paragraph);

				#endregion

				#region Area

				if (string.IsNullOrEmpty(KitchenDoc.AreaDescription))
					throw new ArgumentException("Spooler: Area description is missing in the printers.xml file");

				paragraph = GetParagraph(KitchenDoc.AreaDescription, fontSize: 15, margin: new Thickness(0, 0, 0, 0));
				paragraph.FontStyle = FontStyles.Italic;
				document.AddBlock(paragraph);

				#endregion

				#region Saloon & Table

				if (!string.IsNullOrEmpty(Doc.SaloonDescription) || !string.IsNullOrEmpty(Doc.TableDescription))
				{
					var saloonTable = $"{(string.IsNullOrEmpty(Doc.SaloonDescription) ? "[NoSaloon]" : Doc.SaloonDescription)} / {(string.IsNullOrEmpty(Doc.TableDescription) ? "[NoTable]" : Doc.TableDescription)}";
					paragraph = GetParagraph(saloonTable, fontSize: context.Setting.SaloonFontSize, margin: new Thickness(0, 0, 0, 0));
					paragraph.FontWeight = FontWeights.SemiBold;
					document.AddBlock(paragraph);
				}

				#endregion

				#region Serie

				// document.AddBlock(GetParagraph("SaleNo".Translate()));
				//
				// paragraph = GetParagraph($"{Doc.OpeningNumber}", fontSize: 15, margin: new Thickness(1));
				// paragraph.FontWeight = FontWeights.SemiBold;
				// document.AddBlock(paragraph);

				#endregion

				#region Header

				paragraph = GetParagraph($"{"Stand".Translate()}:   {Doc.StandDescription}\n", fontSize: 12, margin: new Thickness(5), textAlignment: TextAlignment.Left);
				paragraph.Inlines.Add($"{"Cashier".Translate()}: {Doc.CashierDescription}\n");
				if (!string.IsNullOrEmpty(Doc.Room))
					paragraph.Inlines.Add($"{"Room".Translate()}:    {Doc.Room}\n");
				if (!string.IsNullOrEmpty(Doc.Name))
					paragraph.Inlines.Add($"{"Guest".Translate()}:    {Doc.Name}\n");
				paragraph.Inlines.Add($"{"Paxs".Translate()}:    {Doc.Paxs}  User: {Doc.UserDescription}\n");
				paragraph.Inlines.Add($"{"Date".Translate()}:    {now.ToShortDateString()}  {now.ToString("HH:mm")}");

				document.AddBlock(paragraph);

				#endregion

				if (Doc.Clients.Any())
				{
					#region Separator

					document.AddBlock(GetBlockUiContainer(GetRectangle(Brushes.Black, 1.5)));

					#endregion

					#region Client Info

					foreach (var client in Doc.Clients)
					{
						var hasPreferences = !string.IsNullOrEmpty(client.Preferences);
						var hasDiets = !string.IsNullOrEmpty(client.Diets);
						var hasAttentions = !string.IsNullOrEmpty(client.Attentions);
						var hasAllergies = !string.IsNullOrEmpty(client.Allergies);
						var hasUncommonAllergies = !string.IsNullOrEmpty(client.UncommonAllergies);
						var hasSegmentOperations = !string.IsNullOrEmpty(client.SegmentOperations);
						var hasClientInfo = hasPreferences || hasDiets || hasAttentions || hasAllergies || hasUncommonAllergies || hasSegmentOperations;

						if (hasClientInfo)
						{
							var grid = new Grid();
							grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
							grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
							grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

							var leftLine = GetRectangle(Brushes.Black);
							var tbClient = GetTextBlock($"{client.ClientInfo}", trimming: TextTrimming.CharacterEllipsis,
								margin: new Thickness(5, 0, 0, 0), fontSize: 14, fontWeight: FontWeights.Bold);
							var rightLine = GetRectangle(Brushes.Black);

							grid.Children.Add(leftLine);
							grid.Children.Add(tbClient);
							grid.Children.Add(rightLine);

							Grid.SetColumn(leftLine, 0);
							Grid.SetColumn(tbClient, 1);
							Grid.SetColumn(rightLine, 2);

							document.AddBlock(GetBlockUiContainer(grid, fontSize: 10, margin: new Thickness(5, 5, 5, 0)));

							#region Preferences

							if (hasPreferences)
							{
								paragraph = GetParagraph($"{"Preferences".Translate()}:", fontSize: 14, margin: new Thickness(5), textAlignment: TextAlignment.Left);
								paragraph.FontWeight = FontWeights.Bold;
								document.AddBlock(paragraph);
								paragraph = GetParagraph($"{client.Preferences}.", fontSize: 12, margin: new Thickness(10, 5, 5, 5), textAlignment: TextAlignment.Left);
								document.AddBlock(paragraph);
							}

							#endregion

							#region Diets

							if (hasDiets)
							{
								paragraph = GetParagraph($"{"Diets".Translate()}:", fontSize: 14, margin: new Thickness(5), textAlignment: TextAlignment.Left);
								paragraph.FontWeight = FontWeights.Bold;
								document.AddBlock(paragraph);
								paragraph = GetParagraph($"{client.Diets}.", fontSize: 12, margin: new Thickness(10, 5, 5, 5), textAlignment: TextAlignment.Left);
								document.AddBlock(paragraph);
							}

							#endregion

							#region Attentions

							if (hasAttentions)
							{
								paragraph = GetParagraph($"{"Attentions".Translate()}:", fontSize: 14, margin: new Thickness(5), textAlignment: TextAlignment.Left);
								paragraph.FontWeight = FontWeights.Bold;
								document.AddBlock(paragraph);
								paragraph = GetParagraph($"{client.Attentions}.", fontSize: 12, margin: new Thickness(10, 5, 5, 5), textAlignment: TextAlignment.Left);
								document.AddBlock(paragraph);
							}

							#endregion

							#region Allergies

							if (hasAllergies)
							{
								paragraph = GetParagraph($"{"Allergies".Translate()}:", fontSize: 14, margin: new Thickness(5), textAlignment: TextAlignment.Left);
								paragraph.FontWeight = FontWeights.Bold;
								document.AddBlock(paragraph);
								paragraph = GetParagraph($"{client.Allergies}.", fontSize: 12, margin: new Thickness(10, 5, 5, 5), textAlignment: TextAlignment.Left);
								document.AddBlock(paragraph);
								
							}
							
							#endregion

							#region Uncommon Allergies

							if (hasUncommonAllergies)
							{
								paragraph = GetParagraph($"{"UncommonAllergies".Translate()}:", fontSize: 14, margin: new Thickness(5), textAlignment: TextAlignment.Left);
								paragraph.FontWeight = FontWeights.Bold;
								document.AddBlock(paragraph);
								paragraph = GetParagraph($"{client.UncommonAllergies}.", fontSize: 12, margin: new Thickness(10, 5, 5, 5), textAlignment: TextAlignment.Left);
								document.AddBlock(paragraph);
							}							

							#endregion

							#region Segment Operations

							if (hasSegmentOperations)
							{
								paragraph = GetParagraph($"{"OperationSegments".Translate()}:", fontSize: 14, margin: new Thickness(5), textAlignment: TextAlignment.Left);
								paragraph.FontWeight = FontWeights.Bold;
								document.AddBlock(paragraph);
								paragraph = GetParagraph($"{client.SegmentOperations}.", fontSize: 12, margin: new Thickness(10, 5, 5, 5), textAlignment: TextAlignment.Left);
								document.AddBlock(paragraph);
							}
							
							#endregion
						}
					}

					#endregion
				}

				#region Separator

				document.AddBlock(GetBlockUiContainer(GetRectangle(Brushes.Black, 1.5)));

				#endregion

				#region Group Orders

				if (string.IsNullOrEmpty(Doc.SeriePrex))
					Trace.TraceInformation($"Kitchen doc view Num:{Doc.OpeningNumber} Ticket:{Doc.Serie ?? 0}");
				else
					Trace.TraceInformation($"Kitchen doc view Num:{Doc.OpeningNumber} Ticket:{Doc.Serie ?? 0}/{Doc.SeriePrex}");
				foreach (var productLine in Doc.ProductLines)
					Trace.TraceInformation($"ProductLine Id:{productLine.Id}");
				foreach (var orderId in KitchenDoc.OrderIds)
					Trace.TraceInformation($"Order Id:{orderId}");

				var orders = new List<KitchenOrder>();
				var productLines = Doc.ProductLines.Where(x => KitchenDoc.OrderIds.Keys.Contains(x.Id));
				Trace.TraceInformation($"{productLines.Count()} orders found in product lines");

				foreach (var productLine in productLines)
				{
					var similar = orders.FirstOrDefault(x => x.Line.ProductId == productLine.ProductId &&
															 (string.IsNullOrEmpty(productLine.Notes) && string.IsNullOrEmpty(x.Line.Notes) || productLine.Notes == x.Line.Notes) &&
															  !productLine.HasManualPrice && !x.Line.HasManualPrice &&
															  productLine.Separator?.Description == x.Line.Separator?.Description &&
															  (productLine.PaxNumber ?? short.MaxValue) == (x.Line.PaxNumber ?? short.MaxValue) &&
															  productLine.TableLines.Count == 0 && x.Line.TableLines.Count == 0 &&
															 ((productLine.Preparations.Count == 0 && x.Line.Preparations.Count == 0) ||
															  productLine.Preparations.OrderBy(y => y.Id).SequenceEqual(x.Line.Preparations.OrderBy(y => y.Id), new PreparationComparer())));

					if (similar != null)
						similar.Quantity += productLine.Quantity;
					else
						orders.Add(new KitchenOrder(productLine));
				}

				#endregion

				#region Orders

				switch (KitchenDoc.ProductGrouping)
				{
					case TicketProductGrouping.SeatsSeparators:
						{
							Trace.TraceInformation($"Grouping by seats {orders.Count} orders");
							var seats = orders.GroupBy(x => x.Line.PaxName)
											  .OrderBy(x => x.First().Line.PaxNumber ?? short.MaxValue);

							Trace.TraceInformation($"{seats.Count()} seats");
							foreach (var seat in seats)
							{
								Trace.TraceInformation($"Seat: {seat.Key} Items:{seat.Count()}");

								#region Seat

								{
									var grid = new Grid();
									grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
									grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

									var tbSeat = GetTextBlock($"{seat.Key}", trimming: TextTrimming.None, margin: new Thickness(0, 0, 10, 0));
									var line = GetRectangle(Brushes.Black, 2);

									grid.Children.Add(tbSeat);
									grid.Children.Add(line);

									Grid.SetColumn(tbSeat, 0);
									Grid.SetColumn(line, 1);

									document.AddBlock(GetBlockUiContainer(grid, fontSize: 18, fontWeight: FontWeights.Bold, margin: new Thickness(5, 5, 5, 0)));
								}

								#endregion

								Trace.TraceInformation($"Grouping by separator {orders.Count} orders");
								var separators = seat.GroupBy(x => x.Line.Separator.Description)
													 .OrderBy(x => x.First().Line.Separator.ImpresionOrden);

								Trace.TraceInformation($"{separators.Count()} separators");
								foreach (var separator in separators)
								{
									Trace.TraceInformation($"Separator Name:{separator.Key} Items:{separator.Count()}");

									#region Separator

									{
										var grid = new Grid();
										grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
										grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
										grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
										grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

										var leftLine = GetRectangle(Brushes.Black);
										var tbSeparator = GetTextBlock($"{separator.Key}", trimming: TextTrimming.CharacterEllipsis,
											margin: new Thickness(5, 0, 0, 0), fontSize: 14, fontWeight: FontWeights.Bold);
										var tbCount = GetTextBlock($"({separator.Count()})", trimming: TextTrimming.None, margin: new Thickness(5, 0, 5, 0));
										var rightLine = GetRectangle(Brushes.Black);

										grid.Children.Add(leftLine);
										grid.Children.Add(tbSeparator);
										grid.Children.Add(tbCount);
										grid.Children.Add(rightLine);

										Grid.SetColumn(leftLine, 0);
										Grid.SetColumn(tbSeparator, 1);
										Grid.SetColumn(tbCount, 2);
										Grid.SetColumn(rightLine, 3);

										document.AddBlock(GetBlockUiContainer(grid, fontSize: 10, margin: new Thickness(5, 5, 5, 0)));
									}

									#endregion

									#region Separator Orders

									foreach (var order in separator)
									{
										#region Order Manual Separator

										//if (order.Line.HasManualSeparator)
										//{
										//    var line = GetRectangle(Brushes.Black);
										//    document.AddBlock(GetBlockUiContainer(line, margin: new Thickness(2, 2, 2, 0)));
										//}

										#endregion

										#region Order Quantity + Product

										Trace.TraceInformation($"Print separator item Qty:{order.Quantity} Product:{order.Line.ProductDescription}");
										paragraph = GetParagraph(GetTextBlock($"{order.Quantity}  {order.Line.ProductDescription}", trimming: TextTrimming.CharacterEllipsis),
											fontSize: setting.LineFontSize, margin: new Thickness(2, 2, 2, 0), textAlignment: TextAlignment.Left);
										document.AddBlock(paragraph);

										#endregion

										#region Order Notes

										if (!string.IsNullOrEmpty(order.Line.Notes))
										{
											var tbNotes = GetTextBlock(order.Line.Notes, wrapping: TextWrapping.Wrap);
											paragraph = GetParagraph(tbNotes, fontSize: context.Setting.NoteFontSize, fontStyle: FontStyles.Italic, margin: new Thickness(2), textAlignment: TextAlignment.Left);
											document.AddBlock(paragraph);
										}

										#endregion

										#region Order Table Menu

										if (order.Line.TableLines != null && order.Line.TableLines.Count > 0)
										{
											var tables = order.Line.TableLines
												.GroupBy(x => x.Separator.Description)
												.OrderBy(x => x.First().Separator.ImpresionOrden);

											foreach (var table in tables)
											{
												#region Table Separator

												var grid = new Grid();
												grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
												grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
												grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

												var leftLine = GetRectangle(Brushes.Black);
												var tbSeparator = GetTextBlock($"{table.Key}  ({table.Count()})", trimming: TextTrimming.CharacterEllipsis, margin: new Thickness(5, 0, 5, 0));
												var rightLine = GetRectangle(Brushes.Black);

												grid.Children.Add(leftLine);
												grid.Children.Add(tbSeparator);
												grid.Children.Add(rightLine);

												Grid.SetColumn(leftLine, 0);
												Grid.SetColumn(tbSeparator, 1);
												Grid.SetColumn(rightLine, 2);

												document.AddBlock(GetBlockUiContainer(grid, margin: new Thickness(5, 5, 5, 0)));

												#endregion

												#region Table Products Separator

												foreach (var tab in table)
												{
													#region Product

													var tbProduct = GetTextBlock($"{tab.ProductQuantity}  {tab.ProductDescription}", trimming: TextTrimming.CharacterEllipsis);
													paragraph = GetParagraph(tbProduct, fontSize: 13, margin: new Thickness(15, 2, 2, 0));
													paragraph.TextAlignment = TextAlignment.Left;
													document.AddBlock(paragraph);

													#endregion

													#region Product Preparations

													if (tab.Preparations != null && tab.Preparations.Count > 0)
													{
														var preparations = new StackPanel();
														foreach (var p in tab.Preparations)
														{
															var tbPreparation = GetTextBlock($"    - {p.Description}", wrapping: TextWrapping.Wrap);
															tbPreparation.TextAlignment = TextAlignment.Left;
															preparations.Children.Add(tbPreparation);
														}

														if (tab.Preparations.Count > 0)
															document.AddBlock(GetBlockUiContainer(preparations, fontSize: 13, margin: new Thickness(10, 0, 2, 0)));
													}

													#endregion

													#region Observations

													if (string.IsNullOrEmpty(tab.Notes)) continue;
													var sepTabNotesPanel = new StackPanel();
													var sepTabNotesTextBlock = GetTextBlock($"    - {tab.Notes}", wrapping: TextWrapping.Wrap);
													sepTabNotesTextBlock.TextAlignment = TextAlignment.Left;
													sepTabNotesPanel.Children.Add(sepTabNotesTextBlock);
													document.AddBlock(GetBlockUiContainer(sepTabNotesPanel, fontSize: 13, margin: new Thickness(10, 0, 2, 0)));

													#endregion

												}

												#endregion
											}
										}

										#endregion

										#region Order Preparations

										if (order.Line.Preparations != null && order.Line.Preparations.Count > 0)
										{
											var preparations = new StackPanel();
											foreach (var p in order.Line.Preparations)
											{
												var tbPreparation = GetTextBlock($"    - {p.Description}", wrapping: TextWrapping.Wrap);
												tbPreparation.TextAlignment = TextAlignment.Left;
												preparations.Children.Add(tbPreparation);
											}

											document.AddBlock(GetBlockUiContainer(preparations, fontSize: 13, margin: new Thickness(10, 0, 2, 0)));
										}

										#endregion
									}

									#endregion
								}
							}
						}
						break;
					case TicketProductGrouping.SeparatorsSeats:
						{
							Trace.TraceInformation($"Grouping by separator {orders.Count} orders");
							var separators = orders.GroupBy(x => x.Line.Separator.Description)
												   .OrderBy(x => x.First().Line.Separator.ImpresionOrden);

							Trace.TraceInformation($"{separators.Count()} separators");
							foreach (var separator in separators)
							{
								Trace.TraceInformation($"Separator Name:{separator.Key} Items:{separator.Count()}");

								#region Separator

								{
									var grid = new Grid();
									grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
									grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

									var tbSeparator = GetTextBlock($"{separator.Key}", trimming: TextTrimming.None, margin: new Thickness(0, 0, 10, 0));
									var line = GetRectangle(Brushes.Black, 2);

									grid.Children.Add(tbSeparator);
									grid.Children.Add(line);

									Grid.SetColumn(tbSeparator, 0);
									Grid.SetColumn(line, 1);

									document.AddBlock(GetBlockUiContainer(grid, fontSize: 18, fontWeight: FontWeights.Bold, margin: new Thickness(5, 5, 5, 0)));
								}

								#endregion

								Trace.TraceInformation($"Grouping by seats {orders.Count} orders");
								var seats = separator.GroupBy(x => x.Line.PaxName)
													 .OrderBy(x => x.First().Line.PaxNumber ?? short.MaxValue);

								Trace.TraceInformation($"{seats.Count()} seats");
								foreach (var seat in seats)
								{
									Trace.TraceInformation($"Seat: {seat.Key} Items:{seat.Count()}");

									#region Seat

									{
										var grid = new Grid();
										grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
										grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
										// grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
										// grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

										var leftLine = GetRectangle(Brushes.Black);
										var tbSeat = GetTextBlock($"{seat.Key}", trimming: TextTrimming.CharacterEllipsis,
											margin: new Thickness(5, 0, 0, 0), fontSize: 20, fontWeight: FontWeights.Bold);
										// var tbCount = GetTextBlock($"({seat.Count()})", trimming: TextTrimming.None, margin: new Thickness(5, 0, 5, 0));
										// var rightLine = GetRectangle(Brushes.Black);

										grid.Children.Add(leftLine);
										grid.Children.Add(tbSeat);
										// grid.Children.Add(tbCount);
										// grid.Children.Add(rightLine);

										Grid.SetColumn(leftLine, 0);
										Grid.SetColumn(tbSeat, 1);
										// Grid.SetColumn(tbCount, 2);
										// Grid.SetColumn(rightLine, 3);

										document.AddBlock(GetBlockUiContainer(grid, fontSize: 20, margin: new Thickness(5, 5, 5, 0)));
									}

									#endregion

									#region Seat Orders

									foreach (var order in seat)
									{
										#region Order Quantity + Product

										Trace.TraceInformation($"Print separator item Qty:{order.Quantity} Product:{order.Line.ProductDescription}");
										paragraph = GetParagraph(GetTextBlock($"{order.Quantity}  {order.Line.ProductDescription}", trimming: TextTrimming.CharacterEllipsis),
											fontSize: setting.LineFontSize, margin: new Thickness(2, 2, 2, 0), textAlignment: TextAlignment.Left);
										document.AddBlock(paragraph);

										#endregion

										#region Order Notes

										if (!string.IsNullOrEmpty(order.Line.Notes))
										{
											var tbNotes = GetTextBlock(order.Line.Notes, wrapping: TextWrapping.Wrap);
											paragraph = GetParagraph(tbNotes, fontSize: context.Setting.NoteFontSize, fontStyle: FontStyles.Italic, margin: new Thickness(2), textAlignment: TextAlignment.Left);
											document.AddBlock(paragraph);
										}

										#endregion

										#region Order Table Menu

										if (order.Line.TableLines != null && order.Line.TableLines.Count > 0)
										{
											var tables = order.Line.TableLines
												.Where(x => KitchenDoc.OrderIds[order.Line.Id].Contains(x.Id))
												.GroupBy(x => x.Separator.Description)
												.OrderBy(x => x.First().Separator.ImpresionOrden);

											foreach (var table in tables)
											{
												#region Table Separator

												var grid = new Grid();
												grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
												grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
												grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

												var leftLine = GetRectangle(Brushes.Black);
												var tbSeparator = GetTextBlock($"{table.Key}  ({table.Count()})", trimming: TextTrimming.CharacterEllipsis, margin: new Thickness(5, 0, 5, 0));
												var rightLine = GetRectangle(Brushes.Black);

												grid.Children.Add(leftLine);
												grid.Children.Add(tbSeparator);
												grid.Children.Add(rightLine);

												Grid.SetColumn(leftLine, 0);
												Grid.SetColumn(tbSeparator, 1);
												Grid.SetColumn(rightLine, 2);

												document.AddBlock(GetBlockUiContainer(grid, margin: new Thickness(5, 5, 5, 0)));

												#endregion

												#region Table Products Separator

												foreach (var tab in table)
												{
													#region Product

													var tbProduct = GetTextBlock($"{tab.ProductQuantity}  {tab.ProductDescription}", trimming: TextTrimming.CharacterEllipsis);
													paragraph = GetParagraph(tbProduct, fontSize: 13, margin: new Thickness(15, 2, 2, 0));
													paragraph.TextAlignment = TextAlignment.Left;
													document.AddBlock(paragraph);

													#endregion

													#region Product Preparations

													if (tab.Preparations != null && tab.Preparations.Count > 0)
													{
														var preparations = new StackPanel();
														foreach (var p in tab.Preparations)
														{
															var tbPreparation = GetTextBlock($"    - {p.Description}", wrapping: TextWrapping.Wrap);
															tbPreparation.TextAlignment = TextAlignment.Left;
															preparations.Children.Add(tbPreparation);
														}

														if (tab.Preparations.Count > 0)
															document.AddBlock(GetBlockUiContainer(preparations, fontSize: context.Setting.NoteFontSize, margin: new Thickness(10, 0, 2, 0)));
													}

													#endregion

													#region Observations

													if (string.IsNullOrEmpty(tab.Notes)) continue;
													var sepTabNotesPanel = new StackPanel();
													var sepTabNotesTextBlock = GetTextBlock($"    - {tab.Notes}", wrapping: TextWrapping.Wrap);
													sepTabNotesTextBlock.TextAlignment = TextAlignment.Left;
													sepTabNotesPanel.Children.Add(sepTabNotesTextBlock);
													document.AddBlock(GetBlockUiContainer(sepTabNotesPanel, fontSize: context.Setting.NoteFontSize, margin: new Thickness(10, 0, 2, 0)));

													#endregion
												}

												#endregion
											}
										}

										#endregion

										#region Order Preparations

										if (order.Line.Preparations != null && order.Line.Preparations.Count > 0)
										{
											var preparations = new StackPanel();
											foreach (var p in order.Line.Preparations)
											{
												var tbPreparation = GetTextBlock($"    - {p.Description}", wrapping: TextWrapping.Wrap);
												tbPreparation.TextAlignment = TextAlignment.Left;
												preparations.Children.Add(tbPreparation);
											}

											document.AddBlock(GetBlockUiContainer(preparations, fontSize: 13, margin: new Thickness(10, 0, 2, 0)));
										}

										#endregion
									}

									#endregion
								}
							}
						}
						break;
				}

				#endregion

				#region Allow Room Credit

				{
					if (!Doc.AllowRoomCredit && !string.IsNullOrEmpty(Doc.AccountDescription))
					{
						var grid = new Grid();
						grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
						grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
						grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

						var leftLine = GetRectangle(Brushes.Black);
						var tbFooter = GetTextBlock($"{Doc.AccountDescription} \n Room credit not allowed", trimming: TextTrimming.None, margin: new Thickness(2));
						var rightLine = GetRectangle(Brushes.Black);

						grid.Children.Add(leftLine);
						grid.Children.Add(tbFooter);
						grid.Children.Add(rightLine);

						Grid.SetColumn(leftLine, 0);
						Grid.SetColumn(tbFooter, 1);
						Grid.SetColumn(rightLine, 2);

						document.AddBlock(GetBlockUiContainer(grid, fontSize: 10, margin: new Thickness(0, 5, 0, 0)));
					}
				}

				#endregion

				#region Footer

				{
					var grid = new Grid();
					grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
					grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
					grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

					var leftLine = GetRectangle(Brushes.Black);
					var tbFooter = GetTextBlock($" {now.ToShortDateString()}  {now.ToShortTimeString()} ", trimming: TextTrimming.None, margin: new Thickness(2));
					var rightLine = GetRectangle(Brushes.Black);

					grid.Children.Add(leftLine);
					grid.Children.Add(tbFooter);
					grid.Children.Add(rightLine);

					Grid.SetColumn(leftLine, 0);
					Grid.SetColumn(tbFooter, 1);
					Grid.SetColumn(rightLine, 2);

					document.AddBlock(GetBlockUiContainer(grid, fontSize: 10, margin: new Thickness(0, 5, 0, 0)));
				}

				#endregion
			}
			catch (Exception ex)
			{
				Trace.TraceError($"{ex.Message} {ex.StackTrace}");
				throw;
			}

			IDocumentPaginatorSource source = document;
			var paginator = source.DocumentPaginator;
			return paginator;
		}

		private Rectangle GetRectangle(Brush fill, double height = 1, HorizontalAlignment horizontalAlignment = HorizontalAlignment.Stretch)
		{
			return new Rectangle
			{
				Height = height,
				Fill = fill,
				HorizontalAlignment = horizontalAlignment
			};
		}

		private TextBlock GetTextBlock(string text, Thickness? margin = null, TextTrimming trimming = TextTrimming.None, TextWrapping wrapping = TextWrapping.NoWrap, double? fontSize = null, FontWeight? fontWeight = null, HorizontalAlignment horizontalAlignment = HorizontalAlignment.Stretch)
		{
			var tb = new TextBlock
			{
				Text = text,
				TextTrimming = trimming,
				TextWrapping = wrapping,
				VerticalAlignment = VerticalAlignment.Center,
				HorizontalAlignment = horizontalAlignment
			};

			if (fontSize.HasValue)
				tb.FontSize = fontSize.Value;

			if (fontWeight.HasValue)
				tb.FontWeight = fontWeight.Value;

			if (margin.HasValue)
				tb.Margin = margin.Value;

			return tb;
		}

		private Paragraph GetParagraph(string text, string familyName = "Arial", double fontSize = 10, Thickness? margin = null, TextAlignment textAlignment = TextAlignment.Center)
		{
			var p = new Paragraph
			{
				FontFamily = new FontFamily(familyName),
				FontSize = fontSize,
				TextAlignment = textAlignment
			};

			if (margin.HasValue)
				p.Margin = margin.Value;
			p.Inlines.Add(text);

			return p;
		}

		private Paragraph GetParagraph(UIElement uiElement, string familyName = "Arial", double fontSize = 10, FontStyle? fontStyle = null, Thickness? margin = null, TextAlignment textAlignment = TextAlignment.Center)
		{
			var p = new Paragraph
			{
				FontFamily = new FontFamily(familyName),
				FontSize = fontSize,
				FontStyle = fontStyle ?? FontStyles.Normal,
				TextAlignment = textAlignment
			};

			if (margin.HasValue)
				p.Margin = margin.Value;
			p.Inlines.Add(uiElement);

			return p;
		}

		private BlockUIContainer GetBlockUiContainer(UIElement child, string familyName = "Arial", double fontSize = 10, FontWeight? fontWeight = null, Thickness? margin = null, TextAlignment textAlignment = TextAlignment.Center)
		{
			var block = new BlockUIContainer(child)
			{
				FontFamily = new FontFamily(familyName),
				FontSize = fontSize,
				FontWeight = fontWeight ?? FontWeights.Normal,
				TextAlignment = textAlignment
			};

			if (margin.HasValue)
				block.Margin = margin.Value;

			return block;
		}

		private class KitchenOrder
		{
			public KitchenOrder(IPrintableOrder line)
			{
				Line = line;
				Quantity = Line.Quantity;
			}
			public decimal Quantity { get; set; }
			public IPrintableOrder Line { get; }
		}
	}

	public class KitchenDoc
	{
		public string ActionDescription { get; set; }
		public IPrintableDoc Ticket { get; set; }
		public Dictionary<Guid, HashSet<Guid>> OrderIds { get; set; }
		public Guid AreaId { get; set; }
		public string AreaDescription { get; set; }
		public TicketProductGrouping ProductGrouping { get; set; }
	}

	class PreparationComparer : IEqualityComparer<IPrinteablePreparation>
	{
		public bool Equals(IPrinteablePreparation b1, IPrinteablePreparation b2)
		{
			return b2 != null && b1 != null && b1.Id == b2.Id;
		}

		public int GetHashCode(IPrinteablePreparation bx)
		{
			return bx.GetHashCode();
		}
	}

	public static class Ext
	{
		public static void AddBlock(this FlowDocument doc, Block item)
		{
			doc.Blocks.Add(item);
		}
	}
}