﻿using System.Globalization;
using System.Threading;
using System.Windows.Data;

namespace NewHotel.Pos.PrinterDocument
{
    public class LocalizedBinding : Binding
    {
        public LocalizedBinding()
            : base()
        {
            ConverterCulture = (CultureInfo)Thread.CurrentThread.CurrentUICulture.Clone();
            ConverterCulture.NumberFormat.CurrencySymbol = string.Empty;
        }

        public LocalizedBinding(string path)
            : base(path)
        {
            ConverterCulture = (CultureInfo)Thread.CurrentThread.CurrentUICulture.Clone();
            ConverterCulture.NumberFormat.CurrencySymbol = string.Empty;
        }
    }

    public class LocalizedMultiBinding : MultiBinding
    {
        public LocalizedMultiBinding()
        {
            ConverterCulture = (CultureInfo)Thread.CurrentThread.CurrentUICulture.Clone();
            ConverterCulture.NumberFormat.CurrencySymbol = string.Empty;
        }
    }
}