﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Shapes;
using NewHotel.Contracts;
using NewHotel.Pos.PrinterDocument.Interfaces;
using NewHotel.Pos.PrinterDocument.Simple;

namespace NewHotel.Pos.Spooler.Business.Models
{
    public class KitchenDocument : IPreparer<DocumentPaginator>
    {
        public POSTicketContract Ticket { get; set; }
        public List<Guid> OrderIds { get; set; }
        public string ActionDescription { get; set; }
        public string AreaDescription { get; set; }
        public Guid AreaId { get; set; }

        public DocumentPaginator PreparePrint(PrinterContext context)
        {
            if (Ticket?.ProductLineByTicket == null || Ticket.ProductLineByTicket.Count == 0) 
                return null;

            PrinterSetting setting = context.Setting;
            var flowDocument = new FlowDocument
            {
                PageWidth = context.PrintableAreaWidth,
                PagePadding = new Thickness(setting.LeftMargin, setting.TopMargin, setting.RigthMargin, setting.BottomMargin)
            };

            // Action
            var paragraph = new Paragraph
            {
                FontFamily = new FontFamily("Arial"),
                FontSize = 16,
                FontWeight = FontWeights.Bold,
                TextAlignment = TextAlignment.Center
            };
            paragraph.Inlines.Add(ActionDescription);
            flowDocument.Blocks.Add(paragraph);

            //Area
            var paragraphArea = new Paragraph
            {
                FontFamily = new FontFamily("Arial"),
                FontSize = 15,
                FontStyle = FontStyles.Italic,
                TextAlignment = TextAlignment.Center
            };
            paragraphArea.Inlines.Add(AreaDescription);
            flowDocument.Blocks.Add(paragraphArea);

            // Saloon and table
            if (!string.IsNullOrEmpty(Ticket.SaloonDescription) || !string.IsNullOrEmpty(Ticket.TableDescription))
            {
                paragraph = new Paragraph
                {
                    FontFamily = new FontFamily("Arial"),
                    Margin = new Thickness(0),
                    FontSize = 10,
                    TextAlignment = TextAlignment.Center
                };
                paragraph.Inlines.Add("Saloon / Table");
                flowDocument.Blocks.Add(paragraph);
                paragraph = new Paragraph
                {
                    FontFamily = new FontFamily("Arial"),
                    Margin = new Thickness(0),
                    FontSize = 15,
                    FontWeight = FontWeights.SemiBold,
                    TextAlignment = TextAlignment.Center
                };
                paragraph.Inlines.Add(
                    $"{(string.IsNullOrEmpty(Ticket.SaloonDescription) ? "[NoSaloon]" : Ticket.SaloonDescription)} / {(string.IsNullOrEmpty(Ticket.TableDescription) ? "[NoTable]" : Ticket.TableDescription)}");
                flowDocument.Blocks.Add(paragraph);
            }
            // Serie
            paragraph = new Paragraph
            {
                FontFamily = new FontFamily("Arial"),
                Margin = new Thickness(0),
                FontSize = 10,
                TextAlignment = TextAlignment.Center
            };
            paragraph.Inlines.Add("Ticket");
            flowDocument.Blocks.Add(paragraph);

            paragraph = new Paragraph
            {
                FontFamily = new FontFamily("Arial"),
                Margin = new Thickness(1),
                FontSize = 15,
                FontWeight = FontWeights.SemiBold,
                TextAlignment = TextAlignment.Center
            };
            paragraph.Inlines.Add($"{Ticket.Number}");
            flowDocument.Blocks.Add(paragraph);

            #region Header

            paragraph = new Paragraph { FontFamily = new FontFamily("Arial"), FontSize = 12, Margin = new Thickness(5) };
            paragraph.Inlines.Add($"Stand: {Ticket.StandDescription}\n");
            paragraph.Inlines.Add($"Cashier: {Ticket.CashierDescription}\n");
            paragraph.Inlines.Add($"Paxs: {Ticket.Paxs}  User: {Ticket.UserDescription}\n");
            paragraph.Inlines.Add(string.Format("Date: {0:MM/dd/yyyy}  {0:HH:mm}", DateTime.Now));

            #endregion

            flowDocument.Blocks.Add(paragraph);

            // Separator
            paragraph = new Paragraph
            {
                FontFamily = new FontFamily("Arial"),
                Margin = new Thickness(0),
                TextAlignment = TextAlignment.Center
            };
            paragraph.Inlines.Add(new Rectangle { Height = 1.5, Width = flowDocument.PageWidth - 10, Fill = Brushes.Black });
            flowDocument.Blocks.Add(paragraph);

            //Group Orders
            var orders = new List<KitchenOrder>();

            foreach (var order in Ticket.ProductLineByTicket.Where(x => (AreaId == Guid.Empty || (x.AreaId ?? Guid.Empty) == AreaId) && OrderIds.Contains((Guid)x.Id)))
            {
                if (orders.Count == 0)
                {
                    orders.Add(new KitchenOrder(order));
                }
                else
                {
                    var similar = orders.FirstOrDefault(x => x.Line.Product == order.Product &&
                                                                       (string.IsNullOrEmpty(order.Observations) && string.IsNullOrEmpty(x.Line.Observations) || order.Observations == x.Line.Observations) &&
                                                                       !order.HasManualPrice &&
                                                                       !x.Line.HasManualPrice &&
                                                                       order.TableLines.Count == 0 && x.Line.TableLines.Count == 0 &&
                                                                       (order.Preparations.Count == 0 && x.Line.Preparations.Count == 0 ||
                                                                       order.Preparations.OrderBy(y => y.PreparationId).SequenceEqual(x.Line.Preparations.OrderBy(y => y.PreparationId), new PreparationComparer())));
                    if (similar != null) similar.Quantity += order.ProductQtd;
                    else orders.Add(new KitchenOrder(order));
                }
            }

            var ordersList = orders.Where(x => OrderIds.Contains((Guid)x.Line.Id)).GroupBy(x => x.Line.SeparatorDescription).OrderBy(x => x.First().Line.SeparatorOrder);
            bool _printSeparators = true;

            foreach (var separator in ordersList)
            {
                if (_printSeparators)
                {
                    paragraph = new Paragraph
                    {
                        FontFamily = new FontFamily("Arial"),
                        FontSize = 10,
                        Margin = new Thickness(2, 5, 2, 0),
                        TextAlignment = TextAlignment.Center
                    };

                    var grid = new Grid();
                    grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                    grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
                    grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });

                    grid.Children.Add(new Rectangle() { Height = 1, Fill = Brushes.Black, Width = 50 });
                    grid.Children.Add(new TextBlock() { Text = string.Format("{0}  ({1})", separator.Key, separator.Count()), TextTrimming = TextTrimming.CharacterEllipsis, Margin = new Thickness(5, 0, 5, 0) });
                    grid.Children.Add(new Rectangle() { Height = 1, Fill = Brushes.Black, Width = 50 });
                    Grid.SetColumn(grid.Children[1], 1);
                    Grid.SetColumn(grid.Children[2], 2);

                    paragraph.Inlines.Add(grid);
                    flowDocument.Blocks.Add(paragraph);
                }

                // Orders
                foreach (var order in separator)
                {
                    if (order.Line.HasManualSeparator)
                    {
                        paragraph = new Paragraph
                        {
                            FontFamily = new FontFamily("Arial"),
                            FontSize = 12,
                            Margin = new Thickness(2, 2, 2, 0)
                        };
                        paragraph.Inlines.Add(new Rectangle() { Height = 1, Width = 100, HorizontalAlignment = HorizontalAlignment.Center, Fill = Brushes.Black });
                        flowDocument.Blocks.Add(paragraph);
                    }

                    paragraph = new Paragraph
                    {
                        FontFamily = new FontFamily("Arial"),
                        FontSize = 14,
                        Margin = new Thickness(2, 2, 2, 0)
                    };
                    paragraph.Inlines.Add(new TextBlock { Text = $"{order.Quantity}  {order.Line.ProductDescription}", TextTrimming = TextTrimming.CharacterEllipsis });
                    flowDocument.Blocks.Add(paragraph);

                    if (order.Line.TableLines != null && order.Line.TableLines.Count > 0)
                    {
                        var tableProductList = order.Line.TableLines.GroupBy(x => x.SeparatorDescription).OrderBy(x => x.First().SeparatorOrden);

                        foreach (var separatorTable in tableProductList)
                        {
                            if (_printSeparators)
                            {
                                paragraph = new Paragraph() { FontFamily = new FontFamily("Arial") };
                                paragraph.FontSize = 10;
                                paragraph.Margin = new Thickness(2, 5, 2, 0);
                                paragraph.TextAlignment = TextAlignment.Center;

                                Grid grid = new Grid();
                                grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                                grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
                                grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });

                                grid.Children.Add(new Rectangle { Height = 1, Fill = Brushes.Black, Width = 50 });
                                grid.Children.Add(new TextBlock { Text = $"{separatorTable.Key}  ({separatorTable.Count()})", TextTrimming = TextTrimming.CharacterEllipsis, Margin = new Thickness(5, 0, 5, 0) });
                                grid.Children.Add(new Rectangle { Height = 1, Fill = Brushes.Black, Width = 50 });
                                Grid.SetColumn(grid.Children[1], 1);
                                Grid.SetColumn(grid.Children[2], 2);

                                paragraph.Inlines.Add(grid);
                                flowDocument.Blocks.Add(paragraph);
                            }

                            foreach (var item in separatorTable)
                            {
                                paragraph = new Paragraph
                                {
                                    FontFamily = new FontFamily("Arial"),
                                    FontSize = 13,
                                    FontStyle = FontStyles.Italic,
                                    Margin = new Thickness(15, 2, 2, 0)
                                };
                                paragraph.Inlines.Add(new TextBlock { Text = item.ProductDescription, TextTrimming = TextTrimming.CharacterEllipsis });
                                flowDocument.Blocks.Add(paragraph);

                                if (item.TableLinePreparations != null && item.TableLinePreparations.Count > 0)
                                {
                                    string preparations = string.Empty;
                                    preparations = item.TableLinePreparations.Aggregate(preparations, (current, preparation) => current + ("    - " + preparation.PreparationDescription + "\n"));
                                    preparations = preparations.Remove(preparations.Length - 1, 1);
                                    if (!string.IsNullOrEmpty(preparations))
                                    {
                                        paragraph = new Paragraph
                                        {
                                            FontFamily = new FontFamily("Arial"),
                                            FontSize = 13,
                                            Margin = new Thickness(10, 0, 2, 0)
                                        };
                                        paragraph.Inlines.Add(new TextBlock() { Text = preparations, TextWrapping = TextWrapping.Wrap });
                                        flowDocument.Blocks.Add(paragraph);
                                    }
                                }
                            }
                        }
                    }
                    #region Observations

                    if (!string.IsNullOrEmpty(order.Line.Observations))
                    {
                        paragraph = new Paragraph
                        {
                            FontFamily = new FontFamily("Arial"),
                            FontSize = 12,
                            Margin = new Thickness(10, 0, 2, 0)
                        };
                        paragraph.Inlines.Add(new TextBlock { Text = order.Line.Observations, TextWrapping = TextWrapping.Wrap });
                        flowDocument.Blocks.Add(paragraph);
                    }

                    #endregion
                    #region Preparations

                    if (order.Line.Preparations != null && order.Line.Preparations.Count > 0)
                    {
                        foreach (var p in order.Line.Preparations)
                        {
                            paragraph = new Paragraph
                            {
                                FontFamily = new FontFamily("Arial"),
                                FontSize = 12,
                                Margin = new Thickness(10, 0, 2, 0)
                            };
                            paragraph.Inlines.Add(new TextBlock { Text = "    - " + p.PreparationDescription, TextWrapping = TextWrapping.Wrap });
                        }
                        flowDocument.Blocks.Add(paragraph);
                    }

                    #endregion
                }
            }
            return ((IDocumentPaginatorSource)flowDocument).DocumentPaginator;
        }

        private class KitchenOrder
        {
            public KitchenOrder(IPrinteableOrder line)
            {
                Line = line;
                Quantity = Line.Quantity;
            }
            public decimal Quantity { get; set; }
            public IPrinteableOrder Line { get; }
        }
    }

    class PreparationComparer : IEqualityComparer<POSProductLinePreparationContract>
    {
        public bool Equals(POSProductLinePreparationContract b1, POSProductLinePreparationContract b2)
        {
            if (b1.PreparationId == b2.PreparationId) return true;
            return false;
        }

        public int GetHashCode(POSProductLinePreparationContract bx)
        {
            return bx.GetHashCode();
        }
    }
}
