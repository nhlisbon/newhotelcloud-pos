﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using NewHotel.Pos.PrinterDocument.Fiscal;

namespace NewHotel.Pos.PrinterDocument.Simple.Visual
{
    public static class ScrollViewerExt
    {
        public static System.Windows.Media.Visual PreparePrintScroll(this ScrollViewer scroll, PrinterContext context)
        {
            Border document = (Border)scroll.Content;
            document.Width = context.PrintableAreaWidth;
            scroll.ScrollToHome();

            PrinterSetting setting = context.Setting;
            document.Padding = new Thickness(setting.LeftMargin, setting.TopMargin, setting.RigthMargin, setting.BottomMargin);
            scroll.UpdateLayout();
            scroll.ScrollToTop();

            document.Measure(new Size(document.Width, 5000));
            document.Arrange(new Rect(new Size(document.Width, 5000)));
            document.UpdateLayout();

            document.Measure(new Size(document.ActualWidth, document.ActualHeight));
            document.Arrange(new Rect(new Size(document.ActualWidth, document.ActualHeight)));
            document.UpdateLayout();
            return document;
        }
    }
}
