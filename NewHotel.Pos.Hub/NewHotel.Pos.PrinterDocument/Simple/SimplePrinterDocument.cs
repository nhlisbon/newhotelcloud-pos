﻿using System;
using System.Printing;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Diagnostics;
using NewHotel.Contracts;
using NewHotel.Pos.Printer.Interfaces;
using NewHotel.Pos.PrinterDocument.Interfaces;
using NewHotel.Pos.PrinterDocument.Simple.Visual;
using NewHotel.Pos.Localization;
using NewHotel.WPF.App.Pos.Controls;
using ValidationResult = NewHotel.Contracts.ValidationResult;
namespace NewHotel.Pos.PrinterDocument.Simple
{
    public class SimplePrinterDocument : ITicketPrinter, IVisualPrinter
    {
        public string GetPrinterDescription(Printer p) => $"{p.Server}/{p.Name}";

        public ValidationResult PrintKitchenDoc(Printer printer, KitchenDoc doc)
        {
            var document = new KitchenDocView();
            document.Initialize(doc);
            return PrintDirect(printer, document, "Printing");
            // return PrintVisual(printer, document, "Printing");
        }

        public ValidationResult PrintInvoice(Printer printer, IPrintableDoc doc, IPrintDocSetting setting)
        {
            var document = new DocViewControl(TicketTitles.Invoice, doc, setting.Country);
            document.Initialize(doc, setting);
            return PrintVisual(printer, document, "Printing", setting.Copies);
        }

        public ValidationResult PrintBallot(Printer printer, IPrintableDoc doc, IPrintDocSetting setting)
        {
            var document = new DocViewControl(TicketTitles.Ballot, doc, setting.Country);
            document.Initialize(doc, setting);
            return PrintVisual(printer, document, "Printing", setting.Copies);
        }

        public ValidationResult PrintCreditNote(Printer printer, IPrintableDoc doc, IPrintDocSetting setting)
        {
            var document = new DocViewControl(TicketTitles.CreditNote, doc, setting.Country);
            document.Initialize(doc, setting);
            return PrintVisual(printer, document, "Printing", setting.Copies);
        }

        public ValidationResult PrintTicket(Printer printer, IPrintableDoc doc, IPrintDocSetting setting)
        {
            var document = new DocViewControl(TicketTitles.Ticket, doc, setting.Country);
            document.Initialize(doc, setting);
            return PrintVisual(printer, document, "Printing", setting.Copies);
        }

        public ValidationResult PrintLookupTable(Printer printer, IPrintableDoc doc, IPrintDocSetting setting)
        {
            var document = new DocViewControl(TicketTitles.Receipt, doc, setting.Country);
            document.Initialize(doc, setting);
            return PrintVisual(printer, document, "Printing", setting.Copies);
        }
        
        /// <summary>
        /// Connects to the printer and prints the visual
        /// </summary>
        /// <param name="printer">The printer to connect</param>
        /// <param name="prepare">The visual</param>
        /// <param name="description">The description to be written on the visual</param>
        /// <param name="copies">The number of copies of the visual</param>
        /// <returns>A validation result object with the error or success of the operation</returns>
        public ValidationResult PrintVisual(Printer printer, IPreparer<System.Windows.Media.Visual> prepare, string description, int copies = 1)
        {
            var result = new ValidationResult();

            if (printer.ValidationDestination())
            {
                PrintServer? server = null;
                try { server = new PrintServer(printer.Server); }
                catch(Exception e)
                {
                    result.AddError(string.Format("Server0NotFound".Translate(), printer.Server));
                    result.AddError(e.StackTrace);
                }
                if (!result.IsEmpty) return result;
                {
                    try
                    {
                        if (server != null)
                        {
                            var printDialog = new PrintDialog
                            {
                                PrintQueue = new PrintQueue(server, printer.Name),
                                PrintTicket =
                                {
                                    CopyCount = copies
                                }
                            };
                            var setting = new PrinterContext
                            {
                                Setting = printer.Setting,
                                PrintableAreaWidth = printDialog.PrintableAreaWidth,
                                PrintableAreaHeight = printDialog.PrintableAreaHeight
                            };
                            Trace.TraceInformation($"Print settings {description} {setting.ToLog()}");
                            printDialog.PrintVisual(prepare.PreparePrint(setting), description);
                        }
                    }
                    catch(Exception e)
                    {
                        result.AddError(string.Format("Printer0NotFound".Translate(), printer.Name));
                        result.AddError(e.StackTrace);
                    }
                }
            }
            else result.AddError("PrinterCfgNotValid".Translate());

            return result;
        }

        private ValidationResult PrintDirect(Printer printer, IPreparer<DirectPrint.Printable> prepare, string description, int copies = 1)
        {
			var result = new ValidationResult();
            if (printer != null && printer.ValidationDestination())
            {
                try
                {
                    var serverName = printer.Server.TrimStart('\\');
                    var printerName = $@"\\{serverName.Trim()}\{printer.Name.Trim()}";
                    var device = new DirectPrintDevice(printerName);

					var setting = new PrinterContext
					{
						Setting = printer.Setting,
						// PrintableAreaWidth = printDialog.PrintableAreaWidth
					};

					DirectPrint.Printable printable = prepare.PreparePrint(setting);
                    device.Print(printable);

				}
				catch (Exception ex)
                {
					result.AddError(ex.Message);
				}
			}
			else
				result.AddError("TicketPrinterCfgNotValid".Translate());

			return result;
		}

		public ValidationResult PrintVisual(Printer printer, IPreparer<DocumentPaginator> prepare, string description, int copies = 1)
        {
            var result = new ValidationResult();

            if (printer != null && printer.ValidationDestination())
            {
                try
                {
                    if (string.IsNullOrEmpty(printer.Server))
                        throw new ArgumentException(string.Format("Server0NotFound".Translate(), printer.Server));
                    var server = new PrintServer(printer.Server);

                    if (string.IsNullOrEmpty(printer.Name))
                        throw new ArgumentException(string.Format("Printer0NotFound".Translate(), printer.Name));
                    
                    var printDialog = new PrintDialog();
                    printDialog.PrintQueue = new PrintQueue(server, printer.Name);
                    printDialog.PrintTicket.CopyCount = copies;

                    var setting = new PrinterContext
                    {
                        Setting = printer.Setting,
                        PrintableAreaWidth = printDialog.PrintableAreaWidth
                    };

                    printDialog.PrintDocument(prepare.PreparePrint(setting), description);
                }
                catch (Exception ex)
                {
                    result.AddError(ex.Message);
                    result.AddError(ex.StackTrace);
                }
            }
            else
                result.AddError("TicketPrinterCfgNotValid".Translate());

            return result;
        }
    }

    public interface IPreparer<T>
    {
        T PreparePrint(PrinterContext setting);
    }

    public class PrinterContext
    {
        public double PrintableAreaWidth { get; set; }
        public double PrintableAreaHeight { get; set; }
        public PrinterSetting Setting { get; set; }
        public TicketProductGrouping ProductGrouping { get; set; }

        public string ToLog()
        {
            return $"PrinterWidth:{PrintableAreaWidth}; PrinterHeight:{PrintableAreaHeight}; LeftMargin:{Setting.LeftMargin}; RigthMargin:{Setting.RigthMargin}; TopMargin:{Setting.TopMargin}; BottomMargin:{Setting.BottomMargin}; LineFontSize:{Setting.LineFontSize}";
        }
    }

    public class Printer
    {
        public string Server { get; set; }
        public string Name { get; set; }
        public PrinterSetting Setting { get; set; } = new PrinterSetting();

        public bool ValidationDestination()
        {
            return !string.IsNullOrEmpty(Server) && !string.IsNullOrEmpty(Name);
        }
    }

    public class PrinterSetting
    {
        public int LeftMargin { get; set; } = 2;
        public int RigthMargin { get; set; } = 2;
        public int TopMargin { get; set; } = 2;
        public int BottomMargin { get; set; } = 2;
        public int LineFontSize { get; set; } = 14;

        public int NoteFontSize { get; set; } = 9;
        public int ActionFontSize { get; set; } = 20;
        public int SaloonFontSize { get; set; } = 14;
        public int SeatSeparatorFontSize { get; set; } = 14;
    }
}