﻿using System;
using NewHotel.Contracts;
using NewHotel.Pos.Printer.Interfaces;
using NewHotel.Pos.PrinterDocument.Interfaces;

namespace NewHotel.Pos.PrinterDocument.Fiscal.CuracaoPanama.Curacao
{
    public interface IFiscalPrinterCuracao : IFiscalPrinter
    {

    }

    public class FiscalPrinterCuracao : CommonCuracaoPanama, IFiscalPrinterCuracao
    {
        static FiscalPrinterCuracao()
        {
            // TODO Ver con Mario (old)
            WidgthPaper = 42;
        }

        public override string PrinterDescription => "Fiscal Printer Curacao";

        public override ValidationResult ValidateBuyerInfo(IPrinteableBuyerInfo buyerInfo)
        {
            var result = base.ValidateBuyerInfo(buyerInfo);
            if (result.IsEmpty)
            {
                string fiscalNumber = buyerInfo.FiscalNumber;
                string name = buyerInfo.Name;
                if (buyerInfo.IsDefaultCustomer)
                {
                    if (!string.IsNullOrEmpty(fiscalNumber) || !string.IsNullOrEmpty(name))
                        result.AddError("Invalid final consumer");
                }
                else if (string.IsNullOrEmpty(fiscalNumber) || fiscalNumber.Length != 9)
                    result.AddError("Invalid fiscal number, it has to have nine characters");
            }
            return result;
        }

        protected override string GetObservations(IPrintableDoc ticket)
        {
            return ticket.Observations?.Substring(0, Math.Min(ticket.Observations.Length, 40));
        }
    }
}



