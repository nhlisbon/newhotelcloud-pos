﻿namespace NewHotel.Pos.PrinterDocument.Fiscal.CuracaoPanama.Panama
{
    public interface IFiscalPrinterPanama : IFiscalPrinter
    {
    }

    public class FiscalPrinterPanama : CommonCuracaoPanama, IFiscalPrinterPanama
    {
        static FiscalPrinterPanama()
        {
            // TODO Ver con marcel (old)
            WidgthPaper = 42;
        }

        public override string PrinterDescription => "Fiscal Printer Panamá";
    }
}
