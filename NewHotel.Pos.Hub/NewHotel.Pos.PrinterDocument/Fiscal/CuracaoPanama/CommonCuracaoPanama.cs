﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NewHotel.Contracts;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Printer.Interfaces;
using NewHotel.Pos.PrinterDocument.Interfaces;
using NewHotel.Pos.PrinterDocument.NHFiscalPrinterService;

namespace NewHotel.Pos.PrinterDocument.Fiscal.CuracaoPanama
{
    public abstract class CommonCuracaoPanama : BaseFiscalPrinter
    {
        #region Status

        public override ValidationResult GetPrinterStatus()
        {
            var result = new ValidationResult();
            try
            {
                NHFiscalPrinterServiceClient proxy = GetProxy();
                FPResponseBase response = proxy.CheckConnection();
                result = LoadPrinterResult(response);
            }
            catch (Exception e)
            {
                result.AddError(e.Message);
            }
            return result;
        }

        public override Task<ValidationItemResult<string>> GetServerStatusAsync()
        {
            return Task.Run(GetServerStatus);
        }

        public override ValidationItemResult<string> GetServerStatus()
        {
            var result = new ValidationItemResult<string>();
            try
            {
                NHFiscalPrinterServiceClient proxy = GetProxy();
                result.Item = proxy.GetServiceVersion();
            }
            catch (Exception e)
            {
                result.AddError(e.Message);
            }
            return result;
        }

        public override Task<ValidationResult> GetPrinterStatusAsync()
        {
            return Task.Run(GetPrinterStatus);
        }

        #endregion
        #region Management

        public override ValidationResult ExecuteCommand(string cmd)
        {
            var result = new ValidationResult();
            try
            {
                NHFiscalPrinterServiceClient proxy = GetProxy();
                FPResponseBase response = proxy.ExecuteCommand(cmd);
                result = LoadPrinterResult(response);
            }
            catch (Exception e)
            {
                result.AddError(e.Message);
            }
            return result;
        }

        public override Task<ValidationResult> ExecuteCommandAsync(string cmd)
        {
            return Task.Run(() => ExecuteCommand(cmd));

        }

        public override ValidationResult UploadLastDocumentToFile()
        {
            var result = new ValidationResult();
            try
            {
                NHFiscalPrinterServiceClient proxy = GetProxy();
                FPResponseBase response = proxy.UploadToFile(FPUpload.ulLastDocument, new FPUploadInfo());
                result = LoadPrinterResult(response);
            }
            catch (Exception e)
            {
                result.AddError(e.Message);
            }
            return result;
        }

        public override Task<ValidationResult> UploadLastDocumentToFileAsync()
        {
            return Task.Run(UploadLastDocumentToFile);
        }

        public override ValidationResult UploadDocumentToFile(Document document, DateTime start, DateTime end)
        {
            var result = new ValidationResult();
            try
            {
                FPUpload fpUpload;
                switch (document)
                {
                    case Document.Fiscal:
                        fpUpload = FPUpload.ulFiscalByRangeDate;
                        break;
                    case Document.NonFiscal:
                        fpUpload = FPUpload.ulNonFiscalByRangeDate;
                        break;
                    case Document.XReport:
                        fpUpload = FPUpload.ulXReportByRangeDate;
                        break;
                    case Document.ZReport:
                        fpUpload = FPUpload.ulZReportByRangeDate;
                        break;
                    case Document.Last:
                        fpUpload = FPUpload.ulLastDocument;
                        break;
                    case Document.All:
                        fpUpload = FPUpload.ulAllByDateRange;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(document), document, null);
                }

                NHFiscalPrinterServiceClient proxy = GetProxy();
                var uploadInfo = new FPUploadInfo { dRangeStart = start, dRangeEnd = end };
                FPResponseBase response = proxy.UploadToFile(fpUpload, uploadInfo);
                result = LoadPrinterResult(response);
            }
            catch (Exception e)
            {
                result.AddError(e.Message);
            }
            return result;
        }

        public override Task<ValidationResult> UploadDocumentToFileAsync(Document document, DateTime start, DateTime end)
        {
            return Task.Run(() => UploadDocumentToFile(document, start, end));
        }

        public override ValidationResult UploadDocumentToFile(Document document, long start, long end)
        {
            var result = new ValidationResult();
            try
            {
                FPUpload fpUpload;
                switch (document)
                {
                    case Document.Fiscal:
                        fpUpload = FPUpload.ulFiscalByRangeNumber;
                        break;
                    case Document.NonFiscal:
                        fpUpload = FPUpload.ulNonFiscalByRangeNumber;
                        break;
                    case Document.XReport:
                        fpUpload = FPUpload.ulXReportByRangeNumber;
                        break;
                    case Document.ZReport:
                        fpUpload = FPUpload.ulZReportByRangeNumber;
                        break;
                    case Document.All:
                        fpUpload = FPUpload.ulAllByNumberRange;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(document), document, null);
                }

                NHFiscalPrinterServiceClient proxy = GetProxy();
                var uploadInfo = new FPUploadInfo { iRangeStart = (int)start, iRangeEnd = (int)end };
                FPResponseBase response = proxy.UploadToFile(fpUpload, uploadInfo);
                result = LoadPrinterResult(response);
            }
            catch (Exception e)
            {
                result.AddError(e.Message);
            }
            return result;
        }

        public override Task<ValidationResult> UploadDocumentToFileAsync(Document document, long start, long end)
        {
            return Task.Run(() => UploadDocumentToFile(document, start, end));
        }

        public override ValidationResult LoadFile(byte[] file)
        {
            var result = new FpValidationResult();
            try
            {
                NHFiscalPrinterServiceClient proxy = GetProxy();
                FPResponseBase response = proxy.LoadFile(file);
                return LoadPrinterResult(response, result);
            }
            catch (Exception e)
            {
                result.AddError(e.Message);
            }
            return result;
        }

        public override Task<ValidationResult> LoadFileAsync(byte[] file)
        {
            return Task.Run(() => LoadFile(file));
        }

        #endregion
        #region Printing

        public override Task<FpValidationResult> PrintRefundAsync(IPrintableDoc ticket, string creditNoteNumber)
        {
            return Task.Run(() => PrintRefund(ticket, creditNoteNumber));
        }

        public override FpValidationResult PrintRefund(IPrintableDoc ticket, string creditNoteNumber)
        {
            var result = new FpValidationResult();
            try
            {
                FPDocument doc = CreateFiscalDocument(ticket, FPFiscalDocumentType.fdRefund, creditNoteNumber);
                NHFiscalPrinterServiceClient proxy = GetProxy();
                FPResponseBase response = proxy.PrintDocument(doc);
                return LoadPrinterResult(response, result);
            }
            catch (Exception e)
            {
                result.AddError(e.Message);
            }
            return result;
        }

        public override Task<FpValidationResult> PrintNonFiscalAsync(IPrintableDoc ticket, IPrintDocSetting settings)
        {
            return Task.Run(() => PrintNonFiscal(ticket, settings));
        }

        public override FpValidationResult PrintNonFiscal(IPrintableDoc ticket, IPrintDocSetting settings)
        {
            var result = new FpValidationResult();
            try
            {
                FPDocument doc = CreateNonFiscalDocument(ticket, settings);
                if (doc.NonFiscalLines.Length > 100)
                    result.AddError("Document too long. 100 lines maximum");
                else
                {
                    NHFiscalPrinterServiceClient proxy = GetProxy();
                    FPResponseBase response = proxy.PrintDocument(doc);
                    return LoadPrinterResult(response, result);
                }
            }
            catch (Exception e)
            {
                result.AddError(e.Message);
            }
            return result;
        }

        public override Task<FpValidationResult> PrintFiscalAsync(IPrintableDoc ticket)
        {
            return Task.Run(() => PrintFiscal(ticket));
        }

        public override FpValidationResult PrintFiscal(IPrintableDoc ticket)
        {
            var result = new FpValidationResult();
            try
            {
                result.AddValidations(ValidatePayments(ticket));
                if (result.IsEmpty)
                {
                    var doc = CreateFiscalDocument(ticket, FPFiscalDocumentType.fdReceipt);
                    NHFiscalPrinterServiceClient proxy = GetProxy();
                    FPResponseBase response = proxy.PrintDocument(doc);
                    return LoadPrinterResult(response, result);
                }
            }
            catch (Exception e)
            {
                result.AddError(e.Message);
            }
            return result;
        }

        #region Reports

        public override ValidationResult PrintReport(Report report)
        {
            var result = new FpValidationResult();
            try
            {
                FPReport fpReport;
                switch (report)
                {
                    case Report.XDaily:
                        fpReport = FPReport.frXDaily;
                        break;
                    case Report.XAccumulated:
                        fpReport = FPReport.frXAccumulated;
                        break;
                    case Report.ZDaily:
                        return PrintZReport(true);
                    case Report.ZAccumulated:
                        fpReport = FPReport.frZAccumulated;
                        break;
                    case Report.XClearAccumulated:
                        fpReport = FPReport.frXClearAccumulated;
                        break;
                    case Report.ZNumberRange:
                        fpReport = FPReport.frZByNumberRange;
                        break;
                    case Report.ZDateRange:
                        fpReport = FPReport.frZByDateRange;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(report), report, null);
                }

                NHFiscalPrinterServiceClient proxy = GetProxy();
                FPResponseBase response = proxy.PrintReport(fpReport, new FPReportInfo());
                return LoadPrinterResult(response, result);
            }
            catch (Exception e)
            {
                result.AddError(e.Message);
            }
            return result;
        }

        public override Task<ValidationResult> PrintReportAsync(Report report)
        {
            return Task.Run(() => PrintReport(report));
        }

        public override ValidationResult PrintZReport(bool onPaper)
        {
            var result = new FpValidationResult();
            try
            {
                NHFiscalPrinterServiceClient proxy = GetProxy();
                var rInfo = new FPReportInfo { mode = onPaper ? "0" : "1" };
                FPResponseBase response = proxy.PrintReport(FPReport.frZDaily, rInfo);
                return LoadPrinterResult(response, result);
            }
            catch (Exception e)
            {
                result.AddError(e.Message);
            }
            return result;
        }

        public override Task<ValidationResult> PrintZReportAsync(bool onPaper)
        {
            return Task.Run(() => PrintZReport(onPaper));
        }


        public override ValidationResult PrintZReport(long start, long end)
        {
            var result = new ValidationResult();
            try
            {
                NHFiscalPrinterServiceClient proxy = GetProxy();
                var info = new FPReportInfo
                {
                    iRangeStart = (int)start,
                    iRangeEnd = (int)end
                };
                FPResponseBase response = proxy.PrintReport(FPReport.frZByNumberRange, info);
                result = LoadPrinterResult(response);
            }
            catch (Exception e)
            {
                result.AddError(e.Message);
            }
            return result;
        }

        public override Task<ValidationResult> PrintZReportAsync(long start, long end)
        {
            return Task.Run(() => PrintZReport(start, end));
        }

        public override ValidationResult PrintZReport(DateTime start, DateTime end)
        {
            var result = new ValidationResult();
            try
            {
                NHFiscalPrinterServiceClient proxy = GetProxy();
                var info = new FPReportInfo
                {
                    dRangeStart = start,
                    dRangeEnd = end
                };
                FPResponseBase response = proxy.PrintReport(FPReport.frZByDateRange, info);
                result = LoadPrinterResult(response);
            }
            catch (Exception e)
            {
                result.AddError(e.Message);
            }
            return result;
        }

        public override Task<ValidationResult> PrintZReportAsync(DateTime start, DateTime end)
        {
            return Task.Run(() => PrintZReport(start, end));
        }

        #endregion
        #endregion
        #region Reprint

        public override Task<ValidationResult> ReprintLastDocumentAsync()
        {
            return Task.Run(() => ReprintLastDocument());
        }

        public override ValidationResult ReprintLastDocument()
        {
            try
            {
                NHFiscalPrinterServiceClient proxy = GetProxy();
                FPResponseBase response = proxy.ReprintLastDocument();
                return LoadPrinterResult(response);
            }
            catch (Exception e)
            {
                return new FpValidationResult(e);
            }
        }

        public override Task<ValidationResult> ReprintAsync(Document document, long start, long end)
        {
            return Task.Run(() => Reprint(document, start, end));
        }

        public override ValidationResult Reprint(Document document, long start, long end)
        {
            try
            {
                NHFiscalPrinterServiceClient proxy = GetProxy();
                var range = new NumberRange { Start = start, End = end };
                FPResponseBase response = proxy.ReprintDocumentByNumberRange((FPMemoryDocument)document, range);
                return LoadPrinterResult(response);
            }
            catch (Exception e)
            {
                return new FpValidationResult(e);
            }
        }

        public override Task<ValidationResult> ReprintAsync(Document document, DateTime start, DateTime end)
        {
            return Task.Run(() => Reprint(document, start, end));
        }

        public override ValidationResult Reprint(Document document, DateTime start, DateTime end)
        {
            try
            {
                NHFiscalPrinterServiceClient proxy = GetProxy();
                var range = new DateRange { Start = start, End = end };
                FPResponseBase response = proxy.ReprintDocumentByDateRange((FPMemoryDocument)document, range);
                return LoadPrinterResult(response);
            }
            catch (Exception e)
            {
                return new FpValidationResult(e);
            }
        }

        #endregion
        #region Auxiliars

        protected virtual FPDocument CreateNonFiscalDocument(IPrintableDoc ticket, IPrintDocSetting settings)
        {
            var document = new FPDocument
            {
                Header = new FPDocumentHeader
                {
                    Cashier = ticket.CashierDescription,
                    Type = FPFiscalDocumentType.fdNonfiscal,
                    IsCompanyInvoice = ticket.AccountType == CurrentAccountType.Entities,
                    IsTaxExempt = false,
                }
            };

            var lines = new List<FPNonFiscalLine>();
            foreach (var line in DocLines(ticket, settings, settings.Country))
                lines.Add(new FPNonFiscalLine { Text = line.RemoveDiacritics() });
            document.NonFiscalLines = lines.ToArray();

            return document;
        }

        protected FpValidationResult LoadPrinterResult(FPResponseBase response, FpValidationResult result = null)
        {
            result ??= new FpValidationResult();
            if (response.PrinterErrorCode != 0)
                result.AddError(response.ErrorMessage);
            if (response.LastResult.OperationErrorCode != 0)
                result.AddError(response.LastResult.OperationErrorMessage);
            if (response.LastResult.OperationReturnType != FPReturnType.rtOk)
                result.AddError("Error");

            var register = response.Document?.Register;
            if (register != null)
            {
                var number = register.FPNumber;
                if (!string.IsNullOrEmpty(number))
                    result.DocFiscalNumber = long.Parse(number);
                result.FpSerial = register.FPSerial;
            }

            return result;
        }

        protected virtual FPDocument CreateFiscalDocument(IPrintableDoc ticket, FPFiscalDocumentType type, string creditNoteNumber = null)
        {
            var document = new FPDocument();
            document.Header = new FPDocumentHeader
            {
                Cashier = ticket.CashierDescription.RemoveDiacritics(),
                Type = type,
                Titular = ticket.FiscalDocTo?.RemoveDiacritics(),
                FiscalNumber = ticket.FiscalDocFiscalNumber?.RemoveDiacritics(),
                IsCreditInvoice = !string.IsNullOrEmpty(ticket.FiscalDocTo) && !string.IsNullOrEmpty(ticket.FiscalDocFiscalNumber), // si tengo identificado el cliente
                IsCompanyInvoice = ticket.AccountType == CurrentAccountType.Entities,
                DocumentNumber = creditNoteNumber ?? ticket.FiscalDocNumber.ToString().RemoveDiacritics(),
                IsTaxExempt = false,
                Observations = GetObservations(ticket)
            };

            if (type == FPFiscalDocumentType.fdRefund)
                document.Header.DocumentReference = ticket.FiscalDocNumber.ToString().RemoveDiacritics();

            #region Lines

            var lines = new List<FPDocumentLine>();
            int i = 0;
            foreach (var order in ticket.ProductLines.Where(pl => !pl.IsTip && (pl.AnulAfterClosed || pl.IsActive)))
            {
                var line = new FPDocumentLine
                {
                    Code = i++.ToString(), // TODO Pasar el codigo que tiene en la impresora (old)
                    Description = order.ProductDescription.RemoveDiacritics(),
                    Price = order.ProductPriceBeforeDiscount,
                    Quantity = (int)order.Quantity,
                    TaxCode = order.FirstIvaAuxCode
                };

                var discountRecharges = new List<FPDocumentDiscountRecharge>();
                if (order.DiscountType.HasValue)
                {
                    discountRecharges.Add(new FPDocumentDiscountRecharge
                    {
                        Description = order.DiscountDescription.RemoveDiacritics(),
                        Type = FPFiscalDiscountType.dtDiscount,
                        IsPercent = true,
                        Percent = order.DiscountPercent,
                        isLastItem = true
                    });
                }
                line.DiscountRecharges = discountRecharges.ToArray();

                lines.Add(line);
            }
            document.Lines = lines.ToArray();

            #endregion
            #region Discount/Recharge

            var discountsRecharges = new List<FPDocumentDiscountRecharge>();
            if (ticket.Recharge > 0)
            {
                var recharge = new FPDocumentDiscountRecharge
                {
                    Type = FPFiscalDiscountType.dtRecharge,
                    IsPercent = false,
                    Value = ticket.Recharge
                };
                discountsRecharges.Add(recharge);
            }

            document.DiscountsRecharges = discountsRecharges.ToArray();

            #endregion
            #region Payments

            var payments = new List<FPDocumentPayment>();
            foreach (IPrintablePayment p in ticket.Payments)
            {
                var paym = new FPDocumentPayment
                {
                    Code = p.Type?.AuxiliarCode,
                    Description = p.Description.RemoveDiacritics(),
                    Value = p.ReceiveValue,
                    IsClosePayment = false
                };

                payments.Add(paym);
            }

            payments.Last().IsClosePayment = true;
            document.Payments = payments.ToArray();

            #endregion
            return document;
        }

        protected virtual string GetObservations(IPrintableDoc ticket)
        {
            return ticket.Observations;
        }

        protected virtual NHFiscalPrinterServiceClient GetProxy(string endPointName = "NetTcpBinding_INHFiscalPrinterService")
        {
            return new NHFiscalPrinterServiceClient(endPointName);
        }

        #endregion
    }

    public class FpValidationResult : ValidationResult
    {
        public FpValidationResult()
        {
        }

        public FpValidationResult(Exception ex) : base(ex)
        {
        }

        public long? DocFiscalNumber { get; set; }

        public string FpSerial { get; set; }
    }
}
