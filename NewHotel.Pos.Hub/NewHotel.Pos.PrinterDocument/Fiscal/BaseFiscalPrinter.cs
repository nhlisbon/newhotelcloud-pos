﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewHotel.Contracts;
using NewHotel.Contracts.DataProvider;
using NewHotel.Pos.Localization;
using NewHotel.Pos.Printer.Interfaces;
using NewHotel.Pos.PrinterDocument.Common;
using NewHotel.Pos.PrinterDocument.Fiscal.CuracaoPanama;
using NewHotel.WPF.App.Pos.Controls;

namespace NewHotel.Pos.PrinterDocument.Fiscal
{
    public abstract class BaseFiscalPrinter : IFiscalPrinter
    {
        public static int WidgthPaper { get; set; }

        public abstract Task<ValidationResult> ReprintAsync(Document document, DateTime start, DateTime end);
        public abstract FpValidationResult PrintRefund(IPrintableDoc ticket, string creditNoteNumber);

        public abstract Task<FpValidationResult> PrintRefundAsync(IPrintableDoc ticket, string creditNoteNumber);
        public abstract FpValidationResult PrintFiscal(IPrintableDoc ticket);
        public abstract Task<FpValidationResult> PrintFiscalAsync(IPrintableDoc ticket);
        public abstract FpValidationResult PrintNonFiscal(IPrintableDoc ticket, IPrintDocSetting settings);
        public abstract Task<FpValidationResult> PrintNonFiscalAsync(IPrintableDoc ticket, IPrintDocSetting settings);
        public abstract ValidationItemResult<string> GetServerStatus();
        public abstract Task<ValidationItemResult<string>> GetServerStatusAsync();
        public abstract ValidationResult GetPrinterStatus();
        public abstract Task<ValidationResult> GetPrinterStatusAsync();
        public abstract ValidationResult UploadLastDocumentToFile();
        public abstract Task<ValidationResult> UploadLastDocumentToFileAsync();
        public abstract ValidationResult UploadDocumentToFile(Document document, long start, long end);
        public abstract Task<ValidationResult> UploadDocumentToFileAsync(Document document, long start, long end);
        public abstract ValidationResult UploadDocumentToFile(Document document, DateTime start, DateTime end);
        public abstract Task<ValidationResult> UploadDocumentToFileAsync(Document document, DateTime start, DateTime end);
        public abstract ValidationResult LoadFile(byte[] file);
        public abstract Task<ValidationResult> LoadFileAsync(byte[] file);
        public abstract ValidationResult PrintReport(Report report);
        public abstract Task<ValidationResult> PrintReportAsync(Report report);
        public abstract ValidationResult PrintZReport(bool onPaper);
        public abstract Task<ValidationResult> PrintZReportAsync(bool onPaper);
        public abstract ValidationResult PrintZReport(long start, long end);
        public abstract Task<ValidationResult> PrintZReportAsync(long start, long end);
        public abstract ValidationResult PrintZReport(DateTime start, DateTime end);
        public abstract Task<ValidationResult> PrintZReportAsync(DateTime start, DateTime end);
        public abstract ValidationResult ExecuteCommand(string cmd);
        public abstract Task<ValidationResult> ExecuteCommandAsync(string cmd);
        public abstract ValidationResult ReprintLastDocument();
        public abstract Task<ValidationResult> ReprintLastDocumentAsync();
        public abstract ValidationResult Reprint(Document document, long start, long end);
        public abstract Task<ValidationResult> ReprintAsync(Document document, long start, long end);
        public abstract ValidationResult Reprint(Document document, DateTime start, DateTime end);

        public abstract string PrinterDescription { get; }

        public string GetPrinterDescription(Simple.Printer printer) => PrinterDescription;

        public virtual ValidationResult ValidateBuyerInfo(IPrinteableBuyerInfo buyerInfo)
        {
            return new ValidationResult();
        }

        public ValidationResult ValidatePayments(IPrintableDoc ticket, bool errorWithoutPayments = false)
        {
            var result = new ValidationResult();
            if (ticket.Payments.Any())
            {
                if (ticket.HasCashPayment)
                {
                    if (ticket.HasMealPlanPayment || ticket.HasCreditRoomPayment || ticket.HasHouseUsePayment)
                        result.AddError("Invalid payments");
                }
                else if (errorWithoutPayments)
                    result.AddError("Ticket without payments");
            }

            return result;
        }

        protected IEnumerable<string> DocLines(IPrintableDoc ticket, IPrintDocSetting settings, string country)
        {
            var lines = new List<string>();

            var lookupTable = ticket.LookupTables
                .FirstOrDefault(x => x.Number == ticket.LookupTables.Max(y => y.Number));

            #region Title

            TicketTitles title;
            var invoice = ticket.IsInvoice;
            if (!invoice)
                title = ticket.Payments.Any() ? TicketTitles.Ticket : TicketTitles.Receipt;
            else
                title = ticket.IsBallot ? TicketTitles.Ballot : TicketTitles.Invoice;

            var data = Utils.GetTitleData(title, ticket, country);
            var isReceipt = data.IsReceipt;
            var isInvoice = data.IsInvoice;
            var isTicket = data.IsTicket;
            var showLookupTableSerie = data.IsReceipt;
            var showPayments = !data.IsReceipt;

            lines.AddCentredLine(data.TitleDescription);
            if (data.IsCopy)
                lines.AddCentredLine($"({"Copy".Translate()})");
            if (ticket.IsAnul)
                lines.AddCentredLine("Canceled".Translate());

            #endregion
            #region Customize Header

            if (!string.IsNullOrEmpty(settings.PrintMessage1))
                lines.AddCentredLine(settings.PrintMessage1);

            #endregion
            #region Customer Details

            if (!ticket.IsTicket)
            {
                var sb = new StringBuilder();
                sb.AppendLine($"{"Soldto".Translate()}: {ticket.FiscalDocTo}");
                if (!string.IsNullOrEmpty(ticket.FiscalDocAddress))
                    sb.AppendLine($"{"Address".Translate()}: {ticket.FiscalDocAddress}{Environment.NewLine}");
                if (!string.IsNullOrEmpty(ticket.FiscalDocEmail))
                    sb.AppendLine($"{"Email".Translate()}: {ticket.FiscalDocEmail}");
                sb.AppendLine($"{ticket.FiscalIdenTypeName}: {ticket.FiscalDocFiscalNumber}");
                lines.AddCentredLine(sb.ToString());
            }

            #endregion
            #region Fixed Header

            lines.AddLine("~Stand~:", ticket.StandDescription);
            lines.AddLine("~Cashier~:", ticket.CashierDescription);
            lines.AddLine($"~Turn~: {ticket.Shift}", $"~Table~: {ticket.TableDescription}", $"~Paxs~: {ticket.Paxs}");

            if (isInvoice)
                lines.AddLine("~Invoice~:", $"{(ticket.FiscalDocNumber ?? 0)}/{ticket.FiscalDocSeriePrex}");
            else
            {
                string label = showLookupTableSerie ? "~Number~:" : "~TicketNo~:";
                lines.AddLine(label, $"{(ticket.Serie ?? 0)}/{ticket.SeriePrex}");
            }


            if (showLookupTableSerie)
            {
                lines.AddLine("~Receipt~:", $"{(lookupTable.Number ?? 0)}/{lookupTable.Serie}");
            }

            lines.AddLine("~User~:", ticket.UserDescription);

            string dt = "dd/MM/yyyy", tf = "HH:mm";
            if (settings.PrintOpeningTime)
            {
                var date = ticket.CheckOpeningDate.ToString(dt);
                var time = ticket.CheckOpeningTime.ToString(tf);
                lines.AddLine("~Open~:", $"{date} {time}");
            }

            if (settings.PrintCloseDateTime && ticket.CloseDate.HasValue && ticket.CloseTime.HasValue)
            {
                var date = ticket.CloseDate.Value.ToString(dt);
                var time = ticket.CloseTime.Value.ToString(tf);
                lines.AddLine("~Close~: ", $"{date} {time}");
            }

            #endregion
            #region Clean line

            lines.AddCleanLine();

            #endregion
            #region Column captions

            if (settings.PrintColumnCaptions)
                lines.AddLine("Qty", "Description", "Value");

            #endregion
            #region Product List
            
            IEnumerable<OrderLite> orders = Utils.OrderLites(ticket, settings);
            foreach (var order in orders)
            {
                lines.AddLine(order.Quantity, order.ProductDescription, order.ProductPriceBeforeDiscount);
                if (settings.PrintTaxDetailsPerLine)
                {
                    lines.AddLine(order.FirstIvaDescription, (order.FirstIvas ?? 0));
                    if (order.SecondIvas.HasValue)
                        lines.AddLine(order.SecondIvaDescription, order.SecondIvas.Value);
                    if (order.ThirdIvas.HasValue)
                        lines.AddLine(order.ThirdIvaDescription, order.ThirdIvas.Value);
                }

                if (order.ShowDiscountInfo)
                {
                    lines.AddLine(string.Empty, $"{order.DiscountPercent}% off ({order.DiscountLabel}) = {order.DiscountValue}");
                    lines.AddLine(string.Empty, order.Cost);
                }

                if (!string.IsNullOrEmpty(order.Comments))
                {
                    lines.AddLine(order.Comments);
                    lines.AddCleanLine();
                }
            }
            #endregion
            #region Clean line

            lines.AddCleanLine();

            #endregion
            #region Outline

            if (settings.PrintSubtotal)
            {
                string label = "Subtotal";

                switch (settings.SubTotalToPrint)
                {
                    case KindSubTotal.Gross:
                        lines.AddLine(label, ticket.GrossSubTotalAmount);
                        break;
                    case KindSubTotal.Net:
                        lines.AddLine(label, ticket.NetSubTotalAmount);
                        break;
                }
            }

            #endregion
            #region Discounts

            if (!settings.SuppressDiscountWhenCero || ticket.GeneralDiscount > 0)
                lines.AddLine("Discount", ticket.GeneralDiscount);

            #endregion
            #region Tips

            if (settings.PrintTips)
                lines.AddLine(ticket.TipDescription, ticket.TipAmount);

            #endregion
            #region Clean line

            lines.AddCleanLine();

            #endregion
            #region Normal taxes

            // Ocultar impuestos para consulta de mesa con impuestos incluidos
            bool hideTax = isReceipt && ticket.TaxIncluded;
            if (!hideTax && !settings.PrintTaxDetails)
                lines.AddLine("Taxes", ticket.TaxAmount);

            #endregion
            #region Details taxes

            if (settings.PrintTaxDetails)
            {
                var taxes = Common.Utils.TaxGroups(ticket);
                lines.AddLine("Tax", "Incidence", "Value");
                foreach (var tax in taxes)
                    lines.AddLine(tax.TaxRateDescription, tax.TaxBase, tax.TaxValue);
            }

            #endregion
            #region Clean line

            lines.AddCleanLine();

            #endregion
            #region Total

            lines.AddLine("Total", ticket.TotalAmount);

            #endregion
            #region Receipt payment

            if ((isReceipt && (settings.PrintCustomerSignature || settings.PrintPaymentOptions)) ||
                settings.PrintCustomerSignature)
            {
                if (isReceipt && settings.PrintPaymentOptions)
                {
                    lines.AddLine("[    ]  ~Cash~");
                    lines.AddLine("[    ]  ~CreditCard~");
                    lines.AddLine("[    ]  ~Credit~");
                    lines.AddLine("[    ]  ~Houseuse~");
                }
                lines.AddLine("Room");
                lines.AddLine("Name");
                if (settings.PrintCustomerSignature)
                    lines.AddLine("Signature");
            }

            #endregion
            #region Clean line

            lines.AddCleanLine();

            #endregion
            #region Payments

            if (showPayments)
            {
                lines.AddLine("PaymentTotal", ticket.PaymentTotalAmount);
                lines.AddLine("Change", ticket.ChangeAmount);
                if (settings.PrintTips)
                    lines.AddLine("TipDescription", ticket.TipAmount);
            }

            #region Details

            if (!isReceipt && settings.PrintPaymentDetails)
            {
                foreach (var payment in ticket.Payments)
                {
                    lines.AddCentredLine(payment.TireDescription);
                    if (payment.WideDetails)
                        lines.AddLine(payment.Details, payment.ReceiveValue);
                    else
                    {
                        lines.AddLine(payment.Description, payment.ReceiveValue);
                        if (ticket.Account.HasValue)
                            lines.AddLine("~Sig~:");
                    }
                }
            }

            #endregion

            #endregion
            #region TipSuggestion

            if (settings.TipSuggestion)
            {
                var suggestions = Utils.ProposalSuggestions(settings, ticket);
                foreach (var suggestion in suggestions)
                {
                    var description = string.Empty;
                    if (suggestion.Autogenerate && suggestion.Proposal.HasValue)
                        description = suggestion.Proposal.Value.ToString();
                    lines.AddLine("~Description~:", description);
                }
            }

            #endregion
            #region Comments

            if (settings.ShowCommentsTicket && !string.IsNullOrEmpty(ticket.Observations))
                lines.AddLine(ticket.Observations);

            #endregion
            #region Clean line

            lines.AddCleanLine();

            #endregion
            #region Customize Footer

            if (!string.IsNullOrEmpty(settings.PrintMessage2))
                lines.AddLine(settings.PrintMessage2);

            #endregion
            #region Signature

            if (isInvoice)
            {
                lines.AddCentredLine(new string('-', WidgthPaper - 5));
                lines.AddCentredLine(ticket.FiscalDocSignature);
            }

            if (isTicket && !string.IsNullOrEmpty(ticket.TicketSignature))
            {
                lines.AddCentredLine(new string('-', WidgthPaper - 5));
                lines.AddCentredLine(ticket.TicketSignature);
            }

            if (isReceipt && lookupTable != null && !string.IsNullOrEmpty(lookupTable.SignedLabel))
            {
                lines.AddCentredLine(new string('-', WidgthPaper - 5));
                lines.AddLine(lookupTable.SignedLabel);

            }

            #endregion
            #region Date

            lines.AddCentredLine(DateTime.Now.ToString("dd/MM/yy hh:mm:ss tt"));

            #endregion

            return lines;
        }
    }

    public static class ExtLocal
    {
        public static void AddLine(this List<string> nonFiscalLines, string text)
        {
            foreach (var item in Split((text ?? "").Translate().RemoveDiacritics(), BaseFiscalPrinter.WidgthPaper))
                nonFiscalLines.Add(item);
        }

        public static void AddLine(this List<string> nonFiscalLines, params object[] items)
        {
            string text = "";

            string[] sItems = items.Select(x =>
            {
                switch (x)
                {
                    case string s:
                        return s.Translate();
                    case decimal d:
                        return d.Round();
                    case null:
                        return "";
                }

                return x.ToString();
            }).ToArray();

            int filled = sItems.Sum(x => x.Length);
            int free = BaseFiscalPrinter.WidgthPaper - filled;
            int block = free / (items.Length - 1);

            for (int i = 0; i < sItems.Length - 1; i++)
            {
                text += sItems[i];
                text += new string(' ', block);
            }
            if (sItems.Length > 0)
            {
                string last = sItems[sItems.Length - 1];
                int ws = BaseFiscalPrinter.WidgthPaper - (text.Length + last.Length);
                text += new string(' ', ws) + last;
            }


            nonFiscalLines.Add(RemoveDiacritics(text));
        }

        public static void AddCentredLine(this List<string> nonFiscalLines, string text)
        {
            text = (text ?? "").Translate().RemoveDiacritics();
            foreach (var item in Split(text, BaseFiscalPrinter.WidgthPaper))
            {
                int free = BaseFiscalPrinter.WidgthPaper - item.Length;
                int block = free / 2;
                string line = new string(' ', block);
                line += item;
                line += new string(' ', block);
                nonFiscalLines.Add(RemoveDiacritics(line));
            }
        }

        public static void AddCleanLine(this List<string> nonFiscalLines)
        {
            nonFiscalLines.Add(" ");
        }

        public static char[] WS = { '\n', '\r', '\t', 'º', '\'', '&' };
        public static string RemoveDiacritics(this string text)
        {
            if (text == null)
                return text;

            foreach (var ws in WS)
                text = text.Replace(ws, ' ');

            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (char c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                    stringBuilder.Append(c);
            }
            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        private static IEnumerable<string> Split(string s, int length)
        {
            do
            {
                int l = Math.Min(length, s.Length);
                yield return s.Substring(0, l);
                s = s.Substring(l);
            } while (s.Length > 0);
        }


        public static string Round(this decimal value, int decimals = 2)
        {
            return Math.Round(value, decimals).ToString(CultureInfo.CurrentCulture);
        }
    }
}
