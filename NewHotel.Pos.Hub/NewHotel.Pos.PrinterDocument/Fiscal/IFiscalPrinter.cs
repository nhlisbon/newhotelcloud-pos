﻿using System;
using System.Threading.Tasks;
using NewHotel.Contracts;
using NewHotel.Pos.Printer.Interfaces;
using NewHotel.Pos.PrinterDocument.Fiscal.CuracaoPanama;
using IPrinter = NewHotel.Pos.PrinterDocument.Interfaces.IPrinter;

namespace NewHotel.Pos.PrinterDocument.Fiscal
{
    public interface IFiscalPrinter : IPrinter
    {
        ValidationResult ValidateBuyerInfo(IPrinteableBuyerInfo buyerInfo);

        ValidationResult ValidatePayments(IPrintableDoc ticket, bool errorWithoutPayments = false);

        ValidationResult ExecuteCommand(string cmd);
        Task<ValidationResult> ExecuteCommandAsync(string cmd);

        ValidationResult ReprintLastDocument();
        Task<ValidationResult> ReprintLastDocumentAsync();

        ValidationResult Reprint(Document document, long start, long end);
        Task<ValidationResult> ReprintAsync(Document document, long start, long end);

        ValidationResult Reprint(Document document, DateTime start, DateTime end);
        Task<ValidationResult> ReprintAsync(Document document, DateTime start, DateTime end);

        FpValidationResult PrintRefund(IPrintableDoc ticket, string creditNoteNumber);
        Task<FpValidationResult> PrintRefundAsync(IPrintableDoc ticket, string creditNoteNumber);

        FpValidationResult PrintFiscal(IPrintableDoc ticket);
        Task<FpValidationResult> PrintFiscalAsync(IPrintableDoc ticket);

        FpValidationResult PrintNonFiscal(IPrintableDoc ticket, IPrintDocSetting settings);
        Task<FpValidationResult> PrintNonFiscalAsync(IPrintableDoc ticket, IPrintDocSetting settings);

        ValidationItemResult<string> GetServerStatus();
        Task<ValidationItemResult<string>> GetServerStatusAsync();

        ValidationResult GetPrinterStatus();
        Task<ValidationResult> GetPrinterStatusAsync();

        ValidationResult UploadLastDocumentToFile();
        Task<ValidationResult> UploadLastDocumentToFileAsync();

        ValidationResult UploadDocumentToFile(Document document, long start, long end);
        Task<ValidationResult> UploadDocumentToFileAsync(Document document, long start, long end);

        ValidationResult UploadDocumentToFile(Document document, DateTime start, DateTime end);
        Task<ValidationResult> UploadDocumentToFileAsync(Document document, DateTime start, DateTime end);

        ValidationResult LoadFile(byte[] file);
        Task<ValidationResult> LoadFileAsync(byte[] file);

        ValidationResult PrintReport(Report report);
        Task<ValidationResult> PrintReportAsync(Report report);


        ValidationResult PrintZReport(bool onPaper);
        Task<ValidationResult> PrintZReportAsync(bool onPaper);

        ValidationResult PrintZReport(long start, long end);
        Task<ValidationResult> PrintZReportAsync(long start, long end);

        ValidationResult PrintZReport(DateTime start, DateTime end);
        Task<ValidationResult> PrintZReportAsync(DateTime start, DateTime end);
    }

    public enum Document
    {
        Fiscal,
        NonFiscal,
        XReport,
        ZReport,
        Last,
        All
    }

    public enum Report
    {
        XDaily,
        XAccumulated,
        ZDaily,
        ZAccumulated,
        XClearAccumulated,
        ZNumberRange,
        ZDateRange
    }
}