﻿using System.Windows.Documents;
using System.Windows.Media;
using NewHotel.Contracts;
using NewHotel.Pos.PrinterDocument.Simple;

namespace NewHotel.Pos.PrinterDocument.Interfaces
{
    public interface IVisualPrinter
    { 
        ValidationResult PrintVisual(Simple.Printer printer, IPreparer<Visual> preparer, string description, int copies = 1);
        ValidationResult PrintVisual(Simple.Printer printer, IPreparer<DocumentPaginator> preparer, string description, int copies = 1);
    }
}
