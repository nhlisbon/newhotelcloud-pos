﻿using NewHotel.Contracts;
using NewHotel.Pos.Printer.Interfaces;
using NewHotel.Pos.PrinterDocument.Simple.Visual;

namespace NewHotel.Pos.PrinterDocument.Interfaces
{
    public interface ITicketPrinter : IPrinter
    {
        ValidationResult PrintKitchenDoc(Simple.Printer printer, KitchenDoc doc);
        ValidationResult PrintInvoice(Simple.Printer printer, IPrintableDoc invoice, IPrintDocSetting settings);
        ValidationResult PrintCreditNote(Simple.Printer printer, IPrintableDoc creditNote, IPrintDocSetting settings);
        ValidationResult PrintBallot(Simple.Printer printer, IPrintableDoc ballot, IPrintDocSetting settings);
        ValidationResult PrintTicket(Simple.Printer printer, IPrintableDoc ticket, IPrintDocSetting settings);
        ValidationResult PrintLookupTable(Simple.Printer printer, IPrintableDoc bill, IPrintDocSetting settings);
    }
}