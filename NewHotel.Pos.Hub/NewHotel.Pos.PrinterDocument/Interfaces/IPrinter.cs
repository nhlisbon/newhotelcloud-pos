﻿using NewHotel.Pos.PrinterDocument.Simple;

namespace NewHotel.Pos.PrinterDocument.Interfaces
{
    public interface IPrinter
    {
        string GetPrinterDescription(Simple.Printer printer);
    }
}
