﻿using NewHotel.Communication.Interfaces;
using NewHotel.Communication.Services;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;
using System;
using System.Collections.Generic;

namespace NewHotel.Communication.Client
{
    public class NewHotelPosClient : ApplicationClient, INewHotelPosClient
    {
        #region Constants

        private const string NEWHOTELPOSMANAGEMENT = "NewHotelPosManagement";
        private const string NEWHOTELMANAGEMENT = "NewHotelPosManagement";
        private const string SETTINGMANAGEMENT = "SettingManagement";
        private const string EXTERNALMANAGEMENT = "ExternalManagement";
        private const string NEWHOTELCOMMON = "NewHotelCommon";
        private const string APPMANAGEMENT = "AppManagement";
        private const string NEWHOTELPMSMANAGEMENT = "NewHotelManagement";

        #endregion

        #region Properties

        #endregion

        #region Constructors

        public NewHotelPosClient(IRequestSessionData requestSessionData, ContextData contextData)
            : base(requestSessionData, contextData)
        {
        }

        public NewHotelPosClient(IRequestSessionData requestSessionData, ContextData contextData, string baseHostAddress)
            : base(requestSessionData, contextData, baseHostAddress)
        {
        }

        #endregion

        #region Protected Methods
        protected override string GetServiceName(string name)
        {
            switch (name)
            {
                case NEWHOTELCOMMON:
                    return SETTINGMANAGEMENT;
                case EXTERNALMANAGEMENT:
                    return EXTERNALMANAGEMENT;
                case APPMANAGEMENT:
                    return APPMANAGEMENT;
                case NEWHOTELPMSMANAGEMENT:
                    return NEWHOTELPMSMANAGEMENT;
            }

            return NEWHOTELMANAGEMENT;
        }

        protected override string GetAddress(string url, string name)
        {
            switch (name)
            {
                case SETTINGMANAGEMENT:
                    {
                        if (url.Contains("Pos"))
                        {
                            url = url.Replace("Pos/", "");
                            return string.Format("{0}Common/{1}/{1}.svc", url, name);
                        }
                        return string.Format("{0}Common/{1}/{1}.svc", url, name);
                    };
                case EXTERNALMANAGEMENT:
                    {
                        if (url.Contains("Pos"))
                        {
                            url = url.Replace("Pos/", "");
                        }
                        return string.Format("{0}Pms/Services/{1}/{1}.svc", url, name);
                    };
                case APPMANAGEMENT:
                    {
                        if (url.Contains("Pos"))
                        {
                            url = url.Replace("Pos/", "");
                        }
                        return string.Format("{0}Services/{1}/{1}.svc", url, name);
                    };
                case NEWHOTELPMSMANAGEMENT:
                    {
                        if (url.Contains("Pos"))
                        {
                            url = url.Replace("Pos/", "");
                        }
                        return string.Format("{0}Pms/Services/{1}/{1}.svc", url, name);
                    };
            }

            if (url.Contains("Pos"))
                return string.Format("{0}Services/{1}/{1}.svc", url, name);
            else
                return string.Format("{0}Pos/Services/{1}/{1}.svc", url, name);
        }

        #endregion

        #region Private Methods

        protected ObjectClient<INewHotelPosManagement> GetNewHotelManagement()
        {
            ObjectClient<INewHotelPosManagement> proxy = GetProxy<INewHotelPosManagement>();
            proxy.CustomOperationTimeOut = new TimeSpan(0, 1, 0);
            return proxy;
        }

        protected ObjectClient<ISettingManagement> GetSettingManagement()
        {
            ObjectClient<ISettingManagement> proxy = GetProxy<ISettingManagement>(NEWHOTELCOMMON);
            proxy.CustomOperationTimeOut = new TimeSpan(0, 1, 0);
            return proxy;
        }

        protected ObjectClient<IAppManagement> GetAppManagement()
        {
            ObjectClient<IAppManagement> proxy = GetProxy<IAppManagement>(APPMANAGEMENT);
            proxy.CustomOperationTimeOut = new TimeSpan(0, 1, 0);
            return proxy;
        }

        #endregion

        #region Public Methods

        #region Dummy

        public void DummyAsync(EventHandler<ClientEventArgs> callback)
        {
            ExecuteAsync<INewHotelPosManagement>("Dummy", callback, null);
        }

        #endregion

        #endregion

        #region Administration
        public void LoadNewHotelContextAsync(string username, Guid installationId, long languageId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadNewHotelContext", callback, null, username, installationId, languageId);
        }

        public void ClearNewHotelContextAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "ClearNewHotelContext", callback, null, null);
        }

        public void LoadWorkDateAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadWorkDate", callback, null);
        }

        public void CloseContextAsync(int context, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "CloseContext", callback, null, context);
        }

        public void LoadDatabaseVersionAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadDatabaseVersion", callback, null);
        }

        public void NewNPosSettingsAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "NewNPosSettings", callback, null, id);
        }
        public void LoadNPosSettingsAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "LoadNPosSettings", callback, null, id);
        }
        public void UpdateNPosSettingsAsync(SettingNPosContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "UpdateNPosSettings", callback, null, obj);
        }
        #endregion

        #region Settings Operation
        #region AppClasifier
        public void GetApplicationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetApplications", callback, null, req);
        }
        public void GetAppClasifierApplicationsAsync(QueryRequest req, Guid ownerId, string ownerType, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetAppClasifierApplications", callback, null, req, ownerId, ownerType);
        }
        public void AddAppClasifierApplicationsAsync(Guid ownerId, string ownerType, long[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddAppClasifierApplications", callback, null, ownerId, ownerType, ids);
        }
        public void RemoveAppClasifierApplicationsAsync(Guid ownerId, string ownerType, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "RemoveAppClasifierApplications", callback, null, ownerId, ownerType, ids);
        }
        #endregion

        public void GetPaymentLinesByTicketAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetPaymentLinesByTicket", callback, null, req, ownerId);
        }

        #region ProductLine

        public void GetProductLinesByTicketAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductLinesByTicket", callback, null, req, ownerId);
        }

        public void NewProductLineAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewProductLine", callback, null);
        }
        public void LoadProductLineAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadProductLine", callback, null, id);
        }
        public void UpdateProductLineAsync(ProductLineContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateProductLine", callback, null, obj);
        }
        #endregion
        #region Products
        public void GetProductsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProducts", callback, null, req);
        }
        public void GetProductsByCodisAsync(List<string> codis, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductsByCodis", callback, null, codis);
        }
        public void LoadProductAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadProduct", callback, null, id);
        }
        public void NewProductAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewProduct", callback, null);
        }
        public void UpdateProductAsync(ProductContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateProduct", callback, null, obj);
        }
        public void UpdateProductbyDispatchAreasAsync(ProductContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateProductbyDispatchAreas", callback, null, obj);
        }
        public void DeleteProductAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteProduct", callback, null, ids);
        }
        public void InactiveProductAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "InactiveProduct", callback, null, ids);
        }
        public void UpdateProductGroupCategoryAsync(Guid[] ids, Guid groupId, long categoryId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateProductGroupCategory", callback, null, ids, groupId, categoryId);
        }
        public void SetProductsServiceAsync(Guid? serviceId, Guid[] ids, Guid standId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "SetProductsService", callback, null, serviceId, ids, standId);
        }
        public void SetProductstoStandAsync(Guid standId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "SetProductstoStand", callback, null, standId, ids);
        }
        public void GetGroupProductsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetGroupProducts", callback, null, req);
        }
        public void LoadGroupProductAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadGroupProduct", callback, null, id);
        }
        public void NewGroupProductAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewGroupProduct", callback, null);
        }

        public void UpdateGroupProductAsync(GroupProductContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateGroupProduct", callback, null, obj);
        }
        public void DeleteGroupProductAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteGroupProduct", callback, null, ids);
        }

        public void GetProductFamiliesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductFamilies", callback, null, req);
        }
        public void LoadProductFamilyAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadProductFamily", callback, null, id);
        }
        public void NewProductFamilyAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewProductFamily", callback, null);
        }
        public void UpdateProductFamilyAsync(ProductFamilyContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateProductFamily", callback, null, obj);
        }
        public void DeleteProductFamilyAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteProductFamily", callback, null, ids);
        }

        public void GetProductSubFamiliesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductSubFamilies", callback, null, req);
        }
        public void LoadProductSubFamilyAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadProductSubFamily", callback, null, id);
        }
        public void NewProductSubFamilyAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewProductSubFamily", callback, null);
        }
        public void UpdateProductSubFamilyAsync(ProductSubFamilyContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateProductSubFamily", callback, null, obj);
        }
        public void DeleteProductSubFamilyAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteProductSubFamily", callback, null, ids);
        }

        public void GetSeparatorsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetSeparators", callback, null, req);
        }
        public void LoadSeparatorAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadSeparator", callback, null, id);
        }
        public void NewSeparatorAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewSeparator", callback, null);
        }
        public void UpdateSeparatorAsync(SeparatorContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateSeparator", callback, null, obj);
        }
        public void DeleteSeparatorAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteSeparator", callback, null, ids);
        }
        public void MoveUpSeparator(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "MoveUpSeparator", callback, null, id);
        }

        public void MoveDownSeparator(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "MoveDownSeparator", callback, null, id);
        }

        public void GetProductAdditionalsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductAdditionals", callback, null, req, ownerId);
        }
        public void RemoveProductAdditionalsAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteProductAdditionals", callback, null, ownerId, ids);
        }
        public void AddAdditionalProductAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddAdditionalProduct", callback, null, ownerId, ids);
        }

        public void GetProductPriceRateByProductAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductPriceRatesByProduct", callback, null, req, ownerId);
        }

        public void RemoveProductPriceRatesFromProductAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteProductPriceRateFromProduct", callback, null, ownerId, ids);
        }

        public void AddPriceRatesProductAsync(Guid ownerId, ProductPriceRateContract contract, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddPriceRateProduct", callback, null, ownerId, contract);
        }
        public void LoadPriceRateProductAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadProductPriceRateByProduct", callback, null, id);
        }
        public void NewPriceRateProductAsync(Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewProductPriceRateByProduct", callback, null, ownerId);
        }

        public void GetProductImagesAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductImages", callback, null, req, ownerId);
        }
        public void RemoveProductImagesAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteProductImages", callback, null, ownerId, ids);
        }

        public void LoadImageProductAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadImageProduct", callback, null, id);
        }
        public void NewImageProductAsync(Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewImageProduct", callback, null, ownerId);
        }
        public void UpdateImageProductAsync(ProductImageContract image, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateImageProduct", callback, null, image, ownerId);
        }

        public void GetProductBarCodesAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductBarCodes", callback, null, req, ownerId);
        }
        public void RemoveProductBarCodesAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteProductBarCode", callback, null, ownerId, ids);
        }
        public void LoadBarCodeProductAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadProductBarCode", callback, null, id);
        }
        public void NewBarCodeProductAsync(Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewProductBarCode", callback, null, ownerId);
        }
        public void UpdateBarCodeProductAsync(ProductBarCodeContract barCode, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateProductBarCode", callback, null, barCode, ownerId);
        }

        public void GetProductTablesAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductTables", callback, null, req, ownerId);
        }
        public void RemoveProductTablesAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteProductTable", callback, null, ownerId, ids);
        }
        public void LoadTableProductAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadProductTable", callback, null, id);
        }
        public void NewTableProductAsync(Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewProductTable", callback, null, ownerId);
        }
        public void UpdateTableProductAsync(TableProductContract tableProduct, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateProductTable", callback, null, tableProduct, ownerId);
        }
        public void AddAddTableToProductAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddTableToProduct", callback, null, ownerId, ids);
        }
        public void UpdateProductTableValueAsync(Guid ownerId, decimal quantity, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateProductTableValue", callback, null, ownerId, quantity);
        }

        public void RemoveStandProductFromProductAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteStandProductFromProduct", callback, null, ownerId, ids);
        }
        public void AddStandProductToProductAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddStandProductToProduct", callback, null, ownerId, ids);
        }

        public void GetProductsStandbyAreaAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "ProductsStandbyArea", callback, null, req);
        }

        #endregion
        #region Menu
        public void GetMenusAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetMenus", callback, null, req);
        }
        public void NewMenuAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewMenu", callback, null, null);
        }
        public void LoadMenuAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadMenu", callback, null, id);
        }
        public void UpdateMenuAsync(MenuContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateMenu", callback, null, obj);
        }
        public void DeleteMenuAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteMenu", callback, null, ids);
        }
        #endregion
        #region Caja
        public void GetCajasAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetCajas", callback, null, req);
        }

        public void LoadCajaAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadCaja", callback, null, id);
        }

        public void NewCajaAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewCaja", callback, null);
        }

        public void UpdateCajaAsync(CajaContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateCaja", callback, null, obj);
        }

        public void DeleteCajaAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteCaja", callback, null, ids);
        }

        public void RemoveStandCajasFromCajaAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteStandCajasFromCaja", callback, null, ownerId, ids);
        }
        public void AddStandCajasToCajaAsync(CajaContract cajaContract, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddStandCajaToCaja", callback, null, cajaContract, ids);
        }
        public void CheckStandCajasSeriesAsync(Guid standId, Guid cajaId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "CheckStandCajasSeries", callback, null, standId, cajaId);
        }
        public void ValidateSerieUnicityAsync(StandCajasContract contract, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "ValidateSerieUnicity", callback, null, contract);
        }
        #endregion
        #region Duty
        public void GetDutiesByPeriodAsync(QueryRequest req, DateTime initial, DateTime final, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetDutiesByPeriod", callback, null, req, initial, final);
        }

        public void GetDutiesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetDuties", callback, null, req);
        }

        public void LoadDutyAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadDuty", callback, null, id);
        }

        public void NewDutyAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewDuty", callback, null);
        }

        public void UpdateDutyAsync(DutyContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateDuty", callback, null, obj);
        }

        public void DeleteDutyAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteDuty", callback, null, ids);
        }
        #endregion
        #region Mesas
        public void GetSaloonsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetSaloons", callback, null, req);
        }
        public void GetUsedMesasAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetUsedMesas", callback, null, req);
        }
        public void GetMesasAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetMesas", callback, null, req);
        }
        public void GetSaloonsByStandAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetSaloonsByStand", callback, null, req);
        }
        public void GetAllSaloonStandsAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetAllSaloonStands", callback, null);
        }
        public void LoadSaloonAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadSaloon", callback, null, id);
        }
        public void NewSaloonAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewSaloon", callback, null);
        }
        public void UpdateSaloonAsync(SaloonContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateSaloon", callback, null, obj);
        }
        public void DeleteSaloonAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteSaloon", callback, null, ids);
        }

        public void LoadMesaAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadMesa", callback, null, id);
        }
        public void NewMesaAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewMesa", callback, null);
        }
        public void UpdateMesaAsync(MesaContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateMesa", callback, null, obj);
        }
        public void DeleteMesaAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteMesa", callback, null, ids);
        }
        #endregion


        #region TablesGroups

        public void GetAllTablesGroupsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetTablesGroups", callback, null, req);
        }

        public void LoadTablesGroupAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadTablesGroup", callback, null, id);
        }

        public object LoadTablesGroupSync(Guid id)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "LoadTablesGroup", null, null, id);
        }

        public void NewTablesGroupAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewTablesGroup", callback, null);
        }
        public void UpdateTablesGroupAsync(TablesGroupContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateTablesGroup", callback, null, obj);
        }
        public void DeleteTablesGroupAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteTablesGroup", callback, null, ids);
        }
        #endregion

        #region TablesReservationPeriods


        public void GetTablesReservationPeriodsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetTablesReservationPeriods", callback, null, req);
        }

        public void LoadTablesReservationPeriodAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadTablesReservationPeriod", callback, null, id);
        }
        public void NewTablesReservationPeriodAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewTablesReservationPeriod", callback, null);
        }
        public void UpdateTablesReservationPeriodAsync(TablesReservationPeriodContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateTablesReservationPeriod", callback, null, obj);
        }
        public void DeleteTablesReservationPeriodAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteTablesReservationPeriod", callback, null, ids);
        }
        #endregion

        #region TablesReservationLines

        public object GetTablesReservationAvailabilityAsync(TablesReservationPeriodRecord period, DateTime date, Guid? stand, int paxs, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "GetTablesReservationAvailability", callback, null, period, date, stand, paxs);
        }

        public void GetAvailableTablesAsync(TablesReservationLineContract reservation, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetAvailableTables", callback, null, reservation);
        }

        public void GetAvailableTablesGroupsAsync(TablesReservationLineContract reservation, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetAvailableTablesGroups", callback, null, reservation);
        }

        public void GetUnavailableTablesAsync(TablesReservationLineContract reservation, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetUnavailableTables", callback, null, reservation);
        }

        public void CancelTablesReservationAsync(Guid[] ids, CancellationControlContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "CancelTablesReservation", callback, null, ids, obj);
        }

        public void LoadTablesReservationLineAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadTablesReservationLine", callback, null, id);
        }

        public object LoadTablesReservationLineSync(Guid id)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "LoadTablesReservationLine", null, null, id);
        }

        public void NewTablesReservationLineAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewTablesReservationLine", callback, null);
        }

        public void UpdateTablesReservationLineAsync(TablesReservationLineContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateTablesReservationLine", callback, null, obj);
        }

        public ValidationContractResult UpdateTablesReservationLine(TablesReservationLineContract line)
        {
            var proxy = GetNewHotelManagement();
            return (ValidationContractResult) Invoke(proxy, "UpdateTablesReservationLine", null, null, line);
        }

        public ValidationContractResult PersistTablesReservationLine(TablesReservationLineContract line)
        {
            var proxy = GetNewHotelManagement();
            return (ValidationContractResult) Invoke(proxy, "UpdateTablesReservationLine", null, null, line);
        }

        public void DeleteTablesReservationLineAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteTablesReservationLine", callback, null, ids);
        }

        public object GetLiteTablesReservationsByDateAsync(QueryRequest req, DateTime? date, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "GetLiteTablesReservationsByDate", callback, null, req, date);
        }

        public void GetLiteTablesReservationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetLiteTablesReservations", callback, null, req);
        }

        #endregion

        #region Contacts

        public object GetTitlesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetTitles", callback, null, req);

        }
        public object GetContactsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetContacts", callback, null, req);
        }
        public object ClientFromContactAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "ClientFromContact", callback, null, id);
        }
        public object LoadContactAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "LoadContact", callback, null, id);
        }
        public object NewContactAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "NewContact", callback, null);
        }
        public object UpdateContactAsync(ContactContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "UpdateContact", callback, null, obj);
        }
        public object DeleteContactAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "DeleteContact", callback, null, ids);
        }
        public object GetContactTypesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetContactTypes", callback, null, req);
        }
        public object GetContactTypeDetailsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetContactTypeDetails", callback, null, req);
        }

        #region Title
        public object GetTitleAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetTitle", callback, null, req);
        }
        public object LoadTitleAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "LoadTitle", callback, null, id);
        }
        public object NewTitleAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "NewTitle", callback, null);
        }
        public object UpdateTitleAsync(TitleContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "UpdateTitle", callback, null, obj);
        }
        public object RemoveTitleAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "RemoveTitle", callback, null, id);
        }
        #endregion

        #region Contacts' Support

        public object GetCountriesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetCountries", callback, null, req);
        }

        public object GetJobFunctionsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetJobFunctions", callback, null, req);
        }

        public object GetProfessionsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetProfessions", callback, null, req);
        }

        #endregion

        #region Regional
        public object GetLanguagesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetLanguages", callback, null, req);
        }
        public object GetLanguageTranslationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetLanguageTranslations", callback, null, req);
        }
        public object GetWorldRegionsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetWorldRegions", callback, null, req);
        }
        public object GetDistrictsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetDistricts", callback, null, req);
        }
        #endregion

        #endregion


        #region ImagePaymentForm
        public void GetImagesByPaymentFormAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetImagesByPaymentForm", callback, null, req, ownerId);
        }

        public void GetOrderByStandAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetOrderByStand", callback, null, req, ownerId);
        }

        public void GetImagesPaymentFormAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetImagesPaymentForm", callback, null, req);
        }

        public void GetCreditCardTypesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetCreditCardTypes", callback, null, req);
        }

        public void LoadImagePaymentFormAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadImagePaymentForm", callback, null, id);
        }

        public void NewImagePaymentFormAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewImagePaymentForm", callback, null);
        }

        public void UpdateImagePaymentFormAsync(ImagePaymentTypeContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateImagePaymentForm", callback, null, obj);
        }

        public void DeleteImagePaymentFormAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteImagePaymentForm", callback, null, ids);
        }

        public void MoveUpImage(Guid id, Guid stand, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "MoveUpImage", callback, null, id, stand);
        }

        public void MoveDownImage(Guid id, Guid stand, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "MoveDownImage", callback, null, id, stand);
        }


        #endregion
        #region Stand
        public void GetProductRateTypesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductRateTypes", callback, null, req);
        }
        public void LoadProductRateTypeAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadProductRateType", callback, null, id);
        }
        public void NewProductRateTypeAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewProductRateType", callback, null);
        }
        public void UpdateProductRateTypeAsync(ProductRateTypeContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateProductRateType", callback, null, obj);
        }
        public void DeleteProductRateTypeAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteProductRateType", callback, null, ids);
        }
        public void GetCookMarchsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetCookMarchs", callback, null, req);
        }

        public void LoadCookMarchAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadCookMarch", callback, null, id);
        }
        public void NewCookMarchAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewCookMarch", callback, null);
        }
        public void UpdateCookMarchAsync(CookMarchContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateCookMarch", callback, null, obj);
        }
        public void DeleteCookMarchAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteCookMarch", callback, null, ids);
        }

        public void GetStandsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetStands", callback, null, req);
        }
        public void LoadStandAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadStand", callback, null, id);
        }
        public void GetStandTurnDateAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetStandTurnDate", callback, null, id);
        }
        public void NewStandAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewStand", callback, null);
        }
        public void UpdateStandAsync(StandContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateStand", callback, null, obj);
        }
        public void DeleteStandAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteStand", callback, null, ids);
        }

        public void GetStandProductsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetStandProducts", callback, null, req, ownerId);
        }

        public void GetStandProductsByStandAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetStandProductsByStand", callback, null, req, ownerId);
        }

        public void GetStandProductsByStandsAsync(QueryRequest req, Guid[] stands, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetStandProductsByStands", callback, null, req, stands);
        }

        public void GetProductsByStandAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductsByStand", callback, null, req, ownerId);
        }

        public void GetProductsByStandAllAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductsByStandAll", callback, null, req, ownerId);
        }
        public void GetStandProductsByProductAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetStandProductsByProduct", callback, null, req, ownerId);
        }
        public void RemoveStandProductsAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteStandProducts", callback, null, ownerId, ids);
        }
        public void DeleteProductsByStandAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteProductsByStand", callback, null, ownerId, ids);
        }
        public void LoadStandProductAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadStandProduct", callback, null, id);
        }
        public void NewStandProductAsync(Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewStandProduct", callback, null, ownerId);
        }
        public void UpdateStandProductAsync(StandProductsContract standProduct, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateStandProduct", callback, null, standProduct, ownerId);
        }
        public void AddStandProductsToStandAsync(Guid ownerId, StandProductsContract[] stands, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddStandProductToStand", callback, null, ownerId, stands);
        }

        public void DeleteStandProductsFromStandAsync(Guid ownerId, Guid[] standProducts, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteStandProductFromStand", callback, null, ownerId, standProducts);
        }

        public void SetServiceToAllStandProductsAsync(Guid ownerId, Guid? service, string serviceDesc, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "SetServiceToAllStandProducts", callback, null, ownerId, service, serviceDesc);
        }
        public void SetServiceToStandProductsAsync(Guid ownerId, Guid[] standProducts, Guid? service, string serviceDesc, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "SetServiceToStandProducts", callback, null, ownerId, standProducts, service, serviceDesc);
        }

        public void GetStandCajasAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetStandCajas", callback, null, req, ownerId);
        }

        public void GetCajasByStandAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetCajasByStand", callback, null, req);
        }
        public void RemoveStandCajasFromStandAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteStandCajasFromStand", callback, null, ownerId, ids);
        }
        public void AddStandCajasToStandAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddStandCajaToStand", callback, null, ownerId, ids);
        }

        public void GetStandPriceCostsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetStandPriceCosts", callback, null, req, ownerId);
        }
        public void RemoveStandPriceCostsAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteStandPriceCosts", callback, null, ownerId, ids);
        }
        public void LoadStandPriceCostAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadStandPriceCost", callback, null, id);
        }
        public void NewStandPriceCostAsync(Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewStandPriceCost", callback, null, ownerId);
        }
        public void UpdateStandPriceCostAsync(ProductPriceCostContract standPriceCost, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateStandPriceCost", callback, null, standPriceCost, ownerId);
        }

        public void GetStandInternalUsesAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetStandInternalUses", callback, null, req, ownerId);
        }
        public void RemoveStandInternalUsesAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteStandInternalUses", callback, null, ownerId, ids);
        }
        public void LoadStandInternalUseAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadStandInternalUse", callback, null, id);
        }
        public void NewStandInternalUseAsync(Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewStandInternalUse", callback, null, ownerId);
        }
        public void UpdateStandInternalUseAsync(InternalUsexStandContract standInternalUse, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateStandInternalUse", callback, null, standInternalUse, ownerId);
        }

        #endregion
        #region Prices
        public void GetHappyHoursAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetHappyHours", callback, null, req);
        }
        public void GetStandHappyHoursAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetAllHappyHourStands", callback, null, req);
        }

        public void LoadHappyHourAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadHappyHour", callback, null, id);
        }
        public void NewHappyHourAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewHappyHour", callback, null);
        }
        public void UpdateHappyHourAsync(HappyHourContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateHappyHour", callback, null, obj);
        }
        public void DeleteHappyHourAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteHappyHour", callback, null, id);
        }

        public void GetProductPriceRatesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductPriceRates", callback, null, req);
        }

        public void GetPricesByPeriodAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetPricesByPeriod", callback, null, req, ownerId);
        }

        public void GetCopyRateAsync(CopyRateContract copyRateContract, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "CopyRate", callback, null, copyRateContract);
        }

        public void GetProductPriceRatesByPriceRateAsync(Guid priceRateId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductPriceRatesByPriceRate", callback, null, priceRateId);
        }
        public void LoadProductPriceRateAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadProductPriceRate", callback, null, id);
        }
        public void LoadRateVisualAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadRateVisual", callback, null);
        }
        public void LoadProductRateVisualAsync(Guid productId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadProductRateVisual", callback, null, productId);
        }
        public void LoadAllPeriodsAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadAllPeriods", callback, null, ids);
        }
        public void NewProductPriceRateAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewProductPriceRate", callback, null);
        }
        public void UpdateProductPriceRateAsync(ProductPriceRateContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateProductPriceRate", callback, null, obj);
        }
        public void DeleteProductPriceRateAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteProductPriceRate", callback, null, ids);
        }

        public void GetAllPricePeriodsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetAllPricePeriods", callback, null, req);
        }
        public void GetPricePeriodsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetPricePeriods", callback, null, req, ownerId);
        }
        public void RemovePricePeriodsAsync(Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeletePricePeriods", callback, null, ownerId);
        }
        public void LoadPricePeriodAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadPricePeriod", callback, null, id);
        }
        public void GetProductsByPeriodAsync(Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductsByPeriod", callback, null, ownerId);
        }
        public void CopyProductsToPeriodAsync(Guid ownerId, Guid newPeriodId, bool Percent, bool Plus, decimal SelectedValue, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "CopyProductsToPeriod", callback, null, ownerId, newPeriodId, Percent, Plus, SelectedValue);
        }
        public void NewPricePeriodAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewPricePeriod", callback, null);
        }
        public void UpdatePricePeriodAsync(PricePeriodContract pricePeriod, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdatePricePeriod", callback, null, pricePeriod, ownerId);
        }

        public void GetBlockPeriodsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetBlockPeriods", callback, null, req, ownerId);
        }
        public void RemoveBlockPeriodsAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteBlockPeriods", callback, null, ownerId, ids);
        }
        public void LoadBlockPeriodAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadBlockPeriod", callback, null, id);
        }
        public void NewBlockPeriodAsync(Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewBlockPeriod", callback, null, ownerId);
        }
        public void UpdateBlockPeriodAsync(BlockPeriodContract BlockPeriod, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateBlockPeriod", callback, null, BlockPeriod, ownerId);
        }

        public void GetHappyHourStandsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetHappyHourStands", callback, null, req, ownerId);
        }
        public void UpdateHappyHourStandValueAsync(Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateHappyHourStandValue", callback, null, ownerId);
        }
        public void AddStandToHappyHourAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddStandToHappyHour", callback, null, ownerId, ids);
        }
        public void RemoveHappyHourStandsAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteHappyHourStand", callback, null, ownerId, ids);
        }
        public void LoadHappyHourStandAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadHappyHourStand", callback, null, id);
        }
        public void NewHappyHourStandAsync(Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewHappyHourStand", callback, null, ownerId);
        }
        public void UpdateHappyHourStandAsync(StandHappyHourContract standHappyHour, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateHappyHourStand", callback, null, standHappyHour, ownerId);
        }

        public void RemovePricesFromPeriodAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "RemovePricesFromPeriod", callback, null, ownerId, ids);
        }

        public void AddPricesToPeriodAsync(Guid ownerId, ProductPriceRateContract[] objects, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddPricesToPeriod", callback, null, ownerId, objects);
        }

        public void SetPriceToAllFromPeriodAsync(Guid ownerId, decimal price, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "SetPriceToAllFromPeriod", callback, null, ownerId, price);
        }
        public void SetPriceFromPeriodAsync(Guid ownerId, Guid[] productPricesIds, decimal price, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "SetPriceFromPeriod", callback, null, ownerId, productPricesIds, price);
        }

        #endregion
        #region internalUse
        public void GetinternalUseAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetinternalUse", callback, null, req);
        }
        public void GetInternalUsesXStandAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetInternalUsesXStand", callback, null, req);
        }
        public void LoadinternalUseAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadinternalUse", callback, null, id);
        }
        public void NewinternalUseAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewinternalUse", callback, null);
        }
        public void UpdateinternalUseAsync(InternalUseContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateinternalUse", callback, null, obj);
        }
        public void DeleteinternalUseAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteinternalUse", callback, null, ids);
        }

        public void GetInternalUseStandsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetInternalUseStands", callback, null, req, ownerId);
        }
        public void UpdateInternalUseStandValueAsync(Guid ownerId, double minValue, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateInternalUseStandValue", callback, null, ownerId, minValue);
        }
        public void AddStandToInternalUseStandAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddStandToInternalUseStand", callback, null, ownerId, ids);
        }
        public void RemoveInternalUseStandsAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteInternalUseStand", callback, null, ownerId, ids);
        }
        public void LoadInternalUseStandAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadInternalUseStand", callback, null, id);
        }
        public void NewInternalUseStandAsync(Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewInternalUseStand", callback, null, ownerId);
        }
        public void UpdateInternalUseStandAsync(InternalUsexStandContract internalUse, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateInternalUseStand", callback, null, internalUse, ownerId);
        }

        #endregion
        #region Preparations

        public void GetAllProductPreparationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetAllProductPreparations", callback, null, req);
        }

        public void GetAllFamilyPreparationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetAllFamilyPreparations", callback, null, req);
        }

        public void GetAllGroupPreparationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetAllGroupPreparations", callback, null, req);
        }



        public void GetPreparationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetPreparations", callback, null, req);
        }
        public void LoadPreparationsAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadPreparations", callback, null, id);
        }
        public void NewPreparationsAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewPreparations", callback, null);
        }
        public void UpdatePreparationsAsync(PreparationsContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdatePreparations", callback, null, obj);
        }
        public void DeletePreparationsAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeletePreparations", callback, null, ids);
        }

        public void GetPreparationsFamilysAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetPreparationsFamilys", callback, null, req, ownerId);
        }
        public void RemovePreparationsFamilysAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeletePreparationsFamily", callback, null, ownerId, ids);
        }
        public void AddPreparationsFamilyAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddPreparationsFamily", callback, null, ownerId, ids);
        }

        public void GetPreparationsGroupsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetPreparationsGroups", callback, null, req, ownerId);
        }
        public void RemovePreparationsGroupsAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeletePreparationsGroup", callback, null, ownerId, ids);
        }
        public void AddPreparationsGroupAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddPreparationsGroup", callback, null, ownerId, ids);
        }

        public void GetPreparationsProductsAsync(QueryRequest req, Guid productId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetPreparationsProducts", callback, null, req, productId);
        }

        public void GetProductsPreparationAsync(QueryRequest req, Guid productId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductsPreparation", callback, null, req, productId);
        }

        public void RemovePreparationsProductsAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeletePreparationsProduct", callback, null, ownerId, ids);
        }
        public void AddPreparationsProductAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddPreparationsProduct", callback, null, ownerId, ids);
        }
        #endregion

        #region Services
        public void GetProductTaxesAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetServiceTaxes", callback, null, req, ownerId);
        }
        public void NewProductTaxAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewServiceTax", callback, null);
        }
        public void LoadProductTaxAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadServiceTax", callback, null, id);
        }
        public void AddProductTaxAsync(Guid ownerId, ServiceTaxContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddServiceTax", callback, null, ownerId, obj);
        }
        public void RemoveProductTaxAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "RemoveServiceTaxes", callback, null, ownerId, ids);
        }
        #endregion


        #region Categories

        #region CategorySubCategory
        public void GetProductsInCategoriesAsync(Guid standId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductsInCategories", callback, null, standId);
        }
        public void GetCategoriesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetCategories", callback, null, req);
        }
        public void GetTopCategoriesAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetTopCategories", callback, null);
        }

        public void GetCategoriesInUseAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetCategoriesInUse", callback, null);
        }

        public void GetCategoriesByCategoriesAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetCategoriesByCategories", callback, null, ids);
        }

        public void LoadCategoryAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadCategory", callback, null, id);
        }
        public void NewCategoryAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewCategory", callback, null);
        }
        public void UpdateCategoryAsync(CategorySubCategoryContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateCategory", callback, null, obj);
        }
        public void DeleteCategoryAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteCategory", callback, null, ids);
        }

        public void AddCategoryToCategoryAsync(CategorySubCategoryContract parent, CategorySubCategoryContract category, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddCategoryToCategory", callback, null, parent, category);
        }
        public void RemoveCategoryToCategoryAsync(CategorySubCategoryContract category, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "RemoveCategoryToCategory", callback, null, category);
        }
        #endregion

        #region ProductStandCategory

        public void GetProductStandCategoryAsync(Guid standId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductStandCategory", callback, null, standId);
        }
        public void GetProductStandCategoryByCategoryAsync(Guid standId, Guid categoryId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductStandCategoryByCategory", callback, null, standId, categoryId);
        }

        public void GetProductStandCategoryByCategoryStandsAsync(object[] standIds, Guid categoryId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductStandCategoryByCategoryStands", callback, null, standIds, categoryId);
        }

        public void LoadProductStandCategoryAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadProductStandCategory", callback, null, id);
        }
        public void NewProductStandCategoryAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewProductStandCategory", callback, null);
        }
        public void UpdateProductStandCategoryAsync(ProductStandCategoryContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateProductStandCategory", callback, null, obj);
        }

        public void DeleteProductStandCategoryAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteProductStandCategory", callback, null, ids);
        }

        public void UpdateProductStandsCategoryAsync(ProductStandCategoryContract obj, Guid[] stands, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateProductStandsCategory", callback, null, obj, stands);
        }

        public void DeleteProductStandsCategoryAsync(Guid[] products, Guid catId, Guid[] stands, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteProductStandsCategory", callback, null, products, catId, stands);
        }

        public void CopyProductStandCategoriesFromAsync(Guid from, Guid to, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "CopyProductStandCategoriesFrom", callback, null, from, to);
        }

        #endregion

        #endregion

        #region CreditCard
        public void NewCreditCardAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "NewCreditCard", callback, null);
        }
        public object LoadCreditCardAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "LoadCreditCard", callback, null, id);
        }
        public void UpdateCreditCardAsync(CreditCardContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "PersistCreditCard", callback, null, obj);
        }

        #endregion

        #region PricePeriodProduct
        public void DeletePricePeriodProductAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeletePricePeriodProduct", callback, null, id);
        }

        public void UpdatePricePeriodProductAsync(Guid productId, Guid priceRateId, decimal price, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdatePricePeriodProduct", callback, null, productId, priceRateId, price);
        }


        public void AddPricePeriodProductAsync(PricePeriodProductContract contract, EventHandler<ClientEventArgs> callback)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region CurrentAccounts
        public void GetReservationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetReservations", callback, null, req);
        }
        public void GetSpaReservationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetReservations", callback, null, req);
        }
        public void GetEventReservationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetReservations", callback, null, req);
        }
        public void GetEntityInstallationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetEntityInstallations", callback, null, req);
        }
        public void GetReservationsGroupsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetReservationsGroups", callback, null, req);
        }
        public void GetControlAccountSearchAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetControlAccounts", callback, null, req);
        }
        public void GetClientInstallationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetClientInstallations", callback, null, req);
        }
        public void GetGuestsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetGuestsPms", callback, null, req);
        }
        #endregion

        #region Cancellation

        #region Cancellation control
        public void NewCancellationControlAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "NewCancellationControl", callback, null);
        }
        public void LoadCancellationControlAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "LoadCancellationControl", callback, null, id);
        }
        #endregion

        #region Cancellation reason
        public void GetCancellationReasonsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetCancellationReasons", callback, null, req);
        }
        public void LoadCancellationReasonAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "LoadCancellationReason", callback, null, id);
        }
        public void NewCancellationReasonAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "NewCancellationReason", callback, null);
        }
        public void UpdateCancellationReasonAsync(CancellationReasonContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "UpdateCancellationReason", callback, null, obj);
        }
        public void DeleteCancellationReasonAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "DeleteCancellationReason", callback, null, ids);
        }
        #endregion

        #endregion


        #region Taxes


        #endregion

        #region Local

        public void GetPosTaxServicesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetPosTaxServices", callback, null, req);
        }

        public void GetAllTaxesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetAllTaxes", callback, null, req);
        }

        public void GetAllTaxSchemaAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetAllTaxSchema", callback, null, req);
        }

        public void GetAllTaxSequenceAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetAllTaxSequence", callback, null, req);
        }

        public void GetCurrencyInstallationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetCurrencyInstallations", callback, null, req);

        }

        public void GetAllProductsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetAllProducts", callback, null, req);
        }

        public void GetAllCategoriesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetAllCategories", callback, null, req);
        }

        public void GetAllStandProductsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetAllStandProducts", callback, null, req);
        }

        public void GetAllStandProductsCategoryAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetAllStandProductsCategory", callback, null, req);
        }

        public void GetAllAreasAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetAreas", callback, null, req);
        }

        public void GetAllStandProductAreasAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetAllStandProductAreas", callback, null, req);
        }

        public void GetSettingsParametersAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetSettingsParameters", callback, null, req);
        }

        public void LoadAreasAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadAreas", callback, null, id);
        }

        public void NewAreasAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewAreas", callback, null);
        }

        public void UpdateAreasAsync(DispatchAreaContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateAreas", callback, null, obj);
        }

        public void DeleteAreasAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteAreas", callback, null, ids);
        }

        #endregion

        #region License
        public void GetLicenseInformationAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetLicenseInformation", callback, null);
        }
        public void GetLicensePaymentInformationAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetLicensePaymentInformation", callback, null);
        }
        #endregion

        public void GetControlAccountsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetControlAccounts", callback, null, req);
        }
        public void GetServiceAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetServices", callback, null, req);
        }
        public void GetServiceGroupsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetServiceGroups", callback, null, req);
        }
        public void GetServiceTipsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetServiceTips", callback, null, req);
        }
        public void GetTipServicesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetTipServices", callback, null, req);
        }

        public void GetServiceCategoriesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetServiceCategories", callback, null, req);
        }
        public void GetTaxSchemasAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetTaxSchemas", callback, null, req);
        }
        public void GetTaxRatesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetTaxRates", callback, null, req);
        }
        public void GetPropTypesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetPropTypes", callback, null, req);
        }
        public void GetPaymentFormAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetPaymentMethods", callback, null, req);
        }
        public void GetMinStandWorkDateAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetMinStandWorkDate", callback, null, req);
        }
        public void GetPensionByRoomAsync(Guid? lioc_pk, DateTime date, QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetPensionByRoom", callback, null, lioc_pk, date);
        }
        public object GetPriceCategoriesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetPriceCategories", callback, null, req);
        }

        public QueryResponse GetCurrencyInstallationsSync(QueryRequest request)
        {
            var proxy = GetNewHotelManagement();
            return (QueryResponse)Invoke(proxy, "GetCurrencyInstallations", null, null, request);
        }

        #endregion

        #region Management Operation
        public void GetSerieByStandCajaAsync(Guid standId, Guid? cajaId, long type, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetSerieByStandCaja", callback, null, standId, cajaId, type);
        }
        public void GetSerieByStandCajaInfoAsync(Guid standId, Guid cajaId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetSerieByStandCajaInfo", callback, null, standId, cajaId);
        }
        public void GetSeriesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetSeries", callback, null, req);
        }
        public void GetSeriesDescAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetSeriesDesc", callback, null, req);
        }
        public void UpdateSerieControlAsync(DocumentSerieControlContract contract, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "UpdateDocumentSerieControl", callback, null, contract);
        }
        public void UpdateSerieAsync(DocumentSerieContract contract, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "UpdateDocumentSerie", callback, null, contract);
        }

        public void NewSerieControlAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "NewDocumentSerieControl", callback, null);
        }
        public void NewSerieAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "NewDocumentSerie", callback, null);
        }
        public void LoadSerieControlAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "LoadDocumentSerieControl", callback, null, id);
        }

        public void LoadSerieAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "LoadDocumentSerie", callback, null, id);
        }

        public void InactiveSerieByCajaAsync(Guid ipos_pk, Guid caja_pk, Guid serieId, long serieNumber, long type, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "InactiveSerieByCaja", callback, null, ipos_pk, caja_pk, serieId, serieNumber, type);
        }

        //public void BlockCajaAsync(Guid ipos_pk, Guid caja_pk, EventHandler<ClientEventArgs> callback)
        //{
        //    var proxy = GetNewHotelManagement();
        //    Invoke(proxy, "BlockCaja", callback, null, ipos_pk, caja_pk);

        //}

        //public void UnBlockCajaAsync(Guid ipos_pk, Guid caja_pk, long currentNumber, EventHandler<ClientEventArgs> callback)
        //{
        //    var proxy = GetNewHotelManagement();
        //    Invoke(proxy, "UnBlockCaja", callback, null, ipos_pk, caja_pk, currentNumber);

        //}

        public void DeleteSerieAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "DeleteDocumentSerieControl", callback, null, ids);
        }

        public void GetTicketsRealAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetTicketsR", callback, null, req);
        }
        public void LoadTicketRealAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadTicketR", callback, null, id);
        }
        public void UpdateTicketRealAsync(TicketRContract ticket, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateTicketR", callback, null, ticket);
        }
        public void DeleteTicketRealAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteTicketR", callback, null, ids);
        }
        public void DefineTaxProductLineAsync(ProductLineContract productLine, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DefineTaxProductLine", callback, null, productLine);
        }
        public void GetProductPriceRateByRateAndProductAsync(Guid periodId, Guid[] productsId, DateTime workDate, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductPriceRatesByRateAndProducts", callback, null, periodId, productsId, workDate);
        }
        public void CancelTicketAsync(Guid ownerId, Guid cancellationReason, string cancellationRDesc, string comments, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "CancelTicket", callback, null, ownerId, cancellationReason, cancellationRDesc, comments);
        }

        public void CancelTicketContractsAsync(TicketRContract ticket, CancellationControlContract cancellation, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "CancelTicketContracts", callback, null, ticket, cancellation);
        }

        public void VerifyCloseDayAsync(Guid standId, Guid cajaId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "VerifyCloseDay", callback, null, standId, cajaId);
        }

        public void CloseDayStandAsync(Guid standId, Guid? cajaId, DateTime? closeDay, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "CloseDay", callback, null, standId, cajaId, closeDay);
        }
        public void CloseTurnAsync(Guid standId, Guid? cajaId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "CloseTurn", callback, null, standId, cajaId);
        }
        public void ValidateLogInAsync(Guid standId, Guid? cajaId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "ValidateLogIn", callback, null, standId, cajaId);
        }
        public void LogInAsync(Guid standId, Guid cajaId, Guid userId, Guid? fisi, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LogIn", callback, null, standId, cajaId, userId, fisi);
        }

        public void LogOutAsync(Guid standId, Guid cajaId, long ticketSerieNumber, long? invoiceSerieNumber, long? receiptSerieNumber, string ticketSign, string invoiceSign, string receiptSign, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LogOut", callback, null, standId, cajaId, ticketSerieNumber, invoiceSerieNumber, receiptSerieNumber, ticketSign, invoiceSign, receiptSign);
        }

        public void TrasferTicketAsync(TransferContract contract, TicketRContract ticketContract, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "TrasferTicket", callback, null, contract, ticketContract);
        }
        public void AcceptTransferTicketAsync(TransferContract contract, Guid cajaId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AcceptTransferTicket", callback, null, contract, cajaId);
        }
        public void ReturnTransferTicketAsync(TransferContract contract, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "ReturnTransferTicket", callback, null, contract);
        }
        public void VerifyTransferReturnsAsync(Guid stand, Guid caja, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "VerifyTransferReturns", callback, null, stand, caja);
        }
        public void VerifyTransferToAcceptAsync(Guid stand, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "VerifyTransferToAccept", callback, null, stand);
        }

        public void VerifyTaxesSchemaAsync(Guid servicePk, Guid? schemaPk, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "VerifyTaxesSchema", callback, null, servicePk, schemaPk);
        }
        public void GetLicenceTypeAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetLicenseType", callback, null, Guid.Empty, 2);
        }

        public void VoidOldTicketAsync(Guid ticketId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "VoidOldTicket", callback, null, ticketId, null, null);
        }
        #endregion

        #region TicketsError
        public void LoadTicketsErrorAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadTicketsError", callback, null, id);
        }
        public void NewTicketsErrorAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewTicketsError", callback, null);
        }
        public void UpdateTicketsErrorAsync(TicketsErrorContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateTicketsError", callback, null, obj);
        }
        public void DeleteTicketsErrorAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteTicketsError", callback, null, id);
        }
        #endregion

        #region StandCajaLog
        public void LoadStandCajaLogAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadStandCajaLog", callback, null, id);
        }
        public void NewStandCajaLogAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewStandCajaLog", callback, null);
        }
        public void UpdateStandCajaLogAsync(StandCajaLogContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateStandCajaLog", callback, null, obj);
        }
        public void DeleteStandCajaLogAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteStandCajaLog", callback, null, ids);
        }

        public void GetStandCajaLogAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetStandCajaLog", callback, null, req);
        }
        #endregion

        #region Report operations
        public void LoadReportStructureAsync(int appl_pk, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadReportStructure", callback, null, appl_pk);
        }
        public void LoadReportStructureListAsync(short keywordId, int appl_pk, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadReportStructureList", callback, null, keywordId, appl_pk);
        }
        #endregion

        #region Configuration Operation
        #region Tickets
        public void GetTicketsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetTickets", callback, null, req);
        }

        public void LoadTicketAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadTicket", callback, null, id);
        }

        public void NewTicketAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewTicket", callback, null);
        }

        public void UpdateTicketAsync(TicketContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateTicket", callback, null, obj);
        }

        public void DeleteTicketAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteTicket", callback, null, ids);
        }

        public void GetTicketsErrorAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetTicketsError", callback, null, req);
        }

        public void LoadTicketfromTicketErrorAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadTicketfromTicketError", callback, null, id);
        }
        public void UpdateTicketfromTicketErrorAsync(TicketRContract contract, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateTicketfromTicketError", callback, null, contract);
        }
        #endregion
        #region ImagesGallery
        public void GetImagesGalleryAsync(int type, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetImagesGallery", callback, null, type);
        }
        public void MoveImageToServiceAsync(string imageName, byte[] image, int type, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "MoveImageToService", callback, null, imageName, image, type);
        }
        public void GetImagesFromFileAsync(Guid parentId, string path, int page, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetImagesFromFile", callback, null, parentId, path, page);
        }
        public void RemoveImageCustomAsync(string imageName, int type, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "RemoveImageCustom", callback, null, imageName, type);
        }
        public void GetImageFromPathAsync(string path, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetImageFromPath", callback, null, path);
        }
        #endregion
        #endregion

        #region ExternalOperations
        public void UpdateSimpleEntryExternalAsync(MovementExternalContract contract, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateSimpleEntryExternal", callback, null, contract);
        }
        public object UpdateSimpleEntryExternalDebitAsync(MovementExternalContract contract, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "UpdateSimpleEntryExternalDebit", callback, null, contract);
        }

        public void UpdateAllMovementsAsync(List<TicketRContract> tickets, Guid stand, Guid? account, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateAllMovements", callback, null, tickets, stand, account);
        }
        public void UpdateInvoiceExternalAsync(List<Guid> movements, FactInformationContract factInfo, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateInvoiceExternal", callback, null, movements, factInfo);
        }

        public void InvoiceAndPersistTicketAsync(TicketRContract ticket, FactInformationContract buyerInfo, Guid? ccontrol, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "InvoiceAndPersistTicket", callback, null, ticket, buyerInfo);
        }

        #endregion

        #region Reports

        public void GetSalesCashierAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetSalesCashier", callback, null, req);
        }
        public void GetSalesCajaCashierAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetSalesCaja", callback, null, req);
        }

        #endregion

        #region Login
        public void GetInstallationsByUserAsync(QueryRequest req, string naciId, string userName, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetAppManagement();
            Invoke(proxy, "GetInstallationsByUser", callback, null, req, naciId, userName);
        }

        public void GetCountriesByHotelAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetAppManagement();
            Invoke(proxy, "GetCountriesByHotel", callback, null, req);
        }
        public void LoadUserByNameAsync(string userName, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetAppManagement();
            Invoke(proxy, "LoadUserByName", callback, null, userName);
        }
        #endregion

        #region Security
        public void NewProfileAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewProfile", callback, null);
        }

        public void GetProfileAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProfile", callback, null, req);
        }
        public void UpdateProfileAsync(ProfileContract profilecontract, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateProfile", callback, null, profilecontract);
        }

        public void DeleteProfileAsync(Guid[] profileIds, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteProfile", callback, null, profileIds);
        }
        public void LoadProfileAsync(Guid profileId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadProfile", callback, null, profileId);
        }

        public void GetProfilePermissionsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProfilePermissions", callback, null, req);
        }
        public void GetUserPermissionsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetUserPermissions", callback, null, req, ownerId);
        }

        public void LoadApplicationsPermissionsAsync(long[] applicationIds, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadApplicationsPermissions", callback, null, applicationIds);
        }
        public void GetLoginPeriodsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetLoginPeriods", callback, null, req, ownerId);
        }

        public void GetGroupPermissionsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetGroupPermissions", callback, null, req);
        }

        public void GetGroupsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetGroups", callback, null, req);
        }

        public void GetRolesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetRoles", callback, null, req);
        }

        public void GetPermissionDetailsByApplicationAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetPermissionDetailsbyApplication", callback, null, req);
        }


        #region Profile

        #endregion
        #region Users
        public void NewUserAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewUser", callback, null);
        }
        public void GetUsersAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetUsers", callback, null, req);
        }

        public void GetAllUsersAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetAllUsers", callback, null, req);
        }
        public void UpdateUserAsync(UserContract usercontract, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateUser", callback, null, usercontract);
        }

        public void SearchUserAsync(string login, string description, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "SearchUser", callback, null, login, description);
        }
        public void LoadUserAsync(Guid userId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadUser", callback, null, userId);
        }
        public void InactiveUserAsync(Guid[] userIds, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "InactiveUser", callback, null, userIds);
        }

        public void DeleteUserAsync(Guid[] userIds, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteUser", callback, null, userIds);
        }

        public void GetApplicationAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetApplication", callback, null, req);
        }

        public void GetApplicationByUserAsync(QueryRequest req, Guid userId, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetApplicationbyUser", callback, null, req, userId);
        }
        public void AddApplicationByUserAsync(long[] ids, UserContract userContract, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddApplicationbyUser", callback, null, ids, userContract);
        }
        public void RemoveApplicationByUserAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "RemoveApplicationbyUser", callback, null, ownerId, ids);
        }

        #endregion


        #endregion
        /*
        #region Credit card type
        public void LoadCreditCardTypeAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelPMSManagement();
            Invoke(proxy, "LoadCreditCardType", callback, null, id);
        }
        public void NewCreditCardTypeAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelPMSManagement();
            Invoke(proxy, "NewCreditCardType", callback, null);
        }
        public void UpdateCreditCardTypeAsync(CreditCardTypeContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelPMSManagement();
            Invoke(proxy, "UpdateCreditCardType", callback, null, obj);
        }
        public void DeleteCreditCardTypeAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelPMSManagement();
            Invoke(proxy, "DeleteCreditCardType", callback, null, id);
        }
        #endregion
        */
        #region Payment method

        public void GetInstallationCurrenciesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetInstallationCurrencies", callback, null, req);
        }
        public void GetPaymentMethodsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetPaymentMethods", callback, null, req);
        }
        public void LoadPaymentMethodAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "LoadPaymentMethod", callback, null, id);
        }
        public void NewPaymentMethodAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "NewPaymentMethod", callback, null);
        }
        public void UpdatePaymentMethodAsync(PaymentFormContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "UpdatePaymentMethod", callback, null, obj);
        }
        public void DeletePaymentMethodAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "DeletePaymentMethod", callback, null, id);
        }
        #endregion

        #region Discount type
        public void GetDiscountTypesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetDiscountTypes", callback, null, req);
        }
        public void LoadDiscountTypeAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "LoadDiscountType", callback, null, id);
        }
        public void NewDiscountTypeAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "NewDiscountType", callback, null);
        }
        public void UpdateDiscountTypeAsync(DiscountTypeContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "UpdateDiscountType", callback, null, obj);
        }
        public void DeleteDiscountTypeAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "DeleteDiscountType", callback, null, id);
        }
        #endregion

        #region Document serie
        public void GetDocumentSeriesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetDocumentSeries", callback, null, req);
        }
        public void LoadDocumentSerieAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "LoadDocumentSerie", callback, null, id);
        }
        public void NewDocumentSerieAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "NewDocumentSerie", callback, null);
        }
        public void UpdateDocumentSerieAsync(DocumentSerieContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "UpdateDocumentSerie", callback, null, obj);
        }
        public void GetDocumentTypesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetDocumentTypes", callback, null, req);
        }
        #endregion

        #region Market
        public object GetMarketOriginsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetMarketOrigins", callback, null, req);

        }
        public object GetMarketSegmentsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetMarketSegments", callback, null, req);

        }
        #endregion

        #region Entity type
        public object GetEntityTypesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetEntityTypes", callback, null, req);
        }
        #endregion

        #region Reservation Logs
        public object GetReservationLogsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "GetReservationLogs", callback, null, req);
        }
        #endregion

        #region Export Official Document

        public void GetExportOfficialDocumentAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetSettingManagement();
            Invoke(proxy, "GetExportOfficialDocument", callback, null, id);
        }

        public ExportOfficialDocumentResult GetExportOfficialDocumentSync(Guid id)
        {
            var proxy = GetSettingManagement();
            return (ExportOfficialDocumentResult)Invoke(proxy, "GetExportOfficialDocument", null, null, id);
        }

        #endregion

        #region ReductionZ
        public object SubmitReductionZAsync(ReductionZContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "SubmitReductionZ", callback, null, obj);
        }

        public object GetReductionsZAsync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "GetReductionsZ", callback, null, req);
        }
        #endregion
    }
}
