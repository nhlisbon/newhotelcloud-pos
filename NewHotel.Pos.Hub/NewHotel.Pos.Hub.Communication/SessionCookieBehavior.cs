﻿using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace NewHotel.Pos.Hub.Communication
{
    public class SessionCookieBehavior : IClientMessageInspector, IEndpointBehavior
    {
        #region Members

        private string sessionCookie;

        #endregion
        #region Constructor

        private SessionCookieBehavior() { }

        #endregion
        #region Private Methods

        private static string GetSessionCookie(string cookieHeader)
        {
            if (cookieHeader != null)
            {
                var headerValues = cookieHeader.Split(';');
                foreach (var headerValue in headerValues)
                {
                    if (headerValue.Contains("ASP.NET_SessionId"))
                        return headerValue;
                }
            }
            return string.Empty;
        }

        #endregion
        #region IClientMessageInspector Members

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            if (sessionCookie == null)
            {
                // set SessionID
                var properties = reply.Properties;
                var responseProperty = (HttpResponseMessageProperty)properties[HttpResponseMessageProperty.Name];
                var cookieHeader = responseProperty.Headers[HttpResponseHeader.SetCookie];

                sessionCookie = GetSessionCookie(cookieHeader);
            }
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            if (sessionCookie != null)
            {
                // set SessionID in cookie (tell the server which session we are in!)
                var requestProperty = new HttpRequestMessageProperty();
                requestProperty.Headers.Add("Cookie", sessionCookie);
                request.Properties[HttpRequestMessageProperty.Name] = requestProperty;
            }

            return null;
        }

        #endregion
        #region IEndpointBehavior Members

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(this);
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }

        #endregion
        #region Public Methods && Properties

        public static readonly SessionCookieBehavior Instance = new SessionCookieBehavior();

        public override string ToString()
        {
            return sessionCookie;
        }

        #endregion
    }
}
