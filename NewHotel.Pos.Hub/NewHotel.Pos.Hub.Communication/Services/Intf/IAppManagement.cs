﻿using System;
using System.ServiceModel;
using NewHotel.Contracts;

namespace NewHotel.Communication.Interfaces
{
	[ServiceKnownType(typeof(AppContextData))]
	[ServiceKnownType(typeof(KeyDescHotelRecord))]
	[ServiceKnownType(typeof(CountryAppRecord))]
	[ServiceKnownType(typeof(UserAppContract))]
	[ServiceKnownType(typeof(GuestAccessContract))]
	[ServiceContract]
    public interface IAppManagement
    {
        #region Base

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDummy(string dummy, AsyncCallback callback, object asyncState);
        string EndDummy(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginLog(long operId, Guid? hotelId, Guid userId, AsyncCallback callback, object asyncState);
        void EndLog(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
		IAsyncResult BeginLoadAppContext(long languageId, string userName, AsyncCallback callback, object asyncState);
        AppContextData EndLoadAppContext(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetInstallationsByUser(QueryRequest req, string naciId, string userName, AsyncCallback callback, object asyncState);
        InstallationsByUserResponse EndGetInstallationsByUser(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetCountriesByHotel(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCountriesByHotel(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginLoadUserByName(string username, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadUserByName(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRequestPassword(string code, string user, string comments, AsyncCallback callback, object asyncState);
        ValidationResult EndRequestPassword(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetGuestAccessByHotelDesc(string hotelDesc, AsyncCallback callback, object asyncState);
        ValidationContractResult EndGetGuestAccessByHotelDesc(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetGuestAccessByHotelId(Guid hotelId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndGetGuestAccessByHotelId(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginNewGuestAccess(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewGuestAccess(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateGuestAccess(GuestAccessContract contract, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateGuestAccess(IAsyncResult asyncResult);

        #endregion
    }
}