﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;

namespace NewHotel.Communication.Services
{
    [ServiceContract]
    public interface INewHotelPosManagement
    {
        // Example:
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDummy(AsyncCallback callback, object asyncState);
        ValidationContractResult EndDummy(IAsyncResult asyncResult);

        #region Administration
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginLoadNewHotelContext(string userName, Guid installationId, long languageId, AsyncCallback callback, object asyncState);
        NewHotelContextData EndLoadNewHotelContext(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginClearNewHotelContext(AsyncCallback callback, object asyncState);
        void EndClearNewHotelContext(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginLoadWorkDate(AsyncCallback callback, object asyncState);
        DateTime EndLoadWorkDate(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginCloseContext(int context, AsyncCallback callback, object asyncState);
        void EndCloseContext(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginLoadDatabaseVersion(AsyncCallback callback, object asyncState);
        string EndLoadDatabaseVersion(IAsyncResult asyncResult);
        #endregion

        #region AppClasifier
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ApplicationRecord))]
        IAsyncResult BeginGetApplications(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetApplications(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ApplicationContract))]
        IAsyncResult BeginGetAppClasifierApplications(QueryRequest req, Guid ownerId, string ownerType, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAppClasifierApplications(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddAppClasifierApplications(Guid ownerId, string ownerType, long[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddAppClasifierApplications(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveAppClasifierApplications(Guid ownerId, string ownerType, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveAppClasifierApplications(IAsyncResult asyncResult);
        #endregion

        #region GroupContract
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(GroupProductRecord))]
        IAsyncResult BeginGetGroupProducts(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetGroupProducts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(GroupProductContract))]
        IAsyncResult BeginLoadGroupProduct(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadGroupProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(GroupProductContract))]
        IAsyncResult BeginNewGroupProduct(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewGroupProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateGroupProduct(GroupProductContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateGroupProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteGroupProduct(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteGroupProduct(IAsyncResult asyncResult);
        #endregion

        #region ProductFamily
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductFamilyRecord))]
        IAsyncResult BeginGetProductFamilies(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductFamilies(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductFamilyContract))]
        IAsyncResult BeginLoadProductFamily(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadProductFamily(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductFamilyContract))]
        IAsyncResult BeginNewProductFamily(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewProductFamily(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateProductFamily(ProductFamilyContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateProductFamily(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteProductFamily(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteProductFamily(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateProductGroupCategory(Guid[] ids, Guid groupId, long categoryId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateProductGroupCategory(IAsyncResult asyncResult);



        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginSetProductsService(Guid? serviceId, Guid[] ids, Guid standId, AsyncCallback callback, object asyncState);
        ValidationResult EndSetProductsService(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginSetProductstoStand(Guid standId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndSetProductstoStand(IAsyncResult asyncResult);
        #endregion

        #region ProductSubFamily
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductSubFamilyRecord))]
        IAsyncResult BeginGetProductSubFamilies(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductSubFamilies(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductSubFamilyContract))]
        IAsyncResult BeginLoadProductSubFamily(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadProductSubFamily(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductSubFamilyContract))]
        IAsyncResult BeginNewProductSubFamily(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewProductSubFamily(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateProductSubFamily(ProductSubFamilyContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateProductSubFamily(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteProductSubFamily(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteProductSubFamily(IAsyncResult asyncResult);
        #endregion

        #region Menu
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MenuRecord))]
        IAsyncResult BeginGetMenus(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetMenus(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MenuContract))]
        IAsyncResult BeginNewMenu(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewMenu(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MenuContract))]
        IAsyncResult BeginLoadMenu(Guid menuId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadMenu(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateMenu(MenuContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateMenu(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteMenu(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteMenu(IAsyncResult asyncResult);
        #endregion

        #region Guests
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(GuestsSearchRecord))]
        IAsyncResult BeginGetGuestsPms(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetGuestsPms(IAsyncResult asyncResult);
        #endregion

        #region Product
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductRecord))]
        IAsyncResult BeginGetProducts(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProducts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductRecord))]
        IAsyncResult BeginGetProductsByCodis(List<string> codis, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductsByCodis(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductContract))]
        IAsyncResult BeginLoadProduct(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductContract))]
        IAsyncResult BeginNewProduct(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateProduct(ProductContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateProductbyDispatchAreas(ProductContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateProductbyDispatchAreas(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteProduct(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginInactiveProduct(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndInactiveProduct(IAsyncResult asyncResult);
        #endregion

        #region Separator
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SeparatorRecord))]
        IAsyncResult BeginGetSeparators(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetSeparators(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SeparatorContract))]
        IAsyncResult BeginLoadSeparator(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadSeparator(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SeparatorContract))]
        IAsyncResult BeginNewSeparator(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewSeparator(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateSeparator(SeparatorContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateSeparator(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteSeparator(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteSeparator(IAsyncResult asyncResult);

        #region ChangeOrder
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginMoveUpSeparator(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndMoveUpSeparator(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginMoveDownSeparator(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndMoveDownSeparator(IAsyncResult asyncResult);
        #endregion

        #endregion

        #region ProductAdditionals

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AdditionalContract))]
        IAsyncResult BeginGetProductAdditionals(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductAdditionals(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddAdditionalProduct(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddAdditionalProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteProductAdditionals(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteProductAdditionals(IAsyncResult asyncResult);

        #endregion

        #region ProductPriceRate
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductPriceRateContract))]
        IAsyncResult BeginGetProductPriceRatesByProduct(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductPriceRatesByProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductPriceRateContract))]
        IAsyncResult BeginGetPricesByPeriod(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPricesByPeriod(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddPriceRateProduct(Guid ownerId, ProductPriceRateContract contract, AsyncCallback callback, object asyncState);
        ValidationResult EndAddPriceRateProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteProductPriceRateFromProduct(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteProductPriceRateFromProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductPriceRateContract))]
        IAsyncResult BeginLoadProductPriceRateByProduct(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadProductPriceRateByProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductPriceRateContract))]
        IAsyncResult BeginNewProductPriceRateByProduct(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewProductPriceRateByProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddPricesToPeriod(Guid ownerId, ProductPriceRateContract[] objects, AsyncCallback callback, object asyncState);
        ValidationResult EndAddPricesToPeriod(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemovePricesFromPeriod(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemovePricesFromPeriod(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginSetPriceToAllFromPeriod(Guid ownerId, decimal price, AsyncCallback callback, object asyncState);
        ValidationResult EndSetPriceToAllFromPeriod(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginSetPriceFromPeriod(Guid ownerId, Guid[] productPricesIds, decimal price, AsyncCallback callback, object asyncState);
        ValidationResult EndSetPriceFromPeriod(IAsyncResult asyncResult);

        #endregion

        #region ProductImages

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductImageContract))]
        IAsyncResult BeginGetProductImages(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductImages(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteProductImages(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteProductImages(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductImageContract))]
        IAsyncResult BeginLoadImageProduct(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadImageProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductImageContract))]
        IAsyncResult BeginNewImageProduct(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewImageProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateImageProduct(ProductImageContract imageContract, Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateImageProduct(IAsyncResult asyncResult);




        #endregion

        #region ProductBarCode

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductBarCodeContract))]
        IAsyncResult BeginGetProductBarCodes(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductBarCodes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteProductBarCode(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteProductBarCode(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductBarCodeContract))]
        IAsyncResult BeginLoadProductBarCode(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadProductBarCode(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductBarCodeContract))]
        IAsyncResult BeginNewProductBarCode(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewProductBarCode(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateProductBarCode(ProductBarCodeContract barCodeContract, Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateProductBarCode(IAsyncResult asyncResult);


        #endregion

        #region ProductTable

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TableProductContract))]
        IAsyncResult BeginGetProductTables(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductTables(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteProductTable(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteProductTable(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TableProductContract))]
        IAsyncResult BeginLoadProductTable(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadProductTable(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TableProductContract))]
        IAsyncResult BeginNewProductTable(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewProductTable(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateProductTable(TableProductContract tableProductContract, Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateProductTable(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddTableToProduct(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddTableToProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateProductTableValue(Guid ownerId, decimal quantity, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateProductTableValue(IAsyncResult asyncResult);

        #endregion

        #region ProductsStandbyArea
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductsStandbyAreaRecord))]
        IAsyncResult BeginProductsStandbyArea(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndProductsStandbyArea(IAsyncResult asyncResult);
        #endregion

        #region Caja

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CajaRecord))]
        IAsyncResult BeginGetCajasStandsByUser(QueryRequest req, Guid util_pk, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCajasStandsByUser(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CajaRecord))]
        IAsyncResult BeginGetCajas(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCajas(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CajaContract))]
        IAsyncResult BeginLoadCaja(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadCaja(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CajaContract))]
        IAsyncResult BeginNewCaja(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewCaja(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateCaja(CajaContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateCaja(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteCaja(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteCaja(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandCajasContract))]
        IAsyncResult BeginCheckStandCajasSeries(Guid standId, Guid cajaId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndCheckStandCajasSeries(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginValidateSerieUnicity(StandCajasContract contract, AsyncCallback callback, object asyncState);
        bool EndValidateSerieUnicity(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DocumentSerieRecord))]
        IAsyncResult BeginGetAllDocumentSeries(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllDocumentSeries(IAsyncResult asyncResult);
        #endregion

        #region Duty

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DutyRecord))]
        IAsyncResult BeginGetDutiesByPeriod(QueryRequest req, DateTime initial, DateTime final, AsyncCallback callback, object asyncState);
        QueryResponse EndGetDutiesByPeriod(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DutyRecord))]
        IAsyncResult BeginGetDuties(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetDuties(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DutyContract))]
        IAsyncResult BeginLoadDuty(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadDuty(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DutyContract))]
        IAsyncResult BeginNewDuty(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewDuty(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateDuty(DutyContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateDuty(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteDuty(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteDuty(IAsyncResult asyncResult);
        #endregion

        #region ImagePaymentForm

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ImagePaymentFormRecord))]
        IAsyncResult BeginGetImagesByPaymentForm(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetImagesByPaymentForm(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CreditCardTypeRecord))]
        IAsyncResult BeginGetCreditCardTypes(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCreditCardTypes(IAsyncResult asyncResult);

        
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(OrderImageByStandRecord))]
        IAsyncResult BeginGetOrderByStand(QueryRequest req, Guid owner, AsyncCallback callback, object asyncState);
        QueryResponse EndGetOrderByStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ImagePaymentFormRecord))]
        IAsyncResult BeginGetImagesPaymentForm(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetImagesPaymentForm(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ImagePaymentTypeContract))]
        IAsyncResult BeginLoadImagePaymentForm(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadImagePaymentForm(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ImagePaymentTypeContract))]
        IAsyncResult BeginNewImagePaymentForm(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewImagePaymentForm(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateImagePaymentForm(ImagePaymentTypeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateImagePaymentForm(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteImagePaymentForm(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteImagePaymentForm(IAsyncResult asyncResult);

        #region ChangeOrder
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginMoveUpImage(Guid id, Guid stand, AsyncCallback callback, object asyncState);
        ValidationResult EndMoveUpImage(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginMoveDownImage(Guid id, Guid stand, AsyncCallback callback, object asyncState);
        ValidationResult EndMoveDownImage(IAsyncResult asyncResult);
        #endregion
        #endregion

        #region Happy Hour

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(HappyHourRecord))]
        IAsyncResult BeginGetHappyHours(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetHappyHours(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(HappyHourContract))]
        IAsyncResult BeginLoadHappyHour(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadHappyHour(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(HappyHourContract))]
        IAsyncResult BeginNewHappyHour(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewHappyHour(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateHappyHour(HappyHourContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateHappyHour(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteHappyHour(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteHappyHour(IAsyncResult asyncResult);
        #endregion

        #region HappyHourStand

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandHappyHourRecord))]
        IAsyncResult BeginGetAllHappyHourStands(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllHappyHourStands(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandHappyHourContract))]
        IAsyncResult BeginGetHappyHourStands(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetHappyHourStands(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateHappyHourStandValue(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateHappyHourStandValue(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddStandToHappyHour(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddStandToHappyHour(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteHappyHourStand(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteHappyHourStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandHappyHourContract))]
        IAsyncResult BeginLoadHappyHourStand(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadHappyHourStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandHappyHourContract))]
        IAsyncResult BeginNewHappyHourStand(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewHappyHourStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateHappyHourStand(StandHappyHourContract HappyHourStandContract, Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateHappyHourStand(IAsyncResult asyncResult);
        #endregion

        #region ProductPriceRate
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductPriceRateRecord))]
        IAsyncResult BeginGetProductPriceRates(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductPriceRates(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductPriceRateRecord))]
        IAsyncResult BeginGetProductPriceRatesByPriceRate(Guid id, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductPriceRatesByPriceRate(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductPriceRateRecord))]
        IAsyncResult BeginGetProductPriceRatesByRateAndProducts(Guid rateId, Guid[] productsId, DateTime workDate, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductPriceRatesByRateAndProducts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductPriceRateContract))]
        IAsyncResult BeginLoadProductPriceRate(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadProductPriceRate(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductPriceRateContract))]
        IAsyncResult BeginNewProductPriceRate(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewProductPriceRate(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateProductPriceRate(ProductPriceRateContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateProductPriceRate(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteProductPriceRate(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteProductPriceRate(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginCopyRate(CopyRateContract copyRateContract, AsyncCallback callback, object asyncState);
        ValidationResult EndCopyRate(IAsyncResult asyncResult);
        #endregion

        #region ProductRateType
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductRateTypeRecord))]
        IAsyncResult BeginGetProductRateTypes(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductRateTypes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductRateTypeContract))]
        IAsyncResult BeginLoadProductRateType(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadProductRateType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductRateTypeContract))]
        IAsyncResult BeginNewProductRateType(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewProductRateType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateProductRateType(ProductRateTypeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateProductRateType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteProductRateType(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteProductRateType(IAsyncResult asyncResult);
        #endregion

        #region Stand
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandRecord))]
        IAsyncResult BeginGetStands(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetStands(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(POSStandRecord))]
        IAsyncResult BeginGetPOSStands(int language, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPOSStands(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandContract))]
        IAsyncResult BeginLoadStand(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandTurnDateContract))]
        IAsyncResult BeginGetStandTurnDate(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndGetStandTurnDate(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandContract))]
        IAsyncResult BeginNewStand(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateStand(StandContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteStand(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteStand(IAsyncResult asyncResult);
        #endregion

        #region CookMarch
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CookMarchRecord))]
        IAsyncResult BeginGetCookMarchs(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCookMarchs(IAsyncResult asyncResult);


        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CookMarchContract))]
        IAsyncResult BeginLoadCookMarch(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadCookMarch(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CookMarchContract))]
        IAsyncResult BeginNewCookMarch(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewCookMarch(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateCookMarch(CookMarchContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateCookMarch(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteCookMarch(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteCookMarch(IAsyncResult asyncResult);
        #endregion

        #region Ticket

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(type: typeof(ClosedTicketRecord))]
        IAsyncResult BeginGetClosedTickets(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetClosedTickets(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SerieContract))]
        IAsyncResult BeginGetSerieConsecutive(long docTypeId, Guid standId, Guid cajaId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndGetSerieConsecutive(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SignatureContract))]
        IAsyncResult BeginGetSignature(long docTypeId, DateTime documentDate,
            DateTime documentEmissionDateTime, string documentSerie, long documentNumber, decimal documentValue,
            AsyncCallback callback, object asyncState);
        ValidationContractResult EndGetSignature(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TicketRecord))]
        IAsyncResult BeginGetTickets(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetTickets(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(POSTicketPrintConfigurationRecord))]
        IAsyncResult BeginGetTicketPrintConfigurations(AsyncCallback callback, object asyncState);
        QueryResponse EndGetTicketPrintConfigurations(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TicketContract))]
        IAsyncResult BeginLoadTicket(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadTicket(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TicketContract))]
        IAsyncResult BeginNewTicket(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewTicket(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateTicket(TicketContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateTicket(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductLineContract))]
        IAsyncResult BeginDefineTaxProductLine(ProductLineContract obj, AsyncCallback callback, object asyncState);
        ValidationContractResult EndDefineTaxProductLine(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteTicket(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteTicket(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginCancelTicketContracts(TicketRContract obj, CancellationControlContract cancellation, AsyncCallback callback, object asyncState);
        ValidationResult EndCancelTicketContracts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TicketSerieContract))]
        IAsyncResult BeginInactiveSerieByCaja(Guid ipos_pk, Guid caja_pk, Guid serieId, long serieNumber, long type, AsyncCallback callback, object asyncState);
        ValidationContractResult EndInactiveSerieByCaja(IAsyncResult asyncResult);

        //[OperationContract(AsyncPattern = true)]
        //[ServiceKnownType(typeof(TicketSerieContract))]
        //IAsyncResult BeginBlockCaja(Guid ipos_pk, Guid caja_pk, AsyncCallback callback, object asyncState);
        //ValidationContractResult EndBlockCaja(IAsyncResult asyncResult);

        //[OperationContract(AsyncPattern = true)]
        //IAsyncResult BeginUnBlockCaja(Guid ipos_pk, Guid caja_pk, long currentNumber, AsyncCallback callback, object asyncState);
        //ValidationResult EndUnBlockCaja(IAsyncResult asyncResult);
        #endregion

        #region TicketsError
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TicketsErrorRecord))]
        IAsyncResult BeginGetTicketsError(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetTicketsError(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TicketsErrorContract))]
        IAsyncResult BeginLoadTicketsError(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadTicketsError(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TicketRContract))]
        IAsyncResult BeginLoadTicketfromTicketError(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadTicketfromTicketError(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TicketsErrorContract))]
        IAsyncResult BeginNewTicketsError(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewTicketsError(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateTicketsError(TicketsErrorContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateTicketsError(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteTicketsError(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteTicketsError(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateTicketfromTicketError(TicketRContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateTicketfromTicketError(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(POSGeneralSettingsRecord))]
        IAsyncResult BeginGetGeneralSettings(AsyncCallback callback, object asyncState);
        QueryResponse EndGetGeneralSettings(IAsyncResult asyncResult);

        #endregion

        #region StandCajaLog
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandCajaLogContract))]
        IAsyncResult BeginLoadStandCajaLog(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadStandCajaLog(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandCajaLogContract))]
        IAsyncResult BeginNewStandCajaLog(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewStandCajaLog(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateStandCajaLog(StandCajaLogContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateStandCajaLog(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandCajaLogRecord))]
        IAsyncResult BeginGetStandCajaLog(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetStandCajaLog(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteStandCajaLog(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteStandCajaLog(IAsyncResult asyncResult);
        #endregion

        #region Preparations

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductPreparationsRecord))]
        IAsyncResult BeginGetAllProductPreparations(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllProductPreparations(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(FamilyPreparationsRecord))]
        IAsyncResult BeginGetAllFamilyPreparations(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllFamilyPreparations(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(GroupPreparationsRecord))]
        IAsyncResult BeginGetAllGroupPreparations(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllGroupPreparations(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PreparationsRecord))]
        IAsyncResult BeginGetPreparations(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPreparations(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PreparationsContract))]
        IAsyncResult BeginLoadPreparations(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadPreparations(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PreparationsContract))]
        IAsyncResult BeginNewPreparations(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewPreparations(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdatePreparations(PreparationsContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdatePreparations(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeletePreparations(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeletePreparations(IAsyncResult asyncResult);
        #endregion

        #region PreparationsFamily
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PreparationsFamilyProductContract))]
        IAsyncResult BeginGetPreparationsFamilys(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPreparationsFamilys(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddPreparationsFamily(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddPreparationsFamily(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeletePreparationsFamily(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeletePreparationsFamily(IAsyncResult asyncResult);
        #endregion

        #region PreparationsGroup
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PreparationsGroupProductContract))]
        IAsyncResult BeginGetPreparationsGroups(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPreparationsGroups(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddPreparationsGroup(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddPreparationsGroup(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeletePreparationsGroup(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeletePreparationsGroup(IAsyncResult asyncResult);
        #endregion

        #region PreparationsProduct
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PreparationsProductContract))]
        IAsyncResult BeginGetProductsPreparation(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductsPreparation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PreparationsRecord))]
        IAsyncResult BeginGetPreparationsProducts(QueryRequest req, Guid productId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPreparationsProducts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddPreparationsProduct(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddPreparationsProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeletePreparationsProduct(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeletePreparationsProduct(IAsyncResult asyncResult);
        #endregion

        #region Nationalities
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AllNationalitiesRecord))]
        IAsyncResult BeginGetAllNationalities(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllNationalities(IAsyncResult asyncResult);
        #endregion

        #region InternalUse
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(InternalUseRecord))]
        IAsyncResult BeginGetinternalUse(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetinternalUse(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(InternalUsexStandRecord))]
        IAsyncResult BeginGetInternalUsesXStand(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetInternalUsesXStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(InternalUseContract))]
        IAsyncResult BeginLoadinternalUse(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadinternalUse(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(InternalUseContract))]
        IAsyncResult BeginNewinternalUse(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewinternalUse(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateinternalUse(InternalUseContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateinternalUse(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteinternalUse(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteinternalUse(IAsyncResult asyncResult);

        #endregion

        #region InternalUseStand

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(InternalUsexStandContract))]
        IAsyncResult BeginGetInternalUseStands(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetInternalUseStands(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateInternalUseStandValue(Guid ownerId, double minValue, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateInternalUseStandValue(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddStandToInternalUseStand(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddStandToInternalUseStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteInternalUseStand(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteInternalUseStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(InternalUsexStandContract))]
        IAsyncResult BeginLoadInternalUseStand(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadInternalUseStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(InternalUsexStandContract))]
        IAsyncResult BeginNewInternalUseStand(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewInternalUseStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateInternalUseStand(InternalUsexStandContract internalUseStandContract, Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateInternalUseStand(IAsyncResult asyncResult);


        #endregion

        #region TableProduct
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TableProductRecord))]
        IAsyncResult BeginGetTableProducts(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetTableProducts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TableProductContract))]
        IAsyncResult BeginLoadTableProduct(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadTableProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TableProductContract))]
        IAsyncResult BeginNewTableProduct(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewTableProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateTableProduct(TableProductContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateTableProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteTableProduct(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteTableProduct(IAsyncResult asyncResult);
        #endregion

        #region Mesas

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SaloonRecord))]
        IAsyncResult BeginGetSaloons(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetSaloons(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MesaRecord))]
        IAsyncResult BeginGetUsedMesas(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetUsedMesas(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MesaRecord))]
        IAsyncResult BeginGetMesas(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetMesas(IAsyncResult asyncResult);

        //[OperationContract(AsyncPattern = true)]
        //[ServiceKnownType(typeof(MesaRecord))]
        //IAsyncResult BeginGetMesasByTablesGroup(QueryRequest req, AsyncCallback callback, object asyncState);
        //QueryResponse EndGetMesasByTablesGroup(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SaloonStandRecord))]
        IAsyncResult BeginGetSaloonsByStand(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetSaloonsByStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AllSaloonStandRecord))]
        IAsyncResult BeginGetAllSaloonStands(AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllSaloonStands(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SaloonContract))]
        IAsyncResult BeginLoadSaloon(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadSaloon(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SaloonContract))]
        IAsyncResult BeginNewSaloon(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewSaloon(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateSaloon(SaloonContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateSaloon(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteSaloon(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteSaloon(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MesaContract))]
        IAsyncResult BeginLoadMesa(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadMesa(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MesaContract))]
        IAsyncResult BeginNewMesa(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewMesa(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateMesa(MesaContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateMesa(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteMesa(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteMesa(IAsyncResult asyncResult);
        
        #endregion

        #region TablesGroups

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TablesGroupContract))]
        [ServiceKnownType(typeof(MesaTableGroupContract))]
        IAsyncResult BeginGetTablesGroups(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetTablesGroups(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TablesGroupContract))]
        IAsyncResult BeginLoadTablesGroup(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadTablesGroup(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TablesGroupContract))]
        IAsyncResult BeginNewTablesGroup(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewTablesGroup(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TablesGroupContract))]
        IAsyncResult BeginUpdateTablesGroup(TablesGroupContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateTablesGroup(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteTablesGroup(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteTablesGroup(IAsyncResult asyncResult);
        #endregion

        #region TablesReservationPeriods

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TablesReservationPeriodRecord))]
        IAsyncResult BeginGetTablesReservationPeriods(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetTablesReservationPeriods(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TablesReservationPeriodContract))]
        IAsyncResult BeginLoadTablesReservationPeriod(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadTablesReservationPeriod(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TablesReservationPeriodContract))]
        IAsyncResult BeginNewTablesReservationPeriod(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewTablesReservationPeriod(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateTablesReservationPeriod(TablesReservationPeriodContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateTablesReservationPeriod(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteTablesReservationPeriod(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteTablesReservationPeriod(IAsyncResult asyncResult);
        #endregion

        #region Booking Tables

        #region Booking Tables - Operations

        #region Check-In + Undo Check-In

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginCheckInBookingTable(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndCheckInBookingTable(IAsyncResult asyncResult);
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUndoCheckInBookingTable(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndUndoCheckInBookingTable(IAsyncResult asyncResult);

        #endregion
        #region Check-Out + Undo Check-Out

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginCheckOutBookingTable(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndCheckOutBookingTable(IAsyncResult asyncResult);
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUndoCheckOutBookingTable(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndUndoCheckOutBookingTable(IAsyncResult asyncResult);

        #endregion
        #region Cancel + Undo Cancel

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CancellationControlContract))]
        IAsyncResult BeginCancelTablesReservation(Guid[] ids, CancellationControlContract obj, AsyncCallback callback, object asyncState);
        ValidationResult[] EndCancelTablesReservation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUndoCancelTablesReservation(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult[] EndUndoCancelTablesReservation(IAsyncResult asyncResult);

        #endregion



        #endregion

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TablesReservationPeriodRecord))]
        IAsyncResult BeginGetTablesReservationAvailability(TablesReservationPeriodRecord period, DateTime date, Guid? Stand, int paxs, AsyncCallback callback, object asyncState);
        TablesReservationAvailabilityResult EndGetTablesReservationAvailability(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(LiteTableRecord))]
        [ServiceKnownType(typeof(TablesReservationLineContract))]
        IAsyncResult BeginGetAvailableTables(TablesReservationLineContract reservation, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAvailableTables(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TablesGroupRecord))]
        [ServiceKnownType(typeof(TablesReservationLineContract))]
        IAsyncResult BeginGetAvailableTablesGroups(TablesReservationLineContract reservation, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAvailableTablesGroups(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(LiteTableRecord))]
        [ServiceKnownType(typeof(TablesReservationLineContract))]
        IAsyncResult BeginGetUnavailableTables(TablesReservationLineContract reservation, AsyncCallback callback, object asyncState);
        QueryResponse EndGetUnavailableTables(IAsyncResult asyncResult);

        //[OperationContract(AsyncPattern = true)]
        //[ServiceKnownType(typeof(LiteTablesReservationRecord))]
        //IAsyncResult BeginSynchronizeTablesReservations(QueryRequest req, Guid[] localTablesReservationsIds, DateTime[] localTablesReservationsLastModifieds, AsyncCallback callback, object asyncState);
        //SynchronizeQueryResponse EndSynchronizeTablesReservations(IAsyncResult asyncResult);

        #region Get Booking Tables by Date

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(LiteTablesReservationRecord))]
        IAsyncResult BeginGetLiteTablesReservationsByDate(QueryRequest req, DateTime? date, AsyncCallback callback, object asyncState);
        QueryResponse EndGetLiteTablesReservationsByDate(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(LiteTablesReservationRecord))]
        IAsyncResult BeginGetLiteTablesReservations(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetLiteTablesReservations(IAsyncResult asyncResult);

        #endregion

        #region New, Load, Update Booking Tables

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TablesReservationLineContract))]
        IAsyncResult BeginNewTablesReservationLine(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewTablesReservationLine(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TablesReservationLineContract))]
        IAsyncResult BeginLoadTablesReservationLine(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadTablesReservationLine(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TablesReservationLineContract))]
        IAsyncResult BeginUpdateTablesReservationLine(TablesReservationLineContract obj, AsyncCallback callback, object asyncState);
        PersistOccupationLineResult EndUpdateTablesReservationLine(IAsyncResult asyncResult);

        #endregion

        #endregion

        


        

        #region StandProduct

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandProductsContract))]
        IAsyncResult BeginGetStandProducts(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetStandProducts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandProductsContract))]
        IAsyncResult BeginGetProductsByStand(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductsByStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandProductAllRecord))]
        IAsyncResult BeginGetProductsByStandAll(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductsByStandAll(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandProductRecord))]
        IAsyncResult BeginGetStandProductsByStand(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetStandProductsByStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandsProductRecord))]
        IAsyncResult BeginGetStandProductsByStands(QueryRequest req, Guid[] stands, AsyncCallback callback, object asyncState);
        QueryResponse EndGetStandProductsByStands(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandProductsContract))]
        IAsyncResult BeginGetStandProductsByProduct(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetStandProductsByProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteStandProducts(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteStandProducts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteProductsByStand(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteProductsByStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandProductsContract))]
        IAsyncResult BeginLoadStandProduct(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadStandProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandProductsContract))]
        IAsyncResult BeginNewStandProduct(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewStandProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateStandProduct(StandProductsContract standProductContract, Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateStandProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddStandProductToStand(Guid ownerId, StandProductsContract[] stands, AsyncCallback callback, object asyncState);
        ValidationResult EndAddStandProductToStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteStandProductFromStand(Guid ownerId, Guid[] standProducts, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteStandProductFromStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginSetServiceToAllStandProducts(Guid ownerId, Guid? service, string serviceDesc, AsyncCallback callback, object asyncState);
        ValidationResult EndSetServiceToAllStandProducts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginSetServiceToStandProducts(Guid ownerId, Guid[] standProducts, Guid? service, string serviceDesc, AsyncCallback callback, object asyncState);
        ValidationResult EndSetServiceToStandProducts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddStandProductToProduct(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddStandProductToProduct(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteStandProductFromProduct(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteStandProductFromProduct(IAsyncResult asyncResult);


        #endregion

        #region StandCajas

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AllStandCajaRecord))]
        IAsyncResult BeginGetAllStandCajas(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllStandCajas(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandCajasContract))]
        IAsyncResult BeginGetStandCajas(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetStandCajas(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandByCajaRecord))]
        IAsyncResult BeginGetCajasByStand(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCajasByStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddStandCajaToStand(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddStandCajaToStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteStandCajasFromStand(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteStandCajasFromStand(IAsyncResult asyncResult);


        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddStandCajaToCaja(CajaContract cajaContract, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddStandCajaToCaja(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteStandCajasFromCaja(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteStandCajasFromCaja(IAsyncResult asyncResult);
        #endregion

        #region PricePeriod

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PricePeriodContract))]
        IAsyncResult BeginGetPricePeriods(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPricePeriods(IAsyncResult asyncResult);


        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PricePeriodRecord))]
        IAsyncResult BeginGetAllPricePeriods(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllPricePeriods(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeletePricePeriods(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndDeletePricePeriods(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PricePeriodContract))]
        IAsyncResult BeginLoadPricePeriod(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadPricePeriod(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ContainerProductsInPeriod))]
        IAsyncResult BeginGetProductsByPeriod(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndGetProductsByPeriod(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginCopyProductsToPeriod(Guid ownerId, Guid newPeriodId, bool Percent, bool Plus, decimal SelectedValue, AsyncCallback callback, object asyncState);
        ValidationResult EndCopyProductsToPeriod(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RatesVisualContract))]
        IAsyncResult BeginLoadRateVisual(AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadRateVisual(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductRatesVisualContract))]
        IAsyncResult BeginLoadProductRateVisual(Guid productId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadProductRateVisual(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductRatesVisualContract))]
        IAsyncResult BeginLoadAllPeriods(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadAllPeriods(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PricePeriodContract))]
        IAsyncResult BeginNewPricePeriod(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewPricePeriod(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdatePricePeriod(PricePeriodContract pricePeriodContract, Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdatePricePeriod(IAsyncResult asyncResult);


        #endregion

        #region BlockPeriod

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BlockPeriodContract))]
        IAsyncResult BeginGetBlockPeriods(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetBlockPeriods(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteBlockPeriods(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteBlockPeriods(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BlockPeriodContract))]
        IAsyncResult BeginLoadBlockPeriod(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadBlockPeriod(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BlockPeriodContract))]
        IAsyncResult BeginNewBlockPeriod(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewBlockPeriod(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateBlockPeriod(BlockPeriodContract BlockPeriodContract, Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateBlockPeriod(IAsyncResult asyncResult);


        #endregion

        #region StandPriceCost

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductPriceCostContract))]
        IAsyncResult BeginGetStandPriceCosts(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetStandPriceCosts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteStandPriceCosts(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteStandPriceCosts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductPriceCostContract))]
        IAsyncResult BeginLoadStandPriceCost(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadStandPriceCost(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductPriceCostContract))]
        IAsyncResult BeginNewStandPriceCost(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewStandPriceCost(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateStandPriceCost(ProductPriceCostContract standPriceCostContract, Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateStandPriceCost(IAsyncResult asyncResult);


        #endregion

        #region Categories

        #region CategorySubCategory
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CategoryRecord))]
        IAsyncResult BeginGetTopCategories(AsyncCallback callback, object asyncState);
        QueryResponse EndGetTopCategories(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductsInCategoryRecord))]
        IAsyncResult BeginGetProductsInCategories(Guid standId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductsInCategories(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CategoryRecord))]
        IAsyncResult BeginGetCategoriesInUse(AsyncCallback callback, object asyncState);
        QueryResponse EndGetCategoriesInUse(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CategorySubCategoryContract))]
        IAsyncResult BeginGetCategoriesByCategories(Guid[] ids, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCategoriesByCategories(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CategorySubCategoryContract))]
        IAsyncResult BeginGetCategories(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCategories(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CategorySubCategoryContract))]
        IAsyncResult BeginLoadCategory(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadCategory(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CategorySubCategoryContract))]
        IAsyncResult BeginNewCategory(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewCategory(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CategorySubCategoryContract))]
        IAsyncResult BeginUpdateCategory(CategorySubCategoryContract obj, AsyncCallback callback, object asyncState);
        ValidationContractResult EndUpdateCategory(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteCategory(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteCategory(IAsyncResult asyncResult);
        #endregion

        #region ProductStandCategory

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductStandCategoryContract))]
        IAsyncResult BeginGetProductStandCategoryByCategory(Guid standId, Guid categoryId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductStandCategoryByCategory(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductStandCategoryContract))]
        IAsyncResult BeginGetProductStandCategoryByCategoryStands(object[] standIds, Guid categoryId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductStandCategoryByCategoryStands(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductStandCategoryContract))]
        IAsyncResult BeginGetProductStandCategory(Guid standId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductStandCategory(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductStandCategoryContract))]
        IAsyncResult BeginNewProductStandCategory(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewProductStandCategory(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateProductStandCategory(ProductStandCategoryContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateProductStandCategory(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteProductStandCategory(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteProductStandCategory(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateProductStandsCategory(ProductStandCategoryContract obj, Guid[] stands, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateProductStandsCategory(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteProductStandsCategory(Guid[] ids, Guid catId, Guid[] stands, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteProductStandsCategory(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginInsertProductStandsCategory(ProductStandCategoryContract obj, Guid[] stands, Guid? prev, Guid? next, AsyncCallback callback, object asyncState);
        ValidationResult EndInsertProductStandsCategory(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginCopyProductStandCategoriesFrom(Guid from, Guid to, AsyncCallback callback, object asyncState);
        ValidationResult EndCopyProductStandCategoriesFrom(IAsyncResult asyncResult);

        #endregion



        #endregion

        #region TicketSerie

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SerieByStandRecord))]
        IAsyncResult BeginGetSeries(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetSeries(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SerieDescRecord))]
        IAsyncResult BeginGetSeriesDesc(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetSeriesDesc(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TicketSerieContract))]
        IAsyncResult BeginGetSerieByStandCaja(Guid standId, Guid? cajaId, long type, AsyncCallback callback, object asyncState);
        ValidationContractResult EndGetSerieByStandCaja(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TicketSerieContract))]
        IAsyncResult BeginGetSerieByStandCajaInfo(Guid standId, Guid cajaId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndGetSerieByStandCajaInfo(IAsyncResult asyncResult);
        #endregion

        #region TicketR

       


        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TicketRealRecord))]
        IAsyncResult BeginGetTicketsR(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetTicketsR(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TicketRContract))]
        IAsyncResult BeginLoadTicketR(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadTicketR(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginVoidOldTicket(Guid ticketId, CancellationControlContract cancelationControlContract, Guid[] productLinesIds, AsyncCallback callback, object asyncState);
        ValidationResult EndVoidOldTicket(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginVoidTicket(Guid ticketId, Guid? cancellationReasonId, string comments, AsyncCallback callback, object asyncState);
        ValidationResult EndVoidTicket(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TicketRContract))]
        IAsyncResult BeginNewTicketR(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewTicketR(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateTicketR(TicketRContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateTicketR(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdatePOSTicket(POSTicketContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdatePOSTicket(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteTicketR(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteTicketR(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginChargePOSTicketAccount(POSTicketContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndChargePOSTicketAccount(IAsyncResult asyncResult);

        #endregion

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PaymentLineContract))]
        IAsyncResult BeginGetPaymentLinesByTicket(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPaymentLinesByTicket(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PaymentLineRecord))]
        IAsyncResult BeginGetPaymentLines(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPaymentLines(IAsyncResult asyncResult);

        #region ProductLine

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductLineContract))]
        IAsyncResult BeginGetProductLinesByTicket(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductLinesByTicket(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductLineContract))]
        IAsyncResult BeginLoadProductLine(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadProductLine(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductLineContract))]
        IAsyncResult BeginNewProductLine(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewProductLine(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateProductLine(ProductLineContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateProductLine(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteProductLine(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteProductLine(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddProductLineToTicketR(Guid ownerId, ProductLineContract productLineContract, AsyncCallback callback, object asyncState);
        ValidationResult EndAddProductLineToTicketR(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveProductLinesToTicketR(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveProductLinesToTicketR(IAsyncResult asyncResult);

        #endregion

        #region PaymentLine

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PaymentLineContract))]
        IAsyncResult BeginLoadPaymentLine(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadPaymentLine(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PaymentLineContract))]
        [ServiceKnownType(typeof(CreditCardContract))]
        IAsyncResult BeginNewPaymentLine(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewPaymentLine(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdatePaymentLine(PaymentLineContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdatePaymentLine(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeletePaymentLine(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeletePaymentLine(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddPaymentLineToTicketR(Guid ownerId, PaymentLineContract paymentLineContract, AsyncCallback callback, object asyncState);
        ValidationResult EndAddPaymentLineToTicketR(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemovePaymentLinesToTicketR(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemovePaymentLinesToTicketR(IAsyncResult asyncResult);

        #endregion

        #region Users

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateUserCode(Guid userId, string code, AsyncCallback callback, object asyncState);
        UpdateUserCodeResult EndUpdateUserCode(IAsyncResult asyncResult);

        #endregion

        #region Local
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PosTaxServicesRecord))]
        IAsyncResult BeginGetPosTaxServices(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPosTaxServices(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AllTaxesRecord))]
        IAsyncResult BeginGetAllTaxes(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllTaxes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AllTaxSchemaRecord))]
        IAsyncResult BeginGetAllTaxSchema(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllTaxSchema(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AllTaxSequenceRecord))]
        IAsyncResult BeginGetAllTaxSequence(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllTaxSequence(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AllProductsRecord))]
        IAsyncResult BeginGetAllProducts(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllProducts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AllCategoriesRecord))]
        IAsyncResult BeginGetAllCategories(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllCategories(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AllCategoriesRecord))]
        IAsyncResult BeginSynchronizeCategories(QueryRequest req, Guid[] localCategoryIds, DateTime[] localCategoryLastModifieds, AsyncCallback callback, object asyncState);
        SynchronizeQueryResponse EndSynchronizeCategories(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AllProductsRecord))]
        IAsyncResult BeginSynchronizeProducts(QueryRequest req, Guid[] localProductIds, DateTime[] localProductLastModifieds, AsyncCallback callback, object asyncState);
        SynchronizeQueryResponse EndSynchronizeProducts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AllStandProductsRecord))]
        IAsyncResult BeginSynchronizeStandProducts(QueryRequest req, Guid[] localStandProductIds, DateTime[] localStandProductLastModifieds, AsyncCallback callback, object asyncState);
        SynchronizeQueryResponse EndSynchronizeStandProducts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AllStandProductsCategoryRecord))]
        IAsyncResult BeginSynchronizeStandProductsCategory(QueryRequest req, Guid[] localStandProductCategoryIds, DateTime[] localStandProductCategoryLastModifieds, AsyncCallback callback, object asyncState);
        SynchronizeQueryResponse EndSynchronizeStandProductsCategory(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AllStandProductsRecord))]
        IAsyncResult BeginGetAllStandProducts(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllStandProducts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AllStandProductsCategoryRecord))]
        IAsyncResult BeginGetAllStandProductsCategory(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllStandProductsCategory(IAsyncResult asyncResult);

        #region Areas

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AreaRecord))]
        IAsyncResult BeginGetAreas(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAreas(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AllStandProductAreaRecord))]
        IAsyncResult BeginGetAllStandProductAreas(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllStandProductAreas(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DispatchAreaContract))]
        IAsyncResult BeginLoadAreas(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadAreas(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DispatchAreaContract))]
        IAsyncResult BeginNewAreas(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewAreas(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateAreas(DispatchAreaContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateAreas(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteAreas(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteAreas(IAsyncResult asyncResult);

        #endregion

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SettingsRecord))]
        IAsyncResult BeginGetSettingsParameters(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetSettingsParameters(IAsyncResult asyncResult);
        #endregion

        #region Users

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(UserRecord))]
        IAsyncResult BeginGetUser(QueryRequest req, string login, string pass, AsyncCallback callback, object asyncState);
        QueryResponse EndGetUser(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(UserRecord))]
        IAsyncResult BeginGetUserByCode(QueryRequest req, string code, AsyncCallback callback, object asyncState);
        QueryResponse EndGetUserByCode(IAsyncResult asyncResult);

        #endregion

        #region Others

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(GastStandRecord))]
        IAsyncResult BeginGetGastStand(Guid? ipos, AsyncCallback callback, object asyncState);
        QueryResponse EndGetGastStand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PensionByRoomRecord))]
        IAsyncResult BeginGetPensionByRoom(Guid? lioc_pk, DateTime date, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPensionByRoom(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PensionByRoomRecord))]
        IAsyncResult BeginGetGuestsByRoom(string aloj, AsyncCallback callback, object asyncState);
        QueryResponse EndGetGuestsByRoom(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PrintByPeriodRecord))]
        IAsyncResult BeginGetPrintByPeriod(Guid ipos, DateTime date, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPrintByPeriod(IAsyncResult asyncResult);

        #endregion

        #region Services
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ServiceTaxContract))]
        IAsyncResult BeginGetServiceTaxes(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetServiceTaxes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ServiceTaxContract))]
        IAsyncResult BeginNewServiceTax(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewServiceTax(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ServiceTaxContract))]
        IAsyncResult BeginLoadServiceTax(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadServiceTax(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddServiceTax(Guid ownerId, ServiceTaxContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndAddServiceTax(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveServiceTaxes(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveServiceTaxes(IAsyncResult asyncResult);
        #endregion

        #region PricePeriodProduct;
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeletePricePeriodProduct(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeletePricePeriodProduct(IAsyncResult asyncResult);

        #endregion

        #region Management
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginValidateLogIn(Guid standId, Guid? cajaId, AsyncCallback callback, object asyncState);
        ValidationResult EndValidateLogIn(IAsyncResult asyncResult);


        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(LogInContract))]
        IAsyncResult BeginLogIn(Guid standId, Guid cajaId, Guid userId, Guid? fisi, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLogIn(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginLogOut(Guid standId, Guid cajaId, long ticketSerieNumber, long? invoiceSerieNumber, long? receiptSerieNumber, string ticketSign, string invoiceSign, string receiptSign, AsyncCallback callback, object asyncState);
        ValidationResult EndLogOut(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginCancelTicket(Guid ownerId, Guid cancellationReason, string cancellationRDesc, string comments, AsyncCallback callback, object asyncState);
        ValidationResult EndCancelTicket(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginVerifyCloseDay(Guid standId, Guid cajaId, AsyncCallback callback, object asyncState);
        ValidationResult EndVerifyCloseDay(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandTurnDateContract))]
        IAsyncResult BeginCloseDay(Guid standId, Guid? cajaId, DateTime? closeDay, AsyncCallback callback, object asyncState);
        ValidationContractResult EndCloseDay(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandTurnDateContract))]
        IAsyncResult BeginCloseTurn(Guid standId, Guid? cajaId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndCloseTurn(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginVerifyTaxesSchema(Guid servicePk, Guid? schema, AsyncCallback callback, object asyncState);
        bool EndVerifyTaxesSchema(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginSubmitReductionZ(ReductionZContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndSubmitReductionZ(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ReductionZRecord))]
        IAsyncResult BeginGetReductionsZ(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetReductionsZ(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginSubmitNfc(TicketRContract obj, AsyncCallback callback, object asyncState);
        ValidationStringResult EndSubmitNfc(IAsyncResult asyncResult);
        #endregion

        #region TransferActions
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginTrasferTicket(TransferContract contract, TicketRContract ticketContract, AsyncCallback callback, object asyncState);
        ValidationResult EndTrasferTicket(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TicketRContract))]
        IAsyncResult BeginAcceptTransferTicket(TransferContract contract, Guid cajaId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndAcceptTransferTicket(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TicketRContract))]
        IAsyncResult BeginReturnTransferTicket(TransferContract contract, AsyncCallback callback, object asyncState);
        ValidationContractResult EndReturnTransferTicket(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ContainerTransferContract))]
        IAsyncResult BeginVerifyTransferReturns(Guid stand, Guid caja, AsyncCallback callback, object asyncState);
        ValidationContractResult EndVerifyTransferReturns(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ContainerTransferContract))]
        IAsyncResult BeginVerifyTransferToAccept(Guid stand, AsyncCallback callback, object asyncState);
        ValidationContractResult EndVerifyTransferToAccept(IAsyncResult asyncResult);
        #endregion

        #region ImagesGallery
        #region ImagesGallery
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(FolderContract))]
        IAsyncResult BeginGetImagesGallery(int type, AsyncCallback callback, object asyncState);
        ValidationContractResult EndGetImagesGallery(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(FilesFolderContract))]
        IAsyncResult BeginGetImagesFromFile(Guid parentId, string path, int page, AsyncCallback callback, object asyncState);
        ValidationContractResult EndGetImagesFromFile(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ImageInfoContract))]
        IAsyncResult BeginMoveImageToService(string imageName, byte[] image, int type, AsyncCallback callback, object asyncState);
        ValidationContractResult EndMoveImageToService(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveImageCustom(string imageName, int type, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveImageCustom(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ImageInfoContract))]
        IAsyncResult BeginGetImageFromPath(string path, AsyncCallback callback, object asyncState);
        ValidationContractResult EndGetImageFromPath(IAsyncResult asyncResult);

        #endregion
        #endregion

        #region Reports Sales
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SalesCashierRecord))]
        IAsyncResult BeginGetSalesCashier(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetSalesCashier(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SalesCajaRecord))]
        IAsyncResult BeginGetSalesCaja(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetSalesCaja(IAsyncResult asyncResult);
        #endregion

        #region Reports
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ReportContainerContract))]
        IAsyncResult BeginLoadReportStructureUpdate(int appl_pk, Guid reportContainerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadReportStructureUpdate(IAsyncResult asyncResult);


        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ReportContainerContract))]
        IAsyncResult BeginLoadReportStructureListUpdate(short keywordId, int appl_pk, Guid reportContainerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadReportStructureListUpdate(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ReportContainerContract))]
        IAsyncResult BeginLoadReportStructure(int appl_pk, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadReportStructure(IAsyncResult asyncResult);


        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ReportContainerContract))]
        IAsyncResult BeginLoadReportStructureList(short keywordId, int appl_pk, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadReportStructureList(IAsyncResult asyncResult);

        //[OperationContract(AsyncPattern = true)]
        //IAsyncResult BeginSetReportFavorite(long repo_pk, AsyncCallback callback, object asyncState);
        //ValidationResult EndSetReportFavorite(IAsyncResult asyncResult);
        #endregion

        #region User
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(UserContract))]
        IAsyncResult BeginNewUser(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewUser(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(UserRecord))]
        IAsyncResult BeginGetUsers(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetUsers(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(UserRecord))]
        IAsyncResult BeginGetAllUsers(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllUsers(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(UserContract))]
        IAsyncResult BeginLoadUser(Guid userId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadUser(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(UserContract))]
        IAsyncResult BeginUpdateUser(UserContract userContract, AsyncCallback callback, object asyncState);
        PersistUserResult EndUpdateUser(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginSearchUser(string login, string description, AsyncCallback callback, object asyncState);
        PersistUserResult EndSearchUser(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginInactiveUser(Guid[] userIds, AsyncCallback callback, object asyncState);
        ValidationResult EndInactiveUser(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteUser(Guid[] userIds, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteUser(IAsyncResult asyncResult);


        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ApplicationByUserContract))]
        IAsyncResult BeginGetApplicationbyUser(QueryRequest req, Guid userId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetApplicationbyUser(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddApplicationbyUser(long[] ids, UserContract usercontract, AsyncCallback callback, object asyncState);
        ValidationResult EndAddApplicationbyUser(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveApplicationbyUser(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveApplicationbyUser(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ApplicationModuleRecord))]
        IAsyncResult BeginGetApplication(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetApplication(IAsyncResult asyncResult);
        #endregion

        #region Security

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProfileContract))]
        IAsyncResult BeginNewProfile(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewProfile(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProfileRecord))]
        IAsyncResult BeginGetProfile(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProfile(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProfileContract))]
        IAsyncResult BeginLoadProfile(Guid profileId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadProfile(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProfileContract))]
        IAsyncResult BeginUpdateProfile(ProfileContract profileContract, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateProfile(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteProfile(Guid[] profileIds, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteProfile(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProfilePermissionsRecord))]
        IAsyncResult BeginGetProfilePermissions(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProfilePermissions(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(UserPermissionsRecord))]
        IAsyncResult BeginGetUserPermissions(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetUserPermissions(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(LoginPeriodContract))]
        IAsyncResult BeginGetLoginPeriods(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetLoginPeriods(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(GroupPermissionsRecord))]
        IAsyncResult BeginGetGroupPermissions(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetGroupPermissions(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(GroupRecord))]
        IAsyncResult BeginGetGroups(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetGroups(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoleRecord))]
        IAsyncResult BeginGetRoles(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetRoles(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PermissionByApplicationContract))]
        IAsyncResult BeginLoadApplicationsPermissions(long[] applicationIds, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadApplicationsPermissions(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AllUserPermissionRecord))]
        IAsyncResult BeginGetAllUserPermissions(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllUserPermissions(IAsyncResult asyncResult);
        #endregion

        #region License

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(LicenseInformationContract))]
        IAsyncResult BeginGetLicenseInformation(AsyncCallback callback, object asyncState);
        ValidationContractResult EndGetLicenseInformation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(LicensePayContract))]
        IAsyncResult BeginGetLicensePaymentInformation(AsyncCallback callback, object asyncState);
        ValidationContractResult EndGetLicensePaymentInformation(IAsyncResult asyncResult);

        #endregion

        #region MOVEMENT TEMP
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MoviResultsContract))]
        IAsyncResult BeginUpdateSimpleEntryExternal(MovementExternalContract entrieContract, AsyncCallback callback, object asyncState);
        ValidationContractResult EndUpdateSimpleEntryExternal(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MoviResultsContract))]
        IAsyncResult BeginUpdateSimpleEntryExternalDebit(MovementExternalContract entrieContract, AsyncCallback callback, object asyncState);
        ValidationContractResult EndUpdateSimpleEntryExternalDebit(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MoviResultsContract))]
        IAsyncResult BeginUpdateAllMovements(List<TicketRContract> tickets, Guid stand, Guid? accountId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndUpdateAllMovements(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateInvoiceExternal(List<Guid> movements, FactInformationContract factInfo, AsyncCallback callback, object asyncState);
        InvoiceExternalResult EndUpdateInvoiceExternal(IAsyncResult asyncResult);


        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginInvoiceAndPersistTicket(TicketRContract ticket, FactInformationContract buyerInfo, AsyncCallback callback, object asyncState);
        InvoiceExternalResult EndInvoiceAndPersistTicket(IAsyncResult asyncResult);

        #region Fiscal Documents
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TicketFiscalDocumentRecord))]
        IAsyncResult BeginGetTicketFiscalDocuments(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetTicketFiscalDocuments(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginPersistFiscalDocumentFromTickets(List<TicketRealRecord> tickets, AsyncCallback callback, object asyncState);
        ValidationResult EndPersistFiscalDocumentFromTickets(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginCancelFiscalDocumentFromTickets(Guid fiscalDocumentId, AsyncCallback callback, object asyncState);
        ValidationResult EndCancelFiscalDocumentFromTickets(IAsyncResult asyncResult); 
        #endregion

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginCheckCashierUser(Guid caja, Guid user, Guid hotel, AsyncCallback callback, object asyncState);
        ValidationResult EndCheckCashierUser(IAsyncResult asyncResult);

        #endregion

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CurrencyInstallationRecord))]
        IAsyncResult BeginGetCurrencyInstallations(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCurrencyInstallations(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ControlAccountRecord))]
        IAsyncResult BeginGetControlAccounts(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetControlAccounts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ServiceRecord))]
        IAsyncResult BeginGetServices(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetServices(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TaxSchemaRecord))]
        IAsyncResult BeginGetTaxSchemas(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetTaxSchemas(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TaxRateRecord))]
        IAsyncResult BeginGetTaxRates(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetTaxRates(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PropTypeRecord))]
        IAsyncResult BeginGetPropTypes(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPropTypes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MinStandWorkDateRecord))]
        IAsyncResult BeginGetMinStandWorkDate(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetMinStandWorkDate(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginPersistCreditCard(CreditCardContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndPersistCreditCard(IAsyncResult asyncResult);

        #region Cancellation

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateCancellationControl(CancellationControlContract cancellationContract, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateCancellationControl(IAsyncResult asyncResult);

        #endregion

        #region Logs

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ReservationLogRecord))]
        IAsyncResult BeginGetReservationLogs(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetReservationLogs(IAsyncResult asyncResult);

        #endregion

        #region LookupTable
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(LookupTableContract))]
        IAsyncResult BeginNewLookupTable(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewLookupTable(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateLookupTable(LookupTableContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateLookupTable(IAsyncResult asyncResult);
        #endregion

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductRecord))]
        IAsyncResult BeginGetTipServices(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetTipServices(IAsyncResult asyncResult);


        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DateValueNFListContract))]
        IAsyncResult BeginGetNfByDate(DateTime Date1, DateTime Date2, AsyncCallback callback, object asyncState);
        ValidationContractResult EndGetNfByDate(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(LookupTableDetailsContract))]
        IAsyncResult BeginNewLookupTableDetail(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewLookupTableDetail(IAsyncResult asyncResult);

        #region Clients

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateClient(ClientContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateClient(IAsyncResult asyncResult);

        #endregion Clients

    }
}
