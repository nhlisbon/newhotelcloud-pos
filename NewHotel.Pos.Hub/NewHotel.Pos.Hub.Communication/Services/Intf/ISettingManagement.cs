﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using NewHotel.Contracts;

namespace NewHotel.Communication.Interfaces
{
    [ServiceContract]
    public interface ISettingManagement
    {
        #region Enums
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EnumsRecord))]
        IAsyncResult BeginGetEnums(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetEnums(IAsyncResult asyncResult);
        #endregion

        #region Parameters
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SettingHotelContract))]
        IAsyncResult BeginNewHotelSettings(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewHotelSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SettingHotelContract))]
        IAsyncResult BeginLoadHotelSettings(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadHotelSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateHotelSettings(SettingHotelContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateHotelSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SettingGeneralContract))]
        IAsyncResult BeginNewSettingsGeneral(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewSettingsGeneral(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SettingGeneralContract))]
        IAsyncResult BeginLoadSettingsGeneral(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadSettingsGeneral(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateSettingsGeneral(SettingGeneralContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateSettingsGeneral(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SettingInvoiceContract))]
        IAsyncResult BeginNewInvoiceSettings(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewInvoiceSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SettingInvoiceContract))]
        IAsyncResult BeginLoadInvoiceSettings(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadInvoiceSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateInvoiceSettings(SettingInvoiceContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateInvoiceSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SettingNPosContract))]
        IAsyncResult BeginNewNPosSettings(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewNPosSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SettingNPosContract))]
        IAsyncResult BeginLoadNPosSettings(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadNPosSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateNPosSettings(SettingNPosContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateNPosSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SettingCondoContract))]
        IAsyncResult BeginNewCondoSettings(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewCondoSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SettingCondoContract))]
        IAsyncResult BeginLoadCondoSettings(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadCondoSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateCondoSettings(SettingCondoContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateCondoSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SettingPriceContract))]
        IAsyncResult BeginNewPriceSettings(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewPriceSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SettingPriceContract))]
        IAsyncResult BeginLoadPriceSettings(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadPriceSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdatePriceSettings(SettingPriceContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdatePriceSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SettingReservationContract))]
        IAsyncResult BeginNewReservationSettings(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewReservationSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SettingReservationContract))]
        IAsyncResult BeginLoadReservationSettings(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadReservationSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateReservationSettings(SettingReservationContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateReservationSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SettingResourceContract))]
        IAsyncResult BeginNewResourceSettings(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewResourceSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SettingResourceContract))]
        IAsyncResult BeginLoadResourceSettings(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadResourceSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateResourceSettings(SettingResourceContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateResourceSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginLoadSettingsTemplate(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadSettingsTemplate(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateSettingsTemplate(SettingTemplateContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateSettingsTemplate(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateAllSettings(SettingReservationContract reservation, SettingHotelContract hotel, SettingInvoiceContract invoice,
                                            SettingResourceContract resource, SettingPriceContract price, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateAllSettings(IAsyncResult asyncResult);

        #region Resource settings by type
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SettingResourceByTypeContract))]
        IAsyncResult BeginGetResourceSettingsByType(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetResourceSettingsByType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SettingResourceByTypeContract))]
        IAsyncResult BeginLoadResourceSettingsByType(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadResourceSettingsByType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateResourceSettingsByType(SettingResourceByTypeContract obj, Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateResourceSettingsByType(IAsyncResult asyncResult);

        #endregion

        #endregion

        #region Application
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ApplicationRecord))]
        IAsyncResult BeginGetApplications(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetApplications(IAsyncResult asyncResult);
        #endregion

        #region Users
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginCheckUserNameAndPassword(string username, string password, AsyncCallback callback, object asyncState);
        ValidationResult EndCheckUserNameAndPassword(IAsyncResult asyncResult);
        #endregion

        #region Attention
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AttentionRecord))]
        IAsyncResult BeginGetAttentions(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAttentions(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AttentionContract))]
        IAsyncResult BeginLoadAttention(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadAttention(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AttentionContract))]
        IAsyncResult BeginNewAttention(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewAttention(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateAttention(AttentionContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateAttention(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteAttention(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteAttention(IAsyncResult asyncResult);
        #endregion

        #region Additional resource
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AdditionalRecord))]
        IAsyncResult BeginGetAdditionalResources(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAdditionalResources(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AdditionalResourceContract))]
        IAsyncResult BeginLoadAdditionalResource(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadAdditionalResource(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AdditionalResourceContract))]
        IAsyncResult BeginNewAdditionalResource(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewAdditionalResource(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateAdditionalResource(AdditionalResourceContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateAdditionalResource(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteAdditionalResource(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteAdditionalResource(IAsyncResult asyncResult);
        #endregion

        #region Additional fixed

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AdditionalFixedRecord))]
        IAsyncResult BeginGetAdditionalFixed(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAdditionalFixed(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AdditionalFixedContract))]
        IAsyncResult BeginLoadAdditionalFixed(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadAdditionalFixed(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AdditionalFixedContract))]
        IAsyncResult BeginNewAdditionalFixed(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewAdditionalFixed(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateAdditionalFixed(AdditionalFixedContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateAdditionalFixed(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateAdditionalFixedList(List<AdditionalFixedRecord> addFixedContractList, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateAdditionalFixedList(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteAdditionalFixed(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteAdditionalFixed(IAsyncResult asyncResult);

        #endregion

        #region Additional allotment

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AdditionalAllotmentRecord))]
        IAsyncResult BeginGetAdditionalAllotment(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAdditionalAllotment(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AdditionalAllotmentContract))]
        IAsyncResult BeginLoadAdditionalAllotment(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadAdditionalAllotment(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AdditionalAllotmentContract))]
        IAsyncResult BeginNewAdditionalAllotment(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewAdditionalAllotment(IAsyncResult asyncResult);


        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateAdditionalAllotment(AdditionalAllotmentContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateAdditionalAllotment(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteAdditionalAllotment(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteAdditionalAllotment(IAsyncResult asyncResult);

        #endregion

        #region Additional resource type
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AdditionalResourceTypeRecord))]
        IAsyncResult BeginGetAdditionalResourceTypes(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAdditionalResourceTypes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AdditionalResourceTypeContract))]
        IAsyncResult BeginLoadAdditionalResourceType(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadAdditionalResourceType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AdditionalResourceTypeContract))]
        IAsyncResult BeginNewAdditionalResourceType(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewAdditionalResourceType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateAdditionalResourceType(AdditionalResourceTypeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateAdditionalResourceType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteAdditionalResourceType(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteAdditionalResourceType(IAsyncResult asyncResult);
        #endregion

        #region Bank

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BankRecord))]
        IAsyncResult BeginGetBanks(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetBanks(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BankContract))]
        IAsyncResult BeginLoadBank(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadBank(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BankContract))]
        IAsyncResult BeginNewBank(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewBank(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateBank(BankContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateBank(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteBank(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteBank(IAsyncResult asyncResult);

        #endregion
        #region BankAccounts

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BankAccountContract))]
        IAsyncResult BeginGetBankAccount(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetBankAccount(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BankAccountContract))]
        IAsyncResult BeginNewBankAccount(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewBankAccount(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BankAccountContract))]
        IAsyncResult BeginLoadBankAccount(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadBankAccount(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateBankAccount(BankAccountContract obj, Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateBankAccount(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteBankAccount(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteBankAccount(IAsyncResult asyncResult);

        #endregion

        #region Currency

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(InstallationCurrencyRecord))]
        IAsyncResult BeginGetMultiCurrencyExchanges(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetMultiCurrencyExchanges(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(InstallationCurrencyRecord))]
        IAsyncResult BeginGetInstallationCurrencies(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetInstallationCurrencies(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CurrencyInstallationContract))]
        IAsyncResult BeginLoadInstallationCurrency(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadInstallationCurrency(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CurrencyInstallationContract))]
        IAsyncResult BeginLoadFromISO(string code, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadFromISO(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(OnlineExchangeContract))]
        IAsyncResult BeginNewOnlineExchangeRates(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewOnlineExchangeRates(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(OnlineExchangeContract))]
        IAsyncResult BeginUpdateOnlineExchangeRates(OnlineExchangeContract obj, AsyncCallback callback, object asyncState);
        ValidationContractResult EndUpdateOnlineExchangeRates(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(OnlineExchangeContract))]
        IAsyncResult BeginSaveOnlineExchangeRates(OnlineExchangeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndSaveOnlineExchangeRates(IAsyncResult asyncResult);

        #endregion

        #region Contact
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ContactRecord))]
        IAsyncResult BeginGetContacts(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetContacts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientContract))]
        IAsyncResult BeginClientFromContact(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndClientFromContact(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(OwnerContract))]
        IAsyncResult BeginOwnerFromContact(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndOwnerFromContact(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ContactContract))]
        IAsyncResult BeginLoadContact(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadContact(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ContactContract))]
        IAsyncResult BeginNewContact(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewContact(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ContactContract))]
        IAsyncResult BeginAddEntityContactbyBank(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddEntityContactbyBank(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityContactContract))]
        IAsyncResult BeginGetEntityContactbyBank(QueryRequest req, Guid ownerId, string ownerType, AsyncCallback callback, object asyncState);
        QueryResponse EndGetEntityContactbyBank(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveEntityContactbyBank(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveEntityContactbyBank(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateContact(ContactContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateContact(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteContact(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteContact(IAsyncResult asyncResult);

        #region Title
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TitleRecord))]
        IAsyncResult BeginGetTitles(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetTitles(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TitleRecord))]
        IAsyncResult BeginGetTitle(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetTitle(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TitleContract))]
        IAsyncResult BeginLoadTitle(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadTitle(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TitleContract))]
        IAsyncResult BeginNewTitle(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewTitle(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateTitle(TitleContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateTitle(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveTitle(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveTitle(IAsyncResult asyncResult);
        #endregion

        #endregion

        #region External Account
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ExtAccountContract))]
        IAsyncResult BeginNewExtAccountInstallation(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewExtAccountInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ExtAccountContract))]
        IAsyncResult BeginNewExtAccountInstallationbyExt(Guid extAccountId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewExtAccountInstallationbyExt(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ExtAccountContract))]
        IAsyncResult BeginNewExtAccountInstallationbyEnt(Guid entityId, string type, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewExtAccountInstallationbyEnt(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ExtAccountContract))]
        IAsyncResult BeginLoadExtAccountInstallation(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadExtAccountInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateExtAccountInstallation(ExtAccountContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateExtAccountInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ExtAccountInstallationRecord))]
        IAsyncResult BeginGetExtAccountInstallation(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetExtAccountInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ExtAccountRecord))]
        IAsyncResult BeginGetExtAccount(QueryRequest req, bool ent, AsyncCallback callback, object asyncState);
        QueryResponse EndGetExtAccount(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteExtAccountInstallation(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteExtAccountInstallation(IAsyncResult asyncResult);
        #endregion

        #region Cancellation control
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CancellationControlContract))]
        IAsyncResult BeginNewCancellationControl(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewCancellationControl(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CancellationControlContract))]
        IAsyncResult BeginLoadCancellationControl(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadCancellationControl(IAsyncResult asyncResult);
        #endregion

        #region Cancellation reason
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CancellationReasonRecord))]
        IAsyncResult BeginGetCancellationReasons(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCancellationReasons(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CancellationReasonContract))]
        IAsyncResult BeginLoadCancellationReason(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadCancellationReason(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CancellationReasonContract))]
        IAsyncResult BeginNewCancellationReason(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewCancellationReason(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateCancellationReason(CancellationReasonContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateCancellationReason(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteCancellationReason(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteCancellationReason(IAsyncResult asyncResult);
        #endregion

        #region Cancellation Policy
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CancellationPolicyRecord))]
        IAsyncResult BeginGetCancellationPolicies(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCancellationPolicies(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CancellationPolicyContract))]
        IAsyncResult BeginLoadCancellationPolicy(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadCancellationPolicy(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CancellationPolicyContract))]
        IAsyncResult BeginNewCancellationPolicy(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewCancellationPolicy(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateCancellationPolicy(CancellationPolicyContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateCancellationPolicy(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteCancellationPolicy(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteCancellationPolicy(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetCancellationFee(Guid id, AsyncCallback callback, object asyncState);
        ValidationDecimalResult EndGetCancellationFee(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginInsertCancellationFee(Guid id, decimal? value, AsyncCallback callback, object asyncState);
        ValidationObjectResult EndInsertCancellationFee(IAsyncResult asyncResult);

        #endregion

        #region Credit card type
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CreditCardTypeRecord))]
        IAsyncResult BeginGetCreditCardTypes(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCreditCardTypes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CreditCardTypeContract))]
        IAsyncResult BeginLoadCreditCardType(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadCreditCardType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CreditCardTypeContract))]
        IAsyncResult BeginNewCreditCardType(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewCreditCardType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateCreditCardType(CreditCardTypeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateCreditCardType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteCreditCardType(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteCreditCardType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginSyncronizeCommissionPercent(CreditCardTypeContract contract, DateTime? dateFrom, DateTime? dateTo, AsyncCallback callback, object asyncState);
        ValidationResult EndSyncronizeCommissionPercent(IAsyncResult asyncResult);

        #endregion

        #region Salesman
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SalesmanRecord))]
        IAsyncResult BeginGetSalesmanInstallation(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetSalesmanInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SalemanContract))]
        IAsyncResult BeginLoadSalesmanInstallation(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadSalesmanInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SalemanContract))]
        IAsyncResult BeginNewSalesmanInstallation(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewSalesmanInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateSalesmanInstallation(SalemanContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateSalesmanInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveSalesmanInstallation(Guid[] ownerIds, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveSalesmanInstallation(IAsyncResult asyncResult);


        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SalesmanEntitiesContract))]
        IAsyncResult BeginGetSalesmanInstallationEntities(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetSalesmanInstallationEntities(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityContract))]
        IAsyncResult BeginNewEntitySalesmanInstallation(Guid ownerId, Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndNewEntitySalesmanInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveEntitySalesmanInstallation(Guid owner, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveEntitySalesmanInstallation(IAsyncResult asyncResult);


        #endregion

        #region Employees

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EmployeeRecord))]
        IAsyncResult BeginGetEmployeeInstallation(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetEmployeeInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EmployeeContract))]
        IAsyncResult BeginLoadEmployeeInstallation(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadEmployeeInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EmployeeContract))]
        IAsyncResult BeginNewEmployeeInstallation(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewEmployeeInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateEmployeeInstallation(EmployeeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateEmployeeInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveEmployeeInstallation(Guid owner, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveEmployeeInstallation(IAsyncResult asyncResult);
        #endregion

        #region Client type
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientTypeRecord))]
        IAsyncResult BeginGetClientTypes(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetClientTypes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientTypeContract))]
        IAsyncResult BeginLoadClientType(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadClientType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientTypeContract))]
        IAsyncResult BeginNewClientType(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewClientType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateClientType(ClientTypeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateClientType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteClientType(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteClientType(IAsyncResult asyncResult);
        #endregion

        #region Guest Category
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(GuestCategoryRecord))]
        IAsyncResult BeginGetGuestCategories(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetGuestCategories(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(GuestCategoryContract))]
        IAsyncResult BeginLoadGuestCategory(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadGuestCategory(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(GuestCategoryContract))]
        IAsyncResult BeginNewGuestCategory(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewGuestCategory(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateGuestCategory(GuestCategoryContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateGuestCategory(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteGuestCategory(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteGuestCategory(IAsyncResult asyncResult);
        #endregion

        #region Characteristic type
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CharacteristicTypeRecord))]
        IAsyncResult BeginGetCharacteristicTypes(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCharacteristicTypes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ResourceCharacteristicTypeContract))]
        IAsyncResult BeginLoadCharacteristicType(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadCharacteristicType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ResourceCharacteristicTypeContract))]
        IAsyncResult BeginNewCharacteristicType(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewCharacteristicType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateCharacteristicType(ResourceCharacteristicTypeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateCharacteristicType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteCharacteristicType(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteCharacteristicType(IAsyncResult asyncResult);
        #endregion

        #region Zone

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ZoneRecord))]
        IAsyncResult BeginGetZones(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetZones(IAsyncResult asyncResult);

        #endregion

        #region Common zone
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CommonZoneRecord))]
        IAsyncResult BeginGetCommonZones(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCommonZones(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CommonZoneContract))]
        IAsyncResult BeginLoadCommonZone(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadCommonZone(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CommonZoneContract))]
        IAsyncResult BeginNewCommonZone(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewCommonZone(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateCommonZone(CommonZoneContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateCommonZone(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteCommonZone(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteCommonZone(IAsyncResult asyncResult);
        #endregion

        #region Client installation
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientInstallationRecord))]
        IAsyncResult BeginGetClientInstallations(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetClientInstallations(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientContract))]
        IAsyncResult BeginLoadClientInstallation(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadClientInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientContract))]
        IAsyncResult BeginNewInstallationClientFromContact(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewInstallationClientFromContact(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientContract))]
        IAsyncResult BeginNewClientInstallation(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewClientInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateClientInstallation(ClientContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateClientInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteClientInstallation(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteClientInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginMergeClientInstallation(ClientMergeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndMergeClientInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginMergeEntity(EntityMergeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndMergeEntity(IAsyncResult asyncResult);

        #region Client relatives
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientRelativesContract))]
        IAsyncResult BeginGetClientRelatives(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetClientRelatives(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientRelativesContract))]
        IAsyncResult BeginLoadClientRelative(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadClientRelative(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientRelativesContract))]
        IAsyncResult BeginNewClientRelative(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewClientRelative(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateClientRelative(ClientRelativesContract obj, Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateClientRelative(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteClientRelatives(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteClientRelatives(IAsyncResult asyncResult);
        #endregion

        #region Client attentions
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientAttentionContract))]
        IAsyncResult BeginGetClientAttentions(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetClientAttentions(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientAttentionContract))]
        IAsyncResult BeginLoadClientAttention(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadClientAttention(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientAttentionContract))]
        IAsyncResult BeginNewClientAttention(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewClientAttention(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateClientAttention(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateClientAttention(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteClientAttention(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteClientAttention(IAsyncResult asyncResult);
        #endregion

        #region Client preferences
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientPreferenceContract))]
        IAsyncResult BeginGetClientPreferences(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetClientPreferences(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientPreferenceContract))]
        IAsyncResult BeginLoadClientPreference(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadClientPreference(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientPreferenceContract))]
        IAsyncResult BeginNewClientPreference(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewClientPreference(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateClientPreferences(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateClientPreferences(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteClientPreferences(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteClientPreferences(IAsyncResult asyncResult);
        #endregion

        #region Client market segments
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientMarketSegmentContract))]
        IAsyncResult BeginGetClientMarketSegments(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetClientMarketSegments(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientMarketSegmentContract))]
        IAsyncResult BeginLoadClientMarketSegment(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadClientMarketSegment(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientMarketSegmentContract))]
        IAsyncResult BeginNewClientMarketSegment(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewClientMarketSegment(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateClientMarketSegment(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateClientMarketSegment(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteClientMarketSegment(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteClientMarketSegment(IAsyncResult asyncResult);
        #endregion

        #region Client cards

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientCardContract))]
        IAsyncResult BeginGetClientCards(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetClientCards(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientCardContract))]
        IAsyncResult BeginLoadClientCard(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadClientCard(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientCardContract))]
        IAsyncResult BeginNewClientCard(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewClientCard(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateClientCard(ClientCardContract obj, Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateClientCard(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteClientCard(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteClientCard(IAsyncResult asyncResult);

        #endregion

        #region Client parent relations
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ClientParentRelationRecord))]
        IAsyncResult BeginGetClientParentRelations(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetClientParentRelations(IAsyncResult asyncResult);
        #endregion

        #region Base Entity statistics

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BaseEntityStatisticsRecord))]
        IAsyncResult BeginGetClientProfileStatistics(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetClientProfileStatistics(IAsyncResult asyncResult);

        #endregion

        #endregion

        #region Car brands
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BrandRecord))]
        IAsyncResult BeginGetCarBrands(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCarBrands(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BrandsContract))]
        IAsyncResult BeginLoadCarBrand(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadCarBrand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BrandsContract))]
        IAsyncResult BeginNewCarBrand(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewCarBrand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateCarBrand(BrandsContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateCarBrand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteCarBrand(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteCarBrand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddModelBrand(Guid ownerId, ModelsContract modelContract, AsyncCallback callback, object asyncState);
        ValidationResult EndAddModelBrand(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveModelBrand(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveModelBrand(IAsyncResult asyncResult);

        #endregion

        #region Car models
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ModelsContract))]
        IAsyncResult BeginGetCarModels(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCarModels(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ModelsContract))]
        IAsyncResult BeginLoadCarModel(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadCarModel(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ModelsContract))]
        IAsyncResult BeginNewCarModel(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewCarModel(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateCarModel(ModelsContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateCarModel(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteCarModel(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteCarModel(IAsyncResult asyncResult);
        #endregion

        #region Departments
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DepartmentRecord))]
        IAsyncResult BeginGetDepartments(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetDepartments(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ServiceDepartmentRecord))]
        IAsyncResult BeginGetDepartmentServices(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetDepartmentServices(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DepartmentContract))]
        IAsyncResult BeginNewDepartment(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewDepartment(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DepartmentContract))]
        IAsyncResult BeginUpdateDepartment(DepartmentContract departmentContract, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateDepartment(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ServiceContract))]
        IAsyncResult BeginDeleteDepartment(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteDepartment(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ServiceDepartmentRelationContract))]
        IAsyncResult BeginGetServiceByDepartment(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetServiceByDepartment(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteServiceByDepartment(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteServiceByDepartment(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddDepartmentServices(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddDepartmentServices(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveDepartmentServices(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveDepartmentServices(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ApplicationContract))]
        IAsyncResult BeginGetDepartmentApplications(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetDepartmentApplications(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DepartmentContract))]
        IAsyncResult BeginLoadDepartment(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadDepartment(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveDepartmentApplications(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveDepartmentApplications(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddDepartmentApplications(Guid ownerId, long[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddDepartmentApplications(IAsyncResult asyncResult);
        #endregion

        #region Products
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProductsDepartmentRecord))]
        IAsyncResult BeginGetProductsByDepartment(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProductsByDepartment(IAsyncResult asyncResult);
        #endregion

        #region Services
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ServiceRecord))]
        IAsyncResult BeginGetServices(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetServices(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ServiceContract))]
        IAsyncResult BeginNewService(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewService(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ServiceContract))]
        IAsyncResult BeginLoadService(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadService(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateService(ServiceContract serviceContract, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateService(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteService(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteService(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ServiceDepartmentRelationContract))]
        IAsyncResult BeginGetServiceDepartments(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetServiceDepartments(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddServiceDepartments(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddServiceDepartments(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveServiceDepartments(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveServiceDepartments(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginToogleServiceDepartmentInactive(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndToogleServiceDepartmentInactive(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SupplementByServiceContract))]
        IAsyncResult BeginNewServiceSupplement(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewServiceSupplement(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SupplementByServiceContract))]
        IAsyncResult BeginLoadServiceSupplement(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadServiceSupplement(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SupplementByServiceContract))]
        IAsyncResult BeginGetServiceSupplements(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetServiceSupplements(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddServiceSupplement(Guid ownerId, SupplementByServiceContract contract, AsyncCallback callback, object asyncState);
        ValidationResult EndAddServiceSupplement(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveServiceSupplements(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveServiceSupplements(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ServiceTaxContract))]
        IAsyncResult BeginGetServiceTaxes(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetServiceTaxes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ServiceTaxContract))]
        IAsyncResult BeginNewServiceTax(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewServiceTax(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ServiceTaxContract))]
        IAsyncResult BeginLoadServiceTax(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadServiceTax(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddServiceTax(Guid ownerId, ServiceTaxContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndAddServiceTax(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveServiceTaxes(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveServiceTaxes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ApplicationContract))]
        IAsyncResult BeginGetServiceApplications(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetServiceApplications(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddServiceApplications(Guid ownerId, long[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddServiceApplications(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveServiceApplications(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveServiceApplications(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginBulkTaxSubstitution(Guid[] ids, ServiceTaxContract[] taxes, AsyncCallback callback, object asyncState);
        ValidationResult EndBulkTaxSubstitution(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SupplementSchemaRecord))]
        IAsyncResult BeginGetSupplementSchemas(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetSupplementSchemas(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SupplementSchemaContract))]
        IAsyncResult BeginNewSupplementSchema(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewSupplementSchema(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SupplementSchemaContract))]
        IAsyncResult BeginLoadSupplementSchema(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadSupplementSchema(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateSupplementSchema(SupplementSchemaContract supplementSchemaContract, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateSupplementSchema(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteSupplementSchema(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteSupplementSchema(IAsyncResult asyncResult);
        #endregion

        #region Stock
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StockProductRecord))]
        IAsyncResult BeginGetStockProducts(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetStockProducts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StockOperationRecord))]
        IAsyncResult BeginGetStockOperations(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetStockOperations(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StockOperationDetailRecord))]
        IAsyncResult BeginGetStockOperationDetails(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetStockOperationDetails(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StockOperationContract))]
        IAsyncResult BeginNewStockOperation(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewStockOperation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StockOperationContract))]
        IAsyncResult BeginLoadStockOperation(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadStockOperation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateStockOperation(StockOperationContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateStockOperation(IAsyncResult asyncResult);
        #endregion

        #region Document
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DocumentRecord))]
        IAsyncResult BeginGetDocuments(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetDocuments(IAsyncResult asyncResult);
        #endregion

        #region Document serie control
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DocumentSerieControlRecord))]
        IAsyncResult BeginGetDocumentSerieControls(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetDocumentSerieControls(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DocumentSerieControlContract))]
        IAsyncResult BeginLoadDocumentSerieControl(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadDocumentSerieControl(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DocumentSerieControlContract))]
        IAsyncResult BeginNewDocumentSerieControl(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewDocumentSerieControl(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DocumentSerieContract))]
        IAsyncResult BeginNewDocumentSerie(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewDocumentSerie(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateDocumentSerieControl(DocumentSerieControlContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateDocumentSerieControl(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteDocumentSerieControl(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteDocumentSerieControl(IAsyncResult asyncResult);
        #endregion

        #region Document serie
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DocumentSerieRecord))]
        IAsyncResult BeginGetDocumentSeries(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetDocumentSeries(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DocumentSerieContract))]
        IAsyncResult BeginLoadDocumentSerie(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadDocumentSerie(IAsyncResult asyncResult);

        //[OperationContract(AsyncPattern = true)]
        //[ServiceKnownType(typeof(DocumentSerieContract))]
        //IAsyncResult BeginNewDocumentSerie(AsyncCallback callback, object asyncState);
        //ValidationContractResult EndNewDocumentSerie(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateDocumentSerie(DocumentSerieContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateDocumentSerie(IAsyncResult asyncResult);
        #endregion

        #region Document type
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DocumentTypeRecord))]
        IAsyncResult BeginGetDocumentTypes(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetDocumentTypes(IAsyncResult asyncResult);
        #endregion

        #region Document Template
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DocumentTemplateRecord))]
        IAsyncResult BeginGetDocumentTemplates(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetDocumentTemplates(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DocumentTemplateContract))]
        IAsyncResult BeginLoadDocumentTemplate(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadDocumentTemplate(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DocumentTemplateContract))]
        IAsyncResult BeginNewDocumentTemplate(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewDocumentTemplate(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateDocumentTemplate(DocumentTemplateContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateDocumentTemplate(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteDocumentTemplate(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteDocumentTemplate(IAsyncResult asyncResult);
        #endregion

        #region Discount type
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DiscountTypeRecord))]
        IAsyncResult BeginGetDiscountTypes(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetDiscountTypes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DiscountTypeContract))]
        IAsyncResult BeginLoadDiscountType(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadDiscountType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DiscountTypeContract))]
        IAsyncResult BeginNewDiscountType(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewDiscountType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateDiscountType(DiscountTypeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateDiscountType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteDiscountType(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteDiscountType(IAsyncResult asyncResult);
        #endregion

        #region Entities
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityInstallationRecord))]
        IAsyncResult BeginGetEntities(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetEntities(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginLoadEntitiesTextFile(string file, AsyncCallback callback, object asyncState);
        ValidationStringResult EndLoadEntitiesTextFile(IAsyncResult asyncResult);

        #region Entity installation
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityInstallationRecord))]
        IAsyncResult BeginGetEntityInstallations(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetEntityInstallations(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityContract))]
        IAsyncResult BeginLoadEntityInstallation(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadEntityInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityContract))]
        IAsyncResult BeginNewEntityInstallation(Guid? entityId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewEntityInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateEntityInstallation(EntityContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateEntityInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateEntity(Guid entityId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateEntity(IAsyncResult asyncResult);
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteEntityInstallation(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteEntityInstallation(IAsyncResult asyncResult);
        #endregion

        #region Entity address
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityAddressContract))]
        IAsyncResult BeginGetEntityAddresses(QueryRequest req, Guid ownerId, string ownerType, AsyncCallback callback, object asyncState);
        QueryResponse EndGetEntityAddresses(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityAddressContract))]
        IAsyncResult BeginLoadEntityAddress(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadEntityAddress(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityAddressContract))]
        IAsyncResult BeginNewEntityAddress(long contactType, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewEntityAddress(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateEntityAddress(EntityAddressContract obj, Guid ownerId, string ownerType, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateEntityAddress(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteEntityAddress(Guid ownerId, Guid[] ids, string ownerType, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteEntityAddress(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateIdentityDoc(IdentityDocContract obj, Guid ownerId, string ownerType, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateIdentityDoc(IAsyncResult asyncResult);
        #endregion

        #region Credit cards
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CreditCardContract))]
        IAsyncResult BeginGetCreditCards(QueryRequest req, Guid ownerId, string ownerType, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCreditCards(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CreditCardContract))]
        IAsyncResult BeginLoadCreditCard(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadCreditCard(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CreditCardContract))]
        IAsyncResult BeginNewCreditCard(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewCreditCard(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateCreditCard(CreditCardContract obj, Guid ownerId, string ownerType, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateCreditCard(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginPersistCreditCard(CreditCardContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndPersistCreditCard(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteCreditCard(Guid ownerId, Guid[] ids, string ownerType, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteCreditCard(IAsyncResult asyncResult);
        #endregion

        #region Bank accounts
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityBankAccountContract))]
        IAsyncResult BeginGetEntityBankAccounts(QueryRequest req, Guid ownerId, string ownerType, AsyncCallback callback, object asyncState);
        QueryResponse EndGetEntityBankAccounts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityBankAccountContract))]
        IAsyncResult BeginLoadEntityBankAccount(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadEntityBankAccount(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityBankAccountContract))]
        IAsyncResult BeginNewEntityBankAccount(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewEntityBankAccount(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateEntityBankAccount(EntityBankAccountContract obj, Guid ownerId, string ownerType, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateEntityBankAccount(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteEntityBankAccount(Guid ownerId, Guid[] ids, string ownerType, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteEntityBankAccount(IAsyncResult asyncResult);
        #endregion

        #region Entity contacts
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityContactContract))]
        IAsyncResult BeginGetEntityContacts(QueryRequest req, Guid ownerId, string ownerType, AsyncCallback callback, object asyncState);
        QueryResponse EndGetEntityContacts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityContactContract))]
        IAsyncResult BeginLoadEntityContact(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadEntityContact(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityContactContract))]
        IAsyncResult BeginNewEntityContact(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewEntityContact(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateEntityContact(Guid obj, Guid ownerId, string ownerType, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateEntityContact(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteEntityContact(Guid ownerId, Guid[] ids, string ownerType, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteEntityContact(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityContactRecord))]
        IAsyncResult BeginGetEntityContact(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetEntityContact(IAsyncResult asyncResult);
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddEntityContact(Guid benti_pk, Guid cont_pk, AsyncCallback callback, object asyncState);
        ValidationResult EndAddEntityContact(IAsyncResult asyncResult);
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDelEntityContact(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDelEntityContact(IAsyncResult asyncResult);
        #endregion

        #region Entity deposit requests
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityDepositRequestContract))]
        IAsyncResult BeginGetEntityDepositRequests(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetEntityDepositRequests(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityDepositRequestContract))]
        IAsyncResult BeginLoadEntityDepositRequest(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadEntityDepositRequest(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityDepositRequestContract))]
        IAsyncResult BeginNewEntityDepositRequest(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewEntityDepositRequest(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateEntityDepositRequest(EntityDepositRequestContract obj, Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateEntityDepositRequest(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteEntityDepositRequest(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteEntityDepositRequest(IAsyncResult asyncResult);
        #endregion

        #region Entity relations
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityRelationContract))]
        IAsyncResult BeginGetEntityRelations(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetEntityRelations(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityRelationContract))]
        IAsyncResult BeginNewEntityRelation(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewEntityRelation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateEntityRelation(EntityRelationContract obj, Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateEntityRelation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteEntityRelations(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteEntityRelations(IAsyncResult asyncResult);
        #endregion

        #region Entity type
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityTypeRecord))]
        IAsyncResult BeginGetEntityTypes(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetEntityTypes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityTypeContract))]
        IAsyncResult BeginLoadEntityType(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadEntityType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntityTypeContract))]
        IAsyncResult BeginNewEntityType(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewEntityType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateEntityType(EntityTypeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateEntityType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteEntityType(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteEntityType(IAsyncResult asyncResult);
        #endregion

        #endregion

        #region Current account
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CurrentAccountContract))]
        IAsyncResult BeginNewCurrentAccountForEntity(Guid id, CurrentAccountType accountType, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewCurrentAccountForEntity(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CurrentAccountContract))]
        IAsyncResult BeginNewCurrentAccount(CurrentAccountType accountType, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewCurrentAccount(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CurrentAccountContract))]
        IAsyncResult BeginLoadCurrentAccount(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadCurrentAccount(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateCurrentAccount(CurrentAccountContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateCurrentAccount(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginOpenCurrentAccount(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndOpenCurrentAccount(IAsyncResult asyncResult);
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginCloseCurrentAccount(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndCloseCurrentAccount(IAsyncResult asyncResult);
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginLockCurrentAccount(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndLockCurrentAccount(IAsyncResult asyncResult);
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUnlockCurrentAccount(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndUnlockCurrentAccount(IAsyncResult asyncResult);
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginLockUnlockCurrentAccount(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndLockUnlockCurrentAccount(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CurrentAccountProperties))]
        IAsyncResult BeginLoadCurrentAccountProperties(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadCurrentAccountProperties(IAsyncResult asyncResult);

        #region Transfer instruction
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CurrentAccountTransferInstructionContract))]
        IAsyncResult BeginGetCurrentAccountTransferInstructions(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCurrentAccountTransferInstructions(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CurrentAccountTransferInstructionContract))]
        IAsyncResult BeginLoadCurrentAccountTransferInstruction(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadCurrentAccountTransferInstruction(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CurrentAccountTransferInstructionContract))]
        IAsyncResult BeginNewCurrentAccountTransferInstruction(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewCurrentAccountTransferInstruction(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateCurrentAccountTransferInstruction(CurrentAccountTransferInstructionContract obj, Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateCurrentAccountTransferInstruction(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteCurrentAccountTransferInstruction(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteCurrentAccountTransferInstruction(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CurrentAccountTransferInstructionContract))]
        IAsyncResult BeginGetCurrentAccountTransferInstructionsToThisAccount(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCurrentAccountTransferInstructionsToThisAccount(IAsyncResult asyncResult);
        #endregion

        #endregion

        #region Entry country
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EntryCountryRecord))]
        IAsyncResult BeginGetEntryCountries(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetEntryCountries(IAsyncResult asyncResult);
        #endregion

        #region Extension
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ExtensionRecord))]
        IAsyncResult BeginGetExtensions(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetExtensions(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ExtensionContract))]
        IAsyncResult BeginLoadExtension(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadExtension(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ExtensionContract))]
        IAsyncResult BeginNewExtension(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewExtension(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateExtension(ExtensionContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateExtension(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteExtension(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteExtension(IAsyncResult asyncResult);
        #endregion

        #region Calls
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(InternalCallRecord))]
        IAsyncResult BeginGetInternalCalls(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetInternalCalls(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(UnpostedCallRecord))]
        IAsyncResult BeginGetUnpostedCalls(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetUnpostedCalls(IAsyncResult asyncResult);
        #endregion

        #region KeyEmission
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(KeyEmissionRecord))]
        IAsyncResult BeginGetKeyEmissions(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetKeyEmissions(IAsyncResult asyncResult);
        #endregion

        #region PayTV
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PayTVRecord))]
        IAsyncResult BeginGetPayTV(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPayTV(IAsyncResult asyncResult);
        #endregion

        #region Scans
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ScanRecord))]
        IAsyncResult BeginGetScans(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetScans(IAsyncResult asyncResult);
        #endregion

        #region MessageNewGes
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MessageNewGesRecord))]
        IAsyncResult BeginGetMessageNewGes(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetMessageNewGes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MessageNewGesContract))]
        IAsyncResult BeginLoadMessageNewGes(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadMessageNewGes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MessageNewGesContract))]
        IAsyncResult BeginNewMessageNewGes(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewMessageNewGes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateMessageNewGes(MessageNewGesContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateMessageNewGes(IAsyncResult asyncResult);

        #endregion

        #region Employee role
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EmployeeRoleRecord))]
        IAsyncResult BeginGetEmployeeRoles(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetEmployeeRoles(IAsyncResult asyncResult);


        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EmployeeRoleContract))]
        IAsyncResult BeginLoadEmployeeRoles(long id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadEmployeeRoles(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EmployeeRoleContract))]
        IAsyncResult BeginNewEmployeeRoles(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewEmployeeRoles(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateEmployeeRoles(EmployeeRoleContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateEmployeeRoles(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveEmployeeRoles(long ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveEmployeeRoles(IAsyncResult asyncResult);
        #endregion

        #region Flight
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(FlightRecord))]
        IAsyncResult BeginGetFlights(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetFlights(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(FlightContract))]
        IAsyncResult BeginLoadFlight(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadFlight(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(FlightContract))]
        IAsyncResult BeginNewFlight(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewFlight(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateFlight(FlightContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateFlight(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteFlight(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteFlight(IAsyncResult asyncResult);
        #endregion

        #region FNRH
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(FNRHGuestRecord))]
        IAsyncResult BeginGetFNRHGuests(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetFNRHGuests(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateFNRHInfo(AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateFNRHInfo(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginSyncronizeFNRHInfo(AsyncCallback callback, object asyncState);
        ValidationWorkerResult EndSyncronizeFNRHInfo(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginSyncronizeSelectedFNRHInfo(Guid[] guests, AsyncCallback callback, object asyncState);
        ValidationResult EndSyncronizeSelectedFNRHInfo(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginCancelSyncronizeFNRHInfo(AsyncCallback callback, object asyncState);
        ValidationWorkerResult EndCancelSyncronizeFNRHInfo(IAsyncResult asyncResult);
        #endregion

        #region Failure origin
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(FailureOriginRecord))]
        IAsyncResult BeginGetFailureOrigins(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetFailureOrigins(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(FailureOriginContract))]
        IAsyncResult BeginLoadFailureOrigin(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadFailureOrigin(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(FailureOriginContract))]
        IAsyncResult BeginNewFailureOrigin(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewFailureOrigin(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateFailureOrigin(FailureOriginContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateFailureOrigin(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteFailureOrigin(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteFailureOrigin(IAsyncResult asyncResult);
        #endregion

        #region Failure type
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(FailureTypeRecord))]
        IAsyncResult BeginGetFailureTypes(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetFailureTypes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(FailureTypeContract))]
        IAsyncResult BeginLoadFailureType(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadFailureType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(FailureTypeContract))]
        IAsyncResult BeginNewFailureType(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewFailureType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateFailureType(FailureTypeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateFailureType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteFailureType(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteFailureType(IAsyncResult asyncResult);
        #endregion

        #region Failure subtype
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(FailureSubTypesContract))]
        IAsyncResult BeginGetFailureSubTypes(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetFailureSubTypes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(FailureSubTypeRecord))]
        IAsyncResult BeginGetFailureSubTypesAll(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetFailureSubTypesAll(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(FailureSubTypesContract))]
        IAsyncResult BeginNewFailureSubType(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewFailureSubType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(FailureSubTypesContract))]
        IAsyncResult BeginLoadFailureSubType(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadFailureSubType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddFailureSubType(Guid ownerId, FailureSubTypesContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndAddFailureSubType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveFailureSubType(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveFailureSubType(IAsyncResult asyncResult);
        #endregion

        #region Group type
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(GroupTypeRecord))]
        IAsyncResult BeginGetGroupTypes(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetGroupTypes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(GroupTypeContract))]
        IAsyncResult BeginLoadGroupType(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadGroupType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(GroupTypeContract))]
        IAsyncResult BeginNewGroupType(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewGroupType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateGroupType(GroupTypeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateGroupType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteGroupType(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteGroupType(IAsyncResult asyncResult);
        #endregion

        #region Inactivity type
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(InactivityTypeRecord))]
        IAsyncResult BeginGetInactivityTypes(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetInactivityTypes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(InactivityTypeContract))]
        IAsyncResult BeginLoadInactivityType(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadInactivityType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(InactivityTypeContract))]
        IAsyncResult BeginNewInactivityType(bool outOfRental, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewInactivityType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateInactivityType(InactivityTypeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateInactivityType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteInactivityType(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteInactivityType(IAsyncResult asyncResult);
        #endregion

        #region Installation
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(FacilityRecord))]
        IAsyncResult BeginGetInstallations(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetInstallations(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(InstallationContract))]
        IAsyncResult BeginLoadInstallation(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(InstallationContract))]
        IAsyncResult BeginLoadInstallationWithSettings(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadInstallationWithSettings(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(InstallationContract))]
        IAsyncResult BeginNewInstallation(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(InstallationContract))]
        IAsyncResult BeginNewInstallationAlone(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewInstallationAlone(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateInstallation(InstallationContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateInstallation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateInstallationAlone(InstallationContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateInstallationAlone(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteInstallation(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteInstallation(IAsyncResult asyncResult);
        #endregion

        #region Job function

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(JobFunctionRecord))]
        IAsyncResult BeginGetJobFunctions(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetJobFunctions(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(JobTitleContract))]
        IAsyncResult BeginLoadJobFunction(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadJobFunction(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(JobTitleContract))]
        IAsyncResult BeginNewJobFunction(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewJobFunction(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateJobFunction(JobTitleContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateJobFunction(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteJobFunction(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteJobFunction(IAsyncResult asyncResult);

        #endregion

        #region Market origin
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MarketOriginRecord))]
        IAsyncResult BeginGetMarketOrigins(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetMarketOrigins(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(OriginContract))]
        IAsyncResult BeginLoadMarketOrigin(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadMarketOrigin(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(OriginContract))]
        IAsyncResult BeginNewMarketOrigin(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewMarketOrigin(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateMarketOrigin(OriginContract mo, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateMarketOrigin(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteMarketOrigin(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteMarketOrigin(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddOriginSegments(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddOriginSegments(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveOriginSegments(Guid id, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveOriginSegments(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SegmentOriginRelationContract))]
        IAsyncResult BeginGetOriginSegments(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetOriginSegments(IAsyncResult asyncResult);

        #endregion

        #region Market group
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MarketOriginGroupRecord))]
        IAsyncResult BeginGetMarketOriginGroups(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetMarketOriginGroups(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(OriginGroupContract))]
        IAsyncResult BeginLoadMarketOriginGroup(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadMarketOriginGroup(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(OriginGroupContract))]
        IAsyncResult BeginNewMarketOriginGroup(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewMarketOriginGroup(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateMarketOriginGroup(OriginGroupContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateMarketOriginGroup(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteMarketOriginGroup(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteMarketOriginGroup(IAsyncResult asyncResult);
        #endregion

        #region Market segment
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MarketSegmentRecord))]
        IAsyncResult BeginGetMarketSegments(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetMarketSegments(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SegmentContract))]
        IAsyncResult BeginLoadMarketSegment(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadMarketSegment(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SegmentContract))]
        IAsyncResult BeginNewMarketSegment(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewMarketSegment(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateMarketSegment(SegmentContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateMarketSegment(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteMarketSegment(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteMarketSegment(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddSegmentOrigins(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddSegmentOrigins(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveSegmentOrigins(Guid id, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveSegmentOrigins(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SegmentOriginRelationContract))]
        IAsyncResult BeginGetSegmentOrigins(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetSegmentOrigins(IAsyncResult asyncResult);
        #endregion

        #region Market segment group
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MarketSegmentGroupRecord))]
        IAsyncResult BeginGetMarketSegmentGroups(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetMarketSegmentGroups(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SegmentGroupContract))]
        IAsyncResult BeginLoadMarketSegmentGroup(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadMarketSegmentGroup(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SegmentGroupContract))]
        IAsyncResult BeginNewMarketSegmentGroup(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewMarketSegmentGroup(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateMarketSegmentGroup(SegmentGroupContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateMarketSegmentGroup(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteMarketSegmentGroup(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteMarketSegmentGroup(IAsyncResult asyncResult);
        #endregion

        #region Minibar Service
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MinibarServiceRecord))]
        IAsyncResult BeginGetMinibarServices(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetMinibarServices(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteMinibarService(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteMinibarService(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MinibarServiceContract))]
        IAsyncResult BeginNewMinibarService(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewMinibarService(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MinibarServiceContract))]
        IAsyncResult BeginLoadMinibarService(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadMinibarService(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateMinibarService(MinibarServiceContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateMinibarService(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MinibarServiceEntryContract))]
        [ServiceKnownType(typeof(MinibarServiceEntriesContract))]
        IAsyncResult BeginNewMinibarServiceEntry(Guid id, CurrentAccountType type, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewMinibarServiceEntry(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginPersistMinibarServiceEntry(MinibarServiceEntriesContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndPersistMinibarServiceEntry(IAsyncResult asyncResult);
        #endregion

        #region Tips
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TipServiceEntryContract))]
        [ServiceKnownType(typeof(TipServiceEntriesContract))]
        IAsyncResult BeginNewTipServiceEntry(Guid id, CurrentAccountType type, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewTipServiceEntry(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginPersistTipServiceEntry(TipServiceEntriesContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndPersistTipServiceEntry(IAsyncResult asyncResult);
        #endregion

        #region Preference
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PreferenceRecord))]
        IAsyncResult BeginGetPreferences(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPreferences(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PreferenceContract))]
        IAsyncResult BeginLoadPreference(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadPreference(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PreferenceContract))]
        IAsyncResult BeginNewPreference(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewPreference(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdatePreference(PreferenceContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdatePreference(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeletePreference(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeletePreference(IAsyncResult asyncResult);
        #endregion

        #region Profession
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProfRecord))]
        IAsyncResult BeginGetProfessions(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetProfessions(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProfContract))]
        IAsyncResult BeginLoadProfession(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadProfession(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ProfContract))]
        IAsyncResult BeginNewProfession(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewProfession(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateProfession(ProfContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateProfession(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteProfession(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteProfession(IAsyncResult asyncResult);
        #endregion

        #region Parking location
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ParkingLocationRecord))]
        IAsyncResult BeginGetParkingLocations(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetParkingLocations(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ParkingContract))]
        IAsyncResult BeginLoadParkingLocation(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadParkingLocation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ParkingContract))]
        IAsyncResult BeginNewParkingLocation(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewParkingLocation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateParkingLocation(ParkingContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateParkingLocation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteParkingLocation(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteParkingLocation(IAsyncResult asyncResult);
        #endregion

        #region Parking zone
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ParkingZoneRecord))]
        IAsyncResult BeginGetParkingZones(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetParkingZones(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ParkingZoneContract))]
        IAsyncResult BeginLoadParkingZone(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadParkingZone(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ParkingZoneContract))]
        IAsyncResult BeginNewParkingZone(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewParkingZone(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateParkingZone(ParkingZoneContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateParkingZone(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteParkingZone(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteParkingZone(IAsyncResult asyncResult);
        #endregion

        #region Payment method
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PaymentMethodRecord))]
        IAsyncResult BeginGetPaymentMethods(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPaymentMethods(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PaymentFormContract))]
        IAsyncResult BeginLoadPaymentMethod(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadPaymentMethod(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PaymentFormContract))]
        IAsyncResult BeginNewPaymentMethod(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewPaymentMethod(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdatePaymentMethod(PaymentFormContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdatePaymentMethod(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeletePaymentMethod(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeletePaymentMethod(IAsyncResult asyncResult);
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ApplicationContract))]
        IAsyncResult BeginGetPaymentFormApplications(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPaymentFormApplications(IAsyncResult asyncResult);
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemovePaymentFormApplications(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemovePaymentFormApplications(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddPaymentFormApplications(Guid ownerId, long[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddPaymentFormApplications(IAsyncResult asyncResult);
        #endregion

        #region Payment Operations
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PaymentOperationRecord))]
        IAsyncResult BeginGetPaymentOperations(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPaymentOperations(IAsyncResult asyncResult);
        #endregion

        #region Relatives
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RelativeRecord))]
        IAsyncResult BeginGetRelatives(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetRelatives(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RelativeContract))]
        IAsyncResult BeginLoadRelative(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadRelative(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RelativeContract))]
        IAsyncResult BeginNewRelative(RelationType relationType, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewRelative(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateRelative(RelativeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateRelative(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteRelative(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteRelative(IAsyncResult asyncResult);
        #endregion

        #region Room

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoomRecord))]
        IAsyncResult BeginGetAllRooms(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAllRooms(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoomRecord))]
        IAsyncResult BeginGetRooms(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetRooms(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoomInacRecord))]
        [ServiceKnownType(typeof(RoomRecord))]
        IAsyncResult BeginGetRoomsInactivities(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetRoomsInactivities(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoomInacRecord))]
        [ServiceKnownType(typeof(RoomRecord))]
        IAsyncResult BeginGetRoomsOutOfRentals(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetRoomsOutOfRentals(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoomContract))]
        IAsyncResult BeginLoadRoom(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadRoom(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoomContract))]
        IAsyncResult BeginNewRoom(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewRoom(IAsyncResult asyncResult);


        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateRoom(RoomContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateRoom(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteRoom(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteRoom(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoomProximityContract))]
        IAsyncResult BeginGetRoomProximities(QueryRequest req, Guid id, AsyncCallback callback, object asyncState);
        QueryResponse EndGetRoomProximities(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddRoomProximities(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddRoomProximities(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveRoomProximities(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveRoomProximities(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginChangeRoomProximityConnection(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndChangeRoomProximityConnection(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ResourceCharacteristicContract))]
        IAsyncResult BeginGetRoomCharacteristics(QueryRequest req, Guid id, AsyncCallback callback, object asyncState);
        QueryResponse EndGetRoomCharacteristics(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddRoomCharacteristics(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddRoomCharacteristics(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveRoomCharacteristics(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveRoomCharacteristics(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoomOrderContract))]
        [ServiceKnownType(typeof(ERoomOrder))]
        IAsyncResult BeginNewRoomOrder(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewRoomOrder(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ERoomOrder))]
        IAsyncResult BeginUpdateRoomOrder(RoomOrderContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateRoomOrder(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoomRecord))]
        IAsyncResult BeginUpdateRoomOrderManual(RoomOrdenManualContract contract, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateRoomOrderManual(IAsyncResult asyncResult);
        #endregion

        #region Room lock
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoomLockRecord))]
        IAsyncResult BeginGetRoomLocks(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetRoomLocks(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoomLockPeriodRecord))]
        IAsyncResult BeginGetRoomLockPeriods(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetRoomLockPeriods(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoomLockContract))]
        IAsyncResult BeginLoadRoomLock(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadRoomLock(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoomLockContract))]
        IAsyncResult BeginNewRoomLock(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewRoomLock(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateRoomLock(RoomLockContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateRoomLock(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteRoomLock(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteRoomLock(IAsyncResult asyncResult);
        #endregion

        #region Room type
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoomTypeRecord))]
        IAsyncResult BeginGetRoomTypes(QueryRequest req, AsyncCallback callback, object asyncState);
        RoomTypesResponse EndGetRoomTypes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoomTypeContract))]
        IAsyncResult BeginLoadRoomType(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadRoomType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoomTypeContract))]
        IAsyncResult BeginNewRoomType(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewRoomType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateRoomType(RoomTypeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateRoomType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteRoomType(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteRoomType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRoomTypeMoveUp(RoomTypeRecord record, AsyncCallback callback, object asyncState);
        ValidationResult EndRoomTypeMoveUp(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRoomTypeMoveDown(RoomTypeRecord record, AsyncCallback callback, object asyncState);
        ValidationResult EndRoomTypeMoveDown(IAsyncResult asyncResult);
        #endregion

        #region Room block
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoomBlockRecord))]
        IAsyncResult BeginGetRoomBlocks(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetRoomBlocks(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BlockContract))]
        IAsyncResult BeginLoadRoomBlock(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadRoomBlock(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BlockContract))]
        IAsyncResult BeginNewRoomBlock(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewRoomBlock(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateRoomBlock(BlockContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateRoomBlock(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteRoomBlock(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteRoomBlock(IAsyncResult asyncResult);
        #endregion

        #region Series
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SerieRecord))]
        IAsyncResult BeginGetSeries(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetSeries(IAsyncResult asyncResult);
        #endregion

        #region Service group
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ServiceGroupRecord))]
        IAsyncResult BeginGetServiceGroups(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetServiceGroups(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ServiceGroupingContract))]
        IAsyncResult BeginLoadServiceGroup(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadServiceGroup(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ServiceGroupingContract))]
        IAsyncResult BeginNewServiceGroup(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewServiceGroup(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateServiceGroup(ServiceGroupingContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateServiceGroup(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteServiceGroup(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteServiceGroup(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginAddServiceGroupingApplications(Guid ownerId, long[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndAddServiceGroupingApplications(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginRemoveServiceGroupingApplications(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndRemoveServiceGroupingApplications(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ApplicationContract))]
        IAsyncResult BeginGetServiceGroupingApplications(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetServiceGroupingApplications(IAsyncResult asyncResult);
        #endregion

        #region Service category
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ServiceCategoryRecord))]
        IAsyncResult BeginGetServiceCategories(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetServiceCategories(IAsyncResult asyncResult);
        #endregion

        #region Service tip
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ServiceTipRecord))]
        IAsyncResult BeginGetServiceTips(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetServiceTips(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TipsContract))]
        IAsyncResult BeginLoadServiceTip(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadServiceTip(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TipsContract))]
        IAsyncResult BeginNewServiceTip(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewServiceTip(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateServiceTip(TipsContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateServiceTip(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteServiceTip(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteServiceTip(IAsyncResult asyncResult);
        #endregion

        #region Tax schema
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TaxSchemaRecord))]
        IAsyncResult BeginGetTaxSchemas(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetTaxSchemas(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TaxSchemaContract))]
        IAsyncResult BeginLoadTaxSchema(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadTaxSchema(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TaxSchemaContract))]
        IAsyncResult BeginNewTaxSchema(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewTaxSchema(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateTaxSchema(TaxSchemaContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateTaxSchema(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteTaxSchema(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteTaxSchema(IAsyncResult asyncResult);
        #endregion

        #region Tax region
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TaxRegionRecord))]
        IAsyncResult BeginGetTaxRegions(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetTaxRegions(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TaxesRegionContract))]
        IAsyncResult BeginLoadTaxRegion(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadTaxRegion(IAsyncResult asyncResult);
        #endregion

        #region Tax sequence
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TaxSequenceRecord))]
        IAsyncResult BeginGetTaxSequences(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetTaxSequences(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TaxesSequenceContract))]
        IAsyncResult BeginLoadTaxSequence(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadTaxSequence(IAsyncResult asyncResult);
        #endregion

        #region Tax rate
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TaxRateRecord))]
        IAsyncResult BeginGetTaxRates(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetTaxRates(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TaxesRateContract))]
        IAsyncResult BeginLoadTaxRate(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadTaxRate(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginToogleInactiveTaxRate(Guid taxRateId, bool inactive, AsyncCallback callback, object asyncState);
        ValidationResult EndToogleInactiveTaxRate(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateTaxRate(Guid taxRateId, bool inactive, string freeCode, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateTaxRate(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginReplaceTaxRate(TaxesRateReplacementContract contract, AsyncCallback callback, object asyncState);
        ValidationResult EndReplaceTaxRate(IAsyncResult asyncResult);
        #endregion

        #region Tax by Hotel
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TaxHotelRecord))]
        IAsyncResult BeginGetTaxHotel(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetTaxHotel(IAsyncResult asyncResult);
        #endregion

        #region Unexpected departure
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(UnexpectedDepartureRecord))]
        IAsyncResult BeginGetUnexpectedDepartures(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetUnexpectedDepartures(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(UnexpectedDepartureTypeContract))]
        IAsyncResult BeginLoadUnexpectedDeparture(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadUnexpectedDeparture(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(UnexpectedDepartureTypeContract))]
        IAsyncResult BeginNewUnexpectedDeparture(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewUnexpectedDeparture(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateUnexpectedDeparture(UnexpectedDepartureTypeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateUnexpectedDeparture(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteUnexpectedDeparture(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteUnexpectedDeparture(IAsyncResult asyncResult);
        #endregion

        #region Price categories
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PriceCategoryRecord))]
        IAsyncResult BeginGetPriceCategories(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPriceCategories(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PriceCategoryContract))]
        IAsyncResult BeginNewPriceCategory(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewPriceCategory(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PriceCategoryContract))]
        IAsyncResult BeginLoadPriceCategory(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadPriceCategory(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdatePriceCategory(PriceCategoryContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdatePriceCategory(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeletePriceCategory(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeletePriceCategory(IAsyncResult asyncResult);
        #endregion

        #region Warranty type
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(WarrantyTypeRecord))]
        IAsyncResult BeginGetWarrantyTypes(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetWarrantyTypes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(WarrantyTypeContract))]
        IAsyncResult BeginLoadWarrantyType(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadWarrantyType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(WarrantyTypeContract))]
        IAsyncResult BeginNewWarrantyType(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewWarrantyType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateWarrantyType(WarrantyTypeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateWarrantyType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteWarrantyType(Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteWarrantyType(IAsyncResult asyncResult);
        #endregion

        #region Zone type
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ZoneTypeRecord))]
        IAsyncResult BeginGetZoneTypes(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetZoneTypes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ZoneTypeContract))]
        IAsyncResult BeginLoadZoneType(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadZoneType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ZoneTypeContract))]
        IAsyncResult BeginNewZoneType(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewZoneType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateZoneType(ZoneTypeContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateZoneType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteZoneType(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteZoneType(IAsyncResult asyncResult);
        #endregion

        #region Regional
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(LanguageRecord))]
        IAsyncResult BeginGetLanguages(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetLanguages(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(LanguageTranslationRecord))]
        IAsyncResult BeginGetLanguageTranslations(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetLanguageTranslations(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(WorldRegionRecord))]
        IAsyncResult BeginGetWorldRegions(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetWorldRegions(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DistrictRecord))]
        IAsyncResult BeginGetDistricts(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetDistricts(IAsyncResult asyncResult);
        #endregion

        #region Country
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CountryRecord))]
        IAsyncResult BeginGetCountries(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCountries(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CountryContract))]
        IAsyncResult BeginLoadCountry(string id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadCountry(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateCountry(CountryContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateCountry(IAsyncResult asyncResult);
        #endregion

        #region Holidays
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(HolidaysContract))]
        IAsyncResult BeginGetHolidays(QueryRequest req, string ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetHolidays(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(HolidaysContract))]
        IAsyncResult BeginLoadHolidays(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadHolidays(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(HolidaysContract))]
        IAsyncResult BeginNewHolidays(string ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewHolidays(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateHolidays(HolidaysContract obj, string ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateHolidays(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteHolidays(string ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteHolidays(IAsyncResult asyncResult);
        #endregion

        #region Border points
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BorderPointsContract))]
        IAsyncResult BeginGetBorderPoints(QueryRequest req, string ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetBorderPoints(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BorderPointsContract))]
        IAsyncResult BeginLoadBorderPoint(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadBorderPoint(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BorderPointsContract))]
        IAsyncResult BeginNewBorderPoint(string ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewBorderPoint(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateBorderPoint(BorderPointsContract obj, string ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateBorderPoint(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteBorderPoint(string ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteBorderPoint(IAsyncResult asyncResult);
        #endregion

        #region Country zones
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CountryZonesContract))]
        IAsyncResult BeginGetCountryZones(QueryRequest req, string ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCountryZones(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CountryZonesContract))]
        IAsyncResult BeginGetCountryZonesList(QueryRequest req, string ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetCountryZonesList(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CountryZonesContract))]
        IAsyncResult BeginLoadCountryZone(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadCountryZone(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(CountryZonesContract))]
        IAsyncResult BeginNewCountryZone(string ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewCountryZone(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateCountryZone(CountryZonesContract obj, string ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateCountryZone(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteCountryZone(string ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteCountryZone(IAsyncResult asyncResult);
        #endregion

        #region Regions
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RegionsContract))]
        IAsyncResult BeginGetRegions(QueryRequest req, string ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetRegions(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RegionRecord))]
        IAsyncResult BeginGetRegionsAll(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetRegionsAll(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RegionsContract))]
        IAsyncResult BeginLoadRegion(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadRegion(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RegionsContract))]
        IAsyncResult BeginNewRegion(string ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewRegion(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateRegion(RegionsContract obj, string ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateRegion(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteRegion(string ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteRegion(IAsyncResult asyncResult);
        #endregion

        #region Postal codes
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PostalCodeContract))]
        IAsyncResult BeginGetPostalCodes(QueryRequest req, Guid ownerId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPostalCodes(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PostalCodeContract))]
        IAsyncResult BeginLoadPostalCode(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadPostalCode(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PostalCodeContract))]
        IAsyncResult BeginNewPostalCode(Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewPostalCode(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdatePostalCode(PostalCodeContract obj, Guid ownerId, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdatePostalCode(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeletePostalCode(Guid ownerId, Guid[] ids, AsyncCallback callback, object asyncState);
        ValidationResult EndDeletePostalCode(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TypeAddressContract))]
        IAsyncResult BeginGetAddressByPostalCode(string postalCode, AsyncCallback callback, object asyncState);
        ValidationContractResult EndGetAddressByPostalCode(IAsyncResult asyncResult);
        #endregion

        #region Reservation
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ReservationSearchFromExternalRecord))]
        IAsyncResult BeginGetReservations(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetReservations(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EventReservationSearchFromExternalRecord))]
        IAsyncResult BeginGetEventReservations(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetEventReservations(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(SpaReservationSearchFromExternalRecord))]
        IAsyncResult BeginGetSpaReservations(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetSpaReservations(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ReservationsGroupsRecord))]
        IAsyncResult BeginGetReservationsGroups(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetReservationsGroups(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(ControlAccountRecord))]
        IAsyncResult BeginGetControlAccounts(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetControlAccounts(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(GuestsRecord))]
        IAsyncResult BeginGetGuests(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetGuests(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(GuestsRecord))]
        IAsyncResult BeginGetGuestsSearch(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetGuestsSearch(IAsyncResult asyncResult);
        #endregion

        #region Messages
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MessageSearchRecord))]
        IAsyncResult BeginGetMessages(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetMessages(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MessageContract))]
        IAsyncResult BeginNewMessage(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewMessage(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateMessage(MessageContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateMessage(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MessageSearchDetailsRecord))]
        IAsyncResult BeginGetMessagesDetails(QueryRequest req, Guid? msgId, Guid? userId, AsyncCallback callback, object asyncState);
        QueryResponse EndGetMessagesDetails(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MessageContract))]
        IAsyncResult BeginLoadMessage(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadMessage(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginReplyMessage(Guid id, string reply, AsyncCallback callback, object asyncState);
        ValidationResult EndReplyMessage(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MessageRecord))]
        IAsyncResult BeginGetUserMessages(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetUserMessages(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteUserMessages(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteUserMessages(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MessagesContract))]
        IAsyncResult BeginLoadUserMessage(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadUserMessage(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(MessagesContract))]
        IAsyncResult BeginNewUserMessage(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewUserMessage(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateUserMessage(MessagesContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateUserMessage(IAsyncResult asyncResult);
        #endregion

        #region Alerts
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(AlertRecord))]
        IAsyncResult BeginGetAlerts(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetAlerts(IAsyncResult asyncResult);
        #endregion

        #region Time zones
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TimeZoneRecord))]
        IAsyncResult BeginGetTimeZones(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetTimeZones(IAsyncResult asyncResult);
        #endregion

        #region External Booking
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginNotificationAllClassifiers(AsyncCallback callback, object asyncState);
        ValidationResult EndNotificationAllClassifiers(IAsyncResult asyncResult);
        #endregion

        #region License

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(LicenceTypeContract))]
        IAsyncResult BeginGetLicenseType(Guid installationId, long applicationId, AsyncCallback callback, object asyncState);
        ValidationContractResult EndGetLicenseType(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(LicenseInformationContract))]
        IAsyncResult BeginGetLicenseInformation(AsyncCallback callback, object asyncState);
        ValidationContractResult EndGetLicenseInformation(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(LicensePayContract))]
        IAsyncResult BeginGetLicensePaymentInformation(AsyncCallback callback, object asyncState);
        ValidationContractResult EndGetLicensePaymentInformation(IAsyncResult asyncResult);

        #endregion

        #region Stand
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(StandRecord))]
        IAsyncResult BeginGetStands(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetStands(IAsyncResult asyncResult);
        #endregion

        #region Export Official Document
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetExportOfficialDocument(Guid officialDocId, AsyncCallback callback, object asyncState);
        ExportOfficialDocumentResult EndGetExportOfficialDocument(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetExportNfeDocument(Guid officialDocId, AsyncCallback callback, object asyncState);
        ValidationResult EndGetExportNfeDocument(IAsyncResult asyncResult);
        #endregion

        #region Hotels
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(HotelAppRecord))]
        IAsyncResult BeginGetHotels(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetHotels(IAsyncResult asyncResult);
        #endregion

        #region Permission Details by Application
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(PermissionDetailsRecord))]
        IAsyncResult BeginGetPermissionDetailsbyApplication(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetPermissionDetailsbyApplication(IAsyncResult asyncResult);
        #endregion

        #region Reports

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(DocumentEmailContract))]
        IAsyncResult BeginEmailDocument(DocumentEmailContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndEmailDocument(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDailyReports(AsyncCallback callback, object asyncState);
        ValidationResult EndDailyReports(IAsyncResult asyncResult);

        #endregion

        #region Bulk Transactions

        #region viewing
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BulkTransactionsViewContract))]
        IAsyncResult BeginProcessBulkTransactionsViewContract(BulkTransactionsViewContract obj, AsyncCallback callback, object asyncState);
        ValidationContractResult EndProcessBulkTransactionsViewContract(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(TransactionsGroupContract))]
        [ServiceKnownType(typeof(TransactionsGroupDetailsContract))]
        IAsyncResult BeginLoadTransactionsGroupContract(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadTransactionsGroupContract(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginPersistTransactionsGroupContract(TransactionsGroupContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndPersistTransactionsGroupContract(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(BulkTransactionsViewContract))]
        IAsyncResult BeginNewBulkTransactionsViewContract(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewBulkTransactionsViewContract(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)] // //
        [ServiceKnownType(typeof(TransactionsGroupRecord))]
        IAsyncResult BeginLoadTransactionsGroups(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndLoadTransactionsGroups(IAsyncResult asyncResult);
        #endregion
        #endregion

        #region Multihotel
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(HotelUserRecord))]
        IAsyncResult BeginGetHotelsByUser(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetHotelsByUser(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(HotelUserRecord))]
        IAsyncResult BeginGetHotelsByGroup(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetHotelsByGroup(IAsyncResult asyncResult);
        #endregion

        #region SnapShot Export
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetExportXMLDocuments(DateTime Data, short DaysAhead, AsyncCallback callback, object asyncState);
        ValidationStringResult EndGetExportXMLDocuments(IAsyncResult asyncResult);
        #endregion

        #region Virtual Room
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(VirtualRoomRecord))]
        IAsyncResult BeginGetVirtualRooms(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetVirtualRooms(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoomContract))]
        IAsyncResult BeginLoadVirtualRoom(Guid id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadVirtualRoom(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(RoomContract))]
        IAsyncResult BeginNewVirtualRoom(AsyncCallback callback, object asyncState);
        ValidationContractResult EndNewVirtualRoom(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateVirtualRoom(RoomContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateVirtualRoom(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginDeleteVirtualRoom(Guid id, AsyncCallback callback, object asyncState);
        ValidationResult EndDeleteVirtualRoom(IAsyncResult asyncResult);

        #endregion

        #region ConfirmationStatus
        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EventsConfirmationStatusRecord))]
        IAsyncResult BeginGetEventsConfirmationStatus(QueryRequest req, AsyncCallback callback, object asyncState);
        QueryResponse EndGetEventsConfirmationStatus(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [ServiceKnownType(typeof(EventsConfirmationStatusContract))]
        IAsyncResult BeginLoadEventsConfirmationStatus(string id, AsyncCallback callback, object asyncState);
        ValidationContractResult EndLoadEventsConfirmationStatus(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginUpdateEventsConfirmationStatus(EventsConfirmationStatusContract obj, AsyncCallback callback, object asyncState);
        ValidationResult EndUpdateEventsConfirmationStatus(IAsyncResult asyncResult);

        #endregion
    }
}