﻿using NewHotel.Contracts;
using System;

namespace NewHotel.Communication.Client
{

    public interface INewHotelWPFPosClient : INewHotelPosClient
    {

        #region Users

        void UpdateUserCodeAsync(Guid userID, string code, EventHandler<ClientEventArgs> callback);

        void GetUserAsync(QueryRequest req, string login, string pass, string code, EventHandler<ClientEventArgs> callback);

        void GetCajasStandsByUserAsync(QueryRequest req, Guid util_pk, EventHandler<ClientEventArgs> callback);

        #endregion

        #region ProductPrices

        void GetProductPriceRatesByRateAndProductsAsync(Guid RateId, Guid[] productsId, DateTime workDate, EventHandler<ClientEventArgs> callback);

        #endregion

        #region Ticket

        object LoadTicketRAsync(Guid id);
        object NewTicketRAsync(EventHandler<ClientEventArgs> callback);
        object UpdateTicketRAsync(TicketRContract obj, EventHandler<ClientEventArgs> callback);
        object UpdatePOSTicketSync(POSTicketContract obj);
        object SplitTicketManualAsync(Guid ticketId, Guid[] ordersToLeave, EventHandler<ClientEventArgs> callback);
        void DeleteTicketRAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        object ChargePOSTicketAccountSync(POSTicketContract obj);
        object VoidTicketSync(Guid ticketId, Guid? cancellationId, string comments);

        void DeleteProductLineAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        void AddProductLineToTicketRAsync(Guid ownerId, ProductLineContract productLineContract, EventHandler<ClientEventArgs> callback);
        void RemoveProductLinesToTicketRAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);

        void LoadPaymentLineAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewPaymentLineAsync(EventHandler<ClientEventArgs> callback);
        void UpdatePaymentLineAsync(PaymentLineContract obj, EventHandler<ClientEventArgs> callback);
        void DeletePaymentLineAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        void AddPaymentLineToTicketRAsync(Guid ownerId, PaymentLineContract paymentLineContract, EventHandler<ClientEventArgs> callback);
        void RemovePaymentLinesToTicketRAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);

        void NewLookupTableAsync(EventHandler<ClientEventArgs> callback);
        void UpdateLookupTableAsync(LookupTableContract obj, EventHandler<ClientEventArgs> callback);
        void AddLookupTableToTicketRAsync(Guid ownerId, LookupTableContract lookupTableContract, EventHandler<ClientEventArgs> callback);
        void RemoveLookupTableToTicketRAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);

        void CheckCashierUserAsync(Guid caja, Guid user, Guid hotel, EventHandler<ClientEventArgs> callback);

        object GetClosedTicketsSync(QueryRequest req);

        #endregion

        #region Stand

        void GetPrintByPeriodAsync(Guid stand, DateTime nowDateTime, EventHandler<ClientEventArgs> callback);


        #endregion

        #region Plan

        void GetPensionByRoomAsync(string aloj, DateTime date, EventHandler<ClientEventArgs> callback);
        void GetGuestsByRoomAsync(string aloj, EventHandler<ClientEventArgs> callback);

        #endregion

        #region Table Reservations

        object GetLiteTablesReservationsSync(QueryRequest req);

        object CheckInBookingTableSync(Guid bookingTableId);

        object UndoCheckInBookingTableSync(Guid bookingTableId);

        object CheckOutBookingTableSync(Guid bookingTableId);

        object UndoCheckOutBookingTableSync(Guid bookingTableId);

        object CancelTablesReservationSync(Guid bookingTableId, CancellationControlContract cancellationControlContract);

        object UndoCancelTablesReservationSync(Guid bookingTableId);

        #endregion

        #region Clients

        object UpdateClientSync(ClientContract obj);

        #endregion Clients

        void GetSerieConsecutiveAsync(long docTypeId, Guid stand, Guid cashier, EventHandler<ClientEventArgs> callback);
        void GetSignatureAsync(long docTypeId, DateTime documentDate,
            DateTime documentEmissionDateTime, string documentSerie, long documentNumber, decimal documentValue, EventHandler<ClientEventArgs> callback);

        object GetStandCajasSync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void SynchronizeCategoriesAsync(QueryRequest req, Guid[] ids, DateTime[] lMods, EventHandler<ClientEventArgs> callback);
        void SynchronizeProductsAsync(QueryRequest req, Guid[] ids, DateTime[] lMods, EventHandler<ClientEventArgs> callback);
        void SynchronizeStandProductsAsync(QueryRequest req, Guid[] ids, DateTime[] lMods, EventHandler<ClientEventArgs> callback);
        void SynchronizeStandProductCategoriesAsync(QueryRequest req, Guid[] ids, DateTime[] lMods, EventHandler<ClientEventArgs> callback);

        void SynchronizeTablesReservationsAsync(QueryRequest req, Guid[] ids, DateTime[] lMods, EventHandler<ClientEventArgs> callback);

    }
}
