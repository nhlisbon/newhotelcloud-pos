﻿using NewHotel.Contracts;
using NewHotel.DataAnnotations;
using NewHotel.Pos.Hub.Communication;
using System;
using System.Collections.Generic;

namespace NewHotel.Communication.Client
{
    public class NewHotelWPFPosClient : NewHotelPosClient, INewHotelWPFPosClient
    {
        #region Constructors

        public NewHotelWPFPosClient(IRequestSessionData requestSessionData, ContextData contextData)
            : base(requestSessionData, contextData)
        {
        }

        public NewHotelWPFPosClient(IRequestSessionData requestSessionData, ContextData contextData,
            string baseHostAddress)
            : base(requestSessionData, contextData, baseHostAddress)
        {
        }

        #endregion

        #region INewHotelManagement

        public void GetUserAsync(QueryRequest req, string login, string pass, string code, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            if (string.IsNullOrEmpty(code))
                Invoke(proxy, "GetUser", callback, null, req, login, pass);
            else Invoke(proxy, "GetUserByCode", callback, null, req, code);
        }

        public void UpdateUserCodeAsync(Guid userID, string code, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateUserCode", callback, null, userID, code);
        }

        public void GetCajasStandsByUserAsync(QueryRequest req, Guid util_pk, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetCajasStandsByUser", callback, null, req, util_pk);
        }

        public void GetProductPriceRatesByRateAndProductsAsync(Guid rateId, Guid[] productsId, DateTime workDate, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetProductPriceRatesByRateAndProducts", callback, null, rateId, productsId, workDate);
        }

        #region TicketR

        public object LoadTicketRAsync(Guid id)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "LoadTicketR", null, null, id);
        }

        public object NewTicketRAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "NewTicketR", callback, null);
        }

        public object UpdateTicketRAsync(TicketRContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "UpdateTicketR", callback, null, obj);
        }
        public void DeleteTicketRAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteTicketR", callback, null, ids);
        }
        public object UpdatePOSTicketSync(POSTicketContract obj)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "UpdatePOSTicket", null, null, obj);
        }
        public object SplitTicketManualAsync(Guid ticketId, Guid[] ordersToLeave, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "SplitTicketManual", callback, null, ticketId, ordersToLeave);
        }
        public object GetReservationsSync(QueryRequest req)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetReservations", null, null, req);
        }
        public object GetEventReservationsSync(QueryRequest req)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetEventReservations", null, null, req);
        }
        public object GetSpaReservationsSync(QueryRequest req)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetSpaReservations", null, null, req);
        }

        public object ChargePOSTicketAccountSync(POSTicketContract obj)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "ChargePOSTicketAccount", null, null, obj);
        }

        public object VoidTicketSync(Guid ticketId, Guid? cancellationId, string comments)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "VoidTicket", null, null, ticketId, cancellationId, comments);
        }

        public object GetEntityInstallationsSync(QueryRequest req)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetEntityInstallations", null, null, req);
        }
        public object GetReservationsGroupsSync(QueryRequest req)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetReservationsGroups", null, null, req);
        }

        public object GetControlAccountsSync(QueryRequest req)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetControlAccounts", null, null, req);
        }

        public object GetClientInstallationsSync(QueryRequest req)
        {
            var proxy = GetSettingManagement();
            return Invoke(proxy, "GetClientInstallations", null, null, req);
        }

        public object GetTicketsRealSync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "GetTicketsR", callback, null, req);
        }

        public object LoadTicketRealSync(Guid id)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "LoadTicketR", null, null, id);
        }

        public object UpdateTablesGroupSync(TablesGroupContract obj)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "UpdateTablesGroup", null, null, obj);
        }

        public object UpdateMesaSync(MesaContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "UpdateMesa", callback, null, obj);
        }

        public object VoidOldTicketSync(Guid ticketId, CancellationControlContract cancellationContract, Guid[] productLinesIds)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "VoidOldTicket", null, null, ticketId, cancellationContract, productLinesIds);
        }

        public object GetGeneralSettingsSync()
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "GetGeneralSettings", null, null);
        }

        public object CloseDaySync(Guid standId, Guid? cajaId, DateTime? closeDay)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "CloseDay", null, null, standId, cajaId, closeDay);
        }

        public object CloseTurnSync(Guid standId, Guid? cajaId)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "CloseTurn", null, null, standId, cajaId);
        }

        public object GetTicketPrintConfigurationsSync()
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "GetTicketPrintConfigurations", null, null);
        }

        public object GetStandsFromCloudSync(int language)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "GetPOSStands", null, null, language);
        }

        #region Reports

        public object GetSalesCashierSync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "GetSalesCashier", callback, null, req);
        }

        public object GetSalesCajaCashierSync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "GetSalesCaja", callback, null, req);
        }

        #endregion

        public void DeleteProductLineAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeleteProductLine", callback, null, ids);
        }

        public void AddProductLineToTicketRAsync(Guid ownerId, ProductLineContract productLineContract, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddProductLineToTicketR", callback, null, ownerId, productLineContract);
        }

        public void RemoveProductLinesToTicketRAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "RemoveProductLinesToTicketR", callback, null, ownerId, ids);
        }

        public void LoadPaymentLineAsync(Guid id, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "LoadPaymentLine", callback, null, id);
        }

        public void NewPaymentLineAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewPaymentLine", callback, null);
        }

        public void UpdatePaymentLineAsync(PaymentLineContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdatePaymentLine", callback, null, obj);
        }

        public void DeletePaymentLineAsync(Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "DeletePaymentLine", callback, null, ids);
        }

        public void AddPaymentLineToTicketRAsync(Guid ownerId, PaymentLineContract paymentLineContract, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddPaymentLineToTicketR", callback, null, ownerId, paymentLineContract);
        }

        public void RemovePaymentLinesToTicketRAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "RemovePaymentLinesToTicketR", callback, null, ownerId, ids);
        }

        public object GetClosedTicketsSync(QueryRequest req)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "GetClosedTickets", null, null, req);
        }

        #endregion

        #endregion

        #region Stand

        public void GetPrintByPeriodAsync(Guid stand, DateTime nowDateTime, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetPrintByPeriod", callback, null, stand, nowDateTime);
        }

        #endregion

        #region Payments

        #region Plan 

        public void GetPensionByRoomAsync(string aloj, DateTime date, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetPensionByRoom", callback, null, aloj, date);
        }

        public void GetGuestsByRoomAsync(string aloj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetGuestsByRoom", callback, null, aloj);
        }

        #endregion

        #region Cancellation

        public void UpdateCancellationControlAsync(CancellationControlContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateCancellationControl", callback, null, obj);
        }

        #endregion

        #endregion

        #region Tables Reservations

        public object GetLiteTablesReservationsSync(QueryRequest req)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "GetLiteTablesReservations", null, null, req);
        }

        public object CheckInBookingTableSync(Guid bookingTableId)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "CheckInBookingTable", null, null, bookingTableId);
        }

        public object UndoCheckInBookingTableSync(Guid bookingTableId)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "UndoCheckInBookingTable", null, null, bookingTableId);
        }

        public object CheckOutBookingTableSync(Guid bookingTableId)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "CheckOutBookingTable", null, null, bookingTableId);
        }

        public object UndoCheckOutBookingTableSync(Guid bookingTableId)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "UndoCheckOutBookingTable", null, null, bookingTableId);
        }

        public object CancelTablesReservationSync(Guid bookingTableId, CancellationControlContract cancellationControlContract)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "CancelTablesReservation", null, null, new[] { bookingTableId }, cancellationControlContract);
        }

        public object UndoCancelTablesReservationSync(Guid bookingTableId)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "UndoCancelTablesReservation", null, null, new[] { bookingTableId });
        }

        #endregion

        #region Clients

        public object UpdateClientSync(ClientContract obj)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "UpdateClient", null, null, obj);
        }

        #endregion Clients

        #region Protected

        protected override IList<System.ServiceModel.Description.IEndpointBehavior> GetBehaviors()
        {
            var list = base.GetBehaviors();
            list.Add(SessionCookieBehavior.Instance);
            return list;
        }

        #endregion

        public void GetSerieConsecutiveAsync(long docTypeId, Guid stand, Guid cashier, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetSerieConsecutive", callback, null, docTypeId, stand, cashier);
        }

        public void GetSignatureAsync(long docTypeId, DateTime documentDate,
            DateTime documentEmissionDateTime, string documentSerie, long documentNumber, decimal documentValue, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "GetSignature", callback, null, docTypeId, documentDate, documentEmissionDateTime,
            documentSerie, documentNumber, documentValue);
        }

        public object GetStandCajasSync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            StandContract a = new StandContract();
            return Invoke(proxy, "GetStandCajas", callback, null, req);
        }

        public object GetStandsSync(QueryRequest req, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            return Invoke(proxy, "GetStands", callback, null, req);
        }

        public void SynchronizeCategoriesAsync(QueryRequest req, Guid[] ids, DateTime[] lMods, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "SynchronizeCategories", callback, null, req, ids, lMods);
        }

        public void SynchronizeProductsAsync(QueryRequest req, Guid[] ids, DateTime[] lMods, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "SynchronizeProducts", callback, null, req, ids, lMods);
        }

        public void SynchronizeStandProductsAsync(QueryRequest req, Guid[] ids, DateTime[] lMods, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "SynchronizeStandProducts", callback, null, req, ids, lMods);
        }

        public void SynchronizeStandProductCategoriesAsync(QueryRequest req, Guid[] ids, DateTime[] lMods, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "SynchronizeStandProductsCategory", callback, null, req, ids, lMods);
        }

        public void SynchronizeTablesReservationsAsync(QueryRequest req, Guid[] ids, DateTime[] lMods, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "SynchronizeTablesReservations", callback, null, req, ids, lMods);
        }

        public void NewLookupTableAsync(EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "NewLookupTable", callback, null);
        }

        public void UpdateLookupTableAsync(LookupTableContract obj, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "UpdateLookupTable", callback, null, obj);
        }

        public void AddLookupTableToTicketRAsync(Guid ownerId, LookupTableContract lookupTableContract, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "AddLookupTableToTicketR", callback, null, ownerId, lookupTableContract);
        }

        public void RemoveLookupTableToTicketRAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "RemoveLookupTableToTicketR", callback, null, ownerId, ids);
        }

        public void CheckCashierUserAsync(Guid caja, Guid user, Guid hotel, EventHandler<ClientEventArgs> callback)
        {
            var proxy = GetNewHotelManagement();
            Invoke(proxy, "CheckCashierUser", callback, null, caja, user, hotel);
        }
    }
}