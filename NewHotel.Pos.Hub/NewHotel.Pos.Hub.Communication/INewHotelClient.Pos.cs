﻿using System;
using System.Collections.Generic;
using NewHotel.Contracts;

namespace NewHotel.Communication.Client
{
    public interface INewHotelPosClient : IApplicationClient
    {
        #region Dummy

        void DummyAsync(EventHandler<ClientEventArgs> callback);

        #endregion

        #region Administration
        void LoadNewHotelContextAsync(string username, Guid installationId, long languageId, EventHandler<ClientEventArgs> callback);
        void ClearNewHotelContextAsync(EventHandler<ClientEventArgs> callback);
        void LoadWorkDateAsync(EventHandler<ClientEventArgs> callback);
        void CloseContextAsync(int context, EventHandler<ClientEventArgs> callback);
        void LoadDatabaseVersionAsync(EventHandler<ClientEventArgs> callback);

        void NewNPosSettingsAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void LoadNPosSettingsAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void UpdateNPosSettingsAsync(SettingNPosContract obj, EventHandler<ClientEventArgs> callback);
        #endregion
        #region Management
        #region TicketSerie
        void GetSerieByStandCajaAsync(Guid standId, Guid? cajaId, long type, EventHandler<ClientEventArgs> callback);
        void GetSerieByStandCajaInfoAsync(Guid standId, Guid cajaId, EventHandler<ClientEventArgs> callback);
        void GetSeriesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetSeriesDescAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void UpdateSerieControlAsync(DocumentSerieControlContract contract, EventHandler<ClientEventArgs> callback);
        void UpdateSerieAsync(DocumentSerieContract contract, EventHandler<ClientEventArgs> callback);
        void LoadSerieAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewSerieControlAsync(EventHandler<ClientEventArgs> callback);
        void NewSerieAsync(EventHandler<ClientEventArgs> callback);
        void LoadSerieControlAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void DeleteSerieAsync(Guid[] idsc, EventHandler<ClientEventArgs> callback);
        void InactiveSerieByCajaAsync(Guid ipos_pk, Guid caja_pk, Guid serieId, long serieNumber, long type, EventHandler<ClientEventArgs> callback);
        //void BlockCajaAsync(Guid ipos_pk, Guid caja_pk, EventHandler<ClientEventArgs> callback);
        //void UnBlockCajaAsync(Guid ipos_pk, Guid caja_pk, long currentNumber, EventHandler<ClientEventArgs> callback);
        #endregion
        #region TicketReal
        void GetTicketsRealAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadTicketRealAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void UpdateTicketRealAsync(TicketRContract ticket, EventHandler<ClientEventArgs> callback);
        void DeleteTicketRealAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        void DefineTaxProductLineAsync(ProductLineContract productLine, EventHandler<ClientEventArgs> callback);
        void GetProductPriceRateByRateAndProductAsync(Guid periodId, Guid[] productsId, DateTime workDate, EventHandler<ClientEventArgs> callback);
        void VoidOldTicketAsync(Guid ticketId, EventHandler<ClientEventArgs> callback);
        #endregion

        #region Export Official Document

        void GetExportOfficialDocumentAsync(Guid id, EventHandler<ClientEventArgs> callback);
        ExportOfficialDocumentResult GetExportOfficialDocumentSync(Guid id);

        #endregion

        #region TicketsError
        void LoadTicketsErrorAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewTicketsErrorAsync(EventHandler<ClientEventArgs> callback);
        void UpdateTicketsErrorAsync(TicketsErrorContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteTicketsErrorAsync(Guid id, EventHandler<ClientEventArgs> callback);
        #endregion

        #region StandCajaLog
        void LoadStandCajaLogAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewStandCajaLogAsync(EventHandler<ClientEventArgs> callback);
        void UpdateStandCajaLogAsync(StandCajaLogContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteStandCajaLogAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        void GetStandCajaLogAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);

        #endregion

        #region TicketActions
        void TrasferTicketAsync(TransferContract contract, TicketRContract ticketContract, EventHandler<ClientEventArgs> callback);
        void AcceptTransferTicketAsync(TransferContract contract, Guid cajaId, EventHandler<ClientEventArgs> callback);
        void ReturnTransferTicketAsync(TransferContract contract, EventHandler<ClientEventArgs> callback);
        void VerifyTransferReturnsAsync(Guid stand, Guid caja, EventHandler<ClientEventArgs> callback);
        void VerifyTransferToAcceptAsync(Guid stand, EventHandler<ClientEventArgs> callback);
        #endregion
        void CancelTicketAsync(Guid ownerId, Guid cancellationReason, string cancellationRDesc, string comments, EventHandler<ClientEventArgs> callback);
        void CancelTicketContractsAsync(TicketRContract ticket, CancellationControlContract cancellation, EventHandler<ClientEventArgs> callback);
        void VerifyCloseDayAsync(Guid standId, Guid cajaId, EventHandler<ClientEventArgs> callback);
        void CloseDayStandAsync(Guid standId, Guid? cajaId, DateTime? closeDay, EventHandler<ClientEventArgs> callback);
        void CloseTurnAsync(Guid standId, Guid? cajaId, EventHandler<ClientEventArgs> callback);
        void ValidateLogInAsync(Guid standId, Guid? cajaId, EventHandler<ClientEventArgs> callback);

        void LogInAsync(Guid standId, Guid cajaId, Guid userId, Guid? fisi, EventHandler<ClientEventArgs> callback);
        void LogOutAsync(Guid standId, Guid cajaId, long ticketSerieNumber, long? invoiceSerieNumber, long? receiptSerieNumber, string ticketSign, string invoiceSign, string receiptSign, EventHandler<ClientEventArgs> callback);
        void VerifyTaxesSchemaAsync(Guid servicePk, Guid? schemaPk, EventHandler<ClientEventArgs> callback);
        void GetLicenceTypeAsync(EventHandler<ClientEventArgs> callback);
        #endregion
        #region Settings Operations
        #region AppClasifier
        void GetApplicationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetAppClasifierApplicationsAsync(QueryRequest req, Guid ownerId, string ownerType, EventHandler<ClientEventArgs> callback);
        void AddAppClasifierApplicationsAsync(Guid ownerId, string ownerType, long[] ids, EventHandler<ClientEventArgs> callback);
        void RemoveAppClasifierApplicationsAsync(Guid ownerId, string ownerType, Guid[] ids, EventHandler<ClientEventArgs> callback);
        #endregion
        #region ProductLine
        void GetProductLinesByTicketAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void NewProductLineAsync(EventHandler<ClientEventArgs> callback);
        void LoadProductLineAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void UpdateProductLineAsync(ProductLineContract obj, EventHandler<ClientEventArgs> callback);
        #endregion

        void GetPaymentLinesByTicketAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);

        #region Products

        void GetProductsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetProductsByCodisAsync(List<string> codis, EventHandler<ClientEventArgs> callback);
        void LoadProductAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewProductAsync(EventHandler<ClientEventArgs> callback);
        void UpdateProductAsync(ProductContract obj, EventHandler<ClientEventArgs> callback);
        void UpdateProductbyDispatchAreasAsync(ProductContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteProductAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        void InactiveProductAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        void UpdateProductGroupCategoryAsync(Guid[] ids, Guid groupId, long categoryId, EventHandler<ClientEventArgs> callback);
        void SetProductsServiceAsync(Guid? serviceId, Guid[] ids, Guid standId, EventHandler<ClientEventArgs> callback);
        void SetProductstoStandAsync(Guid standId, Guid[] ids, EventHandler<ClientEventArgs> callback);

        void GetGroupProductsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadGroupProductAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewGroupProductAsync(EventHandler<ClientEventArgs> callback);
        void UpdateGroupProductAsync(GroupProductContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteGroupProductAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);

        void GetProductFamiliesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadProductFamilyAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewProductFamilyAsync(EventHandler<ClientEventArgs> callback);
        void UpdateProductFamilyAsync(ProductFamilyContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteProductFamilyAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);

        void GetProductSubFamiliesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadProductSubFamilyAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewProductSubFamilyAsync(EventHandler<ClientEventArgs> callback);
        void UpdateProductSubFamilyAsync(ProductSubFamilyContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteProductSubFamilyAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);

        void GetSeparatorsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadSeparatorAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewSeparatorAsync(EventHandler<ClientEventArgs> callback);
        void UpdateSeparatorAsync(SeparatorContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteSeparatorAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        void MoveUpSeparator(Guid id, EventHandler<ClientEventArgs> callback);
        void MoveDownSeparator(Guid id, EventHandler<ClientEventArgs> callback);

        void GetProductAdditionalsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void RemoveProductAdditionalsAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void AddAdditionalProductAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);

        void GetProductPriceRateByProductAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void RemoveProductPriceRatesFromProductAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void AddPriceRatesProductAsync(Guid ownerId, ProductPriceRateContract contract, EventHandler<ClientEventArgs> callback);
        void LoadPriceRateProductAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewPriceRateProductAsync(Guid ownerId, EventHandler<ClientEventArgs> callback);

        void GetProductImagesAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void RemoveProductImagesAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);

        void LoadImageProductAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewImageProductAsync(Guid ownerId, EventHandler<ClientEventArgs> callback);
        void UpdateImageProductAsync(ProductImageContract image, Guid ownerId, EventHandler<ClientEventArgs> callback);

        void GetProductBarCodesAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void RemoveProductBarCodesAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void LoadBarCodeProductAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewBarCodeProductAsync(Guid ownerId, EventHandler<ClientEventArgs> callback);
        void UpdateBarCodeProductAsync(ProductBarCodeContract barCode, Guid ownerId, EventHandler<ClientEventArgs> callback);

        void GetProductTablesAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void RemoveProductTablesAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void LoadTableProductAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewTableProductAsync(Guid ownerId, EventHandler<ClientEventArgs> callback);
        void UpdateTableProductAsync(TableProductContract tableProduct, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void AddAddTableToProductAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void UpdateProductTableValueAsync(Guid ownerId, decimal quantity, EventHandler<ClientEventArgs> callback);

        void RemoveStandProductFromProductAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void AddStandProductToProductAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);

        void GetProductsStandbyAreaAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);

        #endregion

        #region Menu
        void GetMenusAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void NewMenuAsync(EventHandler<ClientEventArgs> callback);
        void LoadMenuAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void UpdateMenuAsync(MenuContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteMenuAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        #endregion

        #region Caja
        void GetCajasAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadCajaAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewCajaAsync(EventHandler<ClientEventArgs> callback);
        void UpdateCajaAsync(CajaContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteCajaAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        void RemoveStandCajasFromCajaAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void AddStandCajasToCajaAsync(CajaContract caja, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void CheckStandCajasSeriesAsync(Guid standId, Guid cajaId, EventHandler<ClientEventArgs> callback);
        void ValidateSerieUnicityAsync(StandCajasContract contract, EventHandler<ClientEventArgs> callback);
        #endregion

        #region Duty
        void GetDutiesByPeriodAsync(QueryRequest req, DateTime initial, DateTime final, EventHandler<ClientEventArgs> callback);
        void GetDutiesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadDutyAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewDutyAsync(EventHandler<ClientEventArgs> callback);
        void UpdateDutyAsync(DutyContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteDutyAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        #endregion

        #region Mesas
        void GetSaloonsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetUsedMesasAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetMesasAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetSaloonsByStandAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadSaloonAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewSaloonAsync(EventHandler<ClientEventArgs> callback);
        void UpdateSaloonAsync(SaloonContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteSaloonAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);

        void LoadMesaAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewMesaAsync(EventHandler<ClientEventArgs> callback);
        void UpdateMesaAsync(MesaContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteMesaAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        #endregion

        #region TablesGroups

        void GetAllTablesGroupsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadTablesGroupAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewTablesGroupAsync(EventHandler<ClientEventArgs> callback);
        void UpdateTablesGroupAsync(TablesGroupContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteTablesGroupAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        #endregion

        #region TablesReservations

        void GetTablesReservationPeriodsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadTablesReservationPeriodAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewTablesReservationPeriodAsync(EventHandler<ClientEventArgs> callback);
        void UpdateTablesReservationPeriodAsync(TablesReservationPeriodContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteTablesReservationPeriodAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);

        object GetTablesReservationAvailabilityAsync(TablesReservationPeriodRecord period, DateTime date, Guid? Stand, int paxs, EventHandler<ClientEventArgs> callback);
        void GetAvailableTablesAsync(TablesReservationLineContract reservation, EventHandler<ClientEventArgs> callback);
        void GetAvailableTablesGroupsAsync(TablesReservationLineContract reservation, EventHandler<ClientEventArgs> callback);
        void GetUnavailableTablesAsync(TablesReservationLineContract reservation, EventHandler<ClientEventArgs> callback);

        object GetLiteTablesReservationsByDateAsync(QueryRequest req, DateTime? date, EventHandler<ClientEventArgs> callback);
        void GetLiteTablesReservationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void CancelTablesReservationAsync(Guid[] ids, CancellationControlContract obj, EventHandler<ClientEventArgs> callback);

        void LoadTablesReservationLineAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewTablesReservationLineAsync(EventHandler<ClientEventArgs> callback);
        void UpdateTablesReservationLineAsync(TablesReservationLineContract obj, EventHandler<ClientEventArgs> callback);
        ValidationContractResult UpdateTablesReservationLine(TablesReservationLineContract line);
        void DeleteTablesReservationLineAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);

        #endregion

        #region Contacts

        object GetContactsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        object ClientFromContactAsync(Guid id, EventHandler<ClientEventArgs> callback);
        object LoadContactAsync(Guid id, EventHandler<ClientEventArgs> callback);
        object NewContactAsync(EventHandler<ClientEventArgs> callback);
        object UpdateContactAsync(ContactContract obj, EventHandler<ClientEventArgs> callback);
        object DeleteContactAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        object GetContactTypesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        object GetContactTypeDetailsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);

        #region Title
        object GetTitlesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        object GetTitleAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        object LoadTitleAsync(Guid id, EventHandler<ClientEventArgs> callback);
        object NewTitleAsync(EventHandler<ClientEventArgs> callback);
        object UpdateTitleAsync(TitleContract obj, EventHandler<ClientEventArgs> callback);
        object RemoveTitleAsync(Guid ids, EventHandler<ClientEventArgs> callback);
        #endregion

        #region Contacts' Support

        object GetCountriesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        object GetJobFunctionsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        object GetProfessionsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);

        #region Regional
        object GetLanguagesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        object GetLanguageTranslationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        object GetWorldRegionsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        object GetDistrictsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        #endregion

        #endregion

        #endregion

        #region ImagePaymentForm
        void GetImagesByPaymentFormAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void GetOrderByStandAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void GetImagesPaymentFormAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadImagePaymentFormAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewImagePaymentFormAsync(EventHandler<ClientEventArgs> callback);
        void UpdateImagePaymentFormAsync(ImagePaymentTypeContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteImagePaymentFormAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);

        void MoveUpImage(Guid id, Guid stand, EventHandler<ClientEventArgs> callback);
        void MoveDownImage(Guid id, Guid stand, EventHandler<ClientEventArgs> callback);
        #endregion
        #region Stand
        void GetProductRateTypesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadProductRateTypeAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewProductRateTypeAsync(EventHandler<ClientEventArgs> callback);
        void UpdateProductRateTypeAsync(ProductRateTypeContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteProductRateTypeAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);

        void GetCookMarchsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadCookMarchAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewCookMarchAsync(EventHandler<ClientEventArgs> callback);
        void UpdateCookMarchAsync(CookMarchContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteCookMarchAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);

        void GetStandsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadStandAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void GetStandTurnDateAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewStandAsync(EventHandler<ClientEventArgs> callback);
        void UpdateStandAsync(StandContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteStandAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);

        void GetStandProductsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void GetStandProductsByStandAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void GetStandProductsByStandsAsync(QueryRequest req, Guid[] stands, EventHandler<ClientEventArgs> callback);
        void GetProductsByStandAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void GetProductsByStandAllAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void GetStandProductsByProductAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void RemoveStandProductsAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void DeleteProductsByStandAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void LoadStandProductAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewStandProductAsync(Guid ownerId, EventHandler<ClientEventArgs> callback);
        void UpdateStandProductAsync(StandProductsContract standProduct, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void AddStandProductsToStandAsync(Guid ownerId, StandProductsContract[] stands, EventHandler<ClientEventArgs> callback);
        void DeleteStandProductsFromStandAsync(Guid ownerId, Guid[] standProducts, EventHandler<ClientEventArgs> callback);
        void SetServiceToAllStandProductsAsync(Guid ownerId, Guid? service, string serviceDesc, EventHandler<ClientEventArgs> callback);
        void SetServiceToStandProductsAsync(Guid ownerId, Guid[] standProducts, Guid? service, string serviceDesc, EventHandler<ClientEventArgs> callback);

        void GetStandCajasAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void GetCajasByStandAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void RemoveStandCajasFromStandAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void AddStandCajasToStandAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);

        void GetStandPriceCostsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void RemoveStandPriceCostsAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void LoadStandPriceCostAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewStandPriceCostAsync(Guid ownerId, EventHandler<ClientEventArgs> callback);
        void UpdateStandPriceCostAsync(ProductPriceCostContract standPriceCost, Guid ownerId, EventHandler<ClientEventArgs> callback);

        #endregion

        #region Prices

        void GetHappyHoursAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetStandHappyHoursAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadHappyHourAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewHappyHourAsync(EventHandler<ClientEventArgs> callback);
        void UpdateHappyHourAsync(HappyHourContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteHappyHourAsync(Guid id, EventHandler<ClientEventArgs> callback);

        void GetProductPriceRatesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetPricesByPeriodAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void GetCopyRateAsync(CopyRateContract copyRateContract, EventHandler<ClientEventArgs> callback);
        void GetProductPriceRatesByPriceRateAsync(Guid priceRateId, EventHandler<ClientEventArgs> callback);
        void LoadProductPriceRateAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void LoadRateVisualAsync(EventHandler<ClientEventArgs> callback);
        void LoadProductRateVisualAsync(Guid productId, EventHandler<ClientEventArgs> callback);
        void LoadAllPeriodsAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        void NewProductPriceRateAsync(EventHandler<ClientEventArgs> callback);
        void UpdateProductPriceRateAsync(ProductPriceRateContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteProductPriceRateAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);

        void GetAllPricePeriodsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetPricePeriodsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void RemovePricePeriodsAsync(Guid ownerId, EventHandler<ClientEventArgs> callback);
        void LoadPricePeriodAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void GetProductsByPeriodAsync(Guid ownerId, EventHandler<ClientEventArgs> callback);
        void CopyProductsToPeriodAsync(Guid ownerId, Guid newPeriodId, bool Percent, bool Plus, decimal SelectedValue, EventHandler<ClientEventArgs> callback);
        void NewPricePeriodAsync(EventHandler<ClientEventArgs> callback);
        void UpdatePricePeriodAsync(PricePeriodContract pricePeriod, Guid ownerId, EventHandler<ClientEventArgs> callback);

        void GetBlockPeriodsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void RemoveBlockPeriodsAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void LoadBlockPeriodAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewBlockPeriodAsync(Guid ownerId, EventHandler<ClientEventArgs> callback);
        void UpdateBlockPeriodAsync(BlockPeriodContract BlockPeriod, Guid ownerId, EventHandler<ClientEventArgs> callback);

        void GetHappyHourStandsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void UpdateHappyHourStandValueAsync(Guid ownerId, EventHandler<ClientEventArgs> callback);
        void AddStandToHappyHourAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void RemoveHappyHourStandsAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void LoadHappyHourStandAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewHappyHourStandAsync(Guid ownerId, EventHandler<ClientEventArgs> callback);
        void UpdateHappyHourStandAsync(StandHappyHourContract standHappyHour, Guid ownerId, EventHandler<ClientEventArgs> callback);

        void RemovePricesFromPeriodAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void AddPricesToPeriodAsync(Guid ownerId, ProductPriceRateContract[] objects, EventHandler<ClientEventArgs> callback);
        void SetPriceToAllFromPeriodAsync(Guid ownerId, decimal price, EventHandler<ClientEventArgs> callback);
        void SetPriceFromPeriodAsync(Guid ownerId, Guid[] productPricesIds, decimal price, EventHandler<ClientEventArgs> callback);
        #endregion
        #region internalUse
        void GetinternalUseAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetInternalUsesXStandAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadinternalUseAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewinternalUseAsync(EventHandler<ClientEventArgs> callback);
        void UpdateinternalUseAsync(InternalUseContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteinternalUseAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);

        void GetInternalUseStandsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void UpdateInternalUseStandValueAsync(Guid ownerId, double minValue, EventHandler<ClientEventArgs> callback);
        void AddStandToInternalUseStandAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void RemoveInternalUseStandsAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void LoadInternalUseStandAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewInternalUseStandAsync(Guid ownerId, EventHandler<ClientEventArgs> callback);
        void UpdateInternalUseStandAsync(InternalUsexStandContract internalUse, Guid ownerId, EventHandler<ClientEventArgs> callback);
        #endregion

        #region Preparations
        void GetAllProductPreparationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetAllFamilyPreparationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetAllGroupPreparationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);

        void GetPreparationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadPreparationsAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewPreparationsAsync(EventHandler<ClientEventArgs> callback);
        void UpdatePreparationsAsync(PreparationsContract obj, EventHandler<ClientEventArgs> callback);
        void DeletePreparationsAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);

        void GetPreparationsFamilysAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void RemovePreparationsFamilysAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void AddPreparationsFamilyAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);

        void GetPreparationsGroupsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void RemovePreparationsGroupsAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void AddPreparationsGroupAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);

        void GetPreparationsProductsAsync(QueryRequest req, Guid productId, EventHandler<ClientEventArgs> callback);
        void GetProductsPreparationAsync(QueryRequest req, Guid productId, EventHandler<ClientEventArgs> callback);
        void RemovePreparationsProductsAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        void AddPreparationsProductAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        #endregion
        #region Service
        void GetProductTaxesAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void NewProductTaxAsync(EventHandler<ClientEventArgs> callback);
        void LoadProductTaxAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void AddProductTaxAsync(Guid ownerId, ServiceTaxContract obj, EventHandler<ClientEventArgs> callback);
        void RemoveProductTaxAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        #endregion

        #region Taxes


        #endregion

        #region Local
        void GetPosTaxServicesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetAllTaxesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetAllTaxSchemaAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetAllTaxSequenceAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);

        void GetAllProductsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetAllCategoriesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetAllStandProductsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetAllStandProductsCategoryAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetAllAreasAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetAllStandProductAreasAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetSettingsParametersAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);

        void LoadAreasAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewAreasAsync(EventHandler<ClientEventArgs> callback);
        void UpdateAreasAsync(DispatchAreaContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteAreasAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        #endregion

        #region Categories
        void GetProductsInCategoriesAsync(Guid standId, EventHandler<ClientEventArgs> callback);
        void GetTopCategoriesAsync(EventHandler<ClientEventArgs> callback);
        void GetCategoriesInUseAsync(EventHandler<ClientEventArgs> callback);
        void GetCategoriesByCategoriesAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        void GetCategoriesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadCategoryAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewCategoryAsync(EventHandler<ClientEventArgs> callback);
        void UpdateCategoryAsync(CategorySubCategoryContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteCategoryAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        void AddCategoryToCategoryAsync(CategorySubCategoryContract parent, CategorySubCategoryContract category, EventHandler<ClientEventArgs> callback);
        void RemoveCategoryToCategoryAsync(CategorySubCategoryContract category, EventHandler<ClientEventArgs> callback);

        void GetProductStandCategoryAsync(Guid standId, EventHandler<ClientEventArgs> callback);
        void GetProductStandCategoryByCategoryAsync(Guid standId, Guid categoryId, EventHandler<ClientEventArgs> callback);
        void GetProductStandCategoryByCategoryStandsAsync(object[] standIds, Guid categoryId, EventHandler<ClientEventArgs> callback);
        void LoadProductStandCategoryAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewProductStandCategoryAsync(EventHandler<ClientEventArgs> callback);
        void UpdateProductStandCategoryAsync(ProductStandCategoryContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteProductStandCategoryAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        void UpdateProductStandsCategoryAsync(ProductStandCategoryContract obj, Guid[] stands, EventHandler<ClientEventArgs> callback);

        void DeleteProductStandsCategoryAsync(Guid[] ids, Guid catId, Guid[] stands, EventHandler<ClientEventArgs> callback);
        void CopyProductStandCategoriesFromAsync(Guid from, Guid to, EventHandler<ClientEventArgs> callback);

        #endregion

        #region CreditCard
        void NewCreditCardAsync(EventHandler<ClientEventArgs> callback);
        object LoadCreditCardAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void UpdateCreditCardAsync(CreditCardContract obj, EventHandler<ClientEventArgs> callback);
        #endregion

        #region PricePEriodProduct
        void AddPricePeriodProductAsync(PricePeriodProductContract contract, EventHandler<ClientEventArgs> callback);
        void DeletePricePeriodProductAsync(Guid id, EventHandler<ClientEventArgs> callback);
        #endregion

        #region CurrentAccounts
        void GetReservationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetSpaReservationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetEventReservationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetEntityInstallationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetReservationsGroupsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetControlAccountSearchAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetClientInstallationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetGuestsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        #endregion

        #region Cancellation

        #region Cancellation control
        void NewCancellationControlAsync(EventHandler<ClientEventArgs> callback);
        void LoadCancellationControlAsync(Guid id, EventHandler<ClientEventArgs> callback);

        #endregion

        #region Cancellation reason
        void GetCancellationReasonsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadCancellationReasonAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewCancellationReasonAsync(EventHandler<ClientEventArgs> callback);
        void UpdateCancellationReasonAsync(CancellationReasonContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteCancellationReasonAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        #endregion

        #endregion

        void GetCurrencyInstallationsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);

        QueryResponse GetCurrencyInstallationsSync(QueryRequest request);

        void GetControlAccountsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetServiceAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetServiceGroupsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetServiceTipsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetTipServicesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetServiceCategoriesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetTaxSchemasAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetTaxRatesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetPropTypesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetPaymentFormAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetMinStandWorkDateAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetPensionByRoomAsync(Guid? lioc_pk, DateTime date, QueryRequest req, EventHandler<ClientEventArgs> callback);
        object GetPriceCategoriesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);

        #endregion
        #region Configuration Operations
        #region Tickets
        void GetTicketsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadTicketAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewTicketAsync(EventHandler<ClientEventArgs> callback);
        void UpdateTicketAsync(TicketContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteTicketAsync(Guid[] ids, EventHandler<ClientEventArgs> callback);
        void GetTicketsErrorAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadTicketfromTicketErrorAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void UpdateTicketfromTicketErrorAsync(TicketRContract contract, EventHandler<ClientEventArgs> callback);
        #endregion
        #region ImagesGallery
        void GetImagesGalleryAsync(int type, EventHandler<ClientEventArgs> callback);
        void MoveImageToServiceAsync(string imageName, byte[] image, int type, EventHandler<ClientEventArgs> callback);
        void GetImagesFromFileAsync(Guid parentId, string path, int page, EventHandler<ClientEventArgs> callback);
        void RemoveImageCustomAsync(string imageName, int type, EventHandler<ClientEventArgs> callback);
        void GetImageFromPathAsync(string path, EventHandler<ClientEventArgs> callback);
        #endregion


        #endregion
        #region ExternalOperations
        void UpdateSimpleEntryExternalAsync(MovementExternalContract contract, EventHandler<ClientEventArgs> callback);
        object UpdateSimpleEntryExternalDebitAsync(MovementExternalContract contract, EventHandler<ClientEventArgs> callback);
        void UpdateAllMovementsAsync(List<TicketRContract> tickets, Guid stand, Guid? account, EventHandler<ClientEventArgs> callback);
        void UpdateInvoiceExternalAsync(List<Guid> movements, FactInformationContract factInfo, EventHandler<ClientEventArgs> callback);
        void InvoiceAndPersistTicketAsync(TicketRContract ticket, FactInformationContract buyerInfo, Guid? ccontrol, EventHandler<ClientEventArgs> callback);

        #endregion
        #region Report Operations
        void LoadReportStructureAsync(int appl_pk, EventHandler<ClientEventArgs> callback);
        void LoadReportStructureListAsync(short keywordId, int appl_pk, EventHandler<ClientEventArgs> callback);
        #endregion
        #region Login
        void GetInstallationsByUserAsync(QueryRequest req, string naciId, string userName, EventHandler<ClientEventArgs> callback);
        void GetCountriesByHotelAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadUserByNameAsync(string userName, EventHandler<ClientEventArgs> callback);
        #endregion
        #region  Security

        void NewProfileAsync(EventHandler<ClientEventArgs> callback);
        void GetProfileAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void UpdateProfileAsync(ProfileContract profilecontract, EventHandler<ClientEventArgs> callback);
        void DeleteProfileAsync(Guid[] profileIds, EventHandler<ClientEventArgs> callback);
        void GetProfilePermissionsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadProfileAsync(Guid profileId, EventHandler<ClientEventArgs> callback);
        void GetUserPermissionsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void LoadApplicationsPermissionsAsync(long[] applicationIds, EventHandler<ClientEventArgs> callback);
        void GetLoginPeriodsAsync(QueryRequest req, Guid ownerId, EventHandler<ClientEventArgs> callback);
        void GetGroupPermissionsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetGroupsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetRolesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetPermissionDetailsByApplicationAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);


        #region Users
        void NewUserAsync(EventHandler<ClientEventArgs> callback);
        void GetUsersAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetAllUsersAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void UpdateUserAsync(UserContract usercontract, EventHandler<ClientEventArgs> callback);
        void LoadUserAsync(Guid userId, EventHandler<ClientEventArgs> callback);
        void SearchUserAsync(string login, string description, EventHandler<ClientEventArgs> callback);
        void InactiveUserAsync(Guid[] userIds, EventHandler<ClientEventArgs> callback);
        void DeleteUserAsync(Guid[] userIds, EventHandler<ClientEventArgs> callback);

        void GetApplicationAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetApplicationByUserAsync(QueryRequest req, Guid userId, EventHandler<ClientEventArgs> callback);
        void AddApplicationByUserAsync(long[] ids, UserContract userContract, EventHandler<ClientEventArgs> callback);
        void RemoveApplicationByUserAsync(Guid ownerId, Guid[] ids, EventHandler<ClientEventArgs> callback);
        #endregion

        #region Profile
        #endregion
        #endregion
        #region Payment method
        void GetInstallationCurrenciesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void GetPaymentMethodsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadPaymentMethodAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewPaymentMethodAsync(EventHandler<ClientEventArgs> callback);
        void UpdatePaymentMethodAsync(PaymentFormContract obj, EventHandler<ClientEventArgs> callback);
        void DeletePaymentMethodAsync(Guid id, EventHandler<ClientEventArgs> callback);
        #endregion
        #region Discount type
        void GetDiscountTypesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadDiscountTypeAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewDiscountTypeAsync(EventHandler<ClientEventArgs> callback);
        void UpdateDiscountTypeAsync(DiscountTypeContract obj, EventHandler<ClientEventArgs> callback);
        void DeleteDiscountTypeAsync(Guid id, EventHandler<ClientEventArgs> callback);
        #endregion
        #region License
        void GetLicenseInformationAsync(EventHandler<ClientEventArgs> callback);
        void GetLicensePaymentInformationAsync(EventHandler<ClientEventArgs> callback);
        #endregion
        #region Document serie
        void GetDocumentSeriesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        void LoadDocumentSerieAsync(Guid id, EventHandler<ClientEventArgs> callback);
        void NewDocumentSerieAsync(EventHandler<ClientEventArgs> callback);
        void UpdateDocumentSerieAsync(DocumentSerieContract obj, EventHandler<ClientEventArgs> callback);
        void GetDocumentTypesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        #endregion
        #region Market
        object GetMarketOriginsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        object GetMarketSegmentsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        #endregion
        #region Entity type
        object GetEntityTypesAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        #endregion
        #region ReservationLogs
        object GetReservationLogsAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        #endregion
        #region ReductionZ
        object SubmitReductionZAsync(ReductionZContract obj, EventHandler<ClientEventArgs> callback);
        object GetReductionsZAsync(QueryRequest req, EventHandler<ClientEventArgs> callback);
        #endregion
    }
}