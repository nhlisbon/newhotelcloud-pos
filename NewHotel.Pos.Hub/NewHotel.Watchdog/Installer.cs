﻿using System.ComponentModel;
using System.ServiceProcess;
using System.Configuration.Install;

namespace NewHotel.Watchdog
{
    [RunInstaller(true)]
    public class ProjectInstaller : Installer
    {
        private ServiceProcessInstaller process;
        private ServiceInstaller service;

        public ProjectInstaller()
        {
            process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.LocalSystem;
            service = new ServiceInstaller();
            service.ServiceName = WatchdogService.WatchdogServiceName;
            service.DisplayName = service.ServiceName.Replace('.', ' ');
            service.StartType = ServiceStartMode.Automatic;
            Installers.Add(process);
            Installers.Add(service);
        }
    }
}