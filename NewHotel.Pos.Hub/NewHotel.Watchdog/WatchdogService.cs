﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Configuration;
using System.Configuration.Install;
using System.Reflection;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using NewHotel.Watchdog.Properties;

namespace NewHotel.Watchdog
{
    public class WatchdogService : ServiceBase
    {
        private const string LogName = "Application";

        private CancellationTokenSource _watchdogCancellationTokenSource = null;
        private Task[] _watchdog = null;
        private EventLog _log = null;

        private static TimeSpan _watchdogCheckInterval = TimeSpan.FromSeconds(3);

        public static string WatchdogServiceName;

        static WatchdogService()
        {
            WatchdogServiceName = Path.GetFileNameWithoutExtension(Assembly.GetEntryAssembly().Location);
        }

        public WatchdogService()
        {
            CanPauseAndContinue = true;
            ServiceName = WatchdogServiceName;
        }

        private static void Delay(TimeSpan timespan, CancellationToken token)
        {
            try
            {
                Task.Delay(timespan, token).Wait();
            }
            catch (AggregateException ex)
            {
                try
                {
                    throw ex.InnerException;
                }
                catch (TaskCanceledException)
                {
                }
            }
        }

        private static string[] LoadFiles()
        {
            var files = new List<string>();
            foreach (SettingsProperty property in Settings.Default.Properties)
            {
                var propertyValue = Settings.Default[property.Name];
                if (propertyValue != null)
                {
                    Debug.Print("Watching {0}: {1}", property.Name, propertyValue);
                    files.Add(propertyValue.ToString());
                }
            }

            return files.ToArray();
        }

        private void Watch(object obj)
        {
            if (obj != null)
            {
                var path = obj.ToString();
                var token = _watchdogCancellationTokenSource.Token;

                do
                {
                    try
                    {
                        var proc = new Process
                        {
                            StartInfo = new ProcessStartInfo
                            {
                                FileName = path,
                                WorkingDirectory = Path.GetDirectoryName(path),
                                UseShellExecute = false,
                                RedirectStandardOutput = true,
                                RedirectStandardError = true,
                                CreateNoWindow = true
                            }
                        };

                        if (proc.Start())
                        {
                            Debug.Print("Started: {0}", proc.StartInfo.FileName);
                            while (!token.IsCancellationRequested)
                            {
                                if (proc.HasExited)
                                {
                                    Debug.Print("Exited: {0}", proc.StartInfo.FileName);
                                    break;
                                }
                                else
                                    Delay(_watchdogCheckInterval, token);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Print(ex.Message);
                        throw ex;
                    }
                }
                while (!token.IsCancellationRequested);
            }
        }

        public override EventLog EventLog
        {
            get
            {
                if (_log == null)
                {
                    _log = new EventLog(LogName);
                    if (!EventLog.SourceExists(WatchdogServiceName))
                        EventLog.CreateEventSource(WatchdogServiceName, LogName);
                    _log.Source = WatchdogServiceName;
                }

                return _log;
            }
        }

        protected override void OnStart(string[] args)
        {
            OnStop();

            try
            {
                var paths = LoadFiles();
                if (paths != null && paths.Length > 0)
                {
                    _watchdogCancellationTokenSource = new CancellationTokenSource();

                    var watchdog = new List<Task>();
                    for (int index = 0; index < paths.Length; index++)
                    {
                        var task = new Task(Watch, paths[index], _watchdogCancellationTokenSource.Token);
                        watchdog.Add(task);
                        task.Start();
                    }

                    _watchdog = watchdog.ToArray();
                }
                else
                    Debug.Print("Watch files not defined");
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
            }
        }

        protected override void OnStop()
        {
            try
            {
                if (_watchdog != null)
                {
                    if (_watchdogCancellationTokenSource != null)
                    {   
                        _watchdogCancellationTokenSource.Cancel();
                        Task.WaitAll(_watchdog);
                    }

                    try
                    {
                        for (int index = 0; index < _watchdog.Length; index++)
                        {
                            var task = _watchdog[index];
                            switch (task.Status)
                            {
                                case TaskStatus.RanToCompletion:
                                case TaskStatus.Canceled:
                                case TaskStatus.Faulted:
                                    if (task.IsFaulted)
                                        Debug.Print(task.Exception.Message);

                                    task.Dispose();
                                    break;
                            }
                        }
                    }
                    finally
                    {
                        _watchdog = null;
                        if (_watchdogCancellationTokenSource != null)
                        {
                            _watchdogCancellationTokenSource.Dispose();
                            _watchdogCancellationTokenSource = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
            }
        }

        private void ConsoleRun(string[] args)
        {
            OnStart(args);
            try
            {
                Console.WriteLine("Running. Any key to exit ...");
                Console.ReadKey(false);
            }
            finally
            {
                OnStop();
            }
        }

        private static void InstallService()
        {
            ManagedInstallerClass.InstallHelper(new string[] { Assembly.GetExecutingAssembly().Location });
        }

        private static void UninstallService()
        {
            ManagedInstallerClass.InstallHelper(new string[] { "/u", Assembly.GetExecutingAssembly().Location });
        }

        public static void Main(string[] args)
        {
            var debugMode = false;
            var sourceLevel = SourceLevels.Critical | SourceLevels.Error | SourceLevels.Warning;

            if (args.Length > 0)
            {
                switch (args[0].ToUpperInvariant())
                {
                    case "/I":
                        InstallService();
                        return;
                    case "/U":
                        UninstallService();
                        return;
                    case "/D":
                        debugMode = true;
                        sourceLevel = sourceLevel | SourceLevels.Information;
                        break;
                    default:
                        break;
                }
            }

            try
            {
                Debug.Listeners.Remove("Default");

                if (debugMode)
                {
                    var listener = new ConsoleTraceListener(true);
                    Trace.Listeners.Add(listener);

                    using (var host = new WatchdogService())
                    {
                        var arguments = new string[args.Length - 1];
                        Array.Copy(args, 1, arguments, 0, arguments.Length);

                        host.ConsoleRun(arguments);
                    }
                }
                else
                {
                    var service = new WatchdogService();
                    var listener = new EventLogTraceListener(service.EventLog);
                    Trace.Listeners.Add(listener);
                    ServiceBase.Run(service);
                }
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);

                if (debugMode) Console.ReadKey(true);
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (_watchdog != null)
                {
                    for (int index = 0; index < _watchdog.Length; index++)
                        _watchdog[index].Dispose();
                    _watchdog = null;
                }

                if (_watchdogCancellationTokenSource != null)
                {
                    _watchdogCancellationTokenSource.Dispose();
                    _watchdogCancellationTokenSource = null;
                }
            }
        }
    }
}