$currentDirectory = Get-Location
Write-Host "Current directory: $currentDirectory"
cd ..\..
New-Item -ItemType Directory -Path "publish\pos-cloud" -Force -ErrorAction SilentlyContinue

$publishPath = Resolve-Path -Path "publish\pos-cloud"
$outputPath = "$publishPath\build"
$sourcePath = "$publishPath\POS"
$logPath = "$publishPath\logs"

New-Item -ItemType Directory -Path $logPath -Force -ErrorAction SilentlyContinue

function BuildProject ($projectName, $destinationPath, $exeFile = "") {
	$projectPath = $projectName
	$logFile = "$logPath\$projectName.txt"
	
	if ($exeFile -eq "") {
		$exeFile = "$projectName.exe"
	}
	
	Write-Host "To build $projectName"
	$result = Invoke-MsBuild -Path "$projectPath\$projectName.csproj" -MsBuildParameters "/t:Rebuild /t:Restore /p:Configuration=Release;OutDir=$outputPath\$projectPath" -ShowBuildOutputInCurrentWindow -BuildLogDirectoryPath $logFile
		
	if ($result.BuildSucceeded) {
		Write-Host "$projectName Build succeeded. See build log in $logFile"
		xcopy /Y/-I "$outputPath\$projectPath\$exeFile.config" "$sourcePath\$destinationPath\$exeFile(template).config"
		xcopy /y/s "$outputPath\$projectPath\*.dll" "$sourcePath\$destinationPath\"
		xcopy /y/s "$outputPath\$projectPath\*.exe" "$sourcePath\$destinationPath\"
	} else {
		Write-Host "$projectName Build failed. See build log in $logFile for details."
	}
}

function RemovePath ($pathToRemove) {
	if (Test-Path -Path $pathToRemove -PathType Container) {
	  Remove-Item -Path $pathToRemove -Recurse -Force
	}
}

function RemoveFile ($fileToRemove) {
	if (Test-Path -Path $fileToRemove -PathType Leaf) {
	  Remove-Item $fileToRemove
	}
}

Write-Host "publishPath: $publishPath"
Write-Host "outputPath: $outputPath"
Write-Host "sourcePath: $outputPath"

cd $currentDirectory
Unblock-File -Path "$currentDirectory\Invoke-MsBuild.psm1"
Import-Module -Name "$currentDirectory\Invoke-MsBuild.psm1"
rm -r *\obj

RemovePath $outputPath
RemovePath $sourcePath

Write-Host "dotnet restore"
& dotnet restore

BuildProject "NewHotel.Pos.Spooler.View" "HUB"
BuildProject "NewHotel.Pos.Spooler.Settings" "HUB"
BuildProject "NewHotel.Pos.Spooler.Host" "HUB"
BuildProject "NewHotel.Pos.Spooler.Service" "HUB"
BuildProject "NewHotel.Pos.Hub" "HUB"
BuildProject "DrawerShell" "POS"
BuildProject "NewHotel.WPF.App.Pos" "POS" "NewHotel.Pos.exe"

$assemblyVers = [Reflection.AssemblyName]::GetAssemblyName("$sourcePath\HUB\NewHotel.Pos.Hub.exe").Version.ToString()
$versionName = "POS_$assemblyVers"
$versionPath = "$publishPath\$versionName"
$versionFile = "$publishPath\$versionName.zip"

Write-Host $sourcePath
Write-Host $versionName

RemoveFile $versionFile

Rename-Item -Path $sourcePath -NewName $versionName
Compress-Archive -Path $versionPath -DestinationPath $versionFile

RemovePath $versionPath
RemovePath $sourcePath

$newcurrentDirectory = Get-Location
Write-Host "Current directory: $newcurrentDirectory"

$key = Read-Host -Prompt "Press Enter to continue..."
