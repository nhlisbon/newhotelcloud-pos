﻿using System;
using System.Configuration;
using System.Windows.Forms;
using NewHotel.Pos.Notifications;
using NewHotel.Pos.Spooler.Common;

namespace NewHotel.Pos.Hub.Spooler
{
    public partial class Main : Form
    {
        #region Global Variables

        private string SignalRServer => ConfigurationManager.AppSettings["SignalRServer"];
        private static string HubName => ConfigurationManager.AppSettings["SignalRHubName"];
        private SignalRClientWrapper SignalRClient { get; set; }


        #endregion


        public Main()
        {
            InitializeComponent();

            #region Form Resize (Minimize, Maximized)
            Resize += (s, ev) =>
                {
                    if (WindowState == FormWindowState.Minimized)
                    {
                        notifyIcon.Visible = true;
                        notifyIcon.ShowBalloonTip(1000, "Handheld spooler", "Application will keep running in the backgroud", ToolTipIcon.Info);
                        ShowInTaskbar = false;
                    }

                    if (WindowState == FormWindowState.Normal)
                    {
                        notifyIcon.Visible = false;
                        ShowInTaskbar = true;
                    }
                };
            #endregion
            #region Notify Icon

            notifyIcon.MouseDoubleClick += (s, ev) =>
            {
                WindowState = FormWindowState.Normal;
            };

            #endregion
        }

        private void Main_Load(object sender, EventArgs e)
        {
            SignalRClient = new SignalRClientWrapper(HubName, SignalRServer);
            SignalRClient.Initialize();
            SignalRClient.Subscribe("SpoolerEvent", (string a, PrintArgs data) =>
            {
                txtDebug.Invoke((MethodInvoker)(() =>
                {
                    txtDebug.AppendText(data + Environment.NewLine);
                    if (txtDebug.Text.Length > 1000) txtDebug.Text = txtDebug.Text.Remove(0, 900);
                }));
            });
        }
    }
}
