﻿using System;
using NewHotel.Common;

namespace NewHotel.Business
{
    public sealed class NewHotelBusinessFactoryApp : BusinessFactory
    {

        #region Instance

        public static readonly BusinessFactory Instance = new NewHotelBusinessFactoryApp();

        #endregion

        #region Constructor

        private NewHotelBusinessFactoryApp()
        {
            Add<IAdminBusiness, AdminBusiness>();
        }

        #endregion
    }
}
