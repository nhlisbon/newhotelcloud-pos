﻿using System;
using System.Linq;
using System.Collections.Generic;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    public static class NewHotelPermissions
    {
        #region Members

        private static readonly IDictionary<Guid, GroupPermissions> _groupPermissions = new Dictionary<Guid, GroupPermissions>();
        private static readonly IDictionary<Guid, Permissions> _permissions = new Dictionary<Guid, Permissions>();

        #endregion
        #region Constructor

        static NewHotelPermissions()
        {
            #region Group Permissions

            _groupPermissions.Add("B420A53F31E4B641B0620F1DADC50AAF".ToGuid(), GroupPermissions.SettingsSecurity);

            #region PMS

            _groupPermissions.Add("C77F6B3CAA28C043B59BBD31629F7AF7".ToGuid(), GroupPermissions.Administration);

            _groupPermissions.Add("14C03D77292BEA45AEA6406A72C8CA3D".ToGuid(), GroupPermissions.SettingsLocalization);
            _groupPermissions.Add("9B69C8AE6274924A88218BAEFD8BF280".ToGuid(), GroupPermissions.SettingsMarket);
            _groupPermissions.Add("8DF22491C566FC40BEE045390981358E".ToGuid(), GroupPermissions.SettingsEntities);
            _groupPermissions.Add("87943E2584F3844F9A76D93CA8B310EA".ToGuid(), GroupPermissions.SettingsTaxes);
            _groupPermissions.Add("192336603213F04C8E613BFF3FFBD228".ToGuid(), GroupPermissions.SettingsDepartmentServices);
            _groupPermissions.Add("5FDC1BAEF4F5A3489FB0AF44BF96435B".ToGuid(), GroupPermissions.SettingsReservations);
            _groupPermissions.Add("86C1A930AC17EF41AAB36B2138D0F3AE".ToGuid(), GroupPermissions.SettingsDocuments);
            _groupPermissions.Add("9EA3DD39778FC54FA4A1E182D7486DCF".ToGuid(), GroupPermissions.SettingsRoomAndZones);
            _groupPermissions.Add("E59F5924A8A1224DB3A36DD9AB2E9F47".ToGuid(), GroupPermissions.SettingsPaymentsCurrenciesDiscounts);
            _groupPermissions.Add("332AEB4669E0E34C9472289936E6B6A0".ToGuid(), GroupPermissions.SettingsMaintenance);
            _groupPermissions.Add("2C019E640DFD734DB3BB2196E02EABAD".ToGuid(), GroupPermissions.SettingsTransportations);

            _groupPermissions.Add("8673D823CF277B4798B8E5C63322FC4A".ToGuid(), GroupPermissions.Transactions);
            _groupPermissions.Add("B5C82ED2F5E39A40A539712F9641651C".ToGuid(), GroupPermissions.TransactionsAccount);
            _groupPermissions.Add("5B34CEA0A7088F42A3BED59DE2463DC7".ToGuid(), GroupPermissions.TransactionsInvoice);
            _groupPermissions.Add("612AFDABDE79044FA1E8AF2A8B423DAE".ToGuid(), GroupPermissions.TransactionsCreditNoteNew);
            _groupPermissions.Add("860831FEDD0F274A8EEA13BCD17E2F36".ToGuid(), GroupPermissions.TransactionsInvoiceNew);
            _groupPermissions.Add("E2BAECBF1B7D2F43BE2DEB25A440ACF9".ToGuid(), GroupPermissions.TransactionsReceivableAccount);

            _groupPermissions.Add("A05A3146B423B14985D086657C67C955".ToGuid(), GroupPermissions.FrontOfficeReservations);
            _groupPermissions.Add("82BE902467E8A84FBF01192D5163729A".ToGuid(), GroupPermissions.FrontOfficeGroups);
            _groupPermissions.Add("690CFFE55DF3B245ACBCF9B864C204E6".ToGuid(), GroupPermissions.FrontOfficeGuests);
            _groupPermissions.Add("6FA5AC99DB6BF242880DFC6502D81CFC".ToGuid(), GroupPermissions.FrontOfficeReception);
            _groupPermissions.Add("0BE7C97871D5D94EA91F58ADEDD6624C".ToGuid(), GroupPermissions.FrontOfficeRooms);
            _groupPermissions.Add("FE58021DCC67F141BDA99E84E362540F".ToGuid(), GroupPermissions.FrontOfficePlanning);
            _groupPermissions.Add("EAC88C514B80214BAEF179ECD15E364D".ToGuid(), GroupPermissions.FrontOfficeNightAuditor);

            _groupPermissions.Add("DD5F718DDA28490685EFA1DA1C828D05".ToGuid(), GroupPermissions.CreditCard);

            _groupPermissions.Add("6FA5026456C0B440AEE60D8F86371F28".ToGuid(), GroupPermissions.Rates);

            _groupPermissions.Add("FD585507A5E06744A4B35F135ECD36C4".ToGuid(), GroupPermissions.Housekeeping);

            _groupPermissions.Add("89DB6AD7FA46214CBECA63DA705CD238".ToGuid(), GroupPermissions.Reports);

            #endregion

            #region POS

            _groupPermissions.Add("31FE35CE06A01D449DB812AD59F5E37D".ToGuid(), GroupPermissions.Management);

            _groupPermissions.Add("677550BE7F75294FBEC5163283A5A3A3".ToGuid(), GroupPermissions.SettingsPOS);
            _groupPermissions.Add("084ABC8998A038489C8690E09A0636AE".ToGuid(), GroupPermissions.SettingsPointofSales);
            _groupPermissions.Add("A5AB1D574BEFC04694F8AE1B0C1B853A".ToGuid(), GroupPermissions.SettingsProducts);

            _groupPermissions.Add("4D58330506CFA6448759FE8501CCD6BC".ToGuid(), GroupPermissions.Tickets);

            #endregion

            #region SPA

            _groupPermissions.Add("322522EBAC4D3B47A32A92C4C5B01207".ToGuid(), GroupPermissions.SettingsResources);
            _groupPermissions.Add("5C5E9D2F888CE04291650C87BD53030F".ToGuid(), GroupPermissions.SettingsTherapists);

            _groupPermissions.Add("7F738ED77181A94C9F6F15AFFE416FAC".ToGuid(), GroupPermissions.FrontOfficeSPAReservations);
            _groupPermissions.Add("E9B4C3318063444FA5D88DE44996C243".ToGuid(), GroupPermissions.FrontOfficePackageReservations);
            _groupPermissions.Add("7A16AE980CD88D4BB1D491C909AEC700".ToGuid(), GroupPermissions.FrontOfficeSPAPlannings);

            #endregion

            #region EVENTS

            _groupPermissions.Add("9E58D0420DC04ABFAC6F273B0C31AD0B".ToGuid(), GroupPermissions.EventsSettings);
            _groupPermissions.Add("E4BF6738D15340AAAD57E1B62CD3134A".ToGuid(), GroupPermissions.EventsFrontOffice);
            _groupPermissions.Add("C0AB61B182334CAB8DEB3121D18FEF99".ToGuid(), GroupPermissions.EventsFrontOfficeStaff);
            _groupPermissions.Add("D1CF1EADED90404BB5C4F8814F24138B".ToGuid(), GroupPermissions.EventsSettingsLocations);
            _groupPermissions.Add("06752F2F368F4CACB61579F31C56FDF7".ToGuid(), GroupPermissions.EventsSettingsCategories);
            _groupPermissions.Add("AC0E5DBDA686447886F8AE7BE9BAF1FE".ToGuid(), GroupPermissions.EventsSettingsTermsAndConditions);
            _groupPermissions.Add("3A995E92B3F94645BB94D8C0D62C70BE".ToGuid(), GroupPermissions.EventsDocuments);

            #endregion

            #endregion

            #region Permissions

            #region PMS

            _permissions.Add("93810F290DA5A74EB50739BB798F8064".ToGuid(), Permissions.Settings);
            _permissions.Add("EA4FD9C865BF7347BBEB0697AC38A421".ToGuid(), Permissions.ControlAccess);
            _permissions.Add("080A24A994809245B6C76C5CCE65029F".ToGuid(), Permissions.Profiles);
            _permissions.Add("B177FD9E4B884D408CFB731992A277D2".ToGuid(), Permissions.EventViewer);
            _permissions.Add("E0A0190C0034CA4587CBE5A46FACD576".ToGuid(), Permissions.Languajes);
            _permissions.Add("21253B598D10C149B611F228DD677D44".ToGuid(), Permissions.WorldRegions);
            _permissions.Add("BBBD530B5373504B9565A30850A03414".ToGuid(), Permissions.Countries);
            _permissions.Add("7B4FBE553A51D04293D1744C10A61CD1".ToGuid(), Permissions.BorderPoints);
            _permissions.Add("B97D34521DCEF94FA89A29F01F128286".ToGuid(), Permissions.Holidays);
            _permissions.Add("695DAF84CCEABD42B30546029253C99B".ToGuid(), Permissions.CountryZones);
            _permissions.Add("E2A61C795AAB0C4D972405BD0D5C7701".ToGuid(), Permissions.CountryRegions);
            _permissions.Add("CBF9AFB9AC8A8F4AA4E01C027EAFD2B1".ToGuid(), Permissions.PostalCodes);

            _permissions.Add("B0D579B82EC7B046AF7F29895E5B18D8".ToGuid(), Permissions.SourcesGroups);
            _permissions.Add("000A2B6B62AF1945BA3766C7CF841D66".ToGuid(), Permissions.Sources);
            _permissions.Add("E7B74CDBB822064DA48BA6B279B06A74".ToGuid(), Permissions.ServiceGroups);
            _permissions.Add("1DAFCB4F2777D84E9F1F9F3B86AE7845".ToGuid(), Permissions.Segments);
            _permissions.Add("4B72FA6D9451174A9D788E0481332D65".ToGuid(), Permissions.SourcesSegments);

            _permissions.Add("80713905D43DD241B2B691D7C9077B5A".ToGuid(), Permissions.Companies);
            _permissions.Add("22929DAA27882A418E45590012CA7547".ToGuid(), Permissions.CompanyTypes);
            _permissions.Add("CE67425C4C8FB342AF7337B47DCECD9A".ToGuid(), Permissions.Banks);
            _permissions.Add("41B05BC32621864FBF9AD4A5A4F920CE".ToGuid(), Permissions.Clients);
            _permissions.Add("D386AD93D4A345D0B76644CFE66B081E".ToGuid(), Permissions.EditFiscalNumber);

            _permissions.Add("749969C8BB54D14A9B130F6329B4CE28".ToGuid(), Permissions.ClientTypes);
            _permissions.Add("3E49CBA1A52800429A4F00AA7182AC99".ToGuid(), Permissions.GuestCategories);
            _permissions.Add("6C7B93AA1A81834C960CDBCB500BCF17".ToGuid(), Permissions.ClientAttentions);
            _permissions.Add("B69B13806DC83A4A91EF41613874F99B".ToGuid(), Permissions.Preferences);
            _permissions.Add("E8D7D183F281224E9321B0755E2E93AE".ToGuid(), Permissions.Relatives);
            _permissions.Add("9B3603CF92D57D468F180E083D92412D".ToGuid(), Permissions.Professions);

            _permissions.Add("DA6C1145A04CDF49830CDBA555A6DA54".ToGuid(), Permissions.JobFunctions);
            _permissions.Add("EA6A207072639B4AB083B2E49497DA5C".ToGuid(), Permissions.Contacts);
            _permissions.Add("084AEC4705B3584F882BEA2DA7ADB3B5".ToGuid(), Permissions.SalesPeople);

            _permissions.Add("2CAC51BF1A8C3541B99BD1844D7309A6".ToGuid(), Permissions.Titles);
            _permissions.Add("C189E143973EA64092596B1D99FEB98C".ToGuid(), Permissions.Employee);
            _permissions.Add("4719DE2EDF388547BA576CC4B0DAE47B".ToGuid(), Permissions.EmployeeRoles);
            _permissions.Add("E4254D524BB6D941A31BE662BB5B9A13".ToGuid(), Permissions.ReceivablePayableAccounts);

            _permissions.Add("EABADC32F4B3A247B197049B23D38C39".ToGuid(), Permissions.TaxSchemes);
            _permissions.Add("44081E486461C84397BC19B8F766CF71".ToGuid(), Permissions.TaxRegions);
            _permissions.Add("3804FB18420ED14BB06A0213DB678B27".ToGuid(), Permissions.TaxSequences);
            _permissions.Add("627D5C6CF56855419B8BA7BDC13C67C9".ToGuid(), Permissions.TaxRates);
            _permissions.Add("F7ECC8A4CACE5549A02386110205CAC8".ToGuid(), Permissions.Departments);
            _permissions.Add("83BA3A1A4A18A94EB7E3CA9523424667".ToGuid(), Permissions.Services);
            _permissions.Add("2AB120F8B7BDC044B5CC54E221001E07".ToGuid(), Permissions.ServiceGroups);

            _permissions.Add("09707C9EE2D8E64CACEA7F624031E5E2".ToGuid(), Permissions.ServiceCategories);
            _permissions.Add("7B95E0F9B498B74C847C17F6228C667E".ToGuid(), Permissions.Tips);
            _permissions.Add("06CD97E6B2DFAD4DBFCA3BF8B32C21D8".ToGuid(), Permissions.Packages);
            _permissions.Add("55895CCCEA62574EA1ECC74D43921A6F".ToGuid(), Permissions.Resources);
            _permissions.Add("0F05EC2868754441A267F719349D94D2".ToGuid(), Permissions.ResourceTypes);
            _permissions.Add("644B38278BE80E4B8CA4510B46E5972E".ToGuid(), Permissions.ResourceCharacteristics);

            _permissions.Add("83F336333EC26E43BF78B370EA59522C".ToGuid(), Permissions.ResourceInactivities);
            _permissions.Add("0897DDF9366EC04293985C0B9BA32F79".ToGuid(), Permissions.Additionals);
            _permissions.Add("46AC3F227B2EF04FA94512FE1C9D9DCC".ToGuid(), Permissions.AdditionalsTypes);
            _permissions.Add("932BA8828979D14482D3DFE228ECC047".ToGuid(), Permissions.StopSales);

            _permissions.Add("820BC4C28A5167459005D4C912A4606F".ToGuid(), Permissions.Therapists);
            _permissions.Add("C3306CD05F22D940BB93112BC897D8CB".ToGuid(), Permissions.TherapistProfiles);
            _permissions.Add("662817EB98B59645AF9EBA1B25FFA4BD".ToGuid(), Permissions.WorkShiftTypes);
            _permissions.Add("A14FBEF52A81D84FB2BA4978EF230D68".ToGuid(), Permissions.WorkShiftsInactivities);

            _permissions.Add("44485A2384BAE14E8151CAB2ED4FD3E2".ToGuid(), Permissions.PaymentImages);
            _permissions.Add("3705DA9389B65F49968FC68C86CE9176".ToGuid(), Permissions.Duties);
            _permissions.Add("19E8B973A285844C8D805EF3E030AF51".ToGuid(), Permissions.GroupReservationTypes);
            _permissions.Add("50FE333C966D5C46AB02E44BEAC30D2F".ToGuid(), Permissions.GuaranteeTypes);
            _permissions.Add("BD0A039B3CFB5A4080F8A53747800D0F".ToGuid(), Permissions.CancellationReasons);
            _permissions.Add("181037051477204287877B392A055BB6".ToGuid(), Permissions.UnexpectedDepartureTypes);
            _permissions.Add("A0F2F73744D5C7489AAEBA951CA4079E".ToGuid(), Permissions.MealPlan);
            _permissions.Add("349A333B6FE7F94E88C19B96C0CB23EC".ToGuid(), Permissions.TicketConfiguration);

            _permissions.Add("5A46616832D77247954B1AB929FBE88A".ToGuid(), Permissions.Extensions);
            _permissions.Add("439C390B3176A0468E55A201E0BB9A16".ToGuid(), Permissions.PhoneCalls);
            _permissions.Add("357DD3BD6FF3824D8A50AD69BBD4C7CD".ToGuid(), Permissions.NightAuditorConfiguration);
            _permissions.Add("1ADA5D46AFF6EB4DAE9E08D180EA68ED".ToGuid(), Permissions.AgeRanges);
            _permissions.Add("779039A92D93C748AD3568BFEEC27340".ToGuid(), Permissions.DocumentSeries);
            _permissions.Add("0D8E617706DF334086F08B4DE725E754".ToGuid(), Permissions.Rooms);
            _permissions.Add("1BD27496AD115F4A8CE313F5A084A3CB".ToGuid(), Permissions.RoomTypes);
            _permissions.Add("E4D001FDE98A82419B1D43A1E4D5EB4E".ToGuid(), Permissions.RoomBlocks);
            _permissions.Add("270937E1A768174994E785328B9C5A4F".ToGuid(), Permissions.RoomCaracteristics);
            _permissions.Add("D811F60A0F487740BE4D621D9C1BE1A2".ToGuid(), Permissions.ExtraResources);

            _permissions.Add("954BC6F366513B4AABE16D8FB9864F76".ToGuid(), Permissions.ExtraResourcesTypes);
            _permissions.Add("0F06C7CBB1347247B4820B3977E4C0B4".ToGuid(), Permissions.AreaTypes);
            _permissions.Add("D5324C301A226B4FA51AB25F183CD35C".ToGuid(), Permissions.PublicAreas);
            _permissions.Add("5443824B06A11D43AD2AD3BEAD557EB1".ToGuid(), Permissions.Currencies);
            _permissions.Add("E9969C3920FF864493F10388B0AFFEF1".ToGuid(), Permissions.ExchangeRates);

            _permissions.Add("8051E7B16BEA10429F035199FDECE7D2".ToGuid(), Permissions.PaymentTypes);
            _permissions.Add("559C1C7939C9964582038E7EAECE99D9".ToGuid(), Permissions.CreditCardsTypes);
            _permissions.Add("0DDB6B921787C94E9340461D64F84FF3".ToGuid(), Permissions.DiscountTypes);
            _permissions.Add("20FE2DA4792FDB43AAD20354EB92FBEA".ToGuid(), Permissions.PaymentsMethodsCredit);
            _permissions.Add("CE9634E95B02544580AC0322D249AA68".ToGuid(), Permissions.PaymentsMethodsHouseUse);
            _permissions.Add("523210E8B3F2B245B744A27BC8A1BDAB".ToGuid(), Permissions.PaymentsMethodsMealPlan);

            _permissions.Add("D63EB79C9C77F5469B2623AF9FD014E7".ToGuid(), Permissions.MalfunctionTypes);
            _permissions.Add("73D1A146C14C4B47BC85B9C7135C0F0F".ToGuid(), Permissions.MalfunctionSource);
            _permissions.Add("AD4F04DC78342F46A05C0DF1EAC11C47".ToGuid(), Permissions.OutofOrderTypes);
            _permissions.Add("8FA536485BE27445AC422139F0D63763".ToGuid(), Permissions.Flights);
            _permissions.Add("E4D9C18FC50C564393484F284D5E718C".ToGuid(), Permissions.Vehicles);
            _permissions.Add("A25AA78A66D0384A981CCF8326A025CF".ToGuid(), Permissions.VehiclesRegistration);
            _permissions.Add("2C494CBBE4DB344D8324D0223D12E828".ToGuid(), Permissions.ParkingZones);
            _permissions.Add("78ABC7200D933B4CAB39C2D1848223B0".ToGuid(), Permissions.Messages);
            _permissions.Add("765853D22479074784123705FF9DA4AA".ToGuid(), Permissions.WakeUp);
            _permissions.Add("D56A8D57DD330344891C4C2D04140B6D".ToGuid(), Permissions.LostandFound);
            _permissions.Add("69F8E1BAE09F664782AE77985C84795D".ToGuid(), Permissions.Workshift);

            _permissions.Add("8BDDC6F0FAE2AF40A4DD1F73FD9C0B87".ToGuid(), Permissions.ParkingLocations);
            _permissions.Add("F20E3E7EFD43454DB71669BF546CDC1F".ToGuid(), Permissions.Reports);
            _permissions.Add("A0C65170B1941F4389502ABA910173BE".ToGuid(), Permissions.Rates);
            _permissions.Add("7EF2048C1EDB87429F8711FFA00DCC20".ToGuid(), Permissions.Contracts);
            _permissions.Add("B38C956F3563604CADF2C16B9298A7A8".ToGuid(), Permissions.RateBreakdown);
            _permissions.Add("6C1583FC3DD1644C85D3F32C5B0FF0F7".ToGuid(), Permissions.RatePrices);
            _permissions.Add("4C10828981963E4DB59C0544D56853FB".ToGuid(), Permissions.StopReservations);

            _permissions.Add("823D3F31B0873C47BA8953A0D68BCE60".ToGuid(), Permissions.Allotments);
            _permissions.Add("3CEC438FD35BF64CA58C3CA0F5D06DF2".ToGuid(), Permissions.AllotmentsExtraBeds);

            _permissions.Add("910F996C915FAC4F85C2AEA75CC04BE2".ToGuid(), Permissions.Transactions);
            _permissions.Add("D7F514C2339C2C4284BCE6A632B1C405".ToGuid(), Permissions.TransactionRoomCharges);
            _permissions.Add("6A0D2972A1AA4C42936CF2D99218D6B7".ToGuid(), Permissions.TransactionDeposit);
            _permissions.Add("8670897EF69B5842809153D076EB246D".ToGuid(), Permissions.TransactionSplit);
            _permissions.Add("488BA9DFCA87494081D17DF469FDD4B4".ToGuid(), Permissions.TransactionTransfer);
            _permissions.Add("38EAE0AFE730C0468D5F7AD2DE275ADC".ToGuid(), Permissions.Folios);
            _permissions.Add("EA8EE1C603D5884BA3868FC923F209AE".ToGuid(), Permissions.ControlAccount);

            _permissions.Add("29A4CFAF8E06A448B03F4DB0A7E236A4".ToGuid(), Permissions.Invoices);
            _permissions.Add("0C26E851CA5C454981E3DF8BCDE2C58D".ToGuid(), Permissions.InvoicesCreate);
            _permissions.Add("8C1F6007A965F6489381061851FD54C7".ToGuid(), Permissions.InvoicesCopies);
            _permissions.Add("512A9C4F4BEA0749AF2989461B4B4C21".ToGuid(), Permissions.AdvancedInvoices);
            _permissions.Add("38114B421FBC5241B9ECAD7334A81638".ToGuid(), Permissions.CreditNotes);
            _permissions.Add("17E56E11B5D0A14FBCBF4B0F3C33E7B4".ToGuid(), Permissions.CreditNotesCreate);
            _permissions.Add("307A498BFDAA444D894EBE73F896B7CB".ToGuid(), Permissions.CreditNotesCopies);
            _permissions.Add("BBF13214B5FAB746B5EA4273566E631C".ToGuid(), Permissions.PrintCopiesOriginalDocuments);
            _permissions.Add("14AAA0B67A84EF499280CD87DE3EED76".ToGuid(), Permissions.AllowTransferExternalAccount);

            _permissions.Add("BBC0F519349BC34C81034B1645C57026".ToGuid(), Permissions.Receipts);
            _permissions.Add("C74CE1F3B152CC479C49DC417B4C8508".ToGuid(), Permissions.ReceiptsCopies);
            _permissions.Add("416C49731E867747A89EEC562BB457C1".ToGuid(), Permissions.ReceivableAccount);
            _permissions.Add("6E2BA5A0EDCE6E4A937A66ABA3C46E9F".ToGuid(), Permissions.GuestReservations);
            _permissions.Add("D670044CC69D364CB3D8A5863F117FB5".ToGuid(), Permissions.ReservationCheckIn);
            _permissions.Add("183D4DAE5244EE4BAB2F92DB57E40BAF".ToGuid(), Permissions.ReservationCheckOut);
            _permissions.Add("38E0B4DE9494374EBBA4BDC2B5B42676".ToGuid(), Permissions.ReservationNoShow);
            _permissions.Add("7643578CCE053E4F8FCA5F786A08FDED".ToGuid(), Permissions.ReservationOverbooking);
            _permissions.Add("0B8091BD8A824E99B68C99C8321A0BE8".ToGuid(), Permissions.ReservationBulkUpdatePrice);
            _permissions.Add("807A692874C29B44B4AB38FC20E83EFB".ToGuid(), Permissions.MandatoryRatePriceReservations);
            _permissions.Add("7654721428F697428339018EC6A5EB87".ToGuid(), Permissions.LockRoomsReservations);
            _permissions.Add("6174B3AFA806EF4EA88E7A976BC79E5D".ToGuid(), Permissions.ReservationCopies);
            _permissions.Add("7495DCA37F51974F8CDB3997FC75897A".ToGuid(), Permissions.DiscountsReservations);

            _permissions.Add("A29F3C4A24779A4E935755086CAA2A58".ToGuid(), Permissions.OpenCurrentAccounts);
            _permissions.Add("784BA867CBF99C4D9FE53020B3CC9716".ToGuid(), Permissions.CloseCurrentAccounts);
            _permissions.Add("6F477C056DCD234EBD956FC247C950A9".ToGuid(), Permissions.CreditLimit);
            _permissions.Add("37314F0D27D4544C97D617908B1D06DD".ToGuid(), Permissions.CreditAuthorization);

            _permissions.Add("FC25D93282814ABDAB6DAF94C8ED57D0".ToGuid(), Permissions.Dashboard);
            _permissions.Add("4193E4BC2D7BDE408729B38C1BA3D102".ToGuid(), Permissions.Plannings);
            _permissions.Add("D494A131856D9C4FBFA2BF8143810F23".ToGuid(), Permissions.RoomPlanning);

            _permissions.Add("77474F3BA0745440BAEC2702A805FDAE".ToGuid(), Permissions.GroupReservations);

            _permissions.Add("EB2CACDC20C5634A93CB3ACAC2818455".ToGuid(), Permissions.RoomAllocation);
            _permissions.Add("8381C79AB9E0A04FA5F112672EE5192D".ToGuid(), Permissions.RoomingList);
            _permissions.Add("66F3B92628AB154C8F5EF0BF37F6AF54".ToGuid(), Permissions.RoomStatus);
            _permissions.Add("C8BEF5FD36728A4CB64E456F8385F271".ToGuid(), Permissions.RoomDiscrepancies);
            _permissions.Add("3A1E6F5128BC664B81A2318CE052B0F7".ToGuid(), Permissions.RoomsOutofOrder);
            _permissions.Add("F3E6E514BE5DB442885C647D3CF67BC6".ToGuid(), Permissions.LockRooms);

            _permissions.Add("34625DA38CAEFC4BA2306DF2BF10FFEB".ToGuid(), Permissions.Guests);
            _permissions.Add("01444045F8797C46A6A9DFBB20243B94".ToGuid(), Permissions.GuestCheckIn);
            _permissions.Add("24C02D2430BB45419946FC6A2C5C9E58".ToGuid(), Permissions.GuestCheckOut);
            _permissions.Add("835C7CED14C35644A858BCA36F580D77".ToGuid(), Permissions.GuestArrivalDepartureDate);

            _permissions.Add("72205C0735EE654C853D429C996C4D1E".ToGuid(), Permissions.ExecuteNightAudit);
            _permissions.Add("C647B0E10A462B4E9288FC8CC8FD0FAC".ToGuid(), Permissions.ExpectedIncomes);

            _permissions.Add("FB88FA103532FD458040441F7FA7AB0F".ToGuid(), Permissions.ArrivalDepartureControl);
            _permissions.Add("D33F5BF7808F2943A9104579A9CC52A9".ToGuid(), Permissions.NightAuditStatistics);
            _permissions.Add("960B3BAAD7DB914FBCAC75F3ACA180C1".ToGuid(), Permissions.ControlReports);
            _permissions.Add("96D28D384569844EA80DF95A74C07A15".ToGuid(), Permissions.FinalReports);
            _permissions.Add("1E65E7093C2005459ECE2009EEDE1841".ToGuid(), Permissions.ConfirmEndofDay);

            #endregion

            #region POS

            _permissions.Add("34972FA8198CBA458F341D9622037A2D".ToGuid(), Permissions.Products);
            _permissions.Add("54F7B8E76E771744AB0560C4030BA673".ToGuid(), Permissions.ProductsGroups);
            _permissions.Add("22BE8081643F764DBA4533826A976A9D".ToGuid(), Permissions.ProductsFamily);
            _permissions.Add("BFF57E4365EAD546B9B0180D48AAECD1".ToGuid(), Permissions.ProductsSubfamily);
            _permissions.Add("784F5BBF77F144A2969A77576ACBD74D".ToGuid(), Permissions.ProductsMenu);
            _permissions.Add("489C8485841A409E9AA9BC0F64DC647C".ToGuid(), Permissions.ProductsPreparations);
            _permissions.Add("465C9A4B38667944A7E85B8915345A26".ToGuid(), Permissions.ProductsSeparators);
            _permissions.Add("ED39D5E9106C154C8321260E525CB9CE".ToGuid(), Permissions.PointSales);
            _permissions.Add("ED39F4C00F086D4AA0F3B36877209016".ToGuid(), Permissions.Cashiers);
            _permissions.Add("3A6F2C56D6391E4CBB2D62CD21F5E130".ToGuid(), Permissions.Salons);
            _permissions.Add("5439565F0D033A4FB1E9DE674F63C293".ToGuid(), Permissions.DispatchAreas);
            _permissions.Add("C6F091E2EEE7BD49810DB4EA85515BEA".ToGuid(), Permissions.PosServices);

            _permissions.Add("CCD84C81F0079F46A959CC3223118090".ToGuid(), Permissions.HappyHours);
            _permissions.Add("5C90D19EB78D7F4098801D513DB39BFA".ToGuid(), Permissions.InternalConsumptions);
            _permissions.Add("56758585844445EBBE2A8A8DFF943392".ToGuid(), Permissions.Details);
            _permissions.Add("277D0AE8A439AD4B95407AEAB9CA0A3D".ToGuid(), Permissions.Tickets);
            _permissions.Add("2CA11A0C35F1BC45A68833370A3DF9B9".ToGuid(), Permissions.Logs);

            _permissions.Add("2DB3266C05CD5F42872FACACCAD51CB1".ToGuid(), Permissions.TicketsCancelWithInvoice);
            _permissions.Add("8AF7A66F29084FF6AD0BB06188AE6ADC".ToGuid(), Permissions.TicketsCancelWithoutInvoice);
            _permissions.Add("525998ECCC12634A85A9AFF68D82F467".ToGuid(), Permissions.TicketsMerge);
            _permissions.Add("E7960FD32AB5E74E9736725DAF0493A0".ToGuid(), Permissions.TicketsTransfer);
            _permissions.Add("6224CF0D67A7A54FB7D6439BF56BA2ED".ToGuid(), Permissions.TicketsPrintPaymentInstructions);
            _permissions.Add("8F704D62D1FEA44C91CD1317B27E7E6E".ToGuid(), Permissions.TicketsDiscounts);
            _permissions.Add("3F58A517C5009D4A806F462C52C53683".ToGuid(), Permissions.TicketsTips);
            _permissions.Add("03C1510A9B30454A9FE450D2C82F2ED3".ToGuid(), Permissions.TicketWithoutTable);
            _permissions.Add("4B82EE430FA1BF4A94C2D43D08FB537F".ToGuid(), Permissions.AccessAllProductsList);
            _permissions.Add("F7DFDFB31BC6CE4CA91867E83FBF2958".ToGuid(), Permissions.SendProductsToDispatchAreas);
            _permissions.Add("4F18E87E5085D14DAAE6AE646FBCCF42".ToGuid(), Permissions.CancelProductLine);
            _permissions.Add("5B52053F7B9B4876AE444DC74392BDD1".ToGuid(), Permissions.CloseTickets);
            _permissions.Add("1023E1B7A2DD43588EFDDAD0A1E9AFB3".ToGuid(), Permissions.EnableRoomCharge);
            _permissions.Add("29A6D1164AF44B74B7E6F3372A9E27B4".ToGuid(), Permissions.EnableMealPlan);
            _permissions.Add("833DBB82BCE943399FC20A3FA333F657".ToGuid(), Permissions.EnableHouseUse);
            _permissions.Add("09B1C93E20AD402CBB1E502AC5EFBCE0".ToGuid(), Permissions.EnableCashPayment);

            _permissions.Add("B256841EB89EF44FABD4CFD71B385D96".ToGuid(), Permissions.ClosedTickets);
            _permissions.Add("A754BDAFD89C374BB95BC77BBC673E00".ToGuid(), Permissions.Areas);
            _permissions.Add("66DBC781FBB2A3428C4E6A44E38653E9".ToGuid(), Permissions.PrinterDefinition);
            _permissions.Add("F436D3DB8E9CBC4C94B1EDC415A0E829".ToGuid(), Permissions.Users);

            _permissions.Add("78E7C451D0481E46B53A912BD7FA05E7".ToGuid(), Permissions.CloseDay);
            _permissions.Add("293D0C48B1849844A1E5DEA3F72834AD".ToGuid(), Permissions.CloseShift);
            _permissions.Add("BAB022F6DD2D134BB6B8DAE754DDDCF2".ToGuid(), Permissions.Upload);
            _permissions.Add("EC8ABF3BF483264FB4C7C5B6C46A3995".ToGuid(), Permissions.DownloadGeneralSettings);
            _permissions.Add("95E45A2DB613E049AB6CFA09627F4C32".ToGuid(), Permissions.DownloadProducts);
            _permissions.Add("0EF8EC4D5CAE3A4B9F2DF902BDF69CCF".ToGuid(), Permissions.DownloadPricesHappyHours);
            _permissions.Add("84FFE38ECECAB54C944678FDB7E36649".ToGuid(), Permissions.DownloadSaloonTables);
            _permissions.Add("0D847926234E3443AFA45E234AE70909".ToGuid(), Permissions.CashDrawer);
            _permissions.Add("BA16D6921104674D967F7504A3A740A8".ToGuid(), Permissions.ChangeOfStand);

            _permissions.Add("11E81706728111EE93E805C92045802B".ToGuid(), Permissions.VoidProductsToDispatchArea);
            

            #endregion

            #region SPA

            _permissions.Add("AD322C77311B854CA76B1A5F20D10D68".ToGuid(), Permissions.SPAReservations);
            _permissions.Add("5C21AE53FDE4154F880B6EB57083A1B7".ToGuid(), Permissions.SpaReservationCheckIn);
            _permissions.Add("D44AD18C9FA88D429B82F5D7E3BA0DBB".ToGuid(), Permissions.SpaReservationCheckOut);
            _permissions.Add("47E6C4F12AC7EA4DBB283406733D8EB4".ToGuid(), Permissions.SpaReservationNoShow);
            _permissions.Add("BE5A78D5E25FFD4588B2D8514D272BD1".ToGuid(), Permissions.SpaReservationOverbooking);
            _permissions.Add("1A9711AE5BD33F44A6A1FDE9CAFC8C90".ToGuid(), Permissions.SPAPlannings);
            _permissions.Add("A50E358208E0E640B0CBB95CE5F65FA7".ToGuid(), Permissions.ClientSchedules);
            _permissions.Add("B9F548D72B259744B7F9A418EB5F8B82".ToGuid(), Permissions.PackageReservations);

            #endregion

            #region EVENTS

            _permissions.Add("E4574541E4B43143BAF5E8EAE5737E4A".ToGuid(), Permissions.EventsNewEvent);
            _permissions.Add("F318F69A9D675C40A1E5BA3A1DD99842".ToGuid(), Permissions.ViewEvent);
            _permissions.Add("B54D4B9237CACE43901B5F53DABB5C9F".ToGuid(), Permissions.CheckIn);
            _permissions.Add("FCBA6DBFBECD9948A22BF6AE99C7DF13".ToGuid(), Permissions.CheckOut);
            _permissions.Add("783DBC76287B06449B97BA25DF6803E2".ToGuid(), Permissions.Cancel);
            _permissions.Add("9D93525B72100C4882D184649483B929".ToGuid(), Permissions.Copy);
            _permissions.Add("56181089FA7DC545BF8FDDE4119C78F1".ToGuid(), Permissions.UndoCheckIn);
            _permissions.Add("D0AB2B1FBDFD40449AB33FA2B03EB4C4".ToGuid(), Permissions.UndoCheckOut);
            _permissions.Add("0F2AC0C14F29ED4095F8C3EA9D9D6158".ToGuid(), Permissions.ConfirmationState);
            _permissions.Add("264E52BF54664949B559A4FBCBD70E51".ToGuid(), Permissions.Contract);
            _permissions.Add("0708914D62AB924CB9A8FBD165FE9F86".ToGuid(), Permissions.EventOrder);
            _permissions.Add("52B704E42792174AB7C0C85B19946251".ToGuid(), Permissions.Schedule);
            _permissions.Add("7631DD5F84C6824EBF6FD6E1A6A799AA".ToGuid(), Permissions.StaffManager);
            _permissions.Add("1C9BBE08C882774DA1ED2FA52386B748".ToGuid(), Permissions.WorkSchedule);
            _permissions.Add("659B1B0CA6220346906CB8CB753ABCA6".ToGuid(), Permissions.SiteLocations);
            _permissions.Add("78F596B41B02044190B73939BFC4C599".ToGuid(), Permissions.LocationTypes);
            _permissions.Add("4175DB4CA1FE9941BC1622988D889610".ToGuid(), Permissions.LocationAttributes);
            _permissions.Add("735005559125834890A86C5BFABF3E58".ToGuid(), Permissions.SetupStyles);
            _permissions.Add("D5AC360FE3FAED49BFB624DA20C18029".ToGuid(), Permissions.EventsItems);
            _permissions.Add("C806326C67B534448352741CA957EAE5".ToGuid(), Permissions.ItemTypes);
            _permissions.Add("7573D0C121725F4D84FF253A1F04F7C0".ToGuid(), Permissions.EventCategories);
            _permissions.Add("E79A341B17EDA14989072B7D7EF6800B".ToGuid(), Permissions.EventStatus);
            _permissions.Add("90BF07AD46254E488DA13827834296B5".ToGuid(), Permissions.EventResponsabilities);
            _permissions.Add("74A8F4DAEDF719419B218378DCC3F9A8".ToGuid(), Permissions.TermsAndConditions);

            #endregion

            #endregion
        }

        #endregion
        #region Public Methods

        public static GroupPermissions GetGroupPermissionsEnum(Guid id)
        {
            GroupPermissions groupPermissions;
            if (!_groupPermissions.TryGetValue(id, out groupPermissions))
                groupPermissions = GroupPermissions.Administration;

            return groupPermissions;
        }

        public static Permissions? GetPermissionsEnum(Guid id)
        {
            Permissions permission;
            if (_permissions.TryGetValue(id, out permission))
                return permission;

            return null;
        }

        public static Guid? GetPermission(Permissions permission)
        {
            var item = _permissions.FirstOrDefault(p => p.Value == permission);
            if (item.Key != Guid.Empty)
                return item.Key;

            return null;
        }

        #endregion
    }
}