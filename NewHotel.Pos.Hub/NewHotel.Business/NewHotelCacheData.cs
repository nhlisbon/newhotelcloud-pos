﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    public sealed class NewHotelCacheData : CacheData
    {
        public NewHotelCacheData()
            : base() { }

        private readonly static string LanguageCommandText;
        private readonly static string EnumCommandText;

        static NewHotelCacheData()
        {
            StringBuilder sb;

            sb = new StringBuilder();
            sb.AppendLine("select lang.lang_pk, mult.mult_desc as lang_desc");
            sb.AppendLine("from tnht_licl licl, tnht_lang lang, vnht_mult mult");
            sb.AppendLine("where licl.licl_pk = lang.lang_pk");
            sb.AppendLine("and licl.lite_pk = mult.lite_pk");
            sb.AppendLine("and mult.lang_pk = :lang_pk");
            sb.AppendLine("order by licl.licl_pk");
            LanguageCommandText = sb.ToString();

            sb = new StringBuilder();
            sb.AppendLine("select enum.enum_pk, mult.mult_desc as enum_desc");
            sb.AppendLine("from tnht_enum enum, vnht_mult mult");
            sb.AppendLine("where enum.lite_pk = mult.lite_pk");
            sb.AppendLine("and mult.lang_pk = :lang_pk");
            sb.AppendLine("order by enum.enum_pk");
            EnumCommandText = sb.ToString();
        }

        private IDictionary<long, string> GetDictionaryFromQuery(IDatabaseManager manager, long language, string query)
        {
            IDictionary<long, string> dict = new Dictionary<long, string>();

            manager.Open();
            try
            {
                IDbCommand command = manager.CreateCommand();
                command.Connection = manager.Connection;
                command.CommandType = CommandType.Text;
                command.CommandText = query;

                IDbDataParameter parameter = manager.CreateParameter(command);
                parameter.ParameterName = "LANG_PK";
                parameter.Direction = ParameterDirection.Input;
                parameter.Value = language;

                IDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                        dict.Add(reader.GetInt64(0), reader.GetValue(1).ToString());
                }
                finally
                {
                    reader.Close();
                }
            }
            finally
            {
                manager.Close();
            }

            return dict;
        }

        private void BindLanguages(IDatabaseManager manager, long language)
        {
            ColumnDefinition[] colDefs = new ColumnDefinition[] {
                new ColumnDefinition("LANG_PK", typeof(long), 0),
                new ColumnDefinition("LANG_DESC", typeof(string), 1)
            };

            string name = language.ToString();
            LanguageDataStorage[name] =
                new ListData(name, colDefs,
                    GetDictionaryFromQuery(manager, language, LanguageCommandText)
                    .Select(x => new object[] { x.Key, x.Value }).ToList());
        }

        private void BindEnums(IDatabaseManager manager, long language)
        {
            ColumnDefinition[] colDefs = new ColumnDefinition[] {
                new ColumnDefinition("ENUM_PK", typeof(long), 0),
                new ColumnDefinition("ENUM_DESC", typeof(string), 1)
            };

            var dict = GetDictionaryFromQuery(manager, language, EnumCommandText);

            foreach (var enumType in EnumTypes)
            {
                string name = AppContext.GetEnumName(enumType.Key, language);
                if (!EnumDataStorage.Contains(name))
                {
                    List<object[]> data = new List<object[]>();
                    foreach (var key in enumType.Value)
                    {
                        string value;
                        if (dict.TryGetValue(key, out value))
                            data.Add(new object[] { key, value });
                    }

                    if (data.Count > 0)
                        EnumDataStorage[name] = new ListData(name, colDefs, data);
                }
            }
        }

        public override void DataBinding(long language, IDatabaseManager manager)
        {
            try
            {
                BindLanguages(manager, language);
                BindEnums(manager, language);
            }
            catch (Exception ex)
            {
                throw new DataException(ex.Message + "\n" + ex.Source);
            }
        }

        public override ListData GetLanguages(long languageId)
        {
            return LanguageDataStorage.GetData(o => o.Equals(languageId.ToString())).FirstOrDefault().Value;
        }

        public override IDictionary<string, ListData> GetEnums(long languageId)
        {
            return EnumDataStorage.GetData(o => o.EndsWith(languageId.ToString()));
        }
    }

    public sealed class NewHotelClientCacheData : CacheData
    {
        public NewHotelClientCacheData()
            : base() { }

        public override void DataBinding(long language, IDatabaseManager manager)
        {
        }

        public override ListData GetLanguages(long languageId)
        {
            return new ListData();
        }

        public override IDictionary<string, ListData> GetEnums(long languageId)
        {
            return new Dictionary<string, ListData>();
        }
    }

}
