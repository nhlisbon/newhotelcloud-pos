﻿using System;
using System.Collections.Generic;
using NewHotel.Core;

namespace NewHotel.Business
{
    public abstract class BaseCache : Cache
    {
        private HashSet<long> _languages;
        private CacheData _cacheData;

        private HashSet<long> Languages
        {
            get
            {
                if (_languages == null)
                    _languages = new HashSet<long>();

                return _languages;
            }
        }

        public override CacheData CacheData
        {
            get
            {
                lock (Languages)
                {
                    if (_cacheData == null)
                        _cacheData = CreateCacheData();

                    long language = AppContext.GetLanguage();
                    if (!Languages.Contains(language))
                    {
                        _cacheData.DataBinding(language, AppContext.GetManager());
                        Languages.Add(language);
                    }
                }

                return _cacheData;
            }
        }

        public void Clear()
        {
            _cacheData = null;
            _languages = null;
        }

        protected abstract CacheData CreateCacheData();
    }

    public sealed class NewHotelCache : BaseCache
    {
        protected override CacheData CreateCacheData()
        {
            return new NewHotelCacheData();
        }
    }

    public sealed class NewHotelClientCache : BaseCache
    {
        protected override CacheData CreateCacheData()
        {
            return new NewHotelClientCacheData();
        }
    }
}
