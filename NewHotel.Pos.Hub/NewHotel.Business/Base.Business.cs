﻿using System;
using NewHotel.Core;

namespace NewHotel.Business
{
    public abstract class BaseBusiness : IDisposable
    {
        #region Constants

        protected const string BUSINESSLOGKEY = "NewHotel.Business";

        #endregion
        #region Members

        private IDatabaseManager _manager;
        private bool _disposed;

        #endregion
        #region Properties

        protected IDatabaseManager Manager
        {
            get { return _manager; }
        }

        #endregion
        #region Constructor

        protected BaseBusiness(IDatabaseManager manager)
        {
            _manager = manager;
        }

        #endregion

        public void Dispose()
        {
            if (!_disposed)
            {
                _disposed = true;
                try
                {
                    _manager = null;
                }
                finally
                {
                    GC.SuppressFinalize(this);
                }
            }
        }

        ~BaseBusiness()
        {
        }
    }
}
