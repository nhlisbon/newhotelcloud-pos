﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Globalization;
using NewHotel.DataAnnotations;

namespace NewHotel.Core
{
    public static class MailHelper
    {
        public static CultureInfo MailCulture = new CultureInfo(1033);
        private static readonly string MailConfigCommandText;

        static MailHelper()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select smtp_host, smtp_user, smtp_pass, smtp_from, smtp_to from tnht_para");
            MailConfigCommandText = sb.ToString();
        }

        public static void AddHeader(this IList<string> list, string label)
        {
            list.Add(LocalizeHelper.Localize(MailCulture, label));
        }

        public static void Add(this IList<string> list, string label, string value)
        {
            list.Add(string.Format("{0}: {1}", LocalizeHelper.Localize(MailCulture, label), value));
        }

        public static void Add(this IList<string> list)
        {
            list.Add(string.Empty);
        }

        private static bool GetMailConfig(out string host, out string user, out string password, out string from, out string to)
        {
            host = string.Empty;
            user = string.Empty;
            password = string.Empty;
            from = string.Empty;
            to = string.Empty;

            using (var manager = NewHotelAppContext.GetManager(Guid.Empty))
            {
                manager.Open();
                try
                {
                    using (var command = manager.CreateCommand("MailHelper.GetMailConfig"))
                    {
                        command.CommandText = MailConfigCommandText;
                        using (var reader = manager.ExecuteReader(command))
                        {
                            try
                            {
                                if (reader.Read())
                                {
                                    host = reader.GetString(0);
                                    user = reader.GetString(1);
                                    password = reader.GetString(2);
                                    from = reader.GetString(3);
                                    to = reader.GetString(4);
                                    return !string.IsNullOrEmpty(host) && !string.IsNullOrEmpty(user) &&
                                        !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(from) &&
                                        !string.IsNullOrEmpty(to);
                                }

                                return false;
                            }
                            finally
                            {
                                reader.Close();
                            }
                        }
                    }
                }
                finally
                {
                    manager.Close();
                }
            }
        }

        private static string GetRequestBody(CultureInfo culture, string header, string[] details)
        {
            var sb = new StringBuilder();
            sb.AppendLine(header);
            sb.AppendLine(string.Empty);
            foreach (var detail in details)
                sb.AppendLine(detail);
            sb.AppendLine(string.Empty);
            string comment = LocalizeHelper.Localize(culture, "MailComment1");
            sb.AppendLine(comment);
            comment = LocalizeHelper.Localize(culture, "MailComment2");
            sb.AppendLine(comment);
            sb.AppendLine(string.Empty);
            string signature = LocalizeHelper.Localize(culture, "MailSignature1");
            sb.AppendLine(signature);
            signature = LocalizeHelper.Localize(culture, "MailSignature2");
            sb.AppendLine(signature);

            return sb.ToString();
        }

        private static string GetRequestBody( string header, string[] details)
        {
            var sb = new StringBuilder();
            sb.AppendLine(header);
            sb.AppendLine(string.Empty);
            foreach (var detail in details)
                sb.AppendLine(detail);
            sb.AppendLine(string.Empty);
            

            return sb.ToString();
        }

        public static void SendMailNotification(string subject, string address, params string[] details)
        {
            string host;
            string user;
            string password;
            string from;
            string to;

            if (GetMailConfig(out host, out user, out password, out from, out to))
            {
                var client = new SmtpClient(host);
                client.Credentials = new NetworkCredential(user, password);

                var msg = new MailMessage(from, string.IsNullOrEmpty(address) ? to : address);
                msg.Subject = subject;
                msg.IsBodyHtml = false;
                msg.BodyEncoding = Encoding.Unicode;
                msg.Body = GetRequestBody(subject, details);

                client.Send(msg);
            }
            else
                throw new ApplicationException("Mail config data not found");
        }

        public static MailMessage GetMailNotification(long emailType, string from, string to, string subject, params string[] details)
        {
            var msg = new MailMessage();
            msg.From = new MailAddress(from);
            msg.Subject = subject;
            msg.IsBodyHtml = false;
            msg.BodyEncoding = Encoding.Unicode;
            msg.Body = GetRequestBody(MailHelper.MailCulture, subject, details);

            var query = new BaseQuery(NewHotelAppContext.GetManager(Guid.Empty), "Core.SendMailNotification.GetMailConfig",
                "select emai_addr, emai_to from tnht_emai where emai_type = :emai_type");
            query.Parameters["emai_type"] = emailType;
            var emails = query.Execute();

            if (!emails.IsEmpty)
            {
                foreach (IRecord item in emails)
                {
                    if (item.ValueAs<bool>("emai_to"))
                        msg.To.Add(new MailAddress(item.ValueAs<string>("emai_addr")));
                    else
                        msg.CC.Add(new MailAddress(item.ValueAs<string>("emai_addr")));
                }
            }
            else
                msg.To.Add(new MailAddress(to));

            return msg;
        }

        public static void SendMailNotification(long emailType, string subject, params string[] details)
        {
            string host;
            string user;
            string password;
            string from;
            string to;

            if (GetMailConfig(out host, out user, out password, out from, out to))
            {
                var client = new SmtpClient(host);
                client.Credentials = new NetworkCredential(user, password);

                var msg = GetMailNotification(emailType, from, to, subject, details);
                client.Send(msg);
            }
        }

        public static void SendMailNotification(string subject, params string[] details)
        {
            SendMailNotification(subject, null, details);
        }
    }
}