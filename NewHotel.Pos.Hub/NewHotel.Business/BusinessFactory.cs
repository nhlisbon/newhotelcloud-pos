﻿using System;
using System.Linq;
using System.Collections.Generic;
using NewHotel.Core;

namespace NewHotel.Business
{
    public abstract class BusinessFactory<T>
        where T : class, new()
    {
        #region Members

        private static readonly IDictionary<Type, Func<IDatabaseManager, BaseBusiness>> _impls =
            new Dictionary<Type, Func<IDatabaseManager, BaseBusiness>>();

        #endregion
        #region Properties

        public static readonly T Instance = new T();

        #endregion
        #region Constructor

        public BusinessFactory()
        {
            Initialize();
        }

        #endregion
        #region Protected Methods

        protected abstract void Initialize();

        protected void Add<TIntf>(Func<IDatabaseManager, BaseBusiness> implFactory)
            where TIntf : class
        {
            var intfType = typeof(TIntf);
            if (!intfType.IsInterface)
                throw new ArgumentException(string.Format("{0} must be a business interface type.", intfType.FullName));

            lock (_impls)
            {
                if (!_impls.ContainsKey(intfType))
                    _impls.Add(intfType, implFactory);
            }
        }

        protected void Add<TIntf, TImpl>()
            where TIntf : class
            where TImpl : BaseBusiness
        {
            var intfType = typeof(TIntf);
            var implType = typeof(TImpl);
            if (!implType.IsClass || !implType.GetInterfaces().Contains(intfType))
                throw new ArgumentException(string.Format("Business implementation {0} must implement interface {1}.", implType.FullName, intfType.FullName));

            Add<TIntf>((dm) => { return (BaseBusiness)Activator.CreateInstance(implType, dm); });
        }

        #endregion
        #region Public Methods

        public TIntf GetBusiness<TIntf>(IDatabaseManager manager)
            where TIntf : class
        {
            var intfType = typeof(TIntf);
            Func<IDatabaseManager, BaseBusiness> implFactory;
            lock (_impls)
            {
                if (_impls.TryGetValue(intfType, out implFactory))
                    return implFactory.Invoke(manager) as TIntf;
            }
                
            return null;
        }

        public TIntf GetBusiness<TIntf>()
            where TIntf : class
        {
            return GetBusiness<TIntf>(NewHotelAppContext.GetManager());
        }

        #endregion
    }
}