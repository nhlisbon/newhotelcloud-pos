﻿using System;
using System.Collections.Generic;
using NewHotel.Core;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Business
{
    public class NewHotelContext : AppContext
    {
        #region Instance & Constructor

        public static AppContext Instance
        {
            get { return GetInstance(); }
        }

        static NewHotelContext()
        {
            InitCache<NewHotelCache>();
        }

        #endregion
        #region Private Methods

        private void LoadInstallationInfo(out string installationName, out string customInstallationReportPath)
        {
            installationName = string.Empty;
            customInstallationReportPath = string.Empty;

            if (InstallationId.HasValue)
            {
                Query query = new BaseQuery(GetManager(),
                    "select benti_desc as hote_desc from tnht_benti",
                    "where benti_pk = :enti_pk");
                query.Parameters["enti_pk"] = InstallationId.Value;
                ListData records = query.Execute();

                if (!records.IsEmpty)
                    installationName = records.ValueAs<string>("hote_desc");
            }
        }

        #endregion
        #region Public Methods

        public override string ApplicationName
        {
            get { return GetApplicationName(ApplicationId); }
        }

        public override string InstallationName
        {
            get
            {
                string installationName;
                string customInstallationReportPath;
                LoadInstallationInfo(out installationName, out customInstallationReportPath);
                return installationName;
            }
        }

        public override string UserName
        {
            get 
            {
                IDatabaseManager manager = GetManager();

                manager.Open();
                try
                {
                    if (UserId != Guid.Empty)
                    {
                        Query query = new BaseQuery(manager,
                            "select util.util_login from tnht_util util where util.util_pk = :util_pk");
                        query.Parameters["util_pk"] = UserId;
                        ListData records = query.Execute();
                        if (records.Count > 0)
                            return records.ValueAs<string>("util_login");
                    }

                    return null;
                }
                finally
                {
                    manager.Close();
                }
            }
        }

        public override string Copyright
        {
            get { return "NewHotel Software S.A."; }
        }

        public override string CustomReportPath
        {
            get { return string.Empty; }
        }

        public override string CustomInstallationReportPath
        {
            get
            {
                string installationName;
                string customInstallationReportPath;
                LoadInstallationInfo(out installationName, out customInstallationReportPath);
                return customInstallationReportPath;
            }
        }

        protected override void LoadInstallations()
        {            
            IDatabaseManager manager = GetManager(Guid.Empty, 0);

            manager.Open();
            try
            {
                Query query = new BaseQuery(manager,
                    "select hote.hote_pk, hote.hote_prov, hote.hote_serv, hote.hote_user, hote.hote_pwrd",
                    "from tnht_hote hote");
                ListData records = query.Execute();
                foreach (IRecord record in records)
                {
                    // Last parameter must be database name in case of mssql server
                    AddManager(record.ValueAs<Guid>("hote_pk"), (DataProvider)record.ValueAs<short>("hote_prov"),
                        record.ValueAs<string>("hote_user"), record.ValueAs<string>("hote_pwrd"), record.ValueAs<string>("hote_serv"),
                        string.Empty);
                }
            }
            finally
            {
                manager.Close();
            }
        }

        public override DateTime GetSysDate()
        {
            return DateTime.Now.Date;
        }

        public override string GetApplicationName(long applicationId)
        {
            IDatabaseManager manager = GetManager();

            manager.Open();
            try
            {
                if (applicationId > 0)
                {
                    Query query = new BaseQuery(manager,
                        "select appl.appl_name from tnht_appl appl where appl.appl_pk = :appl_pk");
                    query.Parameters["appl_pk"] = applicationId;
                    ListData records = query.Execute();
                    if (records.Count > 0)
                        return records.ValueAs<string>("appl_name");
                }

                return string.Empty;
            }
            finally
            {
                manager.Close();
            }
        }

        //public override DateTime GetWorkDate()
        //{
        //    IDatabaseManager manager = GetManager();

        //    manager.Open();
        //    try
        //    {
        //        if (ApplicationId > 0 && InstallationId.HasValue)
        //        {
        //            Query query = new BaseQuery(manager,
        //                "select datr.datr_datr from tnht_datr datr where datr.appl_pk = :appl_pk and datr.hote_pk = :hote_pk");
        //            query.Parameters["appl_pk"] = ApplicationId;
        //            query.Parameters["hote_pk"] = InstallationId.Value;
        //            ListData records = query.Execute();
        //            if (records.Count > 0)
        //                return records.ValueAs<DateTime>("datr_datr");
        //        }

        //        return DateTime.Now.Date;
        //    }
        //    finally
        //    {
        //        manager.Close();
        //    }
        //}

        //public override short GetWorkShift()
        //{
        //    IDatabaseManager manager = GetManager();

        //    manager.Open();
        //    try
        //    {
        //        if (ApplicationId > 0 && InstallationId.HasValue)
        //        {
        //            Query query = new BaseQuery(manager,
        //                "select datr.datr_tutr from tnht_datr datr where datr.appl_pk = :appl_pk and datr.hote_pk = :hote_pk");
        //            query.Parameters["appl_pk"] = ApplicationId;
        //            query.Parameters["hote_pk"] = InstallationId.Value;
        //            ListData records = query.Execute();
        //            if (records.Count > 0)
        //                return records.ValueAs<short>("datr_tutr");
        //        }

        //        return 0;
        //    }
        //    finally
        //    {
        //        manager.Close();
        //    }
        //}

        public override DateTime? GetFirstOpenedDate()
        {
            IDatabaseManager manager = GetManager();

            manager.Open();
            try
            {
                if (InstallationId.HasValue)
                {
                    Query query = new BaseQuery(manager,
                        "select min(edta.edta_data) as edta_data from tnht_edta edta inner join tnht_trec trec",
                        "on trec.trec_pk = edta.trec_pk where trec.hote_pk = :hote_pk");
                    query.Parameters["hote_pk"] = InstallationId.Value;
                    ListData records = query.Execute();
                    if (records.Count > 0)
                        return records.ValueAs<DateTime>("edta_data");
                }

                return null;
            }
            finally
            {
                manager.Close();
            }
        }


        //public override int? GetLastOpenedYear()
        //{
            //IDatabaseManager manager = GetManager();

            //manager.Open();
            //try
            //{
            //    if (InstallationId.HasValue)
            //    {
            //        Query query = new BaseQuery(manager,
            //            "select hote.para_uaab from tcfg_hote hote where hote.hote_pk = :hote_pk");
            //        query.Parameters["hote_pk"] = InstallationId.Value;
            //        ListData records = query.Execute();
            //        if (records.Count > 0)
            //        {
            //            int? lastOpenedYear = records.ValueAs<int?>("para_uaab");
            //            if (lastOpenedYear.HasValue)
            //                return lastOpenedYear.Value;
            //        }
            //    }

            //    return null;
            //}
            //finally
            //{
            //    manager.Close();
            //}
        //}

        public override IDictionary<long, string> GetLanguages()
        {
            IDatabaseManager manager = GetManager();

            manager.Open();
            try
            {
                Query query = new BaseQuery(manager,
                    "select lang.lang_pk,",
                    "(select mult.mult_desc from tnht_mult mult where mult.lite_pk = licl.lite_pk and mult.lang_pk = :lang_pk) as lang_desc",
                    "from tnht_lang lang inner join tnht_licl licl on licl.licl_pk = lang.lang_pk",
                    "order by lang.lang_prio");

                IDictionary<long, string> languages = new Dictionary<long, string>();
                ListData records = query.Execute();
                foreach (IRecord record in records)
                    languages.Add(record.ValueAs<long>("lang_pk"), record.ValueAs<string>("lang_desc"));

                return languages;
            }
            finally
            {
                manager.Close();
            }
        }

        public override IDictionary<Guid, string> GetInstallations(Guid userId)
        {
            IDatabaseManager manager = GetManager();

            manager.Open();
            try
            {
                Query query = new BaseQuery(manager,
                    "select hote.hote_pk, enti.benti_desc as hote_desc",
                    "from tnht_hote hote inner join tnht_benti enti on enti.benti_pk = hote.hote_pk");

                IDictionary<Guid, string> installations = new Dictionary<Guid, string>();
                ListData records = query.Execute();
                foreach (IRecord record in records)
                    installations.Add(record.ValueAs<Guid>("hote_pk"), record.ValueAs<string>("hote_desc"));

                return installations;
            }
            finally
            {
                manager.Close();
            }
        }

        public  ListData LoadUser(string loginName)
        {
            IDatabaseManager manager = GetManager();

            manager.Open();
            try
            {
                Query query = new BaseQuery(manager,
                    "select util.util_pk, util.util_login, util.util_pass",
                    "from tnht_util util where upper(util.util_login) = upper(:util_login)");
                query.Parameters["util_login"] = loginName;
                return query.Execute();
            }
            finally
            {
                manager.Close();
            }
        }

        public override Guid? GetUser(string loginName)
        {
            ListData list = LoadUser(loginName);
            if (list.Count != 0)
                return list.ValueAs<Guid>("util_pk");

            return null;
        }

        public override Guid? AuthorizeUser(string loginName, string loginPassword)
        {

            ListData list = LoadUser(loginName);
            if (list.Count != 0)
            {
                //TODO: Missing expiration treatment
                if (!list.ValueAs<string>("util_pass").Trim().ToUpperInvariant().Equals(loginPassword.ToUpperInvariant()))
                    return null;

                return list.ValueAs<Guid>("util_pk");
            }

            return null;
        }

        #endregion
    }

    //public sealed class NewHotelClientContext : AppContext
    //{
    //    public static AppContext Instance
    //    {
    //        get { return GetInstance(); }
    //    }

    //    public NewHotelClientContext()
    //    {
    //        InitCache<NewHotelClientCache>();
    //    }

    //    public override string ApplicationName
    //    {
    //        get { return GetApplicationName(ApplicationId); }
    //    }

    //    private void LoadInstallationInfo(out string installationName, out string customInstallationReportPath)
    //    {
    //        installationName = string.Empty;
    //        customInstallationReportPath = string.Empty;

    //        if (InstallationId.HasValue)
    //        {
    //            Query query = new BaseQuery(GetManager(),
    //                "select benti_desc as hote_desc from tnht_benti",
    //                "where benti_pk = :enti_pk");
    //            query.Parameters["enti_pk"] = InstallationId.Value;
    //            ListData records = query.Execute();

    //            if (!records.IsEmpty)
    //                installationName = records.ValueAs<string>("hote_desc");
    //        }
    //    }

    //    public override string InstallationName
    //    {
    //        get
    //        {
    //            string installationName;
    //            string customInstallationReportPath;
    //            LoadInstallationInfo(out installationName, out customInstallationReportPath);
    //            return installationName;
    //        }
    //    }

    //    public override string UserName
    //    {
    //        get
    //        {
    //            IDatabaseManager manager = GetManager();

    //            manager.Open();
    //            try
    //            {
    //                if (UserId != Guid.Empty)
    //                {
    //                    Query query = new BaseQuery(manager,
    //                        "select util.util_login from tnht_util util where util.util_pk = :util_pk");
    //                    query.Parameters["util_pk"] = UserId;
    //                    ListData records = query.Execute();
    //                    if (records.Count > 0)
    //                        return records.ValueAs<string>("util_login");
    //                }

    //                return null;
    //            }
    //            finally
    //            {
    //                manager.Close();
    //            }
    //        }
    //    }

    //    public override string Copyright
    //    {
    //        get { return "NewHotel Software S.A."; }
    //    }

    //    public override string CustomReportPath
    //    {
    //        get { return string.Empty; }
    //    }

    //    public override string CustomInstallationReportPath
    //    {
    //        get
    //        {
    //            string installationName;
    //            string customInstallationReportPath;
    //            LoadInstallationInfo(out installationName, out customInstallationReportPath);
    //            return customInstallationReportPath;
    //        }
    //    }

    //    protected override void LoadSchemas()
    //    {
    //    }

    //    public override DateTime GetSysDate()
    //    {
    //        return DateTime.Now.Date;
    //    }

    //    public override string GetApplicationName(long applicationId)
    //    {
    //        IDatabaseManager manager = GetManager();

    //        manager.Open();
    //        try
    //        {
    //            if (applicationId > 0)
    //            {
    //                Query query = new BaseQuery(manager,
    //                    "select appl.appl_name from tnht_appl appl where appl.appl_pk = :appl_pk");
    //                query.Parameters["appl_pk"] = applicationId;
    //                ListData records = query.Execute();
    //                if (records.Count > 0)
    //                    return records.ValueAs<string>("appl_name");
    //            }

    //            return string.Empty;
    //        }
    //        finally
    //        {
    //            manager.Close();
    //        }
    //    }

    //    public override DateTime GetWorkDate()
    //    {
    //        IDatabaseManager manager = GetManager();

    //        manager.Open();
    //        try
    //        {
    //            if (ApplicationId > 0 && InstallationId.HasValue)
    //            {
    //                Query query = new BaseQuery(manager,
    //                    "select datr.datr_datr from tnht_datr datr where datr.appl_pk = :appl_pk and datr.hote_pk = :hote_pk");
    //                query.Parameters["appl_pk"] = ApplicationId;
    //                query.Parameters["hote_pk"] = InstallationId.Value;
    //                ListData records = query.Execute();
    //                if (records.Count > 0)
    //                    return records.ValueAs<DateTime>("datr_datr");
    //            }

    //            return DateTime.Now.Date;
    //        }
    //        finally
    //        {
    //            manager.Close();
    //        }
    //    }

    //    public override short GetWorkShift()
    //    {
    //        IDatabaseManager manager = GetManager();

    //        manager.Open();
    //        try
    //        {
    //            if (ApplicationId > 0 && InstallationId.HasValue)
    //            {
    //                Query query = new BaseQuery(manager,
    //                    "select datr.datr_tutr from tnht_datr datr where datr.appl_pk = :appl_pk and datr.hote_pk = :hote_pk");
    //                query.Parameters["appl_pk"] = ApplicationId;
    //                query.Parameters["hote_pk"] = InstallationId.Value;
    //                ListData records = query.Execute();
    //                if (records.Count > 0)
    //                    return records.ValueAs<short>("datr_tutr");
    //            }

    //            return 0;
    //        }
    //        finally
    //        {
    //            manager.Close();
    //        }
    //    }

    //    public override DateTime? GetLastOpenedDate()
    //    {
    //        IDatabaseManager manager = GetManager();

    //        manager.Open();
    //        try
    //        {
    //            if (InstallationId.HasValue)
    //            {
    //                Query query = new BaseQuery(manager,
    //                    "select hote.para_uaab from tcfg_hote hote where hote.hote_pk = :hote_pk");
    //                query.Parameters["hote_pk"] = InstallationId.Value;
    //                ListData records = query.Execute();
    //                if (records.Count > 0)
    //                {
    //                    int? lastOpenedYear = records.ValueAs<int?>("para_uaab");
    //                    if (lastOpenedYear.HasValue)
    //                        return new DateTime(lastOpenedYear.Value, 12, 31);
    //                }
    //            }

    //            return null;
    //        }
    //        finally
    //        {
    //            manager.Close();
    //        }
    //    }

    //    public override IDictionary<long, string> GetLanguages()
    //    {
    //        IDatabaseManager manager = GetManager();

    //        manager.Open();
    //        try
    //        {
    //            Query query = new BaseQuery(manager,
    //                "select lang.lang_pk,",
    //                "(select mult.mult_desc from tnht_mult mult where mult.lite_pk = licl.lite_pk and mult.lang_pk = :lang_pk) as lang_desc",
    //                "from tnht_lang lang inner join tnht_licl licl on licl.licl_pk = lang.lang_pk",
    //                "order by lang.lang_prio");

    //            IDictionary<long, string> languages = new Dictionary<long, string>();
    //            ListData records = query.Execute();
    //            foreach (IRecord record in records)
    //                languages.Add(record.ValueAs<long>("lang_pk"), record.ValueAs<string>("lang_desc"));

    //            return languages;
    //        }
    //        finally
    //        {
    //            manager.Close();
    //        }
    //    }

    //    public override IDictionary<Guid, string> GetInstallations(Guid userId)
    //    {
    //        IDatabaseManager manager = GetManager();

    //        manager.Open();
    //        try
    //        {
    //            Query query = new BaseQuery(manager,
    //                "select hote.hote_pk, enti.benti_desc as hote_desc",
    //                "from tnht_hote hote inner join tnht_benti enti on enti.benti_pk = hote.hote_pk");

    //            IDictionary<Guid, string> installations = new Dictionary<Guid, string>();
    //            ListData records = query.Execute();
    //            foreach (IRecord record in records)
    //                installations.Add(record.ValueAs<Guid>("hote_pk"), record.ValueAs<string>("hote_desc"));

    //            return installations;
    //        }
    //        finally
    //        {
    //            manager.Close();
    //        }
    //    }

    //    private ListData LoadUser(string loginName)
    //    {
    //        IDatabaseManager manager = GetManager();

    //        manager.Open();
    //        try
    //        {
    //            Query query = new BaseQuery(manager,
    //                "select util.util_pk, util.util_login, util.util_pass, util.util_dare, util.util_expw, util.util_expu",
    //                "from tnht_util util where upper(util.util_login) = :util_login");
    //            query.Parameters["util_login"] = loginName.ToUpperInvariant();
    //            return query.Execute();
    //        }
    //        finally
    //        {
    //            manager.Close();
    //        }
    //    }

    //    public override Guid? GetUser(string loginName)
    //    {
    //        ListData list = LoadUser(loginName);
    //        if (list.Count != 0)
    //            return list.ValueAs<Guid>("util_pk");

    //        return null;
    //    }

    //    public override Guid? AuthorizeUser(string loginName, string loginPassword)
    //    {

    //        ListData list = LoadUser(loginName);
    //        if (list.Count != 0)
    //        {
    //            // User inactive
    //            //if (list.ValueAs<bool>("util_inac"))
    //            //    return null;

    //            //TODO: Missing expiration treatment
    //            if (!list.ValueAs<string>("util_pass").Trim().ToUpperInvariant().Equals(loginPassword.ToUpperInvariant()))
    //                return null;

    //            return list.ValueAs<Guid>("util_pk");
    //        }

    //        return null;
    //    }
    //}
}
