﻿using System;
using NewHotel.Core;

namespace NewHotel.Business
{
    public interface IBaseBusiness { }

    public class NewHotelBusiness : BaseBusiness, IBaseBusiness
    {
        public NewHotelBusiness(IDatabaseManager manager)
            : base(manager) { }
    }
}
