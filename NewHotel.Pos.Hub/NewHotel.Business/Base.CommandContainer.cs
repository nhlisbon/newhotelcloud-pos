﻿
using System;
using System.Linq;
using NewHotel.Data.Commands;

namespace NewHotel.Business
{
    public class BaseCommandContainer
    {
        public T Get<T>(params object[] args) where T : DbCommand
        {
            return (T)Activator.CreateInstance(typeof(T), args);
        }
    }

    public class CommandContainer<T> : BaseCommandContainer where T : class, new()
    {
        public static readonly T Instance = new T();
    }
}
