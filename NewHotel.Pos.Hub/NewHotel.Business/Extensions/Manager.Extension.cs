﻿using System.Linq;
using System.Collections.Generic;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    public static class ManagerExtension
    {
        public static bool EndTransaction(this IDatabaseManager manager,
            ValidationResult result, bool rollbackOnWarning = false)
        {
            if (result.HasWarnings && rollbackOnWarning)
            {
                manager.RollbackTransaction();
                return false;
            }

            if (result.HasErrors)
            {
                manager.RollbackTransaction();
                return false;
            }

            manager.CommitTransaction();
            return true;
        }

        public static bool InTransaction(this IDatabaseManager manager)
        {
            return manager != null && manager.TransactionLevel > 0;
        }

        public static bool IsConnected(this IDatabaseManager manager)
        {
            return manager != null && manager.IsConnected;
        }

        public static void EndTransaction(this IEnumerable<IDatabaseManager> managers, ValidationResult result)
        {
            foreach (var manager in managers)
                manager.EndTransaction(result);
        }

        public static void BeginTransaction(this IEnumerable<IDatabaseManager> managers)
        {
            foreach (var manager in managers)
                manager.BeginTransaction();
        }

        public static void RollbackTransaction(this IEnumerable<IDatabaseManager> managers)
        {
            foreach (var manager in managers.Where(m => m.InTransaction()))
                manager.RollbackTransaction();
        }

        public static void Open(this IEnumerable<IDatabaseManager> managers)
        {
            foreach (var manager in managers)
                manager.Open();
        }

        public static void Close(this IEnumerable<IDatabaseManager> managers)
        {
            foreach (var manager in managers.Where(m => m.IsConnected()))
                manager.Close();
        }
    }
}