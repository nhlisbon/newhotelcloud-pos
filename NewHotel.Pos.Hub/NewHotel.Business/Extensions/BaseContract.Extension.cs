﻿using System;
using System.Linq;
using System.Collections.Generic;
using NewHotel.Core;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Business
{
    public static class BaseContractExtension
    {
        #region Cast

        public static T As<T>(this BaseContract c)
            where T : BaseContract
        {
            return c as T;
        }

        #endregion
        #region Validation

        public static void Validate(this BaseContract c, ValidationResult vr)
        {
            var type = c.GetType();
            var accessors = type.PropAccessors().Values
                .Where(pa => pa.GetAttributes<Constraint>().Any());

            foreach (var accesor in accessors)
            {
                foreach (var constraint in accesor.GetAttributes<Constraint>())
                    constraint.Validate(accesor.Get(c), vr);
            }
        }

        #endregion
        #region Load

        public static void Load<S, T>(this ICollection<T> target, IEnumerable<S> source, Func<S, T> create, Action<S, T> action = null)
            where T : BaseContract
            where S : IBaseObject
        {
            foreach (var s in source)
            {
                var t = create(s);
                if (t != null)
                {
                    if (action != null)
                        action(s, t);
                    else
                        t.SetValues(s.Values());
                    target.Add(t);
                }
            }
        }

        public static void Load<S, T>(this ICollection<T> target, IEnumerable<S> source, Action<S, T> action)
            where T : BaseContract, new()
            where S : IBaseObject
        {
            Load(target, source, p => new T(), action);
        }

        public static void Load<S, T>(this ICollection<T> target, IEnumerable<S> source)
            where T : BaseContract, new()
            where S : IBaseObject
        {
            Load(target, source, p => new T());
        }

        #endregion
        #region Clone

        public static T Clone<T>(this T s)
            where T : BaseContract, new()
        {
            var t = new T();
            t.SetValues(s.GetValues());
            t.Id = null;
            return t;
        }

        public static void Clone<S, T>(this ICollection<T> target, IEnumerable<S> source, Func<S, T> create, Func<T, S, bool> action)
            where T : BaseContract
            where S : IBaseObject
        {
            target.Clear();
            foreach (var s in source)
            {
                var t = create(s);
                if (t != null)
                {
                    t.SetValues(s.Values());
                    action?.Invoke(t, s);
                    target.Add(t);
                }
            }
        }

        public static void Clone<S, T>(this ICollection<T> target, IEnumerable<S> source, Func<T, S, bool> action)
            where T : BaseContract, new()
            where S : IBaseObject
        {
            Clone(target, source, (s) => { return new T(); }, action);
        }

        public static void Clone<S, T>(this ICollection<T> target, IEnumerable<S> source)
            where T : BaseContract, new()
            where S : IBaseObject
        {
            Clone(target, source, (t, s) => true);
        }

        public static void Clone<S, T>(this ICollection<T> target, IEnumerable<S> source, Action<T, S> action)
            where T : BaseContract, new()
            where S : IBaseObject
        {
            Clone(target, source, (t, s) => { action(t, s); return true; });
        }

        public static void Clone<S, T>(this ICollection<T> target, IEnumerable<S> source, Func<S, T> create, Action<T, S> action)
            where T : BaseContract
            where S : IBaseObject
        {
            Clone(target, source, create, (t, s) => { action(t, s); return true; });
        }

        #endregion
        #region Initialize

        public static void Init<P, C>(this C c, P p, Action<P, C> action = null)
            where C : BaseContract
            where P : BasePersistent
        {
            c.SetValues(p.GetValues());
            action?.Invoke(p, c);
        }

        #endregion
        #region CopyFrom

        public static void CopyFrom<S, T>(this ICollection<T> target, IEnumerable<S> source, Func<T> create, Action<S, T> action = null)
            where S : BasePersistent
            where T : BaseContract
        {
            foreach (var s in source)
            {
                var t = create();
                if (t != null)
                {
                    t.SetValues(s.GetValues());
                    action?.Invoke(s, t);
                    target.Add(t);
                }
            }
        }

        public static void CopyFrom<S, T>(this ICollection<T> target, IEnumerable<S> source, Action<S, T> action = null)
            where S : BasePersistent
            where T : BaseContract, new()
        {
            CopyFrom(target, source, () => new T(), action);
        }

        #endregion
        #region CopyTo

        public static void CopyTo<S, T>(this IEnumerable<S> source, ICollection<T> target, Func<T> create, Action<S, T> action = null)
            where S : BaseContract
            where T : BasePersistent
        {
            foreach (var s in source)
            {
                var t = create();
                if (t != null)
                {
                    t.SetValues(s.GetValues());
                    action?.Invoke(s, t);
                    target.Add(t);
                }
            }
        }

        public static void CopyTo<S, T>(this IEnumerable<S> source, ICollection<T> target, Action<S, T> action = null)
            where S : BaseContract
            where T : BasePersistent, new()
        {
            CopyTo(source, target, () => new T(), action);
        }

        #endregion
        #region Remove

        public static C Remove<C>(this ICollection<C> contracts, object id)
            where C : BaseContract
        {
            var contract = contracts.FirstOrDefault(pc => pc.Id.Equals(id));
            if (contract != null)
                contracts.Remove(contract);

            return contract;
        }

        public static IEnumerable<C> Remove<I, C>(this ICollection<C> contracts, IEnumerable<I> ids)
            where I : struct
            where C : BaseContract
        {
            var removed = new List<C>();
            foreach (var id in ids)
            {
                var contract = contracts.Remove(id);

                if (contract != null)
                {
                    removed.Add(contract);
                    ContractFactory.Remove(id, typeof(C));
                }
            }

            return removed;
        }

        #endregion
        #region Add

        public static bool Add<P, C>(this ICollection<C> contracts, P p, C c, Func<C, C, bool> comparer = null)
            where P : BaseContract
            where C : BaseContract
        {
            var duplicated = false;

            if (!contracts.Exists(pc => pc.Id.Equals(c.Id)))
            {
                if (comparer != null)
                    duplicated = contracts.Exists(pc => comparer(pc, c));

                if (!duplicated)
                {
                    contracts.Add(c);
                    ContractFactory.Add(p, c);
                }
            }

            return !duplicated;
        }

        #endregion
    }
}