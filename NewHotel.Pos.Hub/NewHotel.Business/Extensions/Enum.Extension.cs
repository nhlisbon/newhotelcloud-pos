﻿using System;
using System.Linq;
using System.Collections.Generic;
using NewHotel.Core;

namespace NewHotel.Business
{
    public static class EnumExtension
    {
        public static string ToTranslation<T>(this T value)
            where T : struct
        {
            return GetTranslation(typeof(T), value);
        }

        public static string ToTranslation<T>(this T? value)
            where T : struct
        {
            return value.HasValue ? GetTranslation(typeof(T), value) : string.Empty;
        }

        public static string GetTranslation(this Type type, object value)
        {
            if (type.IsEnum && Enum.GetUnderlyingType(type).Equals(typeof(long)))
            {
                var instance = NewHotelAppContext.GetInstance();
                var enumName = instance.GetEnumName(type);
                var enums = instance.CacheData.EnumDataStorage[enumName];

                if (enums != null && enums.Count > 0)
                {
                    var id = Convert.ToInt64(value);
                    if (enums.Locate(x => x.ValueAs<long>("enum_pk").Equals(id)))
                        return enums.ValueAs<string>("enum_desc");
                }

                return "{" + Enum.ToObject(type, value).ToString() + "}";
            }

            return string.Empty;
        }

        public static long[] ToArray<T>(this Enum values)
            where T : struct
        {
            var list = new List<long>();
            var enums = Enum.GetValues(typeof(T)).Cast<Enum>().ToArray();
            for (int index = 1; index < enums.Length; index++)
            {
                if (values.HasFlag(enums[index]))
                    list.Add(index);
            }

            return list.ToArray();
        }

        public static T ToEnum<T>(this long[] array)
            where T : struct
        {
            long bits = 0;
            if (array != null && array.Length > 0)
            {
                var enums = Enum.GetValues(typeof(T)).Cast<long>().ToArray();
                for (int index = 0; index < array.Length; index++)
                {
                    var item = array[index];
                    if (item >= 0 && item < enums.Length)
                        bits += (long)enums[item];
                }
            }

            return (T)Enum.ToObject(typeof(T), bits);
        }
    }
}