﻿using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    public static class IDataDeleteExtension
    {
        private static int ErrorMask = 0;
        public static int ReferencedObjectError = ErrorMask.GetResId(99);

        public static void DeleteObject(this IDataDelete data, ValidationResult result)
        {
            try
            {
                data.DeleteObject();
            }
            catch (ChildRecordFoundException)
            {
                result.AddValidation(new ValidationError(ReferencedObjectError, "Cannot delete a referenced object"));
            }
        }
    }
}