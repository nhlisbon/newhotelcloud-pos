﻿using System;
using System.Linq;
using System.Resources;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using NewHotel.Contracts;
using NewHotel.Localization;
using NewHotel.DataAnnotations;

namespace NewHotel.Business
{
    public static class ValidationResultExtension
    {
        private static readonly ResourceManager manager;

        static ValidationResultExtension()
        {
            manager = LocalizedText.Manager;
        }

        private const string ResIdFormat = "{0}.{1}";
        private static string FormatedCode(int code)
        {
            return string.Format(ResIdFormat, code.GetLowWord().ToString("D3"), code.GetHighWord().ToString());
        }

        private static CultureInfo GetCulture(long culture)
        {
            return culture == 0 ? CultureInfo.CurrentUICulture : new CultureInfo((int)culture);
        }

        private static string FormatMessage(string message, object[] args)
        {
            if (message != null && args != null)
            {
                try
                {
                    return string.Format(message, args);
                }
                catch
                {
                }
            }

            return message;
        }

        private static string GetMessage(int code, CultureInfo culture)
        {
            return manager.GetString(FormatedCode(code), culture);
        }

        public static T AddError<T>(this T validationResult, int code, string error, params object[] args)
            where T : ValidationResult
        {
            var message = GetMessage(code, GetCulture(validationResult.Culture)) ?? "[" + error + "]";
            var val = new ValidationError(code, FormatMessage(message, args));
            if (!validationResult.Contains(val))
                validationResult.AddValidation(val);

            return validationResult;
        }

        public static T AddError<T>(this T validationResult, string error, params object[] args)
            where T : ValidationResult
        {
            var val = new ValidationError(-1, FormatMessage(error, args));
            validationResult.AddValidation(val);

            return validationResult;
        }

        public static T AddError<T>(this T validationResult, int code, string error)
            where T : ValidationResult
        {
            return AddError(validationResult, code, error, null);
        }

        public static T AddException<T>(this T validationResult, int code, Exception exception)
            where T : ValidationResult
        {
            var val = new ValidationError(0, exception.InnerMessage(), exception.ToLogString());
            validationResult.AddValidation(val);
            return validationResult;
        }

        public static T AddException<T>(this T validationResult, Exception exception)
            where T : ValidationResult
        {
            return validationResult.AddException(0, exception);
        }

        public static T AddWarning<T>(this T validationResult, int code, string warning, params object[] args)
            where T : ValidationResult
        {
            if (!validationResult.IgnoreWarnings)
            {
                var message = GetMessage(code, GetCulture(validationResult.Culture)) ?? "[" + warning + "]";
                var val = new ValidationWarning(code, FormatMessage(message, args));
                if (!validationResult.Contains(val))
                    validationResult.AddValidation(val);
            }

            return validationResult;
        }

        public static T AddWarning<T>(this T validationResult, string warning, params object[] args)
            where T : ValidationResult
        {
            if (!validationResult.IgnoreWarnings)
            {
                var val = new ValidationWarning(-1, FormatMessage(warning, args));
                validationResult.AddValidation(val);
            }

            return validationResult;
        }

        public static T AddWarning<T>(this T validationResult, int code, string warning)
            where T : ValidationResult
        {
            return AddWarning(validationResult, code, warning, null);
        }

        #region Warning Assertions Members

        private const int WarningMask = 0;

        public static bool AssertWarning(this ValidationResult validationResult, bool condition, int code, string message, params string[] names)
        {
            if (condition)
                validationResult.AddWarning((int)code, message, names);

            return condition;
        }
        public static bool AssertWarning(this ValidationResult validationResult, bool condition, int code, string message)
        {
            if (condition)
                validationResult.AddWarning((int)code, message);

            return condition;
        }

        #endregion
        #region Error Assertions Members

        public const int ErrorMask = 0;

        public static bool AssertError(this ValidationResult validationResult, bool condition, int code, string message, params string[] names)
        {
            if (condition)
                validationResult.AddError((int)code, message, names);

            return condition;
        }
        public static bool AssertError(this ValidationResult validationResult, bool condition, int code, string message)
        {
            if (condition)
                validationResult.AddError((int)code, message);

            return condition;
        }

        public static int NotNullError = ErrorMask.GetResId(1);
        public static bool AssertNotNull<T>(this ValidationResult validationResult, T value, string name)
            where T : class
        {
            if (value == null)
            {
                validationResult.AddError(NotNullError, "Must have {0}", name);
                return false;
            }

            return true;
        }

        public static bool AssertNotNull<T>(this ValidationResult validationResult, T? value, string name)
            where T : struct
        {
            if (!value.HasValue)
            {
                validationResult.AddError(NotNullError, "Must have {0}", name);
                return false;
            }

            return true;
        }

        public static int NullError = ErrorMask.GetResId(2);
        public static bool AssertNull<T>(this ValidationResult validationResult, T value, string name)
            where T : class
        {
            if (value != null)
            {
                validationResult.AddError(NullError, "Must not have {0}", name);
                return false;
            }

            return true;
        }
        public static bool AssertNull<T>(this ValidationResult validationResult, T? value, string name)
            where T : struct
        {
            if (value.HasValue)
            {
                validationResult.AddError(NullError, "Must not have {0}", name);
                return false;
            }

            return true;
        }

        public static int EmptyError = ErrorMask.GetResId(3);
        public static bool AssertEmpty(this ValidationResult validationResult, string value, string name)
        {
            if ((value ?? string.Empty) != string.Empty)
            {
                validationResult.AddError(EmptyError, "{0} must be empty", name);
                return false;
            }

            return true;
        }
        public static bool AssertEmpty<T>(this ValidationResult validationResult, IList<T> list, string name)
        {
            if (list != null && list.Count != 0)
            {
                validationResult.AddError(EmptyError, "{0} must be empty", name);
                return false;
            }

            return true;
        }

        public static int NotEmptyError = ErrorMask.GetResId(4);
        public static bool AssertNotEmpty(this ValidationResult validationResult, string value, string name)
        {
            if ((value ?? string.Empty).Equals(string.Empty))
            {
                validationResult.AddError(NotEmptyError, "{0} can't be empty", name);
                return false;
            }

            return true;
        }
        public static bool AssertNotEmpty(this ValidationResult validationResult, Guid value, string name)
        {
            if (value.Equals(Guid.Empty))
            {
                validationResult.AddError(NotEmptyError, "{0} can't be empty", name);
                return false;
            }

            return true;
        }
        public static bool AssertNotEmpty(this ValidationResult validationResult, ICollection coll, string name)
        {
            if (coll == null || coll.Count == 0)
            {
                validationResult.AddError(NotEmptyError, "{0} can't be empty", name);
                return false;
            }

            return true;
        }
        public static bool AssertNotEmpty<T>(this ValidationResult validationResult, ICollection<T> coll, string name)
        {
            if (coll == null || coll.Count == 0)
            {
                validationResult.AddError(NotEmptyError, "{0} can't be empty", name);
                return false;
            }

            return true;
        }
        public static bool AssertNotEmpty(this ValidationResult validationResult, IList list, string name)
        {
            if (list == null || list.Count == 0)
            {
                validationResult.AddError(NotEmptyError, "{0} can't be empty", name);
                return false;
            }

            return true;
        }
        public static bool AssertNotEmpty<T>(this ValidationResult validationResult, IList<T> list, string name)
        {
            if (list == null || list.Count == 0)
            {
                validationResult.AddError(NotEmptyError, "{0} can't be empty", name);
                return false;
            }

            return true;
        }

        public static int EqualError = ErrorMask.GetResId(5);
        public static bool AssertEqual<T>(this ValidationResult validationResult, T value1, string name1, T value2, string name2)
            where T : IComparable
        {
            if ((value1 == null && value2 != null) || (value1 != null && value2 == null) || (value1 != null && value2 != null && value1.CompareTo(value2) != 0))
            {
                validationResult.AddError(EqualError, "{0} and {1} must be equals", name1, name2);
                return false;
            }

            return true;
        }
        public static bool AssertEqual<T>(this ValidationResult validationResult, T value1, string name, T value2)
            where T : IComparable
        {
            if ((value1 == null && value2 != null) || (value1 != null && value2 == null) || (value1 != null && value2 != null && value1.CompareTo(value2) != 0))
            {
                validationResult.AddError(EqualError, "{0} must be equal to {1}", name, value2);
                return false;
            }

            return true;
        }
        public static bool AssertEqual<T>(this ValidationResult validationResult, T? value1, string name1, T? value2, string name2)
            where T : struct, IComparable
        {
            if ((value1.HasValue && !value2.HasValue) || (!value1.HasValue && value2.HasValue) || (value1.HasValue && value2.HasValue && value1.Value.CompareTo(value2) != 0))
            {
                validationResult.AddError(EqualError, "{0} and {1} must be equals", name1, name2);
                return false;
            }

            return true;
        }
        public static bool AssertEqual<T>(this ValidationResult validationResult, T? value1, string name, T? value2)
            where T : struct, IComparable
        {
            if ((value1.HasValue && !value2.HasValue) || (!value1.HasValue && value2.HasValue) || (value1.HasValue && value2.HasValue && value1.Value.CompareTo(value2) != 0))
            {
                validationResult.AddError(EqualError, "{0} must be equal to {1}", name, value2);
                return false;
            }

            return true;
        }

        public static int NotEqualError = ErrorMask.GetResId(6);
        public static bool AssertNotEqual<T>(this ValidationResult validationResult, T value1, string name1, T value2, string name2)
            where T : IComparable
        {
            if (value1.CompareTo(value2) == 0)
            {
                validationResult.AddError(NotEqualError, "{0} and {1} must be not equals", name1, name2);
                return false;
            }

            return true;
        }

        public static bool AssertNotEqual<T>(this ValidationResult validationResult, T? value1, string name1, T? value2, string name2)
            where T : struct, IComparable
        {
            if ((!value1.HasValue && !value2.HasValue) || (value1.HasValue && value1.Value.CompareTo(value2) == 0))
            {
                validationResult.AddError(NotEqualError, "{0} and {1} must be not equals", name1, name2);
                return false;
            }

            return true;
        }

        public static int LessThanError = ErrorMask.GetResId(7);
        public static bool AssertLessThan<T>(this ValidationResult validationResult, T value1, string name1, T value2, string name2)
            where T : IComparable
        {
            if (value1.CompareTo(value2) >= 0)
            {
                validationResult.AddError(LessThanError, "{0} must be less than {1}", name1, name2);
                return false;
            }

            return true;
        }
        public static bool AssertLessThan<T>(this ValidationResult validationResult, T? value1, string name1, T? value2, string name2)
            where T : struct, IComparable
        {
            if (value1.HasValue && value2.HasValue && value1.Value.CompareTo(value2.Value) >= 0)
            {
                validationResult.AddError(LessThanError, "{0} must be less than {1}", name1, name2);
                return false;
            }

            return true;
        }

        public static int LessThanOrEqualError = ErrorMask.GetResId(8);
        public static bool AssertLessOrEqual<T>(this ValidationResult validationResult, T value1, string name1, T value2, string name2)
            where T : IComparable
        {
            if (value1.CompareTo(value2) > 0)
            {
                validationResult.AddError(LessThanOrEqualError, "{0} must be less than or equal to {1}", name1, name2);
                return false;
            }

            return true;
        }
        public static bool AssertLessOrEqual<T>(this ValidationResult validationResult, T? value1, string name1, T? value2, string name2)
            where T : struct, IComparable
        {
            if (value1.HasValue && value2.HasValue && value1.Value.CompareTo(value2.Value) > 0)
            {
                validationResult.AddError(LessThanOrEqualError, "{0} must be less than or equal to {1}", name1, name2);
                return false;
            }

            return true;
        }

        public static int GreaterThanError = ErrorMask.GetResId(9);
        public static bool AssertGreaterThan<T>(this ValidationResult validationResult, T value1, string name1, T value2, string name2)
            where T : IComparable
        {
            if (value1.CompareTo(value2) <= 0)
            {
                validationResult.AddError(GreaterThanError, "{0} must be greater than {1}", name1, name2);
                return false;
            }

            return true;
        }
        public static bool AssertGreaterThan<T>(this ValidationResult validationResult, T? value1, string name1, T? value2, string name2)
            where T : struct, IComparable
        {
            if (value1.HasValue && value2.HasValue && value1.Value.CompareTo(value2.Value) <= 0)
            {
                validationResult.AddError(GreaterThanError, "{0} must be greater than {1}", name1, name2);
                return false;
            }

            return true;
        }
        public static bool AssertGreaterThan<T>(this ValidationResult validationResult, T? value1, string name1, T value2, string name2)
            where T : struct, IComparable
        {
            if (value1.HasValue && value1.Value.CompareTo(value2) <= 0)
            {
                validationResult.AddError(GreaterThanError, "{0} must be greater than {1}", name1, name2);
                return false;
            }

            return true;
        }

        public static int GreaterOrEqualError = ErrorMask.GetResId(10);
        public static bool AssertGreaterOrEqual<T>(this ValidationResult validationResult, T value1, string name1, T value2, string name2)
            where T : IComparable
        {
            if (value1.CompareTo(value2) < 0)
            {
                validationResult.AddError(GreaterOrEqualError, "{0} must be greater than or equal to {1}", name1, name2);
                return false;
            }

            return true;
        }
        public static bool AssertGreaterOrEqual<T>(this ValidationResult validationResult, T? value1, string name1, T? value2, string name2)
            where T : struct, IComparable
        {
            if (value1.HasValue && value2.HasValue && value1.Value.CompareTo(value2.Value) < 0)
            {
                validationResult.AddError(GreaterOrEqualError, "{0} must be greater than or equal to {1}", name1, name2);
                return false;
            }

            return true;
        }

        public static int PositiveError = ErrorMask.GetResId(11);
        public static bool AssertPositive(this ValidationResult validationResult, IComparable value, string name)
        {
            if (value != null && value.CompareTo(Convert.ChangeType(0, value.GetType(), null)) <= 0)
            {
                validationResult.AddError(PositiveError, "{0} must be a positive value", name);
                return false;
            }

            return true;
        }
        public static bool AssertPositive(this ValidationResult validationResult, decimal value, string name)
        {
            if (value <= 0)
            {
                validationResult.AddError(PositiveError, "{0} must be a positive value", name);
                return false;
            }

            return true;
        }
        public static bool AssertPositive(this ValidationResult validationResult, decimal? value, string name)
        {
            if (value.HasValue && value.Value <= 0)
            {
                validationResult.AddError(PositiveError, "{0} must be a positive value", name);
                return false;
            }

            return true;
        }

        public static int NegativeError = ErrorMask.GetResId(12);
        public static bool AssertNegative(this ValidationResult validationResult, decimal value, string name)
        {
            if (value <= 0)
            {
                validationResult.AddError(NegativeError, "{0} must be a negative value", name);
                return false;
            }

            return true;
        }
        public static bool AssertNegative(this ValidationResult validationResult, decimal? value, string name)
        {
            if (value.HasValue && value.Value <= 0)
            {
                validationResult.AddError(NegativeError, "{0} must be a negative value", name);
                return false;
            }

            return true;
        }

        public static int ZeroError = ErrorMask.GetResId(13);
        public static bool AssertZero(this ValidationResult validationResult, decimal value, string name)
        {
            if (value != 0)
            {
                validationResult.AddError(ZeroError, "{0} must be cero", name);
                return false;
            }

            return true;
        }
        public static bool AssertZero(this ValidationResult validationResult, decimal? value, string name)
        {
            if (value.HasValue && value.Value != 0)
            {
                validationResult.AddError(ZeroError, "{0} must be cero", name);
                return false;
            }

            return true;
        }

        public static int NonNegativeError = ErrorMask.GetResId(14);
        public static bool AssertNonNegative(this ValidationResult validationResult, IComparable value, string name)
        {
            if (value != null && value.CompareTo(Convert.ChangeType(0, value.GetType(), null)) < 0)
            {
                validationResult.AddError(NonNegativeError, "{0} must be a non negative value", name);
                return false;
            }

            return true;
        }
        public static bool AssertNonNegative(this ValidationResult validationResult, decimal value, string name)
        {
            if (value < 0)
            {
                validationResult.AddError(NonNegativeError, "{0} must be a non negative value", name);
                return false;
            }

            return true;
        }
        public static bool AssertNonNegative(this ValidationResult validationResult, decimal? value, string name)
        {
            if (value.HasValue && value.Value < 0)
            {
                validationResult.AddError(NonNegativeError, "{0} must be a non negative value", name);
                return false;
            }

            return true;
        }

        public static int NonPositiveError = ErrorMask.GetResId(15);
        public static bool AssertNonPositive(this ValidationResult validationResult, decimal value, string name)
        {
            if (value > 0)
            {
                validationResult.AddError(NonPositiveError, "{0} must be a non positive value", name);
                return false;
            }

            return true;
        }
        public static bool AssertNonPositive(this ValidationResult validationResult, decimal? value, string name)
        {
            if (value.HasValue && value.Value > 0)
            {
                validationResult.AddError(NonPositiveError, "{0} must be a non positive value", name);
                return false;
            }

            return true;
        }

        public static int RangeError = ErrorMask.GetResId(16);
        public static bool AssertRange<T>(this ValidationResult validationResult, T value, T min, T max, string name)
            where T : IComparable
        {
            if ((value.CompareTo(min) < 0) || (value.CompareTo(max) > 0))
            {
                validationResult.AddError(RangeError, "{0} must be between {1} and {2}", name, min.ToString(), max.ToString());
                return false;
            }

            return true;
        }
        public static bool AssertRange<T>(this ValidationResult validationResult, T? value, T min, T max, string name)
            where T : struct, IComparable
        {
            if (value.HasValue && (value.Value.CompareTo(min) < 0 || value.Value.CompareTo(max) > 0))
            {
                validationResult.AddError(RangeError, "{0} must be between {1} and {2}", name, min.ToString(), max.ToString());
                return false;
            }

            return true;
        }

        public static int TodayError = ErrorMask.GetResId(17);
        public static bool AssertToday(this ValidationResult validationResult, DateTime value, string name)
        {
            if (value.Date != DateTime.Today)
            {
                validationResult.AddError(TodayError, "{0} is not today", name);
                return false;
            }

            return true;
        }
        public static bool AssertToday(this ValidationResult validationResult, DateTime? value, string name)
        {
            if (value.HasValue && value.Value.Date != DateTime.Today)
            {
                validationResult.AddError(TodayError, "{0} is not today", name);
                return false;
            }

            return true;
        }

        public static int PastError = ErrorMask.GetResId(18);
        public static bool AssertPast(this ValidationResult validationResult, DateTime value, string name)
        {
            if (value.Date >= DateTime.Today)
            {
                validationResult.AddError(PastError, "{0} is not a past date", name);
                return false;
            }

            return true;
        }
        public static bool AssertPast(this ValidationResult validationResult, DateTime? value, string name)
        {
            if (value.HasValue && value.Value.Date >= DateTime.Today)
            {
                validationResult.AddError(PastError, "{0} is not a past date", name);
                return false;
            }

            return true;
        }

        public static int FutureError = ErrorMask.GetResId(19);
        public static bool AssertFuture(this ValidationResult validationResult, DateTime value, string name)
        {
            if (value.Date <= DateTime.Today)
            {
                validationResult.AddError(FutureError, "{0} is not a future date", name);
                return false;
            }

            return true;
        }
        public static bool AssertFuture(this ValidationResult validationResult, DateTime? value, string name)
        {
            if (value.HasValue && value.Value.Date <= DateTime.Today)
            {
                validationResult.AddError(FutureError, "{0} is not a future date", name);
                return false;
            }

            return true;
        }

        public static int PeriodError = ErrorMask.GetResId(20);
        public static bool AssertPeriod<T>(this ValidationResult validationResult, T initial, string name1, T final, string name2)
            where T : IComparable
        {
            if (initial.CompareTo(final) > 0)
            {
                validationResult.AddError(PeriodError, "{0} is greater than {1}", name1, name2);
                return false;
            }

            return true;
        }
        public static bool AssertPeriod<T>(this ValidationResult validationResult, T? initial, string name1, T? final, string name2)
            where T : struct, IComparable
        {
            if (initial.HasValue && final.HasValue && initial.Value.CompareTo(final) > 0)
            {
                validationResult.AddError(PeriodError, "{0} is greater than {1}", name1, name2);
                return false;
            }

            return true;
        }

        public static int UndefinedError = ErrorMask.GetResId(21);
        public static bool AssertUndefined<T>(this ValidationResult validationResult, T value, string name, params T[] undefinedValues)
            where T : IComparable
        {
            if (undefinedValues.Contains(value))
            {
                validationResult.AddError(UndefinedError, "{0} is undefined", name);
                return false;
            }

            return true;
        }
        public static bool AssertUndefined<T>(this ValidationResult validationResult, T? value, string name, params T[] undefinedValues)
            where T : struct
        {
            if (!value.HasValue || undefinedValues.Contains(value.Value))
            {
                validationResult.AddError(UndefinedError, "{0} is undefined", name);
                return false;
            }

            return true;
        }

        public static int InListError = ErrorMask.GetResId(22);
        public static bool AssertInList<T>(this ValidationResult validationResult, T value, string name, params T[] valueList)
            where T : IComparable
        {
            if (!valueList.Contains(value))
            {
                validationResult.AddError(InListError, "{0} must have one of the following values {1}", name,
                    string.Join(",", valueList.Select<T, string>(x => x.ToString()).ToArray()));

                return false;
            }

            return true;
        }
        public static bool AssertInList<T>(this ValidationResult validationResult, T? value, string name, params T[] valueList)
            where T : struct, IComparable
        {
            if (!value.HasValue || !valueList.Contains(value.Value))
            {
                validationResult.AddError(InListError, "{0} must have one of the following values {1}", name,
                    string.Join(",", valueList.Select<T, string>(x => x.ToString()).ToArray()));

                return false;
            }

            return true;
        }

        public static int NotInListError = ErrorMask.GetResId(23);
        public static bool AssertNotInList<T>(this ValidationResult validationResult, T value, string name, params T[] valueList)
            where T : IComparable
        {
            if (valueList.Contains(value))
            {
                validationResult.AddError(NotInListError, "{0} can't be equal to one of the following values {1}", name,
                    string.Join(",", valueList.Select<T, string>(x => x.ToString()).ToArray()));
                return false;
            }

            return true;
        }
        public static bool AssertNotInList<T>(this ValidationResult validationResult, T? value, string name, params T[] valueList)
            where T : struct
        {
            if (value.HasValue && valueList.Contains(value.Value))
            {
                validationResult.AddError(NotInListError, "{0} can't be equal to one of the following values {1}", name,
                    string.Join(",", valueList.Select<T, string>(x => x.ToString()).ToArray()));

                return false;
            }

            return true;
        }

        public static int LengthError = ErrorMask.GetResId(24);
        public static bool AssertLength(this ValidationResult validationResult, string value, byte min, int max, string name)
        {
            if (value == null || (value.Length < min || value.Length > max))
            {
                if (min == max)
                    validationResult.AddError(LengthError, "{0} length must be equal to {1} characters", name, min);
                else
                    validationResult.AddError(LengthError, "{0} length must be between {1} and {2} characters", name, min, max);

                return false;
            }

            return true;
        }
        public static bool AssertMinLength(this ValidationResult validationResult, string value, int min, string name)
        {
            if (value == null || value.Length < min)
            {
                validationResult.AddError(LengthError, "{0} must have at least {1} characters", name, min);
                return false;
            }

            return true;
        }
        public static bool AssertMaxLength(this ValidationResult validationResult, string value, int max, string name)
        {
            if (value == null || value.Length > max)
            {
                validationResult.AddError(LengthError, "{0} cannot have more than {1} characters", name, max);
                return false;
            }

            return true;
        }

        public static int NIBError = ErrorMask.GetResId(25);
        public static bool AssertNIB(this ValidationResult validationResult, string nib, string name)
        {
            var isValid = false;
            if (nib.Length == 21)
            {
                var value = long.Parse(nib.Substring(0, 19) + "00");
                isValid = (98 - (value % 97)) == long.Parse(nib.Substring(19, 2));
            }

            if (!isValid)
            {
                validationResult.AddError(NIBError, "Invalid {0} NIB: {1}", name, nib);
                return false;
            }

            return true;
        }

        public static int IBANError = ErrorMask.GetResId(26);
        public static bool AssertIBAN(this ValidationResult validationResult, string iban, string name)
        {
            var isValid = false;
            if (iban.Length <= 34)
            {
                isValid = true;
            }
            else
                isValid = false;


            if (!isValid)
            {
                validationResult.AddError(IBANError, "Invalid {0} IBAN: {1}", name, iban);
                return false;
            }

            return true;
        }

        public static int EmailError = ErrorMask.GetResId(27);
        public static bool AssertEmail(this ValidationResult validationResult, string email, string name)
        {
            var isValid = false;
            int ampersand = email.IndexOf('@');
            isValid = ((ampersand > 0) && (email.IndexOf('.', ampersand + 1) != email.Length));

            if (!isValid)
            {
                validationResult.AddError(EmailError, "Invalid {0} email", name);
                return false;
            }

            return true;
        }

        public static int DuplicatedError = ErrorMask.GetResId(28);
        public static bool AssertDuplicated(this ValidationResult validationResult, bool cond, string name)
        {
            if (!cond)
            {
                validationResult.AddError(DuplicatedError, "Duplicated {0}", name);
                return false;
            }

            return true;
        }

        #endregion
    }
}