﻿using System;

namespace NewHotel.Business
{
    public static class DecimalExtension
    {
        public static decimal Trunc(this decimal value, short decimals)
        {
            var power = new decimal(Math.Pow(10, decimals));
            return Math.Truncate(value * power) / power;
        }

        public static decimal RoundTo(this decimal value, short decimals)
        {
            return decimal.Round(value, decimals, MidpointRounding.AwayFromZero);
        }
    }
}