﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Business
{
    public class GroupHotelQuery : Query
    {
        public GroupHotelQuery()
            : base("GroupHotel") { }

        public GroupHotelQuery(IDatabaseManager manager)
            : base("GroupHotel", manager) { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();
            
            sb.AppendLine("select comp_pk, comp_desc from tnht_comp");

            return sb.ToString();
        }

        protected override void Initialize()
        {
            Filters
               .Add("comp_desc", " comp_desc", typeof(string), FilterTypes.Like);

            Sorts.AddAsc("comp_desc");
        }
    }
}
