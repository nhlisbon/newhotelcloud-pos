﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    public class TaxRateAppQuery : Query
    {
        public TaxRateAppQuery()
            : base("TaxRateApp") { }

        public TaxRateAppQuery(IDatabaseManager manager)
            : base("TaxRateApp", manager) { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();
            
            sb.AppendLine("select tiva.tiva_pk, regi.regi_pk, regi.naci_pk, seim.seim_pk, seim.seim_abre, tiva.tiva_perc,");
            sb.AppendLine("       seim.seim_abre || ' ' || TO_CHAR(tiva.tiva_perc, '99.99') || '%' as tiva_name,");
            sb.AppendLine("      (select mult_desc from vnht_mult where lite_pk = tiva.lite_pk and lang_pk = :lang_pk) as tiva_desc,");
            sb.AppendLine("      (select mult_desc from vnht_mult where lite_pk = enum.lite_pk and lang_pk = :lang_pk) as dire_desc,");
            sb.AppendLine("      (select mult_desc from vnht_mult where lite_pk = naci.lite_desc and lang_pk = :lang_pk) ||'/' || (select mult_desc from vnht_mult where lite_pk = regi.lite_pk and lang_pk = :lang_pk) as naci_regi");
            sb.AppendLine("from  tnht_tiva tiva, tnht_regi regi, tnht_seim seim, tnht_enum enum, tnht_naci naci");
            sb.AppendLine("where tiva.regi_pk = regi.regi_pk");
            sb.AppendLine("and   tiva.seim_pk = seim.seim_pk");         
            sb.AppendLine("and   regi.naci_pk = naci.naci_pk");

            return sb.ToString();
        }

        protected override void Initialize()
        {
            Filters
                .Add("tiva_pk", "tiva.tiva_pk", typeof(Guid))
                .AddTranslation("tiva_desc", "tiva.lite_pk")
                .Add("regi_pk", "regi.regi_pk", typeof(Guid))
                .Add("seim_pk", "seim.seim_pk", typeof(Guid))
                .Add("naci_pk", "regi.naci_pk", typeof(Guid));

            Sorts.AddAsc("tiva_perc");
        }
    }
}
