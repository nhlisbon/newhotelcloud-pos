﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    public class TaxRegionAppQuery : Query
    {
        public TaxRegionAppQuery()
            : base("TaxRegionApp") { }

        public TaxRegionAppQuery(IDatabaseManager manager)
            : base("TaxRegionApp", manager) { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select regi_pk, regi_lite_pk, regi_desc, naci_pk, naci_lite_pk, naci_desc,");
            sb.AppendLine("       naci_desc || '/' || regi_desc as naci_regi");
            sb.AppendLine("from(");
            sb.AppendLine("     select  regi.regi_pk, regi.lite_pk as regi_lite_pk, regi.naci_pk, naci.lite_desc as naci_lite_pk,");
            sb.AppendLine("             (select mult_desc from vnht_mult where lite_pk = regi.lite_pk and lang_pk = :lang_pk) as regi_desc,");
            sb.AppendLine("             (select mult_desc from vnht_mult where lite_pk = naci.lite_desc and lang_pk = :lang_pk) as naci_desc");
            sb.AppendLine("     from tnht_regi regi, tnht_naci naci");
            sb.AppendLine("     where regi.naci_pk = naci.naci_pk)"); 

            return sb.ToString();
        }

        protected override void Initialize()
        {
            Filters
                .Add("regi_pk", "regi_pk", typeof(Guid))
                 .Add("naci_pk", "naci_pk", typeof(Guid))
                .AddTranslation("regi_desc", "regi_lite_pk");
        }
    }
}
