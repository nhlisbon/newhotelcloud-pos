﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    public class HotelByUserQuery : Query
    {
        public HotelByUserQuery()
            : base("HotelByUser") { }

        public HotelByUserQuery(IDatabaseManager manager)
            : base("HotelByUser", manager) { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select  hote.hote_desc,hote.hote_pk,  hout.hout_inac, hout.hout_pk");
            sb.AppendLine("from  tnht_hote hote, tnht_hout hout");
            sb.AppendLine("where hout.hote_pk =  hote.hote_pk");

            return sb.ToString();
        }

        protected override void Initialize()
        {
            Filters
                .Add("hote_pk", "hote.hote_pk", typeof(Guid))
                .Add("hout_pk", "hout.hout_pk", typeof(Guid))
                .Add("in_hote", "hote.hote_pk", typeof(Guid), FilterTypes.In)
               .Add("not_in_hote", "hote.hote_pk", typeof(Guid), FilterTypes.NotIn);
        }
    }
}
