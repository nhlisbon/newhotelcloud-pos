﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    public class CountryAppQuery : Query
    {
        public CountryAppQuery()
            : base("CountryApp") { }

        public CountryAppQuery(IDatabaseManager manager)
            : base("CountryApp", manager) { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select naci.naci_pk as naci_pk, naci.remu_pk remu_pk, ");
            sb.AppendLine("      (select mult_desc from vnht_mult where lite_pk = naci.lite_desc and lang_pk = :lang_pk) as naci_desc");
            sb.AppendLine("from   tnht_naci naci");
            
            return sb.ToString();
        }

        protected override void Initialize()
        {
            Filters
                .AddTranslation("naci_desc", "naci.lite_desc");

           
        }
    }
}
