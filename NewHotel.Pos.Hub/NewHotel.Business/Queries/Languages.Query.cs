﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    public class LanguagesQuery : Query
    {
        public LanguagesQuery()
            : base("Languages") { }

        public LanguagesQuery(IDatabaseManager manager)
            : base("Languages", manager) { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select lang.lang_pk,");
            sb.AppendLine("       (select mult.mult_desc from tnht_mult mult where mult.lite_pk = licl.lite_pk and mult.lang_pk = :language_pk) as lang_desc");
            sb.AppendLine("from   tnht_lang lang inner join tnht_licl licl on licl.licl_pk = lang.lang_pk");
            sb.AppendLine("order by lang.lang_prio");

            return sb.ToString();
        }
    }
}
