﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Common;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    public class AdminQueryContainer : BaseQueryContainer
    {
        public static readonly AdminQueryContainer Instance = new AdminQueryContainer();
        private AdminQueryContainer() : base() { }

        #region Queries

        public Query GetInstallationsByUser(IDatabaseManager manager) { return new InstallationsByUserQuery(manager); }
        public Query GetLanguages(IDatabaseManager manager) { return new LanguagesQuery(manager); }
        public Query GetUser(IDatabaseManager manager) { return new UserQuery(manager); }
        public Query GetUserSearch(IDatabaseManager manager) { return new UserSearchQuery(manager); }
        public Query GetPermissionbyRol(IDatabaseManager manager) { return new PermissionbyRolQuery(manager); }
        public Query GetHotel(IDatabaseManager manager) { return new HotelQuery(manager); }
        public Query GetHotelByUser(IDatabaseManager manager) { return new HotelByUserQuery(manager); }
        public Query GetRole(IDatabaseManager manager) { return new RolesQuery(manager); }
        public Query GetGroupHotel(IDatabaseManager manager) { return new GroupHotelQuery(manager);}
        public Query GetApplication(IDatabaseManager manager) { return new ApplicationQuery(manager); }
        public Query GetCurrency(IDatabaseManager manager) { return new CurrencySearchQuery(manager); }

        public Query GetTaxRatesApp(IDatabaseManager manager) { return new TaxRateAppQuery(manager); }
        public Query GetTaxRegionsApp(IDatabaseManager manager) { return new TaxRegionAppQuery(manager); }
        public Query GetTaxSequencesApp(IDatabaseManager manager) { return new TaxSequenceAppQuery(manager); }

        public Query GetCountries(IDatabaseManager manager) { return new CountryAppQuery(manager); }

        #endregion
    }
}
