﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    public class CurrencySearchQuery : Query
    {
        public CurrencySearchQuery()
            : base("CurrencySearch") { }

        public CurrencySearchQuery(IDatabaseManager manager)
            : base("CurrencySearch", manager) { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();


           sb.AppendLine(" select unmo.unmo_pk, ");
           sb.AppendLine("(select mult_desc   from vnht_mult where lite_pk = unmo.lite_pk and lang_pk = :lang_pk)  as unmo_desc");
           sb.AppendLine(" from tnht_unmo unmo");

            return sb.ToString();
        }

        protected override void Initialize()
        {

            Filters
                .Add("unmo_pk", "unmo.unmo_pk", typeof(string))
                .Add("unmo_desc", "unmo_desc", typeof(string));

            Sorts.AddAsc("unmo_desc");
        }
    }
}
