﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    public class InstallationsByUserQuery : Query
    {
        public InstallationsByUserQuery()
            : base("InstallationsByUser") { }

        public InstallationsByUserQuery(IDatabaseManager manager)
            : base("InstallationsByUser", manager) { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select hote.hote_pk, hote.hote_desc");
            sb.AppendLine("from   tnht_hote hote inner join tnht_hout hout on hout.hote_pk = hote.hote_pk");
            sb.AppendLine("where  hout.hout_inac = :hout_inac and hout.util_pk = :util_pk");

            return sb.ToString();
        }

        protected override void Initialize()
        {
            Parameters["hout_inac"] = false;
        }
    }
}
