﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    public class PermissionbyRolQuery : Query
    {
        public PermissionbyRolQuery()
            : base("PermissionbyApp") { }

        public PermissionbyRolQuery(IDatabaseManager manager)
            : base("PermissionbyApp", manager) { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select perm.perm_pk, perm.perm_orde, app.appl_name, rol.role_pk, grpr.grpr_pk, app.appl_pk, rope.rope_pk, rope.secu_code, peap.peap_pk,");
            sb.AppendLine("      (select mult_desc  from vnht_mult   where lite_pk = perm.lite_pk and lang_pk = :lang_pk)  as perm_desc,");
            sb.AppendLine("      (select mult_desc  from vnht_mult   where lite_pk = rol.lite_pk and lang_pk = :lang_pk)   as role_desc,");
            sb.AppendLine("      (select mult_desc  from vnht_mult   where lite_pk = grpr.lite_pk and lang_pk = :lang_pk)    as grpr_desc,");
            sb.AppendLine("      (select mult_desc from vnht_mult where lite_pk = enum_securitycode.lite_pk and lang_pk = :lang_pk) as secu_code_desc");
            sb.AppendLine("from   tnht_peap peap, tnht_rope rope, tnht_appl app, tnht_perm perm, tnht_grpr grpr, tnht_role rol, tnht_enum enum_securitycode");
            sb.AppendLine("where  peap.perm_pk = perm.perm_pk");
            sb.AppendLine("and    peap.appl_pk = app.appl_pk");
            sb.AppendLine("and    rol.role_pk = rope.role_pk");
            sb.AppendLine("and    grpr.grpr_pk = perm.grpr_pk");
            sb.AppendLine("and    rope.peap_pk = peap.peap_pk");
            sb.AppendLine("and    rope.secu_code = enum_securitycode.enum_pk");

           return sb.ToString();
        }

        protected override void Initialize()
        {
            Filters
               .Add("perm_pk", "perm.perm_pk", typeof(Guid))
               .Add("role_pk", "rol.role_pk", typeof(long))
               .Add("appl_name", " app.appl_name", typeof(string), FilterTypes.Like)
               .Add("grpr_desc", " grpr_descc", typeof(string), FilterTypes.Like);
        } 
    }
}
