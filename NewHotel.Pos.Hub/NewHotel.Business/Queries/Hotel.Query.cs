﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Business
{
    public class HotelQuery : Query
    {
        public HotelQuery()
            : base("Hotel") { }

        public HotelQuery(IDatabaseManager manager)
            : base("Hotel", manager) { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select hote.hote_pk, hote.hote_desc, hote.hote_prov, hote.hote_serv, hote.hote_user, hote.hote_pwrd , comp.comp_desc, comp.comp_pk");
            sb.AppendLine("from  tnht_hote hote left outer join  tnht_comp comp on hote.comp_pk = comp.comp_pk");
            
            return sb.ToString();
        }

        protected override void Initialize()
        {
            Filters
               .Add("hote_desc", "hote.hote_desc", typeof(string), FilterTypes.Like)
               .Add("comp_desc", "comp.comp_desc", typeof(string), FilterTypes.Like)
               .Add("in_hote", "hote.hote_pk", typeof(Guid), FilterTypes.In)
               .Add("not_in_hote", "hote.hote_pk", typeof(Guid), FilterTypes.NotIn);

            Sorts.AddAsc("hote_desc");
        }
    }
}
