﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    public class UserSearchQuery : Query
    {
        public UserSearchQuery()
            : base("UserSearch") { }

        public UserSearchQuery(IDatabaseManager manager)
            : base("UserSearch", manager) { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

           sb.AppendLine("select util.util_pk,util.util_desc, util.util_pass, util.util_login, hote.hote_desc,hote.hote_pk, role.role_pk, hout.hout_inac, hout.hout_pk,");
           sb.AppendLine("      (select mult_desc from vnht_mult where lite_pk = role.lite_pk and lang_pk = :lang_pk) as role_desc");
           sb.AppendLine("from   tnht_util util, tnht_role role, tnht_hote hote, tnht_hout hout");
           sb.AppendLine("where  util.role_pk = role.role_pk");
           sb.AppendLine("and    util.util_pk =  hout.util_pk");
           sb.AppendLine("and    hout.hote_pk =  hote.hote_pk");

            return sb.ToString();
        }

        protected override void Initialize()
        {
            Filters
               .Add("hote_desc", "hote.hote_desc", typeof(string), FilterTypes.Like)
               .Add("util_pk", "util.util_pk", typeof(Guid))
               .Add("in_hout", "hout.hote_pk", typeof(Guid), FilterTypes.In)
               .Add("not_in_hout", "hout.hote_pk", typeof(Guid), FilterTypes.NotIn);

            Sorts.AddAsc("hote_desc");
        }
    }
}
