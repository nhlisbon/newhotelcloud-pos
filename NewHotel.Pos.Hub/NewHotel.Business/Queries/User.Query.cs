﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    public class UserQuery : Query
    {
        public UserQuery()
            : base("User") { }

        public UserQuery(IDatabaseManager manager)
            : base("User", manager) { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select util.util_pk, util.util_desc, util.util_pass,");
            sb.AppendLine("(select mult_desc from vnht_mult where lite_pk = role.lite_pk and lang_pk = :lang_pk) as role_desc");
            sb.AppendLine("from tnht_util util inner join tnht_role role on role.role_pk = util.role_pk");
            sb.AppendLine("where upper(util.util_login) = upper(:util_login)");

            return sb.ToString();
        }
    }
}
