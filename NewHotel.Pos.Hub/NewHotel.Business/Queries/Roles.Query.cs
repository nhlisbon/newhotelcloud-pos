﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Business
{
    public class RolesQuery : Query
    {
        public RolesQuery()
            : base("Roles") { }

        public RolesQuery(IDatabaseManager manager)
            : base("Roles", manager) { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("select role.role_pk as role_pk,");
            sb.AppendLine("      (select mult_desc from vnht_mult where lite_pk = role.lite_pk and lang_pk = :lang_pk) as role_desc");
            sb.AppendLine("from  tnht_role role");
            return sb.ToString();
        }

        protected override void Initialize()
        {
            Filters
               .Add("role_pk", " role.role_pk", typeof(string), FilterTypes.Like);

        }
    }
}
