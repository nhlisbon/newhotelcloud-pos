﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    public class TaxSequenceAppQuery : Query
    {
        public TaxSequenceAppQuery()
            : base("TaxSequenceApp") { }

        public TaxSequenceAppQuery(IDatabaseManager manager)
            : base("TaxSequenceApp", manager) { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select seim.seim_pk, seim.seim_abre,");
            sb.AppendLine(" (select mult_desc from vnht_mult where lite_pk = seim.lite_pk and lang_pk = :lang_pk) as seim_desc");
            sb.AppendLine("from tnht_seim seim");
           
            return sb.ToString();
        }        

        protected override void Initialize()
        {
            Filters
                .Add("seim_pk", "seim_pk", typeof(Guid))
                .Add("seim_abre", "seim_abre", typeof(string))
                .AddTranslation("seim_desc", "seim.lite_pk");
            
            Sorts.AddAsc("seim_desc");
        }
    }
}
