﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Business
{
    public class ApplicationQuery : Query
    {
        public ApplicationQuery()
            : base("Application") { }

        public ApplicationQuery(IDatabaseManager manager)
            : base("Application", manager) { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("select app.appl_pk, app.appl_name from  tnht_appl app");
            
            return sb.ToString();
        }

        protected override void Initialize()
        {
            Filters
               .Add("appl_name", "app.appl_name", typeof(string), FilterTypes.Like);

            Sorts.AddAsc("appl_name");
        }
    }
}
