﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using NewHotel.Core;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Business
{
	[Serializable]
	public sealed class ContractNode : IEnumerable<ContractNode>
	{
        #region Members

        [DebuggerBrowsableAttribute(DebuggerBrowsableState.Never)]
		private readonly BaseContract _contract;
		[DebuggerBrowsableAttribute(DebuggerBrowsableState.Never)]
		private readonly IList<ContractNode> _nodes;

        [NonSerialized]
        [DebuggerBrowsableAttribute(DebuggerBrowsableState.Never)]
        public static readonly object RootSync = new object();

        [Serializable]
        public sealed class ContractTree : IContractTree
        {
            #region Members

            private readonly int _context;
            public readonly ContractNode Root;

            [Serializable]
            private class EmptyContract : BaseContract
            {
                public readonly int Context;

                public EmptyContract(int context)
                {
                    Id = Guid.Empty;
                    Context = context;
                }

                public override string ToString()
                {
                    return string.Format("Context({0})", Context);
                }
            }

            #endregion
            #region Constructor

            public ContractTree(int context)
            {
                _context = context;
                Root = new ContractNode(new EmptyContract(context));
            }

            #endregion
            #region Public Members

            public override string ToString()
            {
                return Root.ToString();
            }

            #endregion
            #region IContractTree Members

            public int Context
            {
                get { return _context; }
            }

            #endregion
        }

        #endregion
        #region Constructor

        public ContractNode(BaseContract contract)
		{
			_contract = contract;
			_nodes = new List<ContractNode>();
		}

        #endregion
        #region Private Members

        private bool Remove(BaseContract contract)
		{
			if (_nodes.Remove(x => x._contract.Equals(contract)))
				return true;
			else
			{
				foreach (var item in _nodes)
				{
					if (item.Remove(contract))
						return true;
				}
			}

			return false;
		}

        #endregion
        #region Internal Members

        internal ContractNode Add(ContractNode node)
		{
			Root.Remove(node._contract);
			_nodes.Add(node);
			return node;
		}

		internal ContractNode Add(BaseContract contract)
		{
			return Add(new ContractNode(contract));
		}

		internal void Add<T>(TypedList<T> contracts)
			where T : BaseContract
		{
			foreach (var contract in contracts)
				Add(contract);
		}

		internal bool Exist(object id)
		{
			if (_nodes.Exists(x => id.Equals(ContractFactory.GetKey(x._contract))))
				return true;
			else
			{
				foreach (var item in _nodes)
				{
					if (item.Exist(id))
						return true;
				}
			}

			return false;
		}

		internal bool TryGet(object id, out BaseContract contract)
		{
			var node = _nodes.FirstOrDefault(x => id.Equals(ContractFactory.GetKey(x._contract)));
			if (node != null)
			{
				contract = node._contract;
				return true;
			}
			else
			{
				foreach (var item in _nodes)
				{
					if (item.TryGet(id, out contract))
						return true;
				}
			}

			contract = null;
			return false;
		}

		internal ContractNode Find(object id)
		{
			var node = _nodes.FirstOrDefault(x => id.Equals(ContractFactory.GetKey(x._contract)));
			if (node != null)
				return node;
			else
			{
				foreach (var item in _nodes)
				{
					node = item.Find(id);
					if (node != null)
						return node;
				}
			}

			return null;
		}

		internal bool Remove(object id)
		{
			if (_nodes.Remove(x => id.Equals(ContractFactory.GetKey(x._contract))))
				return true;
			else
			{
				foreach (var item in _nodes)
				{
					if (item.Remove(id))
						return true;
				}
			}

			return false;
		}

        #endregion
        #region Public Members

        public static ContractNode Root
        {
            get
            {
                lock (RootSync)
                {
                    var tree = NewHotelAppContext.Contracts;
                    if (tree == null)
                    {
                        tree = new ContractTree(NewHotelAppContext.CurrentContext);
                        NewHotelAppContext.Contracts = tree;
                    }

                    return ((ContractTree)tree).Root;
                }
            }
        }

        public override string ToString()
		{
			return _contract == null ? string.Empty : _contract.ToString();
		}

        #endregion
        #region IEnumerable<ContractNode> Members

        public IEnumerator<ContractNode> GetEnumerator()
		{
			return _nodes.GetEnumerator();
		}

		#endregion
		#region IEnumerable Members

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _nodes.GetEnumerator();
		}

		#endregion
	}
}