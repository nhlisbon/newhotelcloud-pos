﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;

namespace NewHotel.Business
{
    /// <summary>
    /// TNHT_PEAP
    /// Definición de Permisos por Aplicación
    /// </summary>
    [PersistentTable("TNHT_PEAP", "PEAP_PK")]
    public partial class PermissionbyApp : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        public PermissionbyApp(IDatabaseManager manager)
            : base(manager) { }

        public PermissionbyApp(IDatabaseManager manager, Guid id)
            : base(manager) { }

        #endregion
        #region Properties

        /// <summary>
        /// PERM_PK
        /// Permiso
        /// </summary>
        private Guid _permissionId;
        [PersistentColumn("PERM_PK")]
        public Guid PermissionId
        {
            get { return _permissionId; }
            set
            {
                if (value != _permissionId)
                {
                    _permissionId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// APPL_PK
        /// Application Id
        /// </summary>
        private long _applicationId;
        [PersistentColumn("APPL_PK")]
        public long ApplicationId
        {
            get { return _applicationId; }
            set
            {
                if (value != _applicationId)
                {
                    _applicationId = value;
                    IsDirty = true;
                }
            }
        }

        #endregion


    }
}
