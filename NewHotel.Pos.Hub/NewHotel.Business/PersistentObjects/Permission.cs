﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;

namespace NewHotel.Business
{
    /// <summary>
    /// TNHT_PERM
    /// Permisos
    /// </summary>
    [PersistentTable("TNHT_PERM", "PERM_PK")]
    public partial class PermissionApp : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        public PermissionApp(IDatabaseManager manager)
            : base(manager) { }

        public PermissionApp(IDatabaseManager manager, Guid id)
            : base(manager) { }

        #endregion
        #region Properties

        /// <summary>
        /// column="LITE_PK"
        /// Literales
        /// </summary>
        [PersistentObject("LITE_PK")]
        public LanguageTranslation Description { get; set; }

        /// <summary>
        /// column="GRPR_PK"
        /// Grupo permiso asociado, ref. tnht_grpr(grpr_pk)
        /// </summary>
        [PersistentObject("GRPR_PK")]
        public Guid Group { get; set; }

        /// <summary>
        /// column="PERM_ORDE"
        /// Orden del permiso dentro del grupo
        /// </summary>
        [PersistentObject("PERM_ORDE")]
        public long Order { get; set; }

        #endregion


        #region Persistent Methods

        public override bool SaveObject()
        {
            Description.SaveObject();
            return base.SaveObject();
        }

        #endregion
    }
}
