﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    /// <summary>
    /// TNHT_UTIL
    /// </summary>
    [PersistentTable("TNHT_UTIL", "UTIL_PK")]
    public class UserApp : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        public UserApp(IDatabaseManager manager)
            : base(manager) { }

        public UserApp(IDatabaseManager manager, Guid id)
            : this(manager)
        {
            this.LoadObject(id);
        }
        public override void Initialize()
        {
            _hotelbyUser = BasePersistent.GetList<HotelbyUser>(Manager,
                x => x.HotelId);
        }

        #endregion
        #region Properties

        /// <summary>
        /// UTIL_LOGIN
        /// Login Usuario
        /// </summary>
        private string _login;
        [PersistentColumn("UTIL_LOGIN")]
        public string Login
        {
            get { return _login; }
            set
            {
                if (value != _login)
                {
                    _login = value;
                    IsDirty = true;
                }
            }
        }

        private string _description;
        [PersistentColumn("UTIL_DESC")]
        public string Description
        {
            get { return _description; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// UTIL_PASS
        /// Password Usuario
        /// </summary>
        private string _password;
        [PersistentColumn("UTIL_PASS")]
        public string Password
        {
            get { return _password; }
            set
            {
                if (value != _password)
                {
                    _password = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// ROLE_PK
        /// Role asociado, ref. tmgr_role(role_pk)
        /// </summary>
        private long _roleId;
        [PersistentColumn("ROLE_PK")]
        public long RoleId
        {
            get { return _roleId; }
            set
            {
                if (value != _roleId)
                {
                    _roleId = value;
                    IsDirty = true;
                }
            }
        }
        

        #endregion
        #region Persistent Lists

        private IPersistentList<HotelbyUser> _hotelbyUser;
        [ReflectionExclude()]
        public IPersistentList<HotelbyUser> HotelbyUser
        {
            get
            {
                if (!_hotelbyUser.Populated)
                    _hotelbyUser.Execute(Id);

                return _hotelbyUser;
            }
        }
        #endregion

        #region Persistent Methods

        public override bool SaveObject()
        {
            if (base.SaveObject())
            {
                if (_hotelbyUser.Populated)
                    HotelbyUser.PersistOrDeleteObjects();
                return true;
            }
            else
                return false;
        }

        #endregion
    }
}
