﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    /// <summary>
    /// TNHT_HOUT
    /// Acceso de Usuarios a Hoteles
    /// </summary>
    [PersistentTable("TNHT_HOUT", "HOUT_PK")]
    public class HotelbyUser : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        public HotelbyUser(IDatabaseManager manager)
            : base(manager){ }

        public HotelbyUser(IDatabaseManager manager, Guid id)
            : this(manager)
        {
            this.LoadObject(id);
        }

        public override void Initialize()
        {
            base.Initialize();
            
        }

        #endregion
        #region Properties

        /// <summary>
        /// HOUT_PK
        /// Hotel
        /// </summary>
        private Guid _hotelId;
        [PersistentColumn("HOTE_PK")]
        public Guid HotelId
        {
            get { return _hotelId; }
            set
            {
                if (value != _hotelId)
                {
                    _hotelId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// UTIL_PK
        /// Usuario
        /// </summary>
        private Guid _userId;
        [PersistentColumn("UTIL_PK")]
        public Guid UserId
        {
            get { return _userId; }
            set
            {
                if (value != _userId)
                {
                    _userId = value;
                    IsDirty = true;
                }
            }
        }
        /// <summary>
        /// HOUT_INAC
        /// flag: usuario inactivo para este hotel
        /// </summary>
        private bool _inactiveUser;
        [PersistentColumn("HOUT_INAC")]
        public bool InactiveUser
        {
            get { return _inactiveUser; }
            set
            {
                if (value != _inactiveUser)
                {
                    _inactiveUser = value;
                    IsDirty = true;
                }
            }
        }

        
        #endregion

    }
}
