﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;

namespace NewHotel.Business
{
    [PersistentTable("TNHT_PEAD", "PEAD_PK")]
    public class PermissionAudit : BasePersistentFullOperations
    {
        #region Constructor
        public PermissionAudit(IDatabaseManager manager)
            : base(manager)
        {

        }
        public PermissionAudit(IDatabaseManager manager, Guid id)
            : this(manager)
        {
            this.LoadObject(id);
        }
        public override void Initialize()
        {
            base.Initialize();
            if (NewHotelContext.Instance.InstallationId.HasValue)
                InstallationId = NewHotelContext.Instance.InstallationId.Value;
        }
        #endregion
        #region Properties

        /// <summary>
        /// HOTE_PK
        /// Installation Id
        /// </summary>
        private Guid _installationId;
        [PersistentColumn("HOTE_PK")]
        public Guid InstallationId
        {
            get { return _installationId; }
            set
            {
                if (value != _installationId)
                {
                    _installationId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// PEAP_PK
        /// Permission Application Id
        /// </summary>
        private Guid _permissionApplicationId;
        [PersistentColumn("PEAP_PK")]
        public Guid PermissionApplicationId
        {
            get { return _permissionApplicationId; }
            set
            {
                if (value != _permissionApplicationId)
                {
                    _permissionApplicationId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// PEAD_AUDIT
        /// Auditable
        /// </summary>
        private bool _auditable;
        [PersistentColumn("PEAD_AUDIT")]
        public bool Auditable
        {
            get { return _auditable; }
            set
            {
                if (value != _auditable)
                {
                    _auditable = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
    }
}
