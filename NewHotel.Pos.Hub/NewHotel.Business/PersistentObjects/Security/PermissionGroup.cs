﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;

namespace NewHotel.Business
{
    [PersistentTable("TNHT_GRPR", "GRPR_PK")]
    public class PermissionGroup : BasePersistentFullOperations
    {
        #region Constructor

        public PermissionGroup(IDatabaseManager manager)
            : base(manager)
        {
            Description = new LanguageTranslation(manager);
        }

        public PermissionGroup(IDatabaseManager manager, Guid id)
            : this(manager)
        {
            this.LoadObject(id);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        #endregion
        #region Properties

        [PersistentObject("LITE_PK")]
        public LanguageTranslation Description { get; set; }

        /// <summary>
        /// GRPR_ORDE   
        /// Orden para mostrar los grupos de permisos
        /// </summary>
        private short _order;
        [PersistentColumn("GRPR_ORDE")]
        public short Order
        {
            get { return _order; }
            set
            {
                if (value != _order)
                {
                    _order = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
    }
}
