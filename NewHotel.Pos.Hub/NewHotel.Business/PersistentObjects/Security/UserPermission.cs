﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    [PersistentTable("TNHT_PEUT", "PEUT_PK")]
    public class UserPermission : BasePersistentFullOperations
    {
        #region Constructor

        public UserPermission(IDatabaseManager manager)
            : base(manager) { }
        public UserPermission(IDatabaseManager manager, Guid id)
            : this(manager)
        {
            this.LoadObject(id);
        }
        public override void Initialize()
        {
            base.Initialize();
            SecurityCode = SecurityCode.Read;
        }

        #endregion
        #region Properties

        /// <summary>
        /// UTIL_PK
        /// User Id
        /// </summary>
        private Guid _userId;
        [PersistentColumn("UTIL_PK")]
        public Guid UserId
        {
            get { return _userId; }
            set
            {
                if (value != _userId)
                {
                    _userId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// PEAP_PK
        /// Permission Application Id
        /// </summary>
        private Guid _permissionApplicationId;
        [PersistentColumn("PEAP_PK")]
        public Guid PermissionApplicationId
        {
            get { return _permissionApplicationId; }
            set
            {
                if (value != _permissionApplicationId)
                {
                    _permissionApplicationId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// SECU_CODE
        /// código seguridad aplicado (hide, read, read/print, read/write, read/write/print, full access)  
        /// ref. tnht_enum(enum_pk)
        /// </summary>
        private SecurityCode _securityCode;
        [PersistentColumn("SECU_CODE")]
        public SecurityCode SecurityCode
        {
            get { return _securityCode; }
            set
            {
                if (value != _securityCode)
                {
                    _securityCode = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
    }
}
