﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    [PersistentTable("TNHT_PEPE", "PEPE_PK")]
    public class ProfilePermission : BasePersistentFullOperations
    {
        #region Constructor

        public ProfilePermission(IDatabaseManager manager)
            : base(manager) { }
        public ProfilePermission(IDatabaseManager manager, Guid id)
            : this(manager)
        {
            this.LoadObject(id);
        }
        public override void Initialize()
        {
            base.Initialize();
            SecurityCode = SecurityCode.Read;
        }

        #endregion
        #region Properties

        /// <summary>
        /// PERF_PK
        /// Profile Id
        /// </summary>
        private Guid _profileId;
        [PersistentColumn("PERF_PK")]
        public Guid ProfileId
        {
            get { return _profileId; }
            set
            {
                if (value != _profileId)
                {
                    _profileId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// PEAP_PK
        /// Profile Application Id
        /// </summary>
        private Guid _profileApplicationId;
        [PersistentColumn("PEAP_PK")]
        public Guid ProfileApplicationId
        {
            get { return _profileApplicationId; }
            set
            {
                if (value != _profileApplicationId)
                {
                    _profileApplicationId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// SECU_CODE
        /// código seguridad aplicado (hide, read, read/print, read/write, read/write/print, full access)  
        /// ref. tnht_enum(enum_pk)
        /// </summary>
        private SecurityCode _securityCode;
        [PersistentColumn("SECU_CODE")]
        public SecurityCode SecurityCode
        {
            get { return _securityCode; }
            set
            {
                if (value != _securityCode)
                {
                    _securityCode = value;
                    IsDirty = true;
                }
            }
        }
        
        #endregion
    }
}
