﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    [PersistentTable("TNHT_PERF", "PERF_PK")]
    public class Profile : BasePersistentFullOperations
    {
        #region Constructor

        public Profile(IDatabaseManager manager)
            : base(manager)
        {
            Description = new LanguageTranslation(manager);
        }
        public Profile(IDatabaseManager manager, Guid id)
            : this(manager)
        {
            this.LoadObject(id);
        }
        public override void Initialize()
        {
            base.Initialize();
            _profilePermissions = BasePersistent.GetList<ProfilePermission>(Manager, x => x.ProfileId);
        }

        #endregion
        #region Properties

        [PersistentObject("LITE_PK")]
        public LanguageTranslation Description { get; set; }

        /// <summary>
        /// PERF_LMOD
        /// </summary>
        [PersistentColumn("PERF_LMOD")]
        public DateTime LastModified { set; get; }

        #endregion
        #region PersistentLists

        private IPersistentList<ProfilePermission> _profilePermissions;
        [ReflectionExclude()]
        public IPersistentList<ProfilePermission> ProfilePermission
        {
            get
            {
                if (!_profilePermissions.Populated)
                    _profilePermissions.Execute(Id);
                return _profilePermissions;
            }
        }

        #endregion

        public override bool SaveObject()
        {
            if (base.SaveObject())
            {
                if (_profilePermissions.Populated) 
                    ProfilePermission.PersistOrDeleteObjects();
                return true;
            }
            return false;
        }
    }
}
