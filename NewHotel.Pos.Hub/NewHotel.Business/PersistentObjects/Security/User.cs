﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    /// <summary>
    /// TNHT_UTIL
    /// </summary>
    [PersistentTable("TNHT_UTIL", "UTIL_PK")]
    public class User : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        public User(IDatabaseManager manager)
            : base(manager) { }

        public User(IDatabaseManager manager, Guid id)
            : this(manager)
        {
            this.LoadObject(id);
        }

        public override void Initialize()
        {
            base.Initialize();
            if (NewHotelContext.Instance.InstallationId.HasValue)
                InstallationId = NewHotelContext.Instance.InstallationId.Value;
            _userPermissions = BasePersistent.GetList<UserPermission>(Manager, x => x.UserId);
            _profileModified = false;
            _inactive = false;
        }

        #endregion
        #region Properties

        /// <summary>
        /// UTIL_LOGIN
        /// Login Usuario
        /// </summary>
        private string _login;
        [PersistentColumn("UTIL_LOGIN")]
        public string Login
        {
            get { return _login; }
            set
            {
                if (value != _login)
                {
                    _login = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// UTIL_PASS
        /// Password Usuario
        /// </summary>
        private string _password;
        [PersistentColumn("UTIL_PASS")]
        public string Password
        {
            get { return _password; }
            set
            {
                if (value != _password)
                {
                    _password = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// UTIL_DESC
        /// Descripcion Usuario
        /// </summary>
        private string _description;
        [PersistentColumn("UTIL_DESC")]
        public string Description
        {
            get { return _description; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// UTIL_DARE
        /// Fecha registro usuario
        /// </summary>
        private DateTime _registrationDate;
        [PersistentColumn("UTIL_DARE")]
        public DateTime RegistrationDate
        {
            get { return _registrationDate; }
            set
            {
                if (value != _registrationDate)
                {
                    _registrationDate = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// UTIL_EXPW
        /// Fecha expiración password
        /// </summary>
        private DateTime? _passwordExpirationDate;
        [PersistentColumn("UTIL_EXPW", Nullable = true)]
        public DateTime? PasswordExpirationDate
        {
            get { return _passwordExpirationDate; }
            set
            {
                if (value != _passwordExpirationDate)
                {
                    _passwordExpirationDate = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// UTIL_EXPU
        /// Fecha expiración usuario
        /// </summary>
        private DateTime? _userExpirationDate;
        [PersistentColumn("UTIL_EXPU", Nullable = true)]
        public DateTime? UserExpirationDate
        {
            get { return _userExpirationDate; }
            set
            {
                if (value != _userExpirationDate)
                {
                    _userExpirationDate = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// HOTE_PK
        /// Installation Id
        /// </summary>
        private Guid _installationId;
        [PersistentColumn("HOTE_PK")]
        public Guid InstallationId
        {
            get { return _installationId; }
            set
            {
                if (value != _installationId)
                {
                    _installationId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// PERF_PK
        /// Profile Id
        /// </summary>
        private Guid? _profileId;
        [PersistentColumn("PERF_PK")]
        public Guid? ProfileId
        {
            get { return _profileId; }
            set
            {
                if (value != _profileId)
                {
                    _profileId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// PERF_MODI
        /// flag: indica si el perfil asociado fue modificado
        /// </summary>
        private bool _profileModified;
        [PersistentColumn("PERF_MODI")]
        public bool ProfileModified
        {
            get { return _profileModified; }
            set
            {
                if (value != _profileModified)
                {
                    _profileModified = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// UTIL_INAC
        /// flag: Usuario inactivo
        /// </summary>
        private bool _inactive;
        [PersistentColumn("UTIL_INAC")]
        public bool Inactive
        {
            get { return _inactive; }
            set
            {
                if (value != _inactive)
                {
                    _inactive = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// UTIL_LMOD
        /// Ultima modificación: control de concurrencia
        /// </summary>
        [PersistentColumn("UTIL_LMOD")]
        public DateTime LastModified { set; get; }

        #endregion
        #region PersistentLists

        private IPersistentList<UserPermission> _userPermissions;
        [ReflectionExclude()]
        public IPersistentList<UserPermission> UserPermissions
        {
            get
            {
                if (!_userPermissions.Populated)
                    _userPermissions.Execute(Id);

                return _userPermissions;
            }
        }

        #endregion

        public override bool SaveObject()
        {
            if (base.SaveObject())
            {
                if (_registrationDate == DateTime.MinValue)  
                    _registrationDate = NewHotelContext.Instance.GetWorkDate();
                if (_userPermissions.Populated)
                    UserPermissions.PersistOrDeleteObjects();
                return true;
            }
            return false;
        }
    }
}
