﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;

namespace NewHotel.Business
{
    [PersistentTable("TNHT_PERM", "PERM_PK")]
    public class Permission : BasePersistentFullOperations
    {
        #region Constructor

        public Permission(IDatabaseManager manager)
            : base(manager)
        {
            Description = new LanguageTranslation(manager);
        }
        public Permission(IDatabaseManager manager, Guid id)
            : this(manager)
        {
            this.LoadObject(id);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        #endregion
        #region Properties

        [PersistentObject("LITE_PK")]
        public LanguageTranslation Description { get; set; }

        /// <summary>
        /// GRPR_PK
        /// Grupo de Permisos
        /// </summary>
        private Guid _permissionGroupId;
        [PersistentColumn("GRPR_PK")]
        public Guid PermissionGroupId
        {
            get { return _permissionGroupId; }
            set
            {
                if (value != _permissionGroupId)
                {
                    _permissionGroupId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// PERM_ORDE   
        /// Orden para mostrar los permisos
        /// </summary>
        private short _order;
        [PersistentColumn("PERM_ORDE")]
        public short Order
        {
            get { return _order; }
            set
            {
                if (value != _order)
                {
                    _order = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
    }
}
