﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    /// <summary>
    /// TNHT_UNMO
    /// </summary>
    [PersistentTable("TNHT_UNMO", "UNMO_PK")]
    public class CurrencyApp : BasePersistent, IDataLoad
    {
        #region Constructor + Initialize

        public CurrencyApp(IDatabaseManager manager)
            : base(manager) { }

        public CurrencyApp(IDatabaseManager manager, string id)
            : this(manager)
        {
            this.LoadObject(id);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        #endregion
        #region Properties

        /// <summary>
        /// LITE_PK
        /// Traducción nombre moneda, ref. tnht_lite(lite_pk)
        /// </summary>
        private Guid _descriptionId;
        [PersistentColumn("LITE_PK")]
        public Guid DescriptionId
        {
            get { return _descriptionId; }
            set
            {
                if (value != _descriptionId)
                {
                    _descriptionId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// UNMO_NDEC
        /// Número de decimales
        /// </summary>
        private short _decimals;
        [PersistentColumn("UNMO_NDEC")]
        public short Decimales
        {
            get { return _decimals; }
            set
            {
                if (value != _decimals)
                {
                    _decimals = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
        #region Extended methods

        public static decimal? GetExchangeValues(IDatabaseManager manager, string currency, DateTime day)
        {
            decimal? cachValue = null;

            //Query currencyExchange = SettingsQueryContainer.Instance.GetCurrencyInstallation(manager);
            //currencyExchange.Filters["unmo_pk"].Value = currency;
            //currencyExchange.Parameters["camb_data"] = day.Date;
            //ListData exchangeData = currencyExchange.Execute();

            //if (exchangeData.Count > 0)
            //{
            //    exchangeData.First();
            //    cachValue = exchangeData.ValueAs<decimal>("camb_vamb");
            //}

            return cachValue;
        }

        #endregion
        #region IDataLoad Member

        public bool LoadObject(object id, bool throwNoDataFoundException)
        {
            return LoadObjectFromDB(id, throwNoDataFoundException);
        }

        #endregion
    }
}
