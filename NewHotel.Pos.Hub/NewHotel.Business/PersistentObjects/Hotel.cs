﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Business
{
    /// <summary>
    /// TNHT_HOTE
    /// </summary>
    [PersistentTable("TNHT_HOTE", "HOTE_PK")]
    public class Hotel : BasePersistentFullOperations
    {
        #region Persistent Properties
        /// <summary>
        /// HOTE_DESC
        /// Nombre Hotel
        /// </summary>
        private string _description;
        [PersistentColumn("HOTE_DESC")]
        public string Description
        {
            get { return _description; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    IsDirty = true;
                }
            }
        }
        /// <summary>
        /// column="HOTE_PROV"
        /// Provider (Oracle = 1, SQL Server = 2)
        /// </summary>
        private short _provider;
        [PersistentColumn("HOTE_PROV")]
        public short Provider
        {
            get { return _provider; }
            set
            {
                if (value != _provider)
                {
                    _provider = value;
                    IsDirty = true;
                }
            }
        }
        /// <summary>
        /// HOTE_SERV
        /// Servicio Base Datos
        /// </summary>
        private string _databaseService;
        [PersistentColumn("HOTE_SERV")]
        public string DataBaseService
        {
            get { return _databaseService; }
            set
            {
                if (value != _databaseService)
                {
                    _databaseService = value;
                    IsDirty = true;
                }
            }
        }
        /// <summary>
        /// HOTE_USER
        /// usuario donde se encuentra el hotel real
        /// </summary>
        private string _user;
        [PersistentColumn("HOTE_USER")]
        public string User
        {
            get { return _user; }
            set
            {
                if (value != _user)
                {
                    _user = value;
                    IsDirty = true;
                }
            }
        }
        /// <summary>
        /// HOTE_PWRD
        /// password de acceso al schema
        /// </summary>
        private string _shemapassword;
        [PersistentColumn("HOTE_PWRD", Nullable = true)]
        public string ShemaPassword
        {
            get { return _shemapassword; }
            set
            {
                if (value != _shemapassword)
                {
                    _shemapassword = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// COMP_PK
        /// Grupo del Hotel
        /// </summary>
        private Guid? _groupHotelId;
        [PersistentColumn("COMP_PK")]
        public Guid? GroupHotelId
        {
            get { return _groupHotelId; }
            set
            {
                if (value != _groupHotelId)
                {
                    _groupHotelId = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
    
        #region Constructor + Initialize

        public Hotel(IDatabaseManager manager) 
            : base(manager) { }

        public Hotel(IDatabaseManager manager, Guid id)
            : this(manager)
        {
            this.LoadObject(id);
        }


        #endregion
    }
}
