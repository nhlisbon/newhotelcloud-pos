﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Business
{
    /// <summary>
    /// TNHT_COMP
    /// Grupos de Hoteles
    /// </summary>
    [PersistentTable("TNHT_COMP", "COMP_PK")]
    public class GroupHotel : BasePersistentBasicOperations
    {
        #region Persistent Properties

        /// <summary>
        /// HOTE_DESC
        /// Nombre Hotel
        /// </summary>
        private string _description;
        [PersistentColumn("COMP_DESC")]
        public string Description
        {
            get { return _description; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    IsDirty = true;
                }
            }
        }
        #endregion
    
        #region Constructor + Initialize

        public GroupHotel(IDatabaseManager manager) 
            : base(manager) { }

        public GroupHotel(IDatabaseManager manager, Guid id)
            : this(manager)
        {
            this.LoadObject(id);
        }


        #endregion
    }
}
