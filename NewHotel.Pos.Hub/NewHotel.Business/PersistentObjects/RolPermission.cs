﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    /// <summary>
    /// TNHT_ROPE
    /// Contiene los niveles de seguridad de cada permiso en un Role determinado
    /// </summary>
    [PersistentTable("TNHT_ROPE", "ROPE_PK")]
    public partial class RolePermission : BasePersistentFullOperations
    {
         #region Constructor + Initialize

        public RolePermission(IDatabaseManager manager)
            : base(manager) { }

        public RolePermission(IDatabaseManager manager, Guid id)
            : base(manager) 
        {
            this.LoadObject(id);
        }

        #endregion
        #region Properties
        /// <summary>
        /// SECU_CODE
        /// código seguridad aplicado (hide, read, read/print, read/write, read/write/print, full access)  
        /// ref. tnht_enum(enum_pk)
        /// </summary>
        private SecurityCode _securityCode;
        [PersistentColumn("SECU_CODE")]
        public SecurityCode SecurityCode
        {
            get { return _securityCode; }
            set
            {
                if (value != _securityCode)
                {
                    _securityCode = value;
                    IsDirty = true;
                }
            }
        }
  
        /// <summary>
        /// column="ROLE_PK"
        /// Role
        /// </summary>
        private long _roleId;
        [PersistentColumn("ROLE_PK")]
        public long RoleId
        {
            get { return _roleId; }
            set
            {
                if (value != _roleId)
                {
                    _roleId = value;
                    IsDirty = true;
                }
            }
        }
        /// <summary>
        /// column="PEAP_PK"
        /// Código permiso por aplicación ref. tnht_peap(peap_pk) delete cascade
        /// </summary>
        private Guid _permissionApplicationId;
        [PersistentColumn("PEAP_PK")]
        public Guid PermissionApplicationId 
        {
            get { return _permissionApplicationId; }
            set
            {
                if (value != _permissionApplicationId)
                {
                    _permissionApplicationId = value;
                    IsDirty = true;
                }
            }
        }

        #endregion


    }
}
