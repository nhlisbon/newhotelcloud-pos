﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using NewHotel.Core;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Business
{
    public class TranslationInfo
    {
        #region Constants

        private const int maxLength = 255;

        #endregion
        #region Members

        public readonly long LanguageId;
        public readonly string LanguageName;
        public readonly string Translation;

        #endregion
        #region Constructor

        public TranslationInfo(long languageId, string languageName, string translation)
        {
            LanguageId = languageId;
            LanguageName = languageName;
            Translation = translation != null && translation.Length > maxLength
                ? translation.Substring(0, maxLength) : translation;
        }

        #endregion
    }

    [PersistentTable("TNHT_LITE", "LITE_PK")]
    public class LanguageTranslation : BasePersistentFullOperations, IEnumerable<TranslationInfo>
    {
        #region Members

        private static readonly string LoadTranslationCommandText;
        private static readonly string DeleteLiteralCommandText;
        private static readonly string InsertLiteralCommandText;
        private static readonly string DeleteTranslationCommandText;
        private static readonly string InsertTranslationCommandText;

        private readonly Dictionary<long, TranslationInfo> Translations =
            new Dictionary<long, TranslationInfo>();
        private bool initialized = false;

        #endregion
        #region Properties

        [ReflectionExclude(Hide = true)]
        public override bool IsEmpty 
        {
            get { return !Translations.Values.Any(x => !string.IsNullOrEmpty(x.Translation)); } 
        }

        [ReflectionExclude(Hide = true)]
        public string this[long language]
        {
            get
            {
                if (!initialized)
                    Load();

                TranslationInfo value;
                if (!Translations.TryGetValue(language, out value))
                    throw new ArgumentOutOfRangeException(string.Format("language ({0})", language));

                return value.Translation;
            }
            set
            {
                if (!initialized)
                    Load();

                value = value ?? string.Empty;
                if (this[language] != value)
                {
                    TranslationInfo info;
                    if (!Translations.TryGetValue(language, out info))
                        throw new ArgumentOutOfRangeException(string.Format("language ({0})", language));

                    Translations[language] = new TranslationInfo(language, info.LanguageName, value);
                    IsDirty = true;
                }
            }
        }

        #endregion
        #region Constructor + Initialize

        public LanguageTranslation(IDatabaseManager manager, bool initialize)
            : base(manager)
        {
            if (initialize)
                Load();
        }

        public LanguageTranslation(IDatabaseManager manager)
            : this(manager, true) { }

        public LanguageTranslation(IDatabaseManager manager, Guid id)
            : base(manager)
        {
            Id = id;
            Load();
        }

        #endregion
        #region Private Methods

        private void Load()
        {
            Manager.Open();
            try
            {
                Manager.CreateParameters();
                Manager.AddParameters("lang_pk", AppContext.GetInstance().Language, DbType.Int64);
                Manager.AddParameters("lite_pk", (Guid)Id, DbType.Guid);
                Manager.ExecuteReader(CommandType.Text, LoadTranslationCommandText);

                Translations.Clear();
                try
                {
                    while (Manager.DataReader.Read())
                    {
                        long language = (long)Convert.ChangeType(Manager.DataReader["lang_pk"], typeof(long));
                        Translations.Add(language,
                            new TranslationInfo(language,
                                (string)Convert.ChangeType(Manager.DataReader["lang_name"], typeof(string)),
                                (string)Convert.ChangeType(Manager.DataReader["mult_desc"], typeof(string))));
                    }

                    IsDirty = false;
                    initialized = true;
                }
                finally
                {
                    Manager.CloseReader();
                }
            }
            finally
            {
                Manager.Close();
            }
        }

        #endregion
        #region Static Methods

        static LanguageTranslation()
        {
            // Load transaltions
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select lang.lang_pk, mult_lang.mult_desc lang_name, mult.mult_desc, lang.lang_prio");
            sb.AppendLine("from   tnht_lang lang inner join tnht_licl licl on licl.licl_pk = lang.lang_pk");
            sb.AppendLine("                      inner join vnht_mult mult_lang on mult_lang.lite_pk = licl.lite_pk and mult_lang.lang_pk = :lang_pk");
            sb.AppendLine("                      left outer join tnht_mult mult on mult.lang_pk = lang.lang_pk and mult.lite_pk = :lite_pk");
            sb.AppendLine("order by lang.lang_prio");
            LoadTranslationCommandText = sb.ToString();

            // Delete literal
            DeleteLiteralCommandText = "delete from tnht_lite where lite_pk = :lite_pk";

            // Insert literal
            sb = new StringBuilder();
            sb.AppendLine("merge into tnht_lite lite");
            sb.AppendLine("using dual on (lite.lite_pk = :lite_pk)");
            sb.AppendLine("when not matched then");
            sb.AppendLine("insert (lite_pk) values (:lite_pk)");
            InsertLiteralCommandText = sb.ToString();

            // Delete translations
            DeleteTranslationCommandText = "delete from tnht_mult where lite_pk = :lite_pk";

            // Insert translations
            sb = new StringBuilder();
            sb.AppendLine("insert into tnht_mult (lite_pk, lang_pk, mult_desc)");
            sb.AppendLine("values(:lite_pk, :lang_pk, :mult_desc)");
            InsertTranslationCommandText = sb.ToString();
        }

        public static implicit operator string(LanguageTranslation languageTranslation)
        {
            string translation = languageTranslation[AppContext.GetInstance().Language];

            if (string.IsNullOrEmpty(translation))
            {
                TranslationInfo info = languageTranslation
                    .FirstOrDefault(x => !string.IsNullOrEmpty(x.Translation));

                translation = info != null ? info.Translation : string.Empty;
            }

            return translation;
        }

        #endregion
        #region IEnumerable<TranslationInfo> Members

        public IEnumerator<TranslationInfo> GetEnumerator()
        {
            if (!initialized)
                Load();

            return Translations.Values.GetEnumerator();
        }

        #endregion
        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            if (!initialized)
                Load();

            return Translations.Values.GetEnumerator();
        }

        #endregion
        #region IData Operations

        public override bool SaveObject()
        {
            if (IsEmpty)
                return false;
            else
            {
                Manager.Open();
                try
                {
                    if (IsDirty)
                    {
                        // Insert a literal if necessary
                        Manager.CreateParameters();
                        Manager.AddParameters("lite_pk", (Guid)Id, DbType.Guid);
                        Manager.ExecuteNonQuery(CommandType.Text, InsertLiteralCommandText);

                        // Delete all translations if literal already exists
                        Manager.CreateParameters();
                        Manager.AddParameters("lite_pk", (Guid)Id, DbType.Guid);
                        Manager.ExecuteNonQuery(CommandType.Text, DeleteTranslationCommandText);

                        // Insert all translations
                        foreach (var translation in Translations.Values
                            .Where(x => !string.IsNullOrEmpty(x.Translation)).OrderBy(x => x.LanguageId))
                        {
                            Manager.CreateParameters();
                            Manager.AddParameters("lite_pk", (Guid)Id, DbType.Guid);
                            Manager.AddParameters("lang_pk", translation.LanguageId, DbType.Int64);
                            Manager.AddParameters("mult_desc", translation.Translation, DbType.String);
                            Manager.ExecuteNonQuery(CommandType.Text, InsertTranslationCommandText);
                        }

                        IsDirty = false;
                    }
                }
                finally
                {
                    Manager.Close();
                }
            }

            return true;
        }

        public override bool LoadObject(object id, bool throwNoDataFoundException)
        {
            Id = id;
            Load();
            return true;
        }

        public override bool DeleteObject()
        {
            Manager.Open();
            try
            {
                // Delete a literal with all translations
                Manager.CreateParameters();
                Manager.AddParameters("lite_pk", (Guid)Id, DbType.Guid);
                Manager.ExecuteNonQuery(CommandType.Text, DeleteLiteralCommandText);
                return true;
            }
            finally
            {
                Manager.Close();
            }
        }

        #endregion
    }
}
