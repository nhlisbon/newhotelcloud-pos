﻿using System;
using NewHotel.Core;

namespace NewHotel.Business
{
    /// <summary>
    /// TNHT_EDTD
    /// </summary>
    [PersistentTable("TNHT_EDTD", "EDTA_DATA")]
    public class Dates : BasePersistent<DateTime>, IDataSave
    {
        #region Constructor + Initialize

        public Dates(IDatabaseManager manager, DateTime date)
            : base(manager) 
        {
            Id = date;
        }

        #endregion
        #region IDataSave Members

        public bool SaveObject()
        {
            return PersistObjectToDB();
        }

        #endregion
    }
}