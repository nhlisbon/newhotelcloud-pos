﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Business
{
    /// <summary>
    /// TNHT_APP
    /// Aplicaciones/Modulos 
    /// </summary>
    [PersistentTable("TNHT_APPL", "APPL_PK")]
    public class ApplicationModule : BasePersistentBasicOperations<long>
    {
        #region Persistent Properties

        /// <summary>
        /// APPL_NAME
        /// Nombre Aplicación
        /// </summary>
        private string _description;
        [PersistentColumn("APPL_NAME")]
        public string Description
        {
            get { return _description; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
    
        #region Constructor + Initialize

        public ApplicationModule(IDatabaseManager manager) 
            : base(manager) { }

        public ApplicationModule(IDatabaseManager manager, long id)
            : this(manager)
        {
            this.LoadObject(id);
        }


        #endregion
    }
}
