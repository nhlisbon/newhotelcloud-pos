﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    /// <summary>
    /// table="TNHT_SEIM"
    /// Secuencias de impuestos
    /// </summary>
    [PersistentTable("TNHT_SEIM", "SEIM_PK")]
    public class TaxSequence: BasePersistentFullOperations
    {
        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public TaxSequence(IDatabaseManager manager)
            : base(manager) 
        {
            Description = new LanguageTranslation(manager);
        }

        public TaxSequence(IDatabaseManager manager, object id)
            : base(manager) 
        {
            this.LoadObject(id);
        }

        public override void Initialize() {}

        #endregion
        #region Persistent properties
        /// <summary>
		/// column="SEIM_ABRE"
		/// Abreviatura Secuencia Impuesto
		/// </summary>
        private string _abbreviation;
        [PersistentColumn("SEIM_ABRE")]
        public string Abbreviation
        {
            get { return _abbreviation; }
            set
            {
                if (value != _abbreviation)
                {
                    _abbreviation = value;
                    IsDirty = true;
                }
            }
        }
        /// <summary>
        /// column="LITE_PK"
        /// Literales
        /// </summary>
        [PersistentObject("LITE_PK")]
        public LanguageTranslation Description { get; set; }

        /// <summary>
        /// SEIM_LMOD
        /// </summary>
        [PersistentColumn("SEIM_LMOD")]
        public DateTime LastModified { get; set; }

        #endregion
        #region Persistent Methods
        public override bool SaveObject()
        {
            Description.SaveObject();
            return base.SaveObject();
        }
        #endregion
    }
}
