﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    /// <summary>
    /// table="TNHT_ESIM"
    /// Esquemas Impuestos
    /// </summary>
    [PersistentTable("TNHT_ESIM", "ESIM_PK")]
    public class TaxSchema : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public TaxSchema(IDatabaseManager manager)
            : base(manager)
        {
            Description = new LanguageTranslation(manager);
        }

        public TaxSchema(IDatabaseManager manager, object id)
            : base(manager)
        {
            this.LoadObject(id);
        }

        public override void Initialize()
        {
            if (NewHotelContext.Instance.InstallationId.HasValue)
                Installation = NewHotelContext.Instance.InstallationId.Value;
        }

        #endregion
        #region Persistent properties

        /// <summary>
        /// column=HOTE_PK"
        /// Hotel/Instalación asociada 
        /// </summary>
        private Guid _installation;
        [PersistentColumn("HOTE_PK")]
        public Guid Installation
        {
            get { return _installation; }
            set
            {
                if (value != _installation)
                {
                    _installation = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="LITE_PK"
        /// Literales
        /// </summary>
        [PersistentObject("LITE_PK")]
        public LanguageTranslation Description { get; set; }

        /// <summary>
        /// ESIM_DEFA"
        /// Flag: Esquema Iva por defecto
        /// </summary>
        private bool _isDefault;
        [PersistentColumn("ESIM_DEFA")]
        public bool IsDefault
        {
            get { return _isDefault; }
            set
            {
                if (value != _isDefault)
                {
                    _isDefault = value;
                    IsDirty = true;
                }
            }
        }
        
        /// <summary>
        /// ESIM_EXPD
        /// Fecha vencimiento Esquema Impuesto
        /// </summary>
        private DateTime? _expiredDate;
        [PersistentColumn("ESIM_EXPD")]
        public DateTime? ExpiredDate
        {
            get { return _expiredDate; }
            set
            {
                if (value != _expiredDate)
                {
                    _expiredDate = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// ESIM_LINK
        /// Enlace entre esquemas de impuesto
        /// en caso de vencimiento del esquema
        /// </summary>
        private Guid? _schemaLink;
        [PersistentColumn("ESIM_LINK")]
        public Guid? SchemaLink
        {
            get { return _schemaLink; }
            set
            {
                if (value != _schemaLink)
                {
                    _schemaLink = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// ESIM_LMOD
        /// </summary>
        [PersistentColumn("ESIM_LMOD")]
        public DateTime LastModified { get; set; }

        #endregion
        #region Persistent Methods

        public override bool SaveObject()
        {
            Description.SaveObject();
            return base.SaveObject();
        }

        #endregion

        private static string UpdateSchemaNonDefaultCommandText;

        static TaxSchema()
        {
            StringBuilder sb = new StringBuilder();
            sb = new StringBuilder();
            sb.AppendLine("update tnht_esim ");
            sb.AppendLine("set esim_defa = 0");
            sb.AppendLine("where esim_pk <> :esim_pk");
            sb.AppendLine("  and hote_pk = :hote_pk");
            UpdateSchemaNonDefaultCommandText = sb.ToString();
        }

        public void UpdateSchemaNonDefault(IDatabaseManager manager)
        {
            manager.CreateParameters();
            manager.AddParameters("esim_pk", Id, DbType.Guid);
            manager.AddParameters("hote_pk", NewHotelContext.Instance.InstallationId.Value, DbType.Guid);
            manager.ExecuteNonQuery(CommandType.Text, UpdateSchemaNonDefaultCommandText);
        }
    }
}
