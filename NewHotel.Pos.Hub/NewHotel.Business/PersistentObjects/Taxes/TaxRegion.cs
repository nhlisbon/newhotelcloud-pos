﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    /// <summary>
    /// table="TNHT_REGI"
    /// Regiones de Impuestos por Paises
    /// </summary>
    [PersistentTable("TNHT_REGI", "REGI_PK")]
    public class TaxRegion: BasePersistentFullOperations
    {
        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public TaxRegion(IDatabaseManager manager)
            : base(manager) 
        {
            Description = new LanguageTranslation(manager);
        }

        public TaxRegion(IDatabaseManager manager, object id)
            : base(manager) 
        {
            this.LoadObject(id);
        }

        #endregion
        #region Persistent properties

        /// <summary>
        /// column="LITE_PK"
        /// Literales
        /// </summary>
        [PersistentObject("LITE_PK")]
        public LanguageTranslation Description { get; set; }

        /// <summary>
        /// column="NACI_PK"
        /// Pais
        /// </summary>
        private string _country;
        [PersistentColumn("NACI_PK")]
        public string Country
        {
            get { return _country; }
            set
            {
                if (value != _country)
                {
                    _country = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// REGI_LMOD
        /// </summary>
        [PersistentColumn("REGI_LMOD")]
        public DateTime LastModified { get; set; }

        #endregion
        #region Persistent Methods

        public override bool SaveObject()
        {            
            Description.SaveObject();
            return base.SaveObject();            
        }

        #endregion
    }
}
