﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    /// <summary>
    /// table="TNHT_TIVA"
    /// Tasas de Impuestos
    /// </summary>
    [PersistentTable("TNHT_TIVA", "TIVA_PK")]
    public class TaxRate: BasePersistentFullOperations
    {
        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public TaxRate(IDatabaseManager manager)
            : base(manager) 
        {
            Description = new LanguageTranslation(manager);
        }
        public TaxRate(IDatabaseManager manager, object id)
            : this(manager) 
        {
            this.LoadObject(id);
        }

        public override void Initialize() 
        {
            Type = VatType.Supported;
        }

        #endregion
        #region Persistent properties

        /// <summary>
        /// column="LITE_PK"
        /// Literales
        /// </summary>
        [PersistentObject("LITE_PK")]
        public LanguageTranslation Description { get; set; }

        /// <summary>
        /// table="TNHT_REGI" column="REGI_PK"
		/// Regiones de Impuestos por Paises
		/// </summary>
        private Guid _taxRegion;
        [PersistentColumn("REGI_PK")]
        public Guid TaxRegion
        {
            get { return _taxRegion; }
            set
            {
                if (value != _taxRegion)
                {
                    _taxRegion = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
		/// table="TNHT_SEIM"
		/// Secuencias de impuestos
		/// </summary>
        private Guid _taxSequence;
        [PersistentColumn("SEIM_PK")]
        public Guid TaxSequence
        {
            get { return _taxSequence; }
            set
            {
                if (value != _taxSequence)
                {
                    _taxSequence = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
		/// column="TIVA_PERC"
		/// Porciento impuesto definido
		/// </summary>
        private decimal _percent;
        [PersistentColumn("TIVA_PERC")]
        public decimal Percent
        {
            get { return _percent; }
            set
            {
                if (value != _percent)
                {
                    _percent = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
		/// column="TIVA_DIRE"
		/// Tipo impuesto (soportado, repercutido) 
		/// ref. tnht_enum(enum_pk)
		/// </summary>
        private VatType _type;
        [PersistentColumn("TIVA_DIRE")]
        public VatType Type
        {
            get { return _type; }
            set
            {
                if (value != _type)
                {
                    _type = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// TIVA_LMOD
        /// </summary>
        [PersistentColumn("TIVA_LMOD")]
        public DateTime LastModified { get; set; }

        #endregion
        #region Persistent Methods

        public override bool SaveObject()
        {
            Description.SaveObject();
            return base.SaveObject();
        }

        #endregion
    }
}
