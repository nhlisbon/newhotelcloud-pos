﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Business
{
    /// <summary>
    /// TNHT_APP
    /// Aplicaciones/Modulos 
    /// </summary>
    [PersistentTable("TNHT_APP", "APP_PK")]
    public class AplicationModule : BasePersistentBasicOperations
    {
        #region Persistent Properties

        /// <summary>
        /// APPL_NAME
        /// Nombre Aplicación
        /// </summary>
        [PersistentColumn("APPL_NAME")]
        public string Description {get;set;}

        #endregion
    
        #region Constructor + Initialize

        public AplicationModule(IDatabaseManager manager) 
            : base(manager) { }

        public AplicationModule(IDatabaseManager manager, Guid id)
            : this(manager)
        {
            this.LoadObject(id);
        }


        #endregion
    }
}
