﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;

namespace NewHotel.Business
{
    /// <summary>
    /// TNHT_TIVA
    /// Tasas de Impuesto
    /// </summary>
    [PersistentTable("TNHT_TIVA", "TIVA_PK")]
    public partial class Tax : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        public Tax(IDatabaseManager manager)
            : base(manager) { }

        public Tax(IDatabaseManager manager, Guid id)
            : base(manager) { }

        #endregion
        #region Properties

        /// <summary>
        /// column="LITE_PK"
        /// Literales
        /// </summary>
        [PersistentObject("LITE_PK")]
        public LanguageTranslation Description { get; set; }

        /// <summary>
        /// TIVA_PERC
        /// % impuesto
        /// </summary>
        [PersistentObject("TIVA_PERC")]
        public decimal Percent { get; set; }

        /// <summary>
        /// SEIM_PK
        ///Secuencia impuestos
        /// </summary>
        [PersistentObject("SEIM_PK")]
        public Guid Sequence { get; set; }

        /// <summary>
        /// REGI_PK
        /// Region de Impuesto asociada, ref. tnht_regi(regi_pk)
        /// </summary>
        [PersistentObject("REGI_PK")]
        public Guid Region { get; set; }

        #endregion
    }
}
