﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;

namespace NewHotel.Business
{
    /// <summary>
    /// TNHT_GRPR
    /// Grupos de Permisos
    /// </summary>
    [PersistentTable("TNHT_GRPR", "GRPR_PK")]
    public partial class Group : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        public Group(IDatabaseManager manager)
            : base(manager) { }

        public Group(IDatabaseManager manager, Guid id)
            : base(manager) { }

        #endregion
        #region Properties

        /// <summary>
        /// column="LITE_PK"
        /// Literales
        /// </summary>
        [PersistentObject("LITE_PK")]
        public LanguageTranslation Description { get; set; }

        /// <summary>
        /// column="GRPR_ORDE"
        /// Orden para mostrar los grupos de permisos
        /// </summary>
        [PersistentObject("GRPR_ORDE")]
        public long Order { get; set; }

        #endregion


    }
}
