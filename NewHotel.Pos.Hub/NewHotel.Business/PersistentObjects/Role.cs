﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    /// <summary>
    /// TNHT_ROLE
    /// </summary>
    [PersistentTable("TNHT_ROLE", "ROLE_PK")]
    public partial class Role : BasePersistentFullOperations<long>
    {
        #region Constructor + Initialize

        public Role(IDatabaseManager manager)
            : base(manager) { }

        public Role(IDatabaseManager manager, long id)
            : base(manager) { this.LoadObject(id); }

        #endregion

        public override void Initialize()
        {
            _rolePermission = BasePersistent.GetList<RolePermission>(Manager,
                x => x.RoleId);
        }
        #region Properties

        /// <summary>
        /// column="LITE_PK"
        /// Literales
        /// </summary>
        [PersistentObject("LITE_PK")]
        public LanguageTranslation Description { get; set; }

        #endregion

        #region Persistent Lists

        private IPersistentList<RolePermission> _rolePermission;
        [ReflectionExclude()]
        public IPersistentList<RolePermission> RolePermission
        {
            get
            {
                if (!_rolePermission.Populated)
                    _rolePermission.Execute(Id);

                return _rolePermission;
            }
        }
        #endregion

        #region Persistent Methods

        public override bool SaveObject()
        {
            if (base.SaveObject())
            {
                if (_rolePermission.Populated)
                    _rolePermission.PersistOrDeleteObjects();
                return true;
            }
            else
                return false;
        }

        #endregion
    }
}
