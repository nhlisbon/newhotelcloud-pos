﻿using System;
using System.Linq;
using System.Collections;
using NewHotel.Core;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Business
{
	public static class ContractFactory
	{
        #region Members

        private struct ContractKey
		{
			private readonly Type _type;
			private readonly string _id;

            private string Key
            {
                get { return _type.GUID + "+" + _id; }
            }

            public ContractKey(object id, Type type)
			{
				if (id == null)
					throw new ArgumentNullException("id");

				if (type == null)
					throw new ArgumentNullException("type");

				var idType = id.GetType();
				if (idType.Equals(typeof(string)))
				{
					try
					{
						id = new Guid(id.ToString());
					}
					catch
					{
					}
				}

				_id = id.ToString();
				_type = type;
			}

			public override string ToString()
			{
				return string.Format("{0}({1})", _type.Name, _id);
			}

			public override bool Equals(object obj)
			{
				if (obj is ContractKey)
                {
                    var contractKey = (ContractKey)obj;
                    return contractKey.Key == Key;
                }

                return false;
			}

			public override int GetHashCode()
			{
				return Key.GetHashCode();
			}
		}

		#endregion
		#region Private Methods

		private static ContractKey GetKey(object id, Type type)
		{
			return new ContractKey(id, type);
		}

		private static void RaiseContractNotFoundException(ContractKey id)
		{
			throw new ApplicationException(string.Format("Contract {0} not found in context {1}", id, NewHotelAppContext.CurrentContext));
		}

		#endregion
		#region Public Methods

		public static object GetKey(BaseContract contract)
		{
			return GetKey(contract.Id, contract.KeyType);
		}

		#endregion
		#region Get Methods

		private static BaseContract Get(ContractKey id)
		{
			lock (ContractNode.RootSync)
			{
				BaseContract contract;
				if (!ContractNode.Root.TryGet(id, out contract))
					RaiseContractNotFoundException(id);

				return contract;
			}
		}

		public static BaseContract Get(object id, Type type)
		{
			return Get(GetKey(id, type));
		}

		public static T Get<T>(object id)
			where T : BaseContract
		{
			return Get(id, typeof(T)) as T;
		}

		#endregion
		#region TryGet Methods

		public static bool TryGet(object id, out BaseContract contract, Type type)
		{
			lock (ContractNode.RootSync)
			{
				if (!ContractNode.Root.TryGet(GetKey(id, type), out contract))
					return false;
			}

			return true;
		}

		public static bool TryGet<T>(object id, out T contract)
			where T : BaseContract
		{
			contract = null;
			BaseContract baseContract;

			lock (ContractNode.RootSync)
			{
				if (!ContractNode.Root.TryGet(GetKey(id, typeof(T)), out baseContract))
					return false;
			}

			contract = baseContract as T;
			return true;
		}

		#endregion
		#region Exist Methods

		private static bool Exist(ContractKey id)
		{
			lock (ContractNode.RootSync)
			{
				return ContractNode.Root.Exist(id);
			}
		}

		public static bool Exist(object id, Type type)
		{
			return Exist(GetKey(id, type));
		}

		public static bool Exist<T>(object id)
			where T : BaseContract
		{
			return Exist(id, typeof(T));
		}

		public static bool Exist<T>(T contract)
			where T : BaseContract
		{
			if (contract != null)
				return Exist(contract.Id, contract.GetType());

			return false;
		}

		#endregion
		#region Add Methods

		private static void AddEnumerable(ContractNode node, object obj)
		{
			var items = obj as IEnumerable;

			if (items != null)
				foreach (var item in items)
				{
					var subContract = item as BaseContract;
					if (subContract != null)
						Add(node, subContract);
				}
		}

		private static void Add(ContractNode parent, BaseContract contract)
		{
			var node = new ContractNode(contract);
			parent.Add(node);

			AddEnumerable(node, contract);

			var type = contract.GetType();
			var accessors = type.PropAccessors().Values
				.Where(x =>
					  (typeof(BaseContract).IsAssignableFrom(x.Type) ||
					   x.Type.IsGenericType && x.Type.GetGenericTypeDefinition().Equals(typeof(TypedList<>)) &&
					   typeof(BaseContract).IsAssignableFrom(x.Type.GetGenericArguments()[0]))
				);

			foreach (var accessor in accessors)
			{
                var value = accessor.Get(contract);
				var baseContract = value as BaseContract;

                if (baseContract == null && accessor.Type.GetInterfaces().Contains(typeof(IEnumerable)))
					AddEnumerable(node, value);
                else if (baseContract != null)
                    Add(node, baseContract);
			}
		}

		private static void Add<T>(ContractKey id, T contract)
			where T : BaseContract
		{
			lock (ContractNode.RootSync)
			{
				var node = ContractNode.Root.Find(id);
				if (node == null)
					RaiseContractNotFoundException(id);

				Add(node, contract as BaseContract);
			}
		}

		public static void Add<T>(T contract)
			where T : BaseContract
		{
			lock (ContractNode.RootSync)
			{
				if (contract != null)
					Add(ContractNode.Root, contract as BaseContract);
			}
		}

		public static void Add<P, C>(P parent, C contract)
			where P : BaseContract
			where C : BaseContract
		{
			if (parent != null && contract != null)
				Add(GetKey(parent.Id, parent.GetType()), contract);
		}

		#endregion
		#region Remove Methods

		private static void Remove(ContractKey id)
		{
			lock (ContractNode.RootSync)
			{
				if (!ContractNode.Root.Remove(id))
					RaiseContractNotFoundException(id);
			}
		}

		public static void Remove(object id, Type type)
		{
			Remove(GetKey(id, type));
		}

		public static void Remove<T>(object id)
			where T : BaseContract
		{
			Remove(id, typeof(T));
		}

		public static void Remove<T>(T contract)
			where T : BaseContract
		{
			if (contract != null)
				Remove(contract.Id, contract.GetType());
		}

		#endregion
	}
}