﻿
using System;
using System.Linq;
using System.Reflection;
using NewHotel.Core;
using NewHotel.Data.Commands;

namespace NewHotel.Business
{
    public class BaseQueryContainer
    {
        public T Get<T>(params object[] args) where T : Query
        {
            return (T) Activator.CreateInstance(typeof(T), args);
        }
    }

    public class QueryContainer<T> : BaseQueryContainer
        where T : class, new()
    {
        public static readonly T Instance = new T();
    }
}
