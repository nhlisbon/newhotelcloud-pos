﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Business
{
    public struct WorkerContext
    {
        public Guid InstallationId { get; set; }
        public DateTime ClientDateTime { get; set; }
    }
}
