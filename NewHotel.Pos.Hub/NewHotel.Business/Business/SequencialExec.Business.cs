﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;
using NewHotel.Core;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Business
{
    /*
    public struct ProcessExecStatus
    {
        public readonly object Id;
        public readonly string Name;
        public readonly int Index;
        public readonly int Count;
        public int Steps;
        public int Current;
        public string Message;
        public readonly object Tag;
        public readonly SequentialExecContract Contract;
        public readonly ValidationResult Result;

        public ProcessExecStatus(object id, string name, int index, int count,
            object tag, SequentialExecContract contract, ValidationResult result)
        {
            Id = id;
            Name = name;
            Index = index;
            Count = count;
            Steps = 0;
            Current = 0;
            Message = null;
            Tag = tag;
            Contract = contract;
            Result = result;
        }
    }

    public interface ISequencialExec
    {
        BackgroundWorker BackgroundWorker { get; }
        SequentialExecContract Contract { get; }
        string[] Names { get; }
        int Count { get; }
    }

    public abstract class SequencialExec : BaseBusiness, ISequencialExec
    {
        #region Members

        private readonly IDictionary<object, ProcessData> _proceses = new Dictionary<object, ProcessData>();
        private BackgroundWorker _worker;
        private SequentialExecContract _contract;
        private ProcessExecStatus _status;
        private int _index;
        private int _count;

        #endregion
        #region Properties

        protected bool AbortPending
        {
            get { return _worker != null && _worker.CancellationPending; }
        }

        #endregion
        #region Constructor

        protected SequencialExec(IDatabaseManager manager)
            : base(manager)
        {
            _worker = new BackgroundWorker();
            _worker.WorkerReportsProgress = true;
            _worker.WorkerSupportsCancellation = true;
            Register();
        }

        #endregion
        #region Abstract Methods

        protected abstract void Register();
        protected abstract void PreExecute(IDatabaseManager manager, StringBuilder log, ValidationResult result);
        protected abstract void PostExecute(IDatabaseManager manager, StringBuilder log, ValidationResult result);
        protected abstract void AbortExecute(IDatabaseManager manager, StringBuilder log, ValidationResult result);
        protected abstract string GetLabel(string key);

        #endregion
        #region Private Clases

        private struct ProcessData
        {
            public string Name;
            public bool Mandatory;
            public Action<IDatabaseManager, object, StringBuilder, ValidationResult> Signature;
        }

        #endregion
        #region Private Methods

        private void RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            BackgroundWorker.DoWork -= new DoWorkEventHandler(DoWork);
        }

        private void DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = (BackgroundWorker)sender;

            var result = e.Argument as SequentialExecResult;
            e.Result = result.Tag;

            BaseContextCache.CopyDataToCallContext(result.Data);
            using (var manager = NewHotelAppContext.GetManager())
            {
                manager.Open();
                try
                {
                    var log = new StringBuilder();
                    _index = 0;

                    try
                    {
                        PreExecute(manager, log, result);
                        RegisterLog(manager, string.Empty, log, result);

                        _count = _proceses.Count;
                        if (_count > 0)
                        {
                            lock (Contract)
                            {
                                Contract.Header = log.ToString();
                                Contract.Successfull = false;
                                Contract.Operations = GetOperations();
                            }

                            _status = new ProcessExecStatus(0, string.Empty, 0, _count, result.Tag, Contract, result);
                            worker.ReportProgress(0, _status);

                            foreach (var process in _proceses)
                            {
                                if (worker.CancellationPending && Contract.Operations.Count > 0)
                                {
                                    e.Cancel = true;
                                    for (int i = _index; i < Contract.Operations.Count; i++)
                                        Contract.Operations[i].Aborted = true;
                                    break;
                                }

                                var operContract = Contract.Operations.Count > 0
                                    ? Contract.Operations[_index] : new SequentialOperContract(
                                        process.Key, process.Value.Name, process.Value.Mandatory,
                                        new OperationCompletionTemplate()
                                        {
                                            StartTemplate = GetLabel("OperLogStartTemplate"),
                                            SuccessTemplate = GetLabel("OperLogSuccTemplate"),
                                            FailedTemplate = GetLabel("OperLogFailTemplate"),
                                            AbortedTemplate = GetLabel("OperLogAbortTemplate")
                                        });

                                try
                                {
                                    _status = new ProcessExecStatus(process.Key, operContract.OperationName,
                                        _index, _count, result.Tag, Contract, result);
                                    worker.ReportProgress(_index / _count * 100, _status);

                                    if (operContract.Execute)
                                    {
                                        log = new StringBuilder();
                                        operContract.StartDateTime = DateTime.Now.ToUtcDateTime();

                                        try
                                        {
                                            var vres = new ValidationResult();
                                            process.Value.Signature(manager, process.Key, log, vres);
                                            result.Add(vres);

                                            RegisterLog(manager, operContract.OperationName, log, result);
                                            operContract.Successfull = vres.IsEmpty;
                                            operContract.Details = log.ToString();
                                        }
                                        finally
                                        {
                                            operContract.EndDateTime = DateTime.Now.ToUtcDateTime();
                                            operContract.Executed = true;
                                        }

                                        if (Contract.StopOnError && !operContract.Successfull)
                                            break;
                                    }
                                }
                                finally
                                {
                                    _index++;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Source["NewHotel.Business." + Name].TraceException(ex);
                        result.AddError(ex.Message);
                    }
                    finally
                    {
                        lock (Contract)
                        {
                            Contract.Successfull = !e.Cancel && result.IsEmpty;
                        }

                        log = new StringBuilder();

                        if (e.Cancel)
                            AbortExecute(manager, log, result);
                        else
                            PostExecute(manager, log, result);
                        RegisterLog(manager, string.Empty, log, result);

                        lock (Contract)
                        {
                            Contract.Footer = log.ToString();
                        }

                        _status = new ProcessExecStatus(0, string.Empty, _count, _count, result.Tag, Contract, result);
                        worker.ReportProgress(100, _status);
                    }
                }
                finally
                {
                    manager.Close();
                    BaseContextCache.ClearDataFromCallContext();
                }
            }
        }

        #endregion
        #region Protected Members

        protected virtual string Name
        {
            get { return "SequencialExec"; }
        }

        protected void RegisterProcess(object id, string name, Action<IDatabaseManager, object, StringBuilder, ValidationResult> signature, bool mandatory)
        {
            _proceses.Add(id, new ProcessData { Name = name, Signature = signature, Mandatory = mandatory });
        }

        protected void RegisterProcess(object id, string name, Action<IDatabaseManager, object, StringBuilder, ValidationResult> signature)
        {
            RegisterProcess(id, name, signature, true);
        }

        protected virtual void Initialize()
        {
        }

        protected virtual void RegisterLog(IDatabaseManager manager, string name, StringBuilder log, ValidationResult result)
        {
        }

        protected virtual SequentialExecResult CreateResult(IDictionary<string, object> data, object tag)
        {
            return new SequentialExecResult(data, tag);
        }

        protected ValidationResult Execute(object tag)
        {
            Initialize();
            var result  = CreateResult(NewHotelAppContext.OperationData, tag);
            if (!BackgroundWorker.IsBusy)
            {
                BackgroundWorker.DoWork += new DoWorkEventHandler(DoWork);
                BackgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(RunWorkerCompleted);
                BackgroundWorker.RunWorkerAsync(result);
            }

            return result;
        }

        protected void NotifyStep(int current, int steps, string message = null)
        {
            if (_worker != null && _index >= 0)
            {
                _status.Steps = steps;
                _status.Current = current;
                _status.Message = message;
                _worker.ReportProgress(_index / _count * 100, _status);
            }
        }

        protected ValidationResult Execute()
        {
            return Execute(null);
        }

        #endregion
        #region ISequencialExec Members

        public BackgroundWorker BackgroundWorker
        {
            get { return _worker; }
        }

        public SequentialExecContract Contract
        {
            get
            {
                if (_contract == null)
                {
                    _contract = new SequentialExecContract(NewHotelAppContext.GetInstance().GetWorkDate());
                    _contract.Operations = GetOperations();
                }

                return _contract;
            }
        }

        private TypedList<SequentialOperContract> GetOperations()
        {
            if (_contract != null)
            {
                var operCompletionTemplate = new OperationCompletionTemplate()
                {
                    StartTemplate = GetLabel("OperLogStartTemplate"),
                    SuccessTemplate = GetLabel("OperLogSuccTemplate"),
                    FailedTemplate = GetLabel("OperLogFailTemplate"),
                    AbortedTemplate = GetLabel("OperLogAbortTemplate")
                };

                int index = 0;
                var opers = new List<SequentialOperContract>(_proceses.Count);
                foreach (var process in _proceses)
                {
                    opers.Add(new SequentialOperContract(process.Key,
                        process.Value.Name, process.Value.Mandatory,
                        operCompletionTemplate));
                    index++;
                }

                return new TypedList<SequentialOperContract>(opers.ToArray());
            }

            return null;
        }

        public string[] Names
        {
            get { return _proceses.Values.Select(x => x.Name).ToArray(); }
        }

        public int Count
        {
            get { return _proceses.Count; }
        }

        #endregion
    }
    */
}
