﻿using System;
using System.Linq;
using System.Collections.Generic;
using NewHotel.Core;
using NewHotel.Common;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;
using System.Data;

namespace NewHotel.Business
{
    public interface IAdminBusiness
    {
        #region Hotels
        ValidationContractResult NewHotelMgr();
        ValidationContractResult LoadHotelMgr(Guid hotelId);
        ValidationResult PersistHotelMgr(HotelAppContract hotelMrgContract);
        
        #endregion

        #region Application
        ValidationContractResult NewApplication();
        ValidationContractResult LoadApplication(long appId);
        ValidationResult PersistApplication(ApplicationModuleContract appContract);
        #endregion

        #region Roles
        ValidationContractResult NewRole();
        ValidationContractResult LoadRole(long roleId);
        ValidationResult PersistRole(RoleContract roleContract);
        #endregion

        #region User
        ValidationContractResult NewUser();
        ValidationContractResult LoadUser(Guid userId);
        ValidationResult PersistUser(UserAppContract userContract);
        ValidationResult InactiveUser(Guid UserId, Guid hotelbyuserId, bool inactive);
        ValidationResult DeleteUser(Guid UserId);
        ValidationResult AddHotelsbyUser(Guid[] ids, UserAppContract usercontract);
        ValidationResult RemoveHotelsbyUser(Guid[] ids, UserAppContract usercontract);
        #endregion

        #region GroupHotel
        ValidationContractResult NewGroupHotel();
        ValidationContractResult LoadGroupHotel(Guid groupId);
        ValidationResult PersistGroupHotel(GroupHotelContract groupHotelContract);
        #endregion

        #region Taxes
        ValidationContractResult NewTaxRateApp();
        ValidationResult PersistTaxRateApp(TaxesRateAppContract TaxesRateAppContract);
        ValidationContractResult LoadTaxRateApp(Guid taxesRateId);
        ValidationResult DeleteTaxRateApp(Guid taxesRateId);

        ValidationContractResult NewTaxesSequenceApp();
        ValidationResult PersistTaxesSequenceApp(TaxesSequenceAppContract TaxesSequenceAppContract);
        ValidationContractResult LoadTaxesSequenceApp(Guid taxesSequenceId);
        ValidationResult DeleteTaxesSequenceApp(Guid taxesSequenceId);

        ValidationContractResult NewTaxRegionApp();
        ValidationResult PersistTaxRegionApp(TaxesRegionAppContract TaxesRegionAppContract);
        ValidationContractResult LoadTaxRegionApp(Guid taxesRegionId);
        ValidationResult DeleteTaxRegionApp(Guid taxesRegionId);
        #endregion

        #region Permission
        ValidationResult PersistPermission(List<RolPermissionRelationContract> PermissionbyRoleList, long rolId);
        #endregion
    }

    public class AdminBusiness : BaseBusiness, IAdminBusiness
    {
        public AdminBusiness(IDatabaseManager manager)
            : base(manager) { }

        #region Hotels
        public ValidationContractResult NewHotelMgr()
        {
            Hotel hotel = new Hotel(Manager);
            HotelAppContract hotelAppContract = new HotelAppContract();
            hotelAppContract.SetValues(hotel.Values);
            ContractFactory.Add(hotelAppContract);

            return new ValidationContractResult(hotelAppContract);
        }
        public ValidationContractResult LoadHotelMgr(Guid hotelId)
        {
            ValidationContractResult result;

            HotelAppContract hotelAppContract;
            if (!ContractFactory.TryGet(hotelId, out hotelAppContract))
            {
                Manager.Open();
                try
                {
                    Hotel hotel = new Hotel(Manager, hotelId);
                    hotelAppContract = new HotelAppContract();
                    hotelAppContract.SetValues(hotel.Values);
                    ContractFactory.Add(hotelAppContract);

                    result = new ValidationContractResult(hotelAppContract);
                }
                finally
                {
                    Manager.Close();
                }
            }
            else
                result = new ValidationContractResult(hotelAppContract);

            return result;
        }
        public ValidationResult PersistHotelMgr(HotelAppContract hotelAppContract)
        {
            Manager.Open();
            Manager.BeginTransaction();
            try
            {
                ValidationResult result = new ValidationResult();

                if (result.IsEmpty)
                {
                    Hotel hotel = new Hotel(Manager);
                    hotel.Values = hotelAppContract.GetValues();
                    hotel.GroupHotelId = hotelAppContract.GroupHotelId;
                    hotel.SaveObject();
                }

                Manager.EndTransaction(result);
                if (result.IsEmpty)
                {
                    //Actualizar el manager
                    AppContext.ClearManagers();
                    result = PersistHotelBase(hotelAppContract);
                }
                return result;
            }
            catch (Exception)
            {
                Manager.RollbackTransaction();
                throw;
            }
            finally
            {
                Manager.Close();
            }
        }
        public ValidationResult PersistHotelBase(HotelAppContract contract)
        {
            IDatabaseManager NewManager = AppContext.GetManager((Guid)contract.Id,0);
            try
            {
                NewManager.Open();
                NewManager.BeginTransaction();
                try
                {
                    string flagTrue = "'1'";
                    string flagFalse = "'0'";

                    // hotel + fecha trabajo
                    string strFacilityID = String.Format("'{0}'", Guid.NewGuid().ToHex());
                    NewManager.ExecuteNonQuery(CommandType.Text, "INSERT INTO TNHT_BENTI(BENTI_PK, BENTI_ABRE, BENTI_DESC, LICL_PK) " +
                        "VALUES(" + strFacilityID + ", '" + contract.Abbrevation + "', '" + contract.Description + "', " + contract.Language.ToString() + ")");
                    NewManager.ExecuteNonQuery(CommandType.Text, "INSERT INTO TNHT_HOTE(HOTE_PK) VALUES(" + strFacilityID + ")");
                    NewManager.ExecuteNonQuery(CommandType.Text, "INSERT INTO TNHT_DATR(DATR_PK, APPL_PK, HOTE_PK, DATR_DATR, DATR_TUTR) " +
                        "SELECT SYS_GUID(), APPL_PK, " + strFacilityID + ", TRUNC(SYSDATE), 1 FROM TNHT_APPL");
                    // moneda base
                    string strBaseCurrency = String.Format("'{0}'", Guid.NewGuid().ToHex());
                    NewManager.ExecuteNonQuery(CommandType.Text, "INSERT INTO TNHT_HUNMO(HUNMO_PK, UNMO_PK, HOTE_PK, UNMO_CAJA, UNMO_DEVO) " +
                        "VALUES(" + strBaseCurrency + ", '" + contract.Currency + "', " + strFacilityID + ", '1', '1')");
                    // taxregion
                    string strRegion = String.Format("'{0}'", Guid.NewGuid().ToHex());
                    string strLiteRegion = String.Format("'{0}'", Guid.NewGuid().ToHex());
                    NewManager.ExecuteNonQuery(CommandType.Text, "INSERT INTO TNHT_LITE(LITE_PK) VALUES(" + strLiteRegion + ")");
                    NewManager.ExecuteNonQuery(CommandType.Text, "INSERT INTO TNHT_MULT(LITE_PK, LANG_PK, MULT_DESC) " +
                        "VALUES(" + strLiteRegion + ", 1033, '" + contract.TaxRegion + "')");
                    NewManager.ExecuteNonQuery(CommandType.Text, "INSERT INTO TNHT_REGI(REGI_PK, NACI_PK, LITE_PK) " +
                        "VALUES(" + strRegion + ", '" + contract.Country + "', " + strLiteRegion + ")");
                    // parameters
                    //NewManager.ExecuteNonQuery(CommandType.Text, "INSERT INTO TCFG_GENE(GENE_PK, DATE_VERS) VALUES(1, TRUNC(SYSDATE))");
                    NewManager.ExecuteNonQuery(CommandType.Text, "INSERT INTO TCFG_HOTE(HOTE_PK, PARA_AIAP, REGI_PK, UNMO_BASE, PARA_MIVA, NACI_PK) " +
                        "VALUES(" + strFacilityID + ", " + contract.InitialYear.ToString() + ", " + strRegion + ", " + strBaseCurrency + ", 1921,'" + contract.Country + "' )");
                    NewManager.ExecuteNonQuery(CommandType.Text, "INSERT INTO TCFG_RESE(HOTE_PK, GUEST_CTRL, GUEST_IDEN, GUEST_CARDEX, GUEST_AGE, " +
                        "RATE_DUSE, UNMO_MAST, UNMO_EXT1, UNMO_EXT2, UNMO_EXT3, UNMO_EXT4, UNMO_EXT5) VALUES( " +
                        strFacilityID + ", 241, 5, 5, 5, 511, '" + contract.Currency + "', '" + contract.Currency + "', '" +
                        contract.Currency + "', '" + contract.Currency + "', '" + contract.Currency + "', '" + contract.Currency + "')");
                    NewManager.ExecuteNonQuery(CommandType.Text, "INSERT INTO TCFG_TEMP(HOTE_PK, RESE_MPAL, RESE_ALDE, RESE_TRES, RESE_CKINAUTH, " +
                        "RESE_CONFSTAT, RESE_NUAD, RESE_NUCR, RESE_NUIN, GRUP_CONFTIPO, GRUP_CONFSTAT, RESE_NACI, ENTI_NACI, " +
                        "ENTI_CATE, ENTI_FOFA, ENTI_TIFA, ENTI_FAMA, GUEST_NACI, GUEST_GENDER, GUEST_CVIL, GUEST_TIHO, OPER_TIPO, TPDI_TIPR) " +
                        "VALUES(" + strFacilityID + ", 1262, 1411, 211, 2, 1022, 1, 0, 0, 1891, 2, '" + contract.Country + "', '" +
                         contract.Country + "', 2321, 392, 422, 211, '" + contract.Country + "', 751, 181, 31, 2351, 3)");
                    NewManager.ExecuteNonQuery(CommandType.Text, "INSERT INTO TCFG_PREC(HOTE_PK) VALUES(" + strFacilityID + ")");
                    NewManager.ExecuteNonQuery(CommandType.Text, "INSERT INTO TCFG_RECU(HOTE_PK, PARA_ROST, ROOM_DIRT, OCUP_CRIT, PARA_PEOC, PARA_BLIN) " +
                        "VALUES(" + strFacilityID + ", 1711, 1981, 1681, 85, " + flagTrue + ")");
                    NewManager.ExecuteNonQuery(CommandType.Text, "INSERT INTO TCFG_FACT(HOTE_PK, SEDO_RULE) VALUES(" + strFacilityID + ", 2381)");

                    NewManager.CommitTransaction();
                    return new ValidationResult();
                }
                catch (Exception ex)
                {
                    NewManager.RollbackTransaction();
                    return new ValidationResult(ex);
                }
            }
            finally
            {
                NewManager.Close();
            }
        }
        #endregion

        #region Application
        public ValidationContractResult NewApplication()
        {
            ApplicationModule app = new ApplicationModule(Manager);
            ApplicationModuleContract appContract = new ApplicationModuleContract();
            appContract.SetValues(app.Values);
            ContractFactory.Add(appContract);

            return new ValidationContractResult(appContract);
        }
        public ValidationContractResult LoadApplication(long appId)
        {
            ValidationContractResult result;

            ApplicationModuleContract appContract;
            if (!ContractFactory.TryGet(appId, out appContract))
            {
                Manager.Open();
                try
                {
                    ApplicationModule app = new ApplicationModule(Manager, appId);
                    appContract.SetValues(app.Values);
                    ContractFactory.Add(appContract);

                    result = new ValidationContractResult(appContract);
                }
                finally
                {
                    Manager.Close();
                }
            }
            else
                result = new ValidationContractResult(appContract);

            return result;
        }
        public ValidationResult PersistApplication(ApplicationModuleContract appContract)
        {
            Manager.Open();
            Manager.BeginTransaction();
            try
            {
                ValidationResult result = new ValidationResult();

                if (result.IsEmpty)
                {
                    ApplicationModule app = new ApplicationModule(Manager);
                    app.Values = appContract.GetValues();
                    app.SaveObject();
                }

                Manager.EndTransaction(result);
                return result;
            }
            catch (Exception)
            {
                Manager.RollbackTransaction();
                throw;
            }
            finally
            {
                Manager.Close();
            }
        }
        #endregion

        #region Roles
        public ValidationContractResult NewRole()
        {
            Role role = new Role(Manager);
            RoleContract roleContract = new RoleContract();
            roleContract.SetValues(role.Values);
            ContractFactory.Add(roleContract);

            return new ValidationContractResult(roleContract);
        }

        public ValidationContractResult LoadRole(long roleId)
        {
            ValidationContractResult result;

            RoleContract roleContract;
            if (!ContractFactory.TryGet(roleId, out roleContract))
            {
                Manager.Open();
                try
                {
                    Role role = new Role(Manager, roleId);
                    roleContract = new RoleContract();
                    roleContract.SetValues(role.Values);
                    ListData list = GetPermits(roleId, null);
                    if (!list.IsEmpty)
                    {
                        do
                        {
                            RolPermissionRelationContract rolPermissionRelationContract =
                                new RolPermissionRelationContract(list.ValueAs<Guid>("rope_pk"),list.ValueAs<long>("role_pk"),
                                    list.ValueAs<SecurityCode>("secu_code"), list.ValueAs<Guid>("perm_pk"), list.ValueAs<string>("perm_desc"));
                            roleContract.Permissions.Add(rolPermissionRelationContract);
                            ContractFactory.Add(roleContract);
                        }
                        while (list.Next());
                    }
                  
                    result = new ValidationContractResult(roleContract);
                }
                finally
                {
                    Manager.Close();
                }
            }
            else
                result = new ValidationContractResult(roleContract);

            return result;
        }

        private ListData GetPermits(long roleId, Guid? permId)
        {
            Query query = AdminQueryContainer.Instance.GetPermissionbyRol(Manager);
            query.Filters["role_pk"].Value = roleId;
            if(permId.HasValue)
               query.Filters["perm_pk"].Value = (Guid)permId;
            query.Sorts.AddAsc("perm_desc");
            return query.Execute();

        }

        private ListData GetRole(long roleId)
        {
            Query query = AdminQueryContainer.Instance.GetRole(Manager);
            query.Filters["role_pk"].Value = roleId;
            return query.First();

        }
        public ValidationResult PersistRole(RoleContract roleContract)
        {
            Manager.Open();
            Manager.BeginTransaction();
            try
            {

                ValidationResult result = new ValidationResult();

                Role role = new Role(Manager);
                role.Values = roleContract.GetValues();

                // sincronizar los hoteles
                roleContract.Permissions.Syncronize(Manager, role.RolePermission,
                    (po, pc, f) =>
                    {
                        po.RoleId = (long)role.Id;
                        po.PermissionApplicationId = pc.PermissionApplicationId;
                    }
                );

                role.SaveObject();
                Manager.CommitTransaction();

                return result;
            }
            catch (Exception)
            {
                Manager.RollbackTransaction();
                throw;
            }
            finally
            {
                Manager.Close();
            }
        }
        #endregion

        #region User

        public ValidationContractResult NewUser()
        {
            UserApp user = new UserApp(Manager);
            UserAppContract userContract = new UserAppContract();
            userContract.SetValues(user.Values);
            ContractFactory.Add(userContract);

            return new ValidationContractResult(userContract);
        }
        public ValidationContractResult LoadUser(Guid userId)
        {
            ValidationContractResult result =  null;

            UserAppContract userContract;
            if (!ContractFactory.TryGet(userId, out userContract))
            {
                Manager.Open();
                try
                {
                    UserApp user = new UserApp(Manager, userId);
                    if (userContract == null) userContract = new UserAppContract();
                    userContract.SetValues(user.Values);
                    ListData list = GetUser(userId);
                    if (!list.IsEmpty)
                    {
                        do
                        {
                            HotelbyUserRelationContract hotelbyUserRelationContract =
                                new HotelbyUserRelationContract(list.ValueAs<Guid>("hout_pk"), list.ValueAs<Guid>("hote_pk"),
                                    list.ValueAs<bool>("hout_inac"), list.ValueAs<string>("hote_desc"));

                            userContract.UserbyHotels.Add(hotelbyUserRelationContract);

                        }
                        while (list.Next());
                    }
                    ContractFactory.Add(userContract);
                    result = new ValidationContractResult(userContract);
                }
                finally
                {
                    Manager.Close();
                }
            }
            else
                result = new ValidationContractResult(userContract);

            return result;
        }
        private ListData GetUser( Guid userId)
        {
            Query query = AdminQueryContainer.Instance.GetUserSearch(Manager);
            query.Filters["util_pk"].Value = userId;
            return query.Execute();

        }
        private ListData GetHotelByUser(Guid hotelId)
        {
            Query query = AdminQueryContainer.Instance.GetHotelByUser(Manager);
            query.Filters["hote_pk"].Value = hotelId;
            return query.Execute();

        }
        private ListData GetHotel(Guid[] hotelIds)
        {
            Query query = AdminQueryContainer.Instance.GetHotel(Manager);
            query.Filters["in_hote"].Value = hotelIds;
            return query.Execute();
        }
        public ValidationResult InactiveUser(Guid UserId, Guid hotelbyuserId, bool inactive)
        {
            Manager.Open();
            ValidationResult result = new ValidationResult() ;
            try
            {
                Manager.BeginTransaction();
                try
                {

                    HotelbyUser hotelbyUser = new HotelbyUser(Manager, hotelbyuserId);
                    hotelbyUser.InactiveUser = inactive;
                    hotelbyUser.SaveObject();
                    Manager.CommitTransaction();
                    
                }
                catch
                {
                    Manager.RollbackTransaction();
                }
            }
            catch (Exception)
            {
                Manager.RollbackTransaction();
                throw;
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }
        public ValidationResult PersistUser(UserAppContract userContract)
        {
            Manager.Open();
            Manager.BeginTransaction();
            try
            {
                ValidationResult result = new ValidationResult();

                UserApp userApp = new UserApp(Manager);
                userApp.Values = userContract.GetValues();

                // sincronizar los hoteles
                userContract.UserbyHotels.Syncronize(Manager, userApp.HotelbyUser,
                    (po, pc, f) =>
                    {
                        po.UserId = (Guid)userApp.Id;
                        po.HotelId = pc.HotelorUserId;
                    }
                );

                userApp.SaveObject();
                Manager.CommitTransaction();

                // Salvar los usuarios en la instalacion
                foreach (var item in userContract.UserbyHotels)
                {
                    IDatabaseManager NewManager = AppContext.GetManager(item.HotelorUserId);
                    try
                    {
                        NewManager.Open();
                        NewManager.BeginTransaction();
                        User userinst = new User(NewManager);
                        userinst.Login = userApp.Login;
                        userinst.Password = userApp.Password;
                        userinst.Description = userApp.Description;
                        userinst.InstallationId = item.HotelorUserId;
                        userinst.RegistrationDate = DateTime.Now.Date;
                        userinst.SaveObject();
                        NewManager.CommitTransaction();
                    }
                    catch (Exception)
                    {
                        NewManager.RollbackTransaction();
                        throw;
                    }
                    finally
                    {
                        NewManager.Close();
                    }
                }
                
                return result;
            }
            catch (Exception)
            {
                Manager.RollbackTransaction();
                throw;
            }
            finally
            {
                Manager.Close();
            }
        }
        public ValidationResult DeleteUser(Guid userId)
        {
            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    UserApp user = new UserApp(Manager, userId);
                    user.DeleteObject();
                    Manager.CommitTransaction();
                    // Borrar los usuarios en la instalacion
                    foreach (var item in user.HotelbyUser)
                    {
                        IDatabaseManager NewManager = AppContext.GetManager(item.HotelId);
                        try
                        {
                            NewManager.Open();
                            NewManager.BeginTransaction();
                            User userinst = new User(NewManager);
                            userinst.DeleteObject();
                            NewManager.CommitTransaction();
                        }
                        catch
                        {
                            NewManager.RollbackTransaction();
                            throw;
                        }
                        finally
                        {
                            NewManager.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    return new ValidationResult(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return new ValidationResult();
        }
        public ValidationResult AddHotelsbyUser(Guid[] ids , UserAppContract usercontract)
        {

            ListData list = GetHotel(ids);
            if (!list.IsEmpty)
            {
                do
                {
                    Guid id = list.ValueAs<Guid>("hote_pk");
                    if (usercontract.UserbyHotels.Any(x => x.HotelorUserId == id))
                    {
                        ValidationContractResult result = new ValidationContractResult();
                        result.AddError(256,"Duplicated hotel by user");
                        return result;
                    }

                    HotelbyUser hotelbyUser = new HotelbyUser(Manager);
                    HotelbyUserRelationContract hotelbyUserContract =
                        new HotelbyUserRelationContract((Guid)hotelbyUser.Id,
                            id, false,list.ValueAs<string>("hote_desc"));
                    usercontract.UserbyHotels.Add(hotelbyUserContract);
                    ContractFactory.Add(usercontract, hotelbyUserContract);
                } 
                while (list.Next());
            }

            return new ValidationResult();
        }
        public ValidationResult RemoveHotelsbyUser(Guid[] ids, UserAppContract usercontract)
        {
            foreach (var id in ids)
            {
                HotelbyUserRelationContract contract = usercontract.UserbyHotels.Remove(id);

                if (contract != null)
                    ContractFactory.Remove(contract);
            }

            return new ValidationResult();
        }

        #endregion

        #region Permission
        public ValidationResult PersistPermission(List<RolPermissionRelationContract> PermissionbyRoleList, long rolId)
        {
            Manager.Open();
            try
            {
                ValidationResult result = new ValidationResult();
                
                Manager.Open();
                try
                {
                    Manager.BeginTransaction();
                    foreach (var item in PermissionbyRoleList)
                    {
                        RolePermission rolpermission = new RolePermission(Manager, (Guid)item.Id);
                        rolpermission.SecurityCode = item.SecurityCode;
                        rolpermission.SaveObject();
                    }
                       
                    Manager.CommitTransaction();
                }
                finally
                {
                    Manager.Close();
                }
                return result;
            }
            catch (Exception)
            {
                Manager.RollbackTransaction();
                throw;
            }
            finally
            {
                Manager.Close();
            }
        }
        #endregion

        #region Group
        public ValidationContractResult NewGroupHotel()
        {
            GroupHotel groupHotel = new GroupHotel(Manager);
            GroupHotelContract groupHotelContract = new GroupHotelContract();
            groupHotelContract.SetValues(groupHotel.Values);
            ContractFactory.Add(groupHotelContract);

            return new ValidationContractResult(groupHotelContract);
        }
        public ValidationContractResult LoadGroupHotel(Guid groupId)
        {
            ValidationContractResult result;

            GroupHotelContract groupHotelContract;
            if (!ContractFactory.TryGet(groupId, out groupHotelContract))
            {
                Manager.Open();
                try
                {
                    GroupHotel group = new GroupHotel(Manager, groupId);
                    groupHotelContract = new GroupHotelContract();
                    groupHotelContract.SetValues(group.Values);
                    ContractFactory.Add(groupHotelContract);
                    result = new ValidationContractResult(groupHotelContract);
                }
                finally
                {
                    Manager.Close();
                }
            }
            else
                result = new ValidationContractResult(groupHotelContract);

            return result;
        }
        public ValidationResult PersistGroupHotel(GroupHotelContract groupHotelContract)
        {
            Manager.Open();
            Manager.BeginTransaction();
            try
            {
                ValidationResult result = new ValidationResult();

                if (result.IsEmpty)
                {
                    GroupHotel grouphotel = new GroupHotel(Manager);
                    grouphotel.Values = groupHotelContract.GetValues();
                    grouphotel.SaveObject();
                }

                Manager.EndTransaction(result);
                return result;
            }
            catch (Exception)
            {
                Manager.RollbackTransaction();
                throw;
            }
            finally
            {
                Manager.Close();
            }
        }
        #endregion

        #region Taxes
        #region Tax Rate

        public ValidationContractResult NewTaxRateApp()
        {
            Manager.Open();
            try
            {
                TaxRateApp taxesRate = new TaxRateApp(Manager);
                TaxesRateAppContract taxesRateAppContract = new TaxesRateAppContract();
                taxesRateAppContract.SetValues(taxesRate.Values);
                taxesRateAppContract.Description.CopyFrom(taxesRate.Description);
                ContractFactory.Add(taxesRateAppContract);

                return new ValidationContractResult(taxesRateAppContract);
            }
            finally
            {
                Manager.Close();
            }
        }

        public ValidationContractResult LoadTaxRateApp(Guid taxesRateId)
        {
            ValidationContractResult result;

            TaxesRateAppContract contract;
            if (!ContractFactory.TryGet(taxesRateId, out contract))
            {
                Manager.Open();
                try
                {
                    TaxRateApp taxesRate = new TaxRateApp(Manager, taxesRateId);
                    contract = new TaxesRateAppContract();
                    contract.SetValues(taxesRate.Values);
                    contract.Description.CopyFrom(taxesRate.Description);

                    ContractFactory.Add(contract);

                    result = new ValidationContractResult(contract);
                }
                finally
                {
                    Manager.Close();
                }
            }
            else
                result = new ValidationContractResult(contract);

            return result;
        }

        public ValidationResult PersistTaxRateApp(TaxesRateAppContract taxesRateAppContract)
        {
            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    TaxRateApp taxesRate = new TaxRateApp(Manager);
                    taxesRate.Values = taxesRateAppContract.GetValues();
                    taxesRateAppContract.Description.CopyTo(taxesRate.Description);

                    taxesRate.SaveObject();
                    Manager.CommitTransaction();

                    // Salvar los Taxe en la instalacion
                        //IDatabaseManager NewManager = AppContext.GetManager();
                        //try
                        //{
                        //    NewManager.Open();
                        //    NewManager.BeginTransaction();
                        //    User userinst = new User(NewManager);
                        //    userinst.Login = userApp.Login;
                        //    userinst.Password = userApp.Password;
                        //    userinst.Description = userApp.Description;
                        //    userinst.InstallationId = item.HotelorUserId;
                        //    userinst.RegistrationDate = DateTime.Now.Date;
                        //    userinst.SaveObject();
                        //    NewManager.CommitTransaction();
                        //}
                        //catch (Exception)
                        //{
                        //    NewManager.RollbackTransaction();
                        //    throw;
                        //}
                        //finally
                        //{
                        //    NewManager.Close();
                        //}
                
                }
                catch (Exception)
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
            finally
            {
                Manager.Close();
            }

            return new ValidationResult();
        }

        public ValidationResult DeleteTaxRateApp(Guid taxesRateId)
        {
            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    TaxRateApp taxesRate = new TaxRateApp(Manager, taxesRateId);
                    taxesRate.DeleteObject();

                    Manager.CommitTransaction();
                }
                catch (Exception)
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
            finally
            {
                Manager.Close();
            }

            return new ValidationResult();
        }

        #endregion
        #region Tax Sequence

        public ValidationContractResult NewTaxesSequenceApp()
        {
            Manager.Open();
            try
            {
                TaxSequenceApp taxesSequence = new TaxSequenceApp(Manager);
                TaxesSequenceAppContract TaxesSequenceAppContract = new TaxesSequenceAppContract();
                TaxesSequenceAppContract.SetValues(taxesSequence.Values);
                TaxesSequenceAppContract.Description.CopyFrom(taxesSequence.Description);
                ContractFactory.Add(TaxesSequenceAppContract);

                return new ValidationContractResult(TaxesSequenceAppContract);
            }
            finally
            {
                Manager.Close();
            }
        }

        public ValidationContractResult LoadTaxesSequenceApp(Guid taxesSequenceId)
        {
            ValidationContractResult result;

            TaxesSequenceAppContract contract;
            if (!ContractFactory.TryGet(taxesSequenceId, out contract))
            {
                Manager.Open();
                try
                {
                    TaxSequenceApp taxesSequence = new TaxSequenceApp(Manager, taxesSequenceId);
                    contract = new TaxesSequenceAppContract();
                    contract.SetValues(taxesSequence.Values);
                    contract.Description.CopyFrom(taxesSequence.Description);
                    ContractFactory.Add(contract);

                    result = new ValidationContractResult(contract);
                }
                finally
                {
                    Manager.Close();
                }
            }
            else
                result = new ValidationContractResult(contract);

            return result;
        }

        public ValidationResult PersistTaxesSequenceApp(TaxesSequenceAppContract taxesSequenceAppContract)
        {
            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {

                    TaxSequenceApp taxesSequence = new TaxSequenceApp(Manager);
                    taxesSequence.Values = taxesSequenceAppContract.GetValues();
                    taxesSequenceAppContract.Description.CopyTo(taxesSequence.Description);
                    taxesSequence.SaveObject();

                    Manager.CommitTransaction();
                }
                catch (Exception)
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
            finally
            {
                Manager.Close();
            }

            return new ValidationResult();
        }

        public ValidationResult DeleteTaxesSequenceApp(Guid taxesSequenceId)
        {
            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    TaxSequenceApp taxesSequence = new TaxSequenceApp(Manager, taxesSequenceId);
                    taxesSequence.DeleteObject();

                    Manager.CommitTransaction();
                }
                catch (Exception)
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
            finally
            {
                Manager.Close();
            }

            return new ValidationResult();
        }

        #endregion
        #region Tax Region

        public ValidationContractResult NewTaxRegionApp()
        {
            Manager.Open();
            try
            {
                TaxRegionApp taxesRegion = new TaxRegionApp(Manager);
                TaxesRegionAppContract taxesRegionAppContract = new TaxesRegionAppContract();
                taxesRegionAppContract.SetValues(taxesRegion.Values);
                taxesRegionAppContract.Description.CopyFrom(taxesRegion.Description);
                ContractFactory.Add(taxesRegionAppContract);

                return new ValidationContractResult(taxesRegionAppContract);
            }
            finally
            {
                Manager.Close();
            }
        }

        public ValidationContractResult LoadTaxRegionApp(Guid taxesRegionId)
        {
            ValidationContractResult result;

            TaxesRegionAppContract contract;
            if (!ContractFactory.TryGet(taxesRegionId, out contract))
            {
                Manager.Open();
                try
                {
                    TaxRegionApp taxesRegion = new TaxRegionApp(Manager, taxesRegionId);
                    contract = new TaxesRegionAppContract();
                    contract.SetValues(taxesRegion.Values);
                    contract.Description.CopyFrom(taxesRegion.Description);

                    ContractFactory.Add(contract);

                    result = new ValidationContractResult(contract);
                }
                finally
                {
                    Manager.Close();
                }
            }
            else
                result = new ValidationContractResult(contract);

            return result;
        }

        public ValidationResult PersistTaxRegionApp(TaxesRegionAppContract taxesRegionAppContract)
        {
            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    TaxRegionApp taxesRegion = new TaxRegionApp(Manager);
                    taxesRegion.Values = taxesRegionAppContract.GetValues();
                    taxesRegionAppContract.Description.CopyTo(taxesRegion.Description);

                    taxesRegion.SaveObject();

                    Manager.CommitTransaction();
                }
                catch (Exception)
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
            finally
            {
                Manager.Close();
            }

            return new ValidationResult();
        }

        public ValidationResult DeleteTaxRegionApp(Guid taxesRegionId)
        {
            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    TaxRegionApp taxesRegion = new TaxRegionApp(Manager, taxesRegionId);
                    taxesRegion.DeleteObject();

                    Manager.CommitTransaction();
                }
                catch (Exception)
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
            finally
            {
                Manager.Close();
            }

            return new ValidationResult();
        }

        #endregion
      
        #endregion



    }

   }
