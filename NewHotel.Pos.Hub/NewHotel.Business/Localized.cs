﻿using System;
using System.Globalization;
using NewHotel.Localization;
using NewHotel.Core;

namespace NewHotel.Business
{
    public static class Localized
    {
        private static CultureInfo Culture
        {
            get { return new CultureInfo((int)AppContext.GetLanguage()); }
        }

        public static string GetLabel(string key)
        {
            return new LocalizedText(Culture, key, LocalizedSuffix.Label);
        }

        public static string GetColumn(string key)
        {
            return new LocalizedText(Culture, key, LocalizedSuffix.Column);
        }

        public static string GetMessage(string key)
        {
            return new LocalizedText(Culture, key, LocalizedSuffix.Message);
        }
    }
}
