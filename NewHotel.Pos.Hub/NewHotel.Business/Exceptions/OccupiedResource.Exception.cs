﻿using System;

namespace NewHotel.Business
{
    public class OccupiedResourceException : Exception
    {
        public OccupiedResourceException(Exception ex)
            : base("Occupied resource", ex) { }
    }
}