﻿using System;

namespace NewHotel.Business
{
    public class OutOfRentalResourceException : Exception
    {
        public OutOfRentalResourceException(Exception ex)
            : base("Out of rental resource", ex) { }
    }
}