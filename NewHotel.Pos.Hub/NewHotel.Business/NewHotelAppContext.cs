﻿using System;
using NewHotel.Core;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Business
{
    [DomainType(typeof(SecurityCode))]
    [DomainType(typeof(FilterTypes))]
    public sealed class NewHotelAppContext : NewHotelContext
    {
        
    }
}
