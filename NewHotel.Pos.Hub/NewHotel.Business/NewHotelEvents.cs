﻿using System;

namespace NewHotel.Business
{
    public static class NewHotelEvents
    {
        public const string WorkShiftChanged = "WORKSHIFT_CHANGED";
    }
}
