﻿using Microsoft.Extensions.Options;
using NewHotel.Core;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries;
using NewHotel.Scheduler;
using System;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.BackgroundTasks
{
	public class NewStockState
	{
		public string Schema { get; set; }
		public string Password { get; set; }
		public string Server { get; set; }
	}

	public class StockExistence
	{
		public string ProductId { get; set; }
		public decimal Quantity { get; set; }
	}

	public class NewStockExistenceQuery : Query
	{
		public NewStockExistenceQuery(IDatabaseManager manager)
			: base(nameof(NewStockExistenceQuery), manager)
		{
		}

		protected override string GetCommandText()
		{
			return @"
select artg.nhcl_caux,sum(exas.exas_total) exas_total
from tnht_artg artg
join vnst_exas_artg exas on artg.artg_codi=exas.artg_codi
group by artg.nhcl_caux
			";
		}

		protected override void Initialize()
		{
		}
	}

	public class NewStockTask : IActionJob
	{
		private readonly IDatabaseManager manager;
		private readonly IOptions<NewStockState> options;

		public NewStockTask(IDatabaseManager manager, IOptions<NewStockState> options)
		{
			this.manager = manager;
			this.options = options;
		}

		public Task<object> Execute(object state)
		{
			using (IDatabaseManager stockManager = NewHotelAppContext.GetManager(options.Value.Schema, options.Value.Password, options.Value.Server, string.Empty))
			{
				var query = new NewStockExistenceQuery(stockManager);
			}

			var upsertQuery = CommonQueryFactory.UpdateOrCreateProductStock(manager);
			upsertQuery.Parameters["arst_pk"] = Guid.NewGuid();
			//upsertQuery.Parameters["artg_pk"] = productId;
			//upsertQuery.Parameters["arst_qtds"] = quantity;
			upsertQuery.Parameters["arst_datr"] = DateTime.Now.Date;

			upsertQuery.Execute();

			throw new System.NotImplementedException();
		}

		public Task<bool> MustRepeat(object state)
		{
			throw new System.NotImplementedException();
		}
	}
}
