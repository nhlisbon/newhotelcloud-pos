﻿using Microsoft.Extensions.Options;
using NewHotel.Pos.Hub.Business.Business;
using NewHotel.Pos.Hub.Business.IntegrationConfigs;
using NewHotel.Scheduler;
using System;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.BackgroundTasks
{
	public class MxmState
	{
		public int Counter { get; set; }
		public int MaxRetries { get; set; }

		public override string ToString()
		{
			return $"Counter: {Counter}";
		}
	}

	public class MxmTask : IActionJob
	{
		private readonly IMxmBusiness _mxmBusiness;
		private readonly IIntegrationBusiness _integrationBusiness;
		private readonly IOptions<MxmTaskConfig> _options;
		private readonly bool _hasPath;

		public MxmTask(
			IMxmBusiness mxmBusiness,
			IIntegrationBusiness integrationBusiness,
			IOptions<MxmTaskConfig> options
		)
		{
			_integrationBusiness = integrationBusiness;
			_mxmBusiness = mxmBusiness;
			_options = options;
			_hasPath = false;

			var path = _options.Value.MxmTaskLogsPath;
			if (!System.IO.File.Exists(path))
			{
				try
				{
					Console.WriteLine($"Creating file {path}");
					_hasPath = true;
					Log("CREATED");
				}
				catch (Exception ex)
				{
					Console.WriteLine($"Error creating file {path}.\n Current user may not have permission to create files in the given path \n {ex.Message}");
				}
			}
		}

		private void Log(string message)
		{
			try
			{
				if (_hasPath && !string.IsNullOrEmpty(message))
					System.IO.File.AppendAllText(_options.Value.MxmTaskLogsPath, $"[{DateTime.Now}] \n {message} \n");
			}
			catch { }
		}

		public async Task<object> Execute(object state)
		{
			MxmState mxmState = (MxmState)state ?? new MxmState { Counter = 0 };
			MxmState Exit(string message = default)
			{
				Log(message);
				return new MxmState { Counter = mxmState.Counter + 1 };
			}

			if (mxmState.Counter == 0)
				Log("Background Task - MxmTask started");

			try
			{
				var activeIntegration = _integrationBusiness.GetActiveIntegrationByType(IntegrationType.Stocks);
				if (activeIntegration == null)
				{
					return Exit("Background Task - MxmTask finished with error - No active integration found please check in the table TNHT_INTE if there is any record with the type Stock, with the column INTE_ACTI with the value 1.");
				}

				var config = _integrationBusiness.DeserializeConfig(activeIntegration.Result);
				if (config.HasErrors)
				{
					foreach (var error in config.Errors)
					{
						Log(error.ToString());
					}
					return Exit();
				}

				var operation = await _mxmBusiness.RefreshStock(config.Result.SettingMxm, activeIntegration.Result.Id);
				if (operation.HasErrors)
				{
					foreach (var error in config.Errors)
					{
						Log(error.ToString());
					}
				}
				else
					Log("Background Task - MxmTask was completed without errors");
			}
			catch (Exception ex)
			{
				Log($"Background Task - MxmTask ended early due to exception - {ex.Message}");
			}
			finally
			{
				Log("Background Task - MxmTask finished");
			}

			return Exit();
		}

		public Task<bool> MustRepeat(object state)
		{
			var mxmState = (MxmState)state ?? new MxmState { Counter = 0 };
			if (mxmState.Counter >= mxmState.MaxRetries)
				Log("Background Task - MxmTask reached the maximum number of retries and will close to restart it please restart the Hub");

			return Task.FromResult(mxmState.Counter < mxmState.MaxRetries);
		}
	}
}
