﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DrawerShell
{
    class Program
    {
        static void Main(string[] args)
        {
            //args = new string[] { "HPRT TP805(USB)", "[27;112;0;10;100]" };
            if (args != null && args.Length == 2)
            {
                try
                {
                    string printer = args[0];
                    string escapeCode = args[1];
                    byte[] escapeData = GetEscCode(escapeCode);
                    GCHandle pinnedArray = GCHandle.Alloc(escapeData, GCHandleType.Pinned);
                    IntPtr pointer = pinnedArray.AddrOfPinnedObject();
                    bool result = RawPrinterHelper.SendBytesToPrinter(printer, pointer, escapeData.Length);
                    pinnedArray.Free();
                }
                catch (Exception) { }
            }
        }

        static byte[] GetEscCode(string escapeCode)
        {
            try
            {
                var esc = escapeCode.Trim('[', ']');
                var listEsc = esc.Split(';');
                byte[] ret = new byte[listEsc.Count()];
                for (int i = 0; i < ret.Length; i++)
                    ret[i] = byte.Parse(listEsc.ElementAt(i));
                return ret;
            }
            catch { return new byte[0]; }
        }
    }
}
