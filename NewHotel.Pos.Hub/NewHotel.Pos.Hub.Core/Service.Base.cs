﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel;
using System.Data;
using NewHotel.Core;
using NewHotel.Pos.Hub.Model;
using NewHotel.Contracts;
using System.Text;
using System.Reflection;
using System.Linq;
using System.ServiceModel.Channels;
using System.Collections.Concurrent;
using System.Xml;

namespace NewHotel.Pos.Hub.Core
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    [HubContextBehavior]
    public class BaseService : IBaseService
    {
        #region Members

        // private static readonly IDictionary<Guid, HubSessionData> SessionData = new Dictionary<Guid, HubSessionData>();
        private static readonly ConcurrentDictionary<Guid, HubSessionData> sessionData = new();

		#endregion

		#region Constructor

		static BaseService()
        {
        }

        public BaseService()
        {
            ValidateServiceHealth();
        }


		#endregion

		#region Private Methods

        //private void LogHubContextCacheAndSessionData(HubContextCache current)
        //{
        //    Console.WriteLine($"Token: {current?.Token}");
        //    var formattedSessionData = JsonSerialization.ToJson(sessionData);
        //    Console.WriteLine($"Session data: {formattedSessionData}");
        //}

		private void ValidateServiceHealth()
        {
			var current = HubContextCache.Current;
            //LogHubContextCacheAndSessionData(current);
            if (current != null)
            {
				var validations = current[HubContextCache.NEWHOTEL_HUB_VALIDATIONS] as IEnumerable<Validation>;

				if (validations?.FirstOrDefault() is Validation fault)
				{
					throw new HubException(fault.Code, fault.Message);
				}
			}
		}

		private static HubSessionData UpdateLastAccessTime(Guid token, DateTime currentDateTime)
        {
            if (sessionData.TryGetValue(token, out var hubSessionData))
                hubSessionData.LastAccessTime = currentDateTime;

            return hubSessionData;
        }

        #endregion

        #region Protected Methods

        public static IDatabaseManager GetManager()
        {
            var appSettings = ConfigurationManager.AppSettings;
            var serviceName = appSettings.Get("OracleServiceName");
            var schemaName = appSettings.Get("OracleSchemaName");
            return NewHotelAppContext.GetManager(schemaName, "CLOUD", serviceName, string.Empty);
        }

        #endregion

        #region Public Methods

        public static string GetSessionDataAudit()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine($"Sessions: {sessionData.Count}");

            return sb.ToString();
        }

        // public static bool enterPlease = false;

        public static HubSessionData? GetHubSessionData(Guid token)
        {
            //var tokens = new HashSet<Guid>();
			var currentDateTime = DateTime.Now.ToUniversalTime();
			var timeout = TimeSpan.FromMinutes(15);

            //if (enterPlease)
            //{
            //    timeout = TimeSpan.FromSeconds(10);
            //}

            var tokens = new HashSet<Guid>(sessionData.ToArray().Where(x => currentDateTime - x.Value.LastAccessTime > timeout).Select(x => x.Key));
            foreach (var oldToken in tokens)
			{
				sessionData.TryRemove(oldToken, out _);
			}

			var hubSessionData = UpdateLastAccessTime(token, currentDateTime);
			if (hubSessionData != null)
				return hubSessionData;

			//lock (SessionData)
   //         {
   //             foreach (var item in SessionData)
   //             {
   //                 if (currentDateTime - item.Value.LastAccessTime <= timeout)
   //                     continue;

   //                 tokens.Add(item.Key);
   //             }

   //             if (tokens.Count > 0)
   //             {
   //                 foreach (var tk in tokens)
   //                     SessionData.Remove(tk);
   //             }

   //             var hubSessionData = UpdateLastAccessTime(token, currentDateTime);
   //             if (hubSessionData != null)
   //                 return hubSessionData;
   //         }

            return null;
        }

        protected static void UpdateWorkDateSessionData(Guid standId, DateTime workDate, DateTime currentDateTime)
        {
            sessionData.Values.Where(x => x.StandId == standId).ToList().ForEach(x => (x.WorkDate, x.LastAccessTime) = (workDate, currentDateTime));
            
            //lock (SessionData)
            //{
            //    foreach (var data in SessionData.Values)
            //    {
            //        if (data.StandId != standId) continue;
            //        data.WorkDate = workDate;
            //        data.LastAccessTime = currentDateTime;
            //    }
            //}
        }

        public static DateTime GetLastSyncMark()
        {
            try
            {
                using (var manager = GetManager())
                {
                    manager.Open();
                    try
                    {
                        using (var command = manager.CreateCommand("Service.Base.SyncTimestamp"))
                        {
                            command.CommandText = "select hote_sync from tnht_hote";
                            var data = manager.ExecuteScalar(command);
                            if (data is DateTime time)
                                return time;
                        }
                    }
                    finally
                    {
                        manager.Close();
                    }
                }
            }
            catch
            {
                // ignored
            }

            return DateTime.MinValue;
        }

        public string Echo(string echo)
        {
            return echo;
        }

        public void Ping(string token)
        {
			UpdateLastAccessTime(token.AsGuid(), DateTime.Now.ToUniversalTime());

			//lock (SessionData)
   //         {
   //             UpdateLastAccessTime(token.AsGuid(), DateTime.Now.ToUniversalTime());
   //         }
        }

        public static Guid CreateSessionToken(Model.UserRecord user, Guid installationId)
        {
			var token = DateTime.Now.NewGuid();

			sessionData.TryAdd(token, new HubSessionData(user.Id, user.UserLogin, user.UserPassword, installationId)
			{
				LastAccessTime = DateTime.Now.ToUniversalTime(),
				LastSyncMark = GetLastSyncMark(),
			});

			return token;
		}


		public Guid CreateSessionToken(string installationId, string userId)
        {
			var user = GetUserById(userId.AsGuid());
			if (user.Id == Guid.Empty)
				return Guid.Empty;

            return CreateSessionToken(user, installationId.AsGuid());

			/*
			lock (SessionData)
            {
                // Check if the user has recently created a token
                // foreach (var key in SessionData.Keys)
                // {
                //     SessionData.TryGetValue(key, out var session);
                //     if (session == null || session.InstallationId != installationId.AsGuid() || session.UserId != userId.AsGuid())
                //         continue;
                //
                //     session.LastAccessTime = DateTime.Now.ToUniversalTime();
                //     return key;
                // }
                // If it doesn't have any token, then create a new one
                
                // Note:
                // Every time a user request a token, a new one must be created
                // This action is required because if the same user is already authenticated on another device,
                // don't lose that session
                
                SessionData.Add(token, new HubSessionData(user.Id, user.UserLogin, user.UserPassword, installationId.AsGuid())
                {
                    LastAccessTime = DateTime.Now.ToUniversalTime(),
                    LastSyncMark = GetLastSyncMark(),
                });

            }
			return token;
            */
		}

        public string SetTokenContextData(string standId, string cashierId, string schemaId, string language)
        {
            return SetTokenContext(HubContextCache.Current.Token, standId, cashierId, schemaId, language);
        }
		public static string SetTokenContext(Guid sessionId, string standId, string cashierId, string schemaId, string language)
        {
            var data = GetHubSessionData(sessionId);//(HubContextCache.Current.Token);
            
            if (data is not null)
            {
                data.StandId = standId.AsGuid();
                data.CashierId = cashierId.AsGuid();
                data.TaxSchemaId = schemaId.AsGuid();
                data.WorkDate = GetWorkDateByStand(data.StandId);
                data.Data.LanguageId = language.AsInteger();
                data.Country = GetCountryByHotel(data.InstallationId);
                data.DocumentSign = GetDocumentSignByHotel(data.InstallationId);
                data.DatabaseVersion = GetDatabaseVersion();
            }
            else 
                throw new HubException(999, "Session not found");
            
            return data.WorkDate.ToString("yyyyMMdd");
        }

        public void DestroyAllSessionTokensAndCachedData()
        {
            sessionData.Clear();
            HubContextCache.Destroy();
        }

        public void InvalidateAllSessionDataAndResetIt()
        {
            var currentKeysCount = sessionData.Keys.Count;
            DestroyAllSessionTokensAndCachedData();

            for (var i = 0; i < currentKeysCount; i++)
            {
                var token = DateTime.Now.ToUniversalTime().NewGuid();
                HubContextCache.Create(token);
            }
        }

        private static Model.UserRecord GetUserById(Guid userId)
        {
            var userRecord = new Model.UserRecord();

            using var manager = GetManager();
            manager.Open();
            try
            {
                using var command = manager.CreateCommand("Service.Base.Login");
                command.CommandText = "select util_pk, util_desc, util_login, util_code, util_pass from tnht_util where util_pk = :util_pk";
                manager.CreateParameter(command, "util_pk", userId);

                using var reader = manager.ExecuteReader(command, CommandBehavior.SingleRow);
                try
                {
                    if (reader.Read())
                    {
                        userRecord.Id = reader.GetGuid(0);
                        userRecord.Description = reader.GetString(1);
                        userRecord.UserLogin = reader.GetString(2);
                        userRecord.UserCode = reader.GetString(3);
                        userRecord.UserPassword = reader.GetString(4);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            finally
            {
                manager.Close();
            }

            return userRecord;
        }

        private static string GetCountryByHotel(Guid installationId)
        {
            using (var manager = GetManager())
            {
                manager.Open();
                try
                {
                    using (var command = manager.CreateCommand("Service.Base.Country"))
                    {
                        command.CommandText = "select naci_pk from tnht_hote where hote_pk = :hote_pk";
                        manager.CreateParameter(command, "hote_pk", installationId);

                        using (var reader = manager.ExecuteReader(command, CommandBehavior.SingleRow))
                        {
                            try
                            {
                                if (reader.Read())
                                    return reader.GetString(0);
                            }
                            finally
                            {
                                reader.Close();
                            }
                        }
                    }
                }
                finally
                {
                    manager.Close();
                }
            }

            return null;
        }

        private static long GetDocumentSignByHotel(Guid installationId)
        {
            using (var manager = GetManager())
            {
                manager.Open();
                try
                {
                    using (var command = manager.CreateCommand("Service.Base.Country"))
                    {
                        command.CommandText = "select sign_tipo from tcfg_npos where hote_pk = :hote_pk";
                        manager.CreateParameter(command, "hote_pk", installationId);

                        using (var reader = manager.ExecuteReader(command, CommandBehavior.SingleRow))
                        {
                            try
                            {
                                if (reader.Read())
                                {
                                    if (!reader.IsDBNull(0))
                                        return reader.GetInt64(0);
                                }
                            }
                            finally
                            {
                                reader.Close();
                            }
                        }
                    }
                }
                finally
                {
                    manager.Close();
                }
            }

            return 4;
        }

        private static DateTime GetWorkDateByStand(Guid standId)
        {
            using (var manager = GetManager())
            {
                manager.Open();
                try
                {
                    using (var command = manager.CreateCommand("Service.Base.Country"))
                    {
                        command.CommandText = "select ipos_fctr from tnht_ipos where ipos_pk = :ipos_pk";
                        manager.CreateParameter(command, "ipos_pk", standId);

                        using (var reader = manager.ExecuteReader(command, CommandBehavior.SingleRow))
                        {
                            try
                            {
                                if (reader.Read())
                                    return reader.GetDateTime(0);
                            }
                            finally
                            {
                                reader.Close();
                            }
                        }
                    }
                }
                finally
                {
                    manager.Close();
                }
            }

            return DateTime.MinValue;
        }

        private static string GetDatabaseVersion()
        {
            using (var manager = GetManager())
            {
                manager.Open();
                try
                {
                    using (var command = manager.CreateCommand("Service.Base.Version"))
                    {
                        command.CommandText = "select date_vers from tcfg_gene";

                        using (var reader = manager.ExecuteReader(command, CommandBehavior.SingleRow))
                        {
                            try
                            {
                                if (reader.Read() && !reader.IsDBNull(0))
                                    return reader.GetString(0);
                            }
                            finally
                            {
                                reader.Close();
                            }
                        }
                    }
                }
                finally
                {
                    manager.Close();
                }
            }

            return string.Empty;
        }

        public Guid? Login(string username, string password)
        {
            using (var manager = GetManager())
            {
                manager.Open();
                try
                {
                    using (var command = manager.CreateCommand("Service.Base.Login"))
                    {
                        command.CommandText = "select util_pk, util_pass from tnht_util where upper(util_login) = :util_login";
                        manager.CreateParameter(command, "util_login", username.ToUpperInvariant());

                        using (var reader = manager.ExecuteReader(command, CommandBehavior.SingleRow))
                        {
                            try
                            {
                                if (reader.Read())
                                {
                                    if (reader.GetString(1) == password)
                                        return reader.GetGuid(0);
                                }
                            }
                            finally
                            {
                                reader.Close();
                            }
                        }
                    }
                }
                finally
                {
                    manager.Close();
                }
            }

            return null;
        }

        protected static bool CheckUserWritePermission(Permissions permission)
        {
            var securityCode = GetUserSecurityCode(permission);
            return securityCode == SecurityCode.FullAccess || securityCode == SecurityCode.ReadWrite;
        }

        private static SecurityCode GetUserSecurityCode(Permissions permission)
        {
            var sessionData = GetHubSessionData(HubContextCache.Current.Token);
            var user = GetUserById(sessionData.UserId);

            if (user.Permissions != null && user.Permissions.TryGetValue(permission, out var tuple))
                return tuple.Item1;

            return SecurityCode.FullAccess;
        }

        public string GetUserPermissionTranslation(Permissions permission)
        {
            var sessionData = GetHubSessionData(HubContextCache.Current.Token);
            var user = GetUserById(sessionData.UserId);

            if (user.Permissions != null && user.Permissions.TryGetValue(permission, out var tuple))
                return tuple.Item2;

            return permission.ToString();
        }

        #endregion
    }
}