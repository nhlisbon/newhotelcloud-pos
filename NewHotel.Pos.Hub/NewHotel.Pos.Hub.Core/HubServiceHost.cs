﻿using System;
using System.Net;
using System.ServiceModel;
using System.Diagnostics;
using System.ServiceModel.Activation;
using NewHotel.Pos.IoC;

namespace NewHotel.Pos.Hub.Core
{
    public class HubServiceHost : HubHost
    {
        #region Private Methods

        private void OnFaulted(object sender, EventArgs args)
        {
            Trace.TraceWarning("Service {0} faulted", Host, Host.Description.ServiceType.FullName);
        }

        private void OnOpening(object sender, EventArgs args)
        {
            Trace.TraceInformation("Service {0} opening ...", Host.Description.ServiceType.FullName);
        }

        private void OnOpened(object sender, EventArgs args)
        {
            Trace.TraceInformation("Service {0} opened", Host.Description.ServiceType.FullName);
        }

        private void OnClosing(object sender, EventArgs args)
        {
            Trace.TraceInformation("Service {0} closing ...", Host.Description.ServiceType.FullName);
        }

        private void OnClosed(object sender, EventArgs args)
        {
            Trace.TraceInformation("Service {0} closed", Host.Description.ServiceType.FullName);
        }

        #endregion
        #region Constructor

        static HubServiceHost()
        {
            ServicePointManager.ServerCertificateValidationCallback = (s, cer, ch, e) => { return true; };
        }

        public HubServiceHost(string waitEventName, Type type)
            // : base(waitEventName, new ServiceHost(type))
            : base(waitEventName, CwFactory.Instance.CreateServiceHost(type))
        {
			Host.Faulted += OnFaulted;
            Host.Opening += OnOpening;
            Host.Opened += OnOpened;
            Host.Closing += OnClosing;
            Host.Closed += OnClosed;  
        }

        #endregion
        #region Protected Methods

        public override int Run()
        {
            if (Host != null)
            {
                Host.Open();
                Trace.TraceInformation("Service {0} running ...", Host.Description.ServiceType.FullName);
                Trace.TraceInformation(ToString());
                WaitForEvent();
                Host.Close();
            }

            return 0;
        }

        #endregion
    }
}