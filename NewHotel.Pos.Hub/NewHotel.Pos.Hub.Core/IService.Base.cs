﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace NewHotel.Pos.Hub.Core
{
    [ServiceContract]
    public interface IBaseService
    {
        #region Base

        [OperationContract]
        [WebInvoke(UriTemplate = "/Echo/{echo}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string Echo(string echo);

        [OperationContract]
        [WebInvoke(UriTemplate = "/Ping/{token}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void Ping(string token);

        [OperationContract]
        [WebInvoke(UriTemplate = "/CreateSessionToken/{installationId}&{userId}", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Guid CreateSessionToken(string installationId, string userId);
        [OperationContract]
        [WebInvoke(UriTemplate = "/SetTokenContextData/{standId}&{cashierId}&{schemaId}&{language}", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string SetTokenContextData(string standId, string cashierId, string schemaId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/Login/{username}&{password}", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Guid? Login(string username, string password);

        #endregion
    }
}