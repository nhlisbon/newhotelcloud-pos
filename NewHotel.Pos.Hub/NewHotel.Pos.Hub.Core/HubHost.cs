﻿using System;
using System.Text;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Threading;

namespace NewHotel.Pos.Hub.Core
{
    public abstract class HubHost : IHubHost, IDisposable
    {
        #region Members

        protected readonly string WaitEventName;
        private bool _disposed = false;

        #endregion
        #region Constructor

        static HubHost()
        {
            ServicePointManager.ServerCertificateValidationCallback = (s, cer, ch, e) => { return true; };
        }

        public HubHost(string waitEventName, ServiceHostBase host)
        {
            if (string.IsNullOrEmpty(waitEventName))
                throw new ArgumentNullException("waitEventName");

            WaitEventName = waitEventName;
            Host = host;
        }

        #endregion
        #region Properties
          
        public ServiceHostBase Host { get; private set; }

        #endregion
        #region Protected Methods

        public abstract int Run();

        protected bool WaitForEvent()
        {
            using (var handle = new EventWaitHandle(false, EventResetMode.ManualReset, WaitEventName))
            {
                return handle.WaitOne();
            }
        }

        #endregion
        #region IDisposable Members

        public void Dispose()
        {
            if (!_disposed)
            {
                try
                {
                    if (Host != null)
                        ((IDisposable)Host).Dispose();
                }
                finally
                {
                    _disposed = true;
                }
            }
        }

        #endregion
        #region Public Methods

        public override string ToString()
        {
            var sb = new StringBuilder();

            if (Host != null)
            {
                foreach (var address in Host.Description.Endpoints.Select(s => s.Address.Uri.AbsoluteUri))
                    sb.AppendLine(address);
            }

            return sb.ToString();
        }

        #endregion
    }
}
