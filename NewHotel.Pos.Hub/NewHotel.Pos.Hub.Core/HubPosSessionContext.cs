﻿using NewHotel.Pos.Hub.Model.Session;
using System;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Core
{
	public interface IPosSessionFactory
	{
		IPosSessionContext Current { get; }

		IPosSessionContext Create(Guid token, HubSessionData sessionData, bool isInternal = false);
		IPosSessionContext Create(IPosSessionContext context);
	}

	public sealed class HubPosSessionFactory : IPosSessionFactory
	{
		IPosSessionContext _current;
		public IPosSessionContext Current => _current ?? Create(Guid.Empty, null, false);

		public IPosSessionContext Create(Guid token, HubSessionData sessionData, bool isInternal)
		{
			return _current = new HubPosSessionContext(token, sessionData, isInternal);
		}

		public IPosSessionContext Create(IPosSessionContext context)
		{
			return _current = context;
		}
	}

	public sealed class HubPosSessionContext : IPosSessionContext
	{
        public HubPosSessionContext(Guid token, HubSessionData hubSessionData, bool isInternal)
		{
			SessionId = token;
			UserName = hubSessionData?.UserName ?? string.Empty;
			UserLogin = hubSessionData?.UserLogin;
			UserId = hubSessionData?.UserId ?? Guid.Empty;
			ApplicationId = hubSessionData?.Data?.ApplicationId ?? 107L;
			IsInternal = isInternal;
			InstallationId = hubSessionData?.InstallationId ?? Guid.Empty;
			StandId = hubSessionData?.StandId ?? Guid.Empty;
			CashierId = hubSessionData?.CashierId ?? Guid.Empty;
			TaxSchemaId = hubSessionData?.TaxSchemaId ?? Guid.Empty;
			SignatureMode = (DocumentSign)(hubSessionData?.DocumentSign ?? (long)DocumentSign.None);
			WorkDate = hubSessionData?.WorkDate ?? DateTime.MinValue;
			LanguageId = hubSessionData?.Data?.LanguageId ?? 1033L;
			Country = hubSessionData?.Data?.Country ?? string.Empty;
		}

		public Guid SessionId { get; }
		public long ApplicationId { get; }
		public string UserName { get; }
		public string UserLogin { get; }
		public Guid UserId { get; }
		public bool IsInternal { get; }
		public Guid InstallationId { get; }
		public Guid StandId { get; }
		public Guid CashierId { get; }
		public Guid TaxSchemaId { get; }
		public NewHotel.Contracts.DocumentSign SignatureMode { get; }
		public DateTime WorkDate { get; }
		public long LanguageId { get; }
		public string Country { get; }
	}
}