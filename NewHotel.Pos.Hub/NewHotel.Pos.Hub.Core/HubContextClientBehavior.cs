﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace NewHotel.Pos.Hub.Core
{
    public class HubContextClientBehavior : IClientMessageInspector, IEndpointBehavior
    {
        #region Constants

        public const string NEWHOTEL_HUB_SESSION_TOKEN = "NEWHOTELHUBSESSIONTOKEN";
        public const string NEWHOTEL_HUB_CLIENT_VERSION = "NEWHOTELHUBCLIENTVERSION";
        public const string NEWHOTEL_HUB_CLIENT_WORKDATE = "NEWHOTELHUBCLIENTWORKDATE";
        public const string NEWHOTEL_HUB_INSTALL_TOKEN = "53A52B4A5DDA07408C169AB63B5C1413";

        #endregion
        #region Members

        private static string _token;

        #endregion
        #region Protected Methods

        protected static T GetDataFromHeader<T>(Message reply, string name)
        {
            int index = reply.Headers.FindHeader(name, string.Empty);
            if (index >= 0)
                return reply.Headers.GetHeader<T>(name, string.Empty);

            return default(T);
        }

        protected static void CreateHeaderData(Message request, string name, object data)
        {
            request.Headers.Add(MessageHeader.CreateHeader(name, string.Empty, data));
        }

        #endregion
        #region IClientMessageInspector Members

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            _token = GetDataFromHeader<string>(reply, NEWHOTEL_HUB_SESSION_TOKEN);
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            CreateHeaderData(request, NEWHOTEL_HUB_SESSION_TOKEN, _token);
            return null;
        }

        #endregion
        #region IEndpointBehavior Members

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters) { }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(this);
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher) { }

        public void Validate(ServiceEndpoint endpoint) { }

        #endregion
    }
}