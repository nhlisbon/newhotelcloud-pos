﻿using System.ServiceModel;

namespace NewHotel.Pos.Hub.Core
{
    public class HubException : FaultException
    {
        public HubException(int code, string message, params object[] args)
            : base(new FaultReason(string.Format(message, args)), new FaultCode(code.ToString()))
        {
        }
    }
}
