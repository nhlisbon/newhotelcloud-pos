﻿using System;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Pos.Hub.Core
{
    public sealed class HubSessionData : IRequestSessionData
    {
        #region Private Members

        private Guid _standId;
        private Guid _cashierId;
        private Guid _schemaId;
        private long _documenSign;
        private DateTime _workDate;
        public string _userLogin;
        public string _userPassword;

        #endregion

        public DateTime LastAccessTime;
        public DateTime LastSyncMark;

        NewHotelContextData _data;
        public HubSessionData(Guid userId, string userLogin, string userPassword, Guid installationId)
        {            
            _data = new NewHotelContextData();
            _data.ApplicationId = 2;
            _data.LanguageId = 1033;
            InstallationId = installationId;
            UserId = userId;
            _userLogin = userLogin;
            _userPassword = userPassword;
        }

        public Guid UserId
        {
            get { return Data.UserId; }
            set { Data.UserId = value; }
        }

        public string UserLogin
        {
            get { return _userLogin; }
        }

        public string UserPassword
        {
            get { return _userPassword; }
        }

        public Guid InstallationId
        {
            get { return Data.InstallationId; }
            set { Data.InstallationId = value; }
        }

        public string Country
        {
            get { return Data.Country; }
            set { Data.Country = value; }
        }

        public NewHotelContextData Data
        {
            get
            {
                if (_data == null)
                    _data = new NewHotelContextData();

                return _data;
            }
        }

        public string Password
        {
            get { return _userPassword; }
        }

        public string UserName
        {
            get { return _userLogin; }
        }

        public Guid StandId
        {
            get { return _standId; }
            set { _standId = value; }
        }
        public Guid CashierId
        {
            get { return _cashierId; }
            set { _cashierId = value; }
        }

        public Guid TaxSchemaId
        {
            get { return _schemaId; }
            set { _schemaId = value; }
        }

        public long DocumentSign
        {
            get { return _documenSign; }
            set { _documenSign = value; }
        }

        public DateTime WorkDate
        {
            get { return _workDate; }
            set { _workDate = value; }
        }
        
        public string Version { get; set; }

        public string DatabaseVersion { get; set; }

        public void SetRequestSessionData(ISessionData data)
        {
            if (Data != null)
            {
                Data.SysDate = data.SysDate;
                Data.WorkDate = data.WorkDate;
                Data.WorkShift = data.WorkShift;
                Data.FirstOpenedDate = data.FirstOpenedDate;
                Data.LastOpenedYear = data.LastOpenedYear;
            }
        }

        public ISessionData GetRequestSessionData()
        {
            if (Data != null)
            {
                var data = new SessionData()
                {
                    ApplicationId = Data.ApplicationId,
                    ModuleName = Data.ModuleName,
                    InstallationId = Data.InstallationId,
                    UserId = Data.UserId,
                    LanguageId = Data.LanguageId,
                    SysDate = Data.SysDate,
                    WorkDate = Data.WorkDate,
                    WorkShift = Data.WorkShift,
                    FirstOpenedDate = Data.FirstOpenedDate,
                    LastOpenedYear = Data.LastOpenedYear
                };

                data.Version = Version;
                return data;
            }

            return null;
        }
    }
}
