﻿using System;
using System.Globalization;

namespace NewHotel.Pos.Hub.Core
{
    public static class StringExtensions
    {
        public static NumberFormatInfo NumberInfo = new NumberFormatInfo() { NumberDecimalSeparator = "." };

        #region From String Extensions

        public static Guid AsGuid(this string value) => new Guid(value);

        public static Guid? AsGuidX(this string value) => string.IsNullOrEmpty(value) ? new Guid?() :  new Guid(value);

        public static int AsInteger(this string value) => int.Parse(value);

        public static short AsShort(this string value) => short.Parse(value);

        public static long AsLong(this string value) => long.Parse(value);

        public static long? AsLongX(this string value) => string.IsNullOrEmpty(value) ? new Nullable<long>() : long.Parse(value);

        public static decimal AsDecimal(this string value) => decimal.Parse(value, NumberInfo);

        public static decimal? AsDecimalX(this string value) => string.IsNullOrEmpty(value) ? new decimal?() : decimal.Parse(value, NumberInfo);

        public static bool AsBoolean(this string value)
        {
            value = value.ToLower();
            if (value == "1") return true;
            if (value == "0") return false;
            if (value == "true") return true;
            if (value == "false") return false;

            return false;
        }

        #endregion

        #region To String Extensions

        public static string AsString(this int integer) => integer.ToString();

        public static string AsString(this Guid guid) => guid.ToString();

        public static string AsString(this Guid? guid) => guid.HasValue ? guid.ToString() : string.Empty;

        public static string AsOracleRaw(this Guid guid)
        {
            var raw16  = guid.ToByteArray();
            return BitConverter.ToString(raw16).Replace("-", "");
        }

        #endregion
    }
}