﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace NewHotel.Pos.Hub.Core.Logs
{
	public class HubLogTraceListener : XmlWriterTraceListener
	{
		const string MAXFILESIZE = "maxFileSizeKB";
		const string ALLFILESIZE = "allFileSizeKB";
		const string DAYSTOKEEP = "daysToKeep";

		const long DEFAULT_MAXFILESIZE = 1024 * 1024 * 100;
		const long DEFAULT_ALLFILESIZE = DEFAULT_MAXFILESIZE * 100;
		const int DEFAULT_DAYSTOKEEP = 30;

		RecyclableFileStreamConfig GetConfig()
		{
			long maxFileSize;
			long allFileSize;
			int daysToKeep;
			string attMaxFileSize = this.Attributes[MAXFILESIZE];
			if (long.TryParse(attMaxFileSize, out maxFileSize))
				maxFileSize *= 1024;
			else
				maxFileSize = DEFAULT_MAXFILESIZE;

			string attAllFileSize = this.Attributes[ALLFILESIZE];
			if (long.TryParse(attAllFileSize, out allFileSize))
				allFileSize *= 1024;
			else
				allFileSize = DEFAULT_ALLFILESIZE;

			string attDaysToKeep = this.Attributes[DAYSTOKEEP];
			if (!int.TryParse(attDaysToKeep, out daysToKeep))
				daysToKeep = DEFAULT_DAYSTOKEEP;

			return new RecyclableFileStreamConfig { AllFileSize = allFileSize, MaxFileSize = maxFileSize, DaysToKeep = daysToKeep };
		}

		public HubLogTraceListener(string filename) : base(filename)
		{
			Writer = new StreamWriter(new RecyclableFileStream(filename, () => GetConfig()));
		}
		public HubLogTraceListener() : this("HubLog.svclog")
		{
		}

		protected override string[] GetSupportedAttributes()
		{
			return new string[] { MAXFILESIZE, ALLFILESIZE, DAYSTOKEEP };
		}
	}

	public class RecyclableFileStreamConfig
	{
		public long MaxFileSize { get; set; }
		public long AllFileSize { get; set; }
		public int DaysToKeep { get; set; }
	}

	public class RecyclableFileStream : Stream
	{
		const int FILENUMBERSIZE = 12;

		readonly Func<RecyclableFileStreamConfig> getConfig;
		bool hasConfig;

		readonly string pathPattern;
		long maxFileSize;
		long allFileSize;
		int daysToKeep;
		readonly string searchPath;
		readonly string searchName;
		readonly string searchExtension;
		readonly string searchPattern;

		readonly object synchronizer;

		int maxFileNumber;
		FileStream current;
		string fileName;

		public RecyclableFileStream(string pathPattern, Func<RecyclableFileStreamConfig> getConfig)
		{
			this.getConfig = getConfig;
			hasConfig = false;

			this.pathPattern = pathPattern;
			synchronizer = new object[0];
			maxFileNumber = -1;

			searchExtension = Path.GetExtension(pathPattern);
			searchPath = Path.GetDirectoryName(pathPattern);
			searchName = Path.GetFileName(pathPattern);
			searchName = searchName.Remove(searchName.Length - searchExtension.Length) + "_";
			searchPattern = searchName + "*" + searchExtension;
		}

		private bool ValidateLogFile(string localFileName, out int fileNumber)
		{
			string name = Path.GetFileName(localFileName);
			string extension = Path.GetExtension(localFileName);
			if (string.Equals(extension, searchExtension, System.StringComparison.OrdinalIgnoreCase) && name.StartsWith(searchName, System.StringComparison.OrdinalIgnoreCase))
			{
				string stNumber = name.Remove(name.Length - searchExtension.Length).Substring(searchName.Length);
				return int.TryParse(stNumber, out fileNumber);
			}

			fileNumber = 0;
			return false;
		}

		private Stream EnsureStream()
		{
			var myStream = current;
			if (myStream != null)
				return myStream;

			lock (synchronizer)
			{
				if (current == null)
				{
					if (!hasConfig)
					{
						var config = getConfig();
						maxFileSize = config.MaxFileSize;
						allFileSize = config.AllFileSize;
						daysToKeep = config.DaysToKeep;
						hasConfig = true;
					}

					int retries = 20;

					while (retries > 0)
					{
						retries--;
						int fileNumber = maxFileNumber;
						if (fileNumber < 0)
						{
							SortedList<int, FileInfo> filesByNumber = new();
							DateTime oldest = DateTime.UtcNow.Date.AddDays(-daysToKeep);
							long accumSize = 0;
							foreach (string name in Directory.EnumerateFiles(searchPath, searchPattern))
							{
								if (!ValidateLogFile(name, out int thisFileNumber))
									continue;
								if (fileNumber < thisFileNumber)
									fileNumber = thisFileNumber;
								FileInfo fileInfo = new FileInfo(name);
								filesByNumber.Add(thisFileNumber, fileInfo);
								accumSize += fileInfo.Length;
							}

							while (accumSize > allFileSize && filesByNumber.Any())
							{
								var firstOne = filesByNumber.First();
								var firstLength = firstOne.Value.Length;
								try
								{
									firstOne.Value.Delete();
									accumSize -= firstLength;
								}
								catch { }
								if (!filesByNumber.Remove(firstOne.Key))
									break;
							}

							foreach (var fileInfo in filesByNumber)
							{
								if (fileInfo.Value.CreationTimeUtc < oldest)
								{
									try
									{
										fileInfo.Value.Delete();
									}
									catch { }
								}
							}
						}
						fileNumber++;
						string fname = Path.Combine(searchPath, searchName + fileNumber.ToString("00000000000000000000") + searchExtension);
						if (File.Exists(fname))
							continue;
						try
						{
							FileStream stream = new FileStream(fname, FileMode.Append, FileAccess.Write);
							current = stream;
							fileName = fname;
							break;
						}
						catch (IOException)
						{
							continue;
						}
						catch
						{
							break;
						}
					}
				}
			}

			return current;
		}

		private void VerifySize()
		{
			var myStream = current;
			if ((myStream?.Length ?? 0) >= maxFileSize)
			{
				lock (synchronizer)
				{
					if (current != null)
					{
						current = null;
						myStream.Flush();
						myStream.Dispose();
					}
				}
			}
		}

		public string CurrentFileName => fileName;

		public override bool CanRead => false;

		public override bool CanSeek => false;

		public override bool CanWrite => true;

		public override long Length => EnsureStream()?.Length ?? 0;

		public override long Position { get => EnsureStream()?.Position ?? 0; set => throw new System.NotImplementedException(); }

		public override void Flush()
		{
			EnsureStream()?.Flush();
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			throw new System.NotImplementedException();
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new System.NotImplementedException();
		}

		public override void SetLength(long value)
		{
			throw new System.NotImplementedException();
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			EnsureStream()?.Write(buffer, offset, count);
			VerifySize();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				current?.Dispose();
				current = null;
			}
			base.Dispose(disposing);
		}
	}
}
