﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Core.Logs
{
	public interface IHubLogsFactory
	{
		IHubLogs CreateHubLogs(string logName, string activityName = default, Guid? activityId = default);
	}

	public class HubLogsFactory : IHubLogsFactory
	{
		readonly ConcurrentDictionary<string, TraceSource> sources = new ConcurrentDictionary<string, TraceSource>();
		readonly ConcurrentDictionary<string, Guid> namedActivities = new ConcurrentDictionary<string, Guid>();

		TraceSource GetSource(string sourceName)
		{
			var source = sources.GetOrAdd(sourceName, sn => new TraceSource(sn));
			if (source.HasNoListener())
			{
				lock (source)
				{
					if (source.HasNoListener())
					{
					}
				}
			}
			return source;
		}

		public IHubLogs CreateHubLogs(string logName, string activityName = default, Guid? activityId = default)
		{
			try
			{
				var source = GetSource(logName);
				if (activityId.HasValue)
				{
					return new HubLogs(activityName ?? logName, source, activityId.Value, true);
				}

				if (!string.IsNullOrEmpty(activityName))
				{
					var namedId = namedActivities.GetOrAdd(activityName, an =>
					{
						return Guid.NewGuid();
					});

					return new HubLogs(activityName, source, namedId, false);
				}

				return new HubLogs(logName, source, Guid.NewGuid(), false);

			}
			catch (Exception)
			{
				return new NullHubLogs();
			}
		}

		public static readonly IHubLogsFactory DefaultFactory = new HubLogsFactory();
	}

	public interface IHubLogs : IDisposable
	{
		Guid Id { get; }
		void Error(string message, string error = null);
		void Information(string message);
	}

	public class NullHubLogs : IHubLogs
	{
		public Guid Id => Guid.Empty;

		public void Dispose()
		{
		}

		public void Error(string message, string error)
		{
		}

		public void Information(string message)
		{
		}
	}

	public class HubLogs : IHubLogs
	{
		readonly TraceSource source;
		readonly string logName;
		readonly Guid activityId;
		bool started;

		void RunOnTraceContext(Action action)
		{
			try
			{
				var previousActivityId = Trace.CorrelationManager.ActivityId;
				Trace.CorrelationManager.ActivityId = activityId;

				if (!started)
				{
					source.TraceEvent(TraceEventType.Start, 0, logName);
					started = true;
				}
				action();
				Trace.CorrelationManager.ActivityId = previousActivityId;
			}
			catch { }
		}

		public HubLogs(string logName, TraceSource source, Guid activityId, bool started)
		{
			this.source = source;
			this.logName = logName;
			this.activityId = activityId;
			this.started = started;
		}

		public Guid Id => activityId;

		public void Information(string message)
		{
			RunOnTraceContext(() => source.TraceInformation(message));
		}

		public void Error(string message, string error)
		{
			RunOnTraceContext(() => source.TraceEvent(TraceEventType.Error, 0, new StringBuilder().AppendLine(message).AppendLine(error ?? string.Empty).ToString()));
		}

		public void Dispose()
		{
			if (started)
			{
				RunOnTraceContext(() =>
				{
					source.TraceEvent(TraceEventType.Stop, 0, logName);
				});
				started = false;
			}
		}
	}

	public static class HubLogsExtensions
	{
		public static bool HasNoListener(this TraceSource source)
		{
			return source.Listeners.Count == 0 || (source.Listeners.Count == 1 && source.Listeners[0] is System.Diagnostics.DefaultTraceListener);
		}

		public static void Error(this IHubLogs hubLogs, Exception exception)
		{
			hubLogs.Error(exception.Message, exception.ToString());
		}

		public static void Error(this IHubLogsFactory factory, Exception exception, string activity = "General")
		{
			try
			{
				using var hub = factory.NewBusinessLog(activity);
				hub.Error(exception);
			}
			catch
			{
			}
		}

		public static IHubLogs NewLog(this IHubLogsFactory factory, string source, string activityName)
			=> factory?.CreateHubLogs(source, activityName) ?? new NullHubLogs();

		public static IHubLogs NewBusinessLog(this IHubLogsFactory factory, string activityName)
			=> NewLog(factory, LogsDefinitions.BusinessSource, activityName);
	}

	public static class LogsDefinitions
	{
		public const string BusinessSource = "Business";
	}
}
