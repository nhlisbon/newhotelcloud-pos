﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace NewHotel.Pos.Hub.Core
{
    public class JsonTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            try
            {
                if (value != null && value is string)
                {
                    var serializer = new DataContractJsonSerializer(value.GetType(),
                        null, int.MaxValue, false, null, true);

                    using (var stream = new MemoryStream(Encoding.Unicode.GetBytes(value.ToString())))
                    {
                        return serializer.ReadObject(stream);
                    }
                }

                return base.ConvertFrom(context, culture, value);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error deserializing object from json.", ex);
            }
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            try
            {
                if (value != null)
                {
                    var serializer = new DataContractJsonSerializer(destinationType,
                        null, int.MaxValue, false, null, true);

                    using (var stream = new MemoryStream())
                    {
                        serializer.WriteObject(stream, value);
                        return Encoding.Unicode.GetString(stream.ToArray());
                    }
                }

                return ConvertTo(context, culture, value, destinationType);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error serializing object to json.", ex);
            }
        }
    }
}
