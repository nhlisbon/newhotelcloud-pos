﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;

namespace NewHotel.Pos.Hub.Core
{
	public sealed class HubContextCache : IExtension<OperationContext>
    {
        #region Members

        public const string NEWHOTEL_HUB_INSTALLATIONID = "NEWHOTELHUBINSTALLATIONID";
        public const string NEWHOTEL_HUB_USERID = "NEWHOTELHUBUSERID";
        public const string NEWHOTEL_HUB_USERNAME = "NEWHOTELHUBNAME";
        public const string NEWHOTEL_HUB_USERPASSWORD = "NEWHOTELHUBUSERPASSWORD";
        public const string NEWHOTEL_HUB_STANDID = "NEWHOTELHUBSTANDID";
        public const string NEWHOTEL_HUB_CASHIERID = "NEWHOTELHUBCASHIERID";
        public const string NEWHOTEL_HUB_TAXSCHEMA = "NEWHOTELHUBTAXSCHEMA";
        public const string NEWHOTEL_HUB_SIGNMODE = "NEWHOTELHUBSIGNMODE";
        public const string NEWHOTEL_HUB_WORKDATE = "NEWHOTELHUBWORKDATE";
        public const string NEWHOTEL_HUB_LANGUAGE = "NEWHOTELHUBLANGUAGE";
        public const string NEWHOTEL_HUB_COUNTRY = "NEWHOTELHUBCOUNTRY";
        public const string NEWHOTEL_HUB_VALIDATIONS = "NEWHOTELHUBVALIDATIONS";

        private IDictionary<string, object> _dic = null;

        #endregion

        #region Properties

        public Guid Token { get; }

        public static HubContextCache Current
        {
            get
            {
                if (OperationContext.Current != null)
                    return OperationContext.Current.Extensions.Find<HubContextCache>();

                return null;
            }
        }

        #endregion

        #region Constructor

        public HubContextCache(Guid token)
        {
            Token = token;
        }

        #endregion

        #region Public Methods

        public static void Create(Guid token)
        {
            if (OperationContext.Current != null)
            {
                if (Current == null)
                    OperationContext.Current.Extensions.Add(new HubContextCache(token));
            }
        }

        public static string GetAuditInformation()
        {
            if (OperationContext.Current != null)
            {
                StringBuilder sb = new StringBuilder();
                sb
                    .AppendLine($"Session: {OperationContext.Current.SessionId}, Extensions: {OperationContext.Current.Extensions.Count}");

                return sb.ToString();
			}

            return string.Empty;
		}

        public static void Destroy()
        {
            if (OperationContext.Current != null)
            {
                var current = Current;
                if (current != null)
                    OperationContext.Current.Extensions.Remove(current);
            }
        }

        public bool TryGetValue(string key, out object obj)
        {
            return _dic.TryGetValue(key, out obj);
        }

        public object this[string name]
        {
            get { return _dic[name]; }
            set { _dic[name] = value; }
        }

        #endregion

        #region IExtension<OperationContext> Members

        public void Attach(OperationContext owner)
        {
            _dic = new ConcurrentDictionary<string, object>();
        }

        public void Detach(OperationContext owner)
        {
            _dic = null;
        }

        #endregion

    }
}