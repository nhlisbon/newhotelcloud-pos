﻿using NewHotel.Pos.Hub.Core.Logs;
using System;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace NewHotel.Pos.Hub.Core
{
	public class HubContextBehavior : Attribute, IServiceBehavior
	{
		#region Privaye Methods

		private static IDispatchMessageInspector GetInspector(string scheme)
		{
			switch (scheme)
			{
				case "net.tcp": return new TcpHubContextInspector();
				case "http": return new RestHubContextInspector();
			}

			return null;
		}

		#endregion

		#region IServiceBehavior Members

		public void AddBindingParameters(ServiceDescription serviceDescription,
			ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints,
			BindingParameterCollection bindingParameters)
		{
		}

		public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
		{
			foreach (var channelDispatcherBase in serviceHostBase.ChannelDispatchers)
			{
				var channelDispatcher = (ChannelDispatcher)channelDispatcherBase;
				foreach (var endpoint in channelDispatcher.Endpoints)
				{
					var inspector = GetInspector(endpoint.EndpointAddress.Uri.Scheme);
					if (inspector != null) endpoint.DispatchRuntime.MessageInspectors.Add(inspector);
				}

				channelDispatcher.ErrorHandlers.Add(new HubErrorHandler());
			}
		}

		public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
		{
		}

		#endregion

		private sealed class HubErrorHandler : IErrorHandler
		{
			private const string LogName = "EntryPoint";
			private Guid ActivityId = new Guid("982D4051-670D-4010-8CEB-DBBD1B7DA5BF");
			IHubLogs _log;

			public HubErrorHandler()
			{
				_log = HubLogsFactory.DefaultFactory.CreateHubLogs(LogName, "Failures");// , ActivityId);
			}

			public bool HandleError(Exception error)
			{
				_log.Error(error);

				return false;
			}

			public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
			{
				_log.Error($"FAIL: {fault?.Headers?.Action}\n{error}");
				//if (error is FaultException)
				//	return;

				//Trace.TraceError(error.ToString());
			}
		}
	}
}