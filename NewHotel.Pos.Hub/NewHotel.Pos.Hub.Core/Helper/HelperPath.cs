﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Core.Helper
{
    public static class HelperPath
    {
        public static string GetTemporaryRandomFileName(string ext)
        {
            string tempDirectory = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            Directory.CreateDirectory(tempDirectory);
            tempDirectory = Path.Combine(tempDirectory, Path.GetRandomFileName() + ext);
            return tempDirectory;
        }

        public static void WriteToFile(string fileName, byte[] data)
        {
            var stream = new FileStream(fileName, FileMode.Create);
            var sWriter = new BinaryWriter(stream);
            sWriter.Write(data);
            sWriter.Close();
        }
    }
}
