﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;
using System.Threading;
using System.Configuration;
using System.Xml;
using NewHotel.Pos.Hub.Core.Logs;
using System.Collections.Concurrent;
using System.Linq;
using NewHotel.Contracts;
using NewHotel.Pos.IoC;

namespace NewHotel.Pos.Hub.Core
{
    public abstract class HubContextInspector : IDispatchMessageInspector
    {
        // Change this to match db version!!!
        private const string DbVersion = "20240625";
        private const string LogName = "EntryPoint";

        #region Members

        private static readonly HashSet<string> ExcludedMethods;

        #endregion

        #region Constructor

        static HubContextInspector()
        {
            ExcludedMethods = new HashSet<string>
            {
                "Ping",
                "Mario",
                "Login",
                "GetUserByName",
                "GetUserByCode",
                "GetHotels",
                "CreateSessionToken",
                "SetSessionToken",
                "GetCashierFiles",
                "GetAreas",
                "GetStandAreas",
                "GetAllStands",
                "CheckServiceAlive",
                // Menu
                "GetMenuStandEnvironment",
                "GetMenuOrders",
                "AddMenuOrder",
                "UpdateMenuOrder",
                "DeleteMenuOrder",
                "PlaceMenuOrder",
                //Signatures
                "GetSignatureRequests",
                "ProcessSignature",
                // Help
                "GetHelpPage",
                "GetOperationHelpPage"
            };
        }

        #endregion

        #region Protected Methods

        protected abstract string MethodName { get; }

        protected abstract Guid? Token { get; }

        protected abstract string Version { get; }

        protected abstract string WorkDate { get; }
        protected abstract string SourceName { get; }

        #endregion

        #region IDispatchMessageInspector Members

        private DateTime timer;
        private int threadId;

        struct RequestId
        {
            public RequestId(Guid logid, IDisposable diScope)
            {
                LogId = logid;
				DiScope = diScope;
			}

            public Guid LogId { get; set; }
            public bool NeedReload { get; set; }
			public IDisposable DiScope { get; }
		}

        public virtual object AfterReceiveRequest(ref Message request, IClientChannel channel,
            InstanceContext instanceContext)
        {
            var log = HubLogsFactory.DefaultFactory.CreateHubLogs(LogName, MethodName);
			IDisposable diScope = CwFactory.Instance.BeginScope();
			var requestId = new RequestId(log.Id, diScope);

			try
            {
                //var frames = new System.Diagnostics.StackTrace().GetFrames();
                //var sfr = string.Join(Environment.NewLine, frames.Select(x => x.ToString()));

                List<Validation> validations = new List<Validation>();

                void AddValidation(int code, string message)
                {
					throw new HubException(code, message);
					
                    var validation = new ValidationError(code, message);
					validations.Add(validation);
                    log.Error("VALIDATION: "+validation.ToString());
                }

                log.Information(SourceName);
                log.Information($"HubContextCache Audit: {HubContextCache.GetAuditInformation()}");
                log.Information($"Service Audit: {BaseService.GetSessionDataAudit()} / {Token}");
                timer = DateTime.Now;
                threadId = Thread.CurrentThread.ManagedThreadId;

                // Check if not needed verify the access of the method
                if (OperationContext.Current.EndpointDispatcher.IsSystemEndpoint || string.IsNullOrEmpty(MethodName) ||
                    ExcludedMethods.Contains(MethodName) ||
                    Version == HubContextClientBehavior.NEWHOTEL_HUB_INSTALL_TOKEN) return requestId;

                // Disable handheld version check for now
                if (OperationContext.Current.EndpointDispatcher.ContractName != "IServiceHandheld")
                {
                    // Check Version Compatibility (if provided)
                    var clientVersion = Version;
                    if (!string.IsNullOrEmpty(clientVersion))
                    {
                        var version = Assembly.GetEntryAssembly()?.GetName().Version;
                        if (version != null)
                        {
                            var hubVersion = $"{version.Major}.{version.Minor}.{version.Build}";

                            //if (hubVersion != clientVersion)
                            //    throw new HubException(3,
                            //        "Server/Client Version Mismatch: " + hubVersion + "/" + clientVersion);
                            if (hubVersion != clientVersion)
                                AddValidation(3, "Server/Client Version Mismatch: " + hubVersion + "/" + clientVersion);
                        }
                    }
                    else
                        // throw new HubException(3, "Client Version out of date");
                        AddValidation(3, "Client Version out of date");
                }

                if (Token.HasValue)
                {
                    var hubSessionData = BaseService.GetHubSessionData(Token.Value);
                    if (hubSessionData == null)
                    {
                        // throw new HubException(1, "Token invalid or expired {0}", Token);
                        AddValidation(1, $"Token invalid or expired {Token}");
                    }
                    else
                    {
                        CwFactory.Resolve<IPosSessionFactory>().Create(Token.Value, hubSessionData);

                        var lastSyncMark = BaseService.GetLastSyncMark();
                        if (hubSessionData.LastSyncMark != lastSyncMark)
                        {
                            hubSessionData.LastSyncMark = lastSyncMark;
							requestId.NeedReload = true;
							// AddValidation(2, "Database information updated");
                        }

                        // Check workDate
                        if (!string.IsNullOrEmpty(WorkDate) && hubSessionData.WorkDate != DateTime.MinValue)
                        {
                            if (WorkDate != hubSessionData.WorkDate.ToString("yyyyMMdd"))
                                // throw new HubException(4, hubSessionData.WorkDate.ToString("yyyyMMdd"));
                                AddValidation(4, hubSessionData.WorkDate.ToString("yyyyMMdd"));
                        }

                        // Check database version compatibility
                        if (!string.IsNullOrEmpty(hubSessionData.DatabaseVersion))
                        {
                            if (string.CompareOrdinal(hubSessionData.DatabaseVersion, DbVersion) < 0)
                            {
                                var message =
                                    $"Invalid Database Version {hubSessionData.DatabaseVersion}. Expected >= {DbVersion}";
                                // throw new HubException(5, message);
                                AddValidation(5, message);
                            }
                        }
                    }

                    // Create op cache for current operation
                    HubContextCache.Create(Token.Value);

                    var current = HubContextCache.Current;
                    current[HubContextCache.NEWHOTEL_HUB_INSTALLATIONID] = hubSessionData?.InstallationId ?? Guid.Empty;
                    current[HubContextCache.NEWHOTEL_HUB_USERID] = hubSessionData?.UserId ?? Guid.Empty;
                    current[HubContextCache.NEWHOTEL_HUB_USERNAME] = hubSessionData?.UserName ?? string.Empty;
                    current[HubContextCache.NEWHOTEL_HUB_USERPASSWORD] = hubSessionData?.Password ?? string.Empty;
                    current[HubContextCache.NEWHOTEL_HUB_STANDID] = hubSessionData?.StandId ?? Guid.Empty;
                    current[HubContextCache.NEWHOTEL_HUB_CASHIERID] = hubSessionData?.CashierId ?? Guid.Empty;
                    current[HubContextCache.NEWHOTEL_HUB_TAXSCHEMA] = hubSessionData?.TaxSchemaId ?? Guid.Empty;
                    current[HubContextCache.NEWHOTEL_HUB_SIGNMODE] = hubSessionData?.DocumentSign ?? 0L;
                    current[HubContextCache.NEWHOTEL_HUB_WORKDATE] = hubSessionData?.WorkDate ?? DateTime.MinValue;
                    current[HubContextCache.NEWHOTEL_HUB_LANGUAGE] = hubSessionData?.Data.LanguageId ?? 0L;
                    current[HubContextCache.NEWHOTEL_HUB_COUNTRY] = hubSessionData?.Data.Country ?? string.Empty;
                    current[HubContextCache.NEWHOTEL_HUB_VALIDATIONS] = validations;
                }
                else
                    throw new HubException(2, "Missing token");

                return requestId;
            }
            catch (Exception e)
            {
                log.Error($"CATCH: {e}");
                throw;
            }
        }

        static string callHistotyFilePath = null;
        static bool hasCallHistoryCalculated = false;

        static void SendToLogFile(string logInfo)
        {
            if (!hasCallHistoryCalculated)
            {
                try
                {
                    var conf = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    var path = conf.FilePath;
                    XmlDocument doc = new();
                    doc.Load(path);
                    var nodes = doc["configuration"]["system.diagnostics"]["sharedListeners"].SelectNodes("descendant::add");
                    if (nodes.Count > 0)
                    {
                        foreach (XmlNode node in nodes)
                        {
                            for (int i = 0; i < node.Attributes.Count; i++)
                            {
                                XmlNode attr = node.Attributes[i];
                                if (attr.Name == "initializeData")
                                {
                                    string fpath = System.IO.Path.GetDirectoryName(attr.Value);
                                    if (System.IO.Directory.Exists(fpath))
                                    {
                                        string fileName = System.IO.Path.Combine(fpath, "HUB-CallHistory.txt");
                                        callHistotyFilePath = fileName;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                catch
                {
                    // Cant fail
                }
                finally
                {
                    hasCallHistoryCalculated = true;
                }
            }

            if (!string.IsNullOrEmpty(callHistotyFilePath))
            {
                try
                {
                    System.IO.File.AppendAllLines(callHistotyFilePath, new string[] { logInfo });
                }
                catch
                {
                }
            }

            try
            {
                Console.WriteLine(logInfo);
            }
            catch
			{
				// Cant fail
			}
        }

        protected abstract void ProcessResponseHeaders(Message reply, Dictionary<string, string> headers);

        public virtual void BeforeSendReply(ref Message reply, object correlationState)
        {
            var requestId = correlationState as RequestId?;
            using IHubLogs log = HubLogsFactory.DefaultFactory.CreateHubLogs(LogName, MethodName, requestId?.LogId);

            try
            {
                var headers = new Dictionary<string, string>();

                if (requestId?.NeedReload ?? false)
                {
                    headers.Add("NeedReload", "true");
                    //var header = MessageHeader.CreateHeader("NeedReload", "http://www.newhotel.com/Schemas/PosHub/InternalResult", "true");
                    //reply.Headers.Add(header);
                }
                ProcessResponseHeaders(reply, headers);

                // Destroy op cache for current operation
                HubContextCache.Destroy();
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }

            var consoleColor = ConsoleColor.Gray;
            try
            {
				DateTime endTime = DateTime.Now;
				int endThreadId = Thread.CurrentThread.ManagedThreadId;

				var seconds = (endTime - timer).TotalSeconds;
				consoleColor = Console.ForegroundColor;
				ConsoleColor color;
                if (seconds <= 0.01) color = ConsoleColor.DarkGray;
                else if (seconds <= 0.5) color = ConsoleColor.DarkGreen;
                else if (seconds <= 1) color = ConsoleColor.DarkYellow;
                else color = ConsoleColor.DarkRed;

                Console.ForegroundColor = color;
                string logInfo =
                    $"[{timer:MM/dd HH:mm:ss,ffff}|{threadId,4}] {MethodName,-100} [{endTime:MM/dd HH:mm:ss,ffff}|{endThreadId,4}] {seconds}/{(requestId != null ? requestId.Value.LogId : string.Empty)}";
                SendToLogFile(logInfo);
                log.Information(logInfo);
            }
            catch { /* This cannot fail */}
            finally
            {
                try
                {
                    requestId?.DiScope?.Dispose();
                    Console.ForegroundColor = consoleColor;
                }
                catch
                {
                }
			}
        }

        #endregion
    }

    public class RestHubContextInspector : HubContextInspector
    {
        protected override string SourceName => "-- REST HTTP --";

        private readonly Dictionary<string, string> _requiredHeaders = new Dictionary<string, string>
        {
            { "Access-Control-Allow-Origin", "*" },
            { "Access-Control-Request-Method", "POST,GET,PUT,DELETE,OPTIONS" },
            { "Access-Control-Allow-Headers", "X-Requested-With,Content-Type" }
        };

        protected override string MethodName
        {
            get
            {
                var request = WebOperationContext.Current?.IncomingRequest;
                return request?.UriTemplateMatch?.Data != null
                    ? request.UriTemplateMatch.Data.ToString()
                    : string.Empty;
            }
        }

        protected override Guid? Token
        {
            get
            {
                var request = WebOperationContext.Current?.IncomingRequest;
                var token = request?.Headers.Get(HubContextClientBehavior.NEWHOTEL_HUB_SESSION_TOKEN);

                if (Guid.TryParse(token, out var result)) return result;
                return null;
            }
        }

        protected override string Version
        {
            get
            {
                var request = WebOperationContext.Current?.IncomingRequest;
                return request?.Headers.Get(HubContextClientBehavior.NEWHOTEL_HUB_CLIENT_VERSION);
            }
        }

        protected override string WorkDate
        {
            get
            {
                var request = WebOperationContext.Current?.IncomingRequest;
                return request?.Headers.Get(HubContextClientBehavior.NEWHOTEL_HUB_CLIENT_WORKDATE);
            }
        }

        /// <summary>
        /// Called when an inbound message been received
        /// </summary>
        /// <param name="request">The request message.</param>
        /// <param name="channel">The incoming channel.</param>
        /// <param name="instanceContext">The current service instance.</param>
        /// <returns>
        /// The object used to correlate stateMsg. This object is passed back in the method.
        /// </returns>
        public override object AfterReceiveRequest(ref Message request,
            IClientChannel channel,
            InstanceContext instanceContext)
        {
            if (OperationContext.Current.EndpointDispatcher.IsSystemEndpoint) return null;

            var requestId = base.AfterReceiveRequest(ref request, channel, instanceContext);
            var httpRequest = (HttpRequestMessageProperty)request.Properties[HttpRequestMessageProperty.Name];
            return new
            {
				requestId,
                origin = httpRequest.Headers["Origin"],
                handlePreflight = httpRequest.Method.Equals("OPTIONS", StringComparison.InvariantCultureIgnoreCase)
            };
        }

		protected override void ProcessResponseHeaders(Message reply, Dictionary<string, string> headers)
		{
            try
            {
                var httpHeader = reply.Properties["httpResponse"] as HttpResponseMessageProperty;
                foreach (var item in _requiredHeaders)
                {
                    httpHeader?.Headers.Add(item.Key, item.Value);
                }
                foreach (var item in headers)
                {
                    httpHeader?.Headers.Add(item.Key, item.Value);
                }
            }
            catch
            {
                // This cannot fail
            }
		}

		/// <summary>
		/// Called after the operation has returned but before the reply message is sent.
		/// </summary>
		/// <param name="reply">The reply message. This value is null if the operation is one way.</param>
		/// <param name="correlationState">The correlation object returned from the method.</param>
		public override void BeforeSendReply(ref Message reply, object correlationState)
        {
            if (OperationContext.Current.EndpointDispatcher.IsSystemEndpoint) return;

            var state = (dynamic)correlationState;
            base.BeforeSendReply(ref reply, (object)state?.requestId);

            if (state != null && state.handlePreflight)
            {
                reply = Message.CreateMessage(MessageVersion.None, "PreflightReturn");

                var httpResponse = new HttpResponseMessageProperty();
                reply.Properties.Add(HttpResponseMessageProperty.Name, httpResponse);

                httpResponse.SuppressEntityBody = true;
                httpResponse.StatusCode = HttpStatusCode.OK;
            }

            //var httpHeader = reply.Properties["httpResponse"] as HttpResponseMessageProperty;
            //foreach (var item in _requiredHeaders)
            //{
            //    httpHeader?.Headers.Add(item.Key, item.Value);
            //}
        }
    }

    public class TcpHubContextInspector : HubContextInspector
    {
        protected override string SourceName => "-- TCP --";

        protected override string MethodName
        {
            get
            {
                var action = OperationContext.Current.IncomingMessageHeaders.Action;
                return action.Substring(action.LastIndexOf('/') + 1);
            }
        }

        protected override Guid? Token
        {
            get
            {
                var headers = OperationContext.Current.IncomingMessageHeaders;
                var index = headers.FindHeader(HubContextClientBehavior.NEWHOTEL_HUB_SESSION_TOKEN, string.Empty);
                if (index < 0) return null;
                var token = headers.GetHeader<string>(HubContextClientBehavior.NEWHOTEL_HUB_SESSION_TOKEN,
                    string.Empty);
                if (Guid.TryParse(token, out var result)) return result;
                return null;
            }
        }

        protected override string Version
        {
            get
            {
                var headers = OperationContext.Current.IncomingMessageHeaders;
                var index = headers.FindHeader(HubContextClientBehavior.NEWHOTEL_HUB_CLIENT_VERSION, string.Empty);
                return index >= 0
                    ? headers.GetHeader<string>(HubContextClientBehavior.NEWHOTEL_HUB_CLIENT_VERSION, string.Empty)
                    : string.Empty;
            }
        }

        protected override string WorkDate
        {
            get
            {
                var headers = OperationContext.Current.IncomingMessageHeaders;
                var index = headers.FindHeader(HubContextClientBehavior.NEWHOTEL_HUB_CLIENT_WORKDATE, string.Empty);
                return index >= 0
                    ? headers.GetHeader<string>(HubContextClientBehavior.NEWHOTEL_HUB_CLIENT_WORKDATE, string.Empty)
                    : string.Empty;
            }
        }

		protected override void ProcessResponseHeaders(Message reply, Dictionary<string, string> headers)
		{
            try
            {
                foreach (var item in headers)
                {
                    var header = MessageHeader.CreateHeader(item.Key, "http://www.newhotel.com/Schemas/PosHub/InternalResult", item.Value);
                    reply.Headers.Add(header);
                }
            }
            catch
            {
                // This cannot fail
            }
		}
	}
}