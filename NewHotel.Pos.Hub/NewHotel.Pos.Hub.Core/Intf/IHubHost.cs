﻿using System;

namespace NewHotel.Pos.Hub.Core
{
    public interface IHubHost : IDisposable
    {
        int Run();
    }
}
