﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Contracts
{
	public class NewTicketItemModel
	{
		public Guid TicketId { get; set; }
		public Guid? TableId { get; set; }
		public string TableDescription { get; set; }
		public Guid? SaloonId { get; set; }
		public string ProductCode { get; set; }
		public int Quantity { get; set; }
	}
}
