﻿using System;

namespace NewHotel.Contracts.Pos
{
    public static class Res
    {
        // Example:
        // Operation Group Name
        private static readonly int OperationGroupName = 1;
        public static readonly int OperationName = OperationGroupName.GetResId(1);
    }
}
