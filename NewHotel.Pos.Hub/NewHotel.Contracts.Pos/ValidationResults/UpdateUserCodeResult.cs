﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class UpdateUserCodeResult : ValidationResult
    {
        [DataMember]
        public string Code { get; set; }

        public UpdateUserCodeResult() : base() { }

    }
  
}
