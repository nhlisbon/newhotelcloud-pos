﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class ValidationTicketsResult : ValidationResult
    {
        [DataMember]
        public TypedList<POSTicketContract> Tickets { get; internal set; }

        public ValidationTicketsResult()
        {
            Tickets = new TypedList<POSTicketContract>();
        }

        public ValidationTicketsResult(IEnumerable<Validation> validations)
            : base(validations)
        {
            Tickets = new TypedList<POSTicketContract>();
        }
    }
}
