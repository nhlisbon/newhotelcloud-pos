﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TablesReservationAvailabilityResult : ValidationResult
    {
        [DataMember]
        public int MaxMinutes { get; set; }

        public TablesReservationAvailabilityResult() : base() { }

    }
  
}
