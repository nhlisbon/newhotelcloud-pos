﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class SplitTicketResult : ValidationResult
    {
        [DataMember]
        public TypedList<POSTicketContract> Tickets { get; internal set; }

        public SplitTicketResult()
        {
            Tickets = new TypedList<POSTicketContract>();
        }

        public SplitTicketResult(IEnumerable<Validation> validations)
            : base(validations)
        {
            Tickets = new TypedList<POSTicketContract>();
        }
    }
}