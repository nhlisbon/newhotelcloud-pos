﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PersistTablesReservationLineResult : ValidationContractResult
    {
        [DataMember]
        public string SerieName { get; set; }
        [DataMember]
        public Guid? TablesReservationLineId { get; set; }
        [DataMember]
        public Guid? ReceiptId { get; set; }

        public PersistTablesReservationLineResult() : base() { }

        public PersistTablesReservationLineResult(BaseContract contract)
            : base(contract) { }
    }
  
}
