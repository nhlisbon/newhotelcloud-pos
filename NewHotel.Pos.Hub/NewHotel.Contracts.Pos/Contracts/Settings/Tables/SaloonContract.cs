﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(SaloonContract), "ValidateSaloon")]
    public class SaloonContract:BaseContract
    {
        #region Members
        
        private short? _width;
        private short? _height;
        private string _imageTableFree;
        private string _imageTableOcup;
        private string _backImage;
        
        #endregion

        #region Constructor
        public SaloonContract()
            : base()
        {
            Mesas = new TypedList<MesaContract>();
            Stands = new TypedList<SaloonStandContract>();
            Description = new LanguageTranslationContract();
        }

        #endregion

        #region Properties

        [DataMember]
        internal LanguageTranslationContract _description;

        //[ReflectionExclude]
        [Display(Name = "Description")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        public short? Width { get { return _width; } set { Set(ref _width, value, "Width"); } }
        [DataMember]
        public short? Height { get { return _height; } set { Set(ref _height, value, "Height"); } }
        [DataMember]
        public string ImageTableFree { get { return _imageTableFree; } set { Set(ref _imageTableFree, value, "ImageTableFree"); } }
        [DataMember]
        public string ImageTableOcup { get { return _imageTableOcup; } set { Set(ref _imageTableOcup, value, "ImageTableOcup"); } }
        [DataMember]
        public string BackImage { get { return _backImage; } set { Set(ref _backImage, value, "BackImage"); } }
    
        #endregion

        #region DummmyProperties
        [DataMember]
        public byte[] ImageFree { get; set; } 
        [DataMember]
        public byte[] ImageBusy { get ;set ; }
        [DataMember]
        public byte[] ImageBackground { get; set; } 
    
        #endregion

        #region Contracts
        [ReflectionExclude]
        [DataMember]
        public TypedList<MesaContract> Mesas { get; set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<SaloonStandContract> Stands { get; set; }


        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateSaloon(SaloonContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        [ReflectionExclude]
        public Guid[] MesasIds
        {
            get { return Mesas.Select(x => (Guid)x.Id).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] StandsIds
        {
            get { return Stands.Select(x => (Guid)x.Stand).ToArray(); }
        }

    }
}
