﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(SaloonStandContract), "ValidateSaloonStand")]
    public class SaloonStandContract:BaseContract
    {
        #region Members
        private Guid _saloon;
        private Guid _stand;
        private long _order;
        #endregion

        #region Constructor
        public SaloonStandContract()
            : base()
        {
        }
        public SaloonStandContract(Guid id, Guid stand, Guid saloon, string saloonDesc, string standDesc, long order)
            : base() 
        {
            Id = id;
            Saloon = saloon;
            Stand = stand;
            SaloonDesc = saloonDesc;
            StandDesc = standDesc;
            Order = order;
        }
        #endregion

        #region Properties
        
        [DataMember]
        public Guid Saloon { get { return _saloon; } set { Set(ref _saloon, value, "Saloon"); } }
        [DataMember]
        public Guid Stand { get { return _stand; } set { Set(ref _stand, value, "Stand"); } }
        [DataMember]
        public long Order { get { return _order; } set { Set(ref _order, value, "Order"); } }
        #endregion

        #region DummyProperties
        [DataMember]
        public string SaloonDesc { get; set; }
        [DataMember]
        public string StandDesc { get; set; }
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateSaloonStand(SaloonStandContract obj)
        {
            if (obj.Saloon == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Saloon required.");
            if (obj.Stand == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Stand required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
