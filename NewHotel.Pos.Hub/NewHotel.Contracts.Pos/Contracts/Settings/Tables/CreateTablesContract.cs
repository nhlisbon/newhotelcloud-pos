﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class CreateTablesContract:BaseContract
    {
          #region Members
        private string _iniDesc;
        private string _finDesc;
        private int _step;
        private short _maxPaxs;
        private EPosMesaType _type;
        private string _prefix;
        #endregion

        #region Constructor
        public CreateTablesContract()
            : base()
        {
            Type = EPosMesaType.Standard;
            MaxPaxs = 1;
            Step = 1;
        }
        #endregion

        #region Properties
        [DataMember]
        public string Prefix { get { return _prefix; } set { Set(ref _prefix, value, "Prefix"); } }
        [DataMember]
        public string IniDesc { get { return _iniDesc; } set { Set(ref _iniDesc, value, "IniDesc"); } }
        [DataMember]
        public string FinDesc { get { return _finDesc; } set { Set(ref _finDesc, value, "FinDesc"); } }
        [DataMember]
        public int Step { get { return _step; } set { Set(ref _step, value, "Step"); } }
        [DataMember]
        public short MaxPaxs { get { return _maxPaxs; } set { Set(ref _maxPaxs, value, "MaxPaxs"); } }
        [DataMember]
        public EPosMesaType Type { get { return _type; } set { Set(ref _type, value, "Type"); } }
        #endregion

    }
}
