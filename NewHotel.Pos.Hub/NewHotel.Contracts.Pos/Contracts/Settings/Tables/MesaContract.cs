﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(MesaContract), "ValidateMesa")]
    public class MesaContract:BaseContract
    {
        #region Members
        
        private string _description;
        private Guid _saloon;
        private short _maxPaxs;
        private EPosMesaType _type;
        private short? _row;
        private short? _column;
        private short? _width;
        private short? _height;
        private Guid? _tablesGroup;
      
        #endregion
        #region Constructor

        public MesaContract()
            : base()
        {
        }

        public MesaContract(Guid id, string description, Guid saloon, short maxPaxs, long type, short? row, short? column, short? width, short? height, string prefix )
            : base() 
        {
            Id = id;
            Description = (string.IsNullOrEmpty(prefix)) ? description : string.Concat(prefix,description);
            Saloon = saloon;
            MaxPaxs = maxPaxs;
            Type = (EPosMesaType)type;
            Row = row;
            Column = column;
            Width = width;
            Height = height;
        }

        #endregion

        #region Properties
      
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public Guid Saloon { get { return _saloon; } set { Set(ref _saloon, value, "Saloon"); } }
        [DataMember]
        public short MaxPaxs { get { return _maxPaxs; } set { Set(ref _maxPaxs, value, "MaxPaxs"); } }
        [DataMember]
        public EPosMesaType Type { get { return _type; } set { Set(ref _type, value, "Type"); } }
        [DataMember]
        public short? Row { get { return _row; } set { Set(ref _row, value, "Row"); } }
        [DataMember]
        public short? Column { get { return _column; } set { Set(ref _column, value, "Column"); } }
        [DataMember]
        public short? Width { get { return _width; } set { Set(ref _width, value, "Width"); } }
        [DataMember]
        public short? Height { get { return _height; } set { Set(ref _height, value, "Height"); } }
        [DataMember]
        public Guid? TablesGroup { get { return _tablesGroup; } set { Set(ref _tablesGroup, value, "TablesGroup"); } }
        #endregion

        #region DummyProperties
        public int MesaInt { get; set;  }

        public bool IsAvailable { get; set; }
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateMesa(MesaContract obj)
        {
            if (obj.Saloon == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Saloon required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
        
    }
}
