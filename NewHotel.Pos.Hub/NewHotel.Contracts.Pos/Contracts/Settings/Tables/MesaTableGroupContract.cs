﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class MesaTableGroupContract : BaseContract
    {
        #region Members
        private Guid _table;
        private Guid _tableGroup;
        #endregion

        #region Constructor
        public MesaTableGroupContract()
            : base()
        {
        }
        public MesaTableGroupContract(Guid id, Guid table, Guid tableGroup)
            : base()
        {
            Id = id;
            Table = table;
            TableGroup = tableGroup;
        }
        #endregion

        #region Properties

        [DataMember]
        public Guid Table { get { return _table; } set { Set(ref _table, value, "Table"); } }
        [DataMember]
        public Guid TableGroup { get { return _tableGroup; } set { Set(ref _tableGroup, value, "TableGroup"); } }
        #endregion

    }
}
