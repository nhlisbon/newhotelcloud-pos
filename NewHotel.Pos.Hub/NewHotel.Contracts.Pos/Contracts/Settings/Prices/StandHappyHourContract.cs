﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(StandHappyHourContract), "ValidateStandHappyHour")]
    public class StandHappyHourContract : BaseContract
    {
         #region Members
        private Guid _stand;
        private Guid _happyHour;
        #endregion

        #region Constructor
        public StandHappyHourContract()
            : base()
        {
        }
        public StandHappyHourContract(Guid id, Guid stand, Guid happyHour, string standDesc)
            : base() 
        {
            Id = id;
            Stand = stand;
            HappyHour = happyHour;
            StandDesc = standDesc;
        }
        #endregion

        #region Properties
        [DataMember]
        public Guid Stand { get { return _stand; } set { Set(ref _stand, value, "Stand"); } }
        [DataMember]
        public Guid HappyHour { get { return _happyHour; } set { Set(ref _happyHour, value, "HappyHour"); } }
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateStandHappyHour(StandHappyHourContract obj)
        {
            if (obj.Stand == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Stand required.");

            if (obj.HappyHour == null)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Happy Hour required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        #region DummyProperties
        [DataMember]
        public string StandDesc { get; set; }
        #endregion
    }
}
