﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public class ContainerProductsInPeriod : BaseContract
    {
        #region Constructor
        public ContainerProductsInPeriod()
            : base()
        {
            Products = new TypedList<Guid>();
        }
        #endregion

        #region Contracts
        [DataMember]
        public TypedList<Guid> Products { get; set; }
        #endregion
    }
}
