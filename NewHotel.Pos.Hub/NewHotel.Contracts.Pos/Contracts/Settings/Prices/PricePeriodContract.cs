﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(PricePeriodContract), "ValidatePricePeriod")]
    public class PricePeriodContract:BaseContract
    {
         #region Members
        private Guid _rate;
        private DateTime _initialDate;
        private DateTime _finalDate;
        #endregion

        #region Constructor
        public PricePeriodContract()
            : base()
        {
            PricesRates = new TypedList<ProductPriceRateContract>();
        }
        public PricePeriodContract(Guid id, Guid rate, DateTime initialDate, DateTime finalDate)
            : base() 
        {
            Id = id;
            Rate = rate;
            InitialDate = initialDate;
            FinalDate = finalDate;
        }
        #endregion

        #region Properties
        [DataMember]
        public Guid Rate { get { return _rate; } set { Set(ref _rate, value, "Rate"); } }
        [DataMember]
        public DateTime InitialDate { get { return _initialDate; } set { Set(ref _initialDate, value, "InitialDate"); } }
        [DataMember]
        public DateTime FinalDate { get { return _finalDate; } set { Set(ref _finalDate, value, "FinalDate"); } }
        #endregion

        #region Contracts
        [ReflectionExclude]
        public TypedList<ProductPriceRateContract> PricesRates { get; set; }
        #endregion

        #region Dummy Properties
        public string InitialDateShort { get { return InitialDate.ToShortDateString(); } }
        public string FinalDateShort { get { return FinalDate.ToShortDateString(); } }
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidatePricePeriod(PricePeriodContract obj)
        {
            if (obj.Rate == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Rate required.");

            if(obj.InitialDate > obj.FinalDate)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Final Date must be greater or equal than Initial Date");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        [ReflectionExclude]
        public Guid[] PriceRatesIds
        {
            get { return PricesRates.Select(x => (Guid)x.Id).ToArray(); }
        }
    }
}
