﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public class RatesVisualContract:BaseContract
    {
         #region Constructor
        public RatesVisualContract()
            : base()
        {
            Rates = new TypedList<ProductRateTypeContract>();
        }
        #endregion

        #region Contracts
        [DataMember]
        public TypedList<ProductRateTypeContract> Rates { get;set; }
        #endregion
    }
}
