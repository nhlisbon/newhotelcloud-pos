﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(HappyHourContract), "ValidateHappyHour")]
    public class HappyHourContract: BaseContract
    {
        #region Members
        private DateTime _initialHour;
        private DateTime _finalHour;
        private string _daysOfTheWeek;
        private Guid _taxRate;
        private short _quantity;
        private bool _inactive;
        #endregion
        #region Constructor
        public HappyHourContract()
            : base()
        {
            Description = new LanguageTranslationContract();
            DaysOfTheWeek = "0000000";
            StandHappyHours = new TypedList<StandHappyHourContract>();
        }
        #endregion

        [DataMember]
        internal LanguageTranslationContract _description;

        [Display(Name = "Description")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        #region Properties
        [DataMember]
        public DateTime InitialHour { get { return _initialHour; } set { Set(ref _initialHour, value, "InitialHour"); } }
        [DataMember]
        public DateTime FinalHour { get { return _finalHour; } set { Set(ref _finalHour, value, "FinalHour"); } }
        [DataMember]
        public string DaysOfTheWeek { get { return _daysOfTheWeek; } set { Set(ref _daysOfTheWeek, value, "DaysOfTheWeek"); } }
        [DataMember]
        public Guid TaxRate { get { return _taxRate; } set { Set(ref _taxRate, value, "TaxRate"); } }
        [DataMember]
        public short Quantity { get { return _quantity; } set { Set(ref _quantity, value, "Quantity"); } }
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }
        #endregion
        #region Contracts
        [ReflectionExclude]
        [DataMember]
        public TypedList<StandHappyHourContract> StandHappyHours { get; set; }
        #endregion
        #region DummyProperties
        public bool Sunday 
        {
            get { return DaysOfTheWeek[0] == '1'; }
            set 
            {
                string temp = "";
                if (value) temp += '1';
                else temp+= '0';

                temp+= DaysOfTheWeek[1];
                temp += DaysOfTheWeek[2];
                temp += DaysOfTheWeek[3];
                temp += DaysOfTheWeek[4];
                temp += DaysOfTheWeek[5];
                temp += DaysOfTheWeek[6];

                DaysOfTheWeek = temp;
            }
        }
        public bool Monday
        {
            get { return DaysOfTheWeek[1] == '1'; }
            set
            {
                string temp = "";
                temp += DaysOfTheWeek[0];
                if (value) temp += '1';
                else temp += '0';
                temp += DaysOfTheWeek[2];
                temp += DaysOfTheWeek[3];
                temp += DaysOfTheWeek[4];
                temp += DaysOfTheWeek[5];
                temp += DaysOfTheWeek[6];

                DaysOfTheWeek = temp;
            }
        }
        public bool Tuesday
        {
            get { return DaysOfTheWeek[2] == '1'; }
            set
            {
                string temp = "";
                temp += DaysOfTheWeek[0];
                temp += DaysOfTheWeek[1];
                if (value) temp += '1';
                else temp += '0';
                temp += DaysOfTheWeek[3];
                temp += DaysOfTheWeek[4];
                temp += DaysOfTheWeek[5];
                temp += DaysOfTheWeek[6];

                DaysOfTheWeek = temp;
            }
        }
        public bool Wednesday
        {
            get { return DaysOfTheWeek[3] == '1'; }
            set
            {
                string temp = "";
                temp += DaysOfTheWeek[0];
                temp += DaysOfTheWeek[1];
                temp += DaysOfTheWeek[2];
                if (value) temp += '1';
                else temp += '0';
                temp += DaysOfTheWeek[4];
                temp += DaysOfTheWeek[5];
                temp += DaysOfTheWeek[6];

                DaysOfTheWeek = temp;

            }
        }
        public bool Thursday
        {
            get { return DaysOfTheWeek[4] == '1'; }
            set
            {
                string temp = "";
                temp += DaysOfTheWeek[0];
                temp += DaysOfTheWeek[1];
                temp += DaysOfTheWeek[2];
                temp += DaysOfTheWeek[3];
                if (value) temp += '1';
                else temp += '0';
                temp += DaysOfTheWeek[5];
                temp += DaysOfTheWeek[6];

                DaysOfTheWeek = temp;
            }
        }
        public bool Friday
        {
            get { return DaysOfTheWeek[5] == '1'; }
            set
            {
                string temp = "";
                temp += DaysOfTheWeek[0];
                temp += DaysOfTheWeek[1];
                temp += DaysOfTheWeek[2];
                temp += DaysOfTheWeek[3];
                temp += DaysOfTheWeek[4];
                if (value) temp += '1';
                else temp += '0';
                temp += DaysOfTheWeek[6];

                DaysOfTheWeek = temp;
            }
        }
        public bool Saturday
        {
            get { return DaysOfTheWeek[6] == '1'; }
            set
            {
                string temp = "";
                temp += DaysOfTheWeek[0];
                temp += DaysOfTheWeek[1];
                temp += DaysOfTheWeek[2];
                temp += DaysOfTheWeek[3];
                temp += DaysOfTheWeek[4];
                temp += DaysOfTheWeek[5];
                if (value) temp += '1';
                else temp += '0';

                DaysOfTheWeek = temp;
            }
        }
        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateHappyHour(HappyHourContract obj)
        {
            if (obj.TaxRate == new Guid()) return new System.ComponentModel.DataAnnotations.ValidationResult("Tax Rate Required.");
            if (obj.InitialHour > obj.FinalHour) return new System.ComponentModel.DataAnnotations.ValidationResult("Initial Hour must be greater than Final Hour");
            
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
