﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class CopyRateContract:BaseContract
    {
          #region Members
        private ProductRateTypeContract _contract;
        private ProductRateTypeContract _newRate;
        private Guid _priceCategory;
        private bool _percent;
        private bool _plus;
        private decimal _value;
        private bool _activePeriod;
        private bool _allPeriod;
        private bool _actualPeriod;
        private bool _copyBlocks;
        private string _unmo;
        private long _type;
        private LanguageTranslationContract _description;
        #endregion

        #region Constructor
        public CopyRateContract()
            : base()
        {
        }
        public CopyRateContract(ProductRateTypeContract contract, ProductRateTypeContract newRate, bool percent, bool plus, decimal value, bool activePeriods, bool allPeriods, bool actualPeriod, bool copyBlocks,
                                string unmo, long type, LanguageTranslationContract description, Guid priceCategory)
            : base() 
        {
            Contract = contract;
            Percent = percent;
            Plus = plus;
            Value = value;
            ActivePeriod = activePeriods;
            AllPeriod = allPeriods;
            ActualPeriod = actualPeriod;
            CopyBlocks = copyBlocks;
            Unmo = unmo;
            Type = type;
            Description = description;
            NewRate = newRate;
            PriceCategory = priceCategory;
        }
        #endregion

        #region Properties
        [DataMember]
        public Guid PriceCategory { get { return _priceCategory; } set { Set(ref _priceCategory, value, "PriceCategory"); } }
        [DataMember]
        public ProductRateTypeContract Contract { get { return _contract; } set { Set(ref _contract, value, "Contract"); } }
        [DataMember]
        public ProductRateTypeContract NewRate { get { return _newRate; } set { Set(ref _newRate, value, "NewRate"); } }
        [DataMember]
        public bool Percent { get { return _percent; } set { Set(ref _percent, value, "Percent"); } }
        [DataMember]
        public bool Plus { get { return _plus; } set { Set(ref _plus, value, "Plus"); } }
        [DataMember]
        public decimal Value { get { return _value; } set { Set(ref _value, value, "Value"); } }
        [DataMember]
        public bool ActivePeriod { get { return _activePeriod; } set { Set(ref _activePeriod, value, "ActivePeriod"); } }
        [DataMember]
        public bool AllPeriod { get { return _allPeriod; } set { Set(ref _allPeriod, value, "AllPeriod"); } }
        [DataMember]
        public bool ActualPeriod { get { return _actualPeriod; } set { Set(ref _actualPeriod, value, "ActualPeriod"); } }
        [DataMember]
        public bool CopyBlocks { get { return _copyBlocks; } set { Set(ref _copyBlocks, value, "CopyBlocks"); } }
        [DataMember]
        public string Unmo { get { return _unmo; } set { Set(ref _unmo, value, "Unmo"); } }
        [DataMember]
        public long Type { get { return _type; } set { Set(ref _type, value, "Type"); } }
        [DataMember]
        public LanguageTranslationContract Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        #endregion

    }
}
