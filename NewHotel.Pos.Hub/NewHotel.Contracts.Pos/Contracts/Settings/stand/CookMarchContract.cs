﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(CookMarchContract), "ValidateCookMarch")]
    public class CookMarchContract:BaseContract
    {
        #region Members
        private Guid _tick;
        private bool _printType;
        private bool _ticketComplete;
        private bool _allowMarchInd;
        private bool _closeAutomaticTable;
        private bool _marchPrintReceive;
        #endregion

        #region Constructor
        public CookMarchContract()
            : base()
        {
            Description = new LanguageTranslationContract();
        }
        #endregion

        #region Properties
        [DataMember]
        internal LanguageTranslationContract _description;

        //[ReflectionExclude]
        [Display(Name = "Description")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        public Guid Tick { get { return _tick; } set { Set(ref _tick, value, "Tick"); } }
        [DataMember]
        public bool PrintType { get { return _printType; } set { Set(ref _printType, value, "PrintType"); } }
        [DataMember]
        public bool TicketComplete { get { return _ticketComplete; } set { Set(ref _ticketComplete, value, "TicketComplete"); } }
        [DataMember]
        public bool AllowMarchInd { get { return _allowMarchInd; } set { Set(ref _allowMarchInd, value, "AllowMarchInd"); } }
        [DataMember]
        public bool CloseAutomaticTable { get { return _closeAutomaticTable; } set { Set(ref _closeAutomaticTable, value, "CloseAutomaticTable"); } }
        [DataMember]
        public bool MarchPrintReceive { get { return _marchPrintReceive; } set { Set(ref _marchPrintReceive, value, "MarchPrintReceive"); } }
        
        #endregion


        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateCookMarch(CookMarchContract obj)
        {
            if (obj.Tick == null || obj.Tick == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Tick cannot be empty.");

            if (obj.Description.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description cannot be empty.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
