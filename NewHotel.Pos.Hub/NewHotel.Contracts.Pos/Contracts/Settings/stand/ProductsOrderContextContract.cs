﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    public class ProductsOrderContextContract : BaseContract
    {
        [DataMember]
        public Guid[] StandIds { get; set; }

        [DataMember]
        public Guid CategoryId { get; set; }

        [DataMember]
        public ProductOrderContract[] ProductOrders { get; set; }

        #region To update Next property

        /// <summary>
        /// Categorias en las que hay que actualizar la columna Next 
        /// El mantenimiento de la columna Next deberia desaparecer en algun momento y solo
        /// dejar orde
        /// </summary>
        [DataMember]
        public Guid[] DirtyCategoryIds { get; set; }

        #endregion
    }

    [DataContract]
    public class ProductOrderContract : BaseContract
    {
        [DataMember]
        public Guid ProductId { get; set; }
        [DataMember]
        public long Order { get; set; }

        public ProductOrderContract(Guid productId, long order)
        {
            ProductId = productId;
            Order = order;
        }
    }
}
