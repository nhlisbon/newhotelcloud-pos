﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(StandTimeSlotContract), nameof(ValidateStandSlot))]
    public class StandTimeSlotContract : BaseContract
    {
        #region Variables

        private int? _paxsMax;
        private Guid _standId;
        private string _startHours;
        private string _startMinutes;
        [DataMember]
        internal LanguageTranslationContract _description;

        private int _reservationDurationEstimated;

        #endregion
        #region Constructor

        public StandTimeSlotContract()
        {
            Description = new LanguageTranslationContract();
        }

        #endregion
        #region Properties

        [Display(Name = "Description")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract Description { get { return _description; } set { _description = value; } }

        [DataMember]
        public Guid StandId { get { return _standId; } set { Set(ref _standId, value, nameof(StandId)); } }
        [DataMember]
        public string StartHours { get { return _startHours; } set { Set(ref _startHours, value, nameof(StartHours)); } }
        [DataMember]
        public string StartMinutes { get { return _startMinutes; } set { Set(ref _startMinutes, value, nameof(StartMinutes)); } }
        [DataMember]
        public int? PaxsMaxPercent { get { return _paxsMax; } set { Set(ref _paxsMax, value, nameof(PaxsMaxPercent)); } }
        [DataMember]
        public int ReservationDurationEstimated
        {
            get { return _reservationDurationEstimated; }
            set { Set(ref _reservationDurationEstimated, value, nameof(ReservationDurationEstimated)); }
        }

        public string TimeSlotDescription => Description.Current.Translation == "" ? Description[1033] : Description.Current.Translation;

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateStandSlot(StandTimeSlotContract obj)
        {
            if (obj.Description.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description cannot be empty.");
            if (obj.StandId == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Stand required.");
            if (string.IsNullOrEmpty(obj.StartHours))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Slot start hours cannot be empty.");
            if (string.IsNullOrEmpty(obj.StartHours))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Slot start minutes cannot be empty, can be use 00.");
            if (obj.PaxsMaxPercent < 0 || obj.PaxsMaxPercent > 100)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid arrivals percent, can use values ​​between 0 and 100");
            if (obj.ReservationDurationEstimated < 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid reservation duration, can use values ​​greates or equals to 1");
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
        #region Methods

        public override object Clone()
        {
            var description = new LanguageTranslationContract(Description.Id, Description.Select(x => new TranslationInfoContract(x.LanguageId, x.LanguageName, x.Translation)));
            return new StandTimeSlotContract
            {
                Description = description,
                PaxsMaxPercent = PaxsMaxPercent,
                ReservationDurationEstimated = ReservationDurationEstimated,
                StandId = StandId,
                StartHours = StartHours,
                StartMinutes = StartMinutes
            };
        }

        #endregion

        public TimeSpan Type { get; set; }
    }
}
