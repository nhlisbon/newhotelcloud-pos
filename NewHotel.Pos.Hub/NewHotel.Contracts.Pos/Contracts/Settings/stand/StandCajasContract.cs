﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(StandCajasContract), "ValidateStandCajas")]
    public class StandCajasContract : BaseContract
    {
        #region Members

        private Guid _stand;
        private Guid _caja;
        private bool _block;

        private bool _allowContingencySerie;
        private bool _allowBallotDocument;

        private Guid? _ticketSerie;
        private long? _ticketNumber;
        private string _ticketText;
        private bool _ticketReadOnly;

        private Guid? _invoiceSerie;
        private long? _invoiceNumber;
        private string _invoiceText;
        private bool _invoiceReadOnly;
        private Guid? _invoiceContingencySerie;
        private long? _invoiceContingencyNumber;
        private string _invoiceContingencyText;
        private bool _invoiceContingencyReadOnly;

        private Guid? _creditNoteSerie;
        private long? _creditNoteNumber;
        private string _creditNoteText;
        private bool _creditNoteReadOnly;
        private Guid? _creditNoteContingencySerie;
        private long? _creditNoteContingencyNumber;
        private string _creditNoteContingencyText;
        private bool _creditNoteContingencyReadOnly;

        private long? _ballotNumber;
        private string _ballotText;
        private Guid? _ballotSerie;
        private bool _ballotReadOnly;
        private long? _ballotContingencyNumber;
        private string _ballotContingencyText;
        private Guid? _ballotContingencySerie;
        private bool _ballotContingencyReadOnly;

        private Guid? _lookupTableSerie;
        private long? _lookupTableNumber;
        private string _lookupTableText;
        private bool _lookupTableReadOnly;

        #endregion
        #region Constructor

        public StandCajasContract()
            : base() { }

        public StandCajasContract(Guid id, Guid stand, Guid caja, string cajaDesc, string iposDesc)
            : base()
        {
            Id = id;
            Stand = stand;
            Caja = caja;
            CajaDesc = cajaDesc;
            StandDesc = iposDesc;
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid Stand { get { return _stand; } set { Set(ref _stand, value, "Stand"); } }
        [DataMember]
        public Guid Caja { get { return _caja; } set { Set(ref _caja, value, "Caja"); } }
        [DataMember]
        public bool Block { get { return _block; } set { Set(ref _block, value, "Block"); } }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateStandCajas(StandCajasContract obj)
        {
            if (obj.Stand == Guid.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Stand required.");

            if (obj.Caja == Guid.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Caja required.");

            if (!obj.TicketSerie.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Ticket serie required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
        #region Extended Properties

        [DataMember]
        public string CajaDesc { get; set; }
        [DataMember]
        public string StandDesc { get; set; }

        #endregion
        #region Serie's Properties

        [DataMember]
        public bool AllowContingencySerie { get { return _allowContingencySerie; } set { Set(ref _allowContingencySerie, value, nameof(AllowContingencySerie)); } }
        [DataMember]
        public bool AllowBallotDocument { get { return _allowBallotDocument; } set { Set(ref _allowBallotDocument, value, "AllowBallotDocument"); } }

        #region Ticket

        [DataMember]
        public Guid? TicketSerie { get { return _ticketSerie; } set { Set(ref _ticketSerie, value, "TicketSerie"); } }
        [DataMember]
        public long? TicketNumber { get { return _ticketNumber; } set { Set(ref _ticketNumber, value, "TicketNumber"); } }
        [DataMember]
        public string TicketText { get { return _ticketText; } set { Set(ref _ticketText, value, "TicketText"); } }
        [DataMember]
        public bool TicketReadOnly { get { return _ticketReadOnly; } set { Set(ref _ticketReadOnly, value, "TicketReadOnly"); } }

        #endregion
        #region Invoice

        [DataMember]
        public Guid? InvoiceSerie { get { return _invoiceSerie; } set { Set(ref _invoiceSerie, value, "InvoiceSerie"); } }
        [DataMember]
        public long? InvoiceNumber { get { return _invoiceNumber; } set { Set(ref _invoiceNumber, value, "InvoiceNumber"); } }
        [DataMember]
        public string InvoiceText { get { return _invoiceText; } set { Set(ref _invoiceText, value, "InvoiceText"); } }
        [DataMember]
        public bool InvoiceReadOnly { get { return _invoiceReadOnly; } set { Set(ref _invoiceReadOnly, value, "InvoiceReadOnly"); } }

        #endregion
        #region Invoice Contingency

        [DataMember]
        public Guid? InvoiceContingencySerie { get { return _invoiceContingencySerie; } set { Set(ref _invoiceContingencySerie, value, nameof(InvoiceContingencySerie)); } }
        [DataMember]
        public long? InvoiceContingencyNumber { get { return _invoiceContingencyNumber; } set { Set(ref _invoiceContingencyNumber, value, nameof(InvoiceContingencyNumber)); } }
        [DataMember]
        public string InvoiceContingencyText { get { return _invoiceContingencyText; } set { Set(ref _invoiceContingencyText, value, nameof(InvoiceContingencyText)); } }
        [DataMember]
        public bool InvoiceContingencyReadOnly { get { return _invoiceContingencyReadOnly; } set { Set(ref _invoiceContingencyReadOnly, value, nameof(InvoiceContingencyReadOnly)); } }

        #endregion
        #region Credit Note

        [DataMember]
        public Guid? CreditNoteSerie { get { return _creditNoteSerie; } set { Set(ref _creditNoteSerie, value, "CreditNoteSerie"); } }
        [DataMember]
        public long? CreditNoteNumber { get { return _creditNoteNumber; } set { Set(ref _creditNoteNumber, value, "CreditNoteNumber"); } }
        [DataMember]
        public string CreditNoteText { get { return _creditNoteText; } set { Set(ref _creditNoteText, value, "CreditNoteText"); } }
        [DataMember]
        public bool CreditNoteReadOnly { get { return _creditNoteReadOnly; } set { Set(ref _creditNoteReadOnly, value, "CreditNoteReadOnly"); } }

        #endregion
        #region Cedit Note Contingency

        [DataMember]
        public Guid? CreditNoteContingencySerie { get { return _creditNoteContingencySerie; } set { Set(ref _creditNoteContingencySerie, value, "CreditNoteContingencySerie"); } }
        [DataMember]
        public long? CreditNoteContingencyNumber { get { return _creditNoteContingencyNumber; } set { Set(ref _creditNoteContingencyNumber, value, "CreditNoteContingencyNumber"); } }
        [DataMember]
        public string CreditNoteContingencyText { get { return _creditNoteContingencyText; } set { Set(ref _creditNoteContingencyText, value, "CreditNoteContingencyText"); } }
        [DataMember]
        public bool CreditNoteContingencyReadOnly { get { return _creditNoteContingencyReadOnly; } set { Set(ref _creditNoteContingencyReadOnly, value, "CreditNoteContingencyReadOnly"); } }

        #endregion
        #region Ballot

        [DataMember]
        public long? BallotNumber { get { return _ballotNumber; } set { Set(ref _ballotNumber, value, "BallotNumber"); } }
        [DataMember]
        public string BallotText { get { return _ballotText; } set { Set(ref _ballotText, value, "BallotText"); } }
        [DataMember]
        public Guid? BallotSerie { get { return _ballotSerie; } set { Set(ref _ballotSerie, value, "BallotSerie"); } }
        [DataMember]
        public bool BallotReadOnly { get { return _ballotReadOnly; } set { Set(ref _ballotReadOnly, value, "BallotReadOnly"); } }

        #endregion
        #region Ballot Contingency

        [DataMember]
        public long? BallotContingencyNumber { get { return _ballotContingencyNumber; } set { Set(ref _ballotContingencyNumber, value, "BallotContingencyNumber"); } }
        [DataMember]
        public string BallotContingencyText { get { return _ballotContingencyText; } set { Set(ref _ballotContingencyText, value, "BallotContingencyText"); } }
        [DataMember]
        public Guid? BallotContingencySerie { get { return _ballotContingencySerie; } set { Set(ref _ballotContingencySerie, value, "BallotContingencySerie"); } }
        [DataMember]
        public bool BallotContingencyReadOnly { get { return _ballotContingencyReadOnly; } set { Set(ref _ballotContingencyReadOnly, value, "BallotContingencyReadOnly"); } }

        #endregion
        #region LookupTable

        [DataMember]
        public Guid? LookupTableSerie { get { return _lookupTableSerie; } set { Set(ref _lookupTableSerie, value, "LookupTableSerie"); } }
        [DataMember]
        public long? LookupTableNumber { get { return _lookupTableNumber; } set { Set(ref _lookupTableNumber, value, "LookupTableNumber"); } }
        [DataMember]
        public string LookupTableText { get { return _lookupTableText; } set { Set(ref _lookupTableText, value, "LookupTableText"); } }
        [DataMember]
        public bool LookupTableReadOnly { get { return _lookupTableReadOnly; } set { Set(ref _lookupTableReadOnly, value, "LookupTableReadOnly"); } }

        #endregion

        #endregion
    }
}