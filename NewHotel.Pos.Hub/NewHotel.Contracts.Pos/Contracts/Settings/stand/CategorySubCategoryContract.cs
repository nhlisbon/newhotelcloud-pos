﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;
using System.Collections.ObjectModel;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(CategorySubCategoryContract), "ValidateCategorySubCategory")]
    public class CategorySubCategoryContract : BaseContract
    {
        #region Members
        private Blob? _image;
        private Guid? _category;
        private short _orde;
        #endregion

        #region Constructor
        public CategorySubCategoryContract()
            : base()
        {
            Description = new LanguageTranslationContract();
            Categories = new ObservableCollection<CategorySubCategoryContract>();
            Products = new ObservableCollection<ProductStandCategoryContract>();
        }

        public CategorySubCategoryContract(Guid id, Guid category, Blob image)
            : this()
        {
            Id = id;
            Category = category;
            Image = image;
        }

        #endregion

        #region Properties
        [DataMember]
        public LanguageTranslationContract _description;

        [ReflectionExclude]
        [Display(Name = "Description")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        public short Orde { get { return _orde; } set { Set(ref _orde, value, "Orde"); } }
        [DataMember]
        public Guid? Category { get { return _category; } set { Set(ref _category, value, "Category"); } }
        [DataMember]
        public Blob? Image { get { return _image; } set { Set(ref _image, value, "Image"); } }

        #endregion

        #region Contracts
        private ObservableCollection<CategorySubCategoryContract> _categories;
        [DataMember]
        public ObservableCollection<CategorySubCategoryContract> Categories { get { return _categories; } set { Set(ref _categories, value, "Categories"); } }


        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateCategorySubCategory(CategorySubCategoryContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        #region Dummy Properties

        private string _name_desc;
        [DataMember]
        public string Name_Desc { get { return _name_desc; } set { Set(ref _name_desc, value, "Name_Desc"); } }

        public bool IsFloating
        {
            get;
            set;
        }

        private ObservableCollection<ProductStandCategoryContract> _products;
        public ObservableCollection<ProductStandCategoryContract> Products { get { return _products; } set { Set(ref _products, value, "Products"); } }

        #endregion

        [ReflectionExclude]
        public Guid[] CategoriesIds
        {
            get { return Categories.Select(x => (Guid)x.Id).ToArray(); }
        }


    }
}
