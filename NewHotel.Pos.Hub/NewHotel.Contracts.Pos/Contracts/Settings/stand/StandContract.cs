﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(StandContract), "ValidateStand")]
    public class StandContract : DepartmentContract
    {
        #region Members

        private POSType _standType;
        private short _copiesCant;
        private bool _manualPrice;
        private DateTime _standWorkDate;
        private bool _adittionalProduct;
        private bool _printInClose;
        private bool _applyRechargue;
        private bool _closeDayOpenTickets;
        private bool _previewPrint;
        private bool _cajTickets;
        private Guid? _standardRateType;
        private Guid? _consIntRateType;
        private Guid? _pensionRateType;
        private decimal? _valueConsInt;
        private decimal? _valuePension;
        private Guid? _cControl;
        private Guid? _ticketPrint;
        private Guid? _factPrint;
        private Guid? _compPrint;
        private Guid? _ballotPrint;
        private int _shift;
        private bool _copyKitchenInTicketPrinter;
        private bool _keepTicketsInTableOnManualSplit;
        private Guid? _tipService;
        private decimal? _tipPercent;
        private decimal? _tipValue;
        private bool _tipAuto;
        private bool _tipOverNet;
        private Guid? _menuMonday;
        private Guid? _menuTuesday;
        private Guid? _menuWednesday;
        private Guid? _menuThusday;
        private Guid? _menuFriday;
        private Guid? _menuSaturday;
        private Guid? _menuSunday;
        private bool _includeOnCloseCheck;
        private Guid? _invoiceEntityId;
        private Guid? _standardTaxSchemaId;
        private Guid? _standardMarketSourceId;
        private Guid? _standardMarketSegmentId;
        private bool _isRoomService;

        #endregion
        #region Properties

        [DataMember]
        public POSType StandType { get { return _standType; } set { Set(ref _standType, value, "StandType"); } }//OK
        [DataMember]
        public short CopiesCant { get { return _copiesCant; } set { Set(ref _copiesCant, value, "CopiesCant"); } }//OK
        [DataMember]
        public DateTime StandWorkDate { get { return _standWorkDate; } set { Set(ref _standWorkDate, value, "StandWorkDate"); } }//OK
        [DataMember]
        public decimal? ValueConsInt { get { return _valueConsInt; } set { Set(ref _valueConsInt, value, "ValueConsInt"); } }//OK
        [DataMember]
        public decimal? ValuePension { get { return _valuePension; } set { Set(ref _valuePension, value, "ValuePension"); } }//OK
        [DataMember]
        public Guid? StandardRateType { get { return _standardRateType; } set { Set(ref _standardRateType, value, "StandardRateType"); } }//OK
        [DataMember]
        public Guid? ConsIntRateType { get { return _consIntRateType; } set { Set(ref _consIntRateType, value, "ConsIntRateType"); } }//OK
        [DataMember]
        public Guid? PensionRateType { get { return _pensionRateType; } set { Set(ref _pensionRateType, value, "PensionRateType"); } }//OK
        [DataMember]
        public Guid? CControl { get { return _cControl; } set { Set(ref _cControl, value, "CControl"); } }//OK
        [DataMember]
        public Guid? TicketPrint { get { return _ticketPrint; } set { Set(ref _ticketPrint, value, "TicketPrint"); } }//OK
        [DataMember]
        public Guid? BallotPrint { get { return _ballotPrint; } set { Set(ref _ballotPrint, value, nameof(BallotPrint)); } }
        [DataMember]
        public Guid? FactPrint { get { return _factPrint; } set { Set(ref _factPrint, value, "FactPrint"); } }//OK
        [DataMember]
        public Guid? CompPrint { get { return _compPrint; } set { Set(ref _compPrint, value, "CompPrint"); } }//OK
        [DataMember]
        public bool ManualPrice { get { return _manualPrice; } set { Set(ref _manualPrice, value, "ManualPrice"); } }
        [DataMember]
        public bool AdittionalProduct { get { return _adittionalProduct; } set { Set(ref _adittionalProduct, value, "AdittionalProduct"); } }
        [DataMember]
        public bool PrintInClose { get { return _printInClose; } set { Set(ref _printInClose, value, "PrintInClose"); } }
        [DataMember]
        public bool ApplyRechargue { get { return _applyRechargue; } set { Set(ref _applyRechargue, value, "ApplyRechargue"); } }
        [DataMember]
        public bool CloseDayOpenTickets { get { return _closeDayOpenTickets; } set { Set(ref _closeDayOpenTickets, value, "CloseDayOpenTickets"); } }
        [DataMember]
        public bool PreviewPrint { get { return _previewPrint; } set { Set(ref _previewPrint, value, "PreviewPrint"); } }
        [DataMember]
        public bool CajTickets { get { return _cajTickets; } set { Set(ref _cajTickets, value, "CajTickets"); } }
        [DataMember]
        public int Shift { get { return _shift; } set { Set(ref _shift, value, "Shift"); } }
        [DataMember]
        public bool CopyKitchenInTicketPrinter { get { return _copyKitchenInTicketPrinter; } set { Set(ref _copyKitchenInTicketPrinter, value, "CopyKitchenInTicketPrinter"); } }
        [DataMember]
        public bool KeepTicketsInTableOnManualSplit { get { return _keepTicketsInTableOnManualSplit; } set { Set(ref _keepTicketsInTableOnManualSplit, value, "KeepTicketsInTableOnManualSplit"); } }
        [DataMember]
        public Guid? TipService { get { return _tipService; } set { Set(ref _tipService, value, "TipService"); } }
        [DataMember]
        public decimal? TipPercent { get { return _tipPercent; } set { Set(ref _tipPercent, value, "TipPercent"); } }
        [DataMember]
        public decimal? TipValue { get { return _tipValue; } set { Set(ref _tipValue, value, "TipValue"); } }
        [DataMember]
        public bool TipAuto { get { return _tipAuto; } set { Set(ref _tipAuto, value, "TipAuto"); } }
        [DataMember]
        public bool TipOverNet { get { return _tipOverNet; } set { Set(ref _tipOverNet, value, "TipOverNet"); } }
        [DataMember]
        public Guid? MenuMonday { get { return _menuMonday; } set { Set(ref _menuMonday, value, "MenuMonday"); } }
        [DataMember]
        public Guid? MenuTuesday { get { return _menuTuesday; } set { Set(ref _menuTuesday, value, "MenuTuesday"); } }
        [DataMember]
        public Guid? MenuWednesday { get { return _menuWednesday; } set { Set(ref _menuWednesday, value, "MenuWednesday"); } }
        [DataMember]
        public Guid? MenuThusday { get { return _menuThusday; } set { Set(ref _menuThusday, value, "MenuThusday"); } }
        [DataMember]
        public Guid? MenuFriday { get { return _menuFriday; } set { Set(ref _menuFriday, value, "MenuFriday"); } }
        [DataMember]
        public Guid? MenuSaturday { get { return _menuSaturday; } set { Set(ref _menuSaturday, value, "MenuSaturday"); } }
        [DataMember]
        public Guid? MenuSunday { get { return _menuSunday; } set { Set(ref _menuSunday, value, "MenuSunday"); } }
        [DataMember]
        public bool IncludeOnCloseCheck { get { return _includeOnCloseCheck; } set { Set(ref _includeOnCloseCheck, value, "IncludeOnCloseCheck"); } }
        [DataMember]
        public Guid? InvoiceEntityId { get { return _invoiceEntityId; } set { Set(ref _invoiceEntityId, value, "InvoiceEntityId"); } }
        [DataMember]
        public Guid? StandardTaxSchemaId { get { return _standardTaxSchemaId; } set { Set(ref _standardTaxSchemaId, value, "StandardTaxSchemaId"); } }
        [DataMember]
        public Guid? StandardMarketSourceId { get { return _standardMarketSourceId; } set { Set(ref _standardMarketSourceId, value, "StandardMarketSourceId"); } }
        [DataMember]
        public Guid? StandardMarketSegmentId { get { return _standardMarketSegmentId; } set { Set(ref _standardMarketSegmentId, value, "StandardMarketSegmentId"); } }
        [DataMember]
        public bool IsRoomService { get { return _isRoomService; } set { Set(ref _isRoomService, value, "IsRoomService"); } }

        #endregion
        #region Constructor

        public StandContract()
            : base()
        {
            StandsProducts = new TypedList<StandProductsContract>();
            StandsCajas = new TypedList<StandCajasContract>();
            StandsPriceCosts = new TypedList<ProductPriceCostContract>();
            Saloons = new TypedList<SaloonStandContract>();
            TimeSlots = new TypedList<StandTimeSlotContract>();
            OrderImages = new TypedList<OrderImageByStandContract>();
            UserByStand = new TypedList<UserByStandContract>();
        }

        #endregion
        #region Contracts

        [DataMember]
        public TypedList<StandProductsContract> StandsProducts { get; set; }
        [DataMember]
        public TypedList<StandCajasContract> StandsCajas { get; set; }
        [DataMember]
        public TypedList<ProductPriceCostContract> StandsPriceCosts { get; set; }
        [DataMember]
        public TypedList<SaloonStandContract> Saloons { get; set; }
        [DataMember]
        public TypedList<StandTimeSlotContract> TimeSlots { get; set; }
        [DataMember]
        public TypedList<UserByStandContract> UserByStand { get; set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<OrderImageByStandContract> OrderImages { get; set; }

        #endregion
        #region DummyProperties

        [DataMember]
        public short AccountMoment { get; set; }
        [DataMember]
        public Guid? CookMarch { get; set; }
        [DataMember]
        public string InvoiceEntityDescription { get; set; }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateStand(StandContract obj)
        {
            if (!obj.StandardRateType.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Standard rate cannot be empty.");

            if (obj.Abbreviation.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Abbreviation cannot be empty.");

            if (obj.Description.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description cannot be empty.");

            if (obj.CookMarch == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Cook March cannot be empty.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        [ReflectionExclude]
        public Guid[] StandProductsIds
        {
            get { return StandsProducts.Select(x => x.Stand).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] StandCajasIds
        {
            get { return StandsCajas.Select(x => x.Stand).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] StandsPriceCostsIds
        {
            get { return StandsPriceCosts.Select(x => x.Stand).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] SaloonsIds
        {
            get { return Saloons.Select(x => x.Stand).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] TimeSlotsIds
        {
            get { return TimeSlots.Select(x => x.StandId).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] OrderImagesIds
        {
            get { return OrderImages.Select(x => x.Stand).ToArray(); }
        } 
    }
}