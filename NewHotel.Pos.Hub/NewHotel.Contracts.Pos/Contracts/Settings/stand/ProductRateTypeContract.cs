﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ProductRateTypeContract), "ValidateProductRateType")]
    public class ProductRateTypeContract:BaseContract
    {
         #region Members
        private string _unmo;
        private bool _taxesIncluded;
        private bool _inactive;
        private EPosRateType _type;
        Guid _priceCategory;
        #endregion

        #region Properties

        [DataMember]
        internal LanguageTranslationContract _description;


        [ReflectionExclude]
        [Display(Name = "Description")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        public string Unmo { get { return _unmo; } set { Set(ref _unmo, value, "Unmo"); } }

        [DataMember]
        public bool TaxesIncluded { get { return _taxesIncluded; } set { Set(ref _taxesIncluded, value, "TaxesIncluded"); } }

        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }

        [DataMember]
        public EPosRateType Type { get { return _type; } set { Set(ref _type, value, "Type"); } }

        [DataMember]
        public Guid PriceCategory { get { return _priceCategory; } set { Set(ref _priceCategory, value, "PriceCategory"); } }

        #endregion

        #region Constructor
        public ProductRateTypeContract()
            : base()
        {
            Description = new LanguageTranslationContract();
            PricePeriods = new TypedList<PricePeriodContract>();
            BlockPeriods = new TypedList<BlockPeriodContract>();
        }

        public ProductRateTypeContract(Guid id, string description, string unmo, bool taxesIncluded, bool inactive, EPosRateType type)
            : base()
        {
            this.Id = id;
            Unmo = unmo;
            TaxesIncluded = taxesIncluded;
            DescriptionDummy = description;
            this.Inactive = inactive;
            this.Type = type;

            Description = new LanguageTranslationContract();
            PricePeriods = new TypedList<PricePeriodContract>();
            BlockPeriods = new TypedList<BlockPeriodContract>();
        }

        #endregion

        #region Contracts
        [ReflectionExclude]
        [DataMember]
        public TypedList<PricePeriodContract> PricePeriods { get; set; }
        [ReflectionExclude]
        [DataMember]
        public TypedList<BlockPeriodContract> BlockPeriods { get; set; }
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateProductRateType(ProductRateTypeContract obj)
        {
            if (obj.Unmo == null || obj.Unmo == "")
                return new System.ComponentModel.DataAnnotations.ValidationResult("Currency required.");
            if (obj.Description == null)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Currency required.");
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        #region DummyProperties
        [DataMember]
        public string DescriptionDummy { get; set; }
        #endregion


        [ReflectionExclude]
        public Guid[] PricePeriodsIds
        {
            get { return PricePeriods.Select(x => x.Rate).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] BlockPeriodsIds
        {
            get { return BlockPeriods.Select(x => x.Rate).ToArray(); }
        }

      
    }
}
