﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ProductPriceCostContract), "ValidateProductPriceCost")]
    public class ProductPriceCostContract:BaseContract
    {
        #region Members
        private Guid _stand;
        private Guid _product;
        private long _priceCost;
        #endregion

        #region Constructor
        public ProductPriceCostContract()
            : base()
        {
        }
        public ProductPriceCostContract(Guid id, Guid stand, Guid product,long priceCost, string productDesc)
            : base() 
        {
            Id = id;
            Stand = stand;
            Product = product;
            PriceCost = priceCost;
            ProductDesc = productDesc;
        }
        #endregion

        #region Properties
        [DataMember]
        public Guid Stand { get { return _stand; } set { Set(ref _stand, value, "Stand"); } }
        [DataMember]
        public Guid Product { get { return _product; } set { Set(ref _product, value, "Product"); } }
        [DataMember]
        public long PriceCost { get { return _priceCost; } set { Set(ref _priceCost, value, "PriceCost"); } }
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateProductPriceCost(ProductPriceCostContract obj)
        {
            if (obj.Stand == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Stand required.");

            if (obj.Product == null)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Product required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        #region DummyProperties
        [DataMember]
        public string ProductDesc { get; set; }
        #endregion
    }
}
