﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(GastStandContract), "ValidateGastStand")]
    public class GastStandContract : BaseContract
    {
         #region Members
         private short _ticketC;
        #endregion

        #region Constructor
        public GastStandContract()
            : base()
        {
        }
        #endregion

        #region Properties
        [DataMember]
        public short Category { get { return _ticketC; } set { Set(ref _ticketC, value, "TicketC"); } }
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateGastStand(GastStandContract obj)
        {
           
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
