﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class StandTurnDateContract : BaseContract
    {
        #region Members
        private long _turn;
        private DateTime _workDate;
        #endregion

        #region Constructor
        public StandTurnDateContract()
            : base()
        {
        }
        #endregion

        #region Properties
        [DataMember]
        public long Turn { get { return _turn; } set { Set(ref _turn, value, "Turn"); } }
        [DataMember]
        public DateTime WorkDate { get { return _workDate; } set { Set(ref _workDate, value, "WorkDate"); } }
        
        #endregion
    }
}
