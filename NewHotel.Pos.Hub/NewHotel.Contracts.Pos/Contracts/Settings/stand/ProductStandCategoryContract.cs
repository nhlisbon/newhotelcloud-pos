﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ProductStandCategoryContract), "ValidateProductStandCategory")]
    public class ProductStandCategoryContract:BaseContract
    {
        #region Members
        private Guid _stand;
        private Guid _product;
        private Guid _category;
        private long _orden;
        private Guid? _next;
        private long? _colorId;
        private ARGBColor _color;
        #endregion

        #region Constructor
        public ProductStandCategoryContract()
            : base()
        {
        }
        public ProductStandCategoryContract(Guid id, Guid stand, Guid product, Guid category, long orden, Guid? next, string productDesc, string productAbbre, Blob? productImage, double? productPrice, Guid? separator, decimal step, long? colorId, DateTime lastModified)
            : base() 
        {
            Id = id;
            Stand = stand;
            Product = product;
            Category = category;
            Orden = orden;
            Next = next;
            ProductDesc = productDesc;
            ProductAbbre = productAbbre;
            ProductImage = productImage;
            ProductPrice = productPrice;
            DefaultSeparator = separator;
            Step = step;
            ColorId = colorId;
            LastModified = lastModified;

            if (!ColorId.HasValue)
                Color = ARGBColor.AntiqueWhite;
            else Color = ARGBColor.NewARGBColor(255, (byte)(ColorId.Value / (256 * 256)), (byte)((ColorId / 256) % 256), (byte)(ColorId % 256));
        }

        #endregion

        #region Properties
        [DataMember]
        public Guid Stand { get { return _stand; } set { Set(ref _stand, value, "Stand"); } }
        [DataMember]
        public Guid Product { get { return _product; } set { Set(ref _product, value, "Product"); } }
        [DataMember]
        public Guid Category { get { return _category; } set { Set(ref _category, value, "Category"); } }
        [DataMember]
        public long Orden { get { return _orden; } set { Set(ref _orden, value, "Orden"); } }
        [DataMember]
        public Guid? Next { get { return _next; } set { Set(ref _next, value, "Next"); } }
        [DataMember]
        public long? ColorId { get { return _colorId; } set { Set(ref _colorId, value, "ColorId"); } }

        public ARGBColor Color { get { return _color; } set { Set(ref _color, value, "Color"); } }
       
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateProductStandCategory(ProductStandCategoryContract obj)
        {
            if (obj.Stand == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Stand required.");

            if (obj.Product == null)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Product required.");

            if (obj.Category == null)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Category required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        #region DummyProperties
        [DataMember]
        public string ProductDesc { get; set; }
        [DataMember]
        public string ProductAbbre { get; set; }
        [DataMember]
        public double? ProductPrice { get; set; }
        [DataMember]
        public Blob? ProductImage { get; set; }
        [DataMember]
        public Guid? DefaultSeparator { get; set; }
        [DataMember]
        public decimal Step { get; set; }

        public bool IsFloating { get; set; }

        public ProductStandCategoryContract PrevModel { get; set; }

        public ProductStandCategoryContract NextModel { get; set; }

        #endregion
    }
}
