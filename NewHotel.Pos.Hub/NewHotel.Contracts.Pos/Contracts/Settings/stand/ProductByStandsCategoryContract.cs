﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ProductByStandsCategoryContract:BaseContract
    {
        #region Members
        private Guid _product;
        private long _orden;
        private Guid? _next;
        private long? _colorId;
        private ARGBColor _color;
        #endregion

        #region Constructor
        public ProductByStandsCategoryContract()
            : base()
        {
        }
        public ProductByStandsCategoryContract(Guid product, long orden, string productDesc, string productAbbre, Blob? productImage, double? productPrice, Guid? separator, decimal step, long? colorId, DateTime lastModified)
            : base() 
        {
            Product = product;
            Orden = orden;
            ProductDesc = productDesc;
            ProductAbbre = productAbbre;
            ProductImage = productImage;
            ProductPrice = productPrice;
            DefaultSeparator = separator;
            Step = step;
            ColorId = colorId;
            LastModified = lastModified;

            Color = !ColorId.HasValue ? ARGBColor.AntiqueWhite : ARGBColor.NewARGBColor(255, (byte)(ColorId.Value / (256 * 256)), (byte)((ColorId / 256) % 256), (byte)(ColorId % 256));
        }

        #endregion

        #region Properties
        [DataMember]
        public Guid Product { get { return _product; } set { Set(ref _product, value, "Product"); } }
        [DataMember]
        public long Orden { get { return _orden; } set { Set(ref _orden, value, "Orden"); } }
        [DataMember]
        public Guid? Next { get { return _next; } set { Set(ref _next, value, "Next"); } }
        [DataMember]
        public long? ColorId { get { return _colorId; } set { Set(ref _colorId, value, "ColorId"); } }
        
        public ARGBColor Color { get { return _color; } set { Set(ref _color, value, "Color"); } }
       
        #endregion

        #region Validations

        #endregion

        #region DummyProperties
        [DataMember]
        public string ProductDesc { get; set; }
        [DataMember]
        public string ProductAbbre { get; set; }
        [DataMember]
        public double? ProductPrice { get; set; }
        [DataMember]
        public Blob? ProductImage { get; set; }
        [DataMember]
        public Guid? DefaultSeparator { get; set; }
        [DataMember]
        public decimal Step { get; set; }

        public bool IsFloating { get; set; }


        #endregion
    }
}
