﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [CustomValidation(typeof(UserbyStandContract), "ValidateUserbyStand")]
    public class UserbyStandContract : BaseContract
    {
        #region Members

        private Guid _stand;
        private Guid _userId;
        private SecurityCode _securitycode;

        #endregion
        #region Constructor

        public UserbyStandContract()
            : base()
        {
        }

        public UserbyStandContract(Guid id, Guid stand, Guid userId, SecurityCode securitycode, string userLogin, string iposDesc)
            : base() 
        {
            Id = id;
            Stand = stand;
            UserId = userId;
            UserLogin = userLogin;
            SecurityCode = securitycode;
            StandDesc = iposDesc;
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid Stand { get { return _stand; } set { Set(ref _stand, value, "Stand"); } }
        [DataMember]
        public Guid UserId { get { return _userId; } set { Set(ref _userId, value, "UserId"); } }
        [DataMember]
        public SecurityCode SecurityCode { get { return _securitycode; } set { Set(ref _securitycode, value, "SecurityCode"); } }

        #endregion
        #region Extended Properties

        [DataMember]
        public string UserLogin { get; set; }
        [DataMember]
        public string StandDesc { get; set; }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateUserbyStand(UserbyStandContract obj)
        {
            if (obj.Stand == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Stand required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}