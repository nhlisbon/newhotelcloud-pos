﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ProductbyDispatchAreaContract : BaseContract
    {
        #region Members
        private Guid _areaId;
        private Guid _standId;
        private Guid _productId;
        private long _order;
        #endregion
        #region Properties

        [DataMember]
        public Guid StandId { get { return _standId; } set { Set(ref _standId, value, "StandId"); } }
        [DataMember]
        public string StandDescription { get; set; }
        [DataMember]
        public string ProductDescription { get; set; }
        [DataMember]
        public string ProductCode { get; set; }
        [DataMember]
        public Guid ProductId { get { return _productId; } set { Set(ref _productId, value, "ProductId"); } }
        [DataMember]
        public Guid AreaId { get { return _areaId; } set { Set(ref _areaId, value, "AreaId"); } }
        [DataMember]
        public string AreaDescription { get; set; }
        [DataMember]
        public long Order { get { return _order; } set { Set(ref _order, value, "Order"); } }
        #endregion

        public ProductbyDispatchAreaContract(Guid id, Guid standId, Guid productId, Guid areaId, string productDescription, string productCode, long order, string standDesc, string areaDesc)
            : base()

        {
            Id = id;
            StandId = standId;
            StandDescription = standDesc;
            ProductId = productId;
            AreaId = areaId;
            AreaDescription = areaDesc;
            ProductCode = productCode;
            ProductDescription = productDescription;
            Order = order;
        }
        public ProductbyDispatchAreaContract()
            : base()
        {
        }
    }
}
