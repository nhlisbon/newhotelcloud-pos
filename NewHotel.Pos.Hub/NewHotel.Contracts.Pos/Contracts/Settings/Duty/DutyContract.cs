﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    //[CustomValidation(typeof(DutyContract), "ValidateDuty")]
    public class DutyContract:BaseContract
    {
          #region Members
        private DateTime _initialHour;
        private DateTime _finalHour;
        #endregion

        #region Constructor
        public DutyContract()
            : base()
        {
            Description = new LanguageTranslationContract();
            InitialHour = DateTime.Now;
            FinalHour = DateTime.Now;
        }
        #endregion

        #region Properties
        [DataMember]
        internal LanguageTranslationContract _description;

        [ReflectionExclude]
        [Display(Name = "Description")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        public DateTime InitialHour { get { return _initialHour; } set { Set(ref _initialHour, value, "InitialHour"); } }
        [DataMember]
        public DateTime FinalHour { get { return _finalHour; } set { Set(ref _finalHour, value, "FinalHour"); } }
        #endregion

        #region DummyProperties
        public int IniHour
        {
            get
            {
                if (InitialHour.Hour > 0 && InitialHour.Hour < 25)
                    return InitialHour.Hour;
                else
                    return 1;
            }
            set
            {
                InitialHour = new DateTime(InitialHour.Year, InitialHour.Month, InitialHour.Day, value, IniMinutes, 0);
            }
        }
        public int IniMinutes
        {
            get
            {
                return InitialHour.Minute;
            }
            set
            {
                InitialHour = new DateTime(InitialHour.Year, InitialHour.Month, InitialHour.Day, IniHour, value, 0);
            }
        }
        public int FinHour
        {
            get
            {
                if (FinalHour.Hour > 0 && FinalHour.Hour < 25)
                    return FinalHour.Hour;
                else
                    return 1;
            }
            set
            {
                FinalHour = new DateTime(FinalHour.Year, FinalHour.Month, FinalHour.Day, value, FinMinutes, 0);
            }
        }
        public int FinMinutes
        {
            get
            {
                return FinalHour.Minute;
            }
            set
            {
                FinalHour = new DateTime(FinalHour.Year, FinalHour.Month, FinalHour.Day, FinHour, value, 0);
            }
        }
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateDuty(DutyContract obj)
        {
            if (obj.InitialHour > obj.FinalHour)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Final Hour must be greater or equal than Initial Hour");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
