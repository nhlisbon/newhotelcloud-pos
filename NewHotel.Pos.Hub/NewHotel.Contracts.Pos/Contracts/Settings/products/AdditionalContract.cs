﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(AdditionalContract), "ValidateAdditional")]
    public class AdditionalContract:BaseContract
    {
        #region Members
        private Guid _product;
        private Guid _additionalProduct;
        #endregion

        #region Constructor
        public AdditionalContract(Guid id, Guid product, Guid additionalProduct, string productDesc, string productAbre) : base() 
        {
            Id = id;
            Product = product;
            AdditionalProduct = additionalProduct;
            ProductAbre = productAbre;
            ProductDesc = productDesc;
        }
        #endregion

        #region Properties
        [DataMember]
        public Guid Product { get { return _product; } set { Set(ref _product, value, "Product"); } }

        [DataMember]
        public Guid AdditionalProduct { get { return _additionalProduct; } set { Set(ref _additionalProduct, value, "AdditionalProduct"); } }
        
        #endregion

        #region Dummy Properties
        [DataMember]
        public string ProductDesc { get; set; }
        [DataMember]
        public string ProductAbre { get; set; }
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateAdditional(AdditionalContract obj)
        {
            if (obj.Product != new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Product required.");

            if (obj.AdditionalProduct != new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Additional Product required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
