﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ProductBarCodeContract), "ValidateProductBarCode")]
    public class ProductBarCodeContract:BaseContract
    {
        #region Members

        private Guid _baseProduct;
        private string _barCode;

        #endregion

        #region Constructor
        public ProductBarCodeContract()
            : base()
        {
        }

        public ProductBarCodeContract(Guid id, string barCode)
            : base() 
        {
            Id = id;
            BarCode = barCode;
        }

        #endregion
        #region Properties

        [DataMember]
        public string BarCode { get { return _barCode; } set { Set(ref _barCode, value, "BarCode"); } }

        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateProductBarCode(ProductBarCodeContract obj)
        {
            if (obj.BarCode == null)
                return new System.ComponentModel.DataAnnotations.ValidationResult("BarCode required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}