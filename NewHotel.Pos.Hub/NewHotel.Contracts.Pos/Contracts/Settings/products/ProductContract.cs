﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ProductContract), "ValidateProduct")]
    public class ProductContract : BaseProductContract
    {
        #region Members

        private TableType? _tableType;
        private ProductPriceType _priceType;
        private bool _showAditionalProduct;
        private bool _showPreparationsScreen;
        private Guid? _defaultSeparator;
        private decimal _step;
        private bool _print;
        private ARGBColor? _color;
        private int? _cantStock;
        private decimal? _discountPercent;

        #endregion
        #region Constructor

        public ProductContract()
            : base()
        {
            Step = decimal.One;
            PriceType = ProductPriceType.AlwaysRate;
            Additionals = new TypedList<AdditionalContract>();
            Preparations = new TypedList<PreparationsProductContract>();
            PriceRates = new TypedList<ProductPriceRateContract>();
            Tables = new TypedList<TableProductContract>();
            Taxes = new TypedList<ServiceTaxContract>();
            Stands = new TypedList<StandProductsContract>();
            ProductbyDispatchAreas = new TypedList<ProductbyDispatchAreaContract>();
        }

        #endregion
        #region Properties

        [DataMember]
        public TableType? ProductTableType { get { return _tableType; } set { Set(ref _tableType, value, "ProductTableType"); } }
        [DataMember]
        public ProductPriceType PriceType { get { return _priceType; } set { Set(ref _priceType, value, "PriceType"); } }
        [DataMember]
        public bool ShowAditionalProduct { get { return _showAditionalProduct; } set { Set(ref _showAditionalProduct, value, "ShowAditionalProduct"); } }
        [DataMember]
        public bool ShowPreparationsScreen { get { return _showPreparationsScreen; } set { Set(ref _showPreparationsScreen, value, "ShowPreparationsScreen"); } }
        [DataMember]
        public Guid? DefaultSeparator { get { return _defaultSeparator; } set { Set(ref _defaultSeparator, value, "DefaultSeparator"); } }
        [DataMember]
        public bool Print { get { return _print; } set { Set(ref _print, value, "Print"); } }
        [DataMember]
        public decimal Step { get { return _step; } set { Set(ref _step, value, "Step"); } }
        [DataMember]
        public ARGBColor? Color { get { return _color; } set { Set(ref _color, value, "Color"); } }
        [DataMember]
        public int? CantStock { get { return _cantStock; } set { Set(ref _cantStock, value, "CantStock"); } }
        [DataMember]
        public decimal? DiscountPercent { get { return _discountPercent; } set { Set(ref _discountPercent, value, "DiscountPercent"); } }
        
        #endregion
        #region Contracts

        [ReflectionExclude]
        [DataMember]
        public TypedList<AdditionalContract> Additionals { get; set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<PreparationsProductContract> Preparations { get; set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<ProductPriceRateContract> PriceRates { get; set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<TableProductContract> Tables { get; set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<ServiceTaxContract> Taxes { get; set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<StandProductsContract> Stands { get; set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<ProductbyDispatchAreaContract> ProductbyDispatchAreas { get; set; }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public Guid[] AdditionalsIds
        {
            get { return Additionals.Select(x => x.AdditionalProduct).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] PreparationsIds
        {
            get { return Preparations.Select(x => x.Product).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] PriceRtesIds
        {
            get { return PriceRates.Select(x => x.Product).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] TablesIds
        {
            get { return Tables.Select(x => x.Product).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] StandsIds
        {
            get { return Stands.Select(x => x.Product).ToArray(); }
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateProduct(ProductContract obj)
        {
            if (obj.Abbreviation.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Abbreviation cannot be empty.");

            if (!obj.ProductTableType.HasValue && obj.Tables.Count > 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Product has table products, table type cannot be empty");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}