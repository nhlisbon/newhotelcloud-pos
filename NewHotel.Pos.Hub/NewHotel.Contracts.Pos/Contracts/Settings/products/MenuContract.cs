﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class MenuContract : BaseContract
    {
        #region Constructor
        public MenuContract()
            : base()
        {
            Description = new LanguageTranslationContract();
            MenuDetails = new TypedList<MenuDetailsContract>();
        }

        #endregion
        #region Properties

        [DataMember]
        internal LanguageTranslationContract _description;

        [Display(Name = "Description")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        #endregion
        #region Contracts

        [ReflectionExclude]
        [DataMember]
        public TypedList<MenuDetailsContract> MenuDetails { get; set; }

        [ReflectionExclude]
        public Guid[] MenuDetailIds
        {
            get { return MenuDetails.Select(x => (Guid)x.Id).ToArray(); }
        }

        #endregion
    }
}
