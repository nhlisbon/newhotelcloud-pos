﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(TableProductContract), "ValidateTableProduct")]
    public class TableProductContract:BaseContract
    {
        #region Members
        private Guid _product;
        private Guid _componentProduct;
        private decimal _quantity;
        private Guid? _defaultSeparator;
        #endregion

         #region Constructor
        public TableProductContract()
            : base()
        {
        }
        public TableProductContract(Guid id, Guid product, Guid comp, long quantity, string compDesc, Guid? defaultSeparator)
            : base() 
        {
            Id = id;
            Product = product;
            ComponentProduct = comp;
            Quantity = quantity;
            CompDesc = compDesc;
            DefaultSeparator = defaultSeparator;
        }
        #endregion

        #region Properties
        [DataMember]
        public Guid Product { get { return _product; } set { Set(ref _product, value, "Product"); } }
        [DataMember]
        public Guid ComponentProduct { get { return _componentProduct; } set { Set(ref _componentProduct, value, "ComponentProduct"); } }
        [DataMember]
        public decimal Quantity { get { return _quantity; } set { Set(ref _quantity, value, "Quantity"); } }
        [DataMember]
        public Guid? DefaultSeparator { get { return _defaultSeparator; } set { Set(ref _defaultSeparator, value, "DefaultSeparator"); } }

        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateTableProduct(TableProductContract obj)
        {
            if (obj.Product == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Product required.");
            if (obj.ComponentProduct == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Component Product required.");
            if (obj.Quantity < 1)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Quantity must be less or equal 1");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        #region Dummy
        [DataMember]
        public string CompDesc { get; set;  }
        #endregion
    }
}
