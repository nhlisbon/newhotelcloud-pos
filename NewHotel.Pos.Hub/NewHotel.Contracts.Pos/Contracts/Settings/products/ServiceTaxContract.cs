﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PosServiceTaxContract : BaseContract
    {
        #region Members

        private Guid? _taxSchemaId;
        private string _taxSchemaName;
        private Guid? _taxRateId1;
        private string _taxRateDesc1;
        private decimal? _percent1;
        private Guid? _taxRateId2;
        private string _taxRateDesc2;
        private decimal? _percent2;
        private ApplyTax2? _mode2;
        private Guid? _taxRateId3;
        private string _taxRateDesc3;
        private decimal? _percent3;
        private ApplyTax3? _mode3;

        #endregion
        #region Properties

        [DataMember]
        public Guid? TaxSchemaId { get { return _taxSchemaId; } set { Set(ref _taxSchemaId, value, "TaxSchemaId"); } }
        [DataMember]
        public string TaxSchemaName { get { return _taxSchemaName; } set { Set(ref _taxSchemaName, value, "TaxSchemaName"); } }
        [DataMember]
        public Guid? TaxRateId1 { get { return _taxRateId1; } set { Set(ref _taxRateId1, value, "TaxRateId1"); } }
        [DataMember]
        public string TaxRateDesc1 { get { return _taxRateDesc1; } set { Set(ref _taxRateDesc1, value, "TaxRateDesc1"); } }
        [DataMember]
        public decimal? Percent1 { get { return _percent1; } set { Set(ref _percent1, value, "Percent1"); } }
        [DataMember]
        public Guid? TaxRateId2 { get { return _taxRateId2; } set { Set(ref _taxRateId2, value, "TaxRateId2"); } }
        [DataMember]
        public string TaxRateDesc2 { get { return _taxRateDesc2; } set { Set(ref _taxRateDesc2, value, "TaxRateDesc2"); } }
        [DataMember]
        public decimal? Percent2 { get { return _percent2; } set { Set(ref _percent2, value, "Percent2"); } }
        [DataMember]
        public ApplyTax2? Mode2 { get { return _mode2; } set { Set(ref _mode2, value, "Mode2"); } }
        [DataMember]
        public Guid? TaxRateId3 { get { return _taxRateId3; } set { Set(ref _taxRateId3, value, "TaxRateId3"); } }
        [DataMember]
        public string TaxRateDesc3 { get { return _taxRateDesc3; } set { Set(ref _taxRateDesc3, value, "TaxRateDesc3"); } }
        [DataMember]
        public decimal? Percent3 { get { return _percent3; } set { Set(ref _percent3, value, "Percent3"); } }
        [DataMember]
        public ApplyTax3? Mode3 { get { return _mode3; } set { Set(ref _mode3, value, "Mode3"); } }

        #endregion
        #region Constructor

        public PosServiceTaxContract()
            : base() { }

        public PosServiceTaxContract(Guid id, Guid taxSchemaId, string taxSchemaName, Guid trId1, Guid trId2, Guid trId3, ApplyTax2 mode2, ApplyTax3 mode3,
            decimal percent1, decimal percent2, decimal percent3, string taxRate1, string taxRate2, string taxRate3)
            : base()
        {
            Id = id;
            TaxSchemaId = taxSchemaId;
            TaxSchemaName = taxSchemaName;
            TaxRateDesc1 = taxRate1;
            TaxRateDesc2 = taxRate2;
            TaxRateDesc3 = taxRate3;
            TaxRateId1 = trId1;
            TaxRateId1 = trId2;
            TaxRateId1 = trId3;
            Mode2 = mode2;
            Mode3 = mode3;
            Percent1 = percent1;
            Percent2 = percent2;
            Percent3 = percent3;
        }

        #endregion
    }
}
