﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(GroupProductContract), "ValidateGroupProduct")]
    public class GroupProductContract:AppClasifierContract
    {
        #region Members

        private bool _showInTickes;
        private bool _recharge;
        private string _prefix;
        private Guid _serviceGroup;
        private long _serviceCategory;
        private long? _iniRange;
        private long? _finalRange;
        private string _image;
        private bool? _alcoholic;

        #endregion
        #region Properties

        [DataMember]
        public bool ShowInTickets { get { return _showInTickes; } set { Set(ref _showInTickes, value, "ShowInTickets"); } }
        [DataMember]
        public bool Recharge { get { return _recharge; } set { Set(ref _recharge, value, "Recharge"); } }
        [DataMember]
        public Guid ServiceGroup { get { return _serviceGroup; } set { Set(ref _serviceGroup, value, "ServiceGroup"); } }
        [DataMember]
        public long ServiceCategory { get { return _serviceCategory; } set { Set(ref _serviceCategory, value, "ServiceCategory"); } }
        [DataMember]
        public string Prefix { get { return _prefix; } set { Set(ref _prefix, value, "Prefix"); } }
        [DataMember]
        public long? IniRange { get { return _iniRange; } set { Set(ref _iniRange, value, "IniRange"); } }
        [DataMember]
        public long? FinalRange { get { return _finalRange; } set { Set(ref _finalRange, value, "FinalRange"); } }
        [DataMember]
        public string Image { get { return _image; } set { Set(ref _image, value, "Image"); } }
        [DataMember]
        public bool? Alcoholic { get { return _alcoholic; } set { Set(ref _alcoholic, value, "Alcoholic"); } }

        #endregion
        #region Contracts

        [ReflectionExclude]
        [DataMember]
        public TypedList<PreparationsGroupProductContract> Preparations { get; set; }

        #endregion
        #region Extended Properties

        [DataMember]
        public bool Create { get; set; }

        [ReflectionExclude]
        public Guid[] PreparationsIds
        {
            get { return Preparations.Select(x => x.Group).ToArray(); }
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateGroupProduct(GroupProductContract obj)
        {
            if (obj.ServiceGroup == Guid.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Service Group required.");

            if (obj.ServiceCategory == 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Service Category required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
        #region Constructor

        public GroupProductContract()
            : base()
        {
            Preparations = new TypedList<PreparationsGroupProductContract>();
        }

        #endregion
    }
}