﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(AppClasifierContract), "ValidateAppClasifier")]
    public class AppClasifierContract : BaseContract
    {
        #region Members
        
        #endregion

        #region Properties

        [DataMember]
        internal LanguageTranslationContract _description;

        [ReflectionExclude]
        [Display(Name = "Description")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        #endregion

        #region Contracts
        [ReflectionExclude]
        [DataMember]
        public TypedList<ApplicationContract> Applications { get; set; }
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateAppClasifier(AppClasifierContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        #region Constructor
        public AppClasifierContract() : base() 
        {
            Description = new LanguageTranslationContract();
            Applications = new TypedList<ApplicationContract>();
        }
        #endregion

        [ReflectionExclude]
        public long[] ApplicationIds
        {
            get { return Applications.Select(x => x.ApplicationId).ToArray(); }
        }
    }
}
