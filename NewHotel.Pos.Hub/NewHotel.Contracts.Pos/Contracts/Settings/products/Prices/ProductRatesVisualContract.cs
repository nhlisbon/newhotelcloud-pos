﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ProductRatesVisualContract : BaseContract
    {
          #region Constructor
        public ProductRatesVisualContract()
            : base()
        {
            Rates = new TypedList<ProductRatesContract>();
        }
        #endregion

        #region Contracts
        [DataMember]
        public TypedList<ProductRatesContract> Rates { get;set; }
        #endregion
    }
}
