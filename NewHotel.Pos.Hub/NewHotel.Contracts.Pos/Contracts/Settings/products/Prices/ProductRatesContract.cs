﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
     [DataContract]
	[Serializable]
    public class ProductRatesContract:BaseContract
    {
          #region Constructor
        public ProductRatesContract(string description, Guid id)
            : base()
        {
            this.Description = description;
            this.Id = id;
            PricePeriods = new TypedList<PricePeriodProductContract>();
        }
        #endregion

        #region Properties
        [DataMember]
        public string Description { get; set; }
        #endregion

        #region Contracts
        [DataMember]
        public TypedList<PricePeriodProductContract> PricePeriods { get; set; }
        #endregion
    }
}
