﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
     [DataContract]
	[Serializable]
    public class PricePeriodProductContract:BaseContract
    {
          #region Constructor
        public PricePeriodProductContract(DateTime initialDate, DateTime finalDate, decimal price, Guid id, Guid tppe_pk, Guid artg_pk)
            : base()
        {
            this.InitialDate = initialDate;
            this.FinalDate = finalDate;
            this.Price = price;
            this.Id = id;
            this.Period = tppe_pk;
            this.Product = artg_pk;
        }
        public PricePeriodProductContract(DateTime initialDate, DateTime finalDate, Guid tppe_pk)
            : base()
        {
            this.InitialDate = initialDate;
            this.FinalDate = finalDate;
            this.Period = tppe_pk;
        }
        #endregion

        #region Properties
        [DataMember]
        public Guid Period { get; set; }
        [DataMember]
        public Guid Product { get; set; }
        [DataMember]
        public DateTime InitialDate { get; set; }
        [DataMember]
        public DateTime FinalDate { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        #endregion

        #region DummyProperties
        public string PeriodDesc
        {
            get
            {
                return InitialDate.ToShortDateString() + " - " + FinalDate.ToShortDateString();
            }
        }
        #endregion


    }
}
