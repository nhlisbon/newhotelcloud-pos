﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ProductSubFamilyContract), "ValidateSubFamily")]
    public class ProductSubFamilyContract : AppClasifierContract
    {
        #region Members
        private Guid _family;

        #endregion

        #region Properties
        [DataMember]
        public Guid Family { get { return _family; } set { Set(ref _family, value, "Family"); } }

        #endregion
        #region Dummy Properties
        [DataMember]
        public bool Create { get; set ; }
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateSubFamily(ProductSubFamilyContract obj)
        {
            if (obj.Family == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Family required.");
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
