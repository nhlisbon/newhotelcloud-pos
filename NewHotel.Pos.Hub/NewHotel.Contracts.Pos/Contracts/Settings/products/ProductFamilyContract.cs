﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
     [DataContract]
	[Serializable]
     [CustomValidation(typeof(ProductFamilyContract), "ValidateProductFamily")]
    public class ProductFamilyContract: AppClasifierContract
    {
        #region Constructor

         public ProductFamilyContract()
            : base()
        {
            Preparations = new TypedList<PreparationsFamilyProductContract>();
        }

        #endregion
        #region Members

        private Guid _group;
        private string _prefix;
        private long? _iniRange;
        private long? _finalRange;

        #endregion
        #region Properties

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Group required.")]
        public Guid Group { get { return _group; } set { Set(ref _group, value, "Group"); } }
        [DataMember]
        public string Prefix { get { return _prefix; } set { Set(ref _prefix, value, "Prefix"); } }
        [DataMember]
        public long? IniRange { get { return _iniRange; } set { Set(ref _iniRange, value, "IniRange"); } }
        [DataMember]
        public long? FinalRange { get { return _finalRange; } set { Set(ref _finalRange, value, "FinalRange"); } }

        #endregion
        #region Contracts

        [ReflectionExclude]
        [DataMember]
        public TypedList<PreparationsFamilyProductContract> Preparations { get; set; }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateProductFamily(ProductFamilyContract obj)
        {
            if (obj.Group == new Guid() || obj.Group == null)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Group required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
        #region Dummy Properties

        [DataMember]
        public bool Create { get; set ; }

        #endregion

        [ReflectionExclude]
        public Guid[] PreparationsIds
        {
            get { return Preparations.Select(x => x.Family).ToArray(); }
        }     
    }
}
