﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ProductPriceRateContract), "ValidateProductPriceRate")]
    public class ProductPriceRateContract:BaseContract
    {
        #region Constructor
        public ProductPriceRateContract(Guid id, Guid pricePeriod, Guid product, decimal price, string fullPeriod, string productDesc, DateTime iniDatePeriod, DateTime finDatePeriod, string codi, 
                                        Guid? group, Guid? family, Guid? subfamily) 
        {
            this.Id = id;
            this.PricePeriod = pricePeriod;
            this.Product = product;
            this.Price = price;
            this.FullPeriod = fullPeriod;
            this.ProductDesc = productDesc;
            this.InitialDatePer = iniDatePeriod;
            this.FinalDatePer = finDatePeriod;
            this.Code = codi;
            this.Family = family;
            this.Group = group;
            this.SubFamily = subfamily;
        }

        public ProductPriceRateContract()
        {
        }
        #endregion

        #region Members
        private Guid _pricePeriod;
        private Guid _product;
        private decimal _price;
        #endregion

        #region Properties
        [DataMember]
        public Guid PricePeriod { get { return _pricePeriod; } set { Set(ref _pricePeriod, value, "PricePeriod"); } }
        [DataMember]
        public Guid Product { get { return _product; } set { Set(ref _product, value, "Product"); } }
        [DataMember]
        public decimal Price { get { return _price; } set { Set(ref _price, value, "Price"); } }
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateProductPriceRate(ProductPriceRateContract obj)
        {
            if (obj.PricePeriod == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Price Period required.");

            if (obj.Product == null)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Product required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        #region DummyProperties
        [DataMember]
        public string ProductDesc { get; set; }
        [DataMember]
        public string ProductCodi { get; set; }
        [DataMember]
        public string FullPeriod { get; set; }
        [DataMember]
        public DateTime InitialDatePer { get; set; }
        [DataMember]
        public DateTime FinalDatePer { get; set; }
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public Guid? Group { get; set; }
        [DataMember]
        public Guid? Family { get; set; }
        [DataMember]
        public Guid? SubFamily { get; set; }
        #endregion

    }
}
