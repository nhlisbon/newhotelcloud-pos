﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ProductImageContract), "ValidateProductImage")]
    public class ProductImageContract : BaseContract
    {
        #region Members

        private Guid _baseProduct;
        private POSImageType _imageType;
        private string _path;

        #endregion
        #region Constructor

        private ProductImageContract(Guid id)
            : base()
        {
            Id = id;
        }

        public ProductImageContract()
            : this(Guid.NewGuid())
        {
        }

        public ProductImageContract(Guid id, Guid baseProduct, POSImageType imageType, string path, Blob image)
            : this(id) 
        {
            _baseProduct = baseProduct;
            _imageType = imageType;
            _path = path;
            Image = image;
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid BaseProduct { get { return _baseProduct; } set { Set(ref _baseProduct, value, "BaseProduct"); } }
        [DataMember]
        public POSImageType ImageType { get { return _imageType; } set { Set(ref _imageType, value, "ImageType"); } }        
        [DataMember]
        public string Path { get { return _path; } set { Set(ref _path, value, "Path"); } }

        #endregion
        #region DummyProperties

        [DataMember]
        public Blob Image { get; set; }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateProductImage(ProductImageContract obj)
        {
            if (obj.BaseProduct == Guid.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Base Product required.");

            if (obj.Image.Size == 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Image required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
