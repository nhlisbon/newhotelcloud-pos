﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Runtime.Serialization;
//using System.ComponentModel.DataAnnotations;
//using NewHotel.DataAnnotations;

//namespace NewHotel.Contracts
//{
//    [DataContract]
//    [CustomValidation(typeof(AppClasifierContract), "ValidateBaseService")]
//    public class BaseServiceContract : BaseContract
//    {
//        #region Members

//        private string _freeCode;
//        private bool _manualInsercion;
//        private long _valueMB;
//        private string _allowValue;
//        private bool _treatmentTips;
//        private Guid _tipsType;
//        private bool _servInact;
        
//        #endregion

//        #region Properties

//        [DataMember]
//        internal LanguageTranslationContract _description;
//        internal LanguageTranslationContract _abreviation;

//        //[ReflectionExclude]
//        [Display(Name = "Description")]
//        [LanguageTranslationValidation]
//        public LanguageTranslationContract Description
//        {
//            get { return _description; }
//            set { _description = value; }
//        }

//        //[ReflectionExclude]
//        [Display(Name = "Abreviation")]
//        [LanguageTranslationValidation]
//        public LanguageTranslationContract Abreviation
//        {
//            get { return _abreviation; }
//            set { _abreviation = value; }
//        }

//        [DataMember]
//        public string FreeCode { get { return _freeCode; } set { Set(ref _freeCode, value, "FreeCode"); } }

//        [DataMember]
//        public bool ManualInsercion { get { return _manualInsercion; } set { Set(ref _manualInsercion, value, "ManualInsercion"); } }

//        [DataMember]
//        public long ValueMB { get { return _valueMB; } set { Set(ref _valueMB, value, "ValueMB"); } }

//        [DataMember]
//        public string AllowValue { get { return _allowValue; } set { Set(ref _allowValue, value, "AllowValue"); } }

//        [DataMember]
//        public bool TreatmentTips { get { return _treatmentTips; } set { Set(ref _treatmentTips, value, "TreatmentTips"); } }

//        [DataMember]
//        public Guid TipsType { get { return _tipsType; } set { Set(ref _tipsType, value, "TipsType"); } }

//         [DataMember]
//        public bool ServInact { get { return _servInact; } set { Set(ref _servInact, value, "ServInact"); } }

//        #endregion

//        #region Validations

//        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateBaseService(GroupProductContract obj)
//        {
//            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
//        }

//        #endregion
//    }
//}
