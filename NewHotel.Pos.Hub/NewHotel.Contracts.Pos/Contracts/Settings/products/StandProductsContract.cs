﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(StandProductsContract), "ValidateStandProducts")]
    public class StandProductsContract : BaseContract
    {
        #region Members
        private Guid _stand;
        private Guid _product;
        private Guid? _service;
        private long? _favoritesOrder;
        private bool _productInactive;
        #endregion

        #region Constructor
        public StandProductsContract()
            : base()
        {
        }
        public StandProductsContract(Guid id, Guid stand, Guid product, Guid group, Guid? family, Guid? subFamily, Guid? service, string productDesc, string productAbbre,
            string serviceDesc, Blob? defaultImage, double? defaultPrice, Guid? separator, string standDesc, decimal step, string code, bool productInactive, DateTime lastModified)
            : base() 
        {
            Id = id;
            Stand = stand;
            Product = product;
            Service = service;
            ProductDesc = productDesc;
            ProductAbbre = productAbbre;
            ProductInactive = productInactive;
            ServiceDesc = serviceDesc;
            Group = group;
            Family = family;
            SubFamily = subFamily;
            DefaultImage = defaultImage;
            DefaultPrice = defaultPrice;
            DefaultSeparator = separator;
            StandDesc = standDesc;
            Step = step;
            Code = code;
            LastModified = lastModified;

        }
        #endregion

        #region Properties
        [DataMember]
        public Guid Stand { get { return _stand; } set { Set(ref _stand, value, "Stand"); } }
        [DataMember]
        public Guid Product { get { return _product; } set { Set(ref _product, value, "Product"); } }
        [DataMember]
        public Guid? Service { get { return _service; } set { Set(ref _service, value, "Service"); } }
        [DataMember]
        public long? FavoritesOrder { get { return _favoritesOrder; } set { Set(ref _favoritesOrder, value, "FavoritesOrder"); } }
       
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateStandProducts(StandProductsContract obj)
        {
            if (obj.Stand == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Stand required.");

            if (obj.Product == null)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Product required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        #region DummyProperties
        [DataMember]
        public string StandDesc { get; set; }
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string ProductDesc { get; set; }
        [DataMember]
        public string ProductAbbre { get; set; }
        [DataMember]
        public string ServiceDesc { get; set; }
        [DataMember]
        public Guid Group { get; set; }
        [DataMember]
        public Guid? Family { get; set; }
        [DataMember]
        public Guid? SubFamily { get; set; }
        [DataMember]
        public Blob? DefaultImage { get; set; }
        [DataMember]
        public double? DefaultPrice { get; set; }
        [DataMember]
        public Guid? DefaultSeparator { get; set; }
        [DataMember]
        public decimal Step { get; set; }
        [DataMember]
        public bool ProductInactive { get { return _productInactive; } set { Set(ref _productInactive, value, "ProductInactive"); } }
        #endregion
    }

    [DataContract]
	[Serializable]
    public class ProductItemContract : BaseContract
    {
        private bool _isSelected;
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public Guid GroupId { get; set; }
        [DataMember]
        public string Group { get; set; }
        [DataMember]
        public bool IsSelected
        {
            get { return _isSelected; }
            set { Set(ref _isSelected, value, "IsSelected"); }
        }
        [DataMember]
        public Guid FamilyId { get; set; }
        [DataMember]
        public string Family { get; set; }
        [DataMember]
        public string Article { get; set; }
        [DataMember]
        public long ParentId { get; set; }
        [DataMember]
        public long IdProduct { get; set; }
    }
}
