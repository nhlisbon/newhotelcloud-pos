﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(SeparatorContract), "ValidateSeparator")]
    public class SeparatorContract : BaseContract
    {
        #region Members
        private short _impresionOrden;
        private short _paxMax;
        private bool _acumRound;
        private bool _showInTicket;
        #endregion

        #region Properties

        [DataMember]
        internal LanguageTranslationContract _description;

        [ReflectionExclude]
        [Display(Name = "Description")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        public short ImpresionOrden { get { return _impresionOrden; } set { Set(ref _impresionOrden, value, "ImpresionOrden"); } }
        [DataMember]
        public short PaxMax { get { return _paxMax; } set { Set(ref _paxMax, value, "PaxMax"); } }
        [DataMember]
        public bool AcumRound { get { return _acumRound; } set { Set(ref _acumRound, value, "AcumRound"); } }
        [DataMember]
        public bool ShowInTicket { get { return _showInTicket; } set { Set(ref _showInTicket, value, "ShowInTicket"); } }
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateSeparator(SeparatorContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        #region Constructor
        public SeparatorContract()
            : base() 
        {
            Description = new LanguageTranslationContract();
        }
        #endregion
    }
}
