﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(BaseProductContract), "ValidateBaseProduct")]
    public class BaseProductContract : ServiceBaseContract
    {
        #region Constructor

        public BaseProductContract()
            : base()
        {
            Images = new TypedList<ProductImageContract>();
            BarCodes = new TypedList<ProductBarCodeContract>();
        }

        #endregion
        #region Members

        private string _article;
        private Guid? _group;
        private Guid? _family;
        private Guid? _subFamily;

        #endregion
        #region Properties

        [DataMember]
        public string Article { get { return _article; } set { Set(ref _article, value, "Article"); } }
        [DataMember]
        public Guid? Group { get { return _group; } set { Set(ref _group, value, "Group"); } }
        [DataMember]
        public Guid? Family { get { return _family; } set { Set(ref _family, value, "Family"); } }
        [DataMember]
        public Guid? SubFamily { get { return _subFamily; } set { Set(ref _subFamily, value, "SubFamily"); } }

        #endregion
        #region Contracts

        [ReflectionExclude]
        [DataMember]
        public TypedList<ProductImageContract> Images { get; set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<ProductBarCodeContract> BarCodes { get; set; }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateBaseProduct(BaseProductContract obj)
        {
            if (!obj.Group.HasValue || obj.Group.Value == Guid.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Group required.");

            if (!obj.Family.HasValue || obj.Family.Value == Guid.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Family required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        [ReflectionExclude]
        public Guid[] ImagesIds
        {
            get { return Images.Select(x => (Guid)x.Id).ToArray(); }
        }
    }
}