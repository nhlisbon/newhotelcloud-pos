﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class MenuDetailsContract : BaseContract
    {
        [DataMember(Order = 1)]
        internal string _empty;
        [DataMember(Order = 1)]
        internal string _productDescription;
        private Guid? _productId;
        private string _additionalDescription;
        private long _separatorOrder;
        private long _productOrder;

        [DataMember]
        public Guid SeparatorId { get; set; }
        [DataMember]
        public string SeparatorDescription { get; set; }
        [DataMember]
        public long SeparatorOrder { get { return _separatorOrder; } set { Set(ref _separatorOrder, value, "SeparatorOrder"); } }
        [DataMember(Order = 0)]
        public Guid? ProductId
        {
            get { return _productId; }
            set 
            {
                if (Set(ref _productId, value, "ProductId"))
                {
                    _productDescription = null;
                    _additionalDescription = null;
                    NotifyPropertyChanged("Description");
                    NotifyPropertyChanged("ProductDescription");
                    NotifyPropertyChanged("AdditionalDescription");
                }
            }
        }
        [DataMember]
        public long ProductOrder { get { return _productOrder; } set { Set(ref _productOrder, value, "ProductOrder"); } }
        [DataMember(Order = 1)]
        public string AdditionalDescription { get { return _additionalDescription; } set { Set(ref _additionalDescription, value, "AdditionalDescription"); } }

        public string ProductDescription { get { return ProductId.HasValue ? _productDescription : _empty; } }
        public string Description { get { return string.IsNullOrEmpty(_additionalDescription) ? _productDescription : _additionalDescription; } }

        #region Constructor
        
        public MenuDetailsContract()
            : base()
        {
        }

        public MenuDetailsContract(Guid id, Guid separatorId, string separatorDescription, 
            Guid? productId, string productDescription, string additionalDescription, string empty)
            : base() 
        {
            Id = id;
            SeparatorId = separatorId;
            SeparatorDescription = separatorDescription;
            ProductId = productId;
            _productDescription = productDescription;
            _additionalDescription = additionalDescription;
            _empty = empty;
        }

        #endregion
    }
}
