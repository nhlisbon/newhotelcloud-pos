﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(InternalUseContract), "ValidateinternalUse")]
    public class InternalUseContract:AppClasifierContract
    {
        #region Members
        private bool _invitations;
        private bool _state;
        #endregion

        #region Properties
        [DataMember]
        public bool Invitations { get { return _invitations; } set { Set(ref _invitations, value, "Invitations"); } }
        [DataMember]
        public bool State { get { return _state; } set { Set(ref _state, value, "State"); } }

        #endregion

        #region Constructor
        public InternalUseContract()
            : base()
        {
            InternalUseStands = new TypedList<InternalUsexStandContract>();
        }
        #endregion

        #region Contracts
        [ReflectionExclude]
        [DataMember]
        public TypedList<InternalUsexStandContract> InternalUseStands { get; set; }
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateinternalUse(InternalUseContract obj)
        {

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        #region DummyProperties
        [DataMember]
        public bool Create { get; set;}

        #endregion

        [ReflectionExclude]
        public Guid[] InternalUseStandsIds
        {
            get { return InternalUseStands.Select(x => x.InternalUse).ToArray(); }
        }
    }
}
