﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(InternalUsexStandContract), "ValidateinternalUsexStandContract")]
    public class InternalUsexStandContract:BaseContract
    {
        #region Members
        private Guid _internalUse;
        private Guid _stand;
        private double? _mensualLimit;
        private bool _paymentUnlimited;
        private decimal? _paymentPercent;
        #endregion

        #region Constructor
        public InternalUsexStandContract()
            : base()
        {
        }
        public InternalUsexStandContract(Guid id, Guid stand, Guid internalUse,double? mensualLimit, string standDesc, bool paymentUnlimited, decimal? paymentPercent)
            : base() 
        {
            Id = id;
            Stand = stand;
            InternalUse = internalUse;
            MensualLimit = mensualLimit;
            StandDesc = standDesc;
            PaymentUnlimited = paymentUnlimited;
            PaymentPercent = paymentPercent;
        }
        #endregion

        #region Properties
        [DataMember]
        public Guid InternalUse { get { return _internalUse; } set { Set(ref _internalUse, value, "InternalUse"); } }
        [DataMember]
        public Guid Stand { get { return _stand; } set { Set(ref _stand, value, "Stand"); } }
        [DataMember]
        public double? MensualLimit { get { return _mensualLimit; } set { Set(ref _mensualLimit, value, "MensualLimit"); } }
        [DataMember]
        public bool PaymentUnlimited { get { return _paymentUnlimited; } set { Set(ref _paymentUnlimited, value, "PaymentUnlimited"); } }
        [DataMember]
        public decimal? PaymentPercent { get { return _paymentPercent; } set { Set(ref _paymentPercent, value, "PaymentPercent"); } }
        

        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateinternalUsexStandContract(InternalUsexStandContract obj)
        {
            if (obj.InternalUse == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Internal Use required.");
            if (obj.Stand == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Stand required.");
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        #region Dummy
        [DataMember]
        public string StandDesc { get; set; }
        #endregion
    }
}
