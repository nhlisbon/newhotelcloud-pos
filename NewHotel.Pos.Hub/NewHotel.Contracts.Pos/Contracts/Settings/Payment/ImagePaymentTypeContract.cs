﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ImagePaymentTypeContract), "ValidateImagePaymentForm")]
    public class ImagePaymentTypeContract:BaseContract
    {
          #region Members
        private ReceivableType? _tire;
        private Guid? _fore;
        private int _order;
        private string _path;
        #endregion

        #region Constructor
        public ImagePaymentTypeContract()
            : base()
        {
        }
        #endregion

        #region Properties
        [DataMember]
        public ReceivableType? Tire { get { return _tire; } set { Set(ref _tire, value, "Tire"); } }
        [DataMember]
        public Guid? Fore { get { return _fore; } set { Set(ref _fore, value, "Fore"); } }
       
        [DataMember]
        public int Order { get { return _order; } set { Set(ref _order, value, "Order"); } }
        [DataMember]
        public string Path { get { return _path; } set { Set(ref _path, value, "Path"); } }
        #endregion

        #region DummyProperties
        [DataMember]
        public Blob? Image { get; set ; }
        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateImagePaymentForm(ImagePaymentTypeContract obj)
        {
            if (obj.Fore == null)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Payment Form cannot be empty.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        #region DummyProperties
        [DataMember]
        public string ForeDesc { get; set; }
        #endregion
    }
}
