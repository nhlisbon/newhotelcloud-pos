﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(OrderImageByStandContract), "ValidateImageByStand")]
    public class OrderImageByStandContract:BaseContract
    {
         #region Members
        private Guid _fori;
        private Guid _stand;
        private int _order;
        #endregion

        #region Constructor
        public OrderImageByStandContract()
            : base()
        {
        }
        public OrderImageByStandContract(Guid id, Guid fori, Guid stand, int order, Blob? image, string fore)
            : base()
        {
            this.Id = id;
            this.Fori = fori;
            this.Stand = stand;
            this.Order = order;
            this.Image = image;
            this.Fore = fore;
        }
        #endregion

        #region Properties
        [DataMember]
        public Guid Fori { get { return _fori; } set { Set(ref _fori, value, "Fori"); } }
        [DataMember]
        public Guid Stand { get { return _stand; } set { Set(ref _stand, value, "Stand"); } }
        [DataMember]
        public int Order { get { return _order; } set { Set(ref _order, value, "Order"); } }
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateImageByStand(OrderImageByStandContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        #region DummyProperties
        [DataMember]
        public Blob? Image { get; set; }
        [DataMember]
        public string Fore { get; set; }
        #endregion
    }
}
