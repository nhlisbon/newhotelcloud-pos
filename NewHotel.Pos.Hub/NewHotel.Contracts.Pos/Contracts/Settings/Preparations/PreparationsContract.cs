﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(PreparationsContract), "ValidatePreparations")]
    public class PreparationsContract:BaseContract
    {
         #region Members
        
        #endregion

        #region Properties

        [DataMember]
        internal LanguageTranslationContract _description;

        [ReflectionExclude]
        [Display(Name = "Description")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidatePreparations(PreparationsContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        #region Contracts
        [ReflectionExclude]
        public TypedList<PreparationsProductContract> PreparationProducts { get; private set; }
        [ReflectionExclude]
        public TypedList<PreparationsGroupProductContract> PreparationGroupProducts { get; private set; }
        [ReflectionExclude]
        public TypedList<PreparationsFamilyProductContract> PreparationFamilyProducts { get; private set; }
        #endregion

        #region Constructor
        public PreparationsContract() : base() 
        {
            Description = new LanguageTranslationContract();
            PreparationProducts = new TypedList<PreparationsProductContract>();
            PreparationGroupProducts = new TypedList<PreparationsGroupProductContract>();
            PreparationFamilyProducts = new TypedList<PreparationsFamilyProductContract>();
        }
        #endregion

        [ReflectionExclude]
        public Guid[] PreparationProductsIds
        {
            get { return PreparationProducts.Select(x => x.Preparation).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] PreparationGroupProductIds
        {
            get { return PreparationGroupProducts.Select(x => x.Preparation).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] PreparationFamilyProductIds
        {
            get { return PreparationFamilyProducts.Select(x => x.Preparation).ToArray(); }
        }
    }
}
