﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(PreparationsFamilyProductContract), "ValidatePreparationsFamilyProduct")]
    public class PreparationsFamilyProductContract:BaseContract
    {
         #region Members
        private Guid _preparation;
        private Guid _family;
        #endregion

        #region Constructor
        public PreparationsFamilyProductContract()
            : base()
        {
        }
        public PreparationsFamilyProductContract(Guid id, Guid preparation, Guid family, string description)
            : base() 
        {
            Id = id;
            Preparation = preparation;
            Family = family;
            Description = description;
        }
        #endregion

        #region Properties
        [DataMember]
        public Guid Preparation { get { return _preparation; } set { Set(ref _preparation, value, "Preparation"); } }
        [DataMember]
        public Guid Family { get { return _family; } set { Set(ref _family, value, "Family"); } }
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidatePreparationsfamilyProduct(PreparationsFamilyProductContract obj)
        {
            if (obj.Preparation == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Preparation required.");

            if (obj.Family == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Family required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        #region Dummy Properties
        [DataMember]
        public string Description { get; set; }
        #endregion
    }
}
