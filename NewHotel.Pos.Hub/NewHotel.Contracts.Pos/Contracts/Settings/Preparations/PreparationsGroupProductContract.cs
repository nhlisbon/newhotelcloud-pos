﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(PreparationsGroupProductContract), "ValidatePreparationsGroupProduct")]
    public class PreparationsGroupProductContract:BaseContract
    {
        #region Members

        private Guid _preparation;
        private Guid _group;

        #endregion
        #region Constructor

        public PreparationsGroupProductContract()
            : base()
        {
        }

        public PreparationsGroupProductContract(Guid id, Guid preparation, Guid group, string description)
            : base() 
        {
            Id = id;
            Preparation = preparation;
            Group = group;
            Description = description;
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid Preparation { get { return _preparation; } set { Set(ref _preparation, value, "Preparation"); } }
        [DataMember]
        public Guid Group { get { return _group; } set { Set(ref _group, value, "Group"); } }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidatePreparationsGroupProduct(PreparationsGroupProductContract obj)
        {
            if (obj.Preparation == Guid.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Preparation required.");

            if (obj.Group == Guid.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Group required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
        #region Dummy Properties

        [DataMember]
        public string Description { get; set; }

        #endregion
    }
}
