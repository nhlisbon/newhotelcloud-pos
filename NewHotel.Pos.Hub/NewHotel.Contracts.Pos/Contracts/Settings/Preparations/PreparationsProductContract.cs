﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(PreparationsProductContract), "ValidatePreparationsProduct")]
    public class PreparationsProductContract : BaseContract
    {
        #region Members

        private Guid _preparation;
        private Guid _product;

        #endregion
        #region Constructor

        public PreparationsProductContract()
            : base()
        {
        }

        public PreparationsProductContract(Guid id, Guid preparation, Guid product, string description)
            : base() 
        {
            Id = id;
            Preparation = preparation;
            Product = product;
            Description = description;
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid Preparation { get { return _preparation; } set { Set(ref _preparation, value, "Preparation"); } }
        [DataMember]
        public Guid Product { get { return _product; } set { Set(ref _product, value, "Product"); } }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidatePreparationsProduct(PreparationsProductContract obj)
        {
            if (obj.Preparation == Guid.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Preparation required.");

            if (obj.Product == Guid.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Product required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
        #region Dummy Properties

        [DataMember]
        public string Description { get; set; }

        #endregion
    }
}
