﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TablesReservationContract : BaseContract
    {
        public TablesReservationContract() : base() { }
        
        #region Properties

        private string _groupName;
        [DataMember]
        public string GroupName { get { return _groupName; } set { Set(ref _groupName, value, "GroupName"); } }
        [DataMember]
        public Guid? GroupingType { get; set; }
        [DataMember]
        public ConfirmationType GroupConfirmationType { get; set; } = ConfirmationType.EMail;
        [DataMember]
        public ConfirmationState GroupConfirmationState { get; set; } = ConfirmationState.NotRequired;

        #region RESE Contact

        private string _lastName;
        private string _firstName;
        private string _faxNumber;
        private string _phoneNumber;
        private string _companyName;
        private string _countryId;
        private string _eMailAddress;
        [DataMember]
        public string LastName { get { return _lastName; } set { Set(ref _lastName, value, "LastName"); } }
        [DataMember]
        public string FirstName { get { return _firstName; } set { Set(ref _firstName, value, "FirstName"); } }
        [DataMember]
        public string PhoneNumber { get { return _phoneNumber; } set { Set(ref _phoneNumber, value, "PhoneNumber"); } }
        [DataMember]
        public string FaxNumber { get { return _faxNumber; } set { Set(ref _faxNumber, value, "FaxNumber"); } }
        [DataMember]
        public string CompanyName { get { return _companyName; } set { Set(ref _companyName, value, "CompanyName"); } }
        [DataMember]
        public string CountryId { get { return _countryId; } set { Set(ref _countryId, value, "CountryId"); } }
        [DataMember]
        public string EMailAddress { get { return _eMailAddress; } set { Set(ref _eMailAddress, value, "EMailAddress"); } }

        #endregion
        #region External Channel

        private string _externalReservationId;
        private Guid? _externalChannelId;
        [DataMember]
        public Guid? ExternalChannelId { get { return _externalChannelId; } set { Set<Guid?>(ref _externalChannelId, value, "ExternalChannelId"); } }
        [DataMember]
        public string ExternalReservationId { get { return _externalReservationId; } set { Set<string>(ref _externalReservationId, value, "ExternalReservationId"); } }

        #endregion

        private bool _markAsGroupStatistically = false;
        [DataMember]
        public bool MarkAsGroupStatistically { get { return _markAsGroupStatistically; } set { Set(ref _markAsGroupStatistically, value, "MarkAsGroupStatistically"); } }
        private bool _invoiceToGroup = false;
        [DataMember]
        public bool InvoiceToGroup { get { return _invoiceToGroup; } set { Set(ref _invoiceToGroup, value, "InvoiceToGroup"); } }
        [DataMember]
        public DateTime RegistrationDateTime { get; set; }
        private string _observations;
        [DataMember]
        public string Observations { get { return _observations; } set { Set<string>(ref _observations, value, "Observations"); } }
        private DateTime? _deadlineDate;
        [DataMember]
        public DateTime? DeadlineDate { get { return _deadlineDate; } set { Set<DateTime?>(ref _deadlineDate, value, "DeadlineDate"); } }

        [ReflectionExclude]
        public string FullName => ((!string.IsNullOrEmpty(LastName)) ? (LastName.Trim() + ", ") : string.Empty) + ((!string.IsNullOrEmpty(FirstName)) ? FirstName.Trim() : string.Empty);

        #endregion
    }
}
