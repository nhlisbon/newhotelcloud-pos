﻿using System;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.Collections.ObjectModel;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class GuestContract : BaseContract
    {
        [DataMember]
        public Guid? ContactId { get; set; }
        [DataMember]
        public Guid? OccupationLineId { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Room { get; set; }

        [DataMember]
        public int RatePositive { get; set; }
        [DataMember]
        public int RateNegative { get; set; }
        [DataMember]
        public double? RateAverage { get; set; }

        [DataMember]
        public TypedList<ClientPreferenceContract> TableReservationPreferences { get; set; }
        [DataMember]
        public TypedList<ClientDietContract> TableReservationDiets { get; set; }
        [DataMember]
        public TypedList<ClientAttentionContract> TableReservationAttentions { get; set; }
        [DataMember]
        public TypedList<ClientAllergieContract> TableReservationAllergies { get; set; }
        [DataMember]
        public TypedList<BookingTableIncidentDto> Incidents { get; set; }

        public GuestContract()
        {
            TableReservationPreferences = new TypedList<ClientPreferenceContract>();
            TableReservationDiets = new TypedList<ClientDietContract>();
            TableReservationAttentions = new TypedList<ClientAttentionContract>();
            TableReservationAllergies = new TypedList<ClientAllergieContract>();
            Incidents = new TypedList<BookingTableIncidentDto>();
        }

        [ReflectionExclude]
        public string Preferences => string.Join(", ", TableReservationPreferences.Select(x => x.PreferenceDesc));

        [ReflectionExclude]
        public string Diets => string.Join(", ", TableReservationDiets.Select(x => x.DietDescription));

        [ReflectionExclude]
        public string Attentions => string.Join(", ", TableReservationAttentions.Select(x => x.AttentionTypeDesc));

        [ReflectionExclude]
        public string Allergies => string.Join(", ", TableReservationAllergies.Select(x => x.AllergyDescription));

        [ReflectionExclude]
        public Guid[] TableReservationPreferencesIds => TableReservationPreferences?.Select(x => x.PreferenceId).ToArray();

        [ReflectionExclude]
        public Guid[] TableReservationAllergiesIds => TableReservationAllergies?.Select(x => x.AllergyId).ToArray();

        [ReflectionExclude]
        public Guid[] TableReservationDietsIds => TableReservationDiets?.Select(x => x.DietId).ToArray();

        [DataMember]
        public string UncommonAllergies { get; set; }

        [DataMember]
        public string SegmentOperations { get; set; }
    }

    [DataContract]
    [Serializable]
    [CustomValidation(typeof(TablesReservationLineContract), "ValidateTablesReservationLine")]
    public class TablesReservationLineContract : BaseContract
    {
        #region Members

        private DateTime _creationDate;
        private DateTime _arrivalDate;
        private DateTime? _arrivalTimeReal;
        private DateTime _arrivalTime;
        private DateTime? _departureTimeReal;
        private DateTime _departureTimeEstimated;
        private Guid _reservationId;
        private TablesReservationContract _reservationContract;
        private long? _reservationNumber;
        private string _documentSerie;
        private string _phoneReference;
        private string _emailReference;
        private Guid? _contactId;
        private Guid? _stayId;
        private string _reservationLineComments;
        private string _vehicleRegistration;
        private string _voucher;
        private bool _taxIncluded;
        private Guid? _taxSchemaId;
        private Guid? _marketOriginId;
        private Guid? _marketSegmentId;
        private ReservationConfirmState _confirmationStatus = ReservationConfirmState.WaitingList;
        private Guid? _creditCardId;
        private short _paxsAdults;
        private short _paxsChildren;
        private short _paxsBabies;
        private Guid? _stand;
        private Guid? _saloon;
        private Guid? _table;
        private Guid? _tablesGroup;
        private bool _lock;
        private bool? _smoke;
        private string _tablesDescription = null;
        private string _quickInfo = "000000000000";
        private ObservableCollection<TablesReservationPeriodRecord> _possibleTablesReservationPeriods;
        private ObservableCollection<SaloonRecord> _possibleSaloons;
        private TablesReservationPeriodRecord _selectedReservationPeriod;
        private CreditCardContract _creditCard;

        private TypedList<TableReservationTableRelationContract> _tables;
        private TypedList<TableReservationPreferenceContract> _tableReservationPreferences;
        private TypedList<TableReservationDietContract> _tableReservationDiets;
        private TypedList<TableReservationAttentionContract> _tableReservationAttentions;
        private TypedList<TableReservationAllergieContract> _tableReservationAllergies;

        #endregion
        #region Constructors

        public TablesReservationLineContract()
        {
            _tables = new TypedList<TableReservationTableRelationContract>();
            _tableReservationAllergies = new TypedList<TableReservationAllergieContract>();
            _tableReservationDiets = new TypedList<TableReservationDietContract>();
            _tableReservationPreferences = new TypedList<TableReservationPreferenceContract>();
            _tableReservationAttentions = new TypedList<TableReservationAttentionContract>();
        }

        public TablesReservationLineContract(long? reservationNumber)
            : this()
        {
            _reservationNumber = reservationNumber;
        }

        #endregion
        #region Properties

        #endregion
        #region Creation, Arrival, Departure Dates

        [DataMember]
        public DateTime CreationDate { get { return _creationDate; } set { Set(ref _creationDate, value, "CreationDate"); } }

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Arrival date required.")]
        public DateTime ArrivalDate
        {
            get { return _arrivalDate; }
            set
            {
                var oldArrivalDate = _arrivalDate;
                Set(ref _arrivalDate, value, "ArrivalDate");
            }
        }

        [DataMember]
        public DateTime? ArrivalTimeReal { get { return _arrivalTimeReal; } set { Set(ref _arrivalTimeReal, value, "ArrivalTimeReal"); } }
        [DataMember]
        public DateTime ArrivalTime { get { return _arrivalTime; } set { Set(ref _arrivalTime, value, "ArrivalTime"); } }

        [DataMember]
        public DateTime? DepartureTimeReal { get { return _departureTimeReal; } set { Set(ref _departureTimeReal, value, "DepartureTimeReal"); } }
        [DataMember]
        public DateTime DepartureTimeEstimated { get { return _departureTimeEstimated; } set { Set(ref _departureTimeEstimated, value, "DepartureTimeEstimated"); } }

        #endregion
        #region Reservation

        [DataMember]
        public Guid ReservationId { get { return _reservationId; } set { _reservationId = value; } }

        [ReflectionExclude]
        [DataMember]
        public TablesReservationContract ReservationContract
        {
            get { return _reservationContract; }
            set
            {
                _reservationContract = value;
                if (_reservationContract != null)
                    ReservationId = (Guid)_reservationContract.Id;
            }
        }

        #endregion
        #region Booking Nº

        [DataMember]
        public long? ReservationNumber
        {
            get { return _reservationNumber; }
            internal set
            {
                if (Set(ref _reservationNumber, value, "ReservationNumber"))
                    NotifyPropertyChanged("SerieName");
            }
        }

        [DataMember]
        public string DocumentSerie
        {
            get { return _documentSerie; }
            set
            {
                if (Set(ref _documentSerie, value, "DocumentSerie"))
                    NotifyPropertyChanged("SerieName");
            }
        }

        [ReflectionExclude]
        public string SerieName
        {
            get
            {
                var serieName = string.Empty;
                if (ReservationNumber.HasValue)
                    serieName += ReservationNumber.Value.ToString();
                if (!string.IsNullOrEmpty(DocumentSerie))
                    serieName += $"/{DocumentSerie}";

                return serieName;
            }
        }

        #endregion
        #region Status (Reserved, Checked In, Checked Out, Cancelled)

        private ReservationState _status = ReservationState.Reserved;
        private ReservationType _reservationType = ReservationType.InDesk;
        private string _reservationHolder;
        private string _reservationCountry;


        [DataMember]
        public ReservationState Status { get { return _status; } set { Set(ref _status, value, "Status"); } }
        [DataMember]
        public string StatusTranslation { get; set; }

        #endregion
        #region Type (In Desk, Company, Owner)

        [DataMember]
        public ReservationType ReservationType
        {
            get { return _reservationType; }
            set { Set(ref _reservationType, value, "ReservationType"); }
        }

        #endregion
        #region Main Guest, Country

        [DataMember]
        public string ReservationHolder { get { return _reservationHolder; } set { Set(ref _reservationHolder, value, "ReservationHolder"); } }
        [DataMember]
        public string ReservationCountry { get { return _reservationCountry; } set { Set(ref _reservationCountry, value, "ReservationCountry"); } }
        [DataMember]
        public string PhoneReference { get { return _phoneReference; } set { Set(ref _phoneReference, value, "PhoneReference"); } }
        [DataMember]
        public string EmailReference { get { return _emailReference; } set { Set(ref _emailReference, value, "EmailReference"); } }

        #region Contact Info

        [DataMember]
        public Guid? ContactId { get { return _contactId; } set { Set(ref _contactId, value, "ContactId"); } }
        [DataMember]
        public bool IsCardex { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Email { get; set; }

        #endregion

        [DataMember]
        public Guid? StayId { get { return _stayId; } set { Set(ref _stayId, value, "StayId"); } }
        [DataMember]
        public string ReservationLineComments { get { return _reservationLineComments; } set { Set(ref _reservationLineComments, value, "ReservationLineComments"); } }
        [DataMember]
        public string VehicleRegistration { get { return _vehicleRegistration; } set { Set(ref _vehicleRegistration, value, "VehicleRegistration"); } }
        [DataMember]
        public string Voucher { get { return _voucher; } set { Set(ref _voucher, value, "Voucher"); } }
        [DataMember]
        public bool TaxIncluded { get { return _taxIncluded; } set { Set(ref _taxIncluded, value, "TaxIncluded"); } }
        [DataMember]
        public Guid? TaxSchemaId { get { return _taxSchemaId; } set { Set(ref _taxSchemaId, value, "TaxSchemaId"); } }

        #endregion
        #region Market

        [DataMember]
        public Guid? MarketOriginId { get { return _marketOriginId; } set { Set(ref _marketOriginId, value, "MarketOriginId"); } }
        [DataMember]
        public Guid? MarketSegmentId { get { return _marketSegmentId; } set { Set(ref _marketSegmentId, value, "MarketSegmentId"); } }

        #endregion
        #region Confirmation Status (Confirmed, Waiting List, Attempt)

        [DataMember]
        public ReservationConfirmState ConfirmationStatus
        {
            get { return _confirmationStatus; }
            set { Set(ref _confirmationStatus, value, "ConfirmationStatus"); }
        }

        [DataMember]
        public bool Confirmed
        {
            get { return _confirmationStatus == ReservationConfirmState.Confirmed; }
            set { _confirmationStatus = value ? ReservationConfirmState.Confirmed : ReservationConfirmState.WaitingList; }
        }

        #endregion
        #region Credit Card

        [DataMember]
        public Guid? CreditCardId { get { return _creditCardId; } set { Set(ref _creditCardId, value, "CreditCardId"); } }

        [ReflectionExclude]
        public CreditCardContract CreditCard
        {
            get { return _creditCard; }
            set { Set(ref _creditCard, value, "CreditCard"); }
        }

        #endregion
        #region Paxs

        [DataMember]
        public short PaxsAdults { get { return _paxsAdults; } set { Set(ref _paxsAdults, value, "PaxsAdults"); } }
        [DataMember]
        public short PaxsChildren { get { return _paxsChildren; } set { Set(ref _paxsChildren, value, "PaxsChildren"); } }
        [DataMember]
        public short PaxsBabies { get { return _paxsBabies; } set { Set(ref _paxsBabies, value, "PaxsBabies"); } }

        #endregion
        #region Stand, Saloon, Table, Group of Tables

        [DataMember]
        public Guid? Stand { get { return _stand; } set { Set(ref _stand, value, "Stand"); } }
        [DataMember]
        public Guid? Saloon { get { return _saloon; } set { Set(ref _saloon, value, "Saloon"); } }

        /// <summary>
        /// Sin Usar, cambiado a relacion *->* con TNHT_TRTA
        /// </summary>
        [DataMember]
        public Guid? Table { get { return _table; } set { Set(ref _table, value, "Table"); } }

        /// <summary>
        /// Sin Usar, quitado por completo, usando la relacion *->* con TNHT_TRTA
        /// </summary>
        [DataMember]
        public Guid? TablesGroup { get { return _tablesGroup; } set { Set(ref _tablesGroup, value, "TablesGroup"); } }

        [DataMember]
        [ReflectionExclude]
        public TypedList<TableReservationTableRelationContract> Tables { get { return _tables; } set { _tables = value; } }

        [ReflectionExclude]
        public Guid[] TablesIds { get { return Tables != null ? Tables.Select(x => x.MesaId).ToArray() : null; } }

        public bool IsGroupTables { get { return TablesIds != null ? TablesIds.Length > 1 : false; } }

        #endregion
        #region Lock, Smoke

        [DataMember]
        public bool Lock { get { return _lock; } set { Set(ref _lock, value, "Lock"); } }
        [DataMember]
        public bool? Smoke { get { return _smoke; } set { Set(ref _smoke, value, "Smoke"); } }

        #endregion
        #region Preferences, Diets, Attentions, Allergies

        [DataMember]
        [ReflectionExclude]
        public TypedList<TableReservationPreferenceContract> TableReservationPreferences
        {
            get { return _tableReservationPreferences; }
            set { _tableReservationPreferences = value; }
        }

        [DataMember]
        [ReflectionExclude]
        public TypedList<TableReservationDietContract> TableReservationDiets
        {
            get { return _tableReservationDiets; }
            set { _tableReservationDiets = value; }
        }

        [DataMember]
        [ReflectionExclude]
        public TypedList<TableReservationAttentionContract> TableReservationAttentions
        {
            get { return _tableReservationAttentions; }
            set { _tableReservationAttentions = value; }
        }

        [DataMember]
        [ReflectionExclude]
        public TypedList<TableReservationAllergieContract> TableReservationAllergies
        {
            get { return _tableReservationAllergies; }
            set { _tableReservationAllergies = value; }
        }

        [DataMember]
        public string UncommonAllergies { get; set; }

        [DataMember]
        public DateTime WorkDate { get; set; }

        [DataMember]
        public string TablesDescription
        {
            get { return _tablesDescription; } set { Set(ref _tablesDescription, value, "TablesDescription"); }
        }

        [DataMember]
        public string QuickInfo
        {
            get { return _quickInfo; } set { Set(ref _quickInfo, value, "QuickInfo"); }
        }

        #endregion
        #region List of Guests

        [DataMember]
        public TypedList<GuestContract> OccupationGuests { get; set; }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public string Preferences => string.Join(", ", TableReservationPreferences.Select(x => x.PreferenceDesc));

        [ReflectionExclude]
        public string Diets => string.Join(", ", TableReservationDiets.Select(x => x.DietDesc));

        [ReflectionExclude]
        public string Attentions => string.Join(", ", TableReservationAttentions.Select(x => x.AttentionDesc));

        [ReflectionExclude]
        public string Allergies => string.Join(", ", TableReservationAllergies.Select(x => x.AllergyDescription));

        [ReflectionExclude]
        public ObservableCollection<TablesReservationPeriodRecord> PossibleTablesReservationPeriods
        {
            get { return _possibleTablesReservationPeriods; } set { Set(ref _possibleTablesReservationPeriods, value, "PossibleTablesReservationPeriods"); }
        }

        [ReflectionExclude]
        public ObservableCollection<SaloonRecord> PossibleSaloons
        {
            get { return _possibleSaloons; } set { Set(ref _possibleSaloons, value, "PossibleSaloons"); }
        }

        [ReflectionExclude]
        public TablesReservationPeriodRecord SelectedReservationPeriod
        {
            get { return _selectedReservationPeriod; }
            set
            {
                if (Set(ref _selectedReservationPeriod, value, "SelectedReservationPeriod"))
                {
                    if (value != null)
                    {
                        ArrivalTime = value.InitialDateTime;
                        DepartureTimeEstimated = value.FinalDateTime;
                    }
                }
            }
        }

        [ReflectionExclude]
        public Guid[] TableReservationPreferencesIds
        {
            get { return TableReservationPreferences != null ? TableReservationPreferences.Select(x => x.PreferenceId).ToArray() : null; }
        }

        [ReflectionExclude]
        public Guid[] TableReservationAllergiesIds
        {
            get { return TableReservationAllergies != null ? TableReservationAllergies.Select(x => x.AllergyId).ToArray() : null; }
        }

        [ReflectionExclude]
        public Guid[] TableReservationDietsIds
        {
            get { return TableReservationDiets != null ? TableReservationDiets.Select(x => x.DietId).ToArray() : null; }
        }

        #region QuickInfo

        [ReflectionExclude]
        public bool Vegetarian
        {
            get { return QuickInfo[(int)TablesReservationQuickInfo.Vegetarian] == '1'; }
            set
            {
                char[] arr = null;
                (arr = QuickInfo.ToCharArray())[(int)TablesReservationQuickInfo.Vegetarian] = value ? '1' : '0';
                QuickInfo = new string(arr);
            }
        }

        [ReflectionExclude]
        public bool GlutenFree
        {
            get { return QuickInfo[(int)TablesReservationQuickInfo.GlutenFree] == '1'; }
            set
            {
                char[] arr = null;
                (arr = QuickInfo.ToCharArray())[(int)TablesReservationQuickInfo.GlutenFree] = value ? '1' : '0';
                QuickInfo = new string(arr);
            }
        }

        [ReflectionExclude]
        public bool VIP
        {
            get { return QuickInfo[(int)TablesReservationQuickInfo.VIP] == '1'; }
            set
            {
                char[] arr = null;
                (arr = QuickInfo.ToCharArray())[(int)TablesReservationQuickInfo.VIP] = value ? '1' : '0';
                QuickInfo = new string(arr);
            }
        }

        [ReflectionExclude]
        public bool Vegan
        {
            get { return QuickInfo[(int)TablesReservationQuickInfo.Vegan] == '1'; }
            set
            {
                char[] arr = null;
                (arr = QuickInfo.ToCharArray())[(int)TablesReservationQuickInfo.Vegan] = value ? '1' : '0';
                QuickInfo = new string(arr);
            }
        }

        [ReflectionExclude]
        public bool Disabled
        {
            get { return QuickInfo[(int)TablesReservationQuickInfo.Disabled] == '1'; }
            set
            {
                char[] arr = null;
                (arr = QuickInfo.ToCharArray())[(int)TablesReservationQuickInfo.Disabled] = value ? '1' : '0';
                QuickInfo = new string(arr);
            }
        }

        [ReflectionExclude]
        public bool Bigspender
        {
            get { return QuickInfo[(int)TablesReservationQuickInfo.Bigspender] == '1'; }
            set
            {
                char[] arr = null;
                (arr = QuickInfo.ToCharArray())[(int)TablesReservationQuickInfo.Bigspender] = value ? '1' : '0';
                QuickInfo = new string(arr);
            }
        }

        [ReflectionExclude]
        public bool NutAllergy
        {
            get { return QuickInfo[(int)TablesReservationQuickInfo.NutAllergy] == '1'; }
            set
            {
                char[] arr = null;
                (arr = QuickInfo.ToCharArray())[(int)TablesReservationQuickInfo.NutAllergy] = value ? '1' : '0';
                QuickInfo = new string(arr);
            }
        }

        [ReflectionExclude]
        public bool WheelChair
        {
            get { return QuickInfo[(int)TablesReservationQuickInfo.WheelChair] == '1'; }
            set
            {
                char[] arr = null;
                (arr = QuickInfo.ToCharArray())[(int)TablesReservationQuickInfo.WheelChair] = value ? '1' : '0';
                QuickInfo = new string(arr);
            }
        }

        [ReflectionExclude]
        public bool FriendOwner
        {
            get { return QuickInfo[(int)TablesReservationQuickInfo.FriendOwner] == '1'; }
            set
            {
                char[] arr = null;
                (arr = QuickInfo.ToCharArray())[(int)TablesReservationQuickInfo.FriendOwner] = value ? '1' : '0';
                QuickInfo = new string(arr);
            }
        }

        #endregion

        [ReflectionExclude]
        public bool IsReserved
        {
            get { return Status == ReservationState.Reserved; }
        }

        [ReflectionExclude]
        public bool IsCheckin
        {
            get { return Status == ReservationState.CheckIn; }
        }

        [ReflectionExclude]
        public int TotalPaxs
        {
            get { return PaxsAdults + PaxsChildren + PaxsBabies; }
        }

        #endregion 
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateTablesReservationLine(TablesReservationLineContract obj)
        {
            // TODO Implement traductions
            if (obj.Status == ReservationState.Reserved)
            {
                if (obj.ArrivalDate < obj.WorkDate)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Arrival Date takes place before working date");
            }

            // market segment && origin != null
            if (!obj.MarketOriginId.HasValue || !obj.MarketSegmentId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Market Segment/Origin cannot be empty.");
            // country != null
            if (string.IsNullOrEmpty(obj.ReservationCountry))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Reservation Country cannot be empty.");
            // tax schema != null
            if (!obj.TaxSchemaId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Tax Schema cannot be empty.");

            if (string.IsNullOrEmpty(obj.ReservationHolder))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Reservation holder cannot be empty.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class BookingTableIncidentDto
    {
        [DataMember]
        public DateTime? RegistrationDate { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string DetailComment { get; set; }
        [DataMember]
        public short? RateNumber { get; set; }
        [DataMember]
        public bool? Signal { get; set; }

        public BookingTableIncidentDto(string user, string comment, short? rateNumber, DateTime? registrationDate, bool? signal)
        {
            UserName = user;
            DetailComment = comment;
            RateNumber = rateNumber;
            RegistrationDate = registrationDate;
            Signal = signal;
        }
    }
}