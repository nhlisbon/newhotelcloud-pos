﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class TableReservationAttentionContract : BaseContract
    {
        [DataMember]
        public Guid TableReservationId { get; set; }

        [DataMember]
        public Guid AttentionId { get; set; }

        [DataMember]
        public string AttentionDesc { get; set; }

        public TableReservationAttentionContract()
            : base() { }

        public TableReservationAttentionContract(Guid id, Guid tableReservationId, Guid attentionId, string attentionDesc)
            : base()
        {
            Id = id;
            TableReservationId = tableReservationId;
            AttentionId = attentionId;
            AttentionDesc = attentionDesc;
        }
    }
}