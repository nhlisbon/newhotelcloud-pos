﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TablesReservationPeriodContract : BaseContract
    {
        #region Members
        private string _days = "0000000";
        private DateTime _initialTime;
        private DateTime _finalTime;
        private decimal? _maxPaxsPercent;
        private short? _maxPaxs;
        private int? _maxReservations;
        private Guid _stand;
        private bool _inactive;

        #endregion

        #region Constructor
        public TablesReservationPeriodContract()
            : base()
        {
            Saloons = new TypedList<TablesReservationPeriodSaloonContract>();
        }
        public TablesReservationPeriodContract(Guid id, string days, DateTime initialTime, DateTime finalTime, decimal? maxPaxsPercent, short? maxPaxs, Guid stand, bool inactive)
            : base()
        {
            Id = id;
            _days = days;
            _initialTime = initialTime;
            _finalTime = finalTime;
            _maxPaxsPercent = maxPaxsPercent;
            _maxPaxs = maxPaxs;
            _stand = stand;
            _inactive = inactive;

            Saloons = new TypedList<TablesReservationPeriodSaloonContract>();
        }

        #endregion

        #region Properties

        [DataMember]
        public string Days { get { return _days; } set { Set(ref _days, value, "Days"); } }

        [DataMember]
        public DateTime InitialTime { get { return _initialTime; } set { Set(ref _initialTime, value, "InitialTime"); } }

        [DataMember]
        public DateTime FinalTime { get { return _finalTime; } set { Set(ref _finalTime, value, "FinalTime"); } }

        [DataMember]
        public decimal? MaxPaxsPercent { get { return _maxPaxsPercent; } set { Set(ref _maxPaxsPercent, value, "MaxPaxsPercent"); } }

        [DataMember]
        public short? MaxPaxs { get { return _maxPaxs; } set { Set(ref _maxPaxs, value, "MaxPaxs"); } }

        [DataMember]
        public int? MaxReservations { get { return _maxReservations; } set { Set(ref _maxReservations, value, "MaxReservations"); } }

        [DataMember]
        public Guid Stand { get { return _stand; } set { Set(ref _stand, value, "Stand"); } }

        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }


        #endregion

        #region Contracts

        [ReflectionExclude]
        [DataMember]
        public TypedList<TablesReservationPeriodSaloonContract> Saloons { get; set; }

        #endregion

        [ReflectionExclude]
        public Guid[] SaloonsIds
        {
            get { return Saloons.Select(x => (Guid)x.Saloon).ToArray(); }
        }

        #region Dummy

        //Days
        public bool Sunday
        {
            get { return Days[0] == '1'; }
            set
            {
                string temp = "";
                if (value) temp += '1';
                else temp += '0';

                temp += Days[1];
                temp += Days[2];
                temp += Days[3];
                temp += Days[4];
                temp += Days[5];
                temp += Days[6];

                Days = temp;
            }
        }
        public bool Monday
        {
            get { return Days[1] == '1'; }
            set
            {
                string temp = "";
                temp += Days[0];
                if (value) temp += '1';
                else temp += '0';
                temp += Days[2];
                temp += Days[3];
                temp += Days[4];
                temp += Days[5];
                temp += Days[6];

                Days = temp;
            }
        }
        public bool Tuesday
        {
            get { return Days[2] == '1'; }
            set
            {
                string temp = "";
                temp += Days[0];
                temp += Days[1];
                if (value) temp += '1';
                else temp += '0';
                temp += Days[3];
                temp += Days[4];
                temp += Days[5];
                temp += Days[6];

                Days = temp;
            }
        }
        public bool Wednesday
        {
            get { return Days[3] == '1'; }
            set
            {
                string temp = "";
                temp += Days[0];
                temp += Days[1];
                temp += Days[2];
                if (value) temp += '1';
                else temp += '0';
                temp += Days[4];
                temp += Days[5];
                temp += Days[6];

                Days = temp;

            }
        }
        public bool Thursday
        {
            get { return Days[4] == '1'; }
            set
            {
                string temp = "";
                temp += Days[0];
                temp += Days[1];
                temp += Days[2];
                temp += Days[3];
                if (value) temp += '1';
                else temp += '0';
                temp += Days[5];
                temp += Days[6];

                Days = temp;
            }
        }
        public bool Friday
        {
            get { return Days[5] == '1'; }
            set
            {
                string temp = "";
                temp += Days[0];
                temp += Days[1];
                temp += Days[2];
                temp += Days[3];
                temp += Days[4];
                if (value) temp += '1';
                else temp += '0';
                temp += Days[6];

                Days = temp;
            }
        }
        public bool Saturday
        {
            get { return Days[6] == '1'; }
            set
            {
                string temp = "";
                temp += Days[0];
                temp += Days[1];
                temp += Days[2];
                temp += Days[3];
                temp += Days[4];
                temp += Days[5];
                if (value) temp += '1';
                else temp += '0';

                Days = temp;
            }
        }

        #endregion

        [DataMember]
        public bool IsNew { get; set; }
        public bool CreateIntervals { get; set; }
        public int IntervalMinutes { get; set; }
        public int DurationMinutes { get; set; }
    }
}
