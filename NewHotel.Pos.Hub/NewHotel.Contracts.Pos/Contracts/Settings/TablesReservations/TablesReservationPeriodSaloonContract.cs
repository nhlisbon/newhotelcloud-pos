﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(TablesReservationPeriodSaloonContract), "ValidateSaloonStand")]
    public class TablesReservationPeriodSaloonContract:BaseContract
    {
        #region Members
        private Guid _saloon;
        private Guid _tablesReservationPeriod;
        #endregion

        #region Constructor
        public TablesReservationPeriodSaloonContract()
            : base()
        {
        }
        public TablesReservationPeriodSaloonContract(Guid id, Guid tablesReservationPeriod, Guid saloon, string saloonDesc)
            : base() 
        {
            Id = id;
            Saloon = saloon;
            SaloonDesc = saloonDesc;
            TablesReservationPeriod = tablesReservationPeriod;
        }
        #endregion

        #region Properties
        
        [DataMember]
        public Guid Saloon { get { return _saloon; } set { Set(ref _saloon, value, "Saloon"); } }
        [DataMember]
        public Guid TablesReservationPeriod { get { return _tablesReservationPeriod; } set { Set(ref _tablesReservationPeriod, value, "TablesReservationPeriod"); } }
        #endregion

        #region DummyProperties

        [DataMember]
        public string SaloonDesc { get; set; }

        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateSaloonStand(SaloonStandContract obj)
        {
            if (obj.Saloon == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Saloon required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
