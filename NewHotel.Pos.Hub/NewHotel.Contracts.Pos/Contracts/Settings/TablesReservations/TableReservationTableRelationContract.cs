﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class TableReservationTableRelationContract : BaseContract
    {
        #region Members

        private Guid _mesaId;
        private int _mesaPaxs;

        #endregion
        #region Constructors

        public TableReservationTableRelationContract()
            : base()
        {
        }

        public TableReservationTableRelationContract(Guid id, Guid mesaId, int mesaPaxs = 0)
            : this()
        {
            Id = id;
            MesaId = mesaId;
            MesaPaxs = mesaPaxs;
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid MesaId { get { return _mesaId; } set { Set(ref _mesaId, value, "MesaId"); } }
        [DataMember]
        public int MesaPaxs { get { return _mesaPaxs; } set { Set(ref _mesaPaxs, value, "MesaPaxs"); } }

        #endregion
    }
}