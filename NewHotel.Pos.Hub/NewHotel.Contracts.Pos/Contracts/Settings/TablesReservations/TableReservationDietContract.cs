﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class TableReservationDietContract : BaseContract
    {
        [DataMember]
        public Guid TableReservationId { get; set; }

        [DataMember]
        public Guid DietId { get; set; }

        [DataMember]
        public string DietDesc { get; set; }

        public TableReservationDietContract()
            : base() { }

        public TableReservationDietContract(Guid id, Guid tableReservationId, Guid dietId, string dietDesc)
            : base()
        {
            Id = id;
            TableReservationId = tableReservationId;
            DietId = dietId;
            DietDesc = dietDesc;
        }
    }
}