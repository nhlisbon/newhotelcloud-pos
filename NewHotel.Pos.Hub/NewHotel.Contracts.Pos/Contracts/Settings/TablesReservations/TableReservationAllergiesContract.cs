﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class TableReservationAllergieContract : BaseContract
    {
        [DataMember]
        public Guid TableReservationId { get; set; }

        [DataMember]
        public Guid AllergyId { get; set; }

        [DataMember]
        public string AllergyDescription { get; set; }

        public TableReservationAllergieContract()
            : base() { }

        public TableReservationAllergieContract(Guid id, Guid tableReservationId, Guid allergiesId, string alllergyDesc)
            : base()
        {
            Id = id;
            TableReservationId = tableReservationId;
            AllergyId = allergiesId;
            AllergyDescription = alllergyDesc;
        }
    }
}