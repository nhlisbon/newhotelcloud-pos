﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class TableReservationPreferenceContract : BaseContract
    {
        [DataMember]
        public Guid TableReservationId { get; set; }

        [DataMember]
        public Guid PreferenceId { get; set; }

        [DataMember]
        public string PreferenceDesc { get; set; }

        public TableReservationPreferenceContract() : base() { }

        public TableReservationPreferenceContract(Guid id, Guid tableReservationId, Guid preferenceId, string preferenceDesc)
            : base()
        {
            Id = id;
            TableReservationId = tableReservationId;
            PreferenceId = preferenceId;
            PreferenceDesc = preferenceDesc;
        }
    }
}