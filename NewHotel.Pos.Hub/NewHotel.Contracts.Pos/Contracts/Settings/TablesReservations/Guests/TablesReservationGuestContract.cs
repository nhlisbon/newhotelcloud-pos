﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(TablesReservationGuestContract), "ValidateTablesReservationGuest")]
    public class TablesReservationGuestContract : BaseGuestContract
    {
        #region Constructor

        public TablesReservationGuestContract(DocumentSign documentSiginatureType, string fullNameOrder, DateTime today)
			: base(documentSiginatureType, fullNameOrder, today) 
        {
        }

        #endregion
        #region Properties

        private bool _isHolder;
        private Guid? _guestCategoryId;
        private Guid? _title;
        private string _middleName;
        
        [DataMember]
        public bool IsHolder { get { return _isHolder; } set { Set(ref _isHolder, value, "IsHolder"); } }
        [DataMember]
        public Guid? GuestCategoryId { get { return _guestCategoryId; } set { Set(ref _guestCategoryId, value, "GuestCategoryId"); } }
        [DataMember]
        public Guid? Title { get { return _title; } set { Set(ref _title, value, "Title"); } }
        [DataMember]
        public string MiddleName { get { return _middleName; } set { Set(ref _middleName, value, "MiddleName"); } }

        public ContactContract Contact
        {
            get { return ClientContract as ContactContract; }

        } 
        
        #endregion
        #region Parameters

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateTablesReservationGuest(TablesReservationGuestContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}