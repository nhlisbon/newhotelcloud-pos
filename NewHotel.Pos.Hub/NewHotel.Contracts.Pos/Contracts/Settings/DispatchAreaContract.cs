﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class DispatchAreaContract : BaseContract
    {
        
        #region Members

        private Guid _installationId;
        private ARGBColor? _colorArea;
        private bool _inactive;
        private short _kitchenCopies;

        #endregion
        #region Proprties

        
        [DataMember]
        public Guid InstallationId { get { return _installationId; } set { Set(ref _installationId, value, "InstallationId"); } }
        [DataMember]
        public ARGBColor? ColorArea { get { return _colorArea; } set { Set(ref _colorArea, value, "ColorArea"); } }
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }
        [DataMember]
        public short KitchenCopies { get { return _kitchenCopies; } set { Set(ref _kitchenCopies, value, "KitchenCopies"); } }

        [DataMember]
        internal LanguageTranslationContract _description;

        [ReflectionExclude]
        [Display(Name = "Description")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        #endregion

        #region PersistentList
        [DataMember]
        public TypedList<ProductbyDispatchAreaContract> ProductbyDispatchAreas { get; set; }
        #endregion
        #region Constructor

        public DispatchAreaContract()
            : base()
        {
            Description = new LanguageTranslationContract();
            ProductbyDispatchAreas = new TypedList<ProductbyDispatchAreaContract>();
        }

        #endregion
    }
}
