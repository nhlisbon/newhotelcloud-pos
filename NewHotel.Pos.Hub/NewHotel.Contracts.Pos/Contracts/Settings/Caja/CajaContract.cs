﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(CajaContract), "ValidateCaja")]
    public class CajaContract:BaseContract
    {
        #region Members

        private bool _tickets;
        private bool _report;

        #endregion
        #region Constructor

        public CajaContract()
            : base()
        {
            CajasStands = new TypedList<StandCajasContract>();
            Description = new LanguageTranslationContract();
        }

        #endregion
        #region Properties

        [DataMember]
        internal LanguageTranslationContract _description;

        [Display(Name = "Description")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        public bool Tickets { get { return _tickets; } set { Set(ref _tickets, value, "Tickets"); } }
        [DataMember]
        public bool Report { get { return _report; } set { Set(ref _report, value, "Report"); } }

        #endregion
        #region Contracts

        [DataMember]
        [ReflectionExclude]
        public TypedList<StandCajasContract> CajasStands { get; set; }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateCaja(CajaContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        [ReflectionExclude]
        public Guid[] StandCajasIds
        {
            get { return CajasStands.Select(x => x.Caja).ToArray(); }
        }
    }
}