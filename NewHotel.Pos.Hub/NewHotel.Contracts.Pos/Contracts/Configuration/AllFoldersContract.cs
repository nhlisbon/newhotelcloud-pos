﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
     [DataContract]
	[Serializable]
    public class AllFoldersContract:BaseContract
    {
          #region Members
        private Guid? _parentId;
        private string _name;
        private string _path;
        private bool _finalFolder;
        #endregion

        #region Properties


        [DataMember]
        public Guid? ParentId { get { return _parentId; } set { Set(ref _parentId, value, "ParentId"); } }
        [DataMember]
        public string Name { get { return _name; } set { Set(ref _name, value, "Name"); } }
        [DataMember]
        public string Path { get { return _path; } set { Set(ref _path, value, "Path"); } }
        [DataMember]
        public bool FinalFolder { get { return _finalFolder; } set { Set(ref _finalFolder, value, "FinalFolder"); } }


        #endregion


        #region Constructor
        

        public AllFoldersContract(Guid id,Guid? parentId,string name,bool finalFolder,string path):base()
        {
            this.ParentId = parentId;
            this.Id = id;
            this.Name = name;
            this.FinalFolder = finalFolder;
            this.Path = path;
        }
        #endregion
    }
}
