﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class FilesFolderContract : BaseContract
    {
          #region Members
        private string _name;
        #endregion

        #region Properties


        [DataMember]
        public string Name { get { return _name; } set { Set(ref _name, value, "Name"); } }

        #endregion

        #region Contracts
        [DataMember]
        [ReflectionExclude]
        public TypedList<ImageInfoContract> Images { get; set; }
        #endregion


        #region Constructor
        public FilesFolderContract()
            : base() 
        {
            Images = new TypedList<ImageInfoContract>();
        }
        #endregion
    }
}
