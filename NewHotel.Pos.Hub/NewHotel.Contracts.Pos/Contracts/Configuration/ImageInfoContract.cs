﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ImageInfoContract:BaseContract
    {
          #region Members
        private string _name;
        private string _path;
        private Blob _image;
        private Guid _parentId;
        #endregion

        #region Properties


        [DataMember]
        public string Name { get { return _name; } set { Set(ref _name, value, "Name"); } }
        [DataMember]
        public string Path { get { return _path; } set { Set(ref _path, value, "Path"); } }
        [DataMember]
        public Blob Image { get { return _image; } set { Set(ref _image, value, "Image"); } }
        [DataMember]
        public Guid ParentId { get { return _parentId; } set { Set(ref _parentId, value, "ParentId"); } }

        #endregion


        #region Constructor
        public ImageInfoContract()
            : base() 
        {
        }
        #endregion
    }
}
