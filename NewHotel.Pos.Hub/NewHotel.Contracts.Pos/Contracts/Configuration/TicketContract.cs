﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Runtime.Serialization;
//using System.ComponentModel.DataAnnotations;
//using NewHotel.DataAnnotations;

//namespace NewHotel.Contracts
//{
//    [DataContract]
//    [CustomValidation(typeof(TicketContract), "ValidateTicket")]
//    public class TicketContract:BaseContract
//    {
//         #region Members
//        private bool _printProductLine;
//        private bool _printTaxDetails;
//        private bool _printOpeningTime;
//        private bool _printCloseDateTime;
//        private bool _printResourceDescription;
//        private bool _printSeparators;
//        private bool _printObservations;
//        private string _printMessage1;
//        private string _printMessage2;
//        private bool _printSignature;
//        private short _headerLinesEmpty;
//        private short? _minLines;
//        private Blob? _logo;
//        private short? _logoAlign;
//        #endregion

//        #region Properties

//        [DataMember]
//        internal LanguageTranslationContract _description;

//        //[ReflectionExclude]
//        [Display(Name = "Description")]
//        [LanguageTranslationValidation]
//        public LanguageTranslationContract Description
//        {
//            get { return _description; }
//            set { _description = value; }
//        }

//        [DataMember]
//        public bool PrintProductLine { get { return _printProductLine; } set { Set(ref _printProductLine, value, "PrintProductLine"); } }
//        [DataMember]
//        public bool PrintTaxDetails { get { return _printTaxDetails; } set { Set(ref _printTaxDetails, value, "PrintTaxDetails"); } }
//        [DataMember]
//        public bool PrintOpeningTime { get { return _printOpeningTime; } set { Set(ref _printOpeningTime, value, "PrintOpeningTime"); } }
//        [DataMember]
//        public bool PrintCloseDateTime { get { return _printCloseDateTime; } set { Set(ref _printCloseDateTime, value, "PrintCloseDateTime"); } }
//        [DataMember]
//        public bool PrintResourceDescription { get { return _printResourceDescription; } set { Set(ref _printResourceDescription, value, "PrintResourceDescription"); } }
//        [DataMember]
//        public bool PrintSeparators { get { return _printSeparators; } set { Set(ref _printSeparators, value, "PrintSeparators"); } }
//        [DataMember]
//        public bool PrintObservations { get { return _printObservations; } set { Set(ref _printObservations, value, "PrintObservations"); } }
//        [DataMember]
//        public string PrintMessage1 { get { return _printMessage1; } set { Set(ref _printMessage1, value, "PrintMessage1"); } }
//        [DataMember]
//        public string PrintMessage2 { get { return _printMessage2; } set { Set(ref _printMessage2, value, "PrintMessage2"); } }
//        [DataMember]
//        public bool PrintSignature { get { return _printSignature; } set { Set(ref _printSignature, value, "PrintSignature"); } }
//        [DataMember]
//        public short HeaderLinesEmpty { get { return _headerLinesEmpty; } set { Set(ref _headerLinesEmpty, value, "HeaderLinesEmpty"); } }
//        [DataMember]
//        public short? MinLines { get { return _minLines; } set { Set(ref _minLines, value, "MinLines"); } }
//        [DataMember]
//        public Blob? Logo { get { return _logo; } set { Set(ref _logo, value, "Logo"); } }
//        [DataMember]
//        public short? LogoAlign { get { return _logoAlign; } set { Set(ref _logoAlign, value, "LogoAlign"); } }

//        #endregion

//        #region Validations

//        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateTicket(TicketContract obj)
//        {
//            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
//        }

//        #endregion

//        #region Constructor
//        public TicketContract()
//            : base() 
//        {
//            Description = new LanguageTranslationContract();
//        }
//        #endregion
//    }
//}
