﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
     [DataContract]
	[Serializable]
    public class FolderContract:BaseContract
    {
           #region Members
        private string _name;
        private string _path;
        private Guid _parentId;
        #endregion

        #region Properties


        [DataMember]
        public string Name { get { return _name; } set { Set(ref _name, value, "Name"); } }
        [DataMember]
        public string Path { get { return _path; } set { Set(ref _path, value, "Path"); } }
        [DataMember]
        public Guid ParentId { get { return _parentId; } set { Set(ref _parentId, value, "ParentId"); } }


        #endregion

        #region Dummy Properties
        public bool FinalFolder
        {
            get
            {
                if (this.SubFolders.Count == 0)
                    return true;
                else return false;
            }

        }
        #endregion
        #region Contracts
        [DataMember]
        [ReflectionExclude]
        public TypedList<FolderContract> SubFolders { get; set; }
        #endregion


        #region Constructor
        public FolderContract()
            : base() 
        {
            SubFolders = new TypedList<FolderContract>();
        }
        #endregion
    }
}
