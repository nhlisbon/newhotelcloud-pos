﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.Contracts.Pos;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(ProductLineContract), "ValidateProductLine")]
    public class ProductLineContract : BaseContract, IProductLineContract
    {
        #region Members

        private short _itemNumber;
        private Guid _ticket;
        private Guid _stand;
        private Guid _caja;
        private Guid _product;
        private decimal _productQtd;
        private decimal _importe;
        private decimal _liqValue;
        private decimal _cost;
        private int _isAnul;
        private bool _anulAfterClosed;
        private Guid _util;
        private bool _aditional;
        private bool _march;
        private short _sendToAreaStatus;
        private DateTime? _sendToAreaStartTime;
        private DateTime? _sendToAreaEndTime;
        private short _printed;
        private Guid? _separator;
        private Guid _firstIva;
        private decimal _firstIvas;
        private decimal _percent1;
        private Guid? _secodIva;
        private decimal? _secondIvas;
        private decimal? _percent2;
        private Guid? _thirdIva;
        private decimal? _thirdIvas;
        private decimal? _percent3;
        private Guid? _mainProduct;
        private string _observations;
        private string _observationsMarch;
        private bool _isHappyHour;
        private decimal _valueBeforeDiscount;
        private decimal? _discountPercent;
        private decimal? _discount;
        private string _discountTypeDescription;
        private bool _automaticDiscount;
        private decimal _recharge;
        private short? _paxNumber;
        private bool? _alcoholic;
        private bool? _process;
        private DateTime _lastModify;

        #endregion
        #region Constructor

        public ProductLineContract(Guid id, Guid ticket, Guid stand, Guid caja, Guid product, Guid? separator,
            decimal productQtd, decimal importe, decimal cost, decimal? discount, decimal recharge,
            Guid firstIva, decimal firstIvas, Guid? secondIva, decimal? secondIvas, Guid? thirdIva, decimal? thirdIvas,
            int isAnul, bool anulAfterClosed, short sendToAreaStatus, DateTime? sendToAreaStartTime, DateTime? sendToAreaEndTime,
            short printed, Guid util, bool additional, Guid? mainProduct, string observations, string observationsMarch,
            string productDesc, decimal liqValue, bool isHappyHour, decimal? discountPercent, decimal valueBeforeDiscount,
            short? paxNumber, bool? alcoholic, short itemNumber, DateTime lastModify, string discountTypeDescription)
            : base()
        {
            Id = id;
            ItemNumber = itemNumber;
            Ticket = ticket;
            Stand = stand;
            Caja = caja;
            Product = product;
            Separator = separator;
            ProductQtd = productQtd;
            Importe = importe;
            Cost = cost;
            Discount = discount;
            Recharge = recharge;
            FirstIva = firstIva;
            FirstIvas = firstIvas;
            SecodIva = secondIva;
            SecondIvas = secondIvas;
            ThirdIva = thirdIva;
            ThirdIvas = thirdIvas;
            IsAnul = isAnul;
            AnulAfterClosed = anulAfterClosed;
            SendToAreaStatus = sendToAreaStatus;
            SendToAreaStartTime = sendToAreaStartTime;
            SendToAreaEndTime = sendToAreaEndTime;
            Printed = printed;
            Util = util;
            Aditional = additional;
            MainProduct = mainProduct;
            Observations = observations;
            ObservationsMarch = observationsMarch;
            ProductDesc = productDesc;
            LiqValue = liqValue;
            IsHappyHour = isHappyHour;
            DiscountPercent = discountPercent;
            ValueBeforeDiscount = valueBeforeDiscount;
            PaxNumber = paxNumber;
            Alcoholic = alcoholic;
            LastModify = lastModify;
            DiscountTypeDescription = "discountTypeDescription";
        }

        public ProductLineContract()
            : base()
        {
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid Ticket { get { return _ticket; } set { Set(ref _ticket, value, "Ticket"); } }
        [DataMember]
        public Guid Stand { get { return _stand; } set { Set(ref _stand, value, "Stand"); } }
        [DataMember]
        public Guid Caja { get { return _caja; } set { Set(ref _caja, value, "Caja"); } }
        [DataMember]
        public Guid Product { get { return _product; } set { Set(ref _product, value, "Product"); } }
        [DataMember]
        public Guid? Separator { get { return _separator; } set { Set(ref _separator, value, "Separator"); } }
        [DataMember]
        public decimal ProductQtd { get { return _productQtd; } set { Set(ref _productQtd, value, "ProductQtd"); } }
        [DataMember]
        public decimal Importe { get { return _importe; } set { Set(ref _importe, value, "Importe"); } }
        [DataMember]
        public decimal LiqValue { get { return _liqValue; } set { Set(ref _liqValue, value, "LiqValue"); } }
        [DataMember]
        public decimal Cost { get { return _cost; } set { Set(ref _cost, value, "Cost"); } }
        [DataMember]
        public Guid FirstIva { get { return _firstIva; } set { Set(ref _firstIva, value, "FirstIva"); } }
        [DataMember]
        public decimal FirstIvas { get { return _firstIvas; } set { Set(ref _firstIvas, value, "FirstIvas"); } }
        [DataMember]
        public decimal Percent1 { get { return _percent1; } set { Set(ref _percent1, value, "Percent1"); } }
        [DataMember]
        public Guid? SecodIva { get { return _secodIva; } set { Set(ref _secodIva, value, "SecodIva"); } }
        [DataMember]
        public decimal? SecondIvas { get { return _secondIvas; } set { Set(ref _secondIvas, value, "SecondIvas"); } }
        [DataMember]
        public decimal? Percent2 { get { return _percent2; } set { Set(ref _percent2, value, "Percent2"); } }
        [DataMember]
        public Guid? ThirdIva { get { return _thirdIva; } set { Set(ref _thirdIva, value, "ThirdIva"); } }
        [DataMember]
        public decimal? ThirdIvas { get { return _thirdIvas; } set { Set(ref _thirdIvas, value, "ThirdIvas"); } }
        [DataMember]
        public decimal? Percent3 { get { return _percent3; } set { Set(ref _percent3, value, "Percent3"); } }
        [DataMember]
        public int IsAnul { get { return _isAnul; } set { Set(ref _isAnul, value, "IsAnul"); } }
        [DataMember]
        public bool AnulAfterClosed { get { return _anulAfterClosed; } set { Set(ref _anulAfterClosed, value, "AnulAfterClosed"); } }
        [DataMember]
        public bool March { get { return _march; } set { Set(ref _march, value, "March"); } }
        [DataMember]
        public short SendToAreaStatus { get { return _sendToAreaStatus; } set { Set(ref _sendToAreaStatus, value, "SendToAreaStatus"); } }
        [DataMember]
        public DateTime? SendToAreaStartTime { get { return _sendToAreaStartTime; } set { Set(ref _sendToAreaStartTime, value, nameof(SendToAreaStartTime)); } }
        [DataMember]
        public DateTime? SendToAreaEndTime { get { return _sendToAreaEndTime; } set { Set(ref _sendToAreaEndTime, value, nameof(SendToAreaEndTime)); } }
        [DataMember]
        public short Printed { get { return _printed; } set { Set(ref _printed, value, "Printed"); } }
        [DataMember]
        public Guid Util { get { return _util; } set { Set(ref _util, value, "Util"); } }
        [DataMember]
        public bool Aditional { get { return _aditional; } set { Set(ref _aditional, value, "Aditional"); } }
        [DataMember]
        public Guid? MainProduct { get { return _mainProduct; } set { Set(ref _mainProduct, value, "MainProduct"); } }
        [DataMember]
        public string Observations { get { return _observations; } set { Set(ref _observations, value, "Observations"); } }
        [DataMember]
        public string ObservationsMarch { get { return _observationsMarch; } set { Set(ref _observationsMarch, value, "ObservationsMarch"); } }
        [DataMember]
        public bool IsHappyHour { get { return _isHappyHour; } set { Set(ref _isHappyHour, value, "IsHappyHour"); } }
        [DataMember]
        public decimal ValueBeforeDiscount { get { return _valueBeforeDiscount; } set { Set(ref _valueBeforeDiscount, value, "ValueBeforeDiscount"); } }
        [DataMember]
        public decimal? DiscountPercent { get { return _discountPercent; } set { Set(ref _discountPercent, value, "DiscountPercent"); } }
        [DataMember]
        public decimal? Discount { get { return _discount; } set { Set(ref _discount, value, "Discount"); } }
        [DataMember]
        public string DiscountTypeDescription { get { return _discountTypeDescription; } set { Set(ref _discountTypeDescription, value, "DiscountTypeDescription"); } }
        [DataMember]
        public bool AutomaticDiscount { get { return _automaticDiscount; } set { Set(ref _automaticDiscount, value, "AutomaticDiscount"); } }
        [DataMember]
        public decimal Recharge { get { return _recharge; } set { Set(ref _recharge, value, "Recharge"); } }
        [DataMember]
        public short? PaxNumber { get { return _paxNumber; } set { Set(ref _paxNumber, value, "PaxNumber"); } }
        [DataMember]
        public bool? Alcoholic { get { return _alcoholic; } set { Set(ref _alcoholic, value, "Alcoholic"); } }
        [DataMember]
        public short ItemNumber { get { return _itemNumber; } set { Set(ref _itemNumber, value, "ItemNumber"); } }
        [DataMember]
        public DateTime LastModify { get { return _lastModify; } set { Set(ref _lastModify, value, "LastModify"); } }

        #endregion
        #region Extended Properties

        [DataMember]
        public string ProductDesc { get; set; }

        public string ProductCode { get; set; }

        public bool Process
        {
            get { return _process ?? (_process = true).Value; }
            set { _process = value; }
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateProductLine(ProductLineContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}