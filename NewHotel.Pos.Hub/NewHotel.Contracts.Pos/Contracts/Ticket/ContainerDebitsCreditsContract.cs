﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.Contracts;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ContainerDebitsCreditsContract : BaseContract
    {
        #region Members
        #endregion

        #region Constructor

        public ContainerDebitsCreditsContract()
            : base()
        {

        }
        #endregion

        #region Properties

        [DataMember]
        public List<MovementExternalContract> Credits { get; set; }
        [DataMember]
        public List<MovementExternalContract> Debits { get; set; }

        #endregion
    }
}
