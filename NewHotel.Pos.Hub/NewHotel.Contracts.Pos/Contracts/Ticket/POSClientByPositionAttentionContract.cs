﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(POSClientByPositionAttentionContract), nameof(POSClientByPositionAttentionContract.ValidateClientByPositionAttention))]
    [KnownType(typeof(POSClientByPositionAttentionContract))]
    public class POSClientByPositionAttentionContract : BaseContract
    {
        #region Variables

        private Guid _attentionId;
        private string _description;

        #endregion
        #region Constructor

        public POSClientByPositionAttentionContract()
            : base()
        {
        }

        public POSClientByPositionAttentionContract(Guid id, Guid attentionId, string description)
            : this()
        {
            Id = id;
            AttentionId = attentionId;
            Description = description;
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid AttentionId { get { return _attentionId; } set { Set(ref _attentionId, value, nameof(AttentionId)); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, nameof(Description)); } }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateClientByPositionAttention(POSClientByPositionAttentionContract obj)
        {
            if (string.IsNullOrEmpty(obj.Description))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description cannot be empty.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}