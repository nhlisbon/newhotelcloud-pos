﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.Contracts.Pos;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(PaymentLineContract), nameof(PaymentLineContract.ValidatePaymentLine))]
    [KnownType(typeof(CreditCardContract))]
    public class PaymentLineContract : BaseContract, IPaymentLineContract
    {
        #region Members

        private Guid _ticket;
        private Guid? _payment;
        private long _tire;
        private decimal _valo;
        private Guid? _creditCard;
        private CreditCardContract _creditCardContract;
        private Guid? _account;
        private Guid? _coin;
        private string _observations;
        private string _unmo;
        private string _room;
        private string _reseNumber;
        private string _authCode;
        private decimal _received;
        private string _currencyId;
        private decimal? _exchangeCurrency;
        private bool? _multCurrency;
        private bool? _process;
        private Guid? _depositId;

        #endregion
        #region Constructor

        public PaymentLineContract(Guid id, Guid ticket, Guid? payment, long tire, decimal valo, decimal received,
            CreditCardContract creditCard, Guid? account, Guid? coin, string observations, string tireDesc,
            string paymentDesc, string room, string resenumber, string authCode, string currencyId, decimal exchangeCurrency,
            bool multCurrency, DateTime lastModified)
            : base()
        {
            Id = id;
            Ticket = ticket;
            Payment = payment;
            Tire = tire;
            Valo = valo;
            CreditCardContract = creditCard;
            Account = account;
            Coin = coin;
            Observations = observations;
            TireDesc = tireDesc;
            PaymentDesc = paymentDesc;
            Room = room;
            ReseNumber = resenumber;
            AuthCode = authCode;
            CurrencyId = currencyId;
            ExchangeCurrency = exchangeCurrency;
            MultCurrency = multCurrency;
            LastModified = lastModified;
        }

        public PaymentLineContract()
            : base()
        {
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid Ticket { get { return _ticket; } set { Set(ref _ticket, value, nameof(Ticket)); } }

        [DataMember]
        public long Tire { get { return _tire; } set { Set(ref _tire, value, nameof(Tire)); } }

        [DataMember]
        public Guid? Payment
        {
            get { return _payment; }
            set { Set(ref _payment, value, nameof(Payment)); }
        }

        [DataMember]
        public Guid? CreditCard { get { return CreditCardContract != null ? (Guid?)CreditCardContract.Id : null; } set { Set(ref _creditCard, value, nameof(CreditCard)); } }

        [DataMember]
        public CreditCardContract CreditCardContract { get { return _creditCardContract; } set { Set(ref _creditCardContract, value, nameof(CreditCardContract)); } }

        [DataMember]
        public Guid? Account { get { return _account; } set { Set(ref _account, value, nameof(Account)); } }

        [DataMember]
        public Guid? Coin { get { return _coin; } set { Set(ref _coin, value, nameof(Coin)); } }

        [DataMember]
        public decimal Valo { get { return _valo; } set { Set(ref _valo, value, nameof(Valo)); } }
        [DataMember]
        public decimal Received { get { return _received; } set { Set(ref _received, value, nameof(Received)); } }

        [DataMember]
        public string Observations { get { return _observations; } set { Set(ref _observations, value, nameof(Observations)); } }

        [DataMember]
        public string Room { get { return _room; } set { Set(ref _room, value, nameof(Room)); } }

        [DataMember]
        public string ReseNumber { get { return _reseNumber; } set { Set(ref _reseNumber, value, nameof(ReseNumber)); } }

        [DataMember]
        public string AuthCode { get { return _authCode; } set { Set(ref _authCode, value, nameof(AuthCode)); } }

        [DataMember]
        public string CurrencyId { get { return _currencyId; } set { Set(ref _currencyId, value, nameof(CurrencyId)); } }

        [DataMember]
        public decimal? ExchangeCurrency { get { return _exchangeCurrency; } set { Set(ref _exchangeCurrency, value, nameof(ExchangeCurrency)); } }

        [DataMember]
        public bool? MultCurrency { get { return _multCurrency; } set { Set(ref _multCurrency, value, nameof(MultCurrency)); } }
        
        [DataMember]
        public Guid? DepositId { get { return _depositId; } set { Set(ref _depositId, value, nameof(DepositId)); } }
        #endregion
        #region DummyProperties

        [DataMember]
        public string TireDesc { get; set; }

        [DataMember]
        public string PaymentDesc { get; set; }

        [DataMember]
        public bool IsCreditCard { get { return CreditCard != null; } set { } }

        [DataMember]
        public bool IsAccount { get { return Account.HasValue && Account != default(Guid); } set { } }

        [DataMember]
        public bool IsCoin { get { return Coin.HasValue && Coin != default(Guid); } set { } }

        [DataMember]
        public CurrentAccountType? CreditType { get; set; }

        /// <summary>
        /// Moneda de ticket
        /// </summary>
        [DataMember]
        public string Unmo { get { return _unmo; } set { Set(ref _unmo, value, "Unmo"); } }

        #endregion
        #region IPaymentLine Implementation

        public Guid? PaymentId
        {
            get { return Payment; }
        }

        public ReceivableType ReceivableType
        {
            get { return (ReceivableType)Tire; }
        }

        public Guid? CreditCardId
        {
            get { return CreditCard; }
        }

        public decimal Value
        {
            get { return Valo; }
        }

        ICreditCardContract IPaymentLineContract.CreditCardContract
        {
            get { return CreditCardContract; }
        }
         
        public bool Process
        {
            get { return _process ?? (_process = true).Value; }
            set { _process = value; }
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidatePaymentLine(PaymentLineContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
