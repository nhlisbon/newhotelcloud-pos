﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [KnownType(typeof(POSProductLinePreparationContract))]
    public class POSProductLineAreaContract : BaseContract<Guid>
    {
        #region Constructor

        public POSProductLineAreaContract() { }

        public POSProductLineAreaContract(Guid id, Guid areaId)
            : base() 
        {
            Id = id;
            AreaId = areaId;
        }

        #endregion
        #region Members

        private Guid _areaId;
        
        #endregion
        #region Properties

        [DataMember]
        public Guid AreaId { get { return _areaId; } set { Set(ref _areaId, value, "AreaId"); } }

        #endregion
    }
}