﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(POSClientByPositionDietContract), nameof(POSClientByPositionDietContract.ValidateClientByPositionDiet))]
    [KnownType(typeof(POSClientByPositionDietContract))]
    public class POSClientByPositionDietContract : BaseContract
    {
        #region Variables

        private Guid _dietId;
        private string _description;

        #endregion
        #region Constructor

        public POSClientByPositionDietContract()
            : base()
        {
        }

        public POSClientByPositionDietContract(Guid id, Guid dietId, string description)
            : this()
        {
            Id = id;
            DietId = dietId;
            Description = description;
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid DietId { get { return _dietId; } set { Set(ref _dietId, value, nameof(DietId)); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, nameof(Description)); } }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateClientByPositionDiet(POSClientByPositionDietContract obj)
        {
            if (string.IsNullOrEmpty(obj.Description))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description cannot be empty.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}