﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NewHotel.Contracts.Pos.Contracts.Ticket
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(POSDocumentSerieContract), "ValidateDocumentSerie")]
    public class POSDocumentSerieContract : BaseContract
    {
        #region Constructor

        public POSDocumentSerieContract() { }

        #endregion
        #region Members

        private string _serie;
        private POSDocumentType _docTypeId;
        private Guid? _stand;
        private Guid? _caja;
        private long _nextNumber;
        private long? _endNumber;
        private DateTime? _endDate;
        private SerieStatus _serieStatus;
        private string _signature;

        #endregion
        #region Properties

        [DataMember]
        public string Serie { get { return _serie; } set { Set(ref _serie, value, "Serie"); } }
        [DataMember]
        public POSDocumentType DocTypeId { get { return _docTypeId; } set { Set(ref _docTypeId, value, "DocTypeId"); } }
        [DataMember]
        public Guid? Stand { get { return _stand; } set { Set(ref _stand, value, "Stand"); } }
        [DataMember]
        public Guid? Caja { get { return _caja; } set { Set(ref _caja, value, "Caja"); } }
        [DataMember]
        public long NextNumber { get { return _nextNumber; } set { Set(ref _nextNumber, value, "NextNumber"); } }
        [DataMember]
        public long? EndNumber { get { return _endNumber; } set { Set(ref _endNumber, value, "EndNumber"); } }
        [DataMember]
        public DateTime? EndDate { get { return _endDate; } set { Set(ref _endDate, value, "EndDate"); } }
        [DataMember]
        public SerieStatus SerieStatus { get { return _serieStatus; } set { Set(ref _serieStatus, value, "SerieStatus"); } }
        [DataMember]
        public string Signature { get { return _signature; } set { Set(ref _signature, value, "Signature"); } }

        #endregion
        
        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateDocumentSerie(DocumentSerieContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
    }
}
