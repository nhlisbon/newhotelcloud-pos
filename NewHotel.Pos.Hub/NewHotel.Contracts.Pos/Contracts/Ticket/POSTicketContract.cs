﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.Generic;
using NewHotel.DataAnnotations;
using NewHotel.Contracts.Pos;

namespace NewHotel.Contracts
{
    [Serializable]
    [DataContract]
    [CustomValidation(typeof(POSTicketContract), "ValidateTicket")]
    [KnownType(typeof(POSTicketContract))]
    public class POSTicketContract : BaseContract<Guid>, ITicketContract
    {
        public Guid TicketId => (Guid)Id;

        #region Variables

        private Guid _stand;
        private Guid _caja;
        private Guid? _mesa;
        private string _currency;
        private string _currencySymbol;
        private Guid? _cajaPave;
        private string _serie;
        private string _signature;
        private long? _number;
        private DateTime _openingDate;
        private DateTime _openingTime;
        private DateTime? _closedDate;
        private DateTime? _closedTime;
        private short _paxs;
        private short _currentPaxNumber;
        private int _receiveCons;
        private Guid _userId;
        private string _userDescription;
        private Guid? _internalUse;
        private bool _isAnul;
        private Guid? _cancellationReasonId;
        private Guid? _userCancellationId;
        private DateTime? _cancellationDate;
        private DateTime? _cancellationSystemDate;
        private string _cancellationComments;
        private Guid? _account;
        private string _accountDescription;
        private CurrentAccountType? _accountType;
        private Guid? _accountInstallationId;
        private decimal? _accountBalance;
        private string _cardNumber;
        private bool _allowRoomCredit;
        private long _shift;
        private decimal _generalDiscount;
        private decimal _recharge;
        private string _observations;
        private Guid? _duty;
        private Guid? _taxRate;
        private bool _taxIncluded;
        private decimal _total;
        private Guid? _invoiceAccount;
        private decimal? _tipPercent;
        private decimal? _tipValue;
        private bool _process;
        private bool _acceptedAsTransfer;
        private Guid? _openUserId;
        private string _openUserDescription;
        private bool _opened;
        private Guid? _closeUserId;
        private string _closeUserDescription;
        private Guid? _baseEntityId;
        private long? _fiscalIdenType;
        private Guid? _reservationTableId;
        private Guid? _taxSchemaId;
        private TablesReservationLineContract _reservationTable;
        private POSDocumentType _documentType;
        private long _openingNumber;
        private bool _fiscalDocumentPending;

        private long? _fpInvoiceNumber;
        private long? _fpCreditNoteNumber;
        private string _fpSerialInvoice;
        private string _fpSerialCreditNote;

        private string _fiscalDocSerie;
        private long? _fiscalDocNumber;
        private DateTime? _fiscalDocEmiDate;
        private DateTime? _fiscalDocRegDate;
        private string _fiscalDocTo;
        private string _fiscalDocAddress;
        private string _fiscalDocNacionality;
        private string _fiscalDocFiscalNumber;
        private decimal? _fiscalDocTotal;
        private string _fiscalDocEmail;

        //btw fiscalization requirements
        private string _fiscalPhoneNumber;
        private string _fiscalCityName;
        private string _fiscalCityCode;
        private string _fiscalRegimeTypeCode;
        private string _fiscalResponsabilityCode;
        private string _cufe;

		private string _baseCurrency;
		private decimal _exchangeFactor;
		#endregion
		#region Constructor

		public POSTicketContract()
            : base()
        {
            ProductLineByTicket = new TypedList<POSProductLineContract>();
            PaymentLineByTicket = new TypedList<POSPaymentLineContract>();
            LookupTableByTicket = new TypedList<LookupTableContract>();
            ClientsByPosition = new TypedList<POSClientByPositionContract>();
            LogByTicket = new TypedList<LogTicketContract>();
            Paxs = 1;
        }

        #endregion
        #region Properties

        [DataMember]
        public POSDocumentType DocumentType
        {
            get
            {
                // Initialize fiscal doc type. This is needed in the case of olds POS-HUB, these do not initialize this property
                if (_documentType == default(POSDocumentType))
                    _documentType = FiscalDocNumber.HasValue ? POSDocumentType.CashInvoice : POSDocumentType.Ticket;

                return _documentType;
            }
            set { Set(ref _documentType, value, nameof(DocumentType)); }
        }

        [DataMember]
        public Guid? TaxSchemaId { get { return _taxSchemaId; } set { Set(ref _taxSchemaId, value, nameof(TaxSchemaId)); } }
        [DataMember]
        public Guid? ReservationTableId { get { return _reservationTableId; } set { Set(ref _reservationTableId, value, nameof(ReservationTableId)); } }
        [DataMember]
        public TablesReservationLineContract ReservationTable { get { return _reservationTable; } set { Set(ref _reservationTable, value, nameof(ReservationTable)); } }
        [DataMember]
        public bool IsAcceptedAsTransfer { get { return _acceptedAsTransfer; } set { Set(ref _acceptedAsTransfer, value, nameof(IsAcceptedAsTransfer)); } }
        [DataMember]
        public Guid Stand { get { return _stand; } set { Set(ref _stand, value, nameof(Stand)); } }
        [DataMember]
        public Guid Caja { get { return _caja; } set { Set(ref _caja, value, nameof(Caja)); } }
        [DataMember]
        public Guid? Mesa { get { return _mesa; } set { Set(ref _mesa, value, nameof(Mesa)); } }
        [DataMember]
        public string Currency { get { return _currency; } set { Set(ref _currency, value, nameof(Currency)); } }
        [DataMember]
        public string BaseCurrency { get { return _baseCurrency; } set { Set(ref _baseCurrency, value, nameof(BaseCurrency)); } }
        [DataMember]
        public decimal ExchangeFactor { get { return _exchangeFactor; } set { Set(ref _exchangeFactor, value, nameof(ExchangeFactor)); } }

        [DataMember]
        public string CurrencySymbol { get { return _currencySymbol; } set { Set(ref _currencySymbol, value, nameof(CurrencySymbol)); } }
        [DataMember]
        public Guid? CajaPave { get { return _cajaPave; } set { Set(ref _cajaPave, value, nameof(CajaPave)); } }
        [DataMember]
        public string Serie { get { return _serie; } set { Set(ref _serie, value, nameof(Serie)); } }
        [DataMember]
        public string Signature { get { return _signature; } set { Set(ref _signature, value, nameof(Signature)); } }
        [DataMember]
        public long? Number { get { return _number; } set { Set(ref _number, value, nameof(Number)); } }
        [DataMember]
        public long OpeningNumber { get { return _openingNumber; } set { Set(ref _openingNumber, value, nameof(OpeningNumber)); } }
        [DataMember]
        public DateTime OpeningDate { get { return _openingDate; } set { Set(ref _openingDate, value, nameof(OpeningDate)); } }
        [DataMember]
        public DateTime OpeningTime { get { return _openingTime; } set { Set(ref _openingTime, value, nameof(OpeningTime)); } }
        [DataMember]
        public DateTime? ClosedDate { get { return _closedDate; } set { Set(ref _closedDate, value, nameof(ClosedDate)); } }
        [DataMember]
        public DateTime? ClosedTime { get { return _closedTime; } set { Set(ref _closedTime, value, nameof(ClosedTime)); } }
        [DataMember]
        public short Paxs { get { return _paxs; } set { Set(ref _paxs, value, nameof(Paxs)); } }
        [DataMember]
        public short CurrentPaxNumber { get { return _currentPaxNumber; } set { Set(ref _currentPaxNumber, value, nameof(CurrentPaxNumber)); } }
        [DataMember]
        public int ReceiveCons { get { return _receiveCons; } set { Set(ref _receiveCons, value, nameof(ReceiveCons)); } }
        [DataMember]
        public Guid UserId { get { return _userId; } set { Set(ref _userId, value, nameof(UserId)); } }
        [DataMember]
        public string UserDescription { get { return _userDescription; } set { Set(ref _userDescription, value, nameof(UserDescription)); } }
        [DataMember]
        public Guid? InternalUse { get { return _internalUse; } set { Set(ref _internalUse, value, nameof(InternalUse)); } }
        [DataMember]
        public bool IsAnul { get { return _isAnul; } set { Set(ref _isAnul, value, nameof(IsAnul)); } }
        [DataMember]
        public Guid? CancellationReasonId { get { return _cancellationReasonId; } set { Set(ref _cancellationReasonId, value, nameof(CancellationReasonId)); } }
        [DataMember]
        public Guid? UserCancellationId { get { return _userCancellationId; } set { Set(ref _userCancellationId, value, nameof(UserCancellationId)); } }
        [DataMember]
        public DateTime? CancellationDate { get { return _cancellationDate; } set { Set(ref _cancellationDate, value, nameof(CancellationDate)); } }
        [DataMember]
        public DateTime? CancellationSystemDate { get { return _cancellationSystemDate; } set { Set(ref _cancellationSystemDate, value, nameof(CancellationSystemDate)); } }
        [DataMember]
        public string CancellationComments { get { return _cancellationComments; } set { Set(ref _cancellationComments, value, nameof(CancellationComments)); } }
        
        [DataMember]
        public Guid? Account { get { return _account; } set { Set(ref _account, value, nameof(Account)); } }

        [DataMember]
        public bool AllowRoomCredit
        {
            get
            {
                return _allowRoomCredit;
            }
            set
            {
                Set(ref _allowRoomCredit, value, nameof(AllowRoomCredit));
            }
        }
        
        [DataMember]
        public string AccountDescription { get { return _accountDescription; } set { Set(ref _accountDescription, value, nameof(AccountDescription)); } }
        [DataMember]
        public CurrentAccountType? AccountType { get { return _accountType; } set { Set(ref _accountType, value, nameof(AccountType)); } }
        [DataMember]
        public Guid? AccountInstallationId { get { return _accountInstallationId; } set { Set(ref _accountInstallationId, value, nameof(AccountInstallationId)); } }
        [DataMember]
        public decimal? AccountBalance { get { return _accountBalance; } set { Set(ref _accountBalance, value, nameof(AccountBalance)); } }
        [DataMember]
        public string CardNumber { get { return _cardNumber; } set { Set(ref _cardNumber, value, nameof(CardNumber)); } }
        [DataMember]
        public long Shift { get { return _shift; } set { Set(ref _shift, value, nameof(Shift)); } }
        [DataMember]
        public decimal GeneralDiscount { get { return _generalDiscount; } set { Set(ref _generalDiscount, value, nameof(GeneralDiscount)); } }
        [DataMember]
        public decimal Recharge { get { return _recharge; } set { Set(ref _recharge, value, nameof(Recharge)); } }
        [DataMember]
        public string Observations { get { return _observations; } set { Set(ref _observations, value, nameof(Observations)); } }
        [DataMember]
        public Guid? Duty { get { return _duty; } set { Set(ref _duty, value, nameof(Duty)); } }
        [DataMember]
        public Guid? TaxRate { get { return _taxRate; } set { Set(ref _taxRate, value, nameof(TaxRate)); } }
        [DataMember]
        public bool TaxIncluded { get { return _taxIncluded; } set { Set(ref _taxIncluded, value, nameof(TaxIncluded)); } }
        [DataMember]
        public decimal Total { get { return _total; } set { Set(ref _total, value, nameof(Total)); } }
        [DataMember]
        public Guid? InvoiceAccount { get { return _invoiceAccount; } set { Set(ref _invoiceAccount, value, nameof(InvoiceAccount)); } }
        [DataMember]
        public long? FiscalIdenType { get { return _fiscalIdenType; } set { Set(ref _fiscalIdenType, value, nameof(FiscalIdenType)); } }
        [DataMember]
        public decimal? TipPercent { get { return _tipPercent; } set { Set(ref _tipPercent, value, nameof(TipPercent)); } }
        [DataMember]
        public decimal? TipValue { get { return _tipValue; } set { Set(ref _tipValue, value, nameof(TipValue)); } }
        [DataMember]
        public bool Process { get { return _process; } set { Set(ref _process, value, nameof(Process)); } }
        [DataMember]
        public Guid? OpenUserId { get { return _openUserId; } set { Set(ref _openUserId, value, nameof(OpenUserId)); } }
        [DataMember]
        public string OpenUserDescription { get { return _openUserDescription; } set { Set(ref _openUserDescription, value, nameof(OpenUserDescription)); } }
        [DataMember]
        public bool Opened { get { return _opened; } set { Set(ref _opened, value, nameof(Opened)); } }
        [DataMember]
        public Guid? CloseUserId { get { return _closeUserId; } set { Set(ref _closeUserId, value, nameof(CloseUserId)); } }
        [DataMember]
        public string CloseUserDescription { get { return _closeUserDescription; } set { Set(ref _closeUserDescription, value, nameof(CloseUserDescription)); } }
        [DataMember]
        public Guid? BaseEntityId { get { return _baseEntityId; } set { Set(ref _baseEntityId, value, nameof(BaseEntityId)); } }
        [DataMember]
        public bool FiscalDocumentPending { get { return _fiscalDocumentPending; } set { Set(ref _fiscalDocumentPending, value, nameof(FiscalDocumentPending)); } }
        [DataMember]
        public bool ApplyAutomaticProductDiscount { get; set; }

        [DataMember]
        public string FpSerialInvoice { get { return _fpSerialInvoice; } set { Set(ref _fpSerialInvoice, value, nameof(FpSerialInvoice)); } }
        [DataMember]
        public long? FpInvoiceNumber { get { return _fpInvoiceNumber; } set { Set(ref _fpInvoiceNumber, value, nameof(FpInvoiceNumber)); } }
        [DataMember]
        public string FpSerialCreditNote { get { return _fpSerialCreditNote; } set { Set(ref _fpSerialCreditNote, value, nameof(FpSerialCreditNote)); } }
        [DataMember]
        public long? FpCreditNoteNumber { get { return _fpCreditNoteNumber; } set { Set(ref _fpCreditNoteNumber, value, nameof(FpCreditNoteNumber)); } }

        [DataMember]
        public string FiscalDocSerie { get { return _fiscalDocSerie; } set { Set(ref _fiscalDocSerie, value, nameof(FiscalDocSerie)); } }
        [DataMember]
        public long? FiscalDocNumber { get { return _fiscalDocNumber; } set { Set(ref _fiscalDocNumber, value, nameof(FiscalDocNumber)); } }
        [DataMember]
        public DateTime? FiscalDocEmiDate { get { return _fiscalDocEmiDate; } set { Set(ref _fiscalDocEmiDate, value, nameof(FiscalDocEmiDate)); } }
        [DataMember]
        public DateTime? FiscalDocRegDate { get { return _fiscalDocRegDate; } set { Set(ref _fiscalDocRegDate, value, nameof(FiscalDocRegDate)); } }
        [DataMember]
        public string FiscalDocTo { get { return _fiscalDocTo; } set { Set(ref _fiscalDocTo, value, nameof(FiscalDocTo)); } }
        [DataMember]
        public string FiscalDocAddress { get { return _fiscalDocAddress; } set { Set(ref _fiscalDocAddress, value, nameof(FiscalDocAddress)); } }
        [DataMember]
        public string FiscalDocNacionality { get { return _fiscalDocNacionality; } set { Set(ref _fiscalDocNacionality, value, nameof(FiscalDocNacionality)); } }
        [DataMember]
        public string FiscalDocFiscalNumber { get { return _fiscalDocFiscalNumber; } set { Set(ref _fiscalDocFiscalNumber, value, nameof(FiscalDocFiscalNumber)); } }
        [DataMember]
        public decimal? FiscalDocTotal { get { return _fiscalDocTotal; } set { Set(ref _fiscalDocTotal, value, nameof(FiscalDocTotal)); } }
        [DataMember]
        public string FiscalDocEmail { get { return _fiscalDocEmail; } set { Set(ref _fiscalDocEmail, value, nameof(FiscalDocEmail)); } }
        [DataMember]
        public string FiscalPhoneNumber { get { return _fiscalPhoneNumber; } set { Set(ref _fiscalPhoneNumber, value, nameof(FiscalPhoneNumber)); } }

        [DataMember]
        public string FiscalCityCode { get { return _fiscalCityCode; } set { Set(ref _fiscalCityCode, value, nameof(FiscalCityCode)); } }
        [DataMember]
        public string FiscalCityName { get { return _fiscalCityName; } set { Set(ref _fiscalCityName, value, nameof(FiscalCityName)); } }
        [DataMember]
        public string FiscalRegimeTypeCode { get { return _fiscalRegimeTypeCode; } set { Set(ref _fiscalRegimeTypeCode, value, nameof(FiscalRegimeTypeCode)); } }
        [DataMember]
        public string FiscalResponsabilityCode { get { return _fiscalResponsabilityCode; } set { Set(ref _fiscalResponsabilityCode, value, nameof(FiscalResponsabilityCode)); } }
        [DataMember]
        public string FiscalDocSignature { get; set; }
        [DataMember]
        public string FiscalDocValidationCode { get; set; }
        [DataMember]
        public Blob? FiscalDocQrCode { get; set; }
        [DataMember]
        public string FiscalDocQrCodeText { get; set; }
        [DataMember]
        public short FiscalDocPrints { get; set; }
        [DataMember]
        public string Cufe { get { return _cufe; } set { Set(ref _cufe, value, nameof(Cufe)); } }

        [DataMember]
        public string CreditNoteSerie { get; set; }
        [DataMember]
        public long? CreditNoteNumber { get; set; }
        [DataMember]
        public DateTime? CreditNoteSystemDate { get; set; }
        [DataMember]
        public DateTime? CreditNoteWorkDate { get; set; }
        [DataMember]
        public Guid? CreditNoteUserId { get; set; }
        [DataMember]
        public string CreditNoteSignature { get; set; }
        [DataMember]
        public string CreditNoteValidationCode { get; set; }
        [DataMember]
        public Blob? CreditNoteQrCode { get; set; }
        [DataMember]
        public string CreditNoteQrCodeData { get; set; }
        [DataMember]
        public string CreditNoteUserDescription { get; set; }
        [DataMember]
        public short CreditNotePrints { get; set; }

        [DataMember]
        public string ValidationCode { get; set; }
        [DataMember]
        public Blob? QrCode { get; set; }
        [DataMember]
        public string QrCodeData { get; set; }
        [DataMember]
        public short Prints { get; set; }

        [DataMember]
        public bool DigitalMenu { get; set; }
        [DataMember]
        public string WorkStation { get; set; }
        [DataMember]
        public Blob? DigitalSignature { get; set; }

        [DataMember]
        public DateTime? Sync { get; set; }
        
        [DataMember]
        public string LastUserDispatched { get; set; } 
        

        #region Back compatibility

        [DataMember]
        public string InvoiceSerie { get { return FiscalDocSerie; } set { FiscalDocSerie = value; } }
        [DataMember]
        public long? InvoiceNumber { get { return FiscalDocNumber; } set { FiscalDocNumber = value; } }
        [DataMember]
        public DateTime? InvoiceEmiDate { get { return FiscalDocEmiDate; } set { FiscalDocEmiDate = value; } }
        [DataMember]
        public DateTime? InvoiceRegDate { get { return FiscalDocRegDate; } set { FiscalDocRegDate = value; } }
        [DataMember]
        public string InvoiceTo { get { return FiscalDocTo; } set { FiscalDocTo = value; } }
        [DataMember]
        public string InvoiceAddress { get { return FiscalDocAddress; } set { FiscalDocAddress = value; } }
        [DataMember]
        public string InvoiceNacionality { get { return FiscalDocNacionality; } set { FiscalDocNacionality = value; } }
        [DataMember]
        public string InvoiceFiscalNumber { get { return FiscalDocFiscalNumber; } set { FiscalDocFiscalNumber = value; } }
        [DataMember]
        public decimal? InvoiceTotal { get { return FiscalDocTotal; } set { FiscalDocTotal = value; } }
        [DataMember]
        public string InvoiceSignature { get { return FiscalDocSignature; } set { FiscalDocSignature = value; } }
        [DataMember]
        public string FiscalEmail { get { return FiscalDocEmail; } set { FiscalDocEmail = value; } }

        #endregion

        [ReflectionExclude]
        public decimal TicketValue
        {
            get
            {
                return ProductLineByTicket
                    .Where(x => x.AnnulmentCode == 0 || x.AnnulmentCode == 2)
                    .Sum(x => x.GrossValue);
            }
        }

        [ReflectionExclude]
        public decimal TicketValueBeforeDiscount
        {
            get
            {
                return ProductLineByTicket
                    .Where(x => (x.AnnulmentCode == 0 || x.AnnulmentCode == 2) && !x.IsTip)
                    .Sum(x => x.ValueBeforeDiscount);
            }
        }

        [ReflectionExclude]
        public decimal TotalTaxes
        {
            get
            {
                return ProductLineByTicket
                    .Where(x => x.AnnulmentCode == 0 || x.AnnulmentCode == 2)
                    .Sum(x => x.GrossValue - x.NetValue);
            }
        }

        [ReflectionExclude]
        public decimal PaymentReceived
        {
            get
            {
                return PaymentLineByTicket
                    .Where(x => x.AnnulmentCode == 0 || x.AnnulmentCode == 2)
                    .Sum(x => x.PaymentReceived);
            }
        }

        [ReflectionExclude]
        public decimal PaymentTotal
        {
            get
            {
                return PaymentLineByTicket
                    .Where(x => x.AnnulmentCode == 0 || x.AnnulmentCode == 2)
                    .Sum(x => x.PaymentAmount);
            }
        }

        [ReflectionExclude]
        public decimal PaymentChange
        {
            get
            {
                return PaymentLineByTicket
                    .Where(x => x.AnnulmentCode == 0 || x.AnnulmentCode == 2)
                    .Sum(x => x.PaymentReceived - x.PaymentAmount);
            }
        }

        [ReflectionExclude]
        public string TipDescription
        {
            get
            {
                var productLineByTicket = ProductLineByTicket.FirstOrDefault(x => x.IsTip);
                if (productLineByTicket != null)
                    return productLineByTicket.ProductDescription;

                return string.Empty;
            }
        }

        #endregion
        #region Lists

        [DataMember]
        public TypedList<POSProductLineContract> ProductLineByTicket { get; set; }
        [DataMember]
        public TypedList<POSPaymentLineContract> PaymentLineByTicket { get; set; }
        [DataMember]
        public TypedList<LookupTableContract> LookupTableByTicket { get; set; }
        [DataMember]
        public TypedList<POSClientByPositionContract> ClientsByPosition { get; set; }
        [DataMember]
        public TypedList<LogTicketContract> LogByTicket { get; set; }

        [ReflectionExclude]
        public Guid[] ProductLinesIds
        {
            get { return ProductLineByTicket.Select(x => (Guid)x.Id).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] PaymentLinesIds
        {
            get { return PaymentLineByTicket.Select(x => (Guid)x.Id).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] LookupTablesIds
        {
            get { return LookupTableByTicket.Select(x => (Guid)x.Id).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] LogsIds
        {
            get { return LogByTicket.Select(x => (Guid)x.Id).ToArray(); }
        }

        [ReflectionExclude]
        public string SerieName
        {
            get

            {
                var serieName = string.Empty;
                if (Number.HasValue)
                    serieName += Number.Value.ToString();
                if (!string.IsNullOrEmpty(Serie))
                    serieName += "/" + Serie;

                return serieName;
            }
        }

        #endregion
        #region Other Properties

        [DataMember]
        public bool TicketBlockedIssue { get; set; }
        [DataMember]
        public string StandDescription { get; set; }
        [DataMember]
        public string Pension { get; set; }
        [DataMember]
        public string Room { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string RoomServiceNumber { get; set; }
        [DataMember]
        public string CashierDescription { get; set; }
        [DataMember]
        public string TableDescription { get; set; }
        [DataMember]
        public string SaloonDescription { get; set; }
        [DataMember]
        public Guid? SpaServiceId {  get; set; }
        [DataMember]
        public bool IsSpaServiceDigitallySigned { get; set; }

        public bool HasSpecialPayment
        {
            get { return HasPayments && PaymentLineByTicket.Any(x => x.IsSpecialPayment); }
        }

        public bool HasCashPayment
        {
            get { return HasPayments && PaymentLineByTicket.Any(x => x.IsCash); }
        }

        public bool HasMealPlanPayment
        {
            get { return HasPayments && PaymentLineByTicket.Any(x => x.IsMealPlanPayment); }
        }

        public bool HasCreditRoomPayment
        {
            get { return HasPayments && PaymentLineByTicket.Any(x => x.IsCreditRoom); }
        }

        public bool HasHouseUsePayment
        {
            get { return HasPayments && PaymentLineByTicket.Any(x => x.IsHouseUsePayment); }
        }

        public bool HasAccountDepositPayment
        {
            get { return HasPayments && PaymentLineByTicket.Any(x => x.IsAccountDepositPayment); }
        }

        public bool HasPayments
        {
            get { return PaymentLineByTicket?.Count > 0; }
        }

        public bool IsMealPlanPayment => PaymentLineByTicket.All(x => x.IsMealPlanPayment);
        public bool IsCashPayment => PaymentLineByTicket.All(x => x.IsCash);
        public bool IsHouseUsePayment => HasPayments && PaymentLineByTicket.All(x => x.IsHouseUsePayment);
        public bool IsHouseUseAndCashCombinedPayments => HasHouseUsePayment && PaymentLineByTicket.Count - PaymentLineByTicket.Count(x => x.IsCash) == 1;

        public bool IsOnlyTicket
        {
            get { return DocumentType == POSDocumentType.Ticket; }
        }

        public bool IsBallot
        {
            get { return DocumentType == POSDocumentType.Ballot; }
        }

        public bool IsInvoice
        {
            get { return DocumentType == POSDocumentType.CashInvoice; }
        }

        public bool IsCreditNote
        {
            get { return DocumentType == POSDocumentType.CashCreditNote; }
        }

        public DateTime? CloseDate
        {
            get { return ClosedDate; }
        }

        public string Unmo
        {
            get { return Currency; }
        }

        public IEnumerable<IPaymentLineContract> PaymentLines
        {
            get { return PaymentLineByTicket; }
        }

        public IEnumerable<IProductLineContract> ProductLines
        {
            get { return ProductLineByTicket; }
        }
      
        #endregion
        #region Public Methods

        public void ClearImages()
        {
            foreach (var productLine in ProductLineByTicket)
                productLine.ProductImage = string.Empty;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateTicket(POSTicketContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        public bool AllowPersistInvalid { get; set; }

		#endregion
	}

    public static class POSTicketContractExt
    {
        public static bool MustChargeAccount(this POSTicketContract ticket) => ticket.PaymentLineByTicket != null &&
                                            (ticket.PaymentLineByTicket.Any(x => x.AccountId.HasValue && x.ReceivableType == ReceivableType.Credit)
                                             || ticket.PaymentLineByTicket.Any(x => x.IsAccount && x.ReceivableType == ReceivableType.UseAccountDeposit));

        public static bool HasBookingTableInCheckIn(this POSTicketContract ticket) => ticket.ReservationTableId.HasValue && ticket.ReservationTable.IsCheckin;

        public static bool HasSpaService(this POSTicketContract ticket) => ticket.SpaServiceId.HasValue;

        public static decimal CalculateTotal(this POSTicketContract ticket, bool applyTipOverNetValue = false)
        {
            var subTotal = ticket.ProductLineByTicket
                .Where(pl => pl.IsActive && !pl.IsTip)
                .Sum(pl => applyTipOverNetValue ? pl.NetValue : pl.GrossValue);

            if (ticket.TipPercent.HasValue)
            {
                ticket.TipValue = decimal.Round(subTotal * ticket.TipPercent.Value / 100, 2, MidpointRounding.AwayFromZero);
            }

            var currentTipValue = decimal.Round(ticket.TipValue ?? decimal.Zero, 2, MidpointRounding.AwayFromZero);

            var total = subTotal + currentTipValue;

            return total;
        }
    }
}