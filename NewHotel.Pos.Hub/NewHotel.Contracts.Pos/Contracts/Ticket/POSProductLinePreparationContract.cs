﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [KnownType(typeof(POSProductLinePreparationContract))]
    public class POSProductLinePreparationContract : BaseContract<Guid>
    {
        #region Constructor

        public POSProductLinePreparationContract() { }

        public POSProductLinePreparationContract(Guid id, Guid productLineId, string productDescription, Guid preparationId, string preparationDescription)
            : base() 
        {
            Id = id;
            ProductLineId = productLineId;
            ProductDescription = productDescription;
            PreparationId = preparationId;
            PreparationDescription = preparationDescription;
        }

        #endregion
        #region Members

        private Guid _preparationId;
        private Guid _productLineId;

        #endregion
        #region Properties

        [DataMember]
        public Guid ProductLineId { get { return _productLineId; } set { Set(ref _productLineId, value, "ProductLineId"); } }
        [DataMember]
        public string ProductDescription { get; set; }

        [DataMember]
        public Guid PreparationId { get { return _preparationId; } set { Set(ref _preparationId, value, "PreparationId"); } }
        [DataMember]
        public string PreparationDescription { get; set; }

        #endregion
    }
}