﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [KnownType(typeof(POSCreditCardContract))]
    public class POSCreditCardContract : BaseContract, ICreditCardContract
    {
        #region Constructor

        public POSCreditCardContract()
            : base() { }

        #endregion
        #region Members

        private Guid _creditCardTypeId;
        private string _creditCardTypeDescription;
        private string _creditCardNumber;
        private short? _validationMonth;
        private short? _validationYear;
        private string _validationCode;
        private string _holderName;
        private string _address;
        private string _zipCode;

        private string _firstName;
        private string _lastName;
        private string _street1;
        private string _street2;
        private string _region;
        private string _city;
        private string _postalCode;
        private string _country;
        private string _emailAddress;
        private string _mobilePhone;
        private string _phone;

        private string _transactionNumber;
        private string _authorizationCode;

        #endregion
        #region Properties

        [DataMember]
        public Guid CreditCardTypeId { get { return _creditCardTypeId; } set { Set(ref _creditCardTypeId, value, "CreditCardTypeId"); } }
        [DataMember]
        public string CreditCardTypeDescription { get { return _creditCardTypeDescription; } set { Set(ref _creditCardTypeDescription, value, "CreditCardTypeDescription"); } }
        [DataMember]
        public string CreditCardNumber { get { return _creditCardNumber; } set { Set(ref _creditCardNumber, value, "CreditCardNumber"); } }
        [DataMember]
        public short? ValidationMonth { get { return _validationMonth; } set { Set(ref _validationMonth, value, "ValidationMonth"); } }
        [DataMember]
        public short? ValidationYear { get { return _validationYear; } set { Set(ref _validationYear, value, "ValidationYear"); } }
        [DataMember]
        public string ValidationCode { get { return _validationCode; } set { Set(ref _validationCode, value, "ValidationCode"); } }
        [DataMember]
        public string HolderName { get { return _holderName; } set { Set(ref _holderName, value, "HolderName"); } }
        [DataMember]
        public string Address { get { return _address; } set { Set(ref _address, value, "Address"); } }
        [DataMember]
        public string ZipCode { get { return _zipCode; } set { Set(ref _zipCode, value, "ZipCode"); } }

        [DataMember]
        public string FirstName { get { return _firstName; } set { Set(ref _firstName, value, "FirstName"); } }
        [DataMember]
        public string LastName { get { return _lastName; } set { Set(ref _lastName, value, "LastName"); } }
        [DataMember]
        public string Street1 { get { return _street1; } set { Set(ref _street1, value, "Street1"); } }
        [DataMember]
        public string Street2 { get { return _street2; } set { Set(ref _street2, value, "Street2"); } }
        [DataMember]
        public string Region { get { return _region; } set { Set(ref _region, value, "Region"); } }
        [DataMember]
        public string City { get { return _city; } set { Set(ref _city, value, "City"); } }
        [DataMember]
        public string PostalCode { get { return _postalCode; } set { Set(ref _postalCode, value, "PostalCode"); } }
        [DataMember]
        public string Country { get { return _country; } set { Set(ref _country, value, "Country"); } }
        [DataMember]
        public string EmailAddress { get { return _emailAddress; } set { Set(ref _emailAddress, value, "EmailAddress"); } }
        [DataMember]
        public string MobilePhone { get { return _mobilePhone; } set { Set(ref _mobilePhone, value, "MobilePhone"); } }
        [DataMember]
        public string Phone { get { return _phone; } set { Set(ref _phone, value, "Phone"); } }

        [DataMember]
        public string TransactionNumber { get { return _transactionNumber; } set { Set(ref _transactionNumber, value, "TransactionNumber"); } }
        [DataMember]
        public string AuthorizationCode { get { return _authorizationCode; } set { Set(ref _authorizationCode, value, "AuthorizationCode"); } }

        #endregion
        #region Extended Properties

        Guid? ICreditCardContract.CreditCardTypeId
        {
            get { return CreditCardTypeId; } 
        }

        public string IdentNumber
        {
            get { return CreditCardNumber; }
            set { CreditCardNumber = value; }
        }

        #endregion
    }
}