﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(POSClientByPositionContract), nameof(POSClientByPositionContract.ValidateClientByPosition))]
    [KnownType(typeof(POSClientByPositionContract))]
    public class POSClientByPositionContract : BaseContract
    {
        #region Variables

        private Guid? _clientId;
        private string _clientName;
        private string _clientRoom;
        private short _pax;

        #endregion
        #region Constructor

        public POSClientByPositionContract()
            : base()
        {
            Preferences = new TypedList<POSClientByPositionPreferenceContract>();
            Diets = new TypedList<POSClientByPositionDietContract>();
            Attentions = new TypedList<POSClientByPositionAttentionContract>();
            Allergies = new TypedList<POSClientByPositionAllergyContract>();
        }

        public POSClientByPositionContract(Guid id, Guid? clientId, string clientName, string clientRoom, short pax, string uncommonAllergies, string segmentOperations)
            : this()
        {
            Id = id;
            ClientId = clientId;
            ClientName = clientName;
            ClientRoom = clientRoom;
            Pax = pax;
            UncommonAllergies = uncommonAllergies;
            SegmentOperations = segmentOperations;
        }

        public POSClientByPositionContract(Guid id, Guid? clientId, string clientName, short pax)
            : this(id, clientId, clientName, string.Empty, pax, string.Empty, string.Empty)
        {
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid? ClientId { get { return _clientId; } set { Set(ref _clientId, value, nameof(ClientId)); } }
        [DataMember]
        public string ClientName { get { return _clientName; } set { Set(ref _clientName, value, nameof(ClientName)); } }
        [DataMember]
        public string ClientRoom { get { return _clientRoom; } set { Set(ref _clientRoom, value, nameof(ClientRoom)); } }
        [DataMember]
        public short Pax { get { return _pax; } set { Set(ref _pax, value, nameof(Pax)); } }
        [DataMember]
        public string UncommonAllergies { get; set; }
        [DataMember]
        public string SegmentOperations { get; set; }

        #endregion
        #region Lists

        [DataMember]
        public TypedList<POSClientByPositionPreferenceContract> Preferences { get; set; }
        [DataMember]
        public TypedList<POSClientByPositionDietContract> Diets { get; set; }
        [DataMember]
        public TypedList<POSClientByPositionAttentionContract> Attentions { get; set; }
        [DataMember]
        public TypedList<POSClientByPositionAllergyContract> Allergies { get; set; }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateClientByPosition(POSClientByPositionContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}