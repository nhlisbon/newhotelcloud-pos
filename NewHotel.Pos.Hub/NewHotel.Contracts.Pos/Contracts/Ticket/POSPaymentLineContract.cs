﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using NewHotel.Contracts.Pos;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(POSPaymentLineContract), nameof(POSPaymentLineContract.ValidatePaymentLine))]
    [KnownType(typeof(POSPaymentLineContract))]
    public class POSPaymentLineContract : BaseContract, IPaymentLineContract
    {
        #region Variables

        private short _annulmentCode;
        private Guid _ticket;
        private ReceivableType _receivableType;
        private Guid? _paymentId;
        private decimal _paymentAmount;
        // private decimal _paymentAmountInBase;
        private decimal _paymentReceived;
        private Guid? _creditCard;
        private Guid? _accountId;
        private Guid? _accountInstallationId;
        private Guid? _internalConsumptionId;
        private string _observations;
        private string _unmo;
        private string _paymentTypeAuxCode;
        private string _currencyId;
        private decimal? _exchangeCurrency;
        private bool? _multCurrency;
        private bool? _process;

        private POSCreditCardContract _creditCardContract;
        
        private Guid? _depositId;

        #endregion
        #region Constructor

        public POSPaymentLineContract()
            : base()
        {
        }

        public POSPaymentLineContract(Guid paymentLineId, long receivableId, string receivableDescription, Guid? paymentMethodId, string paymentMethodDescription,
             Guid? creditCardId, string creditCardNumber, Guid? accountId, Guid? internalConsumptionId, string internalConsumptionDescription, string remarks, decimal paymentValue, decimal paymentReceived, short annulmentCode,
             string currencyId, decimal? exchangeCurrency, bool? multCurrency)
            : this()
        {
            Id = paymentLineId;
            ReceivableType = (ReceivableType)receivableId;
            AnnulmentCode = annulmentCode;
            ReceivableDescription = receivableDescription;
            PaymentId = paymentMethodId;
            PaymentDescription = paymentMethodDescription;
            CreditCard = creditCardId;
            CreditCardNumber = creditCardNumber;
            AccountId = accountId;
            InternalConsumptionId = internalConsumptionId;
            InternalConsumptionDescription = internalConsumptionDescription;
            Observations = remarks;
            PaymentAmount = paymentValue;
            PaymentReceived = paymentReceived;
            CurrencyId = currencyId;
            ExchangeCurrency = exchangeCurrency;
            MultCurrency = multCurrency;
        }

        #endregion
        #region Properties

        [DataMember]
        public short AnnulmentCode { get { return _annulmentCode; } set { Set(ref _annulmentCode, value, nameof(AnnulmentCode)); } }
        [DataMember]
        public Guid Ticket { get { return _ticket; } set { Set(ref _ticket, value, nameof(Ticket)); } }
        [DataMember]
        public ReceivableType ReceivableType { get { return _receivableType; } set { Set(ref _receivableType, value, nameof(ReceivableType)); } }
        [DataMember]
        public Guid? PaymentId { get { return _paymentId; } set { Set(ref _paymentId, value, nameof(PaymentId)); } }
        [DataMember]
        public decimal PaymentAmount { get { return _paymentAmount; } set { Set(ref _paymentAmount, value, nameof(PaymentAmount)); } }
        //[DataMember]
        //public decimal PaymentAmountInBase { get { return _paymentAmountInBase; } set { Set(ref _paymentAmountInBase, value, nameof(PaymentAmountInBase)); } }
        [DataMember]
        public decimal PaymentReceived { get { return _paymentReceived; } set { Set(ref _paymentReceived, value, nameof(PaymentReceived)); } }
        [DataMember]
        public Guid? CreditCard { get { return CreditCardContract != null ? (Guid?)CreditCardContract.Id : null; } set { Set(ref _creditCard, value, nameof(CreditCard)); } }
        [DataMember]
        public Guid? InternalConsumptionId { get { return _internalConsumptionId; } set { Set(ref _internalConsumptionId, value, nameof(InternalConsumptionId)); } }
        [DataMember]
        public string Observations { get { return _observations; } set { Set(ref _observations, value, nameof(Observations)); } }
        [DataMember]
        public string PaymentTypeAuxCode { get { return _paymentTypeAuxCode; } set { Set(ref _paymentTypeAuxCode, value, nameof(PaymentTypeAuxCode)); } }
        [DataMember]
        public string CurrencyId { get { return _currencyId; } set { Set(ref _currencyId, value, nameof(CurrencyId)); } }
        [DataMember]
        public decimal? ExchangeCurrency { get { return _exchangeCurrency; } set { Set(ref _exchangeCurrency, value, nameof(ExchangeCurrency)); } }
        [DataMember]
        public bool? MultCurrency { get { return _multCurrency; } set { Set(ref _multCurrency, value, nameof(MultCurrency)); } }
        [DataMember]
        public Guid? AccountId { get { return _accountId; } set { Set(ref _accountId, value, nameof(AccountId)); } }
        [DataMember]
        public Guid? AccountInstallationId { get { return _accountInstallationId; } set { Set(ref _accountInstallationId, value, nameof(AccountInstallationId)); } }

        [ReflectionExclude]
        [DataMember]
        public POSCreditCardContract CreditCardContract { get { return _creditCardContract; } set { Set(ref _creditCardContract, value, nameof(CreditCardContract)); } }

        [DataMember]
        public Guid? DepositId { get { return _depositId; } set { Set(ref _depositId, value, nameof(DepositId)); } }
        
        #region DummyProperties

        [DataMember]
        public string ReceivableDescription { get; set; }
        [DataMember]
        public string PaymentDescription { get; set; }
        [DataMember]
        public bool IsCreditCard { get { return CreditCard != null; } set { } }
        [DataMember]
        public string CreditCardNumber { get; set; }
        [DataMember]
        public bool IsAccount { get { return AccountId.HasValue && AccountId != default(Guid); } set { } }
        [DataMember]
        public bool IsInternalConsumption { get { return InternalConsumptionId.HasValue && InternalConsumptionId != default(Guid); } set { } }
        [DataMember]
        public string InternalConsumptionDescription { get; set; }
        [DataMember]
        public CurrentAccountType? CreditType { get; set; }

        /// <summary>
        /// Moneda de ticket
        /// </summary>
        [DataMember]
        public string Unmo { get { return _unmo; } set { Set(ref _unmo, value, "Unmo"); } }

        public decimal ChangeValue { get { return PaymentReceived - PaymentAmount; } }

        public bool IsCash { get { return ReceivableType == ReceivableType.Payment; } }

        public bool IsMealPlanPayment { get { return ReceivableType == ReceivableType.RoomPlan; } }

        public bool IsCreditRoom { get { return ReceivableType == ReceivableType.Credit; } }

        public bool IsHouseUsePayment { get { return ReceivableType == ReceivableType.HouseUse; } }

        public bool IsSpecialPayment
        {
            get { return IsCreditCard || IsMealPlanPayment || IsHouseUsePayment; }
        }

        public bool IsAccountDepositPayment => ReceivableType == ReceivableType.UseAccountDeposit;

        public Guid? CreditCardId
        {
            get
            {
                return CreditCard;
            }
        }

        public decimal Value
        {
            get
            {
                return PaymentAmount;
            }
        }

        ICreditCardContract IPaymentLineContract.CreditCardContract
        {
            get
            {
                return CreditCardContract;
            }
        }

        public bool Process
        {
            get
            {
                return _process ?? (_process = true).Value;
            }
            set { _process = value; }
        }

        #endregion
        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidatePaymentLine(POSPaymentLineContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}