﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(TableLineContract), "ValidateTableLine")]
    public class TableLineContract : BaseContract
    {
        #region Members

        private Guid _productLineId;
        private Guid _productId;
        private Guid? _separatorId;
        private Guid? _separatorDescriptionId;
        private short? _separatorOrden;
        private string _separatorDescription;
        private decimal _cost;
        private decimal _price;
        private decimal _percent;
        private bool _isAditional;
        private bool _isSendedToArea;
        private short _sendToAreaStatus;
        private bool _printed;
        private string _observations;
        private decimal _quantity;
        private string _productAbbreviation;
        private string _productDescription;
        private Guid? _areaId;
        private TypedList<POSProductLineAreaContract> _areas;
        private Guid _menuId;

        #endregion
        #region Constructor

        public TableLineContract() : base()
        {
            TableLinePreparations = new TypedList<TableLinePreparationContract>();
            Areas = new TypedList<POSProductLineAreaContract>();
        }

        public TableLineContract(Guid id,Guid productLine, Guid product, Guid? separator, short? separatorOrden, string separatorDescription, decimal cost, decimal price,
            decimal pricePercent, bool additional, short sendToAreaStatus, bool print, string observations, decimal qtd, string abbreviation, string description)
            : this()
        {
            Id = id;
            ProductLineId = productLine;
            ProductId = product;
            SeparatorId = separator;
            SeparatorOrden = separatorOrden;
            SeparatorDescription = separatorDescription;
            Cost = cost;
            Price = price;
            Percent = pricePercent;
            IsAdditional = additional;
            SendToAreaStatus = sendToAreaStatus;
            Printed = print;
            Observations = observations;
            Quantity = qtd;
            ProductAbbreviation = abbreviation;
            ProductDescription = description;
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid ProductLineId { get { return _productLineId; } set { Set(ref _productLineId, value, "ProductLineId"); } }
        [DataMember]
        public Guid ProductId { get { return _productId; } set { Set(ref _productId, value, "ProductId"); } }
        [DataMember]
        public Guid? SeparatorId { get { return _separatorId; } set { Set(ref _separatorId, value, "SeparatorId"); } }
        [DataMember]
        public short? SeparatorOrden { get { return _separatorOrden; } set { Set(ref _separatorOrden, value, "SeparatorOrden"); } }
        [DataMember]
        public string SeparatorDescription { get { return _separatorDescription; } set { Set(ref _separatorDescription, value, "SeparatorDescription"); } }
        [DataMember]
        public Guid? SeparatorDescriptionId { get { return _separatorDescriptionId; } set { Set(ref _separatorDescriptionId, value, "SeparatorDescriptionId"); } }
        [DataMember]
        public decimal Cost { get { return _cost; } set { Set(ref _cost, value, "Cost"); } }
        [DataMember]
        public decimal Price { get { return _price; } set { Set(ref _price, value, "Price"); } }
        [DataMember]
        public decimal Percent { get { return _percent; } set { Set(ref _percent, value, "Percent"); } }
        [DataMember]
        public bool IsAdditional { get { return _isAditional; } set { Set(ref _isAditional, value, "IsAdditional"); } }
        [DataMember]
        public bool IsSendedToArea { get { return _isSendedToArea; } set { Set(ref _isSendedToArea, value, "IsSendedToArea"); } }
        [DataMember]
        public short SendToAreaStatus { get { return _sendToAreaStatus; } set { Set(ref _sendToAreaStatus, value, "SendToAreaStatus"); } }
        [DataMember]
        public bool Printed { get { return _printed; } set { Set(ref _printed, value, "Printed"); } }
        [DataMember]
        public string Observations { get { return _observations; } set { Set(ref _observations, value, "Observations"); } }
        [DataMember]
        public decimal Quantity { get { return _quantity; } set { Set(ref _quantity, value, "Quantity"); } }
        [DataMember]
        public string ProductAbbreviation { get { return _productAbbreviation; } set { Set(ref _productAbbreviation, value, "ProductAbbreviation"); } }
        [DataMember]
        public string ProductDescription { get { return _productDescription; } set { Set(ref _productDescription, value, "ProductDescription"); } }
        [DataMember]
        public Guid? AreaId { get { return _areaId; } set { Set(ref _areaId, value, nameof(AreaId)); } }
        [DataMember]
        public Guid MenuId { get { return _menuId; } set { Set(ref _menuId, value, nameof(MenuId)); } }

        #endregion
        #region Lists
        [ReflectionExclude]
        [DataMember]
        public TypedList<TableLinePreparationContract> TableLinePreparations { get; set; }
        [ReflectionExclude]
        [DataMember]
        public TypedList<POSProductLineAreaContract> Areas { get { return _areas; } set { Set(ref _areas, value, nameof(Areas)); } }
        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateTableLine(TableLineContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        public bool HasAreas
        {
            get { return (AreaId != null && AreaId != Guid.Empty) || Areas.Count > 0; }
        }

        #endregion
    }
}