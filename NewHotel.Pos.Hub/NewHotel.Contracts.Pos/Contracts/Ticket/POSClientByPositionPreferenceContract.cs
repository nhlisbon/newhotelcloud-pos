﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(POSClientByPositionPreferenceContract), nameof(POSClientByPositionPreferenceContract.ValidateClientByPositionPreference))]
    [KnownType(typeof(POSClientByPositionPreferenceContract))]
    public class POSClientByPositionPreferenceContract : BaseContract
    {
        #region Variables

        private Guid _preferenceId;
        private string _description;

        #endregion
        #region Constructor

        public POSClientByPositionPreferenceContract()
            : base()
        {
        }

        public POSClientByPositionPreferenceContract(Guid id, Guid preferenceId, string description)
            : this()
        {
            Id = id;
            PreferenceId = preferenceId;
            Description = description;
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid PreferenceId { get { return _preferenceId; } set { Set(ref _preferenceId, value, nameof(PreferenceId)); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, nameof(Description)); } }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateClientByPositionPreference(POSClientByPositionPreferenceContract obj)
        {
            if (string.IsNullOrEmpty(obj.Description))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description cannot be empty.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}