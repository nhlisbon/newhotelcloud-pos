﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TableLinePreparationContract : BaseContract
    {
        #region Constructor

        public TableLinePreparationContract() { }

        public TableLinePreparationContract(Guid id, Guid preparationId, Guid tableLineId, string preparationDescription, string lineDescription)
            : base() 
        {
            Id = id;
            PreparationId = preparationId;
            TableLineId = tableLineId;
            LineDescription = lineDescription;
            PreparationDescription = preparationDescription;
        }

        #endregion
        #region Members

        private Guid _preparationId;
        private Guid _tableLineId;

        #endregion
        #region Properties

        [DataMember]
        public Guid PreparationId { get { return _preparationId; } set { Set(ref _preparationId, value, "PreparationId"); } }
        [DataMember]
        public Guid TableLineId { get { return _tableLineId; } set { Set(ref _tableLineId, value, "TableLineId"); } }

        #endregion
        #region DummyProperties

        [DataMember]
        public string PreparationDescription { get; set; }
        [DataMember]
        public string LineDescription { get; set; }

        #endregion
    }
}
