﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;
using NewHotel.Contracts.Pos;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(TicketRContract), "ValidateTicketR")]
    public class TicketRContract : BaseContract, ITicketContract
    {
        #region Constructor

        public TicketRContract()
            : base()
        {
            ProductLines = new TypedList<ProductLineContract>();
            PaymentLines = new TypedList<PaymentLineContract>();
            LookupTables = new TypedList<LookupTableContract>();
            Logs = new TypedList<LogTicketContract>();
        }

        #endregion
        #region Members

        private Guid _stand;
        private Guid _caja;
        private DateTime? _fiscalDateTime;
        private DateTime _checkOpeningDate;
        private DateTime _checkOpeningTime;
        private short _paxs;
        private int _receiveCons;
        private Guid _util;
        private decimal _generalDiscount;
        private decimal _recharge;
        private Guid? _cajaPave;
        private string _seriePrex;
        private long? _serie;
        private string _signature;
        private long _openingSerie;
        private DateTime? _closeDate;
        private DateTime? _closeTime;
        private Guid? _closeUserId;
        private string _closeUserDescription;
        private Guid? _anul;
        private Guid? _internalUse;
        private Guid? _account;
        private Guid? _mesa;
        private Guid? _fiscalDocId;
        private bool _process;
        private int _shift;
        private Guid? _duty;
        private string _observations;
        private DateTime? _anulDate;
        private DateTime? _anulTime;
        public string _unmo;
        private CancellationControlContract _anulContract;
        private bool _taxIncluded;
        private bool _canceledSelected;
        private string _gerencialCode;
        private string _couponCode;
        private string _cupomFiscalCode;
        private string _davCode;
        private string _nonFiscalCode;
        private short _fiscalType;
        private string _clientName;
        private string _fiscalNumber;
        private string _address;
        private string _fiscalNoteMessage;
        private string _fiscalPrinterSerial;
        private string _fiscalPrinterFirmware;
        private string _fiscalPrinterCashier;
        private string _fiscalPrinterCode;
        private string _fiscalPrinterType;
        private string _fiscalPrinterBrand;
        private string _fiscalPrinterModel;
        private string _installationFiscalNumber;
        private Guid? _reservationTableId;
        private string _fpSerialInvoice;
        private long? _fpInvoiceNumber;
        private string _fpSerialCreditNote;
        private long? _fpCreditNoteNumber;
        private Guid? _taxSchemaId;
        private POSDocumentType _documentType;

		private string _baseCurrency;
		private decimal _exchangeFactor;

		#endregion
		#region Properties

		[DataMember]
        public POSDocumentType DocumentType
        {
            get
            {
                // Initialize fiscal doc type. This is needed in the case of olds POS-HUB, these do not initialize this property
                if (_documentType == default(POSDocumentType))
                {
                    _documentType = FiscalDocId.HasValue ? POSDocumentType.CashInvoice : POSDocumentType.Ticket;
                }
                return _documentType;
            }
            set { Set(ref _documentType, value, nameof(DocumentType)); }
        }

		[DataMember]
		public string BaseCurrency { get { return _baseCurrency; } set { Set(ref _baseCurrency, value, nameof(BaseCurrency)); } }
		[DataMember]
		public decimal ExchangeFactor { get { return _exchangeFactor; } set { Set(ref _exchangeFactor, value, nameof(ExchangeFactor)); } }

		[DataMember]
        public Guid? TaxSchemaId { get { return _taxSchemaId; } set { Set(ref _taxSchemaId, value, nameof(TaxSchemaId)); } }
        [DataMember]
        public Guid Stand { get { return _stand; } set { Set(ref _stand, value, "Stand"); } }
        [DataMember]
        public Guid Caja { get { return _caja; } set { Set(ref _caja, value, "Caja"); } }
        [DataMember]
        public Guid? CajaPave { get { return _cajaPave; } set { Set(ref _cajaPave, value, "CajaPave"); } }
        [DataMember]
        public string SeriePrex { get { return _seriePrex; } set { Set(ref _seriePrex, value, "SeriePrex"); } }
        [DataMember]
        public long? Serie { get { return _serie; } set { Set(ref _serie, value, "Serie"); } }
        [DataMember]
        public string Signature { get { return _signature; } set { Set(ref _signature, value, "Signature"); } }
        [DataMember]
        public long OpeningSerie { get { return _openingSerie; } set { Set(ref _openingSerie, value, nameof(OpeningSerie)); } }
        [DataMember]
        public DateTime? FiscalDateTime { get { return _fiscalDateTime; } set { Set(ref _fiscalDateTime, value, "FiscalDateTime"); } }
        [DataMember]
        public DateTime CheckOpeningDate { get { return _checkOpeningDate; } set { Set(ref _checkOpeningDate, value, "CheckOpeningDate"); } }
        [DataMember]
        public DateTime CheckOpeningTime { get { return _checkOpeningTime; } set { Set(ref _checkOpeningTime, value.ToUtcDateTime(), "CheckOpeningTime"); } }
        [DataMember]
        public DateTime? CloseDate { get { return _closeDate; } set { Set(ref _closeDate, value, "CloseDate"); } }
        [DataMember]
        public DateTime? CloseTime { get { return _closeTime; } set { Set(ref _closeTime, value != null ? value.Value.ToUtcDateTime() : value, "CloseTime"); } }
        [DataMember]
        public Guid? CloseUserId { get { return _closeUserId; } set { Set(ref _closeUserId, value, "CloseUserId"); } }
        [DataMember]
        public string CloseUserDescription { get { return _closeUserDescription; } set { Set(ref _closeUserDescription, value, "CloseUserDescription"); } }
        [DataMember]
        public Guid Util { get { return _util; } set { Set(ref _util, value, "Util"); } }
        [DataMember]
        public Guid? Anul { get { return AnulContract != null ? (Guid?)AnulContract.Id : null; } set { Set(ref _anul, value, "Anul"); } }
        [DataMember]
        public short Paxs { get { return _paxs; } set { Set(ref _paxs, value, "Paxs"); } }
        [DataMember]
        public int ReceiveCons { get { return _receiveCons; } set { Set(ref _receiveCons, value, "ReceiveCons"); } }
        [DataMember]
        public Guid? InternalUse { get { return _internalUse; } set { Set(ref _internalUse, value, "InternalUse"); } }
        [DataMember]
        public Guid? Account { get { return _account; } set { Set(ref _account, value, "Account"); } }
        [DataMember]
        public int Shift { get { return _shift; } set { Set(ref _shift, value, "Shift"); } }
        [DataMember]
        public Guid? Duty { get { return _duty; } set { Set(ref _duty, value, "Duty"); } }
        [DataMember]
        public decimal GeneralDiscount { get { return _generalDiscount; } set { Set(ref _generalDiscount, value, "GeneralDiscount"); } }
        [DataMember]
        public decimal Recharge { get { return _recharge; } set { Set(ref _recharge, value, "Recharge"); } }
        [DataMember]
        public string Observations { get { return _observations; } set { Set(ref _observations, value, "Observations"); } }
        [DataMember]
        public string Unmo { get { return _unmo; } set { Set(ref _unmo, value, "Unmo"); } }
        [DataMember]
        public DateTime? AnulDate { get { return _anulDate; } set { Set(ref _anulDate, value, "AnulDate"); } }
        [DataMember]
        public DateTime? AnulTime { get { return _anulTime; } set { Set(ref _anulTime, value, "AnulTime"); } }
        [DataMember]
        public Guid? Mesa { get { return _mesa; } set { Set(ref _mesa, value, "Mesa"); } }
        [DataMember]
        public Guid? FiscalDocId { get { return _fiscalDocId; } set { Set(ref _fiscalDocId, value, nameof(FiscalDocId)); } }
        [DataMember]
        public bool Process { get { return _process; } set { Set(ref _process, value, "Process"); } }
        [DataMember]
        public CancellationControlContract AnulContract { get { return _anulContract; } set { Set(ref _anulContract, value, "AnulContract"); } }
        [DataMember]
        public FactInformationContract FiscalDocInformationContract { get; set; }
        [DataMember]
        public bool TaxIncluded { get { return _taxIncluded; } set { Set(ref _taxIncluded, value, "TaxIncluded"); } }
        [DataMember]
        public string FiscalDoc { get; set; }
        [DataMember]
        public bool FromErrorTicket { get; set; }
        [DataMember]
        public string GerencialCode { get { return _gerencialCode; } set { Set(ref _gerencialCode, value, "GerencialCode"); } }
        [DataMember]
        public string CouponCode { get { return _couponCode; } set { Set(ref _couponCode, value, "CouponCode"); } }
        [DataMember]
        public string CupomFiscalCode { get { return _cupomFiscalCode; } set { Set(ref _cupomFiscalCode, value, "CupomFiscalCode"); } }
        [DataMember]
        public string DAVCode { get { return _davCode; } set { Set(ref _davCode, value, "DAVCode"); } }
        [DataMember]
        public string NonFiscalCode { get { return _nonFiscalCode; } set { Set(ref _nonFiscalCode, value, "NonFiscalCode"); } }
        [DataMember]
        public short FiscalType { get { return _fiscalType; } set { Set(ref _fiscalType, value, "FiscalType"); } }
        [DataMember]
        public string ClientName { get { return _clientName; } set { Set(ref _clientName, value, "ClientName"); } }
        [DataMember]
        public string FiscalNumber { get { return _fiscalNumber; } set { Set(ref _fiscalNumber, value, "FiscalNumber"); } }
        [DataMember]
        public string Address { get { return _address; } set { Set(ref _address, value, "Address"); } }
        [DataMember]
        public string FiscalNoteMessage { get { return _fiscalNoteMessage; } set { Set(ref _fiscalNoteMessage, value, "FiscalNoteMessage"); } }
        [DataMember]
        public string FiscalPrinterSerial { get { return _fiscalPrinterSerial; } set { Set(ref _fiscalPrinterSerial, value, "FiscalPrinterSerial"); } }
        [DataMember]
        public string FiscalPrinterFirmware { get { return _fiscalPrinterFirmware; } set { Set(ref _fiscalPrinterFirmware, value, "FiscalPrinterFirmware"); } }
        [DataMember]
        public string FiscalPrinterCashier { get { return _fiscalPrinterCashier; } set { Set(ref _fiscalPrinterCashier, value, "FiscalPrinterCashier"); } }
        [DataMember]
        public string FiscalPrinterCode { get { return _fiscalPrinterCode; } set { Set(ref _fiscalPrinterCode, value, "FiscalPrinterCode"); } }
        [DataMember]
        public string FiscalPrinterType { get { return _fiscalPrinterType; } set { Set(ref _fiscalPrinterType, value, "FiscalPrinterType"); } }
        [DataMember]
        public string FiscalPrinterBrand { get { return _fiscalPrinterBrand; } set { Set(ref _fiscalPrinterBrand, value, "FiscalPrinterBrand"); } }
        [DataMember]
        public string FiscalPrinterModel { get { return _fiscalPrinterModel; } set { Set(ref _fiscalPrinterModel, value, "FiscalPrinterModel"); } }
        [DataMember]
        public string InstallationFiscalNumber { get { return _installationFiscalNumber; } set { Set(ref _installationFiscalNumber, value, "InstallationFiscalNumber"); } }

        [DataMember]
        public string FpSerialInvoice { get { return _fpSerialInvoice; } set { Set(ref _fpSerialInvoice, value, nameof(FpSerialInvoice)); } }
        [DataMember]
        public long? FpInvoiceNumber { get { return _fpInvoiceNumber; } set { Set(ref _fpInvoiceNumber, value, nameof(FpInvoiceNumber)); } }
        [DataMember]
        public string FpSerialCreditNote { get { return _fpSerialCreditNote; } set { Set(ref _fpSerialCreditNote, value, nameof(FpSerialCreditNote)); } }
        [DataMember]
        public long? FpCreditNoteNumber { get { return _fpCreditNoteNumber; } set { Set(ref _fpCreditNoteNumber, value, nameof(FpCreditNoteNumber)); } }

        [DataMember]
        public string Error { get; set; }
        [DataMember]
        public bool Changed { get; set; }
        [DataMember]
        public bool DigitalMenu { get; set; }
        [DataMember]
        public string WorkStation { get; set; }

        [DataMember]
        public Guid? ReservationTableId { get { return _reservationTableId; } set { Set(ref _reservationTableId, value, "ReservationTableId"); } }

        [DataMember]
        public string FiscalDocSignature { get; set; }
        [DataMember]
        public Blob? FiscalDocQrCode { get; set; }
        [DataMember]
        public string FiscalDocValidationCode { get; set; }
        [DataMember]
        public short FiscalDocPrints { get; set; }

        [DataMember]
        public DateTime? CreditNoteSystemDate { get; set; }
        [DataMember]
        public DateTime? CreditNoteWorkDate { get; set; }
        [DataMember]
        public Guid? CreditNoteUserId { get; set; }
        [DataMember]
        public string CreditNoteSignature { get; set; }
        [DataMember]
        public Blob? CreditNoteQrCode { get; set; }
        [DataMember]
        public string CreditNoteValidationCode { get; set; }
        [DataMember]
        public short CreditNotePrints { get; set; }

        [DataMember]
        public string FiscalDocSing { get { return FiscalDocSignature; } set { FiscalDocSignature = value; } }
        [DataMember]
        public Blob? QrCode { get { return FiscalDocQrCode; } set { FiscalDocQrCode = value; } }
        [DataMember]
        public string ValidationCode { get { return FiscalDocValidationCode; } set { FiscalDocValidationCode = value; } }
        [DataMember]
        public short Prints { get; set; }

        [ReflectionExclude]
        public string SerieName
        {
            get
            {
                var serieName = string.Empty;
                if (Serie.HasValue)
                    serieName += Serie.Value.ToString();
                if (!string.IsNullOrEmpty(SeriePrex))
                    serieName += "/" + SeriePrex;

                return serieName;
            }
        }

        [ReflectionExclude]
        public decimal Valor
        {
            get { return ProductLines.Where(x => (x.IsAnul == 0 || x.IsAnul == 2) || x.AnulAfterClosed).Sum(x => x.Importe); }
        }

        [ReflectionExclude]
        public decimal ValorCancelado
        {
            get { return ProductLines.Where(x => !x.AnulAfterClosed && !(x.IsAnul == 0 || x.IsAnul == 2)).Sum(x => x.Importe); }
        }

        [ReflectionExclude]
        public bool CanceledSelected
        {
            get { return _canceledSelected; }
            set { Set<bool>(ref _canceledSelected, value, "CanceledSelected"); }
        }

        [ReflectionExclude]
        public Guid[] ProductLinesIds
        {
            get { return ProductLines.Select(x => (Guid)x.Id).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] PaymentLinesIds
        {
            get { return PaymentLines.Select(x => (Guid)x.Id).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] LookupTablesIds
        {
            get { return LookupTables.Select(x => (Guid)x.Id).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] LogsIds
        {
            get { return Logs.Select(x => (Guid)x.Id).ToArray(); }
        }

        [ReflectionExclude]
        public DateTime WorkDate
        {
            get { return CheckOpeningDate; }
        }

        [ReflectionExclude]
        public string OpenDateShort
        {
            get { return CheckOpeningTime.ToShortDateString(); }
        }

        [ReflectionExclude]
        public string OpenTimeShort
        {
            get { return CheckOpeningTime.ToShortTimeString(); }
        }

        [ReflectionExclude]
        public string CloseDateShort
        {
            get
            {
                if (CloseTime.HasValue)
                    return CloseTime.Value.ToShortDateString();

                return string.Empty;
            }
        }

        [ReflectionExclude]
        public string CloseTimeShort
        {
            get
            {
                if (CloseTime.HasValue)
                    return CloseTime.Value.ToShortTimeString();

                return string.Empty;
            }
        }

        [ReflectionExclude]
        public bool HasValidPayments
        {
            get
            {
                if (PaymentLines == null)
                    return false;

                return PaymentLines.Exists(x => x.Payment.HasValue && x.Payment.Value != Guid.Empty);
            }
        }
        [ReflectionExclude]
        public bool IsOnlyTicket => DocumentType == POSDocumentType.Ticket;
        [ReflectionExclude]
        public bool IsBallot => DocumentType == POSDocumentType.Ballot;
        [ReflectionExclude]
        public bool IsInvoice => DocumentType == POSDocumentType.CashInvoice;

        #endregion
        #region Contracts

        [DataMember]
        public TypedList<ProductLineContract> ProductLines { get; set; }
        [DataMember]
        public TypedList<PaymentLineContract> PaymentLines { get; set; }
        [DataMember]
        public TypedList<LookupTableContract> LookupTables { get; set; }
        [DataMember]
        public TypedList<LogTicketContract> Logs { get; set; }

        long ITicketContract.Shift
        {
            get
            {
                return Shift;
            }
        }

        IEnumerable<IPaymentLineContract> ITicketContract.PaymentLines
        {
            get
            {
                return PaymentLines;
            }
        }

        IEnumerable<IProductLineContract> ITicketContract.ProductLines
        {
            get
            {
                return ProductLines;
            }
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateTicketR(TicketRContract obj)
        {
            if (obj.CanceledSelected && !obj.AnulContract.CancellationReasonId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Cancellation Reason required");
            if (obj.FromErrorTicket && !obj.AnulContract.CancellationReasonId.HasValue && obj.PaymentLines != null && !obj.PaymentLines.Any())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Payments required");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
