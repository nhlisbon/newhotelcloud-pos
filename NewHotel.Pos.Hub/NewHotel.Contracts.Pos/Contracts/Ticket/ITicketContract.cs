﻿using System;
using System.Collections.Generic;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts.Pos
{
    public interface ICanExchangeCurrency
	{
		string BaseCurrency { get; set; }
		decimal ExchangeFactor { get; set; }
	}

    public interface ITicketContract : IBaseObject, ICanExchangeCurrency
	{
        DateTime? CloseDate { get; }
        string SerieName { get; }
        long Shift { get; }
        bool TaxIncluded { get; }
        string Unmo { get; }
        Guid? TaxSchemaId { get; }

        IEnumerable<IPaymentLineContract> PaymentLines { get; }
        IEnumerable<IProductLineContract> ProductLines { get; }
    }

    public interface IProcess
    {
        bool Process { get; set; }
    }

    public interface IPaymentLineContract : IBaseObject, IProcess
    {
        Guid? PaymentId { get; }
        ReceivableType ReceivableType { get; }
        bool IsCreditCard { get; }
        Guid? CreditCardId { get; }
        decimal Value { get; }
        ICreditCardContract CreditCardContract { get; }
    }

    public interface IProductLineContract : IBaseObject, IProcess
    {
        bool Aditional { get; }
        bool AnulAfterClosed { get; }
        Guid Caja { get; }
        decimal Cost { get; }
        decimal? Discount { get; }
        decimal? DiscountPercent { get; }
        decimal Importe { get; }
        int IsAnul { get; }
        bool IsHappyHour { get; }
        short ItemNumber { get; }
        decimal LiqValue { get; }
        Guid? MainProduct { get; }
        short SendToAreaStatus { get; }
        DateTime? SendToAreaStartTime { get; }
        DateTime? SendToAreaEndTime { get; }
        string Observations { get; }
        string ObservationsMarch { get; set; }
        short Printed { get; }
        Guid Product { get; }
        string ProductDesc { get; }
        decimal ProductQtd { get; }
        decimal Recharge { get; }
        Guid? Separator { get; }
        Guid Stand { get; }
        Guid FirstIva { get; }
        decimal FirstIvas { get; }
        Guid? SecodIva { get; }
        decimal? SecondIvas { get; }
        Guid? ThirdIva { get; }
        decimal? ThirdIvas { get; }
        Guid Ticket { get; }
        Guid Util { get; }
        decimal ValueBeforeDiscount { get; }
        short? PaxNumber { get; }
        bool? Alcoholic { get; }
        string DiscountTypeDescription { get; }
    }
}