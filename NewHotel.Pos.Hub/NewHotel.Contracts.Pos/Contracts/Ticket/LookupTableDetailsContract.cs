﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class LookupTableDetailsContract : BaseContract, IProductLineTax
    {
        #region Members

        private Guid _lookupTableId;
        private Guid _productId;
        private decimal _quantity;
        private decimal _netValue;
        private decimal _totalValue;
        private short _annulmentCode;
        private Guid _firstIvaId;
        private decimal _firstIvaPercent;
        private decimal _firstIvaValue;
        private Guid? _secondIvaId;
        private decimal? _secondIvaPercent;
        private decimal? _secondIvaValue;
        private Guid? _thirdIvaId;
        private decimal? _thirdIvaPercent;
        private decimal? _thirdIvaValue;

        #endregion
        #region Constructor

        public LookupTableDetailsContract()
            : base()
        {
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid LookupTableId { get { return _lookupTableId; } set { Set(ref _lookupTableId, value, "LookupTableId"); } }
        [DataMember]
        public Guid ProductId { get { return _productId; } set { Set(ref _productId, value, "ProductId"); } }
        [DataMember]
        public decimal Quantity { get { return _quantity; } set { Set(ref _quantity, value, "Quantity"); } }
        [DataMember]
        public decimal NetPrice { get { return _netValue; } set { Set(ref _netValue, value, "NetPrice"); } }
        [DataMember]
        public decimal TotalPrice { get { return _totalValue; } set { Set(ref _totalValue, value, "TotalPrice"); } }
        [DataMember]
        public short AnnulmentCode { get { return _annulmentCode; } set { Set(ref _annulmentCode, value, "AnnulmentCode"); } }

        [DataMember]
        public Guid FirstIvaId { get { return _firstIvaId; } set { Set(ref _firstIvaId, value, "FirstIvaId"); } }
        [DataMember]
        public decimal FirstIvaPercent { get { return _firstIvaPercent; } set { Set(ref _firstIvaPercent, value, "FirstIvaPercent"); } }
        [DataMember]
        public decimal FirstIvaValue { get { return _firstIvaValue; } set { Set(ref _firstIvaValue, value, "FirstIvaValue"); } }

        [DataMember]
        public Guid? SecondIvaId { get { return _secondIvaId; } set { Set(ref _secondIvaId, value, "SecondIvaId"); } }
        [DataMember]
        public decimal? SecondIvaPercent { get { return _secondIvaPercent; } set { Set(ref _secondIvaPercent, value, "SecondIvaPercent"); } }
        [DataMember]
        public decimal? SecondIvaValue { get { return _secondIvaValue; } set { Set(ref _secondIvaValue, value, "SecondIvaValue"); } }

        [DataMember]
        public Guid? ThirdIvaId { get { return _thirdIvaId; } set { Set(ref _thirdIvaId, value, "ThirdIvaId"); } }
        [DataMember]
        public decimal? ThirdIvaPercent { get { return _thirdIvaPercent; } set { Set(ref _thirdIvaPercent, value, "ThirdIvaPercent"); } }
        [DataMember]
        public decimal? ThirdIvaValue { get { return _thirdIvaValue; } set { Set(ref _thirdIvaValue, value, "ThirdIvaValue"); } }

        #endregion
    }
}