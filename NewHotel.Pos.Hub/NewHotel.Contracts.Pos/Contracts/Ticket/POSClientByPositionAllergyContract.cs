﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(POSClientByPositionAllergyContract), nameof(POSClientByPositionAllergyContract.ValidateClientByPositionAllergy))]
    [KnownType(typeof(POSClientByPositionAllergyContract))]
    public class POSClientByPositionAllergyContract : BaseContract
    {
        #region Variables

        private Guid _allergyId;
        private string _description;

        #endregion
        #region Constructor

        public POSClientByPositionAllergyContract()
            : base()
        {
        }

        public POSClientByPositionAllergyContract(Guid id, Guid allergyId, string description)
            : this()
        {
            Id = id;
            AllergyId = allergyId;
            Description = description;
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid AllergyId { get { return _allergyId; } set { Set(ref _allergyId, value, nameof(AllergyId)); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, nameof(Description)); } }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateClientByPositionAllergy(POSClientByPositionAllergyContract obj)
        {
            if (string.IsNullOrEmpty(obj.Description))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description cannot be empty.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}