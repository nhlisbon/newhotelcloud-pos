﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.Contracts.Pos;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public interface IProductLineTax
    {
        decimal NetPrice { get; }

        Guid FirstIvaId { get; }
        decimal FirstIvaPercent { get; }
        decimal FirstIvaValue { get; }

        Guid? SecondIvaId { get; }
        decimal? SecondIvaPercent { get; }
        decimal? SecondIvaValue { get; }

        Guid? ThirdIvaId { get; }
        decimal? ThirdIvaPercent { get; }
        decimal? ThirdIvaValue { get; }
    }

    [DataContract]
    [Serializable]
    [CustomValidation(typeof(POSProductLineContract), nameof(POSProductLineContract.ValidateProductLine))]
    [KnownType(typeof(POSProductLineContract))]
    public class POSProductLineContract : BaseContract<Guid>, IProductLineContract, IProductLineTax
    {
        #region Members

        private Guid _ticket;
        private Guid _stand;
        private Guid _caja;
        private Guid _product;
        private Guid? _separator;
        private short? _separatorOrder;
        private decimal _productQtd;
        private decimal _grossValue;
        private decimal _netValue;
        private decimal _costValue;
        private decimal _discountValue;
        private decimal? _discountPercent;
        private string _discountTypeDescription;
        private bool _automaticDiscount;
        private decimal _valueBeforeDiscount;
        private decimal _recharge;
        private decimal _grossValueInBase;
        private Guid _userId;
        private Guid? _annulUserId;
        private Guid _firstIvaId;
        private string _firstIvaDescription;
        private decimal _firstIvasBase;
        private decimal _firstIvaPercent;
        private decimal _firstIvasValue;
        private string _firstIvaAuxCode;
        private Guid? _secondIvaId;
        private string _secondIvaDescription;
        private decimal? _secondIvaBase;
        private decimal? _secondIvaPercent;
        private decimal? _secondIvaValue;
        private string _secondIvaAuxCode;
        private Guid? _thirdIvaId;
        private string _thridIvaDescription;
        private decimal? _thirdIvaBase;
        private decimal? _thirdIvaPercent;
        private decimal? _thirdIvaValue;
        private string _thirdIvaAuxCode;
        private short _annulmentCode;
        private bool _anulAfterClosed;
        private bool _isSendedToArea;
        private short _sendToAreaStatus;
        private DateTime? _sendToAreaStartTime;
        private DateTime? _sendToAreaEndTime;
        private short _printed;
        private Guid? _areaId;
        private bool _isHappyHour;
        private bool _isAditional;
        private Guid? _mainProduct;
        private string _observations;
        private string _observationsMarch;
        private Guid? _discountTypeId;
        private bool _hasManualSeparator;
        private bool _hasManualPrice;
        private bool _isTip;
        private short? _paxNumber;
        private string _productCode;
        private string _productAbbreviation;
        private string _productDescription;
        private string _separatorDescription;
        private string _userDescription;
        private ARGBColor _productColor;
        private decimal? _productNet;
        private decimal? _productGross;
        private short _itemNumber;
        private bool? _process;
        private bool? _alcoholic;
        private string _manualPriceDesc;
        private string _descIsentoIva;
        private string _codeIsentoIva;
        private bool _isLaunchFromSpa;

        private TypedList<POSProductLinePreparationContract> _preparations;
        private TypedList<POSProductLineAreaContract> _areas;
        private TypedList<TableLineContract> _tableLines;

        #endregion
        #region Constructors

        public POSProductLineContract()
            : base()
        {
            Preparations = new TypedList<POSProductLinePreparationContract>();
            Areas = new TypedList<POSProductLineAreaContract>();
            TableLines = new TypedList<TableLineContract>();
        }

        #endregion
        #region Properties

        #region Product

        [DataMember]
        public Guid Product { get { return _product; } set { Set(ref _product, value, nameof(Product)); } }
        [DataMember]
        public string ProductCode { get { return _productCode; } set { Set(ref _productCode, value, nameof(ProductCode)); } }
        [DataMember]
        public string ProductAbbreviation { get { return _productAbbreviation; } set { Set(ref _productAbbreviation, value, nameof(ProductAbbreviation)); } }
        [DataMember]
        public string ProductDescription { get { return _productDescription; } set { Set(ref _productDescription, value, nameof(ProductDescription)); } }
        [DataMember]
        public string ProductImage { get { return _userDescription; } set { Set(ref _userDescription, value, nameof(ProductImage)); } }
        [DataMember]
        public short ItemNumber { get { return _itemNumber; } set { Set(ref _itemNumber, value, nameof(ItemNumber)); } }
        [DataMember]
        public ARGBColor ProductColor { get { return _productColor; } set { Set(ref _productColor, value, nameof(ProductColor)); } }
        [DataMember]
        public decimal? ProductNet { get { return _productNet; } set { Set(ref _productNet, value, nameof(ProductNet)); } }
        [DataMember]
        public decimal? ProductGross { get { return _productGross; } set { Set(ref _productGross, value, nameof(ProductGross)); } }

        #endregion
        #region Tax Rates

        #region First

        [DataMember]
        public Guid FirstIvaId { get { return _firstIvaId; } set { Set(ref _firstIvaId, value, nameof(FirstIvaId)); } }
        [DataMember]
        public string FirstIvaAuxCode { get { return _firstIvaAuxCode; } set { Set(ref _firstIvaAuxCode, value, nameof(FirstIvaAuxCode)); } }
        [DataMember]
        public string FirstIvaDescription { get { return _firstIvaDescription; } set { Set(ref _firstIvaDescription, value, nameof(FirstIvaDescription)); } }
        [DataMember]
        public decimal FirstIvaPercent { get { return _firstIvaPercent; } set { Set(ref _firstIvaPercent, value, nameof(FirstIvaPercent)); } }
        [DataMember]
        public decimal FirstIvaBase { get { return _firstIvasBase; } set { Set(ref _firstIvasBase, value, nameof(FirstIvaBase)); } }
        [DataMember]
        public decimal FirstIvaValue { get { return _firstIvasValue; } set { Set(ref _firstIvasValue, value, nameof(FirstIvaValue)); } }

        #endregion
        #region Second

        [DataMember]
        public Guid? SecondIvaId { get { return _secondIvaId; } set { Set(ref _secondIvaId, value, nameof(SecondIvaId)); } }
        [DataMember]
        public string SecondIvaAuxCode { get { return _secondIvaAuxCode; } set { Set(ref _secondIvaAuxCode, value, nameof(SecondIvaAuxCode)); } }
        [DataMember]
        public string SecondIvaDescription { get { return _secondIvaDescription; } set { Set(ref _secondIvaDescription, value, nameof(SecondIvaDescription)); } }
        [DataMember]
        public decimal? SecondIvaPercent { get { return _secondIvaPercent; } set { Set(ref _secondIvaPercent, value, nameof(SecondIvaPercent)); } }
        [DataMember]
        public decimal? SecondIvaBase { get { return _secondIvaBase; } set { Set(ref _secondIvaBase, value, nameof(SecondIvaBase)); } }
        [DataMember]
        public decimal? SecondIvaValue { get { return _secondIvaValue; } set { Set(ref _secondIvaValue, value, nameof(SecondIvaValue)); } }

        #endregion
        #region Third

        [DataMember]
        public Guid? ThirdIvaId { get { return _thirdIvaId; } set { Set(ref _thirdIvaId, value, nameof(ThirdIvaId)); } }
        [DataMember]
        public string ThirdIvaAuxCode { get { return _thirdIvaAuxCode; } set { Set(ref _thirdIvaAuxCode, value, nameof(ThirdIvaAuxCode)); } }
        [DataMember]
        public string ThirdIvaDescription { get { return _thridIvaDescription; } set { Set(ref _thridIvaDescription, value, nameof(ThirdIvaDescription)); } }
        [DataMember]
        public decimal? ThirdIvaPercent { get { return _thirdIvaPercent; } set { Set(ref _thirdIvaPercent, value, nameof(ThirdIvaPercent)); } }
        [DataMember]
        public decimal? ThirdIvaValue { get { return _thirdIvaValue; } set { Set(ref _thirdIvaValue, value, nameof(ThirdIvaValue)); } }
        [DataMember]
        public decimal? ThirdIvaBase { get { return _thirdIvaBase; } set { Set(ref _thirdIvaBase, value, nameof(ThirdIvaBase)); } }

        #endregion

        #endregion
        #region User

        [DataMember]
        public Guid UserId { get { return _userId; } set { Set(ref _userId, value, nameof(UserId)); } }
        [DataMember]
        public string UserDescription { get { return _userDescription; } set { Set(ref _userDescription, value, nameof(UserDescription)); } }
        [DataMember]
        public Guid? AnnulUserId { get { return _annulUserId; } set { Set(ref _annulUserId, value, nameof(AnnulUserId)); } }

        #endregion
        #region Area

        [DataMember]
        public Guid? AreaId { get { return _areaId; } set { Set(ref _areaId, value, nameof(AreaId)); } }
        [DataMember]
        public bool IsSendedToArea { get { return _isSendedToArea; } set { Set(ref _isSendedToArea, value, nameof(IsSendedToArea)); } }
        [DataMember]
        public short SendToAreaStatus { get { return _sendToAreaStatus; } set { Set(ref _sendToAreaStatus, value, nameof(SendToAreaStatus)); } }
        [DataMember]
        public DateTime? SendToAreaStartTime { get { return _sendToAreaStartTime; } set { Set(ref _sendToAreaStartTime, value, nameof(SendToAreaStartTime)); } }
        [DataMember]
        public DateTime? SendToAreaEndTime { get { return _sendToAreaEndTime; } set { Set(ref _sendToAreaEndTime, value, nameof(SendToAreaEndTime)); } }

        #endregion
        #region Separator

        [DataMember]
        public Guid? Separator { get { return _separator; } set { Set(ref _separator, value, nameof(Separator)); } }
        [DataMember]
        public short? SeparatorOrder { get { return _separatorOrder; } set { Set(ref _separatorOrder, value, nameof(SeparatorOrder)); } }
        [DataMember]
        public string SeparatorDescription { get { return _separatorDescription; } set { Set(ref _separatorDescription, value, nameof(SeparatorDescription)); } }

        #endregion

        [DataMember]
        public Guid Ticket { get { return _ticket; } set { Set(ref _ticket, value, nameof(Ticket)); } }
        [DataMember]
        public Guid Stand { get { return _stand; } set { Set(ref _stand, value, nameof(Stand)); } }
        [DataMember]
        public Guid Caja { get { return _caja; } set { Set(ref _caja, value, nameof(Caja)); } }
        [DataMember]
        public decimal ProductQtd { get { return _productQtd; } set { Set(ref _productQtd, value, nameof(ProductQtd)); } }
        [DataMember]
        public decimal GrossValue { get { return _grossValue; } set { Set(ref _grossValue, value, nameof(GrossValue)); } }
        [DataMember]
        public decimal NetValue { get { return _netValue; } set { Set(ref _netValue, value, nameof(NetValue)); } }
        [DataMember]
        public decimal CostValue { get { return _costValue; } set { Set(ref _costValue, value, nameof(CostValue)); } }
        [DataMember]
        public decimal DiscountValue { get { return _discountValue; } set { Set(ref _discountValue, value, nameof(DiscountValue)); } }
        [DataMember]
        public decimal? DiscountPercent { get { return _discountPercent; } set { Set(ref _discountPercent, value, nameof(DiscountPercent)); } }
        [DataMember]
        public string DiscountTypeDescription { get { return _discountTypeDescription; } set { Set(ref _discountTypeDescription, value, nameof(DiscountTypeDescription)); } }
        [DataMember]
        public bool AutomaticDiscount { get { return _automaticDiscount; } set { Set(ref _automaticDiscount, value, nameof(AutomaticDiscount)); } }
        [DataMember]
        public decimal ValueBeforeDiscount { get { return _valueBeforeDiscount; } set { Set(ref _valueBeforeDiscount, value, nameof(ValueBeforeDiscount)); } }
        [DataMember]
        public decimal Recharge { get { return _recharge; } set { Set(ref _recharge, value, nameof(Recharge)); } }
		[DataMember]
		public decimal GrossValueInBase 
        { 
            get { return _grossValueInBase; } 
            set { Set(ref _grossValueInBase, value, nameof(GrossValueInBase)); } 
        }
		[DataMember]
        public short AnnulmentCode { get { return _annulmentCode; } set { Set(ref _annulmentCode, value, nameof(AnnulmentCode)); } }
        [DataMember]
        public bool AnulAfterClosed { get { return _anulAfterClosed; } set { Set(ref _anulAfterClosed, value, nameof(AnulAfterClosed)); } }
        [DataMember]
        public short Printed { get { return _printed; } set { Set(ref _printed, value, nameof(Printed)); } }
        [DataMember]
        public bool IsHappyHour { get { return _isHappyHour; } set { Set(ref _isHappyHour, value, nameof(IsHappyHour)); } }
        [DataMember]
        public bool IsAditional { get { return _isAditional; } set { Set(ref _isAditional, value, nameof(IsAditional)); } }
        [DataMember]
        public Guid? MainProduct { get { return _mainProduct; } set { Set(ref _mainProduct, value, nameof(MainProduct)); } }
        [DataMember]
        public string Observations { get { return _observations; } set { Set(ref _observations, value, nameof(Observations)); } }
        [DataMember]
        public string ObservationsMarch { get { return _observationsMarch; } set { Set(ref _observationsMarch, value, nameof(ObservationsMarch)); } }
        [DataMember]
        public Guid? DiscountTypeId { get { return _discountTypeId; } set { Set(ref _discountTypeId, value, nameof(DiscountTypeId)); } }
        [DataMember]
        public bool HasManualSeparator { get { return _hasManualSeparator; } set { Set(ref _hasManualSeparator, value, nameof(HasManualSeparator)); } }
        [DataMember]
        public bool HasManualPrice { get { return _hasManualPrice; } set { Set(ref _hasManualPrice, value, nameof(HasManualPrice)); } }
        [DataMember]
        public bool IsTip { get { return _isTip; } set { Set(ref _isTip, value, nameof(IsTip)); } }
        [DataMember]
        public short? PaxNumber { get { return _paxNumber; } set { Set(ref _paxNumber, value, nameof(PaxNumber)); } }
        [DataMember]
        public bool? Alcoholic { get { return _alcoholic; } set { Set(ref _alcoholic, value, nameof(Alcoholic)); } }
        [DataMember]
        public string ManualPriceDesc { get { return _manualPriceDesc; } set { Set(ref _manualPriceDesc, value, nameof(ManualPriceDesc)); } }
        [DataMember]
        public string DescIsentoIva { get { return _descIsentoIva; } set { Set(ref _descIsentoIva, value, nameof(DescIsentoIva)); } }
        [DataMember]
        public string CodeIsentoIva { get { return _codeIsentoIva; } set { Set(ref _codeIsentoIva, value, nameof(CodeIsentoIva)); } }
        [DataMember]
        public bool IsLaunchFromSpa { get { return _isLaunchFromSpa; } set { Set(ref _isLaunchFromSpa, value, nameof(IsLaunchFromSpa)); } }


        #region Navigation

        [ReflectionExclude]
        [DataMember]
        public TypedList<POSProductLinePreparationContract> Preparations { get { return _preparations; } set { Set(ref _preparations, value, nameof(Preparations)); } }
        [ReflectionExclude]
        [DataMember]
        public TypedList<POSProductLineAreaContract> Areas { get { return _areas; } set { Set(ref _areas, value, nameof(Areas)); } }
        [ReflectionExclude]
        [DataMember]
        public TypedList<TableLineContract> TableLines { get { return _tableLines; } set { Set(ref _tableLines, value, nameof(TableLines)); } }

        #endregion
        #region Calculated Properties

        public bool HasAreas
        {
            get { return AreaId.HasValue || Areas.Count > 0; }
        }

        public bool IsActive
        { 
            get { return AnnulmentCode == 0 || AnnulmentCode == 2; }
        }

        public decimal ProductValueBeforeDiscount
        {
            get { return ProductQtd > decimal.Zero ? ValueBeforeDiscount / ProductQtd : decimal.Zero; }
        }

        #endregion

        #endregion
        #region IProductLine Implementation

        public int IsAnul
        {
            get { return AnnulmentCode; }
        }

        public bool Aditional
        {
            get { return IsAditional; }
        }

        public decimal Cost
        {
            get { return CostValue; }
        }

        public decimal? Discount
        {
            get { return DiscountValue; }
        }

        public Guid FirstIva
        {
            get { return FirstIvaId; }
        }

        public decimal FirstIvas
        {
            get { return FirstIvaValue; }
        }

        public Guid? SecodIva
        {
            get { return SecondIvaId; }
        }

        public decimal? SecondIvas
        {
            get { return SecondIvaValue; }
        }

        public Guid? ThirdIva
        {
            get { return ThirdIvaId; }
        }

        public decimal? ThirdIvas
        {
            get { return ThirdIvaValue; }
        }

        public decimal Importe 
        {
            get { return GrossValue; }
        }

        public decimal LiqValue
        {
            get { return NetValue; }
        }

        public string ProductDesc
        {
            get { return ProductDescription; }
        }

        public Guid Util
        {
            get { return UserId; }
        }

        public bool Process
        {
            get { return _process ?? (_process = true).Value; }
            set { _process = value; }
        }

        #endregion
        #region IProductLineTax Implementation

        public decimal NetPrice
        {
            get { return ProductNet ?? decimal.Zero; }
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateProductLine(ProductLineContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}