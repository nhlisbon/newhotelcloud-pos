﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
     [DataContract]
	[Serializable]
    public class TicketSerieContract:BaseContract
    {
          #region Members
         private string _textPref;
         private long? _numberPref;
         private long _number;
         private long _orden;
         private long _status;
         private DateTime? _finalDate;
         private long? _finalNumber;
         private string _signature;

        #endregion

        #region Constructor
        public TicketSerieContract()
            : base()
        {
        }
        #endregion

        #region Properties

        [DataMember]
        public string TextPref { get { return _textPref; } set { Set(ref _textPref, value, "TextPref"); } }
        [DataMember]
        public long? NumberPref { get { return _numberPref; } set { Set(ref _numberPref, value, "NumberPref"); } }
        [DataMember]
        public long Number { get { return _number; } set { Set(ref _number, value, "Number"); } }
        [DataMember]
        public long Orden { get { return _orden; } set { Set(ref _orden, value, "Orden"); } }
        [DataMember]
        public long Status { get { return _status; } set { Set(ref _status, value, "Status"); } }
        [DataMember]
        public DateTime? FinalDate { get { return _finalDate; } set { Set(ref _finalDate, value, "FinalDate"); } }
        [DataMember]
        public long? FinalNumber { get { return _finalNumber; } set { Set(ref _finalNumber, value, "FinalNumber"); } }
        [DataMember]
        public string Signature { get { return _signature; } set { Set(ref _signature, value, "Signature"); } }

        #endregion

        #region DummyProperties
        [DataMember]
        public bool CajaStand { get; set; }
        [DataMember]
        public string Serie { get; set; }
        #endregion
    }
}
