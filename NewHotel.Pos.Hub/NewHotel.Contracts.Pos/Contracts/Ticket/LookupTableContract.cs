﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(LookupTableContract), "ValidateLookupTable")]
    public class LookupTableContract : BaseContract
    {
        #region Members

        private Guid _stand;
        private Guid _caja;
        private DateTime _workDate;
        private DateTime _systemDateTime;
        private string _signature;
        private Guid _user;
        private decimal _total;
        private string _serie;
        private long _number;
        private string _cooCode;
        private string _cerCode;
        private Guid _ticket;
        private string _validationCode;
        private Blob? _qrCode;
        private string _qrCodeData;

        #endregion
        #region Constructor

        public LookupTableContract()
            : base()
        {
            Details = new TypedList<LookupTableDetailsContract>();
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid Stand { get { return _stand; } set { Set(ref _stand, value, "Stand"); } }
        [DataMember]
        public Guid Caja { get { return _caja; } set { Set(ref _caja, value, "Caja"); } }
        [DataMember]
        public DateTime WorkDate { get { return _workDate; } set { Set(ref _workDate, value, "WorkDate"); } }
        [DataMember]
        public DateTime SystemDateTime { get { return _systemDateTime; } set { Set(ref _systemDateTime, value, "SystemDateTime"); } }
        [DataMember]
        public string Signature { get { return _signature; } set { Set(ref _signature, value, "Signature"); } }
        [DataMember]
        public Guid User { get { return _user; } set { Set(ref _user, value, "User"); } }
        [DataMember]
        public decimal Total { get { return _total; } set { Set(ref _total, value, "Total"); } }
        [DataMember]
        public string Serie { get { return _serie; } set { Set(ref _serie, value, "Serie"); } }      
        [DataMember]
        public long Number { get { return _number; } set { Set(ref _number, value, "Number"); } }
        [DataMember]
        public string COOCode { get { return _cooCode; } set { Set(ref _cooCode, value, "COOCode"); } }
        [DataMember]
        public string CERCode { get { return _cerCode; } set { Set(ref _cerCode, value, "CERCode"); } }
        [DataMember]
        public Guid Ticket { get { return _ticket; } set { Set(ref _ticket, value, "Ticket"); } }
        [DataMember]
        public string ValidationCode { get { return _validationCode; } set { Set(ref _validationCode, value, "ValidationCode"); } }
        [DataMember]
        public Blob? QrCode { get { return _qrCode; } set { Set(ref _qrCode, value, "QrCode"); } }
        [DataMember]
        public string QrCodeData { get { return _qrCodeData; } set { Set(ref _qrCodeData, value, "QrCodeData"); } } 

        #endregion
        #region Lists

        [DataMember]
        [ReflectionExclude]
        public TypedList<LookupTableDetailsContract> Details { get; set; }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidatePaymentLine(LookupTableContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
