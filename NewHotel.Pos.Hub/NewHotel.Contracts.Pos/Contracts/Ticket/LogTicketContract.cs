﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(LogTicketContract), "ValidateLogTicket")]
    public class LogTicketContract : BaseContract
    {
          #region Members
        private Guid _ticket;
        private long _tope;
        private Guid _util;
        private DateTime _time;
        private DateTime _workDate;
        private string _operationDetails;
        #endregion

        #region Constructor
        public LogTicketContract()
            : base()
        {
           
        }
        public LogTicketContract(Guid id, Guid ticket,long tope, Guid util, DateTime time, DateTime workDate, string details)
            : base()
        {
            this.Id = id;
            this.Ticket = ticket;
            this.Tope = tope;
            this.Util = util;
            this.Time = time;
            this.WorkDate = workDate;
            this.OperationDetails = details;
        }
        #endregion

        #region Properties



      [DataMember]
       public Guid Ticket{ get { return _ticket; } set { Set(ref _ticket, value, "Ticket"); } }
         [DataMember]
        public long Tope { get { return _tope; } set { Set(ref _tope, value, "Tope"); } }
         [DataMember]
         public Guid Util { get { return _util; } set { Set(ref _util, value, "Util"); } }
         [DataMember]
         public DateTime Time { get { return _time; } set { Set(ref _time, value, "Time"); } }
         [DataMember]
         public DateTime WorkDate { get { return _workDate; } set { Set(ref _workDate, value, "WorkDate"); } }
         [DataMember]
         public string OperationDetails { get { return _operationDetails; } set { Set(ref _operationDetails, value, "OperationDetails"); } }

        #endregion

        #region Validations

         public static System.ComponentModel.DataAnnotations.ValidationResult ValidateLogTicket(LogTicketContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
