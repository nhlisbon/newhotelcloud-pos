﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SerieContract : BaseContract
    {
        [DataMember]
        public DateTime WorkDate { get; set; }
        [DataMember]
        public DateTime EmissionDateTime { get; set; }
        [DataMember]
        public long Consecutive { get; set; }
        [DataMember]
        public string Serie { get; set; }
    }
}