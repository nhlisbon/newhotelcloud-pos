﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;
using System.Xml.Serialization;
using System.IO;
using NewHotel.Core;
using System.Globalization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(TicketsErrorContract), "ValidateCaja")]
    public class TicketsErrorContract : BaseContract
    {
        #region Members
        private Guid _stand;
        private Guid _caja;
        private Guid _user;
        private Blob _ticketDetails;
        private DateTime _workdate;
        private DateTime _time;
        private string _error;
        private string _seriePrex;
        private long? _serie;
       
        #endregion

        #region Constructor
        public TicketsErrorContract()
            : base()
        {
            
        }

        public TicketsErrorContract(Guid id, Guid stand, Guid caja, Guid user, Blob ticketDetails, DateTime workDate, DateTime time, string error, long? serie, string seriePrex, TicketRContract contract)
            : base()
        {
            this.Id = id;
            this.Stand = stand;
            this.Caja = caja;
            this.User = user;
            this.TicketDetails = ticketDetails;
            this.Workdate = workDate;
            this.Time = time;
            this.Error = error;
            this.Serie = serie;
            this.SeriePrex = seriePrex;
            this.TicketContract = contract;
        }
        #endregion

        #region Properties

        [DataMember]
        public Guid Stand { get { return _stand; } set { Set(ref _stand, value, "Stand"); } }
        [DataMember]
        public Guid Caja { get { return _caja; } set { Set(ref _caja, value, "Caja"); } }
        [DataMember]
        public Guid User { get { return _user; } set { Set(ref _user, value, "User"); } }
        [DataMember]
        public Blob TicketDetails { get { return _ticketDetails; } set { Set(ref _ticketDetails, value, "TicketDetails"); } }
        [DataMember]
        public DateTime Workdate { get { return _workdate; } set { Set(ref _workdate, value, "Workdate"); } }
        [DataMember]
        public DateTime Time { get { return _time; } set { Set(ref _time, value, "Time"); } }
        [DataMember]
        public string Error { get { return _error; } set { Set(ref _error, value, "Error"); } }
        [DataMember]
        public string SeriePrex { get { return _seriePrex; } set { Set(ref _seriePrex, value, "SeriePrex"); } }
        [DataMember]
        public long? Serie { get { return _serie; } set { Set(ref _serie, value, "Serie"); } }
        [DataMember]
        public TicketRContract TicketContract { get;set; }
        [DataMember]
        public bool Processed { get; set; }
        #endregion

        #region Validations

         public static System.ComponentModel.DataAnnotations.ValidationResult ValidateCaja(TicketsErrorContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        #region Public Methods
         public static TicketRContract LoadFromXml(string xml)
         {
             XmlSerializer serializer = new XmlSerializer(typeof(TicketRContract));
             MemoryStream stream = new MemoryStream();
             System.Text.Encoding encoder = System.Text.Encoding.UTF8;
             foreach (byte b in encoder.GetBytes(xml))
             {
                 stream.WriteByte(b);
             }
             stream.Seek(0, SeekOrigin.Begin);
             TicketRContract result = (TicketRContract)serializer.Deserialize(stream);
             stream.Close();
             return result;
         }
         public static string SaveToXml(TicketRContract contract)
         {
             XmlSerializer serializer = new XmlSerializer(typeof(TicketRContract));
             MemoryStream stream = new MemoryStream();
             serializer.Serialize(stream, contract);
             stream.Seek(0, SeekOrigin.Begin);
             StreamReader reader = new StreamReader(stream);
             string result = reader.ReadToEnd();
             reader.Close();
             stream.Close();
             return result;
         }
         public string BinaryToText(byte[] data)
         {
             MemoryStream stream = new MemoryStream(data);
             StreamReader reader = new StreamReader(stream, Encoding.UTF8);
             string text = reader.ReadToEnd();
             return text;
         }
         public TicketsErrorInfo LoadInfoTicket()
         {
             string info = BinaryToText((byte[])TicketDetails);
             if (string.IsNullOrEmpty(info))
                 return null;
             TicketsErrorInfo ticketsErrorInfo = new TicketsErrorInfo();
             string[] infoTicket = info.Replace("\0","").Split('\n');

             ticketsErrorInfo.Serieticket = infoTicket[0].Replace("\0","").Replace("Serie: ","");
             ticketsErrorInfo.Table = infoTicket[1].Replace("\0","").Replace("Table: ","");
             string opdatestr = infoTicket[2].Replace("OpenDate: ","");
             string cldatestr = infoTicket[3].Replace("CloseDate: ","");
             DateTime opdate;
             DateTime cldate;

             DateTime.TryParse(opdatestr, out opdate);
             if (opdate.Equals(DateTime.MinValue))
             {
                 try
                 {
                     opdate = new DateTime(Convert.ToInt32(opdatestr.Substring(6, 4)), Convert.ToInt32(opdatestr.Substring(3, 2)), Convert.ToInt32(opdatestr.Substring(0, 2)));
                 }
                 catch
                 {
                     opdate = new DateTime(Convert.ToInt32(opdatestr.Substring(6, 4)), Convert.ToInt32(opdatestr.Substring(0, 2)), Convert.ToInt32(opdatestr.Substring(3, 2)));
                 }
             }

             ticketsErrorInfo.CheckOpeningDate = opdate;
             //ticketsErrorInfo.CheckOpeningTime = opdate.Add(TimeSpan.Parse(DateTime.UtcNow.TimeOfDay.ToString()));
             ticketsErrorInfo.CheckOpeningTime = DateTime.Now;
             DateTime.TryParse(cldatestr, out cldate);

             if (cldate.Equals(DateTime.MinValue))
             {
                 try
                 {
                     cldate = new DateTime(Convert.ToInt32(cldatestr.Substring(6, 4)), Convert.ToInt32(cldatestr.Substring(3, 2)), Convert.ToInt32(cldatestr.Substring(0, 2)));
                 }
                 catch
                 {
                     cldate = new DateTime(Convert.ToInt32(cldatestr.Substring(6, 4)), Convert.ToInt32(cldatestr.Substring(0, 2)), Convert.ToInt32(cldatestr.Substring(3, 2)));
                 }
             }

             ticketsErrorInfo.CloseDate = cldate;
             ticketsErrorInfo.CloseDateTime = cldate.Add(TimeSpan.Parse(DateTime.UtcNow.TimeOfDay.ToString()));

             decimal quantity;
             ticketsErrorInfo.ProductsItems = new List<ProductItem>();
             ticketsErrorInfo.PaymentLineItems = new List<PaymentLineItem>();
             int index = 5;
         
             foreach (var itemInfo in infoTicket.Skip(5))
             {
                 if (itemInfo.Contains("ProductId:"))
                 {
                     index++;
                     ProductItem productItem = new ProductItem();
                     string[] item = itemInfo.Split(';');
                     productItem.ProductId = item[0].Replace("ProductId:", "");
                     Decimal.TryParse(item[1].Replace("Quantity:", ""), out quantity);
                     productItem.ProductQtd = quantity;
                     productItem.Importe = decimal.Parse(item[2].Replace("Import:", ""),  new CultureInfo("pt-PT"));
                     ticketsErrorInfo.ProductsItems.Add(productItem);
                 }
                 else
                     break;
             }
             foreach (var itemInfo in infoTicket.Skip(index+1))
             {
                 if (itemInfo.Contains("Description:"))
                 {
                     string[] item = itemInfo.Split(';');
                     PaymentLineItem paymentLineItem = new PaymentLineItem();
                     paymentLineItem.Valo = decimal.Parse(item[1].Replace("Amount:", ""), new CultureInfo("pt-PT")); 
                     ticketsErrorInfo.PaymentLineItems.Add(paymentLineItem);
                 }
             }
             return ticketsErrorInfo;
         }
         #endregion
    }

    public class TicketsErrorInfo 
    {
       public Guid? Id { get; set; }
       public string Serieticket {get;set;}
       public string Table {get;set;}
       public DateTime CheckOpeningDate { get; set; }
       public DateTime? CloseDate {get;set;}
       public DateTime CheckOpeningTime { get; set; }
       public DateTime? CloseDateTime { get; set; }
       public Guid? Stand {get;set;}
       public Guid? Caja { get; set; }
       public Guid? CajaPave { get; set; }
       public string SeriePrex { get; set; }
       public long? Serie { get; set; }
       public Guid Util { get; set; }
       public int Shift { get; set; }
       public string Unmo { get; set; }
       public Guid? Mesa { get; set; }
       public short Paxs { get; set; }
       public string Signature { get; set; }
       public Guid? CancellationReasonId { get; set; }
       public string CancellationReasonDescription { get; set; }
       public List<ProductItem> ProductsItems;
       public List<PaymentLineItem> PaymentLineItems;
       public FactInformation FactInformation { get; set; }
    }

    public class PaymentLineItem
    {
       public decimal Valo;
       public Guid? Ticket;
       public long Tire;
       public Guid? Payment;
       public Guid? CreditCardTypeId;
       public Guid? CreditCard;
       public short? CreditCardValidationMonth;
       public short? CreditCardValidationYear;
       public string CreditCardIdentNumber;
       public Guid? Account;
       public Guid? Coin;

    }
    public class ProductItem
    {
        public string ProductId;
        public decimal Importe;
        public Guid Product;
        public Guid Ticket;
        public Guid Stand;
        public Guid Caja;
        public Guid? Separator;
        public decimal ProductQtd;
        public decimal LiqValue;
        public Guid Util;
        public decimal? Discount;
        public int IsAnul;
        public decimal ValueBeforeDiscount;
    }

    public class FactInformation
    {
        public DateTime? DocDate;
        public DateTime? DocSysDateTime;
        public string Holder;
        public string Address;
        public string Country;
        public string NIF;
        public Guid? Benti;
        public string Comments;
        public string FiscalSignature;
        public string FactSerie;
        public long? FactNumber;
        public decimal? Value;
    }
}
