﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
     [DataContract]
	[Serializable]
    public class ContainerTransferContract:BaseContract
    {
          #region Constructor
         public ContainerTransferContract()
            : base()
        {
            Transfers = new TypedList<TransferContract>();
        }
        #endregion

         #region Contracts
         [DataMember]
         [ReflectionExclude]
         public TypedList<TransferContract> Transfers { get; set; }
         #endregion

    }
}
