﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(CajaContract), "ValidateCaja")]
    public class TransferContract : BaseContract
    {
        #region Members

        private Guid _ticketOrig;
        private Guid _standOrig;
        private Guid _cajaOrig;
        private Guid? _ticketDest;
        private Guid _standDest;
        private Guid? _cajaDest;
        private DateTime _tranDate;
        private DateTime _tranTime;
        private DateTime? _acepDate;
        private DateTime? _acepTime;
        private Guid _utilTran;
        private Guid? _utilAcep;

        #endregion
        #region Constructor

        public TransferContract()
            : base()
        {
        }

        public TransferContract(Guid id, Guid ticketOrig, Guid standOrig, Guid cajaOrig, Guid? ticketDest, Guid standDest, Guid? cajaDest,
            DateTime tranDate, DateTime tranTime, DateTime? acepDate, DateTime? acepTime, Guid utilTran, Guid? utilAcept, string utilTranDesc,
            string utilAceptDesc, string ticketOrigDesc, string standOrigDesc, string cajaOrigDesc, string ticketDestDesc, string standDestDesc,
            string cajaDestDesc, decimal valorticket)
            : base()
        {
            Id = id;
            TicketOrig = ticketOrig;
            StandOrig = standOrig;
            CajaOrig = cajaOrig;
            TicketDest = TicketDest;
            StandDest = standDest;
            CajaDest = cajaDest;
            TranDate = tranDate;
            TranTime = tranTime;
            AcepDate = acepDate;
            AcepTime = acepTime;
            UtilTran = utilTran;
            UtilAcep = utilAcept;
            TicketOrigDesc = ticketOrigDesc;
            StandOrigDesc = standOrigDesc;
            CajaOrigDesc = cajaOrigDesc;
            TicketDestDesc = ticketDestDesc;
            StandDestDesc = standDestDesc;
            CajaDestDesc = cajaDestDesc;
            UtilTranDesc = utilTranDesc;
            UtilAcepDesc = utilAceptDesc;
            ValorTicket = valorticket;
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid TicketOrig { get { return _ticketOrig; } set { Set(ref _ticketOrig, value, "TicketOrig"); } }
        [DataMember]
        public Guid StandOrig { get { return _standOrig; } set { Set(ref _standOrig, value, "StandOrig"); } }
        [DataMember]
        public Guid CajaOrig { get { return _cajaOrig; } set { Set(ref _cajaOrig, value, "CajaOrig"); } }
        [DataMember]
        public Guid? TicketDest { get { return _ticketDest; } set { Set(ref _ticketDest, value, "TicketDest"); } }
        [DataMember]
        public Guid StandDest { get { return _standDest; } set { Set(ref _standDest, value, "StandDest"); } }
        [DataMember]
        public Guid? CajaDest { get { return _cajaDest; } set { Set(ref _cajaDest, value, "CajaDest"); } }
        [DataMember]
        public DateTime TranDate { get { return _tranDate; } set { Set(ref _tranDate, value, "TranDate"); } }
        [DataMember]
        public DateTime TranTime { get { return _tranTime; } set { Set(ref _tranTime, value, "TranTime"); } }
        [DataMember]
        public DateTime? AcepDate { get { return _acepDate; } set { Set(ref _acepDate, value, "AcepDate"); } }
        [DataMember]
        public DateTime? AcepTime { get { return _acepTime; } set { Set(ref _acepTime, value, "AcepTime"); } }
        [DataMember]
        public Guid UtilTran { get { return _utilTran; } set { Set(ref _utilTran, value, "UtilTran"); } }
        [DataMember]
        public Guid? UtilAcep { get { return _utilAcep; } set { Set(ref _utilAcep, value, "UtilAcep"); } }

        #endregion
        #region Extended Properties

        [DataMember]
        public string TicketOrigDesc { get; set; }
        [DataMember]
        public string StandOrigDesc { get; set; }
        [DataMember]
        public string CajaOrigDesc { get; set; }
        [DataMember]
        public string TicketDestDesc { get; set; }
        [DataMember]
        public string StandDestDesc { get; set; }
        [DataMember]
        public string CajaDestDesc { get; set; }
        [DataMember]
        public string UtilTranDesc { get; set; }
        [DataMember]
        public string UtilAcepDesc { get; set; }
        [DataMember]
        public decimal ValorTicket { get; set; }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateCaja(CajaContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}