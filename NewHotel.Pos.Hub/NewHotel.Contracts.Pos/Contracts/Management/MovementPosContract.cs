﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.ComponentModel.DataAnnotations;
//using System.Runtime.Serialization;

//namespace NewHotel.Contracts
//{
//    [DataContract]
//    [CustomValidation(typeof(MovementPosContract), "ValidateMovementPos")]
//    public class MovementPosContract : BaseContract
//    {
//        #region Members
//        private string _description;
//        private Guid _department;
//        private Guid _servicesBydepartment;
//        private CurrentAccountType? _accountType;
//        private Guid _accountId;
//        private string _currency;
//        private decimal _quantity;
//        private decimal _value;

//        #endregion

//        #region Constructor
//        public MovementPosContract()
//            : base()
//        {

//        }

//        public MovementPosContract(Guid id, string description,Guid department, Guid servicesByDepartment, CurrentAccountType? accountType, Guid accountId,
//            string currency,decimal quantity, decimal value)
//            : base()
//        {
//            this.Id = id;
//            this.Description = description;
//            this.Department = department;
//            this.ServicesBydepartment = servicesByDepartment;
//            this.AccountType = accountType;
//            this.AccountId = accountId;
//            this.Currency = currency;
//            this.Quantity = quantity;
//            this.Value = value;
//        }
//        #endregion

//        #region Properties

//        [DataMember]
//        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
//        [DataMember]
//        public Guid Department { get { return _department; } set { Set(ref _department, value, "Department"); } }
//        [DataMember]
//        public Guid ServicesBydepartment { get { return _servicesBydepartment; } set { Set(ref _servicesBydepartment, value, "ServicesBydepartment"); } }
//        [DataMember]
//        public CurrentAccountType? AccountType { get { return _accountType; } set { Set(ref _accountType, value, "AccountType"); } }
//        [DataMember]
//        public Guid AccountId { get { return _accountId; } set { Set(ref _accountId, value, "AccountId"); } }
//        [DataMember]
//        public string Currency { get { return _currency; } set { Set(ref _currency, value, "Currency"); } }
//        [DataMember]
//        public decimal Quantity { get { return _quantity; } set { Set(ref _quantity, value, "Quantity"); } }
//        [DataMember]
//        public decimal Value { get { return _value; } set { Set(ref _value, value, "Value"); } }
       

//        #endregion

//        #region DummyMembers

//        #endregion


//        #region Validations

//        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateMovementPos(MovementPosContract obj)
//        {
//            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
//        }

//        #endregion
//    }
//}
