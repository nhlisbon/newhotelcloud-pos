﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public class LicenceTypeContract:BaseContract
    {
        private LicenseProductType _licence;

         #region Constructor
        public LicenceTypeContract()
            : base()
        {
            
        }
        #endregion

        [DataMember]
        public LicenseProductType Licence { get { return _licence; } set { Set(ref _licence, value, "Licence"); } }
    }
}
