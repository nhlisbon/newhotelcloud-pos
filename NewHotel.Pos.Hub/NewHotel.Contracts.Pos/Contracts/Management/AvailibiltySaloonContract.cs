﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Contracts.Pos
{
    public class AvailibiltySaloonContract : BaseContract
    {
        public int Paxs { get; set; }
        public int Reservations { get; set; }
        public List<LiteTableRecord> AvailableTables { get; set; }
    }
}
