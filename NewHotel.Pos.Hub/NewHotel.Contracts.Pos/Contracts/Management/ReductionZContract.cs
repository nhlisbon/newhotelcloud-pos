﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ReductionZContract : BaseContract
    {
        #region Members

        private Guid _cashierId;
        private DateTime _workDate;
        private DateTime _registrationDate;
        private DateTime _registrationTime;
        private string _reductionNumber;
        private decimal _grandTotal;
        private string _initialTicketNumber;
        private string _finalTicketNumber;
        private decimal _cancellationTotal;
        private decimal _netValue;
        private decimal _substitutionTributableValue;
        private decimal _discountValue;
        private decimal _insentoValue;
        private decimal _nonTaxValue;
        private decimal _suprimentoValue;
        private decimal _sangriaValue;
        private short _operationCounter;
        private decimal otherReceivables;
        private decimal _debtTaxValue;
        private decimal _issBaseValue;
        private short _restartControlOrden;
        private decimal _grossSale;
        private string _fiscalPrinterSerial;
        private string _fiscalPrinterModel;
        private decimal _icmsAliquota;
        private decimal _icmsValue;

        #endregion
        #region Constructor

        public ReductionZContract()
            : base()
        {
            TaxesDetails = new TypedList<ReductionZTaxesContract>();
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid CashierId { get { return _cashierId; } set { Set(ref _cashierId, value, "CashierId"); } }
        [DataMember]
        public DateTime WorkDate { get { return _workDate; } set { Set(ref _workDate, value, "WorkDate"); } }
        [DataMember]
        public DateTime RegistrationDate { get { return _registrationDate; } set { Set(ref _registrationDate, value, "RegistrationDate"); } }
        [DataMember]
        public DateTime RegistrationTime { get { return _registrationTime; } set { Set(ref _registrationTime, value, "RegistrationTime"); } }
        [DataMember]
        public string ReductionNumber { get { return _reductionNumber; } set { Set(ref _reductionNumber, value, "ReductionNumber"); } }
        [DataMember]
        public decimal GrandTotal { get { return _grandTotal; } set { Set(ref _grandTotal, value, "GrandTotal"); } }
        [DataMember]
        public string InitialTicketNumber { get { return _initialTicketNumber; } set { Set(ref _initialTicketNumber, value, "InitialTicketNumber"); } }
        [DataMember]
        public string FinalTicketNumber { get { return _finalTicketNumber; } set { Set(ref _finalTicketNumber, value, "FinalTicketNumber"); } }
        [DataMember]
        public decimal CancellationTotal { get { return _cancellationTotal; } set { Set(ref _cancellationTotal, value, "CancellationTotal"); } }
        [DataMember]
        public decimal NetValue { get { return _netValue; } set { Set(ref _netValue, value, "NetValue"); } }
        [DataMember]
        public decimal SubstitutionTributableValue { get { return _substitutionTributableValue; } set { Set(ref _substitutionTributableValue, value, "SubstitutionTributableValue"); } }
        [DataMember]
        public decimal DiscountValue { get { return _discountValue; } set { Set(ref _discountValue, value, "DiscountValue"); } }
        [DataMember]
        public decimal InsentoValue { get { return _insentoValue; } set { Set(ref _insentoValue, value, "InsentoValue"); } }
        [DataMember]
        public decimal NonTaxValue { get { return _nonTaxValue; } set { Set(ref _nonTaxValue, value, "NonTaxValue"); } }
        [DataMember]
        public decimal SuprimentoValue { get { return _suprimentoValue; } set { Set(ref _suprimentoValue, value, "SuprimentoValue"); } }
        [DataMember]
        public decimal SangriaValue { get { return _sangriaValue; } set { Set(ref _sangriaValue, value, "SangriaValue"); } }
        [DataMember]
        public short OperationCounter { get { return _operationCounter; } set { Set(ref _operationCounter, value, "OperationCounter"); } }
        [DataMember]
        public decimal OtherReceivables { get { return otherReceivables; } set { Set(ref otherReceivables, value, "OtherReceivables"); } }
        [DataMember]
        public decimal DebtTaxValue { get { return _debtTaxValue; } set { Set(ref _debtTaxValue, value, "DebtTaxValue"); } }
        [DataMember]
        public decimal IssBaseValue { get { return _issBaseValue; } set { Set(ref _issBaseValue, value, "IssBaseValue"); } }
        [DataMember]
        public short RestartControlOrden { get { return _restartControlOrden; } set { Set(ref _restartControlOrden, value, "RestartControlOrden"); } }
        [DataMember]
        public decimal GrossSale { get { return _grossSale; } set { Set(ref _grossSale, value, "GrossSale"); } }
        [DataMember]
        public string FiscalPrinterSerial { get { return _fiscalPrinterSerial; } set { Set(ref _fiscalPrinterSerial, value, "FiscalPrinterSerial"); } }
        [DataMember]
        public string FiscalPrinterModel { get { return _fiscalPrinterModel; } set { Set(ref _fiscalPrinterModel, value, "FiscalPrinterModel"); } }
        //[DataMember]
        //public decimal IcmsAliquota { get { return _icmsAliquota; } set { Set(ref _icmsAliquota, value, "IcmsAliquota"); } }
        //[DataMember]
        //public decimal IcmsValue { get { return _icmsValue; } set { Set(ref _icmsValue, value, "IcmsValue"); } }

        #endregion
        #region Lists
        [ReflectionExclude]
        [DataMember]
        public TypedList<ReductionZTaxesContract> TaxesDetails { get; private set; }
        #endregion
    }

    [DataContract]
    [Serializable]
    public class ReductionZTaxesContract : BaseContract
    {
        #region Contract properties
        [DataMember]
        public Guid ReductionZ { get; set; }
        [DataMember]
        public decimal BaseValue { get; set; }
        [DataMember]
        public decimal PercentValue { get; set; }

        #endregion

        public ReductionZTaxesContract(Guid id, decimal baseValue, decimal percent)
            : base()
        {
            ReductionZ = id;
            BaseValue = baseValue;
            PercentValue = percent;
        }
    }
}
