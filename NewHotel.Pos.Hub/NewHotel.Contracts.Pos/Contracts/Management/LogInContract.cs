﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class LogInContract : BaseContract
    {
        private Guid? _fisi;

        #region Constructor
        public LogInContract()
            : base()
        {
            
        }

        [DataMember]
        public Guid? Fisi { get { return _fisi; } set { Set(ref _fisi, value, "Fisi"); } }

        [DataMember]
        [ReflectionExclude]
        public TicketSerieContract TicketSerie { get; set; }
        [DataMember]
        [ReflectionExclude]
        public TicketSerieContract InvoiceSerie { get; set; }
        [DataMember]
        [ReflectionExclude]
        public TicketSerieContract ReceiptSerie { get; set; }
        #endregion
    }
}
