﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SignatureContract : BaseContract
    {
        [DataMember]
        public string Signature { get; set; }
    }
}