﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(StandCajaLogContract), "ValidateStandCajaLog")]
    public class StandCajaLogContract : BaseContract
    {
        #region Members

        private Guid _stand;
        private Guid? _caja;
        private Guid _user;
        private DateTime _workdate;
        private DateTime _time;
        private long _operation;
        private Guid? _identifier;
        private bool _operComplete;
        private string _description;
        private DateTime _standWorkdate;
        #endregion
        #region Constructor
        public StandCajaLogContract()
            : base()
        {

        }

        public StandCajaLogContract(Guid id, Guid stand, Guid? caja, Guid user, DateTime workDate, DateTime time, long operation, Guid? identifier, bool operComplete)
            : base()
        {
            this.Id = id;
            this.Stand = stand;
            this.Caja = caja;
            this.User = user;
            this.Workdate = workDate;
            this.StandWorkdate = workDate;
            this.Time = time;
            this.Operation = operation;
            this.Identifier = identifier;
            this.OperComplete = operComplete;
        }
        #endregion
        #region Properties

        [DataMember]
        public Guid Stand { get { return _stand; } set { Set(ref _stand, value, "Stand"); } }
        [DataMember]
        public Guid? Caja { get { return _caja; } set { Set(ref _caja, value, "Caja"); } }
        [DataMember]
        public Guid User { get { return _user; } set { Set(ref _user, value, "User"); } }
        [DataMember]
        public DateTime Workdate { get { return _workdate; } set { Set(ref _workdate, value, "Workdate"); } }
        [DataMember]
        public DateTime Time { get { return _time; } set { Set(ref _time, value, "Time"); } }
        [DataMember]
        public long Operation { get { return _operation; } set { Set(ref _operation, value, "Operation"); } }
        [DataMember]
        public Guid? Identifier { get { return _identifier; } set { Set(ref _identifier, value, "Identifier"); } }
        [DataMember]
        public bool OperComplete { get { return _operComplete; } set { Set(ref _operComplete, value, "OperComplete"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public DateTime StandWorkdate { get { return _standWorkdate; } set { Set(ref _standWorkdate, value, "StandWorkdate"); } }
        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateStandCajaLog(StandCajaLogContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
