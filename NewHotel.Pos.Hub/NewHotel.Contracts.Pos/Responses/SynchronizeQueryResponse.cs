﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SynchronizeQueryResponse : QueryResponse
    {
        #region Properties

        [DataMember]
        public Guid[] ForRemoving { get; internal set; }
        [DataMember]
        public Guid[] ForUploading { get; internal set; }

        #endregion
        #region Constructor

        public SynchronizeQueryResponse(QueryResponse qr, Guid[] forRemoving, Guid[] forUploading)
            : base(qr.Type, qr)
        {
            ForRemoving = forRemoving;
            ForUploading = forUploading;
        }

        #endregion
    }
}
