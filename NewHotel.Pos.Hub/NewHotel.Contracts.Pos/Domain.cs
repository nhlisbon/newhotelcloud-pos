﻿using System;

namespace NewHotel.Contracts
{
    public enum POSDocumentType : long { Ticket = 25, Ballot = 26, CashInvoice = 11, Invoice = 9, CashCreditNote = 18, TableBalance = 48 };
      
    /// <summary>
    /// TNHT_ARIM.ARIM_TIPO = Tipo de Imagen POS (Small for buttons, Medium for Presentation)
    /// </summary>
    public enum POSImageType : long { SmallButtons = 2701, MediumPresentation = 2702 };

    /// <summary>
    /// TNHT_ARTG.ARTG_TABL = Tipo de Table (Table Fijo, Table Dinamico, Table de Rondas)
    /// </summary>
    public enum TableType : long { FixedMenu = 2641, DynamicMenu = 2642, RoundMenu = 2643 };

    /// <summary>
    /// TNHT_IPOS.IPOS_TIPO = Tipo de Punto de Venta (Gastronómico, Supermercado, Tienda)
    /// </summary>
    public enum POSType : long { Gastronomic = 2671, Supermarket = 2672, Shop = 2673 };

    /// <summary>
    /// TPRG.TYPE_PK = EPosRateType (All, General, Internal, MealPlan)
    /// </summary>
    public enum EPosRateType : long { All = 1982, General = 1681, Internal = 2732, MealPlan = 1141 };

    /// <summary>
    /// TNHT_MESA.MESA_TYPE = EPosMesaType (Standard, Bar, Reserved)
    /// </summary>
    public enum EPosMesaType : long { Standard = 1171, Bar = 3041, Reserved = 601 };

    /// <summary>
    /// TNHT_TRAN.LOIP_OPER = Operation (Login, CloseTurn, CloseDay, Account Transfer)
    /// </summary>
    public enum LogStandCaja : long { Login = 3281, CloseTurn = 3282, CloseDay = 3283, AccountTransfer = 2132 };



    #region Tickets Operations

    [Flags]
    public enum TicketOper
    {
        None = 0,
        View = 1,
        Bill = 2,
        Cancel = 4,
        PrintInvoice = 16
    };

    #endregion

    public enum TablesReservationQuickInfo : int
    {
        Vegetarian = 0,
        GlutenFree = 1,
        VIP = 2,
        Vegan = 3,
        Disabled = 4,
        Bigspender = 5,
        NutAllergy = 6,
        WheelChair = 7,
        FriendOwner = 8
    };
}
