﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("BENTI_PK")]
    public class GuestsByRoomRecord : BaseRecord
    {
        [MappingColumn("BENTI_DESC", Nullable = true)]
        public string GuestName { get; set; }
        [MappingColumn("CONT_APEL", Nullable = true)]
        public string GuestApel { get; set; }
    }
}
