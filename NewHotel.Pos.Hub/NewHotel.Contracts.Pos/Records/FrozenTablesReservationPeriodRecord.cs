﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class BookingTablesReservationPeriodRecord : TablesReservationPeriodRecord
    {
        [MappingColumn("TRPE_FROZEN")]
        public bool IsFrozen { get; set; }

        [MappingColumn("TRPE_OVBO")]
        public bool IsInOverBooking { get; set; }
    }
}
