﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("MENU_PK")]
    public class MenuRecord : BaseRecord
    {
        [MappingColumn("MENU_DESC", Nullable = false)]
        public string Description { get; set; }
    }
}
