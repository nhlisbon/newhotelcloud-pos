﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("SEDO_PK")]
    public class SerieDescRecord:BaseRecord
    {
        [MappingColumn("SEDO_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
