﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("VEFI_PK")]
    public class TicketFiscalDocumentRecord : BaseRecord
    {
        [MappingColumn("VEFI_CODE", Nullable = true)]
        public string Code { get; set; }
        [MappingColumn("VEFI_TICK", Nullable = true)]
        public string Tickets { get; set; }
        [MappingColumn("VEFI_CANC", Nullable = false)]
        public bool Cancelled { get; set; }
        [MappingColumn("VEFI_DHRE", Nullable = false)]
        public DateTime RegistrationDate { get; set; }
        [MappingColumn("VEFI_DATE", Nullable = true)]
        public DateTime? Date { get; set; }
    }
}
