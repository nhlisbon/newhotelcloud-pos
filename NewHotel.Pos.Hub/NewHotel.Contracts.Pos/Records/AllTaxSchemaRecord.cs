﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
     [MappingQuery("ESIM_PK")]
    public class AllTaxSchemaRecord : BaseRecord
    {
         [MappingColumn("LITE_PK", Nullable = false)]
         public Guid Lite { get; set; }
         [MappingColumn("ESIM_DESC", Nullable = false)]
         public string EsimDesc { get; set; }
         [MappingColumn("ESIM_LINK", Nullable = true)]
         public Guid? EsimLink { get; set; }
         [MappingColumn("ESIM_DEFA", Nullable = false)]
         public bool Defa { get; set; }
         [MappingColumn("ESIM_EXPD", Nullable = true)]
         public DateTime? Expd { get; set; }
    }
}
