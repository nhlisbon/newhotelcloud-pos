﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
	[MappingQuery("ARTG_PK")]
    public class ProductRecord:BaseRecord
    {
        [MappingColumn("ARTG_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("ARTG_ABRE", Nullable = false)]
        public string Abbreviation { get; set; }
        [MappingColumn("ARTG_CODI", Nullable = true)]
        public string Article { get; set; }
        [MappingColumn("ARTG_GRUP", Nullable = false)]
        public string Group { get; set; }
        [MappingColumn("GRUP_PK", Nullable = false)]
        public Guid GroupId { get; set; }
        [MappingColumn("ARTG_FAMI", Nullable = false)]
        public string Family { get; set; }
        [MappingColumn("FAMI_PK", Nullable = false)]
        public Guid FamilyId { get; set; }
        [MappingColumn("ARTG_SFAM", Nullable = true)]
        public string SubFamily { get; set; }
        [MappingColumn("SFAM_PK", Nullable = true)]
        public Guid? SubFamilyId { get; set; }
        [MappingColumn("SERV_INAC", Nullable = false)]
        public bool Inactive { get; set; }
        [MappingColumn("ARTG_TABL", Nullable = true)]
        public string TableType { get; set; }
        [MappingColumn("ARTG_OBLI", Nullable = false)]
        public bool ShowAditionalProduct { get; set; }
        [MappingColumn("ARTG_OBPR", Nullable = false)]
        public bool ShowPreparationsScreen { get; set; }
        [MappingColumn("ARTG_CMDA", Nullable = false)]
        public bool Print { get; set; }
        [MappingColumn("ARTG_COLO", Nullable = true)]
        public ARGBColor? Color { get; set; }
        [MappingColumn("SEPA_PK", Nullable = true)]
        public Guid? DefaultSeparator { get; set; }
        //[MappingColumn("ARTG_LMOD", Nullable = true)]
        //public DateTime LastModified { get; set; }


    } 
}
