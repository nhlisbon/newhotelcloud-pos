﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("GRPP_PK")]
    public class GroupPreparationsRecord : BaseRecord
    {
        [MappingColumn("GRUP_PK", Nullable = false)]
        public Guid GroupId { get; set; }
        [MappingColumn("PREP_PK", Nullable = false)]
        public Guid PreparationId { get; set; }
    }
}
