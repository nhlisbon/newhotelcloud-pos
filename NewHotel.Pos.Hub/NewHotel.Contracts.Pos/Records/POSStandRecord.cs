﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts.Pos.Records
{
    [Serializable]
    [DataContract]
    [MappingQuery("IPOS_PK")]
    public class POSStandRecord : BaseRecord
    {
        public Guid StandId => (Guid)Id;

        /// <summary>
        /// column="IPOS_ABRE"
        /// Abreviatura Stand
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_ABRE", Nullable = false)]
        public string Abbreviation { get; set; }
        /// <summary>
        /// column="IPOS_DESC"
        /// Nombre Stand
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_DESC", Nullable = false)]
        public string Description { get; set; }
        /// <summary>
        /// column="IPOS_FCTR"
        /// Fecha de trabajo en este punto de venta
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_FCTR", Nullable = true)]
        public DateTime StandWorkDate { get; set; }
        /// <summary>
        /// column="IPOS_TUTR"
        /// Turno de trabajo
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_TUTR", Nullable = false)]
        public long StandWorkShift { get; set; }
        /// <summary>
        /// column="CTRL_PK"
        /// Cuenta control para descargar las ventas de este punto de venta
        /// </summary>
        [DataMember]
        [MappingColumn("CTRL_PK", Nullable = true)]
        public Guid? ControlAccount { get; set; }
        /// <summary>
        /// column="TPRG_STDR"
        /// Tarifa de precio estandar asociada al stand
        /// </summary>
        [DataMember]
        [MappingColumn("TPRG_STDR", Nullable = true)]
        public Guid? StandardRateType { get; set; }
        /// <summary>
        /// column="TPRG_CINT"
        /// Tarifa de precio para consumo interno asociada al stand
        /// </summary>
        [DataMember]
        [MappingColumn("TPRG_CINT", Nullable = true)]
        public Guid? ConsIntRateType { get; set; }
        /// <summary>
        /// column="TPRG_CIPO"
        /// % a aplicar sobre la tarifa estandar para preciar los consumos internos
        /// </summary>
        [DataMember]
        [MappingColumn("TPRG_CIPO", Nullable = true)]
        public decimal? ConsIntPercentValue { get; set; }
        /// <summary>
        /// column="TPRG_PENS"
        /// Tarifa de precio incluidos en pensión asociada al stand
        /// </summary>
        [DataMember]
        [MappingColumn("TPRG_PENS", Nullable = true)]
        public Guid? PensionRateType { get; set; }
        /// <summary>
        /// column="TPRG_PEPO"
        /// % a aplicar sobre la tarifa estandar para preciar los incluídos en pensión
        /// </summary>
        [DataMember]
        [MappingColumn("TPRG_PEPO", Nullable = true)]
        public decimal? PensionRatePercentValue { get; set; }
        /// <summary>
        /// column="CNFG_TICK"
        /// Configuración de la impresión del ticket
        /// </summary>
        [DataMember]
        [MappingColumn("CNFG_TICK", Nullable = true)]
        public Guid? TicketPrintConfiguration { get; set; }
        /// <summary>
        /// column="CNFG_BOLE"
        /// Configuración de la impresión de la boleta
        /// </summary>
        [DataMember]
        [MappingColumn("CNFG_BOLE", Nullable = true)]
        public Guid? BallotPrintConfiguration { get; set; }
        /// <summary>
        /// column="CNFG_FACT"
        /// Configuración de la impresión de la factura
        /// </summary>
        [DataMember]
        [MappingColumn("CNFG_FACT", Nullable = true)]
        public Guid? InvoicePrintConfiguration { get; set; }
        /// <summary>
        /// column="CNFG_COMP"
        /// Configuración de la impresión del comprobante de mesa
        /// </summary>
        [DataMember]
        [MappingColumn("CNFG_COMP", Nullable = true)]
        public Guid? ReceiptPrintConfiguration { get; set; }
        /// <summary>
        /// column="IPOS_CCOP"
        /// Cantidad de copias a imprimir de la tira del cierre
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_CCOP", Nullable = false)]
        public short NightAuditorReportCopies { get; set; }
        /// <summary>
        /// column="IPOS_CKAB"
        /// Cerrar el dia con tickets abiertos
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_CKAB", Nullable = false)]
        public bool AllowCloseDayWithOpenTickets { get; set; }
        /// <summary>
        /// column="IPOS_KITI"
        /// Imprimir una copia del ticket de cocina en la impresora de recibos
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_KITI", Nullable = false)]
        public bool CopyKitchenInTicketPrinter { get; set; }
        /// <summary>
        /// column="IPOS_SPTA"
        /// Si se hace una división manual del ticket ambos quedan en la mesa del ticket original
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_SPTA", Nullable = false)]
        public bool KeepTicketsInTableOnManualSplit { get; set; }
        /// <summary>
        /// column="IPOS_COMP"
        /// Se imprimen comrpobantes
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_COMP", Nullable = false)]
        public bool AllowPrintTableBalance { get; set; }
        /// <summary>
        /// IPOS_TISE
        /// Servicio de tipo propina asociado al hotel                                                                                                                               
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_TISE", Nullable = true)]
        public Guid? TipServiceId { get; set; }
        /// <summary>
        /// IPOS_TIAT
        /// Incluir propina automaticamente                                                                                                                           
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_TIAT", Nullable = false)]
        public bool ApplyTipAutomatically { get; set; }
        /// <summary>
        /// IPOS_TIPE
        /// Prociento de propina automatica asociado al hotel                                                                                                                             
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_TIPE", Nullable = true)]
        public decimal? TipPercent { get; set; }
        /// <summary>
        /// IPOS_TIVL
        /// Valor de propina automatica asociado al hotel                                                                                                                             
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_TIVL", Nullable = true)]
        public decimal? TipValue { get; set; }
        /// <summary>
        /// IPOS_TION
        /// Calcular propinas sobre valor neto                                                                                                                      
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_TION", Nullable = false)]
        public bool ApplyTipOverNetValue { get; set; }
        /// <summary>
        /// IPOS_TIGR
        /// Agrupamiento de productos                                                                                                                
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_TIGR", Nullable = false)]
        public TicketProductGrouping ProductGrouping { get; set; } = TicketProductGrouping.SeatsSeparators;
        /// <summary>
        /// MENU_MON
        /// Menu para lunes                                                                                                                             
        /// </summary>
        [DataMember]
        [MappingColumn("MENU_MON", Nullable = true)]
        public Guid? MenuMonday { get; set; }
        /// <summary>
        /// MENU_TUE
        /// Menu para martes                                                                                                                            
        /// </summary>
        [DataMember]
        [MappingColumn("MENU_TUE", Nullable = true)]
        public Guid? MenuTuesday { get; set; }
        /// <summary>
        /// MENU_WED
        /// Menu para miercoles
        /// </summary>
        [DataMember]
        [MappingColumn("MENU_WED", Nullable = true)]
        public Guid? MenuWednesday { get; set; }
        /// <summary>
        /// MENU_THU
        /// Menu para jueves                                                                                                                        
        /// </summary>
        [DataMember]
        [MappingColumn("MENU_THU", Nullable = true)]
        public Guid? MenuThusday { get; set; }
        /// <summary>
        /// MENU_FRI
        /// Menu para viernes                                                                                                                   
        /// </summary>
        [DataMember]
        [MappingColumn("MENU_FRI", Nullable = true)]        
        public Guid? MenuFriday { get; set; }
        /// <summary>
        /// MENU_SAT
        /// Menu para sabados                                                                                                                  
        /// </summary>
        [DataMember]
        [MappingColumn("MENU_SAT", Nullable = true)]
        public Guid? MenuSaturday { get; set; }
        /// <summary>
        /// MENU_SUN
        /// Menu para domingos
        /// </summary>
        [DataMember]
        [MappingColumn("MENU_SUN", Nullable = true)]
        public Guid? MenuSunday { get; set; }
        /// <summary>
        /// IPOS_ROSE
        /// Es servicio habitación
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_ROSE", Nullable = false)]
        public bool IsRoomService { get; set; }
        /// <summary>
        /// IPOS_PRNO
        /// Prompt a dialog to add a note when a product is added to ticket
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_PRNO", Nullable = false)]
        public bool PromptProductNote { get; set; }
        /// <summary>
        /// ANDR_PAYM
        /// Allow android payments
        /// </summary>
        [DataMember]
        [MappingColumn("ANDR_PAYM", Nullable = false)]
        public bool AllowAndroidPayments { get; set; }

        /// <summary>
        /// RESE_CHST
        /// Allow change the status for a table reservation
        /// </summary>
        [DataMember]
        [MappingColumn("RESE_CHST", Nullable = false)]
        public bool AllowChangeTableReservationStatus { get; set; }

        /// <summary>
        /// ASK_PAX
        /// Ask for table pax
        /// </summary>
        [DataMember]
        [MappingColumn("ASK_PAX", Nullable = false)]
        public bool AskPax { get; set; }
		/// <summary>
		/// UNMO_BASE
		/// Hotel Base Currency code
		/// </summary>
		[DataMember]
        [MappingColumn("UNMO_BASE", Nullable = false)]
        public string BaseCurrency { get; set; }

		/// <summary>
		/// UNMO_BASE
		/// Stand Currency
		/// </summary>
		[DataMember]
        [MappingColumn("UNMO_PK", Nullable = true)]
        public string Currency { get; set; }

		/// <summary>
		/// CAMB_VALO
		/// Currency Exchange Factor to Base Currency
		/// </summary>
		[DataMember]
		[MappingColumn("CAMB_VALO", Nullable = true)]
		public decimal? ExchangeFactor { get; set; }

        /// <summary>
        /// IPOS_MEAL
        /// Enables (true / 1) or disables (false / 0) the meal plan payment option for the stand
        /// by default is true
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_MEAL", Nullable = false)]
        public bool AllowMealPlanPayment { get; set; }
        
        /// <summary>
        /// IPOS_DEPO
        /// Enables (true / 1) or disables (false / 0) the deposit payment option for the stand
        /// by default is true
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_DEPO", Nullable = false)]
        public bool AllowDepositPayment { get; set; }

        /// <summary>
        /// IPOS_ROOM
        /// Enables (true / 1) or disables (false / 0) the credit room payment option for the stand
        /// by default is true
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_ROOM", Nullable = false)]
        public bool AllowCreditRoomPayment { get; set; }

        /// <summary>
        /// IPOS_HOUSE
        /// Enables (true / 1) or disables (false / 0) the house use payment option for the stand
        /// by default is true
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_HOUSE", Nullable = false)]
        public bool AllowHouseUsePayment { get; set; }

        /// <summary>
        /// ASK_DESC
        /// Ask description in ticket creation
        /// </summary>
        [DataMember]
        [MappingColumn("ASK_DESC", Nullable = false)]
        public bool AskDescription { get; set; }
        
        /// <summary>
        /// ASK_ROOM
        /// Ask room in ticket creation
        /// </summary>
        [DataMember]
        [MappingColumn("ASK_ROOM", Nullable = false)]
        public bool AskRoom { get; set; }

        /// <summary>
        /// ASK_CODE
        /// Ask the pre paid code in ticket creation
        /// </summary>
        [DataMember]
        [MappingColumn("ASK_CODE", Nullable = false)]
        public bool AskCode { get; set; }

        /// <summary>
        /// ACTI_INTE
        /// Activates the integration for the stand
        /// </summary>
        [DataMember]
        [MappingColumn("ACTI_INTE", Nullable = false)]
        public bool ActivateIntegration { get; set; }

        /// <summary>
        /// IPOS_AIVD
        /// flag: Ask Invoice Data
        /// </summary>
        [DataMember]
        [MappingColumn("IPOS_AIVD", Nullable = false)]
        public bool AskInvoiceData { get; set; }
	}
}