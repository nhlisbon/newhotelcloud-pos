﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ARAR_PK")]
    public class AllStandProductAreaRecord : BaseRecord
    {
        [MappingColumn("IPOS_PK", Nullable = false)]
        public Guid Stand { get; set; }
        [MappingColumn("ARTG_PK", Nullable = false)]
        public Guid Product { get; set; }
        [MappingColumn("AREA_PK", Nullable = true)]
        public Guid Area { get; set; }
        [MappingColumn("ARAR_ORDE", Nullable = false)]
        public short Order { get; set; }
    }
}
