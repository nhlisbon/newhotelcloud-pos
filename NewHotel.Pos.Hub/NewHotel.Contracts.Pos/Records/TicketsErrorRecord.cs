﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ERTI_PK")]
    public class TicketsErrorRecord : BaseRecord
    {
        [MappingColumn("CAJA_DESC", Nullable = false)]
        public string Caja { get; set; }
        [MappingColumn("IPOS_DESC", Nullable = false)]
        public string Stand { get; set; }
        [MappingColumn("UTIL_LOGIN", Nullable = false)]
        public string User { get; set; }
        [MappingColumn("ERTI_TICK", Nullable = false)]
        public byte[] Ticket { get; set; }
        [MappingColumn("ERTI_DATR", Nullable = false)]
        public DateTime WorkDate { get; set; }
        [MappingColumn("ISVEND", Nullable = false)]
        public bool IsTicket { get; set; }
        [MappingColumn("ERTI_SEDO")]
        public string SeriePrex { get; set; }
        [MappingColumn("ERTI_CODI")]
        public long? SerieCode { get; set; }
        [MappingColumn("ERTI_PROC")]
        public bool Processed { get; set; }
        
        public string Serie
        {
            get
            {
                string serieFinal;
                if (string.IsNullOrEmpty(SeriePrex) && !SerieCode.HasValue)
                {
                    string[] infoTicket = BinaryToText(Ticket).Split('\n');
                    var str = infoTicket[0].Replace("\0", "").Replace("Serie: ", "");
                    string[] serieIfo = str.Split('/');
                    serieFinal = serieIfo[1] + "/" + serieIfo[0];
                }
                else
                    serieFinal = SerieCode.Value + "/" +SeriePrex ;
                return serieFinal;
            }
        }
        public string BinaryToText(byte[] data)
        {
            MemoryStream stream = new MemoryStream(data);
            StreamReader reader = new StreamReader(stream, Encoding.UTF8);
            string text = reader.ReadToEnd();
            return text;
        }
    }
}
