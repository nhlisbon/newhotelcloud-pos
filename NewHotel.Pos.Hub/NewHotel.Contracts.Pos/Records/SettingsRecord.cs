﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
     [MappingQuery("HOTE_PK")]
    public class SettingsRecord : BaseRecord
    {
        [MappingColumn("PARA_SERV", Nullable = true)]
        public bool GroupByService { get; set; }
        [MappingColumn("FAST_PAYM", Nullable = true)]
        public bool UseFastPayment { get; set; }
        [MappingColumn("PARA_FORE", Nullable = true)]
        public Guid? PaymentFormFastPayment { get; set; }
        [MappingColumn("PARA_IVIN", Nullable = true)]
        public bool TaxesIncluded { get; set; }
        [MappingColumn("PARA_ESIM", Nullable = true)]
        public Guid? SchemaDefault { get; set; }
        [MappingColumn("PARA_TICK", Nullable = true)]
        public bool CloseDayOpenTickets { get; set; }
    }
}
