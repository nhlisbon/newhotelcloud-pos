﻿using System;

namespace NewHotel.Contracts
{
    public abstract class SyncBaseRecord : BaseRecord<Guid>
    {
        public abstract DateTime LastModified { get; set; }

        public SyncBaseRecord() { }
    }
}