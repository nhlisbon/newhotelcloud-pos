﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
     [MappingQuery("TPPE_PK")]
    public class PricePeriodRecord:BaseRecord
    {
        [MappingColumn("TPRG_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("TPRG_PK", Nullable = false)]
        public Guid Rate { get; set; }
        [MappingColumn("TPPE_DAIN", Nullable = false)]
        public DateTime InitialDate { get; set; }
        [MappingColumn("TPPE_DAFI", Nullable = false)]
        public DateTime FinalDate { get; set; }

        public string InitialDateShort { get { return InitialDate.ToShortDateString(); } }
        public string FinalDateShort { get { return FinalDate.ToShortDateString(); } }
        public string CompleteDescription { get { return Description + "  " + InitialDateShort + "-" + FinalDateShort; } }
        public string Period { get { return InitialDateShort + "-" + FinalDateShort; } }
    }
}
