﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TPRG_PK")]
    public class ProductRateTypeRecord : BaseRecord
    {
        [MappingColumn("TPRG_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("UNMO_PK", Nullable = false)]
        public string Unmo { get; set; }
        //[MappingColumn("UNMO_SYMB", Nullable = true)]
        //public string CurrencySymbol { get; set; }
        [MappingColumn("IVAS_INCL", Nullable = false)]
        public bool TaxesIncluded { get; set; }
        [MappingColumn("TPRG_INAC", Nullable = false)]
        public bool Inactive { get; set; }
        [MappingColumn("TPRG_TYPE", Nullable = false)]
        public long Type { get; set; }
        [MappingColumn("TYPE_DESC", Nullable = false)]
        public string TypeDesc { get; set; }

        [MappingColumn("CATP_PK", Nullable = false)]
        public Guid PriceCategoryId { get; set; }

        [MappingColumn("CATP_DESC", Nullable = false)]
        public string PriceCategoryDescription { get; set; }
    }
}
