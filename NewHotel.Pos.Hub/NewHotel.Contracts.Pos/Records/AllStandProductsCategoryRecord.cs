﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("BOTV_PK")]
    public class AllStandProductsCategoryRecord : SyncBaseRecord
    {
        [MappingColumn("BCAT_PK", Nullable = false)]
        public Guid Category { get; set; }
        [MappingColumn("IPOS_PK", Nullable = false)]
        public Guid Stand { get; set; }
        [MappingColumn("BARTG_PK", Nullable = false)]
        public Guid Product { get; set; }
        [MappingColumn("BOTV_ORDE", Nullable = false)]
        public short Order { get; set; }
        [MappingColumn("BOTV_NEXT", Nullable = true)]
        public Guid? Next { get; set; }
        [MappingColumn("BOTV_LMOD", Nullable = true)]
        public override DateTime LastModified { get; set; }
    }
}