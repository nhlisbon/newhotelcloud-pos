﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("FAPP_PK")]
    public class FamilyPreparationsRecord : BaseRecord
    {
        [MappingColumn("FAMI_PK", Nullable = false)]
        public Guid FamilyId { get; set; }
        [MappingColumn("PREP_PK", Nullable = false)]
        public Guid PreparationId { get; set; }
    }
}
