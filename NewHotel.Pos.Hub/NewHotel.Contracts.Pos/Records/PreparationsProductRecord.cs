﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ARTP_PK")]
    public class ProductPreparationsRecord : BaseRecord
    {
        [MappingColumn("ARTG_PK", Nullable = false)]
        public Guid ProductId { get; set; }
        [MappingColumn("PREP_PK", Nullable = false)]
        public Guid PreparationId { get; set; }
    }
}
