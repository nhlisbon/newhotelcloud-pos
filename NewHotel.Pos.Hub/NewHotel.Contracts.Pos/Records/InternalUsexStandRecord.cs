﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
     [MappingQuery("PVCI_PK")]
    public class InternalUsexStandRecord:BaseRecord
    {
        [MappingColumn("COIN_PK", Nullable = false)]
         public Guid InternalUse { get; set; }
        [MappingColumn("IPOS_PK", Nullable = false)]
        public Guid Stand { get; set; }
        [MappingColumn("COIN_DESC", Nullable = false)]
        public string InternalUseDesc { get; set; }
        [MappingColumn("IPOS_DESC", Nullable = false)]
        public string StandDesc { get; set; }
        [MappingColumn("PVCI_LIMI", Nullable = true)]
        public double? MensualLimit { get; set; }
        [MappingColumn("PVCI_ILIM", Nullable = false)]
        public bool PaymentUnlimited { get; set; }
        [MappingColumn("PVCI_PORL", Nullable = true)]
        public decimal? PaymentPercent { get; set; }
    }
}
