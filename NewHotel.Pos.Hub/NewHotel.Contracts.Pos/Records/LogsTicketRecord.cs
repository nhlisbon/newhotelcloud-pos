﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts.Pos.Records
{
    [MappingQuery("LOTI_PK")]
    public class LogsTicketRecord : BaseRecord
    {
        [MappingColumn("TOPE_PK", Nullable = false)]
        public long Operation { get; set; }
        [MappingColumn("UTIL_PK", Nullable = false)]
        public Guid User { get; set; }
        [MappingColumn("LOTI_DAHO", Nullable = false)]
        public DateTime Time { get; set; }
        [MappingColumn("LOTI_DATR", Nullable = false)]
        public DateTime WorkDate { get; set; }
        [MappingColumn("LOTI_DETA", Nullable = false)]
        public string Observations { get; set; }
    }
}
