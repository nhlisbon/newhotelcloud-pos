﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TPRL_PK")]
    public class ProductPriceRateRecord : BaseRecord
    {
        [MappingColumn("ARTG_DESC", Nullable = false)]
        public string Product { get; set; }
        [MappingColumn("ARTG_CODI", Nullable = true)]
        public string ProductCodi { get; set; }
        [MappingColumn("TPRG_DESC", Nullable = false)]
        public string Rate { get; set; }
        [MappingColumn("ARTG_PK", Nullable = false)]
        public Guid ProductId { get; set; }
        [MappingColumn("TPPE_PK", Nullable = false)]
        public Guid PriceRateId { get; set; }
        [MappingColumn("TPPE_DAIN", Nullable = false)]
        public DateTime InitialDate { get; set; }
        [MappingColumn("TPPE_DAFI", Nullable = false)]
        public DateTime FinalDate { get; set; }
        [MappingColumn("TPRL_VALOR", Nullable = true)]
        public decimal Price { get; set; }
        [MappingColumn("TPRG_PK", Nullable = false)]
        public Guid RateId { get; set; }

        public string IniDate { get { return InitialDate.ToShortDateString(); } }
        public string FinDate { get { return FinalDate.ToShortDateString(); } }
        public string FullPeriod { get { return Rate + "  " + IniDate + "-" + FinDate; } }
    }
}
