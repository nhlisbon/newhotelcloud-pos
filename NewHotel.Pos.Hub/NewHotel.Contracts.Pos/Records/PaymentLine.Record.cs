﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PAVE_PK")]
    public class PaymentLineRecord : BaseRecord
    {
        [MappingColumn("VEND_PK", Nullable = false)]
        public Guid TicketId{ get; set; }        
        [MappingColumn("FORE_PK", Nullable = true)]
        public Guid? PaymentFormId { get; set; }
        [MappingColumn("FORE_DESC", Nullable = true)]
        public string PaymentFormDescription { get; set; }
        [MappingColumn("VEND_SERI", Nullable = false)]
        public string Serie { get; set; }
        [MappingColumn("VEND_DATI", Nullable = true)]
        public DateTime Date { get; set; }
        [MappingColumn("TIRE_PK", Nullable = true)]
        public long? PaymentType { get; set; }
        [MappingColumn("TIRE_DESC", Nullable = true)]
        public string PaymentTypeDescription { get; set; }
        [MappingColumn("PAVE_VALO", Nullable = true)]
        public decimal? PaymentValue { get; set; }
        [MappingColumn("VEND_TIFI", Nullable = false)]
        public short TicketFiscalType { get; set; }
        [MappingColumn("VEND_DAFI", Nullable = true)]
        public DateTime TicketFiscalDate { get; set; }
        [MappingColumn("PAVE_AUTH", Nullable = true)]
        public string AuthCode { get; set; }
        [MappingColumn("PAVE_ANUL", Nullable = false)]
        public bool IsAnul { get; set; }
    }
}
