﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("IPSL_PK")]
    public class SaloonStandRecord:BaseRecord
    {
        [MappingColumn("SALO_PK", Nullable = false)]
        public Guid Saloon { get; set; }
        [MappingColumn("SALO_DESC", Nullable = false)]
        public string SaloonDesc { get; set; }
        [MappingColumn("IPOS_DESC", Nullable = false)]
        public string StandDesc { get; set; }
        [MappingColumn("IPSL_ORDE", Nullable = false)]
        public string Orden { get; set; }
    }
}
