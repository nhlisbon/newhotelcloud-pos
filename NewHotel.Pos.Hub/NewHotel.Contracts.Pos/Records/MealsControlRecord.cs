﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [Serializable]
    [DataContract]
    public class MealsControlRecord : BaseRecord<Guid>
    {
        private bool _breakfast;
        private bool _lunch;
        private bool _dinner;

        [DataMember]
        public string Reservation { get; set; }
        [DataMember]
        public string GroupName { get; set; }
        [DataMember]
        public string GuestFullName { get; set; }
        [DataMember]
        public string Room { get; set; }
        [DataMember]
        public int? RoomSort { get; set; }
        [DataMember]
        public string ReservationStatus { get; set; }
        [DataMember]
        public string PensionMode { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public DateTime ArrivalDate { get; set; }
        [DataMember]
        public DateTime DepartureDate { get; set; }
        [DataMember]
        public bool HasBreakfast { get; set; }
        [DataMember]
        public bool HasLunch { get; set; }
        [DataMember]
        public bool HasDinner { get; set; }
        [DataMember]
        public bool Breakfast
        {
            get { return _breakfast; }
            set { Set(ref _breakfast, value, nameof(Breakfast)); }
        }
        [DataMember]
        public bool Lunch
        {
            get { return _lunch; }
            set { Set(ref _lunch, value, nameof(Lunch)); }
        }
        [DataMember]
        public bool Dinner
        {
            get { return _dinner; }
            set { Set(ref _dinner, value, nameof(Dinner)); }
        }

        [DataMember]
        public string ConsumptionCardNumber { get; set; }
    }
}