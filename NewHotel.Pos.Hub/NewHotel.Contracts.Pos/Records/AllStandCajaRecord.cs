﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("CAPV_PK")]
    public class AllStandCajaRecord : BaseRecord
    {
        [MappingColumn("CAJA_PK", Nullable = false)]
        public Guid Cashier { get; set; }
        [MappingColumn("IPOS_PK", Nullable = false)]
        public Guid Stand { get; set; }
    }
}
