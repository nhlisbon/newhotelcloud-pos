﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TAGR_PK")]
    public class TablesGroupRecord : BaseRecord
    {
        [MappingColumn("TAGR_PAXS", Nullable = false)]
        public int MaxPaxs { get; set; }
        [MappingColumn("SALO_PK", Nullable = false)]
        public Guid Saloon { get; set; }
        [MappingColumn("TAGR_TACO", Nullable = false)]
        public int TablesCount { get; set; }
    }
}
