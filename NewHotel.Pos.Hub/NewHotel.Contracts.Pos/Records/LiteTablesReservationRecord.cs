﻿using System;
using System.Linq;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("LITR_PK")]
    public class LiteTablesReservationRecord : SyncBaseRecord
    {
        [MappingColumn("LIRE_LMOD", Nullable = true)]
        public override DateTime LastModified { get; set; }
        [MappingColumn("LIRE_SEDO", Nullable = true)]
        public string SerieDocument { get; set; }
        [MappingColumn("LIRE_NUDO", Nullable = true)]
        public long? NumberDocument { get; set; }
        [MappingColumn("LITR_SERIE", Nullable = true)]
        public string SerieName { get; set; }
        [MappingColumn("LIRE_ANPH", Nullable = true)]
        public string FullName { get; set; }
        [MappingColumn("LITR_PHONE", Nullable = true)]
        public string CellPhone { get; set; }
        [MappingColumn("LITR_EMAI", Nullable = true)]
        public string Email { get; set; }
        [MappingColumn("LITR_ESTA", Nullable = false)]
        public long Status { get; set; }
        [MappingColumn("LITR_NUAD", Nullable = false)]
        public short PaxsAdults { get; set; }
        [MappingColumn("LITR_NUCR", Nullable = false)]
        public short PaxsChildren { get; set; }
        [MappingColumn("LITR_NUIN", Nullable = false)]
        public short PaxsBabies { get; set; }
        [MappingColumn("NACI_PK", Nullable = true)]
        public string CountryId { get; set; }
        [MappingColumn("IPOS_PK", Nullable = true)]
        public Guid? Stand { get; set; }
        [MappingColumn("IPOS_DESC", Nullable = true)]
        public string StandDesc { get; set; }
        [MappingColumn("SALA_PK", Nullable = true)]
        public Guid? Saloon { get; set; }
        [MappingColumn("SALA_DESC", Nullable = true)]
        public string SaloonDesc { get; set; }
        [MappingColumn("MESA_PK", Nullable = true)]
        public Guid? TableId { get; set; }
        [MappingColumn("MESA_DESC", Nullable = true)]
        public string TableDesc { get; set; }
        [MappingColumn("LITR_LOCK", Nullable = false)]
        public bool Lock { get; set; }
        [MappingColumn("LITR_SMOK", Nullable = true)]
        public bool? Smoke { get; set; }
        [MappingColumn("LIRE_OBSE", Nullable = true)]
        public string Comments { get; set; }
        [MappingColumn("LITR_CONF", Nullable = false)]
        public bool ConfirmationStatus { get; set; }
        [MappingColumn("SPLA_PK", Nullable = true)]
        public long? ConfirmationStatusId { get; set; }
        [MappingColumn("ESTA_DESC", Nullable = true)]
        public string ReservationStatus { get; set; }
        [MappingColumn("LIRE_DACR", Nullable = false)]
        public DateTime CreatedDate { get; set; }
        [MappingColumn("LIRE_DAEN", Nullable = false)]
        public DateTime Date { get; set; }
        [MappingColumn("LIRE_HOEN", Nullable = false)]
        public DateTime InitialTime { get; set; }
        [MappingColumn("FMT_HOEN", Nullable = true)]
        public string InitialTimeFmt { get; set; }
        [MappingColumn("LITR_HOAR", Nullable = true)]
        public DateTime? RealInitialTime { get; set; }
        [MappingColumn("FMT_HOAR", Nullable = true)]
        public string RealInitialTimeFmt { get; set; }
        [MappingColumn("LITR_HORF", Nullable = true)]
        public DateTime? RealFinalTime { get; set; }
        [MappingColumn("FMT_HORF", Nullable = true)]
        public string RealFinalTimeFmt { get; set; }
        [MappingColumn("LITR_HOFE", Nullable = false)]
        public DateTime FinalTime { get; set; }
        [MappingColumn("FMT_HOFE", Nullable = true)]
        public string FinalTimeFmt { get; set; }
        [MappingColumn("MESAS", Nullable = true)]
        public int NoTables { get; set; } = 1;
        [MappingColumn("LITR_QINF", Nullable = false)]
        public string QuickInformation { get; set; }
        [MappingColumn("SLST_PK", Nullable = true)]
        public Guid? SlotId { get; set; }
        [MappingColumn("LIRE_PK", Nullable = true)]
        public Guid? StayId { get; set; }
        [MappingColumn("ALOJ_ROOM", Nullable = true)]
        public string Room { get; set; }
        [MappingColumn("MOPE_CODI", Nullable = true)]
        public string MealPlan { get; set; }
        [MappingColumn("SLST_DMRE", Nullable = true)]
        public string SlotDuration { get; set; }
        [MappingColumn("PREF_DESC", Nullable = true)]
        public string Preferences { get; set; }
        [MappingColumn("DIET_DESC", Nullable = true)]
        public string Diets { get; set; }
        [MappingColumn("TIAT_DESC", Nullable = true)]
        public string Attentions { get; set; }
        [MappingColumn("ALLE_DESC", Nullable = true)]
        public string Allergies { get; set; }

        public string[] TablesDescription
        {
            get
            {
                if (!string.IsNullOrEmpty(TableDesc))
                    return TableDesc.Substring(1, TableDesc.Length - 2)
                        .Replace(",", "").Split(' ')
                        .Select(x => x.Trim())
                        .ToArray();

                return new string[0];
            }
        }

        public string TableDescription => TableDesc;

        public bool IsGroupTable => TablesDescription.Length > 1;

        public string PaxsDescription => $"{PaxsAdults} {PaxsChildren} {PaxsBabies}";

        public int Paxs => PaxsAdults + PaxsChildren + PaxsBabies;
    }
}