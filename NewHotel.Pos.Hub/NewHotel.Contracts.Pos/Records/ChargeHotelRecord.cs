﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [Serializable]
    [DataContract]
    [MappingQuery("HOTE_PK")]
    public class ChargeHotelRecord : BaseRecord<Guid>
    {
        [DataMember]
        [MappingColumn("HOTE_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}