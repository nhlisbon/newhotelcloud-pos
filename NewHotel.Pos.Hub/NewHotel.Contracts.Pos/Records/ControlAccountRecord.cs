﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("CTRL_PK")]
    public class ControlAccountRecord:BaseRecord
    {
        [MappingColumn("CTRL_DESC", Nullable = false)]
        public string Description { get; set; }
    }
}
