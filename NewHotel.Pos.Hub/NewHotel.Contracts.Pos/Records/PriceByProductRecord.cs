﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TPRL_PK")]
    public class PriceByProductRecord : BaseRecord
    {
        [MappingColumn("TPRG_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("TPPE_DAIN", Nullable = false)]
        public DateTime InitialDate { get; set; }
        [MappingColumn("TPPE_DAFI", Nullable = false)]
        public DateTime FinalDate { get; set; }
        [MappingColumn("TPRL_VALOR", Nullable = false)]
        public DateTime Price { get; set; }

        public string InitialDateShort { get { return InitialDate.ToShortDateString(); } }
        public string FinalDateShort { get { return FinalDate.ToShortDateString(); } }
    }
}
