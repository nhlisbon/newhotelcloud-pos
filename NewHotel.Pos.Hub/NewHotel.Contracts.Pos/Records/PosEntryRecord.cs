﻿using NewHotel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Contracts.Pos
{
    [MappingQuery("MOVI_PK")]
    public class PosEntryRecord : BaseRecord
    {
        [MappingColumn("DOFI_PK", Nullable = true)]
        public Guid? OfficialDocId { get; set; }

        [MappingColumn("CCCO_PK", Nullable = true)]
        public Guid? AccountId { get; set; }

        [MappingColumn("ARVE_PK", Nullable = true)]
        public Guid? ProductLineId { get; set; }

        [MappingColumn("PAVE_PK", Nullable = true)]
        public Guid? PaymentLineId { get; set; }

        public Guid EntryId => (Guid)Id;

        public bool Invoiced => OfficialDocId.HasValue;
    }
}
