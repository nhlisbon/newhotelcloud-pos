﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts.Pos.Records
{
    [MappingQuery("LISP_PK")]
    public class SpaSearchReservationProductRecord : BaseRecord
    {
        [MappingColumn("SSPA_PK", Nullable = true)]
        [DataMember]
        public Guid? ServiceId { get; set; }

        [MappingColumn("SSPA_DESC", Nullable = true)]
        [DataMember]
        public string ServiceDesc { get; set; }

        [MappingColumn("RINI_DESC", Nullable = true)]
        [DataMember]
        public string InitialResourceDesc { get; set; }

        [MappingColumn("RMED_DESC", Nullable = true)]
        [DataMember]
        public string MediumResourceDesc { get; set; }

        [MappingColumn("RFIN_DESC", Nullable = true)]
        [DataMember]
        public string FinalResourceDesc { get; set; }

        [MappingColumn("TINI_NAME", Nullable = true)]
        [DataMember]
        public string InitialTherapistDesc { get; set; }

        [MappingColumn("TMED_NAME", Nullable = true)]
        [DataMember]
        public string MediumTherapistDesc { get; set; }

        [MappingColumn("TFIN_NAME", Nullable = true)]
        [DataMember]
        public string FinalTherapistDesc { get; set; }

        [MappingColumn("ARTG_PK", Nullable = true)]
        [DataMember]
        public Guid? ProductId { get; set; }

        [MappingColumn("ARTG_DESC", Nullable = true)]
        [DataMember]
        public string ProductDesc { get; set; }

        [MappingColumn("LISP_HOIN", Nullable = true)]
        [DataMember]
        public DateTime? InitialHour { get; set; }

        [MappingColumn("FMT_HOIN", Nullable = true)]
        [DataMember]
        public string FmtInitialHour { get; set; }
    }
}