﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("AREA_PK")]
    public class AreaRecord : BaseRecord
    {
        [MappingColumn("AREA_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("AREA_COLO", Nullable = true)]
        public long? Color { get; set; }
        [MappingColumn("AREA_INAC", Nullable = true)]
        public bool Inactive { get; set; }
        [MappingColumn("AREA_NUCO", Nullable = true)]
        public short KitchenCopies { get; set; }
    }
}