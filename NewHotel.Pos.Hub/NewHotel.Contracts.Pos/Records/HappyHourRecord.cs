﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("HOUR_PK")]
    public class HappyHourRecord : BaseRecord
    {
        [MappingColumn("HOUR_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("HOUR_HORI", Nullable = false)]
        public DateTime InitialHour { get; set; }
        [MappingColumn("HOUR_HORF", Nullable = false)]
        public DateTime FinalHour { get; set; }
        [MappingColumn("HOUR_DIAS", Nullable = false)]
        public string DaysOfTheWeek { get; set; }
        [MappingColumn("TPRG_PK", Nullable = false)]
        public Guid TaxRate { get; set; }
        [MappingColumn("TPRG_DESC", Nullable = false)]
        public string TaxRateDescription { get; set; }
        [MappingColumn("ARTG_CANT", Nullable = false)]
        public short Quantity { get; set; }
        [MappingColumn("HOUR_INAC", Nullable = false)]
        public bool Inactive { get; set; }


        public bool Sunday { get { return (string.IsNullOrEmpty(DaysOfTheWeek)) ? false : DaysOfTheWeek[0] == '1'; } }
        public bool Monday { get { return (string.IsNullOrEmpty(DaysOfTheWeek)) ? false : DaysOfTheWeek[1] == '1'; } }
        public bool Tuesday { get { return (string.IsNullOrEmpty(DaysOfTheWeek)) ? false : DaysOfTheWeek[2] == '1'; } }
        public bool Wednesday { get { return (string.IsNullOrEmpty(DaysOfTheWeek)) ? false : DaysOfTheWeek[3] == '1'; } }
        public bool Thursday { get { return (string.IsNullOrEmpty(DaysOfTheWeek)) ? false : DaysOfTheWeek[4] == '1'; } }
        public bool Friday { get { return (string.IsNullOrEmpty(DaysOfTheWeek)) ? false : DaysOfTheWeek[5] == '1'; } }
        public bool Saturday { get { return (string.IsNullOrEmpty(DaysOfTheWeek)) ? false : DaysOfTheWeek[6] == '1'; } }
    }
}
