﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TICK_PK")]
    public class TicketRecord : BaseRecord
    {
        [MappingColumn("TICK_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("TICK_IVAF", Nullable = false)]
        public bool PrintTaxDetails { get; set; }
        [MappingColumn("TICK_HORA", Nullable = false)]
        public bool PrintOpeningTime { get; set; }
        [MappingColumn("TICK_HCIE", Nullable = false)]
        public bool PrintCloseDateTime { get; set; }
        [MappingColumn("TICK_SEPA", Nullable = false)]
        public bool PrintSeparators { get; set; }
        [MappingColumn("TICK_SHLO", Nullable = false)]
        public bool ShowLogo { get; set; }
        [MappingColumn("TICK_MSG1", Nullable = true)]
        public string PrintMessage2 { get; set; }
        [MappingColumn("TICK_MSG2", Nullable = true)]
        public string PrintMessage1 { get; set; }
        [MappingColumn("TICK_SIGN", Nullable = false)]
        public bool PrintSignature { get; set; }
        [MappingColumn("TICK_LINE", Nullable = false)]
        public short HeaderLinesEmpty { get; set; }
        [MappingColumn("TICK_LINP", Nullable = true)]
        public short? MinLines { get; set; }
        [MappingColumn("TICK_LOGO", Nullable = true)]
        public Blob? Logo { get; set; }
        [MappingColumn("TICK_ALIG", Nullable = true)]
        public short? Align { get; set; }
        [MappingColumn("TICK_COLU", Nullable = false)]
        public bool ColumnCaptions { get; set; }
        [MappingColumn("TICK_DISC", Nullable = false)]
        public bool SupressDiscount { get; set; }
        [MappingColumn("TICK_RECA", Nullable = false)]
        public bool SupressRecharge { get; set; }
        [MappingColumn("TICK_RECV", Nullable = false)]
        public bool PaymentInReceipts { get; set; }
        [MappingColumn("TICK_PAYM", Nullable = false)]
        public bool PaymentDetailed { get; set; }
        [MappingColumn("TICK_TIPS", Nullable = false)]
        public bool PrintTips { get; set; }
        [MappingColumn("TICK_LARG", Nullable = false)]
        public bool IsLarge { get; set; }
        [MappingColumn("TICK_COTI", Nullable = false)]
        public bool ShowCommentsTicket { get; set; }
    }
}