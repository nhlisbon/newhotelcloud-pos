﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("REDZ_PK")]
    public class ReductionZRecord : BaseRecord
    {
        [MappingColumn("CAJA_PK", Nullable = false)]
        public Guid CashierId { get; set; }
        [MappingColumn("REDZ_DARE", Nullable = false)]
        public DateTime RegistrationDate { get; set; }
        [MappingColumn("REDZ_HORE", Nullable = false)]
        public DateTime RegistrationTime { get; set; }
        [MappingColumn("REDZ_CONT", Nullable = false)]
        public short OperationCounter { get; set; }
        [MappingColumn("REDZ_CRES", Nullable = false)]
        public short RestartControlOrden { get; set; }
        [MappingColumn("REDZ_NUMB", Nullable = false)]
        public string ReductionNumber { get; set; }
        [MappingColumn("REDZ_COOI", Nullable = false)]
        public string InitialTicketNumber { get; set; }
        [MappingColumn("REDZ_COOF", Nullable = false)]
        public string FinalTicketNumber { get; set; }
        [MappingColumn("REDZ_TOTL", Nullable = false)]
        public decimal GrandTotal { get; set; }
        [MappingColumn("CANC_TOTL", Nullable = false)]
        public decimal CancellationTotal { get; set; }
        [MappingColumn("REDZ_VLIQ", Nullable = false)]
        public decimal NetValue { get; set; }
        [MappingColumn("REDZ_VSTR", Nullable = false)]
        public decimal SubstitutionTributableValue { get; set; }
        [MappingColumn("REDZ_VDES", Nullable = false)]
        public decimal DiscountValue { get; set; }
        [MappingColumn("REDZ_ISEN", Nullable = false)]
        public decimal InsentoValue { get; set; }
        [MappingColumn("REDZ_NTRI", Nullable = false)]
        public decimal NonTaxValue { get; set; }
        [MappingColumn("REDZ_VSUP", Nullable = false)]
        public decimal SuprimentoValue { get; set; }
        [MappingColumn("REDZ_VSAN", Nullable = false)]
        public decimal SangriaValue { get; set; }
        [MappingColumn("REDZ_TAXD", Nullable = false)]
        public decimal DebtTaxValue { get; set; }
        [MappingColumn("ISS_VALO", Nullable = false)]
        public decimal IssBaseValue { get; set; }
        [MappingColumn("REDZ_OTRO", Nullable = false)]
        public decimal OtherReceivables { get; set; }
        [MappingColumn("REDZ_VBRU", Nullable = false)]
        public decimal GrossSale { get; set; }
        [MappingColumn("REDZ_PRNS", Nullable = true)]
        public string FiscalPrinterSerial { get; set; }
        [MappingColumn("REDZ_PRMO", Nullable = true)]
        public string FiscalPrinterModel { get; set; }

        
        [System.Runtime.Serialization.DataMember]
        public System.Collections.Generic.List<ReductionZDetailRecord> Details { get;set; }
    }

    [MappingQuery("RZDE_PK")]
    public class ReductionZDetailRecord : BaseRecord
    {
        [MappingColumn("REDZ_BASE", Nullable = false)]
        public decimal IcmsAliquota { get; set; }
        [MappingColumn("REDZ_PERC", Nullable = false)]
        public decimal IcmsValue { get; set; }
    }
}
