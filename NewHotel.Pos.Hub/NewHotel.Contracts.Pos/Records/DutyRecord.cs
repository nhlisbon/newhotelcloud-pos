﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [Serializable]
    [DataContract]
    [MappingQuery("DUTY_PK")]
    public class DutyRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("DUTY_DESC", Nullable = false)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("DUTY_HORI", Nullable = false)]
        public DateTime InitialDate { get; set; }
        [DataMember]
        [MappingColumn("DUTY_HORF", Nullable = false)]
        public DateTime FinalDate { get; set; }

        public string IniHour { get { return InitialDate.ToShortTimeString(); } }
        public string FinHour { get { return FinalDate.ToShortTimeString(); } }
    }
}