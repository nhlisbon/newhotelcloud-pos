﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ARTG_PK")]
    public class AllProductsRecord : SyncBaseRecord
    {
        [MappingColumn("ARTG_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("ARTG_ABRE", Nullable = false)]
        public string Abbrev { get; set; }
        [MappingColumn("ARIM_PATH", Nullable = true)]
        public string ImagePath { get; set; }
        [MappingColumn("SEPA_PK", Nullable = true)]
        public Guid? Separator { get; set; }
        [MappingColumn("ARTG_STEP", Nullable = false)]
        public decimal Step { get; set; }
        [MappingColumn("GRUP_PK", Nullable = false)]
        public Guid GrupId { get; set; }
        [MappingColumn("FAMI_PK", Nullable = false)]
        public Guid FamiId { get; set; }
        [MappingColumn("SFAM_PK", Nullable = true)]
        public Guid? SfamId { get; set; }
        [MappingColumn("GRUP_DESC", Nullable = false)]
        public string GrupDesc { get; set; }
        [MappingColumn("FAMI_DESC", Nullable = false)]
        public string FamiDesc { get; set; }
        [MappingColumn("SFAM_DESC", Nullable = true)]
        public string SfamDesc { get; set; }
        [MappingColumn("ARTG_COLO", Nullable = true)]
        public long? Color { get; set; }
        [MappingColumn("ARTG_TIPR", Nullable = true)]
        public ProductPriceType PriceType { get; set; }
        [MappingColumn("ARTG_LMOD", Nullable = true)]
        public override DateTime LastModified { get; set; }      
        [MappingColumn("ARTG_CODI", Nullable = true)]
        public string Article { get; set; }
        [MappingColumn("SERV_CNCM", Nullable = true)]
        public string NCMCode { get; set; }
        [MappingColumn("SERV_CEST", Nullable = true)]
        public string CESTCode { get; set; }
        [MappingColumn("SERV_CFOP", Nullable = true)]
        public string CFOPCode { get; set; }
        [MappingColumn("SERV_CSTI", Nullable = true)]
        public string ICMSCSTCode { get; set; }
        [MappingColumn("SERV_CSTP", Nullable = true)]
        public string PISCSTCode { get; set; }
        [MappingColumn("SERV_CSTC", Nullable = true)]
        public string COFINSCSTCode { get; set; }      
        [MappingColumn("SERV_UNME", Nullable = true)]
        public string MeasureUnit { get; set; }
        [MappingColumn("SERV_LOCA")]
        public bool LocalProduct { get; set; }
        [MappingColumn("SERV_ROUN")]
        public bool UseRounding { get; set; }
        [MappingColumn("ARTG_CANT")]
        public int? CantStock { get; set; }

        public Blob Image { get; set; }
    }
}