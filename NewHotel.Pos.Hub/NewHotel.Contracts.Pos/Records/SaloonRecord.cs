﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("SALO_PK")]
    public class SaloonRecord : BaseRecord
    {
        [MappingColumn("SALO_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("MESAS", Nullable = false)]
        public int Tables { get; set; }
        [MappingColumn("SALO_WIDT", Nullable = true)]
        public short? Width { get; set; }
        [MappingColumn("SALO_HEIG", Nullable = true)]
        public short? Height { get; set; }

        [MappingColumn("MESA_FREE", Nullable = true)]
        public string ImageFreePath { get; set; }
        [MappingColumn("MESA_OCUP", Nullable = true)]
        public string ImageBusyPath { get; set; }
        [MappingColumn("SALO_IMAG", Nullable = true)]
        public string ImageBackgroundPath { get; set; }

        public Blob ImageFree { get; set; }
        public Blob ImageBusy { get; set; }
        public Blob ImageBackground { get; set; }

    }
}
