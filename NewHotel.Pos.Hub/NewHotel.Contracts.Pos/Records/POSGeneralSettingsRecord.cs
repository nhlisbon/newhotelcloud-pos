﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts.Pos.Records
{
    /// <summary>
    /// table="TCFG_NPOS"
    /// General Settings
    /// </summary>
    [Serializable]
    [DataContract]
    [MappingQuery("HOTE_PK")]
    public class POSGeneralSettingsRecord : BaseRecord
    {
        /// <summary>
        /// FAST_PAYM
        /// Flag: Usar pago rápido
        /// </summary>
        [DataMember]
        [MappingColumn("FAST_PAYM", Nullable = false)]
        public bool FastPayment { get; set; }
        /// <summary>
        /// PARA_FORE
        /// Forma de pago que se va a usar como FastPayment
        /// </summary>
        [DataMember]
        [MappingColumn("PARA_FORE", Nullable = true)]
        public Guid? FastPaymentFormId { get; set; }
        /// <summary>
        /// PARA_IVIN
        /// Flag: Impuestos incluidos en los precios
        /// </summary>
        [DataMember]
        [MappingColumn("PARA_IVIN", Nullable = false)]
        public bool TaxesIncluded { get; set; }
        /// <summary>
        /// PARA_ESIM
        /// Esquema de impuestos por defecto
        /// </summary>
        [DataMember]
        [MappingColumn("PARA_ESIM", Nullable = true)]
        public Guid? DefaultTaxSchemaId { get; set; }
        /// <summary>
        /// PARA_TICK
        /// Flag: Si se permite cerrar el día con tickets abiertos
        /// </summary>
        [DataMember]
        [MappingColumn("PARA_TICK", Nullable = false)]
        public bool CloseWithOpenTickets { get; set; }
        /// <summary>
        /// PARA_MTCO
        /// Motivo de cancelación por defecto para mezclar tickets                                                                                                                                 
        /// </summary>
        [DataMember]
        [MappingColumn("PARA_MTCO", Nullable = true)]
        public Guid? DefaultCancellationReasonId { get; set; }
        /// <summary>
        /// column="PARA_TICO"
        /// Tipo Cuenta por defecto (master, extra 1 .. extra 5) 
        /// </summary>
        [DataMember]
        [MappingColumn("PARA_TICO", Nullable = false)]
        public DailyAccountType AccountFolder { get; set; }
        /// <summary>
        /// PARA_NPAS
        /// Flag: flag: utilizar login numerico para acceder desde los POS locales (1.si, 0. no)
        /// </summary>
        [DataMember]
        [MappingColumn("PARA_NPAS", Nullable = false)]
        public bool UseNumericLogin { get; set; }
        /// <summary>
        /// PARA_TISE
        /// Producto utilizado como propina
        /// </summary>
        [DataMember]
        [MappingColumn("PARA_TISE", Nullable = true)]
        public Guid? ServiceTipId { get; set; }
        /// <summary>
        /// PARA_TIPE
        /// Prociento de propina automatica
        /// </summary>
        [DataMember]
        [MappingColumn("PARA_TIPE", Nullable = true)]
        public decimal? TipPercent { get; set; }
        /// <summary>
        /// PARA_TIVL
        /// Valor de propina automatica
        /// </summary>
        [DataMember]
        [MappingColumn("PARA_TIVL", Nullable = true)]
        public decimal? TipValue { get; set; }
        /// <summary>
        /// PARA_TIAT
        /// Flag: 1 incluir propina automaticamente
        /// </summary>
        [DataMember]
        [MappingColumn("PARA_TIAT", Nullable = true)]
        public bool AutomaticTip { get; set; }
        /// <summary>
        /// OPEN_PLAN
        /// Flag: 1 dejar abiertos ticket de meal plan con valores distintos de 0
        /// </summary>
        [DataMember]
        [MappingColumn("OPEN_PLAN", Nullable = true)]
        public bool LeaveMealPlanOpened { get; set; }
        /// <summary>
        /// COIN_VALO
        /// Valor agregado a tickets 0 con consumo interno menor que 100%
        /// </summary>
        [DataMember]
        [MappingColumn("COIN_VALO")]
        public decimal AddedValueOnHouseUse { get; set; }
        /// <summary>
        /// DESC_VALO
        /// Valor agregado a tickets 0 con descuento menor que 100%
        /// </summary>
        [DataMember]
        [MappingColumn("DESC_VALO")]
        public decimal AddedValueOnDiscount { get; set; }
        /// <summary>
        /// DESC_VALO
        /// Valor agregado a tickets 0 con descuento menor que 100%
        /// </summary>
        [DataMember]
        [MappingColumn("TICK_FISC")]
        public bool FiscalNumberOnTickets { get; set; }
        /// <summary>
        /// DATR_DASY
        /// No permitir fecha de trabajo al frente de la fecha del sistema
        /// </summary>
        [DataMember]
        [MappingColumn("DATR_DASY")]
        public bool WorkDateOverSystemDate { get; set; }
        /// <summary>
        /// NPOS_TIFI
        /// Tipo de Impresora Fiscal
        /// </summary>
        [DataMember]
        [MappingColumn("NPOS_TIFI")]
        public FiscalPOSType FiscalType { get; set; }
        /// <summary>
        /// NPOS_NCHC
        /// Numero CRIB asignado al hotel en Curacao. 
        /// Usado para la impresion fiscal
        /// </summary>
        [DataMember]
        [MappingColumn("NPOS_NCHC", Nullable = true)]
        public int? CribNumberHotel { get; set; }
        /// <summary>
        /// NPOS_FAON
        /// Exportacion online de documento. 
        /// </summary>
        [DataMember]
        [MappingColumn("NPOS_FAON")]
        public bool OnlineDocExportation { get; set; }
        /// <summary>
        /// TICK_HBOL
        /// Flag: Habilitar generacion de boletas
        /// </summary>
        [DataMember]
        [MappingColumn("NPOS_HBOL")]
        public bool AllowBallotDocument { get; set; }
        /// <summary>
        /// NPOS_HSCO
        /// Flag: Habilitar series de contingencia
        /// </summary>
        [DataMember]
        [MappingColumn("NPOS_HSCO")]
        public bool AllowContingencySerie { get; set; }
        /// <summary>
        /// NPOS_DEAU
        /// Flag: Aplicar descuentos automáticos sobre productos?
        /// </summary>
        [DataMember]
        [MappingColumn("NPOS_DEAU")]
        public bool ApplyAutomaticProductDiscount { get; set; }
        /// <summary>
        /// SIGN_TIPO
        /// Tipo de fiscalización a utilizar
        /// </summary>
        [DataMember]
        [MappingColumn("SIGN_TIPO", Nullable = true)]
        public DocumentSign SignType { get; set; }
        /// <summary>
        /// SIGN_VERS
        /// Versión de fiscalización a utilizar
        /// </summary>
        [DataMember]
        [MappingColumn("SIGN_VERS", Nullable = true)]
        public long? SignVersion { get; set; }
        /// <summary>
        /// SIGN_TEST
        /// Fiscalización en modo de prueba
        /// </summary>
        [DataMember]
        [MappingColumn("SIGN_TEST", Nullable = false)]
        public bool SignTestMode { get; set; }
        /// <summary>
        /// REGI_CAUX
        /// Código de la región fiscal
        /// </summary>
        [DataMember]
        [MappingColumn("REGI_CAUX", Nullable = true)]
        public string FiscalRegionCode { get; set; }
        /// <summary>
        /// FISC_NUMB
        /// Número fiscal
        /// </summary>
        [DataMember]
        [MappingColumn("FISC_NUMB", Nullable = true)]
        public string FiscalNumber { get; set; }
        /// <summary>
        /// FISC_USER
        /// Usuario para conectarse al servicio fiscal
        /// </summary>
        [DataMember]
        [MappingColumn("FISC_USER", Nullable = true)]
        public string FiscalServiceUser { get; set; }
        /// <summary>
        /// FISC_PASW
        /// Contraseña para conectarse al servicio fiscal
        /// </summary>
        [DataMember]
        [MappingColumn("FISC_PASW", Nullable = true)]
        public string FiscalServicePassword { get; set; }
        /// <summary>
        /// NACI_PK
        /// País
        /// </summary>
        [DataMember]
        [MappingColumn("NACI_PK", Nullable = false)]
        public string Country { get; set; }
        /// <summary>
        /// ANUL_DATR
        /// flag: anular factura solo en fecha de emisión
        /// </summary>
        [DataMember]
        [MappingColumn("ANUL_DATR", Nullable = false)]
        public bool CancelInvoicesOnlyEmissionDate { get; set; }
        /// <summary>
        /// ANUL_MONTH
        /// flag: anular factura solo en el mes de emisión
        /// </summary>
        [DataMember]
        [MappingColumn("ANUL_MONTH", Nullable = false)]
        public bool CancelInvoicesOnlyEmissionMonth { get; set; }
        /// <summary>
        /// ANUL_DAYS
        /// Cantidad de dias en los que se puede cancelar el documento
        /// </summary>
        [DataMember]
        [MappingColumn("ANUL_DAYS", Nullable = true)]
        public short? DaysToCancelInvoice { get; set; }
        /// <summary>
        /// TICK_MAFN
        /// Valor del ticket a partir del cual pedir número fiscal
        /// </summary>
        [DataMember]
        [MappingColumn("TICK_MAFN", Nullable = true)]
        public decimal? MandatoryValueForFiscalNumber { get; set; }
        /// <summary>
        /// TICK_VECA
        /// Ventana de tiempo para cancelar el ticket
        /// </summary>
        [DataMember]
        [MappingColumn("TICK_VECA", Nullable = true)]
        public TimeSpan? TicketCancelWindow { get; set; }
        /// <summary>
        /// SYNC_VEND
        /// Enviar para el cloud las ventas en tiempo real
        /// </summary>
        [DataMember]
        [MappingColumn("SYNC_VEND", Nullable = false)]
        public bool RealTimeTicketSync { get; set; }
        /// <summary>
        /// DRAW_CODE
        /// Código de apertura cash drawer
        /// </summary>
        [DataMember]
        public string CashDrawerCode { get; set; }
        /// <summary>
        /// BLOB_PATH
        /// Image Path
        /// </summary>
        [DataMember]
        public string ImagePath { get; set; }

        /// <summary>
        /// FAST_PLAN
        /// Flag: Enable fast plan
        /// </summary>
        [DataMember]
        [MappingColumn("FAST_PLAN", Nullable = false)]
        public bool FastPlan { get; set; }

        /// <summary>
        /// PLAN_ROOM
        /// Flag: Is mandatory the room info for the fast plan
        /// </summary>
        [DataMember]
        [MappingColumn("PLAN_ROOM", Nullable = false)]
        public bool IsMandatoryFastPlanRoomInfo { get; set; }

        /// <summary>
        /// EMIT_SOTK
        /// Emit only tickets
        /// Able only if there is not configured a fiscalization
        /// </summary>
        [DataMember]
        [MappingColumn("EMIT_SOTK", Nullable = false)]
        public bool EmitOnlyTickets { get; set; }
        
        /// <summary>
        /// NPOS_TIMF
        /// Flag: is the auto log out logic being used
        /// </summary>
        [DataMember]
        [MappingColumn("NPOS_TIMF", Nullable = false)]
        public bool UsingAutoLogOutTimer { get; set; }

        /// <summary>
        /// NPOS_TIME
        /// Tiempo de inactividad aceptado antes de auto log out
        /// </summary>
        [DataMember]
        [MappingColumn("NPOS_TIME", Nullable = false)]
        public float AutoLogOutTimerValue { get; set; }
        
        /// <summary>
        /// PROD_FTQT
        /// Flag: Shows/Hides the decimal cases of the quantity of a product in a ticket 
        /// If it's true it shows the decimal cases, otherwise it doesn't (false)
        /// </summary>
        [DataMember]
        [MappingColumn("PROD_FTQT", Nullable = false)]
        public bool ShowQuantityDecimalCase { get; set; }
        
        /// <summary>
        /// NPOS_IBTN
        /// Flag: Shows/Hides the invoice button in the application
        /// If it's true it shows the button, otherwise it doesn't (false)
        /// This happens when the hotel has a control user in PMS 
        /// </summary>
        [DataMember]
        [MappingColumn("NPOS_IBTN", Nullable = false)]
        public bool ShowInvoiceButton { get; set; }
        
        /// <summary>
        /// NPOS_ASGN
        /// Flag: Allows the digital sign in the android application
        /// </summary>
        [DataMember]
        [MappingColumn("NPOS_ASGN", Nullable = false)]
        public bool AllowDigitalSign { get; set; }
        
        /// <summary>
        /// NPOS_ASGN
        /// Flag: Allows the digital sign in the android application
        /// </summary>
        [DataMember]
        [MappingColumn("NPOS_ASEP", Nullable = false)]
        public bool AllowSelectSeparators { get; set; }

        /// <summary>
        /// NPOS_FCON
        /// Flag: Skips the fill of fiscal data step and always puts final consumer in it's place
        /// </summary>
        [DataMember]
        [MappingColumn("NPOS_FCON", Nullable = false)]
        public bool SkipFillFiscalDataAndPutFinalConsumer { get; set; }

		/// <summary>
		/// PAYS_INTALL Payment System Installation Identifier.
		/// </summary>
		[MappingColumn("PAYS_INTALL", Nullable = false)]
		public Guid? PaySystemInstallationId { get; set;}

        /// <summary>
        /// NPOS_ATSK Auto Table After All To Kitchen
        /// </summary>
        [DataMember]
        [MappingColumn("NPOS_ATSK", Nullable = false)]
		public bool AutoTableAfterAllToKitchen { get; set;}

        /// <summary>
        /// TICK_NCRE Permitir crear nota de credito si el ticket tuviera factura asociada
        /// </summary>
        [DataMember]
        [MappingColumn("TICK_NCRE", Nullable = false)]
        public bool AllowCreditNoteTicketReversal { get; set; }
    }
}