﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
     [MappingQuery("IMSE_PK")]
    public class PosTaxServicesRecord : BaseRecord
    {
         [MappingColumn("ESIM_PK", Nullable = false)]
         public Guid Esim { get; set; }
         [MappingColumn("SERV_PK", Nullable = false)]
         public Guid Serv { get; set; }
         [MappingColumn("TIVA_COD1", Nullable = true)]
         public Guid? TivaCod1 { get; set; }
         [MappingColumn("TIVA_COD2", Nullable = true)]
         public Guid? TivaCod2 { get; set; }
         [MappingColumn("TIVA_COD3", Nullable = true)]
         public Guid? TivaCod3 { get; set; }
         [MappingColumn("TIVA_APL2", Nullable = true)]
         public int? TivaApl1 { get; set; }
         [MappingColumn("TIVA_APL3", Nullable = true)]
         public int? TivaApl2 { get; set; }      
    }
}