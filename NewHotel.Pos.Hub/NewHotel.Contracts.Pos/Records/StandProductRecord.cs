﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PVAR_PK")]
    public class StandProductRecord : BaseRecord
    {
        [MappingColumn("ARTG_PK", Nullable = false)]
        public Guid Product { get; set; }
      
    }

    [MappingQuery("PVAR_PK")]
    public class StandProductAllRecord : BaseRecord
    {
        [MappingColumn("ARTG_PK", Nullable = false)]
        public Guid Product { get; set; }
        [MappingColumn("IPOS_PK", Nullable = false)]
        public Guid Stand { get; set; }
        [MappingColumn("SERV_PK")]
        public Guid? Service { get; set; }
        [MappingColumn("IPOS_DESC")]
        public string StandDesc { get; set; }
        [MappingColumn("ARTG_CODI")]
        public string Code { get; set; }
        [MappingColumn("ARTG_DESC")]
        public string ProductDesc { get; set; }
        [MappingColumn("ARTG_ABBRE")]
        public string ProductAbbre { get; set; }
        [MappingColumn("SERV_DESC")]
        public string ServiceDesc { get; set; }
        [MappingColumn("GRUP_PK")]
        public Guid Group { get; set; }
        [MappingColumn("FAMI_PK")]
        public Guid? Family { get; set; }
        [MappingColumn("SFAM_PK")]
        public Guid? SubFamily { get; set; }
        //[MappingColumn("ARTG_PK")]
        //public double? DefaultPrice { get; set; }
        //[MappingColumn("ARTG_PK")]
        //public Guid? DefaultSeparator { get; set; }
        //[MappingColumn("ARTG_PK")]
        //public decimal Step { get; set; }
        [MappingColumn("SERV_INAC")]
        public bool ProductInactive { get; set; }
    }

    [MappingQuery("ARTG_PK")]
    public class StandsProductRecord : BaseRecord
    {
        [MappingColumn("ARTG_CODI")]
        public string Code { get; set; }
        [MappingColumn("ARTG_DESC")]
        public string ProductDesc { get; set; }
        [MappingColumn("ARTG_ABBRE")]
        public string ProductAbbre { get; set; }
        [MappingColumn("GRUP_PK")]
        public Guid Group { get; set; }
        [MappingColumn("FAMI_PK")]
        public Guid? Family { get; set; }
        [MappingColumn("SFAM_PK")]
        public Guid? SubFamily { get; set; }
        [MappingColumn("SERV_INAC")]
        public bool ProductInactive { get; set; }
    }
}
