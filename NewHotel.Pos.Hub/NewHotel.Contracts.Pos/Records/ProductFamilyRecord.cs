﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("FAMI_PK")]
    public class ProductFamilyRecord : BaseRecord
    {
        [MappingColumn("FAMI_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("GRUP_PK", Nullable = false)]
        public Guid GroupId { get; set; }
        [MappingColumn("GRUP_DESC", Nullable = false)]
        public string Group { get; set; }
        [MappingColumn("FAMI_PREF", Nullable = true)]
        public string Prefix { get; set; }
        [MappingColumn("FAMI_PLUI", Nullable = true)]
        public long? IniRange { get; set; }
        [MappingColumn("FAMI_PLUF", Nullable = true)]
        public long? FinalRange { get; set; }
    }
}