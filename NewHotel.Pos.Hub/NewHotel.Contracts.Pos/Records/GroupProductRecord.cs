﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("GRUP_PK")]
    public class GroupProductRecord : BaseRecord
    {
        [MappingColumn("GRUP_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("GRUP_CHEQ", Nullable = false)]
        public bool ShowInTickets { get; set; }
        [MappingColumn("GRUP_RECA", Nullable = false)]
        public bool Recharge { get; set; }
        [MappingColumn("GRUP_PREF", Nullable = true)]
        public string Prefix { get; set; }
        [MappingColumn("GRUP_PLUI", Nullable = true)]
        public long? IniRange { get; set; }
        [MappingColumn("GRUP_PLUF", Nullable = true)]
        public long? FinalRange { get; set; }
        [MappingColumn("GRUP_ALCO", Nullable = true)]
        public bool? Alcoholic { get; set; }
        [MappingColumn("GRSE_PK", Nullable = false)]
        public Guid ServiceGroup { get; set; }
        [MappingColumn("CATS_PK", Nullable = false)]
        public long ServiceCategory { get; set; }
        [MappingColumn("GRSE_DESC", Nullable = false)]
        public string ServiceGroupDesc { get; set; }
        [MappingColumn("CATS_DESC", Nullable = false)]
        public string ServiceCategoryDesc { get; set; }
    }
}