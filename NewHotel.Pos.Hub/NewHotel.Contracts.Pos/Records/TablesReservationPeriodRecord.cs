﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TRPE_PK")]
    public class TablesReservationPeriodRecord : BaseRecord
    {
        [MappingColumn("IPOS_PK", Nullable = false)]
        public Guid Stand { get; set; }

        [MappingColumn("TFMT_HORI", Nullable = false)]
        public string InitialTime { get; set; }

        [MappingColumn("TFMT_HORF", Nullable = false)]
        public string FinalTime { get; set; }

        [MappingColumn("TRPE_HORI", Nullable = false)]
        public DateTime InitialDateTime { get; set; }

        [MappingColumn("TRPE_HORF", Nullable = false)]
        public DateTime FinalDateTime { get; set; }

        [MappingColumn("TRPE_DAYS", Nullable = false)]
        public string Days { get; set; }

        [MappingColumn("TRPE_INAC", Nullable = false)]
        public bool Inactive { get; set; }

        [MappingColumn("TRPE_REMX", Nullable = true)]
        public int? MaxReservations { get; set; }

        #region Dummy

        public bool Sunday { get { return (string.IsNullOrEmpty(Days)) ? false : Days[0] == '1'; } }
        public bool Monday { get { return (string.IsNullOrEmpty(Days)) ? false : Days[1] == '1'; } }
        public bool Tuesday { get { return (string.IsNullOrEmpty(Days)) ? false : Days[2] == '1'; } }
        public bool Wednesday { get { return (string.IsNullOrEmpty(Days)) ? false : Days[3] == '1'; } }
        public bool Thursday { get { return (string.IsNullOrEmpty(Days)) ? false : Days[4] == '1'; } }
        public bool Friday { get { return (string.IsNullOrEmpty(Days)) ? false : Days[5] == '1'; } }
        public bool Saturday { get { return (string.IsNullOrEmpty(Days)) ? false : Days[6] == '1'; } }

        public int Duration
        {
            get
            {
                return (int)(FinalDateTime.TimeOfDay - InitialDateTime.TimeOfDay).TotalMinutes;
            }
        }

        #endregion
    }
}
