﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("IPOS_PK")]
    public class MinStandWorkDateRecord:BaseRecord
    {
        [MappingColumn("IPOS_DATE", Nullable = true)]
        public DateTime StandDate { get; set; }
    }
}
