﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("CAPV_PK")]
    public class StandByCajaRecord:BaseRecord
    {
        [MappingColumn("DESCRIPTION", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("CAJA_PK", Nullable = false)]
        public Guid Caja { get; set; }
        [MappingColumn("IPOS_PK", Nullable = false)]
        public Guid Ipos { get; set; }
    }
}
