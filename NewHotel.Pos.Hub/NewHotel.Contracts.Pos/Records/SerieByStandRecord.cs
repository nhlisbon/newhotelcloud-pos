﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("DOHO_PK")]
    public class SerieByStandRecord : BaseRecord
    {
        [MappingColumn("SEDO_PTEX", Nullable = true)]
        public string TextPref { get; set; }
        [MappingColumn("sedo_saft", Nullable = true)]
        public string Signature { get; set; }
        [MappingColumn("SEDO_PNUM", Nullable = true)]
        public long? NumberPref { get; set; }
        [MappingColumn("SEDO_NUFA", Nullable = true)]
        public long NextNumber { get; set; }
        [MappingColumn("SEDO_ORDE", Nullable = true)]
        public long Orden { get; set; }
        [MappingColumn("SEDO_SEAC", Nullable = true)]
        public long Status { get; set; }
        [MappingColumn("SEDO_DAFI", Nullable = true)]
        public DateTime? FinalDate { get; set; }
        [MappingColumn("SEDO_NUFI", Nullable = true)]
        public long? FinalNumber { get; set; }
        [MappingColumn("IPOS_DESC", Nullable = true)]
        public string StandDesc { get; set; }
        [MappingColumn("CAJA_DESC", Nullable = true)]
        public string CajaDesc { get; set; }
        [MappingColumn("IPOS_PK", Nullable = true)]
        public Guid? Stand { get; set; }
        [MappingColumn("CAJA_PK", Nullable = true)]
        public Guid? Caja { get; set; }
        [MappingColumn("SEDO_PK", Nullable = true)]
        public Guid SerieId { get; set; }
        [MappingColumn("TIDO_DESC", Nullable = true)]
        public string SerieTypeDesc { get; set; }
        [MappingColumn("TIDO_PK", Nullable = true)]
        public POSDocumentType SerieType { get; set; }

        public string Serie 
        {
            get
            {
                if (Orden == (long)SerieOrder.TextPlusNumber)
                    return TextPref + NumberPref.ToString();
                else
                    return NumberPref + TextPref;
            } 
        }

        public string StatusDesc 
        {
            get 
            {
                switch (Status)
                {
                    case (long)SerieStatus.Active:
                        return "Active";
                    case (long)SerieStatus.Inactive:
                        return "Inactive";
                    default:
                        return "Future";
                }
            }
        }

        public string EndSerie
        {
            get
            {
                if (FinalDate.HasValue)
                    return FinalDate.Value.ToShortDateString();
                
                return string.Empty;
            }
        }
    }
}