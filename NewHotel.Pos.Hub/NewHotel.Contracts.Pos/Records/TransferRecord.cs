﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
     [MappingQuery("TRAN_PK")]
    public class TransferRecord:BaseRecord
    {
        [MappingColumn("VEND_ORIG", Nullable = false)]
        public Guid TicketOrig { get; set; }
        [MappingColumn("IPOS_ORIG", Nullable = false)]
        public Guid StandOrig { get; set; }
        [MappingColumn("CAJA_ORIG", Nullable = false)]
        public Guid CajaOrig { get; set; }
        [MappingColumn("VEND_DEST", Nullable = true)]
        public Guid? TicketDest { get; set; }
        [MappingColumn("IPOS_DEST", Nullable = false)]
        public Guid StandDest { get; set; }
        [MappingColumn("CAJA_DEST", Nullable = true)]
        public Guid? CajaDest { get; set; }
        [MappingColumn("TRAN_DATE", Nullable = false)]
        public DateTime TranDate { get; set; }
        [MappingColumn("TRAN_TIME", Nullable = false)]
        public DateTime TranTime { get; set; }
        [MappingColumn("ACEP_DATE", Nullable = true)]
        public DateTime? AcepDate { get; set; }
        [MappingColumn("ACEP_TIME", Nullable = true)]
        public DateTime? AcepTime { get; set; }
        [MappingColumn("UTTR_DESC", Nullable = true)]
        public string UtilTranDesc { get; set; }
        [MappingColumn("UTAC_DESC", Nullable = true)]
        public string UtilAcepDesc { get; set; }
        [MappingColumn("VDOR_DESC", Nullable = true)]
        public string TicketOrigDesc { get; set; }
        [MappingColumn("STOR_DESC", Nullable = true)]
        public string StandOrigDesc { get; set; }
        [MappingColumn("CAOR_DESC", Nullable = true)]
        public string CajaOrigDesc { get; set; }
        [MappingColumn("VDDE_DESC", Nullable = true)]
        public string TicketDestDesc { get; set; }
        [MappingColumn("STDE_DESC", Nullable = true)]
        public string StandDestDesc { get; set; }
        [MappingColumn("CADE_DESC", Nullable = true)]
        public string CajaDestDesc { get; set; }
       
    }
}
