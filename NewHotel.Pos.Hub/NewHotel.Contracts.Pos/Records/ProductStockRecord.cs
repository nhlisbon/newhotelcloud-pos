﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
	[MappingQuery("ARST_PK")]
	public class ProductStockRecord : BaseRecord
    {
		[MappingColumn("ARTG_PK", Nullable = false)]
		public Guid ProductId { get; set; }
		[MappingColumn("ARST_QTDS", Nullable = false)]
		public decimal Quantity { get; set; }
		[MappingColumn("ARVE_QTDS", Nullable = false)]
		public decimal Used { get; set; }

		[MappingColumn("ARST_DATR", Nullable = false)]
		public DateTime Date { get; set; }
    }
 
}
