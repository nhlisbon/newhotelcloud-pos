﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts.Pos.Records
{
    [Serializable]
    [DataContract]
    [MappingQuery("TICK_PK")]
    public class POSTicketPrintConfigurationRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("TICK_DESC", Nullable = false)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("TICK_IVAF", Nullable = false)]
        public bool PrintTaxDetails { get; set; }
        [DataMember]
        [MappingColumn("TICK_HORA", Nullable = false)]
        public bool PrintOpeningTime { get; set; }
        [DataMember]
        [MappingColumn("TICK_HCIE", Nullable = false)]
        public bool PrintCloseDateTime { get; set; }
        [DataMember]
        [MappingColumn("TICK_SEPA", Nullable = false)]
        public bool PrintSeparators { get; set; }
        [DataMember]
        [MappingColumn("TICK_SHLO", Nullable = false)]
        public bool ShowLogo { get; set; }
        [DataMember]
        [MappingColumn("TICK_MSG1", Nullable = true)]
        public string PrintMessage2 { get; set; }
        [DataMember]
        [MappingColumn("TICK_MSG2", Nullable = true)]
        public string PrintMessage1 { get; set; }
        [DataMember]
        [MappingColumn("TICK_SIGN", Nullable = false)]
        public bool PrintSignature { get; set; }
        [DataMember]
        [MappingColumn("TICK_LINE", Nullable = false)]
        public short HeaderLinesEmpty { get; set; }
        [DataMember]
        [MappingColumn("TICK_LINP", Nullable = true)]
        public short? MinLines { get; set; }
        [DataMember]
        [MappingColumn("TICK_LOGO", Nullable = true)]
        public Blob? Logo { get; set; }
        [DataMember]
        [MappingColumn("TICK_ALIG", Nullable = true)]
        public short? Align { get; set; }
        [DataMember]
        [MappingColumn("TICK_COLU", Nullable = false)]
        public bool ColumnCaptions { get; set; }
        [DataMember]
        [MappingColumn("TICK_DISC", Nullable = false)]
        public bool SupressDiscount { get; set; }
        [DataMember]
        [MappingColumn("TICK_RECA", Nullable = false)]
        public bool SupressRecharge { get; set; }
        [DataMember]
        [MappingColumn("TICK_RECV", Nullable = false)]
        public bool PaymentInReceipts { get; set; }
        [DataMember]
        [MappingColumn("TICK_PAYM", Nullable = false)]
        public bool PaymentDetailed { get; set; }
        [DataMember]
        [MappingColumn("TICK_TIPS", Nullable = false)]
        public bool PrintTips { get; set; }
        [DataMember]
        [MappingColumn("TICK_LARG", Nullable = false)]
        public bool IsLarge { get; set; }
        [DataMember]
        [MappingColumn("TICK_COTI", Nullable = false)]
        public bool ShowCommentsTicket { get; set; }
        [DataMember]
        [MappingColumn("TICK_COPR", Nullable = true)]
        public bool ShowCommentsProducts { get; set; }
        [DataMember]
        [MappingColumn("TICK_LPRO", Nullable = true)]
        public bool PrintTipsLines { get; set; }
        [DataMember]
        [MappingColumn("TICK_TSUG", Nullable = false)]
        public bool TipSuggestion { get; set; }
        [DataMember]
        [MappingColumn("TICK_RCDS", Nullable = false)]
        public bool RoomChargeDigitalSignature { get; set; }
        [DataMember]
        [MappingColumn("TICK_TAXD", Nullable = false)]
        public bool PrintTaxDetailsPerLine { get; set; }
        [DataMember]
        [MappingColumn("TICK_SUBT", Nullable = false)]
        public bool PrintSubtotal { get; set; }
        [DataMember]
        [MappingColumn("TICK_TYST", Nullable = false)]
        public KindSubTotal? SubTotalToPrint { get; set; }
        [DataMember]
        [MappingColumn("TICK_CCOP", Nullable = false)]
        public short Copies { get; set; }
        [DataMember]
        [MappingColumn("TICK_FISC", Nullable = true)]
        public string FiscalDescription { get; set; }
        [DataMember]
        public List<TipSuggestionContract> TipSuggestions { get; set; }
		[DataMember]
		[MappingColumn("TICK_ASGO", Nullable = false)]
		public bool ShowSignatureOptions { get; set; }
		[DataMember]
		[MappingColumn("TICK_ARNU", Nullable = false)]
		public string AlternateRoomNumber { get; set; }
		[DataMember]
		[MappingColumn("TICK_ANOM", Nullable = false)]
		public string AlternateName { get; set; }
		[DataMember]
		[MappingColumn("TICK_ASIG", Nullable = false)]
		public string AlternateSignature { get; set; }
        [DataMember]
		[MappingColumn("TICK_PDDE", Nullable = false)]
		public bool PrintDiscountDescription { get; set; }
	}
}