﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts.Pos.Records
{
    [MappingQuery("SLST_PK")]
    public class StandTimeSlotRecord : BaseRecord
    {
        /// <summary>
        /// Traducción de la descripción
        /// </summary>
        [MappingColumn("SLST_DESC")]
        public string Description { get; set; }

        /// <summary>
        /// Stand Id
        /// </summary>
        [MappingColumn("IPOS_PK")]
        public Guid StandId { get; set; }

        /// <summary>
        /// Horas de inicio separadas por comas
        /// </summary>
        [MappingColumn("SLST_HOURS")]
        public string StartHours { get; set; }

        /// <summary>
        /// Minutos de intervalos separados por comas
        /// </summary>
        [MappingColumn("SLST_INTMIN")]
        public string StartMinutes { get; set; }

        /// <summary>
        /// Porciento maximo de la capacidad de paxs del stand que esta permido reservar
        /// </summary>
        [MappingColumn("SLST_PPMA")]
        public int? PaxsMaxPercent { get; set; }

        /// <summary>
        /// Duracion media en minutos para una reserva de mesa
        /// </summary>
        [MappingColumn("SLST_DMRE")]
        public int ReservationDurationEstimated { get; set; }

        /// <summary>
        /// Numero maximo de la capacidad de paxs del slot
        /// </summary>
        [MappingColumn("SLST_PAMA")]
        public int? PaxsMaxNumber { get; set; }
    }
}
