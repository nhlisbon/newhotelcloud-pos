﻿namespace NewHotel.Contracts.Pos.Records
{
    public class SpaServiceRecord
    {
        public SpaSearchReservationProductRecord[] Items { get; set; }
        public string[] Allergies { get; set; }
        public string UncommonAllergies { get; set; }
        public string[] Diets {get; set;}
    }
}