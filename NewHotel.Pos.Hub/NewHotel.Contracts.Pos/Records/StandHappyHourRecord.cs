﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("HAIP_PK")]
    public class StandHappyHourRecord : BaseRecord
    {
        [MappingColumn("IPOS_PK", Nullable = false)]
        public Guid Stand { get; set; }
        [MappingColumn("HOUR_PK", Nullable = false)]
        public Guid HappyHour { get; set; }
    }
}
