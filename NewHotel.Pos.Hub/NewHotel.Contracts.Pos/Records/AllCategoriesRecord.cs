﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("BCAT_PK")]
    public class AllCategoriesRecord : SyncBaseRecord
    {
        [MappingColumn("CAT_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("BCAT_CATE", Nullable = true)]
        public Guid? ParentCategory { get; set; }
        [MappingColumn("BCAT_IMAG", Nullable = true)]
        public Blob? Image { get; set; }
        [MappingColumn("BCAT_ORDE", Nullable = true)]
        public int Orden { get; set; }
        [MappingColumn("BCAT_LMOD", Nullable = true)]
        public override DateTime LastModified { get; set; }
    }
}