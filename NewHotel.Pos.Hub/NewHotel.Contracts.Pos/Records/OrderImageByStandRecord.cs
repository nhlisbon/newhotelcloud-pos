﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.IO;

namespace NewHotel.Contracts
{
      [MappingQuery("BOTP_PK")]
    public class OrderImageByStandRecord:BaseRecord
    {
        [MappingColumn("IPOS_PK", Nullable = false)]
        public Guid Stand { get; set; }
        [MappingColumn("FORI_PK", Nullable = false)]
        public Guid Fori { get; set; }
        [MappingColumn("BOTP_ORDE", Nullable = true)]
        public int Order { get; set; }
        [MappingColumn("FORI_PATH", Nullable = true)]
        public string Path { get; set; }
        [MappingColumn("FORE_DESC", Nullable = true)]
        public string ForeDesc { get; set; }
        [MappingColumn("FORE_ABRE", Nullable = true)]
        public string ForeAbre { get; set; }

        public Blob? Image
        {
            get;
            set;
        }

        #region Fore
        [MappingColumn("FORE_PK", Nullable = false)]
        public Guid Fore { get; set; }
        [MappingColumn("FORE_CACR", Nullable = true)]
        public char CreditCardType { get; set; }
        [MappingColumn("FORE_CASH", Nullable = true)]
        public char CashType { get; set; }
        [MappingColumn("FORE_EVIF", Nullable = true)]
        public string EvaluatedValueFiscalPrinter { get; set; }
        [MappingColumn("FORE_IMPF", Nullable = true)]
        public char UseInFiscalPrinter { get; set; }
        [MappingColumn("FORE_INAC", Nullable = true)]
        public char Inactive { get; set; }
        [MappingColumn("FORE_ORDE", Nullable = true)]
        public short ForeOrder { get; set; }
        [MappingColumn("FORE_PCRM", Nullable = true)]
        public decimal? ValueBaseCurrency { get; set; }
        [MappingColumn("FORE_TEND", Nullable = true)]
        public string TenderValueFiscalPrinter { get; set; }
        [MappingColumn("REPO_CASH", Nullable = true)]
        public char CajaReport { get; set; }
        [MappingColumn("UNMO_PK", Nullable = false)]
        public string Currency { get; set; }
        #endregion
    }
}
