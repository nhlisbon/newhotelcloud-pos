﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PAVE_PK")]
    public class SalesCajaRecord : BaseRecord
    {
        [MappingColumn("PAY_DESC", Nullable = true)]
        public string PaymentDescription { get; set; }
        [MappingColumn("PAVE_VALO", Nullable = true)]
        public decimal Importe { get; set; }
        [MappingColumn("UTIL_DESC", Nullable = true)]
        public string UserDesc { get; set; }
        [MappingColumn("TURN_CODI", Nullable = true)]
        public string Shift { get; set; }
    }
}
