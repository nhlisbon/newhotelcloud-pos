﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PREP_PK")]
    public class PreparationsRecord:BaseRecord
    {
         [MappingColumn("PREP_DESC", Nullable = false)]
         public string Description { get; set; }
    }
}
