﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("COIN_PK")]
    public class InternalUseRecord:BaseRecord
    {
        [MappingColumn("COIN_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("COIN_INVI", Nullable = false)]
        public bool Invitations { get; set; }
        [MappingColumn("COIN_INAC", Nullable = false)]
        public bool State { get; set; }
    }
}
