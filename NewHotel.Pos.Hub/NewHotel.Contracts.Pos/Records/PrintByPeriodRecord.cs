﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("IMAR_PK")]
    public class PrintByPeriodRecord:BaseRecord
    {
        [MappingColumn("PRIN_PATH", Nullable = true)]
        public string Path { get; set; }
        [MappingColumn("PRIN_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("AREA_DESC", Nullable = false)]
        public string Area { get; set; }
    }
}
