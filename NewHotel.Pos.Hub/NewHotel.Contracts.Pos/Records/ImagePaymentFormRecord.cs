﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.IO;

namespace NewHotel.Contracts
{
     [MappingQuery("FORE_PK")]
    public class ImagePaymentFormRecord:BaseRecord
    {
        [MappingColumn("FORE_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("TIRE_DESC", Nullable = true)]
        public string TireDescription { get; set; }
        [MappingColumn("FORI_PATH", Nullable = true)]
        public string Path { get; set; }

        public Blob? Image
        {
            get;
            set;
        }
    }
}
