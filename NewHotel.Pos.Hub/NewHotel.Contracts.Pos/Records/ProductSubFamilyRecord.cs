﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("SFAM_PK")]
    public class ProductSubFamilyRecord:BaseRecord
    {
        [MappingColumn("SFAM_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("FAMI_DESC", Nullable = false)]
        public string Family { get; set; }
        [MappingColumn("FAMI_PK", Nullable = false)]
        public Guid FamilyId { get; set; }
    }
}
