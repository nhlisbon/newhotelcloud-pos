﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ARAR_PK")]
    public class ProductsStandbyAreaRecord : BaseRecord
    {
        [MappingColumn("IPOS_PK", Nullable = false)]
        public Guid StandId { get; set; }
        [MappingColumn("ARTG_PK", Nullable = true)]
        public Guid ProductId { get; set; }
        [MappingColumn("AREA_PK", Nullable = true)]
        public Guid AreaId { get; set; }
        [MappingColumn("ARAR_ORDE", Nullable = true)]
        public long Order { get; set; }
        [MappingColumn("IPOS_DESC", Nullable = false)]
        public string StandDesc { get; set; }
        [MappingColumn("AREA_DESC", Nullable = false)]
        public string AreaDesc { get; set; }
    }
}
