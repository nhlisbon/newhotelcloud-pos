﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
     [MappingQuery("IPOS_PK")]
    public class GastStandRecord:BaseRecord
    {
         [MappingColumn("IPOS_CCCO", Nullable = false)]
         public short AccountMoment { get; set; }
         //[MappingColumn("IPMA_DESC", Nullable = false)]
         //public string ConfigurationDescription { get; set; }
         //[MappingColumn("IPOS_REQI", Nullable = false)]
         //public bool PrintType { get; set; }
         //[MappingColumn("IPOS_MARD", Nullable = false)]
         //public bool TicketComplete { get; set; }
         //[MappingColumn("IPOS_PMAR", Nullable = false)]
         //public bool AllowMarchInd { get; set; }
         //[MappingColumn("IPOS_CERM", Nullable = false)]
         //public short CloseAutomaticTable { get; set; }
         //[MappingColumn("IPOS_MACO", Nullable = false)]
         //public bool MarchPrintReceive { get; set; }
         //[MappingColumn("TICK_DESC", Nullable = false)]
         //public string TicketDescription { get; set; }
         //[MappingColumn("TICK_MSG1", Nullable = false)]
         //public string TicketMessage1 { get; set; }
    }
}
