﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ARTB_PK")]
    public class TableProductRecord:BaseRecord
    {
        [MappingColumn("ARTB_PRO", Nullable = false)]
        public string Product { get; set; }
        [MappingColumn("ARTB_COMP", Nullable = false)]
        public string ComponentProduct { get; set; }
        [MappingColumn("ARTB_CANT", Nullable = false)]
        public long Quantity { get; set; }
    }
}
