﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("IPMA_PK")]
    public class CookMarchRecord:BaseRecord
    {
        [MappingColumn("IPMA_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("TICK_PK", Nullable = false)]
        public Guid Ticket { get; set; }
        [MappingColumn("TICK_DESC", Nullable = false)]
        public string TicketsDescription { get; set; }
        //[MappingColumn("IPOS_MARD", Nullable = false)]
        //public bool MarchCompleteTicket { get; set; }
        //[MappingColumn("IPOS_CERM", Nullable = false)]
        //public bool CloseTableAutomatic { get; set; }
        //[MappingColumn("IPOS_MACO", Nullable = false)]
        //public bool MarchWhenPrintReceive { get; set; }
        //[MappingColumn("IPOS_PMAR", Nullable = false)]
        //public bool MarchAllAndAllowMarchAgain { get; set; }
        //[MappingColumn("IPOS_REQI", Nullable = false)]
        //public bool MarchAllPrintProducts { get; set; }
    }
}
