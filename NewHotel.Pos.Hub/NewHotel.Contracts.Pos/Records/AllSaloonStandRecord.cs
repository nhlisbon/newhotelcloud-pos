﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("IPSL_PK")]
    public class AllSaloonStandRecord : BaseRecord
    {
        [MappingColumn("SALO_PK", Nullable = false)]
        public Guid Saloon { get; set; }
        [MappingColumn("IPOS_PK", Nullable = false)]
        public Guid Stand { get; set; }
        [MappingColumn("IPSL_ORDE", Nullable = false)]
        public short Orden { get; set; }
    }
}
