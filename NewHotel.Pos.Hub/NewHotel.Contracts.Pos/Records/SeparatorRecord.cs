﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("SEPA_PK")]
    public class SeparatorRecord:BaseRecord
    {
        [MappingColumn("SEPA_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("SEPA_ORDE", Nullable = false)]
        public short ImpresionOrden { get; set; }
        [MappingColumn("SEPA_MAXP", Nullable = false)]
        public short PaxMax { get; set; }
        [MappingColumn("SEPA_ACUM", Nullable = false)]
        public bool AcumRound { get; set; }
        [MappingColumn("SEPA_TICK", Nullable = false)]
        public bool ShowInTicket { get; set; }
    }
}
