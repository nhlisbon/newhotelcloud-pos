﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts.Pos.Records
{
    [Serializable]
    [DataContract]
    [MappingQuery("VEND_PK")]
    public class ClosedTicketRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("VEND_COAP", Nullable = true)]
        public string OpeningNumber { get; set; }
        [DataMember]
        [MappingColumn("VEND_NUMBER", Nullable = true)]
        public string Number { get; set; }
        [DataMember]
        [MappingColumn("VEND_DATF")]
        public DateTime CloseDate { get; set; }
        [DataMember]
        [MappingColumn("VEND_HORF")]
        public DateTime CloseTime { get; set; }
        [DataMember]
        [MappingColumn("VEND_VALO")]
        public decimal Total { get; set; }
        [DataMember]
        [MappingColumn("VEND_TIDO")]
        public POSDocumentType DocumentType { get; set; }
        [DataMember]
        [MappingColumn("VEND_ANUL")]
        public bool Cancelled { get; set; }
        [DataMember]
        [MappingColumn("UNMO_PK", Nullable = true)]
        public string CurrencyId { get; set; }
        [DataMember]
        [MappingColumn("IPOS_DESC", Nullable = true)]
        public string StandDescription { get; set; }
        [DataMember]
        [MappingColumn("CAJA_DESC", Nullable = true)]
        public string CashierDescription { get; set; }
        [DataMember]
        [MappingColumn("DOCU_FACT", Nullable = true)]
        public string InvoiceNumber { get; set; }
        [DataMember]
        [MappingColumn("DOCU_NCRE", Nullable = true)]
        public string CreditNoteNumber { get; set; }
        [DataMember]
        [MappingColumn("DOCU_BOLE", Nullable = true)]
        public string BallotNumber { get; set; }
        [DataMember]
        [MappingColumn("PAVE_ROOM", Nullable = true)]
        public string RoomNumber { get; set; }
        [DataMember]
        [MappingColumn("CCCO_PK", Nullable = true)]
        public Guid? AccountId { get; set; }
        [DataMember]
        [MappingColumn("CCCO_DESC", Nullable = true)]
        public string AccountDescription { get; set; }

        [DataMember]
        [MappingColumn("VEND_NFFA", Nullable = true)]
        public long? FpInvoiceNumber { get; set; }
        [DataMember]
        [MappingColumn("VEND_NFNC", Nullable = true)]
        public long? FpCreditNoteNumber { get; set; }
        [DataMember]
        [MappingColumn("VEND_IFFA", Nullable = true)]
        public string FpSerialInvoice { get; set; }
        [DataMember]
        [MappingColumn("VEND_IFNC", Nullable = true)]
        public string FpSerialCreditNote { get; set; }

        public bool IsTicket { get { return DocumentType == POSDocumentType.Ticket; } }
        public bool IsBallot { get { return DocumentType == POSDocumentType.Ballot; } }
        public bool IsInvoice { get { return DocumentType == POSDocumentType.CashInvoice; } }
        public bool IsCreditNote { get { return DocumentType == POSDocumentType.CashCreditNote; } }
    }
}