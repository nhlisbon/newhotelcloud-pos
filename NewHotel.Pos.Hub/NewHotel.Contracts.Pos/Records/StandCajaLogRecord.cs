﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("LOIP_PK")]
    public class StandCajaLogRecord : BaseRecord
    {
        [MappingColumn("CAJA_DESC", Nullable = false)]
        public string Caja { get; set; }
        [MappingColumn("IPOS_DESC", Nullable = false)]
        public string Stand { get; set; }
        [MappingColumn("UTIL_DESC", Nullable = false)]
        public string User { get; set; }
        [MappingColumn("OPER_DESC", Nullable = false)]
        public string Operation { get; set; }
        [MappingColumn("LOIP_DAHO", Nullable = false)]
        public DateTime Time { get; set; }
        [MappingColumn("LOIP_DATR", Nullable = false)]
        public DateTime WorkDate { get; set; }
        [MappingColumn("LOIP_IPDA", Nullable = false)]
        public DateTime StandWorkDate { get; set; }
        [MappingColumn("LOIP_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
