﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("")]
    public class CountGroupTablesReservationRecord : BaseRecord
    {
        [MappingColumn("AGG_TYPE", Nullable = false)]
        public int TypeAgreggation { get; set; }

        [MappingColumn("AGG_DESC", Nullable = false)]
        public string TypeAgreggationDesc { get; set; }

        /// <summary>
        /// enum ReservationState : long { Reserved = 601, CheckIn = 602, NoShow = 604, Cancelled = 605, CheckOut = 606 }
        /// enum ReservationConfirmState : long { WaitingList = 1022, Confirmed = 1023, Attempt = 1050 };
        /// </summary>
        [MappingColumn("ENUM_VAL", Nullable = false)]
        public long EnumVal { get; set; }

        [MappingColumn("ENUM_DESC", Nullable = false)]
        public string EnumDesc { get; set; }

        [MappingColumn("CANT_RESE", Nullable = true)]
        public long CountTRese { get; set; }
    }
}
