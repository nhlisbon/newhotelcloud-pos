﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("CAJA_PK")]
    public class CajaRecord:BaseRecord
    {
        [MappingColumn("CAJA_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("CAJA_VECA", Nullable = false)]
        public bool Tickets { get; set; }
        [MappingColumn("IPOS_EMPC", Nullable = false)]
        public bool Report { get; set; }
    }
}
