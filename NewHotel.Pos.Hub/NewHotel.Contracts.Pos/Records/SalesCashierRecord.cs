﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ARVE_PK")]
    public class SalesCashierRecord : BaseRecord
    {
        [MappingColumn("GRUP_DESC", Nullable = false)]
        public string GroupDescription { get; set; }
        [MappingColumn("ARTG_DESC", Nullable = false)]
        public string ProductDescription { get; set; }
        [MappingColumn("QTD", Nullable = false)]
        public decimal Quantity { get; set; }
        [MappingColumn("IMPORTE", Nullable = false)]
        public decimal Importe { get; set; }
        [MappingColumn("PAXS", Nullable = false)]
        public short Paxs { get; set; }
    }
}