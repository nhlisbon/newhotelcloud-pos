﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
     [MappingQuery("SEIM_PK")]
    public class AllTaxSequenceRecord : BaseRecord
    {
         [MappingColumn("LITE_PK", Nullable = false)]
         public Guid Lite { get; set; }
         [MappingColumn("SEIM_DESC", Nullable = false)]
         public string SeimDesc { get; set; }
         [MappingColumn("SEIM_LMOD", Nullable = false)]
         public DateTime Lmod { get; set; }
         [MappingColumn("SEIM_ABRE", Nullable = false)]
         public string SeimAbre { get; set; }
    }
}
