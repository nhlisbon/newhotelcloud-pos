﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("MESA_PK")]
    public class MesaRecord:BaseRecord
    {
        [MappingColumn("MESA_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("MESA_PAXS", Nullable = false)]
        public short Paxs { get; set; }
        [MappingColumn("TYPE_DESC", Nullable = true)]
        public string TypeDesc { get; set; }
        [MappingColumn("MESA_TYPE", Nullable = false)]
        public int Type { get; set; }
        [MappingColumn("SALO_DESC", Nullable = true)]
        public string Saloon { get; set; }
        [MappingColumn("SALO_PK", Nullable = true)]
        public Guid SaloonId { get; set; }
        [MappingColumn("MESA_ROW", Nullable = true)]
        public short? Row { get; set; }
        [MappingColumn("MESA_COLU", Nullable = true)]
        public short? Column { get; set; }
        [MappingColumn("MESA_WIDT", Nullable = true)]
        public short? Width { get; set; }
        [MappingColumn("MESA_HEIG", Nullable = true)]
        public short? Height { get; set; }
        [MappingColumn("TAGR_PK", Nullable = true)]
        public Guid? TablesGroup { get; set; }

        #region Dummy
        public bool IsAvailable { get; set; }
        #endregion
    }

    [MappingQuery("MESA_PK")]
    public class LiteTableRecord : BaseRecord
    {
        [MappingColumn("MESA_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("MESA_PAXS", Nullable = false)]
        public short Paxs { get; set; }
        [MappingColumn("SALO_PK", Nullable = true)]
        public Guid SaloonId { get; set; }
        [MappingColumn("mesa_row", Nullable = true)]
        public int? Row { get; set; }
        [MappingColumn("mesa_colu", Nullable = true)]
        public int? Column { get; set; }

        #region Dummy
        public bool IsAvailable { get; set; }
      

        #endregion
    }
}
