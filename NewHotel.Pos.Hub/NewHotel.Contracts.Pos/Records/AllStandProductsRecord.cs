﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PVAR_PK")]
    public class AllStandProductsRecord : SyncBaseRecord
    {
        [MappingColumn("ARTG_PK", Nullable = false)]
        public Guid Product { get; set; }
        [MappingColumn("IPOS_PK", Nullable = false)]
        public Guid Stand { get; set; }
        [MappingColumn("SERV_PK", Nullable = true)]
        public Guid? Service { get; set; }
        [MappingColumn("FAVO_ORDE", Nullable = true)]
        public long? FavoritesOrder { get; set; }
        [MappingColumn("PVAR_LMOD", Nullable = true)]
        public override DateTime LastModified { get; set; }
    }
}