﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("VEND_PK")]
    public class TicketRealRecord : BaseRecord
    {
        [MappingColumn("IPOS_DESC", Nullable = true)]
        public string Stand { get; set; }
        [MappingColumn("CAJA_DESC", Nullable = true)]
        public string Caja { get; set; }
        [MappingColumn("IPOS_PK", Nullable = true)]
        public Guid? StandId { get; set; }
        [MappingColumn("CAJA_PK", Nullable = true)]
        public Guid? CajaId { get; set; }
        [MappingColumn("CAJA1_DESC", Nullable = true)]
        public string CajaPave { get; set; }
        [MappingColumn("VEND_SERI", Nullable = true)]
        public string Serie { get; set; }
        [MappingColumn("VEND_CODI", Nullable = true)]
        public string SerieNumber { get; set; }
        [MappingColumn("VEND_DATI", Nullable = true)]
        public DateTime? ApertureDate { get; set; }
        [MappingColumn("VEND_DATF", Nullable = true)]
        public DateTime? CloseDate { get; set; }
        [MappingColumn("VEND_HORI", Nullable = true)]
        public DateTime? ApertureTime { get; set; }
        [MappingColumn("VEND_HORF", Nullable = true)]
        public DateTime? CloseTime { get; set; }
        [MappingColumn("VEND_NPAX", Nullable = true)]
        public short? Paxs { get; set; }
        [MappingColumn("TURN_CODI", Nullable = true)]
        public long? Turn { get; set; }
        [MappingColumn("VEND_DESC", Nullable = true)]
        public string Discount { get; set; }
        [MappingColumn("VEND_RECA", Nullable = true)]
        public string Recharge { get; set; }
        [MappingColumn("VEND_OBSE", Nullable = true)]
        public string Observations { get; set; }
        [MappingColumn("MTCO_DESC", Nullable = true)]
        public string AnulMtco { get; set; }
        [MappingColumn("VEND_VALO", Nullable = true)]
        public decimal? Valor { get; set; }
        [MappingColumn("UNMO_PK", Nullable = false)]
        public string Currency { get; set; }
        [MappingColumn("ANUL_DAAN", Nullable = true)]
        public DateTime? AnulDate { get; set; }
        [MappingColumn("ANUL_HORE", Nullable = true)]
        public DateTime? AnulTime { get; set; }
        [MappingColumn("VEND_ANUL", Nullable = true)]
        public bool IsAnul { get; set; }
        [MappingColumn("UTIL_DESC", Nullable = true)]
        public string Util { get; set; }
        [MappingColumn("DOCU_FACT", Nullable = true)]
        public string Factura { get; set; }
        [MappingColumn("VEND_FACT", Nullable = true)]
        public Guid? FacturaId { get; set; }
        [MappingColumn("DOCU_NCRE", Nullable = true)]
        public string CreditNote { get; set; }
        [MappingColumn("PAYMENT", Nullable = true)]
        public string Payments { get; set; }
        [MappingColumn("PAVE_ROOM", Nullable = true)]
        public string Room { get; set; }
        [MappingColumn("VEND_NUMBER", Nullable = true)]
        public string TicketNumber { get; set; }
        [MappingColumn("VEND_PROC", Nullable = true)]
        public bool IsProcessed { get; set; }
        [MappingColumn("VEND_NOTA", Nullable = true)]
        public string FiscalNoteMessage { get; set; }
        [MappingColumn("CCCO_PK", Nullable = true)]
        public Guid? CurrentAccountId { get; set; }
        [MappingColumn("VEND_CODC", Nullable = true)]
        public string CouponCode { get; set; }
        [MappingColumn("VEND_CCCF", Nullable = true)]
        public string CupomFiscalCode { get; set; }
        [MappingColumn("VEND_CODD", Nullable = true)]
        public string DAVCode { get; set; }
        [MappingColumn("VEND_CODG", Nullable = true)]
        public string GerencialCode { get; set; }
        [MappingColumn("VEND_NAME", Nullable = true)]
        public string ClientName { get; set; }
        [MappingColumn("VEND_FISC", Nullable = true)]
        public string FiscalNumber { get; set; }
        [MappingColumn("VEND_ADDR", Nullable = true)]
        public string Address { get; set; }
        [MappingColumn("VEND_DAFI", Nullable = true)]
        public DateTime? FiscalDateTime { get; set; }
        [MappingColumn("VEND_PRNS", Nullable = true)]
        public string FiscalPrinterSerial { get; set; }
        [MappingColumn("VEND_PRFR", Nullable = true)]
        public string FiscalPrinterFirmware { get; set; }
        [MappingColumn("VEND_PRNC", Nullable = true)]
        public string FiscalPrinterCashier { get; set; }
        [MappingColumn("VEND_PRCO", Nullable = true)]
        public string FiscalPrinterCode { get; set; }
        [MappingColumn("VEND_PRTP", Nullable = true)]
        public string FiscalPrinterType { get; set; }
        [MappingColumn("VEND_PRMA", Nullable = true)]
        public string FiscalPrinterBrand { get; set; }
        [MappingColumn("VEND_PRMO", Nullable = true)]
        public string FiscalPrinterModel { get; set; }
        [MappingColumn("VEFI_PK", Nullable = true)]
        public Guid? FiscalDocumentId { get; set; }
        [MappingColumn("HOTE_FISC", Nullable = true)]
        public string InstallationFiscalNumber { get; set; }

        public string Description
        {
            get
            {
                var description = Stand;
                if (!string.IsNullOrEmpty(Caja))
                    description += "/" + Caja.Trim();

                return description;
            }
        }

        public string SerieComplete
        {
            get { return TicketNumber; }
        }

        public string OpenDateShort 
        { 
            get 
            { 
                if (ApertureDate.HasValue) 
                    return ApertureDate.Value.ToShortDateString();

                return string.Empty;
            } 
        }

        public string OpenTimeShort 
        { 
            get 
            { 
                if (ApertureTime.HasValue) 
                    return ApertureTime.Value.ToShortTimeString();

                return string.Empty;
            } 
        }

        public string CloseDateShort 
        { 
            get 
            { 
                if (CloseDate.HasValue) 
                    return CloseDate.Value.ToShortDateString();

                return string.Empty;
            } 
        }

        public string CloseTimeShort 
        { 
            get 
            { 
                if (CloseTime.HasValue) 
                    return CloseTime.Value.ToShortTimeString();

                return string.Empty;
            } 
        }

        public string AnulDateShort 
        { 
            get 
            { 
                if (AnulDate.HasValue) 
                    return AnulDate.Value.ToShortTimeString();

                return string.Empty;
            } 
        }

        public string AnulTimeShort 
        { 
            get 
            { 
                if (AnulTime.HasValue) 
                    return AnulTime.Value.ToShortTimeString();

                return string.Empty; 
            } 
        }

        public string OpenDateTime
        { 
            get 
            {
                if (ApertureTime.HasValue)
                    return ApertureTime.Value.ToShortDateTimeString();

                return string.Empty;
            } 
        }

        public string CloseDateTime 
        { 
            get 
            {
                if (CloseTime.HasValue)
                    return CloseTime.Value.ToShortDateTimeString();

                return string.Empty;
            } 
        }

        public string WorkDate
        {
            get
            {
                if (CloseDate.HasValue)
                    return CloseDate.Value.ToYearMonthDayString();

                if (ApertureDate.HasValue)
                    return ApertureDate.Value.ToYearMonthDayString();

                return string.Empty;
            }
        }

        public bool Fiscalized
        {
            get { return FiscalDocumentId.HasValue; }
        }

        public long? Operations
        {
            get
            {
                var oper = TicketOper.View;

                if (!IsAnul)
                    oper = oper | TicketOper.Cancel;

                if (!FacturaId.HasValue)
                    oper = oper | TicketOper.Bill | TicketOper.PrintInvoice;

                return (long)oper;
            }
        }
    }
}