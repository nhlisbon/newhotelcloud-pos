﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("MOPE_PK")]
    public class PensionByRoomRecord : BaseRecord
    {
        [MappingColumn("LIRE_DAEN", Nullable = true)]
        public DateTime  InitialDate{ get; set; }
        [MappingColumn("LIOC_DASA", Nullable = true)]
        public DateTime FinalDate { get; set; }
        [MappingColumn("MOPE_ABRE", Nullable = true)]
        public string MopeAbre { get; set; }
        [MappingColumn("MOPE_DESC", Nullable = true)]
        public string MopeDesc { get; set; }
        [MappingColumn("CCCO_PK", Nullable = true)]
        public Guid? AccountId { get; set; }      
    }
}