﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TIPR_PK")]
    public class PropTypeRecord:BaseRecord
    {
        [MappingColumn("TIPR_DESC", Nullable = false)]
        public string Description { get; set; }
    }
}
