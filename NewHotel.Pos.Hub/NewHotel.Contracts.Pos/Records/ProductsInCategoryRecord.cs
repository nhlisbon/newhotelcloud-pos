﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("BOTV_PK")]
    public class ProductsInCategoryRecord : BaseRecord
    {
        [MappingColumn("BARTG_PK", Nullable = false)]
        public Guid Product { get; set; }
    }
}
