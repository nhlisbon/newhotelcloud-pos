﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
     [MappingQuery("TIVA_PK")]
    public class AllTaxesRecord : BaseRecord
    {
         [MappingColumn("LITE_PK", Nullable = false)]
         public Guid Lite { get; set; }
         [MappingColumn("TIVA_DESC", Nullable = false)]
         public string TivaDesc { get; set; }
         [MappingColumn("SEIM_PK", Nullable = false)]
         public Guid Seim { get; set; }
         [MappingColumn("REGI_PK", Nullable = false)]
         public Guid Regi { get; set; }
         [MappingColumn("TIVA_INAC", Nullable = false)]
         public bool Inac { get; set; }
         [MappingColumn("TIVA_PERC", Nullable = false)]
         public decimal Perc { get; set; }
    }
}
