﻿using NewHotel.Pos.Integration.Mxm;
using NewHotel.Pos.Integration.Mxm.Models;
using NewHotel.Pos.Integration.Mxm.Models.Estoque;
using NewHotel.Pos.Integration.Mxm.Models.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Integration.Tests
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            await TestMxm();
        }

        private static async Task TestMxm() 
        {
            var username = "INTEGRAHOTEL";
            var password = "PwisCixExwD3-uEEecSrZy";
            var environment = "SESCMSHOM";
            var endpoints = new Endpoints(username, password, environment, "https://sescmshom.mxmwebmanager.com.br/webmanager/");
            var processNumber = 150048;

            // Test the creation of a process
            try
            {
                Console.WriteLine("Creating process...");
                var process = new MxmDataSaveProcessStock
                {
                   InterfacedoEstoque = new InterfacedoEstoque[]
                   {
                       new InterfacedoEstoque
                       {
                            SequenciadoRegistro = 1,
                            CodigodaEmpresa = "MS",
                            CodigodoEstoque = "17",
                            CodigodoAlmoxarifado = "17",
                            NumerodoDocumento = "NH000105567/002",
                            TipodeMovimentacao = "S",
                            CodigoTipodeNota = "03",
                            IndicadordeClienteouFornecedor = "C",
                            CodigodoClienteouFornecedor = "08448657020",
                            TipodeOperacao = "016",
                            ValorTotaldaNota = "25,43",
                            DatadeEmissaodaNota = "21092024",
                            DatadeEntradadaNota = "23092024",
                            DatadeDigitacao = "21092024",
                            DatadeCompetencia = "21092024",
                            InterfaceItemdoEstoque = new InterfaceItemdoEstoque[]
                            {
                                new InterfaceItemdoEstoque
                                {
                                    SequenciadoItemnaNota = 1053,
                                    CodigodoItem = "3050502500",
                                    QuantidadedoItem = "1,00",
                                    CodigodaUnidadeAdquirida = "LATA",
                                    QuantidadedoAdquirida = "1,00",
                                    ValorBruto = "6,90",
                                    Destinacao = "33111010502",
                                    CentrodeCusto = "002000090001",
                                    ValordoItem = "6,90",
                                    TipodeOperacao = "016"
                                }
                            }
                       }
                   }
                };
                var result = await endpoints.CreateStockProcess(process);
                Console.WriteLine($"The result of the process is: \n {result}");
                if(result.Success)
                {
                    Console.WriteLine($"The process was created with the number: {result.Data}");
                    processNumber = result.Data;
                }
                else
                    Console.WriteLine($"The process was not created. Error: {result.Messages}");
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            // Test the check of a product's availability
            try
            {
                Console.WriteLine("Checking product availability...");
                var product = new MxmDataCheckProductAvailabilityRequest
                {
                    Empresa = "MS",
                    Estoque = "17",
                    Produto = "3050502500",
                    Almoxarifado = "17",
                    DesconsiderarSaldoEmpenhado = "S",
                    ExibeProdutoSaldoZerado = "S",
                    PeriodoDe = "21/09/2024",
                    PeriodoAte = "21/09/2024",
                    SaldoEstoqueLivre = "S",

                };
                var result = await endpoints.CheckProductAvailability(product);
                if(result.Success)
                    Console.WriteLine($"The product is available: \n {result.Data}");
                else
                    Console.WriteLine($"The product is not available. Error: {result.Messages}");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            // Test the check of a process's status
            try
            {
                Console.WriteLine("Checking process...");
                var result = await endpoints.CheckStockProcess(processNumber);
                Console.WriteLine($"The result of the process is: \n Success: {result.Success} \n {result.Data}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
