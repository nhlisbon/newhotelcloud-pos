﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media.Animation;
using Microsoft.Win32;
using NewHotel.Pos.Communication.Cashier;
using NewHotel.Pos.Core;
using NewHotel.Pos.Notifications;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Localization;
using NewHotel.WPF.App.Pos.Controls;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.App.Pos.Skins;
using NewHotel.WPF.Common;
using NewHotel.WPF.Common.FiscalPrinters;
using NewHotel.WPF.Model;
using NewHotel.WPF.Model.Interface;
using NewHotel.WPF.Model.Model;
using NewHotel.Contracts;
using static DevExpress.XtraPrinting.Native.ExportOptionsPropertiesNames;
using System.Windows.Threading;

namespace NewHotel.WPF.App.Pos
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Variables

        private readonly List<LoginDataStore> _contents = [];
        private Action _okDialogAction, _cancelDialogAction;
         
        #region Activity Tracker Variables
        private DispatcherTimer? idleTimer;
        private float inactivityTimeoutMinutes = 1; // placeholder until 
        #endregion

        #endregion
        #region Constructors

        public MainWindow()
        {
            Application.Current.Resources.MergedDictionaries.Add(new default_skin());
            ServicePointManager.ServerCertificateValidationCallback = (s, cer, ch, e) => true;

            Logger.TraceInformation($"{App.NewPosAppDescription} Start");

            InitializeComponent();

            Instance = this;

            // Animation fps
            Timeline.DesiredFrameRateProperty.OverrideMetadata(typeof(Timeline), new FrameworkPropertyMetadata { DefaultValue = 60 });

            InitializeLastUsedLanguage();

            CashierBaseBusinessProxy.VersionErrorEvent += (s, ev) =>
            {
                string message = ((Exception)s).Message;
                WindowHelper.ShowOkCancelErrorWindow("Unable to continue. " + message + " Want to update now?", args =>
                {
                    if (args.GetValueOrDefault()) UpdateCashier();
                    else Close();
                });
            };

            CashierBaseBusinessProxy.DatabaseVersionErrorEvent += (s, ev) =>
            {
                WindowHelper.ShowOkCancelErrorWindow($"Unable to continue. {ev.Message}", _ => Close());
            };
        }
        #endregion
        #region Properties

        public static ILogger Logger => CwFactory.Resolve<ILogger>();
        public static MainWindow Instance { get; set; }
        public static FiscalPrinter FiscalPrinter { get; set; }

        public IWindow WindowHelper => CwFactory.Resolve<IWindow>();
        public ObservableCollection<LanguageView> Languages => Hotel.Laguages;
        public BarCaffeteriaRestaurantPage? barCaffeteriaRestaurantPage { get; set; }

        public string LoadingMessage
        {
            get => (string)GetValue(LoadingMessageProperty);
            set => SetValue(LoadingMessageProperty, value);
        }

        public static readonly DependencyProperty LoadingMessageProperty =
            DependencyProperty.Register(nameof(LoadingMessage), typeof(string), typeof(MainWindow));

        #endregion
        #region Methods

        #region Ativity Tracker Methods
        
        public void SetLogoutTimer()
        {
            var settings = Hotel.Instance;
            if (!settings.UsingAutoLogOutTimer) return;
            inactivityTimeoutMinutes = Hotel.Instance.AutoLogOutTimerValue;
            // Initialize and start the idle timer
            idleTimer = new DispatcherTimer { Interval = TimeSpan.FromMinutes(inactivityTimeoutMinutes) };
            idleTimer.Tick += OnIdleTimerTick;
            StartIdleTimer();

            // Attach event handlers for user interaction
            PreviewMouseDown += OnUserInteraction;
            PreviewKeyDown += OnUserInteraction;
        }

        private void StartIdleTimer()
        {
            idleTimer?.Start();
        }

        private void ResetIdleTimer()
        {
            idleTimer?.Stop();
            idleTimer?.Start();
        }

        private void OnUserInteraction(object sender, InputEventArgs e)
        {
            if (idleTimer != null) ResetIdleTimer();
        }

        private void OnIdleTimerTick(object sender, EventArgs e)
        {  
            idleTimer?.Stop();
            idleTimer = null;
            LogoutAndRedirectToLogin();
        }

        private void LogoutAndRedirectToLogin()
        {
            var location = ReadLocation();
            ChangeUserStandRequested(LoginControl.ResetMode.Total);
        }
        #endregion


        public void RestartApp()
        {
            try
            {
                var location = ReadLocation();
                ErrorsWindow.ShowErrorsWindow("ApplicationWillClose".Translate());
                RestartApp(location);
            }
            catch (Exception ex)
            {
                ErrorsWindow.ShowErrorWindows(ex);
            }
        }

        public LoginDataStore? GetStandCashierDataStore(Guid standId, Guid cashierId)
        {
            return _contents.FirstOrDefault(x => x.StandId == standId && x.CashierId == cashierId);
        }

        private void login_Completed(LoginControl sender, LoginData loginData)
        {
            sender.Visibility = Visibility.Collapsed;
            bigLoadingMessageTB.Visibility = Visibility.Visible;
            userControl.IsEnabled = false;

            if (barCaffeteriaRestaurantPage is null)
            {
                barCaffeteriaRestaurantPage = new BarCaffeteriaRestaurantPage();
                barCaffeteriaRestaurantPage.ChangeUserStandRequested += ChangeUserStandRequested;
                contentGrid.Children.Add(barCaffeteriaRestaurantPage);
            }

            barCaffeteriaRestaurantPage.State = BarCaffeteriaRestaurantPage.States.Tables;

            // Returns a different type of data store depending on the type of login
            var newStore = loginData.GetDataStore();
            var stored = GetStandCashierDataStore(newStore.StandId, newStore.CashierId);

            var standModel = Hotel.Instance.Stands.FirstOrDefault(x => loginData.SelectedStand != null && x.Id == loginData.SelectedStand.Id);
            if (standModel != null)
            {
                if (stored is null)
                {
                    stored = newStore;
                    stored.Context = Hotel.Instance.CurrentStandCashierContext;
                    _contents.Add(stored);
                }
                else
                {
                    Hotel.Instance.CurrentStandCashierContext = stored.Context;
                    Hotel.Instance.CurrentStandCashierContext.SetSeries();
                }

                barCaffeteriaRestaurantPage.SetContext(Hotel.Instance.CurrentStandCashierContext);
                barCaffeteriaRestaurantPage.SetAsInstance();
                if(Hotel.Instance.Tickets != null && loginData is { SelectedStand: not null, SelectedCashier: not null }) Hotel.Instance.UpdateTicketsEditableProperty(loginData.SelectedStand.Id, loginData.SelectedCashier.Id);

                userControl.IsEnabled = true;
                contentGrid.Visibility = Visibility.Visible;
            }
            else Hotel.Instance.RefreshStandTransfersNotifications(Hotel.Instance.CurrentStandCashierContext.Stand, loginData.SelectedCashier.Id);
        }

        public void ChangeUserStandRequested(LoginControl.ResetMode mode)
        {
            IsEnabled = false;
            contentGrid.Visibility = Visibility.Collapsed;
            loginControl.Reset(mode);
            loginControl.Visibility = Visibility.Visible;

            IsEnabled = true;
        }

        public void CloseApplication()
        {
            OpenDialog(new TextBlock { Text = LocalizationMgr.Translation("DoYouWishToExit", LocalizedSuffix.Message), FontSize = 16 }, null,
                () =>
                {
                    Close();
                }, null);
        }

        private void ActionButton_Click(object sender, RoutedEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.LeftAlt))
            {
                Process.Start("explorer", Path.GetDirectoryName(Assembly.GetEntryAssembly().Location));
            }
            else if (Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.LeftShift))
            {
                Process.Start("NewHotel.Deploy.WPF.Updater.exe", "559E7373DC6A8B4B84AD084926F5E098");
                Close();
            }
            else
            {
                CloseApplication();
            }
        }

        public void OpenDialog(object content, object dataContext, Action okAction, Action cancelAction)
        {
            _okDialogAction = okAction;
            _cancelDialogAction = cancelAction;
            dialogContainer.Content = content;
            if (dataContext != null) ((FrameworkElement)content).DataContext = dataContext;

            dialogPanel.Visibility = Visibility.Visible;
        }

        public void CloseDialog()
        {
            dialogPanel.Visibility = Visibility.Collapsed;
        }

        public void CloseDialogInfo()
        {
            dialogPanelInfo.Visibility = Visibility.Collapsed;
        }

        private void OkDialog_Click(object sender, RoutedEventArgs e)
        {
            _okDialogAction?.Invoke();
            CloseDialog();
        }

        private void InitializeLastUsedLanguage()
        {
            int index;
            if (!int.TryParse(ConfigurationManager.AppSettings.Get("IndexSelectedLanguage"), out index))
                index = 0;
            else if (index < 0 || index >= Languages.Count)
                index = 0;

            ((INotifyPropertyChanged)Languages).PropertyChanged += OnPropertyChanged;
            Hotel.SelectedLanguage = Languages[index];

            languagesComboBox.UpdateLayout();
        }

        private async void UpdateCashier()
        {
            var updater = new UpdaterCashier();
            updater.Error += e =>
            {
                Logger.TraceError(e.ToString());
                WindowHelper.ShowError("OopsUnexpectedError".TranslateMsg(), "UpdateCannotContinue".Translate());
            };

            updater.StatusChange += status =>
            {
                UIThread.Invoke(() =>
                {
                    switch (status)
                    {
                        case UpdaterCashier.Status.ResetDirectory:
                            LoadingMessage = "CleaningOldUpdateFiles".Translate();
                            break;
                        case UpdaterCashier.Status.DownloadPkgFile:
                            LoadingMessage = "DownloadingFilesFromServer".Translate();
                            break;
                        case UpdaterCashier.Status.ExtractPkgFile:
                            LoadingMessage = "ExtractingFiles".Translate();
                            break;
                        case UpdaterCashier.Status.StartUpdaterProcess:
                            LoadingMessage = "RunningUpdaterProcess".Translate();
                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(status), status, null);
                    }
                });
            };

            try
            {
                IsEnabled = false;
                await updater.Update();
            }
            finally
            {
                LoadingMessage = string.Empty;
                IsEnabled = true;
            }
        }

        private string ReadLocation()
        {
            RegistryKey? root = null;
            var installationPath = Directory.GetCurrentDirectory();

            try
            {
                root = Registry.LocalMachine.OpenSubKey(@"Software\NewHotel\Pos", RegistryKeyPermissionCheck.ReadSubTree);
                if (root != null && int.TryParse(root.GetValue("Desarrollo")?.ToString(), out _))
                    installationPath = root.GetValue("InstallationPath").ToString();
            }
            finally
            {
                root?.Close();
            }
            return installationPath;
        }

        private void CancelDialog_Click(object sender, RoutedEventArgs e)
        {
            _cancelDialogAction?.Invoke();
            CloseDialog();
        }

        private void okInfoButton_Click(object sender, RoutedEventArgs e)
        {
            CloseDialogInfo();
        }

        public static bool ActionsBlocked { get; set; }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ActionsBlocked = true;
            (new Thread(() =>
            {
                try
                {
                    //Local database
                    UIThread.Invoke(() => { LoadingMessage = "Checking local data access..."; });

                    UIThread.Invoke(() =>
                    {
                        ActionsBlocked = false;
                        LoadingMessage = "";
                    });
                }
                catch
                { }
            })).Start();
        }

        private void RestartApp(string locationPos)
        {
            string batPath = Path.Combine(locationPos, "restart.bat");
            var sWriter = File.CreateText(batPath);
            sWriter.WriteLine("sleep 10");
            sWriter.WriteLine("start \"\" \"{0}\"", Path.Combine(locationPos, "NewHotel.Pos.exe"));
            sWriter.Close();
            Process.Start(batPath);
            Application.Current.MainWindow.Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Process.GetCurrentProcess().Kill();
        }

        private void userControl_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                NewHotelContext.Current.Version = "";
                foreach (var item in _contents)
                {
                    var context = item.Context;
                    dialogPanel.IsEnabled = false;
                }
                dialogPanel.IsEnabled = true;
                CloseDialog();
            }
            catch
            { }
        }

        internal void Unblock()
        {
            UIThread.Invoke(() =>
            {
                contentGrid.Visibility = Visibility.Visible;
                loginControl.Visibility = Visibility.Collapsed;
                closeButton.Visibility = Visibility.Visible;
            });
        }

        internal void Block()
        {
            if (contentGrid.Visibility == Visibility.Visible)
            {
                UIThread.Invoke(() =>
                {
                    contentGrid.Visibility = Visibility.Collapsed;
                    loginControl.Reset(LoginControl.ResetMode.Block);
                    loginControl.Visibility = Visibility.Visible;
                    bigLoadingMessageTB.Visibility = Visibility.Collapsed;
                    closeButton.Visibility = Visibility.Collapsed;
                });
            }
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedLanguage")
            {
                var culture = (CultureInfo)new CultureInfo(Hotel.SelectedLanguage.Code).Clone();
                culture.NumberFormat.CurrencySymbol = string.Empty;
                Language = XmlLanguage.GetLanguage(culture.Name);
                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentUICulture = culture;
                Hotel.Culture = culture;

                var app = App.Current as App;
                if (app != null)
                    app.RefreshTextBlocksTranslation();

                var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var value = Languages.IndexOf(Hotel.SelectedLanguage).ToString();

                try
                { 
                    config.AppSettings.Settings["IndexSelectedLanguage"].Value = value;
                }
                catch
                { 
                    config.AppSettings.Settings.Add("IndexSelectedLanguage", value);
                }
                
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
            }
        }

        #region Kitchen Service Methods

        #region ReceiveMessages

        public Cashier GetOpenCashier(Guid cashierId)
        {
            foreach (var item in _contents)
            {
                if (item.CashierId == cashierId)
                    return item.Context.Cashier;
            }

            return null;
        }

        #endregion

        #region SendMessages

        private void SendToPrinter(IEnumerable<Kitchen.Kitchen> kitchens)
        {
            Trace.TraceInformation($"{kitchens.Count()} kitchen print pending");
            foreach (var kitchen in kitchens)
            {
                Trace.TraceInformation($"Kitchen to print Area:{kitchen.ToString()}");

                var result = kitchen.SendToPrinter();
                if (!result.IsEmpty)
                {
                    WindowHelper.ShowError(result);
                    break;
                }
            }
        }

        private void SendToDisplay(IEnumerable<Kitchen.Kitchen> kitchens)
        {
            foreach (var kitchen in kitchens)
            {
                foreach (var ticket in kitchen.Tickets)
                {
                    if (ticket.Area != null && ticket.Area.UseDisplay)
                    {
                        var isAreaListening = SocketIONotifications.IsAreaListening(ticket.Area.Id);

                        // Notify Order
                        if (ticket.NewOrders.Any())
                        {
                            ticket.FailedOrders.Clear();
                            if (!isAreaListening)
                                ticket.FailedOrders.AddRange(ticket.NewOrders);
                            else
                            {
                                var items = new List<SendOrder>();
                                foreach (var order in ticket.NewOrders)
                                {
                                    var item = new SendOrder();
                                    item.Id = order.Id;
                                    item.Description = order.Order.ProductDescription;
                                    item.Quantity = order.Order.Quantity;
                                    if (order.Order.Separator.HasValue && order.Order.SeparatorModel != null)
                                        item.Separator = order.Order.SeparatorModel.Description;
                                    if (order.Order.PaxNumber.HasValue)
                                        item.PaxNumber = order.Order.PaxNumber.Value;

                                    var preparations = new StringBuilder();
                                    foreach (var preparation in order.Order.OrderPreparations)
                                    {
                                        if (!string.IsNullOrEmpty(preparation.Description))
                                            preparations.AppendLine(preparation.Description);
                                    }

                                    item.Preparations = preparations.ToString();
                                    if (string.IsNullOrEmpty(order.Order.Notes))
                                        item.Observations = order.Order.Notes;

                                    items.Add(item);
                                }

                                var contract = new NotificationContract<SendDispatchAreaOrders>();

                                var instance = Hotel.Instance;
                                var currentStandCashierContext = instance.CurrentStandCashierContext;
                                contract.HotelId = instance.Id;
                                contract.CashierId = currentStandCashierContext.Cashier.Id;
                                contract.StandId = currentStandCashierContext.Stand.Id;

                                var msg = new SendDispatchAreaOrders()
                                {
                                    LanguageId = instance.LanguageId,
                                    TicketId = ticket.Id,
                                    AreaId = ticket.Area.Id,
                                    Area = ticket.Area.Description,
                                    Saloon = ticket.Ticket.SaloonDescription,
                                    Table = ticket.Ticket.TableDescription,
                                    Orders = items.ToArray()
                                };

                                contract.DataContext = msg;

                                SocketIONotifications.Publish("SendDispatchAreaOrders", contract);
                            }
                        }

                        // Notify Void
                        if (ticket.VoidOrders.Any())
                        {
                            ticket.FailedOrders.Clear();
                            if (!isAreaListening)
                                ticket.FailedOrders.AddRange(ticket.NewOrders);
                            else
                            {
                                var items = new List<VoidOrder>();
                                foreach (var order in ticket.VoidOrders)
                                {
                                    var item = new VoidOrder();
                                    item.Id = order.Id;
                                    item.Description = order.Order.ProductDescription;
                                    item.Quantity = order.Order.Quantity;

                                    items.Add(item);
                                }

                                var contract = new NotificationContract<VoidDispatchAreaOrders>();

                                var instance = Hotel.Instance;
                                var currentStandCashierContext = instance.CurrentStandCashierContext;
                                contract.HotelId = instance.Id;
                                contract.CashierId = currentStandCashierContext.Cashier.Id;
                                contract.StandId = currentStandCashierContext.Stand.Id;

                                var msg = new VoidDispatchAreaOrders()
                                {
                                    LanguageId = instance.LanguageId,
                                    TicketId = ticket.Id,
                                    AreaId = ticket.Area.Id,
                                    Area = ticket.Area.Description,
                                    Saloon = ticket.Ticket.SaloonDescription,
                                    Table = ticket.Ticket.TableDescription,
                                    Orders = items.ToArray()
                                };

                                contract.DataContext = msg;

                                SocketIONotifications.Publish("VoidDispatchAreaOrders", contract);
                            }
                        }

                        //Notify Away

                        if (ticket.AwayOrders.Any())
                        {
                            ticket.FailedOrders.Clear();
                            if (!isAreaListening)
                                ticket.FailedOrders.AddRange(ticket.AwayOrders);
                            else
                            {
                                var items = new List<SendOrder>();
                                foreach (var order in ticket.AwayOrders)
                                {
                                    var item = new SendOrder();
                                    item.Id = order.Id;
                                    item.Description = order.Order.ProductDescription;
                                    item.Quantity = order.Order.Quantity;
                                    if (order.Order.Separator.HasValue && order.Order.SeparatorModel != null)
                                        item.Separator = order.Order.SeparatorModel.Description;
                                    if (order.Order.PaxNumber.HasValue)
                                        item.PaxNumber = order.Order.PaxNumber.Value;

                                    var preparations = new StringBuilder();
                                    foreach (var preparation in order.Order.OrderPreparations)
                                    {
                                        if (!string.IsNullOrEmpty(preparation.Description))
                                            preparations.AppendLine(preparation.Description);
                                    }

                                    item.Preparations = preparations.ToString();
                                    if (string.IsNullOrEmpty(order.Order.Notes))
                                        item.Observations = order.Order.Notes;

                                    items.Add(item);
                                }

                                var contract = new NotificationContract<SendDispatchAreaOrders>();

                                var instance = Hotel.Instance;
                                var currentStandCashierContext = instance.CurrentStandCashierContext;
                                contract.HotelId = instance.Id;
                                contract.CashierId = currentStandCashierContext.Cashier.Id;
                                contract.StandId = currentStandCashierContext.Stand.Id;

                                var msg = new SendDispatchAreaOrders()
                                {
                                    LanguageId = instance.LanguageId,
                                    TicketId = ticket.Id,
                                    AreaId = ticket.Area.Id,
                                    Area = ticket.Area.Description,
                                    Saloon = ticket.Ticket.SaloonDescription,
                                    Table = ticket.Ticket.TableDescription,
                                    Orders = items.ToArray()
                                };

                                contract.DataContext = msg;

                                SocketIONotifications.Publish("SendDispatchAreaOrders", contract);
                            }
                        }                    
                    }
                }
            }
        }

        public IEnumerable<Kitchen.Kitchen> SendNewOrderToArea(Ticket ticket, IList<Order> orders, Stand stand)
        {
            if (string.IsNullOrEmpty(ticket.SeriePrex))
                Trace.TraceInformation($"Sending new orders to kitchen Ticket:{ticket.Serie ?? 0}");
            else
                Trace.TraceInformation($"Sending new orders to kitchen Ticket:{ticket.Serie ?? 0}/{ticket.SeriePrex}");

            var kitchens = new Dictionary<Guid, Kitchen.Kitchen>();
            foreach (var order in orders)
            {
                Trace.TraceInformation($"Order Id:{order.Id} Qty:{order.Quantity} Product:{order.ProductDescription}");
                foreach (var orderArea in order.Areas)
                {
                    var area = stand.Areas.FirstOrDefault(x => x.Id == orderArea.AreaId);
                    if (area != null)
                    {
                        Trace.TraceInformation($"Area Id:{area.Id} Area:{area.Description} Printer:{area.PrinterDescription} PrinterServer:{area.PrinterServer} UsePrinter:{area.UsePrinter}");

                        Kitchen.Kitchen kitchen;
                        if (!kitchens.TryGetValue(area.Id, out kitchen))
                        {
                            kitchen = new Kitchen.Kitchen(area);
                            kitchens.Add(area.Id, kitchen);
                        }

                        try
                        {
                            if (order.IsMenu)
                            {
                                var newOrder = new Order();
                                newOrder.CopyFrom(order);
                                newOrder.AsContract.TableLines.Clear();
                                var orderTablesByArea = order.OrderTables.Where(x => x.IsSelected && x.Areas.Any(a => a.AreaId == orderArea.AreaId)).ToList();
                                newOrder.AsContract.TableLines.AddRange(orderTablesByArea.Select(x => x.TableLine));

                                kitchen.AddOrder(ticket, newOrder);
                            }
                            else
                            {
                                kitchen.AddOrder(ticket, order);
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error dispatching: " + ex.Message);
                            break;
                        }
                    }
                    else
                        Trace.TraceInformation($"Order area not found Id:{order.Id} Qty:{order.Quantity} Product:{order.ProductDescription} Area:{order.AreaId.Value}");
                }
            }

            // procesar el envio de todas las ordenes pendientes
            SendToDisplay(kitchens.Values.Where(x => x.MarchPending && x.DisplayValid));
            SendToPrinter(kitchens.Values.Where(x => x.MarchPending && x.PrinterValid));
            return kitchens.Values;
        }

        public IEnumerable<Kitchen.Kitchen> SendVoidOrderToArea(Ticket ticket, IList<Order> orders, Stand stand)
        {
            if (string.IsNullOrEmpty(ticket.SeriePrex))
                Trace.TraceInformation($"Sending void orders to kitchen Ticket:{ticket.Serie ?? 0}");
            else
                Trace.TraceInformation($"Sending void orders to kitchen Ticket:{ticket.Serie ?? 0}/{ticket.SeriePrex}");

            var kitchens = new Dictionary<Guid, Kitchen.Kitchen>();
            foreach (var order in orders)
            {
                Trace.TraceInformation($"Order Id:{order.Id} Qty:{order.Quantity} Product:{order.ProductDescription}");
                
                var areasId = new List<Guid>();
                if(!order.IsMenu)
                {
                    areasId = order.AreasId.ToList();
                } else
                {
                    var tableOrders = order.OrderTables.Where(x => x.IsSelected);
                    foreach(var tableOrder in tableOrders)
                    {
                        areasId.AddRange(tableOrder.Areas.Select(x => x.AreaId));
                    }
                }

                foreach (var areaId in areasId)
                {
                    var area = stand.Areas.FirstOrDefault(x => x.Id == areaId);
                    if (area != null)
                    {
                        Trace.TraceInformation($"Area Id:{area.Id} Area:{area.Description} Printer:{area.PrinterDescription} PrinterServer:{area.PrinterServer} UsePrinter:{area.UsePrinter}");

                        Kitchen.Kitchen kitchen;
                        if (!kitchens.TryGetValue(area.Id, out kitchen))
                        {
                            kitchen = new Kitchen.Kitchen(area);
                            kitchens.Add(area.Id, kitchen);
                        }

                        try
                        {
                            if (order.IsMenu)
                            {
                                var newOrder = new Order();
                                newOrder.CopyFrom(order);
                                newOrder.AsContract.TableLines.Clear();
                                var orderTablesByArea = order.OrderTables.Where(x => x.IsSelected && x.Areas.Any(a => a.AreaId == areaId)).ToList();
                                newOrder.AsContract.TableLines.AddRange(orderTablesByArea.Select(x => x.TableLine));
                                kitchen.VoidOrder(ticket, newOrder);
                            }
                            else
                            {
                                kitchen.VoidOrder(ticket, order);
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error dispatching: " + ex.Message);
                            break;
                        }
                    }
                    else
                        Trace.TraceInformation($"Area not found Id:{areaId}");
                }
            }

            // procesar el envio de todas las ordenes pendientes
            SendToDisplay(kitchens.Values.Where(x => x.VoidPending && x.DisplayValid));
            SendToPrinter(kitchens.Values.Where(x => x.VoidPending && x.PrinterValid));
            return kitchens.Values;
        }

        public IEnumerable<Kitchen.Kitchen> SendAwayOrderToArea(Ticket ticket, IList<Order> orders, Stand stand)
        {
            if (string.IsNullOrEmpty(ticket.SeriePrex))
                Trace.TraceInformation($"Sending away orders to kitchen Ticket:{ticket.Serie ?? 0}");
            else
                Trace.TraceInformation($"Sending away orders to kitchen Ticket:{ticket.Serie ?? 0}/{ticket.SeriePrex}");

            var kitchens = new Dictionary<Guid, Kitchen.Kitchen>();
            foreach (var order in orders)
            {
                Trace.TraceInformation($"Order Id:{order.Id} Qty:{order.Quantity} Product:{order.ProductDescription}");

                var areasId = new List<Guid>();
                if (!order.IsMenu)
                {
                    areasId = order.Areas.Select(x => x.AreaId).ToList();
                }
                else
                {
                    var tableOrders = order.OrderTables.Where(x => x.IsSelected && x.March);
                    foreach (var tableOrder in tableOrders)
                    {
                        areasId.AddRange(tableOrder.Areas.Select(x => x.AreaId));
                    }
                }

                foreach (var areaId in areasId)
                {
                    var area = stand.Areas.FirstOrDefault(x => x.Id == areaId);
                    if (area != null)
                    {
                        Trace.TraceInformation($"Area Id:{area.Id} Area:{area.Description} Printer:{area.PrinterDescription} PrinterServer:{area.PrinterServer} UsePrinter:{area.UsePrinter}");

                        if (!kitchens.TryGetValue(area.Id, out Kitchen.Kitchen kitchen))
                        {
                            kitchen = new Kitchen.Kitchen(area);
                            kitchens.Add(area.Id, kitchen);
                        }

                        try
                        {
                            if (order.IsMenu)
                            {
                                var newOrder = new Order();
                                newOrder.CopyFrom(order);
                                newOrder.AsContract.TableLines.Clear();
                                var orderTablesByArea = order.OrderTables.Where(x => x.IsSelected && x.Areas.Any(a => a.AreaId == areaId)).ToList();
                                newOrder.AsContract.TableLines.AddRange(orderTablesByArea.Select(x => x.TableLine));
                                kitchen.AwayOrder(ticket, newOrder);
                            }
                            else
                            {
                                kitchen.AwayOrder(ticket, order);
                            }
                            kitchen.AwayOrder(ticket, order);                           
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error dispatching: " + ex.Message);
                            break;
                        }
                    }
                    else
                        Trace.TraceInformation($"Area not found Id:{areaId}");
                }
            }

            // procesar el envio de todas las ordenes pendientes
            SendToDisplay(kitchens.Values.Where(x => x.AwayPending && x.DisplayValid));  
            SendToPrinter(kitchens.Values.Where(x => x.AwayPending && x.PrinterValid));
            return kitchens.Values;
        }

        #endregion

        #endregion

        #endregion
    }
}