﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Markup;
using System.Diagnostics;
using Microsoft.Shell;
using NewHotel.WPF.Core;
using NewHotel.Contracts;
using NewHotel.Pos.Core;
using NewHotel.WPF.App.Pos.Controls;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using NewHotel.WPF.Common.Controls.Busy;
using NewHotel.WPF.Model.Interface;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Communication.Behavior;
using NewHotel.Pos.Communication.Cashier;
using NewHotel.Pos.PrinterDocument.Interfaces;
using NewHotel.Pos.PrinterDocument.Simple;
using ValidationResult = NewHotel.Contracts.ValidationResult;
using NewHotel.Pos.Communication;
using System.Collections.Concurrent;
using DevExpress.Internal.WinApi.Windows.UI.Notifications;
using System.Threading.Tasks;
using NewHotel.Pos.Communication.Api;
using System.Windows.Media;
using System.ServiceModel.Configuration;
using NewHotel.Scheduler;
using System.Text;
using SiTef.Terminal;

namespace NewHotel.WPF.App.Pos
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application, ISingleInstanceApp
	{
		#region Constants

		public const string NewPosAppDescription = "Newhotel Cashier Application";

		#endregion
		#region Methods

		[STAThread]
		public static void Main(string[] agrs)
		{
			var id = DateTime.Now.Ticks.ToString(); //Process.GetCurrentProcess().Handle.ToString();
			if (SingleInstance<App>.InitializeAsFirstInstance(NewPosAppDescription + $"({id})"))
			{
				var application = new App();
				application.agrs = agrs;
				application.DispatcherUnhandledException += (s, e) =>
				{
					Trace.TraceError(e.Exception.ToString());
					e.Handled = true;
				};

				AppDomain.CurrentDomain.UnhandledException += (s, e) =>
				{
					Trace.TraceError(e.ExceptionObject.ToString());
					ShowUnhandledException((Exception)e.ExceptionObject);
				};

				Hotel.Laguages = new LanguageCollection();
				application.InitializeComponent();

				application.Run();

				SingleInstance<App>.Cleanup();
			}
			else
				MessageBox.Show("An instance of NewHotel Cashier POS Application is already running",
					"Invalid operation", MessageBoxButton.OK, MessageBoxImage.Exclamation);
		}

		Timer debugTimer;
		string[] agrs;

		protected override void OnStartup(StartupEventArgs e)
		{
			var currentCulture = new CultureInfo("en-US");
			var ietfLanguageTag = XmlLanguage.GetLanguage(currentCulture.IetfLanguageTag);
			FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), new FrameworkPropertyMetadata(ietfLanguageTag));
			Hotel.Culture = (CultureInfo)currentCulture.Clone();
			Hotel.Culture.NumberFormat.CurrencySymbol = string.Empty;
			Hotel.XmlLanguage = ietfLanguageTag;

			SignalRNotifications.Initialize();
			SocketIONotifications.Initialize();

			InitializeDependencys();

			long allWcf = 0;
			long allApi = 0;

			var stPayload = Encoding.UTF8.GetBytes("{\"configuration\":{\"cnpjInstall\":\"12345678912345\",\"cnpjSoftware\":\"12345678912345\",\"siTefIP\":\"localhost\",\"idLoja\":\"00000000\",\"agentUrl\":\"https://localhost:443\",\"terminalId\":\"SE000325\"},\"action\":{\"id\":\"ee8c0ede-223f-4f33-a34c-9ef887f317eb\",\"type\":\"Sale\",\"ammount\":{\"value\":4.500000,\"currencyCode\":\"EUR\"},\"quotas\":1,\"applicationReference\":\"47494898-bb32-4781-89e1-b2600429365f\",\"userId\":\"terenc\",\"workDate\":\"2025-01-09T19:23:29.9666667\",\"localId\":52,\"transId\":52,\"topUpIfCompleteExceedTotal\":true,\"askForNewCard\":false}}");
			var natPayload = PayClient.Serializers.Json.Deserialize(stPayload, typeof(object));
			
			var payload = PayClient.Serializers.Json.DeserializeNative(natPayload, typeof(SiTef.Shared.Messages.STDeviceRequest));

			/*
			Task.Run(async () =>
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                // for (int i = 0; i < 10; i++)
                {
                    var userResult = await BusinessProxyRepository.Stand.GetUserByCodeAsync("1111");
					var hotels = await BusinessProxyRepository.Settings.GetHotelsAsync(userResult.Item.Id);
                    var hotel = hotels.First();
					var sessionId = await BusinessProxyRepository.Cashier.CreateSessionTokenAsync(hotel.Id, userResult.Item.Id);
                    var cashiers = await BusinessProxyRepository.Stand.GetCashiersAsync(hotel.Id, userResult.Item.UserInternal ? Guid.Empty : userResult.Item.Id, 1033);
                    var cashierId = cashiers.First().Id;

					var stands = await BusinessProxyRepository.Stand.GetStandsAsync(cashierId, 1033);
                    var standId =(Guid) stands.First().Id;

                    var series = await BusinessProxyRepository.Ticket.GetTicketSerieByStandCashierAsync(standId, cashierId);
                    var settings = await BusinessProxyRepository.Settings.GetGeneralSettingsAsync(false);

					// SetTokenContextData
					var token = await BusinessProxyRepository.Stand.SetStandCashierSessionTokenAsync(standId, cashierId, settings.DefaultTaxSchemaId ?? Guid.Empty, 1033);
					var environment = await BusinessProxyRepository.Stand.GetStandEnvironmentAsync(standId, 1033);

					//var imageManager = new WPF.Model.Model.Stand.ImageManager();

					//environment.Item.Saloons.ToList().ForEach(se =>
					//{
					//	imageManager.ProcessHubImagePath(se.Saloon.ImageTableVacantState);
					//	imageManager.ProcessHubImagePath(se.Saloon.ImageTableBusyState);
					//	imageManager.ProcessHubImagePath(se.Saloon.ImageSaloonFloorPlan);
					//});

					//if (imageManager.MissImages)
					//{
					//	await imageManager.DownloadNewImages();
					//}

					var toAccept = await BusinessProxyRepository.Stand.VerifyTransferToAcceptAsync(standId, 1033);
					var toReturn = await BusinessProxyRepository.Stand.VerifyTransferToReturnAsync(standId, 1033);
					var forTransfer = await BusinessProxyRepository.Stand.GetStandsForTransferAsync("1033");
					var tickets = await BusinessProxyRepository.Ticket.GetTicketInfoByStandCashierAsync(standId, cashierId);

					NewHotel.Pos.Hub.Model.TicketInfo ticket = tickets.First();

					IPosHubApiClient client = CwFactory.Resolve<IPosHubApiClient>();
					var response = await client.RequestPaymentAsync(new NewHotel.Pos.Hub.Contracts.PaySystem.ExternalPaymentRequestContract
					{
						PaymentMethodId = new Guid("ee5a011f-d011-42fa-8285-b1fb026758ff"),
						TicketId = ticket.Id,
						PaySystemTerminalId = null,
						AuthorizationCode = null,
						ExternalToken = null,
						ReferenceTransactionId = null,
						Values = new NewHotel.Pos.Hub.Contracts.PaySystem.ExternalPaymentValues
						{
							UserReference = "1111",
							Workstation = "1",
							Value = 10,
							Tax = 0,
							CurrencyCode = "EUR",
							Quota = 0,
							Tip = 0,
						}
					});

					var entryResult = await client.ConfirmPaymentAsync(response.Result.RequestId);
				}
				sw.Stop();
				allWcf = sw.ElapsedMilliseconds/10;
            }).Wait();
			/*

			Task.Run(async () =>
			{
				BusinessProxyRepository.Cashier.ToString();

				IPosHubApiClient client = CwFactory.Resolve<IPosHubApiClient>();

				Stopwatch sw = new Stopwatch();
				sw.Start();
				for (int i = 0; i < 10; i++)
				{
					var userResult = await client.GetUserByCodeAsync("1111");
					var hotels = await client.GetHotelsAsync(userResult.Result.Id);
					var hotel = hotels.Result.First();
					var loginResult = await client.LoginAsync(new NewHotel.Pos.Hub.Api.Models.LoginModel { UserId = userResult.Result.Id, InstallationId = hotel.Id, LanguageId = 1033 });
					var cashiers = await client.GetCashiersAsync();
					var cashierId = cashiers.Result.First().Id;

					var stands = await client.GetStandsAsync(cashierId);
					var standId = (Guid)stands.Result.First().Id;

					var series = await client.GetTicketSerieByStandCashierAsync(standId, cashierId);
					var settings = await client.GetGeneralSettingsAsync(false);
				}
				sw.Stop();
				allApi = sw.ElapsedMilliseconds / 10;
			}).Wait();
			/* */

			/*
            // if ((agrs?.Length ?? 0) > 0)
            {
                debugTimer = new Timer((e) =>
                {
					_ = ContextUpdate.Debug(null, null);
					//var context = Hotel.Instance?.CurrentStandCashierContext;
					//context?.Debug(e as string);

				}, agrs.Any() ? agrs[0] : "", 2000, 2000);
            }
            */

			base.OnStartup(e);
		}

		public bool SignalExternalCommandLineArgs(IList<string> args)
		{
			return true;
		}

		private static void ShowUnhandledException(Exception e)
		{
			var window = CwFactory.Resolve<IWindow>();
			if (window != null)
				window.ShowError(e.Message);
			else
				MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
		}

		#region Dependencies

		private static void RegisterPaymentTerminals()
		{
			Payments.Device.Processors.IPayMessageProcessor processor = SiTef.Terminal.STMessageProcessorFactory.Create(PayClient.Serializers.Json);
			ExternalPaymentsSupport.AddTerminalSystems([processor]);
		}

		private static void InitializeDependencys()
		{
			CwFactory.Register<IVisualPrinter, SimplePrinterDocument>();
			CwFactory.Register<ITicketPrinter, SimplePrinterDocument>();

			CwFactory.Register<IWindow, ManagerWindow>();
			CwFactory.Register<IAppSecurity, AppSecurity>();
			CwFactory.RegisterWithFactoryMethod<IPosSettings>(() => Hotel.Instance);

			CwFactory.Register<ISessionDataProvider, SessionDataProvider>();
			CwFactory.Register<ILogger, TraceLog>();
			CwFactory.Instance.RegisterSingletonUsingFactory(() =>
			{
				IActionScheduler scheduler = new ActionScheduler(async actionScoped =>
				{
					using (CwFactory.Instance.RequireScope())
						await actionScoped(CwFactory.Instance.Resolve);
				});

				return scheduler;
			});

			RegisterPaymentTerminals();
		}

		private class ManagerWindow : IWindow
		{
			public void ShowBusyDialog(string message) => BusyControl.Instance.Show(message);

			public void ShowBusyDialog() => BusyControl.Instance.Show();

			public void CloseBusyDialog() => BusyControl.Instance.Close();

			public void ShowError(ValidationResult validation)
			{
				ErrorsWindow.ShowErrorWindows(validation);
			}

			public void ShowError(Exception exception)
			{
				ShowError(exception.Message);
			}

			public void ShowError(string message)
			{
				ErrorsWindow.ShowErrorWindows(message, null);
			}

			public void ShowError(string message, string title)
			{
				ErrorsWindow.ShowErrorsWindow(message, title);
			}

			public void ShowInfo(string message, string header)
			{
				ErrorsWindow.ShowInfoWindow(message, header);
			}

			public void ShowOkCancelErrorWindow(string text, Action<bool?> callback)
			{
				ErrorsWindow.ShowErrorWindows(text, callback);
			}

			public Action OpenDialog(object content, bool autoCancel, object dataContext, Func<bool> okAction,
				   Point location, DialogButtons dialogButtons = DialogButtons.OkCancel,
				   Action cancelAction = null, Action closeAction = null, bool hasMaxHeight = true)
			{
				return MainPage.CurrentInstance.OpenDialog(content, autoCancel, dataContext,
					okAction, location, dialogButtons, cancelAction, closeAction, hasMaxHeight);
			}

			public Action OpenDialog<T>(IOpenDialog<T> content, bool autoCancel, object dataContext, Func<T, bool> okAction,
				Point location, DialogButtons dialogButtons = DialogButtons.OkCancel,
				Action cancelAction = null, Action closeAction = null, bool hasMaxHeight = true)
			{
				return MainPage.CurrentInstance.OpenDialog(content, autoCancel, dataContext,
					okAction, location, dialogButtons, cancelAction, closeAction, hasMaxHeight);
			}

			public Action OpenDialog(bool disableClick, object content, Func<ButtonBase, bool> okAction,
				   Action<ButtonBase> cancelAction = null, DialogButtons dialogButtons = DialogButtons.OkCancel)
			{
				return MainPage.CurrentInstance.OpenDialog(disableClick, content, okAction, dialogButtons, cancelAction);
			}

			public void OpenAcceptCancelDialog(object content, Action<ButtonBase> accept, Action<ButtonBase> cancelAction = null)
			{
				MainPage.CurrentInstance.OpenDialog(false, content, b =>
				{
					accept(b);
					return true;
				}, DialogButtons.OkCancel, cancelAction);
			}

			public void OpenAcceptCancelDialog(object content, Func<ButtonBase, bool> accept, Action<ButtonBase> cancelAction = null)
			{
				MainPage.CurrentInstance.OpenDialog(false, content, accept, DialogButtons.OkCancel, cancelAction);
			}

			public void CloseDialog()
			{
				MainPage.CurrentInstance.CloseDialog(null);
			}

			public void OpenDrawer()
			{
				MainPage.CurrentInstance.OpenDrawer();
			}

			public Action PopCloseAction()
			{
				return MainPage.CurrentInstance.PopCloseAction();
			}

			public void ClosePopUp()
			{
				MainPage.CurrentInstance.ClosePopUp();
			}

			public Action OpenDialog(object content, Func<bool> okAction, DialogButtons dialogButtons = DialogButtons.OkCancel, Action cancelAction = null)
			{
				return OpenDialog(false, content, s => okAction(), s => cancelAction?.Invoke(), dialogButtons);
			}
		}

		private class AppSecurity : IAppSecurity
		{
			public void CheckUserWritePermission(Guid userId, Permissions perm, Action<bool, Guid?> callback)
			{
				MainPage.CurrentInstance.CheckUserWritePermission(userId, perm, callback);
			}
		}

		internal class SessionDataProvider : ISessionDataProvider
		{
			public Guid Token => CashierBaseBusinessProxy.Token;
			public string Version => CashierBaseBusinessProxy.Version;
			public string StandWorkDate => CashierBaseBusinessProxy.StandWorkDate;
		}

		#endregion
		#region TextBlockTranslation

		private readonly Dictionary<TextBlock, CultureInfo> _textBlocksTranslation = new Dictionary<TextBlock, CultureInfo>();

		private static void UpdateBinding(TextBlock tb)
		{
			var binding = BindingOperations.GetBindingExpressionBase(tb, TextBlock.TextProperty);
			binding?.UpdateTarget();
		}

		private void TextBlockTranslationLoaded(object sender, RoutedEventArgs e)
		{
			var tb = (TextBlock)sender;
			if (!_textBlocksTranslation.ContainsKey(tb))
			{
				_textBlocksTranslation.Add(tb, Thread.CurrentThread.CurrentUICulture);
				UpdateBinding(tb);
				_textBlocksTranslation[tb] = Thread.CurrentThread.CurrentUICulture;
			}
		}

		private void TextBlockTranslationUnloaded(object sender, RoutedEventArgs e)
		{
			var tb = (TextBlock)sender;
			if (_textBlocksTranslation.ContainsKey(tb))
				_textBlocksTranslation.Remove(tb);
		}

		public void RefreshTextBlocksTranslation()
		{
			UIThread.Invoke(() =>
			{
				foreach (var tbt in _textBlocksTranslation.ToArray())
				{
					if (tbt.Value != Thread.CurrentThread.CurrentUICulture)
					{
						UpdateBinding(tbt.Key);
						_textBlocksTranslation[tbt.Key] = Thread.CurrentThread.CurrentUICulture;
					}
				}
			});
		}

		#endregion

		#endregion
	}
}