﻿using NewHotel.WPF.Common.Converters;
using System.Globalization;
using System.Threading;
using System.Windows.Data;

namespace NewHotel.WPF.App.Pos.Controls
{
    public class LocalizedMultiBinding : MultiBinding
    {
        public LocalizedMultiBinding()
        {
            ConverterCulture = (CultureInfo)Thread.CurrentThread.CurrentUICulture.Clone();
            ConverterCulture.NumberFormat.CurrencySymbol = string.Empty;
        }
    }

    public class LocalizedBinding : Binding
    {
        public LocalizedBinding()
            : base()
        {
            ConverterCulture = (CultureInfo)Thread.CurrentThread.CurrentUICulture.Clone();
            ConverterCulture.NumberFormat.CurrencySymbol = string.Empty;
        }

        public LocalizedBinding(string path)
            : base(path)
        {
            ConverterCulture = (CultureInfo)Thread.CurrentThread.CurrentUICulture.Clone();
            ConverterCulture.NumberFormat.CurrencySymbol = string.Empty;
        }
    }

    public class BoolToInvisibilityBinding : LocalizedBinding
    {
        public BoolToInvisibilityBinding()
            : base()
        {
            Converter = new BoolToInvisibility();
        }

        public BoolToInvisibilityBinding(string path)
            : base(path)
        {
            Converter = new BoolToInvisibility();
        }
    }
}