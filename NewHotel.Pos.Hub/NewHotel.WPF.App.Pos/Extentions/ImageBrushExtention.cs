﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace NewHotel.WPF.App.Pos.Controls
{
    public class ImageBrushExtention
    {
        /// <summary>
        /// Default Image Source (from resources)
        /// </summary>
        public static readonly DependencyProperty DefaultSourceProperty = DependencyProperty.RegisterAttached("DefaultSource", typeof(string), typeof(ImageBrushExtention), new PropertyMetadata(OnDefaultSourceChanged));
        public static string GetDefaultSource(DependencyObject obj)
        {
            if (null == obj)
            {
                return String.Empty;
            }
            return (string)obj.GetValue(DefaultSourceProperty);
        }
        public static void SetDefaultSource(DependencyObject obj, string value)
        {
            if (null != obj)
            {
                obj.SetValue(DefaultSourceProperty, value);
            }
        }
        private static void OnDefaultSourceChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty((string)e.NewValue) && !DesignerProperties.GetIsInDesignMode(o))
            {
                try
                {
                    //Reset Value
                    ImageBrush imageBrush = (ImageBrush)o;
                    imageBrush.ImageSource = null;
                    //Try Load Default Image
                    BitmapImage image = new BitmapImage();
                    image.BeginInit();
                    image.UriSource = new Uri((string)e.NewValue);
                    image.EndInit();
                    //Set Image
                    imageBrush.ImageSource = image;
                }
                catch (Exception)
                {

                }
                
            }
        }
        /// <summary>
        /// Dynamic Image Source (base64 from hub)
        /// </summary>
        public static readonly DependencyProperty DynamicSourceProperty = DependencyProperty.RegisterAttached("DynamicSource", typeof(string), typeof(ImageBrushExtention), new PropertyMetadata(OnDynamicSourceChanged));
        public static string GetDynamicSource(DependencyObject obj)
        {
            if (null == obj)
            {
                return String.Empty;
            }
            return (string)obj.GetValue(DynamicSourceProperty);
        }
        public static void SetDynamicSource(DependencyObject obj, string value)
        {
            if (null != obj)
            {
                obj.SetValue(DynamicSourceProperty, value);
            }
        }
        private static void OnDynamicSourceChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty((string)e.NewValue) && !DesignerProperties.GetIsInDesignMode(o))
            {
                try
                {
                    string value = e.NewValue.ToString();
                    //Is a Resource Image
                    #region Resource Image
                    if (value.IndexOf("pack") != -1)
                    {
                        //Reset Value
                        ImageBrush imageBrush = (ImageBrush)o;
                        imageBrush.ImageSource = null;
                        //Try Load Default Image
                        BitmapImage image = new BitmapImage();
                        image.BeginInit();
                        image.UriSource = new Uri((string)e.NewValue);
                        image.EndInit();
                        //Set Image
                        imageBrush.ImageSource = image;
                    }
                    #endregion
                    #region Base64 Image
                    //Is a Base64 Image
                    else
                    {
                        //Reset Value
                        ImageBrush imageBrush = (ImageBrush)o;
                        imageBrush.ImageSource = null;
                        //Try Load Default Image
                        byte[] binaryData = Convert.FromBase64String((string)e.NewValue);

                        BitmapImage image = new BitmapImage();
                        image.BeginInit();
                        image.StreamSource = new MemoryStream(binaryData);
                        image.EndInit();

                        //Set Image
                        imageBrush.ImageSource = image;
                    } 
                    #endregion
                }
                catch (Exception)
                {

                }

            }
            else
            {
                ImageBrush imageBrush = (ImageBrush)o;
                imageBrush.ImageSource = null;
            }
        }

    }
}
