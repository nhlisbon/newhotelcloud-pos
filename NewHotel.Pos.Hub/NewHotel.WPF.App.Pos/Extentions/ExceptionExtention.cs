﻿using NewHotel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.WPF.App.Pos
{
    public static class ExceptionExtention
    {
        public static string PrintCashierMsg(this Exception exception)
        {
            string result = String.Empty;

            if (exception is AggregateException && exception.InnerException != null)
            {
                exception = exception.InnerException;
            }

            if (exception is TimeoutException)
            {
                result = "Operation has Timeout. Please try again...";
            }
            else
            {
                result = exception.ToLogString();
            }

            return result;
        }
        
    }
}
