﻿using System.Text;

namespace NewHotel.WPF.App.Pos
{
    public static class StringExtension
    {
        public static string Clean(this string source)
        {
            if (string.IsNullOrEmpty(source))
                return source;

            var bytes = Encoding.GetEncoding(1251).GetBytes(source); // 8 bit characters
            return Encoding.ASCII.GetString(bytes); // 7 bit characters
        }
    }
}