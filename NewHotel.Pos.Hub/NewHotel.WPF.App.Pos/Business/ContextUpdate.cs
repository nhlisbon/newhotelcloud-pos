﻿using DevExpress.Xpf.Editors.Helpers;
using NewHotel.Contracts;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Hub.Model;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace NewHotel.WPF.App.Pos
{
	using TicketSlimList = List<TicketSlim>;

	public static class ContextUpdate
	{
		static bool justOnce = false;
		public static async Task Debug(this StandCashierContext context, string arg)
		{
			{
				if (justOnce)
					return;
				justOnce = true;
				int numOfTries = 1000;
				int completed = 0;
				ConcurrentDictionary<int, (int code, System.Threading.Tasks.Task task, ValidationResult result)> res = new();
				var tasks = new Task[numOfTries];

				for (int i = 0; i < numOfTries; i++)
				{
					int idVal = i;
					var task = BusinessProxyRepository.Stand.GetUserByCodeAsync("2706")
						.ContinueWith(x =>
						{
							if (x.Status == System.Threading.Tasks.TaskStatus.RanToCompletion)
							{
								ValidationResult result = !x.IsFaulted ? x.Result : null;
								res.AddOrUpdate(idVal, id => (id, x, result), (id, vals) => (id, x, result));
								completed++;
							}
						});
					res.AddOrUpdate(idVal, id => (id, task, null), (id, vals) => vals);
					tasks[idVal] = task;
				}
				try
				{
					var tres = Task.WaitAll(tasks, 10000);
				}
				catch (Exception ex)
				{
				}
			}


			/*
			if (!arg.StartsWith("/tk"))
				return;
			ITicket ticket = null;
			string tk = arg.Substring(3);
			if (context.QuickTickets?.OrderBy(x=>x.Id).Any() ?? false)
			{
				// --
				if (int.TryParse(tk, out var ticketInd))
				{
					ticket = context.QuickTickets.ElementAt(ticketInd);
				}
			}

			if (ticket !=null)
			{
				var result = await BusinessProxyRepository.Ticket.LoadTicketForEditionAsync(ticket.Id, true);
				if (!result.HasErrors)
				{
					var ticketContract = (POSTicketContract)result.Contract;
					await Task.Delay(200);
					var persistResult = await BusinessProxyRepository.Ticket.PersistTicketAsync(ticketContract);
					ticketContract = (POSTicketContract)result.Contract;
					var newResult = await BusinessProxyRepository.Ticket.SaveTicketFromEditionAsync(ticket.Id, 1);
				}
			}
			*/
		}

		static async Task InternalUpdateTables(this StandCashierContext context, bool fromBackground)
		{
			var contracts = await BusinessProxyRepository.Ticket.GetTicketInfoByStandCashierAsync(context.Stand.Id, context.Cashier.Id);

			Dictionary<Guid?, TicketSlimList> slimTickets = new Dictionary<Guid?, TicketSlimList>();
			var quickTickets = new TicketSlimList();
			var acceptedTickets = new TicketSlimList();
			IEnumerable<TicketSlim> allTickets;
			foreach (var ticket in (allTickets = contracts.Select(x => new TicketSlim(x))))
			{
				if (ticket.TableId.HasValue)
				{
					if (!slimTickets.TryGetValue(ticket.TableId, out var ticketSlimList))
					{
						ticketSlimList = new TicketSlimList();
						slimTickets[ticket.TableId] = ticketSlimList;
					}
					ticketSlimList.Add(ticket);
				}
				else
				{
					if (ticket.IsAcceptedAsTransfer)
						acceptedTickets.Add(ticket);
					else
						quickTickets.Add(ticket);
				}
			}

			void UpdateVisual()
			{
				context.QuickTickets.Clear();
				context.AcceptedTickets.Clear();
				context.ReturnedTickets.Clear();
				context.Stand.Tickets.Clear();

				foreach (var table in context.Stand.Saloons.SelectMany(x => x.Tables))
				{
					slimTickets.TryGetValue(table.Id, out var ticketSlimList);
					table.Tickets = new ObservableCollection<TicketSlim>(ticketSlimList ?? Enumerable.Empty<TicketSlim>());
					table.RecalculateTotal();
				}
				foreach (var ticket in quickTickets)
					context.QuickTickets.Add(ticket);
				foreach (var ticket in acceptedTickets)
					context.AcceptedTickets.Add(ticket);

				//context.QuickTickets = new ObservableCollection<ITicket>(quickTickets ?? Enumerable.Empty<ITicket>());
				//context.AcceptedTickets = new ObservableCollection<ITicket>(acceptedTickets ?? Enumerable.Empty<ITicket>());
				context.Stand.Tickets = new ObservableCollection<ITicket>(allTickets);
			}

			if (fromBackground)
			{
				UIThread.InvokeAsync(DispatcherPriority.Background, () =>
				{
					UpdateVisual();
				});
			}
			else
				UpdateVisual();
		}

		public static async Task UpdateTables(this StandCashierContext context)
		{
			await context.InternalUpdateTables(false).ConfigureAwait(true);
		}

		public static Task UpdateTablesFromBackground(this StandCashierContext context)
		{
			return context.InternalUpdateTables(true);
			/*
			var contracts = await BusinessProxyRepository.Ticket.GetTicketInfoByStandCashierAsync(context.Stand.Id, context.Cashier.Id);

			Dictionary<Guid?, TicketSlimList> slimTickets = new Dictionary<Guid?, TicketSlimList>();
			var quickTickets = new TicketSlimList();
			var acceptedTickets = new TicketSlimList();
			foreach (var ticket in contracts.Select(x => new TicketSlim(x)))
			{
				if (ticket.TableId.HasValue)
				{
					if (!slimTickets.TryGetValue(ticket.TableId, out var ticketSlimList))
					{
						ticketSlimList = new TicketSlimList();
						slimTickets[ticket.TableId] = ticketSlimList;
					}
					ticketSlimList.Add(ticket);
				}
				else
				{
					if (ticket.IsAcceptedAsTransfer)
						acceptedTickets.Add(ticket);
					else
						quickTickets.Add(ticket);
				}
			}

			UIThread.InvokeAsync(DispatcherPriority.Background, () =>
			{
				context.QuickTickets.Clear();
				context.AcceptedTickets.Clear();
				context.ReturnedTickets.Clear();

				foreach (var table in context.Stand.Saloons.SelectMany(x => x.Tables))
				{
					slimTickets.TryGetValue(table.Id, out var ticketSlimList);
					table.Tickets = new ObservableCollection<TicketSlim>(ticketSlimList ?? Enumerable.Empty<TicketSlim>());
				}

				foreach (var ticket in quickTickets)
					context.QuickTickets.Add(ticket);
				foreach (var ticket in acceptedTickets)
					context.AcceptedTickets.Add(ticket);
			});
			*/

			//context.QuickTickets.Clear();
			//context.AcceptedTickets.Clear();
			//context.ReturnedTickets.Clear();

			//foreach (var table in context.Stand.Saloons.SelectMany(x => x.Tables))
			//{
			//	slimTickets.TryGetValue(table.Id, out var ticketSlimList);
			//	table.Tickets = new ObservableCollection<TicketSlim>(ticketSlimList ?? Enumerable.Empty<TicketSlim>());
			//}

			//context.QuickTickets.Append(quickTickets);
			//context.AcceptedTickets.Append(acceptedTickets);
		}
	}
}
