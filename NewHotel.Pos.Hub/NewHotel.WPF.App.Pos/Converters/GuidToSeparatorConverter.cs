﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using NewHotel.WPF.App.Pos.Model;

namespace NewHotel.WPF.App.Pos.Converters
{
    [ValueConversion(typeof(Guid), typeof(Separator))]
    public class GuidToSeparatorConverter : IValueConverter
    {

        IEnumerable<object> _separators = null;

        public GuidToSeparatorConverter(IEnumerable<object> separators)
        {
            _separators = separators;
        }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (_separators != null)
                return _separators.FirstOrDefault(x => (x as Separator).Id.Equals(value));
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (value as Separator).Id;
        }

        #endregion
    }
}
