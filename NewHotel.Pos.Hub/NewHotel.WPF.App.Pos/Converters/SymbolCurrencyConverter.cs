﻿using System;
using System.Globalization;
using System.Windows.Data;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Converters
{
    public class SymbolCurrencyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is CurrencyExchange exchange)
                return exchange.CurrencyCashier.Symbol ?? exchange.CurrencyCashier.CurrencyId;
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
