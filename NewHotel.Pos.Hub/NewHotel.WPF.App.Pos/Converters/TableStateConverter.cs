﻿using NewHotel.WPF.App.Pos.Model;
using System;
using System.Windows.Data;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Converters
{
    [ValueConversion(typeof(Table.States), typeof(bool))]
    public class TableStateConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value == null ? true : (Table.States)value == Table.States.Ready;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }

        #endregion
    }
}
