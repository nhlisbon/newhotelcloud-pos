﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using NewHotel.Pos.Hub.Model;
using NewHotel.WPF.App.Pos.Model;

namespace NewHotel.WPF.App.Pos.Converters
{
    public class MoneyToStringConverter : IValueConverter
    {
        public string FormatNumber { get; set; } = "C2";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value)
            {
                case ClosedTicketRecord ticket:
                {
                    var asString = ticket.Total.ToString(FormatNumber);
                    var hotel = Hotel.Instance;
                    string symbol = hotel.Currencys.FirstOrDefault(x => x.CurrencyId == ticket.CurrencyId)?.Symbol ?? hotel.BaseCurrency?.Symbol;
                    return $"{asString} {symbol}";
                }
                case string s:
                {
                    if (double.TryParse(s.Trim(), out var asDouble))
                        return Convert(asDouble, targetType, parameter, culture);
                    break;
                }
            }
            return value?.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
