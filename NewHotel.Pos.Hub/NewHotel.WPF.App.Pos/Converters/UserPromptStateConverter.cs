﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Converters
{
	[ValueConversion(typeof(UserPromptState), typeof(bool))]
	[ValueConversion(typeof(UserPromptState), typeof(Visibility))]
	public class UserPromptStateConverter : IValueConverter
	{
		public object? Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is not UserPromptState state) return null;
			if (targetType == typeof(Visibility))
				return state == UserPromptState.Enabled ? Visibility.Visible : Visibility.Collapsed;
			if (targetType == typeof(bool)) return state == UserPromptState.Enabled;
			return null;
		}

		public object? ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (targetType != typeof(UserPromptState)) return null;
			return value switch
			{
				Visibility visibility => visibility == Visibility.Visible ? UserPromptState.Enabled : UserPromptState.Disabled,
				bool b => b ? UserPromptState.Enabled : UserPromptState.Disabled,
				_ => null
			};
		}
	}
}
