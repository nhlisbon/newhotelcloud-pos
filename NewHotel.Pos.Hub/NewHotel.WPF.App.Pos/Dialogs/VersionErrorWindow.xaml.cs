﻿using NewHotel.WPF.Common;
using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace NewHotel.WPF.App.Pos.Dialogs
{
    /// <summary>
    /// Interaction logic for ErrorsWindow.xaml
    /// </summary>
    public partial class VersionErrorWindow : Window
    {
        private static bool updateInstanced = false;
        public static bool? ShowErrorsWindow(string message, string header = "Error", Error error = null, Window parent = null, int count = 0)
        {
            if (!updateInstanced)
            {
                var window = new VersionErrorWindow(header, message, error, count);
                window.Owner = parent == null ? Application.Current.MainWindow : parent;
                updateInstanced = true;
                return window.ShowDialog();
            }
            else return null;
        }

        public VersionErrorWindow()
        {
            InitializeComponent();
        }

        public VersionErrorWindow(string header, string message, Error error, int count)
            : this()
        {
            Header = header;
            Message = message;
            Error = error;
        }

        public string Header
        {
            get { return (string)GetValue(HeaderProperty); }
            private set { SetValue(HeaderProperty, value); }
        }

        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register("Header", typeof(string), typeof(VersionErrorWindow));

        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            private set { SetValue(MessageProperty, value); }
        }

        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register("Message", typeof(string), typeof(VersionErrorWindow));

        public Error Error
        {
            get { return (Error)GetValue(ErrorProperty); }
            set { SetValue(ErrorProperty, value); }
        }

        public static readonly DependencyProperty ErrorProperty =
            DependencyProperty.Register("Error", typeof(Error), typeof(VersionErrorWindow));

        public bool NoAllowIgnore
        {
            get { return (bool)GetValue(NoAllowIgnoreProperty); }
            set { SetValue(NoAllowIgnoreProperty, value); }
        }

        public static readonly DependencyProperty NoAllowIgnoreProperty =
            DependencyProperty.Register("NoAllowIgnore", typeof(bool), typeof(VersionErrorWindow));



        private void updateButton_Click(object sender, RoutedEventArgs e)
        {
            (Resources["hide"] as Storyboard).Begin();
            DialogResult = true;
        }

        private void ignoreButton_Click(object sender, RoutedEventArgs e)
        {
            updateInstanced = false;
            (Resources["hide"] as Storyboard).Begin();
            DialogResult = false;
        }

        private void errorWindow_Loaded(object sender, RoutedEventArgs e)
        {
            (Resources["show"] as Storyboard).Begin();
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            this.Close();
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property.Name == "Error" && e.NewValue != null)
                NoAllowIgnore = !(e.NewValue as Error).AllowIgnore;
        }
    }
}
