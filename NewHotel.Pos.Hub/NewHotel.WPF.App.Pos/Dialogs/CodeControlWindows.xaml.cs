﻿using NewHotel.WPF.App.Pos.Model;
using System;
using System.Reflection;
using System.Windows;
using System.Windows.Media.Animation;

namespace NewHotel.WPF.App.Pos.Dialogs
{
    public enum CodeControlOpt { FullSync }
    /// <summary>
    /// Interaction logic for ErrorsWindow.xaml
    /// </summary>
    public partial class CodeControlWindow : Window
    {
        public CodeControlOpt Opt { get; set; }
        public static bool? ShowCodeWindow(CodeControlOpt opt)
        {
            var window = new CodeControlWindow();
            window.numPad.CurrentControl = window.code;
            window.Owner = Application.Current.MainWindow;
            window.Opt = opt;
            return window.ShowDialog();
        }

        public CodeControlWindow()
        {
            InitializeComponent();
        }

        public string ConditionError
        {
            get { return (string)GetValue(ConditionErrorProperty); }
            set { SetValue(ConditionErrorProperty, value); }
        }

        public static readonly DependencyProperty ConditionErrorProperty =
            DependencyProperty.Register("ConditionError", typeof(string), typeof(CodeControlWindow));



        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            (Resources["hide"] as Storyboard).Begin();
        }

        private void window_Loaded(object sender, RoutedEventArgs e)
        {
            (Resources["show"] as Storyboard).Begin();
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ShowUserError()
        {
            (Resources["showUserError"] as Storyboard).Begin();
        }

        private void HideUserError()
        {
            (Resources["hideUserError"] as Storyboard).Begin();
        }

        private void ShowCodeError()
        {
            (Resources["showCodeError"] as Storyboard).Begin();
        }

        private void HideCodeError()
        {
            (Resources["hideCodeError"] as Storyboard).Begin();
        }

        private void ShowConditionError()
        {
            (Resources["showConditionError"] as Storyboard).Begin();
        }

        private void HideConditionError()
        {
            (Resources["hideConditionError"] as Storyboard).Begin();
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            //LoginData.Password = password.Password;
        }

        private void code_PasswordChanged(object sender, RoutedEventArgs e)
        {
            //LoginData.Code = code.Password;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            //LoginData.Code = "";
            //(Resources["FromByCode"] as Storyboard).Begin();
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            //LoginData.Code = "";
            //(Resources["ToByCode"] as Storyboard).Begin();
        }

        private void login_Click(object sender, RoutedEventArgs e)
        {
            string enteredCode = code.Password;
            string expectedCode = String.Empty;

            switch (Opt)
            {
                case CodeControlOpt.FullSync:
                    {
                        
                        expectedCode = Assembly.GetExecutingAssembly().GetName().Version.ToString().Replace(".", "");
                    }
                    break;
                default:
                    break;
            }

            if (enteredCode == expectedCode)
            {
                DialogResult = true;
                (Resources["hide"] as Storyboard).Begin();
            }
            else
            {
                ShowCodeError();
            }
            
        }
    }
}
