﻿using NewHotel.WPF.Common;
using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace NewHotel.WPF.App.Pos.Dialogs
{
    /// <summary>
    /// Interaction logic for ErrorsWindow.xaml
    /// </summary>
    public partial class CriticalErrorWindow : Window
    {
        public static bool NoShow { get; set; }

        public static bool? ShowErrorsWindow(string message, string header = "Error", Error error = null, Window parent = null, int count = 0, bool? allowRetry = null)
        {
            if (NoShow) return null;
            var window = new CriticalErrorWindow(header, message, error, count);
            window.Owner = parent == null ? Application.Current.MainWindow : parent;
            if (allowRetry.HasValue && error.AllowRetry)
                window.AllowRetry = allowRetry.Value;
            return window.ShowDialog();
        }

        public CriticalErrorWindow()
        {
            InitializeComponent();
        }

        public CriticalErrorWindow(string header, string message, Error error, int count)
            :this()
        {
            Header = header;
            Message = message;
            Error = error;
        }

        public string Header
        {
            get { return (string)GetValue(HeaderProperty); }
            private set { SetValue(HeaderProperty, value); }
        }

        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register("Header", typeof(string), typeof(CriticalErrorWindow));

        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            private set { SetValue(MessageProperty, value); }
        }

        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register("Message", typeof(string), typeof(CriticalErrorWindow));

        public Error Error
        {
            get { return (Error)GetValue(ErrorProperty); }
            set { SetValue(ErrorProperty, value); }
        }

        public static readonly DependencyProperty ErrorProperty =
            DependencyProperty.Register("Error", typeof(Error), typeof(CriticalErrorWindow));

        public bool NoAllowIgnore
        {
            get { return (bool)GetValue(NoAllowIgnoreProperty); }
            set { SetValue(NoAllowIgnoreProperty, value); }
        }

        public static readonly DependencyProperty NoAllowIgnoreProperty =
            DependencyProperty.Register("NoAllowIgnore", typeof(bool), typeof(CriticalErrorWindow));



        public bool AllowRetry
        {
            get { return (bool)GetValue(AllowRetryProperty); }
            set { SetValue(AllowRetryProperty, value); }
        }

        public static readonly DependencyProperty AllowRetryProperty =
            DependencyProperty.Register("AllowRetry", typeof(bool), typeof(CriticalErrorWindow));



        private void retryButton_Click(object sender, RoutedEventArgs e)
        {
            (Resources["hide"] as Storyboard).Begin();
            DialogResult = true;
        }

        private void ignoreButton_Click(object sender, RoutedEventArgs e)
        {
            (Resources["hide"] as Storyboard).Begin();
            DialogResult = false;
        }

        private void errorWindow_Loaded(object sender, RoutedEventArgs e)
        {
            (Resources["show"] as Storyboard).Begin();
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            this.Close();
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property.Name == "Error" && e.NewValue != null)
            {
                NoAllowIgnore = !(e.NewValue as Error).AllowIgnore;
                AllowRetry = (e.NewValue as Error).AllowRetry;
            }
        }
    }
}
