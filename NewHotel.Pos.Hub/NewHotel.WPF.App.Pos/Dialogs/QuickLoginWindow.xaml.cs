﻿using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Animation;
using NewHotel.Pos.Communication;
using NewHotel.WPF.Model.Model;
using UserRecord = NewHotel.Pos.Hub.Model.UserRecord;
using System.Threading;

namespace NewHotel.WPF.App.Pos.Dialogs
{
    /// <summary>
    /// Interaction logic for ErrorsWindow.xaml
    /// </summary>
    public partial class QuickLoginWindow : Window
    {
        #region Variables + Constructor

        private Func<User, bool> _userAction = null;
        private Action _cancelAction = null;

        public QuickLoginWindow()
        {
            InitializeComponent();
            LoginData = new LoginData();
            LoginData.IsByUserPwd = false;
            DataContext = LoginData;
        }

        #endregion
        #region Properties

        public string ConditionError
        {
            get { return (string)GetValue(ConditionErrorProperty); }
            set { SetValue(ConditionErrorProperty, value); }
        }

        public static readonly DependencyProperty ConditionErrorProperty =
            DependencyProperty.Register(nameof(ConditionError), typeof(string), typeof(QuickLoginWindow));

        public static bool NoShow { get; set; }

        public LoginData LoginData { get; set; }

        #endregion
        #region Methods

        public static bool? ShowLoginWindow(Func<User, bool> userAction, string permission, Window parent = null, string error = "The user is not allowed to perform the requested operation", Action cancelAction = null)
        {
            if (!NoShow)
            {
                var window = new QuickLoginWindow();
                window.numPad.CurrentControl = window.code;
                window.Owner = parent ?? Application.Current.MainWindow;
                window._userAction = userAction;
                window.permission.Text = permission.ToUpper();
                window.ConditionError = error;
                return window.ShowDialog();
            }
            
            return null;
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            _userAction(null);
            BeginHideAnimation();
        }

        private void window_Loaded(object sender, RoutedEventArgs e)
        {
            BeginShowAnimation();
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            Close();
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            LoginData.Password = password.Password;
        }

        private void code_PasswordChanged(object sender, RoutedEventArgs e)
        {
            LoginData.Code = code.Password;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            LoginData.Code = "";
            BeginFromByCodeAnimation();
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            LoginData.Code = "";
            BeginToByCodeAnimation();
        }

        private async void login_Click(object sender, RoutedEventArgs e)
        {
            ValidationItemResult<User> validation = LoginData.IsByUserPwd
                ? await Hotel.GetUserByNameAsync(LoginData.UserName)
                : await Hotel.GetUserByCodeAsync(LoginData.Code);

            if (validation.IsEmpty && validation.Item != null)
            {
                var login = true;
                if (LoginData.IsByUserPwd)
                {
                    var userPassword = validation.Item.Password;
                    login = PasswordUtils.VerifyPassword(userPassword, LoginData.Password);
                }

                if (login)
                {
                    var result = _userAction(validation.Item);
                    if (result)
                    {
                        DialogResult = result;
                        BeginHideAnimation();
                    }
                }
                else _userAction(null);
            }
            else _userAction(null);
        }

        private void BeginToByCodeAnimation()
        {
            ((Storyboard)Resources["ToByCode"]).Begin();
        }

        private void BeginHideAnimation()
        {
            var hideStoryboard = (Storyboard)Resources["hide"];
            hideStoryboard.Completed += HideStoryboard_Completed;
            hideStoryboard.Begin();
        }

        private void HideStoryboard_Completed(object sender, EventArgs e)
        {
            if(sender is Storyboard hideStoryboard)
            {
                hideStoryboard.Completed -= HideStoryboard_Completed;
                Close();
            }
            else if(sender is ClockGroup hideClock)
            {
                hideClock.Completed -= HideStoryboard_Completed;
                Close();
            }
        }

        private void BeginShowAnimation()
        {
            ((Storyboard)Resources["show"]).Begin();
        }

        private void BeginFromByCodeAnimation()
        {
            ((Storyboard)Resources["FromByCode"]).Begin();
        }

        #endregion
    }
}
