﻿using System.Windows;
using NewHotel.WPF.App.Pos.Controls.Payments;


namespace NewHotel.WPF.App.Pos.Dialogs
{
    /// <summary>
    /// Interaction logic for CollectWindow.xaml
    /// </summary>
    public partial class CollectWindow : Window
    {
        public CollectWindow()
        {
            InitializeComponent();

            //Application.Current.Resources = new default_skin();
        }

        private void CollectPanel_CloseClick(CollectPanel obj)
        {
            Close();
        }
    }
}
