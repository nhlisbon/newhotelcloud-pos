﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using NewHotel.Contracts;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Hub.Contracts.Common.Contracts;
using NewHotel.Pos.IoC;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using NewHotel.WPF.Common.Controls.Busy;
using NewHotel.WPF.Model.Interface;
using NewHotel.WPF.Model.Model.MealsControl;

namespace NewHotel.WPF.App.Pos.Controls.MealsControl
{
    /// <summary>
    /// Interaction logic for MealsControlViewer.xaml
    /// </summary>
    public partial class MealsControlViewer
    {
        #region Variables

        private readonly IWindow _window = CwFactory.Resolve<IWindow>();
        private CollectionView? _collectionView;
        private bool _refresh;

        #endregion

        #region Constructors

        public MealsControlViewer()
        {
            InitializeComponent();

            var collection = new ObservableCollection<MealsControlRecord>();
            _collectionView = collection.GetCollectionView();
        }

        #endregion

        #region Properties

        public MealsControlFilter Filter
        {
            get => (MealsControlFilter)GetValue(FilterProperty);
            set => SetValue(FilterProperty, value);
        }

        public static readonly DependencyProperty FilterProperty =
            DependencyProperty.Register(nameof(Filter), typeof(MealsControlFilter), typeof(MealsControlViewer));

        public bool IsCheckIn
        {
            get => (bool)GetValue(IsCheckInProperty);
            set => SetValue(IsCheckInProperty, value);
        }

        public static readonly DependencyProperty IsCheckInProperty =
            DependencyProperty.Register(nameof(IsCheckIn), typeof(bool), typeof(MealsControlViewer));

        private Guid? InstallationId { get; set; }

        #endregion

        #region Public Methods

        public void ResetView()
        {
            GoToStartView();
        }

        private void GoToStartView()
        {
            ShowMealsControls();
        }

        public async void Init()
        {
            Filter = new MealsControlFilter
            {
                WorkDate = Hotel.Instance.CurrentStandCashierContext.Stand.StandWorkDate,
                GroupName = string.Empty,
                Room = string.Empty,
                GuestFullName = string.Empty
            };

            _refresh = false;
            if (Hotel.Instance.IsHtml5)
                await UpdateHotels(Hotels);
        }

        #endregion

        #region Private Methods

        private async void LoadDataSource_Click(object sender, RoutedEventArgs e)
        {
            await LoadData();
            _refresh = true;
        }

        private bool FilterRecord(MealsControlRecord record)
        {
            if (Filter == null)
                return true;

            bool matchesGroupName = string.IsNullOrEmpty(Filter.GroupName) || record.GroupName.Trim().ToLower().Contains(Filter.GroupName.Trim().ToLower());
            bool matchesRoom = string.IsNullOrEmpty(Filter.Room) || record.Room.Trim().ToLower().Contains(Filter.Room.Trim().ToLower());
            bool matchesGuestFullName = string.IsNullOrEmpty(Filter.GuestFullName) || record.GuestFullName.Trim().ToLower().Contains(Filter.GuestFullName.Trim().ToLower());
            bool matchesConsumptionCardNumber = string.IsNullOrEmpty(Filter.ConsumptionCardNumber) || record.ConsumptionCardNumber.Trim().ToLower().Contains(Filter.ConsumptionCardNumber.Trim().ToLower());

            return matchesGroupName && matchesRoom && matchesGuestFullName && matchesConsumptionCardNumber;
        }

        private async Task LoadData()
        {
            BusyControl.Instance.Show();
            try
            {
                MealsControlGrid.ItemsSource = null;
                var model = new MealsFilterModel
                {
                    Date = Filter.WorkDate,
                    JustCheckInReservation = Filter.IsCheckIn,
                    InstallationId = InstallationId
                };
                var result = await BusinessProxyRepository.Stand.GetMealsControl(model);
                if (result.IsEmpty)
                {
                    var collection = new ObservableCollection<MealsControlRecord>(result.ItemSource.OrderBy(p => p.RoomSort));
                    foreach (var item in collection)
                        item.PropertyChanged += Item_PropertyChanged;
                    _collectionView = collection.GetCollectionView();
                    _collectionView.Filter = r => FilterRecord((MealsControlRecord)r);
                    MealsControlGrid.ItemsSource = _collectionView;
                    BusyControl.Instance.Close();
                }
                else
                {
                    BusyControl.Instance.Close();
                    _window.ShowError(result);
                }
            }
            catch (Exception e)
            {
                BusyControl.Instance.Close();
                _window.ShowError(e.Message);
            }
        }

        private async Task UpdateData(MealsControlRecord record)
        {
            BusyControl.Instance.Show();
            try
            {
                var result = await BusinessProxyRepository.Stand.UpdateMealsControl(record.Id, record.Breakfast, record.Lunch, record.Dinner, InstallationId);
                BusyControl.Instance.Close();
                if (!result.IsEmpty)
                    _window.ShowError(result);
            }
            catch (Exception e)
            {
                BusyControl.Instance.Close();
                _window.ShowError(e.Message);
            }
        }

        private void ShowMealsControls()
        {
            MealsControlContainer.Visibility = Visibility.Visible;
        }

        private async void Item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var record = (MealsControlRecord)sender;
            switch (e.PropertyName)
            {
                case "Breakfast":
                case "Lunch":
                case "Dinner":
                    await UpdateData(record);
                    break;
            }
        }

        private async Task UpdateHotels(ItemsControl ctrl)
        {
            var result = await BusinessProxyRepository.Stand.GetChargeHotelsAsync();
            if (!result.IsEmpty)
                _window.ShowError(result);
            else
            {
                var items = result.ItemSource.OrderBy(i => i.Id == Hotel.Instance.Id ? 0 : 1);

                // ReSharper disable once PossibleMultipleEnumeration
                ctrl.ItemsSource = items;
                // ReSharper disable once PossibleMultipleEnumeration
                if (items.Any(i => i.Id != Hotel.Instance.Id))
                    ctrl.Visibility = Visibility.Visible;
            }
        }

        private void Hotel_Loaded(object sender, RoutedEventArgs e)
        {
            var rb = (RadioButton)sender;

            var record = (ChargeHotelRecord)rb.DataContext;
            rb.IsChecked = record.Id == Hotel.Instance.Id;
        }

        private async void Hotel_Checked(object sender, RoutedEventArgs e)
        {
            var rb = (RadioButton)sender;

            if (rb.DataContext is not ChargeHotelRecord record) return;

            InstallationId = record.Id == Hotel.Instance.Id ? new Guid?() : record.Id;

            if (_refresh)
                await LoadData();
        }

        #endregion

        private void OnRefreshCollectionView(object sender, RoutedEventArgs e)
        {
            _collectionView.Refresh();
        }
    }
}