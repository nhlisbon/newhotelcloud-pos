﻿using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Animation;
using NewHotel.Pos.Localization;
using NewHotel.WPF.Model.Model;
using Separator = NewHotel.WPF.App.Pos.Model.Separator;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for ProductsViewer.xaml
    /// </summary>
    public partial class ProductsViewer : ContentControl
    {

        #region Variables

        private CategoryButton lastFamily;
        private CategoryButton lastGroup;
        private Category recent;
        private Stand? _stand;

        #endregion

        #region Events
        
        public event Action<Product, int>? Product_Click;
        public event Action<ProductsViewer, bool> AllProductsVisibilityChanged;
        
        #endregion

        #region Constructor

        public ProductsViewer()
        {
            InitializeComponent();
        }
        
        #endregion

        #region Properties
        
        public Separator? SelectedSeparator
        {
            get => (Separator)GetValue(SelectedSeparatorProperty);
            set => SetValue(SelectedSeparatorProperty, value);
        }

        public static readonly DependencyProperty SelectedSeparatorProperty =
            DependencyProperty.Register(nameof(SelectedSeparator), typeof(Separator), typeof(ProductsViewer));
        
        public ObservableCollection<Category> VisualCategories
        {
            get => (ObservableCollection<Category>)GetValue(VisualCategoriesProperty);
            set => SetValue(VisualCategoriesProperty, value);
        }

        public static readonly DependencyProperty VisualCategoriesProperty =
            DependencyProperty.Register("VisualCategories", typeof(ObservableCollection<Category>), typeof(ProductsViewer));
        
        public ObservableCollection<Category> VisualSubCategories
        {
            get => (ObservableCollection<Category>)GetValue(VisualSubCategoriesProperty);
            set => SetValue(VisualSubCategoriesProperty, value);
        }

        public static readonly DependencyProperty VisualSubCategoriesProperty =
            DependencyProperty.Register("VisualSubCategories", typeof(ObservableCollection<Category>), typeof(ProductsViewer));

        public Category FavoritesCategory
        {
            get => (Category)GetValue(FavoritesCategoryProperty);
            set => SetValue(FavoritesCategoryProperty, value);
        }

        public static readonly DependencyProperty FavoritesCategoryProperty =
            DependencyProperty.Register("FavoritesCategory", typeof(Category), typeof(ProductsViewer));

        public bool FavoritesIsChecked
        {
            get => (bool)GetValue(FavoritesIsCheckedProperty);
            set => SetValue(FavoritesIsCheckedProperty, value);
        }

        public static readonly DependencyProperty FavoritesIsCheckedProperty =
            DependencyProperty.Register("FavoritesIsChecked", typeof(bool), typeof(ProductsViewer));

        public bool IsAllProducts
        {
            get => (bool)GetValue(ShowAllProductsProperty);
            set
            {
                SetValue(ShowAllProductsProperty, value);
                if (value)
                    ShowAllProducts();
                else
                    HideAllProducts();

                if (AllProductsVisibilityChanged != null)
                    AllProductsVisibilityChanged(this, value);
            }
        }

        public static readonly DependencyProperty ShowAllProductsProperty =
            DependencyProperty.Register("ShowAllProducts", typeof(bool), typeof(ProductsViewer));
        
        #endregion

        private void CategoryButton_Checked(CategoryButton sender, bool e)
        {
            if (sender.State == CategoryButton.States.Category)
            {
                if (lastGroup != null)
                    lastGroup.IsChecked = false;
                (lastGroup = sender).IsChecked = true;
                VisualSubCategories = (sender.DataContext as Category).SubCategories;
                subfamiliesContainer.DataContext = null;

            }
            else if (sender.State == CategoryButton.States.Subcategory || sender.State == CategoryButton.States.Favorites)
            {
                if (lastFamily != null)
                    lastFamily.IsChecked = false;

                (lastFamily = sender).IsChecked = true;
                if ((subfamiliesContainer.DataContext = (sender.DataContext as Category).SubCategories) is IEnumerable)
                {
                    foreach (Category cat in (IEnumerable)subfamiliesContainer.DataContext)
                    {
                        if (cat.Products == null || cat.Products.Count == 0)
                            cat.GetProducts(null, MainPage.CurrentInstance.Stand);
                    }
                }
            }
        }
        
        /// <summary>
        /// Event handler for when a product is added to the cart, the user clicked on a product button
        /// This method will only be called on a mouse_up or touch_up event
        /// </summary>
        /// <param name="arg1">The button that was clicked</param>
        /// <param name="arg2">The product that belongs to that button</param>
        /// <param name="arg3"></param>
        private void ProductButton_Product_Added(ProductButton arg1, Product arg2, int arg3)
        {
            if (Product_Click == null) return;
            if(SelectedSeparator != null) arg2.Separator = SelectedSeparator.Id;
            if (!recent.Products.Contains(arg2))
            {
                if (recent.Products.Count < 10)
                    recent.Products.Add(arg2);
                else if (!recent.Products.Contains(arg2))
                {
                    recent.Products.RemoveAt(0);
                    recent.Products.Add(arg2);
                }
                    
                if (_stand != null)
                    _stand.RecentProducts = recent.Products;
            }
            Product_Click(arg2, arg3);
        }

        private void ShowAllProducts()
        {
            (Resources["showAllProducts"] as Storyboard)?.Begin();
            (Resources["hideProducts"] as Storyboard)?.Begin();
        }

        private void HideAllProducts()
        {
            (Resources["hideAllProducts"] as Storyboard)?.Begin();
            (Resources["showProducts"] as Storyboard)?.Begin();
        }


        public void AnimatedShow(int ms = 0)
        {
            (Resources["show"] as Storyboard).BeginTime = new TimeSpan(0, 0, 0, 0, ms);
            (Resources["show"] as Storyboard)?.Begin();
        }

        public void AnimatedHide(int ms = 0)
        {
            (Resources["hide"] as Storyboard).BeginTime = new TimeSpan(0, 0, 0, 0, ms);
            (Resources["hide"] as Storyboard)?.Begin();
        }

        public void SetData(Stand stand)
        {
            ClearData();
            _stand = stand;
            if (!(stand.Products == null || stand.Products.Count == 0))
                AfterLoadStandProducts(stand);
        }

        public void ResetView()
        {
            if (_stand != null) SetData(_stand);
        }

        private void AfterLoadStandProducts(Stand stand)
        {
            allProductsViewer.DataContext = stand;
            VisualCategories = stand.TopCategories;

            // Favorites
            FavoritesCategory = new Category() { Description = "Favorites".Translate() };
            Category subCategory = new Category() { Description = "Default".Translate() };
            recent = new Category() { Description = "Recents".Translate() };
            FavoritesCategory.SubCategories = new ObservableCollection<Category>() { /*new Category() { Description = "Top 10" },*/ subCategory, recent };
            subCategory.Products = new ObservableCollection<Product>(stand.Products.Cast<Product>()
                .Where(x => x.FavoritesOrder.HasValue).OrderBy(x => x.FavoritesOrder));
            recent.Products = stand.RecentProducts;
            recent.Products.CollectionChanged += (sender, e) => { stand.RecentProducts = recent.Products; };
        }

        public void ClearData()
        {
            FavoritesCategory = null;
            VisualCategories = null;
            VisualSubCategories = null;
            subfamiliesContainer.DataContext = null;
            allProductsViewer.DataContext = null;
        }

        private void category_Loaded(object sender, RoutedEventArgs e)
        {
            var list = VisualCategories;
            var view = list.GetCollectionView();
            var first = view.GetItemAt(0);
            if ((sender as CategoryButton).DataContext.Equals(first))
                CategoryButton_Checked(sender as CategoryButton, true);
        }

        private void subCategory_Loaded(object sender, RoutedEventArgs e)
        {
            var list = VisualSubCategories;
            var view = list.GetCollectionView();
            var first = view.GetItemAt(0);
            if ((sender as CategoryButton).DataContext.Equals(first))
                CategoryButton_Checked(sender as CategoryButton, true);
        }
    }
}