﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Animation;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for TableButton.xaml
    /// </summary>
    public partial class TableButton : UserControl, INotifyPropertyChanged
    {
        #region Variables + Constructor

        private bool _isHover;
        private bool _isAddPressed;

        //static TableButton()
        //{
        //    timer = new Timer();
        //    timer.Interval = 1000;
        //    timer.Elapsed += timer_Elapsed;
        //    timer.Start();
        //}

        public TableButton(Table table)
        {
            InitializeComponent();

            Table = table;

            var binding = new Binding(nameof(IsExpanded))
            {
                Mode = BindingMode.TwoWay,
                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                Source = Table,
            };

            SetBinding(IsExpandedProperty, binding);
        }

        #endregion
        #region Events

        public event Action<TableButton> NewTicket;
        public event Action<TableButton> OpenTable;
        public event Action<TableButton> Expanding;
        public event Action<TableButton> Collapsing;
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
        #region Properties

        public Table Table
        {
            set
            {
                DataContext = value;
                OnPropertyChanged(nameof(Table));
            }
            get { return (Table)DataContext; }
        }

        public bool IsHover
        {
            get { return _isHover; }
            set
            {
                _isHover = value;
                if (_isHover)
                    ToHover();
                else
                    FromHover();
            }
        }

        private bool IsAddPressed
        {
            get { return _isAddPressed; }
            set
            {
                _isAddPressed = value;
                if (IsAddPressed)
                    ToAddPressed();
                else
                    ToAddUnpressed();
            }
        }

        #region MarchElapsedMinutes

        public string MarchElapsedMinutes
        {
            get { return (string)GetValue(MarchElapsedMinutesProperty); }
            set { SetValue(MarchElapsedMinutesProperty, value); }
        }

        public static readonly DependencyProperty MarchElapsedMinutesProperty =
            DependencyProperty.Register(nameof(MarchElapsedMinutes),
                typeof(string), typeof(TableButton), null);

        public bool ShowMarchElapsedMinutes
        {
            get { return (bool)GetValue(ShowMarchElapsedMinutesProperty); }
            set { SetValue(ShowMarchElapsedMinutesProperty, value); }
        }

        public static readonly DependencyProperty ShowMarchElapsedMinutesProperty =
            DependencyProperty.Register(nameof(ShowMarchElapsedMinutes),
                typeof(bool), typeof(TableButton), new PropertyMetadata(false));

        #endregion

        #region IsExpanded

        public bool IsExpanded
        {
            get { return (bool)GetValue(IsExpandedProperty); }
            set { SetValue(IsExpandedProperty, value); }
        }

        public static readonly DependencyProperty IsExpandedProperty =
            DependencyProperty.Register(nameof(IsExpanded),
                typeof(bool), typeof(TableButton),
                new PropertyMetadata(IsExpandedPropertyChanged));

        #endregion

        #region IsPressed

        public bool IsPressed
        {
            get { return (bool)GetValue(IsPressedProperty); }
            set { SetValue(IsPressedProperty, value); }
        }

        public static readonly DependencyProperty IsPressedProperty =
            DependencyProperty.Register(nameof(IsPressed),
                typeof(bool),
                typeof(TableButton));

        #endregion

        #endregion
        #region Methods

        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var table = e.OldValue as Table;
            if (table != null)
                table.PropertyChanged -= OnTablePropertyChanged;

            table = e.NewValue as Table;
            if (table != null)
                table.PropertyChanged += OnTablePropertyChanged;

            UpdateStateVisuals();
        }

        private void OnTablePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(Table.ShowReservations):
                    if (Table.IsBusy && Table.IsReserved)
                    {
                        if (Table.ShowReservations)
                            ShowReservationsImageExpand();
                        else
                            ShowReservationsImageUnexpand();
                    }
                    break;
            }

            UpdateStateVisuals();
        }

        private void UpdateStateVisuals()
        {
            UIThread.Invoke(() =>
            {
                Table table = Table;

                if (table != null && table.IsBusy)
                {
                    if (table.MainTable)
                    {
                        tableLink.Visibility = Visibility.Collapsed;
                        tableTotal.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        tableLink.Visibility = Visibility.Visible;
                        tableTotal.Visibility = Visibility.Collapsed;
                    }

                    tableNamePanel.VerticalAlignment = VerticalAlignment.Top;
                    nameText.FontSize = 13;
                }
                else
                {
                    tableLink.Visibility = Visibility.Collapsed;
                    tableTotal.Visibility = Visibility.Collapsed;

                    if (table != null && table.IsReserved)
                        tableNamePanel.VerticalAlignment = VerticalAlignment.Top;
                    else
                        tableNamePanel.VerticalAlignment = VerticalAlignment.Center;

                    nameText.FontSize = 15;
                }
            });
        }

        private void userContorl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            IsPressed = true;
        }

        private void userControl_MouseLeave(object sender, MouseEventArgs e)
        {
            IsPressed = false;
        }

        private void userControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (IsPressed)
            {
                IsPressed = false;

                // No esta expandida la mesa, entonces la expandimos
                if (!IsExpanded)
                {
                    if (Table.State == Table.States.Ready && !Table.IsReserved)
                        NewTicket?.Invoke(this);
                    else
                    {
                        IsExpanded = true;
                        Table.ShowReservations = Table.State == Table.States.Ready && Table.IsReserved;
                        Expanding?.Invoke(this);
                    }
                }
                else if (Table.IsReserved && !Table.HasBills)
                {
                    Table.ShowReservations = false;
                    NewTicket?.Invoke(this);
                }
                // Esta expandida la mesa, entonces entramos a sus detalles
                else
                    OpenTable?.Invoke(this);
            }
        }

        private void addImage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            IsAddPressed = true;
        }

        private void addImage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (IsAddPressed)
            {
                e.Handled = true;
                IsAddPressed = false;
                NewTicket?.Invoke(this);
            }
        }

        private void addImage_MouseLeave(object sender, MouseEventArgs e)
        {
            IsAddPressed = false;
        }

        private void ImgReservation_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (Table.IsExpanded)
            {
                // No propagar el evento
                e.Handled = true;
                Table.ShowReservations = !Table.ShowReservations;
            }
        }

        private static void IsExpandedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != e.OldValue)
            {
                var obj = (TableButton)d;
                if (obj.IsExpanded)
                    obj.ToExpanded();
                else
                {
                    obj.Collapsing?.Invoke(obj);
                    obj.ToCollapsed();
                    obj.Table.ShowReservations = false;
                }
            }
        }

        private void ToHover()
        {
            ((Storyboard)Resources["toHover"]).Begin();
        }

        private void FromHover()
        {
            ((Storyboard)Resources["fromHover"]).Begin();
        }

        private void ToAddUnpressed()
        {
            ((Storyboard)Resources["toAddUnpressed"]).Begin();
        }

        private void ToAddPressed()
        {
            ((Storyboard)Resources["toAddPressed"]).Begin();
        }

        private void ToCollapsed()
        {
            ((Storyboard)Resources["toCollapsed"]).Begin();
        }

        private void ShowReservationsImageExpand()
        {
            ((Storyboard)Resources["ShowReservationsPressed"]).Begin();
        }

        private void ShowReservationsImageUnexpand()
        {
            ((Storyboard)Resources["ShowReservationsUnpressed"]).Begin();
        }


        private void ToExpanded()
        {
            ((Storyboard)Resources["toExpanded"]).Begin();
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}