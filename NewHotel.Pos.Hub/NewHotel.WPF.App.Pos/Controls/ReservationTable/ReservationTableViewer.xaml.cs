﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Core;
using NewHotel.Pos.Hub.Model.WaitingListStand.Contract;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Localization;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using NewHotel.WPF.Common.Controls.Busy;
using NewHotel.WPF.Common.Notifications;
using NewHotel.WPF.Model.Interface;
using NewHotel.WPF.Model.Model;
using ValidationResult = NewHotel.Contracts.ValidationResult;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for TablesReservationsViewer.xaml
    /// </summary>
    public partial class ReservationTableViewer : UserControl
    {
        #region Variables  

        private TablesViewerRemoteControl _remoteControl;
        private readonly IWindow _window = CwFactory.Resolve<IWindow>();

        #endregion
        #region Constructors

        public ReservationTableViewer()
        {
            InitializeComponent();
            IsVisibleChanged += ReservationTableViewer_IsVisibleChanged;
           
            if(Filter != null)
                Filter.PropertyChanged += Filter_PropertyChanged;
        }

        #endregion
        #region Properties

        #region Stands

        public ObservableCollection<Stand> Stands
        {
            get => (ObservableCollection<Stand>)GetValue(StandsProperty);
            set => SetValue(StandsProperty, value);
        }

        public static readonly DependencyProperty StandsProperty =
            DependencyProperty.Register(nameof(Stands), typeof(ObservableCollection<Stand>), typeof(ReservationTableViewer));

        #endregion
        #region Saloons

        public ObservableCollection<Saloon> Saloons
        {
            get => (ObservableCollection<Saloon>)GetValue(SaloonsProperty);
            set => SetValue(SaloonsProperty, value);
        }

        public static readonly DependencyProperty SaloonsProperty =
            DependencyProperty.Register(nameof(Saloons), typeof(ObservableCollection<Saloon>), typeof(ReservationTableViewer));

        #endregion
        #region Filter

        public TablesReservationFilter Filter
        {
            get => (TablesReservationFilter)GetValue(FilterProperty);
            set 
            {
                if(Filter != null)
                    Filter.PropertyChanged -= Filter_PropertyChanged;
            
                SetValue(FilterProperty, value);
                
                if (Filter != null)
                    Filter.PropertyChanged += Filter_PropertyChanged;
            }
        }

        public static readonly DependencyProperty FilterProperty =
            DependencyProperty.Register(nameof(Filter), typeof(TablesReservationFilter), typeof(ReservationTableViewer));

        public LiteTablesReservation LiteTablesReservationSelected
        {
            get => (LiteTablesReservation)GetValue(LiteTablesReservationSelectedProperty);
            set => SetValue(LiteTablesReservationSelectedProperty, value);
        }

        public static readonly DependencyProperty LiteTablesReservationSelectedProperty =
            DependencyProperty.Register(nameof(LiteTablesReservationSelected), typeof(LiteTablesReservation), typeof(ReservationTableViewer));

        #endregion
        #region LiteTablesReservationRecords

        public ObservableCollection<LiteTablesReservation> LiteTablesReservationRecords
        {
            get => (ObservableCollection<LiteTablesReservation>)GetValue(LiteTablesReservationRecordsProperty);
            set => SetValue(LiteTablesReservationRecordsProperty, value);
        }

        public static readonly DependencyProperty LiteTablesReservationRecordsProperty =
            DependencyProperty.Register(nameof(LiteTablesReservationRecords), typeof(ObservableCollection<LiteTablesReservation>), typeof(ReservationTableViewer));

        #endregion

        #region TimeSlots

        public ObservableCollection<TimeSlot> TimeSlots
        {
            get => (ObservableCollection<TimeSlot>)GetValue(TimeSlotsProperty);
            set => SetValue(TimeSlotsProperty, value);
        }

        public static readonly DependencyProperty TimeSlotsProperty =
            DependencyProperty.Register(nameof(TimeSlots), typeof(ObservableCollection<TimeSlot>), typeof(ReservationTableViewer));

        #endregion
        #region BookingSlots
        public ObservableCollection<BookingSlot> BookingSlots
        {
            get => (ObservableCollection<BookingSlot>)GetValue(BookingSlotsProperty);
            set => SetValue(BookingSlotsProperty, value);
        }

        public static readonly DependencyProperty BookingSlotsProperty =
            DependencyProperty.Register(nameof(BookingSlots), typeof(ObservableCollection<BookingSlot>), typeof(ReservationTableViewer));

        #endregion
        public ITicketInitializer TicketInitializer { get; set; }

        #endregion
        #region Methods

        #region Public

        public void ResetView()
        {
            GoToStartView();
            ReleaseRemoteControl();
        }

        public void BindRemoteControl(TablesViewerRemoteControl remoteControl)
        {
            _remoteControl = remoteControl;
            _remoteControl.Bind(TablesPicker);
            TablesPicker.ZoomToSaloon();
        }

        public void ReleaseRemoteControl()
        {
            _remoteControl.Unbind();
        }

        public async void UpdateData()
        {
            await LoadData();
        }

        public void GoToStartView()
        {
            ShowTablesReservations();
            HideTablesPicker();
        }

        #endregion
        #region Event Handler

        private async void Filter_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(TablesReservationFilter.StandId))
                await LoadData();
        }

        private async void ReservationTableViewer_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (IsVisible)
                await LoadTimeSlotsAsync();
        }

        private async void LoadDataSource_Click(object sender, RoutedEventArgs e)
        {
            await LoadData();
        }

        private void ProcessRervation_Click(object sender, RoutedEventArgs e)
        {
            if (LiteTablesReservationSelected == null)
            {
                NotificationWindow.ShowNotificationWindow("ExpectedPickup".Translate());
                return;
            }
            //ShowTablesPicker();
            //HideTablesReservations();
        }

        private async void TablesPicker_OnSelectionEnd(Table[] tables)
        {
            BusyControl.Instance.Show();
            try
            {
                GoToStartView();

                LiteTablesReservation reservation = LiteTablesReservationSelected;

                Guid reservationTableId = reservation.Id;
                Guid[] tableIds = tables.Select(t => t.Id).ToArray();
                var result = await BusinessProxyRepository.Ticket.CreateEmptyTicketToReservationTableAsync(tableIds, reservationTableId, reservation.SerieName);

                if (result.IsEmpty)
                {
                    var ticket = new Ticket((POSTicketContract) result.Contract);
                    ticket.Table.AddTicket(ticket);

                    if (tables.Length > 1)
                    {
                        // Is a tables group
                        #region Remove reservation of the tables

                        if (reservation.TableGroup != null)
                        {
                            foreach (var table in reservation.TableGroup)
                                table.RemoveReservation(reservation.Id);
                        }

                        #endregion
                    }
                    else reservation.Table?.RemoveReservation(reservation.Id);
                    TicketInitializer.OpenTicket(ticket.Table, ticket);
                }
                else _window.ShowError(result);
            }
            catch (Exception exception)
            {
                _window.ShowError(exception.Message);
            }
            finally
            {
                BusyControl.Instance.Close();
            }
        }

        private void CancelReservationReservation_Click(object sender, RoutedEventArgs e)
        {
            if (LiteTablesReservationSelected == null)
            {
                NotificationWindow.ShowNotificationWindow("ExpectedPickup".Translate());
                return;
            }

            bool OkCancelReservation()
            {
                var reasonPicker = new CancellationReasonPicker();

                bool OkCancellationReason()
                {
                    CancellationReason cancellation = reasonPicker.CancellationReasonSelected;

                    if (cancellation == null)
                    {
                        _window.ShowError("RepickCancellationReason".Translate());
                        return false;
                    }
                    CancelReservation(LiteTablesReservationSelected, cancellation.Id, reasonPicker.Comments);
                    return true;
                }

                MainPage.CurrentInstance.OpenDialog(reasonPicker, OkCancellationReason);

                return false;
            }

            MainPage.CurrentInstance.OpenDialog(new TextBlock { Text = "CancelReservationConfirmation?".Translate(), FontSize = 16 }, OkCancelReservation);
        }

        private void CleanSaloonFilter_OnClick(object sender, RoutedEventArgs e)
        {
            Filter.SaloonId = null;
        }

        private void CleanSlotFilter_OnClick(object sender, RoutedEventArgs e)
        {
            Filter.SlotId = null;
        }

        private void DecInDateFilter_Click(object sender, RoutedEventArgs e)
        {
            Filter.InDate = Filter.InDate?.AddDays(-1);
        }

        private void IncInDateFilter_Click(object sender, RoutedEventArgs e)
        {
            Filter.InDate = Filter.InDate?.AddDays(1);
        }

        private void CleanInDateFilter_Click(object sender, RoutedEventArgs e)
        {
            Filter.InDate = null;
        }

        private void CleanStandFilter_OnClick(object sender, RoutedEventArgs e)
        {
            Filter.StandId = null;
        }
     
        #endregion
        #region Aux

        private async Task LoadTimeSlotsAsync()
        {
            BusyControl.Instance.Show();
            try
            {
                var standId = Filter != null && Filter.StandId != null ? Filter.StandId.Value : Hotel.Instance.CurrentStandCashierContext.Stand.Id;
                var date = Filter != null && Filter.InDate != null ? Filter.InDate.Value : Hotel.Instance.CurrentStandCashierContext.Stand.StandWorkDate;

                var result = await BusinessProxyRepository.Stand.GetStandTimeSlotsAsync(standId, date);
                if (result.IsEmpty)
                {
                    var timeSlots = result.ItemSource.Select(t => new TimeSlot(t));
                    TimeSlots = new ObservableCollection<TimeSlot>(timeSlots);
                }
                else
                    _window.ShowError(result);
            }
            catch (Exception exception)
            {
                _window.ShowError(exception.Message);
            }
            finally
            {
                BusyControl.Instance.Close();
            }
        }

        private async Task LoadData()
        {
            BusyControl.Instance.Show();
            try
            {
                var filterContract = new TablesReservationFilterContract
                {
                    InDate = Filter.InDate,
                    StandId = Filter.StandId,
                    Name = Filter.Name,
                    Phone = Filter.Phone,
                    Email = Filter.Email,
                    KindReservation = Filter.KindReservation,
                    // Por ahora solo las reservas en estado Reserved
                    StateReservation = ReservationState.Reserved,                
                };

                if (Filter.SaloonId.HasValue)
                    filterContract.SaloonIds = [Filter.SaloonId.Value];

                if(Filter.SlotId.HasValue)
                    filterContract.SlotId = Filter.SlotId.Value;

                var result = await BusinessProxyRepository.Stand.GetTableReservationsAsync(filterContract);
                if (result.IsEmpty)
                {
                    var reservations = result.ItemSource.Select(r => new LiteTablesReservation(r));
                    LiteTablesReservationRecords = new ObservableCollection<LiteTablesReservation>(reservations);
                }
                else
                    _window.ShowError(result);
            }
            catch (Exception exception)
            {
                _window.ShowError(exception.Message);
            }
            finally
            {
                BusyControl.Instance.Close();
            }
        }

        private void ShowTablesPicker()
        {
            TablesPicker.Visibility = Visibility.Visible;
            TablesPicker.Saloons = Saloons;
            TablesPicker.DeselectAllTables();

            LiteTablesReservation reservation = LiteTablesReservationSelected;

            TablesPicker.AvailabilityFilter = new AvailabilityFilter
            {
                Reservation = reservation
            };

            TablesPicker.SelectedSaloon = reservation.SaloonId.HasValue
                ? Saloons.FirstOrDefault(x => x.Id == reservation.SaloonId.Value)
                : Saloons.FirstOrDefault();
            Saloon selectedSaloon = TablesPicker.SelectedSaloon;
            if (selectedSaloon != null)
            {
                if (reservation.TableId.HasValue
                    && selectedSaloon.Tables.Any(t => t.Id == reservation.TableId.Value)
                    && reservation.Table.Available(reservation))
                {
                    TablesPicker.SelectTable(reservation.Table);
                    TablesPicker.SelectionMultiple = true;
                    TablesPicker.CanChangeSelection = true;
                }
                else if (reservation.TableGroupId.HasValue
                     && reservation.TableGroup.All(t => selectedSaloon.Tables.Any(x => x.Id == t.Id)))
                {
                    foreach (var table in reservation.TableGroup)
                    {
                        if (table.Available(reservation))
                            TablesPicker.SelectTable(table);
                    }
                    TablesPicker.SelectionMultiple = true;
                    TablesPicker.CanChangeSelection = true;
                }
                else
                {
                    TablesPicker.SelectionMultiple = true;
                    TablesPicker.CanChangeSelection = true;
                }
            }
        }

        private void HideTablesPicker()
        {
            TablesPicker.Visibility = Visibility.Collapsed;
        }

        private void ShowTablesReservations()
        {
            RevervationsTableContainer.Visibility = Visibility.Visible;
        }

        private void HideTablesReservations()
        {
            RevervationsTableContainer.Visibility = Visibility.Collapsed;
        }

        private async void CancelReservation(LiteTablesReservation reservation, Guid cancellationId, string comments)
        {
            BusyControl.Instance.Show();
            try
            {
                ValidationResult result = await BusinessProxyRepository.Stand.CancelTableReservationAsync(reservation.Id, cancellationId, comments);
                if (result.IsEmpty) LiteTablesReservationRecords.Remove(reservation);
                else _window.ShowError(result);
            }
            catch (Exception e)
            {
                _window.ShowError(e.Message);
            }
            finally
            {
                BusyControl.Instance.Close();
            }
        }

        #endregion

        #endregion
    }

    public interface ITicketInitializer
    {
        Ticket CreateNewTicket(Table? table = null, BuyerInfo buyerInfo = null, short paxs = 0);
        Ticket GetNewTicketFromContract(POSTicketContract ticket);
        void OpenTicket(Table table, Ticket ticket);
        void RemoveVisualTicket(ITicket ticket, bool error = false);
    }
}