﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using NewHotel.WPF.App.Pos.Model;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for ReservationTableDetailView.xaml
    /// </summary>
    public partial class ReservationTableDetailView : UserControl, INotifyPropertyChanged
    {
        public ReservationTableDetailView()
        {
            InitializeComponent();
        }

        public LiteTablesReservation Reservation
        {
            get { return (LiteTablesReservation)DataContext; }
            set
            {
                DataContext = value;
                OnPropertyChanged(nameof(Reservation));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
