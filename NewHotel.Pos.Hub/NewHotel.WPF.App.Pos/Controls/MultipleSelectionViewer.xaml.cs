﻿using System.Windows.Controls;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for MultipleSelectionViewer.xaml
    /// </summary>
    public partial class MultipleSelectionViewer : UserControl
    {
        public MultipleSelectionViewer()
        {
            InitializeComponent();
        }
    }
}
