﻿using System;
using System.Windows;
using System.Windows.Controls;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for StorePage.xaml
    /// </summary>
    public partial class StorePage : UserControl
    {
        private TicketViewer ticketViewer = null;

        public StorePage()
        {
            InitializeComponent();

            ticketViewer = Resources["ticketViewer"] as TicketViewer;
        }

        public Ticket CreateNewTicket()
        {
            throw new NotImplementedException();
        }

        public Ticket CurrentTicket
        {
            get { return (Ticket)GetValue(CurrentTicketProperty); }
            set 
            { 
                SetValue(CurrentTicketProperty, value); 
                // Check, esto es provisional
                ticketViewer.DataContext = value;
            }
        }

        public static readonly DependencyProperty CurrentTicketProperty =
            DependencyProperty.Register("CurrentTicket", typeof(Ticket), typeof(StorePage));

        private void cancelBill_Click(object sender, RoutedEventArgs e)
        {
        }

        private void collect_Click(object sender, RoutedEventArgs e)
        {
            mainPage.OpenPopUp();
        }

        private void ProductsViewer_Product_Click(Product product, int qty)
        {
            if (CurrentTicket == null)
                CurrentTicket = CreateNewTicket();
        }     
    }
}