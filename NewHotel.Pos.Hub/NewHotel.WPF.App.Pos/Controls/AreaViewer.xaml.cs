﻿using NewHotel.Pos.IoC;
using NewHotel.Pos.Localization;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Model.Interface;
using System.Windows;
using System.Windows.Controls;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for AreaViewer.xaml
    /// </summary>
    public partial class AreaViewer : UserControl
    {
        public IWindow WindowHelper => CwFactory.Resolve<IWindow>();

        public AreaViewer()
        {
            InitializeComponent();
        }

        private void UseDisplayCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var context = DataContext as Area;
            if(!context.DisplayActivated)
            {
                WindowHelper.ShowError("DisplayDisabled".Translate());          
            
                var checkBox = sender as CheckBox;
                checkBox.IsChecked = false;
            }
        }

    }
}