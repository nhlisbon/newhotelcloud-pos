﻿using NewHotel.WPF.App.Pos.Model;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for DutyPaxsControl.xaml
    /// </summary>
    public partial class DutyPaxsControl : UserControl
    {
        public DutyPaxsControl()
        {
            InitializeComponent();
        }

        public void SetValues(ObservableCollection<Duty> duties)
        {
            Duties = duties;
            dutyComboBox.ItemsSource = duties;
        }

        public ObservableCollection<Duty> Duties
        {
            get => (ObservableCollection<Duty>)GetValue(DutiesProperty);
            set => SetValue(DutiesProperty, value);
        }

        public static readonly DependencyProperty DutiesProperty = DependencyProperty.Register(nameof(Duties), typeof(ObservableCollection<Duty>), typeof(DutyPaxsControl));
    }
}
