﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using NewHotel.Pos.Localization;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Model.Interface;

namespace NewHotel.WPF.App.Pos.Controls;

public partial class PickerAccompanyingProduct : IOpenDialog<List<Product>>
{
    private readonly List<Product> _selectedProducts;

    public PickerAccompanyingProduct(List<Product> products)
    {
        InitializeComponent();

        _selectedProducts = [];

        TitleTextBlock.Text = "ChooseAccompanyingProduct".Translate();
            
        ProductItemsControl.DataContext = products;
    }

    private void CheckBox_Click(object sender, RoutedEventArgs e)
    {
        var chk = (CheckBox)sender;
        if (chk.DataContext is not Product product) return;
       
        var isSelected = _selectedProducts.Contains(product);
        if (isSelected)
        {
            _selectedProducts.Remove(product);
        }
        else
        {
            _selectedProducts.Add(product);
        }
    }

    public List<Product> GetValue()
    {
        return _selectedProducts;
    }
}