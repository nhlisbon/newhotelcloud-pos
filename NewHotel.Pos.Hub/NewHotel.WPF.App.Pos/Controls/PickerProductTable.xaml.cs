﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using NewHotel.WPF.App.Pos.Model;

namespace NewHotel.WPF.App.Pos.Controls
{
    public partial class PickerProductTable
    {
        private readonly Product _product;

        public PickerProductTable(Product product)
        {
            InitializeComponent();

            foreach (var productTable in product.ProductTables)
            {
                productTable.Selected = productTable.SeparatorId == null;
            }

            _product = product;

            TitleTextBlock.Text = _product.Description;
            
            var productTableViewSource = (CollectionViewSource)Resources["ProductTableSeparatorCollectionViewSource"];
            productTableViewSource.Source = _product.ProductTables;
            ProductTablesItemsControl.DataContext = productTableViewSource.View;
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            var chk = (CheckBox)sender;
            if (chk.DataContext is not TableProductModel { Selected: true } tableProduct) return;
            if (tableProduct.SeparatorMaxQuantitySelection == null || !(tableProduct.SeparatorMaxQuantitySelection > 0)) return;
            
            var selectedBySeparatorList = _product.ProductTables.Where(x => x.SeparatorId == tableProduct.SeparatorId && x.Selected).ToList();
            if (selectedBySeparatorList.Count > tableProduct.SeparatorMaxQuantitySelection)
            {
                tableProduct.Selected = false;
            }
            
            // #region Preparation selection
            //
            // bool OkAction()
            // {
            //     WindowHelper.OpenDialog(Resources["PickTableProductsContent"], false, _productBeingAdded, _tableAction, MiddleScreen);
            //     return false;
            // }
            //
            // WindowHelper.OpenDialog(Resources["PickTableProductsPreparations"], false, tableProduct, OkAction, MiddleScreen);
            //
            // #endregion
        }
    }
}