﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.WPF.App.Pos.Controls.Prepaid
{
    public class PrepaidVisualResult
    {
        public string Balance { get; internal set; }
        public string CardNumber { get; internal set; }
        public string Guests { get; internal set; }
        public string ReservationNumber { get; internal set; }
        public string Room { get; internal set; }
    }
}
