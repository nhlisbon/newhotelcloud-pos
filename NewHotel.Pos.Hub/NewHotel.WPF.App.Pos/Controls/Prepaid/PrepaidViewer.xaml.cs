﻿using NewHotel.Contracts.DataProvider;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Hub.Contracts.Common.Contracts;
using NewHotel.Pos.Hub.Model.Session;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Localization;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.App.Pos.Model.WaitingListStand;
using NewHotel.WPF.Model.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Hotel = NewHotel.WPF.App.Pos.Model.Hotel;

namespace NewHotel.WPF.App.Pos.Controls.Prepaid
{
    /// <summary>
    /// Interaction logic for PrepaidViewer.xaml
    /// </summary>
    public partial class PrepaidViewer : UserControl, INotifyPropertyChanged
    {
        public PrepaidViewer()
        {
            InitializeComponent();
            DataContext = this;
            Filter = new PrePaidFilter();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private PrePaidFilter _filter;
        public PrePaidFilter Filter
        {
            get { return _filter; }
            set
            {
                _filter = value;
                OnPropertyChanged(nameof(Filter));
            }
        }

        private bool _isVisualVisible;
        public bool IsVisualVisible
        {
            get { return _isVisualVisible; }
            set
            {
                _isVisualVisible = value;
                OnPropertyChanged(nameof(IsVisualVisible));
            }
        }

        private PrepaidVisualResult _visual;
        public PrepaidVisualResult Visual
        {
            get { return _visual; }
            set
            {
                _visual = value;
                OnPropertyChanged(nameof(Visual));
            }
        }

        private IWindow WindowHelper => CwFactory.Resolve<IWindow>();

        public void Clear()
        {
            Filter = new PrePaidFilter();
            Visual = new PrepaidVisualResult();
            _isVisualVisible = false;
        }

        private async void SearchPrepaid_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(Filter.CardNumber) && string.IsNullOrEmpty(Filter.ReservationNumber) && string.IsNullOrEmpty(Filter.Room))
               WindowHelper.ShowInfo("EnterFieldValues".TranslateMsg(), "Error".TranslateCol());
            else
            {
                var hotelId = Hotel.Instance.Id;
                try
                {
                    WindowHelper.ShowBusyDialog();
                    var operation = await BusinessProxyRepository.Stand.GetPmsReservationAsync(new PmsReservationFilterModel
                    {
                        CardNumber = Filter.CardNumber,
                        RoomNumber = Filter.Room,
                        HotelId = hotelId,
                        ShowAccountBalance = true,
                        ShowAllGuests = false,
                        JustCheckedInReservations = true
                    });
                    if(!operation.HasErrors)
                    {
                        var item = operation.Item;
                        if(item != null) {
                            var balance = item.Balance.HasValue ? (item.Balance.Value * decimal.MinusOne).ToString("F2") : "---";
                            Visual = new PrepaidVisualResult
                            {
                                Balance = balance,
                                CardNumber = item.CardNumbers,
                                Guests = item.Guests,
                                ReservationNumber = item.ReservationNumber,
                                Room = item.Room
                            };
                            IsVisualVisible = true;
                        }
                        else
                            WindowHelper.ShowInfo("NorecordsFound".Translate(), "Error".TranslateCol());
                        
                        WindowHelper.CloseBusyDialog();
                    }
                    else
                    {
                        Console.WriteLine("Error: " + operation.Errors);
                        WindowHelper.CloseBusyDialog();
                        WindowHelper.ShowInfo("NorecordsFound".Translate(), "Error".TranslateCol());
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + ex.Message);
                    WindowHelper.CloseBusyDialog();
                    WindowHelper.ShowInfo("NorecordsFound".Translate(), "Error".TranslateCol());
                }
            }
        }
    }
}
