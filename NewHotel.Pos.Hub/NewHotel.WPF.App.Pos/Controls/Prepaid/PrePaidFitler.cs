﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.WPF.App.Pos.Controls.Prepaid
{
    public class PrePaidFilter
    {
        public string CardNumber { get; set; }
        public string ReservationNumber { get; set; }
        public string Room { get; set; }
    }
}
