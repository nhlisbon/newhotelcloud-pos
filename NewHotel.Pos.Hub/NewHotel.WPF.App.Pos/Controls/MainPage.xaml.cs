﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Threading.Tasks;
using System.Diagnostics;
using NewHotel.Contracts;
using NewHotel.WPF.Core;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using NewHotel.WPF.Common.Controls.Busy;
using NewHotel.WPF.Common.Controls.Buttons;
using NewHotel.WPF.Model;
using NewHotel.Pos.Communication.Cashier;
using NewHotel.Pos.Localization;
using NewHotel.Pos.Notifications;
using NewHotel.WPF.Model.Interface;
using NewHotel.WPF.Model.Model;
using LocalizedSuffix = NewHotel.Pos.Localization.LocalizedSuffix;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : ContentControl
    {
        #region Variables

        private Ticker _ticker;
        private readonly Dictionary<Guid, Action> _actionsIndex = new Dictionary<Guid, Action>();
        private readonly Stack<Action> _closeActions = new Stack<Action>();
        private int _noCloseCount;
        private bool _ribbonIsPressed;

        #endregion
        #region Constructors

        public MainPage()
        {
            InitializeComponent();

            SetAsInstance();

            CashierBaseBusinessProxy.DatabaseSyncErrorEvent += (s, ev) =>
            {
                UIThread.Invoke(() =>
                {
                    //BusyControl.Show("Refreshing Information....");

                    Task.Run(async () =>
                    {
                        var environmentResult = await Hotel.Instance.CurrentStandCashierContext.Stand.LoadEnvironmentAsync();
                        //if (!environmentResult.HasErrors)
                        //    Hotel.Instance.RefreshEnvironment(environmentResult.Item);
                        return environmentResult;
                    })
                     .ContinueWith(t =>
                     {
                         var environmentResult = t.Result;
						 Hotel.Instance.CurrentStandCashierContext.Stand.UpdateEnvironment(environmentResult);
                         Task.Run(Hotel.Instance.CurrentStandCashierContext.Stand.UpdateImages);
						 if (!environmentResult.HasErrors)
							 Hotel.Instance.RefreshEnvironment(environmentResult.Item);

						 //BusyControl.Close();
						 //SyncRequested?.Invoke(null, null);
					 },
                     UIThread.Scheduler);
                });
            };

            CashierBaseBusinessProxy.WorkDateErrorEvent += (s, ev) =>
            {
                UIThread.Invoke(() =>
                {
                    //ASD Work Date Handling
                    var contract = new StandTurnDateContract() { Turn = 1, WorkDate = ev };
                    Hotel.Instance.CurrentStandCashierContext.Stand.UpdateTurnAndDate(contract);
                });
            };

            _closeActions.Push(new Action(delegate
            {
                Failure_Click(null, null);
            }));

            Hotel.HasCommunicationChanged += online => HasCloudCommunication = online;
            Hotel.CheckCommunication();

            SignalRNotifications.Client.StateChanged += (state) =>
            {
                UIThread.InvokeAsync(DispatcherPriority.ApplicationIdle, () =>
                {
                    txtSignalConnecting.Visibility = state.NewState == ConnectionState.Connecting ? Visibility.Visible : Visibility.Collapsed;
                    txtSignalDisconnecting.Visibility = state.NewState == ConnectionState.Reconnecting ? Visibility.Visible : Visibility.Collapsed;
                    txtSignalOffline.Visibility = state.NewState == ConnectionState.Disconnected ? Visibility.Visible : Visibility.Collapsed;
                    txtSignalOnline.Visibility = state.NewState == ConnectionState.Connected ? Visibility.Visible : Visibility.Collapsed;
                });
            };

            var user = Hotel.Instance.CurrentUserModel;
            //TODO: Uncomment the following line when the user stand permissions are correctly implemented
            HasChangeStandPermission = true; //user.HasPermission(Permissions.ChangeOfStand);
        }

        #endregion
        #region Events

        public event EventHandler SyncRequested;
        public event Action<MainPage> AuxiliarPanelExpanded;
        public event Action<MainPage> AuxiliarPanelCollapsed;
        public event Action<MainPage> DataLoaded;
        public event Action<MainPage, LoginControl.ResetMode> Lock_Click;

        #endregion
        #region Properties

        public Func<ButtonBase, bool> OkActionDialog { get; set; }
        public Action<ButtonBase> CancelActionDialog { get; set; }
        public Action<ButtonBase> CloseActionDialog { get; set; }

        #region AuxiliarPanel

        public bool ShowAuxiliarPanel
        {
            get => (bool)GetValue(ShowAuxiliarPanelProperty);
            set
            {
                SetValue(ShowAuxiliarPanelProperty, value);
                if (value)
                    ExpandAuxiliarPanel();
                else CollapseAuxiliarPanel();
            }
        }
        public static readonly DependencyProperty ShowAuxiliarPanelProperty =
            DependencyProperty.Register("ShowAuxiliarPanel", typeof(bool), typeof(MainPage));

        public object AuxiliarPanelContent
        {
            get => (object)GetValue(AuxiliarPanelContentProperty);
            set => SetValue(AuxiliarPanelContentProperty, value);
        }

        public static readonly DependencyProperty AuxiliarPanelContentProperty =
            DependencyProperty.Register("AuxiliarPanelContent", typeof(object), typeof(MainPage));

        private void ExpandAuxiliarPanel()
        {
            (Resources["expandAuxiliarPanel"] as Storyboard).Begin();
            if (!AllowAnimations)
                (Resources["expandAuxiliarPanel"] as Storyboard).SeekAlignedToLastTick(new TimeSpan(0), TimeSeekOrigin.Duration);
        }
        private void CollapseAuxiliarPanel()
        {
            (Resources["collapseAuxiliarPanel"] as Storyboard).Begin();
            if (!AllowAnimations)
                (Resources["collapseAuxiliarPanel"] as Storyboard).SeekAlignedToLastTick(new TimeSpan(0), TimeSeekOrigin.Duration);
        }
        private void expandAuxiliarPanel_Completed(object sender, EventArgs e)
        {
            AuxiliarPanelExpanded?.Invoke(this);
        }
        private void collapseAuxiliarPanel_Completed(object sender, EventArgs e)
        {
            AuxiliarPanelCollapsed?.Invoke(this);
        }

        #endregion

        public static MainPage CurrentInstance { get; set; }

        public BusyControl BusyControl => BusyControl.Instance;

        private bool IsDialogAutoCancel { get; set; }
        internal bool IsAllowClose => _noCloseCount == 0;

        public bool HasCloudCommunication
        {
            get => (bool)GetValue(HasCloudCommunicationProperty);
            set => SetValue(HasCloudCommunicationProperty, value);
        }

        public static readonly DependencyProperty HasCloudCommunicationProperty =
            DependencyProperty.Register("HasCloudCommunication", typeof(bool), typeof(MainPage));

        public bool HasChangeStandPermission
        {
            get => (bool)GetValue(HasChangeStandPermissionProperty);
            set => SetValue(HasChangeStandPermissionProperty, value);
        }

        public static readonly DependencyProperty HasChangeStandPermissionProperty =
            DependencyProperty.Register("HasChangeStandPermission", typeof(bool), typeof(MainPage));

        public bool AllowAnimations { get; set; }

        public Stand Stand => Hotel.Instance.CurrentStandCashierContext.Stand;
        public Cashier Cashier => Hotel.Instance.CurrentStandCashierContext.Cashier;
        public User User => Hotel.Instance.CurrentUserModel;

        public object ActionsPanelContent
        {
            get => (object)GetValue(ActionsPanelContentProperty);
            set => SetValue(ActionsPanelContentProperty, value);
        }

        public static readonly DependencyProperty ActionsPanelContentProperty =
            DependencyProperty.Register("ActionsPanelContent", typeof(object), typeof(MainPage));

        public Duty CurrentDuty
        {
            get => (Duty)GetValue(CurrentDutyProperty);
            set => SetValue(CurrentDutyProperty, value);
        }

        public static readonly DependencyProperty CurrentDutyProperty =
            DependencyProperty.Register("CurrentDuty", typeof(Duty), typeof(MainPage));

        public object MainPanelContent
        {
            get => (object)GetValue(MainPanelContentProperty);
            set => SetValue(MainPanelContentProperty, value);
        }

        public static readonly DependencyProperty MainPanelContentProperty =
            DependencyProperty.Register("MainPanelContent", typeof(object), typeof(MainPage));

        public object PopUpContent
        {
            get => (object)GetValue(PopUpPanelContentProperty);
            set => SetValue(PopUpPanelContentProperty, value);
        }

        public static readonly DependencyProperty PopUpPanelContentProperty =
            DependencyProperty.Register("PopUpContent", typeof(object), typeof(MainPage));

        public object DialogContent
        {
            get => (object)GetValue(DialogPanelContentProperty);
            set => SetValue(DialogPanelContentProperty, value);
        }

        public static readonly DependencyProperty DialogPanelContentProperty =
            DependencyProperty.Register("DialogContent", typeof(object), typeof(MainPage));

        public Visibility ShowPopUp
        {
            get => (Visibility)GetValue(ShowPopUpProperty);
            set => SetValue(ShowPopUpProperty, value);
        }

        public static readonly DependencyProperty ShowPopUpProperty =
            DependencyProperty.Register("ShowPopUp", typeof(Visibility), typeof(MainPage));

        public Visibility ShowDialog
        {
            get => (Visibility)GetValue(ShowDialogProperty);
            set => SetValue(ShowDialogProperty, value);
        }

        public static readonly DependencyProperty ShowDialogProperty =
            DependencyProperty.Register("ShowDialog", typeof(Visibility), typeof(MainPage));

        #endregion
        #region Methods

        public void SetAsInstance()
        {
            CurrentInstance = this;
        }

        public void SetAlarms(IEnumerable<Duty> duties, IEnumerable<HappyHour> happyHours)
        {
            if (_ticker != null)
                _ticker.ResetAllAlarms();
            else
            {
                _ticker = Application.Current.Resources["ticker"] as Ticker;
                _ticker.AlarmRaised += new Action<AlarmInfo, TimeSpan>(ticker_AlarmRaised);
                _ticker.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(Ticker_PropertyChanged);
            }

            foreach (var item in duties)
            {
                _ticker.AddAlarm(new AlarmInfo(AlarmAction.DutyStart, item.Id, item.InitialDate, "ChangeDuty"), item.InitialDate);
                _ticker.AddAlarm(new AlarmInfo(AlarmAction.DutyEnd, item.Id, item.FinalDate, "ChangeDuty"), item.FinalDate);
            }

            foreach (var item in happyHours)
            {
                switch (DateTime.Today.DayOfWeek)
                {
                    case DayOfWeek.Sunday:
                        if (!item.Sunday) continue;
                        break;
                    case DayOfWeek.Monday:
                        if (!item.Monday) continue;
                        break;
                    case DayOfWeek.Tuesday:
                        if (!item.Tuesday) continue;
                        break;
                    case DayOfWeek.Wednesday:
                        if (!item.Wednesday) continue;
                        break;
                    case DayOfWeek.Thursday:
                        if (!item.Thursday) continue;
                        break;
                    case DayOfWeek.Friday:
                        if (!item.Friday) continue;
                        break;
                    case DayOfWeek.Saturday:
                        if (!item.Saturday) continue;
                        break;
                }

                _ticker.AddAlarm(new AlarmInfo(AlarmAction.HappyHourStart, item.Id, item.InitialHour, "ChangeHappyHour"), item.InitialHour);
                _ticker.AddAlarm(new AlarmInfo(AlarmAction.HappyHourEnd, item.Id, item.FinalHour, "ChangeHappyHour"), item.FinalHour);
            }
        }

        public void ClosePopUp()
        {
            var sb = (Storyboard)Resources["hidePopUp"];
            sb.Begin();
        }

        public void OpenPopUp()
        {
            var fe = (FrameworkElement)PopUpContent;
            fe.Visibility = Visibility.Visible;
            var sb = (Storyboard)Resources["showPopUp"];
            sb.Begin();
        }

        private void InitDialog(bool disableClick, object content,
            Func<ButtonBase, bool> okAction, DialogButtons dialogButtons = DialogButtons.OkCancel,
            Action<ButtonBase> cancelAction = null, Action<ButtonBase> closeAction = null)
        {
            dialogContainer.Content = DialogContent = content;
            IsDialogAutoCancel = false;
            OkActionDialog = okAction;
            CancelActionDialog = cancelAction;
            CloseActionDialog = closeAction;
            okButton.DisableClick = disableClick;

            switch (dialogButtons)
            {
                case DialogButtons.OkCancel:
                    okButton.Visibility = Visibility.Visible;
                    cancelButton.Visibility = Visibility.Visible;
                    break;
                case DialogButtons.OkOnly:
                    okButton.Visibility = Visibility.Visible;
                    cancelButton.Visibility = Visibility.Collapsed;
                    break;
                case DialogButtons.CancelOnly:
                    okButton.Visibility = Visibility.Collapsed;
                    cancelButton.Visibility = Visibility.Visible;
                    break;
            }

            dialogPanel.Visibility = Visibility.Visible;
        }

        public Action OpenDialog(bool disableClick, object content, bool autoCancel, object dataContext, Func<ButtonBase, bool> okAction, Point location,
            DialogButtons dialogButtons = DialogButtons.OkCancel, Action<ButtonBase> cancelAction = null, Action<ButtonBase> closeAction = null,
            bool hasMaxHeight = true)
        {
            InitDialog(disableClick, content, okAction, dialogButtons, cancelAction, closeAction);
            //dialogContainer.Content = DialogContent = content;

            if (dataContext != null)
            {
                var fe = (FrameworkElement)content;
                fe.DataContext = dataContext;
            }

            //IsDialogAutoCancel = autoCancel;
            //OkActionDialog = okAction;
            //CancelActionDialog = cancelAction;
            //CloseActionDialog = closeAction;
            //okButton.DisableClick = disableClick;
            Canvas.SetLeft(dialogContainer, Math.Min(Math.Max(location.X - dialogContainer.ActualWidth / 2, 5), ActualWidth - dialogContainer.ActualWidth - 5));
            Canvas.SetTop(dialogContainer, Math.Min(Math.Max(location.Y - dialogContainer.ActualHeight / 2, 5), ActualHeight - dialogContainer.ActualHeight - 5));

            //switch (dialogButtons)
            //{
            //    case DialogButtons.OkCancel:
            //        okButton.Visibility = Visibility.Visible;
            //        cancelButton.Visibility = Visibility.Visible;
            //        break;
            //    case DialogButtons.OkOnly:
            //        okButton.Visibility = Visibility.Visible;
            //        cancelButton.Visibility = Visibility.Collapsed;
            //        break;
            //    case DialogButtons.CancelOnly:
            //        okButton.Visibility = Visibility.Collapsed;
            //        cancelButton.Visibility = Visibility.Visible;
            //        break;
            //}

            //dialogPanel.Visibility = Visibility.Visible;

            return () => { OkDialog_Click(null, null); };
        }

        public Action OpenDialog(bool disableClick, object content,
            Func<ButtonBase, bool> okAction, DialogButtons dialogButtons = DialogButtons.OkCancel,
            Action<ButtonBase> cancelAction = null, Action<ButtonBase> closeAction = null)
        {
            InitDialog(disableClick, content, okAction, dialogButtons, cancelAction, closeAction);
            //dialogContainer.Content = DialogContent = content;
            //IsDialogAutoCancel = false;
            //OkActionDialog = okAction;
            //CancelActionDialog = cancelAction;
            //CloseActionDialog = closeAction;
            //okButton.DisableClick = disableClick;

            //switch (dialogButtons)
            //{
            //    case DialogButtons.OkCancel:
            //        okButton.Visibility = Visibility.Visible;
            //        cancelButton.Visibility = Visibility.Visible;
            //        break;
            //    case DialogButtons.OkOnly:
            //        okButton.Visibility = Visibility.Visible;
            //        cancelButton.Visibility = Visibility.Collapsed;
            //        break;
            //    case DialogButtons.CancelOnly:
            //        okButton.Visibility = Visibility.Collapsed;
            //        cancelButton.Visibility = Visibility.Visible;
            //        break;
            //}

            //dialogPanel.Visibility = Visibility.Visible;

            return () => { OkDialog_Click(null, null); };
        }

        public Action OpenInfoDialog(object content)
        {
            return OpenDialog(false, content, s => true, DialogButtons.OkOnly);
        }

        public Action OpenDialog(object content, bool autoCancel, object dataContext, Func<bool> okAction, Point location,
            DialogButtons dialogButtons = DialogButtons.OkCancel, Action cancelAction = null, Action closeAction = null,
            bool hasMaxHeight = true)
        {
            return OpenDialog(false, content, autoCancel, dataContext, (s) => { return okAction(); }, location, dialogButtons,
                s => { cancelAction?.Invoke(); }, s => { closeAction?.Invoke(); }, hasMaxHeight);
        }

        public Action OpenDialog<T>(IOpenDialog<T> content, bool autoCancel, object dataContext, Func<T,bool> okAction, Point location,
            DialogButtons dialogButtons = DialogButtons.OkCancel, Action cancelAction = null, Action closeAction = null,
            bool hasMaxHeight = true)
        {
            return OpenDialog(false, content, autoCancel, dataContext, (s) => okAction(content.GetValue()), location, dialogButtons,
                s => { cancelAction?.Invoke(); }, s => { closeAction?.Invoke(); }, hasMaxHeight);
        }

        public Action OpenDialog(object content, Func<bool> okAction, DialogButtons dialogButtons = DialogButtons.OkCancel,
            Action cancelAction = null, Action closeAction = null)
        {
            return OpenDialog(false, content, s => { return okAction(); }, dialogButtons,
                s => { cancelAction?.Invoke(); }, s => { closeAction?.Invoke(); });
        }

        public Action OpenDialog(bool disableClick, object content, bool autoCancel, object dataContext,
            Func<ButtonBase, bool> okAction, Point location, Action<ButtonBase> cancelAction)
        {
            return OpenDialog(disableClick, content, autoCancel, dataContext,
                okAction, location, DialogButtons.OkCancel, cancelAction);
        }

        public Action OpenDialog(bool disableClick, object content, bool autoCancel, object dataContext,
            Func<ButtonBase, bool> okAction, Point location, Action<ButtonBase> cancelAction,
            Action<ButtonBase> closeAction)
        {
            return OpenDialog(disableClick, content, autoCancel, dataContext,
                okAction, location, DialogButtons.OkCancel, cancelAction, closeAction);
        }

        public void CloseDialog(ButtonBase bb)
        {
            dialogPanel.Visibility = Visibility.Collapsed;
            CloseActionDialog?.Invoke(bb);
        }

        public Guid PushCloseAction(Action action)
        {
            Focus();
            var ret = Guid.NewGuid();
            _actionsIndex.Add(ret, action);
            _closeActions.Push(action);

            if (_closeActions.Count > 1 && closeButton.IsFirst)
                closeButton.IsFirst = false;

            return ret;
        }

        public Action PopCloseAction(Guid? id = null)
        {
            if (_closeActions.Count > 1)
            {
                Action actionResult = null;
                if (!id.HasValue)
                    actionResult = _closeActions.Pop();
                else
                {
                    if (_actionsIndex.ContainsKey(id.Value))
                    {
                        actionResult = _actionsIndex[id.Value];
                        var aux = new Stack<Action>();
                        while (_closeActions.Peek() != actionResult)
                            aux.Push(_closeActions.Pop());
                        _closeActions.Pop();
                        _actionsIndex.Remove(id.Value);
                        while (aux.Count > 1)
                            _closeActions.Push(aux.Pop());
                    }
                }

                if (_closeActions.Count == 1)
                    closeButton.IsFirst = true;

                return actionResult;
            }

            return _closeActions.Peek();
        }

        private void Ticker_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UIThread.Invoke(() =>
            {
                if (Stand != null)
                {
                    if (e.PropertyName == "Now" && Stand.IsHappyHourTime)
                    {
                        TimeSpan nowSpan, finalSpan;
                        nowSpan = DateTime.Now.TimeOfDay;
                        finalSpan = Stand.CurrentHappyHour.FinalHour.TimeOfDay;
                        if (nowSpan > finalSpan)
                        {
                            var aux = new TimeSpan(23, 59, 59) - nowSpan + finalSpan;
                            Stand.HappyHourCountdown = new DateTime(1, 1, 1, aux.Hours, aux.Minutes, 0).AddMinutes(1);
                        }
                        else
                            Stand.HappyHourCountdown = (Stand.CurrentHappyHour.FinalHour - nowSpan).AddMinutes(1);
                    }
                }
            });
        }

        private void OutSideDialog_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var ab = sender as ActionButton;
            if (ab != null)
            {
                if (IsDialogAutoCancel)
                {
                    CancelActionDialog?.Invoke(ab);
                    CloseDialog(ab);
                }
            }
        }

        private void OutSideArea_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ClosePopUp();
        }

        private void OkDialog_Click(object sender, RoutedEventArgs e)
        {
            var ab = (ActionButton)sender;
            var close = true;
            if (OkActionDialog != null)
                close = OkActionDialog(ab);
            if (close)
                CloseDialog(ab);
        }

        private void CancelDialog_Click(object sender, RoutedEventArgs e)
        {
            var ab = (ActionButton)sender;
            CancelActionDialog?.Invoke(ab);
            CloseDialog(ab);
        }

        private void ctrl_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Close_Click(sender, e);
                e.Handled = true;
            }
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            if (_closeActions.Count > 1)
            {
                var action = PopCloseAction();
                action();
            }
            else
            {
                OpenDialog(new TextBlock() { Text = LocalizationMgr.Translation("DoYouWantToLogout", LocalizedSuffix.Message), FontSize = 16 },
                () =>
                {
                    var action = PopCloseAction();
                    // CloseDialog((ButtonBase)sender);
                    CloseDialog(sender as ButtonBase);
                    action();
                    return true;
                });
            }
        }
         
        private void ChangeStand_Click(object sender, RoutedEventArgs e)
        {
            Lock_Click?.Invoke(this, LoginControl.ResetMode.ChangeStand);
        }

        private void ChangeUser_Click(object sender, RoutedEventArgs e)
        {
            Lock_Click?.Invoke(this, LoginControl.ResetMode.ChangeUser);
        }

        private void LockStand_Click(object sender, RoutedEventArgs e)
        {
            Lock_Click?.Invoke(this, LoginControl.ResetMode.LockStand);
        }
        
        private void LockUser_Click(object sender, RoutedEventArgs e)
        {
            Lock_Click?.Invoke(this, LoginControl.ResetMode.LockUser);
        }
               
        private void Failure_Click(object sender, RoutedEventArgs e)
        {
            Lock_Click?.Invoke(this, LoginControl.ResetMode.Total);
        }

        internal void ProhibitClose()
        {
            _noCloseCount++;
        }

        internal void AllowClose()
        {
            if (_noCloseCount > 0)
                _noCloseCount--;
            if (_noCloseCount == 0)
                CloseButtons.IsEnabled = true;
        }
         
        private void userControl_Loaded(object sender, RoutedEventArgs e)
        {
            busyControl.Child = BusyControl.CreateElementOnWorkerThread();
        }

        private void ticker_AlarmRaised(AlarmInfo arg1, TimeSpan arg2)
        {
            UIThread.Invoke(() =>
            {
                switch (arg1.Action)
                {
                    case AlarmAction.HappyHourStart:
                        Stand.SetHappyHour(arg1.Identifier);
                        break;
                    case AlarmAction.HappyHourEnd:
                        Stand.ClearHappyHour();
                        break;
                    case AlarmAction.DutyStart:
                        if (Hotel.Instance.Duties != null)
                            CurrentDuty = Hotel.Instance.Duties.FirstOrDefault(x => x.Id == arg1.Identifier);
                        break;
                    case AlarmAction.DutyEnd:
                        CurrentDuty = null;
                        break;
                }
            });
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            var fe = (FrameworkElement)PopUpContent;
            fe.Visibility = Visibility.Collapsed;
        }

        private void loginControl_LoginCompleted(LoginControl obj, LoginData loginData)
        {
            // Ocultar la ventana de login
            loginGrid.Visibility = Visibility.Collapsed;

            // 1- Almacenar el estado anterior de la caja
            // 2- Tomar datos de login y crear una nueva instancia de caja
            userControl_Loaded(this, null);
        }

        private void BtnOpenDrawer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GC.Collect();
            }
            catch { }
            OpenDrawer();
        }

        private void RibbonButton_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            _ribbonIsPressed = true;
        }

        private void StackPanel_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (!_ribbonIsPressed)
                lockRibbon.IsChecked = false;

            _ribbonIsPressed = false;
        }

        public void CheckUserWritePermission(Guid userId, Permissions permission, Action<bool, Guid?> callback, bool showLogin = true)
        {
            var userModel = Hotel.Instance.CurrentUserModel;
            
            // Check with currently logged user to avoid communication with server
            if (userModel.Id == userId)
            {
                // User cleared for operation
                if (userModel.HasWritePermission(permission, out string permissionTranslation))
                    callback(true, userId);
                else if (showLogin)
                {
                    QuickLoginWindow.ShowLoginWindow((user) =>
                    {
                        if (user != null)
                        {
                            Tuple<SecurityCode, string> tuple;
                            if (user.Permissions.TryGetValue(permission, out tuple))
                            {
                                var securityCode = tuple.Item1;
                                if (securityCode == SecurityCode.FullAccess || securityCode == SecurityCode.ReadWrite)
                                {
                                    callback(true, user.Id);
                                    return true;
                                }
                            }
                        }
                        
                        callback(false, user?.Id);
                        return false;
                    }, permissionTranslation);
                }
            }
        }

        public void CheckUserWritePermission(Guid userId, Permissions permission, Func<bool, Guid?, Task> callback, bool showLogin = true)
        {
            var userModel = Hotel.Instance.CurrentUserModel;

            // Check with currently logged user to avoid communication with server
            if (userModel.Id == userId)
            {
                // User cleared for operation
                if (userModel.HasWritePermission(permission, out string permissionTranslation))
                    callback(true, userId);
                else if (showLogin)
                {
                    QuickLoginWindow.ShowLoginWindow((user) =>
                    {
                        if (user != null)
                        {
                            Tuple<SecurityCode, string> tuple;
                            if (user.Permissions.TryGetValue(permission, out tuple))
                            {
                                var securityCode = tuple.Item1;
                                if (securityCode == SecurityCode.FullAccess || securityCode == SecurityCode.ReadWrite)
                                {
                                    callback(true, user.Id);
                                    return true;
                                }
                            }
                        }

                        callback(false, user?.Id);
                        return false;
                    }, permissionTranslation);
                }
            }
        }


        internal void OpenDrawer()
        {
            CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.CashDrawer, (isValid, validUser) =>
            {
                if (isValid)
                {
                    var printersConfiguration = Hotel.Instance.CurrentStandCashierContext.Stand.PrintersConfiguration;
                    if (printersConfiguration != null && printersConfiguration.HasDrawer)
                    {
                        if (printersConfiguration.HasSerialPort)
                        {
                            try
                            {
                                var port = new SerialPort(printersConfiguration.SerialPortDescription, printersConfiguration.SerialPortBaudRate,
                                    (Parity)printersConfiguration.SerialPortParity, printersConfiguration.SerialPortDataBit,
                                    (StopBits)printersConfiguration.SerialPortStopBits);

                                port.Open();
                                try
                                {
                                    var escCode = printersConfiguration.GetEscCode();
                                    port.Write(escCode, 0, escCode.Length);
                                }
                                finally
                                {
                                    port.Close();
                                }
                            }
                            catch
                            {
                                ErrorsWindow.ShowErrorsWindow(string.Format("ErrorSerialPort0".Translate(), printersConfiguration.SerialPortDescription));
                            }                          
                        }
                        else
                        {
                            var printer = printersConfiguration.DrawerDescription;
                            var escCode = printersConfiguration.EscCode;
                            if (System.IO.File.Exists("DrawerShell.exe"))
                            {
                                var info = new ProcessStartInfo("DrawerShell.exe", "\"" + printer + "\" " + escCode)
                                { 
                                    CreateNoWindow = true,
                                    UseShellExecute = false
                                };

                                Process.Start(info);
                            }
                        }
                    }
                }
                else
                    ErrorsWindow.ShowErrorsWindow("UserNotAuthorized".Translate());
            }, false);
        }

        private void dialogPanel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                e.Handled = true;
        }

        public void SetData()
        {
            Stand.ChangeContext();

            if (Hotel.Instance.Duties != null)
                CurrentDuty = Hotel.Instance.GetDuties(DateTime.Now.AddSeconds(5)).FirstOrDefault();

            if (Stand.HappyHours == null)
                Stand.HappyHours = new System.Collections.ObjectModel.ObservableCollection<HappyHour>();

            UpdateLayout();

            SetAlarms(Hotel.Instance.Duties, Stand.HappyHours);

            Stand.SetHappyHour();
        }

        internal void DisableAllButtons()
        {
            DrawerBtn.IsEnabled = false;
            CloseButtons.IsEnabled = false;
        }

        internal void EnableAllButtons()
        {
            DrawerBtn.IsEnabled = true;
            CloseButtons.IsEnabled = true;
        }

        #endregion
    }
}
