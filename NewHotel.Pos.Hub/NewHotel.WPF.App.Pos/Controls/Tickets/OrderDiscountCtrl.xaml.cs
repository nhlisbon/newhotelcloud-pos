﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using NewHotel.Pos.Core.Ext;
using NewHotel.Pos.Localization;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common.Controls;
using NewHotel.WPF.Common.Controls.TouchScreen;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for OrderDiscountCtrl.xaml
    /// </summary>
    public partial class OrderDiscountCtrl : INotifyPropertyChanged
    {
        #region Variables

        private bool _allowManualDiscount;

        #endregion
        #region Constructor

        public OrderDiscountCtrl()
        {
            InitializeComponent();
        }

        #endregion
        #region Events

        public event EventHandler<NHPropertyChangedEventArgs> DiscountChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
        #region Properties

        public Order? Order
        {
            get => DataContext as Order;
            set => DataContext = value;
        }

        public Discount Discount => Order?.DiscountModel;

        public bool AllowManualDiscount
        {
            get => _allowManualDiscount;
            set
            {
                _allowManualDiscount = value;
                OnPropertyChanged(nameof(AllowManualDiscount));
            }
        }

        #endregion
        #region Methods

        public void Prepare()
        {
            if (Order != null && !Order.HasDiscount)
                Order.DiscountModel = Discount.CreateNotDiscount();
        }

        public void Release()
        {
            if (Order != null && Order.DiscountModel?.Type.Id == Hotel.Instance.NotDiscountType.Id)
                Order.CleanDiscount();
            Order = null;
            manualDiscountCtrl.EditValue = null;
        }

        private void OrderDiscount_OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            UpdateAllowManualDiscountProperty();
            Prepare();
        }

        private void CmbDiscountTypes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var cbx = (ComboBox)sender;
            var discountType = cbx.SelectedItem as DiscountType;
            if (discountType != null && Order != null)
            {
                if (Order.DiscountType != discountType.Id)
                {
                    var discountModel = Order.DiscountModel;
                    var oldPercent = discountModel?.Percent ?? decimal.Zero;

                    var discount = Discount.Create(discountType);
                    if (!discountType.Fixed)
                        discount.Percent = oldPercent >= discountType.MinValue && oldPercent <= discountType.MaxValue ?
                                           oldPercent : discountType.MinValue;

                    Order.DiscountModel = discount;
                    OnDiscountChanged(oldPercent, discount.Percent);
                }

                UpdateAllowManualDiscountProperty();
            }
        }

        private void PercentDiscountControl_ValueChanged(object sender, NHPropertyChangedEventArgs args)
        {
            if (Order != null)
            {
                manualDiscountCtrl.EditValue = null;

                // Los porcientos de descuentos interactivos, hechos por el usuario, son enteros.
                // Pero en la primera carga de porciento se tiene que permitir porcientos decimales, estos
                // provienen de los decuentos manuales interactivos
                var oldValue = (decimal)args.OldValue;
                if (oldValue > 0)
                {
                    var newValue = (decimal)args.NewValue;
                    var floor = Math.Floor(newValue);
                    if ((newValue - floor) > decimal.Zero)
                    {
                        quantityPercentCtrl.ValueChanged -= PercentDiscountControl_ValueChanged;
                        quantityPercentCtrl.Value = floor;
                        quantityPercentCtrl.ValueChanged += PercentDiscountControl_ValueChanged;
                    }
                }

                Order.DiscountModel = Discount.Create(Order.DiscountModel.Type, quantityPercentCtrl.Value);
                OnDiscountChanged(oldValue, quantityPercentCtrl.Value);
            }
        }

        private void BtnApplyManualDiscount_OnClick(object sender, RoutedEventArgs e)
        {
            quantityPercentCtrl.ValueChanged -= PercentDiscountControl_ValueChanged;

            var price = Order.ProductPriceBeforeDiscount;
            if (price != decimal.Zero)
            {
                var c = manualDiscountCtrl;
                c.EditValue = DiscountValue(c.EditVal ?? decimal.Zero, price);
            }
            else
                ErrorsWindow.ShowErrorsWindow("TotalValueCannotbeZero".Translate(), "Error".Translate());

            quantityPercentCtrl.ValueChanged += PercentDiscountControl_ValueChanged;
        }

        private void UpdateAllowManualDiscountProperty()
        {
            AllowManualDiscount = Order?.DiscountModel != null && !Order.DiscountModel.Type.Fixed;
        }

        private decimal DiscountValue(decimal amount, decimal productPriceBeforeDiscount)
        {
            var percent = productPriceBeforeDiscount.GetPercent(Math.Min(amount, productPriceBeforeDiscount));
            if (Order.DiscountModel != null && Order.DiscountModel.Type != null)
            {
                if (percent < Order.DiscountModel.Type.MinValue)
                    percent = Order.DiscountModel.Type.MinValue;
                else if (percent > Order.DiscountModel.Type.MaxValue)
                    percent = Order.DiscountModel.Type.MaxValue;
            }

            var oldPercent = Order.DiscountModel.Percent;
            Order.DiscountModel = Discount.Create(Order.DiscountModel.Type, percent);

            OnDiscountChanged(oldPercent, Order.DiscountPercent);
            return productPriceBeforeDiscount.GetDiscountValue(percent);
        }

        private void NumericPad_Loaded(object sender, RoutedEventArgs e)
        {
            var ctrl = (TouchScreenFixNumPad)sender;
            ctrl.CurrentControl = manualDiscountCtrl;
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual void OnDiscountChanged(decimal oldValue, decimal newValue)
        {
            DiscountChanged?.Invoke(this,
                new NHPropertyChangedEventArgs(nameof(WPF.Model.Model.Order.DiscountPercent))
                {
                    NewValue = newValue,
                    OldValue = oldValue
                });
        }

        #endregion
    }
}