﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using NewHotel.DataAnnotations;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls.Tickets
{
    public partial class OrderTableLineDetailsViewerInTicket
    {
        #region Variables

        private readonly FrameworkElement _addCondimentsContent;
        // private readonly FrameworkElement _changeSeparatorContext;

        #endregion

        public OrderTableLineDetailsViewerInTicket()
        {
            InitializeComponent();

            _addCondimentsContent = (FrameworkElement)Resources["addCondimentsPanel"];
        }

        #region Events

        public event Action<OrderTableLineDetailsViewerInTicket> Accepted;
        public event Action<OrderTableLineDetailsViewerInTicket> Canceled;
        public event Action<OrderDetailsViewerInTicket> AddDiscountsClick;
        public event Action<OrderDetailsViewerInTicket, Action<bool>> PickAreaRequested;
        public event Action<OrderTableLineDetailsViewerInTicket, Action<bool>> DispatchRequested;
        public event Action<OrderTableLineDetailsViewerInTicket, Action<bool>> VoidDispatchRequested;
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Properties

        public OrderTable OrderTable => DataContext as OrderTable;

        public Order Order;

        #region AllPreparations

        public ObservableCollection<object> AllPreparations
        {
            get => (ObservableCollection<object>)GetValue(AllPreparationsProperty);
            private set => SetValue(AllPreparationsProperty, value);
        }

        public static readonly DependencyProperty AllPreparationsProperty =
            DependencyProperty.Register(nameof(AllPreparations), typeof(ObservableCollection<object>), typeof(OrderTableLineDetailsViewerInTicket));
        
        public bool HasPreparations
        {
            get => (bool)GetValue(HasPreparationsProperty);
            set => SetValue(HasPreparationsProperty, value);
        }

        public static readonly DependencyProperty HasPreparationsProperty =
            DependencyProperty.Register(nameof(HasPreparations), typeof(bool), typeof(OrderTableLineDetailsViewerInTicket), new PropertyMetadata(false));

        #endregion

        #region AllSeparators

        public ObservableCollection<object> AllSeparators
        {
            get => (ObservableCollection<object>)GetValue(AllSeparatorsProperty);
            private set => SetValue(AllSeparatorsProperty, value);
        }

        public static readonly DependencyProperty AllSeparatorsProperty =
            DependencyProperty.Register(nameof(AllSeparators), typeof(ObservableCollection<object>), typeof(OrderTableLineDetailsViewerInTicket));

        #endregion

        #endregion

        #region Public

        public void Update(Order order, OrderTable orderTable)
        {
            DataContext = order.OrderTables.FirstOrDefault(t => t.Id == orderTable.Id);
            ;
            Order = order;

            // Preparations
            AllPreparations = orderTable.Product?.Preparations != null ? new ObservableCollection<object>(orderTable.Product.Preparations) : new ObservableCollection<object>();
            HasPreparations = AllPreparations.Count > 0;
            UpdateLayout();
        }

        #endregion

        #region Event Handlers

        // Send to Kitchen
        private void Kitchen_Click(object sender, RoutedEventArgs e)
        {
            OrderTable.IsSelected = true;
            DispatchRequested?.Invoke(this, success => { });
        }

        // Void Send to Kitchen
        private void Void_Click(object sender, RoutedEventArgs e)
        {
            OrderTable.IsSelected = true;
            VoidDispatchRequested?.Invoke(this, succes => { });
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            (DataContext as OrderTable)?.ApplyChanges();
            Accepted?.Invoke(this);
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            (DataContext as OrderTable)?.DiscardChanges();
            Canceled?.Invoke(this);
        }

        #region Condiments

        private void AddCondiments_Click(object sender, RoutedEventArgs e)
        {
            if (EditorControl.Instance != null)
            {
                _addCondimentsContent.DataContext = this;

                SetCondimentsMultipleSelection();

                if (EditorControl.Instance.ShowRequest(
                        "Condiments",
                        _addCondimentsContent,
                        (Color)App.Current.Resources["panelColor"],
                        () =>
                        {
                            ForceAllEnabled();
                            return true;
                        }, null, false))
                {
                    IsEnabled = false;
                    MainPage.CurrentInstance.ProhibitClose();
                }
            }
        }

        private void Preparation_Checked(object sender, RoutedEventArgs e)
        {
            var cbx = (CheckBox)sender;
            var preparation = cbx.DataContext as Preparation;
            if (preparation != null)
                (DataContext as OrderTable)?.Preparations.Add(preparation);
        }

        private void Preparation_Unchecked(object sender, RoutedEventArgs e)
        {
            var cbx = (CheckBox)sender;
            if (cbx.DataContext is Preparation preparation)
                (DataContext as OrderTable)?.Preparations.Remove(p => p.Id == preparation.Id);
        }

        #endregion

        #region Separator

        private void ChangeSeparator_Click(object sender, RoutedEventArgs e)
        {
            //    if (EditorControl.Instance != null)
            //    {
            //        // clonamos la orden para no cambiar la original hasta no aceptar los cambios
            //        _changeSeparatorContext.DataContext = this;
            //        ClonedOrder = (Order);

            //        _noRaiseSeparatorEvents = true;
            //        try
            //        {
            //            SetSeparatorSelection(ClonedOrder);
            //        }
            //        finally
            //        {
            //            _noRaiseSeparatorEvents = false;
            //        }

            //        if (EditorControl.Instance.ShowRequest(
            //                "Separators",
            //                _changeSeparatorContext,
            //                (Color)App.Current.Resources["panelColor"],
            //                () =>
            //                {
            //                    ForceAllEnabled();
            //                    return true;
            //                }, null, false))
            //        {
            //            IsEnabled = false;
            //            MainPage.CurrentInstance.ProhibitClose();
            //        }
            //    }
        }

        private void Separator_Checked(object sender, RoutedEventArgs e)
        {
            //    if (_noRaiseSeparatorEvents || ClonedOrder == null)
            //        return;

            //    var cbx = (CheckBox)sender;
            //    //cbx.IsEnabled = false;
            //    var separator = cbx.DataContext as Separator;
            //    if (separator != null)
            //    {
            //        ClonedOrder.Separator = separator.Id;
            //        SetSeparatorSelection(ClonedOrder);
            //    }
        }

        private void Separator_Unchecked(object sender, RoutedEventArgs e)
        {
            //    var cbx = (CheckBox)sender;
            //    //cbx.IsEnabled = true;

            //    if (ClonedOrder.Separator.HasValue)
            //    {
            //        var separator = cbx.DataContext as Separator;
            //        if (separator != null && separator.Id.HasValue)
            //        {
            //            if (ClonedOrder.Separator.Value == separator.Id.Value)
            //                ClonedOrder.Separator = null;
            //        }
            //    }
        }

        #endregion

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region Aux

        private void ForceAllEnabled()
        {
            IsEnabled = true;
            MainPage.CurrentInstance.AllowClose();
        }

        private void SetCondimentsMultipleSelection()
        {
            foreach (Preparation preparation in AllPreparations)
                preparation.IsChecked = (DataContext as OrderTable)?.Preparations.Any(p => p.Id == preparation.Id) ?? false;
        }

        private void SetSeparatorSelection(OrderTable order)
        {
            //    foreach (var separator in AllSeparators.Cast<Separator>())
            //        separator.IsChecked = separator.Id == order.Separator;
        }

        #endregion
    }
}