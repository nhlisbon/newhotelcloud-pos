﻿using NewHotel.WPF.App.Pos.Model;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using NewHotel.WPF.App.Pos.Controls.Tickets;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for OrderDetailsViewer.xaml
    /// </summary>
    public partial class OrderDetailsViewer : UserControl
    {
        private OrderViewer prevOrderViewer = null;
        private OrderViewer orderViewer = null;

        public event Action<OrderDetailsViewer> OrderExpanded;
        public event Action<OrderDetailsViewer, OrderViewer> OrderCollapsed;

        public OrderDetailsViewer()
        {
            InitializeComponent();
        }

        public int EditedQuantity
        {
            get { return (int)GetValue(EditedQuantityProperty); }
            set { SetValue(EditedQuantityProperty, value); }
        }

        // Using a DependencyProperty as the backing store for EditedQuantity.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EditedQuantityProperty =
            DependencyProperty.Register("EditedQuantity", typeof(int), typeof(OrderDetailsViewer));

        private void ok_Click(object sender, RoutedEventArgs e)
        {
            Expand();
            DialogResult = true;
        }

        public void ForceCancel()
        {
            Collapse();
            DialogResult = false;
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            ForceCancel();
        }

        public void Expand()
        {
            Visibility = System.Windows.Visibility.Visible;
            EditedQuantity = (int)(DataContext as Order).Quantity;
            (Resources["toExpandedMode"] as Storyboard).Begin();
          
        }

        public void Collapse()
        {
            prevOrderViewer = orderViewer;
            (Resources["toCollapsedMode"] as Storyboard).Begin();
          
        }

        private void add_MouseDown(object sender, RoutedEventArgs e)
        {
            EditedQuantity++;
        }

        private void sub_MouseDown(object sender, RoutedEventArgs e)
        {
            if (EditedQuantity > 0)
                EditedQuantity--;
        }

        public void Update(OrderViewer orderViewer)
        {
            this.orderViewer = orderViewer; 
            DataContext = orderViewer.DataContext;
            Width = orderViewer.ActualWidth;
            var point = orderViewer.TranslatePoint(new Point(0, 0), Parent as UIElement);
            Margin = new Thickness(point.X, point.Y, 0, 0);
        }

        private void toExpandedMode_Completed(object sender, EventArgs e)
        {
            if (OrderExpanded != null)
                OrderExpanded(this);
        }

        private void toCollapsedMode_Completed(object sender, EventArgs e)
        {
            if (OrderCollapsed != null)
                OrderCollapsed(this, prevOrderViewer);
            prevOrderViewer = null;

            Visibility = System.Windows.Visibility.Collapsed;
        }

        public bool DialogResult
        {
            get;
            private set;
        }
    }
}
