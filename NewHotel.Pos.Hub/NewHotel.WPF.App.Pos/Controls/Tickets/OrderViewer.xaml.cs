﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls.Tickets
{
    public sealed partial class OrderViewer : INotifyPropertyChanged
    {
        #region Variables  

        private const bool IsExpanded = false;
        private const double SlidePercent = 0.1;
        
        private bool _splitIsPressed;
        private bool _isRemoving;
        private bool _isSplitting;
        private bool _isTouching;
        private Point _firstPoint;
        private bool _isPressed;

        private readonly Storyboard _toNormalModeStoryboard;
        private readonly Storyboard _toSplitModeStoryboard;
        private readonly Storyboard _toRemoveSplitModeStoryboard;
        private readonly Storyboard _toSelectedStoryboard;
        private readonly Storyboard _toDeselectedStoryboard;

        #endregion
        
        #region Constructor

        public OrderViewer()
        {
            InitializeComponent();
            _toNormalModeStoryboard = (Storyboard)Resources["toNormalMode"];
            _toSplitModeStoryboard = (Storyboard)Resources["toSplitMode"];
            _toRemoveSplitModeStoryboard = (Storyboard)Resources["toRemoveSplitMode"];
            _toSelectedStoryboard = (Storyboard)Resources["toSelected"];
            _toDeselectedStoryboard = (Storyboard)Resources["toDeselected"];
        }

        #endregion
        #region Events

        public event Action<OrderViewer>? RemoveOrderClick;
        public event Action<OrderViewer, decimal, decimal>? SplitOrder;
        public event Action<Order, Action<bool>>? SeparatorChanged;
        public event Action<OrderViewer>? OrderDetailsClick;
        public event Action<OrderViewer>? OrderTableDetailsClick;
        public event PropertyChangedEventHandler? PropertyChanged;

        #endregion
        
        #region Properties

        #region EditedQuantity

        public int EditedQuantity
        {
            get => (int)GetValue(EditedQuantityProperty);
            set => SetValue(EditedQuantityProperty, value);
        }

        public static readonly DependencyProperty EditedQuantityProperty =
            DependencyProperty.Register(nameof(EditedQuantity),
            typeof(int), typeof(OrderViewer));

        #endregion

        #region Order

        public Order Order
        {
            get => (Order)GetValue(OrderProperty);
            set => SetValue(OrderProperty, value);
        }

        public static readonly DependencyProperty OrderProperty =
            DependencyProperty.Register(nameof(Order), typeof(Order),
            typeof(OrderViewer), new PropertyMetadata(OnOrderPropertyChanged));

        private static void OnOrderPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var orderViewer = (OrderViewer)d;
            orderViewer.OnOrderChanged((Order)e.OldValue, (Order)e.NewValue);
        }

        public OrderTable? OrderTableClicked { get; private set; }

        #endregion

        #endregion

        #region Methods

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            var binding = new Binding(nameof(DataContext)) { Source = this, Mode = BindingMode.TwoWay };
            SetBinding(OrderProperty, binding);
        }

        private void OrderViewer_IsSelectedChanged(Order order, bool selected)
        {
               UpdateSelectionView(selected);
        }

        private void Order_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Order.Ticket.HasRoomServiceNumber) return;
            switch (TicketViewer.State)
            {
                case TicketViewer.ActionState.Editing:
                    switch (_isRemoving)
                    {
                        case false when !IsExpanded && !_isSplitting:
                            _isTouching = true;
                            _firstPoint = e.GetPosition(this);
                            break;
                        case true when !_isPressed && !_splitIsPressed:
                            _isTouching = false;
                            _isRemoving = false;
                            _isSplitting = false;
                            BeginToNormalModeAnimation();
                            break;
                    }

                    break;
                case TicketViewer.ActionState.Selecting:
                {
                    if (!Order.IsMenuExpanded)
                    {
                        Order.IsSelected = !Order.IsSelected;
                    }

                    break;
                }
            }
        }

        private void Order_MouseLeave(object sender, MouseEventArgs e)
        {
            _isTouching = false;
        }

        private void Order_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (Order.Ticket.HasRoomServiceNumber) return;
            if (!_isTouching) return;
            if (Math.Abs(_firstPoint.X - e.GetPosition(this).X) > ActualWidth * SlidePercent)
            {
                if (TicketViewer.Instance.IsEditingSeparators)
                {
                    SeparatorChanged?.Invoke(Order, _ =>
                    {
                        Order.HasManualSeparator = !Order.HasManualSeparator;
                    });
                }
                else
                {
                    _isRemoving = true;
                    BeginToRemoveSplitModeAnimation();
                }
            }
            else if (!IsExpanded)
            {
                if (!Order.Ticket.HasRoomServiceNumber)
                    OrderDetailsClick?.Invoke(this);
            }

            _isTouching = false;
        }

        private void splitImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _splitIsPressed = true;
        }

        private void splitImage_MouseLeave(object sender, MouseEventArgs e)
        {
            _splitIsPressed = false;
        }

        private void splitImage_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (_splitIsPressed && Order.Quantity > 1)
            {
                if (Order.Quantity == 2 * Order.Product.Step)
                {
                    // Solo hay una forma de dividar la cantidad de productos 
                    SplitOrder?.Invoke(this, Order.Product.Step, Order.Product.Step);
                    cancelSplit_Click(null, null);
                }
                else
                {
                    // Dejamos que el usuario escoja como dividir la cantidad de productos 
                    // Hallamos el medio truncado por defecto al modulo inferior segun su incremento y cantidad actual de productos
                    var step = Order.Product.Step;
                    splitSlider.LeftValue = (double)(Math.Truncate(Order.Quantity / (2 * step)) * step);
                    BeginToSplitModeAnimation();
                    _isSplitting = true;
                    _isRemoving = false;
                }
            }

            _splitIsPressed = false;
        }

        private void okSplit_Click(object sender, RoutedEventArgs e)
        {
            SplitOrder?.Invoke(this, (decimal)splitSlider.LeftValue, (decimal)splitSlider.RightValue);
            cancelSplit_Click(null, null);
        }

        private void cancelSplit_Click(object? sender, RoutedEventArgs? e)
        {
            _isTouching = false;
            _isRemoving = false;
            _isSplitting = false;
            BeginToNormalModeAnimation();
        }

        private void removeImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _isPressed = true;
        }

        private void removeImage_MouseLeave(object sender, MouseEventArgs e)
        {
            _isPressed = false;
        }

        private void removeImage_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (_isPressed)
                RemoveOrderClick?.Invoke(this);

            _isPressed = false;
        }

        private void BeginToNormalModeAnimation()
        {
            _toNormalModeStoryboard.Begin();
        }

        private void BeginToSplitModeAnimation()
        {
            _toSplitModeStoryboard.Begin();
        }

        private void BeginToRemoveSplitModeAnimation()
        {
            _toRemoveSplitModeStoryboard.Begin();
        }

        private void UpdateSelectionView(bool selected)
        {
            (selected ? _toSelectedStoryboard : _toDeselectedStoryboard).Begin();
        }

        private void OnOrderChanged(Order? oldValue, Order? newValue)
        {
            if (oldValue != null)
                oldValue.IsSelectedChanged -= OrderViewer_IsSelectedChanged;

            if (newValue == null) return;
            IsEnabled = newValue.IsEditable;
            newValue.IsSelectedChanged += OrderViewer_IsSelectedChanged;
            if (newValue.IsTransfer)
                borderBackground.Color = Colors.LightGreen;
            else
            {
                if (!Order.IsMenu)
                {
                    UpdateSelectionView(newValue.IsSelected);
                }
            }

            var orderTableViewSource = (CollectionViewSource)Resources["OrderTableSeparatorCollectionViewSource"];
            orderTableViewSource.Source = newValue.OrderTables;
            OrderTablesItemsControl.DataContext = orderTableViewSource.View;
        }

        #endregion

        private void OrderTableViewer_OnOrderTableDetailsClick(OrderTable? orderTable)
        {
            OrderTableClicked = orderTable;
            OrderTableDetailsClick?.Invoke(this);
        }
    }
}