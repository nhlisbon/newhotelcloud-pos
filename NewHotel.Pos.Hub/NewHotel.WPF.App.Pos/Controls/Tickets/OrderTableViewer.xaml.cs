﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Animation;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls.Tickets
{
    public partial class OrderTableViewer
    {
        public event Action<OrderTable>? OrderTableDetailsClick;

        public OrderTableViewer()
        {
            InitializeComponent();
        }

        private void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            var tableOrder = DataContext as OrderTable;
            if (TicketViewer.State == TicketViewer.ActionState.Selecting)
            {
                if (tableOrder != null)
                {
                    tableOrder.IsSelected = !tableOrder.IsSelected;
                }
            }
            else
            {
                if (tableOrder != null) OrderTableDetailsClick?.Invoke(tableOrder);
            }
            
            e.Handled = true;
        }

        /// <summary>
        ///  Used to avoid event propagation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue != null)
                (e.OldValue as OrderTable)!.IsSelectedChanged -= OnIsSelectedChanged;
            if (e.NewValue != null)
                ((OrderTable)e.NewValue).IsSelectedChanged += OnIsSelectedChanged;
        }

        private void OnIsSelectedChanged(OrderTable orderTable, bool isSelected)
        {
            if (TicketViewer.State == TicketViewer.ActionState.Selecting)
            {
                ((isSelected ? Resources["ToTableOrderSelected"] : Resources["ToTableOrderUnselected"]) as Storyboard)?.Begin();
            }
        }
    }
}