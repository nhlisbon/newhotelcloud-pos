﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using NewHotel.Contracts;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Localization;
using NewHotel.WPF.Common.Controls.Busy;
using NewHotel.WPF.Common.Controls.Buttons;
using NewHotel.Pos.IoC;
using NewHotel.WPF.Model.Interface;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for ClientsPanel.xaml
    /// </summary>
    public partial class ClientsPanel : UserControl
    {
        #region Members

        private ClientFilters _filters;
        private Action _selectAction;
        
        private IWindow WindowHelper => CwFactory.Resolve<IWindow>();

        #endregion
        #region Contructor

        public ClientsPanel(Ticket ticket, Action selectAction)
        {
            InitializeComponent();
            DataContext = ticket;
            
            _selectAction = selectAction;
            IsInside = _selectAction != null;

            var content = Resources["clientsFilter"] as FrameworkElement;
            _filters = new ClientFilters();
            content.DataContext = _filters;
            FilterContent = content;
        }

        #endregion
        #region Properties
        
        public bool IsInside 
        { 
            get => (bool)GetValue(IsOutsideProperty);
            set => SetValue(IsOutsideProperty, value);
        }

        public static readonly DependencyProperty IsOutsideProperty = DependencyProperty.Register(nameof(IsInside), typeof(bool), typeof(ClientsPanel));

        private Ticket ViewModel => DataContext as Ticket;

        #region FilterContent

        public object FilterContent
        {
            get => (object)GetValue(FilterContentProperty);
            set => SetValue(FilterContentProperty, value);
        }

        public static readonly DependencyProperty FilterContentProperty = DependencyProperty.Register(nameof(FilterContent), typeof(object), typeof(ClientsPanel));

        #endregion

        #endregion
        #region Public Methods

        public async void Search(string room, string fullName, Guid? clientId = null)
        {
            await RunAsync(LoadData, ViewModel, room, fullName, clientId);
        }

        #endregion
        #region Private Methods

        private async Task RunAsync(Func<Ticket, string, string, Guid?, Task> action, Ticket ticket, string room, string fullName, Guid? clientId)
        {
            BusyControl.Instance.Show("~Loading~...".Translate());

            try
            {
                await action(ticket, room, fullName, clientId);
            }
            catch (Exception ex)
            {
                ErrorsWindow.ShowErrorsWindow(ex.Message);
            }
            finally
            {
                BusyControl.Instance.Close();
            }
        }

        private async Task LoadData(Ticket ticket, string room, string fullName, Guid? clientId)
        {
            if (ticket != null)
            {
                ticket.AccountRecords = new ObservableCollection<object>();
                _filters.View = ticket.AccountRecords.GetCollectionView();
                grid.SelectedItem = null;

                var result = await BusinessProxyRepository.Stand.GetClientInstallationsAsync(false, room, fullName, clientId);
                if (result.IsEmpty)
                {
                    var items = result.ItemSource;
                    ticket.AccountRecords = new ObservableCollection<object>(items);

                    grid.GenerateColumns("Pos",
                        new GridColumnDefinition("Name", "Name".Translate(), 150, null),
                        new GridColumnDefinition("Gender", "Gender".Translate(), 70, null),
                        new GridColumnDefinition("Country", "Country".Translate(), 80, null),
                        new GridColumnDefinition("Reservation", "Reservation".Translate(), 80, null),
                        new GridColumnDefinition("Room", "Room".Translate(), 60, null)
                    );

                    _filters.View = ticket.AccountRecords.GetCollectionView();

                    if (clientId.HasValue && items.Count > 0)
                        grid.SelectedItem = items.First();
                }
                else
                    ErrorsWindow.ShowErrorWindows(result);
            }
        }

        private void LoadDataSource_Click(object sender, RoutedEventArgs e)
        {
            Search(_filters.Room, _filters.Name);
        }

        private async void Select_OnClick(object sender, RoutedEventArgs e)
        {
            var ticket = ViewModel;
            if (ticket != null && ticket.Paxs > 0)
            {
                if (!string.IsNullOrEmpty(clientText.Text))
                {
                    POSClientByPositionContract clientByPosition;

                    var record = grid.SelectedItem as ClientInstallationRecord;
                    if (record != null)
                    {
                        clientByPosition = ticket.ClientsByPosition.FirstOrDefault(cp => cp.Pax == ticket.Paxs);
                        if (clientByPosition == null)
                        {
                            clientByPosition = new POSClientByPositionContract();
                            clientByPosition.Pax = ticket.Paxs;
                            ticket.ClientsByPosition.Add(clientByPosition);
                        }

                        var clientId = (Guid)record.Id;
                        clientByPosition.ClientId = clientId;
                        clientByPosition.ClientRoom = record.Room;

                        var result = await BusinessProxyRepository.Stand.GetClientInfo(clientId);
                        if (result.IsEmpty)
                        {
                            clientByPosition.Preferences.Clear();
                            clientByPosition.Preferences.AddRange(result.Item.Preferences.Select(p => new POSClientByPositionPreferenceContract(Guid.NewGuid(), p.Id, p.Description)));

                            clientByPosition.Diets.Clear();
                            clientByPosition.Diets.AddRange(result.Item.Diets.Select(p => new POSClientByPositionDietContract(Guid.NewGuid(), p.Id, p.Description)));

                            clientByPosition.Attentions.Clear();
                            clientByPosition.Attentions.AddRange(result.Item.Attentions.Select(p => new POSClientByPositionAttentionContract(Guid.NewGuid(), p.Id, p.Description)));

                            clientByPosition.Allergies.Clear();
                            clientByPosition.Allergies.AddRange(result.Item.Allergies.Select(p => new POSClientByPositionAllergyContract(Guid.NewGuid(), p.Id, p.Description)));
                        }
                        else
                            WindowHelper.ShowError(result.ToString());
                    }
                    else
                    {
                        clientByPosition = new POSClientByPositionContract();
                        clientByPosition.Pax = ticket.Paxs;
                        ticket.ClientsByPosition.Add(clientByPosition);
                    }

                    clientByPosition.ClientName = clientText.Text;
                    _selectAction?.Invoke();
                }
            }
        }

        private void Clear_OnClick(object sender, RoutedEventArgs e)
        {
            var ticket = ViewModel;
            if (ticket != null && ticket.Paxs > 0)
            {
                var clientByPosition = ticket.ClientsByPosition.FirstOrDefault(cp => cp.Pax == ticket.Paxs);
                if (clientByPosition != null)
                {
                    ticket.ClientsByPosition.Remove(clientByPosition);
                    _selectAction?.Invoke();
                }
            }
        }

        private void Client_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems != null)
            {
                if (e.AddedItems.Count > 0)
                {
                    var record = e.AddedItems[0] as ClientInstallationRecord;
                    if (record != null && !string.IsNullOrEmpty(record.Name))
                        clientText.Text = (record.Name.Length > 20 ? record.Name.Substring(0, 20) : record.Name).ToUpper();
                }
                else
                    clientText.Text = string.Empty;
            }
        }

        #endregion
    }
}