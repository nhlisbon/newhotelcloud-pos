﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;
using NewHotel.Pos.Localization;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using NewHotel.WPF.Common.Controls;
using NewHotel.WPF.Common.Controls.Buttons;
using NewHotel.WPF.Common.Controls.TouchScreen;
using NewHotel.WPF.Model.Model;
using Separator = NewHotel.WPF.App.Pos.Model.Separator;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for OrderDetailsViewerInTicket.xaml
    /// </summary>
    public sealed partial class OrderDetailsViewerInTicket : INotifyPropertyChanged
    {
        #region Variables

        private readonly FrameworkElement _addCondimentsContent;
        private readonly FrameworkElement _changeSeparatorContext;
        private bool _noRaisePreparationEvents;
        private bool _noRaiseSeparatorEvents;
        private Order _clonedOrder;

        #endregion

        #region Constructor

        public OrderDetailsViewerInTicket()
        {
            InitializeComponent();

            _addCondimentsContent = (FrameworkElement)Resources["addCondimentsPanel"];
            _changeSeparatorContext = (FrameworkElement)Resources["changeSeparatorPanel"];

            AllSeparators = new ObservableCollection<Separator>(Hotel.Instance.Separators);
        }

        #endregion

        #region Events

        public event Action<OrderDetailsViewerInTicket> Accepted;
        public event Action<OrderDetailsViewerInTicket> Canceled;
        public event Action<OrderDetailsViewerInTicket> AddCondimentsClick;
        public event Action<OrderDetailsViewerInTicket> AddDiscountsClick;
        public event Action<OrderDetailsViewerInTicket, Action<bool>> PickAreaRequested;
        public event Action<OrderDetailsViewerInTicket, Action<bool>> DispatchRequested;
        public event Action<OrderDetailsViewerInTicket> VoidDispatchRequested;
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Properties

        public Order Order => (Order)DataContext;

        public bool IsNotMenu
        {
            get => (bool)GetValue(IsNotMenuProperty);
            set => SetValue(IsNotMenuProperty, value);
        }

        public static readonly DependencyProperty IsNotMenuProperty =
            DependencyProperty.Register(nameof(IsNotMenu), typeof(bool), typeof(OrderDetailsViewerInTicket), new PropertyMetadata(false));

        #region AllPreparations

        public ObservableCollection<object> AllPreparations
        {
            get { return (ObservableCollection<object>)GetValue(AllPreparationsProperty); }
            private set { SetValue(AllPreparationsProperty, value); }
        }

        public static readonly DependencyProperty AllPreparationsProperty =
            DependencyProperty.Register(nameof(AllPreparations), typeof(ObservableCollection<object>), typeof(OrderDetailsViewerInTicket));

        #endregion

        #region AllSeparators

        public ObservableCollection<Separator> AllSeparators
        {
            get => (ObservableCollection<Separator>)GetValue(AllSeparatorsProperty);
            private set => SetValue(AllSeparatorsProperty, value);
        }

        public static readonly DependencyProperty AllSeparatorsProperty =
            DependencyProperty.Register(nameof(AllSeparators), typeof(ObservableCollection<Separator>), typeof(OrderDetailsViewerInTicket));

        #endregion

        #region Allergies

        public string MedicalData
        {
            get => (string)GetValue(MedicalDataProperty);
            private set => SetValue(MedicalDataProperty, value);
        }

        public static readonly DependencyProperty MedicalDataProperty =
            DependencyProperty.Register(nameof(MedicalData), typeof(string), typeof(OrderDetailsViewerInTicket));

        #endregion

        #region ClonedOrder

        private Order ClonedOrder
        {
            get => _clonedOrder;
            set
            {
                _clonedOrder = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #endregion

        #region Methods

        #region Public

        public void Update(Order order , Ticket ticket)
        {
            DataContext = order;
            ClonedOrder = order;

            IsNotMenu = !order.IsMenu;

            // Aqui vamos a deducir cual es el tipo de descuento
            if (order.DiscountModel == null)
            {
                if (order.DiscountType.HasValue)
                {
                    // Tratemos de identificar el DiscountType
                    var discountType = Hotel.Instance.DiscountTypes.FirstOrDefault(dt => dt.Id == order.DiscountType.Value);
                    if (discountType != null)
                    {
                        // Tenemos cargado en el hotel ese tipo de descuento
                        order.DiscountModel = new Discount { Type = discountType, Percent = order.DiscountPercent };
                    }
                }

                if (order.DiscountModel == null)
                {
                    // No encontramos el tipo de descuento apropiado
                    if (order.DiscountPercent > decimal.Zero)
                    {
                        // Pero sin embargo la orden posee un porciento de descuento
                        order.DiscountModel = new Discount { Percent = order.DiscountPercent };
                    }
                }
            }

            quantity.MinValue = Order.Quantity;
            if (Order.IsHappyHour && Order.Product != null && !Order.Product.IsHappyHour)
                quantity.MaxValue = Order.Quantity;
            else
                quantity.MaxValue = null;

            MainPage.CurrentInstance.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.CancelProductLine, (isValid, _) =>
            {
                if (isValid)
                    quantity.MinValue = null;
            }, false);

            // Preparations
            order.Product.LoadPreparations().ContinueWith(t =>
            {
                if (t.Status == TaskStatus.RanToCompletion)
                {
                    //var allPrep = new List<object> { "-----", "[Multiple]" };
                    var allPrep = new List<object>();
                    allPrep.AddRange(order.Product.ProductPreparations);

                    UIThread.Invoke(() =>
                    {
                        AllPreparations = new ObservableCollection<object>(allPrep);
                        UpdateLayout();
                    });
                }
                else if (t.IsFaulted)
                    ErrorsWindow.ShowErrorWindows(t.Exception);
            });

            // Client
            if(order.PaxNumber.HasValue)
            {
                var client = ticket.ClientsByPosition.FirstOrDefault(x => x.Pax == order.PaxNumber);
                if (client != null)
                {
                    MedicalData = "";
                    if (client.Allergies.Count > 0)
                        MedicalData += "Allergies".Translate() + ":\n" + string.Join("\n", client.Allergies.Select(x => x.Description)) + '\n';
                    if (client.Diets.Count > 0)
                        MedicalData += "Diets".Translate() + ":\n" + string.Join("\n", client.Diets.Select(x => x.Description)) + '\n';
                    if (!string.IsNullOrEmpty(client.UncommonAllergies))
                        MedicalData += "UncommonAllergies".Translate() +":\n" + client.UncommonAllergies;
                }
            }
        }

        #endregion

        #region Event Handlers

        private void Quantity_ValueChanged(object arg1, NHPropertyChangedEventArgs arg2)
        {
            var newValue = (decimal)arg2.NewValue;
            NumericPad.TouchScreenText = newValue.ToString(CultureInfo.InvariantCulture);
        }

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        // Send to Kitchen
        private void Kitchen_Click(object sender, RoutedEventArgs e)
        {
            ForceAllDisabled();

            if (Order.IsMenu)
            {
                foreach (var orderTable in Order.OrderTables.Where(ot => ot.NotMarch))
                {
                    orderTable.IsSelected = true;
                }
            }

            DispatchRequested.Invoke(this, AllEnableOrDisable);
        }

        // Void Send to Kitchen
        private void Void_Click(object sender, RoutedEventArgs e)
        {
            ForceAllDisabled();

            if (Order.IsMenu)
            {
                foreach (var orderTable in Order.OrderTables.Where(ot => !ot.NotMarch))
                {
                    orderTable.IsSelected = true;
                }
            }

            VoidDispatchRequested.Invoke(this);

            ForceAllEnabled();
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            Accepted?.Invoke(this);
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            ForceCancel();
        }

        #region Discounts

        private void BtnAddDiscounts_Click(object sender, RoutedEventArgs e)
        {
            MainPage.CurrentInstance.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.TicketsDiscounts, (isValid, validUser) =>
            {
                if (isValid)
                {
                    if (EditorControl.Instance != null)
                    {
                        // Clonar la orden para no cambiar la original hasta aceptar los cambios
                        var oCloned = Order.Clone();

                        var orderDiscountControl = new OrderDiscountCtrl
                        {
                            VerticalAlignment = VerticalAlignment.Center,
                            HorizontalAlignment = HorizontalAlignment.Center,
                            Order = oCloned,
                        };

                        orderDiscountControl.Prepare();

                        if (
                            EditorControl.Instance.ShowRequest(
                                "Discounts",
                                orderDiscountControl,
                                (Color)Application.Current.Resources["panelColor"],
                                () =>
                                {
                                    ForceAllEnabled();
                                    orderDiscountControl.Release();
                                    Order.CopyFrom(oCloned);
                                    return true;
                                },
                                () =>
                                {
                                    orderDiscountControl.Release();
                                    ForceAllEnabled();
                                    return true;
                                }
                            )
                        )
                        {
                            ForceAllDisabled();
                        }
                    }

                    AddDiscountsClick?.Invoke(this);
                }
                else
                    ErrorsWindow.ShowErrorsWindow("UserNotAuthorized".Translate());
            });
        }

        #endregion

        #region Condiments

        private void AddCondiments_Click(object sender, RoutedEventArgs e)
        {
            if (EditorControl.Instance != null)
            {
                // clonamos la orden para no cambiar la original hasta no aceptar los cambios
                _addCondimentsContent.DataContext = this;
                ClonedOrder = (Order);

                SetCondimentsMultipleSelection(ClonedOrder);

                if (EditorControl.Instance.ShowRequest(
                        "Condiments",
                        _addCondimentsContent,
                        (Color)Application.Current.Resources["panelColor"],
                        () =>
                        {
                            ForceAllEnabled();
                            return true;
                        }, null, false))
                {
                    ForceAllDisabled();
                }
            }

            AddCondimentsClick?.Invoke(this);
        }

        private void Preparation_Checked(object sender, RoutedEventArgs e)
        {
            if (_noRaisePreparationEvents || ClonedOrder == null)
                return;

            var cbx = (CheckBox)sender;
            var preparation = cbx.DataContext as Preparation;
            if (preparation != null)
                ClonedOrder.OrderPreparations.Add(preparation);
        }

        private void Preparation_Unchecked(object sender, RoutedEventArgs e)
        {
            var order = DataContext as Order;
            if (_noRaisePreparationEvents || order == null)
                return;

            if (order.Preparations != null)
            {
                var cbx = (CheckBox)sender;
                var preparation = cbx.DataContext as Preparation;
                if (preparation != null)
                    ClonedOrder.OrderPreparations.Remove(p => p.Id == preparation.Id);
            }
        }

        #endregion

        #region Separator

        private void ChangeSeparator_Click(object sender, RoutedEventArgs e)
        {
            if (EditorControl.Instance != null)
            {
                // clonamos la orden para no cambiar la original hasta no aceptar los cambios
                _changeSeparatorContext.DataContext = this;
                ClonedOrder = (Order);

                _noRaiseSeparatorEvents = true;
                try
                {
                    SetSeparatorSelection(ClonedOrder);
                }
                finally
                {
                    _noRaiseSeparatorEvents = false;
                }

                if (EditorControl.Instance.ShowRequest(
                        "Separators",
                        _changeSeparatorContext,
                        (Color)App.Current.Resources["panelColor"],
                        () =>
                        {
                            ForceAllEnabled();
                            return true;
                        }, null, false))
                {
                    ForceAllDisabled();
                }
            }
        }

        private void Separator_Checked(object sender, RoutedEventArgs e)
        {
            if (_noRaiseSeparatorEvents || ClonedOrder == null)
                return;

            var cbx = (CheckBox)sender;
            //cbx.IsEnabled = false;
            var separator = cbx.DataContext as Separator;
            if (separator != null)
            {
                ClonedOrder.Separator = separator.Id;
                SetSeparatorSelection(ClonedOrder);
            }
        }

        private void Separator_Unchecked(object sender, RoutedEventArgs e)
        {
            var cbx = (CheckBox)sender;
            //cbx.IsEnabled = true;

            if (ClonedOrder.Separator.HasValue)
            {
                var separator = cbx.DataContext as Separator;
                if (separator != null && separator.Id.HasValue)
                {
                    if (ClonedOrder.Separator.Value == separator.Id.Value)
                        ClonedOrder.Separator = null;
                }
            }
        }

        #endregion

        private void QuantityNumPad_Loaded(object sender, RoutedEventArgs e)
        {
            var ctrl = (TouchScreenFixNumPad)sender;
            ctrl.CurrentControl = quantity;
        }

        #endregion

        #region Aux

        private void AllEnableOrDisable(bool enable) => IsEnabled = enable;

        private void ForceAllEnabled()
        {
            IsEnabled = true;
            // MainPage.CurrentInstance.EnableAllButtons();
            // MainPage.CurrentInstance.AllowClose();
        }

        private void ForceAllDisabled()
        {
            IsEnabled = false;
            // MainPage.CurrentInstance.DisableAllButtons();
            // MainPage.CurrentInstance.ProhibitClose();
        }

        private void ForceCancel()
        {
            Canceled?.Invoke(this);
        }

        private void SetCondimentsMultipleSelection(Order order)
        {
            _noRaisePreparationEvents = true;
            try
            {
                if (order.Preparations != null)
                {
                    foreach (var preparation in order.Preparations)
                        preparation.IsChecked = order.OrderPreparations.Any(p => p.Id == preparation.Id);
                }
            }
            finally
            {
                _noRaisePreparationEvents = false;
            }
        }

        private void SetSeparatorSelection(Order order)
        {
            foreach (var separator in AllSeparators.Cast<Separator>())
                separator.IsChecked = separator.Id == order.Separator;
        }

        #endregion

        #endregion
    }
}