﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using NewHotel.Contracts;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using NewHotel.WPF.Common.Controls;
using NewHotel.WPF.Core;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Hub.Model.WaitingListStand.Contract;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Localization;
using NewHotel.WPF.App.Pos.Controls.Payments;
using NewHotel.WPF.App.Pos.Controls.Tickets;
using NewHotel.WPF.Model.Interface;
using NewHotel.WPF.Model.Model;
using NewHotel.Pos.Hub.Contracts.Common.Records;
using Separator = NewHotel.WPF.App.Pos.Model.Separator;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for TicketViewer.xaml
    /// </summary>
    public partial class TicketViewer : ITSKControlsKey
    {
        public IWindow WindowManager => CwFactory.Resolve<IWindow>();

        #region Variables

        internal static TicketViewer Instance;

        private static ActionState _state = ActionState.Editing;
        private static DescriptionStates _descriptionState = DescriptionStates.AddDescription;
        private Order _currentOrderDetails;
        private IEnumerable<object> _separators;
        private bool _lockEvents;

        private readonly List<CheckBox> _userCheckBoxes = new List<CheckBox>();
        private readonly Storyboard _hideTotal;
        private readonly Storyboard _showTotal;
        private readonly Storyboard _hideTaxTexts;
        private readonly Storyboard _showTaxTexts;
        private readonly Storyboard _hideDetails;
        private readonly Storyboard _showDetails;
        private readonly Storyboard _hideTableLineDetails;
        private readonly Storyboard _showTableLineDetails;
        private readonly CollectionViewSource _paxSeparatorOrderView;
        private readonly CollectionViewSource _separatorPaxOrderView;

        #endregion

        #region Enums

        public enum DescriptionStates
        {
            AddDescription,
            EditDescription,
            AcceptDescription
        };

        public enum ActionState
        {
            Editing,
            Selecting
        };

        #endregion

        #region Constructor

        public TicketViewer()
        {
            InitializeComponent();
            _hideTotal = (Storyboard)Resources["hideTotal"];
            _showTotal = (Storyboard)Resources["showTotal"];
            _hideTaxTexts = (Storyboard)Resources["hideTaxTexts"];
            _showTaxTexts = (Storyboard)Resources["showTaxTexts"];
            _hideDetails = (Storyboard)Resources["HideDetails"];
            _showDetails = (Storyboard)Resources["ShowDetails"];
            _hideTableLineDetails = (Storyboard)Resources["HideTableLineDetails"];
            _showTableLineDetails = (Storyboard)Resources["ShowTableLineDetails"];
            _paxSeparatorOrderView = (CollectionViewSource)Resources["ordersViewPaxSeparator"];
            _separatorPaxOrderView = (CollectionViewSource)Resources["ordersViewSeparatorPax"];
            SetAsInstance();
        }

        #endregion

        #region Events

        public event Action OrderDetailsOpened;
        public event Action OrderDetailsClosed;
        public event Action<Ticket, Action<bool>> OrderDetailsDescriptionChanged;
        public event Action<Ticket, IBaseRecord?> OrderDetailsAccountChanged;
        public event Action<Ticket, Order, Order, Action<bool>> OrderDetailsAccepted;
        public event Action<TicketViewer, Order, Action<bool>> OrderDetailsPickAreaRequested;

        public event Action<Order, Action<bool>> OrderDetailsDispatchRequested;
        public event Action<Order, Action<bool>> OrderDetailsVoidDispatchRequested;

        public event Action<Order, Action<bool>> OrderTableDetailsDispatchRequested;
        public event Action<Order, Action<bool>> OrderTableDetailsVoidDispatchRequested;

        public event Action<Order, Action<bool>> OrderDetailsSeparatorChangeRequested;
        public event Action<TicketViewer> ClientChangeRequested;

        public event Action<Ticket, Order> CancelOrderRequested;
        public event Action<Ticket, Order, int> SplitOrderRequested;

        public event Action<TicketViewer> CancelMultipleSelection_Click;
        public event Action<TicketViewer> AcceptMultipleSelection_Click;

        public event Action AddProductFromSpaService;

        #endregion

        #region Properties

        public bool IsDetailsOpen { get; private set; }

        public IEnumerable<object> Separators
        {
            get { return _separators; }
            set
            {
                if (_separators != value)
                    _separators = value;
            }
        }

        private DescriptionStates DescriptionState
        {
            get => _descriptionState;
            set
            {
                _descriptionState = value;
                switch (_descriptionState)
                {
                    case DescriptionStates.AddDescription:
                        descBlockGrid.Visibility = Visibility.Collapsed;
                        descBoxGrid.Visibility = Visibility.Collapsed;
                        addDescButton.Visibility = Visibility.Visible;
                        addAccount.Visibility = Visibility.Visible;
                        clearAccount.Visibility = Visibility.Visible;
                        break;
                    case DescriptionStates.EditDescription:
                        descBlockGrid.Visibility = Visibility.Visible;
                        descBoxGrid.Visibility = Visibility.Collapsed;
                        addDescButton.Visibility = Visibility.Collapsed;
                        addAccount.Visibility = Visibility.Visible;
                        clearAccount.Visibility = Visibility.Visible;
                        break;
                    case DescriptionStates.AcceptDescription:
                        descBlockGrid.Visibility = Visibility.Collapsed;
                        descBoxGrid.Visibility = Visibility.Visible;
                        addDescButton.Visibility = Visibility.Collapsed;
                        addAccount.Visibility = Visibility.Visible;
                        clearAccount.Visibility = Visibility.Visible;
                        break;
                }
            }
        }

        #region State

        /// <summary>
        /// Muestra y permite cambiar el estado del ticket
        /// </summary>
        public static ActionState State
        {
            get { return _state; }
            set
            {
                _state = value;
                if (Instance != null)
                {
                    switch (_state)
                    {
                        case ActionState.Editing:
                            (Instance.Resources["showTaxTexts"] as Storyboard).Begin();
                            (Instance.Resources["showTotal"] as Storyboard).Begin();
                            (Instance.Resources["hideButtons"] as Storyboard).Begin();
                            if (Instance.Ticket.Orders.Count > 0)
                            {
                                // Hay ordenes en el ticket
                                foreach (var order in Instance.Ticket.Orders)
                                {
                                    order.IsSelected = false;
                                }
                            }

                            break;
                        case ActionState.Selecting:
                            (Instance.Resources["hideTaxTexts"] as Storyboard).Begin();
                            (Instance.Resources["hideTotal"] as Storyboard).Begin();
                            (Instance.Resources["showButtons"] as Storyboard).Begin();
                            break;
                    }
                }
            }
        }

        public static readonly DependencyProperty StateProperty =
            DependencyProperty.Register(nameof(State), typeof(ActionState), typeof(TicketViewer));

        #endregion
        
        #region 
        
        public string RoomAllowed
        {
            get => (string)GetValue(RoomAllowedProperty);
            set
            {
                if (Ticket.Account == null || Ticket.AllowRoomCredit) value = "";
                else if (Ticket is { Account: not null, AllowRoomCredit: false }) value = "RoomNotAllowed".Translate();
                else value = "";
                SetValue(RoomAllowedProperty, value);
            } 
        }
        
        public static readonly DependencyProperty RoomAllowedProperty =
            DependencyProperty.Register(nameof(RoomAllowed), typeof(Order), typeof(TicketViewer));

        
        #endregion
        
        #region OrderDetails

        public Order OrderDetails
        {
            get => (Order)GetValue(ProductDetailsProperty);
            set => SetValue(ProductDetailsProperty, value);
        }

        public static readonly DependencyProperty ProductDetailsProperty =
            DependencyProperty.Register(nameof(OrderDetails), typeof(Order), typeof(TicketViewer));

        public OrderTable OrderTableDetails
        {
            get => (OrderTable)GetValue(TableProductDetailsProperty);
            set => SetValue(TableProductDetailsProperty, value);
        }

        public static readonly DependencyProperty TableProductDetailsProperty =
            DependencyProperty.Register(nameof(OrderTableDetails), typeof(OrderTable), typeof(TicketViewer));

        #endregion

        #region IsEditingSeparators

        public bool IsEditingSeparators
        {
            get { return (bool)GetValue(IsEditingSeparatorsProperty); }
            set { SetValue(IsEditingSeparatorsProperty, value); }
        }

        public static readonly DependencyProperty IsEditingSeparatorsProperty =
            DependencyProperty.Register(nameof(IsEditingSeparators), typeof(bool), typeof(TicketViewer));

        #endregion

        #region Ticket

        public Ticket Ticket
        {
            get => (Ticket)GetValue(TicketProperty);
            set => SetValue(TicketProperty, value);
        }

        public static readonly DependencyProperty TicketProperty =
            DependencyProperty.Register(nameof(Ticket), typeof(Ticket), typeof(TicketViewer),
                new PropertyMetadata(TicketChanged));

        private static void TicketChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var viewer = (TicketViewer)d;

            if (e.OldValue is Ticket oldTicket)
                oldTicket.DataContextChanged -= viewer.Ticket_DataContextChanged;

            if (e.NewValue is Ticket ticket)
            {
                ticket.DataContextChanged += viewer.Ticket_DataContextChanged;
                viewer.UpdateOrdersView();
                viewer.DescriptionState = string.IsNullOrEmpty(ticket.Description)
                    ? DescriptionStates.AddDescription
                    : DescriptionStates.EditDescription;

                if (ticket.ReservationTableId.HasValue)
                {
                    viewer.EdtDescription.Visibility = Visibility.Hidden;
                    viewer.BtnDescription.Visibility = Visibility.Visible;
                }
                else
                {
                    viewer.EdtDescription.Visibility = Visibility.Visible;
                    viewer.BtnDescription.Visibility = Visibility.Hidden;
                }
            }
        }

        private void Ticket_DataContextChanged(Ticket t)
        {
            UpdateOrdersView();
        }

        private void OrdersView_OnFilter(object sender, FilterEventArgs e)
        {
            e.Accepted = e.Item is Order order && order.IsVisible;
        }

        #endregion

        #region Client by order details

        public string OrderClientName
        {
            get => (string)GetValue(OrderClientNameProperty); 
            set => SetValue(OrderClientNameProperty, value);
        }

        public static readonly DependencyProperty OrderClientNameProperty = DependencyProperty.Register(nameof(OrderClientName), typeof(string), typeof(TicketViewer));

        #endregion

        #endregion

        #region Methods

        public void UpdateOrdersView(bool refresh = false)
        {
            var ordersView = Hotel.Instance.CurrentStandCashierContext.Stand.ProductGrouping switch
            {
                TicketProductGrouping.SeatsSeparators => _paxSeparatorOrderView,
                TicketProductGrouping.SeparatorsSeats => _separatorPaxOrderView,
                _ => null
            };

            if (ordersView != null)
            {
                ordersView.Source = Ticket.Orders;
                var orderItemsCtrl = ticketScroll.Content as ItemsControl;
                if (refresh) 
                    ordersView.View.Refresh();
                if (orderItemsCtrl != null) 
                    orderItemsCtrl.ItemsSource = ordersView.View;
            }

            ticketScroll.ScrollToEnd();
        }

        public void SetAsInstance()
        {
            Instance = this;
        }

        public void SetData(Stand stand)
        {
            Separators = stand.Separators;
        }
        
        private void UserDescription_Clicked(object sender, RoutedEventArgs e)
        {
            bool OkAction()
            {
                UIThread.Invoke(async () =>
                {
                    var result = await BusinessProxyRepository.Ticket.PersistTicketAsync(Ticket.AsContract());
                    if (!result.IsEmpty)
                        ErrorsWindow.ShowErrorWindows(result);
                });

                return true;
            }

            _userCheckBoxes.Clear();

            MainPage.CurrentInstance.OpenDialog(Resources["changeTicketUserContent"], false,
                new ObservableCollection<User>(Hotel.Instance.PreviousLoguedUsers), OkAction,
                new Point(this.ActualWidth / 2, this.ActualHeight / 2),
                DialogButtons.OkOnly);
        }

        private void Client_Clicked(object sender, RoutedEventArgs e)
        {
            ClientChangeRequested?.Invoke(this);
        }

        private void userCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (!_lockEvents)
            {
                _lockEvents = true;

                var ticket = Ticket;
                var cbx = (CheckBox)sender;
                var checkedUser = (User)cbx.DataContext;
                ticket.UserId = checkedUser.Id;
                ticket.UserDescription = checkedUser.Description;

                foreach (var item in _userCheckBoxes)
                {
                    if (!item.Equals(sender))
                        item.IsChecked = false;
                }

                _lockEvents = false;
            }
        }

        private void userCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!_lockEvents)
            {
                _lockEvents = true;
                var cbx = (CheckBox)sender;
                cbx.IsChecked = true;
                _lockEvents = false;
            }
        }

        private void userCheckBox_Loaded(object sender, RoutedEventArgs e)
        {
            _lockEvents = true;
            var cbx = (CheckBox)sender;
            var checkedUser = (User)cbx.DataContext;
            if (checkedUser.Id == Ticket.UserId)
                cbx.IsChecked = true;
            _userCheckBoxes.Add(cbx);
            _lockEvents = false;
        }

        private void OrderDetailsControl_Dispatch(OrderDetailsViewerInTicket ctrl, Action<bool> callback)
        {
            OrderDetailsDispatchRequested.Invoke(ctrl.Order, (succeed) =>
            {
                if (succeed)
                {
                    // Update DataContext of details
                    var order = Ticket.Orders.FirstOrDefault(o => o.Id == ctrl.Order.Id);
                    if (order != null)
                    {
                        _currentOrderDetails = order;
                        OrderDetails = _currentOrderDetails.Clone();
                        ctrl.Update(OrderDetails, Ticket);
                    }
                    callback(true);
                }
                else
                {
                    WindowManager.ShowError("CouldNotSendToKitchenLbl".Translate());
                    callback(true);
                }
            });
            // This happens because you clicked on the cancel button of the permission dialog, and to not block the UI it needs to renable it
            callback(true);
        }

        private void OrderTableLineDetailsControl_OnDispatchRequested(OrderTableLineDetailsViewerInTicket ctrl,
            Action<bool> callback)
        {
            OrderDetailsDispatchRequested?.Invoke(ctrl.Order, (succeed) =>
            {
                if (succeed)
                {
                    // Update Datacontext of details
                    var order = Ticket.Orders.FirstOrDefault(o => o.Id == ctrl.Order.Id);
                    if (order != null)
                    {
                        _currentOrderDetails = order;
                        OrderDetails = _currentOrderDetails.Clone();
                        var tableOrder = order.OrderTables.FirstOrDefault(o => o.Id.Equals(ctrl.OrderTable.Id));
                        ctrl.Update(OrderDetails, tableOrder);
                        callback(true);
                    }
                }
            });
        }

        private void OrderDetailsControl_VoidDispatchRequested(OrderDetailsViewerInTicket ctrl)
        {
            
            OrderDetailsVoidDispatchRequested.Invoke(ctrl.Order, succeed =>
            {
                if (!succeed) return;
                var order = Ticket.Orders.FirstOrDefault(o => o.Id == ctrl.Order.Id);
                if (order == null) return;
                ctrl.DataContext = order;
                OrderDetails = order;
            });
        }

        private void OrderTableLineDetailsControl_OnVoidDispatchRequested(OrderTableLineDetailsViewerInTicket ctrl,
            Action<bool> callback)
        {
            OrderDetailsVoidDispatchRequested?.Invoke(ctrl.Order, (succeed) =>
            {
                if (!succeed) return;

                // Update DataContext of details
                var order = Ticket.Orders.FirstOrDefault(o => o.Id == ctrl.Order.Id);

                if (order == null) return;

                ctrl.DataContext = order;
                OrderDetails = order;
                callback(true);
            });
        }

        private void OrderViewer_SeparatorChanged(Order order, Action<bool> callback)
        {
            OrderDetailsSeparatorChangeRequested?.Invoke(order, callback);
        }

        private void AddAccount_Click(object sender, RoutedEventArgs e)
        {
            var ticket = Ticket;
            var panel = new CreditPanel
            {
                HideAmount = true
            };
            panel.Init(ticket);

            bool OkAction()
            {
                if (panel.Grid.SelectedItem == null) return true;
                ticket.AccountType = panel.CreditType;
                ticket.AccountInstallationId = panel.InstallationId;

                var selectedItem = panel.Grid.SelectedItem;
                switch (ticket.AccountType.Value)
                {
                    case CurrentAccountType.Reservation:
                    {
                        if (selectedItem is ReservationRecord record)
                        {
                            ticket.Account = record.AccountId;
                            ticket.FiscalDocInfoName = record.Guests;
                            ticket.Room = record.Room;
                            ticket.Pension = record.Pension;

                            var accountDescription = "";
                            if (panel.InstallationId.HasValue)
                                accountDescription +=  $"{panel.InstallationDescription})";
                            accountDescription += $"{record.ReservationNumber} ({record.Room})";
                            if(!string.IsNullOrEmpty(record.Pension))
                                accountDescription += $" {record.Pension}";
                            accountDescription += $"\n{record.Guests}";
                            if (!string.IsNullOrEmpty(record.Company))
                                accountDescription += $"\n{record.Company}";
                            ticket.AccountDescription = accountDescription;

                            ticket.AllowRoomCredit = record.AllowCreditPos;
                            if (!ticket.AllowRoomCredit)
                                WindowManager.ShowError("It is not allowed to add a credit for this reservation.", "Info");
                        }
                        break;
                    }
                    case CurrentAccountType.Entities:
                    {
                        if (selectedItem is PmsCompanyRecord record)
                        {
                            ticket.Account = record.AccountId;
                            ticket.AccountDescription = record.CompanyName;
                        }
                        break;
                    }
                    case CurrentAccountType.Groups:
                    {
                        if (selectedItem is ReservationsGroupsRecord record)
                        {
                            ticket.Account = record.AccountId;
                            ticket.AccountDescription = record.Name;
                        }
                        break;
                    }
                    case CurrentAccountType.Control:
                    {
                        if (selectedItem is PmsControlAccountRecord record)
                        {
                            ticket.Account = record.AccountId;
                            ticket.AccountDescription = record.AccountName;
                            ticket.AccountBalance = (record.Balance ?? 0) * -1;
                        }
                        break;
                    }
                    case CurrentAccountType.Client:
                    {
                        if (selectedItem is ClientInstallationRecord record)
                        {
                            ticket.Account = record.AccountId;
                            ticket.AccountDescription = record.Name;
                        }
                        break;
                    }
                    case CurrentAccountType.SpaReservation:
                    {
                        if (selectedItem is SpaReservationSearchFromExternalRecord record)
                        {
                            ticket.Account = record.AccountId;
                            ticket.Room = record.Room;
                            ticket.FiscalDocInfoName = record.Guest;
           
                            var accountDescription = "";
                            if (panel.InstallationId.HasValue)
                                accountDescription += $"{panel.InstallationDescription})";
                            accountDescription += $"{record.ReservationNumber} ({record.Room})";
                            accountDescription += $"\n{record.Guest}";
                            ticket.AccountDescription = accountDescription;
                        }
                        break;
                    }
                }

                OrderDetailsAccountChanged.Invoke(Ticket,selectedItem as IBaseRecord);

                return true;

                //var closeEditor = selectedItem is not SpaReservationSearchFromExternalRecord { ProductId: not null };
                //return closeEditor;
            }

            EditorControl.Instance?.ShowRequest("Account", panel, Colors.Black, OkAction);
        }

        private void ClearAccount_Click(object sender, RoutedEventArgs e)
        {
            Ticket.Account = null;
            Ticket.AccountType = null;
            Ticket.Room = null;
            Ticket.Pension = null;
            Ticket.FiscalDocInfoName = null;
            Ticket.AccountDescription = null;
            Ticket.AccountInstallationId = null;
            Ticket.AllowRoomCredit = true;
            OrderDetailsAccountChanged.Invoke(Ticket, null);
        }

        private void showDetailsStoryboard_Completed(object sender, EventArgs e)
        {
            TicketHeaderContainer.IsHitTestVisible = false;
        }

        private void hideDetailsStoryboard_Completed(object sender, EventArgs e)
        {
            TicketHeaderContainer.IsHitTestVisible = true;
        }

        private async void BtnDescription_Click(object sender, RoutedEventArgs e)
        {
            if (Ticket.ReservationTableId.HasValue)
            {
                MainPage.CurrentInstance.BusyControl.Show();
                try
                {
                    var filterContract = new TablesReservationFilterContract
                    {
                        TableReservationId = Ticket.ReservationTableId
                    };

                    var vis = await BusinessProxyRepository.Stand.GetTableReservationsAsync(filterContract);
                    if (vis.IsEmpty)
                    {
                        var reservation = vis.ItemSource.FirstOrDefault();
                        if (reservation != null)
                        {
                            var details = new ReservationTableDetailView
                            {
                                Reservation = new LiteTablesReservation(reservation)
                            };

                            MainPage.CurrentInstance.OpenInfoDialog(details);
                        }
                    }
                }
                finally
                {
                    MainPage.CurrentInstance.BusyControl.Close();
                }
            }
        }

        private void OrderViewer_RemoveOrderClick(OrderViewer orderViewer)
        {
            CancelOrderRequested?.Invoke(Ticket, orderViewer.DataContext as Order);
        }

        private void OrderViewer_OrderDetailsClick(OrderViewer obj)
        {
            ShowOrderDetails(obj, true);
        }

        private void OrderViewer_OnOrderTableLineDetailsClick(OrderViewer obj)
        {
            ShowOrderDetails(obj, false);
        }

        private void ShowOrderDetails(FrameworkElement orderViewer, bool isMainOrder)
        {
            if (IsDetailsOpen) return;
            IsDetailsOpen = true;
            OrderDetailsOpened.Invoke();

            if (orderViewer.DataContext is not Order order)
                return; // Actualizar el visualizador con un clone del producto que se desea modificar
            // Es un pedido, no un placeholder
            _currentOrderDetails = order;
            OrderDetails = _currentOrderDetails.Clone();
            OrderDetailsControl.Update(OrderDetails, Ticket);
            happyGrid.Visibility = OrderDetails.IsHappyHour ? Visibility.Visible : Visibility.Collapsed;

            _hideTotal.Begin();
            _hideTotal.SkipToFill();
            _hideTaxTexts.Begin();
            _hideTaxTexts.SkipToFill();

            if (isMainOrder)
            {
                _showDetails.Begin();
                _showDetails.SkipToFill();
            }
            else
            {
                var tableOrder = (orderViewer as OrderViewer)?.OrderTableClicked ?? new OrderTable();
                OrderTableDetails = tableOrder;
                OrderTableLineDetailsControl.Update(OrderDetails, tableOrder);
                _showTableLineDetails.Begin();
                _showTableLineDetails.SkipToFill();
            }

            OrderClientName =  Ticket?.ClientsByPosition.FirstOrDefault(x => x.Pax == OrderDetails.PaxNumber)?.ClientName ?? "";
        }

        private void TicketCancelButton_Click(object sender, RoutedEventArgs e)
        {
            CancelMultipleSelection_Click?.Invoke(this);
        }

        private void TicketAcceptButton_Click(object sender, RoutedEventArgs e)
        {
            AcceptMultipleSelection_Click?.Invoke(this);
        }

        private void OrderDetailsControl_Canceled(OrderDetailsViewerInTicket obj)
        {
            CloseDetails();
        }

        private void OrderTableLineDetailsControl_OnCanceled(OrderTableLineDetailsViewerInTicket obj)
        {
            CloseDetails();
        }

        private void OrderDetailsControl_Accepted(OrderDetailsViewerInTicket obj)
        {
            OrderDetailsAccepted?.Invoke(Ticket, _currentOrderDetails, OrderDetails, (succeed) =>
            {
                if (succeed)
                    CloseDetails();
            });
        }

        private void OrderTableLineDetailsControl_OnAccepted(OrderTableLineDetailsViewerInTicket obj)
        {
            var tableOrder = OrderDetails.OrderTables.FirstOrDefault(o => o.Id == (obj.DataContext as OrderTable)?.Id);
            tableOrder = obj.DataContext as OrderTable;
            OrderDetailsAccepted?.Invoke(Ticket, _currentOrderDetails, OrderDetails, (succeed) =>
            {
                if (succeed)
                    CloseDetails();
            });
        }

        public void CloseDetails()
        {
            _showTotal.Begin();
            _showTotal.SkipToFill();
            _showTaxTexts.Begin();
            _showTaxTexts.SkipToFill();
            _hideDetails.Begin();
            _hideDetails.SkipToFill();
            _hideTableLineDetails.Begin();
            _hideTableLineDetails.SkipToFill();

            IsDetailsOpen = false;
            OrderDetailsClosed.Invoke();
        }

        private void addDescription_Click(object sender, RoutedEventArgs e)
        {
            DescriptionState = DescriptionStates.AcceptDescription;
        }

        private void editDescription_Click(object sender, RoutedEventArgs e)
        {
            DescriptionState = DescriptionStates.AcceptDescription;
        }

        private void acceptDescription_Click(object sender, RoutedEventArgs e)
        {
            if (OrderDetailsDescriptionChanged != null)
            {
                OrderDetailsDescriptionChanged((Ticket)DataContext, (succeed) =>
                {
                    if (succeed)
                    {
                        if (!(DataContext is Ticket) || string.IsNullOrEmpty((Ticket).Description))
                            DescriptionState = DescriptionStates.AddDescription;
                        else
                            DescriptionState = DescriptionStates.EditDescription;
                    }
                });
            }
            else
            {
                if (!(DataContext is Ticket) || string.IsNullOrEmpty((Ticket).Description))
                    DescriptionState = DescriptionStates.AddDescription;
                else
                    DescriptionState = DescriptionStates.EditDescription;
            }
        }

        public void RunCommandEnter(object sender, object control)
        {
            acceptDescription_Click(null, null);
        }

        public void RunCommandTab(object sender, object control)
        {
        }

        private void OrderDetailsControl_PickAreaRequested(OrderDetailsViewerInTicket arg1, Action<bool> arg2)
        {
            OrderDetailsPickAreaRequested?.Invoke(this, (Order)OrderDetails, arg2);
        }

        private void groupGrid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (State == ActionState.Selecting)
            {
                var fe = (FrameworkElement)sender;
                var group = fe.DataContext as CollectionViewGroup;
                var selected = group.Items.Cast<Order>().Count(x => x.IsSelected);
                var noSelected = group.Items.Cast<Order>().Count(x => !x.IsSelected);
                foreach (Order order in group.Items)
                    order.IsSelected = (selected == 0 || (selected > noSelected && noSelected != 0));
            }
        }

        private void OrderViewer_SplitOrder(OrderViewer arg1, decimal arg2, decimal arg3)
        {
            SplitOrderRequested?.Invoke(Ticket, arg1.DataContext as Order, (int)arg3);
        }

        #endregion

        private void AddProductFromSpaService_OnClick(object sender, RoutedEventArgs e)
        {
            AddProductFromSpaService?.Invoke();
        }
    }
}