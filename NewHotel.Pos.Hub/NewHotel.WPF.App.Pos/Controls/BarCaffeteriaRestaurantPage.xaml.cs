﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Media;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using DevExpress.XtraPrinting.Native;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using NewHotel.DataAnnotations;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Core;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Hub.Model.Validator;
using NewHotel.Pos.Hub.Model.WaitingListStand.Contract;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Localization;
using NewHotel.Pos.Notifications;
using NewHotel.Pos.Printer;
using NewHotel.Pos.PrinterDocument.Fiscal;
using NewHotel.Pos.PrinterDocument.Fiscal.CuracaoPanama;
using NewHotel.Pos.PrinterDocument.Interfaces;
using NewHotel.WPF.App.Pos.Controls.File;
using NewHotel.WPF.App.Pos.Controls.File.CloseDayTurn;
using NewHotel.WPF.App.Pos.Controls.File.ClosedTickets;
using NewHotel.WPF.App.Pos.Controls.MealsControl;
using NewHotel.WPF.App.Pos.Controls.Payments;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.App.Pos.Model.WaitingListStand;
using NewHotel.WPF.Common;
using NewHotel.WPF.Common.Controls;
using NewHotel.WPF.Common.Controls.Buttons;
using NewHotel.WPF.Core;
using NewHotel.WPF.Model;
using NewHotel.WPF.Model.Extentions;
using NewHotel.WPF.Model.Interface;
using NewHotel.WPF.Model.Model;
using NewHotel.WPF.MVVM;
using Table = NewHotel.WPF.Model.Model.Table;
using ProductRecord = NewHotel.Pos.Hub.Contracts.Common.Records.Products.ProductRecord;
using Separator = NewHotel.WPF.App.Pos.Model.Separator;
using ValidationResult = NewHotel.Contracts.ValidationResult;
using NewHotel.Pos.Hub.Contracts.Handheld.Contracts;
using NewHotel.WPF.Common.Controls.Busy;
using NewHotel.Pos.Hub.Contracts.Common.Contracts;
using NewHotel.WPF.App.Pos.Controls.Prepaid;
using NewHotel.Pos.Hub.Contracts.Fiscal;
using DevExpress.Internal.WinApi.Windows.UI.Notifications;
using DevExpress.Xpf.Editors;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for BarCaffeteriaRestaurantPage.xaml
    /// </summary>
    public partial class BarCaffeteriaRestaurantPage : ITicketInitializer
    {
        #region Enums

        public enum States { Tables, Products, WaitingList, TableReservation, MealsControl, PrePaid, Clients }

        #endregion

        #region Variables

        private Func<Order, bool>? _multiOrderSelectionFilterCondition;
        private Func<OrderTable, bool> _multiOrderTableSelectionFilterCondition;
        private Action _multiSelectionOkAction;
        private Guid _ticketWindowId;
        private string _barCode = string.Empty;
        private bool _enabledBarCodeCapture;
        private short? _currentPaxNumber;

        private readonly PrepaidViewer _prepaidViewer;
        private readonly EditorControl _editorControl;
        private readonly TicketViewer _ticketViewer;
        private readonly ProductsViewer _productsViewer;
        static TablesViewer _tablesViewer;
        private readonly UIElement _ticketViewerContainer;
        private UIElement _ticketActions;
        private UIElement _ticketSeatActions;
        private readonly UIElement _tablesActions;
        private readonly UIElement _overviewContainer;
        private readonly Grid _splitTicketContainer;
        private readonly TablesViewerRemoteControl _tablesViewerRemoteControl;
        
        private readonly WaitingListViewer _waitingListViewer;
        private readonly ReservationTableViewer _tablesReservationsViewer;
        private readonly MealsControlViewer _mealsControlViewer;
        private string _room;
        
        private UIElement _actionsCtrl;

        private readonly PlanPanel _planPanel;
        public bool AllowSelectSeparators => Hotel.Instance.AllowSelectSeparators;
        public bool SkipFillFiscalDataAndPutFinalConsumer =>  Hotel.Instance.SkipFillFiscalDataAndPutFinalConsumer;
        public bool SkipFillFiscalDataAndPutFinalConsumerStandVersion => !MainPage.CurrentInstance.Stand.StandRecord.AskInvoiceData;

        public bool AutoTableAfterAllToKitchen => Hotel.Instance.AutoTableAfterAllToKitchen;
        private readonly Grid _notesContent;

        #endregion

        #region Constructor

        public BarCaffeteriaRestaurantPage()
        {
            InitializeComponent();

            _forPicking = new ObservableCollection<Order>();
            var viewP = _forPicking.GetCollectionView();
            viewP.Filter = x => !(x as Order).AreaId.HasValue;

            _prepaidViewer = Resources["PrepaidViewer"] as PrepaidViewer;
            _waitingListViewer = (WaitingListViewer)Resources["WaitingListStand"];
            _tablesReservationsViewer = (ReservationTableViewer)Resources["TablesReservationsViewer"];
            _mealsControlViewer = (MealsControlViewer)Resources["MealsControlViewer"];
            _tablesReservationsViewer.TicketInitializer = this;

            _editorControl = (EditorControl)Resources["EditorControl"];
            _ticketViewer = (TicketViewer)Resources["TicketViewer"];
            _ticketViewer.ClientChangeRequested += _ => { CreateNewTicketRequest(CurrentTicket); };
            _productsViewer = Resources["productsViewer"] as ProductsViewer;
            _tablesViewer = Resources["tablesViewer"] as TablesViewer;
            _ticketViewerContainer = Resources["ticketViewerContainer"] as UIElement;
            _tablesActions = (UIElement)Resources["TablesActions"];
            _overviewContainer = Resources["overviewContainer"] as UIElement;
            _splitTicketContainer = Resources["splitTicketContainer"] as Grid;
            _tablesViewerRemoteControl = (TablesViewerRemoteControl)Resources["TablesViewerRemoteControl"];

            _planPanel = (PlanPanel)Resources["PlanPanelContent"];
            _notesContent = (Grid)Resources["notesContent"];
            _tablesViewerRemoteControl.Bind(_tablesViewer);

            IsIntegrationActive = MainPage.CurrentInstance.Stand.StandRecord.ActivateIntegration;

            // Inicializa las mesas 
            mainPage.AllowAnimations = false;
            mainPage.ShowAuxiliarPanel = false;

            // Captura de caracteres para código de barra
            EventManager.RegisterClassHandler(typeof(Window),
                Keyboard.KeyUpEvent, new KeyEventHandler(OnKeyUp), true);

        }

        #endregion
        
        #region Events

        public event Action<LoginControl.ResetMode> ChangeUserStandRequested;

        #endregion
        
        #region Properties

        #region Ports

        public ObservableCollection<string> Ports
        {
            get => (ObservableCollection<string>)GetValue(PortsProperty);
            set => SetValue(PortsProperty, value);
        }

        public static readonly DependencyProperty PortsProperty =
            DependencyProperty.Register(nameof(Ports), typeof(ObservableCollection<string>), typeof(BarCaffeteriaRestaurantPage));

        #endregion
        
        #region Version

        public string Version
        {
            get => (string)GetValue(VersionProperty);
            set => SetValue(VersionProperty, value);
        }

        public static readonly DependencyProperty VersionProperty =
            DependencyProperty.Register(nameof(Version), typeof(string), typeof(BarCaffeteriaRestaurantPage));


        #endregion
        
        #region CurrentTicket

        public Ticket CurrentTicket
        {
            get => (Ticket)GetValue(CurrentTicketProperty);
            set => SetValue(CurrentTicketProperty, value);
        }

        public static readonly DependencyProperty CurrentTicketProperty =
            DependencyProperty.Register(nameof(CurrentTicket), typeof(Ticket), typeof(BarCaffeteriaRestaurantPage), new PropertyMetadata(OnCurrentTicketChanged));

        private static async void OnCurrentTicketChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (BarCaffeteriaRestaurantPage)d;
            var ticket = (Ticket)e.NewValue;
            control._ticketViewer.Ticket = ticket;
            control.UpdatePaxButtons(ticket, control.BtnSeat);
            if(!string.IsNullOrEmpty(ticket.CardNumber) && (ticket.AccountBalance is null || ticket.AccountBalance is 0))
            {
                BusyControl.Instance.Show();
                var result = await BusinessProxyRepository.Stand.GetPmsReservationAsync(
                    new PmsReservationFilterModel
                    {
                        HotelId = ticket.AccountInstallationId,
                        RoomNumber = string.IsNullOrEmpty(ticket.Room) ? null : ticket.Room,
                        CardNumber = ticket.CardNumber,
                        ShowAllGuests = true,
                        ShowAccountBalance = true
                    }
                );
                if(!result.HasErrors)
                {
                    ticket.AccountBalance = result.Item.Balance ?? 0;
                }
                BusyControl.Instance.Close();
            }
        }

        #endregion
        
        #region Bar Code Capture

        private void ResetBarCode()
        {
            _barCode = string.Empty;
        }

        private char GetCharFromKey(KeyEventArgs e)
        {
            int key = KeyInterop.VirtualKeyFromKey(e.Key);
            switch (key)
            {
                case 48:
                case 96:
                    return '0';
                case 49:
                case 97:
                    return '1';
                case 50:
                case 98:
                    return '2';
                case 51:
                case 99:
                    return '3';
                case 52:
                case 100:
                    return '4';
                case 53:
                case 101:
                    return '5';
                case 54:
                case 102:
                    return '6';
                case 55:
                case 103:
                    return '7';
                case 56:
                case 104:
                    return '8';
                case 57:
                case 105:
                    return '9';
                case 109:
                    return '-';
                case 110:
                    return '.';
                case 32:
                    return ' ';
                case 111:
                    return '/';
                case 107:
                    return '+';
                case 106:
                    return '*';
                default:
                    if (key >= 65 && key <= 90)
                        return (char)key;
                    break;
            }

            return char.MinValue;
        }

        private void OnKeyUp(object sender, KeyEventArgs e)
        {
            e.Handled = false;
            if (_ticketViewerContainer.Visibility == Visibility.Visible)
            {
                if (!_enabledBarCodeCapture)
                    return;
                if (e.OriginalSource is TextBoxBase)
                    return;

                if (CurrentTicket != null && CurrentTicket.DataContext != null)
                {
                    switch (e.Key)
                    {
                        case Key.LineFeed:
                        case Key.Return:
                        case Key.Tab:
                            if (!string.IsNullOrEmpty(_barCode))
                            {
                                var product = Hotel.Instance.CurrentStandCashierContext.Stand.Products.FirstOrDefault(p => p.BarCode == _barCode);
                                if (product != null)
                                    ProductsViewer_Product_Click(product, 1);
                                else
                                    SystemSounds.Beep.Play();
                                ResetBarCode();
                            }
                            break;
                        default:
                            var ch = GetCharFromKey(e);
                            if (ch != char.MinValue)
                                _barCode += ch;
                            break;
                    }
                }
            }
        }

        #endregion
        
        #region  CloseDayMinDate

        public DateTime? CloseDayMinDate
        {
            get => (DateTime?)GetValue(CloseDayMinDateProperty);
            set => SetValue(CloseDayMinDateProperty, value);
        }

        public static readonly DependencyProperty CloseDayMinDateProperty =
            DependencyProperty.Register(nameof(CloseDayMinDate), typeof(DateTime?), typeof(BarCaffeteriaRestaurantPage));

        #endregion
        
        #region CloseDaySelectedDate

        public DateTime? CloseDaySelectedDate
        {
            get => (DateTime?)GetValue(CloseDaySelectedDateProperty);
            set => SetValue(CloseDaySelectedDateProperty, value);
        }

        public static readonly DependencyProperty CloseDaySelectedDateProperty =
            DependencyProperty.Register(nameof(CloseDaySelectedDate), typeof(DateTime?), typeof(BarCaffeteriaRestaurantPage));


        #endregion
        
        #region IsCloseSeveralDays

        public bool IsCloseSeveralDays
        {
            get => (bool)GetValue(IsCloseSeveralDaysProperty);
            set => SetValue(IsCloseSeveralDaysProperty, value);
        }

        public static readonly DependencyProperty IsCloseSeveralDaysProperty =
            DependencyProperty.Register(nameof(IsCloseSeveralDays), typeof(bool), typeof(BarCaffeteriaRestaurantPage));

        #endregion
        
        #region State

        public States State
        {
            get { return (States)GetValue(StateProperty); }
            set
            {
                States previous = State;
                SetValue(StateProperty, value);
                _enabledBarCodeCapture = false;
                ResetBarCode();

                #region Out

                switch (previous)
                {
                    case States.WaitingList:
                        _waitingListViewer.ResetView();
                        TryUncheckWaitingListBtn();
                        break;
                    case States.TableReservation:
                        _tablesReservationsViewer.ResetView();
                        TryUncheckReservationsTableBtn();
                        break;
                    case States.MealsControl:
                        _mealsControlViewer.ResetView();
                        TryUncheckMealsControlBtn();
                        break;
                    case States.Clients:
                        //_clientsPanel.ResetView();
                        //TryUncheckClientsControlBtn();
                        break;
                }

                #endregion

                #region In

                //ESTO ES PROVISIONAL
                if (State != previous)
                {
                    switch (value)
                    {
                        case States.Tables:
                            {
                                mainPage.PopCloseAction(_ticketWindowId);

                                #if !SHOW_ACCEPTED_AND_RETURNED_TICKETS_IN_AUXILIAR_PANEL
                                    //OSCAR: doesnt work now, lets hide the panel till is required for showing something
                                    mainPage.ShowAuxiliarPanel = false;
                                #endif

                                _waitingListViewer.Visibility = Visibility.Collapsed;
                                _tablesViewer.Visibility = Visibility.Visible;
                                _productsViewer.Visibility = Visibility.Collapsed;
                                _tablesReservationsViewer.Visibility = Visibility.Collapsed;
                                _mealsControlViewer.Visibility = Visibility.Collapsed;
                                HidePrePaidViewer();

                                // Collapse all tables
                                _tablesViewer.LastExpandedTableButton = null;
                                _tablesViewerRemoteControl.Bind(_tablesViewer);

                                _overviewContainer.Visibility = Visibility.Visible;
                                _ticketViewerContainer.Visibility = Visibility.Collapsed;
                                _ticketActions.Visibility = Visibility.Collapsed;
                                _ticketSeatActions.Visibility = Visibility.Collapsed;
                                _tablesActions.Visibility = Visibility.Visible;

                                ResetBarCode();
                            }
                            break;
                        case States.Products:
                            {
                                _ticketWindowId = mainPage.PushCloseAction(async () =>
                                {
                                    State = States.Tables;

                                    if (CurrentTicket.LastModified == DateTime.MinValue)
                                    {
                                        // Solo borrar los tickets no salvados, los otros tienen que quedarse 
                                        RemoveVisualTicket(CurrentTicket);
                                    }
                                    else if (CurrentTicket.HasOrders)
                                        CurrentTicket.ClearPayments();

                                    if (_ticketViewer.IsDetailsOpen) _ticketViewer.CloseDetails();

                                    if (CurrentTicket.LastModified != DateTime.MinValue)
                                        await SaveTicketFromDetailsAsync(CurrentTicket.Id, CurrentTicket.Paxs, null);

                                    // Notify ticket will be in edition mode
                                    // var signalContract = new NotificationContract<Guid>
                                    // {
                                    //     DataContext = CurrentTicket.Id,
                                    //     TokenID = BusinessProxyRepository.Cashier.GetToken()
                                    // };

                                    // SignalRNotifications.Publish("ClosingTicket", Serializer.SerializeJson(signalContract, signalContract.GetType()));
                                });

                                // Mostrar el panel auxiliar solo cuando debemos mostrar el ticket
                                mainPage.ShowAuxiliarPanel = true;

                                _waitingListViewer.Visibility = Visibility.Collapsed;
                                _tablesViewer.Visibility = Visibility.Collapsed;
                                _tablesReservationsViewer.Visibility = Visibility.Collapsed;
                                _mealsControlViewer.Visibility = Visibility.Collapsed;

                                _productsViewer.ResetView();
                                _productsViewer.Visibility = Visibility.Visible;

                                _overviewContainer.Visibility = Visibility.Collapsed;
                                _ticketViewerContainer.Visibility = Visibility.Visible;
                                _ticketActions.Visibility = Visibility.Visible;
                                _ticketSeatActions.Visibility = Visibility.Visible;
                                _tablesActions.Visibility = Visibility.Collapsed;

                                _enabledBarCodeCapture = true;
                            }
                            break;
                        case States.WaitingList:
                            {
                                ShowWaitingListViewer();
                                HidePrePaidViewer();
                                HideTablesReservationsViewer();
                                HideMealsControlViewer();
                                HideTablesViewer();
                                HideProductsViewer();
                            }
                            break;
                        case States.TableReservation:
                            {
                                ShowTablesReservationsViewer();
                                HidePrePaidViewer();
                                HideMealsControlViewer();
                                HideWaitingListViewer();
                                HideTablesViewer();
                                HideProductsViewer();
                            }
                            break;
                        case States.MealsControl:
                            {
                                ShowMealsControlViewer();
                                HidePrePaidViewer();
                                HideTablesReservationsViewer();
                                HideWaitingListViewer();
                                HideTablesViewer();
                                HideProductsViewer();
                            }
                            break;
                        case States.PrePaid:
                            {
                                ShowPrePaidViewer();
                                HideMealsControlViewer();
                                HideTablesReservationsViewer();
                                HideWaitingListViewer();
                                HideTablesViewer();
                                HideProductsViewer();
                            }
                            break;
                        case States.Clients:
                            {
                                //ShowClientsPanel();
                                HidePrePaidViewer();
                                HideMealsControlViewer();
                                HideTablesReservationsViewer();
                                HideWaitingListViewer();
                                HideTablesViewer();
                                HideProductsViewer();
                            }
                            break;
                        default:
                            // TODO: Log this error properly
                            Console.WriteLine($"{State} not yet implemented");
                            break;
                    }
                }

                #endregion
            }
        }

        public static readonly DependencyProperty StateProperty =
            DependencyProperty.Register(nameof(State), typeof(States), typeof(BarCaffeteriaRestaurantPage));

        #endregion

        #region Separator
        
        public Separator? LastSeparatorChecked
        {
            get => (Separator)GetValue(LastSeparatorCheckedProperty);
            private set => SetValue(LastSeparatorCheckedProperty, value);
        }

        public static readonly DependencyProperty LastSeparatorCheckedProperty =
            DependencyProperty.Register(nameof(LastSeparatorChecked), typeof(Separator), typeof(BarCaffeteriaRestaurantPage));
        
        #endregion

        #region Seat

        public Seat? LastSeatChecked
        {
            get => (Seat)GetValue(LastSeatCheckedProperty);
            private set => SetValue(LastSeatCheckedProperty, value);
        }

        public static readonly DependencyProperty LastSeatCheckedProperty =
            DependencyProperty.Register(nameof(LastSeatChecked), typeof(Seat), typeof(BarCaffeteriaRestaurantPage));

        
        #endregion

        #region HasPrePaidCode

        public bool HasPrePaidCode
        {
            get => (bool)GetValue(HasPrePaidCodeProperty);
            private set => SetValue(HasPrePaidCodeProperty, value);
        }

        public static readonly DependencyProperty HasPrePaidCodeProperty =
            DependencyProperty.Register(nameof(HasPrePaidCode), typeof(bool), typeof(BarCaffeteriaRestaurantPage));



        #endregion

        #region Configuration
        
        public bool IsIntegrationActive 
        { 
            get => (bool)GetValue(IsIntegrationActiveCheckedProperty);
            set => SetValue(IsIntegrationActiveCheckedProperty, value);
        }

        public static readonly DependencyProperty IsIntegrationActiveCheckedProperty =
            DependencyProperty.Register(nameof(IsIntegrationActive), typeof(bool), typeof(BarCaffeteriaRestaurantPage));
            
        public IAppSecurity AppSecurity => CwFactory.Resolve<IAppSecurity>();
        public IWindow WindowHelper => CwFactory.Resolve<IWindow>();
        public ITicketPrinter TicketPrinter => CwFactory.Resolve<ITicketPrinter>();
        public Point MiddleScreen => new Point(ActualWidth / 2, ActualHeight / 2);

        #endregion
        
        #endregion
        
        #region Settings

        private void TablesViewer_OnSaloonZoomChangued(double newValue)
        {
            MgrServices.SetConfigurationSetting(MgrServices.Settings.SaloonZoomValue, newValue.ToString("F2"));
        }

        private void BarCaffeteriaRestaurantPage_OnLoaded(object sender, RoutedEventArgs e)
        {
            SignalRNotifications.InitializeSubscriptions(this);
            SocketIONotifications.InitializeSubscriptions(this);
        }

        #endregion
        
        #region Ticket Operations

        #region Add Product to Ticket

        private void ProductsViewer_Product_Click(Product product, int qty)
        {
            if (mainPage.BusyControl.Visibility == Visibility.Visible) return;
            if (CurrentTicket == null) return;

            if (product.Id == Hotel.Instance.CurrentStandCashierContext.Stand.TipService)
                WindowHelper.ShowError(new Exception("Tip Product cannot be added manually"));
            else
            {
                // CurrentPaxNumber is a temporary number. It is not save in DB, therefore, the result comes with value 0
                // Fill it every time a product is inserted
                CurrentTicket.CurrentPaxNumber = _currentPaxNumber ?? 0;
                if (_room != null) CurrentTicket.Room = _room;
                var ticket = CurrentTicket.AsContract();
                if (!string.IsNullOrEmpty(ticket.RoomServiceNumber))
                    WindowHelper.ShowError("TickedSendedToServiceMonitor".Translate());
                else
                {
                    bool OkAction()
                    {
                        mainPage.BusyControl.Show("Adding Order");

                        Task.Run(async () =>
                                await BusinessProxyRepository.Ticket.AddProductToTicketAsync(ticket, (ProductRecord)product.DataContext, qty, product.ManualPrice, product.ManualPriceDesc))
                            .ContinueWith(t =>
                            {
                                if (t.IsFaulted)
                                {
                                    if (t.Exception != null)
                                        WindowHelper.ShowError(t.Exception);
                                    mainPage.BusyControl.Close();
                                }

                                if (t.Status != TaskStatus.RanToCompletion) return;
                                var result = t.Result;

                                // Reset manual price and description of the product
                                product.ManualPrice = null;
                                product.ManualPriceDesc = null;

                                if (result.IsEmpty)
                                {
                                    // I have to this because these three values are not saved in the DB
                                    var cardNumber = CurrentTicket.CardNumber;
                                    var accountBalance = CurrentTicket.AccountBalance;
                                    var name = CurrentTicket.Name;

                                    if(result.Contract is POSTicketContract contract)
                                    {
                                        contract.CardNumber = cardNumber;
                                        contract.AccountBalance = accountBalance;
                                        contract.Name = name;
                                        CurrentTicket.DataContext = contract;
                                    }
                                    
                                    CurrentTicket.RecalculateAll();
                                    // New Ticket. Add it to Stand
                                    var stand = Hotel.Instance.CurrentStandCashierContext.Stand;
                                    if (!stand.ContainsTicket(CurrentTicket))
                                        stand.AddTicket(CurrentTicket);

                                    mainPage.BusyControl.Close();
                                
                                    if(CurrentTicket.AccountBalance > (CurrentTicket.TotalAmount * -1))
                                        WindowHelper.ShowInfo("PrePaidBalanceNotEnough".Translate(), "Info");    
                                }
                                else
                                {
                                    if (result.ToString().IndexOf("Stale data", StringComparison.Ordinal) != -1)
                                        WindowHelper.ShowError("Last operation still in progress. Try again", "Info");
                                    else if (result.ToString().IndexOf("Ticket modified", StringComparison.Ordinal) != -1)
                                    {
                                        WindowHelper.ShowError("Ticket was modified on another Stand/Cashier. Current Ticket will close now", "Info");
                                        var action = mainPage.PopCloseAction();
                                        action();
                                    }
                                    else
                                        WindowHelper.ShowError(result);

                                    mainPage.BusyControl.Close();
                                }
                            }, UIThread.Scheduler);

                        return true;
                    }

                    _productBeingAdded = product;
                    _tableAction = () =>
                    {
                        if (product.ProductTables.Any(x => x.Selected))
                        {
                            var isValid = true;
                            foreach (var table in product.ProductTables)
                            {
                                if (table.SeparatorMaxQuantitySelection is not > 0) continue;
                                var count = product.ProductTables.Count(x => x.SeparatorId == table.SeparatorId && x.Selected);
                                if (count != 0) continue;
                                isValid = false;
                                break;
                            }

                            if (isValid)
                            {
                                product.SetTableProductSelectionOnRecord();
                                return OkAction();
                            }

                            WindowHelper.ShowError("You must select mandatory products.");
                            return false;
                        }

                        WindowHelper.ShowError("Select at least one product.");
                        return false;
                    };
                    
                    var _accompanyingProductsAction = (List<Product> products) =>
                    {
                        product.SetAccompanyingProductsSelection(products);
                        return OkAction();
                    };

                    var isMenu = product.ProductTableType is TableType.DynamicMenu;

                    if (product.PriceType == (long)ProductPriceType.AlwaysRate)
                    {
                        // Siempre es por tarifa
                        if (product.StandardPrice.HasValue)
                        {
                            if (isMenu)
                            {
                                WindowHelper.OpenDialog(new PickerProductTable(product), false, null, _tableAction, MiddleScreen);
                            }
                            else if (product.HasAccompanyingProducts)
                            {
                                var accompanyingProducts = Hotel.Instance.CurrentStandCashierContext.Stand.Products.Where(x => product.AccompanyingProducts.Contains(x.Id)).ToList();
                                WindowHelper.OpenDialog(new PickerAccompanyingProduct(accompanyingProducts), false, null, _accompanyingProductsAction, MiddleScreen);
                            }
                            else
                                OkAction();
                        }
                        else
                            WindowHelper.ShowError("ProductDontHavePrice".Translate());
                    }
                    else
                    {
                        // Puede ser Manual o Custom
                        object visual;
                        if (product.PriceType == (long)ProductPriceType.AlwaysManual)
                        {
                            // Es manual solamente
                            var priceManual = new ManualPriceControl();
                            priceManual.ValueChange += value => product.ManualPrice = value;
                            priceManual.DescriptionChange += value => product.ManualPriceDesc = value;
                            visual = priceManual;
                        }
                        else
                        {
                            // Es custom
                            visual = Resources["ProductCustomPriceTypeDialog"];
                            product.ManualPrice = product.StandardPrice ?? decimal.Zero;
                        }

                        void CancelAction()
                        {
                            // Reset manual price and description of the product
                            product.ManualPrice = null;
                            product.ManualPriceDesc = null;
                        }

                        bool SelectMenuProductsAction()
                        {
                            if (isMenu)
                            {
                                WindowHelper.OpenDialog(new PickerProductTable(product), false, null, _tableAction, MiddleScreen);
                                return false;
                            }
                            else if (product.HasAccompanyingProducts)
                            {
                                var accompanyingProducts = Hotel.Instance.CurrentStandCashierContext.Stand.Products.Where(x => product.AccompanyingProducts.Contains(x.Id)).ToList();
                                WindowHelper.OpenDialog(new PickerAccompanyingProduct(accompanyingProducts), false, null, _accompanyingProductsAction, MiddleScreen);
                                return false;
                            }
                            else
                                OkAction();

                            return true;
                        }

                        WindowHelper.OpenDialog(visual, false, product, SelectMenuProductsAction,
                            MiddleScreen, DialogButtons.OkCancel, CancelAction, null, false); 
                    }
                }
            }
        }

        #endregion
        
        #region Moving Tickets

        private void TablesViewer_MoveTicketRequested(ITicket ticket, Table table, Action<bool> succeed)
        {
            if (table != null && table.IsReserved)
            {
                var hour = table.ReservationTime.Value.ToString("h:mm tt", CultureInfo.InvariantCulture);
                var message = $"The current table is reserved for {hour}.{Environment.NewLine}Are you sure you want to create a ticket?";

                Func<bool> okAction = () =>
                {
                    MoveTicket(ticket, table, succeed);
                    return true;
                };

                MainPage.CurrentInstance.OpenDialog(message, okAction);
            }
            else
                MoveTicket(ticket, table, succeed);
        }

        private async void MoveTicket(ITicket ticket, Table table, Action<bool> succeed)
        {
            WindowHelper.ShowBusyDialog();
            try
            {
                // var ticketContract = (POSTicketContract)ticket.DataContext;
                var tableId = table?.Id;

                // var result = await BusinessProxyRepository.Ticket.MoveTicketToTable(ticketContract, tableId);
                var result = await BusinessProxyRepository.Ticket.MoveTicketToTableById(ticket.Id, tableId);
                if (result.IsEmpty)
                {
                    //var contract = (POSTicketContract)result.Contract;
                    //ticket.DataContext = contract;
                    succeed?.Invoke(true);
                }
                else
                {
                    WindowHelper.ShowError(result);
                    succeed?.Invoke(false);
                }
            }
            catch (Exception e)
            {
                WindowHelper.ShowError(e);
                succeed?.Invoke(false);
            }
            finally
            {
                WindowHelper.CloseBusyDialog();
            }
        }

        #endregion
        
        #region Open Tiket Details

        private void LoadTicketForDetails(ITicket ticket, bool forceUnlock, Action<bool> succeed)
        {
            mainPage.BusyControl.Show("Opening ticket ...");

            BusinessProxyRepository.Ticket.LoadTicketForEditionAsync(ticket.Id, forceUnlock)
            .ContinueWith(t =>
            {
                // mainPage.BusyControl.Close();

                if (t.IsFaulted)
                {
                    if (t.Exception != null)
                        WindowHelper.ShowError(t.Exception);
                    succeed?.Invoke(false);
                }
                if (t.Status == TaskStatus.RanToCompletion)
                {
                    var result = t.Result;
                    if (result.IsEmpty)
                    {
                        var ticketContract = (POSTicketContract)result.Contract;
                        if (ticketContract.TicketBlockedIssue && !forceUnlock)
                        {
                            // CurrentTicket.BlockTicket();
                            MainWindow.Instance.OpenDialog(new TextBlock { Text = $"This ticket is currently in use by {ticketContract.OpenUserDescription}. Edition not allowed. Are you sure you want to override this?. Ticket might be uncompleted.", FontSize = 14 }, null, () =>
                            {
                                LoadTicketForDetails(ticket, true, succeed);
                            }, null);
                        }
                        else
                        {
                            CurrentTicket = new Ticket(ticketContract); //.ForceUnblockTicket();
                                                                        // CurrentTicket.DataContext = ticketContract;
                            succeed(true);
                        }
                    }
                    else
                    {
                        if (result.ToString().IndexOf("Ticket Closed") != -1)
                        {
                            WindowHelper.ShowError("Ticket was already closed. Information will be refreshed", "Info");
                            if (ticket.Id.Equals(CurrentTicket?.Id))
                                RemoveVisualTicket(CurrentTicket);
                        }
                        else
                            WindowHelper.ShowError(result);

                        succeed?.Invoke(false);
                    }
                }

                mainPage.BusyControl.Close();
            },
            UIThread.Scheduler);
        }


        private void LoadTicketForDetails(Guid ticketId, bool forceUnlock, Action<bool> succeed)
        {
            mainPage.BusyControl.Show("Opening ticket ...");

            Task.Run(async () => await BusinessProxyRepository.Ticket.LoadTicketForEditionAsync(ticketId, forceUnlock))
            .ContinueWith(t =>
            {
                mainPage.BusyControl.Close();

                if (t.IsFaulted)
                {
                    if (t.Exception != null)
                        WindowHelper.ShowError(t.Exception);
                    succeed?.Invoke(false);
                }
                if (t.Status == TaskStatus.RanToCompletion)
                {
                    var result = t.Result;
                    if (result.IsEmpty)
                    {
                        var ticketContract = (POSTicketContract)result.Contract;
                        if (ticketContract.TicketBlockedIssue && !forceUnlock)
                        {
                            CurrentTicket.BlockTicket();
                            MainWindow.Instance.OpenDialog(new TextBlock { Text = $"This ticket is currently in use by {ticketContract.OpenUserDescription}. Edition not allowed. Are you sure you want to override this?. Ticket might be uncompleted.", FontSize = 14 }, null, () =>
                            {
                                LoadTicketForDetails(ticketId, true, succeed);
                            }, null);
                        }
                        else
                        {
                            CurrentTicket.ForceUnblockTicket();
                            CurrentTicket.DataContext = ticketContract;
                            succeed(true);
                        }
                    }
                    else
                    {
                        if (result.ToString().IndexOf("Ticket Closed") != -1)
                        {
                            WindowHelper.ShowError("Ticket was already closed. Information will be refreshed", "Info");
                            RemoveVisualTicket(CurrentTicket);
                        }
                        else
                            WindowHelper.ShowError(result);

                        succeed?.Invoke(false);
                    }
                }
            },
            UIThread.Scheduler);
        }

        private async Task SaveTicketFromDetailsAsync(Guid ticketId, short paxs, Action<bool> succeed)
        {
            mainPage.BusyControl.Show("Closing ticket ...");
            try
            {
                var result = await BusinessProxyRepository.Ticket.SaveTicketFromEditionAsync(ticketId, paxs);

                if (result.IsEmpty)
                {
                    CurrentTicket.DataContext = result.Contract;
                    succeed?.Invoke(true);
                }
                else
                {
                    WindowHelper.ShowError(result);
                    succeed?.Invoke(false);
                }
            }
            catch (Exception e)
            {
                WindowHelper.ShowError(e);
                succeed?.Invoke(false);
            }
            finally
            {
                mainPage.BusyControl.Close();
            }
        }

        private void TablesViewer_Table_Click(TablesViewer sender, Table table, ITicket ticket)
        {
            if (State != States.Products)
            {
                if (table == null)
                {
                    // CurrentTicket = ticket;
                    Action<bool> succeed = success =>
                    {
                        if (success)
                        {
                            // Notify ticket will be in edition mode
                            // var signalContract = new NotificationContract<Guid>
                            // {
                            //     DataContext = ticket.Id,
                            //     TokenID = BusinessProxyRepository.Cashier.GetToken()
                            // };

                            // SignalRNotifications.Publish("OpeningTicket", Serializer.SerializeJson(signalContract, signalContract.GetType()));
                            State = States.Products;
                        }
                    };

                    // LoadTicketForDetails(ticket.Id, false, succeed);
                    LoadTicketForDetails(ticket, false, succeed);
                }
                else
                {
                    if (table.State == Table.States.Ready)
                    {
                        // Es una cuenta nueva
                        CurrentTicket = CreateNewTicket(table);
                        // table.Bills.Add(CurrentTicket);
                        table.AddTicket(CurrentTicket);
                        State = States.Products;
                    }
                    else
                    {
                        // La mesa esta ocupada, revisemos su bill
                        // colocamos el bill, por ahora tomaremos siempre el primero
                        var bill = table.Bills.FirstOrDefault(x => x is Ticket);
                        if (bill != null)
                        {
                            CurrentTicket = bill;
                            State = States.Products;
                        }
                    }
                }

                ResetBarCode();
            }
            else if(Hotel.Instance.CurrentStandCashierContext.Stand.Tickets.Any(t => t.Id == ticket.Id)) CurrentTicket = (Ticket)ticket;
        }

        #endregion
        
        #region Tiket Description

        private async void TicketViewer_DescriptionChanged(Ticket ticket, Action<bool> succeed)
        {
            var ticketContract = (POSTicketContract)ticket.DataContext;

            try
            {
                mainPage.BusyControl.Show("Updating ticket description");
                var result = await BusinessProxyRepository.Ticket.PersistTicketAsync(ticketContract);

                if (result.IsEmpty)
                {
                    ticket.DataContext = result.Contract;
                    succeed?.Invoke(true);
                }
                else
                {
                    WindowHelper.ShowError(result);
                    succeed?.Invoke(false);
                }
            }
            catch (Exception e)
            {
                WindowHelper.ShowError(e);
                succeed?.Invoke(false);
            }
            finally
            {
                mainPage.BusyControl.Close();
            }
        }
        #endregion
        
        #region Ticket Account
        private void TicketViewer_AccountChanged(Ticket ticket, IBaseRecord record)
        {
            PersistTicket(ticket);

            if (record is SpaReservationSearchFromExternalRecord { ProductId: not null } spaRecord)
                AddSpaService(ticket, spaRecord);
            else
                HideEditorControl();
        }

        private void PersistTicket(Ticket ticket)
        {
            if (ticket.IsPersisted)
            {
                var ticketContract = (POSTicketContract)ticket.DataContext;

                mainPage.BusyControl.Show("Updating ticket account");
                Task.Run(async () => await BusinessProxyRepository.Ticket.PersistTicketAsync(ticketContract))
                .ContinueWith(t =>
                {
                    mainPage.BusyControl.Close();
                    if (t.IsFaulted && t.Exception != null) WindowHelper.ShowError(t.Exception);
                    if (t.Status != TaskStatus.RanToCompletion) return;
                    var result = t.Result;
                    if (result.IsEmpty) ticket.DataContext = result.Contract;
                    else WindowHelper.ShowError(result);
                },
                UIThread.Scheduler);
            }
        }

        #endregion
        
        #region Manual Separator

        private void TicketViewer_OrderDetailsSeparatorRequested(Order order, Action<bool> callback)
        {
            mainPage.BusyControl.Show("Updating ticket separators");
            order.HasManualSeparator = !order.HasManualSeparator;
            POSTicketContract ticketContract = CurrentTicket.DataContext as POSTicketContract;

            Task.Run(async () => await BusinessProxyRepository.Ticket.PersistTicketAsync(ticketContract))
            .ContinueWith(t =>
            {
                mainPage.BusyControl.Close();

                if (t.IsFaulted)
                {
                    if (t.Exception != null) WindowHelper.ShowError(t.Exception);
                    callback?.Invoke(false);
                }
                if (t.Status == TaskStatus.RanToCompletion)
                {
                    ValidationContractResult result = t.Result;
                    if (result.IsEmpty)
                    {
                        CurrentTicket.DataContext = result.Contract;
                        CurrentTicket.RecalculateAll();

                        if (callback != null) callback(true);
                    }
                    else
                    {
                        WindowHelper.ShowError(result);
                        if (callback != null) callback(false);
                    }
                }
            },
            UIThread.Scheduler);
        }
        #endregion
        
        #region Split

        private SplitTicketDataContext _splitTicketDataContext;

        private void OldProductButton_Product_Added(ProductButton arg1, Product arg2, int arg3)
        {
            if (_splitTicketDataContext != null)
                SplitTicketDataContext.MoveProducts(_splitTicketDataContext.OldOrders, _splitTicketDataContext.NewOrders, arg1.Tag as Order, arg3);
        }

        private void NewProductButton_Product_Added(ProductButton arg1, Product arg2, int arg3)
        {
            if (_splitTicketDataContext != null)
                SplitTicketDataContext.MoveProducts(_splitTicketDataContext.NewOrders, _splitTicketDataContext.OldOrders, arg1.Tag as Order, arg3);
        }

        private void SplitAutoTicket_Click(object sender, RoutedEventArgs e)
        {
            // Solo si hay productos visibles
            if (CurrentTicket.HasVisibleOrders)
            {
                var context = new AutoSplitModel { Quantity = 2 };
                var visual = Resources["autoSplitContent"] as UIElement;
                WindowHelper.OpenDialog(visual, false, context, () =>
                {
                    var ticketId = CurrentTicket.Id;
                    var quantity = context.Quantity;

                    Task.Run(async () =>
                    {
                        return await BusinessProxyRepository.Ticket.SplitTicketAutoAsync(ticketId, quantity);
                    })
                    .ContinueWith(t =>
                    {
                        mainPage.BusyControl.Close();

                        if (t.IsFaulted)
                        {
                            if (t.Exception != null)
                                WindowHelper.ShowError(t.Exception);
                        }
                        else
                        {
                            var result = t.Result;
                            if (result.IsEmpty)
                            {
                                foreach (var contract in result.Tickets)
                                {
                                    if ((Guid)contract.Id == CurrentTicket.Id)
                                    {
                                        CurrentTicket.DataContext = contract;
                                        CurrentTicket.RecalculateAll();
                                    }
                                    else
                                    {
                                        var ticket = new Ticket(contract);
                                        Hotel.Instance.CurrentStandCashierContext.Stand.AddTicket(ticket);
                                        if (!ticket.TableId.HasValue)
                                            Hotel.Instance.CurrentStandCashierContext.QuickTickets.Add(ticket);
                                        ticket.RecalculateAll();
                                    }
                                }
                            }
                            else
                                WindowHelper.ShowError(result);
                        }
                    }, UIThread.Scheduler);
                    return true;
                }, new Point(ActualWidth / 2, ActualHeight / 2));
            }
            else
                WindowHelper.ShowError("NotSplitEmptyTicket".Translate());
        }

        private void SplitTicket_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentTicket.HasVisibleOrders)
            {
                _enabledBarCodeCapture = false;
                var okAction = () =>
                {
                    mainPage.ShowAuxiliarPanel = true;
                    _ticketActions.IsEnabled = true;
                    _ticketSeatActions.IsEnabled = true;
                    mainPage.AllowClose();
                    mainPage.BusyControl.Show("Splitting ticket...");

                    var orderIds = _splitTicketDataContext.NewOrders.Select(x => new KeyValuePair<Guid, decimal>(x.Id, x.Quantity)).ToArray();
                    var ticketId = CurrentTicket.Id;

                    Task.Run(async () => await BusinessProxyRepository.Ticket.SplitTicketManualAsync(ticketId, orderIds))
                    .ContinueWith(t =>
                    {
                        mainPage.BusyControl.Close();

                        if (t.IsFaulted)
                            WindowHelper.ShowError(t.Exception);
                        else
                        {
                            var result = t.Result;
                            if (result.IsEmpty)
                            {
                                var firstTicketUpdated = false;
                                foreach (var ticketContract in result.Tickets)
                                {
                                    if (!firstTicketUpdated)
                                    {
                                        CurrentTicket.DataContext = ticketContract;
                                        CurrentTicket.RecalculateAll();
                                        firstTicketUpdated = true;
                                    }
                                    else
                                    {
                                        var ticket = new Ticket(ticketContract);
                                        Hotel.Instance.CurrentStandCashierContext.Stand.AddTicket(ticket);
                                        if (ticket.Table == null)
                                            Hotel.Instance.CurrentStandCashierContext.QuickTickets.Add(ticket);
                                        ticket.RecalculateAll();
                                    }
                                }
                            }
                            else
                                WindowHelper.ShowError(result);
                        }
                    }, UIThread.Scheduler);

                    _enabledBarCodeCapture = true;
                    return true;
                };

                Func<bool>? cancelAction = () =>
                {
                    mainPage.ShowAuxiliarPanel = true;
                    _ticketActions.IsEnabled = true;
                    _ticketSeatActions.IsEnabled = true;
                    mainPage.AllowClose();

                    _enabledBarCodeCapture = true;
                    return true;
                };

                mainPage.ShowAuxiliarPanel = false;
                _ticketActions.IsEnabled = false;
                _ticketSeatActions.IsEnabled = false;
                mainPage.ProhibitClose();
                _splitTicketContainer.DataContext = _splitTicketDataContext = new SplitTicketDataContext(CurrentTicket);
                ShowEditorControl("Split ticket", _splitTicketContainer, Colors.Black, okAction, cancelAction);
            }
            else
                WindowHelper.ShowError("NotSplitEmptyTicket".Translate());
        }

        #endregion
        
        #region Split Line
        private void TicketViewer_OnSplitOrderRequested(Ticket ticket, Order order, int quantity)
        {
            mainPage.BusyControl.Show("Splitting order");

            Task.Run(async () =>
            {
                return await BusinessProxyRepository.Ticket.SplitTicketProductAsync(ticket.DataContext as POSTicketContract, order.Id, quantity);
            })
            .ContinueWith(t =>
            {
                mainPage.BusyControl.Close();

                if (t.IsFaulted)
                {
                    if (t.Exception != null) WindowHelper.ShowError(t.Exception);
                }
                if (t.Status == TaskStatus.RanToCompletion)
                {
                    ValidationContractResult result = t.Result;
                    if (result.IsEmpty)
                    {
                        ticket.DataContext = result.Contract as POSTicketContract;
                        ticket.RecalculateAll();
                    }
                    else
                    {
                        WindowHelper.ShowError(result);
                    }
                }
            },
            UIThread.Scheduler);
        }
        #endregion
        
        #region Transfer

        #region Transfer Selected Ticket

        private void TransferTicket_Click(object sender, RoutedEventArgs e)
        {
            var contract = CurrentTicket.AsContract();
            if (!string.IsNullOrEmpty(contract.RoomServiceNumber))
                WindowHelper.ShowError("TickedSendedToServiceMonitor".Translate());
            else
            {
                if (CurrentTicket.HasVisibleOrders)
                {
                    mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.TicketsTransfer,
                        (isValid, validUser) =>
                        {
                            if (isValid)
                                TransferTicketShowDialog(CurrentTicket, false, MiddleScreen);
                            else
                                WindowHelper.ShowError("UserNotAuthorized".Translate());
                        });
                }
                else
                    WindowHelper.ShowError("NotTransferEmptyTicket".Translate());
            }
        }

        private void TransferTicketShowDialog(Ticket bill, bool autoCancel, Point location)
        {
            _enabledBarCodeCapture = false;
            var context = Hotel.Instance.CurrentStandCashierContext;

            var transferContract = new TransferContract();
            transferContract.Id = Guid.NewGuid();
            transferContract.StandOrig = context.Stand.Id;
            transferContract.CajaOrig = context.Cashier.Id;
            transferContract.TicketOrig = bill.Id;
            transferContract.UtilTran = Hotel.Instance.UserId;
            transferContract.TranDate = mainPage.Stand.StandWorkDate;
            transferContract.TranTime = DateTime.Now;

            var okAction = new Func<bool>(delegate
            {
                var selectedStand = (Resources["standsTransferComboBox"] as ComboBox).SelectedValue as LiteStandRecord;
                if (selectedStand != null)
                {
                    transferContract.StandDest = selectedStand.Id;
                    mainPage.BusyControl.Show("Transfering ticket");

                    Task.Run(async () =>
                    {
                        return await BusinessProxyRepository.Stand.TransferTicketAsync(bill.DataContext as POSTicketContract, transferContract);
                    })
                    .ContinueWith(t =>
                    {
                        mainPage.BusyControl.Close();

                        if (t.IsFaulted)
                        {
                            if (t.Exception != null) WindowHelper.ShowError(t.Exception);
                        }
                        if (t.Status == TaskStatus.RanToCompletion)
                        {
                            var result = t.Result;
                            if (result.IsEmpty)
                            {
                                var stand = context.Stand;
                                stand.Tickets.Remove(bill);
                                RemoveVisualTicket(bill);
                                stand.OutTransferNotificationsCount++;
                            }
                            else
                                WindowHelper.ShowError(result);
                        }
                    },
                    UIThread.Scheduler);
                }

                _enabledBarCodeCapture = true;
                return true;
            });

            WindowHelper.OpenDialog(Resources["transferTicketContent"], autoCancel, transferContract, okAction, location, DialogButtons.OkCancel, () => { });
        }
        #endregion
        
        #region Return Ticket (s)
        //Show selected tickets to be returned
        private void returnTickets_Click(object sender, RoutedEventArgs e)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.TicketsTransfer, (isValid, validUser) =>
            {
                if (isValid)
                {
                    ReturnTicketsShowDialog(false, MiddleScreen);
                }
                else
                {
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
                }
            });

        }
        private void ReturnTicketsShowDialog(bool autoCancel, Point location)
        {
            var context = Hotel.Instance.CurrentStandCashierContext;
            mainPage.Stand.VerifyTransferReturns(context.Cashier.Id, (item, exResult) =>
            {
                if (exResult != null)
                {
                    WindowHelper.ShowError(exResult);
                    return;
                }
                ContainerTransferContract transfersReturn = new ContainerTransferContract();
                try
                {
                    transfersReturn = item;
                }
                finally
                {
                    if (transfersReturn.Transfers.Count == 0)
                    {
                        WindowHelper.ShowError("NoTicketsReturnTransferInStand".Translate());
                    }
                    else
                    {
                        WindowHelper.OpenDialog(Resources["returnTicketsContent"], autoCancel, transfersReturn.Transfers, () =>
                            {// Ok Action
                                var dataGrid = Resources["returnTicketsContent"] as DataGrid;

                                if (dataGrid.SelectedItems != null && dataGrid.SelectedItems.Count > 0)
                                {
                                    ReturnTransfers(dataGrid.SelectedItems);
                                }
                                return true;
                            }// Ok Action
                        , location);
                    }
                }
            });
        }
        private void returnAllTransfers_Click(object sender, RoutedEventArgs e)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.TicketsTransfer, (isValid, validUser) =>
            {
                if (isValid)
                {
                    var context = Hotel.Instance.CurrentStandCashierContext;
                    mainPage.Stand.VerifyTransferReturns(context.Cashier.Id, (item, exResult) =>
                    {
                        if (exResult != null)
                        {
                            WindowHelper.ShowError(exResult);
                            return;
                        }
                        ContainerTransferContract transfersReturn = new ContainerTransferContract();
                        try
                        {
                            transfersReturn = item;
                        }
                        finally
                        {
                            if (transfersReturn.Transfers.Count == 0)
                            {
                                WindowHelper.ShowError("NoTicketsReturnTransferInStand".Translate(), "Information".Translate());
                            }
                            else
                            {
                                ReturnTransfers(transfersReturn.Transfers);
                            }
                        }
                    });
                }
                else
                {
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
                }
            });
        }
        private void ReturnTransfers(object transfers)
        {
            mainPage.BusyControl.Show("Returning ticket");

            List<TransferContract> transferContracts = ((IList)transfers).Cast<TransferContract>().ToList();

            //ASD
            Task.Run(async () =>
            {
                return await BusinessProxyRepository.Stand.ReturnTransferedTicketAsync((transferContracts));
            })
            .ContinueWith(t =>
            {
                mainPage.BusyControl.Close();

                if (t.IsFaulted)
                {
                    if (t.Exception != null) WindowHelper.ShowError(t.Exception);
                }
                if (t.Status == TaskStatus.RanToCompletion)
                {
                    ValidationTicketsResult result = t.Result;
                    if (result.IsEmpty)
                    {
                        foreach (POSTicketContract item in result.Tickets)
                        {
                            Ticket ticket = new Ticket(item);
                            Hotel.Instance.CurrentStandCashierContext.Stand.Tickets.Add(ticket);
                            Hotel.Instance.CurrentStandCashierContext.ReturnedTickets.Add(ticket);
                            ticket.RecalculateAll();

                            Hotel.Instance.CurrentStandCashierContext.Stand.OutTransferNotificationsCount--;
                        }

                        foreach (TransferContract tc in transferContracts)
                        {
                            //Notify Acceptance
                            var signalContract = new NotificationContract<TransferContract> { DataContext = tc, TokenID = BusinessProxyRepository.Cashier.GetToken() };
                            SignalRNotifications.Publish("TransferTicketActions", Serializer.SerializeJson(signalContract, signalContract.GetType()));
                        }
                    }
                    else
                    {
                        WindowHelper.ShowError(result);
                    }
                }
            },
            UIThread.Scheduler);
        }
        #endregion
        
        #region Accept Ticket (s)
        private void acceptTickets_Click(object sender, RoutedEventArgs e)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.TicketsTransfer, (isValid, validUser) =>
            {
                if (isValid)
                {
                    AcceptTicketsShowDialog(false, MiddleScreen);
                }
                else
                {
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
                }
            });

        }
        private void AcceptTicketsShowDialog(bool autoCancel, Point location)
        {
            mainPage.Stand.VerifyTransferToAccept((item, exResult) =>
            {
                if (exResult != null)
                {
                    WindowHelper.ShowError(exResult);
                    return;
                }
                ContainerTransferContract transfersAccept = new ContainerTransferContract();
                try
                {
                    transfersAccept = item;
                }
                finally
                {
                    if (transfersAccept.Transfers.Count == 0)
                    {
                        WindowHelper.ShowError("NoTicketsAcceptInStand".Translate(), "Information".Translate());
                    }
                    else
                    {
                        WindowHelper.OpenDialog(Resources["acceptTicketsContent"], autoCancel, transfersAccept.Transfers, () =>
                            {// Ok Action
                                var dataGrid = Resources["acceptTicketsContent"] as DataGrid;

                                if (dataGrid.SelectedItems != null && dataGrid.SelectedItems.Count > 0)
                                    AcceptTransfers(dataGrid.SelectedItems);
                                return true;

                            }// Ok Action
                        , location);
                    }
                }
            });
        }
        private void acceptAllTransfers_Click(object sender, RoutedEventArgs e)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.TicketsTransfer, (isValid, validUser) =>
            {
                if (isValid)
                {
                    mainPage.Stand.VerifyTransferToAccept((item, exResult) =>
                    {
                        if (exResult != null)
                        {
                            WindowHelper.ShowError(exResult);
                            return;
                        }
                        ContainerTransferContract transfersAccept = new ContainerTransferContract();
                        try
                        {
                            transfersAccept = item;
                        }
                        finally
                        {
                            if (transfersAccept.Transfers.Count == 0)
                            {
                                WindowHelper.ShowError("NoTicketsAcceptInStand".Translate(), "Information".Translate());
                            }
                            else
                            {
                                AcceptTransfers(transfersAccept.Transfers);
                            }
                        }
                    });
                }
                else
                {
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
                }
            });
        }
        private void AcceptTransfers(object transfers)
        {
            mainPage.BusyControl.Show("Accepting ticket");

            var transferContracts = ((IList)transfers).Cast<TransferContract>().ToList();

            Task.Run(async () =>
            {
                return await BusinessProxyRepository.Stand.AcceptTransferedTicketsAsync((transferContracts));
            })
            .ContinueWith(t =>
            {
                mainPage.BusyControl.Close();

                if (t.IsFaulted)
                {
                    if (t.Exception != null) WindowHelper.ShowError(t.Exception);
                }
                if (t.Status == TaskStatus.RanToCompletion)
                {
                    var result = t.Result;
                    if (result.IsEmpty)
                    {
                        foreach (var item in result.Tickets)
                        {
                            var ticket = new Ticket(item);
                            Hotel.Instance.CurrentStandCashierContext.Stand.Tickets.Add(ticket);
                            Hotel.Instance.CurrentStandCashierContext.AcceptedTickets.Add(ticket);
                            ticket.RecalculateAll();

                            Hotel.Instance.CurrentStandCashierContext.Stand.InTransferNotificationsCount--;
                        }

                        // TODO: Remove notification TransferTicketActions
                        foreach (var tc in transferContracts)
                        {
                            //Notify Acceptance
                            var signalContract = new NotificationContract<TransferContract> { DataContext = tc, TokenID = BusinessProxyRepository.Cashier.GetToken() };
                            SignalRNotifications.Publish("TransferTicketActions", Serializer.SerializeJson(signalContract, signalContract.GetType()));
                        }
                    }
                    else
                    {
                        WindowHelper.ShowError(result);
                    }
                }
            },
            UIThread.Scheduler);
        }
        
        #endregion
        
        #endregion
        
        #region Merge
        private void mergeTickets_Checked(object sender, RoutedEventArgs e)
        {
            _tablesViewer.IsMerging = true;
        }
        private void mergeTickets_Unchecked(object sender, RoutedEventArgs e)
        {
            _tablesViewer.IsMerging = false;
        }
        private void TablesViewer_MergeTickets_Click(TablesViewer arg1, ITicket originalTicket, ITicket voidTicket)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.TicketsMerge, async (isValid, validUser) =>
            {
                if (isValid)
                {
                    try
                    {
                        mainPage.BusyControl.Show("Merging ticket");

                        var result = await BusinessProxyRepository.Ticket.MergeTicketsByIdAsync(originalTicket.Id, voidTicket.Id);
                        if (result.IsEmpty)
                        {
                            if (originalTicket is Ticket ticket)
                            {
                                ticket.DataContext = (POSTicketContract)result.Contract;
                                ticket.RecalculateAll();
                            }

                            Hotel.Instance.CurrentStandCashierContext.Stand.RemoveTicket(voidTicket);

                            RemoveVisualTicket(voidTicket);
                            _tablesViewer.ResetMergeSlots();
                        }
                        else WindowHelper.ShowError(result);
                    }
                    catch (Exception e)
                    {
                        WindowHelper.ShowError(e);
                    }
                    finally
                    {
                        mainPage.BusyControl.Close();
                    }
                }
            });
        }
        #endregion
        
        #region Void

        private void VoidTicket_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentTicket.HasTransferredOrders)
                WindowHelper.ShowError("NotVoidTicketWithTransferredOrder".Translate());
            else
            {
                mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.TicketsCancelWithoutInvoice, (isValid, validUser) =>
                {
                    if (isValid)
                        CancelBillShowDialog(CurrentTicket, false, MiddleScreen);
                    else
                        WindowHelper.ShowError("UserNotAuthorized".Translate());
                });
            }
        }

        private void CancelBillShowDialog(ITicket bill, bool autoCancel, Point location)
        {
            _enabledBarCodeCapture = false;
            var cancellationReasonPicker = new CancellationReasonPicker();
            var okAction = new Func<bool>(delegate
            {
                var cancellation = cancellationReasonPicker.CancellationReasonSelected;
                if (cancellation == null)
                {
                    WindowHelper.ShowError("RepickCancellationReason".Translate());
                    return false;
                }

                CancelTicket(bill, cancellation, cancellationReasonPicker.Comments, cancellationReasonPicker.CreateQuickTicket);
                _enabledBarCodeCapture = true;
                return true;
            });

            var cancelAction = new Action(delegate
            {
                _enabledBarCodeCapture = true;
            });

            WindowHelper.OpenDialog(cancellationReasonPicker, autoCancel, bill, okAction, location, DialogButtons.OkCancel, cancelAction);
        }

        private async void CancelTicket(ITicket ticket, CancellationReason? cancellationReason, string comments, bool createQuickTicket, Action afterAction = null)
        {
            cancellationReason ??= Hotel.Instance.CancellationReasons[0];

            try
            {
                mainPage.BusyControl.Show("Cancelling ticket");
                try
                {
                    // IEnumerable<Order> originalOrders;
                    ValidationContractResult result;


                    if (ticket is Ticket fullTicket)
                    {
                        // originalOrders = fullTicket.Orders;
                        result = await BusinessProxyRepository.Ticket.CancelTicketAsync(fullTicket.AsContract(), cancellationReason.Id, comments, createQuickTicket);
                    }
                    else
                    {
                        fullTicket = null;
                        result = await BusinessProxyRepository.Ticket.CancelTicketByIdAsync(ticket.Id, cancellationReason.Id, comments, createQuickTicket);

                        if (!result.HasErrors)
                        {
                            var contract = (POSTicketContract)result.Contract;
                            fullTicket = new Ticket(contract);
                        }
                    }

                    //              var result = ticket is Ticket fullTicket
                    //                  ? await BusinessProxyRepository.Ticket.CancelTicketAsync(fullTicket.AsContract(), cancellationReason.Id, comments)
                    //                  : await BusinessProxyRepository.Ticket.CancelTicketByIdAsync(ticket.Id, cancellationReason.Id, comments)
                    //;
                    if (result.IsEmpty)
                    {
                        try
                        {
                            var orders = new List<Order>();
                            foreach (var order in fullTicket.Orders.Where(x => x.IsActive))
                            {
                                if (order.AvailableToVoid)
                                {
                                    order.IsSelected = false;
                                    if (order.OrderTables != null)
                                    {
                                        foreach (var table in order.OrderTables.Where(x => !x.NotMarch))
                                        {
                                            table.IsSelected = true;
                                        }
                                    }
                                    orders.Add(order);
                                }
                            }
                            SendVoidDispatchMessages(fullTicket, orders);
                        }
                        finally
                        {
                            Hotel.Instance.CurrentStandCashierContext.Stand.RemoveTicket(ticket);
                            RemoveVisualTicket(ticket);
                        }
                    }
                    else
                    {
                        if (result.ToString().IndexOf("Ticket modified") != -1)
                        {
                            WindowHelper.ShowError("Ticket was modified on another Stand/Cashier. Current Ticket will close now", "Info");
                            var action = mainPage.PopCloseAction();
                            action();
                        }
                        else
                            WindowHelper.ShowError(result);
                    }
                }
                finally
                {
                    afterAction?.Invoke();
                    mainPage.BusyControl.Close();
                }
            }
            catch (Exception e)
            {
                WindowHelper.ShowError(e);
            }
        }

        #endregion
        
        #region Void Line

        private void TicketViewer_OnCancelOrderRequested(Ticket ticket, Order order)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.CancelProductLine, (isValid, validUser) =>
            {
                if (isValid)
                    CancelOrder(ticket, order);
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        public void CancelOrder(Ticket ticket, Order order)
        {
            if (ticket != null && order != null)
            {
                mainPage.BusyControl.Show("Cancelling order");

                Task.Run(async () => await BusinessProxyRepository.Ticket.CancelTicketProductAsync(ticket.AsContract(), order.Id))
                .ContinueWith(t =>
                {
                    mainPage.BusyControl.Close();
                    if (t.IsFaulted)
                    {
                        if (t.Exception != null)
                            WindowHelper.ShowError(t.Exception);
                    }
                    else if (t.Status == TaskStatus.RanToCompletion)
                    {
                        var result = t.Result;
                        if (result.IsEmpty)
                        {
                            ticket.DataContext = result.Contract as POSTicketContract;
                            ticket.RecalculateAll();

                            var orders = ticket.Orders.Where(x => x.MainProduct == order.Id && order.AvailableToVoid).ToList();
                            foreach (var order in orders)
                            {
                                order.IsSelected = true;
                                if (order.OrderTables != null)
                                {
                                    foreach (var orderTable in order.OrderTables.Where(x => !x.NotMarch))
                                    {
                                        orderTable.IsSelected = true;
                                    }
                                }
                            }

                            DisableAllButtons();
                            SendVoidDispatchToKitchen(CurrentTicket, orders, _ => { EnableAllButtons(); });
                        }
                        else
                            WindowHelper.ShowError(result);
                    }
                },
                UIThread.Scheduler);
            }
        }

        #endregion

        #region Area Section

        #region Separators

        private void OnSeparatorSelected(bool _)
        {
            _productsViewer.SelectedSeparator = LastSeparatorChecked;
            LastSeparatorChecked = null;
        }
        
        private void SelectedSeparator_Click(object sender, RoutedEventArgs e)
        {
            ShowSeparatorPanel(OnSeparatorSelected);
        }
        
        private void ChangeOrdersSeparator_Click(object sender, RoutedEventArgs e)
        {
            _multiOrderSelectionFilterCondition = _ => true;
            _multiOrderTableSelectionFilterCondition = _ => false;

            _multiSelectionOkAction = () =>
            {
                if (CurrentTicket == null) return;
                var orders = CurrentTicket.Orders.Where(o => o.IsVisible && o.IsSelected).ToList();
                DisableAllButtons();

                ShowSeparatorPanel(Callback);
                return;

                async void Callback(bool _)
                {
                    mainPage.BusyControl.Show($"Updating separator...");
                    var result = await BusinessProxyRepository.Ticket.ChangeOrdersSeparatorAsync(CurrentTicket.AsContract(), orders.Select(x => x.Id), LastSeparatorChecked?.Id);
                    if (result.IsEmpty)
                    {
                        CurrentTicket.DataContext = result.Contract;
                        CurrentTicket.RecalculateAll();
                    }
                    else
                    {
                        WindowHelper.ShowError(result);
                    }

                    EnableAllButtons();
                    mainPage.BusyControl.Close();
                }
            };

            if (IsEmptyMultipleSelection())
                EmptySetShowDialog(true, MiddleScreen);
            else
                MultipleSelection_Checked(false);
        }

        private void ShowSeparatorPanel(Action<bool> callback)
        {
            var changeSeparatorContext = (FrameworkElement)Resources["ChangeSeparatorPanel"];
            changeSeparatorContext.DataContext = Hotel.Instance.Separators;
            ShowEditorControl(
                "Separators",
                changeSeparatorContext,
                (Color)Application.Current.Resources["panelColor"], () =>
                {
                    callback.Invoke(true);
                    return true;
                }, 
                null, 
                false
            );
        }

        private void Separator_Checked(object sender, RoutedEventArgs e)
        {
            var cbx = (CheckBox)sender;
            if (cbx.DataContext is not Separator separator) return;
            if (LastSeparatorChecked != null) LastSeparatorChecked.IsChecked = false;
            LastSeparatorChecked = separator;
            _productsViewer.SelectedSeparator = separator;
        }

        private void Separator_Unchecked(object sender, RoutedEventArgs e)
        {
            var cbx = (CheckBox)sender;
            if (cbx.DataContext is not Separator separator) return;
            if (LastSeparatorChecked != null) LastSeparatorChecked.IsChecked = false;
            LastSeparatorChecked = null;
        }
       
        #endregion
        
        #region Seats
        
        private void ChangeOrdersSeat_Click(object sender, RoutedEventArgs e)
        {
            _multiOrderSelectionFilterCondition = _ => true;
            _multiOrderTableSelectionFilterCondition = _ => false;

            _multiSelectionOkAction = () =>
            {
                if (CurrentTicket == null) return;
                var orders = CurrentTicket.Orders.Where(o => o.IsVisible && o.IsSelected).ToList();
                DisableAllButtons();

                ShowSeatPanel(Callback);
                return;

                async void Callback(bool _)
                {
                    mainPage.BusyControl.Show($"Updating seat...");
                    var result = await BusinessProxyRepository.Ticket.ChangeOrdersSeatAsync(CurrentTicket.AsContract(), orders.Select(x => x.Id), (short)LastSeatChecked.Number);
                    if (result.IsEmpty)
                    {
                        CurrentTicket.DataContext = result.Contract;
                        CurrentTicket.RecalculateAll();
                    }
                    else
                    {
                        WindowHelper.ShowError(result);
                    }

                    EnableAllButtons();
                    mainPage.BusyControl.Close();
                }
            };

            if (IsEmptyMultipleSelection())
                EmptySetShowDialog(true, MiddleScreen);
            else
                MultipleSelection_Checked(false);
        }

        private void ShowSeatPanel(Action<bool> callback)
        {
            if (CurrentTicket?.TableId == null) return;
            var table = Hotel.Instance.CurrentStandCashierContext.Stand.FindTable(CurrentTicket.TableId.Value);
            {
                var seats = new ObservableCollection<Seat>();
                var paxs = table.TablesGroup?.MaxPaxs ?? table.Paxs;
                for (short i = 0; i <= paxs; i++)
                {
                    var description = i == 0 ? "Shared".Translate() : "Table".Translate() + " " + i;
                    seats.Add(new Seat(i, description));
                }

                var changeSeatContext = (FrameworkElement)Resources["ChangeSeatPanel"];
                changeSeatContext.DataContext = seats;

                ShowEditorControl(
                    "Seats",
                    changeSeatContext,
                    (Color)Application.Current.Resources["panelColor"], () =>
                    {
                        callback.Invoke(true);
                        return true;
                    }, null, false);
            }
        }

        private void Seat_Checked(object sender, RoutedEventArgs e)
        {
            var cbx = (CheckBox)sender;
            if (cbx.DataContext is not Seat seat) return;
            if (LastSeatChecked != null) LastSeatChecked.IsChecked = false;
            LastSeatChecked = seat;
        }

        private void Seat_Unchecked(object sender, RoutedEventArgs e)
        {
            var cbx = (CheckBox)sender;
            if (cbx.DataContext is not Separator seat) return;
            if (LastSeatChecked != null) LastSeatChecked.IsChecked = false;
            LastSeatChecked = null;
        }

        #endregion
        
        #region Kitchen
        
        private void MultiKitchen_Click(object sender, RoutedEventArgs e)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.SendProductsToDispatchAreas, (isValid, validUser) =>
            {
                if (isValid)
                {
                    _multiOrderSelectionFilterCondition = order => order.AvailableToMarch;
                    _multiOrderTableSelectionFilterCondition = table => table.NotMarch;

                    _multiSelectionOkAction = () =>
                    {
                        var orders = CurrentTicket.Orders.Where(o => o.IsVisible && o.IsSelected).ToList();
                        Trace.TraceInformation($"Dispatching selected orders ({orders.Count})");
                        DisableAllButtons();
                        SendToKitchen(orders, _ => { EnableAllButtons(); });
                    };

                    if (IsEmptyMultipleSelection())
                        EmptySetShowDialog(true, MiddleScreen);
                    else
                        MultipleSelection_Checked();
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        
        private async void SendNewDispatchToKitchen(IList<Order> orders, Action<bool> callback)
        {
            if (orders.Count <= 0 || !(CurrentTicket.DataContext is POSTicketContract contract))
            {
                callback?.Invoke(false);
                return;
            }

            try
            {
                Trace.TraceInformation($"March to kitchen Num:{contract.OpeningNumber} Ticket:{contract.SerieName}");
                //foreach (var productLine in contract.ProductLineByTicket)
                //    Trace.TraceInformation($"Product line Id:{productLine.Id}");
                try
                {
                    mainPage.BusyControl.Show($"Dispatching {orders.Count} products ...");

                    foreach (var order in orders)
                    {
                        Trace.TraceInformation($"Order Id:{order.Id}");
                        var productLine = contract.ProductLineByTicket.FirstOrDefault(pl => (Guid)pl.Id == order.Id);
                        if (productLine != null)
                        {
                            if (order.IsMenu)
                            {
                                foreach (var tableOrder in order.OrderTables.Where(t => t.IsSelected && t.NotMarch))
                                {
                                    var productTableLine = productLine.TableLines.FirstOrDefault(tl => (Guid)tl.Id == tableOrder.Id);
                                    if (productTableLine != null)
                                    {
                                        productTableLine.AreaId = tableOrder.Areas.LastOrDefault()?.AreaId;
                                        productTableLine.Areas = tableOrder.Areas;
                                    }
                                }
                            }
                            else
                            {
                                productLine.AreaId = order.AreaId;
                                productLine.Areas = order.Areas;
                            }
                        }
                        else
                            Trace.TraceInformation($"Product line not found Id:{order.Id}");
                    }

                    // Send in ticket user who dispatched
                    CurrentTicket.LastUserDispatched = Hotel.Instance.CurrentUserModel.Description;

                    var ordersId = SendNewDispatchMessages(CurrentTicket, orders).ToList();
                    if (ordersId.Count <= 0) return;

                    var result = await BusinessProxyRepository.Ticket.DispatchTicketOrdersAsync(contract, ordersId);
                    if (result.IsEmpty)
                    {
                        // Unselect after print
                        foreach (var order in orders)
                        {
                            order.IsSelected = false;
                            if (order.IsMenu)
                            {
                                foreach (var orderTable in order.OrderTables)
                                {
                                    orderTable.IsSelected = false;
                                }
                            }
                        }

                        CurrentTicket.DataContext = result.Contract as POSTicketContract;
                        CurrentTicket.RecalculateAll();
                        callback.Invoke(true);
                        
                        if (Hotel.Instance.AutoTableAfterAllToKitchen)
                        {
                            var action = mainPage.PopCloseAction();
                            action();
                        }
                        
                    }
                    else
                    {
                        callback.Invoke(false);
                        WindowHelper.ShowError(result);
                    }
                }
                finally
                {
                    mainPage.BusyControl.Close();
                }
             }
            catch (Exception e)
            {
                WindowHelper.ShowError(e);
            }
        }

        private void SendToKitchen(IList<Order> orders, Action<bool> callback)
        {
            var readyAreaOrders = new List<Order>();
            var checkAreaOrders = new ObservableCollection<Order>();

            checkAreaOrders.GetCollectionView().Filter = x => ((Order)x).PendingToChooseArea;

            #region Fill lists

            foreach (var order in orders.Where(o => o.NotMarch))
            {
                if (order.IsTip || !order.IsActive)
                    continue;

                if (!order.IsMenu)
                {
                    var count = order.Product.Areas.Count;
                    if (count <= 0) continue;

                    foreach (var area in order.Product.Areas.Where(a => a.Mandatory))
                        order.Areas.Add(new POSProductLineAreaContract(Guid.NewGuid(), area.Id));

                    if (order.Product.Areas.All(a => a.Mandatory))
                        readyAreaOrders.Add(order);

                    var areas = order.Product.Areas.Where(a => !a.Mandatory).ToList();

                    count = areas.Count;
                    if (count <= 0) continue;
                    {
                        if (count == 1)
                        {
                            var area = areas.First();
                            order.AreaId = area.Id;
                            order.Areas.Add(new POSProductLineAreaContract(Guid.NewGuid(), area.Id));
                            readyAreaOrders.Add(order);
                        }
                        else
                            checkAreaOrders.Add(order);
                    }
                }
                else
                {
                    var tableAreasToDispatch = new TypedList<POSProductLineAreaContract>();
                    var hasTableOrdersWithMultipleAreas = false;
                    foreach (var tableOrder in order.OrderTables.Where(t => t.IsSelected && t.NotMarch))
                    {
                        var count = tableOrder.Product?.Areas.Length ?? 0;
                        if (count <= 0) continue;

                        foreach (var area in tableOrder.Product.Areas.Where(a => a.Mandatory))
                            tableOrder.Areas.Add(new POSProductLineAreaContract(Guid.NewGuid(), area.Id));


                        var areas = tableOrder.Product.Areas.Where(a => !a.Mandatory).ToList();
                        count = areas.Count;
                        if (count > 0)
                        {
                            if (count == 1)
                            {
                                var area = areas.First();
                                tableOrder.AreaId = area.Id;
                                tableOrder.Areas.Add(new POSProductLineAreaContract(Guid.NewGuid(), area.Id));
                            }
                            else
                            {
                                order.OrderTablesPendingToDispatch.Add(tableOrder);
                                hasTableOrdersWithMultipleAreas = true;
                            }
                        }

                        foreach (var tableArea in tableOrder.Areas)
                        {
                            if (tableAreasToDispatch.FirstOrDefault(x => x.AreaId == tableArea.AreaId) == null)
                            {
                                tableAreasToDispatch.Add(tableArea);
                            }
                        }
                    }

                    // Have in the main order the areas to which it is going to be dispatched
                    order.Areas = tableAreasToDispatch;

                    if (!hasTableOrdersWithMultipleAreas)
                    {
                        readyAreaOrders.Add(order);
                    }
                    else
                    {
                        checkAreaOrders.Add(order);
                    }
                }
            }

            #endregion

            #region Just one area orders

            if (readyAreaOrders.Count > 0 && checkAreaOrders.Count == 0)
                SendNewDispatchToKitchen(readyAreaOrders, callback);

            #endregion

            #region Multiple area orders

            else if (checkAreaOrders.Count > 0)
            {
                bool OkAction()
                {
                    if (checkAreaOrders.GetCollectionView().Count <= 0) return true;
                    WindowHelper.ShowError("PickAreaForEachProduct".Translate());
                    return false;
                }

                var areaPicker = new PickerAreasOrder(checkAreaOrders);
                areaPicker.PickArea += (order, area) =>
                {
                    order.Areas.Add(new POSProductLineAreaContract(Guid.NewGuid(), area.Id));
                    order.AreaId = area.Id;

                    if (!order.IsMenu)
                    {
                        if (!order.AreaId.HasValue) return;
                        readyAreaOrders.Add(order);
                        var collectionView = checkAreaOrders.GetCollectionView();
                        collectionView.Refresh();

                        if (collectionView.Count != 0) return;

                        _forcePickingOkAction();
                        SendNewDispatchToKitchen(readyAreaOrders, callback);
                    }
                    else
                    {
                        order.OrderTablesPendingToDispatch.Remove(x => x.AreaId != null && x.AreaId != Guid.NewGuid());
                        if (order.OrderTablesPendingToDispatch.Any()) return;
                        readyAreaOrders.Add(order);
                        var collectionView = checkAreaOrders.GetCollectionView();
                        collectionView.Refresh();

                        if (collectionView.Count != 0) return;

                        _forcePickingOkAction();
                        SendNewDispatchToKitchen(readyAreaOrders, callback);
                    }
                };
                _forcePickingOkAction = WindowHelper.OpenDialog(areaPicker, false, null, OkAction, MiddleScreen);

                // foreach (var order in orders)
                // {
                //     order.PropertyChanged += (s, ev) =>
                //     {
                //         if (ev.PropertyName != nameof(Order.AreaId)) return;
                //
                //         if (!order.IsMenu)
                //         {
                //             if (!order.AreaId.HasValue) return;
                //             readyAreaOrders.Add(order);
                //             var collectionView = checkAreaOrders.GetCollectionView();
                //             collectionView.Refresh();
                //
                //             if (collectionView.Count != 0) return;
                //
                //             _forcePickingOkAction();
                //             SendNewDispatchToKitchen(readyAreaOrders, callback);
                //         } else
                //         {
                //             order.OrderTablesPendingToDispatch.Remove(x => x.AreaId != null && x.AreaId != Guid.NewGuid());
                //             if (order.OrderTablesPendingToDispatch.Any()) return;
                //             readyAreaOrders.Add(order);
                //             var collectionView = checkAreaOrders.GetCollectionView();
                //             collectionView.Refresh();
                //
                //             if (collectionView.Count != 0) return;
                //             
                //             _forcePickingOkAction();
                //             SendNewDispatchToKitchen(readyAreaOrders, callback);
                //         }
                //     };
                // }
            }
            else
            {
                callback(false);
            }

            #endregion
        }

        private void SendAllToKitchen()
        {
            var orders = CurrentTicket.Orders.Where(o => o.IsVisible && o.NotMarch).ToList();

            foreach (var order in orders)
            {
                if (order.IsMenu)
                {
                    foreach (OrderTable tableOrder in order.OrderTables.Where(x => x.NotMarch))
                    {
                        tableOrder.IsSelected = true;
                    }
                }
            }

            Trace.TraceInformation($"Dispatching all orders ({orders.Count})");
            DisableAllButtons();
            SendToKitchen(orders, _ => { EnableAllButtons(); });
        }

        private void KitchenAll_Click(object sender, RoutedEventArgs e)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.SendProductsToDispatchAreas, (isValid, validUser) =>
            {
                if (isValid) 
                    SendAllToKitchen();
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        private async void SendNewDispatchToAway(IList<Order> orders, Action<bool> callback)
        {
            if (orders.Count <= 0 || !(CurrentTicket.DataContext is POSTicketContract contract))
            { 
                callback?.Invoke(false);
                return;
            }
            try
            {
                Trace.TraceInformation($"March to away Num:{contract.OpeningNumber} Ticket:{contract.SerieName}");

                try
                {
                    mainPage.BusyControl.Show($"Dispatching {orders.Count} products ...");

                    foreach (var order in orders)
                    {
                        Trace.TraceInformation($"Order Id:{order.Id}");
                        var productLine = contract.ProductLineByTicket.FirstOrDefault(pl => (Guid)pl.Id == order.Id);
                        if (productLine != null)
                        {
                            if (order.IsMenu)
                            {
                                foreach (var tableOrder in order.OrderTables.Where(t => t.IsSelected && t.March))
                                {
                                    var productTableLine = productLine.TableLines.FirstOrDefault(tl => (Guid)tl.Id == tableOrder.Id);
                                    if (productTableLine != null)
                                    {
                                        productTableLine.AreaId = tableOrder.Areas.LastOrDefault()?.AreaId;
                                        productTableLine.Areas = tableOrder.Areas;
                                    }
                                }
                            }
                            else
                            {
                                productLine.AreaId = order.AreaId;
                                productLine.Areas = order.Areas;
                            }
                        }
                        else
                            Trace.TraceInformation($"Product line not found Id:{order.Id}");
                    }

                    // Send in ticket user who dispatched
                    CurrentTicket.LastUserDispatched = Hotel.Instance.CurrentUserModel.Description;

                    var ordersId = SendAwayDispatchMessages(CurrentTicket, orders).ToList();
                    if (ordersId.Count <= 0) return;

                    var dispatches = orders
                        .Select(order => new DispatchContract
                        {
                            OrderId = order.Id,
                            OrderIds = order.OrderTables?
                                .Where(x => x.IsSelected)
                                .Select(orderTable => new DispatchContract
                                {
                                    OrderId = orderTable.Id
                                })
                                .ToArray() ?? []
                        })
                        .ToList();

                    var result = await BusinessProxyRepository.Ticket.AwayDispatchTicketOrdersAsync(contract, dispatches);
                    if (result.IsEmpty)
                    {
                        // Unselect after print
                        foreach (var order in orders)
                        {
                            order.IsSelected = false;
                            if (!order.IsMenu) continue;
                            foreach (var orderTable in order.OrderTables)
                            {
                                orderTable.IsSelected = false;
                            }
                        }

                        CurrentTicket.DataContext = result.Contract as POSTicketContract;
                        CurrentTicket.RecalculateAll();
                        callback.Invoke(true);
                    }
                    else
                    {
                        callback.Invoke(false);
                        WindowHelper.ShowError(result);
                    }
                }
                finally
                {
                    mainPage.BusyControl.Close();
                }
            }
            catch (Exception e)
            {
                WindowHelper.ShowError(e);
            }

        }

        private void SendToAway(IList<Order> orders, Action<bool> callback)
        {
            var readyAreaOrders = new List<Order>();
            var checkAreaOrders = new ObservableCollection<Order>();

            checkAreaOrders.GetCollectionView().Filter = x => ((Order)x).PendingToChooseArea;

            #region Fill lists

            foreach (var order in orders.Where(o => o.March))
            {
                if (order.IsTip || !order.IsActive)
                    continue;

                if (!order.IsMenu)
                {
                    var count = order.Product.Areas.Count;
                    if (count <= 0) continue;

                    foreach (var area in order.Product.Areas.Where(a => a.Mandatory))
                        order.Areas.Add(new POSProductLineAreaContract(Guid.NewGuid(), area.Id));

                    if (order.Product.Areas.All(a => a.Mandatory))
                        readyAreaOrders.Add(order);

                    var areas = order.Product.Areas.Where(a => !a.Mandatory).ToList();

                    count = areas.Count;
                    if (count <= 0) continue;
                    {
                        if (count == 1)
                        {
                            var area = areas.First();
                            order.AreaId = area.Id;
                            order.Areas.Add(new POSProductLineAreaContract(Guid.NewGuid(), area.Id));
                            readyAreaOrders.Add(order);
                        }
                        else
                            checkAreaOrders.Add(order);
                    }
                }
                else
                {
                    var tableAreasToDispatch = new TypedList<POSProductLineAreaContract>();
                    var hasTableOrdersWithMultipleAreas = false;
                    foreach (var tableOrder in order.OrderTables.Where(t => t.IsSelected && t.March))
                    {
                        var count = tableOrder.Product?.Areas.Length ?? 0;
                        if (count <= 0) continue;

                        foreach (var area in tableOrder.Product.Areas.Where(a => a.Mandatory))
                            tableOrder.Areas.Add(new POSProductLineAreaContract(Guid.NewGuid(), area.Id));


                        var areas = tableOrder.Product.Areas.Where(a => !a.Mandatory).ToList();
                        count = areas.Count;
                        if (count > 0)
                        {
                            if (count == 1)
                            {
                                var area = areas.First();
                                tableOrder.AreaId = area.Id;
                                tableOrder.Areas.Add(new POSProductLineAreaContract(Guid.NewGuid(), area.Id));
                            }
                            else
                            {
                                order.OrderTablesPendingToDispatch.Add(tableOrder);
                                hasTableOrdersWithMultipleAreas = true;
                            }
                        }

                        foreach (var tableArea in tableOrder.Areas)
                        {
                            if (tableAreasToDispatch.FirstOrDefault(x => x.AreaId == tableArea.AreaId) == null)
                            {
                                tableAreasToDispatch.Add(tableArea);
                            }
                        }
                    }

                    // Have in the main order the areas to which it is going to be dispatched
                    order.Areas = tableAreasToDispatch;

                    if (!hasTableOrdersWithMultipleAreas)
                    {
                        readyAreaOrders.Add(order);
                    }
                    else
                    {
                        checkAreaOrders.Add(order);
                    }
                }
            }

            #endregion

            #region Just one area orders

            if (readyAreaOrders.Count > 0 && checkAreaOrders.Count == 0)
                SendNewDispatchToAway(readyAreaOrders, callback);
            #endregion

            #region Multiple area orders

            else if (checkAreaOrders.Count > 0)
            {
                bool OkAction()
                {
                    if (checkAreaOrders.GetCollectionView().Count <= 0) return true;
                    WindowHelper.ShowError("PickAreaForEachProduct".Translate());
                    return false;
                }

                var areaPicker = new PickerAreasOrder(checkAreaOrders);
                areaPicker.PickArea += (order, area) =>
                {
                    order.Areas.Add(new POSProductLineAreaContract(Guid.NewGuid(), area.Id));
                    order.AreaId = area.Id;

                    if (!order.IsMenu)
                    {
                        if (!order.AreaId.HasValue) return;
                        readyAreaOrders.Add(order);
                        var collectionView = checkAreaOrders.GetCollectionView();
                        collectionView.Refresh();

                        if (collectionView.Count != 0) return;

                        _forcePickingOkAction();
                        SendNewDispatchToAway(readyAreaOrders, callback);
                    }
                    else
                    {
                        order.OrderTablesPendingToDispatch.Remove(x => x.AreaId != null && x.AreaId != Guid.NewGuid());
                        if (order.OrderTablesPendingToDispatch.Any()) return;
                        readyAreaOrders.Add(order);
                        var collectionView = checkAreaOrders.GetCollectionView();
                        collectionView.Refresh();

                        if (collectionView.Count != 0) return;

                        _forcePickingOkAction();
                        SendNewDispatchToAway(readyAreaOrders, callback);
                    }
                };
                _forcePickingOkAction = WindowHelper.OpenDialog(areaPicker, false, null, OkAction, MiddleScreen);

            }
            else
            {
                callback(false);
            }
            #endregion
        }

        private void SendToAway()
            {
                var orders = CurrentTicket.Orders.Where(o => o.IsVisible && o.March).ToList();

                foreach (var order in orders)
                {
                    order.IsSelected = true;
                    if (order.IsMenu)
                    {
                        foreach (OrderTable tableOrder in order.OrderTables.Where(x => x.March))
                        {
                            tableOrder.IsSelected = true;
                        }
                    }
                }

                Trace.TraceInformation($"Away all orders ({orders.Count})");
                DisableAllButtons();
                SendToAway(orders, _ => { EnableAllButtons(); });
            }

        private void AwayAll_Click(object sender, RoutedEventArgs e)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.SendProductsToDispatchAreas, (isValid, validUser) =>
            {
                if (isValid)
                    SendToAway();
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        private void MultiAway_Click(object sender, RoutedEventArgs e)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.SendProductsToDispatchAreas, (isValid, validUser) =>
            {
                if (isValid)
                {
                    _multiOrderSelectionFilterCondition = order => order.AvailableToVoid;
                    _multiOrderTableSelectionFilterCondition = table => table.March;

                    _multiSelectionOkAction = () =>
                    {
                        var orders = CurrentTicket.Orders.Where(o => o.IsSelected && o.IsVisible).ToList();
                        DisableAllButtons();
                        SendNewDispatchToAway(orders, _ => { EnableAllButtons(); });
                    };

                    if (IsEmptyMultipleSelection())
                        EmptySetShowDialog(true, MiddleScreen);
                    else
                        MultipleSelection_Checked();
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        private void Monitor_Loaded(object sender, RoutedEventArgs e)
        {
            if (Hotel.Instance.CurrentStandCashierContext.Stand.IsRoomService)
            {
                var uie = (UIElement)sender;
                uie.Visibility = Visibility.Visible;
            }
        }

        private async void NotifyRoomServiceMonitor(POSTicketContract contract)
        {
            ValidationResult<string> result = null;

            mainPage.BusyControl.Show($"Sending to service monitor ...");
            try
            {
                result = await BusinessProxyRepository.Ticket.NotifyRoomServiceMonitorAsync(contract);
            }
            finally
            {
                mainPage.BusyControl.Close();
            }

            if (result != null)
            {
                if (!result.IsEmpty)
                    WindowHelper.ShowError(result.ToString());
                else
                {
                    contract.RoomServiceNumber = result.Result;
                    SendAllToKitchen();

                    WindowHelper.ShowInfo(contract.RoomServiceNumber, "RoomServiceNumber".Translate());
                }
            }
        }

        private void Monitor_Click(object sender, RoutedEventArgs e)
        {
            var contract = CurrentTicket.AsContract();
            if (!string.IsNullOrEmpty(contract.RoomServiceNumber))
                WindowHelper.ShowInfo($"{"TickedSendedToServiceMonitor".Translate()}: {contract.RoomServiceNumber}", "RoomServiceNumber".Translate());
            else
            {
                mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.SendProductsToDispatchAreas, (isValid, validUser) =>
                {
                    var roomNumberPicker = new RoomNumberPicker();

                    bool OkAction()
                    {
                        NotifyRoomServiceMonitor(contract);

                        return true;
                    }

                    void CancelAction()
                    {
                        contract.Room = null;
                    }

                    WindowHelper.OpenDialog(roomNumberPicker, false, contract, OkAction, MiddleScreen, DialogButtons.OkCancel, CancelAction);
                });
            }
        }
        #endregion

        #region Voids

        private void VoidAll_Click(object sender, RoutedEventArgs e)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.VoidProductsToDispatchArea, (isValid, validUser) =>
            {
                if (isValid)
                {
                    var orders = CurrentTicket.Orders.Where(o => o.AvailableToVoid && o.IsVisible).ToList();
                    foreach (var order in orders)
                    {
                        if (order.IsMenu)
                        {
                            foreach (var orderTable in order.OrderTables.Where(ot => !ot.NotMarch))
                            {
                                orderTable.IsSelected = true;
                            }
                        }
                    }

                    
                    DisableAllButtons();
                    SendVoidDispatchToKitchen(CurrentTicket, orders, _ => { EnableAllButtons(); });
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        private void MultiVoid_Click(object sender, RoutedEventArgs e)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.VoidProductsToDispatchArea, (isValid, validUser) =>
            {
                if (isValid)
                {
                    _multiOrderSelectionFilterCondition = order => order.AvailableToVoid;
                    _multiOrderTableSelectionFilterCondition = table => !table.NotMarch;

                    _multiSelectionOkAction = () =>
                    {
                        var orders = CurrentTicket.Orders.Where(o => o.IsSelected && o.IsVisible).ToList();
                        DisableAllButtons();
                        SendVoidDispatchToKitchen(CurrentTicket, orders, _ => { EnableAllButtons(); });
                    };

                    if (IsEmptyMultipleSelection())
                        EmptySetShowDialog(true, MiddleScreen);
                    else
                        MultipleSelection_Checked();
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        private async void SendVoidDispatchToKitchen(Ticket ticket, IList<Order> orders, Action<bool> callback)
        {
            if (orders.Count <= 0 || !(CurrentTicket.DataContext is POSTicketContract contract))
            {
                callback?.Invoke(false);
                return;
            }

            try
            {
                mainPage.BusyControl.Show($"Dispatching {orders.Count} products ...");

                // Send in ticket user who dispatched
                CurrentTicket.LastUserDispatched = Hotel.Instance.CurrentUserModel.Description;

                var ordersId = SendVoidDispatchMessages(ticket, orders).ToList();
                if (ordersId.Count <= 0) return;

                foreach (var order in orders)
                {
                    Trace.TraceInformation($"Order Id:{order.Id}");
                    var productLine = contract.ProductLineByTicket.FirstOrDefault(pl => (Guid)pl.Id == order.Id);
                    if (productLine != null)
                    {
                        if (order.IsMenu)
                        {
                            foreach (var tableOrder in order.OrderTables.Where(t => t.IsSelected && !t.NotMarch))
                            {
                                var tableLine = productLine.TableLines.FirstOrDefault(tl => (Guid)tl.Id == tableOrder.Id);
                                if (tableLine != null)
                                {
                                    tableLine.AreaId = null;
                                    tableLine.Areas.Clear();
                                }
                            }
                        }
                        else
                        {
                            productLine.AreaId = null;
                            productLine.Areas.Clear();
                        }
                    }
                    else
                        Trace.TraceInformation($"Product line not found Id:{order.Id}");
                }
               
                try
                {
                    var result = await BusinessProxyRepository.Ticket.VoidDispatchTicketOrdersAsync(contract, ordersId);
                    if (result.IsEmpty)
                    {
                        // Unselect after print
                        foreach(var order in orders)
                        {
                            order.IsSelected = false;
                            if (order.IsMenu)
                            {
                                foreach(var orderTable in order.OrderTables)
                                {
                                    orderTable.IsSelected = false;
                                }
                            }
                        }

                        CurrentTicket.DataContext = result.Contract as POSTicketContract;
                        CurrentTicket.RecalculateAll();
                        callback?.Invoke(true);
                    }
                    else
                    {
                        callback?.Invoke(false);
                        WindowHelper.ShowError(result);
                    }
                }
                finally
                {
                    mainPage.BusyControl.Close();
                }
            }
            catch (Exception e)
            {
                WindowHelper.ShowError(e);
            }
        }

        #endregion
        
        #region Dispatched Orders
        
        private IEnumerable<Guid> GetDispatchedOrders(IEnumerable<Kitchen.Kitchen> kitchens, IList<Order> orders, IList<string> areas)
        {
            var dispatchedOrders = new HashSet<Guid>();
            var failedOrders = new HashSet<Guid>();

            foreach (var kitchen in kitchens)
            {
                foreach (var ticket in kitchen.Tickets)
                {
                    if (ticket.FailedOrders.Count > 0)
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(ticket.Area.Description))
                                areas.Add(ticket.Area.Description);

                            foreach (var order in ticket.FailedOrders)
                                failedOrders.Add(order.Id);
                        }
                        finally
                        {
                            ticket.FailedOrders.Clear();
                        }
                    }
                }
            }

            foreach (var order in orders)
            {
                if (!failedOrders.Contains(order.Id))
                    dispatchedOrders.Add(order.Id);
            }

            return dispatchedOrders;
        }

        private IEnumerable<Guid> SendNewDispatchMessages(Ticket ticket, IList<Order> orders)
        {
            var areas = new List<string>();
            var dispatchedOrders = GetDispatchedOrders(MainWindow.SendNewOrderToArea(ticket, orders, Hotel.Instance.CurrentStandCashierContext.Stand), orders, areas);

            if (areas.Count > 0)
                WindowHelper.ShowInfo(string.Join(Environment.NewLine, areas.Distinct()), "NotListeningAreas".Translate());

            return dispatchedOrders;
        }

        private IEnumerable<Guid> SendVoidDispatchMessages(Ticket ticket, IList<Order> orders)
        {
            var areas = new List<string>();
            var dispatchedOrders = GetDispatchedOrders(MainWindow.SendVoidOrderToArea(ticket, orders, Hotel.Instance.CurrentStandCashierContext.Stand), orders, areas);

            if (areas.Count > 0)
                WindowHelper.ShowInfo(string.Join(Environment.NewLine, areas.Distinct()), "NotListeningAreas".Translate());

            return dispatchedOrders;
        }

        private IEnumerable<Guid> SendAwayDispatchMessages(Ticket ticket, IList<Order> orders)
        {
            var areas = new List<string>();
            var dispatchedOrders = GetDispatchedOrders(MainWindow.SendAwayOrderToArea(ticket, orders, Hotel.Instance.CurrentStandCashierContext.Stand), orders, areas);

            if (areas.Count > 0)
                WindowHelper.ShowInfo(string.Join(Environment.NewLine, areas.Distinct()), "NotListeningAreas".Translate());

            return dispatchedOrders;
        }
        
        #endregion
        
        #endregion
        
        #region Order Details Operations

        private async void  TicketViewer_OrderDetailsAccepted(Ticket ticket, Order original, Order edited, Action<bool> callback)
        {
            mainPage.BusyControl.Show("Updating Order");
            try
            {
                var result = await BusinessProxyRepository.Ticket.ChangeTicketProductDetailsAsync(
                    ticket.DataContext as POSTicketContract, original.Id, edited.DataContext as POSProductLineContract);

                if (result.IsEmpty)
                {
                    ticket.DataContext = result.Contract as POSTicketContract;
                    ticket.RecalculateAll();

                    // Void dispatched orders for inactive product lines
                    var canceledOrders = ticket.Orders.Where(x => !x.IsActive && x.HasAreas).ToList();
                    foreach (var order in canceledOrders)
                    {
                        order.IsSelected = true;
                    }

                    if (canceledOrders.Any())
                    {
                        DisableAllButtons();
                        SendVoidDispatchToKitchen(CurrentTicket, canceledOrders, _ => { EnableAllButtons(); });
                    }

                    callback(true);
                }
                else
                {
                    callback(false);
                    WindowHelper.ShowError(result);
                }
            }
            finally
            {
                mainPage.BusyControl.Close();
            }
        }

        private void TicketViewer_OnOrderDetailsOpened()
        {
            // Inhabilitamos las acciones y los productos
            _ticketActions.IsEnabled = false;
            _ticketSeatActions.IsEnabled = false;
            _productsViewer.IsEnabled = false;
            MainPage.CurrentInstance.DisableAllButtons();
            MainPage.CurrentInstance.ProhibitClose();
        }
        
        private void TicketViewer_OnOrderDetailsClosed()
        {
            // Habilitamos las acciones y los productos
            _ticketActions.IsEnabled = true;
            _ticketSeatActions.IsEnabled = true;
            _productsViewer.IsEnabled = true;
            MainPage.CurrentInstance.EnableAllButtons();
            MainPage.CurrentInstance.AllowClose();
        }

        private void EnableAllButtons()
        {
            // Habilitamos las acciones y los productos
            _ticketActions.IsEnabled = true;
            _ticketSeatActions.IsEnabled = true;
            _productsViewer.IsEnabled = true;
            _ticketViewer.IsEnabled = true;
            MainPage.CurrentInstance.EnableAllButtons();
            MainPage.CurrentInstance.AllowClose();
        }
        
        private void DisableAllButtons()
        {
            // Inhabilitamos las acciones y los productos
            _ticketActions.IsEnabled = false;
            _ticketSeatActions.IsEnabled = false;
            _productsViewer.IsEnabled = false;
            _ticketViewer.IsEnabled = false;
            MainPage.CurrentInstance.DisableAllButtons();
            MainPage.CurrentInstance.ProhibitClose();
        }

        private void TicketViewer_OrderDetailsPickAreaRequested(TicketViewer arg1, Order order, Action<bool> action)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.SendProductsToDispatchAreas, (isValid, validUser) =>
            {
                if (isValid)
                {
                    _forPicking.Clear();
                    if (!SetOrderAreaOrForPicking(order))
                        WindowHelper.ShowError(string.Format("AllDispatchAreasDeactivated".Translate(), order.Product.Description), "Warning".Translate());
                    else
                        ShowPickDialogAndSendNewOrders(new List<object> { order }, true, action);
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        private void TicketViewer_OrderDetailsDispatchRequested(Order order, Action<bool> callback)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.SendProductsToDispatchAreas, (isValid, validUser) =>
            {
                if (isValid)
                {
                    Trace.TraceInformation("Sending dispatch order");
                    SendToKitchen(new List<Order> { order }, callback);
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        private void TicketViewer_OrderDetailsVoidDispatchRequested(Order order, Action<bool> callback)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.VoidProductsToDispatchArea, (isValid, validUser) =>
            {
                if (isValid)
                {
                    Trace.TraceInformation("Voiding dispatch order");
                    SendVoidDispatchToKitchen(CurrentTicket, new List<Order> { order }, callback);
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        #endregion
        
        #region Payments

        private void CollectTicket(CollectPanel cp, PaymentMethod method)
        {
            _enabledBarCodeCapture = false;
            cp.DataContext = CurrentTicket;
            if (CurrentTicket != null)
            {
                var stand = Hotel.Instance.CurrentStandCashierContext.Stand;
                CurrentTicket.InitializeTip(stand);
            }

            mainPage.PushCloseAction(async () =>
            {
                WindowHelper.ShowBusyDialog("Saving");
                try
                {
                    if (CurrentTicket is { IsClosed: false })
                    {
                        CurrentTicket.RestoreStatus(andKeepIt: false, keepDiscounts: true);
                        CurrentTicket.ClearCollectData();
                    }

                    mainPage.ClosePopUp();
                    EditorControl.Instance = _editorControl;

                    var persistTicketResult = await BusinessProxyRepository.Ticket.PersistTicketAsync(CurrentTicket.AsContract());
                    if (persistTicketResult.IsEmpty)
                        CurrentTicket.DataContext = persistTicketResult.Contract;
                    else
                        WindowHelper.ShowError(persistTicketResult);

                    _enabledBarCodeCapture = true;
                }
                finally
                {
                    WindowHelper.CloseBusyDialog();
                }
            });

            mainPage.OpenPopUp();

            if (method != null)
                cp.ActiveFastPaymentByCreditCard(method);
        }

        private async void Payment_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentTicket.HasVisibleOrders)
            {
                _enabledBarCodeCapture = false;
                var method = sender as PaymentMethod;

                var cp = (CollectPanel)mainPage.PopUpContent;
                // cp.Ticket.SaveStatus();
                CurrentTicket.DiscardSavedStatus();
				CurrentTicket.SaveStatus();

				cp.Stand = mainPage.Stand;
                cp.SplitTicket = null;
                cp.PaymentMethod = null;
                cp.FilteredPaymentMethods = cp.Stand.PaymentMethods;
                
                #region Alcoholic Split

                if (CurrentTicket.HasAlcoholicOrders && CurrentTicket.HasNonAlcoholicOrders)
                {
                    var splitTicketResult = await BusinessProxyRepository.Ticket.SplitTicketByAlcoholicGroupAsync(CurrentTicket.Id);
                    if (splitTicketResult.IsEmpty)
                    {
                        var originalTicket = splitTicketResult.Tickets[0];
                        var newTicket = new Ticket(splitTicketResult.Tickets[1]);

                        cp.SplitTicket = newTicket;
                        cp.PaymentMethod = method;

                        mainPage.Stand.AddTicket(newTicket);
                        if (newTicket.Table == null)
                            Hotel.Instance.CurrentStandCashierContext.QuickTickets.Add(newTicket);
                        mainPage.Stand.RemoveTicket(CurrentTicket);

                        CurrentTicket.DataContext = originalTicket;
                        CurrentTicket.RecalculateAll();
                        mainPage.Stand.AddTicket(CurrentTicket);

                        CollectTicket(cp, method);
                    }
                    else
                        WindowHelper.ShowError(splitTicketResult);
                }
                else
                    CollectTicket(cp, method);

                #endregion
            }
            else
                WindowHelper.ShowError("NotPayEmptyTicket".Translate());
        }

        private void Client_Click(object sender, RoutedEventArgs e)
        {
            _actionsCtrl.IsEnabled = false;

            void SelectAction()
            {
                _ticketViewer.UpdateOrdersView(true);

                UIThread.Invoke(async () =>
                {
                    if (CurrentTicket is not { HasLines: true, IsPersisted: true }) return;
                    var result = await BusinessProxyRepository.Ticket.PersistTicketAsync(CurrentTicket.AsContract());
                    if (result.IsEmpty)
                        CurrentTicket.DataContext = result.Contract;
                    else
                        WindowHelper.ShowError(result.ToString());
                });
            }

            bool OkAction()
            {
                _actionsCtrl.IsEnabled = true;
                return true;
            }

            var panel = new ClientsPanel(CurrentTicket, SelectAction);
            ShowEditorControl("Clients", panel, Colors.Black, OkAction, null, false);

            if (_currentPaxNumber is not > 0) return;
            var clientByPosition = CurrentTicket.ClientsByPosition.FirstOrDefault(cp => cp.Pax == _currentPaxNumber.Value);
            if (clientByPosition is { ClientId: not null })
                panel.Search(null, null, clientByPosition.ClientId.Value);
        }

        private void FastPaymentTicket(PaymentMethod type, long documentType, Action okAction = null)
        {
            if (type.IsCashPayment)
            {
                _enabledBarCodeCapture = false;
                try
                {
                    var cash = Payment.Create(CurrentTicket, type, CurrentTicket.TotalAmount, CurrentTicket.TotalAmount);
                    CurrentTicket.AddPayment(cash);

                    void CancelAction()
                    {
                        CurrentTicket.ClearPayments();
                    };

                    switch ((POSDocumentType)documentType)
                    {
                        case POSDocumentType.Ticket:
                            ShowTicketRequest(okAction, CancelAction);
                            break;
                        case POSDocumentType.Ballot:
                            ShowBallotTicketRequest(okAction, CancelAction);
                            break;
                        case POSDocumentType.CashInvoice:
                            ShowInvoiceTicketRequest(okAction, CancelAction);
                            break;
                        default:
                            throw new Exception("Document type not supported");
                    }
                }
                finally
                {
                    _enabledBarCodeCapture = true;
                }
            }
            else if (type.IsCreditCardPayment)
                Payment_Click(type, null);
        }

        private void FastPayment_Click(object sender, RoutedEventArgs e)
        {
            AppSecurity.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.EnableCashPayment, async (isValid, validUser) =>
            {
                if (isValid)
                {
                    if (CurrentTicket.HasVisibleOrders)
                    {
                        if (CurrentTicket.Payments.Count > 0)
                            WindowHelper.ShowError(string.Format("YouMustRemovePreviousPayments".Translate(), "Fast".Translate()), "Warning".Translate());
                        else
                        {
                            var fastPaymentType = Hotel.Instance.FastPaymentType;
                            if (fastPaymentType.HasValue)
                            {
                                var type = mainPage.Stand.PaymentMethods.FirstOrDefault(x => x.Id == fastPaymentType.Value);
                                if (type != null)
                                {
                                    long documentType = 0;
                                    var fe = (FrameworkElement)sender;
                                    if (fe.Tag != null)
                                    {
                                        if (!long.TryParse(fe.Tag.ToString(), out documentType))
                                            documentType = 0;
                                    }

                                    if (CurrentTicket.HasAlcoholicOrders && CurrentTicket.HasNonAlcoholicOrders)
                                    {
                                        var splitTicketResult = await BusinessProxyRepository.Ticket.SplitTicketByAlcoholicGroupAsync(CurrentTicket.Id);
                                        if (splitTicketResult.IsEmpty)
                                        {
                                            var originalTicket = splitTicketResult.Tickets[0];
                                            var newTicket = new Ticket(splitTicketResult.Tickets[1]);

                                            mainPage.Stand.AddTicket(newTicket);
                                            if (newTicket.Table == null)
                                                Hotel.Instance.CurrentStandCashierContext.QuickTickets.Add(newTicket);
                                            mainPage.Stand.RemoveTicket(CurrentTicket);
                                            
                                            CurrentTicket.DataContext = originalTicket;
                                            CurrentTicket.RecalculateAll();
                                            mainPage.Stand.AddTicket(CurrentTicket);

                                            void OkAction()
                                            {
                                                CurrentTicket = newTicket;
                                                FastPaymentTicket(type, documentType);
                                            };

                                            FastPaymentTicket(type, documentType, OkAction);
                                        }
                                        else
                                            WindowHelper.ShowError(splitTicketResult);
                                    }
                                    else
                                        FastPaymentTicket(type, documentType);
                                }
                            }
                        }
                    }
                    else
                        WindowHelper.ShowError("NotPayEmptyTicket".Translate());
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }
        
        private void FastMealPlan_Click(object sender, RoutedEventArgs e)
        {
            AppSecurity.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.EnableMealPlan, (isValid, _) =>
            {
                if (isValid)
                {
                    var ticket = CurrentTicket;
                    if (ticket.HasVisibleOrders)
                    {
                        if (ticket.HasPayments)
                        {
                            WindowHelper.ShowError(string.Format("YouMustRemovePreviousPayments".Translate(), "Fast".Translate()), "Warning".Translate());
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ticket.Room))
                            {
                                var amount = ticket.TotalAmount;

                                var stand = Hotel.Instance.CurrentStandCashierContext.Stand;
                                if (stand.PensionRate.HasValue)
                                {
                                    ticket.SetRateType(RateType.PensionMode);
                                    ticket.RecalculateAll();
                                }

                                var details = new StringBuilder();
                                // If the account description was filled previously then extract the data from there
                                var accountDescription = ticket.AccountDescription?.Split('\n');
                                var index = accountDescription.Length switch
                                {
                                    2 when ticket.AccountInstallationId == null => 0,
                                    3 when ticket.AccountInstallationId != null => 1,
                                    _ => -1
                                };

                                if (index != -1)
                                {
                                    var reservationDescription = accountDescription[index].Split(' ');
                                    if (reservationDescription.Length == 2)
                                    {
                                        details.AppendLine($"{"Reservation".Translate()}: {reservationDescription[0]}");
                                    }
                                }

                                details.AppendLine($"{"Room".Translate()}: {ticket.Room}");
                                details.AppendLine($"{"Titular".Translate()}: {ticket.FiscalDocInfoName}");
                                details.AppendLine($"{"Discount".Translate()}: {amount - ticket.TotalAmount}");

                                amount = decimal.Zero;
                                if (!Hotel.Instance.LeaveMealPlanOpened || ticket.TotalAmount == decimal.Zero)
                                    amount = ticket.TotalAmount;

                                var pay = Payment.CreateRoomPlan(ticket, "MealPlan".Translate(), details.ToString(), amount, stand.CurrencyId, ticket.Account);
                                ticket.AddPayment(pay);
                                
                                void CancelAction()
                                {
                                    ticket.ClearPayments();
                                }
                                
                                ShowTicketRequest(null, CancelAction);
                            } else if (!Hotel.Instance.IsMandatoryRoomInfo)
                            {
                                var amount = ticket.TotalAmount;

                                var stand = Hotel.Instance.CurrentStandCashierContext.Stand;
                                if (stand.PensionRate.HasValue)
                                {
                                    ticket.SetRateType(RateType.PensionMode);
                                    ticket.RecalculateAll();
                                }

                                var details = new StringBuilder();
                                details.AppendLine($"{"Discount".Translate()}: {amount - ticket.TotalAmount}");

                                amount = decimal.Zero;
                                if (!Hotel.Instance.LeaveMealPlanOpened || ticket.TotalAmount == decimal.Zero)
                                    amount = ticket.TotalAmount;

                                var pay = Payment.CreateRoomPlan(ticket, "MealPlan".Translate(), details.ToString(), amount, stand.CurrencyId, ticket.Account);
                                ticket.AddPayment(pay);
                                
                                void CancelAction()
                                {
                                    ticket.ClearPayments();
                                }
                                
                                ShowTicketRequest(null, CancelAction);
                            }
                            else if (EditorControl.Instance != null)
                            {
                                _planPanel.Init(ticket);

                                EditorControl.Instance.ShowRequest("MealPlan".Translate(), _planPanel, Colors.Black, () =>
                                {
                                    if (_planPanel.IsValid)
                                    {
                                        try
                                        {
                                            var amount = ticket.TotalAmount;

                                            var stand = Hotel.Instance.CurrentStandCashierContext.Stand;
                                            if (stand.PensionRate.HasValue)
                                            {
                                                ticket.SetRateType(RateType.PensionMode);
                                                ticket.RecalculateAll();
                                            }

                                            var item = _planPanel.SelectedPlan;

                                            var installationAccountId = _planPanel.InstallationId;
                                            var installationDescription = _planPanel.InstallationDescription;

                                            var accountDescription = new StringBuilder();
                                            if (installationAccountId.HasValue)
                                                accountDescription.AppendLine(installationDescription);
                                            accountDescription.AppendLine($"{item.ReservationNumber} ({item.Room})");
                                            accountDescription.Append(item.Guests);

                                            ticket.Account = item.AccountId;
                                            ticket.AccountInstallationId = installationAccountId;
                                            ticket.AccountType = CurrentAccountType.Reservation;
                                            ticket.AccountDescription = accountDescription.ToString();

                                            var details = new StringBuilder();
                                            details.AppendLine($"{"Reservation".Translate()}: {item.ReservationNumber}");
                                            details.AppendLine($"{"Room".Translate()}: {item.Room}");
                                            details.AppendLine($"{"Titular".Translate()}: {item.Guests}");
                                            details.AppendLine($"{"Discount".Translate()}: {amount - ticket.TotalAmount}");

                                            amount = decimal.Zero;
                                            if (!Hotel.Instance.LeaveMealPlanOpened || ticket.TotalAmount == decimal.Zero)
                                                amount = ticket.TotalAmount;

                                            var pay = Payment.CreateRoomPlan(ticket, "MealPlan".Translate(), details.ToString(), amount, stand.CurrencyId, item.AccountId);
                                            ticket.AddPayment(pay);
                                            
                                            void CancelAction()
                                            {
                                                ticket.ClearPayments();
                                            }
                                
                                            ShowTicketRequest(null, CancelAction);

                                            return true;
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorsWindow.ShowErrorWindows(ex);
                                            return false;
                                        }
                                    }

                                    WindowHelper.ShowError("InvalidData".Translate(), "Error".Translate());
                                    return false;
                                });
                            }
                        }
                    }
                    else
                        WindowHelper.ShowError("NotPayEmptyTicket".Translate());
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        private void ShowInvoiceTicketRequest(Action afterOkAction = null, Action afterCancelAction = null, bool fastInvoice = false)
        {
            AppSecurity.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.CloseTickets, async (isValid, validUser) =>
            {
                if (isValid) 
                {
                    if (!CurrentTicket.AllowPrintInvoice)
                    {
                        WindowHelper.ShowError("CanOnlyInvoiceTicketWhenPayCashCard".Translate());
                        afterCancelAction?.Invoke();
                    }
                    else
                    {
                        // If the DocumentSign is None, then the invoice can be closed without payments.
                        // This code is to force adding  at least one payment, probably the amount is 0
                        //if (!CurrentTicket.HasPayments)
                        //{
                        //    var type = mainPage.Stand.PaymentMethods.FirstOrDefault(x => x.IsCashPayment);
                        //    var cash = Payment.Create(CurrentTicket, type, CurrentTicket.TotalAmount);
                        //    CurrentTicket.AddPayment(cash);
                        //}

                        var contract = CurrentTicket.AsContract();

                        bool OkInvoice()
                        {
                            // If this boolean is set to true then the invoice will be filled with the final consumer
                            if (fastInvoice)
                            {
                                CancelFillFiscalData();
                                return true;
                            }
                            else {
                                var content = new TextBlock { Text = "FillInvoiceData".Translate(LocalizedSuffix.Message), FontSize = 16 };
                                WindowHelper.OpenDialog(content, OkFillFiscalData, DialogButtons.OkCancel, CancelFillFiscalData);
                                return false;
                            }

                            void CancelFillFiscalData()
                            {
                                // Put the final consumer
                                var finalCustomerBuyerInfo = BuyerInfo.CreateFinalCustomerBuyer(POSDocumentType.CashInvoice);
                                CloseAndShowFiscalDoc(contract, finalCustomerBuyerInfo, false, afterOkAction);
                            }

                            bool OkFillFiscalData()
                            {   
                                var buyerInfo = BuyerInfo.CreateBuyer(POSDocumentType.CashInvoice, CurrentTicket,
                                    FiscalValidations.HasFinalCustomer(Hotel.Instance.Country));
                                var buyerInfoFiller = new BuyerInfoFillerControl();

                                buyerInfoFiller.OnDoubleClick += () => { OkAction(); };

                                WindowHelper.OpenDialog(buyerInfoFiller, true, buyerInfo, OkAction, MiddleScreen, DialogButtons.OkCancel, afterCancelAction);

                                return false;

                                bool OkAction()
                                {
                                    if (buyerInfoFiller.IsFillState)
                                        CloseAndShowFiscalDoc(contract, buyerInfoFiller.BuyerInfo, true, afterOkAction);
                                    else
                                        buyerInfoFiller.SetFillState();

                                    return false;
                                }
                            }
                        }

                        void CancelInvoice() => afterCancelAction?.Invoke();

                        // Panama and Colombia want to invoice and audit the generated document online
                        // As a patch solution we will ask first if the user can connect with the pms
                        // In case the user cannot connect, we proceed as usually
                        if (Hotel.Instance.SignType == DocumentSign.FiscalizationColombiaBtw || Hotel.Instance.SignType == DocumentSign.FiscalizationPanamaHKA)
                        {
                            BusyControl.Instance.Show();
                            var result = await BusinessProxyRepository.Stand.GetChargeHotelsAsync();
                            BusyControl.Instance.Close();
                            if (result.HasErrors)
                            {
                                WindowHelper.OpenDialog(new TextBlock { Text = "NotOnlineAudit".Translate(LocalizedSuffix.Message), FontSize = 16 }, () =>
                                {
                                    WindowHelper.OpenDialog(new TextBlock { Text = "DoInvoice".Translate(LocalizedSuffix.Message), FontSize = 16 }, OkInvoice, DialogButtons.OkCancel, CancelInvoice);

                                    return false;
                                }, DialogButtons.OkOnly);

                                return;
                            }
                        }

                        WindowHelper.OpenDialog(new TextBlock { Text = "DoInvoice".Translate(LocalizedSuffix.Message), FontSize = 16 }, OkInvoice, DialogButtons.OkCancel, CancelInvoice);
                    }
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        private void ShowTicketRequest(Action afterOkAction = null, Action afterCancelAction = null)
        {
            AppSecurity.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.CloseTickets, (isValid, validUser) =>
            {
                if (isValid)
                {
                    if (!CurrentTicket.AllowPrint)
                    {
                        WindowHelper.ShowError("InvoiceTicketWhenPayCashCard".Translate());
                        afterCancelAction?.Invoke();
                    }
                    else if (Hotel.Instance.FiscalNumberOnTickets)
                    {
                        // If the DocumentSign is None, then the invoice can be closed without payments.
                        // This code is to force adding  at least one payment, probably the amount is 0
                        //if (!CurrentTicket.HasPayments)
                        //{
                        //    var type = mainPage.Stand.PaymentMethods.FirstOrDefault(x => x.IsCashPayment);
                        //    var cash = Payment.Create(CurrentTicket, type, CurrentTicket.TotalAmount);
                        //    CurrentTicket.AddPayment(cash);
                        //}

                        var contract = CurrentTicket.AsContract();
                        
                        bool OkTicket()
                        {
                            var buyerInfoFiller = new BuyerInfoFillerControl();
                            var buyerInfo = BuyerInfo.CreateBuyer(POSDocumentType.Ticket, CurrentTicket,
                                FiscalValidations.HasFinalCustomer(Hotel.Instance.Country));

                            bool OkAction()
                            {
                                if (buyerInfoFiller.IsFillState)
                                     CloseAndShowFiscalDoc(contract, buyerInfo, true, () => afterOkAction?.Invoke());
                                else
                                    buyerInfoFiller.SetFillState();

                                return false;
                            }

                            void CancellAction() => afterCancelAction?.Invoke();

                            buyerInfoFiller.OnDoubleClick += () =>
                            {
                                OkAction();
                            };

                            WindowHelper.OpenDialog(buyerInfoFiller, true, buyerInfo, OkAction, MiddleScreen, DialogButtons.OkCancel, CancellAction);
                            return false;
                        }

                        void CancelTicket() => CloseAndShowTicket();

                        var validator = GetValidator(contract);
                        if (validator.IsFiscalNumberMandatory(contract))
                            OkTicket();
                        else
                            WindowHelper.OpenDialog(new TextBlock { Text = $"{"FiscalNumber".Translate()}?", FontSize = 16 }, OkTicket, DialogButtons.OkCancel, CancelTicket);
                    }
                    else
                        CloseAndShowTicket();
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        private void ShowBallotTicketRequest(Action afterOkAction = null, Action afterCancelAction = null)
        {
            AppSecurity.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.CloseTickets, (isValid, validUser) =>
            {
                if (isValid)
                {
                    if (!CurrentTicket.AllowBallot)
                    {
                        WindowHelper.ShowError("InvoiceTicketWhenPayCashCard".Translate());
                        afterCancelAction?.Invoke();
                    }
                    else                    
                    {
                        // If the DocumentSign is None, then the invoice can be closed without payments.
                        // This code is to force adding  at least one payment, probably the amount is 0
                        //if (!CurrentTicket.HasPayments)
                        //{
                        //    var type = mainPage.Stand.PaymentMethods.FirstOrDefault(x => x.IsCashPayment);
                        //    var cash = Payment.Create(CurrentTicket, type, CurrentTicket.TotalAmount);
                        //    CurrentTicket.AddPayment(cash);
                        //}
                        
                        var contract = CurrentTicket.AsContract();

                        var buyerInfoFiller = new BuyerInfoFillerControl();
                        var buyerInfo = BuyerInfo.CreateBuyer(POSDocumentType.Ballot, CurrentTicket,
                            FiscalValidations.HasFinalCustomer(Hotel.Instance.Country));

                        bool OkBallot()
                        {
                            if (buyerInfoFiller.IsFillState)
                                CloseAndShowFiscalDoc(contract, buyerInfo, true, () => afterOkAction?.Invoke());
                            else
                                buyerInfoFiller.SetFillState();

                            return false;
                        }

                        void CancellBallot() => afterCancelAction?.Invoke();

                        buyerInfoFiller.OnDoubleClick += () =>
                        {
                            OkBallot();
                        };

                        WindowHelper.OpenDialog(buyerInfoFiller, true, buyerInfo, OkBallot, MiddleScreen, DialogButtons.OkCancel, CancellBallot);
                    }
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        public void CreateNewTicketRequest(Ticket ticket)
        {
            if (Hotel.Instance.ApplyAutomaticProductDiscount)
            {
                var clientInfoFiller = new ClientInfoFillerControl();
                var buyerInfo = BuyerInfo.CreateBuyer(POSDocumentType.Ticket, CurrentTicket);

                bool OkAction()
                {
                    if (clientInfoFiller.IsFillState)
                    {
                        if (buyerInfo.AutomaticProductDiscount)
                        {
                            ticket.BaseEntityId = buyerInfo.ClientId;
                            ticket.FiscalDocInfoName = buyerInfo.Name;
                            ticket.FiscalDocFiscalNumber = buyerInfo.FiscalNumber;
                            ticket.FiscalDocInfoAddress = buyerInfo.Address;
                            ticket.FiscalDocNacionality = buyerInfo.SelectedNationality != null ? buyerInfo.SelectedNationality.CountryCode : null;
                            ticket.FiscalDocEmail = buyerInfo.FiscalMail;
                            ticket.AutomaticProductDiscount = buyerInfo.AutomaticProductDiscount;
                        }

                        return true;
                    }

                    clientInfoFiller.SetFillState();
                    return false;
                }

                clientInfoFiller.OnDoubleClick += () =>
                {
                    if (OkAction())
                        WindowHelper.CloseDialog();
                };

                WindowHelper.OpenDialog(clientInfoFiller, true, buyerInfo, OkAction, MiddleScreen, DialogButtons.OkCancel);
            }
        }

        private void CollectPanel_OkClick(CollectPanel cp)
        {
            _enabledBarCodeCapture = true;
            if (cp.SplitTicket != null)
            {
                CurrentTicket = cp.SplitTicket;
                CurrentTicket.RecalculateAll();
                CollectTicket(cp, cp.PaymentMethod);
                cp.SplitTicket = null;
                cp.PaymentMethod = null;
            }
        }

        private void CollectPanel_TicketClick(CollectPanel cp, Action afterCancelAction = null, Action afterOkAction = null)
        {
            ShowTicketRequest(afterOkAction, afterCancelAction);
        }

        private void CollectPanel_BallotClick(CollectPanel cp, Action afterCancelAction = null, Action afterOkAction = null)
        {
            ShowBallotTicketRequest(afterOkAction, afterCancelAction);
        }

        private void CollectPanel_InvoiceClick(CollectPanel cp, Action afterCancelAction = null, Action afterOkAction = null)
        {
            ShowInvoiceTicketRequest(afterOkAction, afterCancelAction);
        }

        private void CollectPanel_FastInvoiceClick(CollectPanel cp, Action afterCancelAction = null, Action afterOkAction = null)
        {
            ShowInvoiceTicketRequest(afterOkAction, afterCancelAction, true);
        }

        private async void CloseAndShowTicket(Action afterAction = null)
        {
            WindowHelper.ShowBusyDialog();
            try
            {
                try
                {
                    var ticket = CurrentTicket;
                    ticket.DocumentType = POSDocumentType.Ticket;

                    var result = Validate(ticket.AsContract());
                    if (result.IsEmpty)
                        result = await BusinessProxyRepository.Ticket.CloseTicketAsync(ticket.AsContract(), GetTipProduct(ticket), false);
                    
                    if (result.IsEmpty)
                    {
                        CurrentTicket.DataContext = result.Contract;

                        if (CurrentTicket.MustOpenDrawer)
                            WindowHelper.OpenDrawer();

                        ShowTicket(CurrentTicket, afterAction);

                        WindowHelper.PopCloseAction();
                        WindowHelper.PopCloseAction();
                        WindowHelper.ClosePopUp();

                        RemoveVisualTicket(CurrentTicket);
                    }
                    else
                        OnCloseTicketError(result);
                }
                catch (Exception e)
                {
                    WindowHelper.ShowError(e.Message);
                }
            }
            finally
            {
                WindowHelper.CloseBusyDialog();
            }
        }

        private ValidationResult ValidateFiscalDoc(BuyerInfo buyerInfo)
        {
            var definitions = Hotel.Instance.SignType.GetFiscalDefinitions();

            var result = new ValidationResult();

            result
                .ValidateFiscalNumber(definitions, buyerInfo.FiscalNumber)
                .ValidateAddress(definitions, buyerInfo.Address)
                .ValidateName(definitions, buyerInfo.Name)
                .ValidateNationality(definitions, buyerInfo.SelectedNationality)
                .ValidateEmail(definitions, buyerInfo.FiscalMail)
                .ValidatePhoneNumber(definitions, buyerInfo.FiscalPhoneNumber)
                .ValidateCity(definitions, buyerInfo.FiscalCityName)
                .ValidateCityCode(definitions, buyerInfo.FiscalCityCode)
                .ValidateClientRegimeTypeCode(definitions, buyerInfo.FiscalRegimeTypeCode)
                .ValidateClientResponsibilityCode(definitions, buyerInfo.FiscalResponsabilityCode);
                
            return result;
        }

        private async void CloseAndShowFiscalDoc(POSTicketContract ticket, BuyerInfo buyerInfo, bool fillState, Action afterOkAction = null)
        {
            if (fillState)
            {
                ValidationResult fiscalDocValidation = ValidateFiscalDoc(buyerInfo);
                if (fiscalDocValidation.HasErrors)
                {
                    WindowHelper.ShowError(fiscalDocValidation);
                    return;
                }
            }

            try
            {
                ticket.DocumentType = buyerInfo.FiscalDocType;
                ticket.FiscalDocAddress = buyerInfo.Address;
                ticket.FiscalDocFiscalNumber = buyerInfo.FiscalNumber;
                ticket.FiscalDocNacionality = buyerInfo.SelectedNationality != null
                    ? buyerInfo.SelectedNationality.CountryCode : null;
                ticket.FiscalDocTo = buyerInfo.Name;
                ticket.FiscalDocEmail = buyerInfo.FiscalMail;
                ticket.FiscalIdenType = (long?)buyerInfo.SelectedFiscalIdenType;
                ticket.FiscalPhoneNumber = buyerInfo.FiscalPhoneNumber;
                ticket.FiscalCityCode = buyerInfo.FiscalCityCode;
                ticket.FiscalCityName = buyerInfo.FiscalCityName;
                ticket.FiscalResponsabilityCode = buyerInfo.FiscalResponsabilityCode;
                ticket.FiscalRegimeTypeCode = buyerInfo.FiscalRegimeTypeCode;

                var result = Validate(ticket, buyerInfo);
                if (result.IsEmpty)
                {
                    WindowHelper.CloseDialog();
                    WindowHelper.PopCloseAction();
                    WindowHelper.ClosePopUp();

                    // Solo asignar la entidad base si no hubo errores
                    ticket.BaseEntityId = buyerInfo.ClientId;

                    WindowHelper.ShowBusyDialog();
                    try
                    {
                        result = await BusinessProxyRepository.Ticket.CloseTicketAsync(ticket,
                            GetTipProduct(CurrentTicket), buyerInfo.SaveClient);
                    }
                    finally
                    {
                        WindowHelper.CloseBusyDialog();
                    }
                }
                else
                {
                    WindowHelper.CloseBusyDialog();
                    WindowHelper.ShowError(result);
                    return;
                }

                if (!result.HasErrors)
                {
                    //WindowHelper.CloseDialog();

                    var contract = result.Contract as POSTicketContract;
                    if (contract != null)
                    {
                        CurrentTicket.DataContext = contract;

                        bool ShowDocument()
                        {
                            if (CurrentTicket.MustOpenDrawer)
                                WindowHelper.OpenDrawer();

                            if (CurrentTicket.IsInvoice)
                                ShowInvoice(CurrentTicket, afterOkAction);
                            else if (CurrentTicket.IsTicket)
                                ShowTicket(CurrentTicket, afterOkAction);
                            else if (CurrentTicket.IsBallot)
                                ShowBallot(CurrentTicket, afterOkAction);

                            //WindowHelper.PopCloseAction();
                            //WindowHelper.ClosePopUp();

                            RemoveVisualTicket(CurrentTicket);

                            return false;
                        }

                        if (result.HasWarnings)
                            WindowHelper.OpenDialog(result.Message, ShowDocument, DialogButtons.OkOnly);
                        else
                            ShowDocument();
                    }
                    else
                        throw new Exception("Invalid ticket");
                }
                else
                {
                    ResetTicket(ticket);
                    OnCloseTicketError(result);
                }
            }
            catch (Exception e)
            {
                ResetTicket(ticket);
                WindowHelper.ShowError(e.Message);
            }
        }

        private static void ResetTicket(POSTicketContract contract)
        {
            contract.DocumentType = POSDocumentType.Ticket;
        }

        private ProductRecord GetTipProduct(Ticket ticket)
        {
            ProductRecord tipProduct = null;
            if (ticket.TipValue > decimal.Zero)
            {
                var stand = Hotel.Instance.CurrentStandCashierContext.Stand;
                if (stand.TipService.HasValue)
                    tipProduct = stand.GetProductById(stand.TipService.Value);
            }

            return tipProduct;
        }

        private DocValidator GetValidator(POSTicketContract ticket, BuyerInfo buyer = null)
        {
            return new DocValidator(Hotel.Instance.MandatoryValueForFiscalNumber)
            {
                Client = buyer?.Client,
                Country = Hotel.Instance.Country,
                DocumentSign = Hotel.Instance.SignType
            };
        }

        private ValidationContractResult Validate(POSTicketContract ticket, BuyerInfo buyer = null)
        {
            var result = new ValidationContractResult();

            if (buyer != null && Hotel.Instance.HasFiscalPrinter)
            {
                var fiscalPrinter = CwFactory.Resolve<IFiscalPrinter>();
                result.AddValidations(fiscalPrinter.ValidateBuyerInfo(buyer));
            }

            if (result.IsEmpty)
            {
                var validator = GetValidator(ticket, buyer);
                result.Add(validator.Validate(ticket));
            }

            return result;
        }

        private void OnCloseTicketError(ValidationResult result)
        {
            if (result.ToString().IndexOf("Ticket modified") != -1)
            {
                CurrentTicket.ClearPayments();
                WindowHelper.ShowError("Ticket was modified on another Stand/Cashier. Current Ticket will close now", "Info");
                var action = WindowHelper.PopCloseAction();
                action();
            }
            else if (result.ToString().IndexOf("Ticket Closed") != -1)
            {
                CurrentTicket.ClearPayments();
                WindowHelper.ShowError("Ticket was already closed. Information will be refreshed", "Info");
                var action = WindowHelper.PopCloseAction();
                action();
                RemoveVisualTicket(CurrentTicket);
            }
            else
                WindowHelper.ShowError(result);
        }

        #endregion
        
        #region Printing
        /// <summary>
        /// Prints the receipt, to quickly look the state of the ticket/table.
        /// </summary>
        /// <param name="sender">The button that is sending the information</param>
        /// <param name="e">The args</param>
        private void PrintLookupTable_Click(object sender, RoutedEventArgs e)
        {
            AppSecurity.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.TicketsPrintPaymentInstructions, async (isValid, validUser) =>
            {
                if (isValid)
                {
                    _enabledBarCodeCapture = false;
                    WindowHelper.ShowBusyDialog();
                    try
                    {
                        var result = await BusinessProxyRepository.Ticket.CreateLookupTableAsync(CurrentTicket?.AsContract() ?? new POSTicketContract());
                        if (result.IsEmpty && CurrentTicket is not null)
                        {
                            CurrentTicket.DataContext = (POSTicketContract)result.Contract;
                            ShowLookupTable(CurrentTicket);
                        }
                        else
                        {
                            if (result.ToString().IndexOf("Ticket modified", StringComparison.Ordinal) != -1)
                            {
                                WindowHelper.ShowError("Ticket was modified on another Stand/Cashier. Current Ticket will close now", "Info");
                                var action = WindowHelper.PopCloseAction();
                                action();
                            }
                            else WindowHelper.ShowError(result);
                        }
                    }
                    catch (Exception exception)
                    {
                        WindowHelper.ShowError(exception);
                    }
                    finally
                    {
                        WindowHelper.CloseBusyDialog();
                        _enabledBarCodeCapture = true;
                    }
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        private void ShowLookupTable(Ticket ticket, Action afterClose = null) => ShowAs(ticket, POSDocumentType.TableBalance, false, afterClose);

        private void ShowTicket(Ticket ticket, Action afterClose = null) => ShowAs(ticket, POSDocumentType.Ticket, false, afterClose);

        private void ShowBallot(Ticket ticket, Action afterClose = null) => ShowAs(ticket, POSDocumentType.Ballot, false, afterClose);

        private void ShowInvoice(Ticket ticket, Action afterClose = null) => ShowAs(ticket, POSDocumentType.CashInvoice, false, afterClose);

        private void ShowOldInvoice(Ticket ticket, Action afterClose = null) => ShowAs(ticket, POSDocumentType.CashInvoice, true, afterClose);

        private void ShowOldCreditNote(Ticket ticket, Action afterClose = null) => ShowAs(ticket, POSDocumentType.CashCreditNote, true, afterClose);

        private void ShowAs(Ticket ticket, POSDocumentType documentType, bool oldTicket, Action afterClose)
        {
            var settings = GetPrintSettings(documentType);
            if (settings != null)
            {
                if (!settings.IsLarge)
                {

                     var title = Map(documentType);

                    #region Formato estrecho

                    bool PrintAction()
                    {
                        // Thread Printing
                        Action print = async () => 
                        {
                            var result = await PrintAs(ticket, documentType, oldTicket);
                            if (result.IsEmpty)
                            {
                                var incrementPrintResult = await BusinessProxyRepository.Ticket.IncrementPrintsAsync(ticket.Id, (int)title);
                                if (incrementPrintResult.IsEmpty)
                                {
                                    if (incrementPrintResult.Contract is POSTicketContract contract)
                                        UIThread.Invoke(() => { CurrentTicket = new Ticket(contract); });    
                                }
                                else
                                    result.Add(incrementPrintResult);
                            }

                            if (!result.IsEmpty)
                                WindowHelper.ShowError(result);
                        };

                        print.RunInStaThread();
                        return true;
                    }

                    #region Fiscal Description
                    string fiscalDescription = "";
                    switch (documentType)
                    {
                        case POSDocumentType.Ticket:
                            fiscalDescription = Hotel.Instance.CurrentStandCashierContext.Stand.TicketPrintSettings.FiscalDescription;
                            break;
                        case POSDocumentType.Ballot:
                            fiscalDescription = Hotel.Instance.CurrentStandCashierContext.Stand.BallotPrintSettings.FiscalDescription;
                            break;
                        case POSDocumentType.CashInvoice:
                            fiscalDescription = Hotel.Instance.CurrentStandCashierContext.Stand.InvoicePrintSettings.FiscalDescription;
                            break;
                        case POSDocumentType.Invoice:
                            fiscalDescription = Hotel.Instance.CurrentStandCashierContext.Stand.InvoicePrintSettings.FiscalDescription;
                            break;
                        case POSDocumentType.CashCreditNote:
                            fiscalDescription = Hotel.Instance.CurrentStandCashierContext.Stand.InvoicePrintSettings.FiscalDescription;
                            break;
                        case POSDocumentType.TableBalance:
                            fiscalDescription = Hotel.Instance.CurrentStandCashierContext.Stand.ReceiptPrintSettings.FiscalDescription;
                            break;
                        default:
                            break;
                    }
                    #endregion

                    var visual = new DocViewControl(title, ticket, Hotel.Instance.Country, fiscalDescription);
                    visual.Initialize(ticket, settings);
                    WindowHelper.OpenDialog(visual, true, settings, PrintAction, MiddleScreen, DialogButtons.OkCancel, null, afterClose);

                    #endregion
                }
            }
        }

        private async Task<ValidationResult> PrintAs(Ticket ticket, POSDocumentType documentType, bool oldTicket = false)
        {
            var result = new ValidationResult();

            WindowHelper.ShowBusyDialog();
            try
            {
                var settings = GetPrintSettings(documentType);
                if (Hotel.Instance.HasFiscalPrinter)
                {
                    var fiscalPrinter = CwFactory.Resolve<IFiscalPrinter>();
                    if (oldTicket)
                        result.AddValidations(await fiscalPrinter.PrintNonFiscalAsync(ticket, settings));
                    else
                    {
                        result.AddValidations(fiscalPrinter.ValidatePayments(ticket));
                        if (result.IsEmpty)
                        {
                            FpValidationResult pfValidationResult;
                            if (ticket.HasCashPayment)
                            {
                                pfValidationResult = await fiscalPrinter.PrintFiscalAsync(ticket);
                                //  if has print errors then try from closed tickets
                                if (pfValidationResult.IsEmpty)
                                {
                                    // Save doc number asigned by fiscal printer and upload ticket
                                    ticket.FpInvoiceNumber = pfValidationResult.DocFiscalNumber;
                                    ticket.FpSerialInvoice = pfValidationResult.FpSerial;
                                    var persistResult = await BusinessProxyRepository.Ticket.UpdateFiscalPrinterDataAsync(ticket.AsContract());
                                    if (!persistResult.IsEmpty)
                                        pfValidationResult.AddValidations(persistResult);
                                }
                            }
                            else
                                pfValidationResult = await fiscalPrinter.PrintNonFiscalAsync(ticket, settings);

                            result.AddValidations(pfValidationResult);
                        }
                    }
                }
                else
                {
                    var printer = Hotel.Instance.CurrentStandCashierContext.Stand.PrintersConfiguration.GetPrinter(documentType);
                    if (printer != null)
                    {
                        result.AddValidations(documentType switch
                        {
                            POSDocumentType.Ticket => TicketPrinter.PrintTicket(printer, ticket, settings),
                            POSDocumentType.Ballot => TicketPrinter.PrintBallot(printer, ticket, settings),
                            POSDocumentType.CashInvoice => TicketPrinter.PrintInvoice(printer, ticket, settings),
                            POSDocumentType.CashCreditNote => TicketPrinter.PrintCreditNote(printer, ticket, settings),
                            POSDocumentType.TableBalance => TicketPrinter.PrintLookupTable(printer, ticket, settings),
                            _ => throw new ArgumentOutOfRangeException(nameof(documentType), documentType, null)
                        });
                    }
                    else
                        result.AddError(LocalizationMgr.Translation("PrinterCfgNotValid"));
                }
            }
            finally
            {
                WindowHelper.CloseBusyDialog();
            }

            return result;
        }

        private PrintDocSetting GetPrintSettings(POSDocumentType documentType)
        {
            var stand = Hotel.Instance.CurrentStandCashierContext.Stand;
            return documentType switch
            {
                POSDocumentType.Ticket => stand.TicketPrintSettings,
                POSDocumentType.Ballot => stand.BallotPrintSettings,
                POSDocumentType.CashInvoice => stand.InvoicePrintSettings,
                POSDocumentType.CashCreditNote => stand.InvoicePrintSettings,
                POSDocumentType.TableBalance => stand.ReceiptPrintSettings,
                _ => throw new ArgumentOutOfRangeException(nameof(documentType), documentType, null)
            };
        }

        private static TicketTitles Map(POSDocumentType documentType)
        {
            return documentType switch
            {
                POSDocumentType.Ticket => TicketTitles.Ticket,
                POSDocumentType.Ballot => TicketTitles.Ballot,
                POSDocumentType.CashInvoice => TicketTitles.Invoice,
                POSDocumentType.CashCreditNote => TicketTitles.CreditNote,
                POSDocumentType.TableBalance => TicketTitles.Receipt,
                POSDocumentType.Invoice => TicketTitles.Invoice,
                _ => throw new ArgumentOutOfRangeException(nameof(documentType), documentType, null)
            };
        }

        #endregion
        
        #region File Content Section

        #region General Ctrls

        private CheckButton lastCheckButton;
        private CheckButton LastCheckButton
        {
            get { return lastCheckButton; }
            set
            {
                lastCheckButton = value;
                VersionVisibility = value == null ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        private void CheckButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (LastCheckButton == sender)
            {
                LastCheckButton = null;
                FileCategoryContent = null;
                VersionVisibility = Visibility.Visible;
            }
        }

        #endregion
        #region Old Tickets

        private void OldTickets_Click(object sender, RoutedEventArgs e)
        {
            AppSecurity.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.ClosedTickets, async (isValid, validUser) =>
            {
                if (isValid)
                {
                    WindowHelper.ShowBusyDialog();
                    try
                    {
                        ControlCheckBoxes(sender);

                        var context = Hotel.Instance.CurrentStandCashierContext;
                        var closedTickets = new ClosedTickets
                        {
                            Cashier = context.Cashier,
                            Stand = context.Stand,
                            Date = context.Stand.StandWorkDate
                        };

                        closedTickets.ShowOldTicket += (o, ticket) => ShowTicket(ticket);
                        closedTickets.ShowOldBallot += (o, ticket) => ShowBallot(ticket);
                        closedTickets.ShowOldInvoice += (o, ticket) => ShowOldInvoice(ticket);
                        closedTickets.ShowOldCreditNote += (o, ticket) => ShowOldCreditNote(ticket);

                        FileCategoryContent = closedTickets;
                    }
                    finally
                    {
                        WindowHelper.CloseBusyDialog();
                    }
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        #endregion
        #endregion

        #endregion

        #region Clients

        
        public void Clients_Clicked_Outside(object sender, RoutedEventArgs e)
        {
            bool OkAction()
            {
                _actionsCtrl.IsEnabled = true;
                return true;
            }
            
            var panel = new ClientsPanel(new Ticket(), null);
            ShowEditorControl("Clients", panel, Colors.Black, OkAction, null, false);
        }

        #endregion

        #region Table Reservations

        private void MealsControlBtn_Loaded(object sender, RoutedEventArgs e)
        {
            if (Hotel.Instance.IsHtml5)
            {
                var cb = (CheckButton)sender;
                cb.Visibility = Visibility.Visible;
            }
        }

        private void ReservationsTableBtn_OnChecked(object sender, RoutedEventArgs e)
        {
            State = States.TableReservation;
        }

        private void ReservationsTableBtn_OnUnchecked(object sender, RoutedEventArgs e)
        {
            State = States.Tables;
        }

        private void MealsControlBtn_OnChecked(object sender, RoutedEventArgs e)
        {
            State = States.MealsControl;
        }

        private void MealsControlBtn_OnUnchecked(object sender, RoutedEventArgs e)
        {
            State = States.Tables;
        }

        private void TryUncheckReservationsTableBtn()
        {
            if (BtnReservationsTable.IsChecked.GetValueOrDefault())
            {
                BtnReservationsTable.Unchecked -= ReservationsTableBtn_OnUnchecked;
                BtnReservationsTable.IsChecked = false;
                BtnReservationsTable.Unchecked += ReservationsTableBtn_OnUnchecked;
            }
        }

        private void TryUncheckMealsControlBtn()
        {
            if (BtnMealsControl.IsChecked.GetValueOrDefault())
            {
                BtnMealsControl.Unchecked -= MealsControlBtn_OnUnchecked;
                BtnMealsControl.IsChecked = false;
                BtnMealsControl.Unchecked += MealsControlBtn_OnUnchecked;
            }
        }

        private void ShowTablesReservationsViewer()
        {
            _tablesReservationsViewer.BindRemoteControl(_tablesViewerRemoteControl);
            _tablesReservationsViewer.Visibility = Visibility.Visible;
            _tablesReservationsViewer.Filter = new TablesReservationFilter
            {
                StandId = Hotel.Instance.CurrentStandCashierContext.Stand.Id,
                InDate = Hotel.Instance.CurrentStandCashierContext.Stand.StandWorkDate
            };
            _tablesReservationsViewer.Saloons = _tablesViewer.Saloons;
            _tablesReservationsViewer.Stands = Hotel.Instance.Stands;
            _tablesReservationsViewer.UpdateData();
        }

        private void HideTablesReservationsViewer()
        {
            _tablesReservationsViewer.Visibility = Visibility.Collapsed;
        }

        private void ShowMealsControlViewer()
        {
            _mealsControlViewer.Visibility = Visibility.Visible;
            _mealsControlViewer.Init();
        }

        private void HideMealsControlViewer()
        {
            _mealsControlViewer.Visibility = Visibility.Collapsed;
        }

        #endregion
        
        #region Waiting List

        private void WaitingListBtn_OnChecked(object sender, RoutedEventArgs e)
        {
            State = States.WaitingList;
        }

        private void WaitingListBtn_OnUnchecked(object sender, RoutedEventArgs e)
        {
            State = States.Tables;
        }

        private Task<ValidationContractResult> WaitingListViewer_OnPersistPaxWaitingListRequest(PaxWaitingList arg)
        {
            return BusinessProxyRepository.Stand.PersistPaxWaitingListAsync(arg.AsContract());
        }

        private Task<ValidationResult> WaitingListViewer_OnRemovePaxWaitingListRequest(PaxWaitingList paxWaitingList)
        {
            return BusinessProxyRepository.Stand.DeletePaxWaitingListAsync(paxWaitingList.Id);
        }

        private async Task<ValidationItemSource<PaxWaitingList>> WaitingListViewer_OnLoadDataSourceRequest(WaitingListFilter filter)
        {
            var filterContract = new WaitingListFilterContract
            {
                StandId = filter.StandId,
                SaloonId = filter.SaloonId,
                Name = filter.Name,
                PhoneNumber = filter.PhoneNumber,
                Paxs = filter.Paxs
            };

            var serverResultValidation = await BusinessProxyRepository.Stand.GetWaitingListAsync(filterContract);
            var resultValidation = new ValidationItemSource<PaxWaitingList>();
            resultValidation.Add(serverResultValidation);
            if (resultValidation.IsEmpty)
            {
                resultValidation.ItemSource = serverResultValidation.ItemSource.Select(x => new PaxWaitingList(x)).ToList();
            }
            return resultValidation;
        }

        private async void WaitingListViewer_OnCreateTicketRequest(PaxWaitingList paxWaitingList, Table[] tables)
        {
            Table mainTable = tables.First();

            Ticket ticket = CreateNewTicket(mainTable);
            ticket.TableId = mainTable.Id;

            bool saved = await PersistEmptyTicketToPaxWaitingListAsync(ticket, paxWaitingList.Id);
            if (saved)
            {
                ticket.Table = mainTable;
                mainTable.AddTicket(ticket);

                if (tables.Length > 1)
                {
                    // Is a tables group

                    #region Create tables group and asociate to ticket

                    var group = new TablesGroup(ticket.Id);
                    group.AddAndLinkTableRange(tables);
                    ticket.TablesGroup = group;

                    #endregion
                }

                OpenTicket(mainTable, ticket);
            }

            TryUncheckWaitingListBtn();
        }

        private void TryUncheckWaitingListBtn()
        {
            if (BtnWaitingList.IsChecked.GetValueOrDefault())
            {
                BtnWaitingList.Unchecked -= WaitingListBtn_OnUnchecked;
                BtnWaitingList.IsChecked = false;
                BtnWaitingList.Unchecked += WaitingListBtn_OnUnchecked;
            }
        }

        private async Task<bool> PersistEmptyTicketToPaxWaitingListAsync(Ticket ticket, Guid paxWaitingListId)
        {
            try
            {
                mainPage.BusyControl.Show("~Saving~...");

                var result = await BusinessProxyRepository.Ticket.CreateEmptyTicketToPaxWaitingListAsync(ticket.TableId.Value, paxWaitingListId);

                if (result.IsEmpty)
                    ticket.DataContext = result.Contract;
                else
                {
                    WindowHelper.ShowError(result);
                    return false;
                }
            }
            catch (Exception e)
            {
                WindowHelper.ShowError(e);
                return false;
            }
            finally
            {
                mainPage.BusyControl.Close();
            }
            return true;
        }

        private void ShowWaitingListViewer()
        {
            _waitingListViewer.BindRemoteControl(_tablesViewerRemoteControl);
            _waitingListViewer.Visibility = Visibility.Visible;
            _waitingListViewer.Filter = new WaitingListFilter();
            _waitingListViewer.Filter.StandId = Hotel.Instance.CurrentStandCashierContext.Stand.Id;
            _waitingListViewer.Saloons = _tablesViewer.Saloons;
            _waitingListViewer.UpdateData();
        }

        private void HideWaitingListViewer()
        {
            _waitingListViewer.Visibility = Visibility.Collapsed;
        }

        #endregion
        
        #region FileContent

        public object FileCategoryContent
        {
            get => GetValue(FileCategoryContentProperty);
            set => SetValue(FileCategoryContentProperty, value);
        }

        public static readonly DependencyProperty FileCategoryContentProperty =
            DependencyProperty.Register(nameof(FileCategoryContent), typeof(object), typeof(BarCaffeteriaRestaurantPage));

        public Visibility VersionVisibility
        {
            get => (Visibility)GetValue(VersionVisibilityProperty);
            set => SetValue(VersionVisibilityProperty, value);
        }

        public static readonly DependencyProperty VersionVisibilityProperty =
            DependencyProperty.Register(nameof(VersionVisibility), typeof(Visibility), typeof(BarCaffeteriaRestaurantPage));

        private void ControlCheckBoxes(object sender)
        {
            if (LastCheckButton != sender && LastCheckButton != null)
            {
                var prev = LastCheckButton;
                LastCheckButton = (CheckButton)sender;
                prev.IsChecked = false;
            }
            else
                LastCheckButton = (CheckButton)sender;
        }

        private void areas_Click(object sender, RoutedEventArgs e)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.Areas, (isValid, validUser) =>
            {
                if (isValid)
                {
                    ControlCheckBoxes(sender);

                    var content = Resources["fileAreasContent"] as FrameworkElement;
                    if (content != null)
                    {
                        content.DataContext = Hotel.Instance.CurrentStandCashierContext.Stand.Areas;
                        FileCategoryContent = content;
                    }
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        private void printers_Click(object sender, RoutedEventArgs e)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.PrinterDefinition, (isValid, validUser) =>
            {
                if (isValid)
                {
                    ControlCheckBoxes(sender);
                    FileCategoryContent = new PrintersSetting();
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        private void reports_Click(object sender, RoutedEventArgs e)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.Reports, (isValid, validUser) =>
            {
                if (isValid)
                {
                    ControlCheckBoxes(sender);
                    FileCategoryContent = new ReportsView();
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        private void users_Click(object sender, RoutedEventArgs e)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.Users, (isValid, validUser) =>
            {
                if (isValid)
                {
                    ControlCheckBoxes(sender);

                    var content = Resources["fileUsersContent"] as FrameworkElement;
                    if (content != null)
                    {
                        content.DataContext = new LoginData { IsByUserPwd = true };
                        FileCategoryContent = content;
                    }
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        private void download_Click(object sender, RoutedEventArgs e)
        {
            var credentials = CodeControlWindow.ShowCodeWindow(CodeControlOpt.FullSync);
            if (credentials.HasValue && credentials.Value)
            {
                ControlCheckBoxes(sender);
                FileCategoryContent = null;

                MainWindow.Instance.OpenDialog(new TextBlock { Text = "Perform a full syncronization?", FontSize = 14 }, null, () =>
                {
                    mainPage.BusyControl.Show("Resetting status...");
                    Task.Run(async () => await BusinessProxyRepository.Settings.ResetSyncStatusAsync())
                    .ContinueWith(t =>
                    {
                        mainPage.BusyControl.Close();

                        if (t.IsFaulted)
                        {
                            if (t.Exception != null)
                                WindowHelper.ShowError(t.Exception);
                        }
                    }, UIThread.Scheduler);

                }, null);
            }
        }

        private void upload_Click(object sender, RoutedEventArgs e)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.Upload, (isValid, validUser) =>
            {
                if (isValid)
                {
                    ControlCheckBoxes(sender);

                    var content = Resources["fileUploadContent"] as FrameworkElement;
                    if (content != null)
                    {
                        content.DataContext = new List<DownUpLoadData>
                        {
                            new DownUpLoadData {Description = "AllLocalTickets", IsSelected = false, Status = DownUpStatus.Unknown, Tag = new Action<object, RoutedEventArgs>((a,b) => {})},
                            new DownUpLoadData {Description = "Tables", IsSelected = false, Status = DownUpStatus.Unknown, Tag = new Action<object, RoutedEventArgs>((a,b) => {})}
                        };

                        FileCategoryContent = content;
                    }
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        private void closeDayTurn_Click(object sender, RoutedEventArgs e)
        {
            ControlCheckBoxes(sender);
            FileCategoryContent = new CloseDayTurnCtrl();
        }

        private async void syncStock_Click(object sender, RoutedEventArgs e)
        {
            mainPage.BusyControl.Show("Please wait");
            try
            {
                var result = await BusinessProxyRepository.Settings.SyncLocalStockAsync();
                if(result.HasErrors)
                    WindowHelper.ShowError(result.Errors.ToString());
                else
                {
                    var message = new TextBlock { Text = result.Message, FontSize = 14 };
                    MainWindow.Instance.OpenDialog(message, null, null, null);
                }
            }
            catch (Exception ex)
            {
                mainPage.BusyControl.Close();
                WindowHelper.ShowError(ex);
            }
            finally
            {
                mainPage.BusyControl.Close();
            }
        }

        private void downloadSelection_Click(object sender, RoutedEventArgs e)
        {
            _restartApp = false;
            mainPage.BusyControl.Show("Please wait");

            var fe = FileCategoryContent as FrameworkElement;
            if (fe != null)
            {
                var items = fe.DataContext as List<DownUpLoadData>;
                if (items != null)
                {
                    foreach (var item in items)
                    {
                        if (item.IsSelected)
                        {
                            // Este esta seleccionado, debemos hacer el llamado
                            var action = item.Tag as Action<object, RoutedEventArgs>;
                            if (action != null)
                                action(null, null);
                            item.Status = DownUpStatus.Ok;
                        }
                    }
                }
            }

            if (_restartApp)
            {
                MainWindow.Instance.OpenDialog(new TextBlock { Text = "~RestartIsStronglyRecommended~.\n~RestartNow?~".Translate(), FontSize = 14 }, null,
                () =>
                {
                    MainWindow.Instance.RestartApp();
                }, null);
            }

            mainPage.BusyControl.Close();
        }

        private void uploadSelection_Click(object sender, RoutedEventArgs e)
        {
            mainPage.BusyControl.Show("Please wait");

            var fe = FileCategoryContent as FrameworkElement;
            if (fe != null)
            {
                var items = fe.DataContext as List<DownUpLoadData>;
                if (items != null)
                {
                    foreach (var item in items)
                    {
                        if (item.IsSelected)
                        {
                            // Este esta seleccionado, debemos hacer el llamado
                            var action = item.Tag as Action<object, RoutedEventArgs>;
                            if (action != null)
                                action(null, null);
                            item.Status = DownUpStatus.Ok;
                        }
                    }

                    mainPage.BusyControl.Close();
                }
            }
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            var sb = (FrameworkElement)FileCategoryContent;
            var loginData = sb.DataContext as LoginData;
            loginData.Password = password.Password;
        }

        private void loginUserFile_Click(object sender, RoutedEventArgs e)
        {
            loginUsersFileBorder.IsEnabled = false;
        }

        private void changeUsersFile_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var random = new Random(Environment.TickCount);
                var code = random.Next(10000, 99999).ToString();

                var fe = (FrameworkElement)FileCategoryContent;
                var loginData = fe.DataContext as LoginData;

                loginData.UserModel.Code = code;
            }
            catch
            {
                WindowHelper.ShowError("ConnectionError".Translate());
            }
        }

        private void backUsersFile_Click(object sender, RoutedEventArgs e)
        {
            var sb = (Storyboard)Resources["ShowLoginUsersFile"];
            sb.Begin();
        }

        #endregion
        
        #region Common Methods

        public Ticket GetNewTicketFromContract(POSTicketContract contract)
        {
            var ticket = new Ticket(contract);
            if (contract.Mesa.HasValue)
            {
                var table = _tablesViewer.SelectedSaloon.FindTable(contract.Mesa.Value);
                if (table != null)
                {
                    ticket.Table = table;
                    ticket.TablesGroup = table.TablesGroup;
					
                    //var oldTicket = table.Bills.FirstOrDefault(x => x.Id == ticket.Id);
					//if (oldTicket != null)
					//    table.Bills.Remove(oldTicket);
					table.RemoveTicket(ticket);
                    table.AddTicket(ticket);

					// Hotel.Instance.CurrentStandCashierContext.Stand.Tickets.Add(ticket);
					Hotel.Instance.CurrentStandCashierContext.Stand.AddTicket(ticket);
                }
            }
            else
            {
                if (Hotel.Instance.CurrentStandCashierContext.Stand.Tickets.All(x => x.Id != (Guid)contract.Id))
                {
                    Hotel.Instance.CurrentStandCashierContext.Stand.Tickets.Add(ticket);
                    if (contract.IsAcceptedAsTransfer)
                        _tablesViewer.StandCashierContext.AcceptedTickets.Add(ticket);
                    else
                        _tablesViewer.StandCashierContext.QuickTickets.Add(ticket);
                }
            }

            ticket.RecalculateAll();

            return ticket;
        }

        #endregion

        #region ITicketInitializer

        public Ticket CreateNewTicket(Table? table = null, BuyerInfo? buyerInfo = null, short paxs = 0)
        {
            Ticket ticket = null;
            try
            {
                var context = Hotel.Instance.CurrentStandCashierContext;
                ticket = new Ticket(
                    context.Stand.Id,
                    context.Cashier.Id,
                    Hotel.Instance.UserId,
                    "NoSerie",
                    mainPage.Stand.Shift,
                    table is not { TablesGroup: null } ? null : table.Id,
                    Hotel.Instance.TaxIncluded,
                    Hotel.Instance.DefaultTaxSchema
                )
                {
                    Table = table,
                    Tax = x => x * 0.1m,
                    CurrentRate = mainPage.Stand.StandardRate ?? Guid.Empty,
                    IsEditable = true,

                    Paxs = table?.PaxSeated ?? paxs
                };

                if (table?.TablesGroup != null)
                    ticket.TablesGroup = table.TablesGroup;

                ticket.InitRequested += Ticket_InitRequested;
                ticket.InitTip += Ticket_InitTip;
                ticket.InitLookupTableRequested += Ticket_InitLookupTableRequested;
                ticket.CriticalError += Ticket_CriticalError;

                ticket.Unmo = Hotel.Instance.CurrentStandCashierContext.Stand.CurrencyId;
                ticket.UserId = Hotel.Instance.CurrentUserModel.Id;
                ticket.UserDescription = Hotel.Instance.CurrentUserModel.Description;
            }
            catch
            {
                if (ticket != null)
                {
                    ticket.Unmo = Hotel.Instance.CurrentStandCashierContext.Stand.CurrencyId;
                    ticket.UserId = Hotel.Instance.CurrentUserModel.Id;
                    ticket.UserDescription = Hotel.Instance.CurrentUserModel.Description;
                }
            }

            return ticket;
        }

        public void OpenTicket(Table table, Ticket ticket)
        {
            var saloon = _tablesViewer.Saloons.FirstOrDefault(x => x.Tables.Contains(table));
            if (_tablesViewer.SelectedSaloon != saloon)
            {
                // Cuando el salon cambia los ticket son recargados desde la bd
                // en segundo plano y las referencias cambian 
                // Cuando los ticket se recarguen tomar el correspondiente y abrirlo
                void OnSaloonLoaded()
                {
                    CurrentTicket = table.Bills.First(x => x.Id == ticket.Id);
                    State = States.Products;
                    _tablesViewer.SaloonLoaded -= OnSaloonLoaded;
                    ResetBarCode();
                }

                _tablesViewer.SaloonLoaded += OnSaloonLoaded;
                _tablesViewer.SelectedSaloon = saloon;
            }
            else
            {
                CurrentTicket = ticket;
                State = States.Products;
                ResetBarCode();
            }
        }

        #endregion
        
        #region Saloon Edit

        private bool _moveTablesCheckButtonLock;
        private TablesViewer.TablesViewerStatus _prevTablesViewerState = TablesViewer.TablesViewerStatus.Moving;

        private void MoveTables_Checked(object sender, RoutedEventArgs e)
        {
            SetVisibilityActionButtons(Visibility.Collapsed);
            if (!_moveTablesCheckButtonLock)
            {
                if (_tablesViewer.HasSaloonSelected)
                {
                    _tablesViewer.State = _prevTablesViewerState;
                    _tablesViewerRemoteControl.IsExpanded = true;
                    mainPage.ProhibitClose();
                }
                else
                {
                    WindowHelper.ShowError("NoSaloonSelectedCreateInBack");
                    _moveTablesCheckButtonLock = true;
                    var cbx = (CheckButton)sender;
                    cbx.IsChecked = false;
                    _moveTablesCheckButtonLock = false;
                }
            }
        }

        private void MoveTables_Unchecked(object sender, RoutedEventArgs e)
        {
            SetVisibilityActionButtons(Visibility.Visible);
            _prevTablesViewerState = _tablesViewer.State;
            _tablesViewer.State = TablesViewer.TablesViewerStatus.Normal;
            _tablesViewerRemoteControl.IsExpanded = false;
            mainPage.AllowClose();
        }

        private void SetVisibilityActionButtons(Visibility visibility)
        {
            BtnMergeTickets.Visibility = visibility;
            BtnTransfer.Visibility = visibility;
            BtnWaitingList.Visibility = visibility;

            if (Hotel.Instance.IsHtml5)
                BtnMealsControl.Visibility = visibility;
            else
                BtnReservationsTable.Visibility = visibility;
        }

        #endregion
        
        #region To Order
        
        /// <summary>
        /// Sets the current state/context of the page
        /// </summary>
        /// <param name="context">The context that comes from stand in the database</param>
        public void SetContext(StandCashierContext context)
        {
            mainPage.SetData();

            if (Hotel.Instance.AllowFastPayment)
            {
                _ticketActions = (UIElement)Resources["TicketActionsFastPayment"];
                BtnFastPlan.Visibility = Hotel.Instance.AllowFastPlan ? Visibility.Visible : Visibility.Collapsed;
                BtnFastTicketDoc.Visibility = Hotel.Instance.AllowFastTicket ? Visibility.Visible : Visibility.Collapsed;
                BtnFastInvoiceDoc.Visibility = Hotel.Instance.AllowFastInvoice && Hotel.Instance.ShowInvoiceButton ? Visibility.Visible : Visibility.Collapsed;
                BtnFastBallotDoc.Visibility = Hotel.Instance.AllowFastBallot ? Visibility.Visible : Visibility.Collapsed;
            }
            else
                _ticketActions = (UIElement)Resources["TicketActions"];
            _ticketSeatActions = (UIElement)Resources["TicketSeatActions"];

            SetAsInstance();
            
            BtnCheckReservationCredit.Visibility = MainPage.CurrentInstance.Stand.StandRecord.AskCode ? Visibility.Visible : Visibility.Collapsed;
            PrePaidButton.Visibility = MainPage.CurrentInstance.Stand.StandRecord.AskCode ? Visibility.Visible : Visibility.Collapsed;
            PrePaidButtonFast.Visibility = MainPage.CurrentInstance.Stand.StandRecord.AskCode ? Visibility.Visible : Visibility.Collapsed;

            BusinessProxyRepository.Stand.GetStandsForTransferAsync(Hotel.SelectedLanguage.Code.ToString()).ContinueWith(t =>
            {
                if (t.Status == TaskStatus.RanToCompletion)
                {
                    UIThread.Invoke(() =>
                    {
                        var cbx = (ComboBox)Resources["standsTransferComboBox"];
                        cbx.ItemsSource = t.Result.Where(s => s.Id != context.Stand.Id);

                        context.AcceptedTickets.CollectionChanged += NoAllocated_CollectionChanged;
                        context.ReturnedTickets.CollectionChanged += NoAllocated_CollectionChanged;
                        DataContext = context;
                        _productsViewer.SetData(context.Stand);
                        _ticketViewer.SetData(context.Stand);
                        _tablesViewer.ResetData(context);

                        SetTransferRibbonContext();

                        Hotel.Instance.RefreshEmptyCategoriesVisibility();

                        // Inicializando tickets cargados
                        foreach (var ticket in context.Stand.Tickets.Where(x=>x is Ticket).Cast<Ticket>())
                        {
                            ticket.InitLookupTableRequested -= Ticket_InitLookupTableRequested;
                            ticket.InitLookupTableRequested += Ticket_InitLookupTableRequested;
                            ticket.CriticalError -= Ticket_CriticalError;
                            ticket.CriticalError += Ticket_CriticalError;
                            ticket.InitRequested -= Ticket_InitRequested;
                            ticket.InitRequested += Ticket_InitRequested;
                        }
                    });
                }
            });
        }

        public void SetAsInstance()
        {
            mainPage?.SetAsInstance();
            _ticketViewer?.SetAsInstance();
        }

        private void TicketViewer_CancelMultipleSelection_Click(TicketViewer obj)
        {
            MultipleSelection_Unchecked();
        }

        private void TicketViewer_AcceptMultipleSelection_Click(TicketViewer obj)
        {
            if (_multiSelectionOkAction != null)
            {
                _multiSelectionOkAction();
                _multiSelectionOkAction = null;
            }

            MultipleSelection_Unchecked();
        }

        private void HideProductsViewer()
        {
            _productsViewer.Visibility = Visibility.Collapsed;
        }

        private void HideTablesViewer()
        {
            _tablesViewer.Visibility = Visibility.Collapsed;
        }

        private void Ticket_InitTip(Ticket ticket, Action<bool> afterAction, bool isAuto)
        {
            var stand = Hotel.Instance.Stands.FirstOrDefault(x => x.Id == ticket.Stand);
            if (stand == null)
                stand = Hotel.Instance.CurrentStandCashierContext.Stand;
            if (!isAuto || stand.TipAuto)
                stand.InitializeTip(ticket, afterAction);
        }

        public void RemoveVisualTicket(ITicket ticket, bool error = false)
        {
            EditorControl.Instance = _editorControl;
            Hotel.Instance.CurrentStandCashierContext.Stand.RemoveTicket(ticket);

            // Check if payment popup is open
            mainPage.ClosePopUp();

            _tablesViewer.RemoveVisualTicket(ticket, error);
            State = States.Tables;
        }

        private void Ticket_CriticalError(Ticket ticket, string message)
        {
            RemoveVisualTicket(ticket, true);
        }

        private void Ticket_InitRequested(Ticket ticket, Action<bool, Exception> afterAction)
        {
            try
            {
                if (Hotel.Instance.CurrentStandCashierContext.TicketSerie == null)
                    WindowHelper.ShowError("NoSerieToStandCashierDefined".Translate(), "Error".Translate());
                else
                {
                    var fail = true;
                    mainPage.BusyControl.Show("Initializing ticket...");
                    Guid hotelId = Hotel.Instance.Id;
                    Guid cajaId = Hotel.Instance.CurrentStandCashierContext.Cashier.Id;
                    Guid standId = Hotel.Instance.CurrentStandCashierContext.Stand.Id;
                    Guid utilId = Hotel.Instance.UserId;
                    Guid? tableId = ticket.TableId;

                    Task.Run(async () =>
                    {
                        return await BusinessProxyRepository.Ticket.NewTicketAsync(hotelId, cajaId, standId, tableId, utilId);
                    })
                    .ContinueWith(t =>
                    {
                        if (t.IsFaulted)
                        {
                            if (t.Exception != null)
                                WindowHelper.ShowError(t.Exception);
                            mainPage_Lock_Click(mainPage, LoginControl.ResetMode.Block);
                        }
                        else if (t.Status == TaskStatus.RanToCompletion)
                        {
                            var ticketResult = t.Result;
                            if (ticketResult.IsEmpty)
                            {
                                var ticketContract = ticketResult.Contract as POSTicketContract;
                                ticket.Id = (Guid)ticketContract.Id;
                                ticket.IsEditable = true;
                                ticket.Unmo = Hotel.Instance.CurrentStandCashierContext.Stand.CurrencyId;
                                ticket.LastModified = ticketContract.LastModified;
                                ticket.Serie = ticketContract.Number;
                                ticket.SeriePrex = ticketContract.Serie;
                                fail = false;
                                Hotel.Instance.CurrentStandCashierContext.Stand.AddTicket(ticket);

                                mainPage.BusyControl.Close();
                            }
                            else
                            {
                                if (ticketResult.ToString().IndexOf("Stale data") != -1)
                                    WindowHelper.ShowError("Last operation still in progress. Try again", "Info");
                                else if (ticketResult.ToString().IndexOf("Ticket modified") != -1)
                                {
                                    WindowHelper.ShowError("Ticket was modified on another Stand/Cashier. Current Ticket will close now", "Info");
                                    var action = mainPage.PopCloseAction();
                                    action();
                                }
                                else
                                    WindowHelper.ShowError(ticketResult);

                                mainPage.BusyControl.Close();
                            }
                        }

                        afterAction?.Invoke(!fail, null);
                    },
                    UIThread.Scheduler);
                }
            }
            catch
            {
                mainPage.BusyControl.Close();
            }
        }

        private void Ticket_InitLookupTableRequested(Ticket ticket, LookupTable lookupTable, Action<bool> afterAction)
        {
            if (Hotel.Instance.CurrentStandCashierContext.ReceiptSerie == null)
                afterAction(false);
        }

        private void Products_Checked(object sender, RoutedEventArgs e)
        {
            mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.AccessAllProductsList, (isValid, validUser) =>
            {
                if (isValid)
                    _productsViewer.IsAllProducts = true;
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        private void Products_Unchecked(object sender, RoutedEventArgs e)
        {
            _productsViewer.IsAllProducts = false;
        }

        private void MultipleSelection_Checked(bool isMenuExpanded = true)
        {
            if (_multiOrderSelectionFilterCondition != null)
            {
                // Acotamos las ordenes
                CurrentTicket?.ApplyOrderFilter(_multiOrderSelectionFilterCondition, _multiOrderTableSelectionFilterCondition);
                CurrentTicket?.Orders.ForEach(x => x.IsMenuExpanded = isMenuExpanded);
                _ticketViewer.UpdateOrdersView();
                _multiOrderSelectionFilterCondition = null;
                _multiOrderTableSelectionFilterCondition = null;
            }

            TicketViewer.State = TicketViewer.ActionState.Selecting;
            // Inhabilitamos las acciones y los productos
            _ticketActions.IsEnabled = false;
            _ticketSeatActions.IsEnabled = false;
            _productsViewer.IsEnabled = false;
            MainPage.CurrentInstance.ProhibitClose();
        }

        private void MultipleSelection_Unchecked()
        {
            CurrentTicket?.ApplyOrderFilter(null, null);
            CurrentTicket?.Orders.ForEach(x => x.IsMenuExpanded = true);
            _ticketViewer.UpdateOrdersView();

            TicketViewer.State = TicketViewer.ActionState.Editing;
            // Habilitamos las acciones y los productos
            _ticketActions.IsEnabled = true;
            _ticketSeatActions.IsEnabled = true;
            _productsViewer.IsEnabled = true;
            MainPage.CurrentInstance.AllowClose();
        }

        private bool IsEmptyMultipleSelection()
        {
            return !CurrentTicket.Orders.Where(_multiOrderSelectionFilterCondition).Any();
        }

        private void EmptySetShowDialog(bool autoCancel, Point location)
        {
            WindowHelper.OpenDialog(Resources["multipleKitchenContent"], autoCancel, null, () => { return true; }, location, DialogButtons.CancelOnly);
        }

        private readonly ObservableCollection<Order> _forPicking;
        private Action _forcePickingOkAction;

        private bool SetOrderAreaOrForPicking(Order order, bool alwaysFirst = false)
        {
            if (order.NotMarch)
            {
                order.Product.Areas.GetCollectionView().Refresh();

                var count = order.Product.Areas.GetCollectionView().Count;
                if (count == 0)
                {
                    order.AreaId = null;
                    return order.Product.Areas == null || order.Product.Areas.Count == 0;
                }

                if (count > 1 && !alwaysFirst)
                {
                    order.AreaId = null;
                    _forPicking.Add(order);
                }
                else
                {
                    var area = order.Product.Areas.FirstOrDefault(x => x.Activated);
                    if (area != null)
                        order.AreaId = area.Id;
                    else
                        order.AreaId = null;
                }
            }

            return true;
        }

        private void ShowPickDialogAndSendNewOrders(IEnumerable<object> orders, bool noSend = false, Action<bool> action = null)
        {
            Func<bool> okAction = () =>
            {
                if (_forPicking.GetCollectionView().Count > 0)
                {
                    WindowHelper.ShowError("PickAreaForEachProduct".Translate());
                    return false;
                }

                if (!noSend)
                {
                    var context = Hotel.Instance.CurrentStandCashierContext;

                    var ordersToSend = new List<Order>();
                    foreach (var order in orders.Cast<Order>())
                    {
                        if (order.HasAreas && order.NotMarch)
                        {
                            if (order.SendToAreaStatus == 0)
                                order.SendToAreaStartTime = DateTime.UtcNow;
                            order.SendToAreaStatus = 1;
                            ordersToSend.Add(order);
                        }
                    }

                    if (ordersToSend.Count > 0)
                        MainWindow.SendNewOrderToArea(CurrentTicket, ordersToSend, context.Stand);
                }

                action?.Invoke(true);
                return true;
            };

            if (_forPicking.Count > 0)
            {
                var areaPicker = new PickerAreasOrder();
                areaPicker.PickArea += (order, area) =>
                {
                    order.Areas.Add(new POSProductLineAreaContract(Guid.NewGuid(), area.Id));
                    order.AreaId = area.Id;
                };
                _forcePickingOkAction = WindowHelper.OpenDialog(areaPicker, false, _forPicking, okAction, MiddleScreen, DialogButtons.OkCancel, () => action?.Invoke(false));
            }
            else
                okAction();
        }

        private void ShowEditorControl(string name, object content, Color background, Func<bool>? okAction, Func<bool>? cancelAction, bool isCancelButtonVisible = true)
        {
            _editorControl.IsCancelButtonVisible = isCancelButtonVisible;
            _editorControl.EditorName = name;
            _editorControl.EditorContent = content;
            _editorControl.OkAction = okAction;
            _editorControl.CancelAction = cancelAction;
            _editorControl.Color = background;
            _editorControl.FontColor = new Color
            {
                A = background.A,
                R = (byte)(255 - background.R),
                G = (byte)(255 - background.G),
                B = (byte)(255 - background.B)
            };

            _editorControl.AnimatedShow();
        }

        private void acceptedTicket_Click(object sender, RoutedEventArgs e)
        {
            var fe = (FrameworkElement)sender;
            TablesViewer_Table_Click(null, null, fe.DataContext as ITicket);
        }

        private void HideEditorControl()
        {
            _editorControl.AnimatedHide();
        }

        private void EditorControl_ShowRequested(EditorControl sender, EditorControlShowRequestArgs e)
        {
            ShowEditorControl(e.Name, e.Content, e.Background, e.OkAction, e.CancelAction, e.IsCancelButtonVisible);
        }

        private void EditorControl_AcceptChanges(EditorControl obj)
        {
            if (_editorControl.OkAction != null)
            {
                var ok = _editorControl.OkAction();
                if(ok) HideEditorControl();
            }
            else
            {
                HideEditorControl();
            }
        }

        private void EditorControl_CancelChanges(EditorControl obj)
        {
            _editorControl.CancelAction?.Invoke();
            HideEditorControl();
        }

        private void UniformGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (!_ribbonDown)
                CollapseRibbons();

            _ribbonDown = false;
        }

        private void CollapseRibbons()
        {
            var grid = (UniformGrid)_ticketActions;
            foreach (var item in grid.Children)
            {
                if (item is RibbonButton ribbonButton)
                    ribbonButton.IsChecked = false;
            }

            foreach (var item in ((UniformGrid)_ticketSeatActions).Children)
            {
                if (item is RibbonButton ribbonButton)
                    ribbonButton.IsChecked = false;
            }
        }

        private bool _ribbonDown;

        private void RibbonButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _ribbonDown = true;
        }

        private Ticket TablesViewer_OnNewTicketRequest(Table? table, short paxs)
        {
            CurrentTicket = CreateNewTicket(table, paxs: paxs);
            ResetBarCode();

            if (CurrentTicket == null) return CurrentTicket;

            State = States.Products;
            CreateNewTicketRequest(CurrentTicket);

            return CurrentTicket;
        }

        private void mainPage_Loaded(object sender, RoutedEventArgs e)
        {
            mainPage.SyncRequested += (s, ev) =>
            {
                SetContext(Hotel.Instance.CurrentStandCashierContext);
            };
        }

        private void standsTransfer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var cbx = (ComboBox)sender;
            var contract = cbx.DataContext as TransferContract;
            if (contract != null)
            {
                var record = cbx.SelectedItem as LiteStandRecord;
                if (record != null)
                    contract.StandDest = (Guid)record.Id;
            }
        }

        private void NoAllocated_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            #if SHOW_ACCEPTED_AND_RETURNED_TICKETS_IN_AUXILIAR_PANEL
			// Poner los tickets sin mesas al costado

			if (Hotel.Instance.CurrentStandCashierContext.AcceptedTickets.Count > 0)
				(Resources["acceptedTicketGridContent"] as StackPanel).Visibility = System.Windows.Visibility.Visible;
			else (Resources["acceptedTicketGridContent"] as StackPanel).Visibility = System.Windows.Visibility.Collapsed;

			if (Hotel.Instance.CurrentStandCashierContext.ReturnedTickets.Count > 0)
				(Resources["returnedTicketGridContent"] as StackPanel).Visibility = System.Windows.Visibility.Visible;
			else (Resources["returnedTicketGridContent"] as StackPanel).Visibility = System.Windows.Visibility.Collapsed;

			if (Hotel.Instance.CurrentStandCashierContext.AcceptedTickets.Count > 0 ||
				Hotel.Instance.CurrentStandCashierContext.ReturnedTickets.Count > 0)
			{// Hay elementos no asignados 
				if (!mainPage.ShowAuxiliarPanel) mainPage.ShowAuxiliarPanel = true;
			}// Hay elementos no asignados
			else
			{// Sin elementos para asignar 
				if (mainPage.ShowAuxiliarPanel) mainPage.ShowAuxiliarPanel = false;
			}// Sin elementos para asignar
            #endif
        }

        public void mainPage_Lock_Click(MainPage obj, LoginControl.ResetMode mode)
        {
            if (mode == LoginControl.ResetMode.ChangeUser)
            {
                var view = new PreviosLoguedUsersPicker
                {
                    UserSelected = Hotel.Instance.CurrentUserModel
                };

                bool ChangeUserAction()
                {
                    OnChangeUserStandRequested(mode);
                    return true;
                }

                WindowHelper.OpenDialog(view, false, Hotel.Instance.PreviousLoguedUsers, ChangeUserAction, new Point(ActualWidth / 2, ActualHeight / 2));
            }
            else
                OnChangeUserStandRequested(mode);
        }

        private void OnChangeUserStandRequested(LoginControl.ResetMode mode)
        {
            ChangeUserStandRequested?.Invoke(mode);
        }

        private void file_Click(object sender, RoutedEventArgs e)
        {
            Version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            Func<bool>? action = () =>
            {
                if (LastCheckButton != null)
                    LastCheckButton.IsChecked = false;
                _tablesActions.IsEnabled = true;
                mainPage.AllowClose();
                return true;
            };

            _tablesActions.IsEnabled = false;
            mainPage.ProhibitClose();
            (Resources["fileContent"] as FrameworkElement).DataContext = this;
            ShowEditorControl("File", Resources["fileContent"], Colors.Black, action, null, false);
        }

        private bool _restartApp;

        public static MainWindow MainWindow => Application.Current.MainWindow as MainWindow;

        private void TablesViewer_VoidTicket(TablesViewer arg1, ITicket arg2)
        {
            if (!arg2.HasOrders && !arg2.IsPersisted)
                RemoveVisualTicket(arg2);
            else
            {
                mainPage.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.TicketsCancelWithoutInvoice, (isValid, validUser) =>
                {
                    if (isValid)
                        CancelBillShowDialog(arg2, false, MiddleScreen);
                    else
                        WindowHelper.ShowError("UserNotAuthorized".Translate());
                });
            }
        }

        private void TablesViewer_DisableOutsideActionsRequest(TablesViewer obj)
        {
            _tablesActions.IsEnabled = false;
            MainPage.CurrentInstance.ProhibitClose();
        }

        private void TablesViewer_EnableOutsideActionsRequest(TablesViewer obj)
        {
            _tablesActions.IsEnabled = true;
            MainPage.CurrentInstance.AllowClose();
        }

        private RibbonButton _transferRibbon;
        private Product _productBeingAdded;
        private Func<bool> _tableAction;

        private void transferRibbon_Loaded(object sender, RoutedEventArgs e)
        {
            _transferRibbon = sender as RibbonButton;
            SetTransferRibbonContext();
        }

        private void SetTransferRibbonContext()
        {
            if (DataContext != null && _transferRibbon != null)
                _transferRibbon.DataContext = ((StandCashierContext)DataContext).Stand;
        }

        private void UpdatePaxButtons(Ticket ticket, RibbonButton rb)
        {
            // Update Seats Btn
            rb.Items.Clear();
            if (ticket?.TableId != null)
            {
                var table = Hotel.Instance.CurrentStandCashierContext.Stand.FindTable(ticket.TableId.Value);
                if (table != null)
                {
                    var paxs = table.TablesGroup?.MaxPaxs ?? table.Paxs;
                    for (short i = 0; i <= paxs; i++)
                    {
                        var ab = new ActionButton();
                        ab.SetResourceReference(StyleProperty, "buttonAltActionInTextStyle");
						ab.SetResourceReference(ActionButton.ImageSourceProperty, "paxNumberButtonImageSource");
                        ab.Text = i == 0 ? "Shared".Translate() : i.ToString();
                        ab.TextFontSize = i == 0 ? 14 : 24;
						ab.Tag = i;
                        ab.Click += paxNumber_Click;
                        rb.Items.Add(ab);
                    }
                }
            }

            rb.Visibility = rb.Items.Count > 0 ? Visibility.Visible : Visibility.Collapsed;

            if (ticket != null && ticket.CurrentPaxNumber >= 0 && ticket.CurrentPaxNumber < rb.Items.Count)
                _currentPaxNumber = ticket.CurrentPaxNumber;
            else if (rb.Items.Count == 0)
                _currentPaxNumber = null;
            else
                _currentPaxNumber = 1;

            if (_currentPaxNumber.HasValue)
            {
                BtnSeatTextBlock.Text = _currentPaxNumber.Value == 0 ? "Shared".Translate() : _currentPaxNumber.Value.ToString();
                BtnSeatTextBlock.FontSize = _currentPaxNumber.Value == 0 ? 14 : 26;
            }
            else
                BtnSeatTextBlock.Text = string.Empty;

            // Update current pax number
            CurrentTicket.CurrentPaxNumber = _currentPaxNumber ?? 0;

            // Update Paxs Btn
            BtnPaxTextBlock.Text = (ticket?.Paxs ?? 0).ToString();
        }

        private void paxNumber_Click(object sender, RoutedEventArgs e)
        {
            var ab = (ActionButton)sender;
            var currentPaxNumber = (short)ab.Tag;
            _currentPaxNumber = currentPaxNumber;
            BtnSeatTextBlock.Text = currentPaxNumber == 0 ? "Shared".Translate() : _currentPaxNumber.ToString();
            BtnSeatTextBlock.FontSize = currentPaxNumber == 0 ? 16 : 26;
            if (CurrentTicket != null)
                CurrentTicket.CurrentPaxNumber = currentPaxNumber;
        }

        private void Actions_Loaded(object sender, RoutedEventArgs e)
        {
            _actionsCtrl = (UIElement)sender;
        }

        #endregion

        #region PrePaid

        private void CheckPrepaidCredit_Check(object sender, RoutedEventArgs e)
        {
            State = States.PrePaid;
        }

        private void CheckPrepaidCredit_OnUnchecked(object sender, RoutedEventArgs e)
        {
            State = States.Tables;
        }

        private void ShowPrePaidViewer()
        {
            _prepaidViewer.Visibility = Visibility.Visible;
        }

        private void HidePrePaidViewer()
        {
            _prepaidViewer.Visibility = Visibility.Collapsed;
        }   

        private void PrePaidDescButton_Click(object sender, RoutedEventArgs e)
        {
            var content = Resources["PrePaidDescContent"];
            if(content is not null)
                WindowHelper.OpenDialog(content, false, CurrentTicket, null, MiddleScreen, DialogButtons.CancelOnly);
        }

        #endregion

        #region Notes

        private void NotesButton_Click(object sender, RoutedEventArgs e)
        {
            if (EditorControl.Instance == null) 
                return;
            
            ((TextBox)(_notesContent.Children[0] as Border)?.Child)!.Text = CurrentTicket.Observations;

            EditorControl.Instance.ShowRequest(
                "Notes",
                _notesContent,
                Colors.Black,
                delegate
                {
                    CurrentTicket.Observations = ((_notesContent.Children[0] as Border)?.Child as TextBox)?.Text;
                    
                    mainPage.BusyControl.Show("Updating ticket account");
                    var ticketContract = (POSTicketContract)CurrentTicket.DataContext;

                    Task.Run(async () => await BusinessProxyRepository.Ticket.PersistTicketAsync(ticketContract))
                    .ContinueWith(t =>
                    {
                        mainPage.BusyControl.Close();
                        if (t.IsFaulted && t.Exception != null) 
                            WindowHelper.ShowError(t.Exception);
                        if (t.Status != TaskStatus.RanToCompletion) 
                            return;
                        var result = t.Result;
                        if (result.IsEmpty) 
                            CurrentTicket.DataContext = result.Contract;
                        else 
                            WindowHelper.ShowError(result);
                    },
                    UIThread.Scheduler);

                    return true;
                });
        }

        #endregion

        #region AddPax

        private void AddPax_Click(object sender, RoutedEventArgs e)
        {
            bool OkAction()
            {
                BtnPaxTextBlock.Text = CurrentTicket.Paxs.ToString();
                return true;
            }
            var visual = Resources["PaxsDialog"];
            WindowHelper.OpenDialog(visual, false, CurrentTicket, OkAction,
                MiddleScreen, DialogButtons.OkCancel, null, null, false);
        }

        #endregion

        #region Spa

        public async void SpaServices_Click()
        {
            if (CurrentTicket is { AccountType: CurrentAccountType.Reservation, Room: not null } ticket)
            {
                // Find first the spa reservation from a pms reservation.
                var result = await BusinessProxyRepository.Stand.GetSpaReservationsAsync(
                    new SpaSearchReservationFilterModel
                    {
                        HotelId = ticket.AccountInstallationId,
                        Room = ticket.Room,
                    });

                if (!result.HasErrors)
                {
                    var spaReservation = result.ItemSource.FirstOrDefault();
                    if (spaReservation is { ProductId: not null })
                    {
                        var ok = await AddSpaService(ticket, spaReservation);
                        if(!ok) ErrorsWindow.ShowErrorsWindow("NoSpaServicesErrorMsg".Translate());
                    }
                    else
                    {
                        // Show an error message
                        ErrorsWindow.ShowErrorsWindow("NoSpaReservationForRoomErrorMsg".Translate() + ' ' + ticket.Room);
                    }
                }
                else
                {
                    // Show an error message
                    ErrorsWindow.ShowErrorWindows(result);
                }
            }
            else
            {
                // Show an error message the ticket doesn't have a pms reservation 
            }
        }

        private async Task<bool> AddSpaService(Ticket ticket, SpaReservationSearchFromExternalRecord record)
        {
            if (record is not { ProductId: not null } spaRecord) return false;

            try
            {
                BusyControl.Instance.Show("~Loading~...".Translate());
                var result = await BusinessProxyRepository.Stand.GetSpaServicesAsync(ticket.AccountInstallationId, (Guid)spaRecord.Id);
                BusyControl.Instance.Close();

                if (!result.IsEmpty)
                    ErrorsWindow.ShowErrorWindows(result);
                else
                {

                    var records = result.Item.Items;
                    if (!(records?.Length > 0)) return false;

                    var grouped = records.GroupBy(x => new Guid(x.Id.ToString())).ToList();
                    var services = grouped.Select(group => group.FirstOrDefault()).ToList();

                    var picker = new PickerSpaService(services);
                    bool OkAction()
                    {
                        if (picker.DataGrid.SelectedItem is not SpaSearchReservationProductRecord selected) return true;

                        ticket.SpaServiceId = new Guid(selected.Id.ToString());

                        // Add guest
                        var client = new POSClientByPositionContract
                        {
                            Id = Guid.NewGuid(),
                            ClientName = spaRecord.Guest,
                            Pax = 0
                        };
                        client.Allergies.AddRange(result.Item.Allergies.Select(allergy => new POSClientByPositionAllergyContract(Guid.NewGuid(), Guid.Empty, allergy)));
                        client.Diets.AddRange(result.Item.Diets.Select(diet => new POSClientByPositionDietContract(Guid.NewGuid(), Guid.Empty, diet)));
                        client.UncommonAllergies = spaRecord.UncommonAllergies;
                        // Clear all possible clients added before, it's supposed that this ticket is going to have a spa reservation with a service product and only one guest
                        ticket.ClientsByPosition.Clear();
                        ticket.ClientsByPosition.Add(client);

                        var serviceId = new Guid(selected.Id.ToString());
                        var serviceProducts = grouped.FirstOrDefault(x => x.Key == serviceId)?.ToList();
                        var products = Hotel.Instance.CurrentStandCashierContext.Stand.Products.Where(x => serviceProducts?.FirstOrDefault(sp => sp.ProductId == x.Id) != null).ToList();
                        if (products.Count == 0) return true;

                        var productPicker = new PickerProduct(products);
                        EditorControl.Instance?.ShowRequest("SelectProduct".Translate(), productPicker, Colors.Black, ProductSelectedAction);
                        return false;

                        bool ProductSelectedAction()
                        {
                            var productSelected = products.FirstOrDefault(x => x.Selected);
                            if (productSelected == null) return true;
                            productSelected.IsLaunchFromSpaService = true;
                            ProductsViewer_Product_Click(productSelected, 1);
                            return true;
                        }
                    }

                    EditorControl.Instance?.ShowRequest("Services".Translate(), picker, Colors.Black, OkAction);
                }
            }
            catch (Exception ex)
            {
                ErrorsWindow.ShowErrorsWindow(ex.Message);
            }
            finally
            {
                BusyControl.Instance.Close();
            }

            return true;
        }

        #endregion
    }

    public class SplitTicketDataContext : BindableBase
    {
        #region Members

        private ObservableCollection<Order> _oldOrders;
        private ObservableCollection<Order> _newOrders;

        #endregion
        #region Properties

        public ObservableCollection<Order> OldOrders
        {
            get => _oldOrders;
            set => SetProperty(ref _oldOrders, value);
        }

        public ObservableCollection<Order> NewOrders
        {
            get => _newOrders;
            set => SetProperty(ref _newOrders, value);
        }

        #endregion
        #region Constructor

        public SplitTicketDataContext(Ticket ticket)
        {
            OldOrders = new ObservableCollection<Order>();
            NewOrders = new ObservableCollection<Order>();

            foreach (var order in ticket.Orders)
            {
                if (order.IsVisible)
                    OldOrders.Add(order.Clone());
            }
        }

        #endregion
        #region Methods

        public static void MoveProducts(ObservableCollection<Order> ordersOrig, ObservableCollection<Order> ordersDest, Order order, decimal quantity)
        {
            var auxOrder = ordersOrig.FirstOrDefault(o => o.Id == order.Id);
            var id = auxOrder.Id;

            // Quitamos la orden 
            if (auxOrder.Quantity <= quantity)
            {
                quantity = auxOrder.Quantity;
                ordersOrig.Remove(auxOrder);
            }
            // Reducimos la cantidad de productos
            else
                auxOrder.Quantity -= quantity;

            var newOrder = ordersDest.FirstOrDefault(o => o.Id == id);
            if (newOrder == null)
            {
                newOrder = auxOrder.Clone();
                Order.ExecuteWithoutRaiseEvents(() => { newOrder.Quantity = quantity; }, newOrder);
                ordersDest.Add(newOrder);
            }
            else
                Order.ExecuteWithoutRaiseEvents(() => { newOrder.Quantity += quantity; }, newOrder);
        }

        #endregion
    }
}