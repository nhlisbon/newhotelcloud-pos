﻿using NewHotel.WPF.Common;
using NewHotel.WPF.Common.Controls;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Animation;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for TablesViewerRemoteControl.xaml
    /// </summary>
    public partial class TablesViewerRemoteControl : UserControl
    {
        #region Constructor

        public TablesViewerRemoteControl()
        {
            InitializeComponent();
            moveTables.IsChecked = true;
        }

        #endregion
        #region Properties

        #region TablesViewer

        private TablesViewer TablesViewer
        {
            get { return (TablesViewer)GetValue(TablesViewerProperty); }
            set
            {
                SetValue(TablesViewerProperty, value);
            }
        }

        private static readonly DependencyProperty TablesViewerProperty =
            DependencyProperty.Register(nameof(TablesViewer), typeof(TablesViewer), typeof(TablesViewerRemoteControl), new PropertyMetadata(OnTablesViewerPropertyChangued));

        private static void OnTablesViewerPropertyChangued(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (TablesViewerRemoteControl)d;
            control.DataContext = e.NewValue;
        }


        #endregion
        #region IsExpanded

        public bool IsExpanded
        {
            get { return (bool)GetValue(IsExpandedProperty); }
            set
            {
                SetValue(IsExpandedProperty, value);
            }
        }

        public static readonly DependencyProperty IsExpandedProperty =
            DependencyProperty.Register(nameof(IsExpanded), typeof(bool), typeof(TablesViewerRemoteControl), new PropertyMetadata(OnIsExpandedPropertyChangued));

        private static void OnIsExpandedPropertyChangued(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (TablesViewerRemoteControl)d;
            if ((bool)e.NewValue) control.BeginShowEditSaloonsActionsAnimation();
            else control.BeginHideEditSaloonsActionsAnimation();
        }

        #endregion

        #endregion
        #region Methods

        public void Bind(TablesViewer tablesViewer)
        {
            TablesViewer = tablesViewer;
            TablesViewer.ZoomToSaloon();

            var visibilityBinding = new Binding("Visibility")
            {
                Source = tablesViewer,
                Mode = BindingMode.OneWay,
            };
            SetBinding(TablesViewerRemoteControl.VisibilityProperty, visibilityBinding);
        }

        public void Unbind()
        {
            TablesViewer = null;
            Visibility = Visibility.Collapsed;
        }

        private void InZoom_Click(object sender, RoutedEventArgs e)
        {
            if (TablesViewer != null)
                TablesViewer.InZoom();
        }

        private void OutZoom_Click(object sender, RoutedEventArgs e)
        {
            if (TablesViewer != null)
                TablesViewer.OutZoom();
        }

        private void Joystick_Movement(Joystick joystick)
        {
            UIThread.Invoke(() =>
            {
                if (TablesViewer != null)
                    TablesViewer.JoystickMovement(joystick);
            });
        }

        private void SetDefaultTablesPosition_Click(object sender, RoutedEventArgs e)
        {
            Func<bool> okAction = () =>
            {
                if (TablesViewer != null)
                    TablesViewer.SetDefaultTablesPosition();

                return true;
            };
            MainPage.CurrentInstance.OpenDialog(new TextBlock() { Text = "This operation will change the current distribution of tables in the saloon. \nDo you want to set tables in a default position ?", FontSize = 16 }, false, null, okAction, new Point(this.ActualWidth / 2, this.ActualHeight / 2));
        }

        private void SaveDefaultTablesPosition_Click(object sender, RoutedEventArgs e)
        {
            Func<bool> okAction = () =>
            {
                MainPage.CurrentInstance.BusyControl.Show("Saving");

                if (TablesViewer != null)
                    TablesViewer.SaveDefaultTablesPosition();

                MainPage.CurrentInstance.BusyControl.Close();

                return true;
            };
            MainPage.CurrentInstance.OpenDialog(new TextBlock() { Text = "This operation will save the current distribution of tables in the saloon. \nAre you sure you want to store the current position ?", FontSize = 16 }, false, null, okAction, new Point(this.ActualWidth / 2, this.ActualHeight / 2));
        }

        private void DeselectAllTables_Click(object sender, RoutedEventArgs e)
        {
            if (TablesViewer != null)
                TablesViewer.DeselectAllTables();
        }

        private void UploadChanges_Click(object sender, RoutedEventArgs e)
        {
            if (TablesViewer != null)
                TablesViewer.PersistChanges();
        }

        private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            SetValue(TablesViewerProperty, DataContext as TablesViewer);
        }

        private void MoveTables_Checked(object sender, RoutedEventArgs e)
        {
            // POR LA PUBLICACION
            //joinTables.IsChecked = false;
            //joinTables.IsEnabled = true;

            //setGroups.IsChecked = false;
            //setGroups.IsEnabled = true;

            moveTables.IsEnabled = false;
            if (DataContext != null)
                (DataContext as TablesViewer).State = Controls.TablesViewer.TablesViewerStatus.Moving;
            (Resources["showMoveTools"] as Storyboard).Begin();
            (Resources["hideSetTablesGroupsTools"] as Storyboard).Begin();

        }

        private void MoveTables_Unchecked(object sender, RoutedEventArgs e)
        {
            (Resources["hideMoveTools"] as Storyboard).Begin();
            (Resources["showSetTablesGroupsTools"] as Storyboard).Begin();
        }

        private void JoinTables_Unchecked(object sender, RoutedEventArgs e)
        {

        }

        private void SetGroups_Unchecked(object sender, RoutedEventArgs e)
        {
            //(Resources["hideSetTablesGroupsTools"] as Storyboard).Begin();
        }

        private void TextBlock_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                if (TablesViewer != null)
                    TablesViewer.ResetZoom();
            }
        }

        private void BeginShowEditSaloonsActionsAnimation()
        {
            ((Storyboard)Resources["showEditSaloonsActions"]).Begin();
        }

        private void BeginHideEditSaloonsActionsAnimation()
        {
            ((Storyboard)Resources["hideEditSaloonsActions"]).Begin();
        }

        #endregion 
    }
}
