﻿using System.Drawing;
using System.Windows.Forms;

namespace NewHotel.WPF.App.Pos.Controls
{
    public partial class PrintPDFReport : UserControl
    {
        public PrintPDFReport(string fileName)
        {
            InitializeComponent();

            ((System.ComponentModel.ISupportInitialize)(axAcroPDF1)).BeginInit();
            axAcroPDF1.Location = new Point(0,0);
            axAcroPDF1.Size = new Size(300, 400);
            axAcroPDF1.setView("FitH");
            axAcroPDF1.setLayoutMode("SinglePage");
            ((System.ComponentModel.ISupportInitialize)(axAcroPDF1)).EndInit();

            axAcroPDF1.LoadFile(fileName);
            axAcroPDF1.setShowToolbar(false);
        }

        public void PrintAll()
        {
            axAcroPDF1.printAll();
        }
    }
}
