﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using NewHotel.Contracts;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls
{
    public partial class PickerAreasOrderTable
    {
        public event Action<Order, Area> OnAreasPicked;

        public PickerAreasOrderTable()
        {
            InitializeComponent();
        }
        
        public ObservableCollection<OrderTable> OrderTablesSelected
        {
            get => (ObservableCollection<OrderTable>)GetValue(OrderTablesSelectedProperty);
            private set => SetValue(OrderTablesSelectedProperty, value);
        }

        public static readonly DependencyProperty OrderTablesSelectedProperty =
            DependencyProperty.Register(nameof(OrderTablesSelected), typeof(ObservableCollection<OrderTable>), typeof(PickerAreasOrderTable));

        private void AreaButton_Click(object sender, RoutedEventArgs e)
        {
            var b = (Button)sender;
            var orderTable = (OrderTable)b.Tag;
            var area = ((Area)b.DataContext);
            orderTable.AreaId = area.Id;
            orderTable.Areas.Add(new POSProductLineAreaContract(Guid.NewGuid(), area.Id));
            if (DataContext is Order order && OrderTablesSelected.All(ot => ot.AreaId != null && ot.AreaId != Guid.Empty))
            {
                OnAreasPicked?.Invoke(order, area);
                return;
            }

            OrderTablesSelected = new ObservableCollection<OrderTable>(OrderTablesSelected.Where(x => x.AreaId == null || x.AreaId == Guid.Empty));
        }

        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue is Order order)
            {
                OrderTablesSelected =  order.IsMenu ? order.OrderTablesPendingToDispatch : new ObservableCollection<OrderTable>();
            }
        }
    }
}