﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Localization;
using NewHotel.WPF.Common;
using NewHotel.WPF.Common.Controls;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.Contracts;
using NewHotel.WPF.Model.Model;
using System.Windows.Forms;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for LoginControl.xaml
    /// </summary>
    public partial class LoginControl : ContentControl, ITSKControlsKey
    {
        #region Enums

        public enum ResetMode { Initial, Total, LockUser, LockStand, LockCashier, ChangeStand, ChangeUser, Block };
        private enum MainWindowState { BeforeLogin, LogingIn, AfterLogin, ChooseHotel, HotelConfirmation };

        #endregion
        #region Members

        private MainWindowState _state = MainWindowState.BeforeLogin;
        private ResetMode _mode = ResetMode.Initial;
        private readonly LoginData _loginData;
        private bool _noRaiseChangeFromToCodeEvent = false;

        #endregion 
        #region Constructor

        public LoginControl()
        {
            InitializeComponent();

            DataContext = _loginData = new LoginData();
            _loginData.State = UserPromptState.Enabled;

            numPad.CurrentControl = code;
            buttonsContent.IsEnabled = false;

            if (_loginData.IsByUserPwd)
                BeginFromByCodeAnimation();
            else
                BeginToByCodeAnimation();

            button.Text = "Login";
            buttonsContent.IsEnabled = true;
        }

        #endregion
        #region Events

        public event Action<LoginControl, LoginData> LoginCompleted;

        #endregion
        #region Properties

        private MainWindowState State
        {
            get { return _state; }
            set
            {
                _state = value;
                UIThread.Invoke(() => RunStateChangAnimations());
            }
        }

        #endregion
        #region Methods

        private void HotelErrorAction_Click(object sender, RoutedEventArgs e)
        {
            if (hotelErrorAction.Tag is Action action)
            {
                try
                {
                    action();
                }
                catch(Exception ex)
                {
                    ErrorsWindow.ShowErrorWindows(ex);
                }
            }
        }

        private async void Hotel_Click(object sender, RoutedEventArgs e)
        {
            var hotel = (sender is KeyDescHotelRecord record) ? record :
                (sender as FrameworkElement)?.DataContext as KeyDescHotelRecord;

            if (hotel.Inactive)
            {
                _loginData.State = UserPromptState.Enabled;
                BeginShowNoHotelErrorAnimation("This hotel is not ready yet. Try again later...");
            }
            else
            {
                _loginData.SelectedHotel = hotel;
                await SelectHotel(hotel);
            }
        }

        private async void Login_Click(object sender, RoutedEventArgs e)
        {
            if (MainWindow.ActionsBlocked)
            {
                ErrorsWindow.ShowErrorsWindow("CheckingBDandLANWaiting".Translate(), "Information".Translate());
                return;
            }

            if (State == MainWindowState.BeforeLogin)
            {
                _loginData.State = UserPromptState.Disabled;

                try
                {
                    await CheckConnectionUserAndGetHotelsAsync();
                }
                catch (Exception exception)
                {
                    ErrorsWindow.ShowErrorWindows(exception);
                    _loginData.State = UserPromptState.Enabled;
                    State = MainWindowState.BeforeLogin;
                }
            }
            else if (State == MainWindowState.ChooseHotel)
            {
                if (_loginData.SelectedHotel != null)
                {
                    if (_loginData.SelectedHotel.Inactive)
                    {
                        _loginData.State = UserPromptState.Enabled;
                        BeginShowNoHotelErrorAnimation("This hotel is not ready yet. Try again later...");
                    }
                    // Tenemos un hotel seleccionado 
                    else
                        await SelectHotel(_loginData.SelectedHotel);
                }
            }
            else if (State == MainWindowState.HotelConfirmation)
            {
                if (_loginData.SelectedHotel != null)
                {
                    if (_loginData.SelectedHotel.Inactive)
                    {
                        _loginData.State = UserPromptState.Enabled;
                        BeginShowNoHotelErrorAnimation("This hotel is not ready yet. Try again later...");
                    }
                    // Tenemos un hotel seleccionado 
                    else
                        await SelectHotel(_loginData.SelectedHotel);
                }
            }
            else if (State == MainWindowState.LogingIn)
            {
                _loginData.State = UserPromptState.Disabled;
                await CheckLoginDataAndLogin();
            }
        }

        private void CmbUserName_TextChanged(object sender, TextChangedEventArgs e)
        {
            _loginData.UserName = cmbUserName.Text;
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            _loginData.Password = password.Password;
        }

        private void ChangeUserButton_Click(object sender, RoutedEventArgs e)
        {
            State = MainWindowState.BeforeLogin;
        }

        private void ChangeHotelButton_Click(object sender, RoutedEventArgs e)
        {
            _loginData.State = UserPromptState.Disabled;
            CheckConnectionUserAndGetHotelsAsync(true).ContinueWith(tsk =>
            {
                if (tsk.IsFaulted)
                {
                    _loginData.State = UserPromptState.Enabled;
                    State = MainWindowState.BeforeLogin;
                    ErrorsWindow.ShowErrorWindows(tsk.Exception);
                }
            });
        }

        private void Code_PasswordChanged(object sender, RoutedEventArgs e)
        {
            var ctrl = numPad.CurrentControl;
            try
            {
                numPad.CurrentControl = null;
                numPad.TouchScreenText = code.Password;
            }
            finally
            {
                numPad.CurrentControl = ctrl;
            }

            _loginData.Code = code.Password;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (!_noRaiseChangeFromToCodeEvent)
                BeginFromByCodeAnimation();
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!_noRaiseChangeFromToCodeEvent)
                BeginToByCodeAnimation();
        }

        private void RunStateChangAnimations()
        {
            switch (State)
            {
                case MainWindowState.LogingIn:
                    BeginToLogingInAnimation();
                    BeginShowBackButtonAnimation();
                    BeginHideHotelButtonsAnimation();
                    BeginHideHotelComboBoxAnimation();
                    BeginShowLoginButtonAnimation();
                    BeginHideChangeHotelButtonAnimation();
                    BeginShowHotelDescriptionAnimation();

                    _loginData.State = UserPromptState.Enabled;
                    break;
                case MainWindowState.BeforeLogin:
                    numPad.CurrentControl = code;
                    BeginHideHotelButtonsAnimation();
                    BeginHideHotelComboBoxAnimation();
                    BeginShowLoginButtonAnimation();
                    BeginToBeforeLogingAnimation();
                    BeginHideBackButtonAnimation();

                    if (_loginData.IsByUserPwd)
                        BeginFromByCodeAnimation();
                    else
                        BeginToByCodeAnimation();

                    BeginHideNoHotelErrorAnimation();
                    userPwdGrid.IsEnabled = true;
                    BeginHideChangeHotelButtonAnimation();

                    if (_mode == ResetMode.Initial)
                        BeginHideHotelDescriptionAnimation();

                    _loginData.State = UserPromptState.Enabled;
                    break;
                case MainWindowState.ChooseHotel:
                    if (_loginData.Hotels.Count < 0)
                    {
                        BeginHideLoginButtonAnimation();
                        BeginShowHotelButtonsAnimation();
                    }
                    else
                    {
                        BeginShowHotelComboBoxAnimation();
                        hotelsComboBox.SelectedIndex = _loginData.HotelId.HasValue ? Math.Max(_loginData.Hotels
                            .Select(x => ((Guid)x.Id)).ToList().IndexOf(_loginData.HotelId.Value), 0) : 0;
                    }

                    BeginShowBackButtonAnimation();
                    BeginHideHotelDescriptionAnimation();
                    BeginHideChangeHotelButtonAnimation();
                    userPwdGrid.IsEnabled = false;
                    break;
                case MainWindowState.AfterLogin:
                    _loginData.State = UserPromptState.Disabled;
                    break;
                case MainWindowState.HotelConfirmation:
                    _loginData.State = UserPromptState.Enabled;
                    userPwdGrid.IsEnabled = false;
                    BeginShowChangeHotelButtonAnimation();
                    BeginShowHotelDescriptionAnimation();
                    break;
            }
        }

        /// <summary>
        /// Descarga el hotel del Cloud al Local en caso de no ser el mismo
        /// Luego carga los datos basicos del hotel y carga las cajas y stands que tiene acceso el usario
        /// </summary>
        /// <param name="hotelRecord"></param>
        private async Task SelectHotel(KeyDescHotelRecord? hotelRecord)
        {
            try
            {
                if (_loginData.SelectedHotel != hotelRecord)
                    _loginData.SelectedHotel = hotelRecord;

                var localHotelId = (Guid)_loginData.SelectedHotel.Id;
                _loginData.HotelDescription = _loginData.SelectedHotel.Description;
                _loginData.HotelId = localHotelId;

                var hotelId = _loginData.HotelId.Value;
                var userId = _loginData.UserModel.Id;

                await BusinessProxyRepository.Cashier.CreateSessionTokenAsync(hotelId, userId);

                var instance = Hotel.Instance;
                instance.Id = (Guid)hotelRecord.Id;
                instance.Description = hotelRecord.Description;
                instance.CurrentUserModel = _loginData.UserModel;
                instance.UserId = _loginData.UtilId.Value;

                _noRaiseChangeFromToCodeEvent = true;
                _noRaiseChangeFromToCodeEvent = false;

                await SetCashiersAndStands();

                Hotel.Instance.Stands = _loginData.Stands;
                Hotel.Instance.Cashiers = _loginData.Cashiers;

                _loginData.State = UserPromptState.Enabled;
            }
            catch (Exception e)
            {
                ErrorsWindow.ShowErrorWindows(e);
            }
        }

        private async Task CheckConnectionUserAndGetHotelsAsync(bool changingHotelRequested = false)
        {
            try
            {
                // Verificamos si el usuario y su password es valida
                await _loginData.CheckUserAndGetHotelsAsync();
            }
            catch (Exception ex)
            {
                BeginHideCodeErrorAnimation();
                BeginHideUserErrorAnimation();
                ErrorsWindow.ShowErrorWindows(ex);
                return;
            }

            if (_loginData.IsUserValid)
            {
                // El usuario es valido
                if (_mode == ResetMode.Block)
                    MainWindow.Instance.Unblock();
                else
                {
                    BeginHideCodeErrorAnimation();
                    BeginHideUserErrorAnimation();

                    if (_loginData.Hotels != null && _loginData.Hotels.Count > 0)
                    {
                        // Tenemos al menos un hotel
                        BeginHideNoHotelErrorAnimation();
                        if (_mode == ResetMode.Initial)
                        {
                            // Es la primera carga de este hotel
                            if (_loginData.Hotels.Count == 1)
                            {
                                // Tenemos un solo hotel, entramos directamente 
                                if (_loginData.Hotels[0].Inactive)
                                {
                                    _loginData.State = UserPromptState.Enabled;
                                    BeginShowNoHotelErrorAnimation("This hotel is not ready yet. Try again later...");
                                }
                                else
                                    await SelectHotel(_loginData.Hotels[0]);
                            }
                            else
                            {
                                // Tenemos mas de un hotel, dejemos escoger al usuario
                                State = (changingHotelRequested || !_loginData.HotelId.HasValue) ?
                                    MainWindowState.ChooseHotel : MainWindowState.HotelConfirmation;

                                _loginData.State = UserPromptState.Enabled;
                            }
                        }
                        else
                        {
                            // No estamos inicializando el hotel es un cambio de usuario, stand or cashier
                            Hotel.Instance.CurrentUserModel = _loginData.UserModel;
                            Hotel.Instance.UserId = _loginData.UtilId.Value;

                            var hotel = _loginData.Hotels.FirstOrDefault(x => (Guid)x.Id == Hotel.Instance.Id);

                            if (hotel != null)
                            {
                                // Este usuario tiene acceso a este hotel
                                _loginData.SelectedHotel = hotel;

                                // Crear token para el nuevo usuario
                                await BusinessProxyRepository.Cashier.CreateSessionTokenAsync(Hotel.Instance.Id, Hotel.Instance.UserId);
                                await SetCashiersAndStands();
                            }
                            else
                            {
                                // Este usuario NO tiene acceso a este hotel
                                _loginData.State = UserPromptState.Enabled;
                                BeginShowNoHotelErrorAnimation("This user does not have this hotel availabled");
                            }
                        }
                    }
                    // No tenemos hotel vinculado a este usuario, Error
                    else
                    {
                        _loginData.State = UserPromptState.Enabled;
                        BeginShowNoHotelErrorAnimation("This user does not have any available hotel");
                    }
                }
            }
            else
            {
                if (_loginData.IsByUserPwd)
                {
                    BeginShowUserErrorAnimation();
                    BeginHideCodeErrorAnimation();
                    BeginHideNoCashierErrorAnimation();
                    BeginHideNoHotelErrorAnimation();
                }
                else
                {
                    BeginShowCodeErrorAnimation();
                    BeginHideUserErrorAnimation();
                    BeginHideNoCashierErrorAnimation();
                    BeginHideNoHotelErrorAnimation();
                }

                _loginData.State = UserPromptState.Enabled;
            }
        }

        private async Task CheckLoginDataAndLogin()
        {
            Hotel.Instance.CurrentUserModel = _loginData.UserModel;
            Hotel.Instance.UserId = _loginData.UtilId.Value;

            _loginData.CheckTicketSerieAndControlAccount();
            _loginData.State = UserPromptState.Enabled;

            if (!_loginData.IsControlAccountValid)
            {
                _loginData.SelectedStand = null;
                BeginShowNoHotelErrorAnimation("Control account hasn't been defined in this stand-cashier", async () =>
                {
                    var result = await _loginData.GetStandAsync();
                    UIThread.InvokeAsync(() =>
                    {
                        _loginData.SelectedStand = result;
                        BeginHideNoHotelErrorAnimation();
                    });
                });
            }
            else
            {
                if (!_loginData.IsCashierStandValid)
                {
                    BeginShowNoHotelErrorAnimation("This Stand-Caja is already in use");
                    BeginShowActivationCodeAnimation();
                }
                else if (!_loginData.IsTicketSeriesValid)
                    BeginShowNoHotelErrorAnimation("Ticket series hasn't been defined in this stand-cashier");
                else if (!_loginData.IsInvoiceSeriesValid)
                    BeginShowNoHotelErrorAnimation("Invoice series hasn't been defined in this stand-cashier");
                else if (!_loginData.IsReceiptSeriesValid)
                    BeginShowNoHotelErrorAnimation("Receipt series hasn't been defined in this stand-cashier");
                else
                {
                    State = MainWindowState.AfterLogin;
                    await Login();
                }
            }
        }

        /// <summary>
        /// Carga las cajas y stands disponibles para el usuario
        /// Dependiendo del modo de login y la cantidad de opciones para cajas y stands puede que realice automaticamente el login
        /// </summary>
        private async Task SetCashiersAndStands()
        {
            // Rellenamos la lista de las cajas
            //if (_mode == ResetMode.ChangeUser)
            //{
            //    // Solo nos logeamos y punto
            //    CheckLoginDataAndLogin();
            //}
            if (_mode == ResetMode.LockCashier)
            {
                // Verificar si el usuario tiene acceso a ese cajero 
                State = MainWindowState.LogingIn;
                _loginData.State = UserPromptState.Enabled;

                if (_mode == ResetMode.LockStand)
                    // Verificar si el usuario tiene acceso a ese stand
                    await CheckLoginDataAndLogin();
            }
            else
            {
                _loginData.SelectedCashier = null;
                var cashier = await _loginData.GetCashiersAsync();

                // Luego que carguemos las cajas...
                if (cashier != null)
                {
                    // El usuario tiene acceso a cajas
                    BeginHideNoCashierErrorAnimation();
                    _loginData.SelectedCashier = cashier;

                    // Cambiamos el estado
                    _loginData.State = UserPromptState.Enabled;
                    State = MainWindowState.LogingIn;
                }
                else // ERROR: El usuario no tiene acceso a ninguna caja
                {
                    _loginData.State = UserPromptState.Enabled;
                    BeginShowNoCashierErrorAnimation();
                    State = MainWindowState.BeforeLogin;
                }
            }
        }

        private async Task Login()
        {
            var stand = _loginData.SelectedStand;
            _loginData.State = UserPromptState.Loading;

            MainWindow.Instance.bigLoadingMessageTB.Visibility = Visibility.Visible;
            Visibility = Visibility.Collapsed;

            var result = new Contracts.ValidationResult();

            try
            {
                await Hotel.Instance.GetGeneralSettingsAsync();

                // use activity tracker if requested to
                var mainWindow = (MainWindow)System.Windows.Application.Current.MainWindow;
                mainWindow.SetLogoutTimer();
                
                await SetSessionDataAsync();

                var environmentResult = await stand.LoadEnvironmentAsync();
				environmentResult = stand.UpdateEnvironment(environmentResult);
                await stand.UpdateImages();
                result.Add(environmentResult);

                if (!environmentResult.HasErrors)
                    Hotel.Instance.RefreshEnvironment(environmentResult.Item);

                stand.VerifyTransferReturns(_loginData.SelectedCashier.Id);
                stand.VerifyTransferToAccept();

                MainWindow.Instance.bigLoadingMessageTB.Visibility = Visibility.Collapsed;
                Visibility = Visibility.Visible;

                if (!result.HasErrors)
                    OnLoginCompleted();
                else
                {
                    _loginData.State = UserPromptState.Enabled;
                    State = MainWindowState.BeforeLogin;
                    ErrorsWindow.ShowErrorsWindow(result.ToString());
                }
            }
            catch(Exception ex)
            {
                MainWindow.Instance.bigLoadingMessageTB.Visibility = Visibility.Collapsed;
                Visibility = Visibility.Visible;
                _loginData.State = UserPromptState.Enabled;
                State = MainWindowState.BeforeLogin;
                ErrorsWindow.ShowErrorsWindow($"Error the application got the following error: {ex.Message}. Please try login in again.");
            }
        }

        private async Task SetSessionDataAsync()
        {
            var workDateResult = await BusinessProxyRepository.Stand.SetStandCashierSessionTokenAsync
            (
                _loginData.SelectedStand.Id,
                _loginData.SelectedCashier.Id,
                // jj: mira a ver que hacer aqui, pues no permite pasar un DefaultTaxSchema a null. Puede no estar configurado!!!
                // Comenté y pase Guid.Empty. Estaba mal al leer el config (SettingBusiness linea 121) si era null ponia Guid.Empty
                //Hotel.Instance.DefaultTaxSchema.Value,
                Hotel.Instance.DefaultTaxSchema ?? Guid.Empty,
                Hotel.SelectedLanguage.Code
            );

            _loginData.SelectedStand.StandWorkDate = DateTime.ParseExact(workDateResult.ToString(), "yyyyMMdd", CultureInfo.InvariantCulture);
        }

        private void OnLoginCompleted()
        {
            Hotel.Instance.AddLoguedUser(_loginData.UserModel);
            if (LoginCompleted != null)
            {
                _loginData.State = UserPromptState.Enabled;
                LoginCompleted(this, _loginData);
            }
        }

        internal void Reset(ResetMode mode = ResetMode.Total)
        {
            cmbUserName.ItemsSource = Hotel.Instance.PreviousLoguedUsers.Select(x => x.Username);
            _mode = mode;

            BeginHideUserErrorAnimation();
            BeginHideCodeErrorAnimation();
            BeginHideNoCashierErrorAnimation();
            BeginHideNoHotelErrorAnimation();

            switch (mode)
            {
                case ResetMode.LockUser:
                    cashierComboBox.IsEnabled = false;
                    standsComboBox.IsEnabled = true;
                    userNameComboBox.IsEnabled = false;
                    _loginData.IsByUserPwd = true;
                    isByUserPwdCheckBox.IsEnabled = false;
                    State = MainWindowState.BeforeLogin;
                    break;
                case ResetMode.LockStand:
                    cashierComboBox.IsEnabled = false;
                    standsComboBox.IsEnabled = false;
                    userNameComboBox.IsEnabled = true;
                    _loginData.IsByUserPwd = true;
                    isByUserPwdCheckBox.IsEnabled = true;
                    _loginData.UserName = string.Empty;
                    State = MainWindowState.BeforeLogin;
                    break;
                case ResetMode.ChangeStand:
                    cashierComboBox.IsEnabled = true;
                    standsComboBox.IsEnabled = true;
                    userNameComboBox.IsEnabled = true;
                    _loginData.IsByUserPwd = true;
                    isByUserPwdCheckBox.IsEnabled = true;
                    State = MainWindowState.LogingIn;
                    break;
                case ResetMode.ChangeUser:
                    cashierComboBox.IsEnabled = true;
                    standsComboBox.IsEnabled = true;
                    userNameComboBox.IsEnabled = true;
                    isByUserPwdCheckBox.IsEnabled = true;
                    State = MainWindowState.BeforeLogin;
                    break;
                case ResetMode.Total:
                    cashierComboBox.IsEnabled = true;
                    standsComboBox.IsEnabled = true;
                    userNameComboBox.IsEnabled = true;
                    isByUserPwdCheckBox.IsEnabled = true;
                    _loginData.UserName = string.Empty;
                    State = MainWindowState.BeforeLogin;
                    break;
                case ResetMode.Block:
                    userNameComboBox.IsEnabled = true;
                    isByUserPwdCheckBox.IsEnabled = true;
                    _loginData.UserName = string.Empty;
                    State = MainWindowState.BeforeLogin;
                    break;
            }

            password.Password = string.Empty;
            code.Password = string.Empty;
            _loginData.Reset();
        }

        #region Animations

        private void BeginShowNoHotelErrorAnimation(string message, Action updateAction = null)
        {
            UIThread.Invoke(() =>
            {
                hotelErrorAction.Visibility = updateAction != null ? Visibility.Visible : Visibility.Collapsed;
                hotelErrorAction.Tag = updateAction;
                hotelErrorText.Text = message;
                BeginShowHotelErrorAnimation();
            });

        }

        private void BeginHideNoHotelErrorAnimation()
        {
            UIThread.Invoke(() =>
            {
                BeginHideHotelErrorAnimation();
                BeginHideActivationCodeAnimation();
            });
        }

        private void BeginShowUserErrorAnimation() => UIThread.Invoke(() => (Resources["showUserError"] as Storyboard)?.Begin());
        private void BeginHideUserErrorAnimation() => UIThread.Invoke(() => (Resources["hideUserError"] as Storyboard)?.Begin());
        private void BeginShowCodeErrorAnimation() => UIThread.Invoke(() => (Resources["showCodeError"] as Storyboard)?.Begin());
        private void BeginHideCodeErrorAnimation() => UIThread.Invoke(() => (Resources["hideCodeError"] as Storyboard)?.Begin());
        private void BeginShowNoCashierErrorAnimation() => UIThread.Invoke(() => (Resources["showNoCashierError"] as Storyboard)?.Begin());
        private void BeginHideNoCashierErrorAnimation() => UIThread.Invoke(() => (Resources["hideNoCashierError"] as Storyboard)?.Begin());
        private void BeginHideHotelErrorAnimation() => UIThread.Invoke(() => (Resources["hideHotelError"] as Storyboard)?.Begin());
        private void BeginShowHotelErrorAnimation() => UIThread.Invoke(() => (Resources["showHotelError"] as Storyboard)?.Begin());
        private void BeginShowActivationCodeAnimation() => UIThread.Invoke(() => (Resources["showActivationCode"] as Storyboard)?.Begin());
        private void BeginHideActivationCodeAnimation() => UIThread.Invoke(() => (Resources["hideActivationCode"] as Storyboard)?.Begin());
        private void BeginFromByCodeAnimation() => (Resources["FromByCode"] as Storyboard)?.Begin();
        private void BeginToByCodeAnimation() => (Resources["ToByCode"] as Storyboard)?.Begin();
        private void BeginShowBackButtonAnimation() => (Resources["showBackButton"] as Storyboard)?.Begin();
        private void BeginHideHotelButtonsAnimation() => (Resources["hideHotelButtons"] as Storyboard)?.Begin();
        private void BeginHideHotelComboBoxAnimation() => (Resources["hideHotelComboBox"] as Storyboard)?.Begin();
        private void BeginShowLoginButtonAnimation() => (Resources["showLoginButton"] as Storyboard)?.Begin();
        private void BeginHideBackButtonAnimation() => (Resources["hideBackButton"] as Storyboard)?.Begin();
        private void BeginToBeforeLogingAnimation() => (Resources["ToBeforeLoging"] as Storyboard)?.Begin();
        private void BeginHideChangeHotelButtonAnimation() => (Resources["hideChangeHotelButton"] as Storyboard)?.Begin();
        private void BeginShowHotelComboBoxAnimation() => (Resources["showHotelComboBox"] as Storyboard)?.Begin();
        private void BeginShowHotelButtonsAnimation() => (Resources["showHotelButtons"] as Storyboard)?.Begin();
        private void BeginHideLoginButtonAnimation() => (Resources["hideLoginButton"] as Storyboard)?.Begin();
        private void BeginShowHotelDescriptionAnimation() => (Resources["ShowHotelDescription"] as Storyboard)?.Begin();
        private void BeginShowChangeHotelButtonAnimation() => (Resources["showChangeHotelButton"] as Storyboard)?.Begin();
        private void BeginHideHotelDescriptionAnimation() => (Resources["HideHotelDescription"] as Storyboard)?.Begin();
        private void BeginToLogingInAnimation() => (Resources["ToLogingIn"] as Storyboard)?.Begin();

        #endregion

        #endregion 
        #region ITSKControlsKey

        public void RunCommandEnter(object sender, object control)
        {
            // Esto es provisional
            if (!_loginData.IsByUserPwd && !string.IsNullOrEmpty(_loginData.Code))
            {
                if (button.IsEnabled)
                    Login_Click(null, null);
            }
            if (!string.IsNullOrEmpty(cmbUserName.Text) && !string.IsNullOrEmpty(_loginData.Password))
            {
                // Tenemos todos los datos, registremonos entonces
                if (button.IsEnabled)
                    Login_Click(null, null);
            }
        }

        public void RunCommandTab(object sender, object control)
        {
        }

        #endregion
    }
}