﻿using NewHotel.Contracts;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using NewHotel.WPF.Common;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for ProductButton.xaml
    /// </summary>
    public partial class ProductButton : UserControl, INotifyPropertyChanged
    {
        #region Variables + Constructor

        private readonly Timer _incProductQuantytimer;
        private long _ticksIncProductQuantytimer;
        private bool _isTouching;

        public ProductButton()
        {
            InitializeComponent();
            _incProductQuantytimer = new Timer(500);
            _incProductQuantytimer.Elapsed += IncProductQuantytimerOnElapsed;
        }

        #endregion
        #region Events

        public event Action<ProductButton, Product, int> ProductAdded;

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
        #region Properties

        #region Product

        public Product Product
        {
            get { return (Product)GetValue(ProductProperty); }
            set
            {
                SetValue(ProductProperty, value);
            }
        }

        public static readonly DependencyProperty ProductProperty =
            DependencyProperty.Register(nameof(Product), typeof(Product), typeof(ProductButton));

        #endregion
        #region NoShowPrice

        public bool NoShowPrice
        {
            get { return (bool)GetValue(ShowPriceProperty); }
            set { SetValue(ShowPriceProperty, value); }
        }

        public static readonly DependencyProperty ShowPriceProperty =
            DependencyProperty.Register(nameof(NoShowPrice), typeof(bool), typeof(ProductButton));

        #endregion
        #region IsPressed

        public bool IsPressed
        {
            get { return (bool)GetValue(IsPressedProperty); }
            set
            {
                SetValue(IsPressedProperty, value);
            }
        }

        public static readonly DependencyProperty IsPressedProperty =
            DependencyProperty.Register(nameof(IsPressed), typeof(bool), typeof(ProductButton), new PropertyMetadata(OnIsPressedPropertyChangued));

        private static void OnIsPressedPropertyChangued(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (ProductButton)d;
            if ((bool)e.NewValue)
            {
                // saca la tarjeta
                control.BeginPressedAnimation();

                // inicializar
                control.StartIncrementOfQuanty();
            }
            else
            {
                // Mete la tarjeta
                control.BeginUnpressedAnimation();

                control.SoptIncrementOfQuanty();
            }
        }

        #endregion
        #region Quantity

        public int Quantity
        {
            get { return (int)GetValue(QuantityProperty); }
            set
            {
                SetValue(QuantityProperty, value);
            }
        }

        public static readonly DependencyProperty QuantityProperty =
            DependencyProperty.Register(nameof(Quantity), typeof(int), typeof(ProductButton), new PropertyMetadata(OnQuantityPropertyChangued));


        private static void OnQuantityPropertyChangued(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            // Animar bolita
            var control = (ProductButton)d;
            control.BeginCountChangedAnimation();
        }

        #endregion

        #endregion
        #region Methods

        #region Event Handlers

        private void IncProductQuantytimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            _ticksIncProductQuantytimer++;
            if (_ticksIncProductQuantytimer >= 2)
                UIThread.Invoke(() => { Quantity++; });
        }

        private void UserControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (_isTouching) return;
            // Tenemos un producto seleccionado
            if (!Product.Inactive)
            {
                IsPressed = true;
                BegingToWhiteAnimation();
            }
            else
            {
                ErrorsWindow.ShowErrorsWindow("Product Disable");
            }
        }

        private void UserControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (_isTouching) return;
            if (IsPressed)
            {
                IsPressed = false;
                ProductAdded?.Invoke(this, Product, Quantity);
                BeginToGreenAnimation();
            }
        }

        private void UserControl_MouseLeave(object sender, MouseEventArgs e)
        {
            if (_isTouching) return;
            if (IsPressed)
            {
                IsPressed = false;
                BeginToRedAnimation();
            }
        }

        private void UserControl_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != e.OldValue)
            {
                if (e.NewValue.Equals(true)) BeginToEnabledAnimation();
                else BeginToDisabledAnimation();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            BeginLoadingAnimation();
        }

        private void UserControl_TouchDown(object sender, TouchEventArgs e)
        {
            //F111015:pmp016:01
            if (Product != null)
            {
                // Tenemos un producto seleccionado
                if (!Product.Inactive)
                {
                    _isTouching = true;
                    IsPressed = true;
                    BegingToWhiteAnimation();
                }
            }
        }

        private void UserControl_TouchUp(object sender, TouchEventArgs e)
        {
            if (IsPressed)
            {
                _isTouching = false;
                IsPressed = false;
                ProductAdded?.Invoke(this, Product, Quantity);
                BeginToGreenAnimation();
            }
        }

        private void UserControl_TouchLeave(object sender, TouchEventArgs e)
        {
            if (IsPressed)
            {
                IsPressed = false;
                _isTouching = false;
                BeginToRedAnimation();
            }
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
        
        #region Aux

        private void BeginPressedAnimation()
        {
            ((Storyboard)Resources["pressed"]).Begin();
        }

        private void BeginUnpressedAnimation()
        {
            ((Storyboard)Resources["unPressed"]).Begin();
        }

        private void StartIncrementOfQuanty()
        {
            _ticksIncProductQuantytimer = 0;
            Quantity = 1;
            _incProductQuantytimer.Start();
        }

        private void SoptIncrementOfQuanty()
        {
            _incProductQuantytimer.Stop();
        }

        private void BeginCountChangedAnimation()
        {
            (Resources["countChanged"] as Storyboard).Begin();
        }

        private void BeginLoadingAnimation()
        {
            ((Storyboard)Resources["Loading"]).Begin();
        }

        private void BeginToEnabledAnimation()
        {
            ((Storyboard)Resources["toEnabled"]).Begin();
        }

        private void BeginToDisabledAnimation()
        {
            ((Storyboard)Resources["toDisabled"]).Begin();
        }

        private void BegingToWhiteAnimation()
        {
            ((Storyboard)Resources["toWhite"]).Begin();
        }

        private void BeginToGreenAnimation()
        {
            ((Storyboard)Resources["toGreen"]).Begin();
        }

        private void BeginToRedAnimation()
        {
            ((Storyboard)Resources["toRed"]).Begin();
        }

        #endregion

        #endregion
    }
}
