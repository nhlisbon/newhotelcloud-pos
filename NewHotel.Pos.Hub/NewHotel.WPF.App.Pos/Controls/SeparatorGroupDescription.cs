﻿using NewHotel.WPF.App.Pos.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;

namespace NewHotel.WPF.App.Pos.Controls
{
    public class SeparatorGroupDescription : PropertyGroupDescription
    {
        public IEnumerable<object> Separators = null;
        public SeparatorGroupDescription(string propertyName, IEnumerable<object> separators)
            :base (propertyName)
        {
            Separators = separators;
        }

        public override object GroupNameFromItem(object item, int level, System.Globalization.CultureInfo culture)
        {
            Guid id = (Guid)item.GetType().GetProperty(PropertyName).GetValue(item, null);
            return Separators.FirstOrDefault(x => (x as Separator).Id == id);
            //return base.GroupNameFromItem(item, level, culture);
        }

        public override bool NamesMatch(object groupName, object itemName)
        {
            //Guid id = (Guid)item.GetType().GetProperty(PropertyName).GetValue(item, null);
            return base.NamesMatch(groupName, itemName);
        }
    }
}
