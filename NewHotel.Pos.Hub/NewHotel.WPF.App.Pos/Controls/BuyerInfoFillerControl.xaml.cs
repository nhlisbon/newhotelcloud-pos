﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Localization;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.Contracts;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls
{
    public partial class BuyerInfoFillerControl : UserControl
    {
        #region Enums

        private enum StateControl
        {
            Fill, Pick
        }

        #endregion
        #region Variables

        private StateControl _state;

        #endregion
        #region Constructors

        public BuyerInfoFillerControl()
        {
            InitializeComponent();
            State = StateControl.Fill;
            PickPanel.DataContext = new ClientEntityRequestModel();
        }

        #endregion
        #region Events

        public event Action OnDoubleClick;

        #endregion
        #region Properties

        public BuyerInfo BuyerInfo => DataContext as BuyerInfo;
        public ClientEntityRequestModel ViewModel => PickPanel.DataContext as ClientEntityRequestModel;

        private StateControl State
        {
            get { return _state; }
            set
            {
                _state = value;
                switch (value)
                {
                    case StateControl.Fill:
                        FillPanel.Visibility = Visibility.Visible;
                        PickPanel.Visibility = Visibility.Collapsed;
                        break;
                    case StateControl.Pick:
                        FillPanel.Visibility = Visibility.Collapsed;
                        PickPanel.Visibility = Visibility.Visible;
                        if (BuyerInfo.BaseEntityId.HasValue)
                        {
                            ViewModel.Name = null;
                            ViewModel.FiscalNumber = null;
                            ViewModel.Country = null;
                            LoadClientEntities();
                        }
                        //else
                        //{
                        //    ViewModel.Name = BuyerInfo.Name;
                        //    ViewModel.FiscalNumber = BuyerInfo.FiscalNumber;
                        //    ViewModel.Country = BuyerInfo.SelectedNationality != null ? BuyerInfo.SelectedNationality.Description : null;
                        //}
                        break;
                }
            }
        }

        public bool IsFillState => State == StateControl.Fill;

        #endregion
        #region Methods

        public void SetFillState()
        {
            State = StateControl.Fill;
        }

        private void FillCustomer_Click(object sender, RoutedEventArgs e)
        {
            BuyerInfo?.Reset();
            State = StateControl.Pick;
        }

        private void ClientEntityDataGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (clientEntityDataGrid.SelectedItem is not ClientRecord record) 
                return;

            string cleanedName = record.Name
                .Replace("\t", "")
                .Replace("\r", "")
                .Replace("\n", "")
                .Trim();
            
            BuyerInfo.ClientId = record.Id;
            BuyerInfo.Client = record;
            BuyerInfo.Name = cleanedName;
            BuyerInfo.FiscalNumber = record.FiscalNumber;
            BuyerInfo.Address = record.Address;
            BuyerInfo.SelectedNationality = BuyerInfo.Nationalities.FirstOrDefault(x => x.CountryCode == record.Country);
            BuyerInfo.FiscalMail = record.EmailAddress;
            BuyerInfo.AutomaticProductDiscount = record.ApplyAutomaticProductDiscount;
            BuyerInfo.FiscalPhoneNumber = record.PhoneNumber;
            BuyerInfo.FiscalRegimeTypeCode = record.FiscalRegimeCode;
            BuyerInfo.FiscalResponsabilityCode = record.FiscalResponsibilityCode;
        }

        private async void LoadClientEntities()
        {
            MainPage.CurrentInstance.BusyControl.Show("~Loading~...".Translate());
            try
            {
                var qr = new QueryRequest();
                qr.AddParameter("lang_pk", NewHotelContext.Current.Language);
                if (BuyerInfo.BaseEntityId.HasValue)
                    qr.AddFilter("client_id_filter", BuyerInfo.BaseEntityId.Value);
                else
                {
                    if (!string.IsNullOrEmpty(ViewModel.Name))
                        qr.AddFilter("client_name_filter", ViewModel.Name);
                    if (!string.IsNullOrEmpty(ViewModel.FiscalNumber))
                        qr.AddFilter("client_fiscalnum_filter", ViewModel.FiscalNumber);
                    if (!string.IsNullOrEmpty(ViewModel.Country))
                        qr.AddFilter("client_country_filter", ViewModel.Country);
                }

                var result = await BusinessProxyRepository.Settings.GetClientEntitiesAsync(qr);
                if (result.IsEmpty)
                {
                    var clients = result.ItemSource;

                    if (BuyerInfo.BaseEntityId.HasValue)
                        clientEntityDataGrid.SelectedItem = clients.FirstOrDefault(c => c.Id.Equals(BuyerInfo.BaseEntityId.Value));

                    ViewModel.Records = clients;
                }
                else
                    ErrorsWindow.ShowErrorWindows(result);
            }
            finally
            {
                MainPage.CurrentInstance.BusyControl.Close();
            }
        }

        private void ClientEntityDataGrid_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            OnDoubleClick?.Invoke();
        }

        private void OnButtonSearch_Click(object sender, RoutedEventArgs e)
        {
            LoadClientEntities();
        }

        #endregion
    }
}