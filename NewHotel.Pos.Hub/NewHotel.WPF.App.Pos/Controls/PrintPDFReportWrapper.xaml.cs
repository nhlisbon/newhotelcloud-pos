﻿using System.IO;
using System.Windows.Controls;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for PrintPDFReportWrapper.xaml
    /// </summary>
    public partial class PrintPDFReportWrapper : UserControl
    {

        public PrintPDFReportWrapper()
        {
            InitializeComponent();
        }

        public object DocumentSource
        {
            get { return PdfViewer.DocumentSource; }
            set { PdfViewer.DocumentSource = value; }
        }

        public void PrintReport()
        {
            PdfViewer.Print();
            //if (winFormHost.Child is PrintPDFReport)
            // (winFormHost.Child as PrintPDFReport).PrintAll();
        }
    }
}
