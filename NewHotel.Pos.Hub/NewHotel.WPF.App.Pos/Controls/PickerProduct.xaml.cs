﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using NewHotel.WPF.App.Pos.Model;

namespace NewHotel.WPF.App.Pos.Controls;

public partial class PickerProduct
{
    private readonly List<Product> _products; 

    public PickerProduct(List<Product> products)
    {
        InitializeComponent();
        DataContext = products;
        _products = products;
    }

    private void CheckBox_Click(object sender, RoutedEventArgs e)
    {
        var chk = (CheckBox)sender;
        if (chk.DataContext is not Product product) return;
        product.Selected = true;
        _products.ForEach(p => p.Selected = product.Id == p.Id);
    }
}