﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using NewHotel.Contracts;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Hub.Contracts.Common.Contracts;
using NewHotel.Pos.Localization;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using NewHotel.WPF.Common.Controls.Busy;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls.Payments;

public partial class DepositPanel
{
    public DepositPanel()
    {
        InitializeComponent();
    }

    public void Init(Ticket ticket)
    {
        Value = ticket.BalanceAmount;

        CreditTypeDescription = "Control".Translate();
        CreditType = CurrentAccountType.Control;

        // Start a empty filter
        var filters = new ControlFilters { View = new ObservableCollection<object>().GetCollectionView() };
        Filter.DataContext = filters;

        DataContext = ticket;
        
        // if (Hotel.Instance.IsHtml5)
        //     UpdateHotels(Hotels);

        RunAsync(ControlAccountMode, (Ticket)DataContext);
    }

    public Guid? InstallationId { get; set; }
    public string InstallationDescription { get; set; } = null!;
    public CurrentAccountType CreditType { get; private set; }
    
    public decimal Value
    {
        get => (decimal)GetValue(ValueProperty);
        set => SetValue(ValueProperty, value);
    }

    public static readonly DependencyProperty ValueProperty =
        DependencyProperty.Register(nameof(Value), typeof(decimal),
            typeof(DepositPanel));
    
    #region CreditTypeDescription

    public string CreditTypeDescription
    {
        get => (string)GetValue(CreditTypeDescriptionProperty);
        set => SetValue(CreditTypeDescriptionProperty, value);
    }

    public static readonly DependencyProperty CreditTypeDescriptionProperty =
        DependencyProperty.Register(nameof(CreditTypeDescription),
            typeof(string), typeof(DepositPanel));

    #endregion
    
    private void Hotel_Loaded(object sender, RoutedEventArgs e)
    {
        var rb = (RadioButton)sender;

        var record = rb.DataContext as ChargeHotelRecord;
        rb.IsChecked = record != null && record.Id == Hotel.Instance.Id;
    }
    
    private void Hotel_Checked(object sender, RoutedEventArgs e)
    {
        var rb = (RadioButton)sender;

        if (rb.DataContext is not ChargeHotelRecord record) return;
        InstallationId = record.Id == Hotel.Instance.Id ? new Guid?() : record.Id;
        InstallationDescription = record.Description;
    }
    
    private async void SearchButton_OnClick(object sender, RoutedEventArgs e)
    {
        await RunAsync(ControlAccountMode, (Ticket)DataContext);
    }
    
    private static async Task RunAsync(Func<Ticket, Task> action, Ticket ticket)
    {
        BusyControl.Instance.Show("~Loading~...".Translate());
        try
        {
            await action(ticket);
        }
        catch (Exception ex)
        {
            ErrorsWindow.ShowErrorsWindow(ex.Message);
        }
        finally
        {
            BusyControl.Instance.Close();
        }
    }
    
    private async Task ControlAccountMode(Ticket ticket)
    {
        var filterContent = (ControlFilters)Filter.DataContext;
        var result = await BusinessProxyRepository.Stand.GetControlAccountsAsync(new PmsControlAccountFilterModel
        {
            HotelId = InstallationId,
            AccountName = filterContent.Name,
            AccountEmail = filterContent.Email,
            AccountFiscalNumber = filterContent.FiscalNumber,
            AccountFreeCode = filterContent.FreeCode,
            GetDeposits = true
        });

        if (!result.IsEmpty)
            ErrorsWindow.ShowErrorWindows(result);
        else
        {
            var records = result.ItemSource;
            ticket.AccountRecords = new ObservableCollection<object>(records);

            Grid.GenerateColumns("Pos",
                new GridColumnDefinition("AccountName", "Description".Translate(), 70, null!),
                new GridColumnDefinition("Address", "Address".Translate(), 70, null!),
                new GridColumnDefinition("FiscalNumber", "FiscalNumber".Translate(), 70, null!)
                //new GridColumnDefinition("IsBalanceChecked", "IsBalanceChecked".Translate(), 70, null!)
            );

            filterContent.View = ticket.AccountRecords.GetCollectionView();

            // Select Line if Ticket already have information
            if (ticket.Account.HasValue)
            {
                foreach (var record in records)
                {
                    if (!record.AccountId.HasValue || record.AccountId.Value != ticket.Account.Value) continue;
                    Grid.SelectedItem = record;
                    break;
                }
            }
        }
    }
    
    private static async void UpdateHotels(ItemsControl ctrl)
    {
        var result = await BusinessProxyRepository.Stand.GetChargeHotelsAsync();
        if (!result.IsEmpty)
            ErrorsWindow.ShowErrorWindows(result);
        else
        {
            var items = result.ItemSource.OrderBy(i => i.Id == Hotel.Instance.Id ? 0 : 1).ToArray();

            ctrl.ItemsSource = items;
            if (items.Any(i => i.Id != Hotel.Instance.Id))
                ctrl.Visibility = Visibility.Visible;
        }
    }
}