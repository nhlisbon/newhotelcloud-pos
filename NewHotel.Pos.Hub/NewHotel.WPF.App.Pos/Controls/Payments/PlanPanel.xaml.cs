﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using NewHotel.Contracts;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Hub.Contracts.Common.Contracts;
using NewHotel.Pos.Localization;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using NewHotel.WPF.Common.Controls.Busy;
using NewHotel.WPF.Model.Model;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Localization;
using DevExpress.Utils.Design;
using NewHotel.Pos.Hub.Contracts.Common.Records;
using NewHotel.WPF.Model.Model;
using System.ComponentModel;
using System.Windows.Media;

namespace NewHotel.WPF.App.Pos.Controls.Payments
{
    /// <summary>
    /// Interaction logic for PlanPanel.xaml
    /// </summary>
    public partial class PlanPanel
    {
        #region Members

        private FrameworkElement _filter;
        private ControlTemplate _template;
        private bool _refresh;

        #endregion
        #region Constructor

        public PlanPanel()
        {
            InitializeComponent();
            _filter = Resources["reservationFilter"] as FrameworkElement;
            _template = Resources["isLockColumnTemplate"] as ControlTemplate;
        }

        #endregion
        #region Properties

        public bool IsValid => grid.SelectedValue != null;

        public bool IsRoomOnly
        {
            get
            {
                var record = grid.SelectedValue as ReservationSearchFromExternalRecord;
                if (record == null)
                    return false;

                return record.PensionPk == 1;
            }
        }

        public Ticket Ticket
        {
            get => DataContext as Ticket;
            set
            {
                DataContext = null;
                DataContext = value;
            }
        }

        public ReservationRecord SelectedPlan => (ReservationRecord) grid.SelectedValue;

        public Guid? InstallationId { get; set; }
        public string InstallationDescription { get; set; }

        #region FilterContent

        public object FilterContent
        {
            get => (object)GetValue(FilterContentProperty);
            set => SetValue(FilterContentProperty, value);
        }

        public static readonly DependencyProperty FilterContentProperty =
            DependencyProperty.Register(nameof(FilterContent), typeof(object), typeof(PlanPanel));

        #endregion

        #endregion
        #region Private Methods

        private void LoadDataSource_Click(object sender, RoutedEventArgs e)
        {
            Refresh();
            _refresh = true;
        }

        private async Task UpdateHotels(ItemsControl ctrl)
        {
            var result = await BusinessProxyRepository.Stand.GetChargeHotelsAsync();
            if (!result.IsEmpty)
                ErrorsWindow.ShowErrorWindows(result);
            else
            {
                var items = result.ItemSource.OrderBy(i => i.Id == Hotel.Instance.Id ? 0 : 1);

                ctrl.ItemsSource = items;
                if (items.Any(i => i.Id != Hotel.Instance.Id))
                    ctrl.Visibility = Visibility.Visible;
            }
        }

        private void Hotel_Loaded(object sender, RoutedEventArgs e)
        {
            var rb = (RadioButton)sender;

            var record = rb.DataContext as ChargeHotelRecord;
            rb.IsChecked = record.Id == Hotel.Instance.Id;
        }

        private void Hotel_Checked(object sender, RoutedEventArgs e)
        {
            var rb = (RadioButton)sender;

            var record = rb.DataContext as ChargeHotelRecord;
            if (record != null)
            {
                InstallationId = record.Id == Hotel.Instance.Id ? new Nullable<Guid>() : record.Id;
                InstallationDescription = record.Description;

                if (_refresh)
                    Refresh();
            }
        }

        private async void Refresh()
        {
            BusyControl.Instance.Show("~Loading~...".Translate());
            try
            {
                Ticket.AccountRecords.Clear();
                var checkBox = FindChild<CheckBox>(_filter, "IsCheckedIn");
                var result = await BusinessProxyRepository.Stand.GetReservationsAsync(new ReservationFilterContract { HotelId = InstallationId, JustCheckedInReservations = checkBox.IsChecked.Value });
                if (!result.IsEmpty)
                    ErrorsWindow.ShowErrorWindows(result);
                else
                {
                    var records = result.ItemSource;
                    Ticket.AccountRecords = new ObservableCollection<object>(records);

                    grid.GenerateColumns("Pos",
                        new GridColumnDefinition("IsLock", "Locked".Translate(), 10, _template),
                        new GridColumnDefinition("ReservationNumber", "Reservation".Translate(), 60, null),
                        new GridColumnDefinition("Guests", "Guest".Translate(), 220, null),
                        new GridColumnDefinition("Room", "Room".Translate(), 20, null),
                        new GridColumnDefinition("Pension", "Pension".Translate(), 20, null));
                    
                    var content = (FrameworkElement)Resources["reservationFilter"];
                    var filters = new ReservationFilters()
                    {
                        View = Ticket.AccountRecords.GetCollectionView(), IsCheckIn = checkBox.IsChecked.Value,
                        Room = Ticket.Room ?? ""
                    };

                    content.DataContext = filters;
                    FilterContent = content;

                    if (!Ticket.Account.HasValue) return;
                    foreach (var record in records.Where(record => record.Id == Ticket.Account.Value))
                    {
                        grid.SelectedItem = record;
                        break;
                    }
                }
            }
            catch
            {
                ErrorsWindow.ShowErrorsWindow("SystemError".Translate());
            }
            finally
            {
                BusyControl.Instance.Close();
            }
        }

        private T FindChild<T>(DependencyObject parent, string childName) where T : DependencyObject
        {
            // Confirm parent and childName are valid.
            if (parent == null) return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child
                T childType = child as T;
                if (childType == null)
                {
                    // Recursively drill down the tree
                    foundChild = FindChild<T>(child, childName);

                    // If the child is found, break so we do not overwrite the found child.
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found.
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }

        #endregion
        #region Public Methods

        public async void Init(Ticket ticket)
        {
            Ticket = ticket;
            Ticket.AccountRecords.Clear();

            _refresh = false;
            if (Hotel.Instance.IsHtml5)
                await UpdateHotels(Hotels);

            var content = _filter;
            var filters = new ReservationFilters()
            {
                View = Ticket.AccountRecords.GetCollectionView(),
                IsCheckIn = true
            };

            content.DataContext = filters;
            FilterContent = content;

            Refresh();
        }

        #endregion
    }
}