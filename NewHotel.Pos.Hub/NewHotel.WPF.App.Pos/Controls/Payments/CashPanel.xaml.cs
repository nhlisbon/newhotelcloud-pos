﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Editors.Helpers;
using NewHotel.Pos.Core.Ext;
using NewHotel.Pos.Localization;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common.Controls;
using NewHotel.WPF.Common.Controls.TouchScreen;
using NewHotel.WPF.Model.Model;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ToolTip;

namespace NewHotel.WPF.App.Pos.Controls.Payments
{
	/// <summary>
	/// Interaction logic for CashPanel.xaml
	/// </summary>
	public partial class CashPanel : UserControl
	{
		#region Variables

		private Ticket _ticket;

		/// <summary>
		/// if update base currency value when exchange currency value changued.
		/// Only update when user change exchange currency value from interface visual
		/// </summary>
		private bool _uBaseValOnExchangeVal = true;

		#endregion
		#region Constructors

		public CashPanel()
		{
			InitializeComponent();
			TouchScreenFixNumPad.Enter_Click += TouchScreenFixNumPad_Enter_Click;
		}

		#endregion
		#region Events

		public event Action<CashPanel, Payment> TransactionAccepted;
		public event Action<CashPanel> TransactionCanceled;

		#endregion
		#region Properties

		#region ExcessAsTipText

		public string ExcessAsTipText
		{
			get => (string)GetValue(ExcessAsTipTextProperty);
			set => SetValue(ExcessAsTipTextProperty, value);
		}

		public static readonly DependencyProperty ExcessAsTipTextProperty =
			DependencyProperty.Register(nameof(ExcessAsTipText), typeof(string), typeof(CashPanel));

		#endregion
		#region ExcessAsTip

		public bool ExcessAsTip
		{
			get => (bool)GetValue(ExcessAsTipValidationProperty);
			set => SetValue(ExcessAsTipValidationProperty, value);
		}

		public static readonly DependencyProperty ExcessAsTipValidationProperty =
			DependencyProperty.Register(nameof(ExcessAsTip), typeof(bool), typeof(CashPanel));

		#endregion
		#region PaymentMethod

		public PaymentMethod PaymentMethod
		{
			get => (PaymentMethod)GetValue(PaymentMethodProperty);
			set => SetValue(PaymentMethodProperty, value);
		}

		public static readonly DependencyProperty PaymentMethodProperty =
			DependencyProperty.Register(nameof(PaymentMethod), typeof(PaymentMethod), typeof(CashPanel));

		#endregion
		#region Left

		public decimal Left
		{
			get => (decimal)GetValue(LeftProperty);
			set => SetValue(LeftProperty, value);
		}

		public static readonly DependencyProperty LeftProperty =
			DependencyProperty.Register(nameof(Left), typeof(decimal), typeof(CashPanel));

		#endregion
		#region Value

		public decimal Value
		{
			get => (decimal)GetValue(ValueProperty);
			set => SetValue(ValueProperty, value);
		}

		public static readonly DependencyProperty ValueProperty =
			DependencyProperty.Register(nameof(Value), typeof(decimal), typeof(CashPanel), new PropertyMetadata(ValueChanged));

		private static void ValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var cp = (CashPanel)d;
			cp.UpdateBalance();
			decimal value = cp.Value;

			cp._uBaseValOnExchangeVal = false;
			cp.UpdateExchangeValue(cp.ExchangeSelected, value);
			cp._uBaseValOnExchangeVal = true;
		}

		#endregion
		#region ExchangeValue

		public decimal ExchangeValue
		{
			get => (decimal)GetValue(ExchangeValueProperty);
			set => SetValue(ExchangeValueProperty, value);
		}

		public static readonly DependencyProperty ExchangeValueProperty =
			DependencyProperty.Register(nameof(ExchangeValue), typeof(decimal), typeof(CashPanel), new PropertyMetadata(ExchangeValueChanged));

		private static void ExchangeValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var cp = (CashPanel)d;
			var exchangeValue = cp.ExchangeValue;
			if (cp._uBaseValOnExchangeVal)
				cp.UpdateValue(cp.ExchangeSelected, exchangeValue);
		}

		#endregion
		#region Balance

		public decimal Balance
		{
			get => (decimal)GetValue(BalanceProperty);
			set => SetValue(BalanceProperty, value);
		}

		public static readonly DependencyProperty BalanceProperty = DependencyProperty.Register(nameof(Balance), typeof(decimal), typeof(CashPanel));

		#endregion
		#region BaseExchange

		public CurrencyExchange BaseExchange
		{
			get => (CurrencyExchange)GetValue(BaseExchangeProperty);
			set => SetValue(BaseExchangeProperty, value);
		}

		public static readonly DependencyProperty BaseExchangeProperty = DependencyProperty.Register(nameof(BaseExchange), typeof(CurrencyExchange), typeof(CashPanel));

		#endregion
		#region ExchangeSelected

		public CurrencyExchange ExchangeSelected
		{
			get => (CurrencyExchange)GetValue(CurrenyExchangeProperty);
			set => SetValue(CurrenyExchangeProperty, value);
		}
		public static readonly DependencyProperty CurrenyExchangeProperty = DependencyProperty.Register(nameof(ExchangeSelected), typeof(CurrencyExchange),
																								typeof(CashPanel), new PropertyMetadata(ExchangeSelectedChanged));

		private static void ExchangeSelectedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var cp = (CashPanel)d;

			// Not update base currency value if exchange currency changed, only do convertion.
			cp._uBaseValOnExchangeVal = false;
			cp.UpdateExchangeValue(cp.ExchangeSelected, cp.Value);
			cp._uBaseValOnExchangeVal = true;
		}

		#endregion
		#region CurrencyExchanges

		public IEnumerable<CurrencyExchange> CurrencyExchanges
		{
			get => (IEnumerable<CurrencyExchange>)GetValue(CurrencyExchangesProperty);
			set => SetValue(CurrencyExchangesProperty, value);
		}
		public static readonly DependencyProperty CurrencyExchangesProperty =
			DependencyProperty.Register(nameof(CurrencyExchanges), typeof(IEnumerable<CurrencyExchange>), typeof(CashPanel));

		#endregion
		#region MultCurrency

		public bool MultCurrency
		{
			get => (bool)GetValue(MultCurrencyProperty);
			set => SetValue(MultCurrencyProperty, value);
		}
		public static readonly DependencyProperty MultCurrencyProperty =
			DependencyProperty.Register(nameof(MultCurrency), typeof(bool), typeof(CashPanel));

		#endregion

		#endregion
		#region Methods

		/// <summary>
		/// Gets a Payment object to be used/modified from the database
		/// and fires the action after the payment was grabbed from the db
		/// </summary>
		/// <param name="action">The method to fire after the payment was grabbed from the database</param>
		/// <returns>A Payment object to be used/modified</returns>
		public Payment GetPayment(Action<Payment> action)
		{
			if (Math.Abs(Left - Value) < (decimal)0.01) Value = Left;

			// If the Left amount is greater than the Value, we are asuming that the user wants to pay less than the total amount
			// If not, we are asuming that the user wants to pay the total amount or more than that, that could be treated as Change or Tip
			decimal valueToPay = Left > Value ? Value : Left;
			if (ExcessAsTip)
				valueToPay = Value;

			var pay = Payment.Create(
				_ticket,
				PaymentMethod,
				value: valueToPay,
				received: Value);
			pay.CurrencyId = ExchangeSelected.CurrencyCashier.CurrencyId;
			pay.MultCurrency = ExchangeSelected.CurrencyCashier.Mult;
			pay.ExchangeCurrency = ExchangeSelected.ChangeValue;

			if (ExchangeSelected == BaseExchange) pay.Details = ExchangeSelected.CurrencyCashier.CurrencyId;
			else
			{
				pay.Details = $"{LocalizationMgr.Translation("BaseCurrency")}: {BaseExchange.CurrencyCashier.CurrencyId}\n" +
							  $"{LocalizationMgr.Translation("Currency")}: {ExchangeSelected.CurrencyCashier.CurrencyId}\n" +
							  $"{LocalizationMgr.Translation("Payment")}: {ExchangeValue} {ExchangeSelected.CurrencyCashier.Symbol}";
			}
			action.Invoke(pay);

			return pay;
		}

		public void Initialize(Ticket ticket, decimal? iniAmount = null)
		{
			InitializeCurrencyExchange();

			_ticket = ticket;
			NumericPad.CurrentControl = EditBaseValue;
			NumericPad.ResetNext();
			Left = iniAmount ?? ticket.GetCorrectBalanceAmount();
			Value = Left >= 0 ? Left : 0;
			ExcessAsTipText = LocalizationMgr.Translation("ExcessAsTip");
		}

		#region Private

		private void UpdateBalance()
		{
			Balance = Left - Value;
		}

		private void UpdateExchangeValue(CurrencyExchange exchange, decimal baseValue)
		{
			decimal nValue = exchange.CurrencyCashier.Mult ? baseValue * exchange.ChangeValue : baseValue / exchange.ChangeValue;
			ExchangeValue = nValue.Round2();
		}

		private void UpdateValue(CurrencyExchange exchange, decimal exchangeValue)
		{
			var nValue = exchange.CurrencyCashier.Mult
				? exchangeValue / exchange.ChangeValue
				: exchangeValue * exchange.ChangeValue;
			Value = nValue.Round2();
		}

		/*
		private void InitializeCurrencyExchange()
		{
			if (BaseExchange == null)
			{
				BaseExchange = new CurrencyExchange(Hotel.Instance.StandBaseCurrency.Id);
				BaseExchange.IsSelectedChanged += IsSelectedChanged;
			}
			if (CurrencyExchanges == null)
			{
				var currenyExchanges = new List<CurrencyExchange> { BaseExchange };
				foreach (var currency in Hotel.Instance.CurrentStandCashierContext.Stand.CurrencyExchanges)
				{
					currency.IsSelectedChanged += IsSelectedChanged;
					currenyExchanges.Add(currency);
				}
				CurrencyExchanges = currenyExchanges;
				MultCurrency = currenyExchanges.Count > 1;
			}
			else
				CurrencyExchanges.ForEach(x => x.IsSelected = false);

			ExchangeSelected = BaseExchange;
			ExchangeSelected.IsSelected = true;
		}
		*/

		private void InitializeCurrencyExchange()
		{
			if (BaseExchange == null)
			{
				BaseExchange = new CurrencyExchange(Hotel.Instance.BaseCurrency);
				BaseExchange.IsSelectedChanged += IsSelectedChanged;
			}

			if (CurrencyExchanges == null)
			{
				var currenyExchanges = new List<CurrencyExchange>
				{
					BaseExchange
				};

				foreach (var currency in Hotel.Instance.CurrentStandCashierContext.Stand.CurrencyExchanges)
				{
					var cloned = currency.Clone();
					if (cloned.CurrencyId == PaymentMethod.Unmo || cloned.CurrencyId == Hotel.Instance.StandBaseCurrency.CurrencyId)
					{
						cloned.IsSelectedChanged += IsSelectedChanged;
						currenyExchanges.Add(cloned);
					}
				}

				if (!currenyExchanges.Any(x => x.CurrencyId == Hotel.Instance.StandBaseCurrency.CurrencyId))
					currenyExchanges.Add(new CurrencyExchange(Hotel.Instance.StandBaseCurrency));

				CurrencyExchanges = currenyExchanges;
				MultCurrency = currenyExchanges.Count > 1;
			}
			else
				CurrencyExchanges.ForEach(x => x.IsSelected = false);

			ExchangeSelected = (PaymentMethod is not null && CurrencyExchanges.FirstOrDefault(ce => ce.CurrencyId == PaymentMethod.Unmo) is CurrencyExchange currencyExchange)
				? currencyExchange
				: CurrencyExchanges.First();

			//ExchangeSelected = BaseExchange;
			ExchangeSelected.IsSelected = true;
		}


		private void IsSelectedChanged(object sender, NHPropertyChangedEventArgs args)
		{
			var currencyExchange = (CurrencyExchange)sender;
			if (!currencyExchange.IsSelected)
			{
				BaseExchange.IsSelectedChanged -= IsSelectedChanged;
				BaseExchange.IsSelected = true;
				BaseExchange.IsSelectedChanged += IsSelectedChanged;
				ExchangeSelected = BaseExchange;
				EditBaseValue.Focus();
			}
			else
			{
				foreach (var c in CurrencyExchanges.Where(x => x != currencyExchange))
				{
					c.IsSelectedChanged -= IsSelectedChanged;
					c.IsSelected = false;
					c.IsSelectedChanged += IsSelectedChanged;
				}
				ExchangeSelected = currencyExchange;
			}
		}

		private void TouchScreenFixNumPad_Enter_Click(TouchScreenFixNumPad obj)
		{
			if (obj == NumericPad)
			{
				if (Value != 0) TransactionAccepted?.Invoke(this, GetPayment(null));
				else TransactionCanceled?.Invoke(this);
			}
		}

		private void EditValue_OnGotFocus(object sender, RoutedEventArgs e)
		{
			NumericPad.CurrentControl = (Control)sender;
		}

		#endregion

		#endregion
	}
}
