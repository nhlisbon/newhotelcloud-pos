﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using DevExpress.Xpf.Editors;
using NewHotel.Pos.Core.Ext;
using NewHotel.Pos.Localization;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common.Controls;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls.Payments
{
    /// <summary>
    /// Interaction logic for CreditCardPanel.xaml
    /// </summary>
    public partial class CreditCardPanel : UserControl
    {
        #region Contructors

        public CreditCardPanel()
        {
            InitializeComponent();

            for (int i = DateTime.Now.Year; i < DateTime.Now.Year + 10; i++)
                yearComboBox.Items.Add(i);
        }

        #endregion
        #region Properties

        #region Left

        public decimal Left
        {
            get => (decimal)GetValue(LeftProperty);
            set => SetValue(LeftProperty, value);
        }

        public static readonly DependencyProperty LeftProperty =
            DependencyProperty.Register(nameof(Left), typeof(decimal), typeof(CreditCardPanel));

        #endregion
        #region Value

        public decimal Value
        {
            get => (decimal)GetValue(ValueProperty);
            set => SetValue(ValueProperty, value);
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register(nameof(Value), typeof(decimal), typeof(CreditCardPanel), new PropertyMetadata(ValueChanged));

        private static void ValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var cp = (CreditCardPanel)d;
            decimal value = cp.Value;
            if (value.DecimalPlaces() > 2)
                cp.Value = (decimal)e.OldValue;
        }

        #endregion
        #region Stand

        public Stand Stand
        {
            get => (Stand)GetValue(StandProperty);
            set => SetValue(StandProperty, value);
        }

        public static readonly DependencyProperty StandProperty =
            DependencyProperty.Register(nameof(Stand), typeof(Stand), typeof(CreditCardPanel));

        #endregion
        #region MonthYearNoValidation

        public bool MonthYearNoValidation
        {
            get => (bool)GetValue(MonthYearNoValidationProperty);
            set => SetValue(MonthYearNoValidationProperty, value);
        }

        public static readonly DependencyProperty MonthYearNoValidationProperty =
            DependencyProperty.Register(nameof(MonthYearNoValidation), typeof(bool), typeof(CreditCardPanel));

        #endregion

        #region ExcessAsTip

        public bool ExcessAsTip
        {
            get => (bool)GetValue(ExcessAsTipValidationProperty);
            set => SetValue(ExcessAsTipValidationProperty, value);
        }

        public static readonly DependencyProperty ExcessAsTipValidationProperty =
            DependencyProperty.Register(nameof(ExcessAsTip), typeof(bool), typeof(CreditCardPanel));

        #endregion
        #region ExcessAsTipText

        public string ExcessAsTipText
        {
            get => (string)GetValue(ExcessAsTipTextProperty);
            set => SetValue(ExcessAsTipTextProperty, value);
        }

        public static readonly DependencyProperty ExcessAsTipTextProperty =
            DependencyProperty.Register(nameof(ExcessAsTipText), typeof(string), typeof(CreditCardPanel));

        #endregion

        #region TypeOrNumberValidationsText

        public string TypeOrNumberValidationsText
        {
            get => (string)GetValue(TypeOrNumberValidationsTextProperty);
            set => SetValue(TypeOrNumberValidationsTextProperty, value);
        }

        public static readonly DependencyProperty TypeOrNumberValidationsTextProperty =
            DependencyProperty.Register(nameof(TypeOrNumberValidationsText), typeof(string), typeof(CreditCardPanel));

        #endregion

        #region PaymentMethod

        public PaymentMethod PaymentMethod
        {
            get => (PaymentMethod)GetValue(PaymentMethodProperty);
            set => SetValue(PaymentMethodProperty, value);
        }

        public static readonly DependencyProperty PaymentMethodProperty =
            DependencyProperty.Register(nameof(PaymentMethod), typeof(PaymentMethod), typeof(CreditCardPanel), new PropertyMetadata(PaymentMethodChanged));

        private static void PaymentMethodChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = (CreditCardPanel)d;
            if (obj.Resources["creditCardTypeView"] is CollectionViewSource collection)
                collection.View?.Refresh();
        }

        #endregion
        #endregion
        #region Methods

        public void Initialize(Ticket ticket, decimal? iniAmount = null)
        {
            Left = iniAmount ?? ticket.GetCorrectBalanceAmount();
            MonthYearNoValidation = true;
            ExcessAsTip = true;
            Value = Left >= 0 ? Left : 0;
            numberTextBox.Text = null!;
            typeComboBox.Text = null;
            yearComboBox.Text = null;
            monthComboBox.Text = null;
            TypeOrNumberValidationsText = LocalizationMgr.Translation("TypeOrNumberValidationsText");
            ExcessAsTipText = LocalizationMgr.Translation("ExcessAsTip");
        }
        
        /// <summary>
        /// Gets a Payment object to be used/modified from the database
        /// and fires the action after the payment was grabbed from the db
        /// </summary>
        /// <param name="action">The method to fire after the payment was grabbed from the database</param>
        /// <param name="ticket">The ticket to be used and created</param>
        /// <returns>A Payment object to be used/modified</returns>
        public Payment GetPayment(Action<Payment> action, Ticket ticket)
        {
            try
            {
                CreditCard creditCard;

                if (Math.Abs(Left - Value) < (decimal)0.01)
                    Value = Left;

                var pay = Payment.Create(ticket, PaymentMethod, Value, Value);
                pay.Details = typeComboBox.Text;
                pay.CurrencyId = Stand.CurrencyId;  
                pay.ExchangeCurrency = 1;
                pay.MultCurrency = true;
                pay.Description += "/" + typeComboBox.Text;

                var creditCardType = (CreditCardType)typeComboBox.SelectedValue;
                try
                {
                    creditCard = new CreditCard(numberTextBox.Text, creditCardType.Id, creditCardType.Description, short.Parse((monthComboBox.SelectedIndex + 1).ToString()), short.Parse((yearComboBox.SelectedValue ?? "0").ToString()), "PIN", transactionNumberTextbox.Text, authCodeTextbox.Text);
                }
                catch
                {
                    creditCard = new CreditCard(numberTextBox.Text, creditCardType.Id, creditCardType.Description, 0, 0, "PIN", transactionNumberTextbox.Text, authCodeTextbox.Text);
                }
                creditCard.Id = Guid.NewGuid();

                pay.CreditCardModel = creditCard;
                pay.CreditCard = creditCard.Id;
                pay.SetCreditCard(creditCard, Value, (creditCard.IdentNumber == "0000000000000000"));

                action?.Invoke(pay);

                return pay;
            }
            catch { return null; }
        }

        public bool IsValidData()
        {
            return
                !string.IsNullOrEmpty(typeComboBox.Text) &&
                (
                    MonthYearNoValidation ||
                    (
                        !string.IsNullOrEmpty(yearComboBox.Text) &&
                        !string.IsNullOrEmpty(numberTextBox.Text) &&
                        !string.IsNullOrEmpty(monthComboBox.Text) &&
                        (
                            yearComboBox.SelectedIndex > 0 ||
                            monthComboBox.SelectedIndex + 1 >= Stand.StandWorkDate.Month
                        )
                    )
                ) && Value > 0 && (Value <= Left || ExcessAsTip);
        }

        private void CreditCardTypeView_OnFilter(object sender, FilterEventArgs e)
        {
            var cardType = (CreditCardType)e.Item;
            if (PaymentMethod != null && (!PaymentMethod.AllowCreditCard || !PaymentMethod.AllowDebitCard))
                e.Accepted = PaymentMethod.AllowCreditCard ? cardType.CreditCard : cardType.DebitCard;
            else e.Accepted = true;
        }

        #endregion
    }
}
