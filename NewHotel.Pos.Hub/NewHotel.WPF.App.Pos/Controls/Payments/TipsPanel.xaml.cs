﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using DevExpress.Xpf.Editors;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common.Controls;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls.Payments
{
    public partial class TipsPanel : UserControl
    {
        #region Constuctor

        public TipsPanel()
        {
            InitializeComponent();
        }

        #endregion
        #region Properties

        #region ActivatedTip

        public bool ActivatedTip
        {
            get => (bool)GetValue(ActivatedTipProperty);
            set => SetValue(ActivatedTipProperty, value);
        }

        public static readonly DependencyProperty ActivatedTipProperty =
            DependencyProperty.Register(nameof(ActivatedTip), typeof(bool), typeof(TipsPanel), new PropertyMetadata(OnActivatedTipChanged));

        private static void OnActivatedTipChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var tPanel = (TipsPanel)d;

            var ticket = tPanel.Ticket;
            if (tPanel.ActivatedTip)
            {
                //ticket.LoadTipFromStand();
                tPanel.IsTipPercent = ticket.IsTipPercent;
                //tPanel.IsPercentDefault = ticket.IsTipPercent && ticket.TipValue == decimal.Zero;
                //tPanel.IsValueDefault = !ticket.IsTipPercent && ticket.TipValue == decimal.Zero;
                tPanel.OptionsContainer.Opacity = 1;
            }
            else
            {
                ticket.RemoveTip();
                tPanel.IsTipPercent = false;
                tPanel.IsPercentDefault = false;
                tPanel.IsValueDefault = false;
                tPanel.OptionsContainer.Opacity = 0.4;
            }
        }

        #endregion
        #region IsTipPercent

        public bool? IsTipPercent
        {
            get => (bool)GetValue(IsTipPercentProperty);
            set => SetValue(IsTipPercentProperty, value);
        }

        public static readonly DependencyProperty IsTipPercentProperty =
            DependencyProperty.Register(nameof(IsTipPercent), typeof(bool?), typeof(TipsPanel), new PropertyMetadata(default(bool?), OnIsTipPercentChanged));

        private static void OnIsTipPercentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = (TipsPanel)d;
            obj.UpdateVisualStatus(((bool?)e.NewValue).GetValueOrDefault());
        }

        #endregion
        #region IsPercentDefault

        public bool IsPercentDefault
        {
            get => (bool)GetValue(IsPercentDefaultProperty);
            set => SetValue(IsPercentDefaultProperty, value);
        }

        public static readonly DependencyProperty IsPercentDefaultProperty =
            DependencyProperty.Register(nameof(IsPercentDefault), typeof(bool), typeof(TipsPanel), new PropertyMetadata(OnIsPercentDefaultChanged));

        private static void OnIsPercentDefaultChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var tPanel = (TipsPanel)d;
            if ((bool)e.NewValue)
                tPanel.Ticket.TipPercent = Hotel.Instance.CurrentStandCashierContext.Stand.TipPercent ?? 0;
        }

        #endregion
        #region IsValueDefault

        public bool IsValueDefault
        {
            get => (bool)GetValue(IsValueDefaultProperty);
            set => SetValue(IsValueDefaultProperty, value);
        }

        public static readonly DependencyProperty IsValueDefaultProperty =
            DependencyProperty.Register(nameof(IsValueDefault), typeof(bool), typeof(TipsPanel), new PropertyMetadata(OnIsValueDefaultChanged));

        private static void OnIsValueDefaultChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var tPanel = (TipsPanel)d;
            if ((bool)e.NewValue)
                tPanel.Ticket.TipValue = Hotel.Instance.CurrentStandCashierContext.Stand.TipValue ?? 0;
        }

        #endregion
        #region Ticket

        public Ticket Ticket
        {
            get => (Ticket)GetValue(TicketProperty);
            set => SetValue(TicketProperty, value);
        }

        public static readonly DependencyProperty TicketProperty =
            DependencyProperty.Register(nameof(Ticket), typeof(Ticket), typeof(TipsPanel), new PropertyMetadata(OnTicketPropertyChanged));

        private static void OnTicketPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var tPanel = (TipsPanel)d;
            tPanel.numPad.CurrentControl = tPanel.TipValueControl;
            tPanel.numPad.ResetNext();

            var ticket = (Ticket)e.NewValue;
            if (ticket != null)
            {
                tPanel.UpdateVisualStatus(ticket.IsTipPercent);
                //var tip = ticket.GetTipOrder();
                tPanel.ActivatedTip = true; //tip != null;
            }
        }

        #endregion

        #endregion
        #region Methods

        public void PrepareContext(Ticket ticket)
        {
            ReleaseContext();
            Ticket = ticket;
        }

        public void ReleaseContext()
        {
            ClearValue(TicketProperty);
        }

        /// <summary>
        /// Applies the quick percentages to the tip value on click
        /// </summary>
        /// <param name="sender">The button that was clicked</param>
        /// <param name="e">The args</param>
        private void QuickPercent_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var isTipPercent = IsTipPercent ?? false;
                if (!isTipPercent || IsPercentDefault) return;
                var btn = (Button)sender;
                if (btn.Tag == null) return;
                if (decimal.TryParse(btn.Tag.ToString(), out var tipPercent))
                    Ticket.TipPercent = tipPercent;
            }
            catch (Exception exception)
            {
                ErrorsWindow.ShowErrorWindows(exception);
            }
        }

        private void TipValueControl_OnEditValueChanged(object sender, EditValueChangedEventArgs e)
        {
            Ticket?.RecalculateAll();
        }

        private void TipPercentControl_OnValueChanged(object arg1, NHPropertyChangedEventArgs arg2)
        {
            Ticket?.RecalculateAll();
        }

        private static Binding CreateBinding(object source, string path, BindingMode mode = BindingMode.TwoWay)
        {
            return new Binding
            {
                Path = new PropertyPath(path),
                Mode = mode,
                Source = source
            };
        }

        private void UpdateVisualStatus(bool isTipPercent)
        {
            if (isTipPercent)
            {
                var binding = CreateBinding(Ticket, nameof(WPF.Model.Model.Ticket.TipPercent));
                TipPercentControl.SetBinding(QuantityUpDown.ValueProperty, binding);
                TipPercentControl.Value= Ticket.TipPercent;
                IsTipPercent = isTipPercent;
                PercentContainer.Opacity = 1;
                ValueContainer.Opacity = 0.4;
                
                BindingOperations.ClearBinding(TipValueControl, SpinEdit.ValueProperty);
                TipValueControl.Value = decimal.Zero;
                IsValueDefault = false;
            }
            else
            {
                var binding = CreateBinding(Ticket, nameof(WPF.Model.Model.Ticket.TipValue));
                TipValueControl.SetBinding(SpinEdit.ValueProperty, binding);
                TipValueControl.Value = Ticket.TipValue;
                IsTipPercent = isTipPercent;
                PercentContainer.Opacity = 0.4;
                ValueContainer.Opacity = 1;

                BindingOperations.ClearBinding(TipPercentControl, QuantityUpDown.ValueProperty);
                TipPercentControl.Value = 0;
                IsPercentDefault = false;
            }
        }

        #endregion 
    }
}