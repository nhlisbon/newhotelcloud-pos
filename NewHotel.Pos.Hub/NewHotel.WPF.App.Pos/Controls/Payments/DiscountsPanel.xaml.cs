﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Bars;
using NewHotel.Pos.Hub.Model;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common.Controls;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for DiscountsPanel.xaml
    /// </summary>
    public partial class DiscountsPanel : UserControl
    {
        #region Variables

        private DiscountPanelState _state;
        private Ticket _ticket;

        #endregion
        
        #region Constructor

        public DiscountsPanel()
        {
            InitializeComponent();
        }

        #endregion
        
        #region Enums

        public enum DiscountPanelState
        {
            DetailDiscount,
            PickOrders,
            GroupDiscount,
            MultiSelect
        }

        #endregion
        
        #region Properties

        public DiscountPanelState State
        {
            get => _state;
            set
            {
                _state = value;
                switch (State)
                {
                    case DiscountPanelState.DetailDiscount:
                        SelectionGrid.DataContext = null;
                        DetailsGrid.DataContext = SelectedOrders;

                        DetailsGrid.Visibility = Visibility.Visible;
                        MultiSelectGrid.Visibility = Visibility.Collapsed;
                        SelectionGrid.Visibility = Visibility.Collapsed;
                        ApplyDiscountGrid.Visibility = Visibility.Collapsed;
                        break;
                    case DiscountPanelState.PickOrders:
                        DetailsGrid.DataContext = null;
                        SelectionGrid.DataContext = Orders;

                        SelectionGrid.Visibility = Visibility.Visible;
                        MultiSelectGrid.Visibility = Visibility.Collapsed;
                        DetailsGrid.Visibility = Visibility.Collapsed;
                        ApplyDiscountGrid.Visibility = Visibility.Collapsed;
                        break;
                    case DiscountPanelState.GroupDiscount:
                        DetailsGrid.DataContext = null;
                        SelectionGrid.DataContext = null;
                        
                        ApplyDiscountGrid.Visibility = Visibility.Visible;
                        MultiSelectGrid.Visibility = Visibility.Collapsed;
                        SelectionGrid.Visibility = Visibility.Collapsed;
                        DetailsGrid.Visibility = Visibility.Collapsed;
                        break;
                    case DiscountPanelState.MultiSelect:
                        DetailsGrid.DataContext = null;
                        SelectionGrid.DataContext = null;
                        
                        MultiSelectGrid.Visibility = Visibility.Visible;
                        ApplyDiscountGrid.Visibility = Visibility.Collapsed;
                        SelectionGrid.Visibility = Visibility.Collapsed;
                        DetailsGrid.Visibility = Visibility.Collapsed;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public ObservableCollection<Order> Orders
        {
            get => (ObservableCollection<Order>)GetValue(OrdersProperty);
            set => SetValue(OrdersProperty, value);
        }

        public static readonly DependencyProperty OrdersProperty =
            DependencyProperty.Register(nameof(Orders), typeof(ObservableCollection<Order>), typeof(DiscountsPanel));

        public ObservableCollection<Order> SelectedOrders
        {
            get => (ObservableCollection<Order>)GetValue(SelectedOrdersProperty);
            set => SetValue(SelectedOrdersProperty, value);
        }

        public static readonly DependencyProperty SelectedOrdersProperty =
            DependencyProperty.Register(nameof(SelectedOrders), typeof(ObservableCollection<Order>), typeof(DiscountsPanel));

        private ObservableCollection<Order> OriginalOrders { get; set; } = [];
        
        public ObservableCollection<GroupRecord> Groups
        {
            get => (ObservableCollection<GroupRecord>)GetValue(GroupsProperty);
            set => SetValue(GroupsProperty, value);
        }

        public static readonly DependencyProperty GroupsProperty =
            DependencyProperty.Register(nameof(Groups), typeof(ObservableCollection<GroupRecord>), typeof(DiscountsPanel));

        
        public ObservableCollection<FamilyRecord> Families
        {
            get => (ObservableCollection<FamilyRecord>)GetValue(FamiliesProperty);
            set => SetValue(FamiliesProperty, value);
        }

        public static readonly DependencyProperty FamiliesProperty =
            DependencyProperty.Register(nameof(Families), typeof(ObservableCollection<FamilyRecord>), typeof(DiscountsPanel));
        
        public ObservableCollection<SubFamilyRecord> SubFamilies
        {
            get => (ObservableCollection<SubFamilyRecord>)GetValue(SubFamiliesProperty);
            set => SetValue(SubFamiliesProperty, value);
        }

        public static readonly DependencyProperty SubFamiliesProperty =
            DependencyProperty.Register(nameof(SubFamilies), typeof(ObservableCollection<SubFamilyRecord>), typeof(DiscountsPanel));

        
        #endregion
        
        #region Method

        #region State Management  
        
        /// <summary>
        /// This is called when the discount panel is loading
        /// It loads the data from the passed ticket object and sets the state to PickOrders
        /// </summary>
        /// <param name="ticket">The ticket to get the data from</param>
        public void Prepare(Ticket ticket)
        {
            _ticket = ticket;

            if (ticket.Orders != null)
                Orders = new ObservableCollection<Order>(ticket.Orders.Where(o =>
                    o.IsEditable && !o.AutomaticDiscount));
            
            SelectedOrders = [];
            Groups = [];
            Families = [];
            SubFamilies = [];
            
            // Esto es para activar el modo seleccino de los ordersViewers
            TicketViewer.State = TicketViewer.ActionState.Selecting;
            foreach (var item in Orders)
            {
                item.IsSelected = false;
                item.IsMenuExpanded = false;
                
                AddFamilyFromOrders(item);
                AddSubFamilyFromOrders(item);
                AddGroupFromOrders(item);
            }

            State = DiscountPanelState.PickOrders;
        }

        /// <summary>
        /// This is called when the discount panel is closing
        /// Cleans the discount panel objects from memory and returns the ticket to the editing state
        /// </summary>
        /// <returns>Returns true if the operation was successful otherwise returns false</returns>
        public bool Release()
        {
            TicketViewer.State = TicketViewer.ActionState.Editing;

            foreach (var item in Orders)
            {
                item.IsSelected = false;
                if (item.HasDiscount && item.DiscountModel.Type.Id == Hotel.Instance.NotDiscountType.Id)
                    item.CleanDiscount();
            }
            
            Groups.Clear();
            Families.Clear();
            SubFamilies.Clear();
            
            return true;
        }
        
        private void UpdateSelectedOrdersCollection()
        {
            SelectedOrders = new ObservableCollection<Order>(Orders.Where(order => order.IsSelected));
            if (SelectedOrders.Count == 0)
                SelectedOrders = new ObservableCollection<Order>(Orders);
        }
        
        /// <summary>
        /// This is used has the back button for the discount panel
        /// and it's used to go back to the PickOrders state (original state)
        /// </summary>
        /// <param name="sender">The button that was clicked</param>
        /// <param name="e">The args</param>
        private void BtnPickOrders_Click(object sender, RoutedEventArgs e)
        {
            State = DiscountPanelState.PickOrders;
        }

        #endregion
        
        #region Apply Discount Manually

        private void Details_OnDiscountChanged(object sender, NHPropertyChangedEventArgs e)
        {
            _ticket.RecalculateAll();
        }

        private void DiscountGroup_OnDiscountChanged(object sender, NHPropertyChangedEventArgs e)
        {
            ApplyDiscountGroup((OrderDiscountCtrl)sender);
        }
        
        private void ApplyDiscountGroup(OrderDiscountCtrl orderDiscountCtrl)
        {
            var d = orderDiscountCtrl.Discount;
            foreach (var item in SelectedOrders)
                item.DiscountModel = Discount.Create(d.Type, d.Percent);

            if (DiscountGroupControl.Order != null)
            {
                DiscountGroupControl.Order.ProductPriceBeforeDiscount = SelectedOrders.Sum(x => x.CostBeforeDiscount);
                DiscountGroupControl.Order.ProductPrice = SelectedOrders.Sum(x => x.Cost);
            }

            _ticket.RecalculateAll();
            _ticket.OnAllowPrintDocChanged();
        }
        
        private void BtnDiscountDetails_Click(object sender, RoutedEventArgs e)
        {
            DiscountGroupControl.Release();
            UpdateSelectedOrdersCollection();
            State = DiscountPanelState.DetailDiscount;
        }

        private void BtnDiscountGroup_Click(object sender, RoutedEventArgs e)
        {
            if (DiscountGroupControl.Order == null)
            {
                // Para hacer un mismo descuento a un grupo de ordenes se crea una orden virtual y se le hace descuentos
                // El precio del producto y valor de la orden virtual es la suma de los valores de los productos y precios de las
                // ordenes del grupo
                DiscountGroupControl.Order = new Order { Product = new Product { Description = "Selected Products" } };
                DiscountGroupControl.Prepare();
            }

            UpdateSelectedOrdersCollection();
            ApplyDiscountGroup(DiscountGroupControl);
            State = DiscountPanelState.GroupDiscount;
        } 
        
        #endregion

        #region Apply Discount Multi Select/Filter
        
        /// <summary>
        /// Adds the group of the order to the group list
        /// This is called in the Prepare method to fill the group list on load of the panel
        /// </summary>
        /// <param name="order">The order to be grabbed</param>
        private void AddGroupFromOrders(Order order)
        {
            if (order.Product.Group != null && Groups.All(o => o.Id != order.Product.Group.Value))
                Groups.Add(new GroupRecord
                {
                    Id = order.Product.Group.Value,
                    Description = order.Product.GroupDescription
                });
        }
        
        /// <summary>
        /// Adds the family of the order to the family list
        /// This is called in the Prepare method to fill the family list on load of the panel
        /// </summary>
        /// <param name="order">The order to be grabbed</param>
        private void AddFamilyFromOrders(Order order)
        {
            if (order.Product.Family != null && Families.All(o => o.Id != order.Product.Family.Value))
                Families.Add(new FamilyRecord
                {
                    Id = order.Product.Family.Value,
                    Description = order.Product.FamilyDescription
                });
        }
        
        /// <summary>
        /// Adds the family of the order to the family list
        /// This is called in the Prepare method to fill the family list on load of the panel
        /// </summary>
        /// <param name="order">The order to be grabbed</param>
        private void AddSubFamilyFromOrders(Order order)
        {
            if (order.Product.Subfamily != null && SubFamilies.All(o => o.Id != order.Product.Subfamily.Value))
                SubFamilies.Add(new SubFamilyRecord
                {
                    Id = order.Product.Subfamily.Value,
                    Description = order.Product.SubfamilyDescription
                });
        }
        
        private void ApplyMultiSelect()
        {
            foreach (var order in Orders)
            {
                if (Groups.Any(o => o.IsSelected) && !order.IsSelected)
                    order.IsSelected = Groups.Any(g => g.Id == order.Product.Group && g.IsSelected);
                
                if(Families.Any(o => o.IsSelected) && !order.IsSelected)
                    order.IsSelected = Families.Any(f => f.Id == order.Product.Family && f.IsSelected);
                
                if(SubFamilies.Any(o => o.IsSelected) && !order.IsSelected)
                    order.IsSelected = SubFamilies.Any(s => s.Id == order.Product.Subfamily && s.IsSelected);
            }
        }
        
        /// <summary>
        /// Accepts the multi select/filter and shows the selected orders and hides the rest
        /// Returns to the PickOrders state
        /// </summary>
        /// <param name="sender">The button that was clicked</param>
        /// <param name="e">The args</param>
        private void BtnAcceptMultiSelect_Click(object sender, RoutedEventArgs e)
        {
            ApplyMultiSelect();
            if (Groups.Any(g => g.IsSelected) || Families.Any(f => f.IsSelected) || SubFamilies.Any(s => s.IsSelected))
            {
                UpdateSelectedOrdersCollection();
                OriginalOrders = Orders;
                // This ensures that it hides the orders that are not selected
                Orders = SelectedOrders;
            }
            State = DiscountPanelState.PickOrders;
        }
        
        
        private void BtnResetMultiSelect_Click(object sender, RoutedEventArgs e)
        {
            foreach (var group in Groups)
                group.IsSelected = false;
            foreach (var family in Families)
                family.IsSelected = false;
            foreach (var subFamily in SubFamilies)
                subFamily.IsSelected = false;

            if (OriginalOrders.Count <= 0) return;
            foreach (var order in OriginalOrders)
                order.IsSelected = false;
            
            Orders = OriginalOrders;
            OriginalOrders = [];
            UpdateSelectedOrdersCollection();
            // This ensures that the orders are shown
            State = DiscountPanelState.PickOrders;
        }
        
        private void BtnOpenMultiSelect_Click(object sender, RoutedEventArgs e)
        {
            State = DiscountPanelState.MultiSelect;
        }

        #endregion
        
        #endregion
        
    }
}