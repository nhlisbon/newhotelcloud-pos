﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using NewHotel.Contracts;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Hub.Contracts.Common.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.Records;
using NewHotel.Pos.Localization;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using NewHotel.WPF.Common.Controls.Busy;
using NewHotel.WPF.Common.Controls.Buttons;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls.Payments
{
    /// <summary>
    /// Interaction logic for CreditPanel.xaml
    /// </summary>
    public partial class CreditPanel
    {
        #region Variables

        private CheckButton _lastCheckButton;
        private decimal _left;

        #endregion

        #region Contructor

        public CreditPanel()
        {
            InitializeComponent();

            _lastCheckButton = ReservationCheckBox;
            _left = decimal.Zero;
        }

        #endregion

        #region Properties

        #region FilterContent

        public object FilterContent
        {
            get => GetValue(FilterContentProperty);
            set => SetValue(FilterContentProperty, value);
        }

        public static readonly DependencyProperty FilterContentProperty =
            DependencyProperty.Register(nameof(FilterContent),
                typeof(object), typeof(CreditPanel));

        #endregion

        #region CurrentTicket

        public Ticket CurrentTicket
        {
            get => (Ticket)GetValue(CurrentTicketProperty);
            set => SetValue(CurrentTicketProperty, value);
        }

        public static readonly DependencyProperty CurrentTicketProperty =
            DependencyProperty.Register(nameof(CurrentTicket),
                typeof(Ticket), typeof(CreditPanel));

        #endregion

        #region CreditTypeDescription

        public string CreditTypeDescription
        {
            get => (string)GetValue(CreditTypeDescriptionProperty);
            set => SetValue(CreditTypeDescriptionProperty, value);
        }

        public static readonly DependencyProperty CreditTypeDescriptionProperty =
            DependencyProperty.Register(nameof(CreditTypeDescription),
                typeof(string), typeof(CreditPanel));

        #endregion

        #region Value

        public decimal Value
        {
            get => (decimal)GetValue(ValueProperty);
            set => SetValue(ValueProperty, value);
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register(nameof(Value), typeof(decimal),
                typeof(CreditPanel));

        #endregion

        public bool HideAmount { get; set; }
        public CurrentAccountType CreditType { get; private set; }
        public Guid? InstallationId { get; set; }
        public string InstallationDescription { get; set; } = null!;

        public bool IsValid => Value > decimal.Zero && Value <= _left;

        #endregion

        #region Methods

        private void Refresh(CurrentAccountType accountType)
        {
            switch (accountType)
            {
                case CurrentAccountType.Reservation:
                    Reservation_Click(ReservationCheckBox, null);
                    break;
                case CurrentAccountType.Entities:
                    Entities_Click(EntityCheckBox, null);
                    break;
                case CurrentAccountType.Groups:
                    Groups_Click(GroupsCheckBox, null);
                    break;
                case CurrentAccountType.Control:
                    Control_Click(ControlCheckBox, null);
                    break;
                case CurrentAccountType.Client:
                    Client_Click(ClientCheckBox, null);
                    break;
                case CurrentAccountType.SpaReservation:
                    Spa_Click(SpaCheckBox, null);
                    break;
                case CurrentAccountType.EventReservation:
                    Event_Click(EventCheckBox, null);
                    break;
                case CurrentAccountType.Guests:
                    break;
                case CurrentAccountType.Protocol:
                    break;
                case CurrentAccountType.Owner:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(accountType), accountType, null);
            }
        }

        public void Init(Ticket ticket)
        {
            Value = _left = ticket.GetCorrectBalanceAmount();
            DataContext = ticket;
            CurrentTicket = ticket;
            ReservationCheckBox.IsChecked = true;
            Reservation_Click(ReservationCheckBox, null);

            ticket.AccountRecords.Clear();
    
            if (Hotel.Instance.IsHtml5)
                UpdateHotels(Hotels);
        }

        private void ControlCheckBoxes(object sender)
        {
            var button = (CheckButton)sender;
            if (_lastCheckButton != button)
            {
                var prev = _lastCheckButton;
                _lastCheckButton = button;
                prev.IsChecked = false;
            }
            else
                _lastCheckButton = button;

            _lastCheckButton.IsChecked = true;
        }

        /// <summary>
        /// Handles the click event of the reservation button.
        /// </summary>
        /// <param name="sender">The object that triggered the event.</param>
        /// <param name="e">The event arguments.</param>
        private void Reservation_Click(object sender, RoutedEventArgs? e)
        {
            ControlCheckBoxes(sender);
            CreditTypeDescription = "Reservation".Translate();
            CreditType = CurrentAccountType.Reservation;

            var content = (FrameworkElement)Resources["ReservationFilter"];
            var filters = new ReservationFilters { View = new ObservableCollection<object>().GetCollectionView(), IsCheckIn = true, ShowAccountBalance = true, ShowAllGuests = false };
            if(CurrentTicket.AccountType == CurrentAccountType.Reservation || !string.IsNullOrEmpty(CurrentTicket.CardNumber))
            {
                filters.Room = CurrentTicket.Room;
                filters.CardNumber = CurrentTicket.CardNumber;
            }
            content.DataContext = filters;
            FilterContent = content;
        }

        private void Entities_Click(object sender, RoutedEventArgs? e)
        {
            ControlCheckBoxes(sender);
            CreditTypeDescription = "Entities".Translate();
            CreditType = CurrentAccountType.Entities;

            var content = (FrameworkElement)Resources["EntitiesFilter"];
            var filters = new EntityFilters { View = new ObservableCollection<object>().GetCollectionView() };
            if(CurrentTicket.AccountType == CurrentAccountType.Entities)
            {
                filters.FiscalNumber = CurrentTicket.FiscalDocFiscalNumber;
                filters.Name = CurrentTicket.Name;
            }
            content.DataContext = filters;
            FilterContent = content;
        }

        private void Groups_Click(object sender, RoutedEventArgs? e)
        {
            ControlCheckBoxes(sender);
            CreditTypeDescription = "Groups".Translate();
            CreditType = CurrentAccountType.Groups;

            var content = (FrameworkElement)Resources["GroupsFilter"];
            var filters = new GroupFilters() { View = new ObservableCollection<object>().GetCollectionView() };
            if(CurrentTicket.AccountType == CurrentAccountType.Groups)
            {
                filters.GroupName = CurrentTicket.Name;
                filters.Room = CurrentTicket.Room;
            }
            content.DataContext = filters;
            FilterContent = content;
        }

        private void Control_Click(object sender, RoutedEventArgs? e)
        {
            ControlCheckBoxes(sender);
            CreditTypeDescription = "Control".Translate();
            CreditType = CurrentAccountType.Control;

            var content = (FrameworkElement)Resources["ControlFilter"];
            var filters = new ControlFilters { View = new ObservableCollection<object>().GetCollectionView() };
            if(CurrentTicket.AccountType == CurrentAccountType.Control)
            {
                filters.Name = CurrentTicket.Name;
                filters.Email = CurrentTicket.FiscalDocEmail;
                filters.FiscalNumber = CurrentTicket.FiscalDocFiscalNumber;
            }
            content.DataContext = filters;
            FilterContent = content;
        }

        private void Client_Click(object sender, RoutedEventArgs? e)
        {
            ControlCheckBoxes(sender);
            CreditTypeDescription = "Client".Translate();
            CreditType = CurrentAccountType.Client;

            var content = (FrameworkElement)Resources["ClientsFilter"];
            var filters = new ClientFilters() { View = new ObservableCollection<object>().GetCollectionView() };
            if(CurrentTicket.AccountType == CurrentAccountType.Client)
            {
                filters.Name = CurrentTicket.Name;
                filters.Room = CurrentTicket.Room;
            }
            content.DataContext = filters;
            FilterContent = content;
        }

        private void Spa_Click(object sender, RoutedEventArgs? e)
        {
            ControlCheckBoxes(sender);
            CreditTypeDescription = "Spa".Translate();
            CreditType = CurrentAccountType.SpaReservation;

            var content = (FrameworkElement)Resources["SpaFilter"];
            var filters = new SpaFilters() { View = new ObservableCollection<object>().GetCollectionView() };
            if (CurrentTicket.AccountType == CurrentAccountType.SpaReservation)
            {
                filters.Guest = CurrentTicket.Name;
                filters.Room = CurrentTicket.Room;
            }
            content.DataContext = filters;
            FilterContent = content;
        }

        private void Event_Click(object sender, RoutedEventArgs? e)
        {
            ControlCheckBoxes(sender);
            CreditTypeDescription = "Events".Translate();
            CreditType = CurrentAccountType.EventReservation;

            var content = (FrameworkElement)Resources["EventsFilter"];
            var filters = new EventFilters() { View = new ObservableCollection<object>().GetCollectionView() };
            if(CurrentTicket.AccountType == CurrentAccountType.EventReservation)
            {
                filters.Name = CurrentTicket.Name;
            }
            content.DataContext = filters;
            FilterContent = content;
        }

        private void Amount_Loaded(object sender, RoutedEventArgs e)
        {
            if (!HideAmount) return;
            ((Grid)sender).Visibility = Visibility.Collapsed;
        }

        #region Search Modes

        private static async Task RunAsync(Func<Ticket, Task> action, Ticket ticket)
        {
            BusyControl.Instance.Show("~Loading~...".Translate());
            try
            {
                await action(ticket);
            }
            catch (Exception ex)
            {
                ErrorsWindow.ShowErrorsWindow(ex.Message);
            }
            finally
            {
                BusyControl.Instance.Close();
            }
        }

        private async Task ReservationAccountMode(Ticket ticket)
        {
            var filterContent = (ReservationFilters)((FrameworkElement)FilterContent).DataContext;
            
            var result = await BusinessProxyRepository.Stand.GetReservationsAsync(new ReservationFilterContract
            {
                HotelId = InstallationId,
                GuestName = filterContent.Guest,
                ReservationSerie = filterContent.ReservationNumber,
                RoomNumber = filterContent.Room,
                ShowAccountBalance = filterContent.ShowAccountBalance,
                JustCheckedInReservations = filterContent.IsCheckIn,
                CardNumber = filterContent.CardNumber,
                ShowAllGuests = filterContent.ShowAllGuests,
                RfId = filterContent.Code
            });

            if (!result.IsEmpty)
                ErrorsWindow.ShowErrorWindows(result);
            else
            {
                var records = result.ItemSource;
                ticket.AccountRecords = new ObservableCollection<object>(records);

                Grid.GenerateColumns("Pos",
                    new GridColumnDefinition(nameof(ReservationRecord.IsLock), "Locked".Translate(), 10, (ControlTemplate)Resources["IsLockColumnTemplate"]),
                    new GridColumnDefinition(nameof(ReservationRecord.ReservationNumber), "Reservation".Translate(), 60, null!),
                    new GridColumnDefinition(nameof(ReservationRecord.Guests), "Guest".Translate(), 220, null!),
                    new GridColumnDefinition(nameof(ReservationRecord.Room), "Room".Translate(), 20, null!),
                    new GridColumnDefinition(nameof(ReservationRecord.Pension), "Pension".Translate(), 20, null!)
                );

                filterContent.View = ticket.AccountRecords.GetCollectionView();

                // Select Line if Ticket already have information
                if (ticket.Account.HasValue)
                {
                    var record = records.Find(record => record.AccountId == ticket.Account.Value);
                    if (record != null)
                        Grid.SelectedItem = record;
                }
            }
        }

        private async Task EntityAccountMode(Ticket ticket)
        {
            var filterContent = (EntityFilters)((FrameworkElement)FilterContent).DataContext;
            var result = await BusinessProxyRepository.Stand.GetEntityInstallationsAsync(new PmsCompanyFilterModel
            {
                HotelId = InstallationId,
                Name = filterContent.Name,
                FiscalNumber = filterContent.FiscalNumber,
                Abbreviation = filterContent.Abbreviation
            });

            if (!result.IsEmpty)
                ErrorsWindow.ShowErrorWindows(result);
            else
            {
                var records = result.ItemSource;
                ticket.AccountRecords = new ObservableCollection<object>(records);

                Grid.GenerateColumns("Pos",
                    new GridColumnDefinition("Abbreviation", "Abbrev".Translate(), 20, null!),
                    new GridColumnDefinition("CompanyName", "Company".Translate(), 70, null!),
                    new GridColumnDefinition("FiscalNumber", "FiscalNumb".Translate(), 70, null!),
                    new GridColumnDefinition("CompanyType", "Type".Translate(), 40, null!),
                    new GridColumnDefinition("Category", "Category".Translate(), 50, null!),
                    new GridColumnDefinition("MarketSource", "Origin".Translate(), 50, null!),
                    new GridColumnDefinition("MarketSegment", "Segment".Translate(), 50, null!)
                );

                filterContent.View = ticket.AccountRecords.GetCollectionView();

                // Select Line if Ticket already have information
                if (ticket.Account.HasValue)
                {
                    var record = records.Find(record => record.AccountId == ticket.Account.Value);
                    if (record != null)
                        Grid.SelectedItem = record;
                }
            }
        }

        private async Task GroupAccountMode(Ticket ticket)
        {
            var result = await BusinessProxyRepository.Stand.GetReservationsGroupsAsync(InstallationId);
            if (!result.IsEmpty)
                ErrorsWindow.ShowErrorWindows(result);
            else
            {
                var records = result.ItemSource.ToArray();
                ticket.AccountRecords = new ObservableCollection<object>(records);

                Grid.GenerateColumns("Pos",
                    new GridColumnDefinition("Name", "Description".Translate(), 70, null!),
                    new GridColumnDefinition("ReservationGroupType", "Type".Translate(), 70, null!),
                    new GridColumnDefinition("CountryId", "Country".Translate(), 70, null!),
                    new GridColumnDefinition("Contact", "Contact".Translate(), 70, null!),
                    new GridColumnDefinition("Company", "Company".Translate(), 70, null!),
                    new GridColumnDefinition("Rooms", "Rooms".Translate(), 70, null!),
                    new GridColumnDefinition("Arrival", "Arrival".Translate(), 70, null!),
                    new GridColumnDefinition("Departure", "Departure".Translate(), 70, null!)
                );

                var filterContent = (CreditTypeFilters)((FrameworkElement)FilterContent).DataContext;
                filterContent.View = ticket.AccountRecords.GetCollectionView();

                // Select Line if Ticket already have information
                if (ticket.Account.HasValue)
                {
                    foreach (var record in records)
                    {
                        if (!record.AccountId.HasValue || record.AccountId.Value != ticket.Account.Value) continue;
                        Grid.SelectedItem = record;
                        break;
                    }
                }
            }
        }

        private async Task ControlAccountMode(Ticket ticket)
        {
            var filterContent = (ControlFilters)((FrameworkElement)FilterContent).DataContext;
            var result = await BusinessProxyRepository.Stand.GetControlAccountsAsync(new PmsControlAccountFilterModel
            {
                HotelId = InstallationId,
                AccountName = filterContent.Name,
                AccountEmail = filterContent.Email,
                AccountFiscalNumber = filterContent.FiscalNumber,
                AccountFreeCode = filterContent.FreeCode
            });

            if (!result.IsEmpty)
                ErrorsWindow.ShowErrorWindows(result);
            else
            {
                var records = result.ItemSource;
                ticket.AccountRecords = new ObservableCollection<object>(records);

                Grid.GenerateColumns("Pos",
                    new GridColumnDefinition("AccountName", "Description".Translate(), 70, null!),
                    new GridColumnDefinition("Address", "Address".Translate(), 70, null!),
                    new GridColumnDefinition("FiscalNumber", "FiscalNumber".Translate(), 70, null!),
                    new GridColumnDefinition("IsBalanceChecked", "IsBalanceChecked".Translate(), 70, null!)
                );

                filterContent.View = ticket.AccountRecords.GetCollectionView();

                // Select Line if Ticket already have information
                if (ticket.Account.HasValue)
                {
                    foreach (var record in records)
                    {
                        if (!record.AccountId.HasValue || record.AccountId.Value != ticket.Account.Value) continue;
                        Grid.SelectedItem = record;
                        break;
                    }
                }
            }
        }

        private async Task ClientAccountMode(Ticket ticket)
        {
            var result = await BusinessProxyRepository.Stand.GetClientInstallationsAsync(true, null!, null!);
            if (!result.IsEmpty)
                ErrorsWindow.ShowErrorWindows(result);
            else
            {
                var records = result.ItemSource;
                ticket.AccountRecords = new ObservableCollection<object>(records);

                Grid.GenerateColumns("Pos",
                    new GridColumnDefinition("Regular", "Regular".Translate(), 70, null!),
                    new GridColumnDefinition("UnWanted", "UnWanted".Translate(), 70, null!),
                    new GridColumnDefinition("Name", "Name".Translate(), 70, null!),
                    new GridColumnDefinition("Type", "Type".Translate(), 70, null!),
                    new GridColumnDefinition("Birthday", "Birthday".Translate(), 70, null!),
                    new GridColumnDefinition("Age", "Age".Translate(), 70, null!),
                    new GridColumnDefinition("Country", "Country".Translate(), 70, null!),
                    new GridColumnDefinition("Gender", "Gender".Translate(), 70, null!),
                    new GridColumnDefinition("CivilState", "CivilState2".Translate(), 70, null!),
                    new GridColumnDefinition("Profession", "Profession".Translate(), 70, null!),
                    new GridColumnDefinition("JobFunction", "JobFunction".Translate(), 70, null!)
                );

                var filterContent = (CreditTypeFilters)((FrameworkElement)FilterContent).DataContext;
                filterContent.View = ticket.AccountRecords.GetCollectionView();

                // Select Line if Ticket already have information
                if (ticket.Account.HasValue)
                {
                    foreach (var record in records)
                    {
                        if (!record.CurrentAccountId.HasValue || record.CurrentAccountId.Value != ticket.Account.Value) continue;
                        Grid.SelectedItem = record;
                        break;
                    }
                }
            }
        }

        private async Task SpaAccountMode(Ticket ticket)
        {
            var filterContent = (SpaFilters)((FrameworkElement)FilterContent).DataContext;

            var result = await BusinessProxyRepository.Stand.GetSpaReservationsAsync(new SpaSearchReservationFilterModel
            {
                HotelId = InstallationId,
                ReservationNumber = filterContent.Reservation,
                Guest = filterContent.Guest,
                Room = filterContent.Room,
                Code = filterContent.CardNumber
            });

            if (!result.IsEmpty)
                ErrorsWindow.ShowErrorWindows(result);
            else
            {
                var records = result.ItemSource;
                ticket.AccountRecords = new ObservableCollection<object>(records);

                Grid.GenerateColumns("Pos",
                    new GridColumnDefinition("ReservationNumber", "Reservation".Translate(), 60, null!),
                    new GridColumnDefinition("Guest", "Guest".Translate(), 220, null!),
                    new GridColumnDefinition(nameof(SpaReservationSearchFromExternalRecord.Room), "Room".Translate(), 20, null!),
                    new GridColumnDefinition("CountryCode", "Country".Translate(), 20, null!)
                );

                filterContent.View = ticket.AccountRecords.GetCollectionView();

                // Select Line if Ticket already have information
                if (ticket.Account.HasValue)
                {
                    foreach (var record in records)
                    {
                        if (!record.AccountId.HasValue || record.AccountId.Value != ticket.Account.Value) continue;
                        Grid.SelectedItem = record;
                        break;
                    }
                }
            }
        }

        private async Task EventAccountMode(Ticket ticket)
        {
            var result = await BusinessProxyRepository.Stand.GetEventReservationsAsync(InstallationId);
            if (!result.IsEmpty)
                ErrorsWindow.ShowErrorWindows(result);
            else
            {
                var records = result.ItemSource;
                ticket.AccountRecords = new ObservableCollection<object>(records);

                Grid.GenerateColumns("Pos",
                    new GridColumnDefinition("IsLock", "Locked".Translate(), 10, (ControlTemplate)Resources["IsLockColumnTemplate"]),
                    new GridColumnDefinition("CountryId", "Country".Translate(), 40, null!),
                    new GridColumnDefinition("ReservationNumber", "Reservation".Translate(), 60, null!),
                    new GridColumnDefinition("EventName", "Name".Translate(), 220, null!)
                );

                var filterContent = (CreditTypeFilters)((FrameworkElement)FilterContent).DataContext;
                filterContent.View = ticket.AccountRecords.GetCollectionView();

                // Select Line if Ticket already have information
                if (ticket.Account.HasValue)
                {
                    foreach (var record in records)
                    {
                        if (!record.AccountId.HasValue || record.AccountId.Value != ticket.Account.Value) continue;
                        Grid.SelectedItem = record;
                        break;
                    }
                }
            }
        }

        private static async void UpdateHotels(ItemsControl ctrl)
        {
            var result = await BusinessProxyRepository.Stand.GetChargeHotelsAsync();
            if (!result.IsEmpty)
                ErrorsWindow.ShowErrorWindows(result);
            else
            {
                var items = result.ItemSource.OrderBy(i => i.Id == Hotel.Instance.Id ? 0 : 1).ToArray();

                ctrl.ItemsSource = items;
                if (items.Any(i => i.Id != Hotel.Instance.Id))
                    ctrl.Visibility = Visibility.Visible;
            }
        }

        private void Hotel_Loaded(object sender, RoutedEventArgs e)
        {
            var rb = (RadioButton)sender;

            var record = rb.DataContext as ChargeHotelRecord;
            rb.IsChecked = record != null && record.Id == Hotel.Instance.Id;
        }

        private void Hotel_Checked(object sender, RoutedEventArgs e)
        {
            var rb = (RadioButton)sender;

            if (rb.DataContext is not ChargeHotelRecord record) return;
            InstallationId = record.Id == Hotel.Instance.Id ? new Guid?() : record.Id;
            InstallationDescription = record.Description;

            Refresh(CreditType);
        }

        #endregion

        #endregion

        private async void SearchButton_OnClick(object sender, RoutedEventArgs e)
        {   
            switch (CreditType)
            {
                case CurrentAccountType.Reservation:
                    await RunAsync(ReservationAccountMode, (Ticket)DataContext);
                    break;
                case CurrentAccountType.Entities:
                    await RunAsync(EntityAccountMode, (Ticket)DataContext);
                    break;
                case CurrentAccountType.Groups:
                    await RunAsync(GroupAccountMode, (Ticket)DataContext);
                    break;
                case CurrentAccountType.Guests:
                    break;
                case CurrentAccountType.Control:
                    await RunAsync(ControlAccountMode, (Ticket)DataContext);
                    break;
                case CurrentAccountType.EventReservation:
                    await RunAsync(EventAccountMode, (Ticket)DataContext);
                    break;
                case CurrentAccountType.Protocol:
                    break;
                case CurrentAccountType.Client:
                    await RunAsync(ClientAccountMode, (Ticket)DataContext);
                    break;
                case CurrentAccountType.Owner:
                    break;
                case CurrentAccountType.SpaReservation:
                    await RunAsync(SpaAccountMode, (Ticket)DataContext);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private async void CreditPanel_OnLoaded(object sender, RoutedEventArgs e)
        {
            var ticket = (Ticket)DataContext;

            if (string.IsNullOrEmpty(ticket.Room)) return;
            var filterContent = (ReservationFilters)((FrameworkElement)FilterContent).DataContext;
            filterContent.Room = ticket.Room;

            await RunAsync(ReservationAccountMode, ticket);
        }
    }
}