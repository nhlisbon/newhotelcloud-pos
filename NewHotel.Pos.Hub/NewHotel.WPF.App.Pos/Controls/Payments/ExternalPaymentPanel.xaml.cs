﻿using NewHotel.WPF.App.Pos.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewHotel.WPF.App.Pos.Controls.Payments
{
	/// <summary>
	/// Interaction logic for ExternalPaymentPanel.xaml
	/// </summary>
	public partial class ExternalPaymentPanel : UserControl
	{
		public ExternalPaymentPanel()
		{
			InitializeComponent();
		}

		public new ExternalPayment DataContext
		{
			get => (ExternalPayment)GetValue(DataContextProperty);
			set => SetValue(DataContextProperty, value);
		}
	}

	public class BooleanToVisibilityConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
			(bool)value ? Visibility.Visible : Visibility.Collapsed;

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
			(Visibility)value == Visibility.Visible;
	}
}
