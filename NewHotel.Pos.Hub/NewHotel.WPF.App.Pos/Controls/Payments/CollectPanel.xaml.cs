﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Core.Ext;
using NewHotel.Pos.Hub.Api.Models;
using NewHotel.Pos.Hub.Contracts.Common.Records;
using NewHotel.Pos.Hub.Contracts.Common.Records.Tickets;
using NewHotel.Pos.Hub.Contracts.PaySystem;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Localization;
using NewHotel.Pos.Notifications;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using NewHotel.WPF.Common.Controls;
using NewHotel.WPF.Common.Controls.Busy;
using NewHotel.WPF.Common.Controls.Buttons;
using NewHotel.WPF.Core;
using NewHotel.WPF.Model.Interface;
using NewHotel.WPF.Model.Model;
using NewHotel.WPF.Model.Model.Taxes;
using DataGrid = System.Windows.Controls.DataGrid;
using HorizontalAlignment = System.Windows.HorizontalAlignment;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using MouseEventArgs = System.Windows.Input.MouseEventArgs;
using TextBox = System.Windows.Controls.TextBox;

namespace NewHotel.WPF.App.Pos.Controls.Payments
{
    /// <summary>
    /// Interaction logic for CollectPanel.xaml
    /// </summary>
    public partial class CollectPanel
    {
        #region Variables

        private bool _isPressed;
        private bool _isSecondPage;
        private bool _creditCardFastPaymentActive;
        private decimal _extraSplit;
        private readonly DataGrid _paymentsContent;
        private readonly DataGrid _houseUseContent;
        private readonly CreditPanel _creditPanel;
        private readonly DepositPanel _depositPanel;
        private readonly PlanPanel _planPanel;
        private readonly CashPanel _cashPanel;
        private readonly CreditCardPanel _creditCardPanel;
        private readonly Grid _notesContent;

        #endregion

        #region Constructor

        public CollectPanel()
        {
            InitializeComponent();
            _paymentsContent = (DataGrid)Resources["paymentsContent"];
            _houseUseContent = (DataGrid)Resources["houseUseContent"];
            _creditPanel = (CreditPanel)Resources["creditPanelContent"];
            _depositPanel = (DepositPanel)Resources["DepositPanelContent"];
            _planPanel = (PlanPanel)Resources["planPanelContent"];
            _cashPanel = (CashPanel)Resources["cashPanelContent"];
            _creditCardPanel = (CreditCardPanel)Resources["creditCardPanelContent"];
            _notesContent = (Grid)Resources["notesContent"];
        }

        #endregion

        #region Events

        public event Action<Payment> RemoveOrderClick;

        public event Action<CollectPanel> CloseClick;

        public event Action<CollectPanel, Action, Action>? TicketClick;

        public event Action<CollectPanel, Action, Action>? InvoiceClick;

        public event Action<CollectPanel, Action, Action>? FastInvoiceClick;

        public event Action<CollectPanel, Action, Action>? BallotClick;

        public event Action<CollectPanel> OkClick;

        #endregion

        #region Properties

        #region ButtonClicked
        
        private enum ButtonClicked
        {
            InvoiceButton,
            FastInvoiceButton,
            TicketButton
        }
        
        private ButtonClicked _buttonClicked { get; set; }
        
        #endregion
        
        #region Ticket

        public Ticket Ticket
        {
            get => (Ticket)GetValue(TicketProperty);
            set => SetValue(TicketProperty, value);
        }

        public static readonly DependencyProperty TicketProperty =
            DependencyProperty.Register(nameof(Ticket), typeof(Ticket), typeof(CollectPanel), new PropertyMetadata(OnTicketPropertyChanged));

        private static void OnTicketPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            // var control = (CollectPanel)d;
            // var ticket = (Ticket)e.NewValue;
            //
            // var instance = Hotel.Instance;
            // control.DutyPaxsContent.SetValues(instance.GetDuties(ticket.CheckOpeningDate, DateTime.Now));
        }

        #endregion

        #region Stand

        public Stand Stand
        {
            get => (Stand)GetValue(StandProperty);
            set => SetValue(StandProperty, value);
        }

        public static readonly DependencyProperty StandProperty =
            DependencyProperty.Register(nameof(Stand), typeof(Stand), typeof(CollectPanel), new PropertyMetadata(OnStandPropertyChanged));

        private static void OnStandPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (CollectPanel)d;
            var stand = (Stand)e.NewValue;
            control._creditCardPanel.Stand = stand;
            control.HideTips = !stand.TipService.HasValue;
        }

        #endregion

        #region PaymentTypes and Filters
        
        public IEnumerable<PaymentMethod>? FilteredPaymentMethods
        {
            get => (IEnumerable<PaymentMethod>)GetValue(FilteredPaymentMethodsProperty);
            set => SetValue(FilteredPaymentMethodsProperty, value);
        }
        
        public static readonly DependencyProperty FilteredPaymentMethodsProperty =
            DependencyProperty.Register(nameof(FilteredPaymentMethods), typeof(IEnumerable<PaymentMethod>), typeof(CollectPanel));
        
        #endregion
        
        #region HideTips

        public bool HideTips
        {
            get => (bool)GetValue(HideTipsProperty);
            set => SetValue(HideTipsProperty, value);
        }

        public static readonly DependencyProperty HideTipsProperty =
            DependencyProperty.Register(nameof(HideTips), typeof(bool), typeof(CollectPanel));

        #endregion

        #region Paid

        public int Paid
        {
            get => (int)GetValue(PaidProperty);
            set => SetValue(PaidProperty, value);
        }

        public static readonly DependencyProperty PaidProperty =
            DependencyProperty.Register(nameof(Paid), typeof(int), typeof(CollectPanel), new PropertyMetadata(OnPaidPropertyChangued));

        private static void OnPaidPropertyChangued(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (CollectPanel)d;
            control.paidButton.Opacity = (int)e.NewValue > 0 ? 1 : 0;
        }

        #endregion

        #region UnPaid

        public int UnPaid
        {
            get { return (int)GetValue(UnPaidProperty); }
            set { SetValue(UnPaidProperty, value); }
        }

        public static readonly DependencyProperty UnPaidProperty =
            DependencyProperty.Register(nameof(UnPaid), typeof(int), typeof(CollectPanel), new PropertyMetadata(OnUnpaidPropertyChangued));

        private static void OnUnpaidPropertyChangued(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (CollectPanel)d;
            control.unpaidButton.Opacity = (int)e.NewValue > 0 ? 1 : 0;
        }

        #endregion

        #region Paying

        public int Paying
        {
            get { return (int)GetValue(PayingProperty); }
            set { SetValue(PayingProperty, value); }
        }

        public static readonly DependencyProperty PayingProperty =
            DependencyProperty.Register(nameof(Paying), typeof(int), typeof(CollectPanel), new PropertyMetadata(OnPayingPropertyChangued));

        private static void OnPayingPropertyChangued(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (CollectPanel)d;
            control.payingButton.Opacity = (int)e.NewValue > 0 ? 1 : 0;
        }

        #endregion

        #region SplitAmount

        public decimal SplitAmount
        {
            get { return (decimal)GetValue(SplitAmountProperty); }
            set { SetValue(SplitAmountProperty, value); }
        }

        public static readonly DependencyProperty SplitAmountProperty =
            DependencyProperty.Register(nameof(SplitAmount), typeof(decimal), typeof(CollectPanel));

        #endregion

        #region PayingAmount

        public decimal PayingAmount
        {
            get { return (decimal)GetValue(PayingAmountProperty); }
            set { SetValue(PayingAmountProperty, value); }
        }

        public static readonly DependencyProperty PayingAmountProperty =
            DependencyProperty.Register(nameof(PayingAmount), typeof(decimal), typeof(CollectPanel));

        #endregion

        #region LeftAmount

        public decimal LeftAmount
        {
            get { return (decimal)GetValue(LeftAmountProperty); }
            set { SetValue(LeftAmountProperty, value); }
        }

        public static readonly DependencyProperty LeftAmountProperty =
            DependencyProperty.Register(nameof(LeftAmount), typeof(decimal), typeof(CollectPanel));

        #endregion

        #region IsSplitPayment

        public bool IsSplitPayment
        {
            get { return (bool)GetValue(IsSplitPaymentProperty); }
            set { SetValue(IsSplitPaymentProperty, value); }
        }

        public static readonly DependencyProperty IsSplitPaymentProperty =
            DependencyProperty.Register(nameof(IsSplitPayment), typeof(bool), typeof(CollectPanel), new PropertyMetadata(OnIsSplitPaymentPropertyChangued));

        private static void OnIsSplitPaymentPropertyChangued(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (CollectPanel)d;
            var value = (bool)e.NewValue;
            if (value)
            {
                control.StopPaidEmphasisAnimation();
                control.paidBorderColor.Color = Colors.Gray;
                control.splitActionButton.IsEnabled = false;
                control.paymentsActionButton.IsEnabled = false;
                control.houseUseActionButton.IsEnabled = false;
                control.creditActionButton.IsEnabled = false;
                control.planActionButton.IsEnabled = false;

                control.notesButton.IsEnabled = false;
                control.tipsButton.IsEnabled = false;
                control.discountButton.IsEnabled = false;

                control.PaymentTypesContainer.IsEnabled = false;
                control.BeginToSplitModeAnimation();
                MainPage.CurrentInstance.ProhibitClose();
            }
            else
            {
                control.IsSecondPage = false;
                control.splitActionButton.IsEnabled = true;
                control.paymentsActionButton.IsEnabled = true;
                control.houseUseActionButton.IsEnabled = true;
                control.creditActionButton.IsEnabled = true;
                control.planActionButton.IsEnabled = true;

                control.notesButton.IsEnabled = true;
                control.tipsButton.IsEnabled = true;
                control.discountButton.IsEnabled = true;

                control.PaymentTypesContainer.IsEnabled = true;
                control.BeginFromSplitModeAnimation();
                MainPage.CurrentInstance.AllowClose();
            }
        }

        #endregion
        
        #region IsPaymentTicketAllowed

        public bool IsPaymentTicketAllowed
        {
            get => (bool)GetValue(IsPaymentTicketAllowedProperty);
            set => SetValue(IsPaymentTicketAllowedProperty, value);
        }

        public static readonly DependencyProperty IsPaymentTicketAllowedProperty =
            DependencyProperty.Register(nameof(IsPaymentTicketAllowed), typeof(bool), typeof(CollectPanel));

        #endregion

        #region IsPaymentInvoiceAllowed

        public bool IsPaymentInvoiceAllowed
        {
            get => (bool)GetValue(IsPaymentInvoiceAllowedProperty);
            set => SetValue(IsPaymentInvoiceAllowedProperty, value);
        }

        public static readonly DependencyProperty IsPaymentInvoiceAllowedProperty =
            DependencyProperty.Register(nameof(IsPaymentInvoiceAllowed), typeof(bool), typeof(CollectPanel));

        #endregion

        #region DeletePaymentText

        public string DeletePaymentText => "Delete".Translate();

        #endregion
        
        public Ticket SplitTicket { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        private static IPosSettings PosSettings => CwFactory.Resolve<IPosSettings>();
        private static IAppSecurity AppSecurity => CwFactory.Resolve<IAppSecurity>();
        private static IWindow WindowManager => CwFactory.Resolve<IWindow>();
        
        private bool IsSecondPage
        {
            get => _isSecondPage;
            set
            {
                _isSecondPage = value;
                if (value)
                    BeginToSecondSplitPageAnimation();
                else
                    BeginToFirstSplitPageAnimation();
            }
        }

        private bool ShowInvoiceButton => Hotel.Instance.ShowInvoiceButton;

        #region Traslations

        private static string HouseUseLbl => "Houseuse".Translate();
        private static string MealPlanLbl => "MealPlan".Translate();
        private static string CreditRoomLbl => "RoomCredit".Translate();
        private static string ErrorLbl => "Error".Translate();
        private static string UserNotAuthorizedLbl => "UserNotAuthorized".Translate();
        private static string InvalidDataLbl => "InvalidData".Translate();
        private static string PaymentIncompleteLbl => "PaymentIncompleted".Translate();
        public static string SearchLbl => "Search".Translate();

        #endregion

        #endregion

        #region Methods

        #region Public

        public void ActiveFastPaymentByCreditCard(PaymentMethod pay)
        {
            _creditCardFastPaymentActive = true;
            _creditCardPanel.PaymentMethod = pay;
            ShowCardPanel();
        }

        #endregion

        #region Aux
        
        private void Filter_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (FilteredPaymentMethods == null || !FilteredPaymentMethods.Any())
            {
                WindowManager.ShowError("The payment method list is empty. Cannot filter the list.");
                return;
            }
            
            var textBox = sender as TextBox;
            var searchText = textBox?.Text.ToLower();
            
            if(searchText == null) 
                return;
            
            var filtered = Stand.PaymentMethods.Where(x => 
                x.Description.ToLower().Contains( searchText.ToLower() ) || x.Abbreviation.ToLower().Contains( searchText.ToLower() )
            ).ToList();
            FilteredPaymentMethods = filtered;
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property.Name != nameof(UnPaid) || IsSecondPage) return;

            if (UnPaid == 0) UnPaid = 1;
            SplitAmount = (Ticket.DebtAmount / UnPaid).Round2();
            _extraSplit = Ticket.DebtAmount - SplitAmount * UnPaid;
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            CloseClick?.Invoke(this);
        }

        /// <summary>
        /// Handle the click event of the button to reset the payment
        /// This resets the payment to the original state before any changes and also resets the panels
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            // Ticket.ClearCollectData();
            Ticket?.RestoreStatus(andKeepIt: true, keepDiscounts: false);
            _cashPanel.ExcessAsTip = false;
            IsPaymentInvoiceAllowed = true;
            IsPaymentTicketAllowed = true;
            if (!IsSplitPayment) 
                return;
            IsSplitPayment = false;
            PayingAmount = 0;
        }

        private void PaymentType_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var fe = (FrameworkElement)sender;

            if (fe?.DataContext is not PaymentMethod payType) return;
			switch (payType)
			{
				case { IsCashPayment: true }:
					// Es en efectivo
					_cashPanel.PaymentMethod = payType;
					ShowCashPanel();
					break;
				case { IsCreditCardPayment: true }:
					// Es una tarjeta
					_creditCardPanel.PaymentMethod = payType;
					ShowCardPanel();
					break;
				//case { HasPaySystem: true }:
				//	ShowPaySystemPanel(payType);
				//	break;
			}

			//if (payType.IsCreditCardPayment)
   //         {
   //             _creditCardPanel.PaymentMethod = payType;
   //             ShowCardPanel();
   //         }
   //         else
   //         {
   //             _cashPanel.PaymentMethod = payType;
   //             ShowCashPanel();
   //         }
        }

        private void RemoveImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _isPressed = true;
        }

        private void RemoveImage_MouseLeave(object sender, MouseEventArgs e)
        {
            _isPressed = false;
        }

        private void RemoveImage_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (_isPressed)
                RemoveOrderClick?.Invoke(paymentsDataGrid.SelectedValue as Payment);
            _isPressed = false;
        }

        private void EditorControl_ShowRequested(EditorControl sender, EditorControlShowRequestArgs e)
        {
            ShowEditorControl(e.Name, e.Content, e.Background, e.OkAction, e.CancelAction);
        }

        private void EditorControl_AcceptChanges(EditorControl obj)
        {
            // Si el editor es el buscador de payments no se cierra
            var keepEditor = editorControl.EditorContent == _paymentsContent;
            // Si la accion del ok da un error (false), no se cierra
            keepEditor |= !editorControl.OkAction();

            if (!keepEditor)
                HideEditorControl();
        }

        private void EditorControl_CancelChanges(EditorControl obj)
        {
            editorControl.CancelAction?.Invoke();
            HideEditorControl();
        }

        private void UserControl_GotFocus(object sender, RoutedEventArgs e)
        {
            EditorControl.Instance = editorControl;
        }

        private void Payments_Click(object sender, RoutedEventArgs e)
        {
            AppSecurity.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.EnableCashPayment, (isValid, validUser) =>
            {
                if (isValid)
                {
                    if (EditorControl.Instance == null) 
                        return;
                    
                    _paymentsContent.ItemsSource = Stand.PaymentMethods;
                    _paymentsContent.UnselectAll();

                    EditorControl.Instance.ShowRequest("Payments".Translate(), _paymentsContent, Colors.Black, () =>
                    {
                        if (_paymentsContent.SelectedValue is PaymentMethod payType)
                        {
							switch (payType)
                            {
								case { IsCashPayment: true }:
                                    // Es en efectivo
                                    _cashPanel.PaymentMethod = payType;
                                    MainPage.CurrentInstance.AllowClose();
                                    ActionsContainer.IsEnabled = true;
                                    ShowCashPanel();
									break;
								case { IsCreditCardPayment: true }:
                                    // Es una tarjeta
                                    _creditCardPanel.PaymentMethod = payType;
                                    MainPage.CurrentInstance.AllowClose();
                                    ActionsContainer.IsEnabled = true;
                                    ShowCardPanel();
									break;
								//case { HasPaySystem: true }:
								//	MainPage.CurrentInstance.AllowClose();
								//	ActionsContainer.IsEnabled = true;
								//	ShowPaySystemPanel(payType);
								//	break;
                            }

                            return true;
                        }

                        return false;
                    });
                }
                else
                    WindowManager.ShowError(UserNotAuthorizedLbl);
            });
        }

        private void HouseUse_Click(object sender, RoutedEventArgs e)
        {
            AppSecurity.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.EnableHouseUse, (isValid, validUser) =>
            {
                #region Validations

                if (Ticket is not null && (Ticket.HasMealPlanPayment || Ticket.HasHouseUsePayment))
                {
                    var msg = "HaveAnotherPaymentResetPayments".Translate();
                    var pay = "";
                    
                    if (Ticket.HasMealPlanPayment)
                        pay = MealPlanLbl;
                    else if (Ticket.HasHouseUsePayment)
                        pay = HouseUseLbl;
                    
                    WindowManager.ShowError(string.Format(msg, pay), ErrorLbl);
                    return;
                }

                #endregion
                
                if (isValid && EditorControl.Instance != null)
                {
                    _houseUseContent.UnselectAll();

                    EditorControl.Instance.ShowRequest(HouseUseLbl, _houseUseContent, Colors.Black, () =>
                    {
                        if (_houseUseContent.SelectedValue is not InternalUse internalUse) 
                            return false;
                        if (Ticket == null) 
                            return true;
                        
                        var pay = Payment.CreateHouseUse(Ticket, internalUse.Id, HouseUseLbl, internalUse.Description,
                            Ticket.TotalAmount, Stand.CurrencyId
                        );
                        Ticket.InternalUse = internalUse.Id;

                        if (internalUse.Unlimited)
                            pay.InternalUsePercent = -decimal.One;
                        else if (internalUse.LimitPercent.HasValue)
                            pay.InternalUsePercent = internalUse.LimitPercent.Value;
                        else
                            pay.InternalUsePercent = decimal.Zero;

                        if (Stand.InternalUseRate.HasValue && pay.InternalUsePercent < decimal.Zero)
                            // Tenemos una tarifa definida para internal use en el stand y el usuario es unlimited
                            // SOLO EN ESE CASO CAMBIAMOS LA TARIFA
                            Ticket.SetRateType(RateType.InternalUse);
                            
                        // Es unlimited 
                        // Es stand tiene un porciento de descuento a los productos
                        if (pay.InternalUsePercent < decimal.Zero && Stand.InternalUsePercent.HasValue && Ticket.Orders != null)
                            foreach (var item in Ticket.Orders)
                            {
                                // Cambiamos el Price, Tax e Importe de cada orden, incluso las anuladas
                                item.SetPriceRateTax(RateType.InternalUse, null,
                                    Stand.InternalUsePercent.Value);
                            }
                            
                        if (pay.InternalUsePercent is < decimal.Zero or 100)
                            Ticket.ClearPayments();
                        Ticket.RecalculateAll();

                        // Aplicamos si el porciento de pago del usuario por uso interno si es que tiene
                        // y si es unlimited entonces lo paga por uso interno
                        pay.BaseValue = Ticket.TotalAmount.GetDiscountValue(pay.InternalUsePercent < decimal.Zero ? decimal.Zero : pay.InternalUsePercent);

                        // Not allow full payment on percentage different than 100
                        if (pay.InternalUsePercent != 100 && Ticket.TotalAmount - pay.BaseValue < PosSettings.AddedValueOnHouseUse)
                            pay.BaseValue -= PosSettings.AddedValueOnHouseUse;

                        pay.ReceiveValue = pay.BaseValue;
                        Ticket.AddPayment(pay);
                        
                        if (!Ticket.HasCashPayment)
                        {
                            IsPaymentTicketAllowed = true;
                            IsPaymentInvoiceAllowed = true;
                        }
                        return true;
                    });
                }
                else
                    WindowManager.ShowError(!isValid ? UserNotAuthorizedLbl : ErrorLbl);
            });
        }

        private void MealPlan_Click(object sender, RoutedEventArgs e)
        {

            AppSecurity.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.EnableMealPlan, (isValid, _) =>
            {
                #region Validations
                
                if (Ticket is not null && (Ticket.HasMealPlanPayment || Ticket.HasHouseUsePayment))
                {
                    var msg = "HaveAnotherPaymentResetPayments".Translate();
                    var pay = "";
                        
                    if (Ticket.HasMealPlanPayment)
                        pay = MealPlanLbl;
                    else if (Ticket.HasHouseUsePayment)
                        pay = HouseUseLbl;
                        
                    WindowManager.ShowError(string.Format(msg, pay), ErrorLbl);
                    return;
                }

                #endregion
                
                if (isValid && EditorControl.Instance is not null && Ticket is not null)
                {
                    _planPanel.Init(Ticket);

                    EditorControl.Instance.ShowRequest(MealPlanLbl, _planPanel, Colors.Black, () =>
                    {
                        if (_planPanel.IsValid)
                        {
                            try
                            {
                                var amount = Ticket.TotalAmount;
                                if (Stand.PensionRate.HasValue)
                                {
                                    Ticket.SetRateType(RateType.PensionMode);
                                    Ticket.RecalculateAll();
                                }

                                var item = _planPanel.SelectedPlan;

                                var installationAccountId = _planPanel.InstallationId;
                                var installationDescription = _planPanel.InstallationDescription;

                                var accountDescription = new StringBuilder();
                                if (installationAccountId.HasValue)
                                    accountDescription.AppendLine(installationDescription);
                                accountDescription.AppendLine($"{item.ReservationNumber} ({item.Room})");
                                accountDescription.Append(item.Guests);

                                Ticket.Account = item.AccountId;
                                Ticket.AccountInstallationId = installationAccountId;
                                Ticket.AccountType = CurrentAccountType.Reservation;
                                Ticket.AccountDescription = accountDescription.ToString();

                                var details = new StringBuilder();
                                details.AppendLine($"{"Reservation".Translate()}: {item.ReservationNumber}");
                                details.AppendLine($"{"Room".Translate()}: {item.Room}");
                                details.AppendLine($"{"Titular".Translate()}: {item.Guests}");
                                details.AppendLine($"{"Discount".Translate()}: {amount - Ticket.TotalAmount}");

                                amount = decimal.Zero;
                                if (!PosSettings.LeaveMealPlanOpened || Ticket.TotalAmount == decimal.Zero)
                                    amount = Ticket.TotalAmount;

                                var pay = Payment.CreateRoomPlan(Ticket, MealPlanLbl, details.ToString(), amount, Stand.CurrencyId, item.AccountId);
                                Ticket.AddPayment(pay);
                                    
                                if (!Ticket.HasCashPayment)
                                {
                                    IsPaymentTicketAllowed = true;
                                    IsPaymentInvoiceAllowed = true;
                                }
                                return true;
                            }
                            catch (Exception ex)
                            {
                                ErrorsWindow.ShowErrorWindows(ex);
                                return false;
                            }
                        }

                        WindowManager.ShowError(InvalidDataLbl, ErrorLbl);
                        return false;
                    });
                }
                else
                    WindowManager.ShowError(!isValid ? UserNotAuthorizedLbl : ErrorLbl);
            });
        }

        private void RoomCharge_Click(object sender, RoutedEventArgs e)
        {
            AppSecurity.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.EnableRoomCharge, (isValid, _) =>
            {
                #region Validations

                if (Ticket is not null && Ticket.HasCreditRoomPayment)
                {
                    var msg = "HaveAnotherPaymentResetPayments".Translate();
                    var pay = "";
                        
                    if (Ticket.HasCreditRoomPayment)
                        pay = CreditRoomLbl;
                        
                    WindowManager.ShowError(string.Format(msg, pay), ErrorLbl);
                    return;
                }
                
                #endregion
                
                if (isValid && EditorControl.Instance is not null && Ticket is not null)
                {
                    _creditPanel.Init(Ticket);
                    EditorControl.Instance.ShowRequest("Credit".Translate(), _creditPanel, Colors.Black, () =>
                    {
                        if (!_creditPanel.IsValid)
                        {
                            WindowManager.ShowError("InvalidAmountMoney".Translate(), ErrorLbl);
                            return false;
                        }

                        var item = _creditPanel.Grid.SelectedValue;
                        switch (item)
                        {
                            case null:
                                WindowManager.ShowError("You must select a reservation to add a credit.", "Info");
                                return false;
                            case ReservationRecord { AllowCreditPos: false }:
                                WindowManager.ShowError("It is not allowed to add a credit for this reservation.", "Info");
                                return false;
                        }
                        
                        var isLock = false;
                        var isWideDetails = false;
                        var details = new StringBuilder();

                        var installationAccountId = _creditPanel.InstallationId;
                        var installationDescription = _creditPanel.InstallationDescription;

                        var accountId = item.GetPropertyValue<Guid>("AccountId");
                        Ticket.Account = accountId;
                        Ticket.AccountInstallationId = installationAccountId;

                        CurrentAccountType? accountType = null;

                        var accountDescription = new StringBuilder();
                        if (installationAccountId.HasValue)
                            accountDescription.AppendLine(installationDescription);

                        switch (_creditPanel.CreditType)
                        {
                            case CurrentAccountType.Reservation:
                                isLock = item.GetPropertyValue<bool>("IsLock");
                                accountType = CurrentAccountType.Reservation;

                                var reservation = item.GetPropertyValue<string>("ReservationNumber");
                                var room = item.GetPropertyValue<string>("Room");
                                var guest = item.GetPropertyValue<string>("Guests");
                                var pension = item.GetPropertyValue<string>("Pension");
                                
                                accountDescription.Append($"{reservation} | {pension}\n{room}\n{guest}");

                                details = new StringBuilder();
                                details.AppendLine($"{"Reservation".Translate()}: {reservation}");
                                details.AppendLine($"{"Room".Translate()}: {room}");
                                details.Append($"{"Titular".Translate()}: {guest}");

                                isWideDetails = true;

                                Ticket.Room = room;
                                Ticket.FiscalDocTo = guest;
                                break;
                            case CurrentAccountType.Entities:
                                accountType = CurrentAccountType.Entities;
                                accountDescription.Append(item.GetPropertyValue<string>("CompanyName"));
                                details = details.Append(item.GetPropertyValue<string>("Description"));
                                break;
                            case CurrentAccountType.Control:
                                accountType = CurrentAccountType.Control;
                                accountDescription.Append(item.GetPropertyValue<string>(nameof(PmsControlAccountRecord.AccountName)));
                                details = details.Append(item.GetPropertyValue<string>(nameof(PmsControlAccountRecord.AccountName)));
                                break;
                            case CurrentAccountType.Client:
                                accountType = CurrentAccountType.Client;
                                accountDescription.Append(item.GetPropertyValue<string>("Name"));
                                accountId = item.GetPropertyValue<Guid>("CurrentAccountId");
                                details = details.Append(item.GetPropertyValue<string>("Name"));
                                break;
                            case CurrentAccountType.Guests:
                                accountType = CurrentAccountType.Guests;
                                accountDescription.Append(item.GetPropertyValue<string>("GuestName"));
                                details = details.Append($"{item.GetPropertyValue<string>("GuestName")} ({item.GetPropertyValue<string>("Reserva")})");
                                break;
                            case CurrentAccountType.Groups:
                                accountType = CurrentAccountType.Groups;
                                accountDescription.Append(item.GetPropertyValue<string>("Name"));
                                details = details.Append(item.GetPropertyValue<string>("Description"));
                                break;
                        }

                        if (isLock)
                        {
                            WindowManager.ShowError(InvalidDataLbl, ErrorLbl);
                            return false;
                        }

                        Ticket.AccountType = accountType;
                        Ticket.AccountDescription = accountDescription.ToString();

                        var pay = Payment.CreateCredit(Ticket, accountDescription.ToString(), details.ToString(), _creditPanel.Value,
                            Stand.CurrencyId, accountId, _creditPanel.CreditType, isWideDetails);

                        Ticket.AddPayment(pay);

                        IsPaymentTicketAllowed = true;
                        IsPaymentInvoiceAllowed = false;
                        return true;
                    });
                }
                else
                    WindowManager.ShowError(!isValid ? UserNotAuthorizedLbl : ErrorLbl);
            });
        }

        private void AddControlAccountDeposit_Click(object sender, RoutedEventArgs e)
        {
            AppSecurity.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.EnableRoomCharge, (isValid, _) =>
            {
                #region Validations

                if (Ticket is not null && (Ticket.HasMealPlanPayment || Ticket.HasHouseUsePayment))
                {
                    var msg = "HaveAnotherPaymentResetPayments".Translate();
                    var pay = "";
                        
                    if (Ticket.HasMealPlanPayment)
                        pay = MealPlanLbl;
                    else if (Ticket.HasHouseUsePayment)
                        pay = HouseUseLbl;
                        
                    WindowManager.ShowError(string.Format(msg, pay), ErrorLbl);
                    return;
                }
                    
                #endregion
                
                if (isValid && EditorControl.Instance is not null && Ticket is not null)
                {
                    _depositPanel.Init(Ticket);
                    
                    EditorControl.Instance.ShowRequest("Debit".Translate(), _depositPanel, Colors.Black, () =>
                    {
                        
                        var item = (PmsControlAccountRecord)_depositPanel.Grid.SelectedValue;

                        if (_depositPanel.Value < decimal.Zero)
                        {
                            WindowManager.ShowError("InvalidAmountMoney".Translate(), ErrorLbl);
                            return false;
                        }

                        if (_depositPanel.Value < item.AccountDepositList.Where(x => x.IsSelected).Sum(x => x.Balance))
                        {
                            WindowManager.ShowError("It is mandatory consume the entire amount of money from the selected deposits", ErrorLbl);
                            return false;
                        }

                        var depositList = item.AccountDepositList.Where(x => x.IsSelected).ToList();
                        if (depositList.Count == 0)
                        {
                            WindowManager.ShowError("You must select at least a deposit from the control account", ErrorLbl);
                            return false;
                        }

                        var details = new StringBuilder();
                        var installationAccountId = _depositPanel.InstallationId;
                        var installationDescription = _depositPanel.InstallationDescription;

                        var accountId = item.GetPropertyValue<Guid>("AccountId");
                        Ticket.Account = accountId;
                        Ticket.AccountInstallationId = installationAccountId;

                        var accountDescription = new StringBuilder();
                        if (installationAccountId.HasValue)
                            accountDescription.AppendLine(installationDescription);

                        accountDescription.Append(item.GetPropertyValue<string>(nameof(PmsControlAccountRecord.AccountName)));
                        details = details.Append(item.GetPropertyValue<string>(nameof(PmsControlAccountRecord.AccountName)));

                        Ticket.AccountType = CurrentAccountType.Control;
                        Ticket.AccountDescription = accountDescription.ToString();

                        foreach (var deposit in item.AccountDepositList.Where(x => x.IsSelected))
                        {
                            var pay = Payment.CreateControlAccountDeposit(Ticket, deposit, Stand.CurrencyId, details.ToString());
                            Ticket.AddPayment(pay);
                        }

                        if (!Ticket.HasCashPayment)
                        {
                            IsPaymentTicketAllowed = true;
                            IsPaymentInvoiceAllowed = true;
                        }
                        return true;
                    });
                }
                else
                    WindowManager.ShowError(!isValid ? UserNotAuthorizedLbl : ErrorLbl);
            });
        }

        /// <summary>
        /// On load checks if the hotel has the permission to see the invoice button 
        /// </summary>
        /// <param name="sender">The button that is being sent this info</param>
        /// <param name="e">The args</param>
        private void Invoice_Loaded(object sender, RoutedEventArgs e)
        {
            var el = (UIElement)sender;
            if (Hotel.Instance.HasInvoicePrintSettings && Hotel.Instance.ShowInvoiceButton)
            {
                //docGrid.ColumnDefinitions[0].Width = new GridLength(1, GridUnitType.Star);
                el.Visibility = Visibility.Visible;
            }
            /*
            else 
                docGrid.ColumnDefinitions[0].Width = GridLength.Auto;
            */
        }
        
        /// <summary>
        /// Event handler for the click event of the invoice button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Invoice_Click(object sender, RoutedEventArgs e)
        {
            if (Ticket is not null && Ticket.DebtAmount > 0) 
                WindowManager.ShowError(PaymentIncompleteLbl, ErrorLbl);
            
            else
            {
                if (Hotel.Instance.AllowDigitalSign)
                {
                    var signQuestionTextBlock = new TextBlock
                    {
                        Text = "SignQuestion".Translate(),
                        FontSize = 14
                    };
                    WindowManager.OpenDialog(signQuestionTextBlock,  () =>
                    {
                        _buttonClicked = ButtonClicked.InvoiceButton;
                        if(Ticket is not null) UIThread.Invoke(async () => await SignTicket(Ticket.Id));
                        return false;
                    });
                }
                else 
                    InvokeButtonClicked(ButtonClicked.InvoiceButton);
            }
        }
        

        /// <summary>
        /// Event handler for the click event of the invoice button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FastInvoice_Click(object sender, RoutedEventArgs e)
        {
            if (Ticket is not null && Ticket.DebtAmount > 0)
                WindowManager.ShowError(PaymentIncompleteLbl, ErrorLbl);

            else
            {
                if (Hotel.Instance.AllowDigitalSign)
                {
                    var signQuestionTextBlock = new TextBlock
                    {
                        Text = "SignQuestion".Translate(),
                        FontSize = 14
                    };
                    WindowManager.OpenDialog(signQuestionTextBlock, () =>
                    {
                        _buttonClicked = ButtonClicked.FastInvoiceButton;
                        if (Ticket is not null) UIThread.Invoke(async () => await SignTicket(Ticket.Id));
                        return false;
                    });
                }
                else
                    InvokeButtonClicked(ButtonClicked.FastInvoiceButton);
            }
        }


        /// <summary>
        /// Checks if the user has permission to see the ticket button onload
        /// </summary>
        /// <param name="sender">The button that was clicked</param>
        /// <param name="e">The args</param>
        private void Ticket_Loaded(object sender, RoutedEventArgs e)
        {
            var el = (UIElement)sender;
            if (Hotel.Instance.HasTicketPrintSettings)
            {
                //docGrid.ColumnDefinitions[1].Width = new GridLength(1, GridUnitType.Star);
                el.Visibility = Visibility.Visible;
            }
            /*
            else 
                docGrid.ColumnDefinitions[1].Width = GridLength.Auto;
            */
        }

        /// <summary>
        /// Event handler for the click event of the ticket button
        /// </summary>
        /// <param name="sender">The button who is sending the info</param>
        /// <param name="e">The args</param>
        private void Ticket_Click(object sender, RoutedEventArgs? e)
        {
            if (Ticket?.DebtAmount > 0)
            {
                if (Ticket.DebtAmount < (decimal)0.005)
                {
                    if (Ticket.Payments != null) Ticket.Payments.First().BaseValue += Ticket.DebtAmount;
                    Ticket.RecalculateAll();
                }
                else
                {
                    WindowManager.ShowError(PaymentIncompleteLbl, ErrorLbl);
                    return;
                }
            }
            if (Hotel.Instance.AllowDigitalSign)
            {
                var signQuestionTextBlock = new TextBlock
                {
                    Text = "SignQuestion".Translate(),
                    FontSize = 14
                };
                WindowManager.OpenDialog(signQuestionTextBlock,  () =>
                {
                    _buttonClicked = ButtonClicked.TicketButton;
                    if (Ticket is not null) 
                        UIThread.Invoke(async () => await SignTicket(Ticket.Id));
                    return false;
                });
            } 
            else 
                InvokeButtonClicked(ButtonClicked.TicketButton);
        }
        
        
        /// Invokes the event handler for the clicked button
        /// </summary>
        /// <param name="buttonClicked">The button that was clicked</param>
        /// <exception cref="ArgumentOutOfRangeException">If the enum passed does not exist it throws this exception</exception>
        private void InvokeButtonClicked(ButtonClicked buttonClicked)
        {
            VerifyExternalPayments(buttonClicked).ContinueWith(task =>
            {
                var result = task.Result;
				if (result is VerifyExternalPaymentsResult.PaymentProcessing or VerifyExternalPaymentsResult.Failure)
					return;
				switch (buttonClicked)
				{
					case ButtonClicked.InvoiceButton:
                        UIThread.Invoke(()=> InvoiceClick?.Invoke(this, null, () => OkClick.Invoke(this)));
						//InvoiceClick?.Invoke(this, null, () => OkClick.Invoke(this));
						break;
					case ButtonClicked.FastInvoiceButton:
                        UIThread.Invoke(()=> FastInvoiceClick?.Invoke(this, null, () => OkClick.Invoke(this)));
						//FastInvoiceClick?.Invoke(this, null, () => OkClick.Invoke(this));
						break;
					case ButtonClicked.TicketButton:
						UIThread.Invoke(() => TicketClick?.Invoke(this, null, () => OkClick.Invoke(this)));
						//TicketClick?.Invoke(this, null, () => OkClick.Invoke(this));
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(buttonClicked), buttonClicked, null);
				}
			});

            //switch (buttonClicked)
            //{
            //    case ButtonClicked.InvoiceButton:
            //        InvoiceClick?.Invoke(this, null, () => OkClick.Invoke(this));
            //        break;
            //    case ButtonClicked.FastInvoiceButton:
            //        FastInvoiceClick?.Invoke(this, null, () => OkClick.Invoke(this));
            //        break;
            //    case ButtonClicked.TicketButton:
            //        TicketClick?.Invoke(this, null, () => OkClick.Invoke(this));
            //        break;
            //    default:
            //        throw new ArgumentOutOfRangeException(nameof(buttonClicked), buttonClicked, null);
            //}
        }

        enum VerifyExternalPaymentsResult
		{
			PaymentProcessing,
			NoActionNeeded,
			Failure,
		}

		private async Task<VerifyExternalPaymentsResult> VerifyExternalPayments(ButtonClicked continueWith)
        {
            var affected = Ticket.Payments.Where(x => x.Type != null && x.Type.HasPaySystemOriginId).ToArray();

            if (!affected.Any())
                return VerifyExternalPaymentsResult.NoActionNeeded;

            var paymentsResult = await BusinessProxyRepository.ApiClient.GetTicketExternalPaymentsAsync(Ticket.Id);

            if (!paymentsResult.IsSuccess())
                return VerifyExternalPaymentsResult.Failure;

            var payments = paymentsResult.Result.Entries.ToDictionary(x => x.Id);

            foreach (var payment in affected)
			{
                if (!payments.ContainsKey(payment.Id))
				{
                    ShowPaySystemPanel(
                        payment.Type,
                        payment,
						continueWith,
						(state, isSuccess) =>
                        {
                            if (isSuccess)
								InvokeButtonClicked((ButtonClicked)state);
                            return true;
                        });
                    return VerifyExternalPaymentsResult.PaymentProcessing;
				}
			}

            return VerifyExternalPaymentsResult.NoActionNeeded; 
		}


        /// <summary>
        /// Event handler for the click event of the pending button
        /// </summary>
        /// <param name="sender">The button that was clicked</param>
        /// <param name="e">The args</param>
        private void Pending_Click(object sender, RoutedEventArgs? e)
        {
            if (Ticket == null) return;
            WindowManager.ShowBusyDialog();
            var ticketId = Ticket.Id;
            GetSignatureFromClient(ticketId);
            WindowManager.CloseBusyDialog();
        }
        
        /// <summary>
        /// Toggles the visibility of the redo and refresh buttons
        /// </summary>
        /// <param name="redoBtnVisibility">The visibility of the redo button</param>
        /// <param name="refreshBtnVisibility">The visibility of the refresh button</param>
        private void ToggleBtnVisibility(Visibility redoBtnVisibility, Visibility refreshBtnVisibility)
        {
            var pendingGrid = (Grid)TryFindResource("PendingGrid");
            var refreshBtn = FindChild<ActionButton>(pendingGrid, "RefreshBtn");
            var redoBtn = FindChild<ActionButton>(pendingGrid, "RedoBtn");
            if (refreshBtn is null || redoBtn is null) return;
            refreshBtn.Visibility = refreshBtnVisibility;
            redoBtn.Visibility = redoBtnVisibility;
        }
        
        /// <summary>
        /// Event handler for the click event of the undo signature button
        /// </summary>
        /// <param name="sender">The button that was clicked</param>
        /// <param name="e">The args</param>
        private async void Redo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(Ticket == null) return;
                var ticketId = Ticket.Id;
                var result = await BusinessProxyRepository.Ticket.UpdateSignatureAsync(ticketId.ToString(), new SignatureRecord { TicketId = ticketId, IsSigned = false, Signature = null });
                if (!result.IsEmpty) return;
                Ticket.DigitalSignature = null;
                ToggleBtnVisibility(Visibility.Collapsed, Visibility.Visible);
            }
            catch (Exception exception)
            {
                WindowManager.ShowError(exception);
            }
        }
        
        /// <summary>
        /// Sign the ticket with the digital signature that comes from the android
        /// </summary>
        /// <returns>If the operation was a success returns true, otherwise returns false</returns>
        private async Task<bool> SignTicket(Guid ticketId)
        {
            try
            {
                var result  = await BusinessProxyRepository.Ticket.AddSignatureAsync(new SignatureRecord { TicketId = ticketId });
                if (result.IsEmpty)
                {
                    var pendingGrid = (Grid)TryFindResource("PendingGrid");
                    pendingGrid.DataContext = Ticket;
                    WindowManager.OpenDialog(pendingGrid, OnAcceptPending, DialogButtons.OkCancel, OnCancelPending);
                }
            }
            catch (Exception e)
            {
                WindowManager.ShowError(e);
                Console.WriteLine(e);
                throw;
            }
            
            return false;
        }

        /// <summary>
        /// Finds a child element in the visual tree of the parent recursively
        /// </summary>
        /// <param name="parent">The parent object</param>
        /// <param name="childName">The name of the child to be searched for</param>
        /// <typeparam name="T">The type of the object to found</typeparam>
        /// <returns></returns>
        private static T? FindChild<T>(DependencyObject? parent, string childName)
            where T : DependencyObject
        {
            // Confirm parent and childName are valid. 
            if (parent == null) return null;

            T? foundChild = null;

            var childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (var i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child
                if (child is not T childType)
                {
                    // recursively drill down the tree
                    foundChild = FindChild<T>(child, childName);

                    // If the child is found, break so we do not overwrite the found child. 
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    // If the child's name is set for search
                    if (childType is not FrameworkElement frameworkElement || frameworkElement.Name != childName) continue;
                    // if the child's name is of the request name
                    foundChild = childType;
                    break;
                }
                else
                {
                    // child element found.
                    foundChild = childType;
                    break;
                }
            }
            return foundChild;
        }
        
        /// <summary>
        /// Checks if the signature is already signed
        /// If it is signed, then it will show the redo button and hide the refresh button
        /// </summary>
        /// <param name="ticketId">The id of the ticket to be searched for</param>
        private async void GetSignatureFromClient(Guid ticketId)
        {
            try
            {
                var newSignature = await BusinessProxyRepository.Ticket.GetSignatureAsync(ticketId.ToString());
                if (!newSignature.IsSigned || Ticket == null) return;
                Ticket.DigitalSignature = newSignature.Signature;
                ToggleBtnVisibility(Visibility.Visible, Visibility.Collapsed);
            }
            catch (Exception e)
            {
                WindowManager.ShowError(e.Message);
            }
        }

        /// <summary>
        /// Method to handle the click event of the accept button in the pending dialog
        /// </summary>
        private bool OnAcceptPending()
        {
            try
            {
                InvokeButtonClicked(_buttonClicked);
                ToggleBtnVisibility(Visibility.Collapsed, Visibility.Visible);
                if (Ticket == null) return false;
                var ticketId = Ticket.Id;
                BusinessProxyRepository.Ticket.DeleteSignatureAsync(ticketId.ToString());
                return false;
            }
            catch (Exception e)
            {
                WindowManager.ShowError(e.Message);
                return false;
            }
        }

        /// <summary>
        /// Cancel the operation of signing the ticket
        /// </summary>
        private void OnCancelPending()
        {
            if(Ticket == null) return;
            Ticket.DigitalSignature = null;
            ToggleBtnVisibility(Visibility.Collapsed, Visibility.Visible);
            //SocketIOClientWrapper.Instance.Unsuscribe("SignatureDone");
        }

        private void Ballot_Loaded(object sender, RoutedEventArgs e)
        {
            var el = (UIElement)sender;
            if (Hotel.Instance.HasBallotPrintSettings && Hotel.Instance.AllowBallotDocument)
            {

                //docGrid.ColumnDefinitions[2].Width = new GridLength(1, GridUnitType.Star);
                el.Visibility = Visibility.Visible;
            }
            /*
            else
                docGrid.ColumnDefinitions[2].Width = GridLength.Auto;
            */
        }

        private void Ballot_Click(object sender, RoutedEventArgs e)
        {
            if (Ticket.DebtAmount > 0)
                WindowManager.ShowError(PaymentIncompleteLbl, ErrorLbl);
            else
                BallotClick?.Invoke(this, null, () => OkClick?.Invoke(this));
        }

        /// <summary>
        /// Handler to the click event of the button to split the payment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SplitPayment_Click(object sender, RoutedEventArgs e)
        {
            if (Ticket is null) 
                return;

            if (Ticket.Paxs == 0)
            {
                var askNumberPeopleGrid = new Grid();
                askNumberPeopleGrid.RowDefinitions.Add(new RowDefinition());
                askNumberPeopleGrid.RowDefinitions.Add(new RowDefinition());
                    
                var peopleText = new TextBlock
                {
                    Text = "Paxs".Translate(),
                    FontSize = 14,
                    Foreground = Brushes.Black,
                    FontWeight = FontWeights.Bold,
                    Margin = new Thickness(0, 0, 0, 10)
                };
                peopleText.SetValue(Grid.RowProperty, 0);
                askNumberPeopleGrid.Children.Add(peopleText);
               
                var totalPersons = new IntEditControl
                {
                    MinValue = 1,
                    Increment = 1,
                    IsFloatValue = false,
                    Style = (Style)FindResource("IntEditControlStyle"),
                };

                totalPersons.SetValue(Grid.RowProperty, 1);
                askNumberPeopleGrid.Children.Add(totalPersons);
                
                WindowManager.OpenDialog(askNumberPeopleGrid, OkAction);

                bool OkAction()
                {
                    Ticket.Paxs = (short)totalPersons.Value;
                    UnPaid = Ticket.Paxs;
                    Paid = 0;
                    Paying = 0;
                    IsSplitPayment = true;
                    return true;
                }
            }
            else
            {
                UnPaid = Ticket.Paxs;
                Paid = 0;
                Paying = 0;
                IsSplitPayment = true;
            }
        }

        /// <summary>
        /// Handle the click event of the button to split the payment by process
        /// It starts the "wizard" to pay by parts of each individual
        /// </summary>
        /// <param name="sender">The object it's being sent</param>
        /// <param name="e">The args being passed</param>
        private void UnpaidButton_Click(object sender, RoutedEventArgs e)
        {
            // If there is everyone paid, and the button is clicked again, show an error message
            if (UnPaid <= 0)
            {
                WindowManager.ShowError("NoUnpaid".Translate());
                return;
            }

            paymentsActionButton.IsEnabled = true;
            PaymentTypesContainer.IsEnabled = true;
            UnPaid--;
            Paying++;
            if (Paid == 0) PayingAmount = LeftAmount = Paying * SplitAmount + _extraSplit;
            else PayingAmount = LeftAmount = Paying * SplitAmount;
        }

        /// <summary>
        /// Handle the click event of the button to pay the amount of money,
        /// toggles the payment window to allow the user to pay
        /// </summary>
        /// <param name="sender">The object it's being sent</param>
        /// <param name="e">The args being passed</param>
        private void PayingButton_Click(object sender, RoutedEventArgs e)
        {
            // If there is an amount left to pay, show an error message
            if (LeftAmount >= (decimal)0.01)
            {
                WindowManager.ShowError("LeftAmount".Translate());
                return;
            }

            paymentsActionButton.IsEnabled = false;
            PaymentTypesContainer.IsEnabled = false;
            Paid += Paying;
            LeftAmount = PayingAmount = Paying = 0;
            if (UnPaid == 0) BeginPaidEmphasisAnimation();
        }

        /// <summary>
        /// Handle the click event of the button that finishes the split payment process
        /// </summary>
        /// <param name="sender">The object it's being sent</param>
        /// <param name="e">The args being passed</param>
        private void PaidButton_Click(object sender, RoutedEventArgs e)
        {
            // If the payment is not complete, show an error message
            if (UnPaid != 0 || Paying != 0)
            {
                WindowManager.ShowError("PaymentIncompleted".Translate());
                return;
            }

            IsSplitPayment = false;
            IsSecondPage = false;
        }

        private void RemovePayment()
        {
            if (Ticket?.Payments == null) 
                return;
            if (Ticket.Payments.Count == 1)
            {
                Ticket.ClearPayments();
                return;
            }

            if (paymentsDataGrid.SelectedItem is not Payment selectedPayment)
                return;

            Ticket.RemovePayment(selectedPayment);
            if (!IsSplitPayment)
                return;
            
            LeftAmount += selectedPayment.ReceiveValue;
            
            if (LeftAmount >= 0) 
                return;
            
            if (selectedPayment.IsHouseUsePayment || selectedPayment.IsMealPlanPayment)
                Ticket.TipAmount += Math.Abs(LeftAmount);
            else
                Ticket.ChangeAmount += Math.Abs(LeftAmount);
            LeftAmount = 0;
        }

        private void RemoveSelectedPayment_TouchDown(object sender, RoutedEventArgs e)
        {
            RemovePayment();
        }

        private void RemoveSelectedPayment_MouseDown(object sender, RoutedEventArgs e)
        {
            RemovePayment();
        }

        private void OkBeginSplit_Click(object sender, RoutedEventArgs e)
        {
            IsSecondPage = true;
        }

        private void NotesButton_Click(object sender, RoutedEventArgs e)
        {
            if (EditorControl.Instance == null) return;
            ((TextBox)(_notesContent.Children[0] as Border)?.Child)!.Text = Ticket.Observations;

            EditorControl.Instance.ShowRequest(
                "Notes",
                _notesContent,
                Colors.Black,
                delegate
                {
                    Ticket.Observations = ((_notesContent.Children[0] as Border)?.Child as TextBox)?.Text;
                    return true;
                });
        }

        private void DiscountButton_Click(object sender, RoutedEventArgs e)
        {
            #region Validations

            if (Ticket.Payments.Any(p => p.IsHouseUsePayment || p.IsMealPlanPayment))
            {
                WindowManager.ShowError("NotMultDiscounts".Translate());
                return;
            }

            #endregion

            AppSecurity.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.TicketsDiscounts, (isValid, validUser) =>
            {
                if (isValid)
                {
                    if (EditorControl.Instance != null)
                    {
                        var discountsCtrl = new DiscountsPanel();
                        discountsCtrl.Prepare(Ticket);

                        EditorControl.Instance.IsCancelButtonVisible = false;

                        EditorControl.Instance.ShowRequest("Discounts".Translate(), discountsCtrl, Colors.Black,
                            () =>
                            {
                                EditorControl.Instance.IsCancelButtonVisible = true;
                                return discountsCtrl.Release();
                            });
                    }
                }
                else WindowManager.ShowError(UserNotAuthorizedLbl);
            });
        }

        private void TipsButton_Click(object sender, RoutedEventArgs e)
        {
            AppSecurity.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.TicketsTips, (isValid, validUser) =>
            {
                if (isValid)
                {
                    if (EditorControl.Instance != null)
                    {
                        var tipsPanel = new TipsPanel();
                        tipsPanel.PrepareContext(Ticket);

                        EditorControl.Instance.IsCancelButtonVisible = false;

                        bool AfterAction()
                        {
                            tipsPanel.ReleaseContext();
                            EditorControl.Instance.IsCancelButtonVisible = true;
                            return true;
                        }

                        EditorControl.Instance.ShowRequest("Tips".Translate(), tipsPanel, Colors.Black, AfterAction, AfterAction);
                    }
                }
                else WindowManager.ShowError(UserNotAuthorizedLbl);
            });
        }

        private void ChangeTaxSchema_Click(object sender, RoutedEventArgs e)
        {
            var picker = (PickerControl)Resources["TaxSchemaPicker"];
            picker.ItemsSource = Hotel.Instance.TaxSchemas;
            TaxSchema ticketTaxSchema = Hotel.Instance.TaxSchemas.First(x => x.Id == Ticket.TaxSchemaId);
            picker.SelectedItem = ticketTaxSchema;

            bool Accept()
            {
                var selectedTaxSchema = (TaxSchema)picker.SelectedItem;
                if (selectedTaxSchema != null && ticketTaxSchema != selectedTaxSchema)
                    SetTicketTaxSchema(selectedTaxSchema);
                return true;
            }

            EditorControl.Instance.ShowRequest("Schema".Translate(), picker, Colors.Black, Accept);
        }

        private void SetTicketTaxSchema(TaxSchema schema)
        {
            Ticket.ClearCollectData(false);
            Ticket.SetTaxSchema(schema.Id);
        }

        private void ShowEditorControl(string name, object content, Color background, Func<bool>? okAction, Func<bool>? cancelAction = null)
        {
            editorControl.EditorName = name;
            editorControl.EditorContent = content;
            editorControl.OkAction = okAction;
            editorControl.CancelAction = cancelAction;

            editorControl.AnimatedShow();

            // Inhabilitamos las acciones y el cerrar general
            ActionsContainer.IsEnabled = false;
            MainPage.CurrentInstance.DisableAllButtons();
            MainPage.CurrentInstance.ProhibitClose();
        }

        public void HideEditorControl()
        {
            editorControl.EditorName = null;
            editorControl.EditorContent = null;
            editorControl.OkAction = null;
            editorControl.CancelAction = null;

            editorControl.AnimatedHide();

            // Habilitamos las acciones y el cerrar general
            ActionsContainer.IsEnabled = true;
            MainPage.CurrentInstance.EnableAllButtons();
            MainPage.CurrentInstance.AllowClose();
        }

        /// <summary>
        /// Helper method to calculate the ticket's tips, change, left amount, etc.
        /// This also takes in account if the payment is split and which payment panel/method is being used
        /// </summary>
        /// <param name="p">The payment to be used</param>
        /// <param name="cashPanel">If it's a cashPanel or not (null)</param>
        /// <param name="creditCardPanel">If it's a cashPanel or not (null)</param>
        private void CalculateTicket(Payment payment, CashPanel? cashPanel = null, CreditCardPanel? creditCardPanel = null)
        {
            // If the payment is greater than the total amount, we add the difference to the total amount
            if (Ticket.PaymentTotalAmount >= Ticket.TotalAmount) 
                Ticket.PaymentTotalAmount += Math.Abs(Ticket.TotalAmount - payment.BaseValue);

            var excessTipAmount = 0M;
            var isExcessTipEnabled = cashPanel is { ExcessAsTip: true } || creditCardPanel is { ExcessAsTip: true };

            if (isExcessTipEnabled)
            {
                excessTipAmount = payment.BaseValue - Ticket.TotalAmount + Ticket.PaymentTotalAmount;
                /*
                if (excessTipAmount < 0 && Ticket.Payments is {Count: 0})
                {
                    WindowManager.ShowError("InvalidAmountMoney".Translate(), ErrorLbl);
                    return;
                }
                */
            }
          
            if (IsSplitPayment)
            {
                LeftAmount -= payment.ReceiveValue;
                if (LeftAmount >= 0)
                {
                    Ticket.AddPayment(payment, excessTipAmount);
                }
                else 
                {
                    if (cashPanel is { ExcessAsTip: true } || creditCardPanel is { ExcessAsTip: true })
                        Ticket.TipAmount += Math.Abs(LeftAmount);
                    else
                        Ticket.ChangeAmount += Math.Abs(LeftAmount);
                    LeftAmount = 0;
                    Ticket.AddPayment(payment, excessTipAmount);
                }
            }
            else
            {
                Ticket.AddPayment(payment, excessTipAmount);
                var paidDiff = Ticket.TotalAmount - Ticket.PaymentTotalAmount;
                switch (paidDiff)
                {
                    case >= 0:
                        return;
                    case < 0 when isExcessTipEnabled:
                        Ticket.TipAmount += Math.Abs(paidDiff);
                        return;
                    default:
                        Ticket.ChangeAmount = Math.Abs(paidDiff);
                        return;
                }
            }
        }

        private void SetAllowedForPayment(Payment payment)
        {
            if (payment == null)
            {
                return;
            }
            IsPaymentInvoiceAllowed = payment.AllowInvoiceClick;
            IsPaymentTicketAllowed = payment.AllowTicketClick;
        }

		/// <summary>
		/// Shows and builds the cash panel
		/// The local method is fired when the accept button is clicked
		/// </summary>
		private void ShowCashPanel()
        {
            _cashPanel.Initialize(Ticket, IsSplitPayment ? LeftAmount : null);
            _cashPanel.HorizontalAlignment = HorizontalAlignment.Center;
            ShowEditorControl("Cash".Translate(), _cashPanel, Colors.Black,
                () =>
                {
                    if (_cashPanel.Value > 0)
                    {
                        var pay = _cashPanel.GetPayment(p =>
                        {
                            if (p == null) return;
                            lock (Ticket) CalculateTicket(p, _cashPanel);
                        });
                        SetAllowedForPayment(pay);
                        return pay != null;
                    }

                    WindowManager.ShowError(InvalidDataLbl, ErrorLbl);
                    return false;
                });
        }
        
        /// <summary>
        /// Shows and builds the credit card panel
        /// The local method is fired when the accept button is clicked
        /// </summary>
        private void ShowCardPanel()
        {
            _creditCardPanel.Initialize(Ticket, IsSplitPayment ? LeftAmount : null);
            ShowEditorControl("CreditCard".Translate(), _creditCardPanel, Colors.Black,
                () =>
                {
                    if (_creditCardPanel.IsValidData())
                    {
                        var pay = _creditCardPanel.GetPayment(p =>
                        {
                            if (p == null) return;
                            lock (Ticket)
                            {
                                CalculateTicket(p, null, _creditCardPanel);

                                if (!_creditCardFastPaymentActive) return;
                                _creditCardFastPaymentActive = false;
                            }
                        }, Ticket);
                        SetAllowedForPayment(pay);
                        return pay != null;
                    }

                    WindowManager.ShowError(InvalidDataLbl, ErrorLbl);
                    return false;
                });
        }

		private void ShowPaySystemPanel(PaymentMethod payType, Payment payment, object state, Func<object, bool, bool> afterFinish)
		{
			//Ticket.UserDescription
			ExternalPaymentPanel externalPaymentPanel = new()
			{
				DataContext = new ExternalPayment
				{
					Description = "Payments",
					Amount = payment?.ReceiveValue ?? Ticket.TryPayment(),
					Currency = Ticket.Unmo,
					User = Hotel.Instance.CurrentUserModel.Username,
                    PaymentMethod = payType,
                    Ticket = Ticket
				}
			};

            externalPaymentPanel.DataContext.Initialize(
                BusinessProxyRepository.ApiClient,
				ExternalPaymentsSupport.GetTerminalSystems(),
				payType.PaySystemOriginId.Value,
                () => UIThread.Invoke(EditorControl_AcceptChanges, editorControl));

			ShowEditorControl("External Payment".Translate(), externalPaymentPanel, Colors.Black,
				() =>
				{
					if (externalPaymentPanel.DataContext.IsSuccess)
					{
						CreditCardType ccType = Hotel.Instance.CreditCardTypes.FirstOrDefault();
						if (ccType == null)
							return afterFinish(state, false);
						var pay = payment ?? Payment.Create(Ticket, payType, externalPaymentPanel.DataContext.Amount, externalPaymentPanel.DataContext.ReceivedAmount);
						externalPaymentPanel.DataContext.CompletePayment(
							pay,
							(descr, typeDesc, authCode, trans) => new CreditCard(descr, ccType.Id, typeDesc, 0, 0, "PIN", trans, authCode));
                        if (payment is null)
                        {
							CalculateTicket(pay);
							SetAllowedForPayment(pay);
						}
						return afterFinish(state, true);
					}

					if (externalPaymentPanel.DataContext.IsDeviceSuccess)
					{
						CreditCardType ccType = Hotel.Instance.CreditCardTypes.FirstOrDefault();
						if (ccType == null)
							return afterFinish(state, false);
						var pay = payment ?? Payment.Create(Ticket, payType, externalPaymentPanel.DataContext.Amount, externalPaymentPanel.DataContext.ReceivedAmount);
						externalPaymentPanel.DataContext.CompleteDevicePayment(
							pay,
							(descr, typeDesc, authCode, trans) => new CreditCard(descr, ccType.Id, typeDesc, 0, 0, "PIN", trans, authCode));
						if (payment is null)
						{
							CalculateTicket(pay);
							SetAllowedForPayment(pay);
						}
						return afterFinish(state, true);
					}

					//if (true) //(_creditCardPanel.IsValidData())
					//{
					//	var pay = _creditCardPanel.GetPayment(p =>
					//	{
					//		if (p == null) return;
					//		lock (Ticket)
					//		{
					//			CalculateTicket(p, null, _creditCardPanel);

					//			if (!_creditCardFastPaymentActive) return;
					//			_creditCardFastPaymentActive = false;
					//		}
					//	}, Ticket);
					//	IsPaymentInvoiceAllowed = pay.AllowInvoiceClick;
					//	IsPaymentTicketAllowed = pay.AllowTicketClick;
					//	return pay != null;
					//}

					//WindowManager.ShowError(InvalidDataLbl, ErrorLbl);
					return afterFinish(state, false);
				});
		}

        private void BeginFromSplitModeAnimation()
        {
            ((Storyboard)Resources["fromSplitMode"]).Begin();
        }

        private void StopPaidEmphasisAnimation()
        {
            ((Storyboard)Resources["paidEmphasis"]).Stop();
        }

        private void BeginPaidEmphasisAnimation()
        {
            ((Storyboard)Resources["paidEmphasis"]).Begin();
        }

        private void BeginToSplitModeAnimation()
        {
            ((Storyboard)Resources["toSplitMode"]).Begin();
        }

        private void BeginToFirstSplitPageAnimation()
        {
            ((Storyboard)Resources["toFirstSplitPage"]).Begin();
        }

        private void BeginToSecondSplitPageAnimation()
        {
            ((Storyboard)Resources["toSecondSplitPage"]).Begin();
        }

        #endregion

        #endregion
    }
}