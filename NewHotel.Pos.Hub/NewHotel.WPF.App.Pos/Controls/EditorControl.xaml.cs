﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for EditorControl.xaml
    /// </summary>
    public partial class EditorControl
    {

		public event Action<EditorControl>? AcceptChanges;
        public event Action<EditorControl>? CancelChanges;
        public event EditorControlShowRequestHandler? ShowRequested;

        internal static EditorControl? Instance;

        public EditorControl()
        {
            InitializeComponent();
            IsCancelButtonVisible = true;
            Instance ??= this;
        }

        public bool ShowRequest(string name, object content, Color color, Func<bool>? okAction, Func<bool>? cancelAction = null, bool isCancelButtonVisible = true)
        {
            if (ShowRequested == null) return false;
            ShowRequested(this, new EditorControlShowRequestArgs(name, content, color, okAction, cancelAction, isCancelButtonVisible));
            return true;
        }

        public void AnimatedShow()
        {
            (Resources["show"] as Storyboard)?.Begin();
        }

        public void AnimatedHide()
        {
            (Resources["hide"] as Storyboard)?.Begin();
        }

        public Func<bool>? OkAction { get; set; }
        public Func<bool>? CancelAction { get; set; }

        public Color Color
        {
            get => (Color)GetValue(ColorProperty);
            set => SetValue(ColorProperty, value);
        }
        public static readonly DependencyProperty ColorProperty =
            DependencyProperty.Register(nameof(Color), typeof(Color), typeof(EditorControl));



        public Color FontColor
        {
            get => (Color)GetValue(FontColorProperty);
            set => SetValue(FontColorProperty, value);
        }
        public static readonly DependencyProperty FontColorProperty =
            DependencyProperty.Register(nameof(FontColor), typeof(Color), typeof(EditorControl));


        
        public object EditorContent
        {
            get => GetValue(EditorContentProperty);
            set => SetValue(EditorContentProperty, value);
        }
        public static readonly DependencyProperty EditorContentProperty =
            DependencyProperty.Register(nameof(EditorContent), typeof(object), typeof(EditorControl));


        public string EditorName
        {
            get => (string)GetValue(EditorNameProperty);
            set => SetValue(EditorNameProperty, value);
        }
        public static readonly DependencyProperty EditorNameProperty =
            DependencyProperty.Register(nameof(EditorName), typeof(string), typeof(EditorControl));


        private void ok_Click(object sender, RoutedEventArgs e)
        {
            AcceptChanges?.Invoke(this);
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            CancelChanges?.Invoke(this);
        }

        public bool IsCancelButtonVisible
        {
            get => (bool)GetValue(HasCancelButtonProperty);
            set => SetValue(HasCancelButtonProperty, value);
        }

        public static readonly DependencyProperty HasCancelButtonProperty =
            DependencyProperty.Register(nameof(IsCancelButtonVisible), typeof(bool), typeof(EditorControl));
    }

    public delegate void EditorControlShowRequestHandler(EditorControl sender, EditorControlShowRequestArgs e);

    public class EditorControlShowRequestArgs
    {
        public EditorControlShowRequestArgs(string name, object content, Color background, Func<bool>? okAction, Func<bool>? cancelAction = null, bool isCancelButtonVisible = true)
        {
            Content = content;
            Background = background;
            OkAction = okAction;
            CancelAction = cancelAction;
            Name = name;
            IsCancelButtonVisible = isCancelButtonVisible;
        }

        public string Name { get; set; }
        public object Content { get; set; }
        public Color Background { get; set; }
        public Func<bool>? OkAction { get; set; }
        public Func<bool>? CancelAction { get; set; }
        public bool IsCancelButtonVisible { get; set; }
    }
}
