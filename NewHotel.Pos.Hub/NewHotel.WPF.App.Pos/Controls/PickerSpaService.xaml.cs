﻿using System.Collections.Generic;
using NewHotel.Contracts.Pos.Records;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for PickerSpaService.xaml
    /// </summary>
    public partial class PickerSpaService
    {
        public PickerSpaService(List<SpaSearchReservationProductRecord> records)
        {
            InitializeComponent();
            LoadData(records);
        }

        private void LoadData(List<SpaSearchReservationProductRecord> records)
        {
            DataGrid.ItemsSource = records;
        }
    }
}
