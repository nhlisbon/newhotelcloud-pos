﻿using NewHotel.WPF.App.Pos.Model;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Animation;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls
{
    //F111102:ped053:01:Control para el boton correspondiente a una mesa
    /// <summary>
    /// Interaction logic for TableButton.xaml
    /// </summary>
    public partial class LiteTableButton : UserControl, INotifyPropertyChanged
    {
        private Table _table;

        public LiteTableButton()
        {
            InitializeComponent();
        }

        public LiteTableButton(Table table)
        {
            InitializeComponent();

            PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName == nameof(Table))
                    DataContext = Table;
            };

            Table = table;

            BindingBase binding = new Binding(nameof(IsSelected))
            {
                Mode = BindingMode.TwoWay,
                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                Source = Table,
            };
            SetBinding(IsSelectedProperty, binding);
        }

        public event Action<LiteTableButton, bool> IsSelectedChanged;

        public event Func<Table, bool> TablesGroupContainsMeRequested;


        public Table Table
        {
            get { return _table; }
            set
            {
                _table = value;
                OnPropertyChanged(nameof(Table));
            }
        }

        public bool IsHighlight
        {
            get { return (bool)GetValue(IsHighlightProperty); }
            set
            {
                if (IsHighlight != value)
                {
                    SetValue(IsHighlightProperty, value);
                    if (value)
                    {
                        if (!IsSelected)
                            (Resources["toHighlight"] as Storyboard).Begin();
                    }
                    else
                    {
                        if (IsSelected)
                            (Resources["toSelected"] as Storyboard).Begin();
                        else
                            (Resources["toNormal"] as Storyboard).Begin();
                    }
                }
            }
        }

        public static readonly DependencyProperty IsHighlightProperty =
            DependencyProperty.Register("IsHighlight", typeof(bool), typeof(LiteTableButton));

        #region IsSelected 

        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(LiteTableButton), new PropertyMetadata(IsSelectedPropertyChangedCallback));


        private static void IsSelectedPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = (LiteTableButton)d;
            if (obj.IsSelected)
                ((Storyboard)obj.Resources["toSelected"]).Begin();
            else
            {
                if (obj.IsHighlight)
                    ((Storyboard)obj.Resources["toHighlight"]).Begin();
                else ((Storyboard)obj.Resources["toNormal"]).Begin();
            }
        }

        #endregion

        private void userContorl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            IsSelected = !IsSelected;
            if (IsSelectedChanged != null)
                IsSelectedChanged(this, IsSelected);
        }

        private void userControl_MouseEnter(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                bool change = true;
                if (TablesGroupContainsMeRequested != null)
                    change = !TablesGroupContainsMeRequested(DataContext as Table);
                if (change)
                {
                    IsSelected = !IsSelected;
                    if (IsSelectedChanged != null)
                        IsSelectedChanged(this, IsSelected);
                }
            }
        }

        private void userControl_TouchDown(object sender, TouchEventArgs e)
        {
            IsSelected = !IsSelected;
            if (IsSelectedChanged != null)
                IsSelectedChanged(this, IsSelected);
        }

        private void userControl_TouchEnter(object sender, TouchEventArgs e)
        {
            bool change = true;
            if (TablesGroupContainsMeRequested != null)
                change = !TablesGroupContainsMeRequested(DataContext as Table);
            if (change)
            {
                IsSelected = !IsSelected;
                if (IsSelectedChanged != null)
                    IsSelectedChanged(this, IsSelected);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
