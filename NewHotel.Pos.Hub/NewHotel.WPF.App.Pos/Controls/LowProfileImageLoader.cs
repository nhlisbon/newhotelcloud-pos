﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using NewHotel.Contracts;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Hub.Model;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common.Converters;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Provides access to the Image.UriSource attached property which allows
    /// Images to be loaded with less impact to the UI thread.
    /// </summary>
    public static class LowProfileImageLoader
    {
        private const int WorkItemQuantum = 5;
        private static readonly Thread _thread = new Thread(WorkerThreadProc);
        private static readonly ConcurrentQueue<PendingRequest> _pendingRequests = new ConcurrentQueue<PendingRequest>();
        private static readonly ConcurrentQueue<Task<ValidationItemResult<ImageRecord>>> _pendingResponses = new ConcurrentQueue<Task<ValidationItemResult<ImageRecord>>>();
        private static readonly object _syncBlock = new object();
        private static bool _exiting;

        /// <summary>
        /// Identifies the UriSource attached DependencyProperty.
        /// </summary>
        public static readonly DependencyProperty UriSourceProperty = DependencyProperty.RegisterAttached("UriSource", typeof(Uri), typeof(LowProfileImageLoader), new PropertyMetadata(OnUriSourceChanged));

        /// <summary>
        /// Gets the value of the Uri to use for providing the contents of the ImageBrush's Source property.
        /// </summary>
        /// <param name="obj">ImageBrush needing its ImageSource property set.</param>
        /// <returns>Uri to use for providing the contents of the Source property.</returns>
        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "UriSource is applicable only to Image and ImageBrush elements.")]
        public static Uri GetUriSource(DependencyObject obj)
        {
            if (null == obj)
            {
                throw new ArgumentNullException("obj");
            }
            return (Uri)obj.GetValue(UriSourceProperty);
        }

        /// <summary>
        /// Sets the value of the Uri to use for providing the contents of the ImageBrush's Source property.
        /// </summary>
        /// <param name="obj">ImageBrush needing its ImageSource property set.</param>
        /// <param name="value">Uri to use for providing the contents of the Source property.</param>
        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "UriSource is applicable only to Image and ImageBrush elements.")]
        public static void SetUriSource(DependencyObject obj, Uri value)
        {
            if (null == obj)
            {
                throw new ArgumentNullException("obj");
            }
            obj.SetValue(UriSourceProperty, value);
        }

        /// <summary>
        /// Gets or sets a value indicating whether low-profile image loading is enabled.
        /// </summary>
        public static bool IsEnabled { get; set; }

        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Static constructor performs additional tasks.")]
        static LowProfileImageLoader()
        {
            // Start worker thread
            _thread.Start();
            Application.Current.Exit += HandleApplicationExit;
            IsEnabled = true;
        }

        private static void HandleApplicationExit(object sender, EventArgs e)
        {
            // Tell worker thread to exit
            _exiting = true;
            if (Monitor.TryEnter(_syncBlock, 100))
            {
                Monitor.Pulse(_syncBlock);
                Monitor.Exit(_syncBlock);
            }
        }

        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "Relevant exceptions don't have a common base class.")]
        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "Linear flow is easy to understand.")]
        private static void WorkerThreadProc(object unused)
        {
            var rand = new Random();
            var pendingRequests = new List<PendingRequest>();
            var pendingResponses = new Queue<Task<ValidationItemResult<ImageRecord>>>();

            while (!_exiting)
            {
                lock (_syncBlock)
                {
                    // Wait for more work if there's nothing left to do
                    if ((_pendingRequests.Count == 0) && (_pendingResponses.Count == 0) &&
                        (pendingRequests.Count == 0) && (pendingResponses.Count == 0))
                    {
                        Monitor.Wait(_syncBlock);
                        if (_exiting) return;
                    }
                    // Copy work items to private collections
                    while (0 < _pendingRequests.Count)
                    {
                        PendingRequest pendingRequest;
                        if (!_pendingRequests.TryDequeue(out pendingRequest))
                            continue;

                        // Search for another pending request for the same Image element
                        for (var i = 0; i < pendingRequests.Count; i++)
                        {
                            if (pendingRequests[i].Target == pendingRequest.Target)
                            {
                                // Found one; replace it
                                pendingRequests[i] = pendingRequest;
                                pendingRequest = null;
                                break;
                            }
                        }
                        if (null != pendingRequest)
                        {
                            // Unique request; add it
                            pendingRequests.Add(pendingRequest);
                        }
                    }
                    while (_pendingResponses.Count > 0)
                    {
                        Task<ValidationItemResult<ImageRecord>> pendingResponse;
                        if (_pendingResponses.TryDequeue(out pendingResponse))
                            pendingResponses.Enqueue(pendingResponse);
                    }
                }
                Queue<PendingCompletion> pendingCompletions = new Queue<PendingCompletion>();
                // Process pending requests
                var count = pendingRequests.Count;
                for (var i = 0; (0 < count) && (i < WorkItemQuantum); i++)
                {
                    // Choose a random item to behave reasonably at both extremes (FIFO/FILO)
                    var index = rand.Next(count);
                    var pendingRequest = pendingRequests[index];
                    pendingRequests[index] = pendingRequests[count - 1];
                    pendingRequests.RemoveAt(count - 1);
                    count--;

                    Guid imageId;
                    if (Guid.TryParse(pendingRequest.Uri.OriginalString, out imageId))
                    {
                        TaskCompletionSource<ValidationItemResult<ImageRecord>> tcs = new TaskCompletionSource<ValidationItemResult<ImageRecord>>(new ResponseState(pendingRequest.Target, pendingRequest.Uri));

                        BusinessProxyRepository.Product.GetImageAsync(pendingRequest.Uri.OriginalString).ContinueWith(t =>
                        {
                            if (t.IsCanceled) tcs.TrySetCanceled();
                            else if (t.IsFaulted) tcs.TrySetException(t.Exception.InnerException);
                            else if (!t.Result.IsEmpty) tcs.TrySetException(new Exception(t.Result.ToString()));
                            else tcs.TrySetResult(t.Result);

                            // Enqueue the response
                            HandleGetResponseResult(tcs.Task);
                        });
                    }
                    else
                    {
                        // Load from application (must have "Build Action"="Content")
                        var originalUriString = pendingRequest.Uri.OriginalString;
                        // Trim leading '/' to avoid problems
                        var resourceStreamUri = originalUriString.StartsWith("/", StringComparison.Ordinal) ?
                            new Uri(originalUriString.TrimStart('/'), UriKind.Relative) : pendingRequest.Uri;
                        // Enqueue resource stream for completion
                        var streamResourceInfo = Application.GetResourceStream(resourceStreamUri);

                        if (streamResourceInfo != null)
                        {
                            pendingCompletions.Enqueue(new PendingCompletion(pendingRequest.Target,
                                pendingRequest.Uri, streamResourceInfo.Stream));
                        }
                    }
                    // Yield to UI thread
                    Thread.Sleep(1);
                }
                // Process pending responses
                for (var i = 0; (0 < pendingResponses.Count) && (i < WorkItemQuantum); i++)
                {
                    var pendingResponse = pendingResponses.Dequeue();
                    var responseState = (ResponseState)pendingResponse.AsyncState;

                    if (pendingResponse.Status == TaskStatus.RanToCompletion)
                    {
                        var imageRecord = pendingResponse.Result.Item;
                        var response = imageRecord.Image;
                        pendingCompletions.Enqueue(new PendingCompletion(responseState.Target, responseState.Uri, new MemoryStream(Convert.FromBase64String(response))));
                    }
                    else
                    {
                        //OSCAR: enqueue response without stream
                        pendingCompletions.Enqueue(new PendingCompletion(responseState.Target, responseState.Uri, null));

                    }
                    // Yield to UI thread
                    Thread.Sleep(1);
                }
                // Process pending completions
                if (pendingCompletions.Count > 0)
                {
                    // Get the Dispatcher and process everything that needs to happen on the UI thread in one batch
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        while (pendingCompletions.Count > 0)
                        {
                            // Decode the image and set the source
                            var pendingCompletion = pendingCompletions.Dequeue();
                            if (GetUriSource(pendingCompletion.Target) == pendingCompletion.Uri)
                            {

                                try
                                {
                                    BitmapImage bitmap = null;

                                    if (pendingCompletion.Stream != null)
                                    {
                                        bitmap = new BitmapImage();
                                        bitmap.BeginInit();
                                        bitmap.SetCurrentValue(BitmapImage.StreamSourceProperty, pendingCompletion.Stream);
                                        bitmap.EndInit();
                                    }

                                    if (bitmap != null)
                                    {
                                        if (pendingCompletion.Target is ImageBrush)
                                            ((ImageBrush)pendingCompletion.Target).ImageSource = bitmap;
                                        else if (pendingCompletion.Target is Image)
                                            ((Image)pendingCompletion.Target).Source = bitmap;
                                    }

                                }
                                catch (Exception ex)
                                {
                                    // Ignore image decode exceptions (ex: invalid image)
                                    string asd = ex.Message;
                                }
                            }
                            // Dispose of response stream
                            if (pendingCompletion.Stream != null) pendingCompletion.Stream.Dispose();
                        }
                    }));
                }
            }
        }

        private static void OnUriSourceChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var uri = (Uri)e.NewValue;
            if (!IsEnabled || DesignerProperties.GetIsInDesignMode(o))
            {
                // Avoid handing off to the worker thread (can cause problems for design tools)

                if (o is ImageBrush) ((ImageBrush)o).ImageSource = new BitmapImage(uri);
                else if (o is Image) ((Image)o).Source = new BitmapImage(uri);
                else throw new NotSupportedException("UriSource is applicable only to Image and ImageBrush elements.");
            }
            else
            {
                // Clear-out the current image because it's now stale (helps when used with virtualization)
                if (uri != null && !string.IsNullOrEmpty(uri.OriginalString))
                {
                    lock (_syncBlock)
                    {
                        // Enqueue the request
                        _pendingRequests.Enqueue(new PendingRequest(o, uri));
                        Monitor.Pulse(_syncBlock);
                    }
                }
            }
        }

        private static void HandleGetResponseResult(Task<ValidationItemResult<ImageRecord>> result)
        {
            lock (_syncBlock)
            {
                // Enqueue the response
                _pendingResponses.Enqueue(result);
                Monitor.Pulse(_syncBlock);
            }
        }

        private class PendingRequest
        {
            public PendingRequest(DependencyObject obj, Uri uri)
            {
                Target = obj;
                Uri = uri;
            }

            public DependencyObject Target { get; private set; }
            public Uri Uri { get; private set; }
        }

        private class ResponseState
        {
            public ResponseState(DependencyObject obj, Uri uri)
            {
                Target = obj;
                Uri = uri;
            }

            public DependencyObject Target { get; private set; }
            public Uri Uri { get; private set; }
        }

        private class PendingCompletion
        {
            public PendingCompletion(DependencyObject obj, Uri uri, Stream stream)
            {
                Target = obj;
                Uri = uri;
                Stream = stream;
            }

            public DependencyObject Target { get; private set; }
            public Uri Uri { get; private set; }
            public Stream Stream { get; private set; }
        }


        public static ImageBrush GetBrush(string uriId)
        {
            ImageBrush brush = new ImageBrush();
            UriConverter converter = new UriConverter();
            Uri uri = (Uri)converter.Convert(uriId, typeof(ImageBrush), null, System.Globalization.CultureInfo.InvariantCulture);
            LowProfileImageLoader.SetUriSource(brush, uri);
            return brush;
        }
    }
}