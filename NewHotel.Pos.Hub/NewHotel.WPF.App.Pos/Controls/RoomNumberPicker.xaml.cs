﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using NewHotel.WPF.App.Pos.Model;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for RoomNumberPicker.xaml
    /// </summary>
    public partial class RoomNumberPicker : UserControl
    {
        public RoomNumberPicker()
        {
            InitializeComponent();
        }
    }
}