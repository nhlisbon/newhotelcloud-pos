﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Shapes;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common.Controls.Buttons;

namespace NewHotel.WPF.App.Pos.Controls
{
	public class MasterPage : UserControl
    {
        #region Contants
        
        private static TimeSpan time000 = new TimeSpan(0);
        private static TimeSpan time025 = new TimeSpan(0, 0, 0, 0, 250);
        private static TimeSpan time050 = new TimeSpan(0, 0, 0, 0, 500);

        private static ExponentialEase exponentialEaseOut = new ExponentialEase() { EasingMode = EasingMode.EaseOut };
        private static ExponentialEase exponentialEaseIn = new ExponentialEase() { EasingMode = EasingMode.EaseIn };

        private static Thickness thickness0000cm = new Thickness(0);
        private static Thickness thickness0050cm = (Thickness)(new ThicknessConverter()).ConvertFromString(null, Hotel.DefaultCultureInfo, "0.050cm");
        private static Thickness thickness0075cm = (Thickness)(new ThicknessConverter()).ConvertFromString(null, Hotel.DefaultCultureInfo, "0.075cm");
        private static Thickness thickness0500cm = (Thickness)(new ThicknessConverter()).ConvertFromString(null, Hotel.DefaultCultureInfo, "0.500cm");

        #endregion
        #region Variables
      
        protected MainPage mainPage = null;

        #endregion
        #region Events
        private Action OkActionDialog { get; set; }
        public event Action<MasterPage> AuxiliarPanelExpanded;
        public event Action<MasterPage> AuxiliarPanelCollapsed;
		public event Action<MasterPage> DataLoaded;
		#endregion
		#region Storyboards

		private Storyboard expandAuxiliarPanel = null;
        private Storyboard collapseAuxiliarPanel = null;
        private Storyboard showPopUp = null;
        private Storyboard hidePopUp = null;

        #endregion
        #region UIElements

        //BusyControl busyControl = null;
        Grid dialogPanel = null;
        ContentControl dialogContainer = null;
        ActionButton okButton = null;
        ActionButton cancelButton = null;


        #endregion
        #region Constructors

        public MasterPage()
        {
            NameScope.SetNameScope(this, new NameScope());
            RegisterName("masterPage", this);

            #region Resources

            #region Storyboards

            DoubleAnimationUsingKeyFrames doubleUsingKey = null;
            ThicknessAnimationUsingKeyFrames thicknessUsingKey = null;
            Storyboard storyboard = null;

            #region ExpandAuxiliarPanel

            storyboard = new Storyboard() { BeginTime = time025 };
            expandAuxiliarPanel = storyboard;
            storyboard.Completed += expandAuxiliarPanel_Completed;
            doubleUsingKey = new DoubleAnimationUsingKeyFrames();
            Storyboard.SetTargetProperty(doubleUsingKey, new PropertyPath("ScaleX"));
            Storyboard.SetTargetName(doubleUsingKey, "auxiliarPanelContentScale");
            doubleUsingKey.KeyFrames.Add(new EasingDoubleKeyFrame(1, KeyTime.FromTimeSpan(time050), exponentialEaseOut));
            storyboard.Children.Add(doubleUsingKey);
            doubleUsingKey = new DoubleAnimationUsingKeyFrames();
            Storyboard.SetTargetProperty(doubleUsingKey, new PropertyPath("ScaleY"));
            Storyboard.SetTargetName(doubleUsingKey, "auxiliarPanelContentScale");
            doubleUsingKey.KeyFrames.Add(new EasingDoubleKeyFrame(1, KeyTime.FromTimeSpan(time050), exponentialEaseOut));
            storyboard.Children.Add(doubleUsingKey);
            thicknessUsingKey = new ThicknessAnimationUsingKeyFrames();
            Storyboard.SetTargetProperty(thicknessUsingKey, new PropertyPath("Margin"));
            Storyboard.SetTargetName(thicknessUsingKey, "auxliarPanelContainer");
            thicknessUsingKey.KeyFrames.Add(new EasingThicknessKeyFrame(thickness0075cm, KeyTime.FromTimeSpan(time050), exponentialEaseOut));
            storyboard.Children.Add(thicknessUsingKey);
            thicknessUsingKey = new ThicknessAnimationUsingKeyFrames();
            Storyboard.SetTargetProperty(thicknessUsingKey, new PropertyPath("BorderThickness"));
            Storyboard.SetTargetName(thicknessUsingKey, "auxliarPanelContainer");
            thicknessUsingKey.KeyFrames.Add(new EasingThicknessKeyFrame(thickness0050cm, KeyTime.FromTimeSpan(time050), exponentialEaseOut));
            storyboard.Children.Add(thicknessUsingKey);
            doubleUsingKey = new DoubleAnimationUsingKeyFrames();
            Storyboard.SetTargetProperty(doubleUsingKey, new PropertyPath("Opacity"));
            Storyboard.SetTargetName(doubleUsingKey, "auxliarPanelContainer");
            doubleUsingKey.KeyFrames.Add(new EasingDoubleKeyFrame(1, KeyTime.FromTimeSpan(time050), exponentialEaseOut));
            storyboard.Children.Add(doubleUsingKey);
            #endregion

            #region CollapseAuxiliarPanel

            storyboard = new Storyboard();
            collapseAuxiliarPanel = storyboard;
            storyboard.Completed += collapseAuxiliarPanel_Completed;
            doubleUsingKey = new DoubleAnimationUsingKeyFrames();
            Storyboard.SetTargetProperty(doubleUsingKey, new PropertyPath("ScaleX"));
            Storyboard.SetTargetName(doubleUsingKey, "auxiliarPanelContentScale");
            doubleUsingKey.KeyFrames.Add(new EasingDoubleKeyFrame(0, KeyTime.FromTimeSpan(time050), exponentialEaseOut));
            storyboard.Children.Add(doubleUsingKey);
            doubleUsingKey = new DoubleAnimationUsingKeyFrames();
            Storyboard.SetTargetProperty(doubleUsingKey, new PropertyPath("ScaleY"));
            Storyboard.SetTargetName(doubleUsingKey, "auxiliarPanelContentScale");
            doubleUsingKey.KeyFrames.Add(new EasingDoubleKeyFrame(0, KeyTime.FromTimeSpan(time050), exponentialEaseOut));
            storyboard.Children.Add(doubleUsingKey);
            thicknessUsingKey = new ThicknessAnimationUsingKeyFrames();
            Storyboard.SetTargetProperty(thicknessUsingKey, new PropertyPath("Margin"));
            Storyboard.SetTargetName(thicknessUsingKey, "auxliarPanelContainer");
            thicknessUsingKey.KeyFrames.Add(new EasingThicknessKeyFrame(thickness0000cm, KeyTime.FromTimeSpan(time050), exponentialEaseOut));
            storyboard.Children.Add(thicknessUsingKey);
            thicknessUsingKey = new ThicknessAnimationUsingKeyFrames();
            Storyboard.SetTargetProperty(thicknessUsingKey, new PropertyPath("BorderThickness"));
            Storyboard.SetTargetName(thicknessUsingKey, "auxliarPanelContainer");
            thicknessUsingKey.KeyFrames.Add(new EasingThicknessKeyFrame(thickness0000cm, KeyTime.FromTimeSpan(time050), exponentialEaseOut));
            storyboard.Children.Add(thicknessUsingKey);
            doubleUsingKey = new DoubleAnimationUsingKeyFrames();
            Storyboard.SetTargetProperty(doubleUsingKey, new PropertyPath("Opacity"));
            Storyboard.SetTargetName(doubleUsingKey, "auxliarPanelContainer");
            doubleUsingKey.KeyFrames.Add(new EasingDoubleKeyFrame(0, KeyTime.FromTimeSpan(time050), exponentialEaseOut));
            storyboard.Children.Add(doubleUsingKey);

            #endregion

            #region ShowPopUp
            storyboard = new Storyboard();
            showPopUp = storyboard;
            doubleUsingKey = new DoubleAnimationUsingKeyFrames();
            Storyboard.SetTargetProperty(doubleUsingKey, new PropertyPath("Opacity"));
            Storyboard.SetTargetName(doubleUsingKey, "popUpContainer");
            doubleUsingKey.KeyFrames.Add(new DiscreteDoubleKeyFrame(0, KeyTime.FromTimeSpan(time000)));
            doubleUsingKey.KeyFrames.Add(new EasingDoubleKeyFrame(1, KeyTime.FromTimeSpan(time050), exponentialEaseOut));
            storyboard.Children.Add(doubleUsingKey);
            #endregion

            #region HidePopUp
            storyboard = new Storyboard();
            hidePopUp = storyboard;
            doubleUsingKey = new DoubleAnimationUsingKeyFrames();
            Storyboard.SetTargetProperty(doubleUsingKey, new PropertyPath("Opacity"));
            Storyboard.SetTargetName(doubleUsingKey, "popUpContainer");
            doubleUsingKey.KeyFrames.Add(new DiscreteDoubleKeyFrame(1, KeyTime.FromTimeSpan(time000)));
            doubleUsingKey.KeyFrames.Add(new EasingDoubleKeyFrame(0, KeyTime.FromTimeSpan(time050), exponentialEaseOut));
            storyboard.Children.Add(doubleUsingKey);
            #endregion

            #endregion

            #endregion
            #region Content

            var mainBorder = new Border();
            mainBorder.SetBinding(Border.StyleProperty, new Binding() { Source = new DynamicResourceExtension("backBorderStyle") });
            AddChild(mainBorder);

            Grid externalGrid = new Grid();
            Grid semiExternalGrid = new Grid();
            Grid internalGrid = new Grid();

            mainBorder.Child = externalGrid;

            externalGrid.Children.Add(semiExternalGrid);

            semiExternalGrid.Children.Add(internalGrid);
            semiExternalGrid.Children.Add(dialogPanel = new Grid() { Visibility = System.Windows.Visibility.Collapsed });

            Grid auxGrid = null;
            Border auxBorder = null;
            StackPanel auxStackPanel = null;

            Rectangle auxRect = new Rectangle() { Fill = Brushes.Black, Opacity = 0.4 };
            dialogPanel.Children.Add(auxRect);
            dialogPanel.Children.Add(auxBorder = new Border() { Background = Brushes.White, MaxHeight = (double)(new LengthConverter()).ConvertFromString("10cm"), VerticalAlignment = System.Windows.VerticalAlignment.Center, HorizontalAlignment = System.Windows.HorizontalAlignment.Center, CornerRadius = new CornerRadius(6) });
            auxBorder.Effect = new DropShadowEffect() { Color = Colors.White, BlurRadius = 15, ShadowDepth = 0 };
            auxBorder.Child = new Border() { Margin = new Thickness(4), BorderBrush = Brushes.Black, BorderThickness = new Thickness(2), CornerRadius = new CornerRadius(6) };
            auxBorder = auxBorder.Child as Border;

            auxBorder.Child = auxGrid = new Grid();
            auxGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            auxGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            auxGrid.Children.Add(dialogContainer = new ContentControl() { Margin = thickness0500cm, Width = (double)(new LengthConverter()).ConvertFromString("Auto") });

            auxGrid.Children.Add(auxStackPanel = new StackPanel() { Orientation = Orientation.Horizontal, HorizontalAlignment = System.Windows.HorizontalAlignment.Center });
            Grid.SetRow(auxStackPanel, 1);
            auxStackPanel.Children.Add(okButton = new ActionButton() { Width = (double)(new LengthConverter()).ConvertFromString("3.5cm"), Height = (double)(new LengthConverter()).ConvertFromString("1.75cm"), Text = "Ok" });
            okButton.SetBinding(ActionButton.StyleProperty, new Binding() { Source = new DynamicResourceExtension("okButtonStyle") });
            okButton.Click += OkDialog_Click;
            auxStackPanel.Children.Add(cancelButton = new ActionButton() { Width = (double)(new LengthConverter()).ConvertFromString("3.5cm"), Height = (double)(new LengthConverter()).ConvertFromString("1.75cm"), Text = "Cancel" });
            cancelButton.SetBinding(ActionButton.StyleProperty, new Binding() { Source = new DynamicResourceExtension("cancelButtonStyle") });
            cancelButton.Click += CancelDialog_Click;

            internalGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            internalGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            internalGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            internalGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            internalGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });

            internalGrid.Children.Add(auxBorder = new Border());
            auxBorder.SetBinding(Border.StyleProperty, new Binding() { Source = new DynamicResourceExtension("panelBorderStyle") });
            Grid.SetColumnSpan(auxBorder, 2);
            auxBorder.Child = auxGrid = new Grid();
            auxGrid.Children.Add(auxStackPanel = new StackPanel() { Orientation = Orientation.Horizontal, Margin = (Thickness)new ThicknessConverter().ConvertFromString("0.5cm, 0.125cm") });
            auxStackPanel.Children.Add(auxBorder = new Border() { CornerRadius = new CornerRadius(10), Width = (double)(new LengthConverter()).ConvertFromString("1.5cm"), Height = (double)(new LengthConverter()).ConvertFromString("1.5cm"), VerticalAlignment = System.Windows.VerticalAlignment.Center });
            auxBorder.Background = new ImageBrush();
            BindingOperations.SetBinding(auxBorder.Background as ImageBrush, ImageBrush.ImageSourceProperty, new Binding()
            {
                Source = new DynamicResourceExtension("mainLogoImageSource")
            });
            auxBorder.Effect = new DropShadowEffect() { Color = Colors.Black, ShadowDepth = 0 };

            // Me quede por aqui
            auxStackPanel.Children.Add(new Grid());

            auxGrid.Children.Add(auxStackPanel = new StackPanel()
            {
                HorizontalAlignment = System.Windows.HorizontalAlignment.Right,
                Orientation = Orientation.Horizontal,
                Margin = (Thickness)new ThicknessConverter().ConvertFromString("0.125cm")
            });

            internalGrid.Children.Add(auxBorder = new Border());
            internalGrid.Children.Add(auxBorder = new Border());
            internalGrid.Children.Add(auxGrid = new Grid());
            #endregion

            mainPage = new MainPage();

            // Bindings
            mainPage.SetBinding(MainPage.ActionsPanelContentProperty, new Binding("ActionsPanelContent") { ElementName = "masterPage" });
            mainPage.SetBinding(MainPage.AuxiliarPanelContentProperty, new Binding("AuxiliarPanelContent") { ElementName = "masterPage" });
            mainPage.SetBinding(MainPage.DialogPanelContentProperty, new Binding("DialogPanelContent") { ElementName = "masterPage" });
            mainPage.SetBinding(MainPage.MainPanelContentProperty, new Binding("MainPanelContent") { ElementName = "masterPage" });
            mainPage.SetBinding(MainPage.PopUpPanelContentProperty, new Binding("PopUpPanelContent") { ElementName = "masterPage" });
            mainPage.SetBinding(MainPage.ShowAuxiliarPanelProperty, new Binding("ShowAuxiliarPanel") { ElementName = "masterPage" });
            mainPage.SetBinding(MainPage.ShowDialogProperty, new Binding("ShowDialog") { ElementName = "masterPage" });
            mainPage.SetBinding(MainPage.ShowPopUpProperty, new Binding("ShowPopUp") { ElementName = "masterPage" });
        }


        #endregion
        #region Methods

        private void expandAuxiliarPanel_Completed(object sender, EventArgs e)
        {
            AuxiliarPanelExpanded?.Invoke(this);
        }

        private void collapseAuxiliarPanel_Completed(object sender, EventArgs e)
        {
            AuxiliarPanelCollapsed?.Invoke(this);
        }

        private void OkDialog_Click(object sender, RoutedEventArgs e)
        {
            OkActionDialog?.Invoke();
            CloseDialog();
        }

        private void CancelDialog_Click(object sender, RoutedEventArgs e)
        {
            CloseDialog();
        }

        public void CloseDialog()
        {
            dialogPanel.Visibility = System.Windows.Visibility.Collapsed;
        }

        #endregion
    }
}
