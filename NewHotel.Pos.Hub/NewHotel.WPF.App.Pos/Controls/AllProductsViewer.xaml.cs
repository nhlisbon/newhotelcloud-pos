﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using NewHotel.WPF.Common.Controls.TouchScreen;
using NewHotel.WPF.Model.Model;
using System.Windows.Data;

namespace NewHotel.WPF.App.Pos.Controls
{
	/// <summary>
	/// Interaction logic for AllProductsViewer.xaml
	/// </summary>
	public partial class AllProductsViewer : UserControl
    {
        #region Variables

        private readonly HashSet<Guid> _groupChecks = new HashSet<Guid>();
        private readonly HashSet<Guid> _familyChecks = new HashSet<Guid>();
        private readonly HashSet<Guid> _subfamilyChecks = new HashSet<Guid>();
        private Timer _timer;

        #endregion
        #region Constructor

        public AllProductsViewer()
        {
            InitializeComponent();

            TouchScreenFixNumPad.Enter_Click += TouchScreenFixNumPad_Enter_Click;
            TouchScreenFixNumPad.CurrentControlChanged += TouchScreenFixNumPad_CurrentControlChanged;

            Unloaded += AllProductsViewer_Unloaded;
            IsVisibleChanged += AllProductsViewer_IsVisibleChanged;

            _timer = new Timer(TimerCallback);
        }

        #endregion
        #region Events

        public event Action<ProductButton, Product, int> Product_Added;

        #endregion
        #region Properties

        public Stand Stand => DataContext as Stand;

        #region CurrentProduct

        public Product CurrentProduct
        {
            get => (Product)GetValue(CurrentProductProperty);
            set => SetValue(CurrentProductProperty, value);
        }

        public static readonly DependencyProperty CurrentProductProperty =
            DependencyProperty.Register(nameof(CurrentProduct), typeof(Product), typeof(AllProductsViewer), new PropertyMetadata(CurrentProductPropertyChange));

        private static void CurrentProductPropertyChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = (AllProductsViewer)d;
            obj.ShowCurrentProduct = e.NewValue != null;
        }

        #endregion
        #region ShowAll
        public bool ShowAll
        {
            get => (bool)GetValue(ShowAllProperty);
            set => SetValue(ShowAllProperty, value);
        }

        public static readonly DependencyProperty ShowAllProperty =
            DependencyProperty.Register(nameof(ShowAll), typeof(bool), typeof(AllProductsViewer));

        #endregion
        #region ResultsCount

        public int ResultsCount
        {
            get => (int)GetValue(ResultsCountProperty);
            set => SetValue(ResultsCountProperty, value);
        }

        public static readonly DependencyProperty ResultsCountProperty =
            DependencyProperty.Register(nameof(ResultsCount), typeof(int), typeof(AllProductsViewer));

        #endregion
        #region ShowCurrentProduct

        public bool ShowCurrentProduct
        {
            get => (bool)GetValue(ShowCurrentProductProperty);
            set => SetValue(ShowCurrentProductProperty, value);
        }

        public static readonly DependencyProperty ShowCurrentProductProperty =
            DependencyProperty.Register(nameof(ShowCurrentProduct), typeof(bool), typeof(AllProductsViewer));

        #endregion

        #endregion
        #region Methods

        private void AllProductsViewer_Unloaded(object sender, RoutedEventArgs e) => searchNameTextBox.Text = string.Empty;
      
        private void AllProductsViewer_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            dataGridView.SelectedIndex = -1;
            if (!IsVisible)
                searchNameTextBox.Text = string.Empty;
            else
                FilterGrid();
        }

        private void TimerCallback(object state)
        {
            UIThread.Invoke(() =>
            {
                FilterGrid();
            });
        }

		private void FilterGrid()
        {
			FilterProducts();
        }

        private void TouchScreenFixNumPad_CurrentControlChanged(TouchScreenFixNumPad obj)
        {
            if (obj.InstanceName == "ticketNumPad")
            {
                if (obj.CurrentControl != null)
                {
                    var control = (TextBox)obj.CurrentControl;
                    control.TextChanged += CmdTextBox_TextChanged;
                }
            }
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var tb = (TextBox)sender;
            if (string.IsNullOrEmpty(tb.Text) || tb.Text.Length >= 1)// tb.Text.Length >= 3)
                _timer.Change(500, Timeout.Infinite);
        }

        private void TouchScreenFixNumPad_Enter_Click(TouchScreenFixNumPad obj)
        {
            if (obj.InstanceName == "ticketNumPad")
            {
                if (Product_Added != null && CurrentProduct != null)
                {
                    var control = (TextBox)obj.CurrentControl;
                    int index;
                    if ((index = control.Text.IndexOf(" X ", StringComparison.Ordinal)) >= 0)
                    {
                        if (!int.TryParse(control.Text.Substring(index + 3), out var qty))
                            qty = 1;

                        Product_Added(null, CurrentProduct, qty);
                        control.Text = string.Empty;
                    }
                }
            }
        }

        private void CmdTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var control = (TextBox)TouchScreenFixNumPad.Instance["ticketNumPad"].CurrentControl;
            if (!control.Text.Contains(' '))
                CurrentProduct = Stand.Products.FirstOrDefault(p => p.Code.ToString().StartsWith(control.Text));
        }

        private void ProductButton_Product_Added(ProductButton arg1, Product arg2, int arg3)
        {
            Product_Added?.Invoke(arg1, arg2, arg3);
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            FilterGrid();
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            FilterGrid();
        }

        private void GroupCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var cbx = (CheckBox)sender;
            var group = (GroupProduct)cbx.DataContext;
            _groupChecks.Add(group.Id);
            FilterGrid();
        }

        private void GroupCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            var cbx = (CheckBox)sender;
            var group = (GroupProduct)cbx.DataContext;
            _groupChecks.Remove(group.Id);
            FilterGrid();
        }

        private void FamilyCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var cbx = (CheckBox)sender;
            var family = (FamilyProduct)cbx.DataContext;
            _familyChecks.Add(family.Id);
            FilterGrid();
        }

        private void FamilyCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            var cbx = (CheckBox)sender;
            var family = (FamilyProduct)cbx.DataContext;
            _familyChecks.Remove(family.Id);
            FilterGrid();
        }

        private void SubfamilyCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var cbx = (CheckBox)sender;
            var subfamily = (SubfamilyProduct)cbx.DataContext;
            _subfamilyChecks.Add(subfamily.Id);
            FilterGrid();
        }

        private void SubfamilyCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            var cbx = (CheckBox)sender;
            var subfamily = (SubfamilyProduct)cbx.DataContext;
            _subfamilyChecks.Remove(subfamily.Id);
            FilterGrid();
        }

        private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            FilterGrid();
        }

        CollectionView filteredProducts;
        string searchBarCode;
        string searchName;
        Stand localStand;

        private void FilterProducts()
        {
            if (Stand == null)
                return;
            if (Stand != localStand)
			{
				localStand = Stand;
				filteredProducts = null;
			}

			searchBarCode = searchBarCodeTextBox.Text.Clean();
			searchName = searchNameTextBox.Text.Clean();

			if (filteredProducts == null)
			{
				filteredProducts = localStand.Products.GetCollectionView();

				filteredProducts.Filter = x =>
				{
					var product = (Product)x;
					return !product.Inactive && product.CanBeSold && product.Id != localStand.TipService &&
							(_groupChecks.Count == 0 || product.Group.HasValue && _groupChecks.Contains(product.Group.Value)) &&
							(_familyChecks.Count == 0 || product.Family.HasValue && _familyChecks.Contains(product.Family.Value)) &&
							(_subfamilyChecks.Count == 0 || product.Subfamily.HasValue && _subfamilyChecks.Contains(product.Subfamily.Value)) &&
							(string.IsNullOrEmpty(searchBarCode) || (!string.IsNullOrEmpty(product.BarCode) && product.BarCode == searchBarCode)) &&
							(string.IsNullOrEmpty(searchName) || product.Description.Clean().IndexOf(searchName, StringComparison.InvariantCultureIgnoreCase) >= 0);
				};
				filteredProducts.SortDescriptions.Add(new System.ComponentModel.SortDescription(nameof(Product.Description), System.ComponentModel.ListSortDirection.Ascending));
				dataGridView.ItemsSource = filteredProducts;
			}

            if (IsVisible)
            {
                filteredProducts?.Refresh();
                ResultsCount = filteredProducts?.Count ?? 0;
            }
		}

        #endregion
    }
}