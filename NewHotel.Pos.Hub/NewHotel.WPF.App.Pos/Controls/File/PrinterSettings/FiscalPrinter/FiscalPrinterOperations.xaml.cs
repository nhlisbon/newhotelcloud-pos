﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using NewHotel.Contracts;
using NewHotel.Pos.Core;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Localization;
using NewHotel.Pos.PrinterDocument.Fiscal;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.Common.Controls;
using NewHotel.WPF.Common.Controls.Busy;
using NewHotel.WPF.Common.Models;
using NewHotel.WPF.Common.Notifications;
using ValidationResult = NewHotel.Contracts.ValidationResult;

namespace NewHotel.WPF.App.Pos.Controls.File.PrinterSettings.FiscalPrinter
{
    /// <summary>
    /// Interaction logic for FiscalPrinterOperations.xaml
    /// </summary>
    public partial class FiscalPrinterOperations : System.Windows.Controls.UserControl
    {
        #region Variables

        private IFiscalPrinter _fiscalPrinter;
        private IEnumerable<CollectionViewItem<Document>> _documents;

        #endregion
        #region Constructor

        public FiscalPrinterOperations()
        {
            InitializeComponent();
        }

        #endregion
        #region Properties

        public IFiscalPrinter FiscalPrinter => _fiscalPrinter ??= CwFactory.Resolve<IFiscalPrinter>();

        public IEnumerable<CollectionViewItem<Document>> Documents
        {
            get
            {
                if (_documents == null)
                {
                    _documents = new[]
                    {
                        new CollectionViewItem<Document> (Document.Last,Document.Last, LocalizationMgr.Translation("Last")),
                        new CollectionViewItem<Document>(Document.Fiscal,Document.Fiscal ,LocalizationMgr.Translation("Fiscal")),
                        new CollectionViewItem<Document> (Document.NonFiscal, Document.NonFiscal, LocalizationMgr.Translation("NonFiscal")),
                        new CollectionViewItem<Document> (Document.XReport, Document.XReport, LocalizationMgr.Translation("XReport")),
                        new CollectionViewItem<Document> (Document.ZReport, Document.ZReport, LocalizationMgr.Translation("ZReport")),
                        new CollectionViewItem<Document> (Document.All, Document.All, LocalizationMgr.Translation("All")),
                    };
                    DocumentSelected = _documents.First().Id;
                }

                return _documents;
            }
        }

        #region DocumentSelected

        public Document DocumentSelected
        {
            get => (Document)GetValue(DocumentSelectedProperty);
            set => SetValue(DocumentSelectedProperty, value);
        }

        public static readonly DependencyProperty DocumentSelectedProperty =
            DependencyProperty.Register(nameof(DocumentSelected), typeof(Document), typeof(FiscalPrinterOperations), new PropertyMetadata(OnDocumentSelected));

        private static void OnDocumentSelected(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = (FiscalPrinterOperations)d;
            if (obj.RangePicker != null)
                obj.RangePicker.Visibility = obj.DocumentSelected == Document.Last ? Visibility.Collapsed : Visibility.Visible;
        }

        #endregion

        #endregion
        #region Methods

        private async void FiscalPrinterStatus_Click(object sender, RoutedEventArgs e)
        {
            BusyControl.Instance.Show();
            try
            {
                ValidationResult result = await FiscalPrinter.GetPrinterStatusAsync();
                if (result.IsEmpty) NotificationWindow.ShowNotificationWindow("OK");
                else ErrorsWindow.ShowErrorWindows(result);
            }
            finally
            {
                BusyControl.Instance.Close();
            }
        }

        private async void ServerStatus_Click(object sender, RoutedEventArgs e)
        {
            BusyControl.Instance.Show();
            try
            {
                ValidationItemResult<string> result = await FiscalPrinter.GetServerStatusAsync();
                if (result.IsEmpty) NotificationWindow.ShowNotificationWindow(result.Item);
                else ErrorsWindow.ShowErrorWindows(result);
            }
            finally
            {
                BusyControl.Instance.Close();
            }
        }

        private async void ExportToFile_Click(object sender, RoutedEventArgs e)
        {
            BusyControl.Instance.Show();
            try
            {
                Document doc = DocumentSelected;
                ValidationResult result;

                if (doc == Document.Last)
                {
                    result = await FiscalPrinter.UploadLastDocumentToFileAsync();
                }
                else
                {
                    switch (RangePicker.Mode)
                    {
                        case RangePicker.RpMode.DateRange:
                            result = await FiscalPrinter.UploadDocumentToFileAsync(doc, RangePicker.StartDate, RangePicker.EndDate);
                            break;
                        case RangePicker.RpMode.NumberRange:
                            result = await FiscalPrinter.UploadDocumentToFileAsync(doc, RangePicker.StartNumber, RangePicker.EndNumber);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                if (result.IsEmpty) NotificationWindow.ShowNotificationWindow("OK");
                else ErrorsWindow.ShowErrorWindows(result);
            }
            finally
            {
                BusyControl.Instance.Close();
            }
        }

        private async void ReprintDocument_Click(object sender, RoutedEventArgs e)
        {
            BusyControl.Instance.Show();
            try
            {
                Document doc = DocumentSelected;
                ValidationResult result;

                if (doc == Document.Last)
                {
                    result = await FiscalPrinter.ReprintLastDocumentAsync();
                }
                else
                {
                    switch (RangePicker.Mode)
                    {
                        case RangePicker.RpMode.DateRange:
                            result = await FiscalPrinter.ReprintAsync(doc, RangePicker.StartDate, RangePicker.EndDate);
                            break;
                        case RangePicker.RpMode.NumberRange:
                            result = await FiscalPrinter.ReprintAsync(doc, RangePicker.StartNumber, RangePicker.EndNumber);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                if (result.IsEmpty) NotificationWindow.ShowNotificationWindow("OK");
                else ErrorsWindow.ShowErrorWindows(result);
            }
            finally
            {
                BusyControl.Instance.Close();
            }
        }

        private async void ImportFile_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            var rDialog = dialog.ShowDialog();
            if (rDialog == DialogResult.OK)
            {
                BusyControl.Instance.Show();
                try
                {
                    byte[] file = System.IO.File.ReadAllBytes(dialog.FileName);
                    ValidationResult result = await FiscalPrinter.LoadFileAsync(file);

                    if (result.IsEmpty) NotificationWindow.ShowNotificationWindow("OK");
                    else ErrorsWindow.ShowErrorWindows(result);
                }
                finally
                {
                    BusyControl.Instance.Close();
                }
            }
        }

        private async void ExecuteCommand_Click(object sender, RoutedEventArgs e)
        {
            BusyControl.Instance.Show();
            try
            {
                ValidationResult result = await FiscalPrinter.ExecuteCommandAsync(EdtPrinteCmd.Text);
                if (!result.IsEmpty)
                    ErrorsWindow.ShowErrorWindows(result);
            }
            finally
            {
                BusyControl.Instance.Close();
            }
        }

        #endregion
    }

}
