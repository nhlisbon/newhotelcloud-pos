﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NewHotel.WPF.App.Pos.Model;

namespace NewHotel.WPF.App.Pos.Controls.Printers
{
    /// <summary>
    /// Interaction logic for Simple.xaml
    /// </summary>
    public partial class SimplePrinterSetting : UserControl
    {
        #region Constructors

        public SimplePrinterSetting()
        {
            InitializeComponent();
        }


        #endregion
        #region Properties

        #region Printers

        public ObservableCollection<string> Printers
        {
            get => (ObservableCollection<string>)GetValue(PrintersProperty);
            set => SetValue(PrintersProperty, value);
        }

        public static readonly DependencyProperty PrintersProperty =
            DependencyProperty.Register(nameof(Printers), typeof(ObservableCollection<string>), typeof(SimplePrinterSetting));

        #endregion
        #region PrinterDescription

        public string PrinterDescription
        {
            get => (string)GetValue(PrinterDescriptionProperty);
            set => SetValue(PrinterDescriptionProperty, value);
        }

        public static readonly DependencyProperty PrinterDescriptionProperty =
            DependencyProperty.Register(nameof(PrinterDescription), typeof(string), typeof(SimplePrinterSetting));

        #endregion
        #region PrinterServer

        public string PrinterServer
        {
            get => (string)GetValue(PrinterServerProperty);
            set => SetValue(PrinterServerProperty, value);
        }

        public static readonly DependencyProperty PrinterServerProperty =
            DependencyProperty.Register(nameof(PrinterServer), typeof(string), typeof(SimplePrinterSetting));

        #endregion
        #region PrinterServerTranslationId

        public string PrinterServerTranslationId
        {
            get => (string)GetValue(PrinterServerTranslationIdProperty);
            set => SetValue(PrinterServerTranslationIdProperty, value);
        }

        public static readonly DependencyProperty PrinterServerTranslationIdProperty =
            DependencyProperty.Register(nameof(PrinterServerTranslationId), typeof(string), typeof(SimplePrinterSetting));

        #endregion
        #region PrinterDescriptionTranslationId

        public string PrinterDescriptionTranslationId
        {
            get => (string)GetValue(PrinterDescriptionTranslationIdProperty);
            set => SetValue(PrinterDescriptionTranslationIdProperty, value);
        }

        public static readonly DependencyProperty PrinterDescriptionTranslationIdProperty =
            DependencyProperty.Register(nameof(PrinterDescriptionTranslationId), typeof(string), typeof(SimplePrinterSetting));


        #endregion

        #endregion
    }
}
