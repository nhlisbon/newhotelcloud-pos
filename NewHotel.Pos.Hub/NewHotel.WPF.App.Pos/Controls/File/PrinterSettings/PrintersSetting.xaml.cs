﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NewHotel.Pos.IoC;
using NewHotel.Pos.PrinterDocument.Interfaces;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Model.Interface;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for PrintersSetting.xaml
    /// </summary>
    public partial class PrintersSetting : UserControl
    {
        public PrintersSetting()
        {
            InitializeComponent();
        }

        public IPosSettings PosSettings => CwFactory.Resolve<IPosSettings>();
    }
}
