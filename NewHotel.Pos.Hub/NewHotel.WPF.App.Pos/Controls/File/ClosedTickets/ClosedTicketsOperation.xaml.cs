﻿using System;
using System.Windows;
using System.Windows.Controls;
using NewHotel.Pos.Hub.Model;
using NewHotel.WPF.App.Pos.Model;

namespace NewHotel.WPF.App.Pos.Controls.File.ClosedTickets
{
    /// <summary>
    /// Interaction logic for ClosedTicketsOperation.xaml
    /// </summary>
    public partial class ClosedTicketOperations : UserControl
    {
        #region Constructor

        public ClosedTicketOperations()
        {
            InitializeComponent();
        }

        #endregion
        #region Events

        public event EventHandler<Document> Print;
        public event EventHandler<ClosedTicketRecord> CancelTicket;
        public event EventHandler<ClosedTicketRecord> CreditNoteTicket;
        public event EventHandler<ClosedTicketRecord> CreateQuickTicket;

        #endregion
        #region Properties

        #region Ticket

        public ClosedTicketRecord Ticket
        {
            get => (ClosedTicketRecord)GetValue(TicketProperty);
            set => SetValue(TicketProperty, value);
        }

        public static readonly DependencyProperty TicketProperty =
            DependencyProperty.Register(nameof(Ticket), typeof(ClosedTicketRecord), typeof(ClosedTicketOperations), new PropertyMetadata(OnTicketChanged));

        private static void OnTicketChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = (ClosedTicketOperations)d;
            var newTicket = (ClosedTicketRecord)e.NewValue;

            obj.DataContext = newTicket;
            obj.AllowPrintBallot = newTicket.IsBallot;
            obj.AllowPrintInvoice = newTicket.IsInvoice;
            obj.AllowPrintCreditNote = !string.IsNullOrEmpty(newTicket.CreditNoteNumber);
            obj.AllowCancelTicket = !newTicket.Cancelled && string.IsNullOrEmpty(newTicket.CreditNoteNumber) &&
                Hotel.Instance.AllowCancelTicket(newTicket.CloseDate.Add(newTicket.CloseTime.TimeOfDay), !string.IsNullOrEmpty(newTicket.InvoiceNumber));
            obj.AllowCreateCreditNote = newTicket is { LocalTicket: true, Cancelled: false, IsInvoice: true, CreditNoteNumber: null or "" };
            obj.LocalCreditNote = newTicket.LocalCreditNote;

            if (!Hotel.Instance.HasFiscalPrinter) return;

            obj.AllowPrintFpInvoiceCopy = newTicket.FpInvoiceNumber.HasValue;
            obj.AllowPrintFpCreditNoteCopy = newTicket.FpCreditNoteNumber.HasValue;
            obj.AllowPrintFpInvoice = !newTicket.Cancelled &&
                                      !obj.AllowPrintFpCreditNoteCopy &&
                                      !obj.AllowPrintFpInvoiceCopy &&
                                      !string.IsNullOrEmpty(newTicket.InvoiceNumber);

            obj.AllowPrintFpCreditNote = newTicket.Cancelled &&
                                         !newTicket.FpCreditNoteNumber.HasValue &&
                                         newTicket.FpInvoiceNumber.HasValue &&
                                         !string.IsNullOrEmpty(newTicket.InvoiceNumber);

            obj.AllowFpCopy = obj.AllowPrintFpInvoiceCopy || obj.AllowPrintFpCreditNoteCopy;
            obj.AllowFp = obj.AllowPrintFpInvoice || obj.AllowFpCopy;
        }

        #endregion
        #region AllowPrintBallot

        public bool AllowPrintBallot
        {
            get => (bool)GetValue(AllowPrintBallotProperty);
            set => SetValue(AllowPrintBallotProperty, value);
        }

        public static readonly DependencyProperty AllowPrintBallotProperty = DependencyProperty.Register(nameof(AllowPrintBallot), typeof(bool), typeof(ClosedTicketOperations));

        #endregion
        #region AllowPrintInvoice

        public bool AllowPrintInvoice
        {
            get => (bool)GetValue(AllowPrintInvoiceProperty);
            set => SetValue(AllowPrintInvoiceProperty, value);
        }

        public static readonly DependencyProperty AllowPrintInvoiceProperty =
            DependencyProperty.Register(nameof(AllowPrintInvoice), typeof(bool), typeof(ClosedTicketOperations));

        #endregion
        #region AllowPrintCreditNote

        public bool AllowPrintCreditNote
        {
            get => (bool)GetValue(AllowPrintCreditNoteProperty);
            set => SetValue(AllowPrintCreditNoteProperty, value);
        }

        public static readonly DependencyProperty AllowPrintCreditNoteProperty =
            DependencyProperty.Register(nameof(AllowPrintCreditNote), typeof(bool), typeof(ClosedTicketOperations));

        #endregion
        #region AllowCancelTicket

        public bool AllowCancelTicket
        {
            get => (bool)GetValue(AllowCancelTicketProperty);
            set => SetValue(AllowCancelTicketProperty, value);
        }

        public static readonly DependencyProperty AllowCancelTicketProperty =
            DependencyProperty.Register(nameof(AllowCancelTicket), typeof(bool), typeof(ClosedTicketOperations));

        #endregion
        #region AllowCreateCreditNote

        public bool AllowCreateCreditNote
        {
            get => (bool)GetValue(AllowCreateCreditNoteProperty);
            set => SetValue(AllowCreateCreditNoteProperty, value);
        }

        public static readonly DependencyProperty AllowCreateCreditNoteProperty =
            DependencyProperty.Register(nameof(AllowCreateCreditNote), typeof(bool), typeof(ClosedTicketOperations));

        #endregion
        #region AllowFp

        public bool AllowFp
        {
            get => (bool)GetValue(AllowFpProperty);
            set => SetValue(AllowFpProperty, value);
        }

        public static readonly DependencyProperty AllowFpProperty =
            DependencyProperty.Register(nameof(AllowFp), typeof(bool), typeof(ClosedTicketOperations));

        #endregion
        #region AllowFpCopy

        public bool AllowFpCopy
        {
            get => (bool)GetValue(AllowFpCopyProperty);
            set => SetValue(AllowFpCopyProperty, value);
        }

        public static readonly DependencyProperty AllowFpCopyProperty =
            DependencyProperty.Register(nameof(AllowFpCopy), typeof(bool), typeof(ClosedTicketOperations));

        #endregion
        #region AllowPrintFpInvoice

        public bool AllowPrintFpInvoice
        {
            get => (bool)GetValue(AllowPrintFpInvoiceProperty);
            set => SetValue(AllowPrintFpInvoiceProperty, value);
        }

        public static readonly DependencyProperty AllowPrintFpInvoiceProperty =
            DependencyProperty.Register(nameof(AllowPrintFpInvoice), typeof(bool), typeof(ClosedTicketOperations));

        #endregion
        #region AllowPrintFpCreditNote

        public bool AllowPrintFpCreditNote
        {
            get => (bool)GetValue(AllowPrintFpCreditNoteProperty);
            set => SetValue(AllowPrintFpCreditNoteProperty, value);
        }

        public static readonly DependencyProperty AllowPrintFpCreditNoteProperty =
            DependencyProperty.Register(nameof(AllowPrintFpCreditNote), typeof(bool), typeof(ClosedTicketOperations));

        #endregion
        #region AllowPrintFpInvoiceCopy

        public bool AllowPrintFpInvoiceCopy
        {
            get => (bool)GetValue(AllowPrintFpInvoiceCopyProperty);
            set => SetValue(AllowPrintFpInvoiceCopyProperty, value);
        }

        public static readonly DependencyProperty AllowPrintFpInvoiceCopyProperty =
            DependencyProperty.Register(nameof(AllowPrintFpInvoiceCopy), typeof(bool), typeof(ClosedTicketOperations));

        #endregion
        #region AllowPrintFpCreditNoteCopy

        public bool AllowPrintFpCreditNoteCopy
        {
            get => (bool)GetValue(AllowReprintFpCreditNoteProperty);
            set => SetValue(AllowReprintFpCreditNoteProperty, value);
        }
         
        public static readonly DependencyProperty AllowReprintFpCreditNoteProperty =
            DependencyProperty.Register(nameof(AllowPrintFpCreditNoteCopy), typeof(bool), typeof(ClosedTicketOperations));

        #endregion
        #region LocalCreditNoteProperty

        public bool LocalCreditNote
        {
            get => (bool)GetValue(LocalCreditNoteProperty);
            set => SetValue(LocalCreditNoteProperty, value);
        }

        public static readonly DependencyProperty LocalCreditNoteProperty =
            DependencyProperty.Register(nameof(LocalCreditNote), typeof(bool), typeof(ClosedTicketOperations));

        #endregion

        #endregion
        #region Methods

        private void PrintTicket_Click(object sender, RoutedEventArgs e)
        {
            OnPrint(Document.Ticket);
        }

        private void PrintBallot_Click(object sender, RoutedEventArgs e)
        {
            OnPrint(Document.Ballot);
        }

        private void PrintInvoice_Click(object sender, RoutedEventArgs e)
        {
            OnPrint(Document.Invoice);
        }

        private void PrintCreditNote_Click(object sender, RoutedEventArgs e)
        {
            OnPrint(Document.CreditNote);
        }

        private void PrintFpInvoice_Click(object sender, RoutedEventArgs e)
        {
            OnPrint(Document.FpInvoice);
        }

        private void PrintFpCreditNote_Click(object sender, RoutedEventArgs e)
        {
            OnPrint(Document.FpCreditNote);
        }

        private void PrintFpInvoiceCopy_Click(object sender, RoutedEventArgs e)
        {
            OnPrint(Document.FpInvoiceCopy);
        }

        private void PrintFpCreditNoteCopy_Click(object sender, RoutedEventArgs e)
        {
            OnPrint(Document.FpCreditNoteCopy);
        }

        private void CancelTicket_Click(object sender, RoutedEventArgs e)
        {
            CancelTicket.Invoke(this, Ticket);
        }

        private void CreditNote_Click(object sender, RoutedEventArgs e)
        {
            CreditNoteTicket.Invoke(this, Ticket);
        }

        private void CreateQuickTicket_Click(object sender, RoutedEventArgs e)
        {
            CreateQuickTicket.Invoke(this, Ticket);
        }

        protected virtual void OnPrint(Document document)
        {
            Print.Invoke(this, document);
        }

        #endregion
        #region Enums

        public enum Document
        {
            Ticket, Ballot, Invoice, CreditNote, FpInvoice, FpInvoiceCopy, FpCreditNote, FpCreditNoteCopy
        }

        #endregion

        
    }
}