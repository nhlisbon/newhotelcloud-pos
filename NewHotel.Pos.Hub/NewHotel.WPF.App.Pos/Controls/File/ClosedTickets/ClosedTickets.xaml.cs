﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using NewHotel.Contracts;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Localization;
using NewHotel.Pos.PrinterDocument.Fiscal;
using NewHotel.Pos.PrinterDocument.Interfaces;
using NewHotel.WPF.Core;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using NewHotel.WPF.Model.Interface;
using NewHotel.WPF.Model.Model;
using LocalizedSuffix = NewHotel.Pos.Localization.LocalizedSuffix;
using ValidationResult = NewHotel.Contracts.ValidationResult;

namespace NewHotel.WPF.App.Pos.Controls.File.ClosedTickets
{
    /// <summary>
    /// Interaction logic for ClosedTickets.xaml
    /// </summary>
    public partial class ClosedTickets : UserControl
    {
        #region Constructors

        public ClosedTickets()
        {
            InitializeComponent();
        }

        #endregion

        #region Events

        public event EventHandler<Ticket> ShowOldTicket;
        public event EventHandler<Ticket> ShowOldBallot;
        public event EventHandler<Ticket> ShowOldInvoice;
        public event EventHandler<Ticket> ShowOldCreditNote;

        #endregion

        #region Properties

        #region Stand

        public Stand Stand
        {
            get => (Stand)GetValue(StandProperty);
            set => SetValue(StandProperty, value);
        }

        public static readonly DependencyProperty StandProperty =
            DependencyProperty.Register(nameof(Stand), typeof(Stand), typeof(ClosedTickets));

        #endregion

        #region Cashier

        public Cashier Cashier
        {
            get => (Cashier)GetValue(CashierProperty);
            set => SetValue(CashierProperty, value);
        }

        public static readonly DependencyProperty CashierProperty =
            DependencyProperty.Register(nameof(Cashier), typeof(Cashier), typeof(ClosedTickets));

        #endregion

        #region Date

        public DateTime? Date
        {
            get => (DateTime?)GetValue(OldTicketsFilterSelectedDateProperty);
            set => SetValue(OldTicketsFilterSelectedDateProperty, value);
        }

        public static readonly DependencyProperty OldTicketsFilterSelectedDateProperty =
            DependencyProperty.Register(nameof(Date), typeof(DateTime?), typeof(ClosedTickets));

        #endregion

        #region ClosedTicketSelected

        public ClosedTicketRecord ClosedTicketSelected
        {
            get => (ClosedTicketRecord)GetValue(ClosedTicketSelectedProperty);
            set => SetValue(ClosedTicketSelectedProperty, value);
        }

        public static readonly DependencyProperty ClosedTicketSelectedProperty =
            DependencyProperty.Register(nameof(ClosedTicketSelected), typeof(ClosedTicketRecord), typeof(ClosedTickets));

        #endregion

        #region ClosedTicketsSource

        public IEnumerable<ClosedTicketRecord> ClosedTicketsSource
        {
            get => (IEnumerable<ClosedTicketRecord>)GetValue(ClosedTicketsSourceProperty);
            set => SetValue(ClosedTicketsSourceProperty, value);
        }

        public static readonly DependencyProperty ClosedTicketsSourceProperty =
            DependencyProperty.Register(nameof(ClosedTicketsSource), typeof(IEnumerable<ClosedTicketRecord>), typeof(ClosedTickets));

        #endregion

        #region IoC

        public IPosSettings Settings => CwFactory.Resolve<IPosSettings>();
        public IWindow WindowManager => CwFactory.Resolve<IWindow>();
        public IAppSecurity AppSecurity => CwFactory.Resolve<IAppSecurity>();
        public IFiscalPrinter FiscalPrinter => CwFactory.Resolve<IFiscalPrinter>();

        #endregion

        public ITicketPrinter TicketPrinter => CwFactory.Resolve<ITicketPrinter>();
        public IWindow WindowHelper => CwFactory.Resolve<IWindow>();
        public Point MiddleScreen => new Point(ActualWidth / 2, ActualHeight / 2);

        #endregion

        #region Methods

        #region Event Handlers

        private async void ClosedTickets_OnLoaded(object sender, RoutedEventArgs e)
        {
            WindowManager.ShowBusyDialog();
            try
            {
                await RefreshData();
            }
            finally
            {
                WindowManager.CloseBusyDialog();
            }
        }

        private async void RefreshFilterOldTickets_Click(object sender, RoutedEventArgs e)
        {
            WindowManager.ShowBusyDialog();
            try
            {
                await RefreshData();
            }
            finally
            {
                WindowManager.CloseBusyDialog();
            }
        }

        private void FilterDeselectRegistrationDate_Click(object sender, RoutedEventArgs e)
        {
            Date = null;
        }

        private void OldTicketsDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ClosedTicketSelected != null)
            {
                var operations = new ClosedTicketOperations { Ticket = ClosedTicketSelected };
                operations.Print += PrintClosedTicket;
                operations.CancelTicket += CancelOldTicket_Click;
                operations.CreditNoteTicket += CreditNoteOldTicket_Click;
                operations.CreateQuickTicket += CreateQuickTicket_Click;

                WindowManager.OpenDialog(operations, null, DialogButtons.CancelOnly);
            }
        }

        private void PrintClosedTicket(object o, ClosedTicketOperations.Document document)
        {
            var ctrl = (ClosedTicketOperations)o;
            var ticket = ctrl.Ticket;

            switch (document)
            {
                case ClosedTicketOperations.Document.Ticket:
                case ClosedTicketOperations.Document.Ballot:
                case ClosedTicketOperations.Document.Invoice:
                case ClosedTicketOperations.Document.CreditNote:
                    PrintOldPosDocument(ticket, document);
                    break;
                case ClosedTicketOperations.Document.FpInvoice:
                    PrintOldTicketFpInvoice(ticket);
                    break;
                case ClosedTicketOperations.Document.FpCreditNote:
                    PrintOldTicketFpCreditNote(ticket);
                    break;
                case ClosedTicketOperations.Document.FpInvoiceCopy:
                    FpReprint(Document.Fiscal, ticket.FpInvoiceNumber.Value, ticket.FpInvoiceNumber.Value);
                    break;
                case ClosedTicketOperations.Document.FpCreditNoteCopy:
                    FpReprint(Document.Fiscal, ticket.FpCreditNoteNumber.Value, ticket.FpCreditNoteNumber.Value);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(document), document, null);
            }
        }

        private void CancelOldTicket(ClosedTicketRecord record, bool voidTicket)
        {
            var permission = string.IsNullOrEmpty(record.InvoiceNumber) ? Permissions.TicketsCancelWithoutInvoice : Permissions.TicketsCancelWithInvoice;
            AppSecurity.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, permission, (isValid, validUser) =>
            {
                if (isValid)
                {
                    var cancellationReason = new CancellationReasonPicker();

                    bool CancelTicket()
                    {
                        CancelClosedTicketAsync(record,
                            cancellationReason.CancellationReasonSelected.Id,
                            cancellationReason.Comments, voidTicket, cancellationReason.CreateQuickTicket);

                        return true;
                    }

                    WindowManager.OpenDialog(cancellationReason, CancelTicket);
                }
                else
                    WindowManager.ShowError("UserNotAuthorized".Translate());
            });
        }

        private void CancelOldTicket_Click(object o, ClosedTicketRecord record)
        {
            CancelOldTicket(record, true);
        }

        private void CreditNoteOldTicket_Click(object o, ClosedTicketRecord record)
        {
            CancelOldTicket(record, false);
        }
        
        private async void CreateQuickTicket_Click(object o, ClosedTicketRecord record)
        {
            await CreateQuickTicket(record);
        }

        #endregion

        #region Auxiliars

        private async Task CreateQuickTicket(ClosedTicketRecord closedTicket)
        {
            try
            {
                WindowManager.ShowBusyDialog();
                var result = new ValidationResult();
                var ticketId = (Guid)closedTicket.Id;
                var id = ticketId.ToString();
                var createQuickTicketResult = await BusinessProxyRepository.Ticket.CreateQuickTicketFromClosedTicketAsync(id);
                WindowManager.CloseBusyDialog();
                
                if (createQuickTicketResult.IsEmpty)
                {
                    await Hotel.Instance.CurrentStandCashierContext.UpdateTables();
                    await RefreshData();
                }
                else 
                    result.Add(createQuickTicketResult);
                
                if (!result.IsEmpty) 
                    WindowManager.ShowError(result);
                else
                {
                    var ticket = createQuickTicketResult.Contract as POSTicketContract;
                    if(ticket is not null)
                        WindowManager.ShowInfo(
                            string.Format("QuickTicketCreatedSucess".Translate(), ticket.Number),
                            "Success".Translate()
                        );
                }
            }
            catch (Exception e)
            {
                WindowManager.CloseBusyDialog();
                WindowManager.ShowError(e);
            }
        } 

        /// <summary>
        /// Re-opens a closed ticket and prints it again
        /// The user must have the permission to re-open a ticket and must write a reason for it
        /// </summary>
        /// <param name="closedTicket">The ticket to be re-opened</param>
        /// <param name="cancellationReasonId">The cancellation reason id</param>
        /// <param name="comment">The comment/reason of why it's being re-opened</param>
        /// <param name="voidTicket">If it's a void ticket or not</param>
        /// <param name="createQuickTicket">If in the end of the process it's needs to be recreated has a quick ticket</param>
        private async void CancelClosedTicketAsync(ClosedTicketRecord closedTicket, Guid cancellationReasonId, string comment, bool voidTicket, bool createQuickTicket)
        {
            var result = new ValidationResult();
            var ticketId = (Guid)closedTicket.Id;

            // If closed ticket was printed from fiscal printer then print credit note
            if (Hotel.Instance.HasFiscalPrinter && closedTicket.FpInvoiceNumber.HasValue)
            {
                WindowManager.ShowBusyDialog();
                var loadClosedTicketResult = await BusinessProxyRepository.Ticket.LoadClosedTicketAsync(ticketId, closedTicket.LocalTicket);
                WindowManager.CloseBusyDialog();

                if (loadClosedTicketResult.IsEmpty)
                {
                    await Hotel.Instance.CurrentStandCashierContext.UpdateTables();
                    await RefreshData();
                    closedTicket = ClosedTicketsSource.First(x => (Guid)x.Id == (Guid)closedTicket.Id);
                    var ticket = new Ticket((POSTicketContract)loadClosedTicketResult.Contract);
                    var fpResult = await FiscalPrinter.PrintRefundAsync(ticket, closedTicket.CreditNoteNumber);
                    if (fpResult.IsEmpty)
                    {
                        ticket.FpCreditNoteNumber = fpResult.DocFiscalNumber;
                        ticket.FpSerialCreditNote = fpResult.FpSerial;
                        result.Add(await BusinessProxyRepository.Ticket.UpdateFiscalPrinterDataAsync(ticket.AsContract()));
                    }
                    else result.Add(fpResult);
                }
                else result.Add(loadClosedTicketResult);
            }
            else
            {
                WindowManager.ShowBusyDialog();
                var cancelClosedTicketResult = await BusinessProxyRepository.Ticket.CancelClosedTicketAsync(ticketId, cancellationReasonId, comment, voidTicket, closedTicket.LocalTicket, createQuickTicket);
                WindowManager.CloseBusyDialog();

                if (cancelClosedTicketResult.IsEmpty)
                {
                    PrintOldPosDocument(closedTicket, voidTicket ? ClosedTicketOperations.Document.Ticket : ClosedTicketOperations.Document.CreditNote);
                    await Hotel.Instance.CurrentStandCashierContext.UpdateTables();
                    await RefreshData();
                }
                else result.Add(cancelClosedTicketResult);
            }
            if (!result.IsEmpty) WindowManager.ShowError(result);
        }

        private async void PrintOldTicketFpInvoice(ClosedTicketRecord closedTicket)
        {
            WindowManager.ShowBusyDialog();
            try
            {
                var result = await BusinessProxyRepository.Ticket.LoadClosedTicketAsync((Guid)closedTicket.Id, closedTicket.LocalTicket);
                if (result.IsEmpty)
                {
                    var ticket = new Ticket((POSTicketContract)result.Contract);

                    var fpResult = await FiscalPrinter.PrintFiscalAsync(ticket);
                    if (fpResult.IsEmpty)
                    {
                        // Save doc number assigned by the fiscal printer
                        ticket.FpInvoiceNumber = fpResult.DocFiscalNumber;
                        ticket.FpSerialInvoice = fpResult.FpSerial;
                        result.AddValidations(await BusinessProxyRepository.Ticket.UpdateFiscalPrinterDataAsync(ticket.AsContract()));
                        if (result.IsEmpty)
                            await RefreshData();
                    }
                    else
                        result.AddValidations(fpResult);
                }

                if (!result.IsEmpty)
                    WindowManager.ShowError(result);
            }
            finally
            {
                WindowManager.CloseBusyDialog();
            }
        }

        private async void PrintOldTicketFpCreditNote(ClosedTicketRecord closedTicket)
        {
            WindowManager.ShowBusyDialog();
            try
            {
                var result = await BusinessProxyRepository.Ticket.LoadClosedTicketAsync((Guid)closedTicket.Id, closedTicket.LocalTicket);
                if (result.IsEmpty)
                {
                    var ticket = new Ticket((POSTicketContract)result.Contract);
                    var fpResult = await FiscalPrinter.PrintRefundAsync(ticket, closedTicket.CreditNoteNumber);
                    if (fpResult.IsEmpty)
                    {
                        // Save data assigned by the fiscal printer
                        ticket.FpCreditNoteNumber = fpResult.DocFiscalNumber;
                        ticket.FpSerialCreditNote = fpResult.FpSerial;
                        result.AddValidations(await BusinessProxyRepository.Ticket.UpdateFiscalPrinterDataAsync(ticket.AsContract()));
                        if (result.IsEmpty)
                            await RefreshData();
                    }
                    else
                        result.AddValidations(fpResult);
                }

                if (!result.IsEmpty)
                    WindowManager.ShowError(result);
            }
            finally
            {
                WindowManager.CloseBusyDialog();
            }
        }

        private void PrintPosDocument(POSTicketContract contract, ClosedTicketOperations.Document document)
        {
            var ticket = new Ticket(contract);
            switch (document)
            {
                case ClosedTicketOperations.Document.Ticket:
                    ShowOldTicket?.Invoke(this, ticket);
                    break;
                case ClosedTicketOperations.Document.Ballot:
                    ShowOldBallot?.Invoke(this, ticket);
                    break;
                case ClosedTicketOperations.Document.Invoice:
                    ShowOldInvoice?.Invoke(this, ticket);
                    break;
                case ClosedTicketOperations.Document.CreditNote:
                    ShowOldCreditNote?.Invoke(this, ticket);
                    break;
                default:
                    throw new InvalidOperationException();
            }
        }

        private async void PrintOldPosDocument(ClosedTicketRecord ticketRecord, ClosedTicketOperations.Document document)
        {
            try
            {
                WindowManager.ShowBusyDialog();
                var result = await BusinessProxyRepository.Ticket.LoadClosedTicketAsync((Guid)ticketRecord.Id, ticketRecord.LocalTicket);
                WindowManager.CloseBusyDialog();
                if (result.IsEmpty)
                {
                    var contract = (POSTicketContract)result.Contract;
                    PrintPosDocument(contract, document);
                }
                else
                    WindowManager.ShowError(result);
            }
            catch (Exception e)
            {
                WindowManager.ShowError(e);
            }
        }

        private async void FpReprint(Document document, long start, long end)
        {
            WindowManager.ShowBusyDialog();
            try
            {
                var rPrint = await FiscalPrinter.ReprintAsync(document, start, end);
                if (!rPrint.IsEmpty)
                    WindowManager.ShowError(rPrint);
            }
            finally
            {
                WindowManager.CloseBusyDialog();
            }
        }
        
        /// <summary>
        /// Fetches the data when the user clicks on the "Closed Tickets" button and constructs/refreshes the table grid
        /// </summary>
        private async Task RefreshData()
        {
            var result = await BusinessProxyRepository.Stand.GetOldTicketsAsync(Stand.Id, Cashier.Id, Date);
            if (result.IsEmpty)
            {
                ClosedTicketsSource = result.ItemSource;
                GenerateGridLayout();
            }
            else WindowManager.ShowError(result);
        }
    
        /// <summary>
        /// Generates the table grid when the user clicks on the "Closed Tickets" button
        /// </summary>
        private void GenerateGridLayout()
        {
            var columns = new List<GridColumnDefinition>
            {
                new (nameof(ClosedTicketRecord.OpeningNumber), "SaleNo".Translate(), 100, null),
                new (nameof(ClosedTicketRecord.Number), "TicketNo".Translate(), 130, null),
                new (nameof(ClosedTicketRecord.StandDescription), "Stand".Translate(), 170, null),
                new (nameof(ClosedTicketRecord.CashierDescription), "Cashier".Translate(), 130, null),
                new (nameof(ClosedTicketRecord.CloseDateTime), "Closed".Translate(), 130, null),
                new (nameof(ClosedTicketRecord.RoomNumber), "Room".Translate(), 50, null),
                new (nameof(ClosedTicketRecord.InvoiceNumber), "InvoiceNo".Translate(LocalizedSuffix.Column), 130, null),
                new (nameof(ClosedTicketRecord.CreditNoteNumber), "CreditNoteNo".Translate(LocalizedSuffix.Column), 130, null)
            };

            if (Settings.AllowBallotDocument)
                columns.Add(new GridColumnDefinition(nameof(ClosedTicketRecord.BallotNumber), "BallotNo".Translate(LocalizedSuffix.Column), 130, null));

            if (Hotel.Instance.HasFiscalPrinter)
            {
                columns.Add(new GridColumnDefinition(nameof(ClosedTicketRecord.FpSerialInvoice), "InvoicePrinter".Translate(), 50, null));
                columns.Add(new GridColumnDefinition(nameof(ClosedTicketRecord.FpInvoiceNumber), "InvoiceNoPrinter".Translate(), 50, null));
                columns.Add(new GridColumnDefinition(nameof(ClosedTicketRecord.FpSerialCreditNote), "CreditNotePrinter".Translate(), 50, null));
                columns.Add(new GridColumnDefinition(nameof(ClosedTicketRecord.FpCreditNoteNumber), "CreditNoteNoPrinter".Translate(), 50, null));
            }

            OldTicketsDataGrid.GenerateColumns("Pos", columns.ToArray());
            if (OldTicketsDataGrid.Columns.Count <= 0) return;

            #region Total Column

            var style = new Style(typeof(TextBlock));
            style.Setters.Add(new Setter(VerticalAlignmentProperty, VerticalAlignment.Center));
            style.Setters.Add(new Setter(HorizontalAlignmentProperty, HorizontalAlignment.Right));

            var totalColumn = new DataGridTextColumn
            {
                ElementStyle = style,
                Header = "Total".Translate(),
                Binding = new Binding("Total"),
                Width = new DataGridLength(1, DataGridLengthUnitType.Star),
                MinWidth = 100,
                IsReadOnly = true
            };

            OldTicketsDataGrid.Columns.Add(totalColumn);

            #endregion

            #region Cancelled Column

            var cancelledColumn = new DataGridTemplateColumn
            {
                Width = new DataGridLength(1, DataGridLengthUnitType.Star),
                CellTemplate = (DataTemplate)OldTicketsDataGrid.FindResource("CancelledColumnTemplate"),
                MinWidth = 30,
                MaxWidth = 30,
                IsReadOnly = true
            };

            OldTicketsDataGrid.Columns.Add(cancelledColumn);

            #endregion
        }

        #endregion

        #endregion
    }
}