﻿using System.Windows;
using System.Windows.Controls;

namespace NewHotel.WPF.App.Pos.Controls.File.CloseDayTurn
{
    /// <summary>
    /// Interaction logic for CloseTurnDetailsCtrl.xaml
    /// </summary>
    public partial class CloseTurnDetailsCtrl : UserControl
    {
        public CloseTurnDetailsCtrl()
        {
            InitializeComponent();
        }

        public bool AllowPrintOnTurnClose
        {
            get => (bool)GetValue(AllowPrintOnTurnCloseProperty);
            set => SetValue(AllowPrintOnTurnCloseProperty, value);
        }

        public static readonly DependencyProperty AllowPrintOnTurnCloseProperty =
            DependencyProperty.Register(nameof(AllowPrintOnTurnCloseProperty), typeof(bool), typeof(CloseDayTurnCtrl), new PropertyMetadata(true));
    }
}
