﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace NewHotel.WPF.App.Pos.Controls.File.CloseDayTurn
{
    /// <summary>
    /// Interaction logic for CloseDayCtrl.xaml
    /// </summary>
    public partial class CloseDayDetailsCtrl : UserControl
    {
        #region Constructors

        public CloseDayDetailsCtrl()
        {
            InitializeComponent();
        }

        #endregion
        #region Properties
        #region AllowPrintOnDayClose

        public bool AllowPrintOnDayClose
        {
            get => (bool)GetValue(AllowPrintOnDayCloseProperty);
            set => SetValue(AllowPrintOnDayCloseProperty, value);
        }

        public static readonly DependencyProperty AllowPrintOnDayCloseProperty =
            DependencyProperty.Register(nameof(AllowPrintOnDayCloseProperty), typeof(bool), typeof(CloseDayTurnCtrl), new PropertyMetadata(true));
        
        #endregion
        #region IsCloseSeveralDays

        public bool IsCloseSeveralDays
        {
            get => (bool)GetValue(IsCloseSeveralDaysProperty);
            set => SetValue(IsCloseSeveralDaysProperty, value);
        }

        public static readonly DependencyProperty IsCloseSeveralDaysProperty =
            DependencyProperty.Register(nameof(IsCloseSeveralDays), typeof(bool), typeof(CloseDayDetailsCtrl), new PropertyMetadata(default(bool)));

        #endregion
        #region CurrentStandWorkDate

        public DateTime CurrentStandWorkDate
        {
            get => (DateTime)GetValue(CurrentStandWorkDateProperty);
            set => SetValue(CurrentStandWorkDateProperty, value);
        }

        public static readonly DependencyProperty CurrentStandWorkDateProperty =
            DependencyProperty.Register(nameof(CurrentStandWorkDate), typeof(DateTime), typeof(CloseDayDetailsCtrl), new PropertyMetadata(default(DateTime)));

        #endregion
        #region CloseDayMinDate

        public DateTime? CloseDayMinDate
        {
            get => (DateTime?)GetValue(CloseDayMinDateProperty);
            set => SetValue(CloseDayMinDateProperty, value);
        }

        public static readonly DependencyProperty CloseDayMinDateProperty =
            DependencyProperty.Register(nameof(CloseDayMinDate), typeof(DateTime?), typeof(CloseDayDetailsCtrl), new PropertyMetadata(default(DateTime)));


        #endregion
        #region CloseDaySelectedDate

        public DateTime? CloseDaySelectedDate
        {
            get => (DateTime?)GetValue(CloseDaySelectedDateProperty);
            set => SetValue(CloseDaySelectedDateProperty, value);
        }

        public static readonly DependencyProperty CloseDaySelectedDateProperty =
            DependencyProperty.Register(nameof(CloseDaySelectedDate), typeof(DateTime?), typeof(CloseDayDetailsCtrl), new PropertyMetadata(default(DateTime)));

        #endregion


        #endregion
        #region Methods

        private void CloseDaySelectedDate_Click(object sender, RoutedEventArgs e)
        {
            CloseDaySelectedDate = null;
        }

        private void NextDaySelectedDate_Click(object sender, RoutedEventArgs e)
        {
            CloseDaySelectedDate = CloseDaySelectedDate?.AddDays(1);
        }

        private void PrevDaySelectedDate_Click(object sender, RoutedEventArgs e)
        {
            if (CloseDaySelectedDate.HasValue)
            {
                if (CloseDayMinDate.HasValue)
                {
                    if (CloseDaySelectedDate.Value > CloseDayMinDate.Value)
                        CloseDaySelectedDate = CloseDaySelectedDate.Value.AddDays(-1);
                }
                else CloseDaySelectedDate = CloseDaySelectedDate.Value.AddDays(-1);
            }
        }

        #endregion
    }
}
