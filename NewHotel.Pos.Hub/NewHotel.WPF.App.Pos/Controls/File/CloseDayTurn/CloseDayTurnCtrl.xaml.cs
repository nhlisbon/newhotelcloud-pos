﻿using System;
using System.Globalization;
using System.Runtime.Remoting.Contexts;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using NewHotel.Contracts;
using NewHotel.Pos.Communication;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Localization;
using NewHotel.Pos.PrinterDocument.Interfaces;
using NewHotel.Pos.PrinterDocument.Simple;
using NewHotel.WPF.App.Pos.Controls.File.Reports;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Model.Extentions;
using NewHotel.WPF.Model.Interface;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls.File.CloseDayTurn
{
    /// <summary>
    /// Interaction logic for CloseDayTurnCtrl.xaml
    /// </summary>
    public partial class CloseDayTurnCtrl : UserControl
    {
        #region Variables

        private CloseDayDetailsCtrl _closeDayCtrl;
        private CloseTurnDetailsCtrl _closeTurnCtrl;
        private readonly IVisualPrinter _visualPrinter;

        #endregion
        #region Constructors

        public CloseDayTurnCtrl()
        {
            InitializeComponent();
            _visualPrinter = CwFactory.Resolve<IVisualPrinter>();
        }

        #endregion
        #region Properties

        public bool AllowCloseDay
        {
            get => (bool)GetValue(AllowCloseDayProperty);
            set => SetValue(AllowCloseDayProperty, value);
        }

        public static readonly DependencyProperty AllowCloseDayProperty =
            DependencyProperty.Register(nameof(AllowCloseDayProperty), typeof(bool), typeof(CloseDayTurnCtrl), new PropertyMetadata(true));

        private IAppSecurity AppSecurity => CwFactory.Resolve<IAppSecurity>();
        private IWindow WindowHelper => CwFactory.Resolve<IWindow>();
        private Stand Stand => Hotel.Instance.CurrentStandCashierContext.Stand;
        private Cashier Cashier => Hotel.Instance.CurrentStandCashierContext.Cashier;

        #endregion
        #region Methods

        private void CloseDayTurnCtrl_OnLoaded(object sender, RoutedEventArgs e)
        {
            UpdateAllowCloseDay();
        } 

        private void CloseDay_Click(object sender, RoutedEventArgs e)
        {
            AppSecurity.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.CloseDay, (isValid, validUser) =>
            {
                if (isValid)
                {
                    var standWorkDate = Stand.StandWorkDate;
                    var closeDay = standWorkDate.AddDays(1);
                    _closeDayCtrl = new CloseDayDetailsCtrl();
                    _closeDayCtrl.CurrentStandWorkDate = standWorkDate;
                    _closeDayCtrl.CloseDaySelectedDate = closeDay;
                    _closeDayCtrl.CloseDayMinDate = closeDay;
                    WindowHelper.OpenAcceptCancelDialog(_closeDayCtrl, ConfirmCloseDay);
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        private void CloseTurn_Click(object sender, RoutedEventArgs e)
        {
            AppSecurity.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.CloseShift, (isValid, validUser) =>
            {
                if (isValid)
                {
                    _closeTurnCtrl = new CloseTurnDetailsCtrl();
                    WindowHelper.OpenAcceptCancelDialog(_closeTurnCtrl, ConfirmCloseTurn);
                } 
                else 
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }

        private void UndoCloseDay_Click(object sender, RoutedEventArgs e)
        {
            AppSecurity.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.CloseDay, (isValid, validUser) =>
            {
                if (isValid)
                {
                    bool OkUndoDailyClose()
                    {
                        bool OkUndoDailyClose2()
                        {
                            UnodCloseDayAsync(Stand.Id, Cashier.Id);
                            return true;
                        }

                        WindowHelper.OpenDialog(new TextBlock() { Text = "AreYouReallySure".Translate(), FontSize = 16 }, OkUndoDailyClose2);
                        return false;
                    }

                    WindowHelper.OpenDialog(new TextBlock() { Text = "AreYouSure".Translate(), FontSize = 16 }, OkUndoDailyClose);
                }
                else
                    WindowHelper.ShowError("UserNotAuthorized".Translate());
            });
        }
        
        /// <summary>
        /// Submit the close day action and print the sales report
        /// </summary>
        /// <param name="_">The button that the method is associated </param>
        /// <returns>True if everything is ok, otherwise returns false</returns>
        private bool ConfirmCloseDay(ButtonBase _)
        {
            var isCloseSeveralDays = _closeDayCtrl.IsCloseSeveralDays;
            var closeDaySelectedDate = _closeDayCtrl.CloseDaySelectedDate;
            if (!closeDaySelectedDate.HasValue) return true;
            
            if (isCloseSeveralDays && closeDaySelectedDate.Value <= Stand.StandWorkDate.Date.ToUniversalTime())
            {
                WindowHelper.ShowError("InvalidSelectedDate".Translate());
                return false;
            }
            try
            {
                // Get the current stand and cashier context
                var context = Hotel.Instance.CurrentStandCashierContext;
                var sales = new PrintSales();
                sales.SetData(context.Stand, context.Cashier);
                if (_closeDayCtrl.AllowPrintOnDayClose)
                {
                    bool PrintReport()
                    {
                        var printer = context.Stand.PrintersConfiguration.GetPrinter();
                        var result = _visualPrinter.PrintVisual(printer, sales, "Printing Sales Report");
                        // If the result is not empty, show the error
                        if (!result.IsEmpty)
                        {
                            WindowHelper.ShowError(result);
                            return false;
                        }
                        CloseDayAsync(Stand.Id, Cashier.Id, closeDaySelectedDate.Value);
                        return true;
                    }
                    WindowHelper.OpenDialog(sales, true, null, PrintReport, new Point());
                }
                else
                {
                    CloseDayAsync(Stand.Id, Cashier.Id, closeDaySelectedDate.Value);
                    return true;
                } 
            }
            catch (Exception ex)
            {
                WindowHelper.ShowError(ex.Message);
            }

            return !_closeDayCtrl.AllowPrintOnDayClose;
        }

        private async void CloseDayAsync(Guid standId, Guid cashierId, DateTime date)
        {
            WindowHelper.ShowBusyDialog();
            try
            {
                var result = await BusinessProxyRepository.Stand.CloseDayAsync(standId, cashierId, date);
                if (result.IsEmpty)
                {
                    Stand.UpdateTurnAndDate((StandTurnDateContract)result.Contract);
                    var currentWorkDay = Stand.StandWorkDate;
                    var message = string.Format(
                        "WordDateChangedFrom0To1".Translate(),
                        currentWorkDay.AddDays(-1).ToString("dd/MM/yyyy", CultureInfo.CurrentUICulture),
                        currentWorkDay.ToString("dd/MM/yyyy", CultureInfo.CurrentUICulture)
                    );
                    WindowHelper.ShowInfo(message, "Information".Translate());
                }
                else
                {
                    WindowHelper.ShowError(result);
                }
                UpdateAllowCloseDay();
            }
            catch(Exception ex)
            {
                WindowHelper.ShowError(ex.Message);
                WindowHelper.CloseBusyDialog();
            }
            WindowHelper.CloseBusyDialog();
        }

        private bool ConfirmCloseTurn(ButtonBase _)
        {
            WindowHelper.ShowBusyDialog();
            try
            {
                var context = Hotel.Instance.CurrentStandCashierContext;
                var sales = new PrintSales();
                sales.SetData(context.Stand, context.Cashier, true);
                if (_closeTurnCtrl.AllowPrintOnTurnClose)
                {
                    bool PrintReport()
                    {
                        var printer = context.Stand.PrintersConfiguration.GetPrinter();
                        var result = _visualPrinter.PrintVisual(printer, sales, "Printing Sales Report");
               
                        if (!result.IsEmpty)
                        {
                            WindowHelper.ShowError(result);
                            return false;
                        }
                        CloseTurnAsync();
                        return true;
                    }
                    WindowHelper.OpenDialog(sales, true, null, PrintReport, new Point());
                }
                else
                {
                    CloseTurnAsync();
                    WindowHelper.CloseBusyDialog();
                    return true;
                } 
            }
            finally { WindowHelper.CloseBusyDialog(); }

            return !_closeTurnCtrl.AllowPrintOnTurnClose;
        }

        private async void CloseTurnAsync()
        {
            ShowBusy("Closing");
            try
            {
                var previousTurn = Stand.Shift;
                var result = await BusinessProxyRepository.Stand.CloseTurnAsync(Stand.Id, Cashier.Id);
                if (result.IsEmpty)
                {
                    Stand.UpdateTurnAndDate((StandTurnDateContract)result.Contract);
                    var message = "TurnClosed".Translate() + $": {previousTurn} => {Stand.Shift}";
                    WindowHelper.ShowInfo(message, "Information".Translate());
                }
                else
                    WindowHelper.ShowError(result);
            }
            finally
            {
                CloseBusy();
            }
        }

        private async void UnodCloseDayAsync(Guid standId, Guid cashierId)
        {
            ShowBusy();
            try
            {
                var result = await BusinessProxyRepository.Stand.UndoCloseDayAsync(standId, cashierId);
                if (result.IsEmpty && result.Date.HasValue)
                {
                    var contract = new StandTurnDateContract() { Turn = 1, WorkDate = result.Date.Value };
                    Hotel.Instance.CurrentStandCashierContext.Stand.UpdateTurnAndDate(contract);
                }
                else
                    WindowHelper.ShowError(result);
            }
            finally
            {
                CloseBusy();
            }
        }

        private void ShowBusy(string messageId = "Loading")
        {
            IsEnabled = false;
            WindowHelper.ShowBusyDialog(messageId);
        }

        private void CloseBusy()
        {
            IsEnabled = true;
            WindowHelper.CloseBusyDialog();
        }

        private void UpdateAllowCloseDay()
        {
            if (!Hotel.Instance.WorkDateOverSystemDate && Stand.StandWorkDate > DateTime.Today)
                AllowCloseDay = false;
        }

        #endregion
    }
}