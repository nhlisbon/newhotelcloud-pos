﻿using System.Windows;
using System.Windows.Controls;
using NewHotel.Pos.IoC;
using NewHotel.Pos.PrinterDocument.Interfaces;
using NewHotel.Pos.PrinterDocument.Simple;
using NewHotel.WPF.App.Pos.Controls.File.Reports;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Model.Extentions;
using NewHotel.WPF.Model.Interface;
using NewHotel.WPF.Model.Model;
using ValidationResult = NewHotel.Contracts.ValidationResult;

namespace NewHotel.WPF.App.Pos.Controls.File
{
    /// <summary>
    /// Interaction logic for ReportsView.xaml
    /// </summary>
    public partial class ReportsView : UserControl
    {
        #region Variables

        private readonly IWindow _window;
        private readonly IVisualPrinter _visualPrinter;

        #endregion
        #region Constructor

        public ReportsView()
        {
            InitializeComponent();
            _window = CwFactory.Resolve<IWindow>();
            _visualPrinter = CwFactory.Resolve<IVisualPrinter>();
        }

        #endregion 
        #region Methods

        /// <summary>
        /// Method to print the sales report (all turns of the day)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SalesReport_Click(object sender, RoutedEventArgs e)
        {
            _window.ShowBusyDialog();

            try
            {
                var context = Hotel.Instance.CurrentStandCashierContext;
                var sales = new PrintSales();
                sales.SetData(context.Stand, context.Cashier);

                bool PrintReport()
                {
                    var printer = context.Stand.PrintersConfiguration.GetPrinter();
                    var result = _visualPrinter.PrintVisual(printer, sales, "Printing Sales Report");
                    // If the result is not empty, show the error
                    if (!result.IsEmpty) _window.ShowError(result);
                    return true;
                }

                _window.OpenDialog(sales, true, null, PrintReport, new Point());
            }
            finally
            {
                _window.CloseBusyDialog();
            }
        }

        private void PaymentsReport_Click(object sender, RoutedEventArgs e)
        {
            _window.ShowBusyDialog();

            try
            {
                var paymentForms = new Reports.PrintPaymentForms();
                paymentForms.Initialize(ChkJustDrawer, ChkGroupByUsers, ChkGroupByShift, ChkSplitPayments, ChkGroupByPayment, ChkGroupByCurrency);

                bool PrintReport()
                {
                    Stand stand = Hotel.Instance.CurrentStandCashierContext.Stand;
                    Printer printer = stand.PrintersConfiguration?.GetPrinter();
                    ValidationResult result = _visualPrinter.PrintVisual(printer, paymentForms, "Printing Payments Report");
                    if (!result.IsEmpty)
                        _window.ShowError(result);
                    return true;
                }

                _window.OpenDialog(paymentForms, true, null, PrintReport, new Point());
            }
            finally
            {
                 _window.CloseBusyDialog();
            }
        }

        private void OpenTicketsReport_Click(object sender, RoutedEventArgs e)
        {
            _window.ShowBusyDialog();
            try
            {
                var context = Hotel.Instance.CurrentStandCashierContext;
                var openTickets = new PrintOpenTickets();
                openTickets.SetData(context.Stand, context.Cashier);

                bool PrintReport()
                {
                    var printSettings = Hotel.Instance.CurrentStandCashierContext.Stand.PrintersConfiguration;
                    Printer printer = printSettings?.GetPrinter();
                    ValidationResult result = _visualPrinter.PrintVisual(printer, openTickets, "Printing  Open Tickets Report");
                    if (!result.IsEmpty)
                        _window.ShowError(result);
                    return true;
                };

                _window.OpenDialog(openTickets, true, null, PrintReport, new Point());
            }
            finally
            {
                 _window.CloseBusyDialog();
            }
        }

        private void ChkGroupByPayment_CheckChanged(object sender, RoutedEventArgs e)
        {
            if (ChkGroupByPayment.IsChecked.GetValueOrDefault())
                ChkGroupByCurrency.IsChecked = true;
        }

        #endregion

        private async void FiscalPrinterReport_Click(object sender, RoutedEventArgs e)
        {
            await FiscalPrinterReport.PrintReport();
        }
    }
}
