﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Shapes;
using NewHotel.Pos.PrinterDocument.Simple;
using NewHotel.Pos.PrinterDocument.Simple.Visual;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for PrintTicket.xaml
    /// </summary>
    public partial class PrintOpenTickets : UserControl, IPreparer<Visual>
    {
        public PrintOpenTickets()
        {
            InitializeComponent();
        }

        public Stand CurrentStand
        {
            get { return (Stand)GetValue(CurrentStandProperty); }
            set { SetValue(CurrentStandProperty, value); }
        }

        public static readonly DependencyProperty CurrentStandProperty =
            DependencyProperty.Register("CurrentStand", typeof(Stand), typeof(PrintOpenTickets));

        public Cashier CurrentCashier
        {
            get { return (Cashier)GetValue(CurrentCashierProperty); }
            set { SetValue(CurrentCashierProperty, value); }
        }

        public static readonly DependencyProperty CurrentCashierProperty =
            DependencyProperty.Register("CurrentCashier", typeof(Cashier), typeof(PrintOpenTickets));

        public DateTime WorkDate
        {
            get { return (DateTime)GetValue(WorkDateProperty); }
            set { SetValue(WorkDateProperty, value); }
        }

        public static readonly DependencyProperty WorkDateProperty =
            DependencyProperty.Register("WorkDate", typeof(DateTime), typeof(PrintOpenTickets));

        public DateTime ReportDate
        {
            get { return (DateTime)GetValue(ReportDateProperty); }
            set { SetValue(ReportDateProperty, value); }
        }

        public static readonly DependencyProperty ReportDateProperty =
            DependencyProperty.Register("ReportDate", typeof(DateTime), typeof(PrintOpenTickets));

        private Thickness CreateThickness(string margin)
        {
            var converter = new ThicknessConverter();
            return (Thickness)converter.ConvertFromString(null, Hotel.DefaultCultureInfo, margin);
        }

        public void SetData(Stand stand, Cashier cashier)
        {
            mainStackPanel.Children.Clear();

            CurrentStand = stand;
            CurrentCashier = cashier;
            WorkDate = stand.StandWorkDate;
            ReportDate = DateTime.Now;

            var items = stand.Tickets.Where(x => !x.CloseDate.HasValue).GroupBy(x => x.Shift);

            var totalImporte = decimal.Zero;
            var auxImporte = decimal.Zero;
            Dictionary<Guid, Table> tables = new Dictionary<Guid, Table>();

            foreach (var item in items)
            {
                var familyGrid = new UniformGrid() { Columns = 1, Margin = CreateThickness("0.125cm") };
                familyGrid.Children.Add(new TextBlock()
                { 
                    Text = $"Shift {item.Key}",
                    FontWeight = FontWeights.Bold,
                    FontSize = 14
                });

                auxImporte = decimal.Zero;
                foreach (var ticket in item)
                {
                    auxImporte += ticket.TotalAmount;
                    var auxGrid = new UniformGrid() { Columns = 4 };

                    // stand.FindTable(Guid.Empty);
                    if (ticket.TableId.HasValue && !tables.TryGetValue(ticket.TableId.Value, out var table))
                    {
                        table = CurrentStand.FindTable(ticket.TableId.Value);
                        tables[ticket.TableId.Value] = table;
                    }
                    else
                        table = null;

                    auxGrid.Children.Add(new TextBlock()
                    {
                        // Text = ticket.Table == null ? string.Empty : ticket.Table.Saloon,
                        Text = table == null ? string.Empty : table.Saloon,
                        HorizontalAlignment = HorizontalAlignment.Left
                    });

                    auxGrid.Children.Add(new TextBlock()
                    {
                        // Text = ticket.Table == null ? string.Empty : ticket.Table.Name,
                        Text = table == null ? string.Empty : table.Name,
                        HorizontalAlignment = HorizontalAlignment.Left
                    });

                    auxGrid.Children.Add(new TextBlock()
                    {
                        Text = $"{ticket.SeriePrex}/{ticket.Serie}",
                        HorizontalAlignment = HorizontalAlignment.Left
                    });

                    auxGrid.Children.Add(new TextBlock()
                    {
                        Text = ticket.TotalAmount.ToString("C2", Hotel.Culture),
                        HorizontalAlignment = HorizontalAlignment.Right,
                        Margin = CreateThickness("0, 0, 0.25cm, 0")
                    });

                    familyGrid.Children.Add(auxGrid);
                }

                totalImporte += auxImporte;

                familyGrid.Children.Add(new TextBlock()
                {
                    Text = string.Format(Hotel.Culture, "Shift {0} Total: {1:C2}", item.Key, auxImporte),
                    FontWeight = FontWeights.SemiBold,
                    HorizontalAlignment = HorizontalAlignment.Right,
                    Margin = CreateThickness("0, 0, 0.25cm, 0")
                });

                mainStackPanel.Children.Add(familyGrid);
            }

            mainStackPanel.Children.Add(new Rectangle()
            {
                Margin = CreateThickness("0, 0.25cm, 0, 0")
            });

            mainStackPanel.Children.Add(new TextBlock()
            {
                Text = string.Format(Hotel.Culture, "Total: {0:C2}", totalImporte),
                FontWeight = FontWeights.Bold,
                FontSize = 16,
                HorizontalAlignment = HorizontalAlignment.Right,
                Margin = CreateThickness("0, 0, 0.25cm, 0")
            });

            mainStackPanel.Children.Add(new Rectangle());
            mainStackPanel.Children.Add(new TextBlock()
            {
                Text = "NewHotel Software",
                HorizontalAlignment = HorizontalAlignment.Right,
                Margin = CreateThickness("0, 0, 0.25cm, 0")
            });
        }

        public Visual PreparePrint(PrinterContext context)
        {
            return Scroll.PreparePrintScroll(context);
        }
    }
}