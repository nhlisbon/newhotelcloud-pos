﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Hub.Contracts.Common.Records.Tickets;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Localization;
using NewHotel.Pos.PrinterDocument.Simple;
using NewHotel.Pos.PrinterDocument.Simple.Visual;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Model.Interface;
using NewHotel.WPF.Model.Model;
using ColumnDefinition = System.Windows.Controls.ColumnDefinition;

namespace NewHotel.WPF.App.Pos.Controls.File.Reports
{
    /// <summary>
    /// Interaction logic for PrintTicket.xaml
    /// </summary>
    public partial class PrintPaymentForms : UserControl, IPreparer<Visual>
    {
        #region Contructors

        public PrintPaymentForms()
        {
            InitializeComponent();
        }

        #endregion
        #region Properties

        #region CurrentStand

        public Stand CurrentStand
        {
            get => (Stand)GetValue(CurrentStandProperty);
            set => SetValue(CurrentStandProperty, value);
        }

        public static readonly DependencyProperty CurrentStandProperty =
            DependencyProperty.Register(nameof(CurrentStand), typeof(Stand), typeof(PrintPaymentForms));

        #endregion
        #region CurrentCashier

        public Cashier CurrentCashier
        {
            get => (Cashier)GetValue(CurrentCashierProperty);
            set => SetValue(CurrentCashierProperty, value);
        }

        public static readonly DependencyProperty CurrentCashierProperty =
            DependencyProperty.Register(nameof(CurrentCashier), typeof(Cashier), typeof(PrintPaymentForms));

        #endregion
        #region WordDate

        public DateTime WorkDate
        {
            get => (DateTime)GetValue(WorkDateProperty);
            set => SetValue(WorkDateProperty, value);
        }

        public static readonly DependencyProperty WorkDateProperty =
            DependencyProperty.Register(nameof(WorkDate), typeof(DateTime), typeof(PrintPaymentForms));

        #endregion
        #region ReportDate

        public DateTime ReportDate
        {
            get => (DateTime)GetValue(ReportDateProperty);
            set => SetValue(ReportDateProperty, value);
        }

        public static readonly DependencyProperty ReportDateProperty =
            DependencyProperty.Register(nameof(ReportDate), typeof(DateTime), typeof(PrintPaymentForms));

        #endregion
        #region TotalPaxs

        public long TotalPaxs
        {
            get => (long)GetValue(TotalPaxsProperty);
            set => SetValue(TotalPaxsProperty, value);
        }

        public static readonly DependencyProperty TotalPaxsProperty =
            DependencyProperty.Register(nameof(TotalPaxs), typeof(long), typeof(PrintPaymentForms));

        #endregion

        #endregion
        #region Methods

        #region Public

        public Visual PreparePrint(PrinterContext context)
        {
            return Scroll.PreparePrintScroll(context);
        }

        public async void Initialize(CheckBox cbxJustDrawer, CheckBox cbxGroupUsers, CheckBox cbxGroupShift, CheckBox cbxSplitPayments, CheckBox cbxGroupPayments, CheckBox cbxGroupCurrency)
        {
            ContainterView.Children.Clear();

            var context = Hotel.Instance.CurrentStandCashierContext;
            CurrentStand = context.Stand;
            CurrentCashier = context.Cashier;
            WorkDate = CurrentStand.StandWorkDate;           
            ReportDate = DateTime.Now;

            var sb = new StringBuilder();

            var justDrawer = cbxJustDrawer.IsChecked ?? false;
            if (justDrawer)
                sb.AppendLine(LocalizationMgr.Translation(cbxJustDrawer.Content.ToString()));

            var groupUsers = cbxGroupUsers.IsChecked ?? false;
            if (groupUsers)
                sb.AppendLine(LocalizationMgr.Translation(cbxGroupUsers.Content.ToString()));

            var groupShift = cbxGroupShift.IsChecked ?? false;
            if (groupShift)
                sb.AppendLine(LocalizationMgr.Translation(cbxGroupShift.Content.ToString()));

            var splitPayments = cbxSplitPayments.IsChecked ?? false;
            if (splitPayments)
                sb.AppendLine(LocalizationMgr.Translation(cbxSplitPayments.Content.ToString()));

            var groupPayments = cbxGroupPayments.IsChecked ?? false;
            if (groupPayments)
                sb.AppendLine(LocalizationMgr.Translation(cbxGroupPayments.Content.ToString()));

            var groupCurrency = cbxGroupCurrency.IsChecked ?? false;
            if (groupCurrency)
                sb.AppendLine(LocalizationMgr.Translation(cbxGroupCurrency.Content.ToString()));

            filters.Text = sb.ToString();

            var result = await LoadData(justDrawer);
            if (result.IsEmpty)
            {
                var sales = result.ItemSource;

                foreach (var sale in sales)
                {
                    if (sale.CancelledTicket)
                    {
                        if (!string.IsNullOrEmpty(sale.PaymentDescription))
                            sale.PaymentDescription += " *";
                        else if (!string.IsNullOrEmpty(sale.CreditCardDescription))
                            sale.CreditCardDescription += " *";
                    }
                }

                var settings = CwFactory.Resolve<IPosSettings>();
                var showCurrency = settings.MultCurrency || sales.Select(x => x.CurrencyIdPayment).Distinct().Any();

                ImportCurrencyContainer totalImport;
                if (groupUsers && groupShift)
                    totalImport = GroupUserAndShift(sales, groupPayments, groupCurrency, splitPayments, showCurrency);
                else if (groupUsers)
                    totalImport = GroupUser(sales, groupPayments, groupCurrency, splitPayments, showCurrency);
                else if (groupShift)
                    totalImport = GroupShift(sales, groupPayments, groupCurrency, splitPayments, showCurrency);
                else
                    totalImport = FullDetails(sales, groupPayments, groupCurrency, splitPayments, showCurrency);

                ContainterView.Children.Add(new Rectangle { Margin = CreateThickness("0, 0.25cm, 0, 0") });
                AddTotalByCurrencyId(ContainterView, totalImport, FontWeights.Bold, CreateThickness("0.125cm, 0.125cm, 0, 0"), showCurrency, 16);
                ContainterView.Children.Add(new Rectangle());
                ContainterView.Children.Add(new TextBlock { Text = "NewHotel Software", HorizontalAlignment = HorizontalAlignment.Right });
            }
            else
                ErrorsWindow.ShowErrorWindows(result);
        }

        #endregion
        #region Private

        private Grid CreatePaymentGrid()
        {
            var grid = new Grid();
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star), MaxWidth = CreateLength("18cm") });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
            return grid;
        }

        private async Task<ValidationItemSource<SalePaymentRecord>> LoadData(bool justDrawer)
        {
            var qr = new QueryRequest();
            qr.AddParameter("ipos_pk", CurrentStand.Id);
            qr.AddParameter("caja_pk", CurrentCashier.Id);
            qr.AddParameter("vend_datf", CurrentStand.StandWorkDate.Ticks);
            if (justDrawer)
                qr.AddFilter("tire_pk", (long)ReceivableType.Payment, FilterTypes.Equal);

            return await BusinessProxyRepository.Stand.GetPaymentsReportAsync(qr);
        }

        private ImportCurrencyContainer FullDetails(IEnumerable<SalePaymentRecord> source,
            bool groupPayments, bool groupCurrency, bool splitPayments, bool showCurrency)
        {
            ImportCurrencyContainer generalImport;
            if (!groupPayments && groupCurrency)
                generalImport = GroupCurrency(ContainterView, source, splitPayments);
            else
            {
                generalImport = new ImportCurrencyContainer();
                var payments = groupPayments ? GroupPaymentType(source, splitPayments) : source;

                var view = CreatePaymentGrid();
                foreach (var payment in payments.Where(p => p.Value != decimal.Zero))
                {
                    var viewPayment = ViewPayment(view, payment, splitPayments, showCurrency, groupPayments);
                    generalImport.IncImport(payment.CurrencyIdPayment, payment.ValueExchange);
                }

                ContainterView.Children.Add(view);
            }

            return generalImport;
        }

        private ImportCurrencyContainer GroupShift(IEnumerable<SalePaymentRecord> source,
            bool groupPayments, bool groupCurrency, bool splitPayments, bool showCurrency)
        {
            var generalImport = new ImportCurrencyContainer();

            foreach (var shiftGroup in source.GroupBy(x => x.Shift))
            {
                var shiftFamilyView = BeginFamilyPayments(shiftGroup.Key.ToString());

                ImportCurrencyContainer shiftImport;
                if (!groupPayments && groupCurrency)
                    shiftImport = GroupCurrency(shiftFamilyView, shiftGroup, splitPayments);
                else
                {
                    shiftImport = new ImportCurrencyContainer();
                    var payments = groupPayments ? GroupPaymentType(shiftGroup, splitPayments) : shiftGroup;

                    var view = CreatePaymentGrid();
                    foreach (var payment in payments.Where(p => p.Value != decimal.Zero))
                    {
                        var viewPayment = ViewPayment(view, payment, splitPayments, showCurrency, groupPayments);
                        shiftImport.IncImport(payment.CurrencyIdPayment, payment.ValueExchange);
                    }

                    shiftFamilyView.Children.Add(view);
                }

                ContainterView.Children.Add(EndFamilyPayments(shiftFamilyView, shiftImport, showCurrency));
                generalImport.Join(shiftImport);
            }

            return generalImport;
        }

        private ImportCurrencyContainer GroupUser(IEnumerable<SalePaymentRecord> source,
            bool groupPayments, bool groupCurrency, bool splitPayments, bool showCurrency)
        {
            var generalImport = new ImportCurrencyContainer();

            foreach (var userGroup in source.GroupBy(x => x.Username))
            {
                var userFamilyView = BeginFamilyPayments(userGroup.Key);

                ImportCurrencyContainer userImport;
                if (!groupPayments && groupCurrency)
                    userImport = GroupCurrency(userFamilyView, userGroup, splitPayments);
                else
                {
                    userImport = new ImportCurrencyContainer();
                    var payments = groupPayments ? GroupPaymentType(userGroup, splitPayments) : userGroup;

                    var view = CreatePaymentGrid();
                    foreach (var payment in payments.Where(p => p.Value != decimal.Zero))
                    {
                        var viewPayment = ViewPayment(view, payment, splitPayments, showCurrency, groupPayments);
                        userImport.IncImport(payment.CurrencyIdPayment, payment.ValueExchange);
                    }

                    userFamilyView.Children.Add(view);
                }

                ContainterView.Children.Add(EndFamilyPayments(userFamilyView, userImport, showCurrency));
                generalImport.Join(userImport);
            }

            return generalImport;
        }

        private ImportCurrencyContainer GroupUserAndShift(IEnumerable<SalePaymentRecord> source, bool groupPayments, bool groupCurrency, bool splitPayments, bool showCurrency)
        {
            var generalImport = new ImportCurrencyContainer();

            foreach (var userGroup in source.GroupBy(x => x.Username).OrderBy(x => x.Key))
            {
                var userImport = new ImportCurrencyContainer();
                var userFamilyView = BeginFamilyPayments(userGroup.Key);

                foreach (var shiftGroup in userGroup.GroupBy(x => x.Shift).OrderBy(x => x.Key))
                {
                    var shiftFamilyView = BeginFamilyPayments(shiftGroup.Key.ToString());

                    ImportCurrencyContainer shiftImport;
                    if (!groupPayments && groupCurrency)
                        shiftImport = GroupCurrency(shiftFamilyView, shiftGroup, splitPayments);
                    else
                    {
                        shiftImport = new ImportCurrencyContainer();
                        var payments = groupPayments ? GroupPaymentType(shiftGroup, splitPayments) : shiftGroup;

                        var view = CreatePaymentGrid();
                        foreach (var payment in payments.Where(p => p.Value != decimal.Zero))
                        {
                            var viewPayment = ViewPayment(view, payment, splitPayments, showCurrency, groupPayments);
                            shiftImport.IncImport(payment.CurrencyIdPayment, payment.ValueExchange);
                        }

                        shiftFamilyView.Children.Add(view);
                    }

                    userFamilyView.Children.Add(EndFamilyPayments(shiftFamilyView, shiftImport, showCurrency));
                    userImport.Join(shiftImport);
                }

                ContainterView.Children.Add(EndFamilyPayments(userFamilyView, userImport, showCurrency));
                generalImport.Join(userImport);
            }

            return generalImport;
        }

        private ImportCurrencyContainer GroupCurrency(StackPanel container,
            IEnumerable<SalePaymentRecord> source, bool splitPayments)
        {
            var imports = new ImportCurrencyContainer();
            foreach (var payments in source.GroupBy(x => x.CurrencyIdPayment))
            {
                var currencyImports = new ImportCurrencyContainer();
                var currencyFamilyView = BeginFamilyPayments(payments.Key);

                var view = CreatePaymentGrid();
                foreach (var payment in payments.Where(p => p.Value != decimal.Zero))
                {
                    var viewPayment = ViewPayment(view, payment, splitPayments, false, false);
                    currencyImports.IncImport(payments.Key, payment.ValueExchange);
                }

                currencyFamilyView.Children.Add(view);

                container.Children.Add(EndFamilyPayments(currencyFamilyView, currencyImports, false));
                imports.Join(currencyImports);
            }

            return imports;
        }

        private StackPanel BeginFamilyPayments(string header)
        {
            var familyGrid = new StackPanel { Margin = CreateThickness("0.125cm") };
            familyGrid.Children.Add(new TextBlock { Text = header, FontWeight = FontWeights.Bold, FontSize = 14 });
            return familyGrid;
        }

        private StackPanel EndFamilyPayments(StackPanel familyGrid, ImportCurrencyContainer imports, bool showCurrency)
        {
            var fontWeigth = FontWeights.SemiBold;
            AddTotalByCurrencyId(familyGrid, imports, fontWeigth, CreateThickness("0.125cm, 0.125cm, 0, 0"), showCurrency);
            return familyGrid;
        }

        private StackPanel AddTotalByCurrencyId(StackPanel container, ImportCurrencyContainer imports,
            FontWeight fontWeight, Thickness margin, bool showCurrency, double? fontSize = null)
        {
            var grid = new Grid();
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });

            foreach (var import in imports.Imports)
            {
                var total = new TextBlock
                {
                    Text = "Total".Translate(),
                    FontWeight = fontWeight,
                    HorizontalAlignment = HorizontalAlignment.Right,
                    Margin = margin,
                    FontSize = fontSize ?? FontSize
                };

                Grid.SetRow(total, grid.RowDefinitions.Count);
                Grid.SetColumn(total, 0);
                grid.Children.Add(total);

                var value = string.Format(Hotel.Culture, "{0:C2}", import.Import);

                var tbValue = new TextBlock
                {
                    Text = value,
                    FontWeight = fontWeight,
                    Margin = margin,
                    FontSize = fontSize ?? FontSize,
                    HorizontalAlignment = HorizontalAlignment.Right
                };

                Grid.SetRow(tbValue, grid.RowDefinitions.Count);
                Grid.SetColumn(tbValue, 1);
                grid.Children.Add(tbValue);

                if (showCurrency)
                {
                    var tbCurrency = new TextBlock
                    {
                        Text = import.CurrencyId,
                        FontWeight = fontWeight,
                        Margin = margin,
                        FontSize = fontSize ?? FontSize
                    };

                    Grid.SetRow(tbCurrency, grid.RowDefinitions.Count);
                    Grid.SetColumn(tbCurrency, 2);
                    grid.Children.Add(tbCurrency);
                }

                grid.RowDefinitions.Add(new RowDefinition());
            }

            container.Children.Add(grid);
            return container;
        }

        private UIElement ViewPayment(Grid view, SalePaymentRecord payment, bool splitPayments, bool showCurrency, bool showCount)
        {
            var row = view.RowDefinitions.Count;
            view.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

            var tbPayment = new TextBlock
            {
                Text = PaymentDescription(splitPayments, payment),
                HorizontalAlignment = HorizontalAlignment.Left,
                TextWrapping = TextWrapping.Wrap
            };

            Grid.SetColumn(tbPayment, 0);
            Grid.SetRow(tbPayment, row);
            view.Children.Add(tbPayment);

            if (showCount)
            {
                var tbCount = new TextBlock
                {
                    Text = payment.Count.ToString(),
                    HorizontalAlignment = HorizontalAlignment.Right,
                    Margin = CreateThickness("0.25cm, 0, 0, 0"),
                    TextWrapping = TextWrapping.NoWrap
                };

                Grid.SetColumn(tbCount, 1);
                Grid.SetRow(tbCount, row);
                view.Children.Add(tbCount);
            }

            var tbValue = new TextBlock
            {
                Text = payment.ValueExchange.ToString("C2", Hotel.Culture),
                HorizontalAlignment = HorizontalAlignment.Right,
                Margin = CreateThickness("0.25cm, 0, 0, 0"),
                FontStyle = payment.ValueExchange < decimal.Zero ? FontStyles.Italic : FontStyles.Normal
            };

            Grid.SetColumn(tbValue, 2);
            Grid.SetRow(tbValue, row);
            view.Children.Add(tbValue);

            if (showCurrency)
            {
                var tbCurrency = new TextBlock
                {
                    Text = payment.CurrencyIdPayment
                };

                Grid.SetColumn(tbCurrency, 3);
                Grid.SetRow(tbCurrency, row);
                view.Children.Add(tbCurrency);
            }

            return view;
        }

        private (string Description, string CurrencyIdPayment) GroupFilter(bool splitPayments, SalePaymentRecord s)
        {
            return splitPayments
                ? (s.PaymentDescriptionDetailed, s.CurrencyIdPayment)
                : (s.PaymentTypeName, s.CurrencyIdPayment);
        }

        private string PaymentDescription(SalePaymentRecord record)
        {
            var description = record.PaymentTypeName;
            if (string.IsNullOrEmpty(description))
                description = record.PaymentDescription;
            if (record.ReceivableType == ReceivableType.Credit || record.ReceivableType == ReceivableType.RoomPlan)
                description += $"-{record.PaymentDescription.Replace('\n', '-')}";
            return description?.Trim('-');
        }

        private string PaymentDescription(bool splitPayments, SalePaymentRecord record)
        {
            var creditCardDescription = record.CreditCardDescription;
            var credit = !string.IsNullOrEmpty(creditCardDescription) ? creditCardDescription : null;
            var description = splitPayments ? credit ?? PaymentDescription(record) : PaymentDescription(record);

            if (!string.IsNullOrEmpty(record.TicketDescription))
                description += $" {record.TicketDescription}";

            if (!string.IsNullOrEmpty(record.InvoiceDescription))
                description += $" -> {record.InvoiceDescription}";

            if (!string.IsNullOrEmpty(record.CreditNoteDescription))
                description += $" -> {record.CreditNoteDescription}";

            return description;
        }

        private IEnumerable<SalePaymentRecord> GroupPaymentType(IEnumerable<SalePaymentRecord> source, bool splitPayments)
        {
            return from s in source
                   group s by GroupFilter(splitPayments, s) into g
                   select new SalePaymentRecord
                   {
                       PaymentDescription = g.Key.Description,
                       CreditCardDescription = g.Key.Description,
                       Count = g.Count(),
                       Value = g.Sum(x => x.Value),
                       CurrencyIdPayment = g.Key.CurrencyIdPayment,
                       ValueExchange = g.Sum(x => x.ValueExchange)
                   };
        }

        private Thickness CreateThickness(string margin)
        {
            var converter = new ThicknessConverter();
            return (Thickness)converter.ConvertFromString(null, Hotel.DefaultCultureInfo, margin);
        }

        private double CreateLength(string length)
        {
            var converter = new LengthConverter();
            return (double)converter.ConvertFromString(null, Hotel.DefaultCultureInfo, length);
        }

        #endregion

        #endregion
        #region Class

        private class ImportCurrencyContainer
        {
            #region Members

            private readonly Dictionary<string, decimal> _importByCurrency = new Dictionary<string, decimal>();

            public IEnumerable<(string CurrencyId, decimal Import)> Imports => _importByCurrency.Select(x => (x.Key, x.Value));

            public void IncImport(string currencyId, decimal value)
            {
                currencyId ??= string.Empty;
                if (!_importByCurrency.TryGetValue(currencyId, out decimal import))
                    import = decimal.Zero;
                import += value;
                _importByCurrency[currencyId] = import;
            }

            public void Join(ImportCurrencyContainer import)
            {
                foreach (var tuple in import.Imports)
                    IncImport(tuple.CurrencyId, tuple.Import);
            }

            #endregion
        }

        #endregion 
    }
}