﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Shapes;
using NewHotel.Contracts;
using NewHotel.Pos.PrinterDocument.Simple;
using NewHotel.Pos.PrinterDocument.Simple.Visual;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls.File.Reports
{
    /// <summary>
    /// Interaction logic for PrintTicket.xaml
    /// </summary>
    public partial class PrintSales : UserControl, IPreparer<Visual>
    {
        public PrintSales()
        {
            InitializeComponent();
        }

        public Stand CurrentStand
        {
            get { return (Stand)GetValue(CurrentStandProperty); }
            set { SetValue(CurrentStandProperty, value); }
        }

        public static readonly DependencyProperty CurrentStandProperty =
            DependencyProperty.Register("CurrentStand", typeof(Stand), typeof(PrintSales));

        public Cashier CurrentCashier
        {
            get { return (Cashier)GetValue(CurrentCashierProperty); }
            set { SetValue(CurrentCashierProperty, value); }
        }

        public static readonly DependencyProperty CurrentCashierProperty =
            DependencyProperty.Register("CurrentCashier", typeof(Cashier), typeof(PrintSales));

        public DateTime WorkDate
        {
            get { return (DateTime)GetValue(WorkDateProperty); }
            set { SetValue(WorkDateProperty, value); }
        }

        public static readonly DependencyProperty WorkDateProperty =
            DependencyProperty.Register("WorkDate", typeof(DateTime), typeof(PrintSales));

        public DateTime ReportDate
        {
            get { return (DateTime)GetValue(ReportDateProperty); }
            set { SetValue(ReportDateProperty, value); }
        }

        public static readonly DependencyProperty ReportDateProperty =
            DependencyProperty.Register("ReportDate", typeof(DateTime), typeof(PrintSales));

        public short TotalPaxs
        {
            get { return (short)GetValue(TotalPaxsProperty); }
            set { SetValue(TotalPaxsProperty, value); }
        }

        public static readonly DependencyProperty TotalPaxsProperty =
            DependencyProperty.Register("TotalPaxs", typeof(short), typeof(PrintSales));

        private Thickness CreateThickness(string margin)
        {
            var converter = new ThicknessConverter();
            return (Thickness)converter.ConvertFromString(null, Hotel.DefaultCultureInfo, margin);
        }

        public void SetData(Stand stand, Cashier cashier, bool? isTurnReport = false)
        {
            mainStackPanel.Children.Clear();

            CurrentStand = stand;
            CurrentCashier = cashier;
            WorkDate = stand.StandWorkDate;
            ReportDate = DateTime.Now;

            cashier.GetSalesReport(CurrentStand.Id, CurrentCashier.Id, CurrentStand.Shift, CurrentStand.StandWorkDate, (sales, exResult) =>
            {
                var groups = sales.GroupBy(g => g.GroupDescription);

                var totalImport = decimal.Zero;
                TotalPaxs = 0;

                foreach (var group in groups)
                {
                    var familyGrid = new StackPanel
                    { 
                        Margin = CreateThickness("0.125cm")
                    };

                    familyGrid.Children.Add(new TextBlock
                    { 
                        Text = group.Key,
                        FontWeight = FontWeights.Bold,
                        HorizontalAlignment = HorizontalAlignment.Left,
                        Margin = CreateThickness("0, 0, 0, 0.125cm")
                    });

                    decimal auxImport = 0;
                    foreach (var sale in group.Where(g => g.Importe != decimal.Zero))
                    {
                        auxImport += sale.Importe;
                        var auxGrid = new UniformGrid() { Columns = 3 };
                        auxGrid.Children.Add(new TextBlock
                        { 
                            Text = sale.ProductDescription,
                            HorizontalAlignment = HorizontalAlignment.Left
                        });

                        auxGrid.Children.Add(new TextBlock
                        { 
                            Text = sale.Quantity.ToString("F2"),
                            HorizontalAlignment = HorizontalAlignment.Right
                        });

                        auxGrid.Children.Add(new TextBlock
                        {
                            Text = sale.Importe.ToString("C2", Hotel.Culture),
                            HorizontalAlignment = HorizontalAlignment.Right,
                            Margin = CreateThickness("0, 0, 0.25cm, 0")
                        });

                        familyGrid.Children.Add(auxGrid);
                        TotalPaxs = sale.Paxs;
                    }

                    totalImport += auxImport;

                    familyGrid.Children.Add(new TextBlock
                    {
                        Text = string.Format(Hotel.Culture, "Total {0}: {1:C2}", group.Key, auxImport),
                        FontWeight = FontWeights.Bold,
                        HorizontalAlignment = HorizontalAlignment.Right,
                        Margin = CreateThickness("0, 0.125cm, 0.25cm, 0")
                    });

                    mainStackPanel.Children.Add(familyGrid);
                }

                mainStackPanel.Children.Add(new Rectangle
                { 
                    Margin = CreateThickness("0, 0.25cm, 0, 0")
                });

                mainStackPanel.Children.Add(new TextBlock
                {
                    Text = string.Format(Hotel.Culture, "Total: {0:C2}", totalImport),
                    FontWeight = FontWeights.Bold,
                    FontSize = 16,
                    HorizontalAlignment = HorizontalAlignment.Right,
                    Margin = CreateThickness("0, 0, 0.25cm, 0")
                });

                mainStackPanel.Children.Add(new Rectangle());
                mainStackPanel.Children.Add(new TextBlock
                {
                    Text = "NewHotel Software",
                    HorizontalAlignment = HorizontalAlignment.Right,
                    Margin = CreateThickness("0, 0, 0.25cm, 0")
                });
            }, true);
        }

        public Visual PreparePrint(PrinterContext context)
        {
            return Scroll.PreparePrintScroll(context);
        }
    }
}