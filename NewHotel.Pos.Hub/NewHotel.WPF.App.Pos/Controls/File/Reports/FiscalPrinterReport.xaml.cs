﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Core;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Localization;
using NewHotel.Pos.PrinterDocument.Fiscal;
using NewHotel.Pos.PrinterDocument.NHFiscalPrinterService;
using NewHotel.WPF.App.Pos.Controls.File.PrinterSettings.FiscalPrinter;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common.Controls;
using NewHotel.WPF.Common.Controls.Busy;
using NewHotel.WPF.Common.Models;
using NewHotel.WPF.Model.Interface;
using ValidationResult = NewHotel.Contracts.ValidationResult;

namespace NewHotel.WPF.App.Pos.Controls.File.Reports
{
    /// <summary>
    /// Interaction logic for FiscalPrinterReport.xaml
    /// </summary>
    public partial class FiscalPrinterReport : UserControl
    {
        #region Variables

        private IEnumerable<CollectionViewItem<Document>> _reportsReprint;
        private IEnumerable<CollectionViewItem<Report>> _reportsPrint;
        private IFiscalPrinter _printer;

        #endregion
        #region Constructors

        public FiscalPrinterReport()
        {
            InitializeComponent();
        }

        #endregion
        #region Properties

        public IFiscalPrinter Printer => _printer ??= CwFactory.Resolve<IFiscalPrinter>();

        #region Mode

        public FprMode Mode
        {
            get => (FprMode)GetValue(ModeProperty);
            set => SetValue(ModeProperty, value);
        }

        public static readonly DependencyProperty ModeProperty =
            DependencyProperty.Register(nameof(Mode), typeof(FprMode), typeof(FiscalPrinterReport), new PropertyMetadata(OnModeChanged));

        private static void OnModeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = (FiscalPrinterReport)d;

            switch ((FprMode)e.NewValue)
            {
                case FprMode.None:
                    obj.Visibility = Visibility.Collapsed;
                    break;
                case FprMode.Print:
                    obj.PrintControlContainer.Visibility = Visibility.Visible;
                    obj.PrintControl.Visibility = Visibility.Visible;
                    obj.PrintByRangeControl.Visibility = Visibility.Collapsed;
                    obj.ReprintControlContainer.Visibility = Visibility.Collapsed;
                    break;
                case FprMode.PrintByRange:
                    obj.ReprintControlContainer.Visibility = Visibility.Collapsed;
                    obj.PrintControlContainer.Visibility = Visibility.Visible;
                    obj.PrintByRangeControl.Visibility = Visibility.Visible;
                    obj.PrintControl.Visibility = Visibility.Collapsed;
                    break;
                case FprMode.Reprint:
                    obj.ReprintControlContainer.Visibility = Visibility.Visible;
                    obj.PrintControl.Visibility = Visibility.Visible;
                    obj.PrintByRangeControl.Visibility = Visibility.Collapsed;
                    obj.PrintControlContainer.Visibility = Visibility.Collapsed;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        #endregion
        #region ReprintReports

        public IEnumerable<CollectionViewItem<Document>> ReprintReports
        {
            get
            {
                if (_reportsReprint == null)
                {
                    _reportsReprint = new[]
                    {
                        new CollectionViewItem<Document> (Document.XReport,Document.XReport,LocalizationMgr.Translation("XReport")),
                        new CollectionViewItem<Document> (Document.ZReport, Document.ZReport,LocalizationMgr.Translation("ZReport")),
                    };
                    ReprintReportSelected = _reportsReprint.First().Id;
                }

                return _reportsReprint;
            }
        }

        public Document ReprintReportSelected
        {
            get => (Document)GetValue(ReprintReportSelectedProperty);
            set => SetValue(ReprintReportSelectedProperty, value);
        }

        public static readonly DependencyProperty ReprintReportSelectedProperty =
            DependencyProperty.Register(nameof(ReprintReportSelected), typeof(Document), typeof(FiscalPrinterReport));


        #endregion
        #region PrintReports

        public IEnumerable<CollectionViewItem<Report>> PrintReports
        {
            get
            {
                if (_reportsPrint == null)
                {
                    _reportsPrint = new[]
                    {
                        new CollectionViewItem<Report> (Report.XDaily,Report.XDaily,LocalizationMgr.Translation("XDaily")),
                        new CollectionViewItem<Report>(Report.XAccumulated, Report.XAccumulated, LocalizationMgr.Translation("XAccumulated")),
                        new CollectionViewItem<Report>(Report.XClearAccumulated, Report.XClearAccumulated, LocalizationMgr.Translation("XClearAccumulated")),
                        new CollectionViewItem<Report>(Report.ZDaily, Report.ZDaily, LocalizationMgr.Translation("ZDaily")),
                        new CollectionViewItem<Report>(Report.ZAccumulated, Report.ZAccumulated, LocalizationMgr.Translation("ZAccumulated")),
                    };
                    PrintReportSelected = _reportsPrint.First().Id;
                }
                return _reportsPrint;
            }
        }

        public Report PrintReportSelected
        {
            get => (Report)GetValue(PrintReportSelectedProperty);
            set => SetValue(PrintReportSelectedProperty, value);
        }

        public static readonly DependencyProperty PrintReportSelectedProperty =
            DependencyProperty.Register(nameof(PrintReportSelected), typeof(Report), typeof(FiscalPrinterReport));

        #endregion

        #endregion
        #region Methods

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            Mode = FprMode.Print;
        }

        public async Task PrintReport()
        {
            BusyControl.Instance.Show();
            try
            {
                ValidationResult result;
                switch (Mode)
                {
                    case FprMode.Print:
                        if (PrintReportSelected == Report.ZDaily)
                        {
                            var context = Hotel.Instance.CurrentStandCashierContext;
                            result = await BusinessProxyRepository.Stand.CloseDayValidationsAsync(context.Stand.Id, context.Cashier.Id, context.Stand.StandWorkDate);
                            if (result.IsEmpty)
                            {
                                bool printOnPaper = ChkPrintZReportOnPaper.IsChecked.Value;
                                result = await Printer.PrintZReportAsync(printOnPaper);
                            }
                        }
                        else result = await Printer.PrintReportAsync(PrintReportSelected);
                        break;
                    case FprMode.PrintByRange:
                        {
                            RangePicker rangPicker = RpPrintZReportByRange;
                            switch (rangPicker.Mode)
                            {
                                case RangePicker.RpMode.DateRange:
                                    result = await Printer.PrintZReportAsync(rangPicker.StartDate, rangPicker.EndDate);
                                    break;
                                case RangePicker.RpMode.NumberRange:
                                    result = await Printer.PrintZReportAsync(rangPicker.StartNumber, rangPicker.EndNumber);
                                    break;
                                default:
                                    throw new ArgumentOutOfRangeException();
                            }
                        }
                        break;
                    case FprMode.Reprint:
                        {
                            switch (RpReprint.Mode)
                            {
                                case RangePicker.RpMode.DateRange:
                                    result = await Printer.ReprintAsync(ReprintReportSelected, RpReprint.StartDate, RpReprint.EndDate);
                                    break;
                                case RangePicker.RpMode.NumberRange:
                                    result = await Printer.ReprintAsync(ReprintReportSelected, RpReprint.StartNumber, RpReprint.EndNumber);
                                    break;
                                default:
                                    throw new ArgumentOutOfRangeException();
                            }
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                if (!result.IsEmpty)
                {
                    var window = CwFactory.Resolve<IWindow>();
                    window.ShowError(result);
                }
            }
            finally
            {
                BusyControl.Instance.Close();
            }
        }

        private void ChkReprint_CheckChanged(object sender, RoutedEventArgs e)
        {
            ChkByPrintByRange.IsChecked = false;
            Mode = ChkReprint.IsChecked.GetValueOrDefault() ? FprMode.Reprint : FprMode.Print;
        }

        private void ChkByPrintByRange_CheckChanged(object sender, RoutedEventArgs e)
        {
            Mode = ChkByPrintByRange.IsChecked.GetValueOrDefault() ? FprMode.PrintByRange : FprMode.Print;
        }

        #endregion
        #region Enums

        public enum FprMode
        {
            None, Print, PrintByRange, Reprint
        }

        #endregion
    }
}
