﻿using NewHotel.WPF.App.Pos.Model;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace NewHotel.WPF.App.Pos.Controls
{
    public partial class CategoryButton : UserControl
    {
        public enum States { Category, Subcategory, Favorites };

        private bool isLoaded = false;
        private bool isChecked = false;
        private bool isPressed = false;

        public event Action<CategoryButton, bool> Checked;

        public CategoryButton()
        {
            InitializeComponent();
        }

        public States State
        {
            get { return (States)GetValue(StateProperty); }
            set
            {
                if (!isLoaded)
                {// No ha sido cargado
                    if (value == States.Favorites)
                    {
                        groupBorder.CornerRadius = new CornerRadius(20, 20, 20, 20);
                        image.Width = 20;
                        image.Height = 20;
                        image.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                        image.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                        groupText.Margin = new Thickness(0);
                        groupText.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                        groupText.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                        groupText.FontSize = 16;
                        groupText.FontWeight = FontWeights.Bold;
                    }
                    else
                    {
                        image.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                        image.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                        groupText.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                        groupText.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                        groupText.FontSize = 12;
                        groupText.FontWeight = FontWeights.Normal;
                        groupText.Margin = new Thickness(0, 0, 0, 12);

                        if (value == States.Category)
                        {
                            groupBorder.CornerRadius = new CornerRadius(5, 20, 20, 5);
                            image.Width = 60;
                            image.Height = 60;
                        }
                        else if (value == States.Subcategory)
                        {
                            groupBorder.CornerRadius = new CornerRadius(20, 5, 5, 20);
                            image.Width = 40;
                            image.Height = 40;
                        }
                    }
                }// No ha sido cargado
                SetValue(StateProperty, value);
            }
        }

        public static readonly DependencyProperty StateProperty =
            DependencyProperty.Register("State", typeof(States), typeof(CategoryButton), new PropertyMetadata(States.Category));

        private void checkBox_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            State = State;
            isLoaded = true;

            if (DataContext is Category)
            {
                //groupText.Text = (DataContext as Category).Description.Current.Translation;
            }
            (Resources["Loading"] as Storyboard).Begin();
        }

        public bool IsChecked
        {
            get
            {
                return isChecked;
            }
            set
            {
                isChecked = value;
                if (isChecked)
                {
                    (Resources["Checking"] as Storyboard).Begin();
                }
                else
                {
                    (Resources["UnChecking"] as Storyboard).Begin();
                }
            }
        }

        private void checkBox_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            isPressed = true;
        }

        private void checkBox_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (isPressed)
            {
                if (!IsChecked)
                {
                    IsChecked = true;
                    if (Checked != null)
                        Checked(this, false);
                    return;
                }
                isPressed = false;
            }
        }

        private void checkBox_MouseLeave(object sender, MouseEventArgs e)
        {
            isPressed = false;
        }
    }
}
