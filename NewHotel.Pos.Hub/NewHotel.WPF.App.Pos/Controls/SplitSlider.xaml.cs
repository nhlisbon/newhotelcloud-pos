﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for QuantityUpDown.xaml
    /// </summary>
    public partial class SplitSlider : UserControl
    {
        private Point firstPoint, prevPoint;
        //private int multiply;

        public event Action<SplitSlider> SplitChanged;

        public SplitSlider()
        {
            InitializeComponent();
            //Increment = 1;
        }

        //void timer_Elapsed(object sender, ElapsedEventArgs e)
        //{
        //    this.Dispatcher.BeginInvoke(
        //           DispatcherPriority.Normal,
        //           (DispatcherOperationCallback)delegate(object arg)
        //           {
        //               try
        //               {
        //                   Value += multiply * Step;
        //               }
        //               catch { }
        //               return null;
        //           }, null
        //           );
        //}

        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set
            {
                if (value <= MaxValue && value >= MinValue)
                    SetValue(ValueProperty, value);

            }
        }
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(SplitSlider));



        public double LeftValue
        {
            get { return (double)GetValue(LeftValueProperty); }
            set
            {
                var right = Value - value;
                if (value <= MaxValue && value >= MinValue && right <= MaxValue && right >= MinValue)
                    SetValue(LeftValueProperty, value);
            }
        }

        public static readonly DependencyProperty LeftValueProperty =
            DependencyProperty.Register("LeftValue", typeof(double), typeof(SplitSlider));




        public double RightValue
        {
            get { return (double)GetValue(RightValueProperty); }
            set { SetValue(RightValueProperty, value); }
        }

        public static readonly DependencyProperty RightValueProperty =
            DependencyProperty.Register("RightValue", typeof(double), typeof(SplitSlider));

        public double MinValue
        {
            get { return (double)GetValue(MinValueProperty); }
            set { SetValue(MinValueProperty, value); }
        }

        public static readonly DependencyProperty MinValueProperty =
            DependencyProperty.Register("MinValue", typeof(double), typeof(SplitSlider), new PropertyMetadata(double.MinValue));

        public double MaxValue
        {
            get { return (double)GetValue(MaxValueProperty); }
            set { SetValue(MaxValueProperty, value); }
        }

        public static readonly DependencyProperty MaxValueProperty =
            DependencyProperty.Register("MaxValue", typeof(double), typeof(SplitSlider), new PropertyMetadata(double.MaxValue));


        public decimal Increment
        {
            get { return (decimal)GetValue(IncrementProperty); }
            set { SetValue(IncrementProperty, value); }
        }

        private int GetAmountOfDecimals(decimal value)
        {
            int digs = 0;
            while (decimal.Truncate(value) != value)
            {
                digs++;
                value *= 10;
            }

            return digs;
        }

        public static readonly DependencyProperty IncrementProperty =
            DependencyProperty.Register("Increment", typeof(decimal), typeof(SplitSlider));

        public void subButton_Click(object sender, RoutedEventArgs e)
        {
            LeftValue += (double)Increment;
        }
        public void addButton_Click(object sender, RoutedEventArgs e)
        {
            LeftValue -= (double)Increment;
        }

        private void userControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            IsPressed = true;
            firstPoint = e.GetPosition(this);
            prevPoint = firstPoint;
        }

        private void userControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (IsPressed)
                IsPressed = false;
        }

        private void userControl_MouseLeave(object sender, MouseEventArgs e)
        {
            if (IsPressed)
                IsPressed = false;
        }

        public bool IsPressed
        {
            get { return (bool)GetValue(IsPressedProperty); }
            set
            {
                SetValue(IsPressedProperty, value);
                if (value)
                {
                    //(Resources["pressed"] as Storyboard).Begin();
                }
                else
                {
                    //(Resources["unPressed"] as Storyboard).Begin();
                }
            }
        }
        public static readonly DependencyProperty IsPressedProperty =
            DependencyProperty.Register("IsPressed", typeof(bool), typeof(SplitSlider));

        private void userControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (IsPressed)
            {
                var newPoint = e.GetPosition(this);
                var dist = (int)ActualWidth / 11;
                for (int i = 1; i < 11; i++)
                {
                    if (prevPoint.X <= i * dist && i * dist <= newPoint.X)
                        LeftValue -= (double)Increment;
                    if (prevPoint.X >= i * dist && i * dist >= newPoint.X)
                        LeftValue += (double)Increment;
                }
                prevPoint = newPoint;
            }
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property.Name == "Increment" && e.OldValue != e.NewValue)
            {
                int dig = GetAmountOfDecimals((decimal)e.NewValue);
                var leftbind = new Binding() { Path = new PropertyPath("LeftValue"), ElementName = "userControl", StringFormat = "F" + dig.ToString() };
                var rightbind = new Binding() { Path = new PropertyPath("RightValue"), ElementName = "userControl", StringFormat = "F" + dig.ToString() };
                BindingOperations.SetBinding(leftValueTextBlock, TextBlock.TextProperty, leftbind);
                BindingOperations.SetBinding(rightValueTextBlock, TextBlock.TextProperty, rightbind);
            }
            else if (e.Property.Name == "LeftValue")
            {
                RightValue = Value - LeftValue;
                if (SplitChanged != null)
                    SplitChanged(this);
            }

        }
    }
}
