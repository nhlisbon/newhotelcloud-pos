﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using DevExpress.Internal.WinApi.Windows.UI.Notifications;
using NewHotel.Contracts;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Core;
using NewHotel.Pos.Hub.Contracts.Common.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.Records;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Localization;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using NewHotel.WPF.Common.Controls;
using NewHotel.WPF.Common.Controls.Buttons;
using NewHotel.WPF.Common.Notifications;
using NewHotel.WPF.Core;
using NewHotel.WPF.Model;
using NewHotel.WPF.Model.Interface;
using NewHotel.WPF.Model.Model;
using Table = NewHotel.WPF.Model.Model.Table;

namespace NewHotel.WPF.App.Pos.Controls;

/// <summary>
/// Interaction logic for TablesViewer.xaml
/// </summary>
public sealed partial class TablesViewer : INotifyPropertyChanged, ITSKControlsKey
{
    #region Common
    
    #region Constructor

    public TablesViewer()
    {
        InitializeComponent();
        
        ShowQuickTickets = true;
        _quickTicketTimer = new Timer(QuickTicketTimerTick);
        _quickTicketTimer.Change(Timeout.Infinite, 1000);
        
        _ticketInTableTimer = new Timer(TicketInTableTimerTick);
        _ticketInTableTimer.Change(Timeout.Infinite, 1000);
        PropertyChanged += OnPropertyChanged;

        if (double.TryParse(MgrServices.GetConfigurationSetting(MgrServices.Settings.SaloonZoomValue), out var defaultSaloonZoom))
            SaloonZoom = defaultSaloonZoom > 1000 ? defaultSaloonZoom / 100 : defaultSaloonZoom;
        else
            SaloonZoom = 100;

        ZoomToSaloon();
    }

    #endregion
    #region Properties

    private ILogger Logger => CwFactory.Resolve<ILogger>();
    private IWindow WindowHelper => CwFactory.Resolve<IWindow>();
    private Point MiddleScreen => new(ActualWidth / 2, ActualHeight / 2);

    public bool HasSaloonSelected => SelectedSaloon != null;

    public Saloon SelectedSaloon
    {
        get => (Saloon)((ListBox)Resources["SaloonsLbx"]).SelectedItem;
        set
        {
            var before = SelectedSaloon;
            ((ListBox)Resources["SaloonsLbx"]).SelectedItem = value;
            OnSelectedSaloonAssgined(before, SelectedSaloon);
        }
    }

    public TablesViewerStatus State
    {
        get
        {
            return _status;
        }
        set
        {
            // En principio todos los controles estan ocultos, estado Unknown.
            // Cada cuando se entra a un estado se muestran los controles que este usa.
            // Cuando se sale de un estado se ocultan los controles que este usa

            #region Out 

            switch (_status)
            {
                case TablesViewerStatus.Normal:
                {
                    LastExpandedTableButton = null;
                    quickTicketBar.IsEnabled = false;
                    quickTicketBar.Visibility = Visibility.Collapsed;
                    tablesContainer.Visibility = Visibility.Collapsed;
                    tableDetailsGrid.Visibility = Visibility.Collapsed;

                    break;
                }
                case TablesViewerStatus.Moving:
                {
                    SaloonsContainer.Visibility = Visibility.Visible;
                    tablesContainer.Visibility = Visibility.Collapsed;
                    BeginHideMoveTablesAnimation();
                    break;
                }
                case TablesViewerStatus.Picking:
                {
                    pickTablesTopBar.Visibility = Visibility.Collapsed;
                    liteTablesContainer.Visibility = Visibility.Collapsed;
                    break;
                }
            }

            #endregion
            #region In

            _status = value;
            switch (_status)
            {
                case TablesViewerStatus.Normal:
                {
                    quickTicketBar.IsEnabled = true;
                    quickTicketBar.Visibility = Visibility.Visible;
                    tablesContainer.Visibility = Visibility.Visible;
                    tableDetailsGrid.Visibility = Visibility.Visible;

                    break;
                }
                case TablesViewerStatus.Moving:
                {
                    SaloonsContainer.Visibility = Visibility.Collapsed;
                    tablesContainer.Visibility = Visibility.Visible;
                    BeginsShowMoveTablesAnimation();
                    break;
                }
                case TablesViewerStatus.Picking:
                {
                    pickTablesTopBar.Visibility = Visibility.Visible;
                    liteTablesContainer.Visibility = Visibility.Visible;

                    DrawTablesToPick();
                    break;
                }
            }

            #endregion
        }
    }

    public TableButton? LastExpandedTableButton
    {
        get => _lastExpandedTableButton;
        set
        {
            if (_lastExpandedTableButton != null)
            {
                var table = _lastExpandedTableButton.Table;
                // Contraer la tabla o el grupo entero al que pertenece
                if (table.HasTablesGroup)
                {
                    foreach (Table tab in table.TablesGroup)
                        tab.IsExpanded = false;
                }
                else table.IsExpanded = false;
            }

            var tableContext =
                value == null
                ? null
                : value.DataContext as Table;

            if(tableContext != null)
            {
                var sorted = tableContext.Tickets.OrderBy(t => t.Serie);
                tableContext.Tickets = new ObservableCollection<TicketSlim>(sorted);
            }
            
            tableDetailsGrid.DataContext = 
                value == null 
                ? null 
                : tableContext;
            
            IsTableDetailsVisible = tableDetailsGrid.DataContext != null;
            IsTablesGroupDetails = tableDetailsGrid.DataContext != null && value != null && value.Table.HasTablesGroup;
            ticketsByTable.ScrollToHome();
            _lastExpandedTableButton = value;
        }
    }
    
    #endregion

    #endregion
    
    #region Showing + Setting Position + Merging Tickets

    #region Variables  

    private bool _isTableDetailsVisible;
    private bool _isOnTicketBin;
    private List<TableButton> _tableButtons;
    private SaloonButton _firstSaloonLoaded;
    private readonly Timer _quickTicketTimer;
    private bool _quickTicketTick;
    private bool _ticketInTableTick;
    private readonly Timer _ticketInTableTimer;
    private TableButton _lastExpandedTableButton;
    private SaloonButton _lastSaloonButton;
    private ActionButton _lastTicket;
    private TablesViewerStatus _status = TablesViewerStatus.Unknown;
    private Button _lastMergeSlot;
    private TableButton _lastDownTable;
    private bool _onQuickTicket;
    private bool _isTouching;
    private TableButton _floatingTable;
    private bool _movingTablePressed;
    private short _iniRow, _iniColumn;
    private bool _isGranted;
    private double _saloonZoom;
    private Timer _marchTimer;

    #endregion
    
    #region Events

    public event PropertyChangedEventHandler PropertyChanged;
    public event Action<TablesViewer, Table, ITicket> TicketClick;
    public event Action<TablesViewer, ITicket, ITicket> MergeTicketsClick;
    public event Func<Table?, short, Ticket?> NewTicketRequest;
    public event Action<TablesViewer, ITicket> VoidTicket;
    public event Action<TablesViewer> PersistChangesRequested;
    public event Action<TablesViewer> DisableOutsideActionsRequest;
    public event Action<TablesViewer> EnableOutsideActionsRequest;
    public event Action<ITicket, Table, Action<bool>> MoveTicketRequest;
    public event Action<double> SaloonZoomChangued;

    #endregion
    
    #region Properties

    #region NoTableTicketFilterText

    public string NoTableTicketFilterText
    {
        get => (string)GetValue(NoTableTicketFilterTextProperty);
        set => SetValue(NoTableTicketFilterTextProperty, value);
    }

    public static readonly DependencyProperty NoTableTicketFilterTextProperty =
        DependencyProperty.Register(nameof(NoTableTicketFilterText), typeof(string), typeof(TablesViewer), new PropertyMetadata(NoTableTicketFilterTextPropertyChanged));

    private static void NoTableTicketFilterTextPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        var obj = (TablesViewer)d;
        obj.NoTableTickets.Filter = item =>
        {
            if(item is TicketSlim ticket)
                return ticket.Serie.HasValue && ticket.Serie.Value.ToString().Contains(obj.NoTableTicketFilterText.Trim().ToLower());
            
            return false;
        };
    }

    #endregion

    #region TableFilterText

    public string TableFilterText
    {
        get => (string)GetValue(TableFilterTextProperty);
        set => SetValue(TableFilterTextProperty, value);
    }

    public static readonly DependencyProperty TableFilterTextProperty =
        DependencyProperty.Register(nameof(TableFilterText), typeof(string), typeof(TablesViewer), new PropertyMetadata(TableFilterTextPropertyChanged));

    private static void TableFilterTextPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        var obj = (TablesViewer)d;
        obj.DrawTableButtons(obj.TableFilterText);
    }
    #endregion 
    
    #region Saloons

    public ObservableCollection<Saloon> Saloons
    {
        get => (ObservableCollection<Saloon>)GetValue(SaloonsProperty);
        set => SetValue(SaloonsProperty, value);
    }

    public static readonly DependencyProperty ShowQuickTicketsProperty =
        DependencyProperty.Register(nameof(ShowQuickTickets), typeof(bool), typeof(TablesViewer));

    public static readonly DependencyProperty SaloonsProperty =
        DependencyProperty.Register(nameof(Saloons), typeof(ObservableCollection<Saloon>), typeof(TablesViewer), new PropertyMetadata(SaloonsPropertyChangedCallback));

    private static void SaloonsPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        var viewer = (TablesViewer)d;
        viewer.SelectedSaloon = ((ObservableCollection<Saloon>)e.NewValue).FirstOrDefault();
    }

    #endregion

    public StandCashierContext? StandCashierContext
    {
        get => DataContext as StandCashierContext;
        private set => DataContext = value;
    }

    public bool ShowQuickTickets
    {
        get => (bool)GetValue(ShowQuickTicketsProperty);
        set
        {
            SetValue(ShowQuickTicketsProperty, value);
            quickTicketRow.Height = ShowQuickTickets ? GridLength.Auto : new GridLength(0);
        }
    }

    public bool IsTablesGroupDetails
    {
        get => (bool)GetValue(IsTablesGroupDetailsProperty);
        set => SetValue(IsTablesGroupDetailsProperty, value);
    }

    public static readonly DependencyProperty IsTablesGroupDetailsProperty =
        DependencyProperty.Register(nameof(IsTablesGroupDetails), typeof(bool), typeof(TablesViewer));

    public bool IsTableDetailsVisible
    {
        get => _isTableDetailsVisible;
        set
        {
            _isTableDetailsVisible = value;
            if (value)
            {
                tableDetailsGrid.IsEnabled = true;
                BeginShowSelectedTablePanelAnimation();
            }
            else
            {
                tableDetailsGrid.IsEnabled = false;
                BeginHideSelectedTablePanelAnimation();
            }
        }
    }

    public bool IsMerging
    {
        get => (bool)GetValue(IsMergingProperty);
        set
        {
            if (IsMerging != value)
            {
                if (value)
                    BeginShowMergeControlAnimation();
                else
                {
                    BeginHideMergeControlAnimation();
                    ResetMergeSlots();
                }
            }
            SetValue(IsMergingProperty, value);
        }
    }

    public static readonly DependencyProperty IsMergingProperty =
        DependencyProperty.Register(nameof(IsMerging), typeof(bool), typeof(TablesViewer));

    private bool IsOnTicketBin
    {
        get => _isOnTicketBin;
        set
        {
            if (value != _isOnTicketBin)
                ticketBinImageEffect.BlurRadius = value ? 20 : 0;
            _isOnTicketBin = value;
        }
    }

    public double SaloonZoom
    {
        get => _saloonZoom;
        set
        {
            _saloonZoom = value;
            OnPropertyChanged(nameof(SaloonZoom));
        }
    }

    public ICollectionView NoTableTickets
    {
        get => _noTableTickets;
        set
        {
            _noTableTickets = value;
            OnPropertyChanged(nameof(NoTableTickets));
        }
    }

    #endregion
    #region Methods

    public void ResetMergeSlots()
    {
        merge1.DataContext = null;
        merge1.Visibility = Visibility.Collapsed;
        merge2.DataContext = null;
        merge2.Visibility = Visibility.Collapsed;
    }

    public void RemoveVisualTicket(ITicket ticket, bool error)
    {
        if (!HasSaloonSelected)
            return;

        var table = !ticket.TableId.HasValue ? null : SelectedSaloon.FindTable(ticket.TableId.Value);

        if (table != null)
        {
            table.RemoveTicket(ticket, error);

            // if (!table.HasBills)
            if (!table.HasTickets)
            {
                if (LastExpandedTableButton != null && LastExpandedTableButton.Table == table)
                    LastExpandedTableButton = null;
            }
            else table.RecalculateTotal();

            if (ticket is Ticket realTicket)
            {
                // Si el ticket se cancelo sin añadir ordenes entonces
                // en caso de que exista reserva procesada reinsertarla  
                if (realTicket.ReservationTableId.HasValue && !realTicket.IsPersisted)
                {
                    if (realTicket.TablesGroup != null)
                    {
                        foreach (var t in realTicket.TablesGroup)
                            t.RollBackLastRemoveReservation();
                    }
                    else
                        realTicket.Table.RollBackLastRemoveReservation();
                }

                // Si el ticket esta enlazado a un grupo de mesas entonces 
                // eliminar la asociacion de las tablas
                if (realTicket.TablesGroup != null && table.HasTablesGroup)
                    table.TablesGroup.UnlinkTables();
            }
        }
        else
        {
            if (!StandCashierContext.QuickTickets.RemoveTicket(ticket))
                if (!StandCashierContext.AcceptedTickets.RemoveTicket(ticket))
                    StandCashierContext.ReturnedTickets.RemoveTicket(ticket);
        }

        if (merge1.DataContext == ticket)
        {
            merge1.DataContext = null;
            merge1.Visibility = Visibility.Collapsed;
        }

        if (merge2.DataContext != ticket) return;
        merge2.DataContext = null;
        merge2.Visibility = Visibility.Collapsed;
    }

    public void ZoomToSaloon()
    {
        AnimateChangeOfZoom(SaloonZoom / 100);
    }

    public void InZoom()
    {
        if (SaloonZoom + 5 <= 300)
        {
            SaloonZoom += 5;
            AnimateChangeOfZoom(SaloonZoom / 100);
        }
    }

    public void OutZoom()
    {
        if (SaloonZoom - 5 >= 30)
        {
            SaloonZoom -= 5;
            AnimateChangeOfZoom(SaloonZoom / 100);
        }
    }

    public void ResetZoom()
    {
        SaloonZoom = 100;
        AnimateChangeOfZoom(SaloonZoom / 100);
    }

    public void JoystickMovement(Joystick joystick)
    {

        if (Math.Abs(joystick.OffsetX) >= Math.Abs(joystick.OffsetY))
        {
            tablesContainerScroll.ScrollToHorizontalOffset(Math.Max(0, tablesContainerScroll.HorizontalOffset + 100 * joystick.OffsetX));
        }
        else
        {
            tablesContainerScroll.ScrollToVerticalOffset(Math.Max(0, tablesContainerScroll.VerticalOffset + 100 * joystick.OffsetY));
        }
    }

    public void SetDefaultTablesPosition()
    {

        SetRectanglesAsNoBusy(null);
        SetRectanglesAsNormal(null);

        short columns = 0;
        if (_lastSaloonButton != null)
        {
            columns = ((Saloon)_lastSaloonButton.DataContext).Width.Value;
        }

        short currentRow = 0, currentColumn = 0;
        foreach (TableButton tableButton in tablesContainer.Children)
        {
            var table = tableButton.Table;
            table.Row = currentRow;
            table.Column = currentColumn;

            currentColumn += table.Width.Value;
            if (currentColumn >= columns - 1)
            {
                currentColumn = 0;
                currentRow += table.Height.Value;
            }

            Grid.SetRowSpan(tableButton, (int)table.Height);
            Grid.SetColumnSpan(tableButton, (int)table.Width);
            Grid.SetRow(tableButton, (int)table.Row);
            Grid.SetColumn(tableButton, (int)table.Column);

            SetRectanglesAsBusy(table);

            table.PersistObject();
        }
        tablesContainer.UpdateLayout();
    }

    /// <summary>
    /// Almacena en el HUB la posicion de las mesas
    /// (Merojable salvando todas de una vezz)
    /// </summary>
    public void SaveDefaultTablesPosition()
    {
        foreach (TableButton tableButton in tablesContainer.Children)
        {
            var table = tableButton.Table;
            table.PersistObject();
        }
    }

    public void ResetData(StandCashierContext context)
    {
        StandCashierContext = context;
        Saloons = context.Stand.Saloons;
        _firstSaloonLoaded = null;
        InitializeNoTableTickets();
    }

    public void PersistChanges()
    {
        if (PersistChangesRequested != null)
            PersistChangesRequested(this);
    }

    #region Private Methods

    #region Animations

    private void BeginHideSelectedTablePanelAnimation()
    {
        ((Storyboard)Resources["hideSelectedTablePanel"]).Begin();
    }

    private void BeginShowSelectedTablePanelAnimation()
    {
        ((Storyboard)Resources["showSelectedTablePanel"]).Begin();
    }

    private void BeginsShowMoveTablesAnimation()
    {
        ((Storyboard)Resources["showMoveTables"]).Begin();
    }

    private void BeginTicketBinEmphasysAnimation()
    {
        ((Storyboard)Resources["ticketBinEmphasys"]).Begin();
    }

    private void BeginHideMoveTablesAnimation()
    {
        ((Storyboard)Resources["hideMoveTables"]).Begin();
    }

    private void BeginHideMergeControlAnimation()
    {
        ((Storyboard)Resources["hideMergeControl"]).Begin();
    }

    private void BeginShowMergeControlAnimation()
    {
        ((Storyboard)Resources["showMergeControl"]).Begin();
    }

    private void BeginChangeZoomValueAnimation(double newValue)
    {
        var storyboard = (Storyboard)Resources["changeZoomValue"];
        storyboard.Stop();
        ((DoubleAnimationUsingKeyFrames)storyboard.Children[0]).KeyFrames[0].Value = newValue;
        ((DoubleAnimationUsingKeyFrames)storyboard.Children[1]).KeyFrames[0].Value = newValue;
        storyboard.Begin();
    }


    #endregion

    private void DrawTableButtons(string prefix)
    {
        if (!HasSaloonSelected)
            return;
        Saloon saloon = SelectedSaloon;
        if (saloon == null) return;

        Tuple<int, int> dimensions = GetSaloonDimensions(saloon);
        int rows = dimensions.Item1;
        int columns = dimensions.Item2;

        var lengthConverter = new LengthConverter();
        tablesOutContainer.Width = (double)lengthConverter.ConvertFromString(null, Hotel.DefaultCultureInfo, (columns * 1.4).ToString("F2", Hotel.DefaultCultureInfo) + "cm");
        tablesOutContainer.Height = (double)lengthConverter.ConvertFromString(null, Hotel.DefaultCultureInfo, (rows * 1.4).ToString("F2", Hotel.DefaultCultureInfo) + "cm");

        positionGrid.Children.Clear();
        positionGrid.Rows = rows;
        positionGrid.Columns = columns;

        for (int r = 0; r < rows; r++)
        {
            for (int c = 0; c < columns; c++)
            {
                positionGrid.Children.Add(new Border
                {
                    CornerRadius = new CornerRadius(5),
                    Background = Brushes.LightGray,
                    Margin = new Thickness(3)
                });
            }
        }

        tablesContainer.Children.Clear();
        tablesContainer.RowDefinitions.Clear();

        for (int i = 0; i < rows; i++)
            tablesContainer.RowDefinitions.Add(new RowDefinition());

        tablesContainer.ColumnDefinitions.Clear();

        for (int i = 0; i < columns; i++)
            tablesContainer.ColumnDefinitions.Add(new ColumnDefinition());

        _tableButtons = new List<TableButton>();
        const string freeImageDefaultId = "pack://application:,,,/NewHotel.Pos;component/Resources/Tables/doile.png";
        const string busyImageDefaultId = "pack://application:,,,/NewHotel.Pos;component/Resources/Tables/doile-plato.png";

        foreach (Table table in SelectedSaloon.Tables)
        {
            if (table.Name != null && table.Name.StartsWith(prefix, StringComparison.OrdinalIgnoreCase))
            {
                table.FreeImage = SelectedSaloon.ImageFree;
                table.BusyImage = SelectedSaloon.ImageBusy;
                table.DefaultFreeImageId = freeImageDefaultId;
                table.DefaultBusyImageId = busyImageDefaultId;

                var auxTableButton = new TableButton(table);
                auxTableButton.OpenTable += TableButton_OpenTable;
                auxTableButton.Expanding += TableButton_Expanding;
                auxTableButton.NewTicket += TableButton_NewTicket;

                tablesContainer.Children.Add(auxTableButton);

                Grid.SetRowSpan(auxTableButton, table.Height ?? 20);
                Grid.SetColumnSpan(auxTableButton, (int)table.Width);
                Grid.SetRow(auxTableButton, (int)table.Row);
                Grid.SetColumn(auxTableButton, (int)table.Column);

                SetRectanglesAsBusy(table);
                _tableButtons.Add(auxTableButton);
            }          
        }

        _marchTimer = new Timer(MarchTimerTick);
        _marchTimer.Change(1000, 1000);

        UpdateLayout();
    }

    private void MarchTimerTick(object sender)
    {
        UIThread.Invoke(() =>
        {
            if (_tableButtons != null)
            {
                foreach (var tableButton in _tableButtons)
                {
                    var initialMarchTimes = tableButton.Table.Bills.SelectMany(b => b.Orders)
                        .Where(o => o.SendToAreaStartTime.HasValue && o.SendToAreaStatus < 4 && o.HasAreas && SocketIONotifications.IsAreaListening(o.AreasId.ToArray()))
                        .Select(o => o.SendToAreaStartTime.Value);

                    if (initialMarchTimes.Any())
                    {
                        var minutes = (int)Math.Truncate((DateTime.UtcNow - initialMarchTimes.Min()).TotalMinutes);
                        if (minutes > 60)
                            tableButton.MarchElapsedMinutes = "!";
                        else if (minutes > 0)
                            tableButton.MarchElapsedMinutes = $"{minutes}m";
                        else
                            tableButton.MarchElapsedMinutes = string.Empty;
                    }
                    else
                        tableButton.MarchElapsedMinutes = string.Empty;

                    tableButton.ShowMarchElapsedMinutes = !string.IsNullOrEmpty(tableButton.MarchElapsedMinutes);
                }
            }
        });
    }
    
    /// <summary>
    /// Populates the quick tickets bar with the tickets that are not assigned to a table
    /// </summary>
    private void InitializeNoTableTickets()
    {
        var context = StandCashierContext;
        var noTableTickets = context?.QuickTickets.Concat(context.AcceptedTickets);

        var cv = CollectionViewSource.GetDefaultView(noTableTickets ?? new List<ITicket>());
        if (NoTableTicketFilterText != null)
        {
            cv.Filter = t =>
            {
                var filter = NoTableTicketFilterText.ToLowerInvariant();
                if (string.IsNullOrEmpty(filter)) return true;
                var ticket = (ITicket)t;
                return ticket.ShowDescription && ticket.Description.ToLowerInvariant().Contains(filter) ||
                       ticket.Serie.HasValue && ticket.Serie.ToString().Contains(filter);
            };
        }
        cv.SortDescriptions.Add(new SortDescription(nameof(Ticket.Serie), ListSortDirection.Ascending));
        cv.SortDescriptions.Add(new SortDescription(nameof(Ticket.Description), ListSortDirection.Ascending));
        
        NoTableTickets = cv;
        context!.QuickTickets.CollectionChanged += (sender, args) => NoTableTickets.Refresh();
        context.AcceptedTickets.CollectionChanged += (sender, args) => NoTableTickets.Refresh();
    }

    private Tuple<int, int> GetSaloonDimensions(Saloon saloon)
    {
        int rows = saloon.Height ?? 30;
        int columns = saloon.Width ?? 30;
        return new Tuple<int, int>(rows, columns);
    }

    /// <summary>
    /// Creates the buttons to pick the tables 
    /// </summary>
    private void DrawLiteTableButtons()
    {
        if (!HasSaloonSelected)
            return;

        var (rows, columns) = GetSaloonDimensions(SelectedSaloon);

        liteTablesContainer.Children.Clear();
        liteTablesContainer.RowDefinitions.Clear();
        for (var i = 0; i < rows; i++)
            liteTablesContainer.RowDefinitions.Add(new RowDefinition());

        liteTablesContainer.ColumnDefinitions.Clear();

        for (var i = 0; i < columns; i++)
            liteTablesContainer.ColumnDefinitions.Add(new ColumnDefinition());

        foreach (var table in SelectedSaloon.Tables)
        {
            // Tables to configure the groups
            var auxLiteTableButton = new LiteTableButton(table);
            auxLiteTableButton.IsSelectedChanged += AuxLiteTableButton_IsSelectedChanged;
            liteTablesContainer.Children.Add(auxLiteTableButton);

            Grid.SetRowSpan(auxLiteTableButton, (int)table.Height!);
            Grid.SetColumnSpan(auxLiteTableButton, (int)table.Width!);
            Grid.SetRow(auxLiteTableButton, (int)table.Row!);
            Grid.SetColumn(auxLiteTableButton, (int)table.Column!);
        }
    }

    private TableButton GetTableButtonDown(Point point)
    {
        foreach (var tb in _tableButtons)
        {
            var rect = new Rect(tb.TranslatePoint(new Point(0, 0), moveTablesCanvas), new Size(tb.ActualWidth, tb.ActualHeight));
            if (rect.Contains(point))
                return tb;
        }

        return null;
    }

    private void OnPropertyChanged(string propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    private void QuickTicketTimerTick(object sender)
    {
        _quickTicketTimer.Change(Timeout.Infinite, 1000);
        _quickTicketTick = true;

        UIThread.Invoke(() =>
        {
            button.Height = _lastTicket.ActualHeight;
            button.ImageSource = _lastTicket.ImageSource;
            button.DataContext = _lastTicket.DataContext;
            canvas.Visibility = Visibility.Visible;
            canvas.CaptureMouse();
        });
    }

    private void TicketInTableTimerTick(object sender)
    {
        _ticketInTableTimer.Change(Timeout.Infinite, 1000);
        _ticketInTableTick = true;

        UIThread.Invoke(() =>
        {
            button.Height = _lastTicket.ActualHeight;
            button.ImageSource = _lastTicket.ImageSource;
            button.DataContext = _lastTicket.DataContext;
            canvas.Visibility = Visibility.Visible;
            canvas.CaptureMouse();
        });
    }

    private void button_TouchUp(object sender, TouchEventArgs e)
    {
        _isTouching = false;
        button_MouseUp(sender, null);
    }

    private void button_MouseUp(object sender, MouseButtonEventArgs e)
    {
        try
        {
            if (_lastTicket != null && _onQuickTicket)
            {
                // Estamos sobre la barra de tickets rapidos
                MainPage.CurrentInstance.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.TicketWithoutTable, (isValid, validUser) =>
                {
                    if (isValid)
                    {
                        var ticket = _lastTicket.DataContext as ITicket;
                        // var prevTable = ticket?.Table;
                        // if (prevTable != null)
                        if (ticket.HasTable())
                        {
                            MoveTicketRequest?.Invoke(ticket, null, succeed =>
                            {
                                if (!succeed)
                                    return;
                                /*
                                if (ticket.IsAcceptedAsTransfer) StandCashierContext.AcceptedTickets.Add(ticket);
                                else StandCashierContext.QuickTickets.Add(ticket);

                                Hotel.Instance.MoveTicket(ticket, null, prevTable, false);
                                // if (prevTable.Bills.Count == 0) LastExpandedTableButton = null;
                                if (!prevTable.HasTickets) LastExpandedTableButton = null;
                                */
                            });
                        }
                    }
                    else
                    {
                        ErrorsWindow.ShowErrorsWindow("UserNotAuthorized".Translate());
                    }
                });
            }
            if (_lastTicket != null && IsOnTicketBin)
            {
                // Void ticket
                VoidTicket?.Invoke(this, _lastTicket.DataContext as ITicket);
                    
                /*
                 * This validation is done in the HUB
                if (!(_lastTicket.DataContext as Ticket).HasTransferredOrders)
                {
                    // No tiene ordenes transferidas
                    VoidTicket?.Invoke(this, _lastTicket.DataContext as ITicket);
                }
                else ErrorsWindow.ShowErrorsWindow("NotVoidTicketWithTransferredOrder".Translate());
                */
            }
            if (_lastTicket != null && _lastMergeSlot != null)
            {
                // Tenemos que poner el ticket sobre un slot para hacer un merge
                if (_lastMergeSlot == mergeSlot1)
                {
                    // Es sobre el primer slot
                    merge1.Visibility = Visibility.Visible;
                    merge1.DataContext = _lastTicket.DataContext;
                    if (merge1.DataContext == merge2.DataContext)
                    {
                        merge2.DataContext = null;
                        merge2.Visibility = Visibility.Collapsed;
                    }
                }
                else
                {
                    // Es sobre el segundo slot
                    merge2.Visibility = Visibility.Visible;
                    merge2.DataContext = _lastTicket.DataContext;
                    if (merge1.DataContext == merge2.DataContext)
                    {
                        merge1.DataContext = null;
                        merge1.Visibility = Visibility.Collapsed;
                    }
                }
            }
            else if (_lastDownTable != null && _lastTicket != null && _lastTicket.DataContext is ITicket)
            {
                ITicket ticket = _lastTicket.DataContext as ITicket;
                Table table = _lastDownTable.Table;
                // Table prevTable = ticket.Table;

                if (table.Id != ticket.TableId)
                {

                    table = SelectMainTable(table, false);
                    MoveTicketRequest?.Invoke(ticket, table, succeed =>
                    {
                        if (!succeed)
                            return;
                        /*
                        // No estamos moviendo a la misma mesa, podemos mover
                        StandCashierContext.QuickTickets.Remove(ticket);
                        StandCashierContext.AcceptedTickets.Remove(ticket);
                        StandCashierContext.ReturnedTickets.Remove(ticket);

                        Hotel.Instance.MoveTicket(ticket, table, prevTable, false);
                        if (prevTable == null)
                        {
                            Hotel.Instance.AddTicket(ticket);
                        }
                        else
                        {
                            // if (prevTable.Bills.Count == 0) LastExpandedTableButton = null;
                            if (!prevTable.HasTickets) LastExpandedTableButton = null;
                        }
                        */
                    });
                }
            }
            if (_lastDownTable != null) _lastDownTable.IsHover = false;

            _lastDownTable = null;
            _lastTicket = null;
            _lastMergeSlot = null;
            IsOnTicketBin = false;
            _onQuickTicket = false;
            quickTicketsContent.IsSelected = false;
            canvas.Visibility = Visibility.Collapsed;
            canvas.ReleaseMouseCapture();
            ((Storyboard)Resources["ticketBinEmphasys"]).Stop();
        }
        catch (Exception ex)
        {
            ErrorsWindow.ShowErrorWindows(ex);
        }
    }

    private void button_TouchMove(object sender, TouchEventArgs e)
    {
        var currentPointInCanvas = e.GetTouchPoint(moveTablesCanvas).Position;
        var currentPointOutCanvas = e.GetTouchPoint(canvas).Position;
        Canvas.SetLeft(button, currentPointOutCanvas.X - button.ActualWidth / 2);
        Canvas.SetTop(button, currentPointOutCanvas.Y - button.ActualHeight / 2);

        // Verificamos si estamos sobre la papelera de tickets
        var point = button.TranslatePoint(new Point(button.ActualWidth / 2, button.ActualHeight / 2), ticketBinImageGrid);
        if (point.X >= 0 && point.Y >= 0 && point.X <= ticketBinImageGrid.ActualWidth && point.Y <= ticketBinImageGrid.ActualHeight)
            IsOnTicketBin = true;
        else
        {
            // No estamos sobre la papelera
            IsOnTicketBin = false;

            if (IsMerging)
            {
                // Estamos mezclando veamos si estamos sobre algun mergeSlot
                point = button.TranslatePoint(new Point(button.ActualWidth / 2, button.ActualHeight / 2), mergeSlot1);
                if (point.X >= 0 && point.Y >= 0 && point.X <= mergeSlot1.ActualWidth && point.Y <= mergeSlot1.ActualHeight)
                    _lastMergeSlot = mergeSlot1;
                else
                {
                    point = button.TranslatePoint(new Point(button.ActualWidth / 2, button.ActualHeight / 2), mergeSlot2);
                    if (point.X >= 0 && point.Y >= 0 && point.X <= mergeSlot2.ActualWidth && point.Y <= mergeSlot2.ActualHeight)
                        _lastMergeSlot = mergeSlot2;
                    else _lastMergeSlot = null;
                }
            }

            if (_lastMergeSlot != null)
                return; // Estamos sobre un slot de merge no necesitamos seguir buscando

            // No estamos sobre la papelera ni sobre el Merge
            // Verifiquemos si estamos sobre la barra de tickets rapidos 
            point = e.GetTouchPoint(quickTicketsContent).Position;
            _onQuickTicket = point.X >= 0 && point.Y >= 0 &&
                             point.X <= quickTicketsContent.ActualWidth &&
                             point.Y <= quickTicketsContent.ActualHeight;

            if (_onQuickTicket)
            {
                // Estamos sobre la barra de tickets rapidos
                quickTicketsContent.IsSelected = true;
            }
            else
            {
                // No estamos sobre la barra de tickets rapidos
                quickTicketsContent.IsSelected = false;
                // Entonces veamos si estamos sobre alguna mesa

                TableButton currentTable = GetTableButtonDown(currentPointInCanvas);
                if (currentTable != null)
                {
                    var iniPoint = currentTable.TranslatePoint(new Point(0, 0), tablesContainerScroll);
                    var finPoint = currentTable.TranslatePoint(new Point(currentTable.ActualWidth, currentTable.ActualHeight), tablesContainerScroll);
                    if (iniPoint.X > tablesContainerScroll.ActualWidth || iniPoint.Y > tablesContainerScroll.ActualWidth ||
                        finPoint.X < 0 || finPoint.Y < 0)
                        currentTable = null;
                }

                if (_lastDownTable != currentTable)
                {
                    if (_lastDownTable != null)
                    {
                        _lastDownTable.IsHover = false;
                    }
                    if (currentTable != null)
                    {
                        currentTable.IsHover = true;
                    }
                    _lastDownTable = currentTable;
                }
            }// Entonces veamos i estamos sobre alguna mesa
        }
    }

    private void button_MouseMove(object sender, MouseEventArgs e)
    {
        if (_isTouching) return;
        var currentPointInCanvas = e.GetPosition(moveTablesCanvas);
        var currentPointOutCanvas = e.GetPosition(canvas);
        Canvas.SetLeft(button, currentPointOutCanvas.X - button.ActualWidth / 2);
        Canvas.SetTop(button, currentPointOutCanvas.Y - button.ActualHeight / 2);

        // Verificamos si estamos sobre la papelera de tickets
        var point = button.TranslatePoint(new Point(button.ActualWidth / 2, button.ActualHeight / 2), ticketBinImageGrid);
        if (point.X >= 0 && point.Y >= 0 && point.X <= ticketBinImageGrid.ActualWidth && point.Y <= ticketBinImageGrid.ActualHeight)
            IsOnTicketBin = true;
        else
        {
            // No estamos sobre la papelera
            IsOnTicketBin = false;

            if (IsMerging)
            {
                // Estamos mezclando veamos si estamos sobre algun mergeSlot
                point = button.TranslatePoint(new Point(button.ActualWidth / 2, button.ActualHeight / 2), mergeSlot1);
                if (point.X >= 0 && point.Y >= 0 && point.X <= mergeSlot1.ActualWidth && point.Y <= mergeSlot1.ActualHeight)
                    _lastMergeSlot = mergeSlot1;
                else
                {
                    point = button.TranslatePoint(new Point(button.ActualWidth / 2, button.ActualHeight / 2), mergeSlot2);
                    if (point.X >= 0 && point.Y >= 0 && point.X <= mergeSlot2.ActualWidth && point.Y <= mergeSlot2.ActualHeight)
                        _lastMergeSlot = mergeSlot2;
                    else _lastMergeSlot = null;
                }
            }

            if (_lastMergeSlot != null)
                return; // Estamos sobre un slot de merge no necesitamos seguir buscando

            // No estamos sobre la papelera ni sobre el Merge
            // Verifiquemos si estamos sobre la barra de tickets rapidos 
            point = e.GetPosition(quickTicketsContent);
            _onQuickTicket = point.X >= 0 && point.Y >= 0 && point.X <= quickTicketsContent.ActualWidth && point.Y <= quickTicketsContent.ActualHeight;
            if (_onQuickTicket)
            {
                // Estamos sobre la barra de tickets rapidos
                quickTicketsContent.IsSelected = true;
            }
            else
            {
                // No estamos sobre la barra de tickets rapidos
                quickTicketsContent.IsSelected = false;
                // Entonces veamos si estamos sobre alguna mesa
                //var currentTable = (from t in tables
                //                    where t.Value.Contains(currentPointInCanvas)
                //                    select t.Key).FirstOrDefault();
                var currentTable = GetTableButtonDown(currentPointInCanvas);
                if (currentTable != null)
                {
                    var iniPoint = currentTable.TranslatePoint(new Point(0, 0), tablesContainerScroll);
                    var finPoint = currentTable.TranslatePoint(new Point(currentTable.ActualWidth, currentTable.ActualHeight), tablesContainerScroll);
                    if (iniPoint.X > tablesContainerScroll.ActualWidth || iniPoint.Y > tablesContainerScroll.ActualWidth ||
                        finPoint.X < 0 || finPoint.Y < 0)
                        currentTable = null;
                }

                if (_lastDownTable != currentTable)
                {
                    if (_lastDownTable != null)
                    {
                        _lastDownTable.IsHover = false;
                    }
                    if (currentTable != null)
                    {
                        currentTable.IsHover = true;
                    }
                    _lastDownTable = currentTable;
                }
            }
        }
    }

    private void quickTicket_Click(object sender, RoutedEventArgs e)
    {
        //isPressed = false;
        _quickTicketTimer.Change(Timeout.Infinite, 1000);
        canvas.ReleaseMouseCapture();

        if (!_quickTicketTick)
        {
            ((Storyboard)Resources["ticketBinEmphasys"]).Stop();
            TicketClick?.Invoke(this, null, ((Control)sender).DataContext as ITicket);
        }
    }

    private void ticketInTable_Click(object sender, RoutedEventArgs e)
    {
        _ticketInTableTimer.Change(Timeout.Infinite, 1000);
        canvas.ReleaseMouseCapture();
        if (!_ticketInTableTick)
        {
            ((Storyboard)Resources["ticketBinEmphasys"]).Stop();
            TicketClick?.Invoke(this, null, ((Control)sender).DataContext as ITicket);
        }
    }

    private void newTicket_Click(object sender, RoutedEventArgs e)
    {
        MainPage.CurrentInstance.CheckUserWritePermission(Hotel.Instance.CurrentUserModel.Id, Permissions.TicketWithoutTable, (isValid, validUser) =>
        {
            if (isValid)
            {
                if(MainPage.CurrentInstance.Stand.StandRecord.AskPax)
                {
                    var table = new Table(new Guid());
                    BuildOpeningQuestionsDialog(true, table);
                }
                else
                {
                    var ticket = NewTicketRequest(null, 0);
                    if (ticket != null) 
                        StandCashierContext.QuickTickets.Add(ticket);
                }
            }
            else
            {
                ErrorsWindow.ShowErrorsWindow("UserNotAuthorized".Translate());
            }
        });
    }

    /// <summary>
    /// This method is called when the user clicks on the search for a ticket feature
    /// and presses enter on the virtual keyboard, it does not create a new ticket and just 
    /// searches for an existing one, that is why the AskPax and AskRoom parameters are not used here
    /// </summary>
    /// <param name="sender">The sender which is the virtual enter button</param>
    /// <param name="control">The instance of the virtual keyboard component</param>
    public void RunCommandEnter(object sender, object control)
    {
        string filter = NoTableTicketFilterText;
        if (!string.IsNullOrEmpty(filter))
        {
            var ticket = StandCashierContext.QuickTickets
                .Union(StandCashierContext.Stand.Tickets)
                .FirstOrDefault(t => t.ShowDescription && t.Description.ToLowerInvariant().Contains(filter)
                                     || t.Serie.HasValue && t.Serie.ToString().Contains(filter));
            if (ticket != null)
                TicketClick?.Invoke(this, null, ticket);
            else 
                NotificationWindow.ShowNotificationWindow("Not matching ticket", Window.GetWindow(this));
        }
    }

    public void RunCommandTab(object sender, object control)
    {
    }

    /// <summary>
    /// This method is called when the user clicks on the search for a ticket feature
    /// and presses enter on the physical keyboard, it does not create a new ticket and just 
    /// searches for an existing one, that is why the AskPax and AskRoom parameters are not used here
    /// </summary>
    /// <param name="sender">The sender which is the physical keyboard button></param>
    /// <param name="e">The event args that are launched on keydown</param>
    private void TbTicketFilter_OnKeyDown(object sender, KeyEventArgs e)
    {
        string filter = NoTableTicketFilterText;
        if (e.Key == Key.Enter && !string.IsNullOrEmpty(filter))
        {
            var ticket = StandCashierContext.QuickTickets
                .Union(StandCashierContext.Stand.Tickets)
                .FirstOrDefault(t => t.ShowDescription && t.Description.ToLowerInvariant().Contains(filter)
                                     || t.Serie.HasValue && t.Serie.ToString().Contains(filter));
            if (ticket != null)
                TicketClick?.Invoke(this, null, ticket);
            else NotificationWindow.ShowNotificationWindow("Not matching ticket", Window.GetWindow(this));
        }
    }
    
    private void TableFilter_OnKeyDown(object sender, KeyEventArgs e)
    {
       string filter = (sender as TextBox).Text;
        DrawTableButtons(filter);
    }

    private void ActionButton_TouchDown(object sender, TouchEventArgs e)
    {
        if (((sender as ActionButton).DataContext as ITicket).IsEditable)
        {
            BeginTicketBinEmphasysAnimation();
            _lastTicket = sender as ActionButton;
            _isTouching = true;
            _quickTicketTick = false;
            _quickTicketTimer.Change(500, 1000);
            Canvas.SetLeft(button, e.GetTouchPoint(canvas).Position.X - button.ActualWidth / 2);
            Canvas.SetTop(button, e.GetTouchPoint(canvas).Position.Y - button.ActualHeight / 2);
            LastExpandedTableButton = null;
        }
    }

    private void ActionButton_MouseDown(object sender, MouseButtonEventArgs e)
    {
        if (_isTouching) return;
        if (((ITicket)((ActionButton)sender).DataContext).IsEditable)
        {
            BeginTicketBinEmphasysAnimation();
            _lastTicket = sender as ActionButton;
            _quickTicketTick = false;
            _quickTicketTimer.Change(500, 1000);
            Canvas.SetLeft(button, e.GetPosition(canvas).X - button.ActualWidth / 2);
            Canvas.SetTop(button, e.GetPosition(canvas).Y - button.ActualHeight / 2);
            LastExpandedTableButton = null;
        }
    }

    private void TicketInTable_TouchDown(object sender, TouchEventArgs e)
    {
        if (((ITicket)((ActionButton)sender).DataContext).IsEditable)
        {
            BeginTicketBinEmphasysAnimation();
            _lastTicket = (ActionButton)sender;
            _isTouching = true;
            _ticketInTableTick = false;
            _ticketInTableTimer.Change(500, 1000);
            Canvas.SetLeft(button, e.GetTouchPoint(canvas).Position.X - button.ActualWidth / 2);
            Canvas.SetTop(button, e.GetTouchPoint(canvas).Position.Y - button.ActualHeight / 2);
        }
    }

    private void TicketInTable_MouseDown(object sender, MouseButtonEventArgs e)
    {
        if (_isTouching) return;
        if (((ITicket)((ActionButton)sender).DataContext).IsEditable)
        {
            BeginTicketBinEmphasysAnimation();
            _lastTicket = (ActionButton)sender;
            _ticketInTableTick = false;
            _ticketInTableTimer.Change(500, 1000);
            Canvas.SetLeft(button, e.GetPosition(canvas).X - button.ActualWidth / 2);
            Canvas.SetTop(button, e.GetPosition(canvas).Y - button.ActualHeight / 2);
        }
    }

    private void ActionButton_TouchLeave(object sender, TouchEventArgs e)
    {
        if (!_isTouching) return;
        _isTouching = false;
        ((Storyboard)Resources["ticketBinEmphasys"]).Stop();
        _quickTicketTimer.Change(Timeout.Infinite, 1000);
        canvas.ReleaseMouseCapture();
        canvas.Visibility = Visibility.Collapsed;

    }

    private void ActionButton_MouseLeave(object sender, MouseEventArgs e)
    {
        if (_isTouching) return;
        (Resources["ticketBinEmphasys"] as Storyboard).Stop();
        //isPressed = false;
        _quickTicketTimer.Change(Timeout.Infinite, 1000);
        canvas.ReleaseMouseCapture();
        canvas.Visibility = Visibility.Collapsed;

    }

    private void TableButton_OpenTable(TableButton tableButton)
    {
        Table table = tableButton.Table;
        if (!table.MainTable && table.TablesGroup != null && table.TablesGroup.Count > 0)
            table = table.TablesGroup.MainTable;

        if (table.HasBills && table.Bills.Count == 1)
        {
            ((Storyboard)Resources["ticketBinEmphasys"]).Stop();
            if (TicketClick != null)
            {
                if (table.Bills[0].IsEditable)
                {
                    TicketClick(this, null, table.Bills[0]);
                    LastExpandedTableButton = null;
                }
                else if (table.Bills[0].Blocked)
                {
                    MainWindow.Instance.OpenDialog(new TextBlock { Text = "Do you want to unblock this ticket?", FontSize = 14 }, null, () =>
                    {
                        ((Ticket)table.Bills[0]).ForceUnblockTicket();
                    }, null);
                }
            }
        }
    }

    private void SaloonButton_Checked(SaloonButton sender)
    {
        if (_lastSaloonButton != null)
            _lastSaloonButton.IsChecked = false;
        _lastSaloonButton = sender;
        LastExpandedTableButton = null;
    }

    private void TableButton_Expanding(TableButton tableButton)
    {
        SelectMainTable(tableButton.Table);
        LastExpandedTableButton = tableButton;
    }

    private Table SelectMainTable(Table table, bool expandGroup = true)
    {
        try
        {
            if (table.TablesGroup != null)
            {
                // Es un grupo
                // Expandir el resto y seleccionar la mesa principal
                if (expandGroup)
                {
                    foreach (var tab in table.TablesGroup)
                        tab.IsExpanded = true;
                }
                table = table.TablesGroup.MainTable;
            }
        }
        catch
        {
            // ignored
        }

        return table;
    }

    #region Create quick ticket / normal ticket with room and paxs

    private void BuildOpeningQuestionsDialog(bool isQuickTicket, Table table)
    {
        bool OkAction()
        {
            return ValidateTableWithOpeningQuestions(table, isQuickTicket);
        }
        var askRoom = MainPage.CurrentInstance.Stand.StandRecord.AskRoom;
        var askCode = MainPage.CurrentInstance.Stand.StandRecord.AskCode;
        var visual = (FrameworkElement)Resources["PaxDialog"];
                
        if(FindChild<TextBlock>(visual, "RoomTextBlock") is { } RoomTextBlock) 
            RoomTextBlock.Visibility = askRoom ? Visibility.Visible : Visibility.Collapsed;

        if(FindChild<Border>(visual, "RoomBorder") is { } RoomBorder) 
            RoomBorder.Visibility = askRoom ? Visibility.Visible : Visibility.Collapsed;
        
        if(FindChild<TextBlock>(visual, "PrePaidTextBlock") is { } PrePaidTextBlock) 
            PrePaidTextBlock.Visibility = askCode ? Visibility.Visible : Visibility.Collapsed;

        if(FindChild<Border>(visual, "PrePaidBorder") is { } PrePaidBorder) 
            PrePaidBorder.Visibility = askCode ? Visibility.Visible : Visibility.Collapsed;

        WindowHelper.OpenDialog(visual, false, table, OkAction,
            MiddleScreen, DialogButtons.OkCancel, null, null, false);
    }

    /// <summary>
    /// Helper method to validate the room of the ticket to be opened
    /// </summary>
    /// <param name="ticket">The ticket to be validated</param>
    /// <returns>True if it's correct, otherwise it returns false</returns>
    private PmsReservationRecord GetReservation(Ticket ticket)
    {
        try
        {
            var result = Task.Run(() => BusinessProxyRepository.Stand.GetPmsReservationAsync(
                new PmsReservationFilterModel
                {
                    HotelId = ticket.AccountInstallationId,
                    RoomNumber = ticket.Room,
                    CardNumber = ticket.CardNumber,
                    ShowAccountBalance = true,
                    ShowAllGuests = true
                })
            ).Result;

            if(result.HasErrors)
            {
                WindowHelper.ShowError(result.Errors.First().Message);
                return null;
            }
            else if (result.Item != null)
                return result.Item;
            else
                return null;
        }
        catch (Exception ex)
        {
            WindowHelper.ShowError(ex.Message);
            return null;
        }
    }
    
    /// <summary>
    /// Method to validate the table with the paxs and the room
    /// </summary>
    /// <param name="table">The table to be validated</param>
    /// <returns>True if it's correct or false if it's not</returns>
    private bool ValidateTableWithOpeningQuestions(Table table, bool isQuickTicket)
    {
        var ticket = NewTicketRequest(isQuickTicket ? null : table, table.PaxSeated);

        if (ticket == null)
        {
            WindowHelper.ShowError("ErrorCreatingTicket".Translate());
            return false;
        }
        
        if (string.IsNullOrEmpty(table.Room) && string.IsNullOrEmpty(table.PrePaidCode))
            return true;

        ticket.AccountInstallationId = Hotel.Instance.Id;
        ticket.Room = table.Room;
        ticket.CardNumber = table.PrePaidCode;
        var reservation = GetReservation(ticket);

        if (!ValidateReservation(reservation, table))
            return false;

        if (!string.IsNullOrEmpty(table.Room))
            ticket = AddReservationToTicket(ticket, reservation);

        if (!string.IsNullOrEmpty(table.PrePaidCode))
        {
            ticket.AccountBalance = reservation.Balance ?? 0;
            ticket.CardNumber = reservation.CardNumbers;
            ticket.Name = reservation.Guests;
        }
        CreateTicketWithOpeningQuestions(ticket, isQuickTicket);
        return true;
    }

    private bool ValidateReservation(PmsReservationRecord reservation, Table table)
    {
        if (reservation is null)
        {
            WindowHelper.ShowError("No reservation found");
            return false;
        }

        if (!string.IsNullOrEmpty(table.Room) && (reservation.Room != table.Room || string.IsNullOrEmpty(reservation.Room)))
        {
            WindowHelper.ShowError("NoRoom".Translate());
            return false;
        }

        if (!string.IsNullOrEmpty(table.PrePaidCode))
        {
            if (reservation.CardNumbers != table.PrePaidCode || string.IsNullOrEmpty(reservation.CardNumbers))
            {
                WindowHelper.ShowError("NoPrePaidFound".Translate());
                return false;
            }

            if(reservation.Balance is 0 or null)
            {
                WindowHelper.ShowError("PrePaidBalanceEmpty".Translate());
                return false;
            }

            if (reservation.Balance > 0)
            {
                WindowHelper.ShowError($"Balance {reservation.Balance}, is higher than 0, please pay the debt before using this balance");
                return false;
            }
        }

        return true;
    }

    private Ticket AddReservationToTicket(Ticket ticket, PmsReservationRecord reservation)
    {
        ticket.AccountInstallationId = null;
        ticket.Room = reservation.Room;
        ticket.Account = reservation.AccountId;
        ticket.FiscalDocInfoName = reservation.Guests;
        ticket.AccountType = CurrentAccountType.Reservation;
        ticket.AllowRoomCredit = reservation.AllowCreditPos;
        var accountDescription = "";
        accountDescription += $"{reservation.ReservationNumber} ({reservation.Room})";
        accountDescription += $"\n{reservation.Guests}";
        if (!string.IsNullOrEmpty(reservation.Company))
            accountDescription += $"\n{reservation.Company}";
        ticket.AccountDescription = accountDescription;

        if(!string.IsNullOrEmpty(ticket.CardNumber))
        {
            ticket.AccountBalance = reservation.Balance ?? 0;
            ticket.CardNumber = reservation.CardNumbers;
            ticket.Name = reservation.Guests;
        }
        return ticket;
    }

    /// <summary>
    /// Method to create a ticket when the user is inserting with a room 
    /// It populates the information from the reservation
    /// </summary>
    /// <param name="reservation">The reservation to be passed has information in the ticket</param>
    /// <param name="ticket">The ticket to be added/created to Observable collection of tickets</param>
    private void CreateTicketWithOpeningQuestions(Ticket ticket, bool isQuickTicket, ReservationRecord reservation = null)
    {
        if(isQuickTicket)
            StandCashierContext.QuickTickets.Add(ticket);
        else
            Hotel.Instance.CurrentStandCashierContext.Stand.AddTicket(ticket);
        
        if(!string.IsNullOrEmpty(ticket.CardNumber))
        {
            var content = new TextBlock { Text = $"{"PrePaidGuestName".Translate()}: {ticket.Name} \n{"PrePaidCode".Translate()}: {ticket.CardNumber} \n{"PrePaidBalance".Translate()}: {ticket.AccountBalance}" };
            WindowHelper.OpenDialog(content, false, ticket, null, MiddleScreen, DialogButtons.CancelOnly);
        }
        
        TicketClick(this, null!, ticket);
        LastExpandedTableButton = null;
    }

    #endregion

    private void TableButton_NewTicket(TableButton tableButton)
    {
        var table = SelectMainTable(tableButton.Table, false);
        if (table.IsReserved)
        {
            var hour = table.ReservationTime?.ToString("h:mm tt", CultureInfo.InvariantCulture);
            //TODO Implement traduction
            var message = $"The current table is reserved for {hour}. \nAre you sure you want to create a ticket?";

            table.PaxSeated = 1;
            bool OkAction()
            {
                CreateTicketInTable(table);
                return true;
            }

            MainPage.CurrentInstance.OpenDialog(message, OkAction);
        }
        else
        {
            if (MainPage.CurrentInstance.Stand.StandRecord.AskPax)
            {
                table.PaxSeated = table.Paxs;
                BuildOpeningQuestionsDialog(false, table);
            }
            else
                CreateTicketInTable(table);
        }
    }

    private static T FindChild<T>(DependencyObject parent, string childName) where T : DependencyObject
    {
        if (parent == null) 
            return null;

        var childrenCount = VisualTreeHelper.GetChildrenCount(parent);
        for (var i = 0; i < childrenCount; i++)
        {
            var child = VisualTreeHelper.GetChild(parent, i);
            if (child is T childType && ((FrameworkElement)child).Name == childName)
                return childType;
            
            var foundChild = FindChild<T>(child, childName);
            if (foundChild != null)
                return foundChild;
        }
        return null;
    }

    private void CreateTicketInTable(Table table)
    {
        var ticket = NewTicketRequest(table, table.PaxSeated);
        if (ticket == null) 
            return;
        
        // Assign the room to the ticket and the HotelId
        ticket.AccountInstallationId = Hotel.Instance.Id;
        ticket.Room = table.Room;
        // We create a ticket in the table, even if it does not have a series
        Hotel.Instance.CurrentStandCashierContext.Stand.AddTicket(ticket);
        TicketClick(this, null!, ticket);
        LastExpandedTableButton = null;
    }

    private void mainGrid_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
        LastExpandedTableButton = null;
    }

    private void SaloonButton_Loaded(object sender, RoutedEventArgs e)
    {
        if (_firstSaloonLoaded == null)
        {
            _firstSaloonLoaded = (SaloonButton)sender;
            _firstSaloonLoaded.IsChecked = true;
        }
    }

    private void AnimateChangeOfZoom(double newValue)
    {
        BeginChangeZoomValueAnimation(newValue);
    }

    private void positionGrid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
        try
        {
            _movingTablePressed = true;
            var currentPointInCanvas = e.GetPosition(moveTablesCanvas);

            var selected = new Point(currentPointInCanvas.X, currentPointInCanvas.Y);

            var currentTable = GetTableButtonDown(selected);

            if (currentTable != null)
            {
                tablesContainer.Children.Remove(_floatingTable = currentTable);
                moveTablesCanvas.Children.Add(_floatingTable);

                Canvas.SetLeft(_floatingTable, currentPointInCanvas.X - _floatingTable.ActualWidth / 2);
                Canvas.SetTop(_floatingTable, currentPointInCanvas.Y - _floatingTable.ActualHeight / 2);

                _iniRow = (_floatingTable.DataContext as Table).Row.Value;
                _iniColumn = (_floatingTable.DataContext as Table).Column.Value;

                SetRectanglesAsNoBusy(_floatingTable.DataContext as Table);
                SetRectanglesAsGranted(_floatingTable.DataContext as Table);
                _isGranted = false;
            }
        }
        catch (Exception ex)
        {
            Logger.TraceError("Error cogiendo mesa para mover: " + ex.Message);
        }
    }

    private void positionGrid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    {
        try
        {
            if (_movingTablePressed)
            {
                if (_floatingTable != null)
                {
                    moveTablesCanvas.Children.Clear();
                    if (_isGranted)
                        SetTablePosition(_floatingTable);
                    else SetTablePosition(_floatingTable, _iniRow, _iniColumn);

                }
                _floatingTable = null;
                _movingTablePressed = false;
            }
        }
        catch
        {
            positionGrid_MouseLeave(sender, null);
        }
    }

    private void positionGrid_MouseLeave(object sender, MouseEventArgs e)
    {
        try
        {
            if (_movingTablePressed)
            {
                if (_floatingTable != null)
                {
                    moveTablesCanvas.Children.Clear();
                    SetTablePosition(_floatingTable, _iniRow, _iniColumn);
                }
                _floatingTable = null;
                _movingTablePressed = false;
            }
        }
        catch (Exception ex)
        {
            Logger.TraceError("Error moviendo una mesa, [Leave]: " + ex.Message);
        }
    }

    private void positionGrid_MouseMove(object sender, MouseEventArgs e)
    {
        try
        {
            if (_floatingTable != null)
            {
                var currentPointInCanvas = e.GetPosition(moveTablesCanvas);
                Canvas.SetLeft(_floatingTable, currentPointInCanvas.X - _floatingTable.ActualWidth / 2);
                Canvas.SetTop(_floatingTable, currentPointInCanvas.Y - _floatingTable.ActualHeight / 2);

                SetRectanglesAsNormal(_floatingTable.DataContext as Table);

                ((Table)_floatingTable.DataContext).Row = (short)Math.Round((currentPointInCanvas.Y - _floatingTable.ActualHeight / 2) /
                                                                            (moveTablesCanvas.ActualHeight / positionGrid.Rows), 0);
                ((Table)_floatingTable.DataContext).Column = (short)Math.Round((currentPointInCanvas.X - _floatingTable.ActualWidth / 2) /
                                                                               (moveTablesCanvas.ActualWidth / positionGrid.Columns), 0);
                _isGranted = !IsBusy((Table)_floatingTable.DataContext);
                if (_isGranted) SetRectanglesAsGranted((Table)_floatingTable.DataContext);
                else SetRectanglesAsDenied((Table)_floatingTable.DataContext);
            }
        }
        catch (Exception ex)
        {
            Logger.TraceError("Error moviendo una mesa, [Move]: " + ex.Message);
        }
    }

    private void mergeOk_Click(object sender, RoutedEventArgs e)
    {
        if (MergeTicketsClick != null && merge1.DataContext is ITicket && merge2.DataContext is ITicket)
            MergeTicketsClick(this, (ITicket)merge1.DataContext, (ITicket)merge2.DataContext);
        ResetMergeSlots();
    }

    private void mergeCancel_Click(object sender, RoutedEventArgs e)
    {
        ResetMergeSlots();
    }

    private void ActionButton_MouseLeave_1(object sender, MouseEventArgs e)
    {
        if (_isTouching) return;
        if (!_quickTicketTick)
        {
            _quickTicketTimer.Change(Timeout.Infinite, 1000);
            _lastDownTable = null;
            _lastTicket = null;
            _lastMergeSlot = null;
            IsOnTicketBin = false;
            canvas.ReleaseMouseCapture();
            (Resources["ticketBinEmphasys"] as Storyboard).Stop();
        }
    }

    private void TicketInTable_MouseLeave_1(object sender, MouseEventArgs e)
    {
        if (_isTouching) return;
        if (!_ticketInTableTick)
        {
            _ticketInTableTimer.Change(Timeout.Infinite, 1000);
            _lastDownTable = null;
            _lastTicket = null;
            _lastMergeSlot = null;
            IsOnTicketBin = false;
            canvas.ReleaseMouseCapture();
            ((Storyboard)Resources["ticketBinEmphasys"]).Stop();
        }
    }

    #region MovingTables

    private List<Border> GetRectangles(Table table, bool noNull = true)
    {
        List<Border> ret = new List<Border>();

        for (int r = Math.Max(table.Row.Value, (short)0); r < table.Row + table.Height && r < positionGrid.Rows; r++)
        {
            for (int c = Math.Max(table.Column.Value, (short)0); c < table.Column + table.Width && c < positionGrid.Columns; c++)
                ret.Add(positionGrid.Children[r * positionGrid.Columns + c] as Border);
        }
        if (!noNull && (table.Row.Value < 0 || table.Row + table.Height > positionGrid.Rows || table.Column < 0 || table.Column + table.Width > positionGrid.Columns))
            return null;

        return ret;
    }

    private void SetRectanglesAsBusy(Table table)
    {
        foreach (var rect in GetRectangles(table))
            rect.Opacity = 0.1;
    }
    public void SetRectanglesAsNoBusy(Table table)
    {
        if (table == null)
            foreach (Border rect in positionGrid.Children)
                rect.Opacity = 1;
        else
            foreach (var rect in GetRectangles(table))
                rect.Opacity = 1;
    }
    private void SetRectanglesAsDenied(Table table)
    {
        foreach (var rect in GetRectangles(table))
            rect.Background = Brushes.Red;
    }
    private void SetRectanglesAsNormal(Table table)
    {
        if (table == null)
            foreach (Border rect in positionGrid.Children)
                rect.Background = Brushes.LightGray;
        else
            foreach (var rect in GetRectangles(table))
                rect.Background = Brushes.LightGray;
    }
    private void SetRectanglesAsGranted(Table table)
    {
        foreach (var rect in GetRectangles(table))
            rect.Background = Brushes.LightGreen;
    }
    private bool IsBusy(Table table)
    {
        var rects = GetRectangles(table, false);
        if (rects == null)
            return true;

        foreach (var rect in rects)
        {
            if (rect.Opacity != 1)
                return true;
        }
        return false;
    }

    public void SetTablePosition(TableButton tableButton, short? row = null, short? column = null)
    {

        var table = tableButton.DataContext as Table;
        if (row.HasValue || column.HasValue)
        {
            SetRectanglesAsNormal(table);
            if (row.HasValue) table.Row = row.Value;
            if (column.HasValue) table.Column = column.Value;
        }

        if (!tablesContainer.Children.Contains(tableButton))
            tablesContainer.Children.Add(tableButton);

        Grid.SetRowSpan(tableButton, (int)table.Height);
        Grid.SetColumnSpan(tableButton, (int)table.Width);
        Grid.SetRow(tableButton, (int)table.Row);
        Grid.SetColumn(tableButton, (int)table.Column);

        SetRectanglesAsBusy(table);
        SetRectanglesAsNormal(table);

        tablesContainer.UpdateLayout();
        //if (persistAfter)
        //	table.PersistObject();
    }

    #endregion

    #endregion

    #endregion
    #region Enums

    public enum TablesViewerStatus { Unknown, Normal, Moving, Picking }


    #endregion

    #endregion
    #region Tables Selection
    #region Variables

    private bool _canChangeSelection;
    private ICollectionView _noTableTickets;
    private ICollectionView _tables;
    #endregion
    #region Events

    public event Action<Table[]> SelectionEnd;

    public event Action SaloonLoaded;

    #endregion
    #region Properties

    public bool SelectionMultiple { get; set; } = true;

    public bool CanChangeSelection
    {
        get => _canChangeSelection;
        set
        {
            _canChangeSelection = value;
            OnPropertyChanged(nameof(CanChangeSelection));
        }
    }

    public IEnumerable<Table> SelectedTables
    {
        get
        {
            var selectedTables = new List<Table>();
            foreach (var saloon in Saloons)
                selectedTables.AddRange(saloon.Tables.Where(table => table.IsSelected));
            return selectedTables;
        }
    }

    public AvailabilityFilter AvailabilityFilter { get; set; }

    #endregion
    #region Methods

    public void DeselectAllTables()
    {
        foreach (var saloon in Saloons)
        {
            foreach (var table in saloon.Tables)
                table.IsSelected = false;
        }
    }

    public void SelectTable(Table table)
    {
        table.IsSelected = true;
    }

    private void DrawTablesToPick()
    {
        DrawLiteTableButtons();
        UpdateAvailabilityLayout();
    }

    private void UpdateAvailabilityLayout()
    {
        if (!HasSaloonSelected)
            return;
        foreach (Table table in SelectedSaloon.Tables)
        {
            table.IsAvailable = AvailabilityFilter == null || (AvailabilityFilter.Reservation != null
                ? table.Available(AvailabilityFilter.Reservation)
                : table.Available(AvailabilityFilter.AvailabilityPaxs));
        }
    }

    private void AcceptSelection_Click(object sender, RoutedEventArgs e)
    {
        var selection = SelectedTables.ToArray();
        if (selection != null && selection.Length > 0)
            SelectionEnd?.Invoke(selection);
        else
            NotificationWindow.ShowNotificationWindow("Please select any table");
    }

    private void OnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
    {
        switch (propertyChangedEventArgs.PropertyName)
        {
            case nameof(SelectedSaloon):
                OnSelectedSaloonChanged();
                break;
            case nameof(SaloonZoom):
                if (SaloonZoomChangued != null)
                    SaloonZoomChangued(SaloonZoom);
                break;
        }
    }

    private async void OnSelectedSaloonChanged()
    {
        Visibility = HasSaloonSelected ? Visibility.Visible : Visibility.Collapsed;

        if (HasSaloonSelected)
        {
            switch (State)
            {
                case TablesViewerStatus.Picking:
                    DrawTablesToPick();
                    break;
                case TablesViewerStatus.Normal:
                    await LoadTicketsInTables();
                    DrawTableButtons("");
                    break;
            }

            if (SaloonLoaded != null)
                SaloonLoaded();
        }
    }

    private async Task LoadTicketsInTables()
    {
        try
        {
            tablesOutContainer.DataContext = _lastSaloonButton?.DataContext;
            tablesOutContainer.UpdateLayout();

            var context = StandCashierContext;
            if (context != null)
            {
                await context.UpdateTables();
                /*
                // var contracts = await BusinessProxyRepository.Ticket.GetTicketByStandCashierAsync(context.Stand.Id, context.Cashier.Id);
                var contracts = await BusinessProxyRepository.Ticket.GetTicketInfoByStandCashierAsync(context.Stand.Id, context.Cashier.Id);

                context.QuickTickets.Clear();
                context.AcceptedTickets.Clear();
                context.ReturnedTickets.Clear();

                Dictionary<Guid?, TicketSlimList> slimTickets = new Dictionary<Guid?, TicketSlimList>();
                var quickTickets = new TicketSlimList();
                var acceptedTickets = new TicketSlimList();
                foreach(var ticket in contracts.Select(x => new TicketSlim(x)))
                {
                    if (ticket.Mesa.HasValue)
                    {
                        if (!slimTickets.TryGetValue(ticket.Mesa, out var ticketSlimList))
                        {
                            ticketSlimList = new TicketSlimList();
                            slimTickets[ticket.Mesa] = ticketSlimList;
                        }
                        ticketSlimList.Add(ticket);
                    }
                    else
                    {
                        if (ticket.IsAcceptedAsTransfer)
                            acceptedTickets.Add(ticket);
                        else
                            quickTickets.Add(ticket);
                    }
                }

                foreach (var table in context.Stand.Saloons.SelectMany(x => x.Tables))
                {
                    slimTickets.TryGetValue(table.Id, out var ticketSlimList);
                    table.Tickets = new ObservableCollection<TicketSlim>(ticketSlimList ?? Enumerable.Empty<TicketSlim>());
                }

                context.QuickTickets.Append(quickTickets);
                context.AcceptedTickets.Append(acceptedTickets);

                return;
                */
                /*
                var tickets = new ObservableCollection<Ticket>();
                foreach (var contract in contracts)
                {
                    if (!contract.IsAnul && !contract.ClosedTime.HasValue)
                    {
                        var ticket = new Ticket(contract);
                        ticket.Table?.ReplaceOrAddTicket(ticket);
                        tickets.Add(ticket);

                        if (!contract.Mesa.HasValue)
                        {
                            if (contract.IsAcceptedAsTransfer)
                                context.AcceptedTickets.Add(ticket);
                            else
                                context.QuickTickets.Add(ticket);
                        }
                    }
                }

                context.Stand.Tickets = tickets;
                /* */
            }
        }
        catch (Exception e)
        {
            ErrorsWindow.ShowErrorsWindow(e.GetBaseException().Message);
            MainWindow.Instance.ChangeUserStandRequested(LoginControl.ResetMode.Total);
        }
    }

    private void AuxLiteTableButton_IsSelectedChanged(LiteTableButton liteTable, bool isSelected)
    {
        if (isSelected)
        {
            if (!liteTable.Table.IsAvailable)
            {
                NotificationWindow.ShowNotificationWindow("TableIsNotAvailable".Translate());
                liteTable.Table.IsSelected = false;
            }
            else if (!SelectionMultiple)
            {
                foreach (var table in SelectedSaloon.Tables.Where(t => t != liteTable.Table))
                    table.IsSelected = false;
            }
        }
    }

    private void OpenDetailReservation_OnClick(object sender, RoutedEventArgs e)
    {
        var reservation = (LiteTablesReservation)((Control)sender).DataContext;
        var view = new ReservationTableDetailView { Reservation = reservation };
        MainPage.CurrentInstance.OpenInfoDialog(view);
    }

    private void SaloonsLbx_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        SelectedSaloon = (Saloon)((ListBox)sender).SelectedItem;
        OnPropertyChanged(nameof(SelectedSaloon));
    }

    private void userControl_Loaded(object sender, RoutedEventArgs e)
    {

    }

    private void OnSelectedSaloonAssgined(Saloon oldValue, Saloon newValue)
    {
        if (oldValue == newValue && State == TablesViewerStatus.Picking)
            UpdateAvailabilityLayout();
    }

    #endregion

    #endregion
}

public class AvailabilityFilter
{
    public short AvailabilityPaxs { get; set; }
    public LiteTablesReservation Reservation { get; set; }
}