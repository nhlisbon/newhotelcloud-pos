﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using NewHotel.WPF.App.Pos.Model;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for PreviosLoguedUsersPicker.xaml
    /// </summary>
    public partial class PreviosLoguedUsersPicker : UserControl
    {
        private bool _lockEvents;
        private readonly List<CheckBox> _userCheckBoxes = new List<CheckBox>();

        public PreviosLoguedUsersPicker()
        {
            InitializeComponent();
        }

        public User UserSelected { get; set; }

        private void UserCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (!_lockEvents)
            {
                _lockEvents = true;

                var cSender = (CheckBox) sender;
                UserSelected = (User)cSender.DataContext;

                foreach (var item in _userCheckBoxes)
                {
                    if (!item.Equals(sender))
                        item.IsChecked = false;
                }

                _lockEvents = false;
            }
        }

        private void UserCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!_lockEvents)
            {
                _lockEvents = true;
                ((CheckBox) sender).IsChecked = true;
                _lockEvents = false;
            }
        }

        private void UserCheckBox_Loaded(object sender, RoutedEventArgs e)
        {
            _lockEvents = true;
            var cSender = (CheckBox)sender;
            if ((cSender.DataContext as User) == UserSelected)
                cSender.IsChecked = true;
            _userCheckBoxes.Add(cSender);
            _lockEvents = false;
        }
    }
}
