﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using NewHotel.WPF.App.Pos.Model;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for CancellationReasonPicker.xaml
    /// </summary>
    public partial class CancellationReasonPicker : UserControl, INotifyPropertyChanged
    {
        private string _comments;
        private bool _createQuickTicket = false;
        private CancellationReason _cancellationReasonSelected;

        public event PropertyChangedEventHandler PropertyChanged;

        public CancellationReasonPicker()
        {
            InitializeComponent();
        }

        public CancellationReason[] CancellationReasons => Hotel.Instance.CancellationReasons.ToArray();

        public CancellationReason CancellationReasonSelected
        {
            get => _cancellationReasonSelected;
            set
            {
                if (_cancellationReasonSelected == value) return;
                _cancellationReasonSelected = value;
                OnPropertyChanged(nameof(CancellationReasonSelected));
            }
        }
        public string Comments
        {
            get => _comments;
            set
            {
                if (_comments == value) return;
                _comments = value;
                OnPropertyChanged(nameof(Comments));
            }
        }

        public bool CreateQuickTicket
        {
            get => _createQuickTicket;
            set
            {
                if (_createQuickTicket == value) return;
                _createQuickTicket = value;
                OnPropertyChanged(nameof(CreateQuickTicket));
            }
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void CancellationReasonPicker_OnLoaded(object sender, RoutedEventArgs e)
        {
            CancellationReasonSelected = Hotel.Instance.DefaultMergeTicketCancellationReason.HasValue
                ? Hotel.Instance.CancellationReasons.FirstOrDefault(x => x.Id == Hotel.Instance.DefaultMergeTicketCancellationReason.Value)
                : CancellationReasons.FirstOrDefault();
        }

        public void HandleChecked(object sender, RoutedEventArgs e)
        {
            CreateQuickTicket = true;
        }
        public void HandleUnchecked(object sender, RoutedEventArgs e)
        {
            CreateQuickTicket = false;
        }

    }
}