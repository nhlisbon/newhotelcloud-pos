﻿using NewHotel.WPF.App.Pos.Model;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace NewHotel.WPF.App.Pos.Controls
{
    public partial class SaloonButton : UserControl
    {
        #region Variables + Constructor

        private bool _isPressed;

        public SaloonButton()
        {
            InitializeComponent();
        }

        #endregion
        #region Events

        public event Action<SaloonButton> Checked;
        public event Action<SaloonButton> Unchecked;

        #endregion

        #region Properties

        public bool IsChecked
        {
            get { return (bool)GetValue(IsCheckedProperty); }
            set { SetValue(IsCheckedProperty, value); }
        }

        public static readonly DependencyProperty IsCheckedProperty =
            DependencyProperty.Register(nameof(IsChecked), typeof(bool), typeof(SaloonButton), new PropertyMetadata(OnIsCheckedPropertyChangued));

        private static void OnIsCheckedPropertyChangued(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (SaloonButton)d;
            if (control.IsChecked)
            {
                control.Checked?.Invoke(control);
                control.BeginCheckingAnimation();
            }
            else
            {
                control.Unchecked?.Invoke(control);
                control.BeginUnCheckingAnimation();
            }
        }

        #endregion
        #region Methods

        private void checkBox_Loaded(object sender, RoutedEventArgs e)
        {
            BeginLoadingAnimation();
        }

        private void checkBox_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _isPressed = true;
        }

        private void checkBox_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (_isPressed)
            {
                if (!IsChecked)
                {
                    IsChecked = true;
                    return;
                }
                _isPressed = false;
            }
        }

        private void checkBox_MouseLeave(object sender, MouseEventArgs e)
        {
            _isPressed = false;
        }

        #region Aux

        private void BeginLoadingAnimation()
        {
            ((Storyboard)Resources["Loading"]).Begin();
        }

        private void BeginUnCheckingAnimation()
        {
            ((Storyboard)Resources["UnChecking"]).Begin();
        }

        private void BeginCheckingAnimation()
        {
            ((Storyboard)Resources["Checking"]).Begin();
        }

        #endregion


        #endregion
    }
}
