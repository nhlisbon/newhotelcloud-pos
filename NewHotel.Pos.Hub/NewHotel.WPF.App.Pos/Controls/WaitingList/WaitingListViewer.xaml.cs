﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using NewHotel.Contracts;
using NewHotel.Pos.Localization;
using NewHotel.WPF.App.Pos.Controls.WaitingList;
using NewHotel.WPF.App.Pos.Dialogs;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.App.Pos.Model.WaitingListStand;
using NewHotel.WPF.Common;
using NewHotel.WPF.Common.Controls.Busy;
using NewHotel.WPF.Common.Notifications;
using NewHotel.WPF.Model.Model;
using ValidationResult = NewHotel.Contracts.ValidationResult;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for WaitingListStand.xaml
    /// </summary>
    public partial class WaitingListViewer : UserControl, INotifyPropertyChanged
    {
        #region Variables + Constructor

        private WaitingListFilter _filter;
        private ObservableCollection<PaxWaitingList> _waitingList;
        private PaxWaitingList _paxWaitingListSelected;
        private TablesViewerRemoteControl _remoteControl;

        public WaitingListViewer()
        {
            InitializeComponent();
            DataContext = this;
            Filter = new WaitingListFilter();
        }

        #endregion
        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        public event Func<PaxWaitingList, Task<ValidationContractResult>> PersistPaxWaitingListRequest;

        public event Func<PaxWaitingList, Task<ValidationResult>> RemovePaxWaitingListRequest;

        public event Func<WaitingListFilter, Task<ValidationItemSource<PaxWaitingList>>> LoadDataSourceRequest;

        public event Action<PaxWaitingList, Table[]> CreateTicketRequest;

        #endregion
        #region Properties

        public WaitingListFilter Filter
        {
            get { return _filter; }
            set
            {
                _filter = value;
                OnPropertyChanged(nameof(Filter));
            }
        }

        public PaxWaitingList PaxWaitingListSelected
        {
            get { return _paxWaitingListSelected; }
            set
            {
                _paxWaitingListSelected = value;
                OnPropertyChanged(nameof(PaxWaitingListSelected));
            }
        }

        public ObservableCollection<PaxWaitingList> WaitingList
        {
            get { return _waitingList; }
            set
            {
                _waitingList = value;
                OnPropertyChanged(nameof(WaitingList));
            }
        }

        public ObservableCollection<Stand> Stands
        {
            get { return Hotel.Instance.Stands; }
        }

        public ObservableCollection<Saloon> Saloons { get; set; }

        #endregion
        #region Methods

        #region Public

        public void ResetView()
        {
            GoToStartView();
            ReleaseRemoteControl();
        }

        public void BindRemoteControl(TablesViewerRemoteControl remoteControl)
        {
            _remoteControl = remoteControl;
            _remoteControl.Bind(TablesPicker);
            TablesPicker.ZoomToSaloon();
        }

        public void ReleaseRemoteControl()
        {
            _remoteControl.Unbind();
        }

        public void GoToStartView()
        {
            ShowWaitingList();
            HideTablesPicker();
        }

        public async void UpdateData()
        {
            try
            {
                if (LoadDataSourceRequest != null)
                {
                    BusyControl.Instance.Show("~Loading~...");

                    ValidationItemSource<PaxWaitingList> resultValidation = await LoadDataSourceRequest(Filter);
                    if (resultValidation.IsEmpty)
                        WaitingList = new ObservableCollection<PaxWaitingList>(resultValidation.ItemSource);
                    else ErrorsWindow.ShowErrorWindows(resultValidation);
                }
            }
            catch (Exception exception)
            {
                ErrorsWindow.ShowErrorWindows(exception);
            }
            finally
            {
                if (LoadDataSourceRequest != null)
                    BusyControl.Instance.Close();
            }
        }

        #endregion
        #region Event Handler

        private void LoadDataSource_Click(object sender, RoutedEventArgs e)
        {
            UpdateData();
        }

        private void NewPaxWaitingList_Click(object sender, RoutedEventArgs e)
        {
            var manager = new PaxWaitingListManager();
            manager.PaxWaitingList = new PaxWaitingList();
            Func<bool> okAction = () =>
            {
                if (manager.PaxWaitingList.IsValid)
                {
                    Persist(manager.PaxWaitingList);
                    return true;
                }
                else
                {
                    NotificationWindow.ShowNotificationWindow("FillName".Translate());
                    return false;
                }
            };
            MainPage.CurrentInstance.OpenDialog(manager, okAction);
        }

        private void RemovePaxWaitingList_Click(object sender, RoutedEventArgs e)
        {
            if (PaxWaitingListSelected == null)
            {
                NotificationWindow.ShowNotificationWindow("ExpectedPickup".Translate());
                return;
            }
            Func<bool> okAction = () =>
            {
                Remove(PaxWaitingListSelected);
                return true;
            };
            MainPage.CurrentInstance.OpenDialog(new TextBlock() { Text = "AreYouSure".Translate(), FontSize = 16 }, okAction);
        }

        private void ProcessPaxWaitingList_Click(object sender, RoutedEventArgs e)
        {
            if (PaxWaitingListSelected == null)
            {
                NotificationWindow.ShowNotificationWindow("ExpectedPickup".Translate());
                return;
            }
            ShowTablesPicker();
            HideWaitingList();
        }

        private void TablesPicker_OnSelectionEnd(Table[] tables)
        {
            GoToStartView();
            OnCreateTicketRequest(PaxWaitingListSelected, tables);
        }

        private void CleanStandFilter_OnClick(object sender, RoutedEventArgs e)
        {
            Filter.StandId = null;
        }

        private void CleanSaloonFilter_OnClick(object sender, RoutedEventArgs e)
        {
            Filter.SaloonId = null;
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual void OnCreateTicketRequest(PaxWaitingList paxWaitingList, Table[] tables)
        {
            if (CreateTicketRequest != null)
                CreateTicketRequest(paxWaitingList, tables);
        }

        #endregion
        #region Aux

        private void ShowTablesPicker()
        {
            TablesPicker.Visibility = Visibility.Visible;
            TablesPicker.Saloons = Saloons;
            TablesPicker.DeselectAllTables();


            TablesPicker.AvailabilityFilter = new AvailabilityFilter
            {
                AvailabilityPaxs = PaxWaitingListSelected.Paxs
            };

            TablesPicker.SelectedSaloon = Saloons.FirstOrDefault();
            TablesPicker.SelectionMultiple = false;
            TablesPicker.CanChangeSelection = true;
        }

        private void HideTablesPicker()
        {
            TablesPicker.Visibility = Visibility.Collapsed;
        }

        private void ShowWaitingList()
        {
            WaitingListContainer.Visibility = Visibility.Visible;
        }

        private void HideWaitingList()
        {
            WaitingListContainer.Visibility = Visibility.Collapsed;
        }

        private async void Persist(PaxWaitingList item)
        {
            try
            {
                BusyControl.Instance.Show("~Saving~...");
                if (PersistPaxWaitingListRequest != null)
                {
                    item.RegisterDate = DateTime.Now;
                    ValidationContractResult result = await PersistPaxWaitingListRequest(item);
                    if (result.IsEmpty)
                    {
                        item.DataContext = result.Contract;
                        WaitingList.Add(item);
                    }
                    else ErrorsWindow.ShowErrorWindows(result);
                }
            }
            catch (Exception e)
            {
                ErrorsWindow.ShowErrorWindows(e);
            }
            finally
            {
                BusyControl.Instance.Close();
            }
        }

        private async void Remove(PaxWaitingList item)
        {
            try
            {
                BusyControl.Instance.Show("~Deleting~...");
                if (RemovePaxWaitingListRequest != null)
                {
                    ValidationResult result = await RemovePaxWaitingListRequest(item);
                    if (result.IsEmpty)
                        WaitingList.Remove(item);
                    else ErrorsWindow.ShowErrorWindows(result);
                }
            }
            catch (Exception e)
            {
                ErrorsWindow.ShowErrorWindows(e);
            }
            finally
            {
                BusyControl.Instance.Close();
            }
        }

        #endregion

        #endregion
    }
}