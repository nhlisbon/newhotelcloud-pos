﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.App.Pos.Model.WaitingListStand;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls.WaitingList
{
    /// <summary>
    /// Interaction logic for PaxWaitingListManager.xaml
    /// </summary>
    public partial class PaxWaitingListManager : UserControl, INotifyPropertyChanged
    {
        #region Variables + Constructor

        private PaxWaitingList _paxWaitingList;

        public PaxWaitingListManager()
        {
            InitializeComponent();
        }

        #endregion
        #region Properties + Events

        public PaxWaitingList PaxWaitingList
        {
            get { return _paxWaitingList; }
            set
            {
                _paxWaitingList = value;
                OnPropertyChanged(nameof(PaxWaitingList));
            }
        }

        public ObservableCollection<Stand> Stands
        {
            get { return Hotel.Instance.Stands; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
        #region Event Handler

        private void CleanSaloon_OnClick(object sender, RoutedEventArgs e)
        {
            PaxWaitingList.SaloonId = null;
        }

        private void CleanStand_OnClick(object sender, RoutedEventArgs e)
        {
            PaxWaitingList.StandId = null;
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
