﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Controls
{
    /// <summary>
    /// Interaction logic for PickerArea.xaml
    /// </summary>
    public partial class PickerAreasOrder
    {
        public PickerAreasOrder(IEnumerable<Order> orders)
        {
            DataContext = orders;
            InitializeComponent();
        }

        public PickerAreasOrder()
        {
            InitializeComponent();
        }

        public event Action<Order, Area> PickArea;

        private void AreaButton_Click(object sender, RoutedEventArgs e)
        {
            var b = (Button)sender;
            var order = (Order)b.Tag;
            OnPickArea(order, (Area)b.DataContext);
        }

        private void OnPickArea(Order order, Area area)
        {
            PickArea?.Invoke(order, area);
        }

        private void PickerAreasOrderTable_OnOnAreasPicked(Order order, Area area)
        {
            PickArea?.Invoke(order, area);
        }
    }
}