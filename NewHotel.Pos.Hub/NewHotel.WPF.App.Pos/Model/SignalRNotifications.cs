﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows.Threading;
using NewHotel.Contracts;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Core;
using NewHotel.Pos.Notifications;
using NewHotel.WPF.App.Pos.Controls;
using NewHotel.WPF.Common;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Model
{
    public static class SignalRNotifications
    {
        public static SignalRClientWrapper? Client { get; private set; }

        private static Stand Stand => Hotel.Instance.CurrentStandCashierContext.Stand;
        private static Cashier Cashier => Hotel.Instance.CurrentStandCashierContext.Cashier;

        public static void Initialize()
        {
            var hubName = AppSetting.HubName;
            var signalRServer = AppSetting.SignalRServer;
            if (Client == null && !string.IsNullOrEmpty(hubName) && !string.IsNullOrEmpty(signalRServer))
                Client = new SignalRClientWrapper(hubName, signalRServer);
        }

        public static void Publish<T>(string method, T data) where T : class
        {
            Client?.Publish(method, data);
        }

        public static void InitializeSubscriptions(ITicketInitializer ticketViewManager)
        {
            Client?.Initialize((name, eventName) => Trace.WriteLine($"{name} {eventName}"));

            #region Ticket TicketPersisted

            Client?.Subscribe<string>("TicketPersisted", (_, data) =>
            {
                Hotel.Instance.CurrentStandCashierContext.UpdateTablesFromBackground();
                // task.ConfigureAwait()
				/*
                var contract = Serializer.Deserialize<NotificationContract<Guid>>(data);

                var ticketId = contract.DataContext;
                var standId = Stand.Id;
                var cashierId = Cashier.Id;

                var result = BusinessProxyRepository.Ticket.CheckOpenTicket(ticketId, standId, cashierId).Result;
                if (!result.IsEmpty) return;
                if (result.Contract is not POSTicketContract ticket) return;

                UIThread.InvokeAsync(DispatcherPriority.Background, () =>
                {
                    var localTicket = Stand.Tickets.FirstOrDefault(x => x.Id == ticketId);
                    if (localTicket == null)
                    {
                        Stand.Tickets.Add(ticketViewManager.GetNewTicketFromContract(ticket));
                    }
                    else
                    {
                        localTicket.DataContext = ticket;
                        localTicket.RecalculateAll();
                    }

                    foreach (var saloon in Stand.Saloons)
                    {
                        var table = saloon.FindTableByTicketId(ticketId);
                        if (table == null) continue;
                        table.RefreshSendToAreaStatus();
                        break;
                    }
                });
                /* */
            });

            #endregion

            #region Ticket Moved

            Client?.Subscribe<string>("TicketMoved", (_, data) =>
            {
				var task = Hotel.Instance.CurrentStandCashierContext.UpdateTablesFromBackground();
				/*
                var contract = Serializer.Deserialize<NotificationContract<Guid>>(data);
                
                var ticketId = contract.DataContext;
                var standId = Stand.Id;
                var cashierId = Cashier.Id;

                var result = BusinessProxyRepository.Ticket.CheckOpenTicket(ticketId, standId, cashierId).Result;
                if (!result.IsEmpty) return;
                if (result.Contract is not POSTicketContract ticket) return;

                UIThread.InvokeAsync(DispatcherPriority.Background, () =>
                {
                    var localTicket = Stand.Tickets.FirstOrDefault(x => x.Id == ticketId);

                    if (localTicket == null)
                    {
                        Stand.Tickets.Add(ticketViewManager.GetNewTicketFromContract(ticket));
                    }
                    else
                    {
                        localTicket.DataContext = ticket;
                        localTicket.RecalculateAll();
                        
                        var currentStandCashierContext = Hotel.Instance.CurrentStandCashierContext;
                        currentStandCashierContext.QuickTickets.Remove(localTicket);
                        currentStandCashierContext.AcceptedTickets.Remove(localTicket);
                        currentStandCashierContext.ReturnedTickets.Remove(localTicket);

                        foreach (var saloon in Stand.Saloons)
                        {
                            var table = saloon.FindTableByTicketId(ticketId);
                            if (table == null) continue;
                            table.RemoveTicket(localTicket);
                            break;
                        }

                        if (localTicket.Table != null)
                        {
                            localTicket.Table.AddTicket(localTicket);
                            localTicket.TableId = ticket.Mesa;
                        }
                        else if (localTicket.IsAcceptedAsTransfer)
                            currentStandCashierContext.AcceptedTickets.Add(localTicket);
                        else
                            currentStandCashierContext.QuickTickets.Add(localTicket);
                    }
                });
                */
			});

            #endregion

            #region Ticket Closed

            Client?.Subscribe<string>("TicketClosed", (_, data) =>
            {
                Hotel.Instance.CurrentStandCashierContext.UpdateTablesFromBackground();
                /*
                var contract = Serializer.Deserialize<NotificationContract<Guid>>(data);
                UIThread.InvokeAsync(DispatcherPriority.Background, () =>
                {
                    var ticketId = contract.DataContext;
                    var localTicket = Stand.Tickets.FirstOrDefault(x => x.Id == ticketId);
                    if (localTicket == null) return;
                    Stand.Tickets.Remove(localTicket);
                    ticketViewManager.RemoveVisualTicket(localTicket);
                });
                */
            });

            #endregion

            #region Ticket Opened for edition

            Client?.Subscribe<string>("OpeningTicket", (_, data) =>
            {
				var task = Hotel.Instance.CurrentStandCashierContext.UpdateTablesFromBackground();
				/*
                var contract = Serializer.Deserialize<NotificationContract<Guid>>(data);
                UIThread.InvokeAsync(DispatcherPriority.Background, () =>
                {
                    var ticketId = contract.DataContext;
                    var localTicket = Stand.Tickets.FirstOrDefault(x => x.Id == ticketId);
                    localTicket?.BlockTicket();
                });
                */
			});

            #endregion

            #region Ticket Closed from edition

            Client?.Subscribe<string>("ClosingTicket", (_, data) =>
            {
				var task = Hotel.Instance.CurrentStandCashierContext.UpdateTablesFromBackground();
				/*
                var contract = Serializer.Deserialize<NotificationContract<Guid>>(data);
                UIThread.InvokeAsync(DispatcherPriority.Background, () =>
                {
                    var ticketId = contract.DataContext;
                    var localTicket = Stand.Tickets.FirstOrDefault(x => x.Id == ticketId);
                    localTicket?.UnblockTicket(localTicket.BlockCode);
                });
                */
			});

            #endregion

            #region Ticket Transferred

            Client?.Subscribe<string>("TransferTicket", (_, data) =>
            {
                var contract = Serializer.Deserialize<NotificationContract<TransferContract>>(data);
                UIThread.InvokeAsync(DispatcherPriority.Background, () =>
                {
                    var transferContract = contract.DataContext;
                    if (Stand.Id == transferContract.StandDest)
                        Stand.InTransferNotificationsCount++;
                    if (Stand.Id == transferContract.StandOrig)
                        Stand.OutTransferNotificationsCount++;

                    var localTicket = Stand.Tickets.FirstOrDefault(x => x.Id == transferContract.TicketOrig);
                    if (localTicket == null) return;
                    Stand.Tickets.Remove(localTicket);
                    ticketViewManager.RemoveVisualTicket(localTicket);
                });
            });

            #endregion

            #region Ticket Transferred Accepted / Returned other Stand

            Client?.Subscribe<string>("TransferTicketActions", (_, data) =>
            {
                var contract = Serializer.Deserialize<NotificationContract<TransferContract>>(data);
                UIThread.InvokeAsync(DispatcherPriority.Background, () =>
                {
                    var transferContract = contract.DataContext;
                    if (Stand.Id == transferContract.StandOrig)
                        Stand.OutTransferNotificationsCount--;
                    if (Stand.Id == transferContract.StandDest)
                        Stand.InTransferNotificationsCount--;
                });
            });

            #endregion

            #region Turn Closed

            Client?.Subscribe<string>("TurnClosed", (_, data) =>
            {
                var contract = Serializer.Deserialize<NotificationContract<StandTurnDateContract>>(data);
                UIThread.InvokeAsync(DispatcherPriority.Background, () =>
                {
                    var turnCloseContract = contract.DataContext;
                    if (Stand.Id == (Guid)turnCloseContract.Id)
                        Stand.UpdateTurnAndDate(turnCloseContract);
                });
            });

            #endregion

            #region Day Closed

            Client?.Subscribe<string>("DayClosed", (_, data) =>
            {
                var contract = Serializer.Deserialize<NotificationContract<StandTurnDateContract>>(data);
                UIThread.InvokeAsync(DispatcherPriority.Background, () =>
                {
                    var turnCloseContract = contract.DataContext;
                    if (Stand.Id == (Guid)turnCloseContract.Id)
                        Stand.UpdateTurnAndDate(turnCloseContract);
                });
            });

            #endregion
        }
    }
}