﻿using NewHotel.WPF.Model;

namespace NewHotel.WPF.App.Pos.Model
{
    public static class AppSetting
    {
        #region SignalR

        public static string HubName => MgrServices.GetConfigurationSetting("SignalRHubName");
        public static string SignalRServer => MgrServices.GetConfigurationSetting("SignalRServer");

        #endregion

        #region Socket IO

        public static string SocketIOUri => MgrServices.GetConfigurationSetting("SocketIOUri");

        #endregion
    }
}
