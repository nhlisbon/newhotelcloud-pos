﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;

namespace NewHotel.WPF.App.Pos
{
    public class CheckVersion
    {
        private string locationPos;
        private string PosVersion;
        private string updateServer = "";

        public CheckVersion()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);

            ReadRegister();
            ReadWebClient();
            GetPosVersion();
        }

        public void ReadRegister()
        {
            try
            {
                var caca = Environment.OSVersion.Platform;

                RegistryKey root = Registry.LocalMachine.OpenSubKey("Software");

                root = root.OpenSubKey("NewHotel");
                root = root.OpenSubKey("Pos");
                locationPos = root.GetValue("InstallationPath").ToString();
            }
            catch
            {
                throw new Exception("InstallationPath not found");
            }
        }
        public void ReadWebClient()
        {
            updateServer = ConfigurationManager.AppSettings.Get("WebClientLocation");
        }

        public void GetPosVersion()
        {
            //Version v = Assembly.GetExecutingAssembly().GetName().Version;
            //PosVersion = v.Major.ToString().PadLeft(4, '0') + v.Minor.ToString().PadLeft(4, '0') + v.Build.ToString().PadLeft(2, '0');
            PosVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public static int ConvertVersionToInt(string version)
        {
            var split = version.Split('.');
            var newVersion =
                split[0].PadLeft(4, '0') +
                split[1].PadLeft(2, '0') +
                split[2].PadLeft(2, '0');
            return int.Parse(newVersion);
        }

        public string CheckVersionPos(string versionName = null)
        {

            WebClient client = new WebClient();
            var directories = client.DownloadString(updateServer);
            var versions = GetVersions(directories);
            int? version = (versionName != null) ? versions.FirstOrDefault(x => x == ConvertVersionToInt(versionName)) : versions.Max();
            if (version == 0) return null;

            return (version > ConvertVersionToInt(PosVersion)) ? version.ToString() : "0";

        }

        public void DownloadUpdate(string folder)
        {
            string path = "";
            Directory.CreateDirectory(path = Path.Combine(locationPos, "Update"));
            //Directory.CreateDirectory(System.IO.Path.Combine(path, "libs"));
            //Directory.CreateDirectory(System.IO.Path.Combine(path, "libs", "32bits"));
            //Directory.CreateDirectory(System.IO.Path.Combine(path, "libs", "64bits"));

            WebClient client = new WebClient();

            DownloadFile(client, "libs", "Ionic.Zip.dll");

            //// 32bits

            //DownloadFile(client, "System.Data.SQLite.dll", System.IO.Path.Combine("libs", "32bits"), System.IO.Path.Combine("libs", "32bits"));
            //DownloadFile(client, "System.Data.SQLite.Linq.dll", System.IO.Path.Combine("libs", "32bits"), System.IO.Path.Combine("libs", "32bits"));
            //DownloadFile(client, "SQLite.Designer.dll", System.IO.Path.Combine("libs", "32bits"), System.IO.Path.Combine("libs", "32bits"));
            DownloadFile(client, "System.Data.SQLite.dll", System.IO.Path.Combine("libs", "32bits"), "");
            DownloadFile(client, "System.Data.SQLite.Linq.dll", System.IO.Path.Combine("libs", "32bits"), "");
            DownloadFile(client, "SQLite.Designer.dll", System.IO.Path.Combine("libs", "32bits"), "");


            //// 64bits
            //DownloadFile(client, "System.Data.SQLite.dll", System.IO.Path.Combine("libs", "64bits"), System.IO.Path.Combine("libs", "64bits"));
            //DownloadFile(client, "System.Data.SQLite.Linq.dll", System.IO.Path.Combine("libs", "64bits"), System.IO.Path.Combine("libs", "64bits"));
            //DownloadFile(client, "SQLite.Designer.dll", System.IO.Path.Combine("libs", "64bits"), System.IO.Path.Combine("libs", "64bits"));

            // Move sqlite dll to .exe directory
            string platformFolder = /*Environment.Is64BitProcess ? "64bits" :*/ "32bits";
            //File.Copy(Path.Combine(locationPos, "Update", "libs", platformFolder, "System.Data.SQLite.dll"), Path.Combine(locationPos, "Update", "System.Data.SQLite.dll"));
            //File.Copy(Path.Combine(locationPos, "Update", "libs", platformFolder, "System.Data.SQLite.Linq.dll"), Path.Combine(locationPos, "Update", "System.Data.SQLite.Linq.dll"));
            //File.Copy(Path.Combine(locationPos, "Update", "libs", platformFolder, "SQLite.Designer.dll"), Path.Combine(locationPos, "Update", "SQLite.Designer.dll"));

            // exe
            DownloadFile(client, Path.Combine(folder, platformFolder), "UpdatePOS.exe");
            // config
            DownloadFile(client, Path.Combine(folder, platformFolder), "UpdatePOS.exe.exe");
            File.Move(Path.Combine(locationPos, "Update", "UpdatePOS.exe.exe"), Path.Combine(locationPos, "Update", "UpdatePOS.exe.config"));

            // updated application (zip)
            DownloadFile(client, folder, "setup.bin");
        }

        public IEnumerable<int> GetVersions(string directories)
        {
            var regex = new Regex(@"<a href=""[^""]*/([0-9]+)+/"">", RegexOptions.IgnoreCase);

            foreach (Match match in regex.Matches(directories))
            {
                var href = match.Groups[1].Value;
                int value = -1;
                if (int.TryParse(href, out value))
                {
                    yield return value;
                }
            }
            yield break;
        }
        public void DownloadFile(WebClient client, string folder, string filename, bool overrideServerPath = false)
        {
            string folderURL = (overrideServerPath ? "" : updateServer + "/") + folder + "/";
            client.DownloadFile(folderURL + filename, Path.Combine(locationPos, "Update", filename));
        }

        public void DownloadFile(WebClient client, string filename, string fromDir, string toDir)
        {
            string serverURL = System.IO.Path.Combine(updateServer, fromDir, filename);
            string targetURL = Path.Combine(locationPos, "Update");
            client.DownloadFile(serverURL, System.IO.Path.Combine(targetURL, toDir, filename));
        }

        public string UpdateServer
        {
            get
            {
                return updateServer;
            }
        }
    }
}
