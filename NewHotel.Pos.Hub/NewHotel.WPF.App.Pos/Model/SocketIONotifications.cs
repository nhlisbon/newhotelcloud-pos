﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;
using NewHotel.Pos.Core;
using NewHotel.Pos.Notifications;
using NewHotel.WPF.App.Pos.Controls;

namespace NewHotel.WPF.App.Pos.Model
{
    public static class SocketIONotifications
    {
        [DataContract]
        private sealed class ListeningArea
        {
            [DataMember]
            public string Name { get; set; }
            [DataMember]
            public Guid AreaId { get; set; }
            [DataMember]
            public string Area { get; set; }
        }

        private static ICollection<Guid> ListeningAreas { get; set; } = new HashSet<Guid>();

        public static SocketIOClientWrapper Client { get; private set; }

        public static bool IsAreaListening(params Guid[] areasId)
        {
            lock (ListeningAreas)
            {
                foreach (var areaId in areasId)
                {
                    if (ListeningAreas.Contains(areaId))
                        return true;
                }
            }

            return false;
        }

        private static bool ClearAreaListening()
        {
            lock (ListeningAreas)
            {
                if (ListeningAreas.Count > 0)
                {
                    ListeningAreas.Clear();
                    return true;
                }
            }

            return false;
        }

        private static bool AddAreaListening(Guid areaId)
        {
            lock (ListeningAreas)
            {
                if (!ListeningAreas.Contains(areaId))
                {
                    ListeningAreas.Add(areaId);
                    return true;
                }
            }

            return false;
        }

        private static bool RemoveAreaListening(Guid areaId)
        {
            lock (ListeningAreas)
            {
                return ListeningAreas.Remove(areaId);
            }
        }

        public static void Initialize()
        {
            var uri = AppSetting.SocketIOUri;
            if (Client == null && !string.IsNullOrEmpty(uri))
                Client = new SocketIOClientWrapper(uri);
        }

        public static void Publish<T>(string method, NotificationContract<T> contract) where T : class
        {
            Client?.Publish(method, contract);
        }

        public static async void InitializeSubscriptions(ITicketInitializer ticketViewManager)
        {
            const string CloudPosApp = "CloudPos";

            if (Client != null)
            {
                Client.StateChanged += async (sc) =>
                {
                    switch (sc.NewState)
                    {
                        case ConnectionState.Connected:
                            if (Client != null)
                            {
                                await Client.Subscribe((name, data) =>
                                {
                                    const string KitchenDisplay = "KitchenDisplay";

                                    try
                                    {
                                        var contract = Serializer.Deserialize<ListeningArea>(data);

                                        #region Kitchen Display Listening Areas

                                        if (contract.Name == KitchenDisplay && contract.AreaId != Guid.Empty)
                                        {
                                            switch (name)
                                            {
                                                case "Subscribed":
                                                    if (AddAreaListening(contract.AreaId))
                                                        Trace.WriteLine($"Added listening area {contract.Area}");
                                                    break;
                                                case "Unsubscribed":
                                                    if (RemoveAreaListening(contract.AreaId))
                                                        Trace.WriteLine($"Removed listening area {contract.Area}");
                                                    break;
                                            }
                                        }

                                        #endregion
                                    }
                                    catch (Exception ex)
                                    {
                                        Trace.WriteLine(ex.Message);
                                    }
                                });

                                Client.Publish<string>("Subscribe", CloudPosApp);
                            }
                            break;
                        case ConnectionState.Disconnected:
                            if (ClearAreaListening())
                                Trace.WriteLine($"Removed all listening areas");
                            break;
                    }
                };

                await Client.Initialize((name, data) => Trace.WriteLine($"Published {name}"));
            }
        }
    }
}