﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Hub.Core;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;

namespace NewHotel.Pos.Hub.Common
{
	[ServiceKnownType(typeof(BaseContract))]
	[ServiceKnownType(typeof(POSTicketContract))]
    [ServiceKnownType(typeof(POSProductLineContract))]
    [ServiceKnownType(typeof(ContainerTransferContract))]
    [ServiceKnownType(typeof(StandTurnDateContract))]
    [ServiceContract]
    public interface ICommonService : IBaseService
    {       
    }
}