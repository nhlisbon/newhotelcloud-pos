﻿using System;
using System.Reflection;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Hub.Core;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Communication.Behavior;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Common
{
	public class CommonService : BaseService, ICommonService
	{
	    protected T GetBusiness<T>()
	    {
            // return CwFactory.Instance.ResolveScoped<T>();
            return CwFactory.Resolve<T>();
        }
    }

    public sealed class SessionDataProvider : ISessionDataProvider
    {
        public Guid Token => HubContextCache.Current.Token;

        public string Version
        {
            get
            {
                var version = Assembly.GetEntryAssembly().GetName().Version;
                return $"{version.Major}.{version.Minor}.{version.Build}";
            }
        }

        public string StandWorkDate => BusinessContext.WorkDate.ToString("yyyyMMdd");
    }
}