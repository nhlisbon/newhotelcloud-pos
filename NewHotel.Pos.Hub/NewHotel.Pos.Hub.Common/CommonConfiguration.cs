﻿using NewHotel.Pos.IoC;
using NewHotel.Pos.Hub.Core;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Communication.Behavior;
using NewHotel.Pos.Hub.Model.Session;

namespace NewHotel.Pos.Hub.Common
{
	public static class CommonConfiguration
    {
        public static void Configure(IServiceConfigurator configurator)
        {
			BusinessContext.InitializeNotifications();
			BusinessContext.InitializeServiceMonitor();

			configurator
				.AddTransient<ISessionDataProvider, SessionDataProvider>()
                .AddScoped(BaseService.GetManager)
				.AddScoped<IPosSessionFactory, HubPosSessionFactory>()
                .AddTransient<IPosSessionContext, IPosSessionFactory>(factory => factory.Current)
				;
		}
	}
}