﻿using System;

namespace NewHotel.DataAnnotations
{
    public class PermissionGroupDetails
    {
        public long Permission { get; set; }
        public long SecurityCodePermission { get; set; }
    }
}
