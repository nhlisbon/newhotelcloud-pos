﻿using System;

namespace NewHotel.DataAnnotations
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class GridColumnHeader : MappingAttribute
    {
        readonly string propertyName;
        readonly string header;
        readonly double minWidth;

        public GridColumnHeader(string attributeName, string header = "", double minWidth = 0)
        {
            this.propertyName = attributeName;
            if (string.IsNullOrEmpty(header))
                this.header = propertyName;
            else this.header = header;
            this.minWidth = minWidth;
        }

        public string AttributeName
        {
            get { return propertyName; }
        }

        public string Header
        {
            get { return header; }
        }

        public double MinWidth
        {
            get { return minWidth; }
        }
    }

}
