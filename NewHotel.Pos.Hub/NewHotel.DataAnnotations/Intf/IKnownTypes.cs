﻿using System;
using System.Collections.Generic;

namespace NewHotel.DataAnnotations
{
    public interface IKnownTypes
    {
        IEnumerable<Type> KnownTypes { get; }
    }
}
