﻿using System;

namespace NewHotel.DataAnnotations
{
    public interface IRequestSessionData
    {
        string UserName { get; }
        string Password { get; }
        Guid InstallationId { get; }
        void SetRequestSessionData(ISessionData data);
        ISessionData GetRequestSessionData();
    }
}
