﻿using System;
using System.Collections.Generic;

namespace NewHotel.DataAnnotations
{
    public interface IKeyedCollection<TKey, TItem> : ICollection<TItem>
    {
        bool ContainsKey(TKey key);
        bool TryGetItem(TKey key, out TItem item);
    }
}