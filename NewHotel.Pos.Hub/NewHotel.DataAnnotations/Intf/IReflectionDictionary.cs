﻿using System;
using System.Collections.Generic;

namespace NewHotel.DataAnnotations
{
    public interface IReflectionDictionary : IEnumerable<KeyValuePair<string, IReflectionEntry>>
    {
        bool Contains(string key);
        bool TryGetValue(string key, out IReflectionEntry entry);
        object this[string key] { get; set; }
    }
}
