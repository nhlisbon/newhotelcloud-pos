﻿using System;

namespace NewHotel.DataAnnotations
{
    public interface IReflectionEntry
    {
        object Value { get; set; }
        bool CanRead { get; }
        bool CanWrite { get; }
    }
}
