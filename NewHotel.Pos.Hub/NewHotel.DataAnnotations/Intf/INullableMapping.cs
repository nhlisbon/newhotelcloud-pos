﻿using System;

namespace NewHotel.DataAnnotations
{
    public interface INullableMapping
    {
        Type EmptyType { get; }
    }
}
