﻿using System;

namespace NewHotel.DataAnnotations
{
    public interface IKeyDescRecord<T>
    {
        T Id { get; set;  }
        string Description { get; set; }
    }
}