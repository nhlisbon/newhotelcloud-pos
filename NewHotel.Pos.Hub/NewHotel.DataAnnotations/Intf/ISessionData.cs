﻿using System;

namespace NewHotel.DataAnnotations
{
    public interface ISessionData
    {
		long ApplicationId { get; set; }
        string ModuleName { get; set; }
        Guid InstallationId { get; set; }
		Guid UserId { get; set; }
        long LanguageId { get; set; }
        DateTime SysDate { get; set; }
        DateTime WorkDate { get; set; }
        short WorkShift { get; set; }
        DateTime? FirstOpenedDate { get; set; }
        int? LastOpenedYear { get; set; }
        DateTime ClientDateTime { get; set; }
        Guid ChannelId { get; set; }
    }
}