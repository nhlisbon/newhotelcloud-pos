﻿using System;

namespace NewHotel.DataAnnotations
{
    public interface IAccessor : IPropDescriptor
    {
        object Get(object obj);
        void Set(object obj, object value);
        Type EmptyType { get; }
        bool IsTimestamp { get; }
        bool IsLastModified { get; }
    }
}
