﻿using System;
using System.Collections.Generic;

namespace NewHotel.DataAnnotations
{
    public interface IReflectionDictionaryKey
    {
        string Key { get; }
    }
}
