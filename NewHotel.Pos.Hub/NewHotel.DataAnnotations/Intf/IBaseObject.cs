﻿using System;

namespace NewHotel.DataAnnotations
{
    public interface IBaseObject
    {
        #region Properties

        object Id { get; set; }
        Type IdType { get; }
        Type KeyType { get; }
        DateTime LastModified { get; set; }
        bool IsPersisted { get; }

        #endregion
    }
}
