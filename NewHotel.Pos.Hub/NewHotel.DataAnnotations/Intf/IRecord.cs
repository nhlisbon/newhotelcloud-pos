﻿using System;
using System.Collections;

namespace NewHotel.DataAnnotations
{
    public interface IRecord : IEnumerable
    {
        int Index { get; }
        string[] Fields { get; }
        object this[int index] { get; set; }
        object this[string name] { get; set; }
        bool Contains(string name);
        int Count { get; }
        object ValueAs(Type type, string name);
        bool IsDBNull(string name);
        Type GetDataType(string name);
        object[] ToArray();
        IRecord Clone();
    }
}
