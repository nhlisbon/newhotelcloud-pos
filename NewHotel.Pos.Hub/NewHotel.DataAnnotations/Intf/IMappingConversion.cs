﻿using System;

namespace NewHotel.DataAnnotations
{
    public interface IMappingConversion
    {
        Type Type { get; }
    }
}
