﻿using System;

namespace NewHotel.DataAnnotations
{
    public interface IArgument
    {
        object Value { get; }
        object Text { get; }
    }
}
