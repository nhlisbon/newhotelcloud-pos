﻿using System;
using System.Collections.Generic;

namespace NewHotel.DataAnnotations
{
    public interface IPropDescriptor
    {
        string Name { get; }
        string FullName { get; }
        Type Type { get; }
        Type DeclaringType { get; }
        Type BaseType { get; }
        bool Nullable { get; }
        bool NullStringAsEmpty { get; }
        IEnumerable<T> GetAttributes<T>();
        bool CanRead { get; }
        bool CanWrite { get; }
    }
}
