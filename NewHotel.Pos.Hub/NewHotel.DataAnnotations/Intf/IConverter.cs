﻿using System;

namespace NewHotel.DataAnnotations
{
    public interface IConverter
    {
        object Get(IPropDescriptor propDesc, object value);
        object Set(IPropDescriptor propDesc, object value);
    }
}
