﻿using System;

namespace NewHotel.DataAnnotations
{
    public interface IHierarchicalRecord<T> : IKeyDescRecord<T>
    {
        T ParentId { get; set; }
    }
}