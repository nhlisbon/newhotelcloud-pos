﻿using System;

namespace NewHotel.DataAnnotations
{
    public interface IDataFill
    {
        void FillData(IRecord record);
    }
}
