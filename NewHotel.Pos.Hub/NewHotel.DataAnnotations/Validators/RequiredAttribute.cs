﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.DataAnnotations
{
    public class RequiredAttribute : System.ComponentModel.DataAnnotations.RequiredAttribute
    {
        protected override System.ComponentModel.DataAnnotations.ValidationResult IsValid(object value, System.ComponentModel.DataAnnotations.ValidationContext validationContext)
        {
            System.ComponentModel.DataAnnotations.ValidationResult vres = base.IsValid(value, validationContext);
            if (vres == System.ComponentModel.DataAnnotations.ValidationResult.Success)
            {
                if (value is Guid)
                {
                    Guid guid = (Guid)value;
                    if (guid == Guid.Empty)
                        return new System.ComponentModel.DataAnnotations.ValidationResult(this.ErrorMessage);
                }

                if (value is DateTime)
                {
                    DateTime date = (DateTime)value;
                    if (date == DateTime.MinValue)
                        return new System.ComponentModel.DataAnnotations.ValidationResult(this.ErrorMessage);
                }

                return System.ComponentModel.DataAnnotations.ValidationResult.Success;
            }
            else return vres;
        }
    }
}
