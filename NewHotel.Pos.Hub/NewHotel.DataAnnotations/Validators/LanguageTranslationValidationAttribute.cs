﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.DataAnnotations
{
    public class LanguageTranslationValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
                return new ValidationResult("Language translation required.");

            IEnumerable ie=value as IEnumerable;
            if (ie == null)
                return new ValidationResult("Language translation required.");

            bool hasTranslation = false;
            foreach (object obj in ie)
            {
                if (!string.IsNullOrEmpty(obj.ToString()))
                {
                    hasTranslation = true;
                    break;
                }
            }
            if (!hasTranslation)
                return new ValidationResult("Invalid language translation. Empty translation.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
    }
}
