﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.DataAnnotations
{
    [DataContract]
	[Serializable]
    [KnownType(typeof(DBNull))]
    public struct ArgumentValue : IArgument, IEquatable<ArgumentValue>
    {
        #region Members

        [DataMember]
        internal object _value;
        [DataMember]
        internal string _text;

        public static readonly ArgumentValue Null = new ArgumentValue(DBNull.Value);

        #endregion
        #region Constructors

        public ArgumentValue(object value, string text = null)
        {
            if (value == null)
                throw new ArgumentNullException("value");

            _value = value;
            _text = text;
        }

        #endregion
        #region Public Methods

        public static ArgumentValue New(object value, string text)
        {
            return new ArgumentValue(value, text);
        }

        public static ArgumentValue New(object value)
        {
            return new ArgumentValue(value);
        }

        public object Value
        {
            get { return _value; }
        }

        public object Text
        {
            get
            {
                if (!string.IsNullOrEmpty(_text))
                    return _text;

                return null;
            }
        }

        public bool Equals(ArgumentValue av)
        {
            return Value.Equals(av.Value);
        }

        public override bool Equals(object obj)
        {
            if (obj is ArgumentValue)
                return Equals((ArgumentValue)obj);

            return false;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
          
        public override string ToString()
        {
            if (!string.IsNullOrEmpty(_text))
                return _text;

            return Value.ToString();
        }

        public static bool operator ==(ArgumentValue lhs, ArgumentValue rhs)
        {
            return lhs.Equals(rhs);
        }

        public static bool operator !=(ArgumentValue lhs, ArgumentValue rhs)
        {
            return !lhs.Equals(rhs);
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    public struct Argument : IArgument
    {
        #region Members

        private readonly static IDictionary<string, Func<object>> _valuesResolver =
            new Dictionary<string, Func<object>>();

        [DataMember]
        internal string _name;

        #endregion
        #region Constructors

        public Argument(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException("name");

            _name = name.ToUpperInvariant();
        }

        #endregion
        #region Static

        public void AddResolver(string name, Func<object> resolver)
        {
            _valuesResolver[name.ToUpperInvariant()] = resolver;
        }

        #endregion
        #region Public Methods

        public object Value
        {
            get 
            { 
                Func<object> resolver;
                if (_valuesResolver.TryGetValue(_name, out resolver))
                    return resolver.Invoke();

                return null; 
            }
        }

        public object Text
        {
            get
            {
                if (Value != null)
                    return Value.ToString();

                return null;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is Argument)
            {
                var arg = (Argument)obj;
                return _name.Equals(arg._name);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return _name.GetHashCode();
        }

        public override string ToString()
        {
            return _name;
        }

        #endregion
    }
}