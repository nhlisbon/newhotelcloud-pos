﻿using System;

namespace NewHotel.DataAnnotations
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class DomainType : Attribute
    {
        public readonly Type Type;

        public DomainType(Type type)
        {
            if (!type.IsEnum || !Enum.GetUnderlyingType(type).Equals(typeof(long)))
                throw new ArgumentException(string.Format("Invalid type {0} in domain", type.Name));

            Type = type;
        }
    }
}
