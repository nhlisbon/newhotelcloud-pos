﻿using System;

namespace NewHotel.Core
{
    /// <summary>
    /// Tipos Filtros (= <> >= <= IN BETWEEN LIKE NOT IN)
    /// </summary>
    public enum FilterTypes : long { Equal = 2221, NotEqual = 2222, LessThan = 2223, LessThanOrEqual = 2224, GreaterThan = 2225, GreaterThanOrEqual = 2226, In = 2227, Between = 2228, Like = 2229, NotIn = 2230 };
}
