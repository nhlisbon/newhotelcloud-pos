﻿using System;

namespace NewHotel.DataAnnotations
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class MappingTable : MappingAttribute
    {
        public string Name { get; protected set; }
        public string Id { get; protected set; }
        public short Index { get; protected set; }

        public MappingTable(string name, string id, byte index)
        {
            Name = name.ToUpperInvariant();
            Id = id.ToUpperInvariant();
            Index = index;
        }

        public MappingTable(string name, string id)
            : this(name, id, 0) { }
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class MappingQuery : MappingTable
    {
        public MappingQuery(string id)
            : base(string.Empty, id) { }
    }
}
