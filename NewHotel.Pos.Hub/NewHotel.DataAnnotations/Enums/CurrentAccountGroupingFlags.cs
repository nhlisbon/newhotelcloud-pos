﻿using System;

namespace NewHotel.DataAnnotations
{
    [Flags]
    public enum CurrentAccountGroupingFlags { None = 0, ByService = 2, ByDepartment = 4, ByDate = 8, ByDaily = 16, ByDailyTaxRate = 32, ByPhone = 64, ByDocNumber = 128, ByPaymentsType = 256 };
}
