﻿using System;
using System.Linq;

namespace NewHotel.DataAnnotations
{
    public class ComparableArray : IComparable<ComparableArray>
    {
        private readonly IComparable[] Array;
        private readonly int HashCode;

        public ComparableArray(params IComparable[] array)
        {
            Array = array;
            HashCode = ToString().GetHashCode();
        }

        public int CompareTo(ComparableArray other)
        {
            if (other != null)
            {
                var x = Array;
                var y = other.Array;

                if (y == null && x == null)
                    return 0;
                else if (y != null && x != null)
                {
                    if (x.Length == y.Length)
                    {
                        for (int i = 0; i < x.Length; i++)
                        {
                            if (x[i] != null && y[i] != null)
                            {
                                var comparison = x[i].CompareTo(y[i]);
                                if (comparison != 0)
                                    return comparison;
                            }
                            else if ((x[i] == null && y[i] != null))
                                return -1;
                            else if ((x[i] != null && y[i] == null))
                                return 1;
                        }

                        return 0;
                    }
                    else
                        return x.Length > y.Length ? 1 : -1;
                }
            }

            return 1;
        }

        public override bool Equals(object obj)
        {
            var other = obj as ComparableArray;
            return CompareTo(other) == 0;
        }

        public override int GetHashCode()
        {
            return HashCode;
        }

        public override string ToString()
        {
            if (Array == null)
                return string.Empty;
            else
                return string.Join(", ", Array.Select(x => x == null ? "NULL" : x.ToString()).ToArray());
        }
    }
}