﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace NewHotel.DataAnnotations
{
    [DataContract]
	[Serializable]
    public struct ARGBColor
    {
        private static readonly NamedColorCollection _namedColors;

        [DataMember]
        internal readonly byte _a;
        public byte A
        {
            get { return _a; }
        }

        [DataMember]
        internal readonly byte _r;
        public byte R
        {
            get { return _r; }
        }

        [DataMember]
        internal readonly byte _g;
        public byte G
        {
            get { return _g; }
        }

        [DataMember]
        internal readonly byte _b;
        public byte B
        {
            get { return _b; }
        }

        [DataMember]
        internal readonly string _name;

        public ARGBColor(byte a, byte r, byte g, byte b, string name)
        {
            _a = a;
            _r = r;
            _g = g;
            _b = b;
            _name = name;
        }

        public static implicit operator int(ARGBColor color)
        {
            return BitConverter.ToInt32(new byte[] { color.B, color.G, color.R, color.A }, 0);
        }

        private ARGBColor(byte a, byte r, byte g, byte b)
            : this(a, r, g, b, null) { }

        public ARGBColor(int color)
        {
            var bytes = BitConverter.GetBytes(color);
            _a = bytes[3];
            _r = bytes[2];
            _g = bytes[1];
            _b = bytes[0];
            _name = null;
        }

        private static string FormatName(byte a, byte r, byte g, byte b)
        {
            return string.Format("{0},{1},{2},{3}", a, r, g, b);
        }

        public bool IsEmpty
        {
            get { return this == Empty; }
        }

        public override string ToString()
        {
            return _name == null ? FormatName(_a, _r, _g, _b) : _name;
        }

        private class NamedColorCollection : KeyedCollection<string, ARGBColor>
        {
            protected override string GetKeyForItem(ARGBColor item)
            {
                return item.ToString();
            }
        }

        public static IEnumerable<ARGBColor> NamedColors
        {
            get { return _namedColors; }
        }

        static ARGBColor()
        {
            _namedColors = new NamedColorCollection();
            _namedColors.Add(new ARGBColor(0, 0, 0, 0, "Empty"));
            _namedColors.Add(new ARGBColor(255, 240, 248, 255, "AliceBlue"));
            _namedColors.Add(new ARGBColor(255, 250, 235, 215, "AntiqueWhite"));
            _namedColors.Add(new ARGBColor(255, 0, 255, 255, "Aqua"));
            _namedColors.Add(new ARGBColor(255, 127, 255, 212, "Aquamarine"));
            _namedColors.Add(new ARGBColor(255, 240, 255, 255, "Azure"));
            _namedColors.Add(new ARGBColor(255, 245, 245, 220, "Beige"));
            _namedColors.Add(new ARGBColor(255, 255, 228, 196, "Bisque"));
            _namedColors.Add(new ARGBColor(255, 0, 0, 0, "Black"));
            _namedColors.Add(new ARGBColor(255, 255, 235, 205, "BlanchedAlmond"));
            _namedColors.Add(new ARGBColor(255, 0, 0, 255, "Blue"));
            _namedColors.Add(new ARGBColor(255, 138, 43, 226, "BlueViolet"));
            _namedColors.Add(new ARGBColor(255, 165, 42, 42, "Brown"));
            _namedColors.Add(new ARGBColor(255, 222, 184, 135, "BurlyWood"));
            _namedColors.Add(new ARGBColor(255, 95, 158, 160, "CadetBlue"));
            _namedColors.Add(new ARGBColor(255, 127, 255, 0, "Chartreuse"));
            _namedColors.Add(new ARGBColor(255, 210, 105, 30, "Chocolate"));
            _namedColors.Add(new ARGBColor(255, 255, 127, 80, "Coral"));
            _namedColors.Add(new ARGBColor(255, 100, 149, 237, "CornflowerBlue"));
            _namedColors.Add(new ARGBColor(255, 255, 248, 220, "Cornsilk"));
            _namedColors.Add(new ARGBColor(255, 220, 20, 60, "Crimson"));
            _namedColors.Add(new ARGBColor(255, 0, 255, 255, "Cyan"));
            _namedColors.Add(new ARGBColor(255, 0, 0, 139, "DarkBlue"));
            _namedColors.Add(new ARGBColor(255, 0, 139, 139, "DarkCyan"));
            _namedColors.Add(new ARGBColor(255, 184, 134, 11, "DarkGoldenrod"));
            _namedColors.Add(new ARGBColor(255, 169, 169, 169, "DarkGray"));
            _namedColors.Add(new ARGBColor(255, 0, 100, 0, "DarkGreen"));
            _namedColors.Add(new ARGBColor(255, 189, 183, 107, "DarkKhaki"));
            _namedColors.Add(new ARGBColor(255, 139, 0, 139, "DarkMagenta"));
            _namedColors.Add(new ARGBColor(255, 85, 107, 47, "DarkOliveGreen"));
            _namedColors.Add(new ARGBColor(255, 255, 140, 0, "DarkOrange"));
            _namedColors.Add(new ARGBColor(255, 153, 50, 204, "DarkOrchid"));
            _namedColors.Add(new ARGBColor(255, 139, 0, 0, "DarkRed"));
            _namedColors.Add(new ARGBColor(255, 233, 150, 122, "DarkSalmon"));
            _namedColors.Add(new ARGBColor(255, 143, 188, 139, "DarkSeaGreen"));
            _namedColors.Add(new ARGBColor(255, 72, 61, 139, "DarkSlateBlue"));
            _namedColors.Add(new ARGBColor(255, 47, 79, 79, "DarkSlateGray"));
            _namedColors.Add(new ARGBColor(255, 0, 206, 209, "DarkTurquoise"));
            _namedColors.Add(new ARGBColor(255, 148, 0, 211, "DarkViolet"));
            _namedColors.Add(new ARGBColor(255, 255, 20, 147, "DeepPink"));
            _namedColors.Add(new ARGBColor(255, 0, 191, 255, "DeepSkyBlue"));
            _namedColors.Add(new ARGBColor(255, 105, 105, 105, "DimGray"));
            _namedColors.Add(new ARGBColor(255, 30, 144, 255, "DodgerBlue"));
            _namedColors.Add(new ARGBColor(255, 178, 34, 34, "Firebrick"));
            _namedColors.Add(new ARGBColor(255, 255, 250, 240, "FloralWhite"));
            _namedColors.Add(new ARGBColor(255, 34, 139, 34, "ForestGreen"));
            _namedColors.Add(new ARGBColor(255, 255, 0, 255, "Fuchsia"));
            _namedColors.Add(new ARGBColor(255, 220, 220, 220, "Gainsboro"));
            _namedColors.Add(new ARGBColor(255, 248, 248, 255, "GhostWhite"));
            _namedColors.Add(new ARGBColor(255, 255, 215, 0, "Gold"));
            _namedColors.Add(new ARGBColor(255, 218, 165, 32, "Goldenrod"));
            _namedColors.Add(new ARGBColor(255, 128, 128, 128, "Gray"));
            _namedColors.Add(new ARGBColor(255, 0, 128, 0, "Green"));
            _namedColors.Add(new ARGBColor(255, 173, 255, 47, "GreenYellow"));
            _namedColors.Add(new ARGBColor(255, 240, 255, 240, "Honeydew"));
            _namedColors.Add(new ARGBColor(255, 255, 105, 180, "HotPink"));
            _namedColors.Add(new ARGBColor(255, 205, 92, 92, "IndianRed"));
            _namedColors.Add(new ARGBColor(255, 75, 0, 130, "Indigo"));
            _namedColors.Add(new ARGBColor(255, 255, 255, 240, "Ivory"));
            _namedColors.Add(new ARGBColor(255, 240, 230, 140, "Khaki"));
            _namedColors.Add(new ARGBColor(255, 230, 230, 250, "Lavender"));
            _namedColors.Add(new ARGBColor(255, 255, 240, 245, "LavenderBlush"));
            _namedColors.Add(new ARGBColor(255, 124, 252, 0, "LawnGreen"));
            _namedColors.Add(new ARGBColor(255, 255, 250, 205, "LemonChiffon"));
            _namedColors.Add(new ARGBColor(255, 173, 216, 230, "LightBlue"));
            _namedColors.Add(new ARGBColor(255, 240, 128, 128, "LightCoral"));
            _namedColors.Add(new ARGBColor(255, 224, 255, 255, "LightCyan"));
            _namedColors.Add(new ARGBColor(255, 250, 250, 210, "LightGoldenrodYellow"));
            _namedColors.Add(new ARGBColor(255, 211, 211, 211, "LightGray"));
            _namedColors.Add(new ARGBColor(255, 144, 238, 144, "LightGreen"));
            _namedColors.Add(new ARGBColor(255, 255, 182, 193, "LightPink"));
            _namedColors.Add(new ARGBColor(255, 255, 160, 122, "LightSalmon"));
            _namedColors.Add(new ARGBColor(255, 32, 178, 170, "LightSeaGreen"));
            _namedColors.Add(new ARGBColor(255, 135, 206, 250, "LightSkyBlue"));
            _namedColors.Add(new ARGBColor(255, 119, 136, 153, "LightSlateGray"));
            _namedColors.Add(new ARGBColor(255, 176, 196, 222, "LightSteelBlue"));
            _namedColors.Add(new ARGBColor(255, 255, 255, 224, "LightYellow"));
            _namedColors.Add(new ARGBColor(255, 0, 255, 0, "Lime"));
            _namedColors.Add(new ARGBColor(255, 50, 205, 50, "LimeGreen"));
            _namedColors.Add(new ARGBColor(255, 250, 240, 230, "Linen"));
            _namedColors.Add(new ARGBColor(255, 255, 0, 255, "Magenta"));
            _namedColors.Add(new ARGBColor(255, 128, 0, 0, "Maroon"));
            _namedColors.Add(new ARGBColor(255, 102, 205, 170, "MediumAquamarine"));
            _namedColors.Add(new ARGBColor(255, 0, 0, 205, "MediumBlue"));
            _namedColors.Add(new ARGBColor(255, 186, 85, 211, "MediumOrchid"));
            _namedColors.Add(new ARGBColor(255, 147, 112, 219, "MediumPurple"));
            _namedColors.Add(new ARGBColor(255, 60, 179, 113, "MediumSeaGreen"));
            _namedColors.Add(new ARGBColor(255, 123, 104, 238, "MediumSlateBlue"));
            _namedColors.Add(new ARGBColor(255, 0, 250, 154, "MediumSpringGreen"));
            _namedColors.Add(new ARGBColor(255, 72, 209, 204, "MediumTurquoise"));
            _namedColors.Add(new ARGBColor(255, 199, 21, 133, "MediumVioletRed"));
            _namedColors.Add(new ARGBColor(255, 25, 25, 112, "MidnightBlue"));
            _namedColors.Add(new ARGBColor(255, 245, 255, 250, "MintCream"));
            _namedColors.Add(new ARGBColor(255, 255, 228, 225, "MistyRose"));
            _namedColors.Add(new ARGBColor(255, 255, 228, 181, "Moccasin"));
            _namedColors.Add(new ARGBColor(255, 255, 222, 173, "NavajoWhite"));
            _namedColors.Add(new ARGBColor(255, 0, 0, 128, "Navy"));
            _namedColors.Add(new ARGBColor(255, 253, 245, 230, "OldLace"));
            _namedColors.Add(new ARGBColor(255, 128, 128, 0, "Olive"));
            _namedColors.Add(new ARGBColor(255, 107, 142, 35, "OliveDrab"));
            _namedColors.Add(new ARGBColor(255, 255, 165, 0, "Orange"));
            _namedColors.Add(new ARGBColor(255, 255, 69, 0, "OrangeRed"));
            _namedColors.Add(new ARGBColor(255, 218, 112, 214, "Orchid"));
            _namedColors.Add(new ARGBColor(255, 238, 232, 170, "PaleGoldenrod"));
            _namedColors.Add(new ARGBColor(255, 152, 251, 152, "PaleGreen"));
            _namedColors.Add(new ARGBColor(255, 175, 238, 238, "PaleTurquoise"));
            _namedColors.Add(new ARGBColor(255, 219, 112, 147, "PaleVioletRed"));
            _namedColors.Add(new ARGBColor(255, 255, 239, 213, "PapayaWhip"));
            _namedColors.Add(new ARGBColor(255, 255, 218, 185, "PeachPuff"));
            _namedColors.Add(new ARGBColor(255, 205, 133, 63, "Peru"));
            _namedColors.Add(new ARGBColor(255, 255, 192, 203, "Pink"));
            _namedColors.Add(new ARGBColor(255, 221, 160, 221, "Plum"));
            _namedColors.Add(new ARGBColor(255, 176, 224, 230, "PowderBlue"));
            _namedColors.Add(new ARGBColor(255, 128, 0, 128, "Purple"));
            _namedColors.Add(new ARGBColor(255, 255, 0, 0, "Red"));
            _namedColors.Add(new ARGBColor(255, 188, 143, 143, "RosyBrown"));
            _namedColors.Add(new ARGBColor(255, 65, 105, 225, "RoyalBlue"));
            _namedColors.Add(new ARGBColor(255, 139, 69, 19, "SaddleBrown"));
            _namedColors.Add(new ARGBColor(255, 250, 128, 114, "Salmon"));
            _namedColors.Add(new ARGBColor(255, 244, 164, 96, "SandyBrown"));
            _namedColors.Add(new ARGBColor(255, 46, 139, 87, "SeaGreen"));
            _namedColors.Add(new ARGBColor(255, 255, 245, 238, "SeaShell"));
            _namedColors.Add(new ARGBColor(255, 160, 82, 45, "Sienna"));
            _namedColors.Add(new ARGBColor(255, 192, 192, 192, "Silver"));
            _namedColors.Add(new ARGBColor(255, 135, 206, 235, "SkyBlue"));
            _namedColors.Add(new ARGBColor(255, 106, 90, 205, "SlateBlue"));
            _namedColors.Add(new ARGBColor(255, 112, 128, 144, "SlateGray"));
            _namedColors.Add(new ARGBColor(255, 255, 250, 250, "Snow"));
            _namedColors.Add(new ARGBColor(255, 0, 255, 127, "SpringGreen"));
            _namedColors.Add(new ARGBColor(255, 70, 130, 180, "SteelBlue"));
            _namedColors.Add(new ARGBColor(255, 210, 180, 140, "Tan"));
            _namedColors.Add(new ARGBColor(255, 0, 128, 128, "Teal"));
            _namedColors.Add(new ARGBColor(255, 216, 191, 216, "Thistle"));
            _namedColors.Add(new ARGBColor(255, 255, 99, 71, "Tomato"));
            _namedColors.Add(new ARGBColor(0, 255, 255, 255, "Transparent"));
            _namedColors.Add(new ARGBColor(255, 64, 224, 208, "Turquoise"));
            _namedColors.Add(new ARGBColor(255, 238, 130, 238, "Violet"));
            _namedColors.Add(new ARGBColor(255, 245, 222, 179, "Wheat"));
            _namedColors.Add(new ARGBColor(255, 255, 255, 255, "White"));
            _namedColors.Add(new ARGBColor(255, 245, 245, 245, "WhiteSmoke"));
            _namedColors.Add(new ARGBColor(255, 255, 255, 0, "Yellow"));
            _namedColors.Add(new ARGBColor(255, 154, 205, 50, "YellowGreen"));
        }

        public static ARGBColor NewARGBColor(byte a, byte r, byte g, byte b)
        {
            var name = FormatName(a, r, g, b);
            if (_namedColors.Contains(name))
                return _namedColors[name];
            else
                return new ARGBColor(a, r, g, b);
        }

        public static readonly ARGBColor Empty = new ARGBColor(0, 0, 0, 0);
        public static readonly ARGBColor ActiveBorder = new ARGBColor(255, 212, 208, 200);
        public static readonly ARGBColor ActiveCaption = new ARGBColor(255, 0, 84, 227);
        public static readonly ARGBColor ActiveCaptionText = new ARGBColor(255, 255, 255, 255);
        public static readonly ARGBColor AliceBlue = new ARGBColor(255, 240, 248, 255);
        public static readonly ARGBColor AntiqueWhite = new ARGBColor(255, 250, 235, 215);
        public static readonly ARGBColor AppWorkspace = new ARGBColor(255, 128, 128, 128);
        public static readonly ARGBColor Aqua = new ARGBColor(255, 0, 255, 255);
        public static readonly ARGBColor Aquamarine = new ARGBColor(255, 127, 255, 212);
        public static readonly ARGBColor Azure = new ARGBColor(255, 240, 255, 255);
        public static readonly ARGBColor Beige = new ARGBColor(255, 245, 245, 220);
        public static readonly ARGBColor Bisque = new ARGBColor(255, 255, 228, 196);
        public static readonly ARGBColor Black = new ARGBColor(255, 0, 0, 0);
        public static readonly ARGBColor BlanchedAlmond = new ARGBColor(255, 255, 235, 205);
        public static readonly ARGBColor Blue = new ARGBColor(255, 0, 0, 255);
        public static readonly ARGBColor BlueViolet = new ARGBColor(255, 138, 43, 226);
        public static readonly ARGBColor Brown = new ARGBColor(255, 165, 42, 42);
        public static readonly ARGBColor BurlyWood = new ARGBColor(255, 222, 184, 135);
        public static readonly ARGBColor ButtonFace = new ARGBColor(255, 236, 233, 216);
        public static readonly ARGBColor ButtonHighlight = new ARGBColor(255, 255, 255, 255);
        public static readonly ARGBColor ButtonShadow = new ARGBColor(255, 172, 168, 153);
        public static readonly ARGBColor CadetBlue = new ARGBColor(255, 95, 158, 160);
        public static readonly ARGBColor Chartreuse = new ARGBColor(255, 127, 255, 0);
        public static readonly ARGBColor Chocolate = new ARGBColor(255, 210, 105, 30);
        public static readonly ARGBColor Control = new ARGBColor(255, 236, 233, 216);
        public static readonly ARGBColor ControlDark = new ARGBColor(255, 172, 168, 153);
        public static readonly ARGBColor ControlDarkDark = new ARGBColor(255, 113, 111, 100);
        public static readonly ARGBColor ControlLight = new ARGBColor(255, 241, 239, 226);
        public static readonly ARGBColor ControlLightLight = new ARGBColor(255, 255, 255, 255);
        public static readonly ARGBColor ControlText = new ARGBColor(255, 0, 0, 0);
        public static readonly ARGBColor Coral = new ARGBColor(255, 255, 127, 80);
        public static readonly ARGBColor CornflowerBlue = new ARGBColor(255, 100, 149, 237);
        public static readonly ARGBColor Cornsilk = new ARGBColor(255, 255, 248, 220);
        public static readonly ARGBColor Crimson = new ARGBColor(255, 220, 20, 60);
        public static readonly ARGBColor Cyan = new ARGBColor(255, 0, 255, 255);
        public static readonly ARGBColor DarkBlue = new ARGBColor(255, 0, 0, 139);
        public static readonly ARGBColor DarkCyan = new ARGBColor(255, 0, 139, 139);
        public static readonly ARGBColor DarkGoldenrod = new ARGBColor(255, 184, 134, 11);
        public static readonly ARGBColor DarkGray = new ARGBColor(255, 169, 169, 169);
        public static readonly ARGBColor DarkGreen = new ARGBColor(255, 0, 100, 0);
        public static readonly ARGBColor DarkKhaki = new ARGBColor(255, 189, 183, 107);
        public static readonly ARGBColor DarkMagenta = new ARGBColor(255, 139, 0, 139);
        public static readonly ARGBColor DarkOliveGreen = new ARGBColor(255, 85, 107, 47);
        public static readonly ARGBColor DarkOrange = new ARGBColor(255, 255, 140, 0);
        public static readonly ARGBColor DarkOrchid = new ARGBColor(255, 153, 50, 204);
        public static readonly ARGBColor DarkRed = new ARGBColor(255, 139, 0, 0);
        public static readonly ARGBColor DarkSalmon = new ARGBColor(255, 233, 150, 122);
        public static readonly ARGBColor DarkSeaGreen = new ARGBColor(255, 143, 188, 139);
        public static readonly ARGBColor DarkSlateBlue = new ARGBColor(255, 72, 61, 139);
        public static readonly ARGBColor DarkSlateGray = new ARGBColor(255, 47, 79, 79);
        public static readonly ARGBColor DarkTurquoise = new ARGBColor(255, 0, 206, 209);
        public static readonly ARGBColor DarkViolet = new ARGBColor(255, 148, 0, 211);
        public static readonly ARGBColor DeepPink = new ARGBColor(255, 255, 20, 147);
        public static readonly ARGBColor DeepSkyBlue = new ARGBColor(255, 0, 191, 255);
        public static readonly ARGBColor Desktop = new ARGBColor(255, 0, 0, 0);
        public static readonly ARGBColor DimGray = new ARGBColor(255, 105, 105, 105);
        public static readonly ARGBColor DodgerBlue = new ARGBColor(255, 30, 144, 255);
        public static readonly ARGBColor Firebrick = new ARGBColor(255, 178, 34, 34);
        public static readonly ARGBColor FloralWhite = new ARGBColor(255, 255, 250, 240);
        public static readonly ARGBColor ForestGreen = new ARGBColor(255, 34, 139, 34);
        public static readonly ARGBColor Fuchsia = new ARGBColor(255, 255, 0, 255);
        public static readonly ARGBColor Gainsboro = new ARGBColor(255, 220, 220, 220);
        public static readonly ARGBColor GhostWhite = new ARGBColor(255, 248, 248, 255);
        public static readonly ARGBColor Gold = new ARGBColor(255, 255, 215, 0);
        public static readonly ARGBColor Goldenrod = new ARGBColor(255, 218, 165, 32);
        public static readonly ARGBColor GradientActiveCaption = new ARGBColor(255, 61, 149, 255);
        public static readonly ARGBColor GradientInactiveCaption = new ARGBColor(255, 157, 185, 235);
        public static readonly ARGBColor Gray = new ARGBColor(255, 128, 128, 128);
        public static readonly ARGBColor GrayText = new ARGBColor(255, 172, 168, 153);
        public static readonly ARGBColor Green = new ARGBColor(255, 0, 128, 0);
        public static readonly ARGBColor GreenYellow = new ARGBColor(255, 173, 255, 47);
        public static readonly ARGBColor Highlight = new ARGBColor(255, 49, 106, 197);
        public static readonly ARGBColor HighlightText = new ARGBColor(255, 255, 255, 255);
        public static readonly ARGBColor Honeydew = new ARGBColor(255, 240, 255, 240);
        public static readonly ARGBColor HotPink = new ARGBColor(255, 255, 105, 180);
        public static readonly ARGBColor HotTrack = new ARGBColor(255, 0, 0, 128);
        public static readonly ARGBColor InactiveBorder = new ARGBColor(255, 212, 208, 200);
        public static readonly ARGBColor InactiveCaption = new ARGBColor(255, 122, 150, 223);
        public static readonly ARGBColor InactiveCaptionText = new ARGBColor(255, 216, 228, 248);
        public static readonly ARGBColor IndianRed = new ARGBColor(255, 205, 92, 92);
        public static readonly ARGBColor Indigo = new ARGBColor(255, 75, 0, 130);
        public static readonly ARGBColor Info = new ARGBColor(255, 255, 255, 225);
        public static readonly ARGBColor InfoText = new ARGBColor(255, 0, 0, 0);
        public static readonly ARGBColor Ivory = new ARGBColor(255, 255, 255, 240);
        public static readonly ARGBColor Khaki = new ARGBColor(255, 240, 230, 140);
        public static readonly ARGBColor Lavender = new ARGBColor(255, 230, 230, 250);
        public static readonly ARGBColor LavenderBlush = new ARGBColor(255, 255, 240, 245);
        public static readonly ARGBColor LawnGreen = new ARGBColor(255, 124, 252, 0);
        public static readonly ARGBColor LemonChiffon = new ARGBColor(255, 255, 250, 205);
        public static readonly ARGBColor LightBlue = new ARGBColor(255, 173, 216, 230);
        public static readonly ARGBColor LightCoral = new ARGBColor(255, 240, 128, 128);
        public static readonly ARGBColor LightCyan = new ARGBColor(255, 224, 255, 255);
        public static readonly ARGBColor LightGoldenrodYellow = new ARGBColor(255, 250, 250, 210);
        public static readonly ARGBColor LightGray = new ARGBColor(255, 211, 211, 211);
        public static readonly ARGBColor LightGreen = new ARGBColor(255, 144, 238, 144);
        public static readonly ARGBColor LightPink = new ARGBColor(255, 255, 182, 193);
        public static readonly ARGBColor LightSalmon = new ARGBColor(255, 255, 160, 122);
        public static readonly ARGBColor LightSeaGreen = new ARGBColor(255, 32, 178, 170);
        public static readonly ARGBColor LightSkyBlue = new ARGBColor(255, 135, 206, 250);
        public static readonly ARGBColor LightSlateGray = new ARGBColor(255, 119, 136, 153);
        public static readonly ARGBColor LightSteelBlue = new ARGBColor(255, 176, 196, 222);
        public static readonly ARGBColor LightYellow = new ARGBColor(255, 255, 255, 224);
        public static readonly ARGBColor Lime = new ARGBColor(255, 0, 255, 0);
        public static readonly ARGBColor LimeGreen = new ARGBColor(255, 50, 205, 50);
        public static readonly ARGBColor Linen = new ARGBColor(255, 250, 240, 230);
        public static readonly ARGBColor Magenta = new ARGBColor(255, 255, 0, 255);
        public static readonly ARGBColor Maroon = new ARGBColor(255, 128, 0, 0);
        public static readonly ARGBColor MediumAquamarine = new ARGBColor(255, 102, 205, 170);
        public static readonly ARGBColor MediumBlue = new ARGBColor(255, 0, 0, 205);
        public static readonly ARGBColor MediumOrchid = new ARGBColor(255, 186, 85, 211);
        public static readonly ARGBColor MediumPurple = new ARGBColor(255, 147, 112, 219);
        public static readonly ARGBColor MediumSeaGreen = new ARGBColor(255, 60, 179, 113);
        public static readonly ARGBColor MediumSlateBlue = new ARGBColor(255, 123, 104, 238);
        public static readonly ARGBColor MediumSpringGreen = new ARGBColor(255, 0, 250, 154);
        public static readonly ARGBColor MediumTurquoise = new ARGBColor(255, 72, 209, 204);
        public static readonly ARGBColor MediumVioletRed = new ARGBColor(255, 199, 21, 133);
        public static readonly ARGBColor Menu = new ARGBColor(255, 255, 255, 255);
        public static readonly ARGBColor MenuBar = new ARGBColor(255, 236, 233, 216);
        public static readonly ARGBColor MenuHighlight = new ARGBColor(255, 49, 106, 197);
        public static readonly ARGBColor MenuText = new ARGBColor(255, 0, 0, 0);
        public static readonly ARGBColor MidnightBlue = new ARGBColor(255, 25, 25, 112);
        public static readonly ARGBColor MintCream = new ARGBColor(255, 245, 255, 250);
        public static readonly ARGBColor MistyRose = new ARGBColor(255, 255, 228, 225);
        public static readonly ARGBColor Moccasin = new ARGBColor(255, 255, 228, 181);
        public static readonly ARGBColor NavajoWhite = new ARGBColor(255, 255, 222, 173);
        public static readonly ARGBColor Navy = new ARGBColor(255, 0, 0, 128);
        public static readonly ARGBColor OldLace = new ARGBColor(255, 253, 245, 230);
        public static readonly ARGBColor Olive = new ARGBColor(255, 128, 128, 0);
        public static readonly ARGBColor OliveDrab = new ARGBColor(255, 107, 142, 35);
        public static readonly ARGBColor Orange = new ARGBColor(255, 255, 165, 0);
        public static readonly ARGBColor OrangeRed = new ARGBColor(255, 255, 69, 0);
        public static readonly ARGBColor Orchid = new ARGBColor(255, 218, 112, 214);
        public static readonly ARGBColor PaleGoldenrod = new ARGBColor(255, 238, 232, 170);
        public static readonly ARGBColor PaleGreen = new ARGBColor(255, 152, 251, 152);
        public static readonly ARGBColor PaleTurquoise = new ARGBColor(255, 175, 238, 238);
        public static readonly ARGBColor PaleVioletRed = new ARGBColor(255, 219, 112, 147);
        public static readonly ARGBColor PapayaWhip = new ARGBColor(255, 255, 239, 213);
        public static readonly ARGBColor PeachPuff = new ARGBColor(255, 255, 218, 185);
        public static readonly ARGBColor Peru = new ARGBColor(255, 205, 133, 63);
        public static readonly ARGBColor Pink = new ARGBColor(255, 255, 192, 203);
        public static readonly ARGBColor Plum = new ARGBColor(255, 221, 160, 221);
        public static readonly ARGBColor PowderBlue = new ARGBColor(255, 176, 224, 230);
        public static readonly ARGBColor Purple = new ARGBColor(255, 128, 0, 128);
        public static readonly ARGBColor Red = new ARGBColor(255, 255, 0, 0);
        public static readonly ARGBColor RosyBrown = new ARGBColor(255, 188, 143, 143);
        public static readonly ARGBColor RoyalBlue = new ARGBColor(255, 65, 105, 225);
        public static readonly ARGBColor SaddleBrown = new ARGBColor(255, 139, 69, 19);
        public static readonly ARGBColor Salmon = new ARGBColor(255, 250, 128, 114);
        public static readonly ARGBColor SandyBrown = new ARGBColor(255, 244, 164, 96);
        public static readonly ARGBColor ScrollBar = new ARGBColor(255, 212, 208, 200);
        public static readonly ARGBColor SeaGreen = new ARGBColor(255, 46, 139, 87);
        public static readonly ARGBColor SeaShell = new ARGBColor(255, 255, 245, 238);
        public static readonly ARGBColor Sienna = new ARGBColor(255, 160, 82, 45);
        public static readonly ARGBColor Silver = new ARGBColor(255, 192, 192, 192);
        public static readonly ARGBColor SkyBlue = new ARGBColor(255, 135, 206, 235);
        public static readonly ARGBColor SlateBlue = new ARGBColor(255, 106, 90, 205);
        public static readonly ARGBColor SlateGray = new ARGBColor(255, 112, 128, 144);
        public static readonly ARGBColor Snow = new ARGBColor(255, 255, 250, 250);
        public static readonly ARGBColor SpringGreen = new ARGBColor(255, 0, 255, 127);
        public static readonly ARGBColor SteelBlue = new ARGBColor(255, 70, 130, 180);
        public static readonly ARGBColor Tan = new ARGBColor(255, 210, 180, 140);
        public static readonly ARGBColor Teal = new ARGBColor(255, 0, 128, 128);
        public static readonly ARGBColor Thistle = new ARGBColor(255, 216, 191, 216);
        public static readonly ARGBColor Tomato = new ARGBColor(255, 255, 99, 71);
        public static readonly ARGBColor Transparent = new ARGBColor(0, 255, 255, 255);
        public static readonly ARGBColor Turquoise = new ARGBColor(255, 64, 224, 208);
        public static readonly ARGBColor Violet = new ARGBColor(255, 238, 130, 238);
        public static readonly ARGBColor Wheat = new ARGBColor(255, 245, 222, 179);
        public static readonly ARGBColor White = new ARGBColor(255, 255, 255, 255);
        public static readonly ARGBColor WhiteSmoke = new ARGBColor(255, 245, 245, 245);
        public static readonly ARGBColor Window = new ARGBColor(255, 255, 255, 255);
        public static readonly ARGBColor WindowFrame = new ARGBColor(255, 0, 0, 0);
        public static readonly ARGBColor WindowText = new ARGBColor(255, 0, 0, 0);
        public static readonly ARGBColor Yellow = new ARGBColor(255, 255, 255, 0);
        public static readonly ARGBColor YellowGreen = new ARGBColor(255, 154, 205, 50);
    }
}