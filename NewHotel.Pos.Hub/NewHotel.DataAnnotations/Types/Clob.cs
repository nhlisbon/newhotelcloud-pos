﻿using System;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.DataAnnotations
{
    [DataContract]
	[Serializable]
    public struct Clob
    {
        [DataMember]
        internal readonly string buffer;

        private string Buffer
        {
            get { return buffer == null ? string.Empty : buffer; }
        }

        public Clob(string value)
        {
            buffer = value;
        }

        public int Size
        {
            get { return Buffer.Length; }
        }

        public static implicit operator byte[](Clob clob)
        {
            return Encoding.Unicode.GetBytes(clob.Buffer);
        }

        public static implicit operator Clob(byte[] value)
        {
            return new Clob(Encoding.Unicode.GetString(value, 0, value.Length));
        }

        public static implicit operator string(Clob clob)
        {
            return clob.Buffer;
        }

        public static implicit operator Clob(string value)
        {
            return new Clob(value);
        }

        public static bool operator ==(Clob clob1, Clob clob2)
        {
            return clob1.Equals(clob2);
        }

        public static bool operator !=(Clob clob1, Clob clob2)
        {
            return !clob1.Equals(clob2);
        }

        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(this, obj))
                return true;

            if (obj is Clob)
                return Buffer.Equals(((Clob)obj).Buffer);

            return false;
        }

        public override int GetHashCode()
        {
            return Buffer.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("Clob({0})", Size);
        }
    }
}