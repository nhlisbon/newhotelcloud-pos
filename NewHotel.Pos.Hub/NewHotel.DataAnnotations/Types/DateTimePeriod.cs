﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace NewHotel.DataAnnotations
{
    public sealed class DateTimePeriod : Period<DateTime>
    {
        public DateTimePeriod(DateTime from, DateTime to)
            : base(from, to)
        {
        }

        public DateTimePeriod(DateTime date)
            : base(date)
        {
        }

        public override IEnumerator<DateTime> GetEnumerator()
        {
            return Enumerable.Range(0, (To - From).Days + 1).Select(x => From.AddDays(x)).GetEnumerator();
        }

        protected override string ToString(DateTime value)
        {
            return value.ToShortDateString();
        }

        protected override DateTime Initialize(DateTime value)
        {
            return value.Date;
        }
    }
}