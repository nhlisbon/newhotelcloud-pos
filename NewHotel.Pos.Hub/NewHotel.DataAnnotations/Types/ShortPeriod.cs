﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace NewHotel.DataAnnotations
{
    public sealed class ShortPeriod : Period<short>
    {
        public ShortPeriod(short from, short to)
            : base(from, to)
        { 
        }

        public override IEnumerator<short> GetEnumerator()
        {
            return Enumerable.Range(0, To - From + 1).Select(x => From + x).Cast<short>().GetEnumerator();
        }
    }
}
