﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.DataAnnotations
{
    [DataContract]
	[Serializable]
    public struct Blob
    {
        [DataMember]
        internal readonly byte[] buffer;

        private byte[] Buffer
        {
            get { return buffer == null ? new byte[0] : buffer; }
        }

        public Blob(byte[] value)
        {
            buffer = value;
        }

        public int Size
        {
            get { return Buffer.Length; }
        }

        public static implicit operator byte[](Blob blob)
        {
            return blob.Buffer;
        }

        public static implicit operator Blob(byte[] value)
        {
            return new Blob(value);
        }

        public static bool operator ==(Blob blob1, Blob blob2)
        {
            return blob1.Equals(blob2);
        }

        public static bool operator !=(Blob blob1, Blob blob2)
        {
            return !blob1.Equals(blob2);
        }

        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(this, obj))
                return true;

            if (obj is Blob)
                Buffer.Equals(((Blob)obj).Buffer);

            return false;
        }

        public override int GetHashCode()
        {
            return Buffer.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("Blob({0})", Size);
        }
    }
}