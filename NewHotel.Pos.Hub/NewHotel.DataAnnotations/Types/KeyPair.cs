﻿using System;

namespace NewHotel.DataAnnotations
{
    public struct KeyPair<T1, T2> : IEquatable<KeyPair<T1, T2>>
        where T1 : IComparable
        where T2 : IComparable
    {
        public readonly T1 Key1;
        public readonly T2 Key2;

        public KeyPair(T1 key1, T2 key2)
        {
            Key1 = key1;
            Key2 = key2;
        }

        public bool Equals(KeyPair<T1, T2> other)
        {
            return Key1.CompareTo(other.Key1) == 0 && Key2.CompareTo(other.Key2) == 0;
        }

        public override string ToString()
        {
            return string.Format("{0}+{1}", Key1, Key2);
        }
    }
}