﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NewHotel.DataAnnotations
{
    public class Period<T> : IEnumerable<T>, IEqualityComparer<Period<T>>
        where T : IComparable
    {
        private T _from;
        private T _to;
        private int _hashCode;

        public T From
        {
            get { return _from; }
            set
            {
                _from = value;
                _hashCode = 0;
            }
        }

        public T To
        {
            get { return _to; }
            set
            {
                _to = value;
                _hashCode = 0;
            }
        }

        public Period(T from, T to)
        {
            _from = Initialize(from);
            _to = Initialize(to);
        }

        public Period(T value)
            : this(value, value)
        {
        }

        public override bool Equals(object obj)
        {
            var period = obj as Period<T>;
            if (period != null)
                return Equals(this, period);

            return false;
        }

        public override int GetHashCode()
        {
            if (_hashCode == 0)
            {
                unchecked
                {
                    _hashCode = 31 * (31 * 17 + From.GetHashCode()) + To.GetHashCode();
                }
            }

            return _hashCode;
        }

        public override string ToString()
        {
            return ToString(From) + " - " + ToString(To);
        }

        public bool Overlaped(Period<T> period)
        {
            return (From.CompareTo(period.From) <= 0 && To.CompareTo(period.From) >= 0 && To.CompareTo(period.To) <= 0) ||
                   (From.CompareTo(period.From) >= 0 && From.CompareTo(period.To) <= 0 && To.CompareTo(period.To) >= 0) ||
                   (From.CompareTo(period.From) > 0 && To.CompareTo(period.To) < 0) ||
                   (From.CompareTo(period.From) < 0 && To.CompareTo(period.To) > 0);
        }

        public bool Overlaped(T from, T to)
        {
            return Overlaped(new Period<T>(from, to));
        }

        protected virtual T Initialize(T value)
        {
            return value;
        }

        protected virtual string ToString(T value)
        {
            return value.ToString();
        }

        #region IEnumerable<T> Members

        public virtual IEnumerator<T> GetEnumerator()
        {
            return new List<T>().GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion

        #region IEqualityComparer<Period<T>>

        public bool Equals(Period<T> x, Period<T> y)
        {
            return x.From.CompareTo(y.From) == 0 && x.To.CompareTo(y.To) == 0;
        }

        public int GetHashCode(Period<T> obj)
        {
            return obj.GetHashCode();
        }

        #endregion
    }
}