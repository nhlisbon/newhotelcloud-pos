﻿using System;

namespace NewHotel.DataAnnotations
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class MappingColumn : MappingAttribute
    {
        public string Name { get; protected set; }
        public bool Nullable { get; set; }
        public int StringSize { get; set; }
        public bool NullStringAsEmpty { get; set; }
        public bool IsTimestamp { get; private set; }
        public bool IsLastModified { get; private set; }

        protected MappingColumn(string name, bool isTimestamp, bool isLastModified)
        {
            Name = name.ToUpperInvariant();
            Nullable = true;
            StringSize = 255;
            NullStringAsEmpty = true;
            IsTimestamp = isTimestamp;
            IsLastModified = isLastModified;
        }

        public MappingColumn(string name)
            : this(name, false, false)
        {
        }
    }
}
