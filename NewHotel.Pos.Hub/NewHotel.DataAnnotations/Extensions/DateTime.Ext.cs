﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;

namespace NewHotel.DataAnnotations
{
    public static class DateTimeExt
    {
        private const string _separator = "/";
        private static readonly Calendar _persianCalendar = new PersianCalendar();
        private static readonly string[] _searchPatterns = new string[] { "d", "m", "y" };
        private static readonly string[] _resultPatterns = new string[] { "dd", "MM", "yyyy" };
        private static readonly IDictionary<string, string[]> _dtFormatPatterns = new Dictionary<string, string[]>();

        private static string[] GetDatePatterns(string shortDatePattern)
        {
            string[] patterns;

            lock (_dtFormatPatterns)
            {
                if (!_dtFormatPatterns.TryGetValue(shortDatePattern, out patterns))
                {
                    patterns = _resultPatterns;
                    Array.Sort(_searchPatterns.Select(p => shortDatePattern
                         .IndexOf(p, StringComparison.CurrentCultureIgnoreCase)).ToArray(), patterns);
                    _dtFormatPatterns.Add(shortDatePattern, patterns);
                }
            }

            return patterns;
        }

        public static DateTime ToUtcDateTime(this DateTime dateTime)
        {
            if (dateTime.Kind != DateTimeKind.Utc)
                return DateTime.SpecifyKind(dateTime, DateTimeKind.Utc);

            return dateTime;
        }

        public static DateTime ToCalendarCultureDateTime(this DateTime dateTime, CultureInfo culture)
        {
            if (string.Compare(culture.TwoLetterISOLanguageName, "FA", StringComparison.CurrentCultureIgnoreCase) == 0)
                dateTime = new DateTime(_persianCalendar.GetYear(dateTime), _persianCalendar.GetMonth(dateTime), _persianCalendar.GetDayOfMonth(dateTime),
                    dateTime.Hour, dateTime.Minute, dateTime.Second, dateTime.Millisecond);

            return dateTime;
        }

        /// <summary>
        /// Format the Datetime to Day and Month with culture order 
        /// </summary>
        /// <param name="dateTime">DateTime</param>
        /// <returns>Formatted String</returns>
        public static string ToMonthAndDay(this DateTime dateTime)
        {
            var culture = CultureInfo.CurrentUICulture;
            dateTime = dateTime.ToCalendarCultureDateTime(culture);

            var datePatterns = GetDatePatterns(culture.DateTimeFormat.ShortDatePattern);
            return string.Join(_separator, datePatterns.Where(p => !p.Contains("y")).Select(p => dateTime.ToString(p)));
        }

        //// <summary>
        /// Format the Datetime to Hour (24 Format) and minutes with culture order 
        /// </summary>
        /// <param name="dateTime">DateTime</param>
        /// <returns>Formatted String</returns>
        public static string ToHourAndMinute(this DateTime dateTime)
        {
            return dateTime.ToString("HH:mm");
        }

        /// <summary>
        /// Format the DateTime with MM/dd - HH24:mm. Month and Day Order is taken from current culture
        /// </summary>
        /// <param name="dateTime">DateTime</param>
        /// <param name="separator">string</param>
        /// <returns>Formatted String</returns>
        public static string ToShortDateTimeString(this DateTime dateTime, string separator = " - ")
        {
            return dateTime.ToMonthAndDay() + separator + dateTime.ToHourAndMinute();
        }

        /// <summary>
        /// Format the DateTime with yyyy/MM/dd. Use short patterns taken from current culture
        /// </summary>
        /// <param name="dateTime">DateTime</param>
        /// <param name="separator">string</param>
        /// <returns>Formatted String</returns>
        public static string ToYearMonthDayString(this DateTime dateTime)
        {
            var culture = CultureInfo.CurrentUICulture;
            dateTime = dateTime.ToCalendarCultureDateTime(culture);

            var datePatterns = GetDatePatterns(culture.DateTimeFormat.ShortDatePattern);
            return string.Join(_separator, datePatterns.Select(p => dateTime.ToString(p)));
        }

        /// <summary>
        /// Format the DateTime with yyyy/MM/dd - HH24:mm. Use short patterns taken from current culture
        /// </summary>
        /// <param name="dateTime">DateTime</param>
        /// <param name="separator">string</param>
        /// <returns>Formatted String</returns>
        public static string ToYearMonthDayTimeString(this DateTime dateTime, string separator = " - ")
        {
            return dateTime.ToYearMonthDayString() + separator + dateTime.ToHourAndMinute();
        }

        public static IEnumerable<DateTime> EachDay(this DateTime start, DateTime end)
        {
            var current = new DateTime(start.Year, start.Month, start.Day);
            while (current <= end)
            {
                yield return current;
                current = current.AddDays(1);
            }
        }
    }
}