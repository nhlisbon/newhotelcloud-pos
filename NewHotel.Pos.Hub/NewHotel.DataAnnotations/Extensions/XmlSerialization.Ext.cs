﻿using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace NewHotel.DataAnnotations
{
    public static class DataContractSerializationExt
    {
        public static string ToDCXml(this object obj, Encoding encoding, bool indent)
        {
            var serializer = new DataContractSerializer(obj.GetType());
            using (var buffer = new MemoryStream())
            {
                using (var writer = XmlDictionaryWriter.Create(buffer, new XmlWriterSettings() { Encoding = encoding, Indent = indent }))
                {
                    serializer.WriteObject(writer, obj);
                }

                buffer.Position = 0;
                using (var stream = new StreamReader(buffer))
                    return stream.ReadToEnd();
            }
        }

        public static string ToDCXml(this object obj, Encoding encoding)
        {
            return ToDCXml(obj, encoding, true);
        }

        public static string ToDCXml(this object obj, bool indent)
        {
            return ToDCXml(obj, Encoding.Default, indent);
        }

        public static string ToDCXml(this object obj)
        {
            return ToDCXml(obj, Encoding.Default, true);
        }
    }

    public static class XmlSerializationExt
    {
        public static string ToXml(this object obj, Encoding encoding, bool indent, params XmlQualifiedName[] namespaces)
        {
            var serializer = new XmlSerializer(obj.GetType());
            using (var buffer = new MemoryStream())
            {
                using (var writer = XmlWriter.Create(buffer, new XmlWriterSettings() { Encoding = encoding, Indent = indent }))
                {
                    if (namespaces != null)
                        serializer.Serialize(writer, obj, new XmlSerializerNamespaces(namespaces));
                    else
                        serializer.Serialize(writer, obj);
                }

                buffer.Position = 0;
                using (var stream = new StreamReader(buffer))
                    return stream.ReadToEnd();
            }
        }

        public static string ToXml(this object obj, Encoding encoding, params XmlQualifiedName[] namespaces)
        {
            return ToXml(obj, encoding, true, namespaces);
        }

        public static string ToXml(this object obj, Encoding encoding)
        {
            return ToXml(obj, encoding, true, null);
        }

        public static string ToXml(this object obj, Encoding encoding, bool indent)
        {
            return ToXml(obj, encoding, indent, null);
        }

        public static string ToXml(this object obj, bool indent, params XmlQualifiedName[] namespaces)
        {
            return ToXml(obj, Encoding.Default, indent, namespaces);
        }

        public static string ToXml(this object obj, params XmlQualifiedName[] namespaces)
        {
            return ToXml(obj, Encoding.Default, true, namespaces);
        }

        public static string ToXml(this object obj)
        {
            return ToXml(obj, Encoding.Default, true, null);
        }

        public static string ToXml(this object obj, bool indent)
        {
            return ToXml(obj, Encoding.Default, indent, null);
        }

        public static T ToObject<T>(this string s, Encoding encoding)
            where T : class
        {
            using (var buffer = new MemoryStream(encoding.GetBytes(s)))
            {
                using (var reader = XmlReader.Create(buffer, new XmlReaderSettings() { IgnoreComments = true, IgnoreWhitespace = true }))
                {
                    var serializer = new XmlSerializer(typeof(T));
                    return (T)serializer.Deserialize(reader);
                }
            }
        }

        public static T ToObject<T>(this string s)
            where T : class
        {
            return ToObject<T>(s, Encoding.UTF8);
        }
    }
}