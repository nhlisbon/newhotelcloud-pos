﻿using System;
using System.Linq;

namespace NewHotel.DataAnnotations
{
    public static class EnumExt
    {
        public static bool TryGetEnum(this Type type, object value, out object obj)
        {
            obj = null;
            if (type.IsEnum)
            {
                var underlyingType = Enum.GetUnderlyingType(type);
                value = Convert.ChangeType(value, underlyingType);
                var outOfBounds = true;
                if (type.GetCustomAttributes(typeof(FlagsAttribute), false).Length > 0)
                {
                    var values = Enum.GetValues(type).Cast<object>();
                    if (underlyingType == typeof(long))
                        outOfBounds = ((long)value & ~values.Sum(x => (long)x)) != 0;
                    else if (underlyingType == typeof(int))
                        outOfBounds = ((int)value & ~values.Sum(x => (int)x)) != 0;
                    else if (underlyingType == typeof(short))
                        outOfBounds = ((short)value & ~values.Sum(x => (short)x)) != 0;
                }
                else
                    outOfBounds = !Enum.IsDefined(type, value);

                if (!outOfBounds)
                {
                    obj = Enum.ToObject(type, value);
                    return true;
                }

                return false;
            }

            throw new ArgumentException("Not an enum type", "obj");
        }
    }
}
