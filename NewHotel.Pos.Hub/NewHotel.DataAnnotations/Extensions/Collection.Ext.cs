﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace NewHotel.DataAnnotations
{
    public static class CollectionExt
    {
        #region Private Class

        private class PredicateEqualityComparer<T> : IEqualityComparer<T>
        {
            private Func<T, T, bool> predicate;

            public PredicateEqualityComparer(Func<T, T, bool> predicate)
            {
                this.predicate = predicate;
            }

            #region IEqualityComparer<T> Members

            public bool Equals(T x, T y)
            {
                return predicate.Invoke(x, y);
            }

            public int GetHashCode(T obj)
            {
                return obj.GetHashCode();
            }

            #endregion
        }

        #endregion
        #region Public Methods

        public static bool Remove<T>(this ICollection<T> coll, Func<T, bool> predicate)
        {
            T[] elementsToDelete = coll.Where(predicate).ToArray();
            foreach (T element in elementsToDelete)
                coll.Remove(element);
            return elementsToDelete.Length > 0;
        }

        public static bool Exists<T>(this ICollection<T> coll, Func<T, bool> predicate)
        {
            var item = coll.FirstOrDefault(predicate);
            return item != null && predicate(item);
        }

        public static bool Duplicates<T>(this ICollection<T> coll, Func<T, bool> predicate)
        {
            return coll.Count(predicate) > 1;
        }

        public static IEnumerable<T> Duplicates<T>(this ICollection<T> coll, Func<T, T, bool> predicate)
        {
            IEnumerable<T> distincts = coll.Distinct(new PredicateEqualityComparer<T>(predicate));

            IList<T> duplicates = new List<T>();
            foreach (var item in distincts)
            {
                if (coll.Count(x => predicate.Invoke(x, item)) > 1)
                    duplicates.Add(item);
            }

            return duplicates;
        }

        #endregion
    }
}
