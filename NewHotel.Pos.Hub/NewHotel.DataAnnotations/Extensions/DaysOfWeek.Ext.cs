﻿using System;
using System.Linq;

namespace NewHotel.DataAnnotations
{
	public static class DaysOfWeekExt
	{
		private static int[] values;

		static DaysOfWeekExt()
		{
			values = Enum.GetValues(typeof(DaysOfWeek)).Cast<int>().ToArray();
		}

		public static bool[] ToArray(this DaysOfWeek daysOfWeek)
		{
			var weekDays = new bool[7];
			if (daysOfWeek != DaysOfWeek.None)
			{
				if ((daysOfWeek & DaysOfWeek.Sunday) == DaysOfWeek.Sunday)
					weekDays[(int)DayOfWeek.Sunday] = true;
				if ((daysOfWeek & DaysOfWeek.Monday) == DaysOfWeek.Monday)
					weekDays[(int)DayOfWeek.Monday] = true;
				if ((daysOfWeek & DaysOfWeek.Tuesday) == DaysOfWeek.Tuesday)
					weekDays[(int)DayOfWeek.Tuesday] = true;
				if ((daysOfWeek & DaysOfWeek.Wednesday) == DaysOfWeek.Wednesday)
					weekDays[(int)DayOfWeek.Wednesday] = true;
				if ((daysOfWeek & DaysOfWeek.Thursday) == DaysOfWeek.Thursday)
					weekDays[(int)DayOfWeek.Thursday] = true;
				if ((daysOfWeek & DaysOfWeek.Friday) == DaysOfWeek.Friday)
					weekDays[(int)DayOfWeek.Friday] = true;
				if ((daysOfWeek & DaysOfWeek.Saturday) == DaysOfWeek.Saturday)
					weekDays[(int)DayOfWeek.Saturday] = true;
			}

			return weekDays;
		}

		public static DaysOfWeek FromArray(this DaysOfWeek daysOfWeek, params bool[] weekDays)
		{
			daysOfWeek = DaysOfWeek.None;
			for (int dayOfWeek = (int)DayOfWeek.Sunday; dayOfWeek <= (int)DayOfWeek.Saturday; dayOfWeek++)
				if (weekDays[dayOfWeek])
					daysOfWeek = daysOfWeek | (DaysOfWeek)values[dayOfWeek + 1];

			return daysOfWeek;
		}

		public static bool Contains(this DaysOfWeek daysOfWeek, DayOfWeek dayOfWeek)
		{
			var day = (DaysOfWeek)values[(int)dayOfWeek + 1];
			return (daysOfWeek & day) == day;
		}

		public static bool ContainsAll(this DaysOfWeek daysOfWeek)
		{
			return daysOfWeek.Contains(DayOfWeek.Sunday) &&
				daysOfWeek.Contains(DayOfWeek.Monday) &&
				daysOfWeek.Contains(DayOfWeek.Thursday) &&
				daysOfWeek.Contains(DayOfWeek.Wednesday) &&
				daysOfWeek.Contains(DayOfWeek.Thursday) &&
				daysOfWeek.Contains(DayOfWeek.Friday) &&
				daysOfWeek.Contains(DayOfWeek.Sunday);
		}
	}
}