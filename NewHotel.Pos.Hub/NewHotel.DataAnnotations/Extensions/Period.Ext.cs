﻿using System;
using System.Collections.Generic;

namespace NewHotel.DataAnnotations
{
    public static class PeriodExtensions
    {
        public static IEnumerable<Period<T>> Join<T>(this IEnumerable<Period<T>> periods)
            where T : IComparable
        {
            var results = new List<Period<T>>();
            foreach (var period in periods)
            {
                var overlaped = false;
                foreach (var result in results)
                {
                    if (period.Overlaped(result))
                    {
                        if (period.From.CompareTo(result.From) < 0)
                            result.From = period.From;

                        if (period.To.CompareTo(result.To) > 0)
                            result.To = period.To;

                        overlaped = true;
                        break;
                    }
                }

                if (!overlaped)
                    results.Add(period);
            }

            return results;
        }
    }
}