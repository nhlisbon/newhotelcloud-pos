﻿using System;
using System.Text;

namespace NewHotel.DataAnnotations
{
    public static class ExceptionExtension
    {
        /// <summary>Emite información de la excepción en formato detallado incluyendo el tipo, el mensaje y el CallStack, de esta excepción y la cadena de InnerExceptions.</summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public static string ToLogString(this Exception exception)
        {
            var sb = new StringBuilder();

            var ex = exception;
            do
            {
                sb.Append("[");
                sb.Append(ex.GetType().Name);
                sb.AppendLine("]");
                sb.AppendLine(ex.Message);
                sb.AppendLine(ex.StackTrace);

                ex = ex.InnerException;
            } while (ex != null);

            return sb.ToString();
        }

        /// <summary>Obtiene el mensaje de la excepción anidada mas interna.</summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public static string InnerMessage(this Exception exception)
        {
            var ex = exception;
            while (ex.InnerException != null)
                ex = ex.InnerException;
            return ex.Message;
        }
    }
}