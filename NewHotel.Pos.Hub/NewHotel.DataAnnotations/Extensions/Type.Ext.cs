﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;

namespace NewHotel.DataAnnotations
{
    public static class TypeExtension
    {
        #region Members

        private static IDictionary<Type, PropertyAccessorCollection> _propAccessors =
            new Dictionary<Type, PropertyAccessorCollection>();

        #endregion
        #region Property Accessor Collection

        private sealed class PropertyAccessorCollection : KeyedCollection<string, IAccessor>
        {
            public readonly IAccessor LastModifiedAccessor;

            protected override string GetKeyForItem(IAccessor item)
            {
                return item.Name;
            }

            public PropertyAccessorCollection(Type type, BindingFlags flags)
            {
                var props = type.GetProperties(flags);

                foreach (var accessor in props
                    .Where(pi => !pi.IsSpecialName && (pi.CanRead || pi.CanWrite) && !ExcludeProperty(pi))
                    .Select(pi => new PropertyAccessor(pi)))
                {
                    if (accessor.IsLastModified)
                        LastModifiedAccessor = accessor;

                    Add(accessor);
                }
            }

            new public IDictionary<string, IAccessor> Dictionary
            {
                get { return base.Dictionary; }
            }
        }

        #endregion
        #region Property Accessor

        private sealed class PropertyAccessor : IAccessor
        {
            private readonly Delegate _getter;
            private readonly Delegate _setter;
            private readonly bool _isNullable;
            private readonly bool _isGenericNullableType;
            private readonly object[] _attributes;

            private readonly string _name;
            private readonly Type _type;
            private readonly Type _declaringType;
            private readonly int _stringSize;
            private readonly bool _nullable;
            private readonly bool _nullStringAsEmpty = true;
            private readonly bool _isTimestamp;
            private readonly bool _isLastModified;

            private readonly IConverter _converter;
            private readonly Type _emptyType = null;

            public string Name
            {
                get { return _name; }
            }

            public Type Type
            {
                get { return _type; }
            }

            public Type DeclaringType
            {
                get { return _declaringType; }
            }

            public bool Nullable
            {
                get { return _nullable; }
            }

            public bool NullStringAsEmpty
            {
                get { return _nullStringAsEmpty; }
            }

            public bool IsTimestamp
            {
                get { return _isTimestamp; }
            }

            public bool IsLastModified
            {
                get { return _isLastModified; }
            }

            public IEnumerable<T> GetAttributes<T>()
            {
                return _attributes.OfType<T>();
            }

            private Type NullableType
            {
                get { return _isGenericNullableType ? _type.GetGenericArguments()[0] : null; }
            }

            public PropertyAccessor(PropertyInfo info)
            {
                _name = info.Name;
                _type = info.PropertyType;
                _declaringType = info.DeclaringType;

                var mappingConversion = info.GetCustomAttributes(typeof(IMappingConversion), true)
                    .OfType<IMappingConversion>().FirstOrDefault();
                if (mappingConversion != null && mappingConversion.Type != null)
                    _converter = (IConverter)Activator.CreateInstance(mappingConversion.Type);

                _attributes = info.GetCustomAttributes(true);
                var attr = GetAttributes<INullableMapping>().FirstOrDefault();
                if (attr != null)
                    _emptyType = attr.EmptyType;

                if (info.CanRead)
                    _getter = BuildGetter(info);

                if (info.CanWrite)
                    _setter = BuildSetter(info);

                _isGenericNullableType = _type.IsGenericType && _type.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
                _isNullable = _isGenericNullableType || info.PropertyType == typeof(string) || info.PropertyType.IsClass || info.PropertyType.IsInterface;

                _nullable = _isNullable;
                if (_nullable || info.PropertyType == typeof(DateTime))
                {
                    var mappingColumn = GetAttributes<MappingColumn>().FirstOrDefault();
                    if (mappingColumn != null)
                    {
                        _stringSize = mappingColumn.StringSize;
                        _nullable = mappingColumn.Nullable;
                        _nullStringAsEmpty = mappingColumn.NullStringAsEmpty;
                        _isTimestamp = mappingColumn.IsTimestamp;
                        _isLastModified = mappingColumn.IsLastModified;
                    }
                }
            }

            public Type BaseType
            {
                get { return NullableType ?? _type; }
            }

            public Type EmptyType
            {
                get { return _emptyType; }
            }

            public string FullName
            {
                get { return string.Format("{0}.{1}", _declaringType.FullName, Name); }
            }

            public bool CanRead
            {
                get { return _getter != null; }
            }

            public object Get(object obj)
            {
                if (!CanRead)
                    throw new InvalidOperationException(string.Format("Can´t read from {0}", FullName));

                object value = null;
                try
                {
                    value = _getter.DynamicInvoke(obj);
                    if (Type.Equals(typeof(string)))
                    {
                        if (value != null && _stringSize > 0)
                        {
                            var str = value.ToString();
                            if (str.Length > _stringSize)
                                value = str.Substring(0, _stringSize);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException(string.Format("Error reading propery {0}", FullName) +
                        "\r\n" + (ex.InnerException != null ? ex.InnerException : ex).Message);
                }

                if (_converter != null)
                    value = _converter.Get(this, value);

                return value;
            }

            public bool CanWrite
            {
                get { return _setter != null; }
            }

            public void Set(object obj, object value)
            {
                if (!CanWrite)
                    throw new InvalidOperationException(string.Format("Can´t write to {0}", FullName));

                if (_converter != null)
                    value = _converter.Set(this, value);

                if (value == null)
                {
                    if (!_isNullable)
                        throw new InvalidOperationException(string.Format("Invalid cast from {0} to {1} in {2}", "null", Type.Name, FullName));
                }
                else
                {
                    var valueType = value.GetType();
                    if (!_type.IsAssignableFrom(valueType))
                        throw new InvalidOperationException(string.Format("Invalid cast from {0} to {1} in {2}", value.GetType().Name, Type.Name, FullName));

                    if (_isNullable)
                    {
                        if ((valueType.Equals(typeof(DateTime)) && value.Equals(DateTime.MinValue)) ||
                            (valueType.Equals(typeof(Guid)) && value.Equals(Guid.Empty)) ||
                            (valueType.IsEnum && value.Equals(0)))
                            value = null;
                    }
                }

                try
                {
                    _setter.DynamicInvoke(obj, value);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException(string.Format("Error writing propery {0}", FullName) +
                        "\r\n" + (ex.InnerException != null ? ex.InnerException : ex).Message);
                }
            }

            public override string ToString()
            {
                return string.Format("Name={0}, Nullable={1}, Read={2}, Write={3}", FullName, Nullable, CanRead, CanWrite);
            }
        }

        #endregion
        #region Private Methods

        private static Delegate BuildGetter(PropertyInfo info)
        {
            var methodInfo = info.GetGetMethod();
            if (methodInfo == null || methodInfo.GetParameters().Length != 0)
                return null;

            var instance = Expression.Parameter(info.DeclaringType, "obj");
            var expr = Expression.Property(instance, info);
            var lambda = Expression.Lambda(expr, instance);
            return lambda.Compile();
        }

        private static Delegate BuildSetter(PropertyInfo info)
        {
            var methodInfo = info.GetSetMethod();
            if (methodInfo == null || methodInfo.GetParameters().Length != 1)
                return null;

            var instance = Expression.Parameter(info.DeclaringType, "obj");
            var argument = Expression.Parameter(info.PropertyType, "arg");
            var expr = Expression.Call(instance, methodInfo, argument);
            var lambda = Expression.Lambda(expr, instance, argument);
            return lambda.Compile();
        }

        private static bool ExcludeProperty(PropertyInfo propInfo)
        {
            const int hide = 1;
            var attrs = propInfo.GetCustomAttributes(true);
            if (attrs.Length > 0)
                return attrs.Cast<Attribute>().Any(x => x.Match(hide));

            return false;
        }

        private static PropertyAccessorCollection GetPropAccessors(this Type type, bool cached)
        {
            PropertyAccessorCollection accessors;

            lock (_propAccessors)
            {
                if (!_propAccessors.TryGetValue(type, out accessors))
                {
                    accessors = new PropertyAccessorCollection(type, BindingFlags.Public | BindingFlags.Instance);
                    if (cached)
                        _propAccessors.Add(type, accessors);
                }
            }

            return accessors;
        }

        #endregion
        #region Accessors

        public static IDictionary<string, IAccessor> GetAccessors(this Type type, BindingFlags flags)
        {
            return new PropertyAccessorCollection(type, flags).Dictionary;
        }

        public static IDictionary<string, IAccessor> PropAccessors(this Type type, bool cached)
        {
            return GetPropAccessors(type, cached).Dictionary;
        }

        public static IDictionary<string, IAccessor> PropAccessors(this Type type)
        {
            return GetPropAccessors(type, true).Dictionary;
        }

        public static IEnumerable<KeyValuePair<string, IAccessor>> PropAccessors(this Type type, Func<KeyValuePair<string, IAccessor>, bool> filter = null)
        {
            return PropAccessors(type).Where(pa => filter(pa));
        }

        public static IAccessor LastModifiedAccessor(this Type type)
        {
            return GetPropAccessors(type, true).LastModifiedAccessor;
        }

        #endregion
    }
}
