﻿using System;

namespace NewHotel.Pos.IoC
{
	public interface IServiceConfigurator
	{
		void RegisterScoped<TService, TImplementation>() where TService : class where TImplementation : TService;
		void RegisterScopedUsingFactory<TService>(Func<TService> factory) where TService : class;

		void RegisterSingleton<TService, TImplementation>() where TService : class where TImplementation : TService;
		void RegisterTransient<TService, TImplementation>() where TService : class where TImplementation : TService;
		void RegisterTransientUsingFactory<TService, TFactory>(Func<TFactory, TService> factory) where TService : class;
		void RegisterTransientUsingFactory<TService>(Func<TService> factory) where TService : class;
		void RegisterSingletonUsingFactory<TService, TFactory>(Func<TFactory, TService> factory) where TService : class;
		void RegisterSingletonUsingFactory<TService>(Func<TService> factory) where TService : class;
	}

	public static class ServiceConfiguratorEx
	{
		public static IServiceConfigurator AddScoped<TService, TImplementation>(this IServiceConfigurator configurator)
			where TService : class
			where TImplementation : TService
		{
			configurator.RegisterScoped<TService, TImplementation>();
			return configurator;
		}

		public static IServiceConfigurator AddScoped<TService>(this IServiceConfigurator configurator, Func<TService> factory)
			where TService : class
		{
			configurator.RegisterScopedUsingFactory(factory);
			return configurator;
		}


		public static IServiceConfigurator AddTransient<TService, TImplementation>(this IServiceConfigurator configurator)
			where TService : class
			where TImplementation : TService
		{
			configurator.RegisterTransient<TService, TImplementation>();
			return configurator;
		}

		public static IServiceConfigurator AddTransient<TService>(this IServiceConfigurator configurator, Func<TService> factory)
			where TService : class
		{
			configurator.RegisterTransientUsingFactory(factory);
			return configurator;
		}

		public static IServiceConfigurator AddTransient<TService, TFactory>(this IServiceConfigurator configurator, Func<TFactory, TService> factory)
			where TService : class
		{
			configurator.RegisterTransientUsingFactory(factory);
			return configurator;
		}
	}
}
