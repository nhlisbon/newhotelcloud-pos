﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel;
using Castle.MicroKernel.Lifestyle;
using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace NewHotel.Pos.IoC
{
	public class PosDependencyResolver
    {
		//private readonly IKernel kernel;
		private readonly IWindsorContainer container;

		public PosDependencyResolver(IWindsorContainer container)
		{
			this.container = container;
		}

		public void RegisterBasedOn<T>()
		{
			//container.Register(Classes.FromAssemblyContaining<T>().BasedOn<T>().WithServiceAllInterfaces().LifestyleScoped());
			container.Register(Classes.FromAssemblyContaining<T>().BasedOn<T>().LifestyleScoped());
		}

		public PosDependencyScope BeginScope()
		{
			// return new PosDependencyScope(this.container.Kernel);
			return new PosDependencyScope(this.container);
		}

		public object GetService(Type serviceType)
		{
			//container.Register(Component.For(serviceType).LifestyleScoped());
			return this.container.Kernel.HasComponent(serviceType) ? this.container.Kernel.Resolve(serviceType) : null;
		}

		public IEnumerable<object> GetServices(Type serviceType)
		{
			return this.container.Kernel.ResolveAll(serviceType).Cast<object>();
		}
	}

	public class PosDependencyScope : IDisposable
	{
		//private readonly IKernel container;
		private readonly IDisposable scope;
		private readonly IWindsorContainer container;

		//public PosDependencyScope(IKernel container)
		public PosDependencyScope(IWindsorContainer container)
		{
			this.container = container;
			//this.scope = container.BeginScope();
			this.scope = container.RequireScope();
		}

		public void Dispose()
		{
			this.scope?.Dispose();
		}

		public object GetService(Type serviceType)
		{
			return container.Resolve(serviceType);
			// return container.Kernel.HasComponent(serviceType) ? container.Resolve(serviceType) : null;
		}

		public IEnumerable<object> GetServices(Type serviceType)
		{
			return container.ResolveAll(serviceType).Cast<object>();
		}
	}
}
