﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel;
using Castle.MicroKernel.Context;
using Castle.MicroKernel.Lifestyle;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Releasers;
using Castle.Windsor;
using Castle.Windsor.Installer;

namespace NewHotel.Pos.IoC
{
	public class CwFactory : IServiceConfigurator
	{
		private readonly WindsorContainer _container;
		private static CwFactory _instance;
		private CwFactory()
		{
			_container = new WindsorContainer();
			_container.Kernel.ReleasePolicy = new NoTrackingReleasePolicy();
		}

		public static CwFactory Instance => _instance ??= new CwFactory();

		public object Resolve(Type type) => _container.Resolve(type);
		public IDisposable BeginScope() => _container.BeginScope();
		public IDisposable RequireScope() => _container.RequireScope();

		public PosDependencyResolver GetDependencyResolver() => new PosDependencyResolver(_container);

		public static CwFactory Register<T, K>() where T : class where K : T
		{
			ComponentRegistration<T> registration = RegisterUsingUniqueName<T, K>();
			Instance._container.Register(registration);
			return Instance;
		}

		public static CwFactory RegisterWithFactoryMethod<T, K>(Func<K> factoryFunc) where T : class where K : T
		{
			var registration = RegisterUsingUniqueName<T, K>().UsingFactoryMethod(factoryFunc);
			Instance._container.Register(registration);
			return Instance;
		}

		public static CwFactory RegisterWithFactoryMethodSingleton<T, K>(Func<K> factoryFunc) where T : class where K : T
		{
			var registration = RegisterUsingUniqueNameSingleton<T, K>().UsingFactoryMethod(factoryFunc).LifeStyle.Singleton;
			Instance._container.Register(registration);
			return Instance;
		}

		public static CwFactory RegisterWithFactoryMethod<T>(Func<T> factoryFunc) where T : class
		{
			var registration = RegisterUsingUniqueName<T>().UsingFactoryMethod(factoryFunc);
			Instance._container.Register(registration);
			return Instance;
		}

		public static CwFactory RegisterWithFactoryMethod<TArg, T>(Func<TArg, T> func) where T : class
		{
			var registration = RegisterUsingUniqueName<T>()
				.UsingFactoryMethod((k, c) => func((TArg)c.AdditionalArguments.FirstOrDefault().Value));
			Instance._container.Register(registration);
			return Instance;
		}

		public static CwFactory RegisterWithFactoryMethod<TArg1, TArg2, T>(Func<TArg1, TArg2, T> func) where T : class
		{
			var registration = RegisterUsingUniqueName<T>()
				.UsingFactoryMethod((k, c) => func((TArg1)c.AdditionalArguments.ElementAt(0).Value,(TArg2)c.AdditionalArguments.ElementAt(1).Value));
			Instance._container.Register(registration);
			return Instance;
		}

		public static T Resolve<T>() => Instance._container.Resolve<T>();

		public static T Resolve<T>(params (string name, object value)[] args)
		{
			var arguments = new Arguments();
			arguments.Add(args.Select(x => new KeyValuePair<object, object>(x.name, x.value)));
			return Instance._container.Resolve<T>(arguments);
		}

		private static ComponentRegistration<T> RegisterUsingUniqueName<T, K>() where T : class where K : T
		{
			return Component.For<T>().ImplementedBy<K>().LifeStyle.Transient
				.Named(Guid.NewGuid().ToString());
		}

		private static ComponentRegistration<T> RegisterUsingUniqueNameSingleton<T, K>() where T : class where K : T
		{
			return Component.For<T>().ImplementedBy<K>().LifeStyle.Singleton
				.Named(Guid.NewGuid().ToString());
		}

		private static ComponentRegistration<T> RegisterUsingUniqueNameSingleton<T>() where T : class
		{
			return Component.For<T>().LifeStyle.Singleton
				.Named(Guid.NewGuid().ToString());
		}

		private static ComponentRegistration<T> RegisterUsingUniqueNameScoped<T, K>() where T : class where K : T
		{
			return Component.For<T>().ImplementedBy<K>().LifestyleScoped()
				.Named(Guid.NewGuid().ToString());
		}

		private static ComponentRegistration<T> RegisterUsingUniqueName<T>() where T : class
		{
			return Component.For<T>().LifeStyle.Transient
				.Named(Guid.NewGuid().ToString());
		}

		public static CwFactory RegisterWithFactoryMethodScoped<T, K>(Func<K> factoryFunc) where T : class where K : T
		{
			var registration = RegisterUsingUniqueNameScoped<T, K>().UsingFactoryMethod(factoryFunc);
			//Instance._container.Register(registration);
			Instance._facility.Register(registration);
			return Instance;
		}

		public void RegisterWcfService<T, K>() where T : class where K : class, T
		{
			var registration = Component
				.For<K>()
				//.LifestyleScoped()
				.LifestylePerWcfOperation()
				.AsWcfService(new DefaultServiceModel().Hosted())
				.Named(typeof(K).GUID.ToString())
				;
			//var registration = Component
			//			 .For<T>()
			//			 .ImplementedBy<K>()
			//			 .LifestylePerWcfOperation()
			//			 .AsWcfService(new DefaultServiceModel().Hosted())
			//	.Named(Guid.NewGuid().ToString());
			_container.Register(registration);
			_facility.Register(registration);
		}

		public T ResolveScoped<T>()
		{
			return _container.Resolve<T>();
		}

		DefaultServiceHostFactory _factory;
		IWindsorContainer _facility;
		public void InitializeForWcfServices()
		{
			_facility = _container.AddFacility<WcfFacility>(c => c.CloseTimeout = TimeSpan.Zero);
			_factory = new DefaultServiceHostFactory(_container.Kernel);
		}

		public ServiceHostBase CreateServiceHost(Type type) => _factory.CreateServiceHost(type.AssemblyQualifiedName, []);

		public void RegisterScoped<TService, TImplementation>() where TService : class where TImplementation : TService
		{
			var registration = RegisterUsingUniqueNameScoped<TService, TImplementation>();
			_container.Register(registration);
		}

		public void RegisterScopedUsingFactory<TService>(Func<TService> factory) where TService : class
		{
			var registration = RegisterUsingUniqueNameScoped<TService, TService>().UsingFactoryMethod(factory);
			_container.Register(registration);
		}


		public void RegisterSingleton<TService, TImplementation>() where TService : class where TImplementation : TService
		{
			var registration = RegisterUsingUniqueNameSingleton<TService, TImplementation>();
			_container.Register(registration);
		}


		public void RegisterTransient<TService, TImplementation>() where TService : class where TImplementation : TService
		{
			var registration = RegisterUsingUniqueName<TService, TImplementation>();
			_container.Register(registration);
		}

		public void RegisterTransientUsingFactory<TService, TFactory>(Func<TFactory, TService> factory) where TService : class
		{
			var registration = RegisterUsingUniqueName<TService>().UsingFactory(factory);
			_container.Register(registration);
		}

		public void RegisterTransientUsingFactory<TService>(Func<TService> factory) where TService : class
		{
			var registration = RegisterUsingUniqueName<TService>().UsingFactoryMethod(factory);
			_container.Register(registration);
		}

		public void RegisterSingletonUsingFactory<TService, TFactory>(Func<TFactory, TService> factory) where TService : class
		{
			var registration = RegisterUsingUniqueNameSingleton<TService>().UsingFactory(factory);
			_container.Register(registration);
		}

		public void RegisterSingletonUsingFactory<TService>(Func<TService> factory) where TService : class
		{
			var registration = RegisterUsingUniqueNameSingleton<TService>().UsingFactoryMethod(factory);
			_container.Register(registration);
		}
	}

	public class CreationContext
	{
		private Castle.MicroKernel.Context.CreationContext _creation;

		public CreationContext(Castle.MicroKernel.Context.CreationContext creation)
		{
			this._creation = creation;
		}

		public object GetArgument(int ind) => _creation.AdditionalArguments.FirstOrDefault().Value;
	}
}
