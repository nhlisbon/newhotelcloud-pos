﻿using System.Globalization;

namespace NewHotel.Fiscal.Dfacture
{
    public static class Dfacture
    {
        #region Members

        public static readonly CultureInfo Culture = CultureInfo.InvariantCulture;

        #endregion
        #region Public Methods

        // proxy 2.1
        public static DfactureService.ServiceClient GetProxy21(bool testMode)
        {
            return testMode
                ? new DfactureService.ServiceClient("DfactureBasicHttpBindingTest_IService")
                : new DfactureService.ServiceClient("DfactureBasicHttpBinding_IService");
        }

        #endregion
    }
}