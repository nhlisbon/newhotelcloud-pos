﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Core
{
    #region Table & View Mapping Attributes

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class PersistentTable : MappingTable
    {
        public PersistentTable(string name, string id, byte index)
            : base(name, id, index) { }

        public PersistentTable(string name, string id)
            : base(name, id) { }

        public PersistentTable(string name)
            : base(name, string.Empty) { }
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class PersistentView : MappingTable
    {
        public PersistentView(string name)
            : base(name, string.Empty) { }
    }

    #endregion
    #region Table & View Column Mapping Attributes

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class PersistentColumn : MappingColumn
    {
        public PersistentColumn(string name)
            : base(name) { }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class PersistentTimestampColumn : MappingColumn
    {
        public PersistentTimestampColumn(string name)
            : base(name, true, false) { }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class PersistentLastModifiedColumn : MappingColumn
    {
        public PersistentLastModifiedColumn(string name)
            : base(name, true, true) { }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class PersistentRefColumn : MappingAttribute
    {
        /// <summary>
        /// Represent referenced table and column in the form: TABLE.COLUMN
        /// </summary>
        public string TableColumnName { get; protected set; }
        /// <summary>
        /// Represent referenced table primary key
        /// </summary>
        public string PrimaryColumnId { get; protected set; }
        /// <summary>
        /// Represent foreign key to a referenced table
        /// </summary>
        public string ReferencedColumnId { get; protected set; }
        /// <summary>
        /// Represent aditional filters in clause
        /// </summary>
        public string[] Filters { get; protected set; }

        public PersistentRefColumn(string name, string pkId, string fkId)
        {
            TableColumnName = name.ToUpper();
            PrimaryColumnId = pkId.ToUpper();
            ReferencedColumnId = fkId.ToUpper();
            Filters = new string[0];
        }

        public PersistentRefColumn(string name, string pkId) 
            : this(name, pkId, pkId)
        {
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class PersistentExpressionColumn : MappingAttribute
    {
        /// <summary>
        /// Represent an sql expression
        /// </summary>
        public string Expression { get; protected set; }
        /// <summary>
        /// Represent an expression alias
        /// </summary>
        public string Alias { get; protected set; }

        public PersistentExpressionColumn(string expression, string alias)
        {
            Expression = expression.ToUpper();
            Alias = alias.ToUpper();
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class PersistentTranslation : PersistentRefColumn
    {
        /// <summary>
        /// Table that's serve as a relation between table in wich
        ///  we want translation and tranlation's table
        /// </summary>
        public string RelationTablePk { get; protected set; }
        /// <summary>
        /// Foreign key to the relation table
        /// </summary>
        public string ForeignKeyToRelationTable { get; protected set; }

        /// <summary>
        /// Mark the property as a referenced translation
        /// </summary>
        /// <param name="foreignKeyRelationTableToTranslationTable">Foreign key from relation table to translation table</param>
        /// <param name="relationTablePk">Table that's serve as a relation between table in wich we want translation and translation's table</param>
        /// <param name="foreignKeyToRelationTable">Foreign key to the relation table</param>
        public PersistentTranslation(string foreignKeyRelationTableToTranslationTable, string relationTablePk, string foreignKeyToRelationTable)
            : base("VNHT_MULT.MULT_DESC", "LITE_PK", foreignKeyRelationTableToTranslationTable)
        {
            RelationTablePk = relationTablePk;
            ForeignKeyToRelationTable = foreignKeyToRelationTable;
            Filters = new string[] { "VNHT_MULT.LANG_PK" };
        }

        public PersistentTranslation(string foreignTableTranslationPkId, string foreignTablePkId)
            : this(foreignTableTranslationPkId, foreignTablePkId, foreignTablePkId.Substring(foreignTablePkId.IndexOf('.') + 1))
        {
        }

        public PersistentTranslation(string foreignTableTranslationPkId)
            : this(foreignTableTranslationPkId, string.Empty, string.Empty)
        {
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class PersistentDomainTranslation : PersistentTranslation
    {
        public PersistentDomainTranslation(string foreignKeyToRelationTable)
            : base("LITE_PK", "TNHT_ENUM.ENUM_PK", foreignKeyToRelationTable)
        {
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class PersistentObject : MappingAttribute, INullableMapping
    {
        public string Name { get; protected set; }
        public Type EmptyType { get; set; }
        public bool CascadePersist { get; set; }
        public bool CascadeDelete { get; set; }

        #region INullableMapping Members

        Type INullableMapping.EmptyType
        {
            get { return EmptyType; }
        }

        #endregion

        public PersistentObject(string name)
        {
            Name = name.ToUpper();
        }
    }

    #endregion
}