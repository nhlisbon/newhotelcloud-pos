﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace NewHotel.Core
{
    public static class SqlUtils
    {
        #region Private Constants

        private const string SelectToken = "SELECT";
        private const string FromToken = "FROM";
        private const string WhereToken = "WHERE";
        private const string UnionToken = "UNION";
        private const char LeftParentesis = '(';
        private const char RightParentesis = ')';
        private const char TextDelimiter = '\'';
        private const char ParamPrefix = ':';

        #endregion
        #region Private Members

        private static char[] spaceDelimiters = { ' ', '\r', '\n' };
        private static char[] colDelimiters = new char[] { ' ', '.', '\r', '\n' };
        private static char[] paramDelimiters = new char[] { ' ', ',', ')', '-', '+', '*', '/', '|', '!', '=', '\r', '\n' };
        private static char[] parentesis = new char[] { LeftParentesis, RightParentesis };

        #endregion
        #region Private Methods

        private static int IndexOf(string sql, char[] chars, int start, char leftskip, char rightskip)
        {
			var index = 0;
            do
            {
                var delimiters = new List<char>(chars);
                delimiters.Add(leftskip);
                try
                {
                    index = sql.IndexOfAny(delimiters.ToArray(), start);
                }
                catch
                {
                    throw;
                }
                if (index < 0 || sql[index] != leftskip)
                    break;
                else
                {
                    start = index + 1;
                    var count = 1;
                    while (count > 0)
                    {
                        index = sql.IndexOfAny(new char[] { leftskip, rightskip }, start);
                        if (index < 0)
                            break;
                        if (sql[index] == rightskip)
                            count--;
                        else if (sql[index] == leftskip)
                            count++;
                        start = index + 1;
                    }
                }
            } while (true);

            return index;
        }

        private static int FindToken(string sql, int index, string token)
        {
            do
            {
                index = sql.IndexOfAny(new char[] { LeftParentesis, token[0] }, index);

                if (index >= 0)
                {
                    if (sql[index].Equals(LeftParentesis))
                    {
                        int count = 0;
                        do
                        {
                            index = sql.IndexOfAny(parentesis, index);
                            if (index < 0)
                                break;
                            else
                            {
                                switch (sql[index])
                                {
                                    case LeftParentesis:
                                        count++;
                                        break;
                                    case RightParentesis:
                                        count--;
                                        break;
                                }
                                index++;
                            }
                        }
                        while (count > 0);
                    }
                    else if (StringComparer.InvariantCultureIgnoreCase.Compare(sql.Substring(index, Math.Min(sql.Length - index, token.Length)), token) == 0)
                        break;
                    else
                        index++;
                }
                else
                    break;

                if (index < 0)
                    break;
            } while (index < sql.Length);

            return index;
        }

        #endregion
		#region Public Methods

		public static string[] ExtractColumnsFromSql(string sql)
        {
            var names = new HashSet<string>();

            var exit = false;
            var start = 0;
            do
            {
                var i = IndexOf(sql, new char[] { ',', FromToken.ToUpperInvariant()[0], FromToken.ToLowerInvariant()[0] }, start, '(', ')');
                if (i < 0)
                    break;

                if (sql[i] == FromToken.ToUpperInvariant()[0] || sql[i] == FromToken.ToLowerInvariant()[0])
                {
                    var length = i + FromToken.Length - 1;
                    if (length <= sql.Length && sql.Substring(i, FromToken.Length).Equals(FromToken, StringComparison.InvariantCultureIgnoreCase) &&
                        (sql[i - 1] == ' ' || sql[i - 1] == '\n') && (sql[length + 1] == ' ' || sql[length + 1] == '\r'))
                        exit = true;
                    else
                    {
                        start = i + 1;
                        continue;
                    }
                }

                start = i + 1;
                var end = i - 1;
                while (spaceDelimiters.Contains(sql[end]))
                    end--;

                i = sql.LastIndexOfAny(colDelimiters, end);
                if (i >= 0)
                    names.Add(sql.Substring(i + 1, end - i).Trim());
            } while (!exit);

            return names.ToArray();
        }

        public static string[] ExtractParametersFromSql(string sql)
        {
            var names = new HashSet<string>();

            var index = 0;
            do
            {
                var begin = IndexOf(sql, new char[] { ParamPrefix }, index, TextDelimiter, TextDelimiter);
                if (begin < 0)
                    break;

                var end = sql.IndexOfAny(paramDelimiters, ++begin);
                if (end >= 0)
                    names.Add(sql.Substring(begin, end - begin).Trim().ToUpperInvariant());
                index = end;
            } while (index < sql.Length);

            return names.ToArray();
        }

        public static bool SqlContainsWhere(string sql)
        {
            return FindToken(sql, 0, WhereToken) >= 0;
		}

        public static string GetInParameterExpression(string fieldName, string paramName, IEnumerable<object> suffixes)
        {
            return fieldName + " in (" + string.Join(",", suffixes.Select(x => ":" + paramName + x.ToString()).ToArray()) + ")";
        }

        public static string GetNotInParameterExpression(string fieldName, string paramName, IEnumerable<object> suffixes)
        {
            return fieldName + " not in (" + string.Join(",", suffixes.Select(x => ":" + paramName + x.ToString()).ToArray()) + ")";
        }

        public static string GetInParameterExpression(string fieldName, string paramName, int count)
        {
            if (count > 1)
                return GetInParameterExpression(fieldName, paramName, Enumerable.Range(0, count).Cast<object>());
            else if (count > 0)
                return string.Format("{0} = :{1}0", fieldName, paramName);

            return string.Format("{0} = :{1}", fieldName, paramName);
        }

        public static string GetNotInParameterExpression(string fieldName, string paramName, int count)
        {
            if (count > 1)
                return GetNotInParameterExpression(fieldName, paramName, Enumerable.Range(0, count).Cast<object>());
            else if (count > 0)
                return string.Format("{0} != :{1}0", fieldName, paramName);

            return string.Format("{0} != :{1}", fieldName, paramName);
        }

		public static string GetInParameterExpression(string name, int count)
		{
            return GetInParameterExpression(name, name, count);
		}

        public static string GetNotInParameterExpression(string name, int count)
        {
            return GetNotInParameterExpression(name, name, count);
        }

		#endregion
	}
}
