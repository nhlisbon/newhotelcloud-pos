﻿using System.Collections.ObjectModel;

namespace NewHotel.Core
{
    public class PersistentCollection<T> : KeyedCollection<object, T>
        where T : BasePersistent
    {
        public PersistentCollection() : base(null, 10) { }

        protected override object GetKeyForItem(T item)
        {
            return item.Id;
        }

        public bool TryGetValue(object key, out T value)
        {
            return Dictionary.TryGetValue(key, out value);
        }
    }
}