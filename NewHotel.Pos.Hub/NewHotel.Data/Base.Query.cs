﻿using System.Text;

namespace NewHotel.Core
{
    /// <summary>
    /// Custom queries
    /// </summary>
    public class BaseQuery : Query
    {
        private readonly string _commandText;
        private string _groupBy;

        public BaseQuery(IDatabaseManager manager, string name, params string[] lines)
            : base(name, manager)
        {
            var sb = new StringBuilder();
            foreach (var line in lines)
                sb.AppendLine(line.Trim().ToUpper());

            _commandText = sb.ToString();
        }

        public void AddGroupBy(params string[] groupBy)
        {
            _groupBy = string.Join(",", groupBy);
        }

        protected override bool CommandTextCached
        {
            get { return false; }
        }

        protected override string GetCommandText()
        {
            return _commandText;
        }

        protected override string GetCommandGroupBy()
        {
            return _groupBy;
        }
    }
}