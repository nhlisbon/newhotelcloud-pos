﻿//#define O12
using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Diagnostics;
using NewHotel.DataAnnotations;

namespace NewHotel.Core
{
    [DataContract]
    [Serializable]
    public abstract class Filter : IFilter, IEnumerable<Filter>, IOperFilter
    {
        [DataMember(Name = "Name")]
        internal string _name;
        [DataMember(Name = "Text")]
        internal string _text;
        [DataMember(Name = "IsManual")]
        internal bool _isManual;
        [DataMember]
        public readonly string Expr;
        [DataMember(Name = "Owner")]
        internal readonly IFilterOwner _owner;
        public abstract FilterTypes[] Operators { get; }
        [DataMember]
        public abstract FilterTypes Operator { get; set; }
        [DataMember]
        public abstract object Value { get; set; }
        [DataMember]
        public abstract bool Enabled { get; set; }
        public abstract IEnumerable<string> Names { get; }

        internal Filter() { }

        public Filter(IFilterOwner owner, string name, string expr, string text)
        {
            _owner = owner;
            _name = name;
            _text = text;
            Expr = expr.ToUpper();
        }

        public Filter(IFilterOwner owner, string name, string expr)
            : this(owner, name, expr, null) { }

        public bool IsManual
        {
            get { return _isManual; }
            protected set { _isManual = value; }
        }

        protected virtual IEnumerator<Filter> Enumerator
        {
            get
            {
                var list = new List<Filter>();
                list.Add(this);
                return list.GetEnumerator();
            }
        }

        #region IEnumerable<Filter> Members

        public IEnumerator<Filter> GetEnumerator()
        {
            return Enumerator;
        }

        #endregion
        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Enumerator;
        }

        #endregion

        public static FilterTypes[] GetOperators(Type type)
        {
            FilterTypes[] opers;
            if (type == typeof(string))
                opers = new FilterTypes[] { FilterTypes.Equal, FilterTypes.NotEqual, FilterTypes.Like, FilterTypes.In, FilterTypes.NotIn };
            else if (type == typeof(Guid))
                opers = new FilterTypes[] { FilterTypes.Equal, FilterTypes.NotEqual, FilterTypes.In, FilterTypes.NotIn };
            else if (type == typeof(bool))
                opers = new FilterTypes[] { FilterTypes.Equal, FilterTypes.NotEqual };
            else if (type == typeof(DateTime))
                opers = new FilterTypes[] { FilterTypes.Equal, FilterTypes.NotEqual, FilterTypes.GreaterThan, FilterTypes.GreaterThanOrEqual, FilterTypes.LessThan, FilterTypes.LessThanOrEqual, FilterTypes.In, FilterTypes.NotIn, FilterTypes.Between };
            else
                opers = new FilterTypes[] { FilterTypes.Equal, FilterTypes.NotEqual, FilterTypes.GreaterThan, FilterTypes.GreaterThanOrEqual, FilterTypes.LessThan, FilterTypes.LessThanOrEqual, FilterTypes.In, FilterTypes.NotIn, FilterTypes.Between, FilterTypes.Like };

            return opers;
        }

        #region IFilter Members

        public string Name
        {
            get { return _name.ToUpperInvariant(); }
        }

        public string Text
        {
            get { return _text ?? Name; }
        }

        public abstract IEnumerable<IFilter> Filters { get; }

        #endregion
    }

    public class OperFilter : Filter
    {
        private string[] _filterOpers = { "=", "!=", "<", "<=", ">", ">=", "IN", "BETWEEN", "LIKE", "NOT IN", "MEMBER OF" };

        private const long FilterOffset = 2221;
        private const string IsEqual = "=";
        private const string IsNotEqual = "!=";
        private const string IsNull = "IS NULL";
        private const string IsNotNull = "IS NOT NULL";

        public const int InOperatorLimit = 999;

        public readonly Type Type;

        public override FilterTypes[] Operators
        {
            get { return GetOperators(Type); }
        }

        private FilterTypes _operator;
        public override FilterTypes Operator
        {
            get { return _operator; }
            set
            {
                if (Operators.Contains(value))
                    _operator = value;
            }
        }

        private object _value;
        public override object Value
        {
            get
            {
                if (!_enabled)
                    return null;

                return _value;
            }
            set
            {
                if (_value != value)
                {
                    if (value != null && value.GetType() == typeof(string))
                    {
                        if (Type.IsEnum)
                        {
                            try
                            {
                                _value = Enum.Parse(Type, value.ToString(), true);
                            }
                            catch (ArgumentException)
                            {
                                throw new ArgumentOutOfRangeException(_name, value, "Invalid query argument value.");
                            }
                        }
                        else if (Type == typeof(Guid))
                        {
                            Guid guid;
                            if (Guid.TryParse(value.ToString(), out guid))
                                _value = guid;
                            else
                                throw new ArgumentOutOfRangeException(_name, value, "Invalid query argument value.");
                        }
                        else
                        {
                            try
                            {
                                _value = Convert.ChangeType(value, Type);
                            }
                            catch (InvalidCastException)
                            {
                                _value = value;
                            }
                        }
                    }
                    else
                        _value = value;

                    _owner.Invalidate();
                }
            }
        }

        private bool _enabled = true;
        public override bool Enabled
        {
            get { return _enabled; }
            set
            {
                if (_enabled != value)
                {
                    _enabled = value;
                    _owner.Invalidate();
                }
            }
        }

        public override IEnumerable<string> Names
        {
            get
            {
                switch (Operator)
                {
                    case FilterTypes.In:
                    case FilterTypes.NotIn:
                        if (Value.GetType().IsArray)
                        {
                            var values = ((IEnumerable)Value).Cast<object>()
                                .Where(x => x != null).Distinct().ToArray();

                            if (values.Length > 1)
                                return values.Select((x, i) => Name + i.ToString());
                        }

                        break;
                    case FilterTypes.Between:
                        if (Value.GetType().IsArray)
                        {
                            var values = ((IEnumerable)Value).Cast<object>()
                                .Where(x => x != null).ToArray();

                            if (values.Length == 2)
                            {
                                if (values[0] != null && values[1] != null && !values[0].Equals(values[1]))
                                    return values.Select((x, i) => Name + i.ToString());
                            }
                        }

                        break;
                }

                return new string[] { Name };
            }
        }

        public OperFilter(IFilterOwner owner, string name, string expr, Type type, FilterTypes filter, string text)
            : base(owner, name, expr, text)
        {
            Type = type;
            Operator = filter;
            IsManual = owner.ExcludeFilter(name);
        }

        public OperFilter(IFilterOwner owner, string name, string expr, Type type, FilterTypes filter)
            : this(owner, name, expr, type, filter, name) { }

        public OperFilter(IFilterOwner owner, string name, string expr, Type type, string text)
            : this(owner, name, expr, type, FilterTypes.Equal, text) { }

        public OperFilter(IFilterOwner owner, string name, string expr, Type type)
            : this(owner, name, expr, type, name) { }

        public OperFilter(IFilterOwner owner, string name, string expr, Type type, FilterTypes filter, object[] values, string text)
            : this(owner, name, expr, type, filter, text)
        {
            Value = values.Length == 1 ? values[0] : values;
            if (values != null)
                owner.Invalidate();
        }

        public OperFilter(IFilterOwner owner, string name, string expr, Type type, FilterTypes filter, object[] values)
            : this(owner, name, expr, type, filter, values, name) { }

        public OperFilter(IFilterOwner owner, string name, string expr, Type type, object[] values, string text)
            : this(owner, name, expr, type, FilterTypes.Equal, values, text) { }

        public OperFilter(IFilterOwner owner, string name, string expr, Type type, object[] values)
            : this(owner, name, expr, type, FilterTypes.Equal, values, name) { }

        public override IEnumerable<IFilter> Filters
        {
            get { return new IFilter[0]; }
        }

        public override string ToString()
        {
            var value = Query.GetArgumentValue(Value);
            if (Enabled && value != null)
            {
                var oper = _filterOpers[(long)Operator - FilterOffset];

                switch (Operator)
                {
                    case FilterTypes.Equal:
                        return Expr + " " + (value == DBNull.Value ? IsNull : oper + " :" + Name);
                    case FilterTypes.NotEqual:
                        return Expr + " " + (value == DBNull.Value ? IsNotNull : oper + " :" + Name);
                    case FilterTypes.In:
                    case FilterTypes.NotIn:
                        if (value.GetType().IsArray)
                        {
                            var values = ((IEnumerable)value).Cast<object>()
                                .Where(x => x != null).Distinct().ToArray();

                            if (values.Length == 0)
                                break;
                            else if (values.Length > InOperatorLimit &&
                                values[0] != null && values[0].GetType() == typeof(Guid))
                            {
                                var count = values.Length / InOperatorLimit;
                                var remainder = values.Length % InOperatorLimit;

                                var exprs = new List<string>();
                                for (int i = 0; i < count; i++)
                                    exprs.Add(Expr + " " + oper + " (" + string.Join(", ", Enumerable.Range(InOperatorLimit * i, InOperatorLimit).Select(n => ":" + Name + n.ToString())) + ")");

                                if (remainder > 0)
                                    exprs.Add(Expr + " " + oper + " (" + string.Join(", ", Enumerable.Range(InOperatorLimit * count, remainder).Select(n => ":" + Name + n.ToString())) + ")");

                                return "(" + string.Join(" OR ", exprs) + ")";
                            }
                            else if (values.Length > 1)
                            {
                                var names = values.Select((x, i) => ":" + Name + i.ToString());
                                return Expr + " " + oper + " (" + string.Join(", ", names.ToArray()) + ")";
                            }
                        }

                        return Expr + " " + (Operator == FilterTypes.In ? IsEqual : IsNotEqual) + " :" + Name;
                    case FilterTypes.Between:
                        if (value.GetType().IsArray)
                        {
                            var values = ((IEnumerable)value).Cast<object>()
                                .Where(x => x != null).Distinct().ToArray();

                            if (values.Length == 2)
                                return Expr + " " + oper + " :" + Name + "0 AND :" + Name + "1";
                            else if (values.Length == 1)
                                return Expr + " " + IsEqual + " :" + Name;
                        }
                        break;
                    default:
                        return Expr + " " + oper + " :" + Name;
                }
            }

            return string.Empty;
        }
    }

    public class TranslationFilter : Filter
    {
        private static readonly string _translationCommandText;

        static TranslationFilter()
        {
            var sb = new StringBuilder();
            sb.Append("(SELECT UPPER(MULT.MULT_DESC) FROM VNHT_MULT MULT");
            sb.Append(" WHERE MULT.LANG_PK = :{0}_LANG_PK AND MULT.LITE_PK = {1})");
            sb.Append(" LIKE UPPER(:{0})");

            _translationCommandText = sb.ToString();
        }

        public readonly Type Type = typeof(string);

        public override FilterTypes[] Operators
        {
            get { return new FilterTypes[] { FilterTypes.Like }; }
        }

        private FilterTypes _operator;
        public override FilterTypes Operator
        {
            get { return _operator; }
            set
            {
                if (Operators.Contains(value))
                    _operator = value;
            }
        }

        private object _value;
        public override object Value
        {
            get
            {
                if (!_enabled)
                    return null;

                return _value;
            }
            set
            {
                if (_value != value)
                {
                    _value = value;
                    _owner.Invalidate();
                }
            }
        }

        private bool _enabled = true;
        public override bool Enabled
        {
            get { return _enabled; }
            set
            {
                if (_enabled != value)
                {
                    _enabled = value;
                    _owner.Invalidate();
                }
            }
        }

        public override IEnumerable<string> Names
        {
            get { return new string[] { Name }; }
        }

        public TranslationFilter(IFilterOwner owner, string name, string expr, bool enabled, string text)
            : base(owner, name, expr, text)
        {
            Operator = FilterTypes.Like;
            Enabled = enabled;
        }

        public TranslationFilter(IFilterOwner owner, string name, string expr, bool enabled)
            : this(owner, name, expr, enabled, name) { }

        public override IEnumerable<IFilter> Filters
        {
            get { return new IFilter[0]; }
        }

        public override string ToString()
        {
            if (Enabled && Value != null)
            {
                if (Value == DBNull.Value)
                    return Expr + " IS NULL";
                else
                    return string.Format(_translationCommandText, Name, Expr);
            }

            return string.Empty;
        }
    }

    public class SubqueryFilter : Filter
    {
        private ConjunctionFilter _subqueryFilters;
        private ConjunctionFilter SubqueryFilters
        {
            get
            {
                //query.CheckInitialize();
                if (_subqueryFilters == null)
                    _subqueryFilters = new ConjunctionFilter(_owner);

                return _subqueryFilters;
            }
        }

        public override FilterTypes[] Operators
        {
            get { return new FilterTypes[0]; }
        }

        public override FilterTypes Operator
        {
            get { return FilterTypes.Equal; }
            set { }
        }

        public override object Value
        {
            get { return null; }
            set { }
        }

        private bool _enabled;
        public override bool Enabled
        {
            get { return _enabled; }
            set
            {
                if (_enabled != value)
                {
                    _enabled = value;
                    _owner.Invalidate();
                }
            }
        }

        public override IEnumerable<string> Names
        {
            get { return new string[0]; }
        }

        private string _cond;
        private string _grouping;

        public SubqueryFilter(IFilterOwner owner, string name, string cond, string sql, string grouping, bool enabled, string text)
            : base(owner, name, sql, text)
        {
            _cond = cond.Trim().ToUpper();
            _grouping = grouping.Trim().ToUpper();
            _enabled = enabled;
            if (_enabled)
                owner.Invalidate();
        }

        public SubqueryFilter(IFilterOwner owner, string name, string cond, string sql, string grouping, bool enabled)
            : this(owner, name, cond, sql, grouping, enabled, name) { }

        public SubqueryFilter(IFilterOwner owner, string name, string cond, string sql, string grouping, string text)
            : this(owner, name, cond, sql, grouping, false, text) { }

        public SubqueryFilter(IFilterOwner owner, string name, string cond, string sql, string grouping)
            : this(owner, name, cond, sql, grouping, name) { }

        public override IEnumerable<IFilter> Filters
        {
            get { return SubqueryFilters; }
        }

        public SubqueryFilter Add(IFilter filter)
        {
            SubqueryFilters.Add(filter);
            return this;
        }

        public SubqueryFilter Add(string name, string expr, Type type, FilterTypes filter)
        {
            return Add(new OperFilter(_owner, name, expr, type, filter));
        }

        public SubqueryFilter Add(string name, string expr, Type type, FilterTypes filter, string text)
        {
            return Add(new OperFilter(_owner, name, expr, type, filter, text));
        }

        public SubqueryFilter Add(string name, string expr, Type type)
        {
            return Add(new OperFilter(_owner, name, expr, type));
        }

        public SubqueryFilter Add(string name, string expr, Type type, string text)
        {
            return Add(new OperFilter(_owner, name, expr, type, text));
        }

        public SubqueryFilter Add(string name, string expr, Type type, FilterTypes filter, params object[] values)
        {
            return Add(new OperFilter(_owner, name, expr, type, filter, values));
        }

        public SubqueryFilter Add(string name, string expr, Type type, FilterTypes filter, string text, params object[] values)
        {
            return Add(new OperFilter(_owner, name, expr, type, filter, values, text));
        }

        public SubqueryFilter Add(string name, string expr, Type type, params object[] values)
        {
            return Add(new OperFilter(_owner, name, expr, type, values));
        }

        public SubqueryFilter Add(string name, string expr, Type type, string text, params object[] values)
        {
            return Add(new OperFilter(_owner, name, expr, type, values, text));
        }

        public SubqueryFilter AddTranslation(string name, string expr, bool enabled)
        {
            SubqueryFilters.Add(new TranslationFilter(_owner, name, expr, enabled));
            return this;
        }

        public SubqueryFilter AddTranslation(string name, string expr)
        {
            return AddTranslation(name, expr, true);
        }

        protected override IEnumerator<Filter> Enumerator
        {
            get
            {
                var list = new List<Filter>();
                list.Add(this);
                foreach (IEnumerable<Filter> filter in SubqueryFilters)
                    list.AddRange(filter);

                return list.GetEnumerator();
            }
        }

        private const string whereClause = "WHERE";
        private const string andClause = "AND";

        public override string ToString()
        {
            if (Enabled)
            {
                var sql = new StringBuilder();

                sql.Append("(");
                sql.Append(Expr);

                var filter = Filters.ToString();
                if (filter != string.Empty)
                {
                    sql.Append(" ");
                    if (SqlUtils.SqlContainsWhere(Expr))
                        sql.Append(andClause);
                    else
                        sql.Append(whereClause);
                    sql.Append(" ");
                    sql.Append(filter);
                }

                if (_grouping != string.Empty)
                {
                    sql.Append(" ");
                    sql.Append(_grouping);
                }

                sql.Append(")");

                if (_cond != string.Empty)
                    return _cond + " " + sql.ToString();

                return sql.ToString();
            }

            return string.Empty;
        }
    }

    public abstract class LogicalFilter : IFilter, IQueryFilter, IEnumerable<Filter>
    {
        private readonly IList<IFilter> _filters = new List<IFilter>();
        protected readonly IFilterOwner _owner;
        private readonly string _logicalOperator;

        public LogicalFilter(IFilterOwner owner, string logicalOperator)
        {
            _logicalOperator = logicalOperator;
            _owner = owner;
        }

        public LogicalFilter Add(IFilter filter)
        {
            _filters.Add(filter);
            return this;
        }

        public LogicalFilter Add(string name, string expr, Type type, FilterTypes filter, string text)
        {
            return Add(new OperFilter(_owner, name, expr, type, filter, text));
        }

        public LogicalFilter Add(string name, string expr, Type type, FilterTypes filter)
        {
            return Add(new OperFilter(_owner, name, expr, type, filter));
        }

        public LogicalFilter Add(string name, string expr, Type type, string text)
        {
            return Add(new OperFilter(_owner, name, expr, type, text));
        }

        public LogicalFilter Add(string name, string expr, Type type)
        {
            return Add(new OperFilter(_owner, name, expr, type));
        }

        public LogicalFilter Add(string name, string expr, Type type, FilterTypes filter, string text, params object[] values)
        {
            return Add(new OperFilter(_owner, name, expr, type, filter, values, text));
        }

        public LogicalFilter Add(string name, string expr, Type type, FilterTypes filter, params object[] values)
        {
            return Add(new OperFilter(_owner, name, expr, type, filter, values));
        }

        public LogicalFilter Add(string name, string expr, Type type, string text, params object[] values)
        {
            return Add(new OperFilter(_owner, name, expr, type, values, text));
        }

        public LogicalFilter Add(string name, string expr, Type type, params object[] values)
        {
            return Add(new OperFilter(_owner, name, expr, type, values));
        }

        public LogicalFilter Add(string name, string expr, string text)
        {
            return Add(new SubqueryFilter(_owner, name, string.Empty, expr, string.Empty, text));
        }

        public LogicalFilter Add(string name, string expr)
        {
            return Add(new SubqueryFilter(_owner, name, string.Empty, expr, string.Empty));
        }

        public LogicalFilter Add(string name, string expr, bool enabled, string text)
        {
            return Add(new SubqueryFilter(_owner, name, string.Empty, expr, string.Empty, enabled, text));
        }

        public LogicalFilter Add(string name, string expr, bool enabled)
        {
            return Add(new SubqueryFilter(_owner, name, string.Empty, expr, string.Empty, enabled));
        }

        public LogicalFilter AddTranslation(string name, string expr, bool enabled, string text)
        {
            _filters.Add(new TranslationFilter(_owner, name, expr, enabled, text));
            return this;
        }

        public LogicalFilter AddTranslation(string name, string expr, bool enabled)
        {
            return AddTranslation(name, expr, enabled, name);
        }

        public LogicalFilter AddTranslation(string name, string expr, string text)
        {
            return AddTranslation(name, expr, true, text);
        }

        public LogicalFilter AddTranslation(string name, string expr)
        {
            return AddTranslation(name, expr, true);
        }

        public SubqueryFilter AddQuery(string name, string cond, string sql, string grouping, bool enabled = false)
        {
            var filter = new SubqueryFilter(_owner, name, cond, sql, grouping, enabled);
            Add(filter);
            return filter;
        }

        public SubqueryFilter AddQuery(string name, string sql, bool enabled = false)
        {
            return AddQuery(name, string.Empty, sql, string.Empty, enabled);
        }

        public SubqueryFilter AddQuery(string name, string cond, string sql, bool enabled = false)
        {
            return AddQuery(name, cond, sql, string.Empty, enabled);
        }

        public SubqueryFilter CreateQuery(string name, string cond, string sql, string grouping, bool enabled = false)
        {
            return new SubqueryFilter(_owner, name, cond, sql, grouping, enabled);
        }

        public SubqueryFilter CreateQuery(string name, string cond, string sql, bool enabled = false)
        {
            return CreateQuery(name, cond, sql, string.Empty, enabled);
        }

        public bool IsManual
        {
            get { return false; }
        }

        private Filter Find(string name)
        {
            return this.FirstOrDefault(x => x.Name.Equals(name.ToUpper()));
        }

        public Filter this[string name]
        {
            get
            {
                var filter = Find(name);
                if (filter == null)
                    throw new ArgumentOutOfRangeException(name, "Invalid query filter");

                return filter;
            }
        }

        IOperFilter IQueryFilter.this[string name]
        {
            get { return this[name]; }
        }

        public bool Contains(string name)
        {
            return Find(name) != null;
        }

        public bool TryGetValue(string name, out IOperFilter filter)
        {
            filter = Find(name);
            return filter != null;
        }

        public bool Remove(Filter filter)
        {
            return _filters.Remove(filter);
        }

        public override string ToString()
        {
            var expr = string.Join(" " + _logicalOperator + " ", _filters.Where(x => !x.IsManual)
                .Select(x => x.ToString()).Where(x => x != string.Empty).ToArray()).Trim();
            if (expr != string.Empty)
                return "(" + expr + ")";

            return expr;
        }

        private IEnumerator<Filter> Enumerator
        {
            get
            {
                var filters = new List<Filter>();
                foreach (IEnumerable<Filter> filter in _filters)
                    filters.AddRange(filter);

                return filters.GetEnumerator();
            }
        }

        #region IEnumerable<Filter> Members

        public IEnumerator<Filter> GetEnumerator()
        {
            return Enumerator;
        }

        #endregion
        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Enumerator;
        }

        #endregion
        #region IFilter Members

        public string Name
        {
            get { return string.Empty; }
        }

        public IEnumerable<IFilter> Filters
        {
            get { return _filters; }
        }

        #endregion
    }

    public class ConjunctionFilter : LogicalFilter
    {
        public ConjunctionFilter(IFilterOwner owner) : base(owner, "AND") { }
    }

    public class DisjunctionFilter : LogicalFilter
    {
        public DisjunctionFilter(IFilterOwner owner) : base(owner, "OR") { }
    }

    public class SortCollection : IEnumerable<KeyValuePair<string, int>>
    {
        private readonly IDictionary<string, int> _items = new Dictionary<string, int>();
        private readonly string[] _columns;

        public SortCollection(string[] columns)
        {
            _columns = columns;
        }

        private bool Contains(string name)
        {
            return _columns.Contains(name, StringComparer.InvariantCultureIgnoreCase);
        }

        private void Add(string name, int dir)
        {
            if (Contains(name))
                _items.Add(name, dir);
            else
                throw new ArgumentOutOfRangeException(name, "Invalid query sort");
        }

        public void AddAsc(string name)
        {
            Add(name.ToUpperInvariant(), 1);
        }

        public void AddAscNullsFirst(string name)
        {
            Add(name.ToUpperInvariant(), 2);
        }

        public void AddDesc(string name)
        {
            Add(name.ToUpperInvariant(), -1);
        }

        public void AddDescNullsFirst(string name)
        {
            Add(name.ToUpperInvariant(), -2);
        }

        public void Clear()
        {
            _items.Clear();
        }

        #region IEnumerable<KeyValuePair<string, int>> Members

        public IEnumerator<KeyValuePair<string, int>> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        #endregion
        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        #endregion

        public override string ToString()
        {
            const string DescClause = "DESC";
            const string AscClause = "ASC";
            const string NullsFirstClause = "NULLS FIRST";

            var sorts = new List<string>();
            foreach (var item in _items)
            {
                var index = Array.IndexOf(_columns, item.Key.ToUpperInvariant()) + 1;
                if (index > 0)
                {
                    var sort = index.ToString() + " " + (item.Value < 0 ? DescClause : AscClause);
                    if (Math.Abs(item.Value) == 2)
                        sort += " " + NullsFirstClause;
                    sorts.Add(sort);
                }
            }

            return string.Join(", ", sorts);
        }
    }

    public delegate void CalcRecordEventHandler(object sender, CalcRecordEventArgs e);

    public class CalcRecordEventArgs : EventArgs
    {
        public readonly IQueryRecord Record;
        public bool Handled = true;

        public CalcRecordEventArgs(IQueryRecord record)
        {
            Record = record;
        }
    }

    /// <summary>
    /// Represents an abstract definition of Query. This class will contains all logic to access the database, and get information from there.
    /// </summary>
    [DebuggerTypeProxy(typeof(QueryDebugView))]
    public abstract class Query : IFilterOwner, IReportQuery
    {
        #region Constants

        protected const string QUERYLOGKEY = "NewHotel.Query";

        #endregion

        internal class QueryDebugView
        {
            private readonly Query _query;

            public QueryDebugView(Query query)
            {
                this._query = query;
            }

            public string Sql
            {
                get { return _query.SqlDebugInfo; }
            }
        }

        public readonly string Name;
        public readonly IDatabaseManager Manager;

        private ConjunctionFilter _filters;
        private SortCollection _sorts;
        private string[] _columns;
        private Parameters _parameters;
        private bool _paramsInvalidated = false;
        private bool _initialized;
        private bool _buildingCommandText;
        private bool _invalidateCommandText;
        private string _commandColumns;
        private string _commandText;
        private string _groupBy;

        public DisjunctionFilter Disjunction()
        {
            return new DisjunctionFilter(this);
        }

        public ConjunctionFilter Conjunction()
        {
            return new ConjunctionFilter(this);
        }

        private ConjunctionFilter GetFilters(bool checkInitialize)
        {
            if (_filters == null)
            {
                if (checkInitialize)
                    CheckInitialize();
                if (_filters == null)
                    _filters = new ConjunctionFilter(this);
            }

            return _filters;
        }

        /// <summary>
        /// List of Filters to be applied. Check remarks query class for specific details
        /// </summary>
        public ConjunctionFilter Filters
        {
            get { return GetFilters(true); }
        }

        public Query(string name, IDatabaseManager manager)
        {
            Name = name;
            Manager = manager ?? NewHotelAppContext.GetManager();

            _initialized = true;
            try
            {
                FilterSetup();
            }
            finally
            {
                _initialized = false;
            }
        }

        public Query(string name)
            : this(name, null)
        {
        }

        protected virtual ListData CreateListData(string listName, ColumnDefinition[] colDefs, IList list)
        {
            return new ListData(listName, colDefs, list);
        }

        /// <summary>
        /// Returns a list of all column names, as described by the query text
        /// </summary>
        public string[] Columns
        {
            get
            {
                if (_columns == null)
                {
                    var cols = new List<string>(GetColumns());
                    cols.AddRange(SqlUtils.ExtractColumnsFromSql(GetCommandTextWarapper(CommandText)));
                    _columns = cols.ToArray();
                }

                return _columns;
            }
        }

        private static IEnumerable<string> GetFilterNames(string name, int count)
        {
            var names = new List<string>();
            for (int i = 0; i < count; i++)
                names.Add(name + i.ToString());

            return names;
        }

        private static bool ContainsFilter(IEnumerable<Filter> filters, string name)
        {
            return filters.Any(x => x.Value != null && x.Names.Contains(name, StringComparer.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// List of Parameters to be applied
        /// </summary>
        public Parameters Parameters
        {
            get
            {
                CheckInitialize();
                if (_parameters == null || _paramsInvalidated)
                {
                    try
                    {
                        var names = new List<string>(SqlUtils.ExtractParametersFromSql(GetQuery()));
                        names.Remove(f => ContainsFilter(Filters, f));

                        if (_parameters != null)
                            _parameters.UpdateParameters(names);
                        else
                            _parameters = new Parameters(names);
                    }
                    finally
                    {
                        _paramsInvalidated = false;
                    }
                }

                return _parameters;
            }
        }

        /// <summary>
        /// List of Sorts. Column names are required here to specify sorts
        /// </summary>
        public SortCollection Sorts
        {
            get
            {
                if (_sorts == null)
                    _sorts = new SortCollection(Columns);

                return _sorts;
            }
        }

        public Query SetParameter(string name, object value)
        {
            Parameters[name] = value;

            return this;
        }

        public Query SetFilter(string name, object value)
        {
            Filters[name].Value = value;

            return this;
        }

        #region IFilterOwner Members

        public void Invalidate()
        {
            _paramsInvalidated = true;
            if (_invalidateCommandText)
                _commandText = null;
        }

        public bool ExcludeFilter(string name)
        {
            return GetManualFilters().Contains(name, StringComparer.InvariantCultureIgnoreCase);
        }

        #endregion

        protected virtual IEnumerable<string> GetColumns()
        {
            return new string[0];
        }

        protected virtual bool CommandTextCached
        {
            get { return true; }
        }

        public virtual string CommandText
        {
            get
            {
                if (_commandText == null)
                {
                    _invalidateCommandText = false;
                    if (!_buildingCommandText)
                    {
                        _buildingCommandText = true;
                        try
                        {
                            // CommandTextCached no utilizada por el momento
                            _commandText = GetCommandText();
                            if (_commandText != null)
                                _commandText = _commandText.Trim().ToUpperInvariant() + " ";
                        }
                        finally
                        {
                            _buildingCommandText = false;
                        }
                    }
                    else
                        throw new InvalidOperationException("Cannot call CommandText from inside CommandText method.");

                    CheckInitialize();
                }

                return _commandText;
            }
        }

        private string CommandColumns
        {
            get
            {
                if (_commandColumns == null)
                {
                    _commandColumns = GetCommandColumns();
                    if (_commandColumns != null)
                        _commandColumns = _commandColumns.Trim().ToUpperInvariant();
                }

                return _commandColumns;
            }
        }

        private string GroupBy
        {
            get
            {
                if (_groupBy == null)
                {
                    _groupBy = GetCommandGroupBy();
                    if (_groupBy != null)
                        _groupBy = _groupBy.Trim().ToUpperInvariant();
                }

                return _groupBy;
            }
        }

        protected string GetCommandFilter()
        {
            if (_buildingCommandText)
                _invalidateCommandText = true;

            var isInitialized = _initialized;
            _initialized = true;
            try
            {
                return Filters.ToString();
            }
            finally
            {
                _initialized = isInitialized;
            }
        }

        protected string GetFilterExpression(string name)
        {
            if (_buildingCommandText)
                _invalidateCommandText = true;

            var isInitialized = _initialized;
            _initialized = true;
            try
            {
                IOperFilter filter;
                if (Filters.TryGetValue(name, out filter))
                    return filter.ToString();
            }
            finally
            {
                _initialized = isInitialized;
            }

            return string.Empty;
        }

        protected virtual IEnumerable<string> GetManualFilters()
        {
            return new string[0];
        }

        protected virtual string GetCommandTextWarapper(string commandText)
        {
            return commandText;
        }

        protected virtual string GetCommandText()
        {
            return null;
        }

        protected virtual string GetCommandGroupBy()
        {
            return null;
        }

        protected virtual string GetCommandColumns()
        {
            return null;
        }

        protected virtual string GetSummaryCommandText(string commandText)
        {
            return null;
        }

        protected virtual void FilterSetup() { }

        internal void CheckInitialize()
        {
            if (!_initialized)
            {
                _initialized = true;
                Initialize();
            }
        }

        protected virtual void Initialize() { }
        protected virtual void BeforeExecute() { }
        protected virtual void AfterExecute() { }

        private static Func<object, object> GetTypeConverter(Type type)
        {
            const char TrueValue = '1';
            const char FalseValue = '0';

            if (type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                type = type.GetGenericArguments()[0];

            if (type == typeof(Guid))
            {
                Func<object, object> guidCast = (value) =>
                {
                    try
                    {
                        if (value.GetType() == typeof(byte[]))
                            return new Guid((byte[])value);
                        else if (value.GetType() != typeof(Guid))
                            throw new InvalidCastException(string.Format("Invalid value type: {0}", value.GetType().Name));

                        return value;
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidCastException(string.Format("Error casting from {0} to {1}", value, type.Name), ex);
                    }
                };

                return guidCast;
            }
            else if (type == typeof(string))
            {
                Func<object, object> strCast = (value) =>
                {
                    try
                    {
                        return (string)value;
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidCastException(string.Format("Error casting from {0} to {1}", value, type.Name), ex);
                    }
                };

                return strCast;
            }
            else if (type == typeof(bool))
            {
                Func<object, object> boolCast = (value) =>
                {
                    try
                    {
                        var boolStr = value.ToString();
                        switch (boolStr[0])
                        {
                            case FalseValue:
                                return false;
                            case TrueValue:
                                return true;
                            default:
                                throw new InvalidCastException(string.Format("Invalid value: {0}", value));
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidCastException(string.Format("Error casting from {0} to {1}", value, type.Name), ex);
                    }
                };

                return boolCast;
            }
            else if (type == typeof(DateTime))
            {
                Func<object, object> dateTimeCast = (value) =>
                {
                    try
                    {
                        return ((DateTime)value).ToUtcDateTime();
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidCastException(string.Format("Error casting from {0} to {1}", value, type.Name), ex);
                    }
                };

                return dateTimeCast;
            }
            else if (type.IsEnum)
            {
                if (type.GetCustomAttributes(typeof(FlagsAttribute), false).Length > 0)
                {
                    var length = Enum.GetValues(type).Length;

                    Func<object, object> enumFlagCast = (value) =>
                    {
                        try
                        {
                            if (value.GetType() != typeof(string))
                                throw new InvalidCastException(string.Format("Invalid value type: {0}", value.GetType().Name));

                            var values = value.ToString().ToCharArray();
                            if (values.Length != length)
                                throw new InvalidCastException("Length mismatch");

                            long bits = 0;
                            for (int i = 0; i < values.Length; i++)
                            {
                                switch (values[i])
                                {
                                    case TrueValue:
                                        bits += 2 << i;
                                        break;
                                    case FalseValue:
                                        break;
                                    default:
                                        throw new InvalidCastException(string.Format("Invalid value: {0}", value));
                                }
                            }

                            return Enum.ToObject(type, bits);
                        }
                        catch (Exception ex)
                        {
                            throw new InvalidCastException(string.Format("Error casting from {0} to {1}", value, type.Name), ex);
                        }
                    };

                    return enumFlagCast;
                }
                else
                {
                    var underlyingType = Enum.GetUnderlyingType(type);

                    Func<object, object> enumCast = (value) =>
                    {
                        try
                        {
                            return Enum.ToObject(type, Convert.ChangeType(value, underlyingType));
                        }
                        catch (Exception ex)
                        {
                            throw new InvalidCastException(string.Format("Error casting from {0} to {1}", value, type.Name), ex);
                        }
                    };

                    return enumCast;
                }
            }
            else if (type == typeof(ARGBColor))
            {
                Func<object, object> argbCast = (value) =>
                {
                    try
                    {
                        var color = (int)Convert.ChangeType(value, typeof(int));
                        return new ARGBColor(color);
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidCastException(string.Format("Error casting from {0} to {1}", value, type.Name), ex);
                    }
                };

                return argbCast;
            }
            else if (type == typeof(Blob))
            {
                Func<object, object> blobCast = (value) =>
                {
                    try
                    {
                        if (value.GetType() == typeof(byte[]))
                            value = new Blob((byte[])value);

                        return value;
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidCastException(string.Format("Error casting from {0} to {1}", value, type.Name), ex);
                    }
                };

                return blobCast;
            }
            else if (type == typeof(Clob))
            {
                Func<object, object> clobCast = (value) =>
                {
                    try
                    {
                        if (value.GetType() == typeof(string))
                            value = new Clob((string)value);

                        return value;
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidCastException(string.Format("Error casting from {0} to {1}", value, type.Name), ex);
                    }
                };

                return clobCast;
            }
            else if (type == typeof(object))
            {
                Func<object, object> objCast = (value) =>
                {
                    try
                    {
                        if (value.GetType() == typeof(byte[]))
                            value = new Guid((byte[])value);

                        return value;
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidCastException(string.Format("Error casting from {0} to {1}", value, type.Name), ex);
                    }
                };

                return objCast;
            }

            Func<object, object> genericCast = (value) =>
            {
                try
                {
                    return Convert.ChangeType(value, type);
                }
                catch (Exception ex)
                {
                    throw new InvalidCastException(string.Format("Error casting from {0} to {1}", value, type.Name), ex);
                }
            };

            return genericCast;
        }

        private Type GetDataType(IDataReader reader, DataTable schema, int index)
        {
            var metadata = schema.Rows[index].ItemArray;

            // Type
            var type = reader.GetFieldType(index);
            // DataTypeName
            var dataTypeName = reader.GetDataTypeName(index).ToUpperInvariant();

            bool isNullable;
            var dataType = Manager.GetTypeFromMetadata(type, dataTypeName, metadata, out isNullable);
            return isNullable && dataType.IsValueType ? typeof(Nullable<>).MakeGenericType(dataType) : dataType;
        }

        private Type GetDataType(IDataReader reader, int index)
        {
            return GetDataType(reader, reader.GetSchemaTable(), index);
        }

        protected event CalcRecordEventHandler OnCalcRecord;

        private ListData GetData(IDataReader reader, string name, object correlationObj)
        {
            var schema = reader.GetSchemaTable();

            var defs = new ColumnDefinition[reader.FieldCount];
            for (int i = 0; i < reader.FieldCount; i++)
            {
                var dataType = GetDataType(reader, schema, i);
                defs[i] = new ColumnDefinition(reader.GetName(i), dataType, i, GetTypeConverter(dataType));
            }

            var list = new List<object[]>(100);
            var record = Manager.GetQueryReader(reader);

            Action<Query, IQueryRecord, object> dlg = (q, qr, o) =>
            {
                var objs = new object[qr.FieldCount];
                for (int i = 0; i < qr.FieldCount; i++)
                {
                    var value = qr.GetValue(i);
                    if (value != DBNull.Value)
                        objs[i] = defs[i].Converter(value);
                }

                list.Add(objs);
            };

            if (OnCalcRecord != null)
            {
                while (record.Read())
                {
                    var args = new CalcRecordEventArgs(record);
                    OnCalcRecord(this, args);
                    if (args.Handled)
                        dlg(this, record, correlationObj);
                }
            }
            else
            {
                while (record.Read())
                    dlg(this, record, correlationObj);
            }

            list.TrimExcess();
            return CreateListData(name.ToUpper(), defs, list);
        }

        private void LogDuration(string operation, DateTime start)
        {
            Log.Source[QUERYLOGKEY].TraceInfo("{0} {1}: {2}", operation, Name, (DateTime.Now - start).Duration());
        }

        private void CreateParameter(IDbCommand command, string name, object value)
        {
            var parameter = Manager.CreateParameter(command);
            parameter.ParameterName = name;
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = Manager.ConvertValueType(value);
        }

        internal static object GetArgumentValue(object value)
        {
            var args = value as IArgument[];
            if (args != null)
            {
                if (args.Length > 0)
                {
                    if (args.Length == 1)
                        value = args[0].Value;
                    else
                        value = args.Select(x => x.Value).ToArray();
                }
                else
                    value = null;
            }

            return value;
        }

        private IDictionary<string, object> GetQueryArguments()
        {
            var arguments = new Dictionary<string, object>();

            foreach (var name in Parameters)
            {
                var value = GetArgumentValue(Parameters[name]);
                if (value == null || value == DBNull.Value)
                    throw new ArgumentNullException(name, "Parameter not specified.");

                arguments.Add(name, value);
            }

            foreach (var filter in Filters.Where(x => x.Value != null))
            {
                var value = GetArgumentValue(filter.Value);
                if (value != null && !value.Equals(DBNull.Value))
                {
                    if (value.GetType().IsArray)
                    {
                        var values = ((IEnumerable)value).Cast<object>()
                            .Where(x => x != null).Distinct().ToArray();

                        switch (filter.Operator)
                        {
                            case FilterTypes.Between:
                            case FilterTypes.In:
                            case FilterTypes.NotIn:
                                if (values.Length == 1)
                                {
                                    arguments.Add(filter.Name, values[0]);
                                    continue;
                                }
                                //else if (values.Length > OperFilter.InOperatorLimit &&
                                //    values[0] != null && values[0].GetType() == typeof(Guid))
                                //{
                                //    arguments.Add("NH___" + filter.Name, values);
                                //    continue;
                                //}
                                break;
                        }

                        for (int i = 0; i < values.Length; i++)
                            arguments.Add(filter.Name + i.ToString(), values[i]);
                    }
                    else
                    {
                        switch (filter.Operator)
                        {
                            case FilterTypes.Like:
                                arguments.Add(filter.Name, "%" + value.ToString() + "%");
                                break;
                            default:
                                arguments.Add(filter.Name, value);
                                break;
                        }
                    }
                }
            }

            return arguments;
        }

        private IDataReader Execute(string commandText, string commandCols, int pageNumber, short pageSize, out long recordCount)
        {
            recordCount = 0;
            var paging = pageNumber > 0 && pageSize > 0;

            using (var command = Manager.CreateCommand("NewHotel.Query." + Name, pageNumber, pageSize))
            {
                foreach (var arg in GetQueryArguments())
                    CreateParameter(command, arg.Key, arg.Value);

                var summaryCommandText = GetSummaryCommandText(commandText);

                if (paging)
                {
                    var sqlText = Manager.GetPagedCommandText(commandText, commandCols, pageNumber, pageSize);
                    if (!string.IsNullOrEmpty(summaryCommandText))
                        sqlText = Manager.GetUnionCommandText(sqlText, summaryCommandText);

                    command.CommandText = sqlText;
                }
                else
                {
                    var sqlText = Manager.GetCommandText(commandText, commandCols);

                    if (!string.IsNullOrEmpty(summaryCommandText))
                        sqlText = Manager.GetUnionCommandText(sqlText, summaryCommandText);

                    command.CommandText = GetCommandTextWarapper(sqlText);
                }


                return Manager.ExecuteReader(command);
            }
        }

        private IDataReader Execute(string commandText)
        {
            long recordCount;
            return Execute(commandText, null, 0, 0, out recordCount);
        }

        /// <summary>
        /// Executes the current query, and returns a provided page, with a defined page size
        /// </summary>
        /// <param name="pageNumber">page to be returned</param>
        /// <param name="pageSize">number of rows per page</param>
        /// <returns>IDataReader with page rows</returns>
        public IDataReader Execute(int pageNumber, short pageSize)
        {
            long recordCount;
            return Execute(GetQuery(), null, pageNumber, pageSize, out recordCount);
        }

        /// <summary>
        /// Executes the current query, and returns a provided page, with a defined page size
        /// </summary>
        /// <param name="pageNumber">page to be returned</param>
        /// <param name="pageSize">number of rows per page</param>
        /// <param name="recordCount">total of records founded</param>
        /// <returns>ListData with records</returns>
        public ListData Execute(int pageNumber, short pageSize, out long recordCount)
        {
            Manager.Open();
            try
            {
                BeforeExecute();
                using (var reader = Execute(GetQuery(), CommandColumns, pageNumber, pageSize, out recordCount))
                {
                    var commandStopwatch = Manager.StartCommand("NewHotel.Query." + Name);
                    try
                    {
                        var list = GetData(reader, Name, null);

                        if (recordCount == 0)
                            recordCount = list.Count;

                        return list;
                    }
                    finally
                    {
                        if (!reader.IsClosed)
                            reader.Close();

                        if (commandStopwatch != null)
                        {
                            commandStopwatch.Fetched();
                            Manager.EndCommand(commandStopwatch);
                        }
                        AfterExecute();
                    }
                }
            }
            finally
            {
                Manager.Close();
            }
        }
         
        /// <summary>
        /// Executes current query and load information into a ListData
        /// </summary>
        /// <returns>ListData with results</returns>
        public ListData Execute()
        {
            long recordCount;
            return Execute(0, 0, out recordCount);
        }

        public ListData First()
        {
            long recordCount;
            return Execute(1, 1, out recordCount);
        }

        public long Count()
        {
            Manager.Open();
            try
            {
                BeforeExecute();
                using (var reader = Execute(Manager.GetCountCommandText(GetQuery())))
                {
                    var commandStopwatch = Manager.StartCommand("NewHotel.Query." + Name);
                    try
                    {
                        if (reader.Read())
                            return decimal.ToInt64((decimal)reader[0]);
                        else
                            return -1;
                    }
                    finally
                    {
                        if (!reader.IsClosed)
                            reader.Close();

                        if (commandStopwatch != null)
                        {
                            commandStopwatch.Fetched();
                            Manager.EndCommand(commandStopwatch);
                        }
                        AfterExecute();
                    }
                }
            }
            finally
            {
                Manager.Close();
            }
        }

        public bool Exist()
        {
            Manager.Open();
            try
            {
                BeforeExecute();
                using (var reader = Execute(GetQuery()))
                {
                    var commandStopwatch = Manager.StartCommand("NewHotel.Query." + Name);
                    try
                    {
                        return reader.Read();
                    }
                    finally
                    {
                        if (!reader.IsClosed)
                            reader.Close();

                        if (commandStopwatch != null)
                        {
                            commandStopwatch.Fetched();
                            Manager.EndCommand(commandStopwatch);
                        }
                        AfterExecute();
                    }
                }
            }
            finally
            {
                Manager.Close();
            }
        }

        private static T CreateObject<T>(IDatabaseManager manager, QueryReader qr)
            where T : BasePersistent
        {
            var item = (T)Activator.CreateInstance(typeof(T), manager);
            item.FillObject(qr);
            return item;
        }

        public T ExecuteScalar<T>()
        {
            Manager.Open();
            try
            {
                BeforeExecute();
                using (var reader = Execute(GetQuery()))
                {
                    var commandStopwatch = Manager.StartCommand("NewHotel.Query." + Name);
                    try
                    {
                        object value = DBNull.Value;
                        var converter = GetTypeConverter(GetDataType(reader, 0));
                        if (reader.Read())
                            value = reader[0];

                        value = value == DBNull.Value ? null : converter(value);
                        if (value != null)
                        {
                            try
                            {
                                var t = typeof(T);
                                if (!t.IsValueType && Nullable.GetUnderlyingType(t) != null)
                                    value = Convert.ChangeType(value, Nullable.GetUnderlyingType(t));
                                else
                                    value = Convert.ChangeType(value, t);
                            }
                            catch (InvalidCastException) { }
                        }

                        return value == null ? default(T) : (T)value;
                    }
                    finally
                    {
                        if (!reader.IsClosed)
                            reader.Close();

                        if (commandStopwatch != null)
                        {
                            commandStopwatch.Fetched();
                            Manager.EndCommand(commandStopwatch);
                        }
                        AfterExecute();
                    }
                }
            }
            finally
            {
                Manager.Close();
            }
        }

        public IList<T> ExecuteScalarList<T>(int pageNumber, short pageSize, out long recordCount)
            where T : struct
        {
            Manager.Open();
            try
            {
                BeforeExecute();
                using (var reader = Execute(GetQuery(), CommandColumns, pageNumber, pageSize, out recordCount))
                {
                    var commandStopwatch = Manager.StartCommand("NewHotel.Query." + Name);
                    try
                    {
                        var list = new List<T>();
                        var converter = GetTypeConverter(GetDataType(reader, 0));
                        while (reader.Read())
                        {
                            object value = reader[0];
                            value = value == DBNull.Value ? null : converter(value);
                            if (value != null)
                            {
                                try
                                {
                                    value = Convert.ChangeType(value, typeof(T));
                                }
                                catch (InvalidCastException)
                                {
                                }
                            }

                            list.Add(value == null ? default(T) : (T)value);
                        }

                        if (recordCount == 0)
                            recordCount = list.Count;

                        return list;
                    }
                    finally
                    {
                        if (!reader.IsClosed)
                            reader.Close();

                        if (commandStopwatch != null)
                        {
                            commandStopwatch.Fetched();
                            Manager.EndCommand(commandStopwatch);
                        }
                        AfterExecute();
                    }
                }
            }
            finally
            {
                Manager.Close();
            }
        }

        public IList<T> ExecuteScalarList<T>()
            where T : struct
        {
            long recordCount;
            return ExecuteScalarList<T>(0, 0, out recordCount);
        }

        /// <summary>
        /// Executes a query and return a list with a predefined Record type
        /// </summary>
        /// <typeparam name="T">The record to be filled</typeparam>
        /// <param name="pageNumber">starting page number</param>
        /// <param name="pageSize">number of rows per page</param>
        /// <param name="recordCount">total of record affected by the query</param>
        /// <returns>List with all records already filled as the result of the query</returns>
        public IList<T> ExecuteList<T>(int pageNumber, short pageSize, out long recordCount)
            where T : class, new()
        {
            Manager.Open();
            try
            {
                BeforeExecute();
                using (var reader = Execute(GetQuery(), CommandColumns, pageNumber, pageSize, out recordCount))
                {
                    var commandStopwatch = Manager.StartCommand("NewHotel.Query." + Name);
                    try
                    {
                        var list = new List<T>();
                        var record = Manager.GetQueryReader(reader);

                        Action<Query, IQueryRecord, object> dlgFill = (q, qr, o) =>
                        {
                            var item = new T();
                            item.FillObject(qr);
                            list.Add(item);
                        };

                        Action<Query, IQueryRecord, object> dlgCalc = (q, qr, o) =>
                        {
                            var args = new CalcRecordEventArgs(qr);
                            OnCalcRecord(this, args);

                            if (args.Handled)
                                dlgFill(this, qr, o);
                        };

                        var dlg = OnCalcRecord != null ? dlgCalc : dlgFill;

                        while (record.Read())
                            dlg(this, record, null);

                        if (recordCount == 0)
                            recordCount = list.Count;

                        return list;
                    }
                    finally
                    {
                        if (!reader.IsClosed)
                            reader.Close();

                        if (commandStopwatch != null)
                        {
                            commandStopwatch.Fetched();
                            Manager.EndCommand(commandStopwatch);
                        }
                        AfterExecute();
                    }
                }
            }
            finally
            {
                Manager.Close();
            }
        }

        /// <summary>
        /// Executes a query and return a dictionary with a predefined record type
        /// </summary>
        /// <typeparam name="TKey">The key</typeparam>
        /// <typeparam name="TValue">The record to be filled</typeparam>
        /// <param name="predicate">predicate key definition</param>
        /// <returns>Dictionary with all records already filled as the result of the query</returns>
        public IDictionary<TKey, IEnumerable<TValue>> ExecuteDictionary<TKey, TValue>(Func<TValue, TKey> predicate)
            where TKey : IEquatable<TKey>
            where TValue : class, new()
        {
            Manager.Open();
            try
            {
                BeforeExecute();
                long recordCount;
                using (var reader = Execute(GetQuery(), CommandColumns, 0, 0, out recordCount))
                {
                    var commandStopwatch = Manager.StartCommand("NewHotel.Query." + Name);
                    try
                    {
                        var dict = new Dictionary<TKey, IEnumerable<TValue>>();
                        var list = new List<TValue>();
                        var record = Manager.GetQueryReader(reader);
                        var lastKey = default(TKey);

                        Action<Query, IQueryRecord, object> dlgFill = (q, qr, o) =>
                        {
                            var item = new TValue();
                            item.FillObject(record);

                            var key = predicate(item);
                            if (!lastKey.Equals(key))
                            {
                                if (list.Count > 0)
                                {
                                    dict.Add(lastKey, list);
                                    list = new List<TValue>();
                                }

                                lastKey = key;
                            }

                            list.Add(item);
                        };

                        Action<Query, IQueryRecord, object> dlgCalc = (q, qr, o) =>
                        {
                            var args = new CalcRecordEventArgs(qr);
                            OnCalcRecord(this, args);

                            if (args.Handled)
                                dlgFill(this, qr, o);
                        };

                        var dlg = OnCalcRecord != null ? dlgCalc : dlgFill;

                        while (record.Read())
                            dlg(this, record, null);

                        if (list.Count > 0)
                            dict.Add(lastKey, list);

                        return dict;
                    }
                    finally
                    {
                        if (!reader.IsClosed)
                            reader.Close();

                        if (commandStopwatch != null)
                        {
                            commandStopwatch.Fetched();
                            Manager.EndCommand(commandStopwatch);
                        }
                        AfterExecute();
                    }
                }
            }
            finally
            {
                Manager.Close();
            }
        }

        /// <summary>
        /// Executes a query and return a list with a predefined record type
        /// </summary>
        /// <typeparam name="T">The record to be filled</typeparam>
        /// <returns>List with all records already filled as the result of the query</returns>
        public IList<T> ExecuteList<T>()
            where T : class, new()
        {
            long recordCount;
            return ExecuteList<T>(0, 0, out recordCount);
        }

        public IPersistentList<T> ExecutePersistentList<T>(int pageNumber, short pageSize, out long recordCount)
            where T : BasePersistent
        {
            Manager.Open();
            try
            {
                BeforeExecute();
                var name = "NewHotel.Query." + Name;
                using (var reader = Execute(GetQuery(), CommandColumns, pageNumber, pageSize, out recordCount))
                {
                    var commandStopwatch = Manager.StartCommand(name);
                    try
                    {
                        var list = new PersistentList<T>(Manager);
                        var record = Manager.GetQueryReader(reader);

                        Action<Query, IQueryRecord, object> dlgFill = (q, qr, o) =>
                        {
                            var item = (T)Activator.CreateInstance(typeof(T), Manager);
                            item.FillObject(qr);
                            list.Add(item);
                        };

                        Action<Query, IQueryRecord, object> dlgCalc = (q, qr, o) =>
                        {
                            var args = new CalcRecordEventArgs(qr);
                            OnCalcRecord(this, args);

                            if (args.Handled)
                                dlgFill(this, qr, o);
                        };

                        var dlg = OnCalcRecord != null ? dlgCalc : dlgFill;

                        while (record.Read())
                            dlg(this, record, null);

                        if (recordCount == 0)
                            recordCount = list.Count;

                        return list;
                    }
                    finally
                    {
                        if (!reader.IsClosed)
                            reader.Close();

                        if (commandStopwatch != null)
                        {
                            commandStopwatch.Fetched();
                            Manager.EndCommand(commandStopwatch);
                        }
                        AfterExecute();
                    }
                }
            }
            finally
            {
                Manager.Close();
            }
        }

        public IPersistentList<T> ExecutePersistentList<T>()
            where T : BasePersistent
        {
            long recordCount;
            return ExecutePersistentList<T>(0, 0, out recordCount);
        }

        private const string whereClause = "WHERE {0}";
        private const string andClause = "AND {0}";
        private const string groupByClause = "GROUP BY {0}";
        private const string orderByClause = "ORDER BY {0}";
        private const string descClause = "DESC";
        private const string ascClause = "ASC";
        private const string nullClause = "NULL";

        private string GetQuery()
        {
            var sb = new StringBuilder();

            var sql = CommandText;
            if (!sql.EndsWith(Environment.NewLine))
                sb.AppendLine(sql);
            else
                sb.Append(sql);

            if (!_invalidateCommandText)
            {
                var where = Filters.ToString();
                if (!string.IsNullOrEmpty(where))
                {
                    if (SqlUtils.SqlContainsWhere(sql))
                        sb.AppendFormat(andClause, where);
                    else
                        sb.AppendFormat(whereClause, where);
                    sb.AppendLine();
                    sql = sb.ToString();
                }
            }

            var groupBy = GroupBy;
            if (!string.IsNullOrEmpty(groupBy))
            {
                sb.AppendFormat(groupByClause, groupBy);
                sb.AppendLine();
            }

            var orderBy = Sorts.ToString();
            if (!string.IsNullOrEmpty(orderBy))
            {
                sb.AppendFormat(orderByClause, orderBy);
                sb.AppendLine();
            }

            return sb.ToString();
        }

        internal string SqlDebugInfo
        {
            get
            {
                const string paramFormat = ":{0} = {1}";
                IList<string> parameters = new List<string>();

                foreach (var name in Parameters)
                {
                    object value = GetArgumentValue(Parameters[name]);
                    parameters.Add(string.Format(paramFormat, name, value == null ? "NULL" : value));
                }

                foreach (var filter in Filters)
                {
                    object value = GetArgumentValue(filter.Value);
                    if (value != null && !value.Equals(DBNull.Value))
                    {
                        if (value.GetType().IsArray)
                        {
                            object[] values = ((IEnumerable)value).Cast<object>()
                                .Where(x => x != null).Distinct().ToArray();

                            if (values.Length == 1)
                                value = values[0];
                            else
                            {
                                for (int i = 0; i < values.Length; i++)
                                    parameters.Add(string.Format(paramFormat, filter.Name + i.ToString(), values[i]));
                                continue;
                            }
                        }

                        parameters.Add(string.Format(paramFormat, filter.Name, value));
                    }
                }

                string sql = GetQuery();
                if (parameters.Count > 0)
                    sql += ";\r\n" + string.Join(", ", parameters.ToArray());

                return sql;
            }
        }

        #region IReportQuery Members

        /// <summary>
        /// Gets the SQL Text of the Current query
        /// </summary>
        public string Text
        {
            get
            {
                var sql = GetQuery();
                var commandCols = GetCommandColumns();
                if (!string.IsNullOrEmpty(commandCols))
                    sql = Manager.GetCommandText(sql, commandCols);

                foreach (var arg in GetQueryArguments().OrderByDescending(a => a.Key.Length))
                {
                    var expr = Manager.GetSqlExpression(arg.Value, arg.Value.GetType());
                    if (string.IsNullOrEmpty(expr))
                        throw new ApplicationException(string.Format("Query argument type {0} not supported.", arg.Value.GetType().Name));
                    sql = sql.Replace(":" + arg.Key.ToUpperInvariant(), expr);
                }

                return GetCommandTextWarapper(sql);
            }
        }

        #endregion

        private static int PosInStr(string subStr, string str)
        {
            for (int i = 0; i < subStr.Length; i++)
            {
                if (subStr.Substring(i, 1) == str)
                    return i + 1;
            }

            return -1;
        }

        protected static IEnumerable<string> GetSorts(string priority, params string[] cols)
        {
            var sorts = new List<string>();
            for (int i = 1; i <= 5; i++)
            {
                var index = PosInStr(priority, i.ToString());
                if (index > 0 && index <= cols.Length)
                    sorts.Add(cols[index - 1]);
            }

            return sorts;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}