﻿using System;

namespace NewHotel.Core
{
    public abstract class BasePersistentBasicOperations<T> : BasePersistent<T>, IDataBasicOperations
    {
        #region Constructors

        public BasePersistentBasicOperations(IDatabaseManager manager)
            : base(manager) { }

        protected BasePersistentBasicOperations(IDatabaseManager manager, IIdGenerator idGenerator)
            : base(manager, idGenerator) { }

        #endregion
        #region IDataBasicOperations operations

        public virtual bool SaveObject()
        {
            return PersistObjectToDB();
        }

        public virtual bool LoadObject(object id, bool throwNoDataFoundException)
        {
            return LoadObjectFromDB(id, throwNoDataFoundException);
        }

        #endregion
    }

    public abstract class BasePersistentBasicOperations : BasePersistentBasicOperations<Guid>
    {
        #region Constructors

        public BasePersistentBasicOperations(IDatabaseManager manager)
            : base(manager, GuidIdGenerator) { }

        #endregion
    }
}
