﻿using System.Data;
using NewHotel.Core;

namespace NewHotel.Data.Commands
{
    public abstract class DbCommand
    {
        public IDatabaseManager Manager { get; }

        public DbCommand(IDatabaseManager manager)
        {
            Manager = manager;
        }

        protected abstract IDbCommand Create(params object[] args);

        public int ExecuteNonQuery(params object[] args)
        {
            using (var command = Create(args))
            {
                return command.ExecuteNonQuery();
            }
        }

        public object ExecuteScalar(params object[] args)
        {
            using (var command = Create(args))
            {
                return command.ExecuteScalar();
            }
        }
    }
}