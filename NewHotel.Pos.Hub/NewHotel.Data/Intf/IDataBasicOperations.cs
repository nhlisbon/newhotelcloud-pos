﻿using System;

namespace NewHotel.Core
{
    public interface IDataBasicOperations : IDataLoad, IDataSave
    {
    }
}
