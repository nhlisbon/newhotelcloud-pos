﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Core
{
    public interface IDataDelete
    {
        bool DeleteObject();
    }
}
