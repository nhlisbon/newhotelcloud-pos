﻿using System.Collections.Generic;

namespace NewHotel.Core
{
    public interface IFilter
    {
        string Name { get; }
        bool IsManual { get; }
        IEnumerable<IFilter> Filters { get; }
    }
}
