﻿using System;

namespace NewHotel.Core
{
    public interface IQueryFilter
    {
        IOperFilter this[string name] { get; }
    }
}
