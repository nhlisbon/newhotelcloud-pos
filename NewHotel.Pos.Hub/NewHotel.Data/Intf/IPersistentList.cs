﻿using System.Collections.Generic;

namespace NewHotel.Core
{
    public interface IPersistentList<T> : IList<T>
    {
        bool Populated { get; }
        bool IsDirty { get; }
        IEnumerable<T> Dirty { get; }
        IEnumerable<T> Removed { get; }
        void AddRange(IEnumerable<T> items);
        void PersistObjects();
        void DeleteObjects();
        void DeleteAllObjects();
        void PersistOrDeleteObjects();
        IPersistentList<T> Execute(params object[] values);
        T[] ExecuteReadOnly(params object[] values);
        IPersistentList<T> ExecuteTop(int count, params object[] values);
        T ExecuteFirst(params object[] values);
    }
}
