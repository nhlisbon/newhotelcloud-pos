﻿using System;

namespace NewHotel.Core
{
    public interface IOperFilter
    {
        bool Enabled { get; set; }
        object Value { get; set; }
        FilterTypes Operator { get; set; }
    }
}
