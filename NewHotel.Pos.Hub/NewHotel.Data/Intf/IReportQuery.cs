﻿using System;

namespace NewHotel.Core
{
    public interface IReportQuery
    {
        string Text { get; }
    }
}
