﻿using System;

namespace NewHotel.Core
{
    public interface IQueryCommand
    {
        string Sql { get; }
        ConjunctionFilter Filters { get; }
        Parameters Parameters { get; }
        SortCollection Sorts { get; }
    }
}
