﻿using System;

namespace NewHotel.Core
{
    public interface IQueryParam
    {
        object this[string name] { get; set; }
    }
}
