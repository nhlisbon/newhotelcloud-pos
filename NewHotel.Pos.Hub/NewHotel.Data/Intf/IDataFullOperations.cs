﻿using System;

namespace NewHotel.Core
{
    public interface IDataFullOperations : IDataLoad, IDataSave, IDataDelete
    {
    }
}
