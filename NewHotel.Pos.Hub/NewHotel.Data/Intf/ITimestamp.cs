﻿using System;

namespace NewHotel.Core
{
    public interface ITimestamp
    {
        void OnLoad(DateTime timestamp);
        void OnPersist(DateTime timestamp);
    }
}
