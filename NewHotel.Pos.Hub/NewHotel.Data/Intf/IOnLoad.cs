﻿using System;

namespace NewHotel.Core
{
    public interface IOnLoad
    {
        bool BeforeLoad();
        void AfterLoad();
    }
}
