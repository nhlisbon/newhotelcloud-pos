﻿using System;

namespace NewHotel.Core
{
    public interface IFilterOwner
    {
        void Invalidate();
        bool ExcludeFilter(string name);
    }
}
