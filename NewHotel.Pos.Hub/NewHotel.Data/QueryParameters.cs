﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace NewHotel.Core
{
    public class Parameters : IQueryParam, IEnumerable<string>
    {
        #region Constants

        private const string LanguageParamName = "LANG_PK";
        private const string TrueParamName = "TRUE";
        private const string FalseParamName = "FALSE";
        private const string TimeFormatParamName = "TIME_FORMAT";
        private const string DateFormatParamName = "DATE_FORMAT";
        private const string DateTimeFormatParamName = "DATETIME_FORMAT";
        private const string MaxDecPrecParamName = "MAX_DECPREC";

        #endregion
        #region Members

        private static readonly IDictionary<string, Func<object>> _defaults =
            new Dictionary<string, Func<object>>(StringComparer.InvariantCultureIgnoreCase);

        private IDictionary<string, object> _parameters =
            new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);

        #endregion
        #region Constructor

        static Parameters()
        {
            _defaults.Add(TrueParamName, () => true);
            _defaults.Add(FalseParamName, () => false);
            _defaults.Add(TimeFormatParamName, () => "HH24:MI");
            _defaults.Add(DateFormatParamName, () => "dd/MM/yyyy");
            _defaults.Add(DateTimeFormatParamName, () => "dd/MM/yyyy HH24:MI");
            _defaults.Add(MaxDecPrecParamName, () => 18);
        }

        public Parameters(IEnumerable<string> names)
        {
            Initialize(_parameters, names);
        }

        #endregion
        #region Private Methods

        private static void Initialize(IDictionary<string, object> parameters, IEnumerable<string> names)
        {
            if (names.Any())
            {
                var language = NewHotelAppContext.GetLanguage();
                foreach (var name in names)
                {
                    object value = null;
                    if (name.EndsWith(LanguageParamName, StringComparison.InvariantCultureIgnoreCase))
                        value = language;
                    else
                    {
                        Func<object> defaultValue;
                        if (_defaults.TryGetValue(name, out defaultValue))
                            value = defaultValue();
                    }

                    parameters.Add(name, value);
                }
            }
        }

        #endregion
        #region Public Properties

        public static void AddParamDefault(string name, Func<object> value)
        {
            _defaults.Add(name, value);
        }

        public object this[string name]
        {
            get
            {
                object value;
                if (!_parameters.TryGetValue(name, out value))
                    throw new ArgumentOutOfRangeException(name, "Invalid query parameter");

                return value;
            }
            set
            {
                if (!Contains(name))
                    throw new ArgumentOutOfRangeException(name, "Invalid query parameter");

                _parameters[name] = value;
            }
        }

        public bool Contains(string name)
        {
            return _parameters.ContainsKey(name);
        }

        public int Count
        {
            get { return _parameters.Count; }
        }

        public void Add(string key, object value)
        {
            _parameters.Add(new KeyValuePair<string, object>(key, value));
        }

        public void UpdateParameters(IEnumerable<string> names)
        {
            var parameters = new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);
            Initialize(parameters, names);

            foreach (var parameterName in _parameters.Keys)
            {
                if (parameters.ContainsKey(parameterName))
                {
                    object value;
                    if (_parameters.TryGetValue(parameterName, out value))
                        parameters[parameterName] = value;
                }
            }

            _parameters = parameters;
        }

        #endregion
        #region IEnumerable<string> Members

        public IEnumerator<string> GetEnumerator()
        {
            return _parameters.Keys.GetEnumerator();
        }

        #endregion
        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _parameters.Keys.GetEnumerator();
        }

        #endregion
    }
}