﻿using System;

namespace NewHotel.Core
{
    public class PersistentClass<T>
        where T : BasePersistent
    {
        private readonly T _value;
        private object _id;

        private T Value
        {
            get
            {
                if (_id != null)
                {
                    var dataLoad = _value as IDataLoad;
                    if (dataLoad != null)
                    {
                        dataLoad.LoadObject(_id);
                        _id = null;
                    }
                }

                return _value;
            }
        }

        public PersistentClass(IDatabaseManager manager, object id)
        {
            _value = (T)Activator.CreateInstance(typeof(T), manager);
            _id = id;
        }

        public PersistentClass(T value)
        {
            _value = value;
            _id = null;
        }

        public static implicit operator T(PersistentClass<T> container)
        {
            return container.Value;
        }

        public static implicit operator PersistentClass<T>(T contained)
        {
            return new PersistentClass<T>(contained);
        }
    }
}
