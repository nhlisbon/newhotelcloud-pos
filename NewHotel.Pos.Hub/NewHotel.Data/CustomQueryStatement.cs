﻿using System;

namespace NewHotel.Core
{
    public class CustomQueryStatement : IQueryStatement
    {
        private const string WhereToken = "WHERE";
        private readonly string Query;
        private string Where;

        public CustomQueryStatement(string query, string where)
        {
            Query = query == null ? string.Empty : query.ToUpper();
            Where = where == null ? string.Empty : where.ToUpper();
        }

        public CustomQueryStatement(string query)
            : this(query, null) { }

        public override string ToString()
        {
            return Query + (string.IsNullOrEmpty(Where) 
                ? string.Empty : WhereToken + " " + Where + "\r\n");
        }

        #region IQueryStatement Members

        public void ClearWhere()
        {
            Where = null;
        }

        public void AppendWhere(string where)
        {
            Where = where;
        }

        public IQueryStatement Clone()
        {
            return new CustomQueryStatement(Query, Where);
        }

        #endregion
    }
}
