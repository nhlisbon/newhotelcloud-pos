﻿using System;
using System.Collections.Generic;
using System.Data;
using NewHotel.DataAnnotations;

namespace NewHotel.Core
{
    public class PersistentMapper<T>
        where T : class
    {
        private static string GetName(string name)
        {
            var len = name.Length - 1;
            if (len >= 0)
            {
                switch (name[len])
                {
                    case '+':
                    case '-':
                        return name.Substring(0, len);
                }
            }

            return name;
        }

        private static string GetAliasName(string name)
        {
            return GetName(name).Replace('.', '$');
        }

        public PersistentMapper(IDataRecord record)
        {
            const char TrueValue = '1';
            const char FalseValue = '0';

            var actions = new Dictionary<int, Func<int, object, object>>();

            var objType = typeof(T);
            foreach (var prop in objType.PersistentProps())
            {
                var accessor = prop.Value;
                try
                {
                    var name = GetAliasName(prop.Key);
                    var type = accessor.BaseType;
                    //if (type.Equals(typeof(object)))
                    //    type = record.GetDataType(name);
                    var isNullable = accessor.Nullable;

                    Func<int, object, object> action = null;

                    var index = record.GetOrdinal(name);
                    if (type.IsEnum)
                    {
                        action = (i, o) =>
                        {
                            try
                            {
                                object enumValue;
                                if (!type.TryGetEnum(o, out enumValue))
                                    throw new ApplicationException(string.Format("Error asigning {0} to {1}. Out of bounds", o, objType.FullName));

                                return enumValue;
                            }
                            catch (Exception e)
                            {
                                throw new ApplicationException(string.Format("Error asigning {0} to {1}", o, objType.FullName), e);
                            }
                        };
                    }
                    else if (type == typeof(bool))
                    {
                        action = (i, o) =>
                        {
                            if (o.GetType() != typeof(bool))
                            {
                                var boolStr = o.ToString();
                                switch (boolStr.Length > 0 ? boolStr[0] : char.MinValue)
                                {
                                    case FalseValue:
                                        return false;
                                    case TrueValue:
                                        return true;
                                    default:
                                        throw new ApplicationException(string.Format("Error asigning {0} to {1}", boolStr, objType.FullName));
                                }
                            }

                            return o;
                        };
                    }
                    else if (type == typeof(Guid))
                    {
                        action = (i, o) =>
                        {
                            if (o.GetType() == typeof(byte[]))
                                return new Guid((byte[])o);
                            else if (o.GetType() != typeof(Guid))
                                throw new ApplicationException(string.Format("Error asigning {0} to {1}", o, objType.FullName));

                            return null;
                        };
                    }
                    else if (type == typeof(DateTime))
                    {
                        action = (i, o) =>
                        {
                            return ((DateTime)o).ToUtcDateTime();
                        };
                    }
                    else if (type == typeof(ARGBColor))
                    {
                        action = (i, o) =>
                        {
                            var color = (int)Convert.ChangeType(o, typeof(int));
                            return new ARGBColor(color);
                        };
                    }
                    else if (type == typeof(Blob))
                    {
                        action = (i, o) =>
                        {
                            if (o.GetType() == typeof(byte[]))
                                return new Blob((byte[])o);

                            return null;
                        };
                    }
                    else if (type == typeof(Clob))
                    {
                        action = (i, o) =>
                        {
                            if (o.GetType() == typeof(string))
                                return new Clob((string)o);

                            return null;
                        };
                    }
                    else if (type == typeof(object))
                    {
                        action = (i, o) =>
                        {
                            return o;
                        };
                    }
                    else
                    {
                        action = (i, o) =>
                        {
                            try
                            {
                                return type.ChangeType(o);
                            }
                            catch (Exception e)
                            {
                                throw new ApplicationException(string.Format("Error asigning {0} to {1}", o, objType.Name), e);
                            }
                        };
                    }

                    if (action != null)
                        actions.Add(index, action);
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }
    }
}