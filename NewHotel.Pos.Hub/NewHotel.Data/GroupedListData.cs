﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Core
{
    [KnownType(typeof(Record))]
    public abstract class GroupedListData<T> : ListData
    {
        #region Private Clases

        private class ArrayComparer : IComparer<object[]>
        {
            #region IComparer<object[]> Members

            public int Compare(object[] x, object[] y)
            {
                for (int i = 0; i < x.Length; i++)
                {
                    var value1 = x[i];

                    var comparable = value1 as IComparable;
                    if (comparable != null)
                    {
                        var value2 = y[i];

                        var comparison = comparable.CompareTo(value2);
                        if (comparison < 0 || comparison > 0)
                            return comparison;
                    }
                }

                return 0;
            }

            #endregion
        }

        #endregion
        #region Constructor

        public GroupedListData(string listName, ListData listData, T args) 
            : base(listName, listData) 
        {
            Initialize(args);
            var dict = new Dictionary<ArrayKey, IRecord>();

            int index = -1;
            if (!string.IsNullOrEmpty(AcumulatedFieldName))
                index = listData.IndexOf(AcumulatedFieldName);

            var records = GetRecords(listData);
            foreach (var record in records)
            {
                var key = GetKey(record, args);

				if (key != null && key.Length > 0)
                {
                    IList<object> ids = null;
                    IRecord rec;
                    object id;

                    if (!TryGetRecord(dict, ref key, out rec))
                    {
                        rec = Append();
                        if (index >= 0)
                        {
                            ids = new List<object>();
                            rec[index] = ids;
                        }

                        dict.Add(new ArrayKey(key), rec);
                        for (int i = 0; i < rec.Count; i++)
                        {
                            if (index != i)
                                rec[i] = record[i];
                        }

                        id = InitAcumulator(key, record, rec, args);
                    }
                    else
                    {
                        if (index >= 0)
                            ids = (IList<object>)rec[index];
                        id = Acumulate(key, record, rec, args);
                    }

                    if (ids != null)
                        ids.Add(id);
                }
                else
                    AddRecord(record);
            }
        }

        #endregion
        #region Protected Methods

        protected virtual void Initialize(T args)
        {
        }

        protected virtual IEnumerable<IRecord> GetRecords(IEnumerable<IRecord> records)
        {
            return records;
        }

        protected virtual bool TryGetRecord(IDictionary<ArrayKey, IRecord> dict, ref IComparable[] key, out IRecord record)
        {
            return dict.TryGetValue(new ArrayKey(key), out record);
        }

        protected virtual void AddRecord(IRecord record)
        {
            Add(record);
        }

        #endregion
        #region Abstract Methods

        protected abstract IComparable[] GetKey(IRecord record, T args);
        protected abstract string AcumulatedFieldName { get; }
        protected abstract object InitAcumulator(IComparable[] key, IRecord record, IRecord acumulator, T args);
        protected abstract object Acumulate(IComparable[] key, IRecord record, IRecord acumulator, T args);

        #endregion
    }
}
