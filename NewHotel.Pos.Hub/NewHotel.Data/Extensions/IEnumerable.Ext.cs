﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.ComponentModel;

namespace NewHotel.Core
{
    public static class IEnumerableTypedListExt
    {
        public static DataTable ToDataTable<T>(this T list, string tableName, bool fillTable)
            where T : IEnumerable, ITypedList
        {
            DataTable table = new DataTable(tableName);
            PropertyDescriptorCollection descs = list.GetItemProperties(null);

            foreach (PropertyDescriptor desc in descs)
            {
                DataColumn col = table.Columns.Add(desc.Name);

                col.AllowDBNull = desc.PropertyType.IsClass || desc.PropertyType.IsInterface;
                bool genericNullable = false;

                if (!col.AllowDBNull)
                {
                    genericNullable = desc.PropertyType.IsGenericType &&
                        desc.PropertyType.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
                    col.AllowDBNull = genericNullable;
                }

                Type type = genericNullable
                    ? desc.PropertyType.GetGenericArguments()[0]
                    : desc.PropertyType;

                if (type.IsEnum)
                    col.DataType = typeof(long);
                else if (type.IsValueType)
                {
                    if (type.Equals(typeof(Guid)))
                        col.DataType = typeof(string);
                    else
                        col.DataType = type;
                }
                else
                    col.DataType = typeof(string);
            }

            if (fillTable)
            {
                DataColumnCollection cols = table.Columns;
                foreach (var item in list)
                {
                    object[] objs = new object[cols.Count];
                    int i = 0;
                    foreach (PropertyDescriptor desc in descs)
                    {
                        DataColumn col = table.Columns[i];
                        object value = desc.GetValue(item);
                        if (value != null)
                        {
                            if (col.DataType.Equals(typeof(string)))
                                value = value.ToString();
                            else if (col.DataType.Equals(typeof(long)))
                                value = (long)value;
                        }

                        objs[i++] = value != null ? Convert.ChangeType(value, col.DataType) : null;
                    }

                    table.Rows.Add(objs);
                }
            }

            return table;
        }

        public static DataTable ToDataTable<T>(this T list, string tableName)
            where T : IEnumerable, ITypedList
        {
            return ToDataTable(list, tableName, false);
        }
    }
}
