﻿using System;
using System.Collections.Generic;
using NewHotel.Contracts;

namespace NewHotel.Core
{
    public static class QueryExt
    {
        public static void SetSorts(this Query query, IList<SortArgument> sorts)
        {
            if (sorts != null && sorts.Count > 0)
            {
                query.Sorts.Clear();
                foreach (var s in sorts)
                {
                    switch (s.Direction)
                    {
                        case SortDirection.Asc:
                            query.Sorts.AddAsc(s.Name);
                            break;
                        case SortDirection.AscNullFirst:
                            query.Sorts.AddAscNullsFirst(s.Name);
                            break;
                        case SortDirection.Desc:
                            query.Sorts.AddDesc(s.Name);
                            break;
                        case SortDirection.DescNullFirst:
                            query.Sorts.AddDescNullsFirst(s.Name);
                            break;
                    }
                }
            }
        }

        public static void SetFilters(this Query query, IEnumerable<FilterArgument> filters)
        {
            if (filters != null)
            {
                foreach (var f in filters)
                {
                    if (string.IsNullOrEmpty(f.Name))
                        throw new ApplicationException(string.Format("Query filter name not specified: {0}", query.Name));

                    if (query.Filters.Contains(f.Name))
                    {
                        var filter = query.Filters[f.Name];
                        filter.Enabled = f.Enabled;

                        if (f.IsAvailable)
                        {
                            if (f.Type.HasValue)
                                filter.Operator = f.Type.Value;

                            filter.Value = f.ArgumentValues;
                        }
                    }
                }
            }
        }

        public static void SetParameters(this Query query, IEnumerable<QueryArgument> parameters)
        {
            if (parameters != null)
            {
                foreach (var p in parameters)
                {
                    if (string.IsNullOrEmpty(p.Name))
                        throw new ApplicationException(string.Format("Query parameter name not specified: {0}", query.Name));

                    if (query.Parameters.Contains(p.Name))
                    {
                        if (p.IsAvailable)
                        {
                            var val = p.ArgumentValues;
                            if (val != null)
                                query.Parameters[p.Name] = val;
                        }
                    }
                }
            }
        }

        public static void SetRequest(this Query query, Request req)
        {
            #region Query Filters

            SetFilters(query, req.Filters);

            #endregion
            #region Query Parameters

            SetParameters(query, req.Parameters);

            #endregion
            #region Query Sorts

            SetSorts(query, req.Sorts);

            #endregion
        }
    }
}