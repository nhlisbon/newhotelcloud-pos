﻿using System;

namespace NewHotel.Core
{
    public static class IDataLoadExt
    {
        public static void LoadObject(this IDataLoad dataLoad, object id)
        {
            dataLoad.LoadObject(id, true);
        }
    }
}
