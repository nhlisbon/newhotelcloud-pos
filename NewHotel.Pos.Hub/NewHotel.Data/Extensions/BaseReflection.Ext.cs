﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using NewHotel.DataAnnotations;

namespace NewHotel.Core
{
    public static class BaseReflectionExt
    {
        #region Private Classes

        private class ReflectionEntry : IReflectionEntry
        {
            private readonly IAccessor Accessor;
            private readonly object Owner;

            public ReflectionEntry(IAccessor accessor, object owner)
            {
                Accessor = accessor;
                Owner = owner;
            }

            public object Value
            {
                get { return Accessor.Get(Owner); }
                set { Accessor.Set(Owner, value); }
            }

            public bool CanRead
            {
                get { return Accessor.CanRead; }
            }

            public bool CanWrite
            {
                get { return Accessor.CanWrite; }
            }

            public override string ToString()
            {
                return Accessor.ToString();
            }
        }

        private class ReflectionDictionary : IReflectionDictionary
        {
            [DebuggerBrowsableAttribute(DebuggerBrowsableState.Never)]
            private readonly IDictionary<string, IReflectionEntry> entries =
                new Dictionary<string, IReflectionEntry>();

            public void Add(string key, IAccessor accessor, object owner)
            {
                if (!Contains(key))
                    entries.Add(key, new ReflectionEntry(accessor, owner));
            }

            public bool Contains(string key)
            {
                return entries.ContainsKey(key);
            }

            public bool TryGetValue(string key, out IReflectionEntry entry)
            {
                return entries.TryGetValue(key, out entry);
            }

            public object this[string key]
            {
                get
                {
                    var entry = entries[key];
                    return entry.Value;
                }
                set
                {
                    var entry = entries[key];
                    entry.Value = value;
                }
            }

            public override string ToString()
            {
                return string.Join("\n", entries.Select(x => x.Key + " = " + (x.Value.Value ?? "NULL").ToString()).ToArray());
            }

            #region IEnumerable<KeyValuePair<string, IReflectionEntry>> Members

            public IEnumerator<KeyValuePair<string, IReflectionEntry>> GetEnumerator()
            {
                return entries.GetEnumerator();
            }

            #endregion
            #region IEnumerable Members

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return entries.Values.GetEnumerator();
            }

            #endregion
        }

        private static void BuildReflectionDictionary<T>(this T obj,
            string path, ReflectionDictionary values, HashSet<T> instances,
            Func<KeyValuePair<string, IAccessor>, bool> filter)
            where T : class
        {
            var type = obj.GetType();
            foreach (var propAccessor in type.PropAccessors(filter))
            {
                T baseObj = null;
                if (typeof(T).IsAssignableFrom(propAccessor.Value.Type))
                    baseObj = propAccessor.Value.Get(obj) as T;

                if (baseObj == null)
                {
                    var propKey = string.IsNullOrEmpty(path) ? propAccessor.Key : path + "." + propAccessor.Key;
                    values.Add(propKey, propAccessor.Value, obj);
                }
            }
        }

        private static bool ReflectionIncludeRead(IAccessor accessor)
        {
            const int ExcludeRead = 2;
            return accessor.CanRead && !accessor.GetAttributes<Attribute>()
                .Any(x => x.Match(ExcludeRead));
        }

        private static bool ReflectionIncludeWrite(IAccessor accessor)
        {
            const int ExcludeWrite = 4;
            return accessor.CanWrite && !accessor.GetAttributes<Attribute>()
                .Any(x => x.Match(ExcludeWrite));
        }

        #endregion

        public static IReflectionDictionary GetReflectionValues<T>(this T obj)
            where T : class
        {
            var items = new ReflectionDictionary();
            obj.BuildReflectionDictionary<T>(string.Empty, items, new HashSet<T>(),
                pa => ReflectionIncludeRead(pa.Value));

            return items;
        }

        public static void SetReflectionValues<T>(this T obj, IReflectionDictionary source)
            where T : class
        {
            var items = new ReflectionDictionary();
            obj.BuildReflectionDictionary<T>(string.Empty, items, new HashSet<T>(),
                pa => ReflectionIncludeWrite(pa.Value));

            foreach (var item in items)
            {
                IReflectionEntry entry;
                if (item.Value.CanWrite && source.TryGetValue(item.Key, out entry))
                {
                    if (entry.CanRead)
                        item.Value.Value = entry.Value;
                }
            }
        }
    }
}
