﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Core
{
    public static class IRecordExt
    {
        public static T ValueAs<T>(this IRecord record, string name)
        {
            return (T)record.ValueAs(typeof(T), name);
        }

        public static object Value(this IRecord record, string name)
        {
            return record[name];
        }

        public static bool IsNull(this IRecord record, string name)
        {
            var value = record[name];
            return value == null || value.Equals(DBNull.Value);
        }

        //public static object[] ToArray(this IRecord record)
        //{
        //    var values = new List<object>();
        //    for (int i = 0; i < record.Count; i++)
        //        values.Add(record[i]);

        //    return values.ToArray();
        //}

        public static void CopyTo(this IRecord source, IRecord target)
        {
			foreach (var name in source.Fields)
			{
				if (target.Contains(name))
					target[name] = source[name];
			}
        }
    }
}
