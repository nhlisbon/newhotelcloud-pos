﻿using NewHotel.DataAnnotations;
using NewHotel.Contracts;

namespace NewHotel.Core
{
    public static class IBaseObjectExt
    {
        public static IReflectionDictionary Values(this IBaseObject obj)
        {
            var p = obj as BasePersistent;
            if (p != null)
                return p.GetValues();
            else
            {
                var c = obj as BaseContract;
                if (c != null)
                    return c.GetValues();
            }

            return null;
        }
    }
}
