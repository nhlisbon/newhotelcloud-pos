﻿using System;
using System.Linq;
using System.Collections.Generic;
using NewHotel.DataAnnotations;

namespace NewHotel.Core
{
    public static class BasePersistentExt
    {
        #region Aux methods

        private static string GetName(string name)
        {
            var len = name.Length - 1;
            if (len >= 0)
            {
                switch (name[len])
                {
                    case '+':
                    case '-':
                        return name.Substring(0, len);
                }
            }

            return name;
        }

        private static string GetAliasName(string name)
        {
            return GetName(name).Replace('.', '$');
        }

        private static string GetFullName(string name)
        {
            return GetName(name).Replace('$', '.');
        }

        private static string GetParamName(string name)
        {
            return ":" + GetAliasName(name);
        }

        private static string GetTableName(string name)
        {
            name = GetName(name);
            return name.Substring(0, name.IndexOf('.'));
        }

        private static string GetColName(string name)
        {
            name = GetName(name);
            return name.Substring(name.IndexOf('.') + 1);
        }

        #endregion
        #region Reflection attributes

        public static IDictionary<Type, MappingTable[]> TableAttributes(this Type type, bool inherit)
        {
            var attrs = new Dictionary<Type, MappingTable[]>();

            while (type != typeof(object))
            {
                var mappings = type.GetCustomAttributes(typeof(MappingTable), false)
                    .Cast<MappingTable>().OrderBy(x => x.Index).ToArray();
                if (mappings.Length > 0)
                    attrs.Add(type, mappings);

                type = type.BaseType;
                if (!inherit)
                    break;
            }

            return attrs;
        }

        /// <summary>
        /// Identificador de la clase
        /// </summary>
        public static IAccessor IdPropAccessor(this Type type)
        {
            return type.PropAccessors()["Id"];
        }

        private static IDictionary<Type, IDictionary<Type, string[]>> _persistentTablesMap =
            new Dictionary<Type, IDictionary<Type, string[]>>();
        public static IDictionary<Type, string[]> PersistentTables(this Type type)
        {
            IDictionary<Type, string[]> persistentTables;

            lock (_persistentTablesMap)
            {
                if (!_persistentTablesMap.TryGetValue(type, out persistentTables))
                {
                    persistentTables = new Dictionary<Type, string[]>();
                    var tableAttrs = TableAttributes(type, true);
                    if (tableAttrs.Count > 0)
                    {
                        foreach (var attr in tableAttrs)
                        {
                            if (persistentTables.ContainsKey(attr.Key))
                                throw new ApplicationException(string.Format("Duplicated database table for {0}", type.Name));

                            var values = attr.Value.Select(x => x.Name + '.' + x.Id).Distinct().ToArray();
                            if (values.Length > 0)
                                persistentTables.Add(attr.Key, values);
                        }
                        _persistentTablesMap.Add(type, persistentTables);
                    }
                }
            }

            return persistentTables;
        }

        private static IDictionary<Type, IDictionary<string, IAccessor>> _persistentPropsMap =
            new Dictionary<Type, IDictionary<string, IAccessor>>();
        public static IDictionary<string, IAccessor> PersistentProps(this Type type)
        {
            IDictionary<string, IAccessor> persistentProps;

            lock (_persistentPropsMap)
            {
                if (!_persistentPropsMap.TryGetValue(type, out persistentProps))
                {
                    var tableAttrs = TableAttributes(type, true);
                    if (tableAttrs.Count > 0)
                    {
                        var idPropAccessor = IdPropAccessor(type);
                        persistentProps = new Dictionary<string, IAccessor>();

                        foreach (var tabAttr in tableAttrs.Values.SelectMany(x => x))
                        {
                            if (!string.IsNullOrEmpty(tabAttr.Id))
                            {
                                var colName = tabAttr.Id;
                                var tableName = tabAttr.Name;
                                if (!string.IsNullOrEmpty(tableName))
                                    colName = tableName + "." + colName;

                                if (persistentProps.ContainsKey(colName))
                                    throw new ApplicationException(string.Format("Duplicated database key column for {0}.{1}", idPropAccessor.DeclaringType.Name, idPropAccessor.Name));

                                persistentProps.Add(colName, idPropAccessor);
                            }
                        }

                        foreach (var prop in type.PropAccessors().Values)
                        {
                            var colAttr = prop.GetAttributes<MappingColumn>().FirstOrDefault();
                            if (colAttr != null)
                            {
                                var colName = colAttr.Name;
                                var tableName = string.Empty;
                                MappingTable[] mappings;
                                if (tableAttrs.TryGetValue(prop.DeclaringType, out mappings) && mappings.Length == 1)
                                    tableName = mappings[0].Name;
                                if (colName.IndexOf('.') < 0 && !string.IsNullOrEmpty(tableName))
                                    colName = tableName + "." + colName;

                                if (persistentProps.ContainsKey(colName))
                                    throw new ApplicationException(string.Format("Duplicated database column for {0}.{1}", prop.DeclaringType.Name, prop.Name));

                                persistentProps.Add(colName, prop);
                            }
                        }

                        _persistentPropsMap.Add(type, persistentProps);
                    }
                }
            }

            return persistentProps;
        }

        private static IDictionary<Type, IDictionary<string, string>> _referenceExprsMap = new Dictionary<Type, IDictionary<string, string>>();
        public static IDictionary<string, string> ReferenceExprs(this Type type)
        {
            IDictionary<string, string> referenceExprs;

            lock (_referenceExprsMap)
            {
                if (!_referenceExprsMap.TryGetValue(type, out referenceExprs))
                {
                    var tableAttrs = TableAttributes(type, true);
                    if (tableAttrs.Count > 0)
                    {
                        referenceExprs = new Dictionary<string, string>();

                        var persistentProps = PersistentProps(type);
                        int i = 0;
                        foreach (var prop in type.PropAccessors().Values)
                        {
                            var colAttr = prop.GetAttributes<PersistentRefColumn>().FirstOrDefault();
                            if (colAttr != null)
                            {
                                var mappings = tableAttrs[prop.DeclaringType];
                                var tableName = GetTableName(colAttr.TableColumnName);
                                var colName = GetColName(colAttr.TableColumnName);
                                var filters = string.Join(" AND ", colAttr.Filters.Select(x => x + " = :" + GetColName(x)).ToArray());
                                var translationAttr = colAttr as PersistentTranslation;
                                var expr = "(SELECT " + colName + " FROM " + tableName;
                                if (translationAttr != null && !translationAttr.RelationTablePk.Equals(string.Empty))
                                {
                                    var foreignTableName = GetTableName(translationAttr.RelationTablePk);
                                    var foreignKeyName = translationAttr.ForeignKeyToRelationTable;
                                    if (foreignKeyName.IndexOf('.') < 0 && mappings.Length == 1)
                                        foreignKeyName = mappings[0].Name + "." + foreignKeyName;
                                    expr += " INNER JOIN " + foreignTableName +
                                            " ON " + tableName + "." + translationAttr.PrimaryColumnId + " = " + foreignTableName + "." + translationAttr.ReferencedColumnId +
                                            " WHERE " + translationAttr.RelationTablePk + " = " + foreignKeyName;
                                }
                                else
                                {
                                    var referencedColumnId = colAttr.ReferencedColumnId;
                                    if (referencedColumnId.IndexOf('.') < 0 && mappings.Length == 1)
                                        referencedColumnId = mappings[0].Name + "." + referencedColumnId;
                                    expr += " WHERE " + colAttr.PrimaryColumnId + " = " + referencedColumnId;
                                }

                                var alias = colAttr.TableColumnName + i.ToString();

                                if (filters != string.Empty)
                                    expr += " AND " + filters;
                                expr += ") AS " + GetAliasName(alias);

                                if (persistentProps.ContainsKey(alias))
                                    throw new ApplicationException(string.Format("Duplicated database column alias {0} for {1}.{2}", alias, prop.DeclaringType.Name, prop.Name));
                                persistentProps.Add(alias, prop);

                                if (referenceExprs.ContainsKey(alias))
                                    throw new ApplicationException(string.Format("Duplicated database column alias {0} for {1}.{2}", alias, prop.DeclaringType.Name, prop.Name));
                                referenceExprs.Add(alias, expr);

                                i++;
                            }
                            else
                            {
                                var exprAttr = prop.GetAttributes<PersistentExpressionColumn>().FirstOrDefault();
                                if (exprAttr != null)
                                {
                                    var alias = exprAttr.Alias;
                                    var expression = "(" + exprAttr.Expression + ") AS " + GetAliasName(alias);

                                    if (persistentProps.ContainsKey(alias))
                                        throw new ApplicationException(string.Format("Duplicated database expression alias {0} for {1}.{2}", alias, prop.DeclaringType.Name, prop.Name));
                                    persistentProps.Add(alias, prop);

                                    if (referenceExprs.ContainsKey(alias))
                                        throw new ApplicationException(string.Format("Duplicated database expression alias {0} for {1}.{2}", alias, prop.DeclaringType.Name, prop.Name));
                                    referenceExprs.Add(alias, expression);
                                }
                            }
                        }

                        _referenceExprsMap.Add(type, referenceExprs);
                    }
                }
            }

            return referenceExprs;
        }

        private static IDictionary<Type, IDictionary<string, IAccessor>> _referencePropsMap =
            new Dictionary<Type, IDictionary<string, IAccessor>>();
        public static IDictionary<string, IAccessor> ReferenceProps(this Type type)
        {
            IDictionary<string, IAccessor> referenceProps;

            lock (_referencePropsMap)
            {
                if (!_referencePropsMap.TryGetValue(type, out referenceProps))
                {
                    var tableAttrs = TableAttributes(type, true);
                    if (tableAttrs.Count > 0)
                    {
                        referenceProps = new Dictionary<string, IAccessor>();

                        foreach (var prop in type.PropAccessors().Values
                            .Where(pa => typeof(BasePersistent).IsAssignableFrom(pa.Type)))
                        {
                            var colAttr = prop.GetAttributes<PersistentObject>().FirstOrDefault();
                            if (colAttr != null)
                            {
                                var colName = colAttr.Name;
                                if (colName.IndexOf('.') < 0)
                                {
                                    var mappings = tableAttrs[prop.DeclaringType];
                                    if (mappings.Length == 1)
                                        colName = mappings[0].Name + "." + colName;
                                }

                                if (referenceProps.ContainsKey(colName))
                                    throw new ApplicationException(string.Format("Duplicated database referenced column for {0}.{1}", prop.DeclaringType.Name, prop.Name));

                                referenceProps.Add(colName, prop);
                            }
                        }

                        _referencePropsMap.Add(type, referenceProps);
                    }
                }
            }

            return referenceProps;
        }

        #endregion
        #region Public Methods

        public static IReflectionDictionary GetValues(this BasePersistent obj)
        {
            return obj.GetReflectionValues<BasePersistent>();
        }

        public static void SetValues(this BasePersistent obj, IReflectionDictionary dict)
        {
            obj.SetReflectionValues<BasePersistent>(dict);
        }

        public static void FillObject<T>(this T obj, IRecord record)
            where T : class
        {
            const char TrueValue = '1';
            const char FalseValue = '0';

            var onLoad = obj as IOnLoad;

            if (onLoad != null)
                onLoad.BeforeLoad();

            var objType = obj.GetType();
            foreach (var prop in objType.PersistentProps())
            {
                var accessor = prop.Value;
                try
                {
                    var name = GetAliasName(prop.Key);
                    var type = accessor.BaseType;
                    if (type.Equals(typeof(object)))
                        type = record.GetDataType(name);
                    var isNullable = accessor.Nullable;

                    object value = null;
                    if (record.IsNull(name))
                    {
                        if (isNullable)
                        {
                            if (type == typeof(string) && accessor.NullStringAsEmpty)
                                value = string.Empty;
                        }
                        else
                        {
                            throw new ApplicationException(string.Format("Error asigning null to {0}.{1}", objType.FullName, accessor.Name));
                        }
                    }
                    else if (type.IsEnum)
                    {
                        try
                        {
                            if (!type.TryGetEnum(record[name], out value))
                                throw new ApplicationException(string.Format("Error asigning {0} to {1}.{2}. Out of bounds", value, objType.FullName, accessor.Name));
                        }
                        catch (Exception e)
                        {
                            throw new ApplicationException(string.Format("Error reading enum {0} to {1}.{2}", value, objType.FullName, accessor.Name), e);
                        }
                    }
                    else if (type == typeof(bool))
                    {
                        value = record[name];
                        if (value.GetType() != typeof(bool))
                        {
                            var boolStr = value.ToString();
                            switch (boolStr.Length > 0 ? boolStr[0] : char.MinValue)
                            {
                                case FalseValue:
                                    value = false;
                                    break;
                                case TrueValue:
                                    value = true;
                                    break;
                                default:
                                    throw new ApplicationException(string.Format("Error asigning as boolean {0} to {1}.{2}", boolStr, objType.FullName, accessor.Name));
                            }
                        }
                    }
                    else if (type == typeof(Guid))
                    {
                        value = record[name];
                        if (value.GetType() == typeof(byte[]))
                            value = new Guid((byte[])value);
                        else if (value.GetType() != typeof(Guid))
                            throw new ApplicationException(string.Format("Error asigning as Guid {0} to {1}.{2}", value, objType.FullName, accessor.Name));
                    }
                    else if (type == typeof(DateTime))
                    {
                        value = ((DateTime)record[name]).ToUtcDateTime();
                    }
                    else if (type == typeof(ARGBColor))
                    {
                        value = record[name];
                        int color = (int)Convert.ChangeType(value, typeof(int));
                        value = new ARGBColor(color);
                    }
                    else if (type == typeof(Blob))
                    {
                        value = record[name];
                        if (value.GetType() == typeof(byte[]))
                            value = new Blob((byte[])value);
                    }
                    else if (type == typeof(Clob))
                    {
                        value = record[name];
                        if (value.GetType() == typeof(string))
                            value = new Clob((string)value);
                    }
                    else if (type == typeof(object))
                    {
                        value = record[name];
                    }
                    else
                    {
                        value = record[name];
                        try
                        {
                            value = type.ChangeType(value);
                        }
                        catch (Exception e)
                        {
                            throw new ApplicationException(string.Format("Error casting {0} to {1}.{2}", value, objType.Name, accessor.Name), e);
                        }
                    }

                    try
                    {
                        accessor.Set(obj, value);
                    }
                    catch (Exception e)
                    {
                        throw new ApplicationException(string.Format("Error asigning {0} to {1}.{2}", value, objType.FullName, accessor.Name), e);
                    }
                }
                catch (Exception failure) when (!(failure is ApplicationException))
                {
					throw new ApplicationException($"Error asigning to {objType.FullName}.{accessor.Name}", failure);
				}
            }

            var dataFill = obj as IDataFill;
            if (dataFill != null)
                dataFill.FillData(record);

            if (onLoad != null)
                onLoad.AfterLoad();
        }

        #endregion
        #region Default Comparer

        private static Func<P1, P2, bool> DefaultComparer<P1, P2>()
            where P1 : IBaseObject
            where P2 : IBaseObject
        {
            return delegate(P1 obj1, P2 obj2)
            {
                return obj1.Id.Equals(obj2.Id);
            };
        }

        #endregion
        #region Syncronize Methods

        public static void Syncronize<T, S>(this ICollection<S> source, IDatabaseManager manager, ICollection<T> target,
            Action<T, S, bool> beforeAction, Action<T, S, bool> afterAction, Action<T> removeAction, Func<T, bool> remove, Func<T, S, bool> compare)
            where S : IBaseObject
            where T : BasePersistent
        {
            var toDelete = target.Where(to => !source.Exists(so => compare.Invoke(to, so))).ToArray();
            foreach (var t in toDelete)
            {
                if (remove.Invoke(t) && target.Remove(t))
                {
                    if (removeAction != null)
                        removeAction.Invoke(t);
                }
            }

            foreach (var s in source)
            {
                var t = target.FirstOrDefault(to => compare.Invoke(to, s));

                var isNew = t == null;
                if (isNew)
                {
                    t = (T)Activator.CreateInstance(typeof(T), manager);
                    target.Add(t);
                }

                if (beforeAction != null)
                    beforeAction.Invoke(t, s, isNew);

                t.SetValues(s.Values());

                if (afterAction != null)
                    afterAction.Invoke(t, s, isNew);
            }
        }

        public static void Syncronize<T, S>(this ICollection<S> source,
            IDatabaseManager manager, ICollection<T> target, Action<T, S, bool> afterAction, Action<T> removeAction, Func<T, bool> remove)
        where S : IBaseObject
        where T : BasePersistent
        {
            Syncronize(source, manager, target, null, afterAction, removeAction, remove, DefaultComparer<T, S>());
        }

        public static void Syncronize<T, S>(this ICollection<S> source,
            IDatabaseManager manager, ICollection<T> target, Action<T, S, bool> beforeAction, Action<T, S, bool> afterAction, Func<T, bool> remove)
        where S : IBaseObject
        where T : BasePersistent
        {
            Syncronize(source, manager, target, beforeAction, afterAction, null, remove, DefaultComparer<T, S>());
        }

        public static void Syncronize<T, S>(this ICollection<S> source,
            IDatabaseManager manager, ICollection<T> target, Action<T, S, bool> afterAction, Func<T, bool> remove)
        where S : IBaseObject
        where T : BasePersistent
        {
            Syncronize(source, manager, target, null, afterAction, null, remove, DefaultComparer<T, S>());
        }

        public static void Syncronize<T, S>(this ICollection<S> source,
            IDatabaseManager manager, ICollection<T> target, Action<T, S, bool> afterAction)
        where S : IBaseObject
        where T : BasePersistent
        {
            Syncronize(source, manager, target, null, afterAction, null, (po) => { return true; }, DefaultComparer<T, S>());
        }

        public static void Syncronize<T, S>(this ICollection<S> source,
            IDatabaseManager manager, ICollection<T> target, Action<T, S, bool> afterAction, Func<T, bool> remove, Func<T, S, bool> comparer)
        where S : IBaseObject
        where T : BasePersistent
        {
            Syncronize(source, manager, target, null, afterAction, null, remove, comparer);
        }

        public static void Syncronize<T, S>(this ICollection<S> source,
            IDatabaseManager manager, ICollection<T> target, Action<T, S, bool> afterAction, Func<T, S, bool> comparer)
        where S : IBaseObject
        where T : BasePersistent
        {
            Syncronize(source, manager, target, null, afterAction, null, (po) => { return true; }, comparer);
        }

        public static void Syncronize<T, S>(this ICollection<S> source,
            IDatabaseManager manager, ICollection<T> target, Action<T> removeAction, Func<T, bool> remove)
        where S : IBaseObject
        where T : BasePersistent
        {
            Syncronize(source, manager, target, null, null, removeAction, remove, DefaultComparer<T, S>());
        }

        public static void Syncronize<T, S>(this ICollection<S> source,
            IDatabaseManager manager, ICollection<T> target, Action<T> removeAction)
        where S : IBaseObject
        where T : BasePersistent
        {
            Syncronize(source, manager, target, null, null, removeAction, (po) => { return true; }, DefaultComparer<T, S>());
        }

        public static void Syncronize<T, S>(this ICollection<S> source,
            IDatabaseManager manager, ICollection<T> target, Action<T> removeAction, Func<T, bool> remove, Func<T, S, bool> comparer)
        where S : IBaseObject
        where T : BasePersistent
        {
            Syncronize(source, manager, target, null, null, removeAction, remove, comparer);
        }

        public static void Syncronize<T, S>(this ICollection<S> source,
            IDatabaseManager manager, ICollection<T> target, Func<T, bool> remove)
        where S : IBaseObject
        where T : BasePersistent
        {
            Syncronize(source, manager, target, null, null, null, remove, DefaultComparer<T, S>());
        }

        public static void Syncronize<T, S>(this ICollection<S> source,
            IDatabaseManager manager, ICollection<T> target)
        where S : IBaseObject
        where T : BasePersistent
        {
            Syncronize(source, manager, target, null, null, null, (po) => { return true; }, DefaultComparer<T, S>());
        }

        public static void Syncronize<T, S>(this ICollection<S> source,
			IDatabaseManager manager, ICollection<T> target, Func<T, bool> remove, Func<T, S, bool> comparer)
        where S : IBaseObject
        where T : BasePersistent
        {
            Syncronize(source, manager, target, null, null, null, remove, comparer);
        }

        #endregion
        //#region IKeyedCollection Sysncronize Methods

        //public static void Syncronize<T, S>(this IKeyedCollection<object, S> source,
        //    IDatabaseManager manager, IKeyedCollection<object, T> target,
        //    Action<T, S, bool> persistAction, Action<T> removeAction)
        //    where S : IBaseObject
        //    where T : BasePersistent
        //{
        //    var toDelete = new List<T>();
        //    foreach (var t in target)
        //    {
        //        if (!source.ContainsKey(t.Id))
        //            toDelete.Add(t);
        //    }

        //    foreach (var t in toDelete)
        //    {
        //        if (target.Remove(t) && removeAction != null)
        //            removeAction.Invoke(t);
        //    }

        //    foreach (var s in source)
        //    {
        //        T t;
        //        var isNew = false;
        //        if (!target.TryGetItem(s.Id, out t))
        //        {
        //            isNew = true;
        //            t = (T)Activator.CreateInstance(typeof(T), manager);
        //            target.Add(t);
        //        }

        //        t.SetValues(s.Values());
        //        if (persistAction != null)
        //            persistAction.Invoke(t, s, isNew);
        //    }
        //}

        //public static void Syncronize<T, S>(this IKeyedCollection<object, S> source,
        //    IDatabaseManager manager, IKeyedCollection<object, T> target, Action<T, S, bool> persistAction)
        //    where S : IBaseObject
        //    where T : BasePersistent
        //{
        //    Syncronize(source, manager, target, persistAction, null);
        //}

        //public static void Syncronize<T, S>(this IKeyedCollection<object, S> source,
        //    IDatabaseManager manager, IKeyedCollection<object, T> target, Action<T> removeAction)
        //    where S : IBaseObject
        //    where T : BasePersistent
        //{
        //    Syncronize(source, manager, target, null, removeAction);
        //}

        //#endregion
        #region Load Methods

        public static T LoadOrNewObject<T, K>(this T basePersistent, IDatabaseManager manager, ref K? id)
            where T : BasePersistent
            where K : struct
        {
            if (id.HasValue)
                return (T)Activator.CreateInstance(typeof(T), manager, id.Value);
            else
            {
                var obj = (T)Activator.CreateInstance(typeof(T), manager);
                id = (K)obj.Id;
                return obj;
            }
        }

        public static void Load<S, T>(this ICollection<T> target, IEnumerable<S> source, IDatabaseManager manager,
            Func<S, IDatabaseManager, T> action)
            where T : BasePersistent
            where S : IBaseObject
        {
            foreach (var sourceItem in source)
            {
                var targetItem = action.Invoke(sourceItem, manager);
                if (targetItem != null)
                {
                    targetItem.SetValues(sourceItem.Values());
                    target.Add(targetItem);
                }
            }
        }

        public static void Load<S, T>(this ICollection<T> target, IEnumerable<S> source, IDatabaseManager manager)
            where T : BasePersistent
            where S : IBaseObject
        {
            Load(target, source, manager, (p, m) => (T)Activator.CreateInstance(typeof(T), m));
        }

        #endregion
        #region Update Methods

        public static void Update<S, T>(this ICollection<T> target, IDatabaseManager manager, ICollection<S> source,
            Action<S, T> updateAction, Func<S, T, bool> compare)
            where T : IBaseObject
            where S : BasePersistent
        {
            foreach (var s in source)
            {
                var t = target.FirstOrDefault(to => compare.Invoke(s, to));
                if (t != null)
                    updateAction(s, t);
            }
        }

        public static void Update<S, T>(this ICollection<T> target, IDatabaseManager manager, ICollection<S> source,
            Action<S, T> updateAction)
            where T : IBaseObject
            where S : BasePersistent
        {
            Update(target, manager, source, updateAction, DefaultComparer<S, T>());
        }

        #endregion
        #region Clone Methods

        public static T Clone<T>(this T obj, IDatabaseManager manager)
            where T : BasePersistent
        {
            var newObj = (T)Activator.CreateInstance(typeof(T), manager);
            newObj.SetValues(obj.GetValues());
            return newObj;
        }

        public static void Clone<S, T>(this ICollection<T> target, IEnumerable<S> source, IDatabaseManager manager,
            Func<T, S, bool> action)
            where T : BasePersistent
            where S : IBaseObject
        {
            target.Clear();
            foreach (var sourceItem in source)
            {
                var targetItem = (T)Activator.CreateInstance(typeof(T), manager);
                targetItem.SetValues(sourceItem.Values());
                if (action.Invoke(targetItem, sourceItem))
                    target.Add(targetItem);
            }
        }

        public static void Clone<S, T>(this ICollection<T> target, IEnumerable<S> source, IDatabaseManager manager)
            where T : BasePersistent
            where S : IBaseObject
        {
            Clone(target, source, manager, (t, s) => true);
        }

        public static void Clone<S, T>(this ICollection<T> target, IEnumerable<S> source, IDatabaseManager manager,
            Action<T, S> action)
            where T : BasePersistent
            where S : IBaseObject
        {
            Clone(target, source, manager, (t, s) => { action.Invoke(t, s); return true; });
        }

        #endregion
    }
}
