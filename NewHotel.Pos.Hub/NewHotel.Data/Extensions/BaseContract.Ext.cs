﻿using NewHotel.DataAnnotations;
using NewHotel.Contracts;

namespace NewHotel.Core
{
    public static class BaseContractExt
    {
        public static IReflectionDictionary GetValues(this BaseContract obj)
        {
            return obj.GetReflectionValues<BaseContract>();
        }

        public static void SetValues(this BaseContract obj, IReflectionDictionary dict)
        {
            obj.SetReflectionValues<BaseContract>(dict);
        }
    }
}