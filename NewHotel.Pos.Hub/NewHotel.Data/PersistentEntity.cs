﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Core
{
    public class PersistentEntity<K, T>
        where K : struct
        where T : BasePersistent
    {
        private IDatabaseManager manager;
        private K? id;
        private T basePersistent;

        public IDatabaseManager Manager
        {
            set { manager = value; }
        }

        private T BasePersistent
        {
            get 
            {
                if (basePersistent == null)
                {
                    basePersistent = basePersistent.LoadOrNewObject(manager, ref id);
                }

                return basePersistent;
            }
        }

        private PersistentEntity(T obj)
        {
            basePersistent = obj;
            id = (K)obj.Id;
        }

        public static implicit operator T(PersistentEntity<K, T> obj)
        {
            return obj.BasePersistent;
        }

        public static implicit operator PersistentEntity<K, T>(T obj)
        {
            return new PersistentEntity<K, T>(obj);
        }

        public PersistentEntity(K id)
        {
            this.id = id;
            basePersistent = null;
        }

        public static implicit operator K?(PersistentEntity<K, T> obj)
        {
            return obj.id;
        }

        public static implicit operator PersistentEntity<K, T>(K id)
        {
            return new PersistentEntity<K, T>(id);
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
