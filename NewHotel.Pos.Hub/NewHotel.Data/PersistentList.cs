﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;

namespace NewHotel.Core
{
    public sealed class PersistentList<T> : IPersistentList<T>
        where T : BasePersistent
    {
        #region Members

        private IList<T> _list;
        private List<T> _removePending;
        private readonly IDatabaseManager _manager;
        private readonly string[] _parameters;
        private readonly IQueryStatement _query;

        #endregion
        #region Private Properties

        private string CommandText
        {
            get { return string.Format(_query.ToString(), string.Empty); }
        }

        private IList<T> Collection
        {
            get
            {
                if (_list == null)
                    _list = new PersistentCollection<T>();

                return _list;
            }
        }

        private IList<T> List
        {
            get { return Collection; }
        }

        private List<T> RemovePending
        {
            get
            {
                if (_removePending == null)
                    _removePending = new List<T>();

                return _removePending;
            }
        }

        #endregion
        #region Constructors

        public PersistentList(IDatabaseManager manager)
            : base()
        {
            _manager = manager;
        }

        public PersistentList(IDatabaseManager manager, IQueryStatement query)
            : this(manager)
        {
            _query = query;
            if (_query != null)
                _parameters = SqlUtils.ExtractParametersFromSql(CommandText);
        }

        public PersistentList(IDatabaseManager manager, IList<T> list)
            : this(manager)
        {
            _list = new PersistentCollection<T>();
            foreach (var item in list)
                _list.Add(item);
        }

        #endregion
        #region Public Properties

        public bool IsDirty
        {
            get { return List.Any(x => x.IsDirty); }
        }

        public IEnumerable<T> Dirty
        {
            get { return List.Where(x => x.IsDirty); }
        }

        public IEnumerable<T> Removed
        {
            get { return RemovePending; }
        }

        #endregion
        #region IList<T> Members

        public int IndexOf(T item)
        {
            return List.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            List.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            if (index >= 0 && index < List.Count)
            {
                RemovePending.Add(List[index]);
                List.RemoveAt(index);
            }
        }

        public T this[int index]
        {
            get
            {
                return List[index];
            }
            set
            {
                List[index] = value;
            }
        }

        #endregion
        #region ICollection<T> Members

        public void Add(T item)
        {
            List.Add(item);
        }

        public void Clear()
        {
            RemovePending.AddRange(List);
            List.Clear();
        }

        public bool Contains(T item)
        {
            return List.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            List.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return List.Count; }
        }

        public bool IsReadOnly
        {
            get { return List.IsReadOnly; }
        }

        public bool Remove(T item)
        {
            if (List.Contains(item))
            {
                RemovePending.Add(item);
                return List.Remove(item);
            }

            return false;
        }

        #endregion
        #region IEnumerable<T> Members

        public IEnumerator<T> GetEnumerator()
        {
            return List.GetEnumerator();
        }

        #endregion
        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return List.GetEnumerator();
        }

        #endregion
        #region IPersistentList<T> Members

        public void AddRange(IEnumerable<T> items)
        {
            foreach (var item in items)
                List.Add(item);
        }

        public bool Populated
        {
            get { return _list != null; }
        }

        public void PersistObjects()
        {
            if (Populated)
            {
                foreach (var item in this)
                {
                    var dataSave = item as IDataSave;
                    if (dataSave != null)
                        dataSave.SaveObject();
                }
            }
        }

        public void PersistOrDeleteObjects()
        {
            DeleteObjects();
            PersistObjects();
        }

        public void DeleteObjects()
        {
            if (_removePending != null)
            {
                var deleted = new List<T>();
                try
                {
                    foreach (var item in RemovePending)
                    {
                        var dataDelete = item as IDataDelete;
                        if (dataDelete != null)
                        {
                            dataDelete.DeleteObject();
                            deleted.Add(item);
                        }
                    }
                }
                finally
                {
                    foreach (var item in deleted)
                        RemovePending.Remove(item);
                }
            }
        }

        public void DeleteAllObjects()
        {
            Clear();
            PersistOrDeleteObjects();
        }

        private static void AssignDefaultParameters(IDataParameterCollection parameters)
        {
            if (parameters.Contains("LANG_PK"))
                ((IDbDataParameter)parameters["LANG_PK"]).Value = NewHotelAppContext.GetLanguage();
            if (parameters.Contains("MAX_DECPREC"))
                ((IDbDataParameter)parameters["MAX_DECPREC"]).Value = 18;
        }

        private IDbCommand GetCommand(object[] values)
        {
            if (_query == null)
                throw new InvalidOperationException("Query not specified.");

            var command = _manager.CreateCommand("NewHotel.List." + typeof(T).Name);
            command.CommandText = CommandText;

            foreach (var paramName in _parameters)
            {
                var parameter = _manager.CreateParameter(command);
                parameter.ParameterName = paramName;
                parameter.Direction = ParameterDirection.Input;
            }

            AssignDefaultParameters(command.Parameters);

            if (values == null)
                values = new object[] { null };
            for (int i = 0; i < values.Length; i++)
            {
                var name = "ARG" + i.ToString();
                if (!command.Parameters.Contains(name))
                    throw new ArgumentException(string.Format("Param name {0} not specified.", name));

                var value = values[i] == null ? DBNull.Value : _manager.ConvertValueType(values[i]);
                ((IDbDataParameter)command.Parameters[name]).Value = value;
            }

            return command;
        }

        private void ExecuteReader(IDbCommand command, int count)
        {
            var type = typeof(T);
            using (var reader = _manager.ExecuteReader(command))
            {
                var commandStopwatch = _manager.StartCommand(command);
                try
                {
                    var data = _manager.GetQueryReader(reader);
                    int i = 0;
                    while (data.Read())
                    {
                        i++;
                        if (count > 0 && i > count)
                            break;
                        var item = (T)Activator.CreateInstance(type, _manager);
                        item.LoadObjectFromDB(data);
                        List.Add(item);
                    }
                }
                finally
                {
                    reader.Close();
                    if (commandStopwatch != null)
                    {
                        commandStopwatch.Fetched();
                        _manager.EndCommand(commandStopwatch);
                    }
                }
            }
        }

        public IPersistentList<T> ExecuteTop(int count, params object[] values)
        {
            _manager.Open();
            try
            {
                List.Clear();
                using (var command = GetCommand(values))
                {
                    ExecuteReader(command, count);
                }
            }
            finally
            {
                _manager.Close();
            }

            return this;
        }

        public T ExecuteFirst(params object[] values)
        {
            _manager.Open();
            try
            {
                List.Clear();
                using (var command = GetCommand(values))
                {
                    ExecuteReader(command, 1);
                }
            }
            finally
            {
                _manager.Close();
            }

            return List.Count != 0 ? List[0] : null;
        }

        public IPersistentList<T> Execute(params object[] values)
        {
            _manager.Open();
            try
            {
                List.Clear();
                using (var command = GetCommand(values))
                {
                    ExecuteReader(command, 0);
                }
            }
            finally
            {
                _manager.Close();
            }

            return this;
        }

        public T[] ExecuteReadOnly(params object[] values)
        {
            return Execute(values).ToArray();
        }

        #endregion
    }
}