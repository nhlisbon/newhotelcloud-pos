﻿using System;

namespace NewHotel.Core
{
    public abstract class BasePersistentFullOperations<T> : BasePersistent<T>, IDataFullOperations
    {
        #region Constructors

        public BasePersistentFullOperations(IDatabaseManager manager) 
            : base(manager) { }

        protected BasePersistentFullOperations(IDatabaseManager manager, IIdGenerator idGenerator)
            : base(manager, idGenerator) { }

        #endregion
        #region IDataFullControl operations

        public virtual bool SaveObject()
        {
            return PersistObjectToDB();
        }

        public virtual bool SaveObject(bool skipLastModifiedVerification)
        {
            return PersistObjectToDB(skipLastModifiedVerification);
        }

        public virtual bool LoadObject(object id, bool throwNoDataFoundException)
        {
            return LoadObjectFromDB(id, throwNoDataFoundException);
        }

        public virtual bool DeleteObject()
        {
            return DeleteObjectFromDB();
        }

        #endregion
    }

    public abstract class BasePersistentFullOperations : BasePersistentFullOperations<Guid>
    {
        #region Constructors

        public BasePersistentFullOperations(IDatabaseManager manager)
            : base(manager, GuidIdGenerator) { }

        public BasePersistentFullOperations(IDatabaseManager manager, object id)
            : this(manager)
        {
            this.LoadObject(id);
        }

        #endregion
    }
}
