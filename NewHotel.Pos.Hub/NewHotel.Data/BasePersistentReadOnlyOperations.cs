﻿using System;
using System.Collections.Generic;

namespace NewHotel.Core
{
    public abstract class BasePersistentReadonlyOperations<T> : BasePersistent<T>, IDataReadOnlyOperations
    {
        #region Constructors

        public BasePersistentReadonlyOperations(IDatabaseManager manager)
            : base(manager) { }

        protected BasePersistentReadonlyOperations(IDatabaseManager manager, IIdGenerator idGenerator)
            : base(manager, idGenerator) { }

        #endregion
        #region IDataReadOnlyOperations operations

        public virtual bool LoadObject(object id, bool throwNoDataFoundException)
        {
            return LoadObjectFromDB(id, throwNoDataFoundException);
        }

        #endregion
    }

    public abstract class BasePersistentReadonlyOperations : BasePersistentReadonlyOperations<Guid>
    {
        #region Constructors

        public BasePersistentReadonlyOperations(IDatabaseManager manager)
            : base(manager, GuidIdGenerator) { }

        #endregion
    }
}
