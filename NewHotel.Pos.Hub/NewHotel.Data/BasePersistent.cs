﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Core
{
    public interface IIdGenerator
    {
        Type Type { get; }
        object Generate();
    }

    public interface ILogPropertyChange
    {
        void LogPropertyChange(object oldValue, object newValue, string propertyName);
    }

    public abstract class BasePersistent : BaseReflection, IBaseObject
    {
        private static DateTime UtcMinValue = DateTime.MinValue.ToUtcDateTime();

        #region Private Clases

        [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
        private class ReflectionHide : Attribute
        {
            private const int hide = 1;
            public override bool Match(object obj)
            {
                if (obj == this)
                    return true;

                if (obj != null && obj.GetType().Equals(typeof(int)))
                {
                    int flags = (int)obj;
                    return (flags & hide) == flags;
                }

                return false;
            }
        }

        private class GuidGenerator : IIdGenerator
        {
            public Type Type
            {
                get { return typeof(Guid); }
            }

            public object Generate()
            {
                return GuidExt.GenerateGuid();
            }
        }

        #endregion

        /// <summary>
        /// Database manager
        /// </summary>
        public readonly IDatabaseManager Manager;

        /// <summary>
        /// Id type
        /// </summary>
        public readonly Type _idType;

        /// <summary>
        /// Id generator type
        /// </summary>
        private readonly IIdGenerator IdGenerator;

        /// <summary>
        /// Guid generator
        /// </summary>
        public static readonly IIdGenerator GuidIdGenerator = new GuidGenerator();

        private DateTime _lastModified = UtcMinValue;

        /// <summary>
        /// Usado para saber si el ojeto representa un objeto vacio
        /// </summary>
        [ReflectionHide]
        public virtual bool IsEmpty 
        {
            get { return false; }
        }

        /// <summary>
        /// Usado para saber si es necesario hacer update
        /// </summary>
        [ReflectionHide]
        public bool IsDirty { get; set; }

        /// <summary>
        /// Se bloquea cuando se lee?
        /// </summary>
        [ReflectionHide]
        public bool ForUpdate { get; set; }

        /// <summary>
        /// Id generado del objeto
        /// </summary>
        private object _id;
        public object Id
        {
            get
            {
                if (_id == null && IdGenerator != null)
                    _id = IdGenerator.Generate();

                if (_id == null)
                    throw new ArgumentNullException("Id");

                return _id;
            }
            set
            {
                if (value != null)
                    _id = value.ToType(_idType);
                else
                    _id = value;
            }
        }

        [ReflectionHide]
        Type IBaseObject.IdType
        {
            get { return _idType; }
        }

        [ReflectionHide]
        Type IBaseObject.KeyType
        {
            get { return GetType(); }
        }

        [ReflectionHide]
        DateTime IBaseObject.LastModified
        {
            get { return _lastModified; }
            set { _lastModified = value; }
        }

        [ReflectionHide]
        public bool IsPersisted
        {
            get { return _lastModified != UtcMinValue; }
        }

        protected BasePersistent(IDatabaseManager manager, Type idType, IIdGenerator idGenerator)
        {
            _idType = idType;
            IdGenerator = idGenerator;
            IsDirty = true;
            ForUpdate = false;
            Manager = manager;
            Initialize();
        }

        public BasePersistent(IDatabaseManager manager)
            : this(manager, GuidIdGenerator.Type, GuidIdGenerator) { }

        public virtual void Initialize() { }

        #region Attributes + Reflection

        private MappingColumn[] ColAttributes
        {
            get
            {
                var attrs = new List<MappingColumn>();
                foreach (var prop in GetType().PropAccessors().Values)
                {
                    var colAttr = prop.GetAttributes<MappingColumn>().FirstOrDefault();
                    if (colAttr != null)
                        attrs.Add(colAttr);
                }

                return attrs.ToArray();
            }
        }

        #endregion

        #region Log Property Change

        private Action<object, object, string> _logPropertyChange = (o, n, pn) => { };

        public void BeginLog()
        {
            var logPropertyChange = this as ILogPropertyChange;
            if (logPropertyChange != null)
                _logPropertyChange = (o, n, pn) => { logPropertyChange.LogPropertyChange(o, n, pn); };
            else
                EndLog();
        }

        public void EndLog()
        {
            _logPropertyChange = (o, n, pn) => { };
        }

        protected bool Set<T>(ref T oldValue, T newValue, string propertyName, Action<T,T> afterChange = default)
        {
            if (oldValue == null && newValue == null)
                return false;

            if ((oldValue == null && newValue != null) || !oldValue.Equals(newValue))
            {
                _logPropertyChange(oldValue, newValue, propertyName);
                T savedOldValue = oldValue;
                oldValue = newValue;
                IsDirty = true;
                afterChange?.Invoke(savedOldValue, newValue);
                return true;
            }

            return false;
        }

        #endregion

        private static string GetName(string name)
        {
            int len = name.Length - 1;
            if (len >= 0)
            {
                switch (name[len])
                {
                    case '+':
                    case '-':
                        return name.Substring(0, len);
                }
            }

            return name;
        }

        private static string GetAliasName(string name)
        {
            return GetName(name).Replace('.', '$');
        }

        private static string GetFullName(string name)
        {
            return GetName(name).Replace('$', '.');
        }

        private static string GetParamName(string name)
        {
            return ":" + GetAliasName(name);
        }

        private static string GetTableName(string name)
        {
            name = GetName(name);
            return name.Substring(0, name.IndexOf('.'));
        }

        private static string GetColName(string name)
        {
            name = GetName(name);
            return name.Substring(name.IndexOf('.') + 1);
        }

        private static void AssignDefaultParameters(IDataParameterCollection parameters)
        {
            if (parameters.Contains("LANG_PK"))
                ((IDbDataParameter)parameters["LANG_PK"]).Value = NewHotelAppContext.GetLanguage();
            if (parameters.Contains("MAX_DECPREC"))
                ((IDbDataParameter)parameters["MAX_DECPREC"]).Value = 18;
        }

        public void LoadObjectFromDB(QueryReader reader)
        {
            Manager.Open();
            try
            {
                this.FillObject(reader);

                foreach (var prop in GetType().ReferenceProps())
                {
                    var key = reader[GetAliasName(prop.Key)];

                    if (key == null || key.Equals(DBNull.Value))
                    {
                        var emptyType = prop.Value.EmptyType;
                        BasePersistent obj = null;
                        if (emptyType != null && typeof(BasePersistent).IsAssignableFrom(emptyType))
                            obj = (BasePersistent)Activator.CreateInstance(emptyType, Manager);
                        prop.Value.Set(this, obj);
                    }
                    else
                    {
                        var type = prop.Value.Type;
                        var obj = (BasePersistent)Activator.CreateInstance(type, Manager);
                        var dataLoad = obj as IDataLoad;

                        if (dataLoad != null)
                        {
                            dataLoad.LoadObject(key.GetType() == typeof(byte[]) ? new Guid((byte[])key) : key);
                            prop.Value.Set(this, obj);
                        }
                        else
                            throw new ApplicationException(string.Format("{0} doesn´t implements interface IDataLoad", type));
                    }
                }
            }
            finally
            {
                IsDirty = false;
                Manager.Close();
            }
        }

        protected bool LoadObjectFromDB(object id, bool throwNoDataFoundException = true)
        {
            Manager.Open();
            try
            {
                var type = GetType();
                if (id == null)
                    throw new ArgumentException(string.Format("Invalid loading argument for {0}. Id cannot be null.", type.FullName));

                using (var command = Manager.CreateCommand("NewHotel.Load." + type.Name))
                {
                    var query = Manager.GetLoadCommandText(type);
                    var commandText = ForUpdate ?
                        string.Format(query.ToString(), Manager.TableLock, Manager.StatementLock) :
                        string.Format(query.ToString(), string.Empty, string.Empty);
                    command.CommandText = commandText;
                    var parameters = SqlUtils.ExtractParametersFromSql(command.CommandText);

                    foreach (var paramName in parameters)
                    {
                        var parameter = Manager.CreateParameter(command);
                        parameter.ParameterName = paramName;
                        parameter.Direction = ParameterDirection.Input;
                    }

                    ((IDbDataParameter)command.Parameters["ID_PK"]).Value = Manager.ConvertValueType(id);
                    AssignDefaultParameters(command.Parameters);

                    var commandParams = command.Parameters.Cast<IDataParameter>()
                        .Select(x => new KeyValuePair<string, object>(x.ParameterName, x.Value));

                    var commandStopwatch = Manager.StartCommand(command);
                    using (var reader = Manager.ExecuteReader(command))
                    {
                        try
                        {
                            var data = Manager.GetQueryReader(reader);
                            if (data.Read())
                            {
                                LoadObjectFromDB(data);

                                var timeStamp = this as ITimestamp;
                                if (timeStamp != null)
                                {
                                    var timestamp = GetLastModified(type);
                                    if (timestamp.HasValue)
                                        timeStamp.OnLoad(timestamp.Value);
                                }

                                return true;
                            }
                            else if (throwNoDataFoundException)
                                throw new NoDataFoundException(Manager, commandText, commandParams);
                            else
                                return false;
                        }
                        finally
                        {
                            reader.Close();
                            if (commandStopwatch != null)
                            {
                                commandStopwatch.Fetched();
                                Manager.EndCommand(commandStopwatch);
                            }
                        }
                    }
                }
            }
            finally
            {
                Manager.Close();
            }
        }

        private DateTime? UpdateLastModified(Type type)
        {
            var accessor = type.LastModifiedAccessor();

            DateTime? lastModified = null;
            if (accessor != null)
            {
                lastModified = DateTime.UtcNow;
                accessor.Set(this, lastModified.Value);
            }

            _lastModified = lastModified ?? DateTime.UtcNow;
            return lastModified;
        }

        private DateTime? GetLastModified(Type type)
        {
            var accessor = type.LastModifiedAccessor();

            DateTime? lastModified = null;
            if (accessor != null)
                lastModified = ((DateTime)accessor.Get(this)).ToUtcDateTime();

            return lastModified;
        }

        protected bool PersistObjectToDB(bool skipLastModifiedVerification = false)
        {
            Manager.Open();
            try
            {
                var type = GetType();
                var refIds = new Dictionary<string, object>();
                foreach (var prop in type.ReferenceProps())
                {
                    var obj = (BasePersistent)prop.Value.Get(this);
                    if (obj != null)
                    {
                        var dataSave = obj as IDataSave;
                        if (dataSave != null)
                        {
                            bool persisted = dataSave.SaveObject();
                            refIds.Add(GetName(prop.Key), persisted ? obj.Id : null);
                        }
                    }
                }

                var oldTimestamp = GetLastModified(type);
                DateTime? newTimestamp = null;
                if (oldTimestamp.HasValue)
                    newTimestamp = UpdateLastModified(type);

                if (IsDirty || refIds.Count > 0)
                {
                    foreach (var statement in Manager.GetMergeCommandText(type).Values.Reverse())
                    {
                        string timestampName = null;
                        if (!string.IsNullOrEmpty(statement.TimestampName))
                            timestampName = GetAliasName(statement.TimestampName);

                        var commandText = statement.ToString();
                        var parameters = SqlUtils.ExtractParametersFromSql(commandText).Where(x => !x.Equals(timestampName));
                        if (parameters.Count() > 0)
                        {
                            using (var command = Manager.CreateCommand("NewHotel.Persist." + type.Name))
                            {
                                command.CommandText = commandText;

                                var persistentProps = type.PersistentProps();
                                foreach (string name in parameters)
                                {
                                    object value = null;
                                    var isNullable = true;
                                    var isTimestamp = false;
                                    var columnName = GetFullName(name);

                                    if (!refIds.TryGetValue(columnName, out value))
                                    {
                                        IAccessor accessor;
                                        if (persistentProps.TryGetValue(columnName, out accessor))
                                        {
                                            value = accessor.Get(this);
                                            isNullable = accessor.Nullable;
                                            isTimestamp = accessor.IsTimestamp;
                                        }
                                    }

                                    IDbParameter parameter = null;
                                    if (value != null)
                                    {
                                        parameter = Manager.CreateParameter(command, name, Manager.ConvertValueType(value));
                                        if (isTimestamp)
                                            parameter.IsTimestamp = true;
                                    }
                                    else
                                    {
                                        if (!isNullable)
                                            throw new ApplicationException(string.Format("Error writing <null> to {0}", columnName));

                                        parameter = Manager.CreateParameter(command, name, DBNull.Value);
                                    }
                                }

                                if (oldTimestamp.HasValue && !string.IsNullOrEmpty(statement.TimestampName))
                                    Manager.CreateParameter(command, timestampName, statement.FormatTimestamp(oldTimestamp.Value));

                                var commandParams = command.Parameters.Cast<IDataParameter>()
                                    .Select(x => new KeyValuePair<string, object>(x.ParameterName, x.Value));

                                if (Manager.ExecuteNonQuery(command) == 0 && oldTimestamp.HasValue && !string.IsNullOrEmpty(statement.TimestampName) && !skipLastModifiedVerification)
                                    throw new StaleDataException(Manager, commandText, commandParams);

                                if (newTimestamp.HasValue)
                                {
                                    var timeStamp = this as ITimestamp;
                                    if (timeStamp != null)
                                        timeStamp.OnPersist(newTimestamp.Value);
                                }
                            }
                        }
                    }
                }
            }
            finally
            {
                IsDirty = false;
                Manager.Close();
            }

            return true;
        }

        private static bool DeleteObjectFromDB(IDatabaseManager manager, Type type, object id)
        {
            if (id == null || !id.GetType().IsValueType)
                throw new ApplicationException("Invalid delete id value");

            manager.Open();
            try
            {
                id = manager.ConvertValueType(id);
                int count = 0;
                foreach (var statement in manager.GetDeleteCommandText(type).Values)
                {
                    using (var command = manager.CreateCommand("NewHotel.Delete." + type.Name))
                    {
                        command.CommandText = statement.ToString();
                        var parameters = SqlUtils.ExtractParametersFromSql(command.CommandText);

                        var parameter = manager.CreateParameter(command);
                        parameter.ParameterName = parameters[0];
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = id;

                        manager.ExecuteNonQuery(command);
                        count++;
                    }
                }

                return true;
            }
            finally
            {
                manager.Close();
            }
        }

        public static bool DeleteObjectFromDB<T>(IDatabaseManager manager, params object[] ids)
            where T : BasePersistent
        {
            var deleted = true;
            foreach (var id in ids)
                deleted = deleted && DeleteObjectFromDB(manager, typeof(T), id);
            return deleted;
        }

        protected bool DeleteObjectFromDB()
        {
            var type = GetType();
            return DeleteObjectFromDB(Manager, type, type.IdPropAccessor().Get(this));
        }

        private static string TimeStampKey = "TIMESTAMP_CACHE";
        private static object TimeStampSync = new object();
        private static IDictionary<string, DateTime> GetTimeStamps(Type type)
        {
            object value = null;
            IDictionary<Type, IDictionary<string, DateTime>> _timestamps;
            lock (TimeStampSync)
            {
                if (!NewHotelAppContext.OperationData.TryGetValue(TimeStampKey, out value))
                {
                    _timestamps = new Dictionary<Type, IDictionary<string, DateTime>>();
                    NewHotelAppContext.OperationData.Add(TimeStampKey, _timestamps);
                }
                else
                    _timestamps = (IDictionary<Type, IDictionary<string, DateTime>>)value;
            }

            IDictionary<string, DateTime> timestamps;
            if (!_timestamps.TryGetValue(type, out timestamps))
            {
                timestamps = new Dictionary<string, DateTime>();
                _timestamps.Add(type, timestamps);
            }

            return timestamps;
        }

        public static DateTime TimestampFromCache<T>(object id)
            where T : BasePersistent
        {
            var dict = GetTimeStamps(typeof(T));
            lock (dict)
            {
                DateTime timestamp;
                if (!dict.TryGetValue(id.ToString(), out timestamp))
                    timestamp = UtcMinValue;
                return timestamp;
            }
        }

        public static void TimestampToCache<T>(object id, DateTime timestamp)
            where T : BasePersistent
        {
            var dict = GetTimeStamps(typeof(T));
            lock (dict)
            {
                if (!dict.ContainsKey(id.ToString()))
                    dict[id.ToString()] = timestamp.ToUtcDateTime();
            }
        }

        public static bool Lock<T>(IDatabaseManager manager)
        {
            manager.Open();
            try
            {
                var type = typeof(T);
                foreach (var statement in manager.GetLockCommandText(type).Values)
                {
                    using (var command = manager.CreateCommand("NewHotel.Lock." + type.Name))
                    {
                        command.CommandText = statement.ToString();

                        try
                        {
                            manager.ExecuteNonQuery(command);
                        }
                        catch (ResourceLockedException)
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            finally
            {
                manager.Close();
            }
        }

         public static IPersistentList<T> GetList<T>(IDatabaseManager manager,
            params Expression<Func<T, object>>[] exprs)
            where T : BasePersistent
        {
            var query = manager.GetQueryCommandText(typeof(T), exprs);
            return new PersistentList<T>(manager, query);
        }

        public static IPersistentList<T> GetList<T>(IDatabaseManager manager)
            where T : BasePersistent
        {
            return GetList<T>(manager, null);
        }

        public static IPersistentList<T> GetListById<T>(IDatabaseManager manager, params object[] ids)
            where T : BasePersistent
        {
            var list = new List<T>();

            if (ids.Length > 0)
            {
                int index = 0;
                while (index < ids.Length)
                {
                    var length = ids.Length - index;
                    var count = length > 1000 ? 1000 : length;

                    var values = new object[count];
                    Array.Copy(ids, index, values, 0, count);

                    var query = manager.GetQueryCommandText(typeof(T),
                        new Expression<Func<T, object>>[] { x => x.Id }, values.Length);

                    list.AddRange(new PersistentList<T>(manager, query).Execute(values));

                    index += count;
                }
            }

            return new PersistentList<T>(manager, list);
        }

        [IgnoreDataMember]
        [ReflectionHide]
        public IReflectionDictionary Values
        {
            get { return this.GetReflectionValues<BasePersistent>(); }
            set { this.SetReflectionValues<BasePersistent>(value); }
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj.GetType() == GetType())
            {
                var basePersistent = obj as BasePersistent;
                return basePersistent != null && basePersistent.Id.Equals(Id);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Id.ToString().GetHashCode();
        }

        public override string ToString()
        {
            return GetType().Name + "(" + (Id == null ? string.Empty : Id.ToString()) + ")";
        }
    }

    public abstract class BasePersistent<T> : BasePersistent
    {
        public BasePersistent(IDatabaseManager manager)
            : base(manager, typeof(T), null) { }

        protected BasePersistent(IDatabaseManager manager, IIdGenerator idGenerator)
            : base(manager, typeof(T), idGenerator) { }
    }

    public abstract class BasePersistent<T, G> : BasePersistent
        where G : IIdGenerator
    {
        public BasePersistent(IDatabaseManager manager)
            : base(manager, typeof(T), Activator.CreateInstance<G>()) { }
    }
}
