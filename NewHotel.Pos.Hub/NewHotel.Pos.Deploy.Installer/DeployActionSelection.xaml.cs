﻿using NewHotel.Pos.Deploy.Installer.Cashier;
using NewHotel.Pos.Deploy.Installer.Hub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewHotel.Pos.Deploy.Installer
{
    /// <summary>
    /// Interaction logic for DeployActionSelection.xaml
    /// </summary>
    public partial class DeployActionSelection : UserControl
    {
        public DeployActionSelection()
        {
            InitializeComponent();
        }

        private void Cashier_InstallClick(object sender, RoutedEventArgs e)
        {
            DeployCashierInstall cashierConfig = new DeployCashierInstall();
            MainPage.Container.Content = cashierConfig;
        }

        private void Hub_InstallClick(object sender, RoutedEventArgs e)
        {
            DeployHubInstall hubConfig = new DeployHubInstall();
            MainPage.Container.Content = hubConfig;
        }
        
    }
}
