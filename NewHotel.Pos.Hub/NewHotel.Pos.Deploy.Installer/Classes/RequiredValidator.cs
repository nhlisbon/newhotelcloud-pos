﻿using System;
using System.Windows.Controls;
using System.Globalization;

namespace NewHotel.Pos
{
    public class RequiredField : ValidationRule
    {
        private String _errorMessage = String.Empty;
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var str = value as string;

            if (String.IsNullOrEmpty(str))
            {
                return new ValidationResult(false, this.ErrorMessage);
            }

            return new ValidationResult(true, null);
        }
    }
}
