﻿using Ionic.Zip;
using NewHotel.Pos.Hub.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace NewHotel.Pos.Deploy.Installer.Cashier
{
    /// <summary>
    /// Interaction logic for DeployCashierConfig.xaml
    /// </summary>
    public partial class DeployCashierInstall : UserControl
    {
        public DeployCashierInstall()
        {
            InitializeComponent();
            CashierConfigurationClass datacontext = new CashierConfigurationClass();
            this.ViewModel = datacontext;
        }

        public CashierConfigurationClass ViewModel
        {
            get { return this.DataContext as CashierConfigurationClass; }
            set { this.DataContext = value; }
        }

        private void btnInstallFolderBrowse_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.SelectedPath = txtInstallFolder.Text;
            var result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                txtInstallFolder.Text = dialog.SelectedPath;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void btnInstall_Click(object sender, RoutedEventArgs e)
        {
            Install();    
        }

        private void Install()
        {
            BackgroundWorker worker = new BackgroundWorker() { WorkerSupportsCancellation = true, WorkerReportsProgress = true };
            worker.DoWork += Worker_DoWork;
            worker.ProgressChanged += Worker_ProgressChanged;

            worker.RunWorkerCompleted += (s, ev) =>
            {
                if (ev.Result != null)
                {
                    System.Windows.Forms.MessageBox.Show(ev.Result.ToString());
                    this.progressBar.Value = 0;
                    this.txtStatus.Text = "error";
                    this.progressBarVisual.IsIndeterminate = false;
                }
                else
                {
                    this.progressBar.Value = 0;
                    this.progressBarVisual.IsIndeterminate = false;
                    System.Windows.Forms.MessageBox.Show("Installation Complete");
                }
            };

            this.progressBar.Value = 0;
            this.progressBar.Maximum = 10;
            this.progressBarVisual.IsIndeterminate = true;

            worker.RunWorkerAsync(ViewModel);
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            CashierConfigurationClass config = e.Argument as CashierConfigurationClass;

            #region Downloading Files
            {
                ((BackgroundWorker)sender).ReportProgress(1, "Downloading setup files...");


                NetTcpBinding binding = new NetTcpBinding() { MaxReceivedMessageSize = 67108864, Security = new NetTcpSecurity() { Mode = SecurityMode.None } };
                EndpointAddress address = new EndpointAddress(config.CashierService);

                ChannelFactory<IService> factory = new ChannelFactory<IService>(binding, address);
                factory.Endpoint.Address = new EndpointAddress(config.CashierService);
                IService client = factory.CreateChannel();
                Stream stream = null;

                using (new OperationContextScope((IContextChannel)client))
                {
                    var versionHeader = MessageHeader.CreateHeader(HubContextClientBehavior.NEWHOTEL_HUB_CLIENT_VERSION, string.Empty, HubContextClientBehavior.NEWHOTEL_HUB_INSTALL_TOKEN);
                    OperationContext.Current.OutgoingMessageHeaders.Add(versionHeader);
                    try
                    {
                        stream = client.GetCashierFiles();
                    }
                    catch (Exception ex)
                    {
                        e.Result = ex.Message;
                        return;
                    }
                }
                

                string appFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "NewHotel");
                if (Directory.Exists(appFolder)) Directory.CreateDirectory(appFolder);
                string localFile = Path.Combine(appFolder, "pos.bin");

                FileStream writer = File.Create(localFile);
                stream.CopyTo(writer);
                writer.Close();
            }
            #endregion
            #region Extract Files
            {
                ((BackgroundWorker)sender).ReportProgress(1, "Extracting setup files...");

                string appFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "NewHotel");
                if (!Directory.Exists(config.InstallationFolder)) Directory.CreateDirectory(config.InstallationFolder);

                ZipFile zip = ZipFile.Read(System.IO.Path.Combine(appFolder, "pos.bin"));
                zip.ExtractAll(config.InstallationFolder, ExtractExistingFileAction.OverwriteSilently);
                zip.Dispose();
            }
            #endregion
            #region Applying Configurations
            {
                ((BackgroundWorker)sender).ReportProgress(1, "Applying configuration...");

                ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap();
                configFileMap.ExeConfigFilename = Path.Combine(config.InstallationFolder, "Pos\\NewHotel.Pos.exe.config");
                Configuration appConfig = ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);

                //Settings
                var section = (AppSettingsSection)appConfig.GetSection("appSettings");
                section.Settings["SignalRServer"].Value = config.SignalR;
                section.Settings["SignalR"].Value = String.IsNullOrEmpty(config.SignalR) ? "False" : "True";
                appConfig.Save();

                //Cashier Address
                StreamReader reader = new StreamReader(configFileMap.ExeConfigFilename);
                string cashierConfigContent = reader.ReadToEnd();
                reader.Close();

                //Replace
                cashierConfigContent = cashierConfigContent.Replace("[SERVICE]", config.CashierService);
                
                //Save File
                StreamWriter writer = new StreamWriter(configFileMap.ExeConfigFilename, false);
                writer.Write(cashierConfigContent);
                writer.Close();


            }
            #endregion
        }
        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.progressBar.Value += 1;
            this.txtStatus.Text = e.UserState.ToString();            
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            DeployActionSelection actionSelection = new Installer.DeployActionSelection();
            MainPage.Container.Content = actionSelection;
        }
    }
}
