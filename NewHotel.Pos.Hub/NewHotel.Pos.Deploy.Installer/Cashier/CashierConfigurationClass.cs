﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Deploy.Installer
{
    [CustomValidation(typeof(CashierConfigurationClass), "ValidateCashierConfigurationClass")]
    public class CashierConfigurationClass : BaseObject
    {
        private string _cashierService;
        private string _signalR;
        private string _installationFolder;

        public CashierConfigurationClass()
        {
            CashierService = "net.tcp://localhost:13301/NewHotel.Pos.Hub.Cashier/Service/";
            SignalR = "http://localhost:8081/";
        }

        public string CashierService { get { return _cashierService; } set { Set(ref _cashierService, value, "CashierService"); } }
        public string SignalR { get { return _signalR; } set { Set(ref _signalR, value, "SignalR"); } }
        public string InstallationFolder { get { return _installationFolder; } set { Set(ref _installationFolder, value, "InstallationFolder"); } }

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateCashierConfigurationClass(CashierConfigurationClass obj)
        {
            if (String.IsNullOrEmpty(obj.CashierService)) return new System.ComponentModel.DataAnnotations.ValidationResult("Cashier service is missing");
            if (String.IsNullOrEmpty(obj.SignalR)) return new System.ComponentModel.DataAnnotations.ValidationResult("Signal R address is missing");
            if (String.IsNullOrEmpty(obj.InstallationFolder)) return new System.ComponentModel.DataAnnotations.ValidationResult("Installation folder is missing");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;

        }
    }
}
