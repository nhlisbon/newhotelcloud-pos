﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace NewHotel.Pos.Deploy.Installer.CloudService {
    using System.Diagnostics;
    using System;
    using System.Xml.Serialization;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System.Web.Services;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.9032.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="NewHotelBackOfficeSoap", Namespace="https://NewHotel/Web/Services/")]
    public partial class NewHotelBackOffice : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback GetHotelsOperationCompleted;
        
        private System.Threading.SendOrPostCallback UpdateDataOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetLicenseOperationCompleted;
        
        private System.Threading.SendOrPostCallback UpdateLicenseOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public NewHotelBackOffice() {
            this.Url = global::NewHotel.Pos.Deploy.Installer.Properties.Settings.Default.NewHotel_Pos_Deploy_Installer_CloudService_NewHotelBackOffice;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event GetHotelsCompletedEventHandler GetHotelsCompleted;
        
        /// <remarks/>
        public event UpdateDataCompletedEventHandler UpdateDataCompleted;
        
        /// <remarks/>
        public event GetLicenseCompletedEventHandler GetLicenseCompleted;
        
        /// <remarks/>
        public event UpdateLicenseCompletedEventHandler UpdateLicenseCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("https://NewHotel/Web/Services/GetHotels", RequestNamespace="https://NewHotel/Web/Services/", ResponseNamespace="https://NewHotel/Web/Services/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public HotelCollectionResponse GetHotels(BaseRequest request) {
            object[] results = this.Invoke("GetHotels", new object[] {
                        request});
            return ((HotelCollectionResponse)(results[0]));
        }
        
        /// <remarks/>
        public void GetHotelsAsync(BaseRequest request) {
            this.GetHotelsAsync(request, null);
        }
        
        /// <remarks/>
        public void GetHotelsAsync(BaseRequest request, object userState) {
            if ((this.GetHotelsOperationCompleted == null)) {
                this.GetHotelsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetHotelsOperationCompleted);
            }
            this.InvokeAsync("GetHotels", new object[] {
                        request}, this.GetHotelsOperationCompleted, userState);
        }
        
        private void OnGetHotelsOperationCompleted(object arg) {
            if ((this.GetHotelsCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetHotelsCompleted(this, new GetHotelsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("https://NewHotel/Web/Services/UpdateData", RequestNamespace="https://NewHotel/Web/Services/", ResponseNamespace="https://NewHotel/Web/Services/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public BaseResponse UpdateData(BackOfficeRequest request) {
            object[] results = this.Invoke("UpdateData", new object[] {
                        request});
            return ((BaseResponse)(results[0]));
        }
        
        /// <remarks/>
        public void UpdateDataAsync(BackOfficeRequest request) {
            this.UpdateDataAsync(request, null);
        }
        
        /// <remarks/>
        public void UpdateDataAsync(BackOfficeRequest request, object userState) {
            if ((this.UpdateDataOperationCompleted == null)) {
                this.UpdateDataOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUpdateDataOperationCompleted);
            }
            this.InvokeAsync("UpdateData", new object[] {
                        request}, this.UpdateDataOperationCompleted, userState);
        }
        
        private void OnUpdateDataOperationCompleted(object arg) {
            if ((this.UpdateDataCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UpdateDataCompleted(this, new UpdateDataCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("https://NewHotel/Web/Services/GetLicense", RequestNamespace="https://NewHotel/Web/Services/", ResponseNamespace="https://NewHotel/Web/Services/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public HotelLicenseResponse GetLicense(BaseRequest request) {
            object[] results = this.Invoke("GetLicense", new object[] {
                        request});
            return ((HotelLicenseResponse)(results[0]));
        }
        
        /// <remarks/>
        public void GetLicenseAsync(BaseRequest request) {
            this.GetLicenseAsync(request, null);
        }
        
        /// <remarks/>
        public void GetLicenseAsync(BaseRequest request, object userState) {
            if ((this.GetLicenseOperationCompleted == null)) {
                this.GetLicenseOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetLicenseOperationCompleted);
            }
            this.InvokeAsync("GetLicense", new object[] {
                        request}, this.GetLicenseOperationCompleted, userState);
        }
        
        private void OnGetLicenseOperationCompleted(object arg) {
            if ((this.GetLicenseCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetLicenseCompleted(this, new GetLicenseCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("https://NewHotel/Web/Services/UpdateLicense", RequestNamespace="https://NewHotel/Web/Services/", ResponseNamespace="https://NewHotel/Web/Services/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public BaseResponse UpdateLicense(BackOfficeLicenseRequest request) {
            object[] results = this.Invoke("UpdateLicense", new object[] {
                        request});
            return ((BaseResponse)(results[0]));
        }
        
        /// <remarks/>
        public void UpdateLicenseAsync(BackOfficeLicenseRequest request) {
            this.UpdateLicenseAsync(request, null);
        }
        
        /// <remarks/>
        public void UpdateLicenseAsync(BackOfficeLicenseRequest request, object userState) {
            if ((this.UpdateLicenseOperationCompleted == null)) {
                this.UpdateLicenseOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUpdateLicenseOperationCompleted);
            }
            this.InvokeAsync("UpdateLicense", new object[] {
                        request}, this.UpdateLicenseOperationCompleted, userState);
        }
        
        private void OnUpdateLicenseOperationCompleted(object arg) {
            if ((this.UpdateLicenseCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UpdateLicenseCompleted(this, new UpdateLicenseCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BackOfficeLicenseRequest))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BackOfficeRequest))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.9032.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://NewHotel/Web/Services/")]
    public partial class BaseRequest {
        
        private string requestIDField;
        
        private string userIDField;
        
        private string passwordField;
        
        private string publicKeyField;
        
        private string cultureField;
        
        private System.DateTime clientDateTimeField;
        
        private string hotelIDField;
        
        /// <remarks/>
        public string RequestID {
            get {
                return this.requestIDField;
            }
            set {
                this.requestIDField = value;
            }
        }
        
        /// <remarks/>
        public string UserID {
            get {
                return this.userIDField;
            }
            set {
                this.userIDField = value;
            }
        }
        
        /// <remarks/>
        public string Password {
            get {
                return this.passwordField;
            }
            set {
                this.passwordField = value;
            }
        }
        
        /// <remarks/>
        public string PublicKey {
            get {
                return this.publicKeyField;
            }
            set {
                this.publicKeyField = value;
            }
        }
        
        /// <remarks/>
        public string Culture {
            get {
                return this.cultureField;
            }
            set {
                this.cultureField = value;
            }
        }
        
        /// <remarks/>
        public System.DateTime ClientDateTime {
            get {
                return this.clientDateTimeField;
            }
            set {
                this.clientDateTimeField = value;
            }
        }
        
        /// <remarks/>
        public string HotelID {
            get {
                return this.hotelIDField;
            }
            set {
                this.hotelIDField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.9032.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://NewHotel/Web/Services/")]
    public partial class HotelResponse {
        
        private System.Guid idField;
        
        private string descriptionField;
        
        private string licenseCodeField;
        
        private string expireDateField;
        
        private string startDateField;
        
        private string hotelCodeField;
        
        private int daysAlertField;
        
        private int daysSuspendField;
        
        private string countryField;
        
        private string contactNameField;
        
        private string contactLastNameField;
        
        private string contactEmailField;
        
        private string contactPhoneField;
        
        private string contactCountryField;
        
        private string contactLocationField;
        
        private string contactAddressField;
        
        private string nightAuditorDateField;
        
        private string licenseTypeField;
        
        private string roomsField;
        
        /// <remarks/>
        public System.Guid Id {
            get {
                return this.idField;
            }
            set {
                this.idField = value;
            }
        }
        
        /// <remarks/>
        public string Description {
            get {
                return this.descriptionField;
            }
            set {
                this.descriptionField = value;
            }
        }
        
        /// <remarks/>
        public string LicenseCode {
            get {
                return this.licenseCodeField;
            }
            set {
                this.licenseCodeField = value;
            }
        }
        
        /// <remarks/>
        public string ExpireDate {
            get {
                return this.expireDateField;
            }
            set {
                this.expireDateField = value;
            }
        }
        
        /// <remarks/>
        public string StartDate {
            get {
                return this.startDateField;
            }
            set {
                this.startDateField = value;
            }
        }
        
        /// <remarks/>
        public string HotelCode {
            get {
                return this.hotelCodeField;
            }
            set {
                this.hotelCodeField = value;
            }
        }
        
        /// <remarks/>
        public int DaysAlert {
            get {
                return this.daysAlertField;
            }
            set {
                this.daysAlertField = value;
            }
        }
        
        /// <remarks/>
        public int DaysSuspend {
            get {
                return this.daysSuspendField;
            }
            set {
                this.daysSuspendField = value;
            }
        }
        
        /// <remarks/>
        public string Country {
            get {
                return this.countryField;
            }
            set {
                this.countryField = value;
            }
        }
        
        /// <remarks/>
        public string ContactName {
            get {
                return this.contactNameField;
            }
            set {
                this.contactNameField = value;
            }
        }
        
        /// <remarks/>
        public string ContactLastName {
            get {
                return this.contactLastNameField;
            }
            set {
                this.contactLastNameField = value;
            }
        }
        
        /// <remarks/>
        public string ContactEmail {
            get {
                return this.contactEmailField;
            }
            set {
                this.contactEmailField = value;
            }
        }
        
        /// <remarks/>
        public string ContactPhone {
            get {
                return this.contactPhoneField;
            }
            set {
                this.contactPhoneField = value;
            }
        }
        
        /// <remarks/>
        public string ContactCountry {
            get {
                return this.contactCountryField;
            }
            set {
                this.contactCountryField = value;
            }
        }
        
        /// <remarks/>
        public string ContactLocation {
            get {
                return this.contactLocationField;
            }
            set {
                this.contactLocationField = value;
            }
        }
        
        /// <remarks/>
        public string ContactAddress {
            get {
                return this.contactAddressField;
            }
            set {
                this.contactAddressField = value;
            }
        }
        
        /// <remarks/>
        public string NightAuditorDate {
            get {
                return this.nightAuditorDateField;
            }
            set {
                this.nightAuditorDateField = value;
            }
        }
        
        /// <remarks/>
        public string LicenseType {
            get {
                return this.licenseTypeField;
            }
            set {
                this.licenseTypeField = value;
            }
        }
        
        /// <remarks/>
        public string Rooms {
            get {
                return this.roomsField;
            }
            set {
                this.roomsField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.9032.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://NewHotel/Web/Services/")]
    public partial class Error {
        
        private short codeField;
        
        private short subCodeField;
        
        /// <remarks/>
        public short Code {
            get {
                return this.codeField;
            }
            set {
                this.codeField = value;
            }
        }
        
        /// <remarks/>
        public short SubCode {
            get {
                return this.subCodeField;
            }
            set {
                this.subCodeField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.9032.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://NewHotel/Web/Services/")]
    public partial class ErrorResponse {
        
        private Error idField;
        
        private string messageField;
        
        /// <remarks/>
        public Error ID {
            get {
                return this.idField;
            }
            set {
                this.idField = value;
            }
        }
        
        /// <remarks/>
        public string Message {
            get {
                return this.messageField;
            }
            set {
                this.messageField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(HotelLicenseResponse))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(HotelCollectionResponse))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.9032.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://NewHotel/Web/Services/")]
    public partial class BaseResponse {
        
        private string requestIDField;
        
        private ErrorResponse[] errorsField;
        
        /// <remarks/>
        public string RequestID {
            get {
                return this.requestIDField;
            }
            set {
                this.requestIDField = value;
            }
        }
        
        /// <remarks/>
        public ErrorResponse[] Errors {
            get {
                return this.errorsField;
            }
            set {
                this.errorsField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.9032.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://NewHotel/Web/Services/")]
    public partial class HotelLicenseResponse : BaseResponse {
        
        private string licenseField;
        
        /// <remarks/>
        public string License {
            get {
                return this.licenseField;
            }
            set {
                this.licenseField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.9032.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://NewHotel/Web/Services/")]
    public partial class HotelCollectionResponse : BaseResponse {
        
        private HotelResponse[] hotelsField;
        
        /// <remarks/>
        public HotelResponse[] Hotels {
            get {
                return this.hotelsField;
            }
            set {
                this.hotelsField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.9032.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://NewHotel/Web/Services/")]
    public partial class BackOfficeLicenseRequest : BaseRequest {
        
        private string licenseField;
        
        /// <remarks/>
        public string License {
            get {
                return this.licenseField;
            }
            set {
                this.licenseField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.9032.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://NewHotel/Web/Services/")]
    public partial class BackOfficeRequest : BaseRequest {
        
        private string hotelCodeField;
        
        private string dateField;
        
        private int daysAlertField;
        
        private int daysSuspendField;
        
        /// <remarks/>
        public string HotelCode {
            get {
                return this.hotelCodeField;
            }
            set {
                this.hotelCodeField = value;
            }
        }
        
        /// <remarks/>
        public string Date {
            get {
                return this.dateField;
            }
            set {
                this.dateField = value;
            }
        }
        
        /// <remarks/>
        public int DaysAlert {
            get {
                return this.daysAlertField;
            }
            set {
                this.daysAlertField = value;
            }
        }
        
        /// <remarks/>
        public int DaysSuspend {
            get {
                return this.daysSuspendField;
            }
            set {
                this.daysSuspendField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.9032.0")]
    public delegate void GetHotelsCompletedEventHandler(object sender, GetHotelsCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.9032.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetHotelsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetHotelsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public HotelCollectionResponse Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((HotelCollectionResponse)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.9032.0")]
    public delegate void UpdateDataCompletedEventHandler(object sender, UpdateDataCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.9032.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class UpdateDataCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal UpdateDataCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public BaseResponse Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((BaseResponse)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.9032.0")]
    public delegate void GetLicenseCompletedEventHandler(object sender, GetLicenseCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.9032.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetLicenseCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetLicenseCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public HotelLicenseResponse Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((HotelLicenseResponse)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.9032.0")]
    public delegate void UpdateLicenseCompletedEventHandler(object sender, UpdateLicenseCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.9032.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class UpdateLicenseCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal UpdateLicenseCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public BaseResponse Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((BaseResponse)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591