﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Security.Principal;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Diagnostics;
using Microsoft.Win32;
using System.Security.AccessControl;
using System.Threading;

namespace NewHotel.Pos.Deploy.Installer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string FooterText
        {
            get { return (string)GetValue(FooterTextProperty); }
            set { SetValue(FooterTextProperty, value); }
        }

        public static readonly DependencyProperty FooterTextProperty =
            DependencyProperty.Register("FooterText", typeof(string), typeof(MainWindow));


        string path = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "POSInstaller20121221");

        public MainWindow()
        {
            InitializeComponent();

            if (!IsAnAdministrator())
            {
                System.Windows.Forms.MessageBox.Show("You need administrative rights to install Pos", "Error");
                this.Close();
            }

            ServicePointManager.ServerCertificateValidationCallback = (s, cer, ch, e) => { return true; };

            var args = Environment.GetCommandLineArgs();

            if (args.Count() > 1 && !string.IsNullOrEmpty(args[1]) && args[1] == "remove")
            {// Tenemos que borrar el instalador 
                headerTextBlock.Text = "The installation is ending";
                content.Content = Resources["RemoveData"];
                RemoveAuxiliarData();
            }// Tenemos que borrar el instalador
            else
            {// Tenemos que instalar 
                DownloadProgressBar = System.Windows.Visibility.Hidden;
                content.Content = Resources["DownloadData"];
                content.DataContext = this;
            }// Tenemos que instalar

            FooterText = "NewHotel Software S.A.\nwww.newhotel.com";
        }

        private void RemoveAuxiliarData()
        {
            try
            {
                Thread.Sleep(2000);
                RemoveRegisters();
                Directory.Delete(System.IO.Path.Combine(path), true);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                Close();
            }
        }

        private bool RemoveRegisters()
        {
            try
            {
                RegistryKey key = Registry.LocalMachine;
                key = key.OpenSubKey("Software");
                key = key.OpenSubKey("NewHotel", true);
                key.DeleteSubKeyTree("PosInstaller");
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool IsAnAdministrator()
        {
            WindowsIdentity identity =
               WindowsIdentity.GetCurrent();
            WindowsPrincipal principal =
               new WindowsPrincipal(identity);
            return principal.IsInRole
               (WindowsBuiltInRole.Administrator);
        }

        private string serverUri = "";

        private void demoButton_Click(object sender, RoutedEventArgs e)
        {
            serverUri = @"https://demo.newhotelcloud.com/NewHotel2011/LiveUpdate/Pos/";
            Install("https://demo.newhotelcloud.com/");
        }

        private void usButton_Click(object sender, RoutedEventArgs e)
        {
            serverUri = @"https://us.newhotelcloud.com/NewHotel2011/LiveUpdate/Pos/";
            Install("https://us.newhotelcloud.com/");
        }

        private void brButton_Click(object sender, RoutedEventArgs e)
        {
            serverUri = @"https://br.newhotelcloud.com/NewHotel2011/LiveUpdate/Pos/";
            Install("https://br.newhotelcloud.com/");
        }

        private void euButton_Click(object sender, RoutedEventArgs e)
        {
            serverUri = @"https://eu.newhotelcloud.com/NewHotel2011/LiveUpdate/Pos/";
            Install("https://eu.newhotelcloud.com/");
        }

        private void ukButton_Click(object sender, RoutedEventArgs e)
        {
            serverUri = @"https://uk.newhotelcloud.com/NewHotel2011/LiveUpdate/Pos/";
            Install("https://uk.newhotelcloud.com/");
        }

        private void nlButton_Click(object sender, RoutedEventArgs e)
        {
            serverUri = @"https://nl.newhotelcloud.com/NewHotel2011/LiveUpdate/Pos/";
            Install("https://nl.newhotelcloud.com/");
        }

        private void DownloadInstaller()
        {
            try { Directory.Delete(path, true); }
            catch { }

            Directory.CreateDirectory(path);
            DirectoryInfo info = new DirectoryInfo(path);
            info.Attributes |= FileAttributes.Hidden;

            Directory.CreateDirectory(System.IO.Path.Combine(path, "libs"));
            Directory.CreateDirectory(System.IO.Path.Combine(path, "libs", "32bits"));

            WebClient client = new WebClient();

            DownloadFile(client, "Ionic.Zip.dll", System.IO.Path.Combine("libs"));

            // 32bits
            DownloadFile(client, "System.Data.SQLite.dll", System.IO.Path.Combine("libs", "32bits"), System.IO.Path.Combine("libs", "32bits"));
            DownloadFile(client, "System.Data.SQLite.Linq.dll", System.IO.Path.Combine("libs", "32bits"), System.IO.Path.Combine("libs", "32bits"));
            DownloadFile(client, "SQLite.Designer.dll", System.IO.Path.Combine("libs", "32bits"), System.IO.Path.Combine("libs", "32bits"));

            // exe
            DownloadFile(client, "POS_Installer.exe");
            // config
            DownloadFile(client, "POS_Installer.exe.exe");
            File.Move(System.IO.Path.Combine(path, "POS_Installer.exe.exe"), System.IO.Path.Combine(path, "POS_Installer.exe.config"));
            // initial application (zip)
            DownloadFile(client, "setup.bin");
        }

        public void DownloadFile(WebClient client, string filename)
        {
            string folderURL = System.IO.Path.Combine(serverUri, "Installer") + "/";
            string targetURL = path;
            client.DownloadFile(folderURL + filename, System.IO.Path.Combine(targetURL, filename));
        }

        public void DownloadFile(WebClient client, string filename, string fromDir)
        {
            string serverURL = System.IO.Path.Combine(serverUri, fromDir, filename);
            string targetURL = path;
            client.DownloadFile(serverURL, System.IO.Path.Combine(targetURL, filename));
        }

        public void DownloadFile(WebClient client, string filename, string fromDir, string toDir)
        {
            string serverURL = System.IO.Path.Combine(serverUri, fromDir, filename);
            string targetURL = path;
            client.DownloadFile(serverURL, System.IO.Path.Combine(targetURL, toDir, filename));
        }

        private void Install(string server)
        {
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += (s1, ev) =>
            {
                try
                {
                    DownloadInstaller();
                    ev.Result = null;
                }
                catch (Exception ex)
                {
                    ev.Result = ex;
                }
            };

            bw.RunWorkerCompleted += (s1, ev) =>
            {
                if (ev.Result == null)
                {
                    string arg = string.Format("\"{0}\" {1}", Environment.GetCommandLineArgs()[0], server);
                    Process.Start(System.IO.Path.Combine(path, "POS_Installer.exe"), arg);
                    Close();
                }
                else
                {
                    MessageBox.Show(this, "There was an error dowloading files: " + (ev.Result as Exception).Message);
                    DownloadProgressBar = System.Windows.Visibility.Hidden;
                }
            };

            DownloadProgressBar = System.Windows.Visibility.Visible;

            bw.RunWorkerAsync();
        }

        public Visibility DownloadProgressBar
        {
            get { return (Visibility)GetValue(DownloadProgressBarProperty); }
            set { SetValue(DownloadProgressBarProperty, value); }
        }

        public static readonly DependencyProperty DownloadProgressBarProperty =
            DependencyProperty.Register("DownloadProgressBar", typeof(Visibility), typeof(MainWindow));
    }
}
