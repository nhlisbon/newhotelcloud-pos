﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NewHotel.Core;

namespace NewHotel.Pos.Deploy.Installer
{
    public sealed class OracleScriptParser : ScriptParser
    {
        private const string CreateTriggerToken = "CREATE OR REPLACE TRIGGER";
        private const string CreateFuncToken = "CREATE OR REPLACE FUNCTION";
        private const string CreateProcToken = "CREATE OR REPLACE PROCEDURE";
        private const string DeclareToken = "DECLARE";
        private const string BeginToken = "BEGIN";
        private const string EndToken = "END;\r\n/";
        private const string ReturnLineFeed = "\r\n";
        private const string SingleCommentLine = "--";
        private const string MultiLineCommentStart = "/*";
        private const string MultiLineCommentEnd = "*/";
        private const char SeparatorToken = ';';
        private const char Space = ' ';
        private const char LiteralTextDelimiter = '\'';

        protected override string ResourcePath
        {
            get { return "NewHotel.Pos.Deploy.Installer.Database.Scripts"; }
        }

        private void ExtractCommand(string s, int prgIndex, int prgCount)
        {
            if (s.Length > 0)
            {
                int start = 0;
                int index;
                do
                {
                    var cmd = FindSeparator(s, start, out index);
                    if (!string.IsNullOrEmpty(cmd))
                    {
                        ExecCommand(cmd, prgIndex + index, prgCount);
                        start = index;
                    }
                    else
                        break;
                } while (true);
            }
        }

        private void ExecCommand(string commandText, int index, int count)
        {
            if (!string.IsNullOrEmpty(commandText))
            {
                manager.BeginTransaction();
                try
                {
                    var command = manager.CreateCommand("ScriptParser.ExecCommand");
                    commandText = commandText.TrimStart(ReturnLineFeed[0], ReturnLineFeed[1]);
                    commandText = commandText.TrimEnd(ReturnLineFeed[0], ReturnLineFeed[1]);
                    commandText = commandText.Replace(ReturnLineFeed, ReturnLineFeed[1].ToString());
                    commandText = commandText.Trim();

                    command.CommandText = commandText;
                    NotifyExecute(new ScriptParserEventArgs(index, count, commandText));

                    command.ExecuteNonQuery();
                    manager.CommitTransaction();
                }
                catch (Exception ex)
                {
                    manager.RollbackTransaction();
                    throw new DatabaseException(manager, ex, commandText, new KeyValuePair<string, object>[0]);
                }
            }
        }

        private static string FindSeparator(string s, int start, out int last)
        {
            var lastIndex = s.Length - 1;
            var result = string.Empty;
            last = start;
            while (true)
            {
                var index = s.IndexOfAny(new char[] { MultiLineCommentStart[0], SingleCommentLine[0], LiteralTextDelimiter, SeparatorToken }, start);
                if (index >= 0)
                {
                    if (s[index] == SeparatorToken)
                    {
                        last = index + 1;
                        return result + s.Substring(start, index - start);
                    }
                    else if (s[index] == MultiLineCommentStart[0] && index < lastIndex && s[index + 1] == MultiLineCommentStart[1])
                    {
                        result += s.Substring(start, index - start);
                        index = s.IndexOf(MultiLineCommentEnd, index + 2);
                        if (index >= 0)
                        {
                            start = index + 2;
                        }
                    }
                    else if (s[index] == SingleCommentLine[0] && index < lastIndex && s[index + 1] == SingleCommentLine[1])
                    {
                        result += s.Substring(start, index - start);
                        index = s.IndexOf(ReturnLineFeed, index + 2);
                        if (index >= 0)
                        {
                            start = index + 2;
                        }
                    }
                    else if (s[index] == LiteralTextDelimiter)
                    {
                        index = s.IndexOf(LiteralTextDelimiter.ToString(), index + 1);
                        if (index >= 0)
                        {
                            result += s.Substring(start, index - start + 1);
                            start = index + 1;
                        }
                    }
                    else
                    {
                        result += s.Substring(start, index - start + 1);
                        start = index + 1;
                    }
                }
                else
                    return string.Empty;
            }
        }

        public override void Execute(string name)
        {
            manager.Open();
            try
            {
                var script = GetScript(name);
                //NotifyExecute(new ScriptParserEventArgs(0, script.Length, caption));

                if (!string.IsNullOrEmpty(script))
                {
                    var start = 0;

                    while (start >= 0 && start < script.Length)
                    {
                        var indexDeclare = script.IndexOf(DeclareToken, start, StringComparison.InvariantCultureIgnoreCase);
                        if (indexDeclare < 0)
                            indexDeclare = int.MaxValue;
                        var indexBegin = script.IndexOf(BeginToken, start, StringComparison.InvariantCultureIgnoreCase);
                        if (indexBegin < 0)
                            indexBegin = int.MaxValue;
                        var indexFunc = script.IndexOf(CreateFuncToken, start, StringComparison.InvariantCultureIgnoreCase);
                        if (indexFunc < 0)
                            indexFunc = int.MaxValue;
                        var indexProc = script.IndexOf(CreateProcToken, start, StringComparison.InvariantCultureIgnoreCase);
                        if (indexProc < 0)
                            indexProc = int.MaxValue;
                        var indexTrig = script.IndexOf(CreateTriggerToken, start, StringComparison.InvariantCultureIgnoreCase);
                        if (indexTrig < 0)
                            indexTrig = int.MaxValue;

                        var index = Math.Min(indexDeclare, indexBegin);
                        index = Math.Min(index, indexFunc);
                        index = Math.Min(index, indexProc);
                        index = Math.Min(index, indexTrig);

                        if (index == int.MaxValue)
                        {
                            ExtractCommand(script.Substring(start), start, script.Length);
                            start = script.Length;
                        }
                        else
                        {
                            ExtractCommand(script.Substring(start, index - start), start, script.Length);
                            start = index;
                            index = script.IndexOf(EndToken, start, StringComparison.InvariantCultureIgnoreCase);

                            if (index >= 0)
                            {
                                string s = script.Substring(start, index - start + 4);
                                start = index + 7;
                                ExecCommand(s, start, script.Length);
                            }
                        }
                    }
                }
            }
            finally
            {
                manager.Close();
            }
        }

        public OracleScriptParser(string user, string password, string server, DataProvider provider)
            : base(user, password, server, string.Empty, provider) { }
    }
}
