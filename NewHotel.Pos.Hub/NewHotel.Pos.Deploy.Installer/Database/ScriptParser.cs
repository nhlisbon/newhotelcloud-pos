﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using System.Reflection;
using System.IO;

namespace NewHotel.Pos.Deploy.Installer
{
    public sealed class ScriptParserEventArgs : EventArgs
    {
        public readonly int Index;
        public readonly int Count;
        public readonly string Text;

        public ScriptParserEventArgs(int index, int count, string text)
        {
            Index = index;
            Count = count;
            Text = text;
        }
    }

    public delegate void ScriptParserEventHandler(object sender, ScriptParserEventArgs e);

    public abstract class ScriptParser
    {
        protected readonly IDatabaseManager manager;

        public event ScriptParserEventHandler OnExecute;

        protected abstract string ResourcePath { get; }

        protected void NotifyExecute(ScriptParserEventArgs e)
        {
            if (OnExecute != null)
                OnExecute(this, e);
        }

        protected ScriptParser(string user, string password, string server, string database, DataProvider provider)
        {
            manager = NewHotelAppContext.GetManager(provider, user, password, server, database);
        }

        protected string GetScript(string name)
        {
            var assembly = Assembly.GetExecutingAssembly();
            using (var reader = new StreamReader(assembly.GetManifestResourceStream(ResourcePath + "." + name)))
            {
                return reader.ReadToEnd();
            }
        }

        public abstract void Execute(string name);
    }
}
