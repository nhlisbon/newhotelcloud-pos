﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Core;
using System.Data;

namespace NewHotel.Pos.Deploy.Installer
{
    public class SetupManager
    {
        #region Members
        protected readonly IDatabaseManager manager;
        protected string _systemUser = "system";
        protected string _systemPassword;
        protected string _server;
        protected string _username;
        protected string _userPassword;
        protected DataProvider _provider;
        #endregion
        #region Constructor
        public SetupManager(string password, string username, string userpassword, string server, DataProvider provider)
        {
            _provider = provider;
            _systemPassword = password;
            _server = server;
            _username = username;
            _userPassword = userpassword;
            manager = NewHotelAppContext.GetManager(_provider, _systemUser, _systemPassword, _server, string.Empty);
        }
        #endregion
        #region Public Properties
        public bool BrandNewUser { get; set; }
        #endregion
        #region Public Methods
        public bool TestConnection()
        {
            try
            {
                manager.Open();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                manager.Close();
            }
        }
        public bool TestUserConnection()
        {
            //IDatabaseManager usermanager = AppContext.GetManager(_provider, _username, _userPassword, _database); ;
            //try
            //{
            //    usermanager.Open();
            return true;
            //}
            //catch (Exception)
            //{
            //    return false;
            //}
            //finally
            //{
            //    usermanager.Close();
            //    usermanager.Dispose();
            //}
        }
        public bool CheckForTablespaces()
        {
            manager.Open();
            try
            {
                Query query = new BaseQuery(manager, "DBSetup.CountTablespaces", "select count(*) from SYS.V_$TABLESPACE TBL where TBL.NAME IN (:nhtdata, :nhtindex, :nhttemp)");
                query.Parameters["nhtdata"] = "MYNHTDATA";
                query.Parameters["nhtindex"] = "MYNHTINDEX";
                query.Parameters["nhttemp"] = "MYNHTTEMP";
                try
                {
                    return (query.ExecuteScalar<decimal>() > 0);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            finally
            {
                manager.Close();
            }
        }
        public void CreateTablespaces()
        {
            ScriptParser adminScripts = new OracleScriptParser(_systemUser, _systemPassword, _server, _provider);
            adminScripts.Execute("tablespace-user.sql");
        }
        public bool CheckForUser()
        {
            Log.Source["NewHotel.DBSetup.Cloud"].TraceInformation(manager.ToString());
            manager.Open();
            try
            {
                Query query = new BaseQuery(manager, "DBSetup.CountUsers", "SELECT COUNT(*) FROM SYS.ALL_USERS WHERE USERNAME = UPPER(:username)");
                query.Parameters["username"] = _username;
                try
                {
                    return (query.ExecuteScalar<decimal>() > 0);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            finally
            {
                manager.Close();
            }
        }
        public void DeleteUser()
        {
            manager.Open();
            try
            {
                var command = manager.CreateCommand("SetupManager.DeleteUser");
                command.CommandText = "DROP USER " + _username.ToUpper() + " CASCADE";
                manager.ExecuteNonQuery(command);
            }
            finally
            {
                manager.Close();
            }
        }
        public void CreateUser()
        {
            manager.Open();
            try
            {
                manager.ExecuteNonQuery("BEGIN " +
                        "EXECUTE IMMEDIATE 'CREATE USER " + _username.ToUpper() + " IDENTIFIED BY \"" + _userPassword.ToUpper() + "\" DEFAULT TABLESPACE MYNHTDATA TEMPORARY TABLESPACE MYNHTTEMP'; " +
                        "EXECUTE IMMEDIATE 'GRANT \"CONNECT\" TO " + _username.ToUpper() + " WITH ADMIN OPTION'; " +
                        "EXECUTE IMMEDIATE 'GRANT \"EXP_FULL_DATABASE\" TO " + _username.ToUpper() + " WITH ADMIN OPTION'; " +
                        "EXECUTE IMMEDIATE 'GRANT \"IMP_FULL_DATABASE\" TO " + _username.ToUpper() + " WITH ADMIN OPTION'; " +
                        "EXECUTE IMMEDIATE 'ALTER USER " + _username.ToUpper() + " DEFAULT ROLE \"CONNECT\", \"EXP_FULL_DATABASE\", \"IMP_FULL_DATABASE\"'; " +
                        "EXECUTE IMMEDIATE 'ALTER USER " + _username.ToUpper() + " QUOTA UNLIMITED ON MYNHTDATA'; " +
                        "EXECUTE IMMEDIATE 'ALTER USER " + _username.ToUpper() + " QUOTA UNLIMITED ON MYNHTINDEX'; " +
                        "END;",
                        "SetupManager.CreateUser");
            }
            finally
            {
                manager.Close();
            }
        }
        public void CreateStructure()
        {
            ScriptParser userScripts = new OracleScriptParser(_username, _userPassword, _server, _provider);
            userScripts.Execute("tables.sql");
        }
        public void InitializeDatabase()
        {
            ScriptParser userScripts = new OracleScriptParser(_username, _userPassword, _server, _provider);
            userScripts.Execute("init.sql");
        }

        public void InitializeInstallation(Guid id)
        {
            manager.Open();
            try
            {
                Query query = new BaseQuery(manager, "", "insert into " + _username + ".tnht_hote (hote_pk, hote_abre, hote_desc, naci_pk, hote_sync ) values (:hote_pk, :hote_abre, :hote_desc, :naci_pk, null)");
                query.Parameters["hote_pk"] = id;
                query.Parameters["hote_abre"] = "To be updated";
                query.Parameters["hote_desc"] = "To be updated";
                query.Parameters["naci_pk"] = "PT";
                query.Execute();
            }
            finally
            {
                manager.Close();
            }
            
        }
        #endregion
    }
}
