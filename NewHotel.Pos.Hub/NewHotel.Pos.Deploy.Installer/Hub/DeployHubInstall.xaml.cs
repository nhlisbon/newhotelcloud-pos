﻿using Ionic.Zip;
using NewHotel.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace NewHotel.Pos.Deploy.Installer.Hub
{
    /// <summary>
    /// Interaction logic for DeployCashierConfig.xaml
    /// </summary>
    public partial class DeployHubInstall : UserControl
    {
        public DeployHubInstall()
        {
            InitializeComponent();
            HubConfigurationClass datacontext = new HubConfigurationClass();
            this.ViewModel = datacontext;

            this.Loaded += (s, ev) =>
            {
                //txtImagePath.Text = " "; txtImagePath.Text = "";
                //txtInstallFolder.Text = " "; txtInstallFolder.Text = "";
                //txtOracleSystemPassword.Text = " "; txtOracleSystemPassword.Text = "";
            };

            this.ViewModel.PropertyChanged += (s, ev) =>
            {
                if (ev.PropertyName == "CloudLocation")
                {
                    CloudService.NewHotelBackOffice client = new CloudService.NewHotelBackOffice();
                    client.Url = ViewModel.CloudLocation + "/NewHotel2011/Services/Web/NewHotelBackOffice.asmx";
                    client.GetHotelsCompleted += (s1, ev1) =>
                    {
                        if (ev1.Error == null)
                        {
                            CloudService.HotelCollectionResponse response = ev1.Result;
                            cmbHotels.ItemsSource = response.Hotels.OrderBy(x => x.Description);
                        }
                    };
                    client.GetHotelsAsync(new CloudService.BaseRequest() { ClientDateTime = DateTime.UtcNow, Password = "cloudbooking", RequestID = "1", UserID = "web.booking" });
                }
            };
        }

        public HubConfigurationClass ViewModel
        {
            get { return this.DataContext as HubConfigurationClass; }
            set { this.DataContext = value; }
        }

        private void btnInstallFolderBrowse_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.SelectedPath = txtInstallFolder.Text;
            var result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                txtInstallFolder.Text = dialog.SelectedPath;
                ViewModel.InstallationFolder = dialog.SelectedPath;
            }
        }

        private void btnImageFolderBrowse_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.SelectedPath = txtImagePath.Text;
            var result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                txtImagePath.Text = dialog.SelectedPath;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void btnInstall_Click(object sender, RoutedEventArgs e)
        {
            if (ViewModel.ValidateObject())
            {
                grdChecks.Visibility = Visibility.Visible;
                ctrlChecks.Run();
                ctrlChecks.ChecksCompleted += (s, ev) =>
                {
                    grdChecks.Visibility = Visibility.Collapsed;
                    tabCtrl.SelectedIndex = 1;
                    Download();
                };
            }
        }

        private void Download()
        {
            WebClient webClient = new WebClient();
            string server = ViewModel.CloudLocation + "/Newhotel2011/LiveUpdate/Pos/Installer/";
            string serverFile = Path.Combine(server, "hub.bin");
            string appFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "NewHotel");
            if (Directory.Exists(appFolder)) Directory.CreateDirectory(appFolder);
            string localFile = Path.Combine(appFolder, "server.bin");

            this.txtStatus.Text = "Downloading installation files";
            this.listviewInfo.Items.Add("Downloading installation files...");
            this.progressBar.Value = 0;
            this.progressBar.Maximum = 100;
            this.progressBarVisual.IsIndeterminate = true;

            webClient.DownloadProgressChanged += (s, ev) =>
            {
                this.txtStatus.Text = "Downloading installation files " + (ev.BytesReceived / 1024).ToString() + "kb of " + (ev.TotalBytesToReceive / 1024).ToString() + "kb";
                this.progressBar.Value = ev.ProgressPercentage;
            };
            webClient.DownloadFileCompleted += (s, ev) =>
            {
                this.progressBar.Value = 0;
                this.progressBar.Maximum = 10;
                Install();
            };
            webClient.DownloadFileAsync(new Uri(serverFile, UriKind.Absolute), localFile);
        }

        private void Install()
        {
            BackgroundWorker worker = new BackgroundWorker() { WorkerSupportsCancellation = true, WorkerReportsProgress = true };
            worker.DoWork += Worker_DoWork;
            worker.ProgressChanged += Worker_ProgressChanged;
            worker.RunWorkerCompleted += (s, ev) =>
            {
                if (ev.Result != null )
                {
                    System.Windows.Forms.MessageBox.Show(ev.Result.ToString());
                    this.progressBar.Value = 0;
                    this.progressBarVisual.IsIndeterminate = false;
                }
                else
                {
                    this.progressBar.Value = 0;
                    this.progressBarVisual.IsIndeterminate = false;
                    System.Windows.Forms.MessageBox.Show("Installation Complete");
                }
            };

            this.progressBar.Value = 0;
            this.progressBar.Maximum = 10;
            this.progressBarVisual.IsIndeterminate = true;

            worker.RunWorkerAsync(ViewModel);
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            HubConfigurationClass config = e.Argument as HubConfigurationClass;

            #region Extract Files
            {
                ((BackgroundWorker)sender).ReportProgress(1, "Extracting setup files...");

                string appFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "NewHotel");
                if (!Directory.Exists(config.InstallationFolder)) Directory.CreateDirectory(config.InstallationFolder);

                ZipFile zip = ZipFile.Read(System.IO.Path.Combine(appFolder, "server.bin"));
                zip.ExtractAll(config.InstallationFolder, ExtractExistingFileAction.OverwriteSilently);
                zip.Dispose();
            }
            #endregion
            #region Database Section
            {
                ((BackgroundWorker)sender).ReportProgress(1, "Initializing Database...");
                SetupManager manager = new SetupManager(config.OracleSystem, config.OracleSchemaName, "CLOUD", config.OracleServiceName,DataProvider.Oracle);
                
                //Check connection
                ((BackgroundWorker)sender).ReportProgress(1, "Testing connection...");
                if (!manager.TestConnection())
                {
                    e.Result = "Invalid connection information";
                    return;
                }

                //Check for tablespace
                ((BackgroundWorker)sender).ReportProgress(1, "Creating database tablespaces...");
                if (!manager.CheckForTablespaces())
                {
                    manager.CreateTablespaces();
                }

                //Check for user
                ((BackgroundWorker)sender).ReportProgress(1, "Creating database user...");
                if (!manager.CheckForUser())
                {
                    manager.CreateUser();
                }
                else
                {
                    e.Result = "User already exists. Unable to continue";
                    return;
                }

                //Create structure
                ((BackgroundWorker)sender).ReportProgress(1, "Creating database structure...");
                manager.CreateStructure();

                //Fill data
                ((BackgroundWorker)sender).ReportProgress(1, "Creating database start information...");
                manager.InitializeDatabase();

                //Initializing Installations
                ((BackgroundWorker)sender).ReportProgress(1, "Initializing installation information...");
                manager.InitializeInstallation(config.HotelID);
            }
            #endregion
            #region Applying Hub Configurations
            {
                ((BackgroundWorker)sender).ReportProgress(1, "Applying Hub configuration...");

                ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap();
                configFileMap.ExeConfigFilename = Path.Combine(config.InstallationFolder, "Hub\\NewHotel.Pos.Hub.exe.config");
                Configuration appConfig = ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);
                var section = (AppSettingsSection)appConfig.GetSection("appSettings");
                section.Settings["OracleServiceName"].Value = config.OracleServiceName;
                section.Settings["OracleSchemaName"].Value = config.OracleSchemaName;
                section.Settings["CloudLocation"].Value = config.CloudLocation + "/NewHotel2011/Pos/";
                section.Settings["SignalRServer"].Value = config.SignalR;
                section.Settings["ImageRootPath"].Value = config.ImagePath;
                var serviceModel = appConfig.GetSection("");
                appConfig.Save();
            }
            #endregion
            #region Applying Sync Server Configurations
            {
                ((BackgroundWorker)sender).ReportProgress(1, "Applying Sync Server configuration...");

                //Read File
                string syncServerFile = Path.Combine(config.InstallationFolder, "SyncServer\\NewHotel.Sync.Server.exe.config");
                StreamReader reader = new StreamReader(syncServerFile);
                string syncServerFileContent = reader.ReadToEnd();
                reader.Close();

                //Replace
                syncServerFileContent = syncServerFileContent.Replace("[SERVICE]", config.OracleServiceName);
                syncServerFileContent = syncServerFileContent.Replace("[SID]", config.OracleSIDName);
                syncServerFileContent = syncServerFileContent.Replace("[SERVER]", config.OracleIPAddress);
                syncServerFileContent = syncServerFileContent.Replace("[PORT]", config.OraclePortNumber);

                //Save File
                StreamWriter writer = new StreamWriter(syncServerFile, false);
                writer.Write(syncServerFileContent);
                writer.Close();
            }
            #endregion
            #region Applying Sync Client Configurations
            {
                ((BackgroundWorker)sender).ReportProgress(1, "Applying Sync Client configuration...");

                //Read File
                string syncClientFile = Path.Combine(config.InstallationFolder, "SyncClient\\NewHotel.Sync.Client.exe.config");
                StreamReader reader = new StreamReader(syncClientFile);
                string syncClientFileContent = reader.ReadToEnd();
                reader.Close();

                //Replace
                syncClientFileContent = syncClientFileContent.Replace("[SERVICE]", config.OracleServiceName);
                syncClientFileContent = syncClientFileContent.Replace("[SID]", config.OracleSIDName);
                syncClientFileContent = syncClientFileContent.Replace("[SERVER]", config.OracleIPAddress);
                syncClientFileContent = syncClientFileContent.Replace("[PORT]", config.OraclePortNumber);
                syncClientFileContent = syncClientFileContent.Replace("[SCHEMA]", config.OracleSchemaName);

                //Save File
                StreamWriter writer = new StreamWriter(syncClientFile, false);
                writer.Write(syncClientFileContent);
                writer.Close();
            }
            #endregion
            #region Applying SignalR Configurations
            {
                ((BackgroundWorker)sender).ReportProgress(1, "Applying Signal R configuration...");

                //Read File
                string signalRClientFile = Path.Combine(config.InstallationFolder, "SignalR\\NewHotel.SignalR.Server.exe.config");
                StreamReader reader = new StreamReader(signalRClientFile);
                string signalRClientFileContent = reader.ReadToEnd();
                reader.Close();

                signalRClientFileContent = signalRClientFileContent.Replace("[PORT]", config.OraclePortNumber);

                //Save File
                StreamWriter writer = new StreamWriter(signalRClientFile, false);
                writer.Write(signalRClientFileContent);
                writer.Close();
            }
            #endregion
            #region Installing HUB Service
            {
                ((BackgroundWorker)sender).ReportProgress(1, "Installing Hub Service...");
                string executable = Path.Combine(config.InstallationFolder, "Hub\\NewHotel.Pos.Hub.exe");
                var process = Process.Start(executable, "/i");
                process.WaitForExit();
            }
            #endregion
            #region Installing Sync Server Service
            {
                ((BackgroundWorker)sender).ReportProgress(1, "Installing Sync Server Service...");
                string executable = Path.Combine(config.InstallationFolder, "SyncServer\\NewHotel.Sync.Server.exe");
                var process = Process.Start(executable, "/i");
                process.WaitForExit();
            }
            #region Installing Sync Client Service
            {
                ((BackgroundWorker)sender).ReportProgress(1, "Installing Sync Client Service...");
                string executable = Path.Combine(config.InstallationFolder, "SyncClient\\NewHotel.Sync.Client.exe");
                var process = Process.Start(executable, "/i");
                process.WaitForExit();
            }
            #endregion
            #endregion
            #region Installing SignalR
            {
                ((BackgroundWorker)sender).ReportProgress(1, "Installing SignalR Server Service...");
                string executable = Path.Combine(config.InstallationFolder, "SignalR\\NewHotel.SignalR.Server.exe");
                var process = Process.Start(executable, "/i");
                process.WaitForExit();
            }
            #endregion
            #region Starting HUB Service
            {
                ((BackgroundWorker)sender).ReportProgress(1, "Starting Hub Service...");
                ServiceController service = new ServiceController("NewHotel Pos Hub");
                service.Start();
            }
            #endregion
            #region Starting Sync Server
            {
                ((BackgroundWorker)sender).ReportProgress(1, "Starting Sync Server...");
                ServiceController service = new ServiceController("NewHotel.Sync.Server");
                service.Start();
            }
            #endregion
            #region Starting Sync Client
            {
                ((BackgroundWorker)sender).ReportProgress(1, "Starting Sync Client...");
                ServiceController service = new ServiceController("NewHotel.Sync.Client");
                service.Start();
            }
            #endregion

            ((BackgroundWorker)sender).ReportProgress(1, "Installation completed");
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.progressBar.Value += 1;
            this.txtStatus.Text = e.UserState.ToString();
            this.listviewInfo.Items.Add(e.UserState.ToString());

            this.listviewInfo.IsSynchronizedWithCurrentItem = true;
            this.listviewInfo.Items.MoveCurrentToLast();
            this.listviewInfo.ScrollIntoView(this.listviewInfo.SelectedItem);

        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            DeployActionSelection actionSelection = new Installer.DeployActionSelection();
            MainPage.Container.Content = actionSelection;
        }
    }
}
