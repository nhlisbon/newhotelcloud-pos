﻿using NewHotel.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewHotel.Pos.Deploy.Installer.Hub
{
    /// <summary>
    /// Interaction logic for HubPreChecks.xaml
    /// </summary>
    public partial class HubPreChecks : UserControl
    {
        public event EventHandler ChecksCompleted;
        public HubConfigurationClass ViewModel { get { return this.DataContext as HubConfigurationClass; } }

        public HubPreChecks()
        {
            InitializeComponent();
        }

        public void Run()
        {
            if (ViewModel != null)
            {
                bool failed = false;

                #region DB User
                try
                {
                    IDatabaseManager manager = NewHotelAppContext.GetManager(DataProvider.Oracle, ViewModel.OracleSchemaName, "CLOUD", ViewModel.OracleServiceName, String.Empty);
                    manager.Open();
                    manager.Close();
                    failed = true;
                }
                catch (Exception)
                {
                    chkDbUserEr.Visibility = Visibility.Collapsed;
                }
                #endregion
                List<ServiceController> services = new List<ServiceController>(ServiceController.GetServices());
                ServiceController ctrl = services.FirstOrDefault(x => x.DisplayName == "NewHotel Pos Hub");
                #region HUB Service
                if (!services.Exists(x => x.DisplayName == "NewHotel Pos Hub"))
                {
                    chkHubEr.Visibility = Visibility.Collapsed;
                }
                else failed = true;
                #endregion
                #region Sync Client
                if (!services.Exists(x => x.DisplayName == "NewHotel Sync Client"))
                {
                    chkSyncClientEr.Visibility = Visibility.Collapsed;
                }
                else failed = true;
                #endregion
                #region Sync Server
                if (!services.Exists(x => x.DisplayName == "NewHotel Sync Server"))
                {
                    chkSyncServerEr.Visibility = Visibility.Collapsed;
                }
                else failed = true;
                #endregion
                #region Signal R
                if (!services.Exists(x => x.DisplayName == "NewHotel.SignalR.Server"))
                {
                    chkSignalREr.Visibility = Visibility.Collapsed;
                }
                else failed = true;
                #endregion

                progressBar.IsIndeterminate = false;

                if (!failed)
                {
                    ChecksCompleted(this, null);
                }
                else
                {
                    txtInfo.Text = "Components found. Click remove to try to delete them";
                }
                
            }
        }

        private void bntRemove_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
