﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewHotel.Pos.Deploy.Installer
{
    [CustomValidation(typeof(HubConfigurationClass), "ValidateHubConfigurationClass")]
    public class HubConfigurationClass : BaseObject
    {
        private string _oracleSIDName;
        private string _oracleIPAddress;
        private string _oraclePortNumber;
        private string _oracleServiceName;
        private string _oracleSchemaName;
        private string _oracleSystem;
        private string _cashierService;
        private string _cloudLocation;
        private string _signalR;
        private string _installationFolder;
        private string _imagePath;
        private Guid _hotelId;

        public HubConfigurationClass()
        {
            CashierService = "net.tcp://localhost:13303/NewHotel.Pos.Hub.Cashier/Service/";
            //CloudLocation = "https://oscar-ext:8443";
            SignalR = "http://localhost:8081/";
            OracleSchemaName = "HUBTEST";
            OracleServiceName = "DESARROLLO";
            OraclePortNumber = "1521";
            OracleSIDName = "ORCL";
            OracleIPAddress = "localhost";
            ImagePath = @"C:\Users\oscar_000\Desktop\HUB INSTALL";
            InstallationFolder = @"C:\Users\oscar_000\Desktop\HUB INSTALL";
            OracleSystem = "MANAGER";
        }

        
        public string OracleSIDName { get { return _oracleSIDName; } set { Set(ref _oracleSIDName, value, "OracleSIDName"); } }
        public string OracleSystem { get { return _oracleSystem; } set { Set(ref _oracleSystem, value, "OracleSystem"); } }
        public string OracleIPAddress { get { return _oracleIPAddress; } set { Set(ref _oracleIPAddress, value, "OracleIPAddress"); } }
        public string OraclePortNumber { get { return _oraclePortNumber; } set { Set(ref _oraclePortNumber, value, "OraclePortNumber"); } }
        public string OracleServiceName { get { return _oracleServiceName; } set { Set(ref _oracleServiceName, value, "OracleServiceName"); } }
        public string OracleSchemaName { get { return _oracleSchemaName; } set { Set(ref _oracleSchemaName, value, "OracleSchemaName"); } }
        public string CashierService { get { return _cashierService; } set { Set(ref _cashierService, value, "CashierService"); } }
        public string CloudLocation { get { return _cloudLocation; } set { Set(ref _cloudLocation, value, "CloudLocation"); } }
        public string SignalR { get { return _signalR; } set { Set(ref _signalR, value, "SignalR"); } }
        public string InstallationFolder { get { return _installationFolder; } set { Set(ref _installationFolder, value, "InstallationFolder"); } }
        public string ImagePath { get { return _imagePath; } set { Set(ref _imagePath, value, "ImagePath"); } }
        public Guid HotelID { get { return _hotelId; } set { Set(ref _hotelId, value, "HotelID"); } }

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateHubConfigurationClass(HubConfigurationClass obj)
        {
            if (String.IsNullOrEmpty(obj.OracleSIDName)) return new System.ComponentModel.DataAnnotations.ValidationResult("Cashier service is missing");
            if (String.IsNullOrEmpty(obj.OracleIPAddress)) return new System.ComponentModel.DataAnnotations.ValidationResult("Oracle address is missing");
            if (String.IsNullOrEmpty(obj.OraclePortNumber)) return new System.ComponentModel.DataAnnotations.ValidationResult("Oracle port is missing");
            if (String.IsNullOrEmpty(obj.OracleSystem)) return new System.ComponentModel.DataAnnotations.ValidationResult("Oracle system password is missing");
            if (String.IsNullOrEmpty(obj.OracleServiceName)) return new System.ComponentModel.DataAnnotations.ValidationResult("Oracle service name is missing");
            if (String.IsNullOrEmpty(obj.OracleSchemaName)) return new System.ComponentModel.DataAnnotations.ValidationResult("Oracle schema name is missing");
            if (String.IsNullOrEmpty(obj.CloudLocation)) return new System.ComponentModel.DataAnnotations.ValidationResult("Cloud location is missing");
            if (String.IsNullOrEmpty(obj.SignalR)) return new System.ComponentModel.DataAnnotations.ValidationResult("Signal R is missing");
            if (String.IsNullOrEmpty(obj.InstallationFolder)) return new System.ComponentModel.DataAnnotations.ValidationResult("Installation folder is missing");
            if (String.IsNullOrEmpty(obj.ImagePath)) return new System.ComponentModel.DataAnnotations.ValidationResult("Image path is missing");
            if (obj.HotelID == Guid.Empty) return new System.ComponentModel.DataAnnotations.ValidationResult("Hotel is missing");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;

        }
    }
}
