﻿using System;
using System.Configuration;
using System.ServiceModel;
using NewHotel.Contracts;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Contracts.Handheld.Contracts;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Notifications;
using NewHotel.Pos.PrinterDocument.Interfaces;
using NewHotel.Pos.PrinterDocument.Simple;
using NewHotel.Pos.Spooler.Business;

namespace NewHotel.Pos.Spooler
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class SpoolerService : ISpoolerService
    {
        #region Initialize

        private static string SignalRServer => ConfigurationManager.AppSettings["SignalRServer"];
        private static string HubName => ConfigurationManager.AppSettings["SignalRHubName"];
        private static SignalRClientWrapper SignalRClient { get; set; }

        static SpoolerService()
        {
            CwFactory.Register<IPrinterBusiness, PrinterBusiness>();
            CwFactory.Register<IVisualPrinter, SimplePrinterDocument>();
            CwFactory.Register<ITicketPrinter, SimplePrinterDocument>();

            SignalRClient = new SignalRClientWrapper(HubName, SignalRServer);
            SignalRClient.Initialize();
            PrinterBusiness.PrintEvent += (s, ev) =>
            {
                SignalRClient.Publish("SpoolerEvent", ev);
            };
        }

        #endregion
        #region Methods

        private static T GetBusiness<T>()
        {
            return CwFactory.Resolve<T>();
        }

        public ValidationResult PrintOrders(string headerTitle, POSTicketContract ticket, DispatchContract[] orders, bool standPrint)
        {
            var business = GetBusiness<IPrinterBusiness>();
            return business.PrintOrders(headerTitle, ticket, orders, standPrint);
        }

         public ValidationResult PrintTicket(POSTicketContract ticket, Settings settings, bool isInvoice)
        {
            var business = GetBusiness<IPrinterBusiness>();
            return business.PrintTicket(ticket, settings, isInvoice);
        }

        #endregion
    }
}