﻿using System;
using System.Text;
using NewHotel.Contracts;
using NewHotel.Pos.Localization;
using NewHotel.Pos.PrinterDocument.Interfaces;

namespace NewHotel.Pos.Hub.Spooler.Models.Print
{
    class PrinteableBuyerInfo : IPrinteableBuyerInfo
    {
        public bool Invoice { get; set; }
        public string Name { get; set; }
        public string FiscalMail { get; set; }
        public string FiscalNumber { get; set; }
        public string FiscalDocSerie { get; set; }
        public string FactSignature { get; set; }
        public bool IsDefaultCustomer { get; set; }
        public DocumentSign SignType { get; set; }
        public string Address { get; set; }
        public long FiscalIdenType { get; set; }
        public string Text
        {
            get
            {
                var sb = new StringBuilder();
                sb.AppendLine($"{"Soldto".Translate()}: {Name}");
                if (!string.IsNullOrEmpty(Address))
                    sb.AppendLine($"{"Address".Translate()}: {Address}{Environment.NewLine}");
                if (!string.IsNullOrEmpty(FiscalMail))
                    sb.AppendLine($"{"Email".Translate()}: {FiscalMail}");

                if (SignType == DocumentSign.FiscalizationPortugal && Name == "Consumidor final" && FiscalNumber == "999999990")
                {
                    //Skip NIF Portaria 1192 /2009 
                }
                else
                {
                    if (FiscalIdenType != 0) sb.AppendLine($"{((PersonalDocType)FiscalIdenType).ToString().Translate()}: {FiscalNumber}");
                    else sb.AppendLine($"{"FiscalNumber".Translate()}: {FiscalNumber}");
                }

                return sb.ToString();
            }
        }

        public static IPrinteableBuyerInfo CreateFromTicket(POSTicketContract ticket, DocumentSign sign)
        {
            IPrinteableBuyerInfo buyerInfo = null;
            if (!string.IsNullOrEmpty(ticket.FiscalDocSerie))
            {
                buyerInfo = new PrinteableBuyerInfo
                {
                    FiscalDocSerie = ticket.FiscalDocSerie,
                    FiscalNumber = ticket.FiscalDocFiscalNumber,
                    Name = ticket.FiscalDocTo,
                    FiscalMail = ticket.FiscalDocEmail,
                    Invoice = true,
                    SignType = sign,
                    Address = ticket.FiscalDocAddress,
                    FiscalIdenType = (long)ticket.FiscalIdenType,
                };
            }

            return buyerInfo;
        }
    }
}
