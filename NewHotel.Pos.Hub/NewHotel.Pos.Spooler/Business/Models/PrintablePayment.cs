﻿using System;
using NewHotel.Contracts;
using NewHotel.Pos.Localization;
using NewHotel.Pos.Printer;
using NewHotel.Pos.Printer.Interfaces;

namespace NewHotel.Pos.Spooler.Business.Models
{
    public class PrintablePayment : IPrintablePayment
    {
        #region Variables

        private readonly POSPaymentLineContract _payment;

        #endregion

        #region Constructor

        public PrintablePayment(POSPaymentLineContract payment, string details)
        {
            _payment = payment;
            Details = details;
        }

        #endregion

        #region Properties

        public bool IsCash => _payment.IsCash;

        public decimal ReceiveValue => _payment.PaymentReceived;

        public string TireDescription
        {
            get
            {
                var receivable = _payment.ReceivableType;
                switch (receivable)
                {
                    case ReceivableType.RoomPlan:
                        return LocalizationMgr.Translation("RoomPlan");
                    case ReceivableType.HouseUse:
                        return LocalizationMgr.Translation("HouseUse");
                    case ReceivableType.Credit:
                        return LocalizationMgr.Translation("RoomCredit");
                    case ReceivableType.UseAccountDeposit:
                        return LocalizationMgr.Translation("Deposit");
                    case ReceivableType.Payment:
                    case ReceivableType.AccountTransfer:
                    case ReceivableType.DiscountPoints:
                    case ReceivableType.CashAdvance:
                    case ReceivableType.Devolution:
                    case ReceivableType.UnofficialInvoice:
                    case ReceivableType.Discount:
                    case ReceivableType.FreeTitle:
                    case ReceivableType.TicketTransfer:
                    case ReceivableType.TaxRetention:
                    case ReceivableType.InternalAccountSettlement:
                    case ReceivableType.RetentionOverNet:
                    default:
                        return string.Empty;
                }
            }
        }

        public bool WideDetails => _payment.CreditType == CurrentAccountType.Reservation;
        public string Details { get; set; }
        public string Description => _payment.PaymentDescription;

        public IPrinteablePaymentType Type => new PrinteablePaymentType
        {
            Id = _payment.PaymentId ?? Guid.Empty,
            AuxiliarCode = _payment.PaymentTypeAuxCode
        };

        #endregion
    }
}