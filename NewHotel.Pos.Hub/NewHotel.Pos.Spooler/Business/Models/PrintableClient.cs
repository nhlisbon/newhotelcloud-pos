﻿using System;
using System.Linq;
using NewHotel.Contracts;
using NewHotel.Pos.Localization;
using NewHotel.Pos.Printer.Interfaces;

namespace NewHotel.Pos.Spooler.Business.Models
{
    public class PrintableClient : IPrintableClient
    {
        #region Variables

        private readonly POSClientByPositionContract _client;

        #endregion
        #region Constructor

        public PrintableClient(POSClientByPositionContract client)
        {
            _client = client;
        }

        #endregion
        #region Properties

        public string ClientInfo
        {
            get
            {
                var clientInfo = _client.ClientName;
                if (!string.IsNullOrEmpty(_client.ClientRoom))
                    clientInfo += $" # {_client.ClientRoom}";

                return clientInfo;
            }
        }

        public string Preferences => string.Join(", ", _client.Preferences.Select(p => p.Description));
        public string Diets => string.Join(", ", _client.Diets.Select(p => p.Description));
        public string Attentions => string.Join(", ", _client.Attentions.Select(p => p.Description));
        public string Allergies => string.Join(", ", _client.Allergies.Select(p => p.Description));
        public string UncommonAllergies => _client.UncommonAllergies;
        public string SegmentOperations => _client.SegmentOperations;

        public short Pax => _client.Pax;

        #endregion
    }
}