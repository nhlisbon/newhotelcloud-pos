﻿using System;
using System.Collections.Generic;
using System.Linq;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;
using NewHotel.Fiscal.Saft;
using NewHotel.Pos.Localization;
using NewHotel.Pos.Printer.Interfaces;
using NewHotel.WPF.Model.Model.CommonUtils;

namespace NewHotel.Pos.Spooler.Business.Models
{
    public class PrintableTicket : IPrintableDoc
    {
        #region Variables

        private readonly POSTicketContract _ticket;
        private IEnumerable<IPrinteableLookupTable>? _lookupTables;
        private IEnumerable<PrintableOrder>? _productLines;
        private IEnumerable<PrintablePayment>? _payments;

        #endregion

        #region Constructors

        public PrintableTicket(POSTicketContract contract)
        {
            _ticket = contract;
            if (!string.IsNullOrEmpty(TicketSignature))
                SignedLabel = SAFTPT.GetLabel(TicketSignature);
        }

        #endregion

        #region Properties

        public string SaloonDescription => _ticket.SaloonDescription;
        public decimal TotalAmount => _ticket.Total;
        public long OpeningNumber => _ticket.OpeningNumber;
        public long? Serie => _ticket.Number;
        public string SeriePrex => _ticket.Serie;
        public string ValidationCode => _ticket.ValidationCode;
        public Blob? QrCode
        {
            get
            {
                if (_ticket.QrCode == null && !string.IsNullOrEmpty(_ticket.QrCodeData))
                {
                    _ticket.QrCode = QRCodeUtil.GetQRCodeFromData(_ticket.QrCodeData);
                }
                return _ticket.QrCode;
            }
        }
        public string TableDescription => _ticket.TableDescription;
        public decimal TaxAmount => _ticket.TotalTaxes;
        public decimal TipAmount => _ticket.TipValue ?? decimal.Zero;
        public string UserDescription => _ticket.UserDescription;
        public string FiscalDocFiscalNumber => _ticket.FiscalDocFiscalNumber;
        public string FiscalDocSignature => _ticket.FiscalDocSignature;
        public string FiscalDocTo
        {
            get => _ticket.FiscalDocTo;
            set => _ticket.FiscalDocTo = value;
        }
        public string FiscalIdenTypeName
        {
            get
            {
                var identity = (PersonalDocType?)_ticket.FiscalIdenType;
                return (identity switch
                {
                    0 => "FiscalNumber".Translate(),
                    null => "FiscalNumber".Translate(),
                    _ => LocalizationMgr.Translation(identity.ToString()),
                }) + ":";
            }
        }
        public string FiscalDocAddress => _ticket.FiscalDocAddress;
        public string FiscalDocEmail => _ticket.FiscalDocEmail; 
        public string SignedLabel { get; }
        public string TicketSignature => _ticket.Signature;
        public Blob? DigitalSignature => _ticket.DigitalSignature;
        public DateTime? CloseDate => _ticket.ClosedDate;
        public DateTime? CloseTime => _ticket.ClosedTime;
        public decimal GeneralDiscount => _ticket.GeneralDiscount;
        public decimal Recharge => _ticket.Recharge;
        public bool TaxIncluded => _ticket.TaxIncluded;
        public string Observations => _ticket.Observations;
        public Guid? Account => _ticket.Account;
        public string FiscalDocSerie => string.IsNullOrEmpty(FiscalDocSeriePrex) ? string.Empty : $"{FiscalDocSeriePrex}/{FiscalDocNumber ?? 0}";
        public string FiscalDocSeriePrex => _ticket.FiscalDocSerie;
        public long? FiscalDocNumber => _ticket.FiscalDocNumber;
        public string FiscalDocValidationCode => _ticket.FiscalDocValidationCode;
        public Blob? FiscalDocQrCode
        {
            get
            {
                if (_ticket.FiscalDocQrCode == null && !string.IsNullOrEmpty(_ticket.FiscalDocQrCodeText))
                {
                    _ticket.FiscalDocQrCode = QRCodeUtil.GetQRCodeFromData(_ticket.FiscalDocQrCodeText);
                }
                return _ticket.FiscalDocQrCode;
            }
        }
        public short FiscalDocPrints => _ticket.FiscalDocPrints;
        public string Cufe => _ticket.Cufe;
        public int VisibleOrdersCount => ProductLines?.Count(x => x.IsVisible) ?? 0;
        public DateTime CheckOpeningDate => _ticket.OpeningDate;
        public DateTime CheckOpeningTime => _ticket.OpeningTime;
        public string CashierDescription => _ticket.CashierDescription;
        public CurrentAccountType? AccountType => _ticket.AccountType;
        public short Prints => _ticket.Prints;
        public bool IsAnul => _ticket.IsAnul;
        public string StandDescription => _ticket.StandDescription;

        public string Pension => _ticket.Pension;
        public string Room => _ticket.Room;
        public string Name => _ticket.Name;
        public short Paxs => _ticket.Paxs;
        public int Shift => (int)_ticket.Shift;
        public string TipDescription => _ticket.ProductLineByTicket.FirstOrDefault(x => x.IsTip)?.ProductDescription ?? "Tip";
        public decimal PaymentTotalAmount => _ticket.PaymentTotal;
        public decimal ChangeAmount => _ticket.PaymentChange;
        public long? FpInvoiceNumber => _ticket.FpInvoiceNumber;
        public bool IsBallot => _ticket.IsBallot;
        public bool IsTicket => _ticket.IsOnlyTicket;
        public bool IsInvoice => _ticket.IsInvoice;
        public bool IsCreditNote => _ticket.IsCreditNote;
        public string AccountDescription => _ticket.AccountDescription;
        public bool AllowRoomCredit => _ticket.AllowRoomCredit;
        public string CurrencySymbol => _ticket.CurrencySymbol;
        public string LastUserDispatched => _ticket.LastUserDispatched;
        public string CreditNoteSerie => string.IsNullOrEmpty(CreditNoteSeriePrex) ? string.Empty : $"{CreditNoteSeriePrex}/{CreditNoteNumber ?? 0}";
        public string CreditNoteSeriePrex => _ticket.CreditNoteSerie;
        public long? CreditNoteNumber => _ticket.CreditNoteNumber;
        public DateTime? CreditNoteSystemDate => _ticket.CreditNoteSystemDate;
        public DateTime? CreditNoteWorkDate => _ticket.CreditNoteWorkDate;
        public string CreditNoteValidationCode => _ticket.CreditNoteValidationCode;
        public Blob? CreditNoteQrCode
        {
            get
            {
                if (_ticket.CreditNoteQrCode == null && !string.IsNullOrEmpty(_ticket.CreditNoteQrCodeData))
                {
                    _ticket.CreditNoteQrCode = QRCodeUtil.GetQRCodeFromData(_ticket.CreditNoteQrCodeData);
                }
                return _ticket.CreditNoteQrCode;
            }
        }
        public string CreditNoteSignature => _ticket.CreditNoteSignature;
        public string CreditNoteUserDescription => _ticket.CreditNoteUserDescription;
        public short CreditNotePrints => _ticket.CreditNotePrints;
        public bool HasCashPayment => _ticket.HasCashPayment;
        public bool HasMealPlanPayment => _ticket.HasMealPlanPayment;
        public bool HasCreditRoomPayment => _ticket.HasCreditRoomPayment;
        public bool HasHouseUsePayment => _ticket.HasHouseUsePayment;
        public bool HasAccountDepositPayment =>_ticket.PaymentLineByTicket.Any(x => x.ReceivableType == ReceivableType.UseAccountDeposit);

        // Esto es provisional hasta que tengamos una variable para esta preferencia
        public decimal GrossSubTotalAmount => _ticket.TaxIncluded
            ? _ticket.ProductLineByTicket.Where(IsVisible).Sum(x => _ticket.TaxIncluded ? x.GrossValue : x.NetValue)
            : _ticket.ProductLineByTicket.Where(IsVisible).Sum(x => x.ValueBeforeDiscount);

        public decimal NetSubTotalAmount => _ticket.ProductLineByTicket.Where(IsVisible).Sum(x => x.NetValue);
        public IEnumerable<IPrinteableLookupTable> LookupTables => _lookupTables ??= _ticket.LookupTableByTicket.Select(x => new PrinteableLookupTable(x));
        public IEnumerable<IPrintableOrder> ProductLines => _productLines ??= _ticket.ProductLineByTicket.Select(x => new PrintableOrder(x, _ticket.ClientsByPosition.FirstOrDefault(cp => cp.Pax == _ticket.Paxs)));
        public IEnumerable<IPrintablePayment> Payments => _payments ??= _ticket.PaymentLineByTicket.Select(x => new PrintablePayment(x, _ticket.AccountDescription));
        public IEnumerable<IPrintableClient> Clients => _ticket.ClientsByPosition.OrderBy(x => x.Pax).Select(x => new PrintableClient(x));

        #endregion

        #region Methods

        private bool IsVisible(POSProductLineContract pl) => pl.IsActive && !pl.IsTip;

        #endregion

        #region Class

        private class PrinteableLookupTable : IPrinteableLookupTable
        {
            #region Members

            private readonly LookupTableContract _lookupTable;

            public PrinteableLookupTable(LookupTableContract lookupTable)
            {
                _lookupTable = lookupTable;
                if (!string.IsNullOrEmpty(lookupTable.Signature))
                    SignedLabel = SAFTPT.GetLabel(lookupTable.Signature); 
            }

            public string Serie => _lookupTable.Serie;

            public long? Number => _lookupTable.Number;

            public string Signature
            {
                get => _lookupTable.Signature;
                set => _lookupTable.Signature = value;
            }

            public string SignedLabel { get; set; }

            public string ValidationCode => _lookupTable.ValidationCode;

            public Blob? QrCode
            {
                get
                {
                    if (_lookupTable.QrCode == null && !string.IsNullOrEmpty(_lookupTable.QrCodeData))
                    {
                        QRCodeUtil.GetQRCodeFromData(_lookupTable.QrCodeData);
                    }
                    return _lookupTable.QrCode;
                }
            }

            #endregion
        }

        #endregion
    }
}