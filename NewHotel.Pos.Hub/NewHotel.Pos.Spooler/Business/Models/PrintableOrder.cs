﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NewHotel.Contracts;
using NewHotel.Pos.Localization;
using NewHotel.Pos.Printer;
using NewHotel.Pos.Printer.Interfaces;
using NewHotel.WPF.App.Pos.Model;

namespace NewHotel.Pos.Spooler.Business.Models
{
    public class PrintableOrder : IPrintableOrder
    {
        #region Variables

        private static readonly PrinteableSeparator _separator;
        private readonly POSProductLineContract _line;
        private readonly string _clientInfo;

        #endregion

        #region Constructor

        static PrintableOrder()
        {
            _separator = new PrinteableSeparator
            {
                Id = Guid.Empty,
                Description = string.Empty,
                ImpresionOrden = short.MaxValue
            };
        }

        public PrintableOrder(POSProductLineContract line, POSClientByPositionContract? client)
        {
            _line = line;
            if (client != null)
            {
                _clientInfo = client.ClientName;
                if (!string.IsNullOrEmpty(client.ClientRoom))
                    _clientInfo += $": {LocalizationMgr.Translation("Room")} {client.ClientRoom}";
            }
            else
                _clientInfo = string.Empty;
        }

        #endregion

        #region Properties

        public Guid Id => (Guid)_line.Id;
        public decimal Quantity => _line.ProductQtd;
        public decimal Cost => _line.CostValue;
        public short CancellationStatus => _line.AnnulmentCode;
        public bool AnulAfterClosed => _line.AnulAfterClosed;
        public string Notes => _line.Observations;
        public decimal DiscountPercent => _line.DiscountPercent ?? decimal.Zero;
        public string DiscountTypeDescription => _line.DiscountTypeDescription;
        public bool AutomaticDiscount => _line.AutomaticDiscount;
        public Guid? DiscountType => _line.DiscountTypeId;
        public bool IsActive => _line.IsActive;
        public bool IsTip => _line.IsTip;
        public bool IsVisible => IsActive && !IsTip;
        public string ProductDescription
        {
            get
            {
                if(_line.ManualPriceDesc != null)
                {
                    return _line.ProductDescription + " " + _line.ManualPriceDesc;
                }
                
                return _line.ProductDescription;
            }
        }

        public decimal ProductPriceBeforeDiscount => _line.ProductQtd > decimal.Zero
            ? _line.ValueBeforeDiscount / _line.ProductQtd
            : _line.ValueBeforeDiscount;

        public decimal CostBeforeDiscount => _line.ValueBeforeDiscount;
        public short ItemNumber => _line.ItemNumber;
        public Guid ProductId => _line.Product;
        public decimal DiscountValue => _line.DiscountValue;
        public bool ShowDiscountInfo => _line.DiscountValue > decimal.Zero;
        public Guid FirstIvaCode => _line.FirstIvaId;
        public string FirstIvaDescription => _line.FirstIvaDescription;
        public decimal FirstIvaBase => _line.FirstIvaBase;
        public decimal FirstIvaPercent => _line.FirstIvaPercent;
        public decimal FirstIvas => _line.FirstIvaValue;
        public string FirstIvaAuxCode => _line.FirstIvaAuxCode;
        public Guid? SecondIvaCode => _line.SecondIvaId;
        public string SecondIvaDescription => _line.SecondIvaDescription;
        public decimal? SecondIvaBase => _line.SecondIvaBase;
        public decimal? SecondIvaPercent => _line.SecondIvaPercent;
        public decimal? SecondIvas => _line.SecondIvaValue;
        public string SecondIvaAuxCode => _line.SecondIvaAuxCode;
        public Guid? ThirdIvaCode => _line.ThirdIvaId;
        public string ThirdIvaDescription => _line.ThirdIvaDescription;
        public decimal? ThirdIvaBase => _line.ThirdIvaBase;
        public decimal? ThirdIvaPercent => _line.ThirdIvaPercent;
        public decimal? ThirdIvas => _line.ThirdIvaValue;
        public string ThirdIvaAuxCode => _line.ThirdIvaAuxCode;
        public string DiscountDescription => "Discount";
        public Guid? AreaId => _line.AreaId;

        public bool HasManualPrice => _line.HasManualPrice;
        public bool HasManualSeparator => _line.HasManualSeparator;
        public short? PaxNumber => _line.PaxNumber;

        public string PaxName
        {
            get
            {
                var label = PaxNumber == null || PaxNumber == 0 ? LocalizationMgr.Translation("Shared") : PaxNumber.ToString();
                // if (!string.IsNullOrEmpty(_clientInfo))
                //     label += " " + _clientInfo;
                return label;
            }
        }

        public IPrinteableSeparator Separator
        {
            get
            {
                if (_line.Separator.HasValue)
                {
                    return new PrinteableSeparator
                    {
                        Id = _line.Separator.Value,
                        Description = _line.SeparatorDescription,
                        ImpresionOrden = _line.SeparatorOrder.GetValueOrDefault()
                    };
                }

                return _separator;
            }
        }

        public ICollection<IPrinteablePreparation> Preparations
        {
            get
            {
                return _line.Preparations
                    .Select(x => new PrinteablePreparation(x.PreparationId, x.PreparationDescription))
                    .Cast<IPrinteablePreparation>()
                    .ToList();
            }
        }

        public ICollection<IPrinteableArea> Areas
        {
            get
            {
                return _line.Areas
                    .Select(x => new PrinteableArea(x.AreaId))
                    .Cast<IPrinteableArea>()
                    .ToList();
            }
        }

        public ICollection<IPrintableTableLine> TableLines
        {
            get
            {
                return _line.TableLines.Select(x =>
                    {
                        var line = new PrintableTableLine
                        {
                            Id = (Guid)x.Id,
                            ProductQuantity = x.Quantity.ToString(CultureInfo.InvariantCulture),
                            ProductDescription = x.ProductDescription,
                            Preparations = x.TableLinePreparations
                                .Select(p => new PrinteablePreparation(p.PreparationId, p.PreparationDescription))
                                .Cast<IPrinteablePreparation>()
                                .ToList()
                        };

                        if (x.SeparatorId.HasValue || !string.IsNullOrEmpty(x.SeparatorDescription))
                        {
                            line.Separator = new PrinteableSeparator
                            {
                                Id = x.SeparatorId,
                                Description = x.SeparatorDescription,
                                ImpresionOrden = x.SeparatorOrden.GetValueOrDefault()
                            };
                        }

                        line.Notes = x.Observations;

                        return line;
                    })
                    .Cast<IPrintableTableLine>()
                    .ToList();
            }
        }

        public string CodeIsentoIva => _line.CodeIsentoIva;

        public string DescIsentoIva => _line.DescIsentoIva;

        public bool ShowQuantityDecimalCase { get; set; }
        
        #endregion
    }
}