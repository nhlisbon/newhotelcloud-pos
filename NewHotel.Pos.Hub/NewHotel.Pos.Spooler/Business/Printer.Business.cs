﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Xml.Serialization;
using NewHotel.Contracts;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Contracts.Handheld.Contracts;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Localization;
using NewHotel.Pos.Printer;
using NewHotel.Pos.PrinterDocument.Fiscal;
using NewHotel.Pos.PrinterDocument.Fiscal.CuracaoPanama.Curacao;
using NewHotel.Pos.PrinterDocument.Fiscal.CuracaoPanama.Panama;
using NewHotel.Pos.PrinterDocument.Interfaces;
using NewHotel.Pos.PrinterDocument.Simple.Visual;
using NewHotel.Pos.Spooler.Business.Models;
using NewHotel.Pos.Spooler.Common;
using NewHotel.Pos.Spooler.Ext;

namespace NewHotel.Pos.Spooler.Business
{
    public class PrinterBusiness : IPrinterBusiness
    {
        #region Variables

        private readonly ConfigModel _printerConfig;

        #endregion

        #region Constructor

        public PrinterBusiness()
        {
            _printerConfig = LoadFromConfig();
        }

        #endregion

        #region Events

        public static EventHandler<PrintArgs> PrintEvent;

        #endregion

        #region Properties

        private static ITicketPrinter TicketPrinter => CwFactory.Resolve<ITicketPrinter>();

        private IEnumerable<PrinterModel> Areas =>
            _printerConfig != null ? _printerConfig.DispatchAreas.ToList() : new List<PrinterModel>();

        private PrinterModel TicketConfiguration(Guid standId)
        {
            return _printerConfig != null
                ? _printerConfig.TicketPrinters.FirstOrDefault(x => x.StandId == standId.ToString("N").ToUpper())
                : new PrinterModel();
        }

        #endregion

        #region Methods

        public ValidationResult PrintOrders(string headerTitle, POSTicketContract ticket, DispatchContract[] orders, bool standPrint)
        {
            var result = new ValidationResult();

            var messages = OrganizeProductsByArea(orders);
            foreach (var message in messages)
            {
                var printValidation = KitchenPrint(ticket, message.OrderIds, message.AreaId, headerTitle, standPrint);
                result.Add(printValidation);
            }

            return result;
        }

        public ValidationResult PrintTicket(POSTicketContract ticket, Settings settings, bool isInvoice)
        {
            var result = new ValidationResult();

            string printerName = null;
            const string action = "Print";

            try
            {
                var setting = PrintDocSetting.Create(settings.Print, settings.GeneralSettings.SignType,
                    settings.GeneralSettings.Country, settings.Stand);
                var pTicket = new PrintableTicket(ticket);

                if (settings.GeneralSettings.FiscalType != FiscalPOSType.None)
                {
                    var fPrinter = GetFiscalPrinter(settings.GeneralSettings.FiscalType);
                    printerName = fPrinter.GetPrinterDescription(null);
                    OnPrintEvent(ticket, true, printerName, action);
                    // With fiscal printer from android only non fiscal because invoice not is implemented
                    result = fPrinter.PrintNonFiscal(pTicket, setting);
                }
                else
                {
                    var config = TicketConfiguration(ticket.Stand);
                    if (config == null)
                        throw new Exception(
                            "Spooler: There is no a printer configuration which allows to print the ticket.");
                    var printer = config.GetPrinter();
                    var tPrinter = TicketPrinter;
                    printerName = tPrinter.GetPrinterDescription(printer);
                    OnPrintEvent(ticket, true, printerName, action);
                    result = isInvoice
                        ? RunInStaThread(() => tPrinter.PrintInvoice(printer, pTicket, setting))
                        : RunInStaThread(() => ticket.ClosedDate.HasValue ? tPrinter.PrintTicket(printer, pTicket, setting) : tPrinter.PrintLookupTable(printer, pTicket, setting));
                }

                OnPrintEvent(ticket, false, printerName, action, !result.IsEmpty ? result.Message : null);
            }
            catch (Exception ex)
            {
                result.AddError(ex.Message);
                result.AddError(ex.StackTrace);
                OnPrintEvent(ticket, false, printerName, action, ex.Message);
            }

            return result;
        }

        private static ValidationResult RunInStaThread(Func<ValidationResult> function)
        {
            var result = new ValidationResult();
            var handle = new EventWaitHandle(false, EventResetMode.AutoReset);

            var start = new Thread(() =>
            {
                try
                {
                    result = function();
                }
                catch (Exception e)
                {
                    Trace.WriteLine(e.Message);
                }
                finally
                {
                    handle.Set();
                }
            });

            start.SetApartmentState(ApartmentState.STA);
            start.Start();
            handle.WaitOne();
            return result;
        }

        private static IFiscalPrinter GetFiscalPrinter(FiscalPOSType fiscalPosType)
        {
            return fiscalPosType switch
            {
                FiscalPOSType.CuracaoBixolon => new FiscalPrinterCuracao(),
                FiscalPOSType.PanamaTally => new FiscalPrinterPanama(),
                FiscalPOSType.None => throw new ArgumentOutOfRangeException(nameof(fiscalPosType), fiscalPosType, null),
                FiscalPOSType.BrazilBematech => throw new ArgumentOutOfRangeException(nameof(fiscalPosType),
                    fiscalPosType, null),
                _ => throw new ArgumentOutOfRangeException(nameof(fiscalPosType), fiscalPosType, null)
            };
        }

        private ValidationResult KitchenPrint(POSTicketContract ticket, Dictionary<Guid, HashSet<Guid>> orderIds,
            Guid areaId,
            string action, bool standPrint)
        {
            var result = new ValidationResult();
            
            try
            {
                // Setup language before print
                SetupLanguage();

                var area = Areas.FirstOrDefault(x =>
                    x.AreaId == areaId.ToString("N").ToUpper() && x.StandId == ticket.Stand.ToString("N").ToUpper());

                if (area != null)
                {
                    if (!string.IsNullOrEmpty(area.Server) && !string.IsNullOrEmpty(area.PrinterDescription))
                    {
                        OnPrintEvent(ticket, true, area.PrinterDescription, action);

                        var document = new KitchenDoc
                        {
                            Ticket = new PrintableTicket(ticket),
                            ActionDescription = action.Translate().ToUpper(),
                            AreaId = action == "VOID" ? Guid.Empty : areaId,
                            AreaDescription = area.Area,
                            OrderIds = orderIds,
                            ProductGrouping = TicketProductGrouping.SeparatorsSeats
                        };

                        var printer = area.GetPrinter();
                        printer.Setting.LeftMargin = area.MarginLeft;
                        printer.Setting.RigthMargin = area.MarginRight;
                        printer.Setting.LineFontSize = area.LineSize ?? 14;
                        printer.Setting.NoteFontSize = area.NoteLineSize ?? 9;
                        printer.Setting.ActionFontSize = area.ActionLineSize ?? 20;
                        printer.Setting.SaloonFontSize = area.SaloonLineSize ?? 14;
                        printer.Setting.SeatSeparatorFontSize = area.SeatSeparatorSize ?? 14;

                        var numberOfCopies = area.NumberOfCopies ?? 1;
                        for (var i = 0; i < numberOfCopies; i++)
                        {
                            // result.Add(TicketPrinter.PrintKitchenDoc(printer, document));
                            result.Add(RunInStaThread(() => TicketPrinter.PrintKitchenDoc(printer, document)));
                            OnPrintEvent(ticket, false, area.PrinterDescription, action,
                                !result.IsEmpty ? result.Message : null);
                        }
                    }
                    else
                    {
                        const string error = "Area not configured";
                        result.AddError(error);
                        OnPrintEvent(ticket, false, area.PrinterDescription, action, error);
                    }
                }
                else
                {
                    const string error = "Area not found";
                    result.AddError(error);
                    OnPrintEvent(ticket, false, null, action, error);
                }

                if (result.IsEmpty && standPrint)
                {
                    var config = TicketConfiguration(ticket.Stand);
                    var serverName = config.Server;
                    var printName = config.PrinterDescription;
                    if (!string.IsNullOrEmpty(serverName) && !string.IsNullOrEmpty(printName))
                    {
                        OnPrintEvent(ticket, true, printName, action);

                        var document = new KitchenDoc
                        {
                            Ticket = new PrintableTicket(ticket),
                            ActionDescription = action.Translate().ToUpper(),
                            AreaId = action == "VOID" ? Guid.Empty : areaId,
                            AreaDescription = area?.Area ?? "Unknown area",
                            OrderIds = orderIds,
                            ProductGrouping = TicketProductGrouping.SeparatorsSeats
                        };

                        var printer = config.GetPrinter();
                        printer.Setting.LeftMargin = config.MarginLeft;
                        printer.Setting.RigthMargin = config.MarginRight;

                        // result = TicketPrinter.PrintKitchenDoc(printer, document);
                        result.Add(RunInStaThread(() => TicketPrinter.PrintKitchenDoc(printer, document))); 
                        OnPrintEvent(ticket, false, printName, action, !result.IsEmpty ? result.Message : null);
                    }
                    else
                    {
                        const string error = "Area not configured";
                        result.AddError(error);
                        OnPrintEvent(ticket, false, printName, action, error);
                    }
                }
            }
            catch (Exception ex)
            {
                result.AddError(ex.Message);
            }

            return result;
        }

        private void OnPrintEvent(POSTicketContract ticket, bool initial, string printName, string action,
            string error = null)
        {
            var args = GetPrintArgs(initial, GetNumber(ticket), printName, action, error);
            PrintEvent?.Invoke(this, args);
            if (string.IsNullOrEmpty(error)) return;
            try
            {
                const string folderPath = "logs"; // Specify the folder path
                Directory.CreateDirectory(folderPath); // Create the folder if it doesn't exist

                var fileName = DateTime.Now.ToString("yyyy-MM-dd-HH_mm-ss-fff") + ".txt"; // Generate unique file name
                var filePath = Path.Combine(folderPath, fileName); // Combine folder path and file name

                // Save stack trace to a text file
                File.WriteAllText(filePath, error);
            }
            catch
            {
                // ignored
            }
        }

        private static string GetNumber(POSTicketContract ticket)
        {
            return (ticket.Number ?? 0).ToString();
        }

        private static PrintArgs GetPrintArgs(bool initial, string ticket, string printer, string operation,
            string error = null)
        {
            return new PrintArgs
            {
                Initial = initial,
                Ticket = ticket,
                Printer = printer,
                Operation = operation,
                Error = error
            };
        }

        private static ConfigModel LoadConfigFromFile(string file)
        {
            try
            {
                var serializer = new XmlSerializer(typeof(ConfigModel));
                var reader = new StreamReader(file);
                var result = serializer.Deserialize(reader) as ConfigModel;
                reader.Close();
                return result;
            }
            catch (Exception)
            {
                return new ConfigModel();
            }
        }

		private static ConfigModel LoadFromConfig()
		{
			try
			{
				var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                if (config.AppSettings.Settings.AllKeys.FirstOrDefault(x => "PrintConfigFile".Equals(x, StringComparison.OrdinalIgnoreCase)) is string keyName)
                {
					var file = config.AppSettings.Settings[keyName].Value;
					if (File.Exists(file))
					{
						return LoadConfigFromFile(file);
					}
				}
			}
			catch
			{
				// If cant read the config or file, continues with the previous logic.
			}

			try
			{
				var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
				var file = Path.Combine(path ?? string.Empty, "printers.xml");
				return LoadConfigFromFile(file);
			}
			catch (Exception)
			{
				return new ConfigModel();
			}
		}

		private static IEnumerable<DispatchArea> OrganizeProductsByArea(DispatchContract[] dispatchList)
        {
            var list = new List<DispatchArea>();

            // Organize the areas with products
            foreach (var dispatch in dispatchList)
            {
                // Get areas id
                var areaIds = new List<Guid>();
                if (dispatch.OrderIds.Length > 0)
                {
                    foreach (var orderId in dispatch.OrderIds)
                    {
                        foreach (var areaId in orderId.AreaIds)
                        {
                            if (!areaIds.Contains(areaId))
                            {
                                areaIds.Add(areaId);
                            }
                        }
                    }
                }
                else
                {
                    areaIds.AddRange(dispatch.AreaIds);
                }

                // Put order to each area 
                foreach (var areaId in areaIds)
                {
                    var index = list.FindIndex(el => areaId == el.AreaId);
                    if (index == -1)
                    {
                        var dispatchArea = new DispatchArea
                        {
                            AreaId = areaId,
                            OrderIds = new Dictionary<Guid, HashSet<Guid>>
                            {
                                {
                                    dispatch.OrderId,
                                    new HashSet<Guid>(dispatch.OrderIds
                                        .Where(x => x.AreaIds.Contains(areaId))
                                        .Select(x => x.OrderId))
                                }
                            }
                        };


                        list.Add(dispatchArea);
                    }
                    else
                    {
                        if (!list[index].OrderIds.ContainsKey(dispatch.OrderId))
                        {
                            list[index].OrderIds.Add(dispatch.OrderId,
                                new HashSet<Guid>(dispatch.OrderIds.Where(x => x.AreaIds.Contains(areaId))
                                    .Select(x => x.OrderId)));
                        }
                    }
                }
            }

            return list;
        }

        private static void SetupLanguage()
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var lang = "en-US";
            try
            {
                lang = config.AppSettings.Settings["LanguageCode"].Value;
            }
            catch
            {
                config.AppSettings.Settings.Add("LanguageCode", lang);
            }

            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
            LocalizationMgr.SetCultureInfo(lang);
        }

        #endregion

        private class DispatchArea
        {
            public Guid AreaId;
            public Dictionary<Guid, HashSet<Guid>> OrderIds;
        }
    }
}