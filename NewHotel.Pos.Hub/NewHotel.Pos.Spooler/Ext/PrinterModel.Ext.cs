﻿using NewHotel.Pos.Spooler.Common;

namespace NewHotel.Pos.Spooler.Ext
{
    public static class PrinterModelExt
    {
        public static PrinterDocument.Simple.Printer GetPrinter(this PrinterModel source)
        {
            return new PrinterDocument.Simple.Printer { Server = source.Server, Name = source.PrinterDescription };
        }
    }
}
