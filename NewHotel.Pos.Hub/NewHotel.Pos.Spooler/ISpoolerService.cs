﻿using System;
using System.ServiceModel;
using NewHotel.Contracts;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Contracts.Handheld.Contracts;

namespace NewHotel.Pos.Spooler
{
    [ServiceContract]
    public interface ISpoolerService 
    {
        [OperationContract]
        ValidationResult PrintOrders(string headerTitle, POSTicketContract ticket, DispatchContract[] orders, bool standPrint);
        [OperationContract]
        ValidationResult PrintTicket(POSTicketContract ticket, Settings settings, bool isInvoice);
    }
}
