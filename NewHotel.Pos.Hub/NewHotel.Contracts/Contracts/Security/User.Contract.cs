﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations; 

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(UserContract), "ValidateUserContract")]
    public class UserContract : BaseContract, ITwoFactorAutheticableUser
    {
        #region Private Members

        private Guid? _profileId;
        private string _login;
        private string _password;
        private Guid _mgrId;
        private string _code;
        private bool _profileDefault;
        private bool _profileOverride;
        private bool _profileModified;
        private string _fiscalNumber;
        private bool _groupAdministrator;
        private long? _languageId;
        private string _userDateFormat;
        private TypedList<UserPermissionContract> _userPermissions;

        [DataMember]
        internal int? _fiscalNumberLength;
        private bool _isEnableTwoFactorAuthentication;
        private string _keyTwoFactorAuthentication;

        #endregion
        #region Constructor

        public UserContract(int? fiscalNumberLength)
        {
            _fiscalNumberLength = fiscalNumberLength;
            _userPermissions = new TypedList<UserPermissionContract>();
            LoginPeriods = new TypedList<LoginPeriodContract>();
            Hotels = new TypedList<UserByHotelRecord>();
            UserByApplications = new TypedList<ApplicationByUserContract>();
            UserByStand = new TypedList<UserByStandContract>();
        }

        public UserContract()
            : this(null)
        {
        }

        #endregion
        #region Public Properties

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Login required.", AllowEmptyStrings = false)]
        public string Login { get { return _login; } set { Set(ref _login, value, "Login"); } }
        [DataMember]
        [System.ComponentModel.DataAnnotations.Display(Name = "Password", Description = "Please enter paswword.")]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Password required.", AllowEmptyStrings = false)]
        public string Password { get { return _password; } set { Set(ref _password, value, "Password"); } }
        [DataMember]
        public string Code { get { return _code; } set { Set(ref _code, value, "Code"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Description required.", AllowEmptyStrings = false)]
        public string Description { get; set; }
        [DataMember]
        public DateTime RegistrationDate { get; set; }
        [DataMember]
        public DateTime? PasswordExpirationDate { get; set; }
        [DataMember]
        public DateTime? UserExpirationDate { get; set; }
        [DataMember]
        public string UserDateFormat { get { return _userDateFormat; } set { Set(ref _userDateFormat, value, "UserDateFormat"); } }
        [DataMember]
        public Guid? ProfileId { get { return _profileId; } set { Set(ref _profileId, value, "ProfileId"); } }
        [DataMember]
        public bool ProfileModified { get { return _profileModified; } set { Set(ref _profileModified, value, "ProfileModified"); } }
        [DataMember]
        public bool ProfileDefault { get { return _profileDefault; } set { Set(ref _profileDefault, value, "ProfileDefault"); } }
        [DataMember]
        public string FiscalNumber { get { return _fiscalNumber; } set { Set(ref _fiscalNumber, value, "FiscalNumber"); } }
        [DataMember]
        public bool Inactive { get; set; }
        [DataMember]
        public bool Manipulatebox { get; set; }
        [DataMember]
        public bool UseNumericLogin { get; set; }
        [DataMember]
        public bool GroupAdministrator { get { return _groupAdministrator; } set { Set(ref _groupAdministrator, value, "GroupAdministrator"); } }
        [DataMember]
        public bool ProfileOverride { get { return _profileOverride; } set { Set(ref _profileOverride, value, "ProfileOverride"); } }
        [DataMember]
        public long? LanguageId { get { return _languageId; } set { Set(ref _languageId, value, "LanguageId"); } }

        [DataMember]
        public TypedList<HotelUserRecord> AddedHotels { get; set; }
        [DataMember]
        public TypedList<UserByHotelRecord> Hotels { get; set; }
        [DataMember]
        public TypedList<LoginPeriodContract> LoginPeriods { get; set; }
        [DataMember]
        public TypedList<ApplicationByUserContract> UserByApplications { get; set; }
        [DataMember]
        public TypedList<UserByStandContract> UserByStand { get; set; }
        [DataMember]
        public TypedList<UserPermissionContract> UserPermissions
        {
            get { return _userPermissions; }
            set
            {
                if (Set(ref _userPermissions, value, "UserPermissions"))
                {
                    if (UserPermissions != null)
                        NotifyPropertyChanged("Applications");
                }
            }
        }

        [DataMember]
        public long[] AllowedToChangeApplications { get; set; }

        [IgnoreDataMember]
        public bool UserLocked { get; set; }
        [IgnoreDataMember]
        public bool PasswordExpired { get; set; }

        #region ITwoFactorAutheticableUser Implementation

        [DataMember]
        public Guid MgrId { get { return _mgrId; } set { Set(ref _mgrId, value, "MgrId"); } }
        [DataMember]
        public bool IsEnableTwoFactorAuthentication
        {
            get { return _isEnableTwoFactorAuthentication; }
            set { Set(ref _isEnableTwoFactorAuthentication, value, nameof(IsEnableTwoFactorAuthentication)); }
        }
        [DataMember]
        public string KeyTwoFactorAuthentication
        {
            get { return _keyTwoFactorAuthentication; }
            set { Set(ref _keyTwoFactorAuthentication, value, nameof(KeyTwoFactorAuthentication)); }
        }

        #endregion

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public Guid[] UserPermissionsIds
        {
            get { return UserPermissions.Select(x => (Guid)x.Id).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] LoginPeriodIds
        {
            get { return LoginPeriods.Select(x => (Guid)x.Id).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] UserApplicationsIds
        {
            get { return UserPermissions.Select(x => (Guid)x.PermissionApplicationId).ToArray(); }
        }

        [ReflectionExclude]
        public IEnumerable<ApplicationModuleContract> Applications
        {
            get
            {
                return UserByApplications
                    .Select(x => new ApplicationModuleContract
                    {
                        Id = x.ApplicationId,
                        Description = x.ApplicationDescription
                    }).OrderBy(x => x.Id);
            }
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateUserContract(UserContract obj)
        {
            if (string.IsNullOrEmpty(obj.Password))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Password Required");
            if (obj._fiscalNumberLength.HasValue && (obj.FiscalNumber ?? string.Empty).Length != obj._fiscalNumberLength)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid fiscal number");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}