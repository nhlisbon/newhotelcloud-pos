﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class LoginPeriodContract : BaseContract
    {
        #region Members

        private DateTime _initial;
        private DateTime _final;

        #endregion
        #region Public Properties

        [DataMember]
        public DateTime Initial { get { return _initial; } set { Set(ref _initial, value, "Initial"); } }
        [DataMember]
        public DateTime Final { get { return _final; } set { Set(ref _final, value, "Final"); } }

        #endregion
        #region Visual Properties

        public string InitialGrid { get { return Initial.ToString("HH:mm"); } }
        public string FinalGrid { get { return Final.ToString("HH:mm"); } }

        #endregion
        #region Public Methods

        public bool Overlap(LoginPeriodContract contract)
        {
            var start = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, Initial.Hour, Initial.Minute, Initial.Second);
            var end = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, Final.Hour, Final.Minute, Final.Second);
            var contractInitial = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, contract.Initial.Hour, contract.Initial.Minute, contract.Initial.Second);
            var contractFinal = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, contract.Final.Hour, contract.Final.Minute, contract.Final.Second);

            if (contractInitial >= start && contractInitial <= end) return true;
            if (contractFinal >= start && contractFinal <= end) return true;
            if (contractInitial <= start && contractFinal >= end) return true;

            return false;
        }

        public bool ValidPeriod()
        {
            var start = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, Initial.Hour, Initial.Minute, Initial.Second);
            var end = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, Final.Hour, Final.Minute, Final.Second);

            return (start < end);
        }

        public bool ValidHour(TimeZoneInfo timeZone, DateTime date)
        {
            //date is Now in Server
            //Contract has hour in timeZone
            //move date to timeZone to compare

            var nowInClient = TimeZoneInfo.ConvertTime(date, timeZone);
            var start = new DateTime(nowInClient.Year, nowInClient.Month, nowInClient.Day, Initial.Hour, Initial.Minute, Initial.Second);
            var end = new DateTime(nowInClient.Year, nowInClient.Month, nowInClient.Day, Final.Hour, Final.Minute, Final.Second);
            var now = new DateTime(nowInClient.Year, nowInClient.Month, nowInClient.Day, nowInClient.Hour, nowInClient.Minute, nowInClient.Second);

            return (start <= now && end >= now);
        }

        #endregion
        #region Constructor

        public LoginPeriodContract()
        {
            Initial = Final = DateTime.Today;
        }

        #endregion
    }
}