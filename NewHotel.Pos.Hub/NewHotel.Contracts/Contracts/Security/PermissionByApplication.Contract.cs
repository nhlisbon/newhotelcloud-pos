﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class PermissionByApplicationContract : BaseContract
    {
        #region Properties

        [DataMember]
        public TypedList<UserPermissionContract> UserPermissions { get; set; }

        #endregion
        #region Constructor

        public PermissionByApplicationContract()
        {
            UserPermissions = new TypedList<UserPermissionContract>();
        }

        #endregion
    }
}