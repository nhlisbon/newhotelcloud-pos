﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class UserPermissionContract : BaseContract
    {
        #region Members

        private SecurityCode _securityCode;

        #endregion
        #region Properties

        [DataMember]
        public Guid PermissionApplicationId { get; set; }
        [DataMember]
        public Guid PermissionId { get; set; }
        [DataMember]
        public string PermissionDescription { get; set; }
        [DataMember]
        public long ApplicationId { get; set; }
        [DataMember]
        public string ApplicationName { get; set; }       
        [DataMember]
        public Guid GroupId { get; set; }
        [DataMember]
        public string Group { get; set; }
        [DataMember]
        public short GroupOrder { get; set; }
        [DataMember]
        public long PermissionOrder { get; set; }
        [DataMember]
        public SecurityCode SecurityCode { get { return _securityCode; } set { Set(ref _securityCode, value, "SecurityCode"); } }

        #endregion
        #region Constructor

        public UserPermissionContract() { }

        #endregion
    }
}