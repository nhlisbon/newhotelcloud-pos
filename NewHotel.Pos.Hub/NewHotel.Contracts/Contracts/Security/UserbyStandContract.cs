﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(UserByStandContract), "ValidateUserbyStand")]
    public class UserByStandContract : BaseContract
    {
        #region Members

        private Guid _stand;
        private Guid _userId;
        private SecurityCode _securitycode;

        #endregion
        #region Properties

        [DataMember]
        public Guid Stand { get { return _stand; } set { Set(ref _stand, value, "Stand"); } }
        [DataMember]
        public string StandDesc { get; set; }
        [DataMember]
        public Guid UserId { get { return _userId; } set { Set(ref _userId, value, "UserId"); } }
        [DataMember]
        public string UserLogin { get; set; }
        [DataMember]
        public SecurityCode SecurityCode { get { return _securitycode; } set { Set(ref _securitycode, value, "SecurityCode"); } }

        #endregion
        #region Constructor

        public UserByStandContract()
            : base()
        {
        }

        public UserByStandContract(Guid id, Guid stand, string standDesc, Guid userId, string userLogin, SecurityCode securitycode)
            : base()
        {
            Id = id;
            Stand = stand;
            StandDesc = standDesc;
            UserId = userId;
            UserLogin = userLogin;
            SecurityCode = securitycode;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateUserbyStand(UserByStandContract obj)
        {
            if (obj.Stand == new Guid())
                return new System.ComponentModel.DataAnnotations.ValidationResult("Stand required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}