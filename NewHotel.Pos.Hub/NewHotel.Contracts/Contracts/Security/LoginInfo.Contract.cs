﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class LoginInfo : BaseContract
    {
        #region Members

        internal Guid? _userId;
        internal string _userName;
        internal bool _rememberMe;
        internal Guid? _installationId;
        internal string _installationUrl;
        internal string _installationName;
        internal string _installationCommercialName;
        internal string _installationFiscalName;
        internal long? _languageId;
        private short _rolId;

        private Guid? _userInstallationId;
        private string _code;

        #endregion
        #region Properties

        public string CultureName
        {
            get
            {
                if (_languageId.HasValue)
                {
                    switch (_languageId.Value)
                    {
                        case 1033: return "en-US";
                        case 2070: return "pt-PT";
                        case 3082: return "es-ES";
                        case 1036: return "fr-FR";
                        case 1063: return "lt-LT";
                        case 1040: return "it-IT";
                        case 1049: return "ru-RU";
                        case 1027: return "ca-ES";
                        case 1031: return "de-DE";
                        case 1062: return "lv-LV";
                        case 2052: return "zh-CN";
                        case 1046: return "pt-BR";
                        case 12297: return "en-ZW";
                        case 1043: return "nl-NL";
                        case 1055: return "tr-TR";
                        case 1065: return "fa-Ir";
                    }
                }
                return string.Empty;
            }
        }

        [System.ComponentModel.DataAnnotations.Display(Name = "Username", Description = "Please enter username.")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Username required.", AllowEmptyStrings = false)]
        [System.ComponentModel.DataAnnotations.StringLength(50, MinimumLength = 3, ErrorMessage = "Invalid username length.")]
        public string UserName
        {
            get { return _userName; }
            set { Set(ref _userName, value, "UserName"); }
        }

        public Guid? UserId
        {
            get { return _userId; }
            set { Set(ref _userId, value, "UserId"); }
        }

        public short RolId
        {
            get { return _rolId; }
            set { Set(ref _rolId, value, "RolId"); }
        }

        /// <summary>
        /// Gets or sets a function that returns the password.
        /// </summary>
        public Func<string> PasswordAccessor { get; set; }

        /// <summary>
        /// Gets and sets the password.
        /// </summary>
        [System.ComponentModel.DataAnnotations.Display(Name = "Password", Description = "Please enter paswword.")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Password required.", AllowEmptyStrings = false)]
        public string Password
        {
            get { return this.PasswordAccessor == null ? string.Empty : this.PasswordAccessor(); }
            set
            {
                // Do not store the password in a private field as it should
                // not be stored in memory in plain-text.  Instead, the supplied
                // PasswordAccessor serves as the backing store for the value.
                if (ValidateProperty(this.Password, "Password"))
                    NotifyPropertyChanged("Password");
            }
        }

        /// <summary>
        /// Gets and sets the password.
        /// </summary>
        [System.ComponentModel.DataAnnotations.Display(Name = "Code", Description = "Please enter code.")]
        public string Code
        {
            get { return _code; }
            set { Set(ref _code, value, nameof(Code)); }
        }

        [System.ComponentModel.DataAnnotations.Display(Name = "Remember me")]
        public bool RememberMe
        {
            get { return _rememberMe; }
            set { _rememberMe = value; }
        }

        [System.ComponentModel.DataAnnotations.Display(Name = "Installation", Description = "Please select current installation.")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Installation required.", AllowEmptyStrings = false)]
        public Guid? InstallationId
        {
            get { return _installationId; }
            set { Set(ref _installationId, value, "InstallationId"); }
        }

        public string InstallationUrl
        {
            get { return _installationUrl; }
            set { Set(ref _installationUrl, value, "InstallationUrl"); }
        }

        public string InstallationName
        {
            get { return _installationName; }
            set { Set(ref _installationName, value, "InstallationName"); }
        }

        public string InstallationCommercialName
        {
            get { return _installationCommercialName; }
            set { Set(ref _installationCommercialName, value, "InstallationCommercialName"); }
        }

        public string InstallationFiscalName
        {
            get { return _installationFiscalName; }
            set { Set(ref _installationFiscalName, value, "InstallationFiscalName"); }
        }

        [System.ComponentModel.DataAnnotations.Display(Name = "Language", Description = "Please select language.")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Language required.", AllowEmptyStrings = false)]
        public long? LanguageId
        {
            get { return _languageId; }
            set { Set(ref _languageId, value, "LanguageId"); }
        }
        #endregion

        public Guid? UserInstallationId
        {
            get { return _userInstallationId; }
            set { Set(ref _userInstallationId, value, "UserInstallationId"); }
        }

        public UserAppContract UserContract { get; set; }
    }
}
