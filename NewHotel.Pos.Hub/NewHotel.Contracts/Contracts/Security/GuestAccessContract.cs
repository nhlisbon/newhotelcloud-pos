﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class GuestAccessContract : BaseContract
    {
        #region Private Members

        private Guid _user;
        private Guid _installationId;
        private bool _inactive;
        private string _hotelDescription;
        private string _userDescription;

        #endregion
        #region Constructor

        public GuestAccessContract()
        {
        }

        #endregion
        #region Public Properties

        [DataMember]
        public string HotelDescription { get { return _hotelDescription; } set { Set(ref _hotelDescription, value, "HotelDescription"); } }
        [DataMember]
        public string UserDescription { get { return _userDescription; } set { Set(ref _userDescription, value, "UserDescription"); } }
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }
        [DataMember]
        public Guid InstallationId { get { return _installationId; } set { Set(ref _installationId, value, "InstallationId"); } }
        [DataMember]
        public Guid User { get { return _user; } set { Set(ref _user, value, "User"); } }

        #endregion    
    }
}