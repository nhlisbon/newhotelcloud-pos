﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PermissionAuditContract : BaseContract
    {
        public PermissionAuditContract() { }
        public PermissionAuditContract(Guid id, Guid installationId, Guid permissionApplicationId, 
            bool auditable, string profile, string group)
        {
            this.Id = id;
            this.InstallationId = installationId;
            this.PermissionApplicationId = permissionApplicationId;
            this.Auditable = auditable;
            this.ProfileDescription = profile;
            this.GroupDescription = group;
        }

        [DataMember]
        public Guid InstallationId { get; set; }
        [DataMember]
        public Guid PermissionApplicationId { get; set; }
        [DataMember]
        public bool Auditable { get; set; }
        
        [IgnoreDataMember]
        public string ProfileDescription { get; set; }
        [IgnoreDataMember]
        public string GroupDescription { get; set; }
    }
}
