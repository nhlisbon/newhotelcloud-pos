﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PermissionApplicationContract : BaseContract
    {
        [DataMember]
        public Guid PermissionId { get; set; }
        [DataMember]
        public long ApplicationId { get; set; }
    }
}