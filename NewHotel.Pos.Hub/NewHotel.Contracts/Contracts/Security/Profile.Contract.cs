﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class ApplicationProfileContract : BaseContract
    {
        private InitialPanel? _initialPanel;
        [DataMember]
        internal long _applicationId;

        [DataMember]
        public InitialPanel? InitialPanel { get { return _initialPanel; } set { Set(ref _initialPanel, value, "InitialPanel"); } }

        public long ApplicationId { get { return _applicationId; } }

        public ApplicationProfileContract(long applicationId)
        {
            _applicationId = applicationId;
        }
    }

    [DataContract]
	[Serializable]
    public class ProfileContract : BaseContract
    {
        [DataMember]
        internal LanguageTranslationContract _description;

        public ProfileContract()
        {
            _description = new LanguageTranslationContract();
            ProfilePermissions = new TypedList<ProfilePermissionContract>();
            ApplicationProfiles = new TypedList<ApplicationProfileContract>();
            ApplicationProfiles.Add(new ApplicationProfileContract(Applications.NewHotelPms));
        }

        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
        }

        [DataMember]
        public TypedList<ProfilePermissionContract> ProfilePermissions{ get; set; }

        [DataMember]
        public TypedList<ApplicationProfileContract> ApplicationProfiles { get; set; }

        [ReflectionExclude]
        public Guid[] ProfilePermissionsIds
        {
            get { return ProfilePermissions.Select(x => (Guid)x.Id).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] ProfileApplicationIds
        {
            get { return ProfilePermissions.Select(x => x.ProfileApplicationId).ToArray(); }
        }

        [ReflectionExclude]
        public List<ApplicationModuleContract> ProfileApplications
        {
            get
            {
                return ProfilePermissions.Select(x => new { x.ApplicationId, x.ApplicationName }).Distinct()
                    .Select(x => new ApplicationModuleContract
                    {
                        Id = x.ApplicationId,
                        Description = x.ApplicationName
                    }).ToList();
            }
        }

        public InitialPanel? GetInitialPanel(long applicationId)
        {
            var applicationProfile = ApplicationProfiles.FirstOrDefault(x => x.ApplicationId == applicationId);
            if (applicationProfile != null)
                return applicationProfile.InitialPanel;

            return null;
        }
    }
}
