﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PermissionContract : BaseContract
    {
        #region Properties

        [DataMember]
        public Guid PermissionGroupId { get; internal set; }
        [DataMember]
        public string Description { get; internal set; }

        #endregion
        #region Constructor

        public PermissionContract(Guid permissionGroupId, string description) : base() 
        {
            PermissionGroupId = permissionGroupId;
            Description = description;
        }

        #endregion
    }
}
