﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Contracts
{
    public interface ITwoFactorAutheticableUser : INotifyPropertyChanged
    {
        Guid MgrId { get; set; }
        bool IsEnableTwoFactorAuthentication { get; set; }
        string KeyTwoFactorAuthentication { get; set; }

    }
}
