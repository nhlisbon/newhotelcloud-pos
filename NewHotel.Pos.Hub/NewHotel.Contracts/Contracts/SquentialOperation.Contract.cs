﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public class OperationCompletionTemplate
    {
        public string StartTemplate;
        public string SuccessTemplate;
        public string FailedTemplate;
        public string AbortedTemplate;
    }

    [DataContract]
	[Serializable]
    public class SequentialOperContract : BaseContract<object>
    {
        #region Members

        public string _operationName;
        private bool _execute;
        private DateTime? _startDateTime;
        private DateTime? _endDateTime;
        private bool _executed;
        private bool _successfull;
        private bool _aborted;
        private int _index;
        private int _count;
        private string _details;

        #endregion
        #region Properties

        [DataMember]
        public bool Mandatory { get; internal set; }
        [DataMember]
        public string OperationName { get { return _operationName; } set { Set(ref _operationName, value, "OperationName"); } } 
        [DataMember]
        public DateTime? StartDateTime { get { return _startDateTime; } set { Set(ref _startDateTime, value, "StartDateTime"); } }
        [DataMember]
        public DateTime? EndDateTime { get { return _endDateTime; } set { Set(ref _endDateTime, value, "EndDateTime"); } }
        [DataMember]
        public bool Execute { get { return _execute; } set { Set(ref _execute, value, "Execute"); } }
        [DataMember]
        public bool Executed { get { return _executed; } set { Set(ref _executed, value, "Executed"); } }
        [DataMember]
        public bool Successfull { get { return _successfull; } set { Set(ref _successfull, value, "Successfull"); } }
        [DataMember]
        public bool Aborted { get { return _aborted; } set { Set(ref _aborted, value, "Aborted"); } }
        [DataMember]
        public int Index { get { return _index; } set { Set(ref _index, value, "Index"); } }
        [DataMember]
        public int Count { get { return _count; } set { Set(ref _count, value, "Count"); } }
        [DataMember]
        public string Details { get { return _details; } set { Set(ref _details, value, "Details"); } }

        #endregion
        #region Extended Members

        [DataMember]
        internal readonly OperationCompletionTemplate OperCompletionTemplate;

        [ReflectionExclude]
        public string Header
        {
            get
            {
                if (StartDateTime.HasValue)
                    return string.Format(OperCompletionTemplate.StartTemplate,
                        OperationName, StartDateTime.Value.ToLongTimeString());
                else
                    return string.Empty;
            }
        }

        [ReflectionExclude]
        public string Footer
        {
            get
            {
                if (Aborted)
                    return string.Format(OperCompletionTemplate.AbortedTemplate, OperationName);
                else
                {
                    if (EndDateTime.HasValue)
                        return string.Format(Successfull
                            ? OperCompletionTemplate.SuccessTemplate : OperCompletionTemplate.FailedTemplate,
                            OperationName, EndDateTime.Value.ToLongTimeString());
                    else
                        return string.Empty;
                }
            }
        }

        [ReflectionExclude]
        public string Log
        {
            get 
            {
                const string CRLF = "\r\n";
                return Header + CRLF + Details + Footer + CRLF;
            }
        }

        #endregion
        #region Public Methods

        public void CopyTo(SequentialOperContract contract)
        {
            contract.StartDateTime = StartDateTime;
            contract.EndDateTime = EndDateTime;
            contract.Execute = Execute;
            contract.Executed = Executed;
            contract.Successfull = Successfull;
            contract.Aborted = Aborted;
            contract.Index = Index;
            contract.Count = Count;
            contract.Details = Details;
        }

        #endregion
        #region Constructors

        public SequentialOperContract(object id, string operName, bool mandatory,
            OperationCompletionTemplate operCompletionTemplate)
            : base()
        {
            Id = id;
            OperationName = operName;
            Mandatory = mandatory;
            Details = string.Empty;
            Execute = true;
            OperCompletionTemplate = operCompletionTemplate;            
        }

        public SequentialOperContract(object id, string operName, bool mandatory,
            OperationCompletionTemplate operCompletionTemplate, int count)
            : this(id, operName, mandatory, operCompletionTemplate)
        {
            Count = count;
        }

        #endregion
    }
}
