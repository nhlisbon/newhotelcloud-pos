﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ReportKeywordContract : BaseContract<long>
    {
        private short _priority;
        private long _reportId;
        private short _keywordId;

        [DataMember]
        public short Priority { get { return _priority; } set { Set(ref _priority, value, "Priority"); } }
        [DataMember]
        public long ReportId { get { return _reportId; } set { Set(ref _reportId, value, "ReportId"); } }
        [DataMember]
        public short KeywordId { get { return _keywordId; } set { Set(ref _keywordId, value, "KeywordId"); } }
    }
}
