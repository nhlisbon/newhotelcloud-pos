﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class KeywordContract : BaseContract<short>
    {
        private string _description;

        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
    }
}
