﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ReportContract : BaseContract<long>
    {
        #region Members

        private string _title;
        private string _description;
        private string _reportName;
        private bool _allowFilter;
        private bool _allowEnter;
        private bool _enableFavorite;
        private bool _ReportEnabled;
        public Guid _lite;
       
        #endregion

        #region Constructor
        public ReportContract()
        {
            this.Keywords = new TypedList<ReportKeywordContract>();
            this.Categories = new TypedList<EReportType>();
            this._ReportEnabled = true;
        }
        #endregion

        #region Properties
       
        [DataMember]
        public string Title { get { return _title; } set { Set(ref _title, value, "Title"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public string ReportName { get { return _reportName; } set { Set(ref _reportName, value, "ReportName"); } }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public bool Favorite { get { return _enableFavorite; } set { Set(ref _enableFavorite, value, "Favorite"); } }
        [DataMember]
        public bool AllowFilter { get { return _allowFilter; } set { Set(ref _allowFilter, value, "AllowFilter"); } }
        [DataMember]
        public bool AllowEnter { get { return _allowEnter; } set { Set(ref _allowEnter, value, "AllowEnter"); } }
        public bool IsDaily { get { return Categories.Contains(EReportType.Daily); } }
        public bool IsGraphic { get { return Categories.Contains(EReportType.Graphic); } }
        public bool IsPeriod { get { return Categories.Contains(EReportType.Period); } }
        public bool IsSingleDay { get { return Categories.Contains(EReportType.SingleDay); } }
        public bool IsSummary { get { return Categories.Contains(EReportType.Summary); } }
        [DataMember]
        public int KeyWordWeigth { get; set; }
        [DataMember]
        public int PriorityWeight { get; set; }
        [DataMember]
        public int PrecedenceWeight { get; set; }

        [DataMember]
        public bool ReportEnabled { get { return _ReportEnabled; } set { Set(ref _ReportEnabled, value, "ReportEnabled"); } }

        [DataMember]
        public Guid Lite { get { return _lite; } set { Set(ref _lite, value, "Lite"); } }
        #endregion

        #region Lists
        [DataMember]
        public TypedList<ReportKeywordContract> Keywords { get; set; }
        [DataMember]
        public TypedList<EReportType> Categories { get; set; }
        #endregion

        #region Methods

        public void RefreshPriority(IList<KeywordContract> keywords)
        {
            Priority = 0;
            for (int i = 0; i < keywords.Count; i++)
			{
                var keyword = keywords[i];
                foreach (var repoKeyword in Keywords)
                {
                    if (repoKeyword.KeywordId == (short)keyword.Id)
                    {
                        //contains keyword 
                        Priority += KeyWordWeigth;
                        //keyword priority
                        Priority += (PriorityWeight - repoKeyword.Priority);
                        //keyword precedence
                        Priority += (PrecedenceWeight - i);
                        break;
                    }
                }
			}
        }

        public void RefreshPriority(short keywordId)
        {
            Priority = 0;
            foreach (var repoKeyword in Keywords)
            {
                if (repoKeyword.KeywordId == keywordId)
                {
                    //contains keyword 
                    Priority += KeyWordWeigth;
                    //keyword priority
                    Priority += (PriorityWeight - repoKeyword.Priority);
                    break;
                }
            }
        }
      
        #endregion        
    }
}