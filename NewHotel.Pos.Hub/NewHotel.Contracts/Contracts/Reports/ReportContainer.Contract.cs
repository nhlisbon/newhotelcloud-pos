﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ReportContainerContract : BaseContract
    {
        #region Constructor

        public ReportContainerContract()
        {
            Reports = new TypedList<ReportContract>();
            Keywords = new TypedList<KeywordContract>();
        } 

        #endregion
        #region Properties

        [DataMember]
        public TypedList<ReportContract> Reports { get; set; }
        [DataMember]
        public TypedList<KeywordContract> Keywords { get; set; }
        [DataMember]
        public string FilterText { get; set; }
        [DataMember]
        public int ApplicationId { get; set; }

        #endregion
        #region Public Methods

        public IList<ReportContract> FilterReports(IList<KeywordContract> keywords = null, Func<ReportContract, bool> predicate = null)
        {
            IEnumerable<ReportContract> reports = Reports;

            if (keywords != null && keywords.Count > 0)
            {
                foreach (var report in reports)
                    report.RefreshPriority(keywords);

                reports = reports.Where(r => r.Priority > 0);
            }

            if (!string.IsNullOrEmpty(FilterText))
                reports = reports.Where(r => Match(r, FilterText));

            if (predicate != null)
                reports = reports.Where(predicate);

            return reports.OrderByDescending(x => x.Priority).ToList();
        }

        #endregion
        #region Private Methods

        private static bool Match(ReportContract contract, string pattern)
        {
            if (string.IsNullOrEmpty(pattern))
                return true;

            return Regex.IsMatch(contract.Title, pattern, RegexOptions.IgnoreCase) ||
                   Regex.IsMatch(contract.Description, pattern, RegexOptions.IgnoreCase);
        }

        #endregion
    }
}