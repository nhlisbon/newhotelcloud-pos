﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(CentralContract), "ValidateCentralContract")]
    public class CentralContract : BaseContract
    {
        #region Members

        private string _centralName;
        private string _centralCode;
        private bool _active;
        private string _schemaName;
        private string _serviceName;
        private string _systemPassword;

        #endregion
        #region Constructor

        public CentralContract()
        {
            Hotels = new TypedList<HotelByCentralContract>();
        }

        #endregion
        #region Properties

        [DataMember]
        public string CentralName { get { return _centralName; } set { Set(ref _centralName, value, "CentralName"); } }
        [DataMember]
        public string CentralCode { get { return _centralCode; } set { Set(ref _centralCode, value, "CentralCode"); } }
        [DataMember]
        public bool Active { get { return _active; } set { Set(ref _active, value, "Active"); } }
        [DataMember]
        public string SchemaName { get { return _schemaName; } set { Set(ref _schemaName, value, "SchemaName"); } }
        [DataMember]
        public string ServiceName { get { return _serviceName; } set { Set(ref _serviceName, value, "ServiceName"); } }
        [DataMember]
        public string SystemPassword { get { return _systemPassword; } set { Set(ref _systemPassword, value, "SystemPassword"); } }

        #endregion
        #region List

        [ReflectionExclude]
        [DataMember]
        public TypedList<HotelByCentralContract> Hotels { get; set; }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateCentralContract(CentralContract obj)
        {
            if (String.IsNullOrEmpty(obj.CentralName)) return new System.ComponentModel.DataAnnotations.ValidationResult("Name required");
            if (String.IsNullOrEmpty(obj.CentralCode)) return new System.ComponentModel.DataAnnotations.ValidationResult("Code required");
            if (String.IsNullOrEmpty(obj.ServiceName)) return new System.ComponentModel.DataAnnotations.ValidationResult("Service name required");
            if (String.IsNullOrEmpty(obj.SystemPassword)) return new System.ComponentModel.DataAnnotations.ValidationResult("System password required");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
