﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class HotelByCentralContract : BaseContract
    {
        #region Properties
        [DataMember]
        public Guid CentralId { get; set; }
        [DataMember]
        public Guid HotelId { get; set; }
        [DataMember]
        public string HotelDescription { get; set; }
        [DataMember]
        public bool Inactive { get; set; }
        #endregion
        #region Constructor
        public HotelByCentralContract(Guid id, Guid centralId, Guid hotelId, string hotelDescription, bool inactive)
            : base()
        {
            Id = id;
            CentralId = centralId;
            HotelId = hotelId;
            HotelDescription = hotelDescription;
            Inactive = inactive;
        } 
        #endregion

    }
}
