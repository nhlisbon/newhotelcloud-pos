﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class TupleContract<T1, T2>
    {
        public TupleContract(T1 item1, T2 item2)
        {
            Item1 = item1;
            Item2 = item2;
        }

        public TupleContract()
            : this(default(T1), default(T2))
        {
        }

        [DataMember]
        public T1 Item1 { get; set; }
        [DataMember]
        public T2 Item2 { get; set; }
    }

    [DataContract]
    [Serializable]
    public class TupleContract<T1, T2, T3>
    {
        public TupleContract(T1 item1, T2 item2, T3 item3)
        {
            Item1 = item1;
            Item2 = item2;
            Item3 = item3;
        }

        public TupleContract()
            : this(default(T1), default(T2), default(T3))
        {
        }

        [DataMember]
        public T1 Item1 { get; set; }
        [DataMember]
        public T2 Item2 { get; set; }
        [DataMember]
        public T3 Item3 { get; set; }
    }
}
