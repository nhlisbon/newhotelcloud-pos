﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SequentialExecContract : BaseContract
    {
        #region Properties

        [DataMember]
        public Request Request { get; internal set; }
        [DataMember]
        public string Header { get; set; }
        [DataMember]
        public string Footer { get; set; }
        [DataMember]
        public bool Successfull { get; set; }
        [DataMember]
        public bool StopOnError { get; set; }
        [DataMember]
        public DateTime WorkDate { get; internal set; }

        #endregion
        #region Contracts

        [ReflectionExclude]
        [DataMember]
        public TypedList<SequentialOperContract> Operations { get; set; }

        #endregion
        #region Extended Properties

        private string GetLog(bool detailed)
        {
            const string indent = "   ";
            const string CRLF = "\r\n";
            var log = new StringBuilder();

            log.AppendLine(Header);
            foreach (var oper in Operations)
            {
                log.AppendLine(oper.Header);
                if (detailed)
                {
                    foreach (var detail in oper.Details.ToString()
                        .Split(new string[] { CRLF }, StringSplitOptions.RemoveEmptyEntries))
                        log.AppendLine(indent + detail.Trim());
                }
                log.AppendLine(oper.Footer);
                log.AppendLine();
            }
            log.AppendLine(Footer);

            return log.ToString();
        }

        public string SummaryLog
        {
            get { return GetLog(false); }
        }

        public string DetailedLog
        {
            get { return GetLog(true); }
        }

        #endregion
        #region Public Methods

        public void Update(SequentialExecContract contract)
        {
            for (var index = 0; index < contract.Operations.Count; index++)
                contract.Operations[index].CopyTo(Operations[index]);
        }

        #endregion
        #region Constructors

        public SequentialExecContract(DateTime workDate)
            : base() 
        {
            Request = new Request();
            Header = string.Empty;
            Footer = string.Empty;
            WorkDate = workDate;
            Operations = new TypedList<SequentialOperContract>();
        }

        #endregion
    }
}
