﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TemplateContract : BaseContract
    {
        #region Members
        private Clob _distributorTemplate;
        private Clob _clientTemplate;
        private bool _default;
        private string _description;
        #endregion
        #region Properties
        [DataMember]
        public Clob DistributorTemplate { get { return _distributorTemplate; } set { Set(ref _distributorTemplate, value, "DistributorTemplate"); } }
        [DataMember]
        public Clob ClientTemplate { get { return _clientTemplate; } set { Set(ref _clientTemplate, value, "ClientTemplate"); } }
        [DataMember]
        public bool Default { get { return _default; } set { Set(ref _default, value, "Default"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        #endregion
        #region Public Methods
        public string GenerateClientHTML(string user, string username, string password, string dateTime)
        {
            string result = ClientTemplate;

            result = result.Replace("[USER]", user);
            result = result.Replace("[USERNAME]", username);
            result = result.Replace("[PASSWORD]", password);
            result = result.Replace("[DATETIME]", dateTime);

            return result;
        }
        public string GenerateDistributorHTML(string username, string password, string dateTime)
        {
            string result = DistributorTemplate;

            result = result.Replace("[USERNAME]", username);
            result = result.Replace("[PASSWORD]", password);
            result = result.Replace("[DATETIME]", dateTime);

            return result;
        }
        #endregion
    }
}
