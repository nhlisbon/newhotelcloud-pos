﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.Contracts;
using System.Xml.Serialization;
using System.IO;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(JoinRequestContract), "ValidateJoinRequest")]
    public class JoinRequestContract : BaseContract
    {
        #region Contract properties

        [DataMember]
        public bool IsTrial { get; set; }
        [DataMember]
        public string HotelName { get; set; }
        [DataMember]
        public string ContactName { get; set; }
        [DataMember]
        public string HotelEmail { get; set; }
        [DataMember]
        public string HotelUrl { get; set; }
        [DataMember]
        public string HotelTelephone { get; set; }
        [DataMember]
        public string HotelCountry { get; set; }
        [DataMember]
        public long RoomAmount { get; set; }
        [DataMember]
        public decimal BillValue { get; set; }
        [DataMember]
        public string BillCurrency { get; set; }
        [DataMember]
        public string FiscalName { get; set; }
        [DataMember]
        public string FiscalAddress { get; set; }
        [DataMember]
        public string FiscalLocal { get; set; }
        [DataMember]
        public string FiscalNumber { get; set; }
        [DataMember]
        public string FiscalPostalCode { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string HotelCity { get; set; }
        [DataMember]
        public string HotelAddress { get; set; }
        [DataMember]
        public string HotelCompany { get; set; }
        [DataMember]
        public DateTime SuggestedDate { get; set; }
        [DataMember]
        public DateTime BestTime { get; set; }


        #endregion
        #region Constructor

        public JoinRequestContract()
            : base()
        {

        }

        #endregion
        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateJoinRequest(JoinRequestContract obj)
        {
            if (String.IsNullOrEmpty(obj.HotelName)) return new System.ComponentModel.DataAnnotations.ValidationResult("Hotel Name can't be empty.");
            if (String.IsNullOrEmpty(obj.ContactName)) return new System.ComponentModel.DataAnnotations.ValidationResult("Contact Name can't be empty.");
            if (String.IsNullOrEmpty(obj.HotelEmail)) return new System.ComponentModel.DataAnnotations.ValidationResult("Hotel Email can't be empty.");
            if (String.IsNullOrEmpty(obj.HotelTelephone)) return new System.ComponentModel.DataAnnotations.ValidationResult("Hotel Phone can't be empty.");
            if (String.IsNullOrEmpty(obj.HotelCountry)) return new System.ComponentModel.DataAnnotations.ValidationResult("Hotel Country can't be empty.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
    }

    public class JoinRequestXML 
    {
        #region Contract properties
        public Guid Id { get; set; }
        public bool IsTrial { get; set; }
        public string HotelName { get; set; }
        public string ContactName { get; set; }
        public string HotelEmail { get; set; }
        public string HotelUrl { get; set; }
        public string HotelTelephone { get; set; }
        public string HotelCountry { get; set; }
        public long RoomAmount { get; set; }
        public decimal BillValue { get; set; }
        public string BillCurrency { get; set; }
        public string FiscalName { get; set; }
        public string FiscalAddress { get; set; }
        public string FiscalLocal { get; set; }
        public string FiscalNumber { get; set; }
        public string Description { get; set; }
        #endregion
        #region Constructor

        public JoinRequestXML()
            : base()
        {
        }


        #endregion

        #region Public Methods
        public static JoinRequestXML LoadFromXml(string xml)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(JoinRequestXML));
            MemoryStream stream = new MemoryStream();
            System.Text.Encoding encoder = System.Text.Encoding.UTF8;
            foreach (byte b in encoder.GetBytes(xml))
            {
                stream.WriteByte(b);
            }
            stream.Seek(0, SeekOrigin.Begin);
            JoinRequestXML result = (JoinRequestXML)serializer.Deserialize(stream);
            stream.Close();
            return result;
        }
        public static string SaveToXml(JoinRequestXML contract)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(JoinRequestXML));
            MemoryStream stream = new MemoryStream();
            serializer.Serialize(stream, contract);
            stream.Seek(0, SeekOrigin.Begin);
            StreamReader reader = new StreamReader(stream);
            string result = reader.ReadToEnd();
            reader.Close();
            stream.Close();
            return result;
        }
        #endregion
    }




}
