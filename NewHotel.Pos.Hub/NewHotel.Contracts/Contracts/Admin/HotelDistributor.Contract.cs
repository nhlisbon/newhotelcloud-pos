﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class HotelDistributorContract : BaseContract
    {
        #region Members
        private string _description;
        private string _email;
        private string _country;
        private Guid? _templateId;
        #endregion
        #region Contract properties

        [NewHotel.DataAnnotations.Required(ErrorMessage = "Description required.", AllowEmptyStrings = false)]
        [DataMember]
        public string Description { get { return _description; } set { Set<string>(ref _description, value, "Description"); } }
        [DataMember]
        public string Email { get { return _email; } set { Set(ref _email, value, "Email"); } }
        [DataMember]
        public string Country { get { return _country; } set { Set(ref _country, value, "Country"); } }
        [DataMember]
        public Guid? TemplateId { get { return _templateId; } set { Set(ref _templateId, value, "TemplateId"); } }

        #endregion
        #region Constructor

        public HotelDistributorContract()
            : base()
        {
        }

        #endregion
    }
}
