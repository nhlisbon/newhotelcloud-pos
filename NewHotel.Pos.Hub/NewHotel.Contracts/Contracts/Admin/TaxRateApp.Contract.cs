﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TaxesRateAppContract : BaseContract
    {
        #region Members

        [DataMember]
        internal LanguageTranslationContract _description;
        private Guid? _taxRegion;
        private Guid? _taxSequence;
        private decimal? _percent;
        
        #endregion
        #region Contract properties

        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
        }

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Tax region required.")]
        public Guid? TaxRegion
        {
            get { return _taxRegion; }
            set { Set(ref _taxRegion, value, "TaxRegion"); }
        }

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Sequence required.")]
        public Guid? TaxSequence
        {
            get { return _taxSequence; }
            set { Set(ref _taxSequence, value, "TaxSequence"); }
        }

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Percent required.")]
        public decimal? Percent
        {
            get { return _percent; }
            set { Set(ref _percent, value, "Percent"); }
        }
        [DataMember]
        public string Country { get; set; }

        #endregion
        #region Constructor

        public TaxesRateAppContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
    }
}
