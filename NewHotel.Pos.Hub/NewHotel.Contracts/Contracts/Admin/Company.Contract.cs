﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class CompanyContract : BaseContract
    {
        #region Members

        #endregion
        #region Contract properties

        [DataMember]
        public string Name {get;set;}
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string WebPage { get; set; }
        [DataMember]
        public string NoFiscal { get; set; }
        [DataMember]
        public string Country { get; set; }

        #endregion
        #region Constructor

        public CompanyContract()
            : base()
        {
        }

        #endregion
    }
}
