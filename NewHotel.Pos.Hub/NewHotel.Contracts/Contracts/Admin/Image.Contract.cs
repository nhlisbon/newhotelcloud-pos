﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ImageContract : BaseContract
    {
        //private  double _width;
        //private double _height;
        //private string _description;
       
        #region Contract properties
        [DataMember]
        public string Name { get ; set; } 
        [DataMember]
        public bool IsSelected { get ; set; } 
        [DataMember]
        public Blob Image { get ; set; }
        [DataMember]
        public string Path { get; set; }

        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Page { get; set; }
        [DataMember]
        public int MaxCountImg { get; set; }

        [DataMember]
        public double Width { get; set; }

        [DataMember]
        public double Height { get; set; }
        [DataMember]
        public int Index { get; set; }
        #endregion
        #region Constructor

       

        #endregion
    }
}
