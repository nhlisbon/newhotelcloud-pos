﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class EventContract : BaseContract<long>
    {
        #region Members
        private DateTime _date;
        private ARGBColor? _btnColor;
        private ARGBColor? _btnColor1;
        private string _kid; 
        #endregion
        #region Contract properties
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Name required.", AllowEmptyStrings = false)]
        public string Name {get;set;}
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Date required.", AllowEmptyStrings = false)]
        public DateTime Date {  get { return _date; }
            set { Set<DateTime>(ref _date, value, "Date"); }
        }
        [DataMember]
        public DateTime PublicationDate { get; set; }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Responsible required.", AllowEmptyStrings = false)]
        public string Responsible { get; set; }
        [DataMember]
        public string Distribution { get; set; }
        [DataMember]
        public string ImgDetail { get; set; }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Country required.", AllowEmptyStrings = false)]
        public string Country { get; set; }
        [DataMember]
        public string Cities { get; set; }
        [DataMember]
        public string Kid
        {
            get { return _kid; }
            set
            {
                bool change = _kid != value;
               // if (change)
               // {
                    if (Kid == "FormEvents.aspx")
                        ImagesSourceL1 = ImagesSource;
                    else if (Kid == "FormEvents2B.aspx")
                        ImagesSourceL2 = ImagesSource;
                    else if (Kid == "FormEventsImg.aspx")
                        ImagesSourceL3 =  ImagesSource;
               // }
                Set<string>(ref _kid, value, "Kid");
            }
        }

        public void UpdateImageSource()
        {
            if (!string.IsNullOrEmpty(Kid))
            {
                if (Kid == "FormEvents.aspx")
                    ImagesSource = ImagesSourceL1 == null ? new TypedList<ImageContract>() : new TypedList<ImageContract>(ImagesSourceL1);
                else if (Kid == "FormEvents2B.aspx")
                    ImagesSource = ImagesSourceL2 == null ? new TypedList<ImageContract>() : new TypedList<ImageContract>(ImagesSourceL2);
                else if (Kid == "FormEventsImg.aspx")
                    ImagesSource = ImagesSourceL3 == null ? new TypedList<ImageContract>() : new TypedList<ImageContract>(ImagesSourceL3);
            }

        }
        [DataMember]
        public bool FName { get; set; }
        [DataMember]
        public bool LName { get; set; }
        [DataMember]
        public bool FullName { get; set; }
        [DataMember]
        public bool City { get; set; }
        [DataMember]
        public bool HotelEmp { get; set; }
        [DataMember]
        public bool Phone { get; set; }
        [DataMember]
        public bool Fax { get; set; }
        [DataMember]
        public bool ShowCountry { get; set; }
        [DataMember]
        public bool EMail { get; set; }
        [DataMember]
        [ReflectionExclude]
        public int MaxImagesCount { get; set; }
        [DataMember]
        [ReflectionExclude]
        public TypedList<ImageContract> ImagesSource { get; set; }
        [DataMember]
        [ReflectionExclude]
        public TypedList<ImageContract> ImagesList { get; set; }
        [DataMember]
        public ARGBColor? BtnColor { get { return _btnColor; } set { Set(ref _btnColor, value, "BtnColor"); } }
        [DataMember]
        public ARGBColor? BtnColor1 { get { return _btnColor1; } set { Set(ref _btnColor1, value, "BtnColor1"); } }
        [ReflectionExclude]
        public TypedList<ImageContract> ImagesSourceL1 { get; set; }
        [ReflectionExclude]
        public TypedList<ImageContract> ImagesSourceL2 { get; set; }
        [ReflectionExclude]
        public TypedList<ImageContract> ImagesSourceL3 { get; set; }
        
        #endregion
        #region Constructor

        public EventContract()
            : base()
        {
            PublicationDate = DateTime.Now;
            Date = DateTime.Now;
            ImagesList = new TypedList<ImageContract>();
            ImagesSource = new TypedList<ImageContract>();
            ImagesSourceL1 = new TypedList<ImageContract>();
            ImagesSourceL2 = new TypedList<ImageContract>();
            ImagesSourceL3 = new TypedList<ImageContract>();
            MaxImagesCount = 0;
        }

        #endregion
    }
}
