﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class HotelInformationContract : BaseContract
    {
        #region Private Members
        private string _hotelCode;
        private string _hotelDescription;
        private DateTime? _lastAccessDate;
        private string _lastAccessUser;
        private DateTime? _nightAuditorDate;
        private string _nightAuditorUser;
        private string _reservationAmount;
        private DateTime? _reservationDate;
        #endregion
        #region Constructor
        public HotelInformationContract()
        {
            Details = new List<DetailsContract>();
        } 
        #endregion
        #region Public Properties
        [DataMember]
        public string HotelCode { get { return _hotelCode; } set { Set(ref _hotelCode, value, "HotelCode"); } }
        [DataMember]
        public string HotelDescription { get { return _hotelDescription; } set { Set(ref _hotelDescription, value, "HotelDescription"); } }
        [DataMember]
        public DateTime? LastAccessDate { get { return _lastAccessDate; } set { Set(ref _lastAccessDate, value, "LastAccessDate"); } }
        [DataMember]
        public string LastAccessUser { get { return _lastAccessUser; } set { Set(ref _lastAccessUser, value, "LastAccessUser"); } }
        [DataMember]
        public DateTime? NightAuditorDate { get { return _nightAuditorDate; } set { Set(ref _nightAuditorDate, value, "NightAuditorDate"); } }
        [DataMember]
        public string NightAuditorUser { get { return _nightAuditorUser; } set { Set(ref _nightAuditorUser, value, "NightAuditorUser"); } }
        [DataMember]
        public string ReservationAmount { get { return _reservationAmount; } set { Set(ref _reservationAmount, value, "ReservationAmount"); } }
        [DataMember]
        public DateTime? ReservationDate { get { return _reservationDate; } set { Set(ref _reservationDate, value, "ReservationDate"); } }

        [DataMember]
        public List<DetailsContract> Details { get; set; }
        #endregion
    }

    [DataContract]
	[Serializable]
    public class DetailsContract : BaseContract
    {
        [DataMember]
        public string Year { get; set; }
        [DataMember]
        public string Month { get; set; }
        [DataMember]
        public string Logins { get; set; }
        [DataMember]
        public string Reservations { get; set; }
        [DataMember]
        public string Transactions { get; set; }
        [DataMember]
        public string Invoices { get; set; }
    }
}
