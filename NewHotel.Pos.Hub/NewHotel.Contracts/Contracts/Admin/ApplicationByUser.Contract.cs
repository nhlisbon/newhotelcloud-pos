﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ApplicationByUserContract : BaseContract
    {
        #region Members

        private long _applicationId;
        private string _applicationDescription;

        #endregion
        #region Properties

        [DataMember]
        public long ApplicationId { get { return _applicationId; } set { Set(ref _applicationId, value, "ApplicationId"); } }
        [DataMember]
        public string ApplicationDescription { get { return _applicationDescription; } set { Set(ref _applicationDescription, value, "ApplicationDescription"); } }

        #endregion
        #region Constructors

        public ApplicationByUserContract(long applicationId, string applicationDescription)
        {
            _applicationId = applicationId;
            _applicationDescription = applicationDescription;
        }

        public ApplicationByUserContract(Guid id, long applicationId, string applicationDescription)
            : this(applicationId, applicationDescription)
        {
            Id = id;
        }

        #endregion
    }
}