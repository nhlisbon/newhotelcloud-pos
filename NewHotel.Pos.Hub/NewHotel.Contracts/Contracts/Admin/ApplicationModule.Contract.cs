﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ApplicationModuleContract : BaseContract<long>
    {
        #region Properties

        [DataMember]
        [System.ComponentModel.DataAnnotations.Display(Name = "Description", Description = "Please enter description.")]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Description required.", AllowEmptyStrings = false)]
        public string Description { get; set; }

        #endregion
    }
}