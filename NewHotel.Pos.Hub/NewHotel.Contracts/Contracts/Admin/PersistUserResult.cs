﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PersistUserResult : ValidationResult
    {
        [DataMember]
        public List<string> LoginNames { get; set; }
        [DataMember]
        public List<KeyDescRecord> Logins { get; set; }
        
        [DataMember]
        public Guid UserId { get; set; }

        [DataMember]
        public string NumericLoginGenerated { get; set; }

        public PersistUserResult() : base() { LoginNames = new List<string>(); Logins = new List<KeyDescRecord>(); }
    }

    [DataContract]
	[Serializable]
    public class GeneratedStringResult: ValidationResult
    {
        [DataMember]
        public List<string> Login { get; set; }
         [DataMember]
        public List<string> Password { get; set; }

        [DataMember]
        public Guid UserId { get; set; }

        public GeneratedStringResult() : base() { Login = new List<string>(); Password = new List<string>(); }
    }
}
