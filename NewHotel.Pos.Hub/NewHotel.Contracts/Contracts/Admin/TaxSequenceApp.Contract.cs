﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TaxesSequenceAppContract : BaseContract
    {
        #region Members

        private string _abbreviation;
        [DataMember]
        internal LanguageTranslationContract _description;

        #endregion

        #region Properties
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Abbreviation required.")]
        public string Abbreviation
        {
            get { return _abbreviation; }
            set { Set(ref _abbreviation, value, "Abbreviation"); }
        }

        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
        }

        [DataMember]
        public string Country { get; set; }

        #endregion

        #region Constructor

        public TaxesSequenceAppContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
    }
}
