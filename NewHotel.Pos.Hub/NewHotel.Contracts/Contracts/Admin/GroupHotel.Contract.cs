﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class GroupHotelContract: BaseContract
    {
        [DataMember]
        public string Description { get; set; }
    }
}
