﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(HotelAppContract), "ValidateHotelAppContract")]
    public class HotelAppContract : BaseContract
    {
        #region Members

        private string _url;
        private string _abbreviation;
        private string _description;
        private string _user;
        private string _schemaPassword;
        private short _provider;
        private string _databaseService;
        private Guid? _groupHotelId;
        private string _groupDescription;
        private long _language;
        private string _timeZoneId;
        private string _country;
        private string _currency;
        private Guid? _taxRegion;
        private string _systemPassword;
        private decimal _rating;
        private short _stars;
        private string _contlastName;
        private string _contmiddleName;
        private string _contName;
        private string _contMail;
        private string _contAlternativeMail;
        private string _contReservationMail;
        private string _contPhone;
        private string _reportFolder;
        private string _code;
        private string _userpassword;
        private string _userDescription;
        private short _roleId;
        private string _userlogin;

        private DateTime? _nightAuditorDate;
        private string _nightAuditorUser;
        private DateTime? _reservationDate;
        private string _reservationAmount;
        private int _licenseUserNumber;
        private int _licenseRoomNumber;
        private string _webServExternalUser;
        private string _webServExternalPassword;
        private bool _webServExternalActive;

        private bool _notifyByEmail;
        private bool _allowDbInitialization;
        private bool _inactive;

        private string _accountingCompany;
        private string _accountingBranch;
        private Guid? _accountingUserId;
        private bool _powerBI;

        private TypedList<KeyDescRecord> _dataBaseServices;

        #endregion
        #region Constructor

        public HotelAppContract()
        {
            Language = 1033;
            _inactive = false;
            LicenseProductType = 1;
            _allowDbInitialization = true;
            _dataBaseServices = new TypedList<KeyDescRecord>();
        }

        #endregion
        #region Properties

        [DataMember]
        public string Url { get { return _url; } set { Set(ref _url, value, "Url"); } }
        [DataMember]
        public string Abbrevation { get { return _abbreviation; } set { Set(ref _abbreviation, value, "Abbrevation"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public string User { get { return _user; } set { Set(ref _user, value, "User"); } }
        [DataMember]
        public string ShemaPassword { get { return _schemaPassword; } set { Set(ref _schemaPassword, value, "ShemaPassword"); } }
        [DataMember]
        public short Provider { get { return _provider; } set { Set(ref _provider, value, "Provider"); } }
        [DataMember]
        public string DataBaseService { get { return _databaseService; } set { Set(ref _databaseService, value, "DataBaseService"); } }
        [DataMember]
        public Guid? GroupHotelId { get { return _groupHotelId; } set { Set(ref _groupHotelId, value, "GroupHotelId"); } }
        [DataMember]
        public string GroupDescription { get { return _groupDescription; } set { Set(ref _groupDescription, value, "GroupDescription"); } }
        [DataMember]
        public long Language { get { return _language; } set { Set(ref _language, value, "Language"); } }
        [DataMember]
        public string TimeZoneId { get { return _timeZoneId; } set { Set(ref _timeZoneId, value, "TimeZoneId"); } }
        [DataMember]
        public string Country { get { return _country; } set { Set(ref _country, value, "Country"); } }
        [DataMember]
        public string Currency { get { return _currency; } set { Set(ref _currency, value, "Currency"); } }
        [DataMember]
        public Guid? TaxRegion { get { return _taxRegion; } set { Set(ref _taxRegion, value, "TaxRegion"); } }
        [DataMember]
        public string SystemPassword { get { return _systemPassword; } set { Set(ref _systemPassword, value, "SystemPassword"); } }
        [DataMember]
        public decimal Rating { get { return _rating; } set { Set(ref _rating, value, "Rating"); } }
        [DataMember]
        public short Stars { get { return _stars; } set { Set(ref _stars, value, "Stars"); } }
        [DataMember]
        public string ContLastName { get { return _contlastName; } set { Set(ref _contlastName, value, "ContLastName"); } }
        [DataMember]
        public string ContMiddleName { get { return _contmiddleName; } set { Set(ref _contmiddleName, value, "ContMiddleName"); } }
        [DataMember]
        public string ContName { get { return _contName; } set { Set(ref _contName, value, "ContName"); } }
        [DataMember]
        public string ContMail { get { return _contMail; } set { Set(ref _contMail, value, "ContMail"); } }
        [DataMember]
        public string ContAlternativeMail { get { return _contAlternativeMail; } set { Set(ref _contAlternativeMail, value, "ContAlternativeMail"); } }
        [DataMember]
        public string ContReservationMail { get { return _contReservationMail; } set { Set(ref _contReservationMail, value, "ContReservationMail"); } }
        [DataMember]
        public string ContPhone { get { return _contPhone; } set { Set(ref _contPhone, value, "ContPhone"); } }
        [DataMember]
        public string ReportFolder { get { return _reportFolder; } set { Set(ref _reportFolder, value, "ReportFolder"); } }
        [DataMember]
        public string Code { get { return _code; } set { Set(ref _code, value, "Code"); } }
        [DataMember]
        public string UserLogin { get { return _userlogin; } set { Set(ref _userlogin, value, "UserLogin"); } } 
        [DataMember]
        public string UserPassword { get { return _userpassword; } set { Set(ref _userpassword, value, "UserPassword"); } }
        [DataMember]
        public short UserRoleId { get { return _roleId; } set { Set(ref _roleId, value, "UserRoleId"); } }
        [DataMember]
        public string UserDescription { get { return _userDescription; } set { Set(ref _userDescription, value, "UserDescription"); } }
        [DataMember]
        public string LicenseNumber { get; set; }
        [DataMember]
        public string LicensePropertyName { get; set; }
        [DataMember]
        public DateTime LicenseExpireDate { get; set; }
        [DataMember]
        public DateTime LicenseInvoiceDate { get; set; }
        [DataMember]
        public short? LicenseProductType { get; set; }
        [DataMember]
        public int LicenseRoomNumber { get { return _licenseRoomNumber; } set { Set(ref _licenseRoomNumber, value, "LicenseRoomNumber"); } }
        [DataMember]
        public int LicenseUserNumber { get { return _licenseUserNumber; } set { Set(ref _licenseUserNumber, value, "LicenseUserNumber"); } }
        [DataMember]
        public LicenseProductType LicenseType { get; set; }
        [DataMember]
        public Guid? HotelDistributorId { get; set; }
        [DataMember]
        public Guid? HotelPaymentId { get; set; }
        [DataMember]
        public Guid? CompanyId { get; set; }
        [DataMember]
        public string CompanyName { get; set; }
        [DataMember]
        public string ContCountry { get; set; }
        [DataMember]
        public string ContLocation { get; set; }
        [DataMember]
        public string ContAddress { get; set; }

        [DataMember]
        public DateTime? NightAuditorDate { get { return _nightAuditorDate; } set { Set(ref _nightAuditorDate, value, "NightAuditorDate"); } }
        [DataMember]
        public string NightAuditorUser { get { return _nightAuditorUser; } set { Set(ref _nightAuditorUser, value, "NightAuditorUser"); } }
        [DataMember]
        public DateTime? ReservationDate { get { return _reservationDate; } set { Set(ref _reservationDate, value, "ReservationDate"); } }
        [DataMember]
        public string ReservationAmount { get { return _reservationAmount; } set { Set(ref _reservationAmount, value, "ReservationAmount"); } }
        [DataMember]
        public Guid? JoinRequestId { get; set; }

        [DataMember]
        public string WebServExternalUser { get { return _webServExternalUser; } set { Set(ref _webServExternalUser, value, "WebServExternalUser"); } }
        [DataMember]
        public string WebServExternalPassword { get { return _webServExternalPassword; } set { Set(ref _webServExternalPassword, value, "WebServExternalPassword"); } }
        [DataMember]
        public bool WebServExternalActive { get { return _webServExternalActive; } set { Set(ref _webServExternalActive, value, "WebServExternalActive"); } }

        [DataMember]
        public bool NotifyByEmail { get { return _notifyByEmail; } set { Set(ref _notifyByEmail, value, "NotifyByEmail"); } }
        [DataMember]
        public bool AllowDbInitialization { get { return _allowDbInitialization; } set { Set(ref _allowDbInitialization, value, "AllowDbInitialization"); } }
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }

        [DataMember]
        public string AccountingCompany { get { return _accountingCompany; } set { Set(ref _accountingCompany, value, "AccountingCompany"); } }
        [DataMember]
        public string AccountingBranch { get { return _accountingBranch; } set { Set(ref _accountingBranch, value, "AccountingBranch"); } }
        [DataMember]
        public Guid? AccountingUserId { get { return _accountingUserId; } set { Set(ref _accountingUserId, value, "AccountingUserId"); } }

        [DataMember]
        public bool PowerBI { get { return _powerBI; } set { Set(ref _powerBI, value, "PowerBI"); } }

        [DataMember]
        public TypedList<KeyDescRecord> DataBaseServices { get { return _dataBaseServices; } set { Set(ref _dataBaseServices, value, "DataBaseServices"); } }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateHotelAppContract(HotelAppContract obj)
        {
            if (string.IsNullOrEmpty(obj.Description))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description Required");

            if (string.IsNullOrEmpty(obj.User))
                return new System.ComponentModel.DataAnnotations.ValidationResult("User Required");

            if (char.IsNumber(obj.User[0]))
                return new System.ComponentModel.DataAnnotations.ValidationResult("User cannot start with a numeric value");

            if (string.IsNullOrEmpty(obj.ShemaPassword))
                return new System.ComponentModel.DataAnnotations.ValidationResult("SchemaPassword Required");

            if (obj.Provider != 1 && obj.Provider != 2)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid Provider");

            if (string.IsNullOrEmpty(obj.DataBaseService))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Database Service Required");

            if (obj.Language == 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Language Required");

            if (!string.IsNullOrEmpty(obj.ContMail) && !IsValidEmail(obj.ContMail))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Email not valid");

            if (string.IsNullOrEmpty(obj.TimeZoneId))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Missing Time Zone");

            if (string.IsNullOrEmpty(obj.Country))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Country Required");
       
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        public static bool IsValidEmail(string email)
        {
            // Return false if not valid e-mail format.
            return Regex.IsMatch(email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }

        #endregion
    }   
}