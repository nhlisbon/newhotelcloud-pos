﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class RgpdTermsServiceContract : BaseContract
    {
        public RgpdTermsServiceContract() : base() { }

        private long _languageId;
        [DataMember]
        public long LanguageId { get { return _languageId; } set { Set(ref _languageId, value, "LanguageId"); } }

        private string _subject;
        [DataMember]
        public string Subject { get { return _subject; } set { Set(ref _subject, value, "Subject"); } }

        private Clob _Template; 
        [DataMember]
        public Clob Template { get { return _Template; } set { Set(ref _Template, value, "Template"); } }
    }
}