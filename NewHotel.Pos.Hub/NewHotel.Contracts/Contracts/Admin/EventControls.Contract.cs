﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    public class EventControlsContract : BaseContract<long>
    {
        #region Members
        #endregion
        #region Contract properties
        [DataMember]
        public bool FName {get;set;}
        [DataMember]
        public bool LName { get; set; }
        [DataMember]
        public bool FullName {  get ;set; }
        [DataMember]
        public bool City { get; set; }
        [DataMember]
        public bool HotelEmp { get; set; }
        [DataMember]
        public bool Phone { get; set; }
        [DataMember]
        public bool Country { get; set; }
        [DataMember]
        public bool EMail { get; set; }
        
        
      
        
        #endregion
        #region Constructor

        public EventControlsContract()
            : base()
        {
            
        }

        #endregion
    }
}
