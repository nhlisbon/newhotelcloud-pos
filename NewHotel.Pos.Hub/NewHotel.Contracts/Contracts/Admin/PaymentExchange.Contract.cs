﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PaymentExchangeContract : BaseContract
    {
        #region Private Members
        private string _baseCurrency;
        private string _foreingCurrency;
        private decimal _exchangeValue;
        private decimal _baseValue;
        private bool _multiplyValue;
        #endregion
        #region Properties
        [DataMember]
        public string BaseCurrency { get { return _baseCurrency; } set { Set(ref _baseCurrency, value, "BaseCurrency"); } }
        [DataMember]
        public string ForeingCurrency { get { return _foreingCurrency; } set { Set(ref _foreingCurrency, value, "ForeingCurrency"); } }
        [DataMember]
        public decimal ExchangeValue 
        {
            get { return _exchangeValue; } 
            set 
            { 
                Set(ref _exchangeValue, value, "ExchangeValue"); 
				if (Multiply && value == 0) return;
                BaseValue = (Multiply) ? Math.Round(1 / value, 4) : value;
            } 
        }
        [DataMember]
        public decimal BaseValue { get { return _baseValue; } set { Set(ref _baseValue, value, "BaseValue"); NotifyPropertyChanged("BaseValueDescription"); } }
        [DataMember]
        public bool Multiply
		{ 
			get { return _multiplyValue; } 
			set
			{ 
				Set(ref _multiplyValue, value, "Multiply");
                if (value && ExchangeValue == 0) return;
                BaseValue = (value) ? Math.Round(1 / ExchangeValue, 4) : ExchangeValue;
			} 
		}

        public string BaseValueDescription { get { return "(" + BaseCurrency + ") 1 => " + BaseValue.ToString() + " (" + ForeingCurrency + ")"; } }
        #endregion
        
    }
}
