﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class HotelbyUserContract : BaseContract
    {

        #region Properties

        [DataMember]
        public Guid HotelId { get; set; }
        [DataMember]
        public Guid UserId { get; set; }
        [DataMember]
        public bool InactiveUser { get; set; }
        [DataMember]
        public string Hotel { get; set; }
        [DataMember]
        public string User { get; set; }

        #endregion
        #region Constructors

        public HotelbyUserContract()
            : base() { }

      

        #endregion


    }
    [DataContract]
	[Serializable]
    public class HotelbyUserRelationContract : BaseContract
    {
        #region Members

        private Guid _hotelorUserId;
        private bool _inactiveUser;
        private string _description;

        #endregion

        #region Properties

        [DataMember]
        public Guid HotelorUserId { get { return _hotelorUserId; } set { Set(ref _hotelorUserId, value, "HotelorUserId"); } }
        [DataMember]
        public bool InactiveUser { get { return _inactiveUser; } set { Set(ref _inactiveUser, value, "InactiveUser"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }

        #endregion
        #region Constructors

        public HotelbyUserRelationContract(Guid id, Guid hotelorUserId, bool inactive, string description)
            : base()
        {
            Id = id;
            HotelorUserId = hotelorUserId;
            InactiveUser = inactive;
            Description = description;
        }

        #endregion
    }
    
}
