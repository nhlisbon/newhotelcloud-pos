﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class FileFolderContract : BaseContract
    {
       
        #region Properties
        [DataMember]
        public string Name { get ; set ; }
        [DataMember]
        [ReflectionExclude]
        
        public List<ImageContract> Images { get; set; }
        #endregion
        #region Constructor
        public FileFolderContract()
            : base() 
        {
            Images = new List<ImageContract>();
        }
        #endregion
    }
}
