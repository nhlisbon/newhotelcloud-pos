﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RolePermissionContract : BaseContract
    {
        #region Members
        //internal Guid  _roleid;
        //internal string  _roleDescription;
        //internal PermissionContract _permisionId;
        //internal string  _permisionDescription;
        internal SecurityCode _securecode;
        #endregion
        #region Properties

        [DataMember]
        public Guid Roleid { get; set; }
        [DataMember]
        public string Role { get; set; }
        [DataMember]
        public Guid Permissionid { get; set; }
        [DataMember]
        public string AppicationName { get; set; }
        [DataMember]
        public string GroupDescription { get; set; }
        [DataMember]
        public SecurityCode SecurityCode
        {
            get { return _securecode; }
            set { Set(ref _securecode, value, "SecurityCode"); }
        }
     
        #endregion
   
    }


    [DataContract]
	[Serializable]
    public class RolPermissionRelationContract : BaseContract
    {
        #region Properties
        internal SecurityCode _securecode;
        [DataMember]
        public short RoleId { get; set; }
        [DataMember]
        public SecurityCode SecurityCode
        {
            get { return _securecode; }
            set { Set(ref _securecode, value, "SecurityCode"); }
        }
        [DataMember]
        public Guid PermissionApplicationId { get; set; }
        [DataMember]
        public string PermissionDescription { get; set; }

        [DataMember]
        public string Group{ get; set; }

        [DataMember]
        public short GroupOrder { get; set; }
        #endregion
        #region Constructors

        public RolPermissionRelationContract(Guid id, short rolorPermissionId,
            SecurityCode securitycode, Guid permissionApplicationId, string permisionDescription, string group, short grouporder)
            : base()
        {
            Id = id;
            RoleId = rolorPermissionId;
            SecurityCode = securitycode;
            PermissionApplicationId = permissionApplicationId;
            PermissionDescription = permisionDescription;
            Group = group;
            GroupOrder = grouporder;
        }
        public RolPermissionRelationContract()
            : base()
        { }

        #endregion
    }
}
