﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RoleContract : BaseContract<short>
    {
        #region Members
        [DataMember]
        internal LanguageTranslationContract _description;
        #endregion

        #region Properties

        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
        }

        [DataMember]
        public TypedList<RolPermissionRelationContract> Permissions { get; set; }
        [DataMember]
        public List<string> RolesOptions { get; set; }
        [DataMember]
        public TypedList<OptionRolCheck> RolesOptionsCheck { get; set; }

        #endregion

        #region Constructor
        public RoleContract()
            : base()
        {
            _description = new LanguageTranslationContract();
            Permissions = new TypedList<RolPermissionRelationContract>();
            RolesOptions = new List<string>();
            RolesOptionsCheck = new TypedList<OptionRolCheck>();
        }
        #endregion
    }
    [DataContract]
	[Serializable]
    public class OptionRolCheck : BaseContract
    {
        [DataMember]
        public bool Active { get; set; }
        [DataMember]
        public string Options { get; set; }
    }
}
