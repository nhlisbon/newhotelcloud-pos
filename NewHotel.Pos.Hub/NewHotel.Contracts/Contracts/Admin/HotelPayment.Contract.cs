﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class HotelPaymentContract : BaseContract
    {
        #region Members

        [DataMember]
        internal LanguageTranslationContract _description;
        private bool _inactive;

        #endregion
        #region Contract properties

        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
        }

        [DataMember]
        public bool Inactive
        {
            get { return _inactive; }
            set { Set(ref _inactive, value, "Inactive"); }
        }


        #endregion
        #region Constructor

        public HotelPaymentContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
    }
}
