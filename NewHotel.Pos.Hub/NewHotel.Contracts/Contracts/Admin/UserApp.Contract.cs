﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(UserAppContract), "ValidateUserAppContract")]
    public class UserAppContract : BaseContract, ITwoFactorAutheticableUser
    {
        #region Constants

        public const long NewHotelPms = 1;
        public const long NewHotelPos = 2;
        public const long NewHotelAcc = 3;
        public const long NewHotelSpa = 4;
        public const long NewHotelGolf = 5;
        public const long NewHotelEvents = 6;
        public const long NewHotelStock = 7;
        public const long NewHotelCondo = 8;
        public const long NewHotelSurvey = 9;
        public const long NewHotelHousekeeping = 12;
        public const long NewHotelCentral = 13;
        public const long NewHotelGes = 99;

        #endregion
        #region Members

        private string _password;
        private short _roleId;
        private string _login;
        private string _roleOpts;
        private string _keyTwoFactorAuthentication = "SuperSecretKeyGoesHere";
        private bool _isEnableTwoFactorAuthentication = true;

        #endregion
        #region Constructor

        public UserAppContract()
            : base()
        {
            UserbyHotels = new TypedList<HotelbyUserRelationContract>();
            UserByApplications = new TypedList<ApplicationByUserContract>();
            UserPermissions = new TypedList<UserAppPermissionContract>();
            Applications = new TypedList<long>();
        }

        #endregion
        #region Properties

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Login required.", AllowEmptyStrings = false)]
        public string Login { get { return _login; } set { Set(ref _login, value, nameof(Login)); } }

        [DataMember]
        [Display(Name = "Password", Description = "Please enter paswword.")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Password required.", AllowEmptyStrings = false)]
        public string Password { get { return _password; } set { Set(ref _password, value, nameof(Password)); } }

        [DataMember]
        public short RoleId { get { return _roleId; } set { Set(ref _roleId, value, nameof(RoleId)); } }

        [DataMember]
        public string RoleOpts { get { return _roleOpts; } set { Set(ref _roleOpts, value, nameof(RoleOpts)); } }

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.", AllowEmptyStrings = false)]
        public string Description { get; set; }

        [DataMember]
        public string Email { get; set; }

        #region ITwoFactorAutheticableUser Implementation

        [DataMember]
        public bool IsEnableTwoFactorAuthentication
        {
            get { return _isEnableTwoFactorAuthentication; }
            set { Set(ref _isEnableTwoFactorAuthentication, value, nameof(IsEnableTwoFactorAuthentication)); }
        }

        [DataMember]
        public string KeyTwoFactorAuthentication
        {
            get { return _keyTwoFactorAuthentication; }
            set { Set(ref _keyTwoFactorAuthentication, value, nameof(KeyTwoFactorAuthentication)); }
        }

        [DataMember]
        public Guid MgrId
        {
            get
            {
                return (Guid)Id;
            }

            set
            {
                Id = value;
            }
        }

        #endregion

        [ReflectionExclude]
        public TypedList<HotelbyUserRelationContract> UserbyHotels { get; set; }

        [DataMember]
        public TypedList<ApplicationByUserContract> UserByApplications { get; set; }

        [ReflectionExclude]
        public TypedList<UserAppPermissionContract> UserPermissions { get; set; }
        
        #endregion
        #region Applications

        [DataMember]
        public TypedList<long> Applications { get; set; }

        public bool IsMultiAppUser
        {
            get { return Applications.Count > 1; }
        }

        public bool AppPMS => Applications.Contains(NewHotelPms);

        public bool AppPOS => Applications.Contains(NewHotelPos);

        public bool AppACC => Applications.Contains(NewHotelAcc);

        public bool AppSPA => Applications.Contains(NewHotelSpa);

        public bool AppGOLF => Applications.Contains(NewHotelGolf);

        public bool AppEVENTS => Applications.Contains(NewHotelEvents);

        public bool AppSTOCK => Applications.Contains(NewHotelStock);

        public bool AppCONDO => Applications.Contains(NewHotelCondo);

        public bool AppSURVEY => Applications.Contains(NewHotelSurvey);

        public bool AppHOUSEKEEPING => Applications.Contains(NewHotelHousekeeping);

        public bool AppCENTRAL => Applications.Contains(NewHotelCentral); 

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateUserAppContract(UserAppContract obj)
        {
            if (obj.RoleId == 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Role Required");
            if (string.IsNullOrEmpty(obj.Password))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Password Required");
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class UserAppPermissionContract : BaseContract
    {
        [DataMember]
        public Guid UserId { get; set; }
        [DataMember]
        public Guid PermissionApplicationId { get; set; }
        [DataMember]
        public SecurityCode SecurityCode { get; set; }
    }

}