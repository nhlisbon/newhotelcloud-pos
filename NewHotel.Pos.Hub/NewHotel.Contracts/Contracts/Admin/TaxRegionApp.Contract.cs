﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TaxesRegionAppContract : BaseContract
    {
        #region Members

        [DataMember]
        internal LanguageTranslationContract _description;
        private string _country;
        private string _code;
        
        #endregion
        #region Properties

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Country required.")]
        public string Country
        {
            get { return _country; }
            set { Set<string>(ref _country, value, "Country"); }
        }

        [DataMember]
        public string Code
        {
            get { return _code; }
            set { Set<string>(ref _code, value, "Code"); }
        }

        #endregion

        #region Constructor

        public TaxesRegionAppContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
    }
}