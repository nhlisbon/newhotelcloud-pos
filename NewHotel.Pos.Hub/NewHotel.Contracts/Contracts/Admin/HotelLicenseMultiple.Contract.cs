﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class HotelLicenseMultipleContract : BaseContract
    {
        #region Private Members
        private bool _pms;
        private bool _pos;
        private bool _spa;
        private bool _acc;
        private bool _condo;
        private bool _golf;
        private bool _events;
        private bool _stock;
        private bool _survey;
        private bool _housekeeping;
        private bool _central;
        private bool _newges;
        private bool _guests;
        private bool _mobile;

        private DateTime? _pmsDate;
        private DateTime? _posDate;
        private DateTime? _spaDate;
        private DateTime? _accDate;
        private DateTime? _condoDate;
        private DateTime? _golfDate;
        private DateTime? _eventsDate;
        private DateTime? _stockDate;
        private DateTime? _surveyDate;
        private DateTime? _housekeepingDate;
        private DateTime? _centralDate;
        private DateTime? _newgesDate;
        private DateTime? _guestsDate;
        private DateTime? _mobileDate;

        #endregion
        #region Constructor
        public HotelLicenseMultipleContract()
        {
            
        } 
        #endregion
        #region Public Properties
        [DataMember]
        public bool Pms { get { return _pms; } set { Set(ref _pms, value, "Pms"); } }
        [DataMember]
        public bool Pos { get { return _pos; } set { Set(ref _pos, value, "Pos"); } }
        [DataMember]
        public bool Spa { get { return _spa; } set { Set(ref _spa, value, "Spa"); } }
        [DataMember]
        public bool Acc { get { return _acc; } set { Set(ref _acc, value, "Acc"); } }
        [DataMember]
        public bool Condo { get { return _condo; } set { Set(ref _condo, value, "Condo"); } }
        [DataMember]
        public bool Golf { get { return _golf; } set { Set(ref _golf, value, "Golf"); } }
        [DataMember]
        public bool Events { get { return _events; } set { Set(ref _events, value, "Events"); } }
        [DataMember]
        public bool Stock { get { return _stock; } set { Set(ref _stock, value, "Stock"); } }
        [DataMember]
        public bool Survey { get { return _survey; } set { Set(ref _survey, value, "Survey"); } }
        [DataMember]
        public bool Housekeeping { get { return _housekeeping; } set { Set(ref _housekeeping, value, "Housekeeping"); } }
        [DataMember]
        public bool Central { get { return _central; } set { Set(ref _central, value, "Central"); } }
        [DataMember]
        public bool Newges { get { return _newges; } set { Set(ref _newges, value, "Newges"); } }
        [DataMember]
        public bool Guests { get { return _guests; } set { Set(ref _guests, value, "Guests"); } }
        [DataMember]
        public bool Mobile { get { return _mobile; } set { Set(ref _mobile, value, "Mobile"); } }


        [DataMember]
        public DateTime? PmsDate { get { return _pmsDate; } set { Set(ref _pmsDate, value, "PmsDate"); } }
        [DataMember]
        public DateTime? PosDate { get { return _posDate; } set { Set(ref _posDate, value, "PosDate"); } }
        [DataMember]
        public DateTime? SpaDate { get { return _spaDate; } set { Set(ref _spaDate, value, "SpaDate"); } }
        [DataMember]
        public DateTime? AccDate { get { return _accDate; } set { Set(ref _accDate, value, "AccDate"); } }
        [DataMember]
        public DateTime? CondoDate { get { return _condoDate; } set { Set(ref _condoDate, value, "CondoDate"); } }
        [DataMember]
        public DateTime? GolfDate { get { return _golfDate; } set { Set(ref _golfDate, value, "GolfDate"); } }
        [DataMember]
        public DateTime? EventsDate { get { return _eventsDate; } set { Set(ref _eventsDate, value, "EventsDate"); } }
        [DataMember]
        public DateTime? StockDate { get { return _stockDate; } set { Set(ref _stockDate, value, "StockDate"); } }
        [DataMember]
        public DateTime? SurveyDate { get { return _surveyDate; } set { Set(ref _surveyDate, value, "SurveyDate"); } }
        [DataMember]
        public DateTime? HousekeepingDate { get { return _housekeepingDate; } set { Set(ref _housekeepingDate, value, "HousekeepingDate"); } }
        [DataMember]
        public DateTime? CentralDate { get { return _centralDate; } set { Set(ref _centralDate, value, "CentralDate"); } }
        [DataMember]
        public DateTime? NewgesDate { get { return _newgesDate; } set { Set(ref _newgesDate, value, "NewgesDate"); } }
        [DataMember]
        public DateTime? GuestsDate { get { return _guestsDate; } set { Set(ref _guestsDate, value, "GuestsDate"); } }
        [DataMember]
        public DateTime? MobileDate { get { return _mobileDate; } set { Set(ref _mobileDate, value, "MobileDate"); } }

        public bool Active { get { return Pms || Pos || Spa || Acc || Condo || Golf || Events || Stock || Survey || Housekeeping || Central || Newges || Guests || Mobile; } }
        #endregion
    }
}
