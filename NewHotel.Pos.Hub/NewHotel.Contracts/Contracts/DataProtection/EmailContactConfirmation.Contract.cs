﻿using System;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace NewHotel.Contracts
{
    public class EmailContactConfirmationContract : BaseContract
    {
        #region Members

        private string _adminLogin;
        private string _adminEmail;
        private string _adminConfirmationEmailCode;
        private string _adminEmailCode;
        private bool _adminEmailCodeExpired;
        private string _contactName;
        private string _contactEmail;
        private string _contactConfirmationEmailCode;
        private string _contactEmailCode;
        private bool _contactEmailCodeExpired;

        #endregion
        #region Properties

        [DataMember]
        public string PrivacyPolicySubject { get; set; }
        [DataMember]
        public string PrivacyPolicy { get; set; }

        [DataMember]
        public Guid? AdminId { get; set; }
        [DataMember]
        public string Admin { get; set; }

        [DataMember]
        public string AdminLogin
        {
            get { return _adminLogin; }
            set
            {
                if (Set(ref _adminLogin, value, "AdminLogin"))
                    NotifyPropertyChanged("CanSendAdminConfirmationEmail");
            }
        }
        [DataMember]
        public string AdminEmail
        {
            get { return _adminEmail; }
            set
            {
                if (Set(ref _adminEmail, value, "AdminEmail"))
                    NotifyPropertyChanged("IsAdminEmailValid", "CanSendAdminConfirmationEmail", "CanContinue");
            }
        }
        [DataMember]
        public string AdminConfirmationEmailCode
        {
            get { return _adminConfirmationEmailCode; }
            set
            {
                if (Set(ref _adminConfirmationEmailCode, value, "AdminConfirmationEmailCode"))
                    NotifyPropertyChanged("AdminEmailCodeNotMatch", "AdminEmailCodeMatch", "CanContinue");
            }
        }
        [DataMember]
        public string AdminEmailCode
        {
            get { return _adminEmailCode; }
            set
            {
                if (Set(ref _adminEmailCode, value, "AdminEmailCode"))
                    NotifyPropertyChanged("ShowAdminConfirmationEmailCode", "AdminEmailCodeNotMatch", "AdminEmailCodeMatch");
            }
        }
        [DataMember]
        public bool AdminConfirmedEmail { get; set; }
        [DataMember]
        public bool AdminEmailCodeExpired { get { return _adminEmailCodeExpired; } set { Set(ref _adminEmailCodeExpired, value, "AdminEmailCodeExpired"); } }

            [DataMember]
        public string ContactName
        {
            get { return _contactName; }
            set
            {
                if (Set(ref _contactName, value, "ContactName"))
                    NotifyPropertyChanged("CanSendContactConfirmationEmail");
            }
        }
        [DataMember]
        public string ContactEmail
        {
            get { return _contactEmail; }
            set
            {
                if (Set(ref _contactEmail, value, "ContactEmail"))
                    NotifyPropertyChanged("IsContactEmailValid", "CanSendContactConfirmationEmail", "CanContinue");
            }
        }
        [DataMember]
        public string ContactConfirmationEmailCode
        {
            get { return _contactConfirmationEmailCode; }
            set
            {
                if (Set(ref _contactConfirmationEmailCode, value, "ContactConfirmationEmailCode"))
                    NotifyPropertyChanged("ContactEmailCodeNotMatch", "ContactEmailCodeMatch", "CanContinue");
            }
        }
        [DataMember]
        public string ContactEmailCode
        {
            get { return _contactEmailCode; }
            set
            {
                if (Set(ref _contactEmailCode, value, "ContactEmailCode"))
                    NotifyPropertyChanged("ShowContactConfirmationEmailCode", "ContactEmailCodeNotMatch", "ContactEmailCodeMatch");
            }
        }
        [DataMember]
        public bool ContactConfirmedEmail { get; set; }
        [DataMember]
        public bool ContactEmailCodeExpired { get { return _contactEmailCodeExpired; } set { Set(ref _contactEmailCodeExpired, value, "ContactEmailCodeExpired"); } }

        #endregion
        #region Admin Extended Properties

        public bool AdminEmailCodeNotMatch
        {
            get
            {
                if (string.IsNullOrEmpty(AdminEmailCode))
                    return false;

                if (string.IsNullOrEmpty(AdminConfirmationEmailCode))
                    return false;

                return AdminConfirmationEmailCode != AdminEmailCode;
            }
        }

        public bool AdminEmailCodeMatch
        {
            get
            {
                if (string.IsNullOrEmpty(AdminEmailCode))
                    return false;

                if (string.IsNullOrEmpty(AdminConfirmationEmailCode))
                    return false;

                return AdminConfirmationEmailCode == AdminEmailCode;
            }
        }

        public bool ShowAdminConfirmationEmailCode
        {
            get { return !string.IsNullOrEmpty(AdminEmailCode) && !AdminEmailCodeExpired; }
        }

        public bool IsAdminEmailValid
        {
            get { return IsEmailValid(AdminEmail); }
        }

        public bool CanSendAdminConfirmationEmail
        {
            get { return !string.IsNullOrEmpty(AdminLogin) && !string.IsNullOrEmpty(AdminEmail) && IsAdminEmailValid; }
        }

        #endregion
        #region Contact Extended Properties

        public bool ContactEmailCodeNotMatch
        {
            get
            {
                if (string.IsNullOrEmpty(ContactEmailCode))
                    return false;

                if (string.IsNullOrEmpty(ContactConfirmationEmailCode))
                    return false;
                    
                return ContactConfirmationEmailCode != ContactEmailCode;
            }
        }

        public bool ContactEmailCodeMatch
        {
            get
            {
                if (string.IsNullOrEmpty(ContactEmailCode))
                    return false;

                if (string.IsNullOrEmpty(ContactConfirmationEmailCode))
                    return false;

                return ContactConfirmationEmailCode == ContactEmailCode;
            }
        }

        public bool ShowContactConfirmationEmailCode
        {
            get { return !string.IsNullOrEmpty(ContactEmailCode) && !ContactEmailCodeExpired; }
        }

        public bool IsContactEmailValid
        {
            get { return IsEmailValid(ContactEmail); }
        }

        public bool CanSendContactConfirmationEmail
        {
            get { return !string.IsNullOrEmpty(ContactName) && !string.IsNullOrEmpty(ContactEmail) && IsContactEmailValid; }
        }

        #endregion

        public bool IsAdmin
        {
            get { return AdminId.HasValue; }
        }

        public bool CanContinue
        {
            get
            {
                if (!AdminId.HasValue)
                    return true;

                if (!string.IsNullOrEmpty(AdminEmail) && !IsAdminEmailValid)
                    return false;

                if (AdminEmailCodeNotMatch)
                    return false;

                if (!string.IsNullOrEmpty(ContactEmail) && !IsContactEmailValid)
                    return false;

                if (ContactEmailCodeNotMatch)
                    return false;

                return true;
            }
        }

        private bool IsEmailValid(string email)
        {
            return Regex.IsMatch(email,
                 @"^(?("")(""[^""]+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                 @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
        }

        public EmailContactConfirmationContract() { }
    }
}