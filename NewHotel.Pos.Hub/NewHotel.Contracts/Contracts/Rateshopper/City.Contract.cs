﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class CityContract : BaseContract
    {
        #region Contract properties

        [DataMember]
        public string Name {get;set;}
        [DataMember]
        public decimal Longitude { get; set; }
        [DataMember]
        public decimal Latitude { get; set; }
        [DataMember]
        public string Currency { get; set; }
        [DataMember]
        public short Radio { get; set; }
        [DataMember]
        public bool Active { get; set; }

        #endregion
        #region Constructor

        public CityContract()
            : base()
        {
        }

        #endregion
    }
}
