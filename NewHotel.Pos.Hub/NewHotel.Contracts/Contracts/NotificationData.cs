﻿using System;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.ServiceModel.Channels;
using System.Collections.Generic;
using NewHotel.Contracts;

namespace NewHotel.Communication.Interfaces
{
    public interface INotificationData
    {
        Message CreateMessage();
    }

    [MessageContract]
    public class NotificationData : INotificationData
    {
        #region Constants

        public const string NotificationAction = "http://tempuri.org/IBaseSubscriberService/Notify";
        public const int NOT_FOUND = 1;
        public const int ALREADY_RUNNING = 2;
        public const int NOT_RUNNING = 3;
        public const int UNKNOWN = 99;

        #endregion
        #region Members

        private readonly static DataContractSerializer _serializer = new DataContractSerializer(typeof(NotificationData));

        #endregion
        #region Properties

        [MessageBodyMember]
        public object Id { get; set; }
        [MessageBodyMember]
        public string Name { get; set; }
        [MessageBodyMember]
        public int Index { get; set; }
        [MessageBodyMember]
        public int Count { get; set; }
        [MessageBodyMember]
        public int Steps { get; set; }
        [MessageBodyMember]
        public int Current { get; set; }
        [MessageBodyMember]
        public string Message { get; set; }
        [MessageBodyMember]
        public SequentialExecContract Contract { get; set; }
        [MessageBodyMember]
        public ExceptionDetail Error { get; set; }
        [MessageBodyMember]
        public int Status { get; set; }
        [MessageBodyMember]
        public ValidationResult Result { get; set; }

        public bool Completed
        {
            get { return Index == Count; }
        }

        #endregion
        #region Public Methods

        public virtual Message CreateMessage()
        {
            return System.ServiceModel.Channels.Message.CreateMessage(MessageVersion.Soap12WSAddressing10,
                NotificationAction, this, _serializer);
        }

        #endregion
    }

    [MessageContract]
    public class ExportDocumentNotificationData : NotificationData
    {
        #region Members

        private readonly static DataContractSerializer _serializer = new DataContractSerializer(typeof(ExportDocumentNotificationData));

        #endregion
        #region Properties

        [MessageBodyMember]
        public List<byte[]> ExportDoc { get; set; }

        #endregion
        #region Public Methods

        public override Message CreateMessage()
        {
            return System.ServiceModel.Channels.Message.CreateMessage(MessageVersion.Soap12WSAddressing10,
                NotificationAction, this, _serializer);
        }

        #endregion
    }

    [MessageContract]
    public class SubscriptionData
    {
        #region Constants

        public const string SubscriptionAction = "http://tempuri.org/IBaseSubscriberService/Subscription";

        #endregion
        #region Members

        private readonly static DataContractSerializer _serializer = new DataContractSerializer(typeof(SubscriptionData));

        #endregion
        #region Properties

        [MessageBodyMember]
        public ExceptionDetail Error { get; set; }
        [MessageBodyMember]
        public bool IsSubscribed { get; set; }
        [MessageBodyMember]
        public int Status { get; set; }

        #endregion
        #region Public Methods

        public virtual Message CreateMessage()
        {
            return System.ServiceModel.Channels.Message.CreateMessage(MessageVersion.Soap12WSAddressing10,
                SubscriptionAction, this, _serializer);
        }

        #endregion
    }

    [MessageContract]
    public class SubscriptionWorkerData : SubscriptionData
    {
        #region Members

        private readonly static DataContractSerializer _serializer = new DataContractSerializer(typeof(SubscriptionWorkerData));

        #endregion
        #region Properties

        [MessageBodyMember]
        public SequentialExecContract Contract { get; set; }

        #endregion
        #region Public Methods

        public override Message CreateMessage()
        {
            return System.ServiceModel.Channels.Message.CreateMessage(MessageVersion.Soap12WSAddressing10,
                SubscriptionAction, this, _serializer);
        }

        #endregion
    }

    [DataContract]
    public class WorkerExecData
    {
        #region Constants

        public const string ExecuteAction = "http://tempuri.org/IBaseWorkerService/Execute";

        #endregion
        #region Members

        private readonly static DataContractSerializer _serializer = new DataContractSerializer(typeof(WorkerExecData));

        #endregion
        #region Properties

        [DataMember]
        public SequentialExecContract UserState { get; set; }
        [DataMember]
        public bool StopOnError { get; set; }
        [DataMember]
        public Guid Id { get; set; }

        #endregion
        #region Public Methods

        public virtual Message CreateMessage()
        {
            return Message.CreateMessage(MessageVersion.Soap12WSAddressing10,
                ExecuteAction, this, _serializer);
        }

        #endregion
    }
}