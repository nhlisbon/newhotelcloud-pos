﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Globalization;

namespace NewHotel.Contracts
{ 
    [DataContract]
	[Serializable]
    public class AppContextData : BaseContract
    {
        #region Members

        [DataMember]
        internal long _languageId;
        [DataMember]
        internal string _cultureName;
        [DataMember]
        internal Guid? _userId;
        [DataMember]
        internal DateTime _sysDate;
        [DataMember]
        internal KeyDescRecord[] _languages;
        [DataMember]
        internal int _maxTabCount;
        [DataMember]
        internal Dictionary<string, KeyDescRecord[]> _enums;
        [DataMember]
        internal short _roleId;
        [DataMember]
        internal string _roleOpts;

        #endregion
        #region Constructors

        public AppContextData(long languageId, string cultureName, Guid? userId, DateTime sysDate, KeyDescRecord[] languages,
            int maxTabCount, Dictionary<string, KeyDescRecord[]> enums)
        {
            _languageId = languageId;
            _cultureName = cultureName;
            _userId = userId;
            _sysDate = sysDate;
            _languages = languages;
            _maxTabCount = maxTabCount;
            _enums = enums;
        }

        #endregion
        #region Properties

        public long LanguageId
        {
            get { return _languageId; }
        }

        public string CultureName
        {
            get { return _cultureName; }
        }

        public Guid? UserId
        {
            get { return _userId; }
        }

        public DateTime SysDate
        {
            get { return _sysDate; }
        }

        public KeyDescRecord[] LanguageData
        {
            get { return _languages;}
        }

        public int MaxTabCount
        {
            get { return _maxTabCount; }
        }

        public Dictionary<string, KeyDescRecord[]> EnumData
        {
            get { return _enums; }
            set { _enums = value; }
        }

        public short RoleId
        {
            get { return _roleId; }
            set { _roleId = value; }
        }

        public string RoleOpts
        {
            get { return _roleOpts; }
            set { _roleOpts = value; }
        }

        public IDictionary<long, long> UserPermissions { get; set; }

        #endregion
    }
}
