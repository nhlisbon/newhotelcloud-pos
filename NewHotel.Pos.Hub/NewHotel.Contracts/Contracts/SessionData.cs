﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SessionData : ISessionData
    {
        #region Properties

		[DataMember]
		public string Version { get; set; }
		[DataMember]
		public long ApplicationId { get; set; }
        [DataMember]
        public string ModuleName { get; set; }
        [DataMember]
        public Guid InstallationId { get; set; }
		[DataMember]
		public Guid UserId { get; set; }
        [DataMember]
        public long LanguageId { get; set; }
        [DataMember]
        public DateTime SysDate { get; set; }
        [DataMember]
        public DateTime WorkDate { get; set; }
        [DataMember]
        public short WorkShift { get; set; }
        [DataMember]
        public DateTime? FirstOpenedDate { get; set; }
        [DataMember]
        public int? LastOpenedYear { get; set; }
        [DataMember]
        public DateTime ClientDateTime { get; set; }
        [DataMember]
        public Guid ChannelId { get; set; }

        #endregion
    }
}