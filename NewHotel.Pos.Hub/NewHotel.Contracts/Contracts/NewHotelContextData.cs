﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Globalization;
using System.Threading;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    /// <summary>
    /// SECU_CODE = Codigo de Seguridad (Hide, Read, Read/Print, Read/Write, Full Access)
    /// </summary>
    public enum SecurityCode : long { Hide = 2581, Read = 2582, ReadPrint = 2583, ReadWrite = 2584, FullAccess = 2586 };

    [DataContract]
	[Serializable]
    public class NewHotelContextData : BaseContract
    {
        #region Members

		[DataMember]
		internal string _version;
        [DataMember]
        internal int _maxTabCount;
        [DataMember]
        internal Guid _userId;
        [DataMember]
        internal string _name;
        [DataMember]
        internal long _applicationId;
        [DataMember]
        internal string _moduleName;
        [DataMember]
        internal Guid _installationId;
        [DataMember]
        internal string _installationName;
        [DataMember]
        internal string _installationCommercialName;
        [DataMember]
        internal string _installationFiscalName;
        [DataMember]
        internal long _languageId;
        [DataMember]
        internal string _cultureName;
        [DataMember]
        internal string _cultureNameFormat;
        [DataMember]
        internal Dictionary<string, KeyDescRecord[]> _enums;
        [DataMember]
        internal KeyDescRecord[] _languages;
        [DataMember]
        internal DateTime _sysDate;
        [DataMember]
        internal DateTime _workDate;
        [DataMember]
        internal short _workshift;
        [DataMember]
        internal DateTime? _firstOpenedDate;
        [DataMember]
        internal int? _lastOpenedYear;
        [DataMember]
        internal string _currencyId;
        [DataMember]
        internal string _currencySymbol;
        [DataMember]
        internal short? _currencyDecInPercents;
        [DataMember]
        internal short? _currencyDecInPrices;
        [DataMember]
        internal short? _currencyDecInExchange;
        [DataMember]
        internal IDictionary<string, short> _optionPermissions;
        [DataMember]
        internal IDictionary<long, long> _userPermissions;
        [DataMember]
        internal IDictionary<long, IList<PermissionGroupDetails>> _userPermissionsDetails;
        [DataMember]
        internal bool _firstTimeRun;
        [DataMember]
        internal string _country;
        [DataMember]
        internal LicenseLimitsContract _limits;
        [DataMember]
        internal bool _pointOfSaleIntegration;
        [DataMember]
        internal bool _isInternalUser;
        [DataMember]
        internal bool _isGroupAdmin;
        [DataMember]
        internal InitialPanel? _initialPanel;
        [DataMember]
        internal bool _keepKeyWindowsOpened;
        [DataMember]
        internal bool _hideCheckOutOnRoomPlanning;
        [DataMember]
        internal bool _preventAutoloadReservation;
        [DataMember]
        internal bool _preventAutoloadGroup;
        [DataMember]
        internal bool _preventAutoloadGuest;
        [DataMember]
        internal bool _preventAutoloadClient;
        [DataMember]
        internal bool _preventAutoloadEntity;
        [DataMember]
        internal bool _preventAutoloadMovement;
        [DataMember]
        internal bool _exportAccounting;
        [DataMember]
        internal short? _licenseType;
        [DataMember]
        internal bool _availabilityOnline;
        [DataMember]
        internal bool _availabilityExtraBed;
        [DataMember]
        internal bool _availabilityReservations;
        [DataMember]
        internal bool _availabilityWaitingList;
        [DataMember]
        internal bool _availabilityAttempt;
        [DataMember]
        internal bool _availabilitySummary;
        [DataMember]
        internal bool _availabilityExtended;
        [DataMember]
        internal bool _linkedSegmentOrigins;
        [DataMember]
        internal KeyDescRecord[] _confirmationStates;

        private CultureInfo _culture = null;
        private CultureInfo _cultureFormat = null;

        #endregion
        #region Constructors

        public NewHotelContextData(int maxTabCount,
            Guid userId, string name, long applicationId,
            Guid installationId, string installationName,
            string installationCommercialName, string installationFiscalName, long languageId,
            string cultureName, short workshift, DateTime workDate, DateTime sysDate,
            DateTime? firstOpenedDate, int? lastOpenedYear,
            string currencyId, string currencySymbol,
            short? currencyDecInPercents, short? currencyDecInPrices, short? currencyDecInExchange,
            Dictionary<string, KeyDescRecord[]> enums, KeyDescRecord[] languages,
            IDictionary<long, long> userPermissions,
            IDictionary<long, IList<PermissionGroupDetails>> userPermissionsDetails,
            LicenseLimitsContract limits,
            InitialPanel? initialPanel = null)
        {
            _maxTabCount = maxTabCount;
            _userId = userId;
            _name = name;
            _applicationId = applicationId;
            _installationId = installationId;
            _installationName = installationName;
            _installationCommercialName = installationCommercialName;
            _installationFiscalName = installationFiscalName;
            _languageId = languageId;
            _cultureName = cultureName;
            _enums = enums;
            _languages = languages;
            _workDate = workDate;
            _workshift = workshift;
            _firstOpenedDate = firstOpenedDate;
            _lastOpenedYear = lastOpenedYear;
            _sysDate = sysDate;
            _currencyId = currencyId;
            _currencySymbol = currencySymbol;
            _currencyDecInPercents = currencyDecInPercents;
            _currencyDecInPrices = currencyDecInPrices;
            _currencyDecInExchange = currencyDecInExchange;
            _userPermissions = userPermissions;
            _userPermissionsDetails = userPermissionsDetails;
            _limits = limits;
            _initialPanel = initialPanel;
        }

        public NewHotelContextData(int maxTabCount,
           Guid userId, string name, long applicationId,
           Guid installationId, string installationName, string installationCommercialName, string installationFiscalName,
           long languageId, string cultureName, short workshift, DateTime workDate, DateTime sysDate,
           DateTime? firstOpenedDate, int? lastOpenedYear, string currencyId, string currencySymbol,
           short? currencyDecInPercents, short? currencyDecInPrices, short? currencyDecInExchange,
           Dictionary<string, KeyDescRecord[]> enums, KeyDescRecord[] languages, InitialPanel? initialPanel = null)
        {
            _maxTabCount = maxTabCount;
            _userId = userId;
            _name = name;
            _applicationId = applicationId;
            _installationId = installationId;
            _installationName = installationName;
            _installationCommercialName = installationCommercialName;
            _installationFiscalName = installationFiscalName;
            _languageId = languageId;
            _cultureName = cultureName;
            _enums = enums;
            _languages = languages;
            _workDate = workDate;
            _workshift = workshift;
            _firstOpenedDate = firstOpenedDate;
            _lastOpenedYear = lastOpenedYear;
            _sysDate = sysDate;
            _currencyId = currencyId;
            _currencySymbol = currencySymbol;
            _currencyDecInPercents = currencyDecInPercents;
            _currencyDecInPrices = currencyDecInPrices;
            _currencyDecInExchange = currencyDecInExchange;
            _userPermissions = new Dictionary<long, long>();
            _userPermissionsDetails =  new Dictionary<long,  IList<PermissionGroupDetails>>();
            _initialPanel = initialPanel;
        }

        public NewHotelContextData(int maxTabCount, Guid userId, long languageId, string cultureName, DateTime sysDate,
            KeyDescRecord[] languages, Dictionary<string, KeyDescRecord[]> enums, IDictionary<long, long> userPermissions, 
            IDictionary<long, IList<PermissionGroupDetails>> userPermissionsDetails, LicenseLimitsContract limits, InitialPanel? initialPanel = null)
            : this(maxTabCount, userId, string.Empty, 1, Guid.Empty, string.Empty, string.Empty, string.Empty, languageId, cultureName,
            0, sysDate, sysDate, null, null, string.Empty, string.Empty, 2, 2, 2, enums, languages, userPermissions, userPermissionsDetails, limits, initialPanel)
        {
        }

        public NewHotelContextData(Guid installationId, string installationName, string installationCommercialName, 
            string installationFiscalName, long languageId, string cultureName, Dictionary<string, KeyDescRecord[]> enums)
            : this(0, Guid.Empty, string.Empty, 1, installationId, installationName, installationCommercialName, installationFiscalName, languageId, cultureName,
            0, DateTime.Now.ToUtcDateTime().Date, DateTime.Now.ToUtcDateTime().Date, null, null, string.Empty, string.Empty, 2, 2, 2, enums, null,null,null, null)
        {
        }

        public NewHotelContextData(Guid installationId, string installationName, string installationCommercialName, string installationFiscalName, 
            long languageId, string cultureName)
            : this(0, Guid.Empty, string.Empty, 1, installationId, installationName, installationCommercialName, installationFiscalName, languageId, cultureName,
            0, DateTime.Now.ToUtcDateTime().Date, DateTime.Now.ToUtcDateTime().Date, null, null, string.Empty, string.Empty, 2, 2, 2, null, null, null, null, null)
        {
        }

        public NewHotelContextData()
            : this(Guid.Empty, string.Empty, string.Empty, string.Empty, 1033, "en-US", null)
        {
        }

        #endregion
        #region Properties

		public string Version
		{
			get { return _version; }
			set { Set(ref _version, value, "Version"); }
		}

        public int MaxTabCount
        {
            get { return _maxTabCount; }
            set { Set(ref _maxTabCount, value, "MaxTabCount"); }
        }

        public Guid UserId
        {
            get { return _userId; }
            set { Set(ref _userId, value, "UserId"); }
        }

        public string Name
        {
            get { return _name; }
            set { Set(ref _name, value, "Name"); }
        }

        public long ApplicationId
        {
            get { return _applicationId; }
            set { Set(ref _applicationId, value, "ApplicationId"); }
        }

        public string ModuleName
        {
            get { return _moduleName; }
            set { Set(ref _moduleName, value, "ModuleName"); }
        }

        public Guid InstallationId
        {
            get { return _installationId; }
            set { Set(ref _installationId, value, "InstallationId"); }
        }

        public string InstallationName
        {
            get { return _installationName; }
            set { Set(ref _installationName, value, "InstallationName"); }
        }

        public string InstallationCommercialName
        {
            get { return _installationCommercialName; }
            set { Set(ref _installationCommercialName, value, "InstallationCommercialName"); }
        }

        public string InstallationFiscalName
        {
            get { return _installationFiscalName; }
            set { Set(ref _installationFiscalName, value, "InstallationFiscalName"); }
        }
        
        public long LanguageId
        {
            get { return _languageId; }
            set { Set(ref _languageId, value, "LanguageId"); }
        }

        public string CultureName
        {
            get { return _cultureName; }
            set 
            {
                if (Set(ref _cultureName, value, "CultureName"))
                {
                    _culture = null;
                    Thread.CurrentThread.CurrentUICulture = CultureInfo;
                    Thread.CurrentThread.CurrentCulture = CultureInfo;
                }
            }
        }

        public string CultureNameFormat
        {
            get { return _cultureNameFormat; }
            set
            {
                if (Set(ref _cultureNameFormat, value, "CultureNameFormat"))
                {
                    _cultureFormat = null;
                    
                    var culture = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
                    culture.DateTimeFormat.ShortDatePattern = CultureInfoFormat.DateTimeFormat.ShortDatePattern;
                    culture.DateTimeFormat.ShortTimePattern = CultureInfoFormat.DateTimeFormat.ShortTimePattern;
                    culture.DateTimeFormat.YearMonthPattern = CultureInfoFormat.DateTimeFormat.YearMonthPattern;

                    Thread.CurrentThread.CurrentCulture = culture;
                    Thread.CurrentThread.CurrentUICulture = culture;
                }
            }
        }

        public CultureInfo CultureInfo
        {
            get
            {
                if (_culture == null)
                {
                    if (string.IsNullOrEmpty(_cultureName))
                        _culture = CultureInfo.InvariantCulture;
                    else
                        _culture = new CultureInfo(_cultureName);
                }

                return _culture;
            }
        }

        public CultureInfo CultureInfoFormat
        {
            get
            {
                if (_cultureFormat == null)
                {
                    if (string.IsNullOrEmpty(_cultureNameFormat))
                        _cultureFormat = CultureInfo.InvariantCulture;
                    else
                        _cultureFormat = new CultureInfo(_cultureNameFormat);
                }

                return _cultureFormat;
            }
        }

        public DateTime SysDate
        {
            get { return _sysDate; }
            set { Set(ref _sysDate, value, "SysDate"); }
        }

        public DateTime WorkDate
        {
            get { return _workDate; }
            set { Set(ref _workDate, value, "WorkDate"); }
        }

        public short WorkShift
        {
            get { return _workshift; }
            set { Set(ref _workshift, value, "WorkShift"); }
        }

        public DateTime ClientDateTime
        {
            get { return DateTime.Now.ToUtcDateTime(); }
        }

        public int? FirstOpenedYear
        {
            get
            {
                if (_firstOpenedDate.HasValue)
                    return _firstOpenedDate.Value.Year;

                return null;
            }
        }

        public DateTime? FirstOpenedDate
        {
            get { return _firstOpenedDate; }
            set { Set(ref _firstOpenedDate, value, "FirstOpenedDate"); }
        }

        public int? LastOpenedYear
        {
            get { return _lastOpenedYear; }
            set { Set(ref _lastOpenedYear, value, "LastOpenedYear"); }
        }

        public DateTime? LastOpenedDate
        {
            get
            {
                if (_lastOpenedYear.HasValue)
                    return new DateTime(_lastOpenedYear.Value, 12, 31);

                return null;
            }
        }

        public KeyDescRecord[] LanguageData
        {
            get { return _languages; }
            set { _languages = value; }
        }

        public Dictionary<string, KeyDescRecord[]> EnumData
        {
            get { return _enums; }
            set { _enums = value; }
        }

        public string CurrencyId
        {
            get { return _currencyId; }
            set { _currencyId = value; }
        }

        public string CurrencySymbol
        {
            get { return _currencySymbol; }
            set { _currencySymbol = value; }
        }

        public short? CurrencyDecInPercents
        {
            get { return _currencyDecInPercents; }
            set { _currencyDecInPercents = value; }
        }

        public short? CurrencyDecInPrices
        {
            get { return _currencyDecInPrices; }
            set { _currencyDecInPrices = value; }
        }

        public short? CurrencyDecInExchange
        {
            get { return _currencyDecInExchange; }
            set { _currencyDecInExchange = value; }
        }

        public bool FirstTimeRun
        {
            get { return _firstTimeRun; }
            set { _firstTimeRun = value; }
        }

        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }

        public bool PointOfSaleIntegration
        {
            get { return _pointOfSaleIntegration; }
            set { _pointOfSaleIntegration = value; }
        }

        public bool IsInternalUser
        {
            get { return _isInternalUser; }
            set { _isInternalUser = value; }
        }

        public bool IsGroupAdministrator
        {
            get { return _isGroupAdmin; }
            set { _isGroupAdmin = value; }
        }

        public bool KeepKeyWindowsOpened
        {
            get { return _keepKeyWindowsOpened; }
            set { _keepKeyWindowsOpened = value; }
        }

        public bool HideCheckOutOnRoomPlanning
        {
            get { return _hideCheckOutOnRoomPlanning; }
            set { _hideCheckOutOnRoomPlanning = value; }
        }

        public bool PreventAutoloadReservation
        {
            get { return _preventAutoloadReservation; }
            set { _preventAutoloadReservation = value; }
        }

        public bool PreventAutoloadGroup
        {
            get { return _preventAutoloadGroup; }
            set { _preventAutoloadGroup = value; }
        }

        public bool PreventAutoloadGuest
        {
            get { return _preventAutoloadGuest; }
            set { _preventAutoloadGuest = value; }
        }

        public bool PreventAutoloadClient
        {
            get { return _preventAutoloadClient; }
            set { _preventAutoloadClient = value; }
        }

        public bool PreventAutoloadEntity
        {
            get { return _preventAutoloadEntity; }
            set { _preventAutoloadEntity = value; }
        }

        public bool PreventAutoloadMovement
        {
            get { return _preventAutoloadMovement; }
            set { _preventAutoloadMovement= value; }
        }

        public bool ExportAccounting
        {
            get { return _exportAccounting; }
            set { _exportAccounting = value; }
        }

        public bool AvailabilityOnline
        {
            get { return _availabilityOnline; }
            set { _availabilityOnline = value; }
        }

        public bool AvailabilityExtraBed
        {
            get { return _availabilityExtraBed; }
            set { _availabilityExtraBed = value; }
        }

        public bool AvailabilityReservations
        {
            get { return _availabilityReservations; }
            set { _availabilityReservations = value; }
        }

        public bool AvailabilityWaitingList
        {
            get { return _availabilityWaitingList; }
            set { _availabilityWaitingList = value; }
        }

        public bool AvailabilityAttempt
        {
            get { return _availabilityAttempt; }
            set { _availabilityAttempt = value; }
        }

        public bool AvailabilitySummary
        {
            get { return _availabilitySummary; }
            set { _availabilitySummary = value; }
        }

        public bool AvailabilityExtended
        {
            get { return _availabilityExtended; }
            set { _availabilityExtended = value; }
        }

        public bool LinkedSegmentOrigins
        {
            get { return _linkedSegmentOrigins; }
            set { _linkedSegmentOrigins = value; }
        }

        public short? LicenseType
        {
            get { return _licenseType; }
            set { _licenseType = value; }
        }
        
        public KeyDescRecord[] ConfirmationStates
        {
            get { return _confirmationStates; }
            set { _confirmationStates = value; }
        }

        public IDictionary<long, IList<PermissionGroupDetails>> UserPermissionsDetails
        {
            get { return _userPermissionsDetails; }
            set { _userPermissionsDetails = value; }
        }

        public IDictionary<string, short> OptionPermissions
        {
            get { return _optionPermissions; }
            set { _optionPermissions = value; }
        }

        public IDictionary<long, long> UserPermissions
        {
            get { return _userPermissions; }
            set { _userPermissions = value; }
        }

        public InitialPanel? InitialPanel
        {
            set { _initialPanel = value; }
            get { return _initialPanel; }
        }

        public LicenseLimitsContract Limits
        {
            get { return _limits; }
            set { Set(ref _limits, value, "Limits"); }
        }
       
        [DataMember]
        public bool ValidLoginPeriod { get; set; }
        [DataMember]
        public DateTime? UserValidUntil { get; set; }
        [DataMember]
        public bool AllowMultiSchemaInvoicing { get; set; }
        [DataMember]
        public bool IsMultiProperty { get; set; }
        [DataMember]
        public bool IsCondoFriendly { get; set; }
        [DataMember]
        public bool LocalStock { get; set; }
        [DataMember]
        public bool IsUserMultiProperty { get; set; }
        [DataMember]
        public bool AsTimeshare { get; set; }
        [DataMember]
        public bool RestrictedInvoiceGrouping { get; set; }

        public SecurityCode? GetSecurityCode(long group)
        {
            SecurityCode? code = null;

            long value;
            if (_userPermissions.TryGetValue(group, out value))
                code = (SecurityCode)value;

            return code;
        }

        public SecurityCode? GetSecurityCodePermission(long group, long permission)
        {
            if (permission == 0)
                return GetSecurityCode(group);
            else
            {
                IList<PermissionGroupDetails> details;
                if (_userPermissionsDetails.TryGetValue(group, out details))
                {
                    var securityCode = details.FirstOrDefault(x => x.Permission.Equals(permission));
                    if (securityCode != null)
                        return (SecurityCode)securityCode.SecurityCodePermission;
                }
            }

            return null;
        }

        #endregion
    }
}