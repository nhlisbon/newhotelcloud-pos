﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ValidationGroupedContract : BaseContract
    {
        #region Properties

        [DataMember]
        public new IList<string> Errors { get; internal set; }

        [DataMember]
        public IList<string> Warnings { get; internal set; }

        [ReflectionExclude]
        [DataMember]
        public bool IsEmpty 
        { 
            get { return !HasWarnings && !HasErrors; } 
        }

        [ReflectionExclude]
        [DataMember]
        public bool HasWarnings 
        { 
            get { return Warnings.Count != 0; } 
        }

        [ReflectionExclude]
        [DataMember]
        public bool HasErrors 
        { 
            get { return Errors.Count != 0; } 
        }

        [DataMember]
        public string GroupName { get; internal set; }

        #endregion
        #region Constructor

        public ValidationGroupedContract(string groupName)
        {
            GroupName = groupName;
            Errors = new List<string>();
            Warnings = new List<string>();
        }

        #endregion
    }
}
