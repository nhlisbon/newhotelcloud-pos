﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class NameValueContract : BaseContract<string>
    {
        #region Members

        private string _name;
        private string _value;

        #endregion
        #region Properties

        [DataMember]
        public string Name { get { return _name; } set { _name = value; } }
        [DataMember]
        public string Value { get { return _value; } set { Set(ref _value, value, "Value"); } }

        #endregion

        public NameValueContract(string id, string name, string value)
        {
            Id = id;
            Name = name;
            Value = value;
        }

        public NameValueContract(string id, string name)
            : this(id, name, string.Empty)
        {
        }
    }
}