﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace NewHotel.Contracts
{
    public class SerializableDictionary<TKey, TValue>
    {
        private List<TKey> _keys;
        private List<TValue> _values;

        public SerializableDictionary()
        {
            _keys = new List<TKey>();
            _values = new List<TValue>();
        }

        public TValue this[TKey key]
        {
            get
            {
                if (key == null) throw new ArgumentNullException();
                int index = _keys.IndexOf(key);
                return _values[index];
            }
            set
            {
                if (key == null) throw new ArgumentNullException();
                int index = _keys.IndexOf(key);
                _values[index] = value;
            }
        }

        public void Add(TKey key, TValue value)
        {
            if(key == null) throw new ArgumentNullException();
            if (_keys.Contains(key)) throw new ArgumentException();
            _keys.Add(key);
            _values.Add(value);
        }

        public bool ContainsKey(TKey key)
        {
            return _keys.Contains(key);
        }

        public List<TKey> Keys
        {
            get
            {
                return _keys;
            }
        }

        public List<TValue> Values
        {
            get
            {
                return _values;
            }
        }
	}


}
