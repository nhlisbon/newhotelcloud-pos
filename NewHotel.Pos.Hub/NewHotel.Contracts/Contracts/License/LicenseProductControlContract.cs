﻿namespace NewHotel.Contracts
{
    public class LicenseProductControlContract
    {
        #region Properties

        public int Identifier { get; set; }
        public string ControlName { get; set; }
        public bool Active { get; set; }
        public long Value { get; set; }

        #endregion
    }
}