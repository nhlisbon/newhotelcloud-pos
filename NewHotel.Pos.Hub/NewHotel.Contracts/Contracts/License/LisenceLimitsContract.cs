﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public enum LicensePeriodStatus : long
    {
        NotFound = 0,
        Ok = 1,
        Warning = 2,
        DuePayment = 3,
        Overdue = 4,
        LastCall = 5,
        Cancelled = 6,
        Forbidden = 7
    }

    [DataContract]
	[Serializable]
    public class LicenseLimitsContract : BaseContract
    {
        [DataMember]
        public LicensePeriodStatus Status { get; set; }
        [DataMember]
        public DateTime DateLimit { get; set; }
        [DataMember]
        public bool MultiProperty { get; set; }

        [DataMember]
        public long Users { get; set; }
        [DataMember]
        public bool UserActive { get; set; }

        #region PMS

        [DataMember]
        public long Rooms { get; set; }
        [DataMember]
        public bool RoomsActive { get; set; }
        [DataMember]
        public long Bookings { get; set; }
        [DataMember]
        public bool BookingsActive { get; set; }
        [DataMember]
        public long Transactions { get; set; }
        [DataMember]
        public bool TransactionsActive { get; set; }
        [DataMember]
        public long Companies { get; set; }
        [DataMember]
        public bool CompaniesActive { get; set; }

        [DataMember]
        public bool RevenueManagement { get; set; }
        [DataMember]
        public bool PostalCodeSoftware { get; set; }

        #endregion
        #region Accounting

        [DataMember]
        public bool AccountingIKASA { get; set; }
        [DataMember]
        public bool AccountingCIGAM { get; set; }
        [DataMember]
        public bool AccountingTOTVS { get; set; }
        [DataMember]
        public bool AccountingSALES { get; set; }
        [DataMember]
        public bool AccountingPURCHASES { get; set; }
        [DataMember]
        public bool AccountingBAVEL { get; set; }
        [DataMember]
        public bool AccountingCMFlex { get; set; }

        #endregion
        #region Constructor

        public LicenseLimitsContract()
            : base()
        {
            Status = LicensePeriodStatus.NotFound;
            DateLimit = DateTime.MaxValue;
        }

        #endregion
    }
}