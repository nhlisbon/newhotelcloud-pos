﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public class LicensePropertyContract
    {
        #region Constructor
        public LicensePropertyContract()
        {
            Products = new List<LicenseProductContract>();
        }
        #endregion
        #region Properties
        public string PropertyCode { get; set; }
        public string PropertyName { get; set; }
        #endregion
        #region Lists
        public List<LicenseProductContract> Products { get; set; }
        #endregion
    }
}
