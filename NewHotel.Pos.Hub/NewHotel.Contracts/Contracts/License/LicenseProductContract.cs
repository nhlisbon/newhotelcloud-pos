﻿using System;
using System.Collections.Generic;

namespace NewHotel.Contracts
{
    public class LicenseProductContract
    {
        #region Constructor

        public LicenseProductContract()
        {
            Type = LicenseProductType.Trial;
            Level = LicenseProductLevel.Limited;
            Controls = new List<LicenseProductControlContract>();
        }

        #endregion
        #region Properties

        public string Code { get; set; }
        public LicenseProductType Type { get; set; }
        public DateTime DateLimit { get; set; }
        public DateTime StartDate { get; set; }
        public int DaysAlert { get; set; }
        public int DaysSuspend { get; set; }
        public bool Autogenerate { get; set; }
        public bool Active { get; set; }
        public LicenseProductLevel Level { get; set; }
        public LicenseProductModificationContract Modified { get; set; }

        #endregion
        #region Lists

        public List<LicenseProductControlContract> Controls { get; set; }

        #endregion
    }
}