﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace NewHotel.Contracts
{
    public class LicenseContract
    {
        #region Constructor

        public LicenseContract()
        {
            Properties = new List<LicensePropertyContract>();
        }

        #endregion
        #region Properties

        public string LicenseNumber { get; set; }

        #endregion
        #region Lists

        public List<LicensePropertyContract> Properties { get; set; }

        #endregion
        #region Public Methods

        public static LicenseContract LoadFromXml(string xml)
        {
            var serializer = new XmlSerializer(typeof(LicenseContract));
            using (var stream = new MemoryStream())
            {
                try
                {
                    var encoder = Encoding.UTF8;
                    foreach (var b in encoder.GetBytes(xml))
                        stream.WriteByte(b);
                    stream.Seek(0, SeekOrigin.Begin);

                    return (LicenseContract)serializer.Deserialize(stream);
                }
                finally
                {
                    stream.Close();
                }
            }
        }

        public static string SaveToXml(LicenseContract contract)
        {
            var serializer = new XmlSerializer(typeof(LicenseContract));
            using (var stream = new MemoryStream())
            {
                try
                {
                    serializer.Serialize(stream, contract);
                    stream.Seek(0, SeekOrigin.Begin);
                    using (var reader = new StreamReader(stream))
                    {
                        try
                        {
                            return reader.ReadToEnd();
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }
                }
                finally
                {
                    stream.Close();
                }
            }
        }

        #endregion
        #region Default Property Shortcut

        public LicensePropertyContract Property { get { return Properties.Count > 0 ? Properties[0] : null; } }

        #endregion
        #region Static Help Properties

        public static LicenseProductContract GenerateProduct(string name)
        {
            var contract = new LicenseProductContract();
            contract.Active = true;
            contract.Autogenerate = true;
            contract.Code = name;
            contract.Controls = new List<LicenseProductControlContract>();
            contract.DateLimit = DateTime.Today;
            contract.DaysAlert = 5;
            contract.DaysSuspend = 5;
            contract.StartDate = DateTime.Today;

            return contract;
        } 

        #endregion       
    }
}