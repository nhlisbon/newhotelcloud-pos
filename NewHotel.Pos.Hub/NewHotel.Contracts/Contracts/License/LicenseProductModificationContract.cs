﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public class LicenseProductModificationContract
    {
        #region Properties
        public string User { get; set; }
        public DateTime TimeStamp { get; set; }
        #endregion
    }
}
