﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class HotelLicenseContract : BaseContract
    {
        #region Private Members
        private Guid _installationId;
        #endregion
        #region Constructor
        public HotelLicenseContract()
        {

        } 
        #endregion
        #region Public Properties
        [DataMember]
        public Guid InstallationId { get { return _installationId; } set { Set(ref _installationId, value, "InstallationId"); } }
        #endregion
        #region Licenses
        [DataMember]
        public HotelLicenseContractPms Pms { get; set; }
        [DataMember]
        public HotelLicenseContractPos Pos { get; set; }
        [DataMember]
        public HotelLicenseContractAccounting Acc { get; set; }
        [DataMember]
        public HotelLicenseContractSpa Spa { get; set; }
        [DataMember]
        public HotelLicenseContractGolf Golf { get; set; }
        [DataMember]
        public HotelLicenseContractEvents Events { get; set; }
        [DataMember]
        public HotelLicenseContractStock Stock { get; set; }
        [DataMember]
        public HotelLicenseContractCondo Condo { get; set; }
        [DataMember]
        public HotelLicenseContractSurvey Survey { get; set; }
        [DataMember]
        public HotelLicenseContractNewGes NewGes { get; set; } 
        #endregion
    }


    [DataContract]
	[Serializable]
    public class HotelLicenseGeneralContract : BaseContract
    {
        //[ACTIVE]:1+[TYPE]:5+[LEVEL]:5+[STARTDATE]:6(YYMMDD)+[ENDDATE]:6(YYMMDD)+[DAYSBEFORE]:2+[DAYSAFTER]:2 (26)
        #region Constructor
        public HotelLicenseGeneralContract()
        {
            License = new string('0', 27);
            Active = false;
            Type = LicenseProductType.Trial;
            Level = LicenseProductLevel.Basic;
            StartDate = DateTime.Today;
            EndDate = DateTime.Today.AddMonths(1);
            DaysAfter = 7;
            DaysBefore = 7;

            Id = NewGuid();
        }
        #endregion
        #region Private Members
        private long _application;
        private StringBuilder _licenseContent;
        #endregion
        #region Public Property
        [DataMember]
        public long Application { get { return _application; } set { Set(ref _application, value, "Application"); } }
        [DataMember]
        public string License
        {
            get
            {
                return _licenseContent.ToString();
            }
            set
            {
                _licenseContent = new StringBuilder(value);
            }
        }
        #endregion
        #region Decoding Properties
        /// <summary>
        /// Active
        /// Position:0 Lenght:1
        /// </summary>
        public bool Active
        {
            get
            {
                return Get(0, 1) == "1";
            }
            set
            {
                if (value) Set("1", 0, 1); else Set("0", 0, 1);
            }
        }
        /// <summary>
        /// Type (Sale, Trial)
        /// Position:1 Lenght:5
        /// </summary>
        public LicenseProductType Type
        {
            get
            {
                return (LicenseProductType)Enum.Parse(typeof(LicenseProductType),Get(1, 5), true);
            }
            set
            {
                Set(((long)value).ToString("00000"), 1, 5);
            }
        }
        /// <summary>
        /// Level (Full, Basic, Limited)
        /// </summary>
        public LicenseProductLevel Level
        {
            get
            {
                return (LicenseProductLevel)Enum.Parse(typeof(LicenseProductType), Get(6, 5), true);
            }
            set
            {
                Set(((long)value).ToString("00000"), 6, 5);
            }
        }
        /// <summary>
        /// Start Date (Year:11 Month:13, Day:15)
        /// </summary>
        public DateTime StartDate
        {
            get
            {

                return DateTime.ParseExact(Get(11, 6), "ddMMyy", System.Threading.Thread.CurrentThread.CurrentCulture);
            }
            set
            {
                Set(value.ToString("ddMMyy"), 11, 6);
            }
        }
        /// <summary>
        /// End Date (Year:17 Month:19, Day:21)
        /// </summary>
        public DateTime EndDate
        {
            get
            {
                return DateTime.ParseExact(Get(17, 6), "ddMMyy", System.Threading.Thread.CurrentThread.CurrentCulture);
            }
            set
            {
                Set(value.ToString("ddMMyy"), 17, 6);
            }
        }
        /// <summary>
        /// Days Before Alert (Days:23)
        /// </summary>
        public int DaysBefore
        {
            get
            {
                return int.Parse(Get(23, 2));
            }
            set
            {
                Set(value.ToString("00"), 23, 2);
            }
        }
        /// <summary>
        /// Days After Alert (Days:25)
        /// </summary>
        public int DaysAfter
        {
            get
            {
                return int.Parse(Get(25, 2));
            }
            set
            {
                Set(value.ToString("00"), 25, 2);
            }
        }
        #endregion
        #region Protected Methods
        protected void Set(string value, int index, int count)
        {
            _licenseContent.Remove(index, count);
            _licenseContent.Insert(index, value);
        }
        protected string Get(int index, int count)
        {
            string valor = License.Substring(index, count);
            return valor;
        }
        #endregion
    }

    [DataContract]
	[Serializable]
    public class HotelLicenseContractPms : HotelLicenseGeneralContract
    {
        #region Constructor
        public HotelLicenseContractPms()
            : base()
        {
            License += new string('0', 19);
            Application = Applications.NewHotelPms;
            
            Users = 0;
            Rooms = 0;
            Bookings = 0;
            Companies = 0;
            Revenue = 0;
            Messages = 0;
        }
        #endregion
        #region Public Properties
        /// <summary>
        /// Users (0 => inactive) (# => quantity) (27,3)
        /// </summary>
        public int Users
        {
            get
            {
                return int.Parse(Get(27, 3));
            }
            set
            {
                Set(value.ToString("000"), 27, 3);
            }
        }
        /// <summary>
        /// Rooms (0 => inactive) (# => quantity) (30,4)
        /// </summary>
        public int Rooms
        {
            get
            {
                return int.Parse(Get(30, 4));
            }
            set
            {
                Set(value.ToString("0000"), 30, 4);
            }
        }
        /// <summary>
        /// Bookings (0 => inactive) (# => quantity) (34,5)
        /// </summary>
        public int Bookings
        {
            get
            {
                return int.Parse(Get(34, 5));
            }
            set
            {
                Set(value.ToString("00000"), 34, 5);
            }
        }
        /// <summary>
        /// Companies (0 => inactive) (# => quantity) (39,4)
        /// </summary>
        public int Companies
        {
            get
            {
                return int.Parse(Get(39, 4));
            }
            set
            {
                Set(value.ToString("0000"), 39, 4);
            }
        }
        /// <summary>
        /// Revenue Features
        /// </summary>
        private int Revenue
        {
            get
            {
                return int.Parse(Get(43, 1));
            }
            set
            {
                Set(value.ToString(), 43, 1);
            }
        }
        /// <summary>
        /// Messages Features
        /// </summary>
        private int Messages
        {
            get
            {
                return int.Parse(Get(44, 1));
            }
            set
            {
                Set(value.ToString(), 44, 1);
            }
        }

        /// <summary>
        /// Determinate if User Restrictions are applied
        /// </summary>
        public bool UserRestrictions { get { return Users != 0; } }
        /// <summary>
        /// Determinate if Room Restrictions are applied
        /// </summary>
        public bool RoomRestrictions { get { return Rooms != 0; } }
        /// <summary>
        /// Determinate if Bookings Restrictions are applied
        /// </summary>
        public bool BookingRestrictions { get { return Bookings != 0; } }
        /// <summary>
        /// Determinate if Company Restrictions are applied
        /// </summary>
        public bool CompanyRestrictions { get { return Companies != 0; } }
        /// <summary>
        /// Determinate if Revenue Features are available or not
        /// </summary>
        public bool RevenueRestriction
        {
            get { return Revenue != 0; }
            set { Revenue = (value) ? 1 : 0; }
        }
        /// <summary>
        /// Determinate if Messages Features are available or not
        /// </summary>
        public bool MessagesRestriction
        {
            get { return Messages != 0; }
            set { Messages = (value) ? 1 : 0; }
        } 
        #endregion
    }
    [DataContract]
	[Serializable]
    public class HotelLicenseContractAccounting : HotelLicenseGeneralContract
    {
        public HotelLicenseContractAccounting()
        {
            Application = Applications.NewHotelAcc;
        }
    }
    
    [DataContract]
	[Serializable]
    public class HotelLicenseContractPos : HotelLicenseGeneralContract
    {
        public HotelLicenseContractPos()
        {
            Application = Applications.NewHotelPos;
        }
    }

    [DataContract]
	[Serializable]
    public class HotelLicenseContractSpa : HotelLicenseGeneralContract 
    {
        public HotelLicenseContractSpa()
        {
            Application = Applications.NewHotelSpa;
        }
    }

    [DataContract]
	[Serializable]
    public class HotelLicenseContractGolf : HotelLicenseGeneralContract
    {
        public HotelLicenseContractGolf()
        {
            Application = Applications.NewHotelGolf;
        }
    }

    [DataContract]
	[Serializable]
    public class HotelLicenseContractEvents : HotelLicenseGeneralContract
    {
        public HotelLicenseContractEvents()
        {
            Application = Applications.NewHotelEvents;
        }
    }

    [DataContract]
	[Serializable]
    public class HotelLicenseContractStock : HotelLicenseGeneralContract
    {
        public HotelLicenseContractStock()
        {
            Application = Applications.NewHotelStock;
        }
    }

    [DataContract]
	[Serializable]
    public class HotelLicenseContractCondo : HotelLicenseGeneralContract
    {
        public HotelLicenseContractCondo()
        {
            Application = Applications.NewHotelCondo;
        }
    }

    [DataContract]
	[Serializable]
    public class HotelLicenseContractSurvey : HotelLicenseGeneralContract
    {
        public HotelLicenseContractSurvey()
        {
            Application = Applications.NewHotelSurvey;
        }
    }

    [DataContract]
	[Serializable]
    public class HotelLicenseContractNewGes : HotelLicenseGeneralContract
    {
        public HotelLicenseContractNewGes()
        {
            Application = Applications.NewHotelGes;
        }
    }

}
