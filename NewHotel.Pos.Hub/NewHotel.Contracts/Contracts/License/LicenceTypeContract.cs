﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public class LicenceTypeContract : BaseContract
    {
        private long _license;
        private bool _active;
        private DateTime _limitDate;

        #region Constructor

        public LicenceTypeContract()
            : base()
        {
        }

        #endregion

        [DataMember]
        public long Licence { get { return _license; } set { Set(ref _license, value, "Licence"); } }
        [DataMember]
        public bool Active { get { return _active; } set { Set(ref _active, value, "Active"); } }
        [DataMember]
        public DateTime LimitDate { get { return _limitDate; } set { Set(ref _limitDate, value, "LimitDate"); } }
    }
}