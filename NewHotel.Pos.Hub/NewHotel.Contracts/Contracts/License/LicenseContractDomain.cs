﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Contracts
{
    public enum LicenseProductType : long
    {
        Sale = 0,
        Trial = 1
    };

    public enum LicenseProductLevel : long
    {
        Full = 0,
        Basic = 1,
        Limited = 2
    };

    
}
