﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class LicenseSearchContract : BaseContract
    {
        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Hotel { get; set; }

        [DataMember]
        public DateTime? DateLimit { get; set; }
        
        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string LicenseXML { get; set; }

        [DataMember]
        public string Distributor { get; set; }

        [DataMember]
        public string HotelGroup { get; set; }

        [DataMember]
        public bool Inactive { get; set; }

    }
}
