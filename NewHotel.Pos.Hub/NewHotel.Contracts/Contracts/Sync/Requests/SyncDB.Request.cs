﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SyncDBRequest
    {
        public SyncDBTableRequest[] TableRequest { get; set; } 
    }
}
