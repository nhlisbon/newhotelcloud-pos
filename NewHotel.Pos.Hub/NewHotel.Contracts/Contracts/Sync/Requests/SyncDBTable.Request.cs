﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SyncDBTableRequest
    {
        public string TableName { get; set; }
        public DateTime TableLastModified { get; set; }
        public long TablePage { get; set; }
        public long TablePageSize { get; set; }
    }
}
