﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SyncDBResponse
    {
        public SyncDBMergeResponse[] MergeResponse { get; set; }
        public SyncDBDeleteResponse[] DeleteResponse { get; set; } 
    }
}
