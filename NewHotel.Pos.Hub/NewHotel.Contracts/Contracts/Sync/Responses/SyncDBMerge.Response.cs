﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SyncDBMergeResponse
    {
        public string TableName { get; set; }
        public string[] TableFieldNames { get; set; }
        public int TablePkFieldIndex { get; set; }
        public object[][] TableRecords { get; set; }
    }
}
