﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SyncDBDeleteResponse
    {
        public string TableName { get; set; }
        public string TablePkFieldName { get; set; }
        public object[] TablePks { get; set; }
    }
}
