﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ParameterContract : BaseContract<short>
    {
        #region Members
        private string _host;
        private string _username;
        private string _password;
        private string _fromAddress;
        private string _toAddress;
        private string _version;
        private string _webServExternalAddress;
        private string _webServCRMAddress;

        #endregion
        #region Properties
        [DataMember]
        public string Host { get { return _host; } set { Set(ref _host, value, "Host"); } }
        [DataMember]
        public string Username { get { return _username; } set { Set(ref _username, value, "Username"); } }
        [DataMember]
        public string Password { get { return _password; } set { Set(ref _password, value, "Password"); } }
        [DataMember]
        public string FromAddress { get { return _fromAddress; } set { Set(ref _fromAddress, value, "FromAddress"); } }
        [DataMember]
        public string ToAddress { get { return _toAddress; } set { Set(ref _toAddress, value, "ToAddress"); } }
        [DataMember]
        public string Version { get { return _version; } set { Set(ref _version, value, "Version"); } }
        [DataMember]
        public string WebServExternalAddress { get { return _webServExternalAddress; } set { Set(ref _webServExternalAddress, value, "WebServExternalAddress"); } }
        [DataMember]
        public string WebServCRMAddress { get { return _webServCRMAddress; } set { Set(ref _webServCRMAddress, value, "WebServCRMAddress"); } }
       
        #endregion
    }
}
