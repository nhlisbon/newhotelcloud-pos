﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TranslationInfoContract : BaseContract, IReflectionDictionaryKey
    {
        #region Members

        [DataMember]
        internal readonly long _languageId;
        [DataMember]
        internal readonly string _languageName;

        private string _translation;

        #endregion
        #region Properties

        [DataMember]
        public string Translation
        {
            get { return _translation; }
            set { Set(ref _translation, value, "Translation"); }
        }
        
        [ReflectionExclude]
        public long LanguageId { get { return _languageId; } }
        
        [ReflectionExclude]
        public string LanguageName { get { return _languageName; } }

        #endregion
        #region IReflectionDictionaryKey Members

        string IReflectionDictionaryKey.Key
        {
            get { return _languageId.ToString(); }
        }

        #endregion
        #region Public Methods

        public TranslationInfoContract(long languageId, string languageName, string translation)
        {
            _languageId = languageId;
            _languageName = languageName;
            Translation = translation;
        }

        public TranslationInfoContract(TranslationInfoContract contract)
            : this(contract.LanguageId, contract.LanguageName, contract.Translation) 
        {
        }

        public TranslationInfoContract() { }

        public override string ToString()
        {
            return Translation;
        }

        #endregion
    }

    /// <summary>
    /// Language Translation Contract
    /// </summary>
    [DataContract]
    [Serializable]
    [KnownType(typeof(TranslationInfoContract))]
    public partial class LanguageTranslationContract : BaseContract, IEnumerable<TranslationInfoContract>
    {
        #region Members

        [DataMember]
        internal bool _nullable;

        private List<TranslationInfoContract> _translations;
        private long _language;

        #endregion
        #region Properties

        /// <summary>
        /// Nullable?
        /// </summary>
        public bool Nullable { get { return _nullable; } }

        /// <summary>
        /// Language
        /// </summary>
        [DataMember(Order = 0)]
        public long Language
        { 
            get { return _language; }
            set
            {
                if (Set(ref _language, value, "Language"))
                    NotifyPropertyChanged("Current");
            }
        }

        /// <summary>
        /// Translations
        /// </summary>
        [ReflectionExclude(Hide = true)]
        [DataMember(Order = 1)]
        public IEnumerable<TranslationInfoContract> Translations
        {
            get { return _translations; }
            internal set
            {
                DetachPropertyChange(_translations);
                _translations = new List<TranslationInfoContract>(value);
                AttachPropertyChange(_translations);
            }
        }

        [ReflectionExclude(Hide = true)]
        public TranslationInfoContract Current
        {
            get { return Find(Language); }
        }

        [ReflectionExclude(Hide = true)]
        public IEnumerable<TranslationInfoContract> AllExceptCurrent
        {
            get { return this.Where(x => !x.LanguageId.Equals(Language)); }
        }

        /// <summary>
        /// Direct Access to Text (Ex: Property[Language])
        /// </summary>
        [ReflectionExclude(Hide = true)]
        public string this[long language]
        {
            get
            {
                TranslationInfoContract info = Find(language);

                if (info == null)
                    throw new ArgumentOutOfRangeException(string.Format("language ({0})", language));

                return info.Translation;
            }
            set
            {
                var info = Find(language);
                if (info == null)
                    throw new ArgumentOutOfRangeException(string.Format("language ({0})", language));

                if (info.Translation != value)
                {
                    int index = _translations.IndexOf(info);
                    _translations[index] = new TranslationInfoContract(language,
                        info.LanguageName, value ?? string.Empty);
                }
            }
        }

        /// <summary>
        /// Is Empty?
        /// </summary>
        [ReflectionExclude(Hide = true)]
        public bool IsEmpty
        {
            get
            {
                if (_translations != null)
                    return !_translations.Any(ti => !string.IsNullOrEmpty(ti.Translation));

                return true;
            }
        }

        /// <summary>
        /// Nº of Translations
        /// </summary>
        [ReflectionExclude(Hide = true)]
        public int Count
        {
            get
            {
                if (_translations != null)
                    return _translations.Count;

                return 0;
            }
        }

        #endregion
        #region Constructors

        public LanguageTranslationContract(bool nullable = false)
            : base()
        {
            _nullable = nullable;
            _language = 1033;
            _translations = new List<TranslationInfoContract>();
        }

        public LanguageTranslationContract(bool nullable, object id, IEnumerable<TranslationInfoContract> translations)
            : this(nullable)
        {
            Id = id;
            AddRange(translations);
        }

        public LanguageTranslationContract(object id, IEnumerable<TranslationInfoContract> translations)
            : this(false, id, translations) { }

        #endregion
        #region Public Methods

        public TranslationInfoContract Find(long language)
        {
            if (_translations != null)
                return _translations.FirstOrDefault(ti => ti.LanguageId.Equals(language));

            return null;
        }

        public static implicit operator string(LanguageTranslationContract contract)
        {
            var translation = contract[contract.Language];

            if (string.IsNullOrEmpty(translation))
            {
                var info = contract.FirstOrDefault(ti => !string.IsNullOrEmpty(ti.Translation));
                translation = info != null ? info.Translation : string.Empty;
            }

            return translation;
        }

        public void Clear()
        {
            DetachPropertyChange(_translations);
            if (_translations != null)
                _translations.Clear();
            NotifyPropertyChanged("Current");
        }

        public void AddRange(IEnumerable<TranslationInfoContract> translations)
        {
            if (_translations != null)
                _translations.AddRange(translations);
            AttachPropertyChange(_translations);
            NotifyPropertyChanged("Current");
        }

        public void Append(IEnumerable<TranslationInfoContract> translations)
        {
            AddRange(translations.Select(ti => new TranslationInfoContract(ti)));
        }

        public void Assign(IEnumerable<TranslationInfoContract> translations)
        {
            if (_translations != null)
            {
                foreach (var translation in _translations)
                {
                    var current = translations.FirstOrDefault(ti => ti.LanguageId == translation.LanguageId);
                    translation.Translation = current != null ? current.Translation : string.Empty;
                }
            }
        }

        #endregion
        #region Private Methods

        private void OnTranslationChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Translation")
                NotifyPropertyChanged("Current");
        }

        private void AttachPropertyChange(IEnumerable<TranslationInfoContract> translations)
        {
            if (translations != null)
            {
                foreach (var translation in translations)
                    translation.PropertyChanged += OnTranslationChanged;
            }
        }

        private void DetachPropertyChange(IEnumerable<TranslationInfoContract> translations)
        {
            if (translations != null)
            {
                foreach (var translation in translations)
                    translation.PropertyChanged -= OnTranslationChanged;
            }
        }

        #endregion
        #region IEnumerable<TranslationInfoContract> Members

        public IEnumerator<TranslationInfoContract> GetEnumerator()
        {
            return _translations.GetEnumerator();
        }

        #endregion
        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _translations.GetEnumerator();
        }

        #endregion
    }
}
