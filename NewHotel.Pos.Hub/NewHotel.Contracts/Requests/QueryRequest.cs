﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.Core;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    /// <summary>
    /// Class for make request to queries, containing basic filtering and sorting information.
    /// </summary>
    /// <remarks>
    /// You can use this class to set up filters, parameters and sorts from client side and pass this to a Query/>
    /// </remarks>
    [DataContract]
	[Serializable]
    public class Request : BaseObject
    {
        #region Members

        private List<QueryArgument> _parameters;
        private List<FilterArgument> _filters;
        private List<SortArgument> _sorts;

        #endregion
        #region Properties

        /// <summary>
        /// List of possible parameters
        /// </summary>
        [DataMember(Order = 1)]
        public List<QueryArgument> Parameters
        {
            get
            {
                if (_parameters == null)
                    _parameters = new List<QueryArgument>();

                return _parameters;
            }
            set
            {
                _parameters = value;
                NotifyPropertyChanged("Parameters");
            }
        }

        /// <summary>
        /// List of possible filters
        /// </summary>
        [DataMember(Order = 2)]
        public List<FilterArgument> Filters
        {
            get
            {
                if (_filters == null)
                    _filters = new List<FilterArgument>();

                return _filters;
            }
            set
            {
                _filters = value;
                NotifyPropertyChanged("Filters");
            }
        }

        /// <summary>
        /// List of possible sorts
        /// </summary>
        [DataMember(Order = 3)]
        public List<SortArgument> Sorts
        {
            get
            {
                if (_sorts == null)
                    _sorts = new List<SortArgument>();

                return _sorts;
            }
            set
            {
                _sorts = value;
                NotifyPropertyChanged("Sorts");
            }
        }

        #endregion
        #region Public Methods

        /// <summary>
        /// Copy all information from other request
        /// </summary>
        public void CopyFrom(Request request)
        {
            _parameters = request.Parameters;
            _filters = request.Filters;
            _sorts = request.Sorts;
        }

        /// <summary>
        /// Get a particular parameter from its name
        /// </summary>
        public QueryArgument GetParameter(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException("name");

            return Parameters.FirstOrDefault(o => o.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// Get a particular filter from its name
        /// </summary>
        public FilterArgument GetFilter(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException("name");

            return Filters.FirstOrDefault(o => o.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// Looks for a filter by its name and indicates if filter exists
        /// </summary>
        public bool HasFilter(string name)
        {
            var filter = GetFilter(name);
            return filter != null;
        }

        /// <summary>
        /// Looks for a parameter by its name and indicates if parameter exists
        /// </summary>
        public bool HasParameter(string name)
        {
            var parameter = GetParameter(name);
            return parameter != null;
        }

        /// <summary>
        /// Looks for a parameter by its name and indicates if parameter has a value
        /// </summary>
        public bool HasValue(string name)
        {
            var parameter = GetParameter(name);
            return parameter != null && parameter.Value != null &&
                !parameter.Value.ToString().Equals(string.Empty);
        }

        /// <summary>
        /// Set all values set to filters to null
        /// </summary>
        public void ResetFilters()
        {
            if (_filters != null)
            {
                foreach (var filter in _filters)
                    filter.Value = null;
            }
        }

        /// <summary>
        /// Set all values set to parameters to null
        /// </summary>
        public void ResetParameters()
        {
            if (_parameters != null)
            {
                foreach (var parameter in _parameters)
                    parameter.Value = null;
            }
        }

        /// <summary>
        /// Add or update filter
        /// </summary>
        public FilterArgument AddFilter(string name, object value, string description, FilterTypes? type)
        {
            var fa = GetFilter(name);
            if (fa == null)
            {
                fa = new FilterArgument() { Name = name };
                Filters.Add(fa);
            }

            fa.Value = value;
            fa.Description = description;
            fa.Type = type;

            return fa;
        }

        /// <summary>
        /// Add or update filter
        /// </summary>
        public FilterArgument AddFilter(string name, object value = null, FilterTypes? type = null)
        {
            return AddFilter(name, value, string.Empty, type);
        }

        /// <summary>
        /// Add or Update parameter
        /// </summary>
        public QueryArgument AddParameter(string name, object value, string description)
        {
            var qa = GetParameter(name);
            if (qa == null)
            {
                qa = new QueryArgument() { Name = name };
                Parameters.Add(qa);
            }

            qa.Value = value;
            qa.Description = description;

            return qa;
        }

        /// <summary>
        /// Add or Update parameter
        /// </summary>
        public QueryArgument AddParameter(string name, object value = null)
        {
            return AddParameter(name, value, string.Empty);
        }

        /// <summary>
        /// Adds a sort column
        /// </summary>
        public SortArgument AddSort(string name, SortDirection direction)
        {
            var sa = new SortArgument(name, direction);
            Sorts.Add(sa);
            return sa;
        }

        /// <summary>
        /// Adds a sort column
        /// </summary>
        public SortArgument AddSort(string name)
        {
            return AddSort(name, SortDirection.Asc);
        }

        /// <summary>
        /// Remove all sorts
        /// </summary>
        public void ClearSorts()
        {
            Sorts.Clear();
        }

        public override string ToString()
        {
			var st = string.Empty;

			if (_parameters != null)
			{
				var p = string.Join(",", _parameters.Select(x => x.ToString()).Where(x => x != string.Empty));
				if (p != string.Empty)
					st += string.Format(" Parameters [{0}]", p);
			}

			if (_filters != null)
			{
				var f = string.Join(",", _filters.Select(x => x.ToString()).Where(x => x != string.Empty));
				if (f != string.Empty)
					st += string.Format(" Filters [{0}]", f);
			}

			if (_sorts != null)
			{
				var s = string.Join(",", _sorts.Select(x => x.ToString()).Where(x => x != string.Empty));
				if (s != string.Empty)
					st += string.Format(" Sorts [{0}]", s);
			}

			return st;
        }

        #endregion
    }

    /// <summary>
    /// Derived class from <see cref="NewHotel.Contracts.Request"/>
    /// </summary>
    [DataContract]
	[Serializable]
    public class QueryRequest : Request
    {
        #region Members

        private int _page = 1;
        private short _pageSize;

        #endregion
        #region Properties
        /// <summary>
        /// Number of the page
        /// </summary>
        [DataMember]
        public int Page 
        { 
            get { return _page; }
            set 
            {
                if (_page != value)
                {
                    _page = value;
                    NotifyPropertyChanged("Page");
                }
            } 
        }

        /// <summary>
        /// Size of items per page
        /// </summary>
        [DataMember]
        public short PageSize 
        { 
            get { return _pageSize; } 
            set 
            {
                if (_pageSize != value)
                {
                    _pageSize = value;
                    NotifyPropertyChanged("PageSize");
                }
            } 
        }

        #endregion
        #region Contructor

        public QueryRequest()
        {
        }

        #endregion
        #region Public Methods

        public override string ToString()
        {
            if (_pageSize > 0 && _page > 0)
                return base.ToString() + " Paging {" + string.Format("Count={0}, Size={1}", _page.ToString(), _pageSize.ToString() + "}");

            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            var qr = obj as QueryRequest;
            if (qr != null)
                return _page == qr._page && _pageSize == qr._pageSize && base.Equals(obj);

            return false;
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    [KnownType(typeof(DBNull))]
    [KnownType(typeof(ArgumentValue))]
    [KnownType(typeof(Argument))]
    [KnownType(typeof(object[]))]
    [KnownType(typeof(Guid[]))]
    public class QueryArgument : BaseObject
    {
        #region Members

        private string _name;

        [DataMember(Order = 2, EmitDefaultValue = false)]
        internal IList<object> _values;
        [DataMember(Order = 3, EmitDefaultValue = false)]
        internal IList<string> _description;

        #endregion
        #region Properties

        [DataMember(Order = 1)]
        public string Name 
        { 
            get { return _name; } 
            set { Set(ref _name, value, "Name"); } 
        }

        public object Value 
        { 
            get 
            {
                if (_values != null && _values.Count == 1)
                    return _values[0];

                return _values; 
            } 
            set 
            {
                IList<object> values = null;
                if (value != null)
                {
                    values = value as IList<object>;
                    if (values == null)
                        values = new List<object>() { value };

                    for (int i = 0; i < values.Count; i++)
                    {
                        if (values[i] != null && values[i] is Enum)
                        {
                            var enumValue = values[i];
                            var enumType = Enum.GetUnderlyingType(enumValue.GetType());
                            if (enumType == typeof(long))
                                values[i] = (long)enumValue;
                        }
                    }
                }

                Set(ref _values, values, "Value");
            } 
        }

        public object Description
        {
            get
            {
                if (_description != null && _description.Count == 1)
                    return _description[0];

                return _description;
            }
            set
            {
                var description = value as IList<string>;
                if (description == null && value != null)
                    description = new List<string>() { value.ToString() };

                Set(ref _description, description, "Description");
            }
        }

        public Type ValueType { get; set; }

        public object ArgumentValues
        {
            get
            {
                if (_values != null)
                {
                    var args = new List<IArgument>();
                    for (int i = 0; i < _values.Count; i++)
                    {
                        string description = null;
                        if (_description != null && i < _description.Count)
                            description = _description[i];

                        var value = _values[i];
                        var arg = value as IArgument;
                        if (arg != null)
                            args.Add(arg);
                        else if (value != null)
                            args.Add(new ArgumentValue(value, description));
                    }

                    if (args.Count > 0)
                        return args.ToArray();
                }

                return null;
            }
        }

        public bool HasValue
        {
            get { return Value != null; }
        }

        public virtual bool IsAvailable
        {
            get 
            {
                if (_values == null || _values.Count == 0)
                    return false;
                else if (_values.Count == 1)
                {
                    var value = _values[0] as string;
                    if (string.Empty.Equals(value))
                        return false;
                }

                return true;
            }
        }

        #endregion
        #region Public Methods

        public T GetArgumentValue<T>()
        {
            if (_values != null)
            {
                if (_values.Count == 1)
                    return (T)_values[0];
                else
                    throw new InvalidCastException("Query argument invalid cast");
            }

            return default(T);
        }

        public T[] GetArgumentValues<T>()
        {
            if (_values != null)
            {
                if (_values.Count > 1)
                    return _values.Cast<T>().ToArray();
                else
                    throw new InvalidCastException("Query argument invalid cast");
            }

            return null;
        }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(_name) && _values != null)
                return _name + "=" + string.Join(",", _values.Select(x => (x ?? "NULL").ToString()));

            return string.Empty;
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class FilterArgument : QueryArgument
    {
        #region Members

        private bool _enabled = true;
        private FilterTypes? _type;

        #endregion
        #region Properties

        [DataMember(Order = 4, EmitDefaultValue = false)]
        public bool Enabled 
        { 
            get { return _enabled; } 
            set { Set(ref _enabled, value, "Enabled"); } 
        }

        [DataMember(Order = 5, EmitDefaultValue = false)]
        public FilterTypes? Type 
        { 
            get { return _type; } 
            set { Set(ref _type, value, "Type"); } 
        }

        public override bool IsAvailable
        {
            get { return Enabled; }
        }

        #endregion
        #region Methods

        public FilterArgument Enable()
        {
            Enabled = true;
            return this;
        }

        public FilterArgument Disable()
        {
            Enabled = false;
            return this;
        }

        #endregion
    }

    public enum SortDirection { Asc = 1, AscNullFirst = 2, Desc = -1, DescNullFirst = -2 };

    [DataContract]
	[Serializable]
    public class SortArgument : BaseObject
    {
        #region Members

        private string _name;
        private SortDirection _direction = SortDirection.Asc;

        #endregion
        #region Properties

        [DataMember(Order = 1)]
        public string Name 
        { 
            get { return _name; } 
            set { Set(ref _name, value, "Name"); } 
        }

        [DataMember(Order = 2)]
        public SortDirection Direction
        {
            get { return _direction; }
            set { Set(ref _direction, value, "Direction"); }
        }

        #endregion
        #region Public Methods

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(_name))
                return _name + "=" + _direction.ToString();

            return string.Empty;
        }

        #endregion
        #region Constructor

        public SortArgument(string name, SortDirection direction)
        {
            _name = name;
            _direction = direction;
        }

        #endregion
        #region Static Methods

        public static SortArgument Asc(string name)
        {
            return new SortArgument(name, SortDirection.Asc);
        }

        public static SortArgument AscNullFirst(string name)
        {
            return new SortArgument(name, SortDirection.AscNullFirst);
        }

        public static SortArgument Desc(string name)
        {
            return new SortArgument(name, SortDirection.Desc);
        }

        public static SortArgument DescNullFirst(string name)
        {
            return new SortArgument(name, SortDirection.DescNullFirst);
        }

        #endregion
    }
}