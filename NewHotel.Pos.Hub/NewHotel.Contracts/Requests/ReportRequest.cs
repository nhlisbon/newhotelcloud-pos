﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using NewHotel.Core;

namespace NewHotel.Contracts
{
    [DataContract]
    public class ReportRequest : BaseObject
    {
        #region Members
        private string _id;
        private string _name;
        #endregion

        #region Properties
        [DataMember]
        public string Id { get { return _id; } set { Set(ref _id, value, "Id"); } }
        [DataMember]
        public string Name { get { return _name; } set { Set(ref _name, value, "Name"); } }
        #endregion

        public ReportRequest()
        {
        }

        public override string ToString()
        {
            return string.Format("?ID={0}&Name={1}", Id, Name);
        }
    }
}
