﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class ValidationNamedResult
    {
        public readonly string Name;
        public readonly ValidationResult Result;

        public ValidationNamedResult(string name, ValidationResult result)
        {
            Name = name;
            Result = result;
        }
    }

    public class ValidationGroupedResult
    {

    }
}
