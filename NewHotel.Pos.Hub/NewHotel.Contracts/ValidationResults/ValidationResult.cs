﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;
using System.Threading.Tasks;
using System.ComponentModel;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public abstract class Validation : IEquatable<Validation>
    {
        #region Mermbers

        [DataMember(Name = "Code", Order = 1)]
        internal int _code;
        [DataMember(Name = "Message", Order = 2)]
        internal string _message;
        [DataMember(Name = "TraceMessage", Order = 3)]
        internal string _traceMessage;

        #endregion
        #region Public Properties

        public int Code
        {
            get { return _code; }
        }

        public virtual string Message
        {
            get { return _message ?? string.Empty; }
        }

        public virtual string TraceMessage
        {
            get { return _traceMessage ?? string.Empty; }
        }

        #endregion
        #region Constructor

        public Validation(int code, string message, string traceMessage = null)
        {
            _code = code;
            _message = message;
            _traceMessage = traceMessage;
        }

        public Validation(string message)
        {
            _message = message;
        }

        #endregion
        #region Public Methods

        public override int GetHashCode()
        {
            return _code.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var val = obj as Validation;
            return Equals(val);
        }

        public override string ToString()
        {
            return string.Format("({0}.{1}) {2}", _code.GetLowWord().ToString("D3"), _code.GetHighWord(), Message);
        }

        #endregion
        #region IEquatable<Validation> Members

        public bool Equals(Validation other)
        {
            if (other != null)
                return other.Code == _code;

            return false;
        }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class ValidationError : Validation
    {
        #region Constructor

        public ValidationError(string errorMessage)
            : base(errorMessage) { }

        public ValidationError(int code, string errorMessage, string traceMessage = null)
            : base(code, errorMessage, traceMessage) { }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class ValidationWarning : Validation
    {
        #region Constructor

        public ValidationWarning(string warningMessage)
            : base(warningMessage) { }

        public ValidationWarning(int code, string warningMessage, string traceMessage = null)
            : base(code, warningMessage, traceMessage) { }

        #endregion
    }

    /// <summary>
    /// General Structure for Business Response
    /// </summary>
    [DataContract]
    [Serializable]
    [KnownType(typeof(ValidationError))]
    [KnownType(typeof(ValidationWarning))]
    public class ValidationResult : IEnumerable<Validation>
    {
        #region Members

        private List<Validation> _validations;
        [IgnoreDataMember]
        public readonly static ValidationResult Empty = new ValidationResult();

        [DataMember(Name = "IgnoreWarnings", Order = 1)]
        internal bool _ignoreWarnings;
        [DataMember(Name = "Culture", Order = 2)]
        internal long _culture;

        #endregion
        #region Properties

        /// <summary>
        /// flag: Ignore Warnings
        /// </summary>
        public bool IgnoreWarnings
        {
            get { return _ignoreWarnings; }
        }

        /// <summary>
        /// Culture
        /// </summary>
        public long Culture
        {
            get { return _culture; }
        }

        /// <summary>
        /// flag: Is Empty
        /// </summary>
        public bool IsEmpty
        {
            get { return Validations.Count == 0; }
        }

        /// <summary>
        /// Quantity of Validations
        /// </summary>
        public int Count
        {
            get { return Validations.Count; }
        }

        /// <summary>
        /// flag: Contain any Error in the Validations
        /// </summary>
        [DataMember(Order = 3)]
        public bool HasErrors
        {
            get { return Errors.Length > 0; }
            internal set { }
        }

        /// <summary>
        /// Array of Errors
        /// </summary>
        [DataMember(Order = 4)]
        public ValidationError[] Errors
        {
            get { return Validations.OfType<ValidationError>().ToArray(); }
            internal set { Validations.AddRange(value); }
        }

        /// <summary>
        /// flag: Contain any Warning in the Validations
        /// </summary>
        [DataMember(Order = 5)]
        public bool HasWarnings
        {
            get { return Warnings.Length > 0; }
            internal set { }
        }

        /// <summary>
        /// Array of Warnings
        /// </summary>
        [DataMember(Order = 6)]
        public ValidationWarning[] Warnings
        {
            get { return Validations.OfType<ValidationWarning>().ToArray(); }
            internal set { Validations.AddRange(value); }
        }

        /// <summary>
        /// Extra ??
        /// </summary>
		[DataMember(Order = 7)]
        public string Extra { get; set; }

        /// <summary>
        /// Return All Message
        /// </summary>
        public string Message
        {
            get
            {
                if (Validations.Count > 0)
                {
                    var sb = new StringBuilder();

                    sb.Append(Validations[0].Message);
                    for (int index = 1; index < Validations.Count; index++)
                    {
                        sb.AppendLine();
                        sb.Append(Validations[index].Message);
                    }

                    return sb.ToString();
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Return All Trace Message
        /// </summary>
        public string TraceMessage
        {
            get
            {
                if (Validations.Count > 0)
                {
                    var sb = new StringBuilder();

                    sb.Append(Validations[0].TraceMessage);
                    for (int index = 1; index < Validations.Count; index++)
                    {
                        sb.AppendLine();
                        sb.Append(Validations[index].TraceMessage);
                    }

                    return sb.ToString();
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Return All Texts
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (Validations.Count > 0)
            {
                var sb = new StringBuilder();

                sb.Append(Validations[0].ToString());
                for (int index = 1; index < Validations.Count; index++)
                {
                    sb.AppendLine();
                    sb.Append(Validations[index].ToString());
                }

                return sb.ToString();
            }

            return string.Empty;
        }

        #endregion
        #region Contructors

        public ValidationResult(bool ignoreWarnings)
            : base()
        {
            _ignoreWarnings = ignoreWarnings;
        }

        public ValidationResult(bool ignoreWarnings, long culture)
            : this(ignoreWarnings)
        {
            _culture = culture;
        }

        public ValidationResult(long culture)
            : this(false, culture) { }

        public ValidationResult()
            : this(false) { }

        public ValidationResult(IEnumerable<Validation> collection, bool ignoreWarnings, long culture)
            : this(ignoreWarnings, culture)
        {
            Validations.AddRange(collection);
        }

        public ValidationResult(IEnumerable<Validation> collection, bool ignoreWarnings)
            : this(ignoreWarnings)
        {
            Validations.AddRange(collection);
        }

        public ValidationResult(IEnumerable<Validation> collection, long culture)
            : this(collection, false, culture) { }

        public ValidationResult(IEnumerable<Validation> collection)
            : this(collection, false) { }

        public ValidationResult(Exception ex)
            : this(ValidationFromException(ex)) { }

        #endregion
        #region Private Members

        private static IEnumerable<Validation> ValidationFromException(Exception ex)
        {
            var validations = new List<Validation>();
            if (ex != null)
                validations.Add(new ValidationError(ex.ToLogString()));

            return validations;
        }

        private List<Validation> Validations
        {
            get
            {
                if (_validations == null)
                    _validations = new List<Validation>();

                return _validations;
            }
        }

        private IEnumerator<Validation> Enumerate()
        {
            return Validations.GetEnumerator();
        }

        #endregion
        #region Public Members

        /// <summary>
        /// Clear List of Validations
        /// </summary>
        public void Clear()
        {
            Validations.Clear();
        }

        /// <summary>
        /// Contain any Validation with code ID
        /// </summary>
        /// <param name="code">code ID</param>
        /// <returns>bool</returns>
        public bool Contains(int code)
        {
            return Validations.Any(x => x.Code.Equals(code));
        }

        /// <summary>
        /// Contain any Error with code ID
        /// </summary>
        /// <param name="code">code ID</param>
        /// <returns>bool</returns>
        public bool ContainsError(int code)
        {
            return Errors.Any(x => x.Code.Equals(code));
        }

        /// <summary>
        /// Contain any Warning with code ID
        /// </summary>
        /// <param name="code">code ID</param>
        /// <returns>bool</returns>
        public bool ContainsWarning(int code)
        {
            return Warnings.Any(x => x.Code.Equals(code));
        }

        /// <summary>
        /// Contain only one Validation with code ID
        /// </summary>
        /// <param name="code">code ID</param>
        /// <returns>bool</returns>
        public bool ContainsOnly(int code)
        {
            var codes = Validations.Select(x => x.Code).Distinct();
            return codes.Count() == 1 && codes.First() == code;
        }

        /// <summary>
        /// Contain Validation
        /// </summary>
        /// <param name="validation">Validation</param>
        /// <returns>bool</returns>
		public bool Contains(Validation validation)
        {
            return Validations.Contains(validation);
        }

        /// <summary>
        /// Add a new Validation
        /// </summary>
        /// <param name="validation">Validation</param>
        public ValidationResult AddValidation(Validation validation)
        {
            Validations.Add(validation);
            return this;
        }

        /// <summary>
        /// Add new Validations
        /// </summary>
        /// <param name="validations">IEnumerable(Validation)</param>
        public ValidationResult AddValidations(IEnumerable<Validation> validations)
        {
            foreach (var validation in validations)
                Validations.Add(validation);
            return this;
        }

        /// <summary>
        /// Add a Validation Error
        /// </summary>
        /// <param name="error">string</param>
        public ValidationResult AddError(string error)
        {
            var val = new ValidationError(error);
            return AddValidation(val);
        }

        /// <summary>
        /// Add a Validation Warning
        /// </summary>
        /// <param name="warning">string</param>
        public ValidationResult AddWarning(string warning)
        {
            var val = new ValidationWarning(warning);
            return AddValidation(val);
        }

        /// <summary>
        /// Add a new Validation Result
        /// </summary>
        /// <param name="validationResult">ValidationResult</param>
        public ValidationResult Add(ValidationResult validationResult)
        {
            Validations.AddRange(validationResult.Validations);
            return this;
        }

        #endregion
        #region IEnumerable<string> Members

        public IEnumerator<Validation> GetEnumerator()
        {
            return Enumerate();
        }

        #endregion
        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Enumerate();
        }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class ValidationResult<TResult> : ValidationResult
    {
        #region Properties

        [DataMember]
        public TResult Result { get; set; }

        public ValidationResult()
          : base(false)
        {
        }

        #endregion
        #region Constructors

        public ValidationResult(TResult result)
            : base(false)
        {
            Result = result;
        }

        public ValidationResult(TResult result, bool ignoreWarnings)
            : base(ignoreWarnings)
        {
            Result = result;
        }

        public ValidationResult(TResult result, bool ignoreWarnings, long culture)
            : base(ignoreWarnings, culture)
        {
            Result = result;
        }

        public ValidationResult(TResult result, long culture)
            : base(false, culture)
        {
            Result = result;
        }

        public ValidationResult(TResult result, IEnumerable<Validation> collection, bool ignoreWarnings, long culture)
            : base(collection, ignoreWarnings, culture)
        {
            Result = result;
        }

        public ValidationResult(TResult result, IEnumerable<Validation> collection, bool ignoreWarnings)
            : base(collection, ignoreWarnings)
        {
            Result = result;
        }

        public ValidationResult(TResult result, IEnumerable<Validation> collection, long culture)
            : base(collection, false, culture)
        {
            Result = result;
        }

        public ValidationResult(TResult result, IEnumerable<Validation> collection)
            : base(collection, false)
        {
            Result = result;
        }

        public ValidationResult(TResult result, Exception ex)
            : base(ex)
        {
            Result = result;
        }

        #endregion
	}

	public static class ValidationResultSupport
	{
        #region Public Methods

        public static ValidationResult<T> ToValidationResult<T>(this ValidationContractResult validations)
            where T : BaseContract
		{
			var resut = new ValidationResult<T>();
            resut.Set(validations.Contract as T);
            resut.Add(validations);
            return resut;
		}

        public static bool HasValue<T>(this ValidationResult<T> validations)
        {
            return validations != null && !Equals(validations.Result, default(T));
        }

        public static ValidationResult<R> Map<T, R>(
            this ValidationResult<T> validations, Func<T, R> func)
		{
			return validations.IsEmpty
                ? new ValidationResult<R>(func(validations.Result))
                : new ValidationResult<R>().Validate(validations);
		}

		public static ValidationResult<TResult> Set<TResult>(
			this ValidationResult<TResult> validations,
			TResult value)
		{
            validations.Result = value;
			return validations;
		}
		public static ValidationResult<TResult> AddError<TResult>(
			this ValidationResult<TResult> validations,
			int code, string errorMessage, string traceMessage = default)
		{
			validations.AddValidation(new ValidationError(code, errorMessage, traceMessage));
			return validations;
		}

        /*
		public static ValidationResult<TResult> Validate<TResult, TOutput>(
			this ValidationResult<TResult> validations, Func<ValidationResult> validationFunc, out TOutput value)
			where TOutput : class
		{
			value = default;
			if (!validations.HasErrors &&
				validationFunc() is ValidationResult vresult)
			{
				if (vresult.HasErrors)
					validations.Add(vresult);
				value = vresult switch
				{
					ValidationContractResult vcreslt => vcreslt.Contract as TOutput,
					_ => null,
				};
			}
			return validations;
		}
        */


		public static T Validate<T, R>(
			this T validations, R validation)
			where T : ValidationResult
			where R : ValidationResult
		{
			if (!validations.HasErrors)
				validations.Add(validation);
			return validations;
		}

		public static T Validate<T, R>(
			this T validations, Func<R> validationFunc)
			where T : ValidationResult
			where R : ValidationResult
		{
			if (!validations.HasErrors &&
				validationFunc() is R vresult)
			{
				if (vresult.HasErrors)
					validations.Add(vresult);
			}
			return validations;
		}

		public static async Task<T> ValidateAsync<T, R>(
			this T validations, Func<Task<R>> validationFunc)
			where T : ValidationResult
			where R : ValidationResult
		{
			if (!validations.HasErrors &&
				(await validationFunc()) is R vresult)
			{
				if (vresult.HasErrors)
					validations.Add(vresult);
			}
			return validations;
		}

		public static T Validate<T>(
			this T validations,
			Func<bool> validationFunc, int code, string errorMessage, string traceMessage = default)
			where T : ValidationResult
		{
			if (!validations.HasErrors && !validationFunc())
				validations.AddValidation(new ValidationError(code, errorMessage, traceMessage));
			return validations;
		}

        /*
		public static ValidationResult<TResult> ValidateNull<TResult>(this ValidationResult<TResult> validations, int code = 999, string message = $"{nameof(TResult)} is not null", string traceMessage = default)
		{
			if (!validations.HasErrors && validations.Result is not null)
				validations.AddValidation(new ValidationError(code, message, traceMessage));
			return validations;
		}

		public static ValidationResult<TResult> ValidateNotNull<TResult>(this ValidationResult<TResult> validations, int code = 999, string message = $"{nameof(TResult)} is null", string traceMessage = default)
		{
			if (!validations.HasErrors && validations.Result is null)
				validations.AddValidation(new ValidationError(code, message, traceMessage));
			return validations;
		}
        */

		#endregion
	}
}