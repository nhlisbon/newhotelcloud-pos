﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ExportDocumentResult : SequentialExecResult
    {
        [DataMember]
        public List<byte[]> ExportDoc { get; set; }
        
        public ExportDocumentResult(IDictionary<string, object> data, object tag)
            : base(data, tag)
        {           
        }
    }

    [DataContract]
	[Serializable]
    public class ExportOfficialDocumentResult : ValidationResult
    {
        [DataMember]
        public byte[] ExportOfficialDoc { get; set; }

        public ExportOfficialDocumentResult()
            : base()
        {
        }
    }
}
