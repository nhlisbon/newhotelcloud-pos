﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    /// <summary>
    /// Structure for Business Response with BaseContract
    /// </summary>
    [DataContract]
	[Serializable]
    public class ValidationContractResult : ValidationResult, IKnownTypes
    {
        #region Members

        /// <summary>
        /// Base Contract 
        /// </summary>
        [DataMember]
        public BaseContract Contract { get; set; }

        #endregion
        #region Constructors

        public ValidationContractResult(ValidationResult result)
            : base()
        {
            Add(result);
        }

        public ValidationContractResult(bool ignoreWarning = false) 
            : base(ignoreWarning) 
        { 
        }

        public ValidationContractResult(long culture)
            : base(culture)
        {
        }

        public ValidationContractResult(BaseContract contract, bool ignoreWarning = false)
            : base()
        {
            Contract = contract;
        }

        public ValidationContractResult(IEnumerable<Validation> collection)
            : base(collection)
        {
        }

        public ValidationContractResult(Exception ex)
            : base(ex)
        {
        }

        public ValidationContractResult()
         : base()
        {
        }

        #endregion
        #region IKnownTypes Members

        public IEnumerable<Type> KnownTypes
        {
            get { return Contract == null ? new Type[0] : new Type[] { Contract.GetType() }; }
        }

        #endregion
    }
}