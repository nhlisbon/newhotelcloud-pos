﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SequentialExecResult : ValidationResult
    {
        [DataMember]
        public IDictionary<string, object> Data { get; internal set; } 
        [DataMember]
        public object Tag { get; internal set; } 

        public SequentialExecResult(IDictionary<string, object> data, object tag)
            : base()
        {
            Data = data;
            Tag = tag;
        }
    }
}
