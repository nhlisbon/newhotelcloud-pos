﻿using System;
using System.Text.RegularExpressions;

namespace NewHotel.Contracts
{
    public abstract class Constraint : Attribute
    {
        public abstract void Validate(object value, ValidationResult result);
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class Required : Constraint
    {
        public readonly string Name;

        public Required(string name)
        {
            Name = name;
        }

        public override void Validate(object value, ValidationResult result)
        {
            //if (result.AssertNotNull(value, Name))
            //{
            //    if (value.GetType().Equals(typeof(string)))
            //        result.AssertNotEmpty(value.ToString(), Name);
            //    else if (value.GetType().Equals(typeof(Guid)))
            //        result.AssertNotEmpty(new Guid(value.ToString()), Name);
            //}
        }
    }

    //[AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    //public class RegExpr : Constraint
    //{
    //    public readonly string Name;
    //    public readonly Regex RegEx;

    //    public RegExpr(string name, string pattern)
    //    {
    //        Name = name;
    //        RegEx = new Regex(pattern);
    //    }

    //    public override void Validate(object value, ValidationResult result)
    //    {
    //        result.AssertError(!RegEx.IsMatch(value.ToString()), -1, "Invalid " + Name);
    //    }
    //}

    //[AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    //public class Bounds : Constraint
    //{
    //    public readonly string Name;
    //    public readonly IComparable Min;
    //    public readonly IComparable Max;

    //    public Bounds(string name, IComparable min, IComparable max)
    //    {
    //        Name = name;
    //        Min = min;
    //        Max = max;
    //    }

    //    public override void Validate(object value, ValidationResult result)
    //    {
    //        IComparable comp = value as IComparable;
    //        if (comp != null)
    //            result.AssertRange(comp, Min, Max, Name); 
    //    }
    //}

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class PositiveValue : Constraint
    {
        public readonly string Name;

        public PositiveValue(string name)
        {
            Name = name;
        }

        public override void Validate(object value, ValidationResult result)
        {
            //IComparable comp = value as IComparable;
            //if (comp != null)
            //    result.AssertPositive(comp, Name);
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class NonNegativeValue : Constraint
    {
        public readonly string Name;

        public NonNegativeValue(string name)
        {
            Name = name;
        }

        public override void Validate(object value, ValidationResult result)
        {
            //IComparable comp = value as IComparable;
            //if (comp != null)
            //    result.AssertNonNegative(comp, Name);
        }
    }

    //[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    //public class DiscreteValues : Constraint
    //{
    //    public readonly string Name;
    //    public readonly IComparable[] Values;

    //    public DiscreteValues(string name, IComparable[] values)
    //    {
    //        Name = name;
    //        Values = values;
    //    }

    //    public override void Validate(object value, ValidationResult result)
    //    {
    //        IComparable comp = value as IComparable;
    //        if (comp != null)
    //            result.AssertInList(comp, Name, Values);
    //    }
    //}

    //[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    //public class TextLength : Constraint
    //{
    //    public readonly string Name;
    //    public readonly byte MinLength;
    //    public readonly byte MaxLength;

    //    public TextLength(string name, byte minLength, byte maxLength)
    //    {
    //        Name = name;
    //        MinLength = minLength;
    //        MaxLength = maxLength;
    //    }

    //    public override void Validate(object value, ValidationResult result)
    //    {
    //        result.AssertLength(value.ToString(), MinLength, MaxLength, Name);
    //    }
    //}

    //[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    //public class IsCreditCard : RegExpr
    //{
    //    public IsCreditCard(string name)
    //        : base(name, "")
    //    {
    //    }
    //}

    //[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    //public class IsNIB : RegExpr
    //{
    //    public IsNIB(string name)
    //        : base(name, "")
    //    {
    //    }
    //}

    //[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    //public class IsIBAN : RegExpr
    //{
    //    public IsIBAN(string name)
    //        : base(name, "")
    //    {
    //    }
    //}

    //[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    //public class IsEMail : RegExpr
    //{
    //    public IsEMail(string name)
    //        : base(name, "")
    //    {
    //    }
    //}

    //[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    //public class IsPhone : RegExpr
    //{
    //    public IsPhone(string name)
    //        : base(name, "")
    //    {
    //    }
    //}
}
