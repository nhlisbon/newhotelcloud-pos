﻿using System;

namespace NewHotel.Contracts
{
    public class ReadonlyPropertyException : Exception
    {
        public ReadonlyPropertyException(string propertyName)
            : base(string.Format("cannot set {0} property", propertyName))
        {
        }
    }
}
