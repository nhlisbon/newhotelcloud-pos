﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public interface IBaseRecord
    {
        object Id { get; }
    }

    /// <summary>
    /// Represents the base of all records
    /// Each record is a representation of a database row for an specific query class
    /// </summary>
    [Serializable]
    [DataContract]
    public class BaseRecord<T> : BaseObject, IBaseRecord
    {
        #region Members

        private T _id;

        #endregion
        #region Properties

        /// <summary>
        /// Identifier of the record
        /// Usually primary key
        /// </summary>
        [DataMember]
        public T Id { get { return _id; } set { Set(ref _id, value, "Id"); } }

        #endregion
        #region IBaseRecord

        [IgnoreDataMember]
        object IBaseRecord.Id
        {
            get { return _id; }
        }

        #endregion
    }

    [Serializable]
    [DataContract]
    public class BaseRecord : BaseRecord<object>
    {
    }
}