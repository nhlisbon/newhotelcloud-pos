﻿using System;

namespace NewHotel.Contracts
{
    #region Domain Types

    /// <summary>
    /// TIVA.TIVA_DIRE = Tipo impuesto (soportado, repercutido)
    /// </summary>
    public enum VatType : long { Supported = 811, Impacted = 812 };

    /// <summary>
    /// TRPT_RETI.REPO_TIPO = Tipo Reporte (Diário, Resumen, Período, Único Día, Gráfico)
    /// </summary>
    public enum EReportType : long { Daily = 2551, Summary = 2552, Period = 2553, SingleDay = 2554, Graphic = 2555 };  

    #endregion
}
