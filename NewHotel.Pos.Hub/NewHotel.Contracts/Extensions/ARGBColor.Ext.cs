﻿using System;
using System.Drawing;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public static class ARGBColorExt
    {
        public static Color ToColor(this ARGBColor color)
        {
            return Color.FromArgb(color.A, color.R, color.G, color.B);
        }

        public static ARGBColor ToARGBColor(this Color color)
        {
            return new ARGBColor(color.A, color.R, color.G, color.B);
        }

        public static ARGBColor ToARGBColor(this int value)
        {
            Color color = Color.FromArgb(value);
            return ToARGBColor(color);
        }
    }
}
