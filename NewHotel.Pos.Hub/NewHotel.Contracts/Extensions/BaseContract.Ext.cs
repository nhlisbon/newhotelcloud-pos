﻿using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Diagnostics;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public static class BaseContractExt
    {
        #region Private classes

        private class ReflectionEntry : IReflectionEntry
        {
            private readonly IAccessor Accessor;
            private readonly object Owner;

            public ReflectionEntry(IAccessor accessor, object owner)
            {
                Accessor = accessor;
                Owner = owner;
            }

            public object Value
            {
                get { return Accessor.Get(Owner); }
                set { Accessor.Set(Owner, value); }
            }

            public bool CanRead
            {
                get { return Accessor.CanRead; }
            }

            public bool CanWrite
            {
                get { return Accessor.CanWrite; }
            }

            public override string ToString()
            {
                return Accessor.ToString();
            }
        }

        private class ReflectionDictionary : IReflectionDictionary
        {
            [DebuggerBrowsableAttribute(DebuggerBrowsableState.Never)]
            private readonly IDictionary<string, IReflectionEntry> entries =
                new Dictionary<string, IReflectionEntry>();

            public void Add(string key, IAccessor accessor, object owner)
            {
                if (!Contains(key))
                    entries.Add(key, new ReflectionEntry(accessor, owner));
            }

            public bool Contains(string key)
            {
                return entries.ContainsKey(key);
            }

            public bool TryGetValue(string key, out IReflectionEntry entry)
            {
                return entries.TryGetValue(key, out entry);
            }

            public object this[string key]
            {
                get
                {
                    var entry = entries[key];
                    return entry.Value;
                }
                set
                {
                    var entry = entries[key];
                    entry.Value = value;
                }
            }

            #region IEnumerable<KeyValuePair<string, IReflectionEntry>> Members

            public IEnumerator<KeyValuePair<string, IReflectionEntry>> GetEnumerator()
            {
                return entries.GetEnumerator();
            }

            #endregion
            #region IEnumerable Members

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return entries.Values.GetEnumerator();
            }

            #endregion
        }

        #endregion
        #region Property or Field accessor

        private sealed class PropertyFieldAccessor : IAccessor, IComparable
        {
            private MemberInfo _info;
            private readonly bool _isNullable;
            private readonly bool _isGenericNullableType;
            private readonly object[] _attributes;
            private readonly int _order;
            private readonly bool _nullable;
            private readonly bool _nullStringAsEmpty = true;
            private readonly bool _isTimestamp;
            private readonly bool _isLastModified;

            private readonly IConverter _converter;
            private readonly Type _emptyType = null;

            public string Name
            {
                get { return _info.Name; }
            }

            public Type Type
            {
                get { return _info.MemberType == MemberTypes.Property 
                    ? ((PropertyInfo)_info).PropertyType : ((FieldInfo)_info).FieldType; }
            }

            public Type DeclaringType
            {
                get { return _info.DeclaringType; }
            }

            public bool Nullable
            {
                get { return _nullable; }
            }

            public bool NullStringAsEmpty
            {
                get { return _nullStringAsEmpty; }
            }

            public bool IsTimestamp
            {
                get { return _isTimestamp; }
            }

            public bool IsLastModified
            {
                get { return _isLastModified; }
            }

            public IEnumerable<T> GetAttributes<T>()
            {
                return _attributes.OfType<T>();
            }

            private Type NullableType
            {
                get { return _isGenericNullableType ? Type.GetGenericArguments()[0] : null; }
            }

            public PropertyFieldAccessor(MemberInfo info)
            {
                _info = info;

                var mappingConversion = info.GetCustomAttributes(typeof(IMappingConversion), true)
                    .OfType<IMappingConversion>().FirstOrDefault();
                if (mappingConversion != null && mappingConversion.Type != null)
                    _converter = (IConverter)Activator.CreateInstance(mappingConversion.Type);

                _attributes = _info.GetCustomAttributes(true);
                var dataMember = _info.GetCustomAttributes(false).OfType<DataMemberAttribute>().FirstOrDefault();
                _order = dataMember != null && dataMember.Order >= 0 ? dataMember.Order : int.MaxValue;
                var attr = GetAttributes<INullableMapping>().FirstOrDefault();
                if (attr != null)
                    _emptyType = attr.EmptyType;

                _isGenericNullableType = Type.IsGenericType && Type.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
                _isNullable = _isGenericNullableType || Type == typeof(string) || Type.IsClass || Type.IsInterface;

                _nullable = _isNullable;
                if (_nullable || Type == typeof(DateTime))
                {
                    var mappingColumn = GetAttributes<MappingColumn>().FirstOrDefault();
                    if (mappingColumn != null)
                    {
                        _nullable = mappingColumn.Nullable;
                        _nullStringAsEmpty = mappingColumn.NullStringAsEmpty;
                        _isTimestamp = mappingColumn.IsTimestamp;
                        _isLastModified = mappingColumn.IsLastModified;
                    }
                }
            }

            public Type BaseType
            {
                get { return NullableType ?? Type; }
            }

            public Type EmptyType
            {
                get { return _emptyType; }
            }

            public string FullName
            {
                get { return string.Format("{0}.{1}", DeclaringType.FullName, Name); }
            }

            public bool CanRead
            {
                get { return _info.MemberType == MemberTypes.Property ? ((PropertyInfo)_info).CanRead : true; }
            }

            private object GetValue(MemberInfo info, object obj)
            {
                return info.MemberType == MemberTypes.Property ? ((PropertyInfo)info).GetValue(obj, null) : ((FieldInfo)_info).GetValue(obj);
            }

            public object Get(object obj)
            {
                if (!CanRead)
                    throw new InvalidOperationException(string.Format("Can´t read from {0}", FullName));

                object value = null;
                try
                {
                    value = GetValue(_info, obj);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException(string.Format("Error reading propery {0}", FullName) +
                        "\r\n" + (ex.InnerException != null ? ex.InnerException : ex).Message);
                }

                if (_converter != null)
                    value = _converter.Get(this, value);

                return value;
            }

            public bool CanWrite
            {
                get { return _info.MemberType == MemberTypes.Property ? ((PropertyInfo)_info).CanWrite : true; }
            }

            private void SetValue(MemberInfo info, object obj, object value)
            {
                if (info.MemberType == MemberTypes.Property)
                    ((PropertyInfo)info).SetValue(obj, value, null);
                else
                    ((FieldInfo)_info).SetValue(obj, value);
            }

            public void Set(object obj, object value)
            {
                if (!CanWrite)
                    throw new InvalidOperationException(string.Format("Can´t write to {0}", FullName));

                if (_converter != null)
                    value = _converter.Set(this, value);

                if (value == null)
                {
                    if (!_isNullable)
                        throw new InvalidOperationException(string.Format("Invalid cast from {0} to {1} in {2}", "null", Type.Name, FullName));
                }
                else
                {
                    var valueType = value.GetType();
                    if (!Type.IsAssignableFrom(valueType))
                        throw new InvalidOperationException(string.Format("Invalid cast from {0} to {1} in {2}", value.GetType().Name, Type.Name, FullName));

                    if (_isNullable)
                    {
                        if ((valueType.Equals(typeof(DateTime)) && value.Equals(DateTime.MinValue)) ||
                            (valueType.Equals(typeof(Guid)) && value.Equals(Guid.Empty)) ||
                            (valueType.IsEnum && value.Equals(0)))
                            value = null;
                    }
                }

                try
                {
                    SetValue(_info, obj, value);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException(string.Format("Error writing propery {0}", FullName) +
                        "\r\n" + (ex.InnerException != null ? ex.InnerException : ex).Message);
                }
            }

            public int CompareTo(object obj)
            {
                var accessor = obj as PropertyFieldAccessor;
                if (accessor != null)
                {
                    if (_order < accessor._order)
                        return -1;
                    else if (_order > accessor._order)
                        return 1;
                }

                return 0;
            }

            public override string ToString()
            {
                return string.Format("Name={0}, Nullable={1}, Read={2}, Write={3}, Order={4}", FullName, Nullable, CanRead, CanWrite, _order);
            }
        }

        #endregion

        private static bool IncludePropertyCriteria(PropertyInfo info)
        {
            return !info.IsSpecialName && info.CanRead && info.CanWrite;
        }

        private static bool IncludeFieldCriteria(FieldInfo info)
        {
            return !info.IsSpecialName;
        }

        private static bool IncludeMemberCriteria(MemberInfo info, object obj)
        {
            if (info.MemberType == MemberTypes.Property
                ? IncludePropertyCriteria((PropertyInfo)info) : IncludeFieldCriteria((FieldInfo)info))
            {
                bool containsDataMember = info.GetCustomAttributes(typeof(DataMemberAttribute), false).Any();
                return containsDataMember ||
                    (!containsDataMember &&
                    info.DeclaringType.GetCustomAttributes(typeof(DataContractAttribute), false).Any()
                    ? false
                    : !info.GetCustomAttributes(typeof(IgnoreDataMemberAttribute), false).Any());
            }

            return false;
        }

        private static IDictionary<string, IAccessor> GetAccessors(this Type type, BindingFlags flags)
        {
            return type.FindMembers(MemberTypes.Property | MemberTypes.Field, flags, IncludeMemberCriteria, null)
                .ToDictionary(mi => mi.Name, mi => (IAccessor)(new PropertyFieldAccessor(mi)));
        }

        private static void ReflectionValues<T>(this T obj, string path,
            ReflectionDictionary values, HashSet<T> instances)
            where T : BaseContract
        {
            var type = obj.GetType();
            foreach (var acc in type.GetAccessors(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                var key = (string.IsNullOrEmpty(path) ? string.Empty : path + ".") + acc.Key;
                values.Add(key, acc.Value, obj);
            }

            var items = obj as IEnumerable;
            if (items != null)
            {
                foreach (var item in items)
                {
                    if (item != null)
                    {
                        T baseObj = null;
                        if (typeof(T).IsAssignableFrom(item.GetType()))
                            baseObj = item as T;

                        var itemKey = item as IReflectionDictionaryKey;
                        if (baseObj != null && itemKey != null)
                        {
                            var key = (string.IsNullOrEmpty(path) ? string.Empty : path) + "[" + itemKey.Key + "]";
                            instances.Add(baseObj);
                            baseObj.ReflectionValues<T>(key, values, instances);
                        }
                    }
                }
            }
        }

        public static void AssignTo<T>(this T source, T target)
            where T : BaseContract
        {
            var sourceDict = new ReflectionDictionary();
            source.ReflectionValues<BaseContract>(string.Empty, sourceDict, new HashSet<BaseContract>());

            var targetDict = new ReflectionDictionary();
            target.ReflectionValues<BaseContract>(string.Empty, targetDict, new HashSet<BaseContract>());
            foreach (var item in targetDict)
            {
                IReflectionEntry entry;
                if (item.Value.CanWrite && sourceDict.TryGetValue(item.Key, out entry))
                    if (entry.CanRead)
                        item.Value.Value = entry.Value;
            }
        }

        public static string SerializeContract<T>(this T source)
           where T : BaseContract
        {
            var dcs = new DataContractSerializer(typeof(T));
            using (var ms = new MemoryStream())
            {
                try
                {
                    dcs.WriteObject(ms, source);
                    var bytes = ms.ToArray();
                    return Encoding.UTF8.GetString(bytes, 0, bytes.Length);
                }
                finally
                {
                    ms.Close();
                }
            }
        }

        public static T DeserializeContract<T>(this string serialized)
          where T : BaseContract
        {
            var dcs = new DataContractSerializer(typeof(T));
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(serialized)))
            {
                try
                {
                    var obj = (T)dcs.ReadObject(ms);
                    return obj;
                }
                finally
                {
                    ms.Close();
                }
            }
        }
    }
}
