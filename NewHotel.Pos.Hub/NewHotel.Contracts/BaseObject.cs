﻿using System;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public abstract class BaseObject : INotifyPropertyChanged, IDataErrorInfo
	{
		#region Private Class

		private sealed class ValidationHandler
		{
            [XmlIgnore]
			public readonly Dictionary<string, string> Errors;

			public ValidationHandler()
			{
				Errors = new Dictionary<string, string>();
			}

			public string this[string property]
			{
				get { return Errors[property]; }
			}

			public bool Exists(string property)
			{
				return Errors.ContainsKey(property);
			}

			public bool ValidateRule(string property, string message, Func<bool> ruleCheck)
			{
				if (!ruleCheck())
				{
					Add(property, message);
					return false;
				}
				else
				{
					Remove(property);
					return true;
				}
			}

			public void Add(string property, string message)
			{
				if (!Exists(property))
					Errors.Add(property, message);
			}

			public void Remove(string property)
			{
				if (Exists(property))
					Errors.Remove(property);
			}

			public void Clear()
			{
				Errors.Clear();
			}
		}

		#endregion
		#region Members

        [NonSerialized]
		private ValidationHandler _validationHandler;

        #endregion
        #region Properties

        [GetValueExcludeProperty]
        [XmlIgnore]
        public Dictionary<string, string> Errors 
        { 
           get
           {
               if (_validationHandler != null)
				   return BrokenRules.Errors;
               
               return null;
            }

        }

        [ReflectionExclude(Hide = true)]
        private ValidationHandler BrokenRules
        {
            get
            {
                if (_validationHandler == null)
                    _validationHandler = new ValidationHandler();

                return _validationHandler;
            }
        }

        #endregion
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
        #region Protected Methods

        protected virtual void OnNotifyPropertyChanged(HashSet<string> propertyNames)
        {
            if (PropertyChanged != null)
            {
                foreach (var propertyName in propertyNames)
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void NotifyPropertyChanged(params string[] propertyNames)
        {
            OnNotifyPropertyChanged(new HashSet<string>(propertyNames));
        }

        protected bool Set<T>(ref T oldValue, T newValue, string propertyName)
        {
            ValidateProperty(newValue, propertyName);
            if (oldValue == null && newValue == null)
                return false;

            if ((oldValue == null && newValue != null) || !oldValue.Equals(newValue))
            {
                oldValue = newValue;
                NotifyPropertyChanged(propertyName);
                return true;
            }

            return false;
        }

        protected bool SetMin<T>(ref T? oldMinValue, T? newMinValue, string minPropertyName, ref T? maxValue, string maxPropertyName)
            where T : struct, IComparable<T>
        {
            if (Set(ref oldMinValue, newMinValue, minPropertyName))
            {
                if (maxValue.HasValue && newMinValue.HasValue && newMinValue.Value.CompareTo(maxValue.Value) > 0)
                {
                    maxValue = newMinValue;
                    NotifyPropertyChanged(maxPropertyName);
                }

                return true;
            }

            return false;
        }

        protected bool SetMax<T>(ref T? oldMaxValue, T? newMaxValue, string maxPropertyName, ref T? minValue, string minPropertyName)
            where T : struct, IComparable<T>
        {
            if (Set(ref oldMaxValue, newMaxValue, maxPropertyName))
            {
                if (minValue.HasValue && newMaxValue.HasValue && newMaxValue.Value.CompareTo(minValue.Value) < 0)
                {
                    minValue = newMaxValue;
                    NotifyPropertyChanged(minPropertyName);
                }

                return true;
            }

            return false;
        }

        protected void AddBrokenRule(string contextName, string message)
        {
			BrokenRules.Add(contextName, message);
        }

        protected void RemoveBrokenRule(string contextName)
        {
			BrokenRules.Remove(contextName);
        }

		protected bool ValidateProperty(object value, string propertyName)
		{
			var validations = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
			var context = new ValidationContext(this, null, null) { MemberName = propertyName };
			if (!Validator.TryValidateProperty(value, context, validations))
			{
				var sb = new StringBuilder();
				foreach (var vres in validations)
				{
					if (vres != System.ComponentModel.DataAnnotations.ValidationResult.Success)
						sb.AppendLine(vres.ErrorMessage);
				}
				if (sb.Length > 0)
					AddBrokenRule(propertyName, sb.ToString());

				return false;
			}
			else
				RemoveBrokenRule(propertyName);

			return true;
		}

        #endregion
		#region Public Methods

		public bool ValidateObject(ICollection<System.ComponentModel.DataAnnotations.ValidationResult> validations)
        {
            var context = new ValidationContext(this, null, null);
            if (!Validator.TryValidateObject(this, context, validations))
            {
                foreach (var vres in validations)
                {
                    if (vres != System.ComponentModel.DataAnnotations.ValidationResult.Success)
                        AddBrokenRule(vres.MemberNames.FirstOrDefault() ?? string.Empty, vres.ErrorMessage);
                }

				return false;
            }

            return true;
        }

        public bool ValidateObject()
        {
			return ValidateObject(new List<System.ComponentModel.DataAnnotations.ValidationResult>());
        }

        #endregion
        #region IDataErrorInfo Members

        [ReflectionExclude(Hide = true)]
        string IDataErrorInfo.Error
        {
            get { return string.Join(Environment.NewLine, BrokenRules.Errors.Values.ToArray()); }
        }

        [ReflectionExclude(Hide = true)]
        string IDataErrorInfo.this[string propertyName]
        {
            get
            {
				if (BrokenRules.Exists(propertyName))
					return BrokenRules[propertyName];

                return null;
            }
        }

        #endregion
		#region Serialization Methods

		public static string SerializeXML<T>(T obj)
		{
            try
            {
                using (var stream = new MemoryStream())
                {
                    var serializer = new DataContractSerializer(typeof(T));
                    serializer.WriteObject(stream, obj);
                    stream.Seek(0, SeekOrigin.Begin);
                    using (var reader = new StreamReader(stream))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error serializing object.", ex);
            }
		}

		public static T DeserializeXML<T>(string xml)
		{
			try
			{
				using (var reader = new StringReader(xml))
				{
					using (var xmlReader = XmlReader.Create(reader))
					{
						var serializer = new DataContractSerializer(typeof(T));
						return (T)serializer.ReadObject(xmlReader);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Error deserializing object.", ex);
			}
		}

		#endregion
    }
}
