﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [Serializable]
    [DataContract]
    public class BaseContract : BaseObject, IBaseObject
    {
        #region Members

        private static readonly DateTime UtcMinValue = DateTime.MinValue.ToUtcDateTime();

        private DateTime _lastModified = UtcMinValue;
        private Type _idType;
        private object _id;

        #endregion
        #region Properties

        [GetValueExcludeProperty]
        [DataMember]
        internal long LastModifiedTicks
        {
            get { return _lastModified.Ticks; }
            set { _lastModified = new DateTime(value, DateTimeKind.Utc); }
        }

        [GetValueExcludeProperty]
        [ReflectionExclude(Hide = true)]
        [DataMember(Order = 0)]
        internal string IdTypeFullName
        {
            get
            {
                if (_idType == null)
                    return null;

                return _idType.FullName;
            }
            set
            {
                if (value == null)
                    _idType = null;
                else
                    _idType = Type.GetType(value);
            }
        }

        [IgnoreDataMember]
        public DateTime LastModified
        {
            get { return _lastModified; }
            set { _lastModified = value; }
        }

        [GetValueExcludeProperty]
        [ReflectionExclude]
        public bool IsPersisted
        {
            get { return LastModified != UtcMinValue; }
        }

        [GetValueExcludeProperty]
        [ReflectionExclude]
        public Type IdType
        {
            get { return _idType; }
        }

        [GetValueExcludeProperty]
        [ReflectionExclude]
        public virtual Type KeyType
        {
            get { return GetType(); }
        }

        [DataMember(Order = 1)]
        public object Id
        {
            get
            {
                return _id;
            }
            set
            {
                if (value != null)
                {
                    if (IdType == null)
                    {
                        _idType = value.GetType();
                        _id = value;
                    }
                    else if (IdType == typeof(Guid))
                        _id = new Guid(value.ToString());
                    else if (IdType == typeof(object))
                        _id = value;
                    else
                        _id = Convert.ChangeType(value, IdType, null);
                }
                else
                    _id = value;
            }
        }

        #endregion
        #region Protected Methods

        protected virtual object NewId(Type idType)
        {
            if (idType == typeof(Guid))
                return NewGuid();

            return null;
        }

        #endregion
        #region Public Methods

        public static Guid NewGuid()
        {
            var guidArray = Guid.NewGuid().ToByteArray();

            var baseDate = new DateTime(1900, 1, 1);
            var now = DateTime.Now.ToUtcDateTime();

            // Get the days and milliseconds which will be used to build the byte string 
            var days = new TimeSpan(now.Ticks - baseDate.Ticks);
            var msecs = now.TimeOfDay;

            // Convert to a byte array 
            // Note that SQL Server is accurate to 1/300th of a millisecond so we divide by 3.333333 
            var daysArray = BitConverter.GetBytes(days.Days);
            var msecsArray = BitConverter.GetBytes((long)(msecs.TotalMilliseconds / 3.333333));

            // Reverse the bytes to match SQL Servers ordering 
            Array.Reverse(daysArray);
            Array.Reverse(msecsArray);

            // Copy the bytes into the guid 
            Array.Copy(daysArray, daysArray.Length - 2, guidArray, guidArray.Length - 6, 2);
            Array.Copy(msecsArray, msecsArray.Length - 4, guidArray, guidArray.Length - 4, 4);

            return new Guid(guidArray);
        }

        public void NewId()
        {
            _id = NewId(_idType);
        }

        public override string ToString()
        {
            return string.Format("{0}({1})", GetType().Name, _id ?? string.Empty);
        }

        public virtual object Clone()
        {
            var contract = (BaseContract)Activator.CreateInstance(GetType());
            this.AssignTo(contract);
            return contract;
        }

        public T Clone<T>()
            where T : BaseContract
        {
            return (T)Clone();
        }

        #endregion
        #region Constructors

        public BaseContract(Type type)
            : base()
        {
            _idType = type;
            NewId();
        }

        public BaseContract()
            : this(typeof(Guid))
        {
        }

        #endregion
    }

    [Serializable]
    [DataContract]
    public class BaseContract<T> : BaseContract
    {
        public BaseContract()
            : base(typeof(T))
        {
        }
    }
}