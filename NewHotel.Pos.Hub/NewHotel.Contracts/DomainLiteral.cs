﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Contracts
{
    [Serializable]
    public struct DomainLiteral<T>
        where T : struct
    {
        private readonly T Id;
        private readonly string Literal;

        public DomainLiteral(T id, string literal)
        {
            Id = id;
            Literal = literal;
        }

        public static implicit operator T(DomainLiteral<T> domainLiteral)
        {
            return domainLiteral.Id;
        }

        public static implicit operator DomainLiteral<T>(T domain)
        {
            return new DomainLiteral<T>(domain, string.Empty);
        }

        public override string ToString()
        {
            return Literal.ToString();
        }
    }
}
