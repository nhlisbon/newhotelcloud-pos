﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace NewHotel.Contracts
{
    public partial class TypedList<T> : ITypedList
    {
        [NonSerialized]
        private PropertyDescriptorCollection _typedProperties;

        public PropertyDescriptorCollection TypedProperties
        {
            get
            {
                if (_typedProperties == null)
                    _typedProperties = GetTypeProperties();
                return _typedProperties;
            }
        }

        private PropertyDescriptorCollection GetTypeProperties()
        {
            var provider = TypeDescriptor.GetProvider(Type ?? typeof(T));
            return provider.GetTypeDescriptor(Type ?? typeof(T)).GetProperties();
        }

        #region ITypedList Members

        public PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors)
        {
            if (listAccessors == null)
                return TypedProperties;

            return null;
        }

        public string GetListName(PropertyDescriptor[] listAccessors)
        {
            return string.Format("List<{0}>", typeof(T).Name);
        }

        #endregion

        private class TypedListComparer : IComparer<T>
        {
            private readonly IDictionary<PropertyDescriptor, short> descriptors;

            public TypedListComparer(PropertyDescriptorCollection coll, IDictionary<string, short> sorts)
            {
                descriptors = new Dictionary<PropertyDescriptor, short>();

                foreach (var sort in sorts)
                {
                    PropertyDescriptor propDesc = coll.Find(sort.Key, true);
                    if (propDesc != null)
                        descriptors.Add(propDesc, sort.Value);
                }
            }

            #region IComparer<T> Members

            public int Compare(T x, T y)
            {
                foreach (var propDesc in descriptors)
                {
                    object value1 = propDesc.Key.GetValue(x);

                    IComparable comparable = value1 as IComparable;
                    if (comparable != null)
                    {
                        object value2 = propDesc.Key.GetValue(y);

                        if (comparable.CompareTo(value2) < 0)
                            return -1 * propDesc.Value;
                        else if (comparable.CompareTo(value2) > 0)
                            return 1 * propDesc.Value;
                    }
                }

                return 0;
            }

            #endregion
        }

        private IComparer<T> _typedListComparer;
        public void Sort()
        {
            if (_typedListComparer == null)
                _typedListComparer = new TypedListComparer(TypedProperties, Sorts);

            this.OrderBy(x => x, _typedListComparer);
        }

        private IDictionary<string, short> _sorts;
        private IDictionary<string, short> Sorts
        {
            get
            {
                if (_sorts == null)
                    _sorts = new Dictionary<string, short>();

                return _sorts;
            }
        }

        public void ClearOrder()
        {
            _sorts = null;
            _typedListComparer = null;
        }

        public void AddAscOrder(string propName)
        {
            if (!Sorts.ContainsKey(propName))
            {
                Sorts.Add(propName, 1);
                _typedListComparer = null;
            }
        }

        public void AddDescOrder(string propName)
        {
            if (!Sorts.ContainsKey(propName))
            {
                Sorts.Add(propName, -1);
                _typedListComparer = null;
            }
        }
    }
}
