﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace NewHotel.Contracts
{
    [Serializable]
    public partial class TypedList<T> : ObservableCollection<T>
    {
        #region Members

        private readonly Type Type;

        #endregion
        #region Constructors

        public TypedList(IEnumerable<T> coll)
            : base(coll)
        {
        }

        public TypedList()
        {
        }

        public TypedList(Type type)
            : this()
        {
            Type = type;
        }

        #endregion
        #region Extension methods

        public void RaiseCollectionChanged(NotifyCollectionChangedAction action)
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(action));
        }

        public void AddRange(IEnumerable<T> items)
        {
            foreach (var item in items)
                Add(item);
        }

        public bool IsEmpty
        {
            get { return Count == 0; }
        }

        public TypedList<T> GetPage(int pageNumber, int pageSize)
        {
            if (pageNumber > 0 && pageSize > 0)
                return new TypedList<T>(this.Skip((pageNumber - 1) * pageSize).Take(pageSize));
            
            return this;
        }

        public TypedList<T1> OfType<T1>()
        {
            return new TypedList<T1>(((IEnumerable<T>)this).OfType<T1>());
        }

        #endregion
    }
}