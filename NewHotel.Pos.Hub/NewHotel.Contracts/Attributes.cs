﻿using System;

namespace NewHotel.Contracts
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ReflectionExclude : Attribute
    {
        public bool Hide { get; set; }
        public bool Read { get; set; }
        public bool Write { get; set; }

        public ReflectionExclude()
        {
        }

        public int Flags
        {
            get { return (Hide ? 1 : 0) | (Read ? 0 : 2) | (Write ? 0 : 4); }
        }

        public override bool Match(object obj)
        {
            if (obj == this)
                return true;

            if (obj != null && obj.GetType().Equals(typeof(int)))
            {
                int flags = (int)obj;
                return (flags & Flags) == flags;
            }

            return false;
        }

        public override string ToString()
        {
            return string.Format("Hide={0}, Read={1}, Write={2}", Hide, Read, Write);
        }
    }

    public class CompareExcludeProperty : Attribute { }
    public class GetValueExcludeProperty : Attribute { }
}
