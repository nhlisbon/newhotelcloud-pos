﻿using System;

namespace NewHotel.Contracts
{
    public class ContextData
    {
        #region Members

        public readonly Action<ContextData, int> Action;

        #endregion
        #region Properties

        public int Context { get; set; }
        public bool CloseAfterOperation { get; set; }

        #endregion
        #region Constructor

        public ContextData() { }

        public ContextData(Action<ContextData, int> action)
        {
            Action = action;
        }

		public ContextData(int context, bool closeAfterOperation)
			: this((cd, c) => { cd.Context = c; })
		{
			Context = context;
			CloseAfterOperation = closeAfterOperation;
		}

        #endregion
    }
}
