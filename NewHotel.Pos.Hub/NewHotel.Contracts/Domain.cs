﻿using System;

namespace NewHotel.Contracts
{
    #region Demo Applications

    [Flags]
    public enum DemoApplications
    {
        None = 0,
        NewHotelPms = 2,
        NewHotelPos = 4,
        NewHotelSpa = 8,
        NewHotelGolf = 16,
        NewHotelStockFB = 32,
        NewHotelEvents = 64,
        NewHotelPortablePos = 128,
        NewHotelGroupManager = 256,
        Cloud = 512,
        ChannelManager = 1024,
        WebSiteManager = 2048,
        BookingEngine = 4096,
        WebOthers = 8192,
        NewHotelIntegratedSolution = 16384
    };

    #endregion

    public enum GenerateType { letters = 1, numbers = 2, both = 3 };

    #region Application Types

    public struct Applications
    {
        public const long NewHotelPms = 1;
        public const long NewHotelPos = 2;
        public const long NewHotelAcc = 3;
        public const long NewHotelSpa = 4;
        public const long NewHotelGolf = 5;
        public const long NewHotelEvents = 6;
        public const long NewHotelStock = 7;
        public const long NewHotelCondo = 8;
        public const long NewHotelSurvey = 9;
        public const long NewHotelMobile = 10;
        public const long NewHotelGMobile = 11;
        public const long NewHotelHousekeeping = 12;
        public const long NewHotelCentral = 13;
        public const long NewHotelGes = 99;
    }

    public enum ApplicationOperation
    {
        AccountDeposit
    }

    #endregion

    /// <summary>
    /// PERF.PERF_INIP = Initial Panel (None, Booking Manager, Check-in Reservations, Check-out Reservations, Interactive Panel, Room Planning)
    /// </summary>
    public enum InitialPanel : long { None = 4, BookingManager = 5001, CheckInReservations = 5002, CheckOutReservations = 5003, InteractivePanel = 5004, RoomPlanning = 5005 };

    #region Permissions

    public enum GroupPermissions : long
    {
        Administration = 1, SettingsLocalization = 5, SettingsMarket = 9, SettingsEntities = 14,
        SettingsTaxes = 19, SettingsDepartmentServices = 23, SettingsReservations = 27, SettingsDocuments = 32,
        SettingsRoomAndZones = 37, SettingsPaymentsCurrenciesDiscounts = 42, SettingsMaintenance = 48,
        SettingsTransportations = 52, Reports = 57, Housekeeping = 64, Rates = 70, Transactions = 80, TransactionsNew = 82,
        TransactionsAccount = 87, TransactionsInvoice = 95, TransactionsInvoiceNew = 97, TransactionsReceivableAccount = 99,
        TransactionsCreditNoteNew = 101, FrontOfficeReservations = 105, FrontOfficeGroups = 115, FrontOfficeGuests = 125,
        FrontOfficeReception = 135, FrontOfficeReservationsNew = 136, FrontOfficeRooms = 145, FrontOfficePlanning = 155,
        FrontOfficeNightAuditor = 165, CreditCard = 175, SettingsResources = 185,
        SettingsTherapists = 195, SettingsProducts = 200, SettingsPointofSales = 205, FrontOfficeSPAReservations = 210,
        FrontOfficePackageReservations = 215, FrontOfficeSPAPlannings = 220, Management = 225, Tickets = 230, SettingsPOS = 235, SettingsSecurity = 240,
        EventsFrontOffice = 245, EventsSettings = 250, EventsDocuments = 255, EventsFrontOfficeStaff = 260, EventsSettingsLocations = 265, EventsSettingsCategories = 270,
        EventsSettingsTermsAndConditions = 275
    };

    public enum Permissions : long
    {
        Settings = 1, ControlAccess = 2, EventViewer = 3, Languajes = 4,
        WorldRegions = 5, Countries = 6, BorderPoints = 7, Holidays = 8,
        CountryZones = 9, CountryRegions = 10, PostalCodes = 11,
        SourcesGroups = 12, Sources = 13, SegmentGroups = 14, Segments = 15, SourcesSegments = 16,
        Companies = 17, CompanyTypes = 18, Banks = 19, Clients = 20, ClientTypes = 21,
        GuestCategories = 22, ClientAttentions = 23, Preferences = 24, Relatives = 25,
        Professions = 26, JobFunctions = 27, Contacts = 28, SalesPeople = 29, Titles = 30, Employee = 31,
        EmployeeRoles = 32, ReceivablePayableAccounts = 33, TaxSchemes = 34, TaxRegions = 35,
        TaxSequences = 36, TaxRates = 37, Departments = 38, Services = 39, ServiceGroups = 40,
        ServiceCategories = 41, Tips = 42, Packages = 43, Resources = 44, ResourceTypes = 45,
        ResourceCharacteristics = 46, ResourceInactivities = 47, Additionals = 48, AdditionalsTypes = 49,
        StopSales = 50, Therapists = 51, WorkShiftTypes = 52, WorkShiftsInactivities = 53,
        TherapistProfiles = 54, Products = 55, ProductsGroups = 56, ProductsFamily = 57,
        ProductsSubfamily = 58, ProductsSeparators = 59, PointSales = 60, Cashiers = 61,
        Salons = 62, DispatchAreas = 63, PosServices = 64, TicketConfiguration = 65, PaymentImages = 66,
        Duties = 67, GroupReservationTypes = 68, GuaranteeTypes = 69, CancellationReasons = 70,
        UnexpectedDepartureTypes = 71, MealPlan = 72, Extensions = 73, NightAuditorConfiguration = 74,
        AgeRanges = 75, DocumentSeries = 76, Rooms = 77, RoomTypes = 78, RoomBlocks = 79,
        ExtraResources = 80, ExtraResourcesTypes = 81, RoomCaracteristics = 82, AreaTypes = 83,
        PublicAreas = 84, Currencies = 85, PaymentTypes = 86, CreditCardsTypes = 87,
        DiscountTypes = 88, PaymentsMethodsCredit = 89, PaymentsMethodsHouseUse = 90, PaymentsMethodsMealPlan = 91,
        MalfunctionTypes = 92, MalfunctionSource = 93, OutofOrderTypes = 94, Flights = 95, Vehicles = 96,
        ParkingZones = 97, ParkingLocations = 98, Reports = 99, Rates = 100, Contracts = 101,
        RateBreakdown = 102, StopReservations = 103, Allotments = 104, AllotmentsExtraBeds = 105,
        HappyHours = 106, InternalConsumptions = 107, Transactions = 108, TransactionRoomCharges = 109,
        TransactionDeposit = 110, TransactionSplit = 111, TransactionTransfer = 112, Folios = 113,
        ControlAccount = 114, Invoices = 115, InvoicesCreate = 116, InvoicesCopies = 117,
        AdvancedInvoices = 118, PrintCopiesOriginalDocuments = 119, AllowTransferExternalAccount = 120,
        CreditNotes = 121, CreditNotesCreate = 122, CreditNotesCopies = 123, Receipts = 124, ReceiptsCopies = 125,
        ReceivableAccount = 126, GuestReservations = 127, ReservationCheckIn = 128, ReservationCheckOut = 129,
        ReservationNoShow = 130, ReservationOverbooking = 131, LockRoomsReservations = 132,
        ReservationCopies = 133, MandatoryRatePriceReservations = 134, DiscountsReservations = 135,
        OpenCurrentAccounts = 136, CloseCurrentAccounts = 137, PhoneCalls = 138, Messages = 139,
        WakeUp = 140, VehiclesRegistration = 141, ExchangeRates = 142, LostandFound = 143,
        Workshift = 144, RoomPlanning = 145, Plannings = 146, GroupReservations = 147, RoomingList = 148,
        RoomStatus = 149, RoomDiscrepancies = 150, RoomsOutofOrder = 151, Guests = 152, GuestCheckIn = 153,
        GuestCheckOut = 154, GuestArrivalDepartureDate = 155, LockRooms = 156, RoomAllocation = 157,
        ExecuteNightAudit = 158, ExpectedIncomes = 159, ArrivalDepartureControl = 160,
        NightAuditStatistics = 161, ControlReports = 162, FinalReports = 163, ConfirmEndofDay = 164,
        Details = 165, Tickets = 166, Logs = 167, TicketsCancelWithInvoice = 168, TicketsMerge = 169,
        TicketsTransfer = 170, TicketsPrintPaymentInstructions = 171, TicketsDiscounts = 172,
        TicketsTips = 173, TicketWithoutTable = 174, AccessAllProductsList = 175,
        SendProductsToDispatchAreas = 176, CancelProductLine = 177, ClosedTickets = 178, Areas = 179,
        PrinterDefinition = 180, Users = 181, CloseDay = 182, CloseShift = 183, Upload = 184,
        DownloadGeneralSettings = 185, DownloadProducts = 186, DownloadPricesHappyHours = 187,
        DownloadSaloonTables = 188, CashDrawer = 189, ChangeOfStand = 190, SPAReservations = 191,
        SpaReservationCheckIn = 192, SpaReservationCheckOut = 193, SpaReservationNoShow = 194, SpaReservationOverbooking = 195,
        ClientSchedules = 196, PackageReservations = 197, SPAPlannings = 198, ProductsMenu = 199, ProductsPreparations = 200,
        EventsNewEvent = 201, EventsItems = 202, ViewEvent = 203, CheckIn = 204, CheckOut = 205, Cancel = 206, Copy = 207,
        UndoCheckIn = 208, UndoCheckOut = 209, ConfirmationState = 210, Contract = 211, EventOrder = 212, Schedule = 213,
        StaffManager = 214, WorkSchedule = 215, SiteLocations = 216, LocationTypes = 217, LocationAttributes = 218,
        SetupStyles = 219, ItemTypes = 220, EventCategories = 221, EventStatus = 222, EventResponsabilities = 223,
        TermsAndConditions = 224, Dashboard = 225, CreditLimit = 226, CreditAuthorization = 227, RatePrices = 228, Profiles = 229,
        ReservationBulkUpdatePrice = 230, EditFiscalNumber = 231, TicketsCancelWithoutInvoice = 232, CloseTickets = 233,
        EnableRoomCharge = 234, EnableMealPlan = 235, EnableHouseUse = 236, EnableCashPayment = 237, VoidProductsToDispatchArea = 238
    };

    #endregion 
}