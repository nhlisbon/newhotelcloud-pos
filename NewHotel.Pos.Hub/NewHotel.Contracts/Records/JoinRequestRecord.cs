﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;
using System.Xml.Serialization;
using System.IO;

namespace NewHotel.Contracts
{
    [MappingQuery("ADHE_PK")]
    public class JoinRequestRecord : BaseRecord
    {
        [MappingColumn("IS_TRIAL", Nullable = true)]
        public bool IsTrial { get; set; }

        [MappingColumn("IS_SALE", Nullable = true)]
        public bool IsSale { get; set; }

        [MappingColumn("HOTE_NAME", Nullable = true)]
        public string HotelName { get; set; }

        [MappingColumn("CONT_NAME", Nullable = true)]
        public string ContactName { get; set; }

        [MappingColumn("HOTE_EMAI", Nullable = true)]
        public string HotelEmail { get; set; }

        [MappingColumn("HOTE_URL", Nullable = true)]
        public string HotelUrl { get; set; }

        [MappingColumn("HOTE_TELE", Nullable = true)]
        public string HotelTelephone { get; set; }

        [MappingColumn("HOTE_NACI", Nullable = true)]
        public string HotelCountry { get; set; }

        [MappingColumn("HOTE_ROOM", Nullable = true)]
        public long RoomAmount { get; set; }

        [MappingColumn("BILL_VALO", Nullable = true)]
        public decimal? BillValue { get; set; }

        [MappingColumn("BILL_UNMO", Nullable = true)]
        public string BillCurrency { get; set; }

        [MappingColumn("BILL_COMP", Nullable = true)]
        public string FiscalName { get; set; }

        [MappingColumn("BILL_ADDR", Nullable = true)]
        public string FiscalAddress { get; set; }

        [MappingColumn("BILL_LOCA", Nullable = true)]
        public string FiscalLocal { get; set; }

        [MappingColumn("BILL_NUCO", Nullable = true)]
        public string FiscalNumber { get; set; }

        [MappingColumn("ADHE_DESC", Nullable = true)]
        public string Description { get; set; }

        [MappingColumn("ADHE_LMOD", Nullable = true)]
        public DateTime RegistrationDate { get; set; }
    }
}
