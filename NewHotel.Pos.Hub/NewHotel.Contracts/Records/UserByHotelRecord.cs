﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class UserByHotelRecord : BaseRecord, IKeyDescRecord<object>
    {
        #region Members

        private string _description;
        private bool _inactive;
        private bool _currentInstallation;

        #endregion
        #region Properties

        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }
        [DataMember]
        public bool CurrentInstallation { get { return _currentInstallation; } set { Set(ref _currentInstallation, value, "CurrentInstallation"); } }

        #endregion
        #region Public Methods

        public override string ToString()
        {
            return Description;
        }

        #endregion
    }
}