﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PROP_PK")]
    public class ProposalRequestRecord : BaseRecord
    {
        [MappingColumn("PROP_NAME", Nullable = true)]
        public string Name { get; set; }

        [MappingColumn("PROP_EMAI", Nullable = true)]
        public string Email { get; set; }

        [MappingColumn("PROP_TELF", Nullable = true)]
        public string Phone { get; set; }

        [MappingColumn("PROP_ENTI", Nullable = true)]
        public string Entity { get; set; }

        [MappingColumn("NACI_PK", Nullable = true)]
        public string Country { get; set; }

        [MappingColumn("NACI_DESC", Nullable = true)]
        public string CountryDescription { get; set; }

        [MappingColumn("PROP_HTTP", Nullable = true)]
        public string Website { get; set; }

        [MappingColumn("WIND_OBSE", Nullable = true)]
        public string WindowsComments { get; set; }

        [MappingColumn("WEBS_OBSE", Nullable = true)]
        public string WebComments { get; set; }
    }
}
