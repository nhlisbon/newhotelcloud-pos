﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("COMP_PK")]
    public class GroupHotelRecord : BaseRecord
    {
        [MappingColumn("COMP_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}
