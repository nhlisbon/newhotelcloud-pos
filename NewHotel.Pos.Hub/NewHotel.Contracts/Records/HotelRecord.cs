﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("HOTE_PK")]
    public class HotelRecord : BaseRecord
    {
        [MappingColumn("HOTE_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [MappingColumn("HOTE_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}