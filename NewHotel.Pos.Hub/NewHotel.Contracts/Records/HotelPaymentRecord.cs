﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TIPA_PK")]
    public class HotelPaymentRecord : BaseRecord
    {
        [MappingColumn("TIPA_DESC", Nullable = true)]
        public string Description { get; set; }

        [MappingColumn("TIPA_INAC")]
        public bool Inactive { get; set; }
    }
}
