﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TPLT_PK")]
    public class TemplateRecord : BaseRecord
    {
        [MappingColumn("TPLT_BASE", Nullable = true)]
        public bool Default { get; set; }

        [MappingColumn("TPLT_DESC", Nullable = true)]
        public string Description { get; set; }

    }
}
