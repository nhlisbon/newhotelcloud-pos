﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("UNMO_PK")]
    public class CurrencyAppRecord : BaseRecord
    {
        [MappingColumn("UNMO_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}