﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("IPOS_PK")]
    public class StandRecord : BaseRecord
    {
        [MappingColumn("IPOS_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("IPOS_ABRE", Nullable = false)]
        public string Abbreviation { get; set; }
        [MappingColumn("IPOS_TYPE", Nullable = false)]
        public string StandType { get; set; }
        [MappingColumn("TPRG_STDR", Nullable = true)]
        public Guid? StandardRateId { get; set; }
        [MappingColumn("TPRG_PENS", Nullable = true)]
        public Guid? PensionRateId { get; set; }
        [MappingColumn("TPRG_CINT", Nullable = true)]
        public Guid? InternalUseRateId { get; set; }
        [MappingColumn("CTRL_PK", Nullable = true)]
        public Guid? ControlAccount { get; set; }      
        [MappingColumn("STDR_DESC", Nullable = true)]
        public string StandardRate { get; set; }
        [MappingColumn("PENS_DESC", Nullable = true)]
        public string PensionRate { get; set; }
        [MappingColumn("CINT_DESC", Nullable = true)]
        public string InternalUseRate { get; set; }
        [MappingColumn("IPOS_FCTR", Nullable = true)]
        public DateTime WorkDate { get; set; }
        [MappingColumn("TPRG_CIPO", Nullable = true)]
        public decimal? InternalUsePercent { get; set; }
        [MappingColumn("TPRG_PEPO", Nullable = true)]
        public decimal? PensionPercent { get; set; }
        [MappingColumn("IPOS_CKAB", Nullable = false)]
        public bool CloseDayOpenTickets { get; set; }
        [MappingColumn("IPOS_TUTR", Nullable = false)]
        public int Shift {  get; set;  }
        [MappingColumn("CNFG_TICK", Nullable = true)]
        public Guid? TicketPrint { get; set; }
        [MappingColumn("CNFG_COMP", Nullable = true)]
        public Guid? ReceiptPrint { get; set; }
        [MappingColumn("CNFG_FACT", Nullable = true)]
        public Guid? InvoicePrint { get; set; }
        [MappingColumn("IPOS_KITI", Nullable = false)]
        public bool CopyKitchenInTicketPrinter { get; set; }
        [MappingColumn("IPOS_SPTA", Nullable = false)]
        public bool KeepTicketsInTableOnManualSplit { get; set; }
        [MappingColumn("IPOS_TISE", Nullable = true)]
        public Guid? TipService {get;set;}
        [MappingColumn("IPOS_TIPE", Nullable = true)]
        public decimal? TipPercent { get; set; }
        [MappingColumn("IPOS_TIVL", Nullable = true)]
        public decimal? TipValue { get; set; }
        [MappingColumn("IPOS_TIAT", Nullable = false)]
        public bool TipAuto { get; set; }
        [MappingColumn("IPOS_TION", Nullable = false)]
        public bool TipOverNet { get; set; }
       
        public string WorkDateDesc 
        {
            get { return WorkDate.ToShortDateString(); }
        }
    }
}