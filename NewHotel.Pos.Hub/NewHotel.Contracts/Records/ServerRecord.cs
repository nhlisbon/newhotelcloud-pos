﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("SRVR_PK")]
    public class ServerRecord : BaseRecord
    {
        [MappingColumn("SRVR_DESC", Nullable = false)]
        public string Description { get; set; }
        [MappingColumn("SRVR_URL")]
        public string Url { get; set; }
        [MappingColumn("SRVR_CONT")]
        public long Count { get; set; }
        [MappingColumn("SRVR_ORCL", Nullable = false)]
        public string Services { get; set; }

        public TypedList<KeyDescRecord> DatabaseServices
        {
            get
            {
                var databaseServices = new TypedList<KeyDescRecord>();
                if (!string.IsNullOrEmpty(Services))
                {
                    var services = Services.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int index = 0; index < services.Length; index++)
                        databaseServices.Add(new KeyDescRecord() { Id = index, Description = services[index] });
                }
                else
                    databaseServices.Add(new KeyDescRecord() { Id = 0, Description = "DESARROLLO" });

                return databaseServices;
            }
        }
    }
}
