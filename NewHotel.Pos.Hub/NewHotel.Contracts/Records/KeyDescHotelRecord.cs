﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class KeyDescHotelRecord : KeyDescRecord
    {
        #region Members

        private string _url;
        private bool _inactive;
        private string _country;

        #endregion
        #region Properties

        [DataMember]
        public string Url { get { return _url; } set { Set(ref _url, value, "Url"); } }
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }
        [DataMember]
        public string Country { get { return _country; } set { Set(ref _country, value, "Country"); } }

        #endregion
    }
}
