﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("ONPA_PK")]
    public class OnlinePaymentRecord : BaseRecord
    {
        [MappingColumn("ONPA_DAOP", Nullable = false)]
        public DateTime OperationDate { get; set; }

        [MappingColumn("ONPA_VALO", Nullable = false)]
        public decimal Value { get; set; }

        [MappingColumn("ONPA_MONE", Nullable = false)]
        public string Currency { get; set; }

        [MappingColumn("ONPA_ECOD", Nullable = true)]
        public string ReplyCode { get; set; }

        [MappingColumn("ONPA_EMSG", Nullable = true)]
        public string ReplyMessage { get; set; }

        [MappingColumn("ONPA_EDES", Nullable = true)]
        public string ReplyDescription { get; set; }

        [MappingColumn("ONPA_MOEX", Nullable = true)]
        public string ForeingCurrency { get; set; }

        [MappingColumn("ONPA_VAEX", Nullable = true)]
        public decimal? ForeingValue { get; set; }

        [MappingColumn("ONPA_CAMB", Nullable = true)]
        public decimal? ForeingChange { get; set; }

        [MappingColumn("ONPA_PAYM", Nullable = false)]
        public bool Receipt { get; set; }

        [MappingColumn("ONPA_PDAT", Nullable = true)]
        public DateTime? ReceiptDate { get; set; }

        [MappingColumn("ONPA_DETA", Nullable = true)]
        public string HotelName { get; set; }

        [MappingColumn("FACT_NACI", Nullable = true)]
        public string Country { get; set; }

        [MappingColumn("NAME_CONTACT", Nullable = true)]
        public string NameContact { get; set; }
    }
}
