﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("EVEN_PK")]
    public class EventRecord : BaseRecord
    {
        [MappingColumn("EVEN_NAME", Nullable = false)]
        public string Name { get; set; }
        [MappingColumn("EVEN_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("EVEN_DATE", Nullable = false)]
        public DateTime Date { get; set; }
        [MappingColumn("EVEN_PUB", Nullable = false)]
        public DateTime PublicationDate { get; set; }
        [MappingColumn("EVEN_RESP", Nullable = false)]
        public string Responsible { get; set; }
        [MappingColumn("EVEN_ORG", Nullable = true)]
        public string Distribution { get; set; }
        [MappingColumn("NACI_PK", Nullable = false)]
        public string Country { get; set; }

        public EventRecord()
        {
            Date = DateTime.Now;
        }
       
    }

}
