﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("REGI_PK")]
    public class TaxRegionAppRecord:BaseRecord
    {
        [MappingColumn("REGI_DESC", Nullable = true)]
        public string Description { get; set; }

        [MappingColumn("NACI_DESC", Nullable = true)]
        public string Country { get; set; }

        [MappingColumn("NACI_PK", Nullable = true)]
        public string CountryCode { get; set; }

        [MappingColumn("REGI_CAUX", Nullable = true)]
        public string Code { get; set; }
    }
}
