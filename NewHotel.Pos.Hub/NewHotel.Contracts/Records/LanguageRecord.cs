﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("LICL_PK")]
    public class AllLanguageRecord : BaseRecord
    {
        [MappingColumn("LICL_DESC", Nullable = true)]
        public string Description { get; set; }
    }

    [MappingQuery("LANG_PK")]
    public class TranslationLanguagesRecord : BaseRecord
    {
        [MappingColumn("LANG_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}