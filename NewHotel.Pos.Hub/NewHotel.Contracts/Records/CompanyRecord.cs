﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("EMPR_PK")]
    public class CompanyRecord : BaseRecord
    {
        [MappingColumn("EMPR_NAME", Nullable = true)]
        public string Name { get; set; }
        [MappingColumn("EMPR_MORA", Nullable = true)]
        public string Address { get; set; }
        [MappingColumn("EMPR_PHONE", Nullable = true)]
        public string Phone { get; set; }
        [MappingColumn("EMPR_WPAG", Nullable = true)]
        public string WebPage { get; set; }
        [MappingColumn("EMPR_NIF", Nullable = true)]
        public string NoFiscal { get; set; }
        [MappingColumn("NACI_DESC", Nullable = true)]
        public string Country { get; set; }
    }

}
