﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("DIST_PK")]
    public class HotelDistributorRecord : BaseRecord
    {
        [MappingColumn("DIST_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("NACI_DESC", Nullable = true)]
        public string Country { get; set; }
        [MappingColumn("DIST_MAIL", Nullable = true)]
        public string Email { get; set; }
    }
}
