﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("DEMO_PK")]
    public class DemoRequestRecord : BaseRecord
    {
        [MappingColumn("DEMO_NAME", Nullable = true)]
        public string Name { get; set; }

        [MappingColumn("DEMO_EMAI", Nullable = true)]
        public string Email { get; set; }

        [MappingColumn("DEMO_TELF", Nullable = true)]
        public string Phone { get; set; }

        [MappingColumn("DEMO_ENTI", Nullable = true)]
        public string Entity { get; set; }

        [MappingColumn("NACI_PK", Nullable = true)]
        public string Country { get; set; }

        [MappingColumn("NACI_DESC", Nullable = true)]
        public string CountryDescription { get; set; }

        [MappingColumn("DEMO_HTTP", Nullable = true)]
        public string Website { get; set; }

        [MappingColumn("DEMO_DATE", Nullable = true)]
        public string SuggestedDate { get; set; }

        [MappingColumn("DEMO_TIME", Nullable = true)]
        public string SuggestedTime { get; set; }

        [MappingColumn("DEMO_ADDR", Nullable = true)]
        public string Address { get; set; }

        //[MappingColumn("DEMO_APPL", Nullable = true)]
        //public DemoApplications Applications { get; set; }
    }
}
