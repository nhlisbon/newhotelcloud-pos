﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("LANG_PK")]
    public class LanguageTranslationRecord : BaseRecord
    {
        [MappingColumn("LANG_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("LANG_PRIO", Nullable = true)]
        public short Priority { get; set; }
    }
}