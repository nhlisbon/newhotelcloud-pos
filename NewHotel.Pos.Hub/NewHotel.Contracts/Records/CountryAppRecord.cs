﻿using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("NACI_PK")]
    public class CountryAppRecord : BaseRecord<string>
    {
        [MappingColumn("NACI_DESC", Nullable = true)]
        public string Description { get; set; }
    }
}