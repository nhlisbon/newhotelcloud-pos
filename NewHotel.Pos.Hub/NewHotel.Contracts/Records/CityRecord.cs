﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("RSHO_PK")]
    public class CityRecord : BaseRecord
    {
        [MappingColumn("RSHO_CITY", Nullable = true)]
        public string Name { get; set; }
        [MappingColumn("RSHO_LONG", Nullable = true)]
        public decimal Longitude { get; set; }
        [MappingColumn("RSHO_LATI", Nullable = true)]
        public decimal Latitude { get; set; }
        [MappingColumn("RSHO_UNMO", Nullable = true)]
        public string Currency { get; set; }
        [MappingColumn("RSHO_RADI", Nullable = true)]
        public short Radio { get; set; }
        [MappingColumn("RSHO_ACTI", Nullable = true)]
        public bool Active { get; set; }
    }

}
