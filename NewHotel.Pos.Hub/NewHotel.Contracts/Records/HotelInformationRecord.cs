﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;
using System.Xml.Serialization;
using System.IO;

namespace NewHotel.Contracts
{
    [MappingQuery("HOTE_PK")]
    public class HotelInformationRecord : BaseRecord
    {
        [MappingColumn("HOTE_DESC", Nullable = true)]
        public string Description { get; set; }
        //[MappingColumn("CIDI_DATE", Nullable = true)]
        //public DateTime DayClosedDate { get; set; }
        //[MappingColumn("CIDI_USER", Nullable = true)]
        //public string DayClosedUser { get; set; }
        [MappingColumn("LOGI_DATE", Nullable = true)]
        public DateTime LoginDate { get; set; }
        [MappingColumn("LOGI_USER", Nullable = true)]
        public string LoginUser { get; set; }
    }
}
