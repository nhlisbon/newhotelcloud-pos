﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class HierarchicalRecord<T> : BaseRecord<T>
    {
        [DataMember]
        public IHierarchicalRecord<T> Record { get; internal set; }
        [DataMember]
        public IList<HierarchicalRecord<T>> ChildRecords { get; internal set; }

        public string Description
        {
            get { return Record.Description; }
        }

        public HierarchicalRecord(T id, IHierarchicalRecord<T> record)
        {
            Id = id;
            Record = record;
            ChildRecords = new List<HierarchicalRecord<T>>();
        }
    }
}