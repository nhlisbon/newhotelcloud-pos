﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("APPL_PK")]
    public class ApplicationModuleRecord : BaseRecord<long>
    {
        [DataMember]
        [MappingColumn("APPL_NAME", Nullable = true)]
        public string Description { get; set; }
    }
}