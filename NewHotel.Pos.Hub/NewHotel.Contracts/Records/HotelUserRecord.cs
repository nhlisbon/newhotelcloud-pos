﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;
using System.Xml.Serialization;
using System.IO;

namespace NewHotel.Contracts
{
    [MappingQuery("HOTE_PK")]
    public class HotelUserRecord : BaseRecord
    {
        [MappingColumn("HOTE_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("HOUT_PK", Nullable = true)]
        public Guid UserByHotelId { get; set; }
        [MappingColumn("COMP_DESC", Nullable = true)]
        public string GroupDescription { get; set; }
        [MappingColumn("UTIL_LOGIN", Nullable = true)]
        public string Login { get; set; }
        [MappingColumn("UTIL_PASS", Nullable = true)]
        public string Password { get; set; }
    }
}
