﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("CENT_PK")]
    public class CentralRecord : BaseRecord
    {
        [MappingColumn("CENT_NAME", Nullable = true)]
        public string Name { get; set; }
        [MappingColumn("CENT_CODE", Nullable = true)]
        public string Code { get; set; }
        [MappingColumn("CENT_ACTI", Nullable = true)]
        public bool Active { get; set; }
        [MappingColumn("SCMA_NAME", Nullable = true)]
        public string SchemaName { get; set; }
        [MappingColumn("SERV_NAME", Nullable = true)]
        public string ServiceName { get; set; }
    }

}
