﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [Serializable]
    [DataContract]
    public class KeyDescRecord<T> : BaseRecord<T>, IKeyDescRecord<T>, IEquatable<KeyDescRecord<T>>
    {
        #region Members

        private string _description;

        #endregion
        #region Constructors

        protected KeyDescRecord(string description)
        {
            Description = description;
        }

        public KeyDescRecord()
        {
        }

        public KeyDescRecord(T id, string description)
            : this(description)
        {
            Id = id;
        }

        #endregion
        #region Properties

        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }

        #endregion
        #region Public Methods

        public bool Equals(KeyDescRecord<T> other)
        {
            return Id.Equals(other.Id);
        }

        public override string ToString()
        {
            return Description;
        }

        #endregion
    }

    [Serializable]
    [DataContract]
    public class KeyDescRecord : KeyDescRecord<object>
    {
        #region Constructors

        public KeyDescRecord()
        {
        }

        public KeyDescRecord(object id, string description)
            : base(description)
        {
            Id = id;
        }

        #endregion
    }
}