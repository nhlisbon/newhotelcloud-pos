﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TIVA_PK")]
    public class TaxRateAppRecord:BaseRecord
    {

        [MappingColumn("TIVA_DESC", Nullable = true)]
        public string Description { get; set; }

        [MappingColumn("TIVA_PERC", Nullable = true)]
        public string Percent { get; set; }

        [MappingColumn("SEIM_ABRE", Nullable = true)]
        public string TaxSequence { get; set; }

        [MappingColumn("NACI_REGI", Nullable = true)]
        public string TaxRegion { get; set; }

        [MappingColumn("SEIM_PK", Nullable = false)]
        public Guid TaxSequenceId { get; set; }

        [MappingColumn("NACI_DESC", Nullable = true)]
        public string Country { get; set; }
    }
}
