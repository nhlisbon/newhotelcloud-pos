﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("NACI_PK")]
    public class CountryRecord : BaseRecord
    {
        [MappingColumn("NACI_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("NACI_REMU", Nullable = true)]
        public string Region { get; set; }
        [MappingColumn("NACI_LICL", Nullable = true)]
        public string Language { get; set; }
        [MappingColumn("NACI_CACO", Nullable = true)]
        public string CallCode { get; set; }
        [MappingColumn("ISO_3166", Nullable = true)]
        public string Alpha3 { get; set; }
    }
}
