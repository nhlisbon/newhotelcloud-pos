﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("RGPD_PK")]
    public class RgpdTermsServiceRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("LANG_DESC", Nullable = true)]
        public string Language { get; set; }
        [DataMember]
        [MappingColumn("RGPD_SUBJ", Nullable = true)]
        public string EmailSubject { get; set; }
        [DataMember]
        [MappingColumn("RGPD_TEXT", Nullable = true)]
        public string TermsService { get; set; }
    }
}
