﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("SEIM_PK")]
    public class TaxSequenceAppRecord:BaseRecord
    {
        [MappingColumn("SEIM_ABRE", Nullable = true)]
        public string Abbreviation { get; set; }
        [MappingColumn("SEIM_DESC", Nullable = true)]
        public string Description { get; set; }

    }
}
