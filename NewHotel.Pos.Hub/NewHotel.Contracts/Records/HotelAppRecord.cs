﻿using System;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [MappingQuery("HOTE_PK")]
    public class HotelAppRecord : BaseRecord
    {
        [DataMember]
        [MappingColumn("HOTE_DESC", Nullable = true)]
        public string Description { get; set; }
        [DataMember]
        [MappingColumn("HOTE_PROV", Nullable = true)]
        public short Provider { get; set; }
        [DataMember]
        [MappingColumn("HOTE_SERV", Nullable = true)]
        public string DataBaseSerice { get; set; }
        [DataMember]
        [MappingColumn("HOTE_USER", Nullable = true)]
        public string User { get; set; }
        [DataMember]
        [MappingColumn("COMP_DESC", Nullable = true)]
        public string GroupDescription { get; set; }
        [DataMember]
        [MappingColumn("CENT_NAME", Nullable = true)]
        public string CentralDescription { get; set; }
        [DataMember]
        [MappingColumn("HOTE_STAR", Nullable = true)]
        public short Stars { get; set; }
        [DataMember]
        [MappingColumn("HOTE_CODE", Nullable = true)]
        public string Code { get; set; }
        [DataMember]
        [MappingColumn("HOTE_TZNE", Nullable = true)]
        public string TimeZoneId { get; set; }
        [DataMember]
        [MappingColumn("HOTE_RPTF", Nullable = true)]
        public string ReportFolder { get; set; }
        [MappingColumn("HOTE_LICE", Nullable = true)]
        public string License { get; set; }
        [DataMember]
        [MappingColumn("LICE_TYPE", Nullable = true)]
        public string LicenseType { get; set; }
        [DataMember]
        [MappingColumn("NIAU_DATA", Nullable = true)]
        public DateTime? NightAuditorDate { get; set; }
        [DataMember]
        [MappingColumn("LOOP_DAHO", Nullable = true)]
        public DateTime? LoginDate { get; set; }
        [DataMember]
        [MappingColumn("NGES_DATA", Nullable = true)]
        public DateTime? NewGesDate { get; set; }
        [DataMember]
        [MappingColumn("HOTE_NACI", Nullable = true)]
        public string HotelCountry { get; set; }
        [DataMember]
        [MappingColumn("DIST_DESC", Nullable = true)]
        public string Distributor { get; set; }
        [DataMember]
        [MappingColumn("HOTE_INAC", Nullable = true)]
        public bool Inactive { get; set; }

        private DateTime? _dateLimit;
        [DataMember]
        public DateTime? DateLimit { get { return LoadDateFromXml(License); } set { _dateLimit = value; } }

        private decimal _rating;
        [DataMember]
        public decimal Rating { get { return ((decimal)(Stars * 2.0 / 10)); } set { _rating = value; } }

        public DateTime? LoadDateFromXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml))
            {
                using (var stream = new MemoryStream())
                {
                    var encoder = System.Text.Encoding.UTF8;
                    foreach (byte b in encoder.GetBytes(xml))
                        stream.WriteByte(b);
                    stream.Seek(0, SeekOrigin.Begin);

                    var serializer = new XmlSerializer(typeof(LicenseContract));
                    var result = (LicenseContract)serializer.Deserialize(stream);
                    stream.Close();

                    var properties = result.Properties.FirstOrDefault(x => x.PropertyCode == "nht" || x.PropertyCode == Code);
                    if (properties != null)
                        return properties.Products.Where(y => y.Code == "nht").FirstOrDefault().DateLimit.Date;
                }
            }
            
            return null;               
        }
    }
}