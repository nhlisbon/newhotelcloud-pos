﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("PERM_PK")]
    public class PermissionByRoleRecord : BaseRecord
    {     
        [MappingColumn("PERM_DESC", Nullable = true)]
        public string PermissionDescription { get; set; }
        [MappingColumn("PERM_ORDE", Nullable = true)]
        public string Order { get; set; }
        [MappingColumn("ROLE_PK", Nullable = true)]
        public short  RoleId { get; set; }
        [MappingColumn("ROLE_DESC", Nullable = true)]
        public string Rol { get; set; }
        [MappingColumn("SECU_CODE", Nullable = true)]
        public long SecureCode { get; set; }
        [MappingColumn("SECU_DESC", Nullable = true)]
        public string SecureCodeDescription { get; set; }
        [MappingColumn("ROPE_PK", Nullable = true)]
        public Guid PermissionRoleId { get; set; }        
    }
}