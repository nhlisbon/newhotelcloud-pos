﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("TEFT_PK")]
    public class TEFTerminalsRecord : BaseRecord
    {
        [MappingColumn("TEFT_DESC", Nullable = false)]
        public string Description { get; set; }


    }


    public class OnlinePaymentTerminalRecord : BaseRecord
    {
        public string Description { get; set; }
      
    }

}
