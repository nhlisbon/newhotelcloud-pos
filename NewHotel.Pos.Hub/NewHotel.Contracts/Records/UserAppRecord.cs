﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [MappingQuery("UTIL_PK")]
    public class UserAppRecord : BaseRecord
    {
        [MappingColumn("UTIL_LOGIN", Nullable = true)]
        public string Login { get; set; }
        [MappingColumn("UTIL_DESC", Nullable = true)]
        public string Description { get; set; }
        [MappingColumn("ROLE_DESC", Nullable = true)]
        public string RoleDescription { get; set; }
        [MappingColumn("HOUT_INAC", Nullable = true)]
        public bool? Inactive { get; set; }
        [MappingColumn("HOTE_DESC", Nullable = true)]
        public string HotelDescription { get; set; }
        [MappingColumn("HOUT_PK", Nullable = true)]
        public Guid? HotelbyUserId{ get; set; }
        [MappingColumn("HOTE_PK", Nullable = true)]
        public Guid? HotelId { get; set; }
        [MappingColumn("ROLE_OPTS", Nullable = true)]
        public string RolOpts { get; set; }
        
    }
}
