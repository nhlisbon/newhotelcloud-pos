﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    /// <summary>
    /// Class to encapsulate results for Queries
    /// </summary>
    /// <remarks>
    /// This class can be use to deliver query result to client side or transport between layers
    /// </remarks>
    [DataContract]
	[Serializable]
    public partial class MultiQueryResponse
    {
        #region Members
        private IDictionary<string, QueryResponse> _responses;
        #endregion
        #region Properties
        /// <summary>
        /// Column mappings between query columns and data properties
        /// </summary>
        [DataMember]
        public IDictionary<string, QueryResponse> Responses
        {
            get { return _responses; }
            set { _responses = value; }
        }
        #endregion
        #region Constructors
        

        #endregion
    }
}
