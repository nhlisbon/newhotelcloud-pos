﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class InstallationsByUserResponse : QueryResponse
    {
        #region Properties

        [DataMember]
        public Guid? UserId { get; internal set; }

        #endregion
        #region Constructor

        public InstallationsByUserResponse(QueryResponse qr, Guid? userId)
            : base(qr.Type, qr)
        {
            UserId = userId;
        }

        #endregion
    }
}