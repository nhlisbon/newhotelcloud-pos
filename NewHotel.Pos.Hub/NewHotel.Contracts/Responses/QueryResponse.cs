﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    /// <summary>
    /// Class to encapsulate results for Queries
    /// </summary>
    /// <remarks>
    /// This class can be use to deliver query result to client side or transport between layers
    /// </remarks>
    [DataContract]
	[Serializable]
    public partial class QueryResponse : BaseObject, IEnumerable<object>
    {
        #region Members

        public readonly Type Type;

        private IDictionary<string, string> _columnMappings;
        private object[] _data;
        private long _recordCount;
        private bool _hasTotal;
        private readonly int _hash;

        #endregion
        #region Properties

        /// <summary>
        /// Column mappings between query columns and data properties
        /// </summary>
        [DataMember]
        public IDictionary<string, string> ColumnMappings
        {
            get { return _columnMappings; }
            set { _columnMappings = value; }
        }
        /// <summary>
        /// Amount of records
        /// </summary>
        [DataMember]
        public long RecordCount 
        { 
            get
            {
                if (_recordCount < 0 && _data != null)
                    return _data.Length;

                return _recordCount;
            } 
            internal set 
            { 
                _recordCount = value;
                NotifyPropertyChanged("RecordCount");
            } 
        }
        /// <summary>
        /// Array of Data
        /// </summary>
        /// <remarks>
        /// Usually records are transported in here, but not mandatory
        /// </remarks>
        [DataMember]
        public object[] Data
        {
            get { return _data; }
            internal set 
            { 
                _data = value;
                NotifyPropertyChanged("Data");
            }
        }
        /// <summary>
        /// Indicates if totalizer are present
        /// </summary>
        [DataMember]
        public bool HasTotal
        {
            get { return _hasTotal; }
            set { _hasTotal = value; }
        }

        /// <summary>
        /// Indicate wether there is an error in the data retrieve process
        /// </summary>
        public bool IsEmpty
        {
            get { return Errors == null || Errors.Count == 0; }
        }

        #endregion
        #region Constructors

        private QueryResponse(Type type, object[] data, long recordCount, int hash = 0)
        {
            Type = type;
            _data = new object[0];
            _columnMappings = new Dictionary<string, string>();

            if (type != null && type != typeof(object))
            {
                var accessors = type.PropAccessors();
                foreach (var accessor in accessors)
                {
                    var mappingColumn = accessor.Value.GetAttributes<MappingColumn>().FirstOrDefault();
                    if (mappingColumn != null)
                        _columnMappings.Add(accessor.Key, mappingColumn.Name);
                }
            }

            _data = data;
            _recordCount = recordCount;
            _hash = hash;
        }

        public QueryResponse(Type type, int hash = 0)
            : this(type, new object[0], 0L, hash)
        {
        }

        public QueryResponse(QueryResponse qr)
            : this(qr.Type, qr, qr.RecordCount)
        {
        }

        public QueryResponse(Type type, IEnumerable collection)
            : this(type, collection.Cast<object>().ToArray(), -1)
        {
        }

        public QueryResponse(Type type, IEnumerable collection, long recordCount)
            : this(type, collection.Cast<object>().ToArray(), recordCount)
        {
        }

        public QueryResponse(Type type, IEnumerable collection, int page, int pageSize, long recordCount)
            : this(type, collection.Cast<object>().Skip((page - 1) * pageSize).Take(pageSize).ToArray(), recordCount)
        {
        }

        #endregion
        #region Public Methods

        /// <summary>
        /// Clear records
        /// </summary>
        public void Clear()
        {
            _data = new object[0];
        }

        public override int GetHashCode()
        {
            return _hash;
        }

        #endregion
        #region IEnumerable<object> Members

        public IEnumerator<object> GetEnumerator()
        {
            return _data.Cast<object>().GetEnumerator();
        }

        #endregion
        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        #endregion
    }
}
