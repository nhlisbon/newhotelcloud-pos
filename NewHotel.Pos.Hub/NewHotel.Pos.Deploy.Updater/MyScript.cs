﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Pos.Deploy.Updater
{
    public class MyScript
    {
        string _name;
        string _script;

        public string Script
        {
            get { return _script; }
            set { _script = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }
}
