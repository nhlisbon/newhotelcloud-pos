﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using System.IO;
using Ionic.Zip;
using System.Diagnostics;
using System.Xml.Linq;
using System.Net;
using System.Net.Security;
using System.Threading;

namespace NewHotel.Pos.Deploy.Updater
{
    public partial class MainPage : Form
    {
        string locationPos = "";
        string cloudLocation = "";
        string webClientLocation = "";

        public MainPage()
        {
            InitializeComponent();
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
        }
        bool succed = false;

        private void MainPage_Load(object sender, EventArgs e)
        {
            BackgroundWorker bw = new BackgroundWorker();
            bw.WorkerReportsProgress = true;
            bw.DoWork += (s, ev) =>
                {
                    OpenPathInstallationRegister();
                    (s as BackgroundWorker).ReportProgress(5);
                    ReadCloudLocation();
                    (s as BackgroundWorker).ReportProgress(10);
                    ReadWebClientLocation();
                    (s as BackgroundWorker).ReportProgress(15);
                    succed = MoveAllToFolder(s as BackgroundWorker);
                };
            bw.RunWorkerCompleted += bw_RunWorkerCompleted;
            bw.ProgressChanged += bw_ProgressChanged;
            bw.RunWorkerAsync();
        }

        void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
        }

        void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (succed)
            {
                Process.Start(Path.Combine(locationPos, "NewHotel.Pos.exe"));
            }
            else
            {
                MessageBox.Show("The update failed");
            }
            this.Close();
        }

        public void OpenPathInstallationRegister()
        {
            RegistryKey root = Registry.LocalMachine.OpenSubKey("Software", RegistryKeyPermissionCheck.ReadSubTree);
            root = root.OpenSubKey("NewHotel", RegistryKeyPermissionCheck.ReadSubTree);
            root = root.OpenSubKey("Pos", RegistryKeyPermissionCheck.ReadSubTree);
            locationPos = root.GetValue("InstallationPath").ToString();
        }

        private bool MoveAllToFolder(BackgroundWorker s)
        {
            try
            {
                ZipFile zip = ZipFile.Read(Path.Combine(Application.StartupPath, "setup.bin"));
                zip.ExtractAll(Application.StartupPath, ExtractExistingFileAction.OverwriteSilently);
                zip.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            (s as BackgroundWorker).ReportProgress(35);

            string localizationDllName = "NewHotel.Localization.resources.dll";
            List<string> localizationDirs = new List<string>
            {
                "de-DE",
                "en-US",
                "es-ES",
                "fr-FR",
                "hr-HR",
                "it-IT",
                "lt-LT",
                "pt-PT",
                "ru-RU",
                "zh-CN"
            };

            foreach (var item in localizationDirs)
            {
                try
                {
                    if (!Directory.Exists(Path.Combine(locationPos, item)))
                        Directory.CreateDirectory(Path.Combine(locationPos, item));
                    File.Copy(Path.Combine(Application.StartupPath, "data", item, localizationDllName), Path.Combine(locationPos, item, localizationDllName), true);
                }
                catch
                {
                    MessageBox.Show(string.Format("{0} not found or cannot be copy", Path.Combine(locationPos, item)));
                    return false;
                }
                
            }

            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "Microsoft.VisualStudio.OLE.Interop.dll"), Path.Combine(locationPos, "libs", "Microsoft.VisualStudio.OLE.Interop.dll"), true);
            }
            catch
            {
                MessageBox.Show("Microsoft.VisualStudio.OLE.Interop.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(40);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "Microsoft.VisualStudio.ProjectAggregator.dll"), Path.Combine(locationPos, "libs", "Microsoft.VisualStudio.ProjectAggregator.dll"), true);
            }
            catch
            {
                MessageBox.Show("Microsoft.VisualStudio.ProjectAggregator.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(42);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "Microsoft.VisualStudio.Shell.dll"), Path.Combine(locationPos, "libs", "Microsoft.VisualStudio.Shell.dll"), true);
            }
            catch
            {
                MessageBox.Show("Microsoft.VisualStudio.Shell.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(44);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "Microsoft.VisualStudio.Shell.Interop.dll"), Path.Combine(locationPos, "libs", "Microsoft.VisualStudio.Shell.Interop.dll"), true);
            }
            catch
            {
                MessageBox.Show("Microsoft.VisualStudio.Shell.Interop.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(46);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "Microsoft.VisualStudio.TextManager.Interop.dll"), Path.Combine(locationPos, "libs", "Microsoft.VisualStudio.TextManager.Interop.dll"), true);
            }
            catch
            {
                MessageBox.Show("Microsoft.VisualStudio.TextManager.Interop.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(48);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "Microsoft.VisualStudio.VSHelp.dll"), Path.Combine(locationPos, "libs", "Microsoft.VisualStudio.VSHelp.dll"), true);
            }
            catch
            {
                MessageBox.Show("Microsoft.VisualStudio.VSHelp.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(50);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "NewHotel.Business.dll"), Path.Combine(locationPos, "libs", "NewHotel.Business.dll"), true);
            }
            catch
            {
                MessageBox.Show("NewHotel.Business.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(52);
            //try
            //{
            //    File.Copy(Path.Combine(Application.StartupPath, "data", "NewHotel.Common.dll"), Path.Combine(locationPos, "libs", "NewHotel.Common.dll"), true);
            //}
            //catch
            //{
            //    MessageBox.Show("NewHotel.Common.dll not found or cannot be copy");
            //    return false;
            //}
            try
            {
                File.Delete(Path.Combine(locationPos, "libs", "NewHotel.Common.dll"));
            }
            catch
            {
            }
            (s as BackgroundWorker).ReportProgress(54);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "NewHotel.Communication.Core.dll"), Path.Combine(locationPos, "libs", "NewHotel.Communication.Core.dll"), true);
            }
            catch
            {
                MessageBox.Show("NewHotel.Communication.Core.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(56);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "NewHotel.Contracts.Common.dll"), Path.Combine(locationPos, "libs", "NewHotel.Contracts.Common.dll"), true);
            }
            catch
            {
                MessageBox.Show("NewHotel.Contracts.Common.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(58);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "NewHotel.Contracts.dll"), Path.Combine(locationPos, "libs", "NewHotel.Contracts.dll"), true);
            }
            catch
            {
                MessageBox.Show("NewHotel.Contracts.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(60);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "NewHotel.Contracts.Pos.dll"), Path.Combine(locationPos, "libs", "NewHotel.Contracts.Pos.dll"), true);
            }
            catch
            {
                MessageBox.Show("NewHotel.Contracts.Pos.dll not found or cannot be copy");
                return false;
            }
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "NewHotel.Contracts.Pms.dll"), Path.Combine(locationPos, "libs", "NewHotel.Contracts.Pms.dll"), true);
            }
            catch
            {
                MessageBox.Show("NewHotel.Contracts.Pms.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(62);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "NewHotel.Core.dll"), Path.Combine(locationPos, "libs", "NewHotel.Core.dll"), true);
            }
            catch
            {
                MessageBox.Show("NewHotel.Core.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(64);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "NewHotel.DataAnnotations.dll"), Path.Combine(locationPos, "libs", "NewHotel.DataAnnotations.dll"), true);
            }
            catch
            {
                MessageBox.Show("NewHotel.DataAnnotations.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(66);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "NewHotel.Localization.dll"), Path.Combine(locationPos, "libs", "NewHotel.Localization.dll"), true);
            }
            catch
            {
                MessageBox.Show("NewHotel.Localization.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(68);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "NewHotel.Security.dll"), Path.Combine(locationPos, "libs", "NewHotel.Security.dll"), true);
            }
            catch
            {
                MessageBox.Show("NewHotel.Security.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(68);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "NewHotel.Fiscal.SAFTPT.dll"), Path.Combine(locationPos, "libs", "NewHotel.Fiscal.SAFTPT.dll"), true);
            }
            catch
            {
                MessageBox.Show("NewHotel.Fiscal.SAFTPT.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(68);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "NewHotel.WPF.Common.dll"), Path.Combine(locationPos, "libs", "NewHotel.WPF.Common.dll"), true);
            }
            catch
            {
                MessageBox.Show("NewHotel.WPF.Common.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(70);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "NewHotel.WPF.Communication.dll"), Path.Combine(locationPos, "libs", "NewHotel.WPF.Communication.dll"), true);
            }
            catch
            {
                MessageBox.Show("NewHotel.WPF.Communication.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(72);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "NewHotel.XmlManager.Pos.dll"), Path.Combine(locationPos, "libs", "NewHotel.XmlManager.Pos.dll"), true);
            }
            catch
            {
                MessageBox.Show("NewHotel.XmlManager.Pos.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(74);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "AxInterop.AcroPDFLib.dll"), Path.Combine(locationPos, "libs", "AxInterop.AcroPDFLib.dll"), true);
            }
            catch
            {
                MessageBox.Show("AxInterop.AcroPDFLib.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(75);
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "Interop.AcroPDFLib.dll"), Path.Combine(locationPos, "libs", "Interop.AcroPDFLib.dll"), true);
            }
            catch
            {
                MessageBox.Show("Interop.AcroPDFLib.dll not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(76);

            //sqlite
            try
            {
                Directory.CreateDirectory(System.IO.Path.Combine(locationPos, "libs"));
                //Directory.CreateDirectory(System.IO.Path.Combine(locationPos, "libs", "32bits"));
                //Directory.CreateDirectory(System.IO.Path.Combine(locationPos, "libs", "64bits"));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error creating directories in '" + locationPos + "'\n" + ex.Message);
            }
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "System.Data.SQLite.dll"), Path.Combine(locationPos, "System.Data.SQLite.dll"), true);
            }
            catch
            {
                MessageBox.Show("System.Data.SQLite.dll not found or cannot be copy");
                return false;
            }
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "System.Data.SQLite.Linq.dll"), Path.Combine(locationPos, "System.Data.SQLite.Linq.dll"), true);
            }
            catch
            {
                MessageBox.Show("System.Data.SQLite.Linq.dll not found or cannot be copy");
                return false;
            }
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "SQLite.Designer.dll"), Path.Combine(locationPos, "SQLite.Designer.dll"), true);
            }
            catch
            {
                MessageBox.Show("SQLite.Designer.dll not found or cannot be copy");
                return false;
            }
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "Ionic.Zip.dll"), Path.Combine(locationPos, "Ionic.Zip.dll"), true);
            }
            catch
            {
                MessageBox.Show("Ionic.Zip.dll not found or cannot be copy");
                return false;
            }
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "NewHotel.SendLocalDB.exe"), Path.Combine(locationPos, "NewHotel.SendLocalDB.exe"), true);
            }
            catch
            {
                MessageBox.Show("NewHotel.SendLocalDB.exe not found or cannot be copy");
                return false;
            }
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "NewHotel.SendMail.bat"), Path.Combine(locationPos, "NewHotel.SendMail.bat"), true);
            }
            catch
            {
                MessageBox.Show("NewHotel.SendMail.bat not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(82);

            string error = "";
            try
            {
                MergeConfig();
            }
            catch
            {
                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        Thread.Sleep(1000);
                        error = "";
                        MergeConfig();
                        break;
                    }
                    catch (Exception exx)
                    { error = exx.Message; }
                }
            }
            if (error != "")
            {
                MessageBox.Show("NewHotel.Pos.exe.config not found or cannot be copy : " + error);
                return false;
            }
            (s as BackgroundWorker).ReportProgress(92);
            try
            {
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error running sqlite scripts : " + ex.Message);
            }
            try
            {
                File.Copy(Path.Combine(Application.StartupPath, "data", "NewHotel.Pos.exe"), Path.Combine(locationPos, "NewHotel.Pos.exe"), true);
            }
            catch
            {
                MessageBox.Show("NewHotel.Pos.exe not found or cannot be copy");
                return false;
            }
            (s as BackgroundWorker).ReportProgress(100);
            return true;
        }

        private void ReadCloudLocation()
        {
            StreamReader reader = new StreamReader(Path.Combine(locationPos, "NewHotel.Pos.exe.config"));
            XDocument doc = XDocument.Load(reader);
            var list = doc.Root.Descendants("add");
            var item = list.FirstOrDefault(x => x.Attribute("key") != null && x.Attribute("key").Value == "CloudLocation");
            if (item != null) cloudLocation = item.Attribute("value").Value;
        }

        private void ReadWebClientLocation()
        {
            StreamReader reader = new StreamReader(Path.Combine(locationPos, "NewHotel.Pos.exe.config"));
            XDocument doc = XDocument.Load(reader);
            var list = doc.Root.Descendants("add");
            var item = list.FirstOrDefault(x => x.Attribute("key") != null && x.Attribute("key").Value == "WebClientLocation");
            if (item != null) webClientLocation = item.Attribute("value").Value;
        }

        private void MergeConfig()
        {
            StreamReader reader = null, reader1 = null;
            string oldConfig, finalConfig, newConfig;
            try
            {
                reader = new StreamReader(Path.Combine(locationPos, "NewHotel.Pos.exe.config"));
                oldConfig = reader.ReadToEnd();
                finalConfig = "";
            }
            finally
            {
                reader.Close();
            }
            try
            {
                reader1 = new StreamReader(Path.Combine(Application.StartupPath, "data", "NewHotel.Pos.exe.config"));
                newConfig = reader1.ReadToEnd();
            }
            finally
            {
                reader1.Close();
            }

            newConfig = newConfig.Replace(@"C:\NewHotelPos", Path.Combine(locationPos, "LocalData"));
            newConfig = newConfig.Replace("CloudLocationValue", cloudLocation);
            newConfig = newConfig.Replace("WebClientLocationValue", webClientLocation);
            string[] keys = oldConfig.Split(new string[] { "<add key=", "</appSettings>" }, StringSplitOptions.None);
            //Copiar las configuracones que ya existan
            if (keys.Length > 4)
            {
                string[] newConfigParts = newConfig.Split(new string[] { "</appSettings>" }, StringSplitOptions.None);
                finalConfig += newConfigParts[0];
                for (int i = 4; i < keys.Length - 1; i++)
                {
                    string plus = "<add key=" + keys[i];
                    finalConfig += plus;
                }
                finalConfig += "</appSettings>";
                finalConfig += newConfigParts[1];
            }
            else
            {
                finalConfig = newConfig;
            }
            StreamWriter sw = null;
            try
            {
                Thread.Sleep(1000);
                sw = new StreamWriter(Path.Combine(locationPos, "NewHotel.Pos.exe.config"), false);
                sw.Write(finalConfig);
            }
            finally
            {
                sw.Close();
            }
        }
    }
}
