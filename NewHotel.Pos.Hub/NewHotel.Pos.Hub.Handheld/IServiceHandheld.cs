﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using NewHotel.Pos.Hub.Common;
using NewHotel.Pos.Hub.Contracts.Common.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.Records;
using NewHotel.Pos.Hub.Contracts.Common.Records.CurrentAccount;
using NewHotel.Pos.Hub.Contracts.Common.Records.Stands;
using NewHotel.Pos.Hub.Contracts.Handheld.Contracts;
using NewHotel.Pos.Hub.Contracts.Handheld.DTOs;
using NewHotel.Pos.Hub.Contracts.Handheld.DTOs.Requests;
using NewHotel.Pos.Hub.Contracts.Handheld.DTOs.Responses;
using NewHotel.Pos.Hub.Model;
using HotelRecord = NewHotel.Pos.Hub.Model.HotelRecord;
using UserRecord = NewHotel.Pos.Hub.Model.UserRecord;

namespace NewHotel.Pos.Hub.Handheld
{
    [ServiceContract]
    public interface IServiceHandheld : ICommonService
    {
        #region Handheld

        /// <summary>
        /// Get User from user and password
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <returns>User ID if founded</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/User/{username}&{password}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<UserRecord> GetUserByName(string username, string password);

        /// <summary>
        /// Get User from quick access code
        /// </summary>
        /// <param name="code">Quick access code</param>
        /// <returns>User ID if founded</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/User/{code}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<UserRecord> GetUserByCode(string code);

        /// <summary>
        /// Get Hotels by User
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <returns>List of available hotels</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetHotels/{userId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<HotelRecord> GetHotels(string userId);

        /// <summary>
        /// Get the Cashiers available to provided user and hotel
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="installationId">Hotel Id</param>
        /// <param name="language">Language code (ex: 1033)</param>
        /// <returns>List of cashiers</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetCashiers/{userId}&{installationId}&{language}", Method = "GET",
            BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<CashierRecord> GetCashiers(string userId, string installationId, string language);

        /// <summary>
        /// Get the Stands available to provided cashier
        /// </summary>
        /// <param name="cashierId">Cashier Id</param>
        /// <param name="language">Language code (ex: 1033)</param>
        /// <returns>List of stands</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetStands/{cashierId}&{language}", Method = "GET",
            BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<POSStandRecord> GetStands(string cashierId, string language);

        [OperationContract]
        [WebInvoke(UriTemplate = "/GetCashiersWithStands/{userId}&{installationId}&{language}", Method = "GET",
            BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<CashierWithStandsRecord> GetCashiersWithStands(string userId, string installationId, string language);

        /// <summary>
        /// Create the Token to be used on further communications
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="installationId">Hotel Id</param>
        /// <returns>Token</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/SetSessionToken", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<Guid> SetSessionToken(string userId, string installationId);

        /// <summary>
        /// Set Context Data with work information
        /// </summary>
        /// <returns>WorkDate</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/SetSessionData", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationStringResult SetSessionData(string cashierId, string standId, string language);

        /// <summary>
        /// Loads full environment of the Stand / Cashier (All relevant information)
        /// </summary>
        /// <param name="standId">Stand Id</param>
        /// <returns>Stand Environment</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetStandEnvironment/{standId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<StandEnvironment> GetStandEnvironment(string standId);

        /// <summary>
        /// Loads all settings for POS operations
        /// </summary>
        /// <returns>General Settings</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetGeneralSettings", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<POSGeneralSettingsRecord> GetGeneralSettings();

        /// <summary>
        /// Get list of open tables in a stand
        /// </summary>
        /// <param name="standId">Stand Id</param>
        /// <returns>List of Opened tables</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetOpenTables/{standId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<Guid> GetOpenTables(string standId);

        /// <summary>
        /// Get list of open tables in a stand / saloon
        /// </summary>
        /// <param name="standId">Stand Id</param>
        /// /// <param name="saloonId">Saloon Id</param>
        /// <returns>List of Opened tables</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetOpenTablesBySaloon/{standId}&{saloonId}", Method = "GET",
            BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<Guid> GetOpenTablesBySaloon(string standId, string saloonId);

        /// <summary>
        /// Get list of tables in saloon
        /// </summary>
        /// <param name="saloonId">Saloon Id</param>
        /// <returns>List of tables in Saloon</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetTablesBySaloon/{saloonId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<TableRecord> GetTablesBySaloon(string saloonId);

        /// <summary>
        /// Load Ticket for Edition (mark it as opened)
        /// </summary>
        /// <param name="ticketId">Stand Id</param>
        /// /// <param name="forceOpen">Open ticket even if another cashier opened as well</param>
        /// <returns>Ticket</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/LoadTicketForEdition/{ticketId}&{forceOpen}", Method = "GET",
            BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult LoadTicketForEdition(string ticketId, string forceOpen);

        /// <summary>
        /// Save Ticket from Edition (mark it as not opened)
        /// </summary>
        /// <param name="ticketId">Stand Id</param>
        /// <returns>Ticket</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/CloseTicketFromEdition", Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult CloseTicketFromEdition(string ticketId);

        /// <summary>
        /// Load opened tickets
        /// </summary>
        /// <returns>List with all tickets</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetOpenTickets", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<TicketInfo> GetOpenTickets();

        /// <summary>
        /// Load opened tickets by Table
        /// </summary>
        /// <returns>List with all tickets</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetOpenTicketsByTable/{tableId}", Method = "GET",
            BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<TicketInfo> GetOpenTicketsByTable(string tableId);

        /// <summary>
        /// Get Dispatch Areas
        /// </summary>
        /// <returns>List if will Areas</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetAreas", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<AreasRecord> GetAreas();

        /// <summary>
        /// Get Dispatch Areas for each stand
        /// </summary>
        /// <returns>List if will Areas</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetStandAreas", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<StandAreaRecord> GetStandAreas();

        /// <summary>
        /// Get all stands 
        /// </summary>
        /// <returns>List with all stands</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetAllStands", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<LiteStandRecord> GetAllStands();

        /// <summary>
        /// Void Ticket (cancel full ticket)
        /// </summary>
        /// <param name="ticketId">Id of the ticket</param>
        /// <param name="cancelReasonId">Id of the cancellation reason</param>
        /// <param name="comment">Additional comments</param>
        /// <param name="fromMerge">Cancel ticket because merge operation took place</param>
        /// <returns>Empty if cancel was possible</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/VoidTicket", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationResult VoidTicket(string ticketId, string cancelReasonId, string comment, string fromMerge);


        /// <summary>
        /// Dispatch Order
        /// </summary>
        /// <param name="ticketId">Id of the ticket</param>
        /// <param name="orders">List of order / area pair</param>
        /// <returns>Ticket with order's dispatch void</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/DispatchOrders", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult DispatchOrders(string ticketId, DispatchContract[] orders);


        /// <summary>
        /// Void Dispatch Order 
        /// </summary>
        /// <param name="ticketId">Id of the ticket</param>
        /// <param name="orders">List of orders</param>
        /// <returns>Ticket with order's dispatch void</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/VoidDispatchOrders", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult VoidDispatchOrders(string ticketId, DispatchContract[] orders);

        /// <summary>
        /// Void Dispatch Order 
        /// </summary>
        /// <param name="ticketId">Id of the ticket</param>
        /// <param name="orders">List of orders</param>
        /// <returns>Ticket with order's dispatch void</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/AwayDispatchOrders", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult AwayDispatchOrders(string ticketId, DispatchContract[] orders);

        /// <summary>
        /// Create a brand new Ticket
        /// </summary>
        /// <param name="productId">Id of the product</param>
        /// <param name="quantity">Quantity of the product</param>
        /// <param name="manualPrice">Manual price of the product</param>
        /// <param name="manualPriceDescription">Description of the manual price of the product</param>
        /// <param name="tableId">Table if available, empty otherwise</param>
        /// <param name="selectedDetailed">Table products selected</param>
        /// <param name="preparations">List of preparation ids</param>
        /// <param name="digitalMenu">If ticket creation come from menu digital</param>
        /// <param name="paxs"></param>
        /// <param name="client">Main client to add to the ticket</param>
        /// <param name="seat">Seat number</param>
        /// <param name="separatorId">Separator id</param>
        /// <param name="description"></param>
        /// <param name="seatedClients">The clients that were given a seat in the table</param>
        /// <returns>Created Ticket</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/CreateTicket", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult CreateTicket(
            string productId,
            string quantity,
            string manualPrice,
            string manualPriceDescription,
            string tableId,
            string[] selectedDetailed,
            string[] preparations,
            string description,
            short? paxs,
            ClientRecord? client,
            bool? digitalMenu,
            short? seat,
            string separatorId,
            TypedList<POSClientByPositionContract> seatedClients,
            string observations = null
        );
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/NewTicket", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult NewTicket(NewTicketDto request);

        /// <summary>
        /// Adds a product to a ticket
        /// </summary>
        /// <param name="ticketId">Id of the ticket</param>
        /// <param name="productId">Id of the product</param>
        /// <param name="quantity">Quantity of the product</param>
        /// <param name="manualPrice">Manual price of the product</param>
        /// <param name="manualPriceDescription">Description of the manual price give to the product</param>
        /// <param name="selectedDetailed">Table products selected</param>
        /// <param name="preparations">List of preparation ids</param>
        /// <param name="seat">Seat number</param>
        /// <param name="separatorId">Separator id</param>
        /// <returns>Ticket with line added</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/AddProductToTicket", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult AddProductToTicket(
            string ticketId,
            string productId,
            string quantity,
            string manualPrice,
            string manualPriceDescription,
            string[] selectedDetailed,
            string[] preparations,
            short? seat,
            string separatorId = null,
            string observations = null
        );

        /// <summary>
        /// Adds a product to a ticket by code
        /// </summary>
        /// <param name="ticketId">Id of the ticket</param>
        /// <param name="productCode">Code of the product</param>
        /// <param name="quantity">Quantity of the product</param>
        /// <param name="preparations">List of preparation ids</param>
        /// <returns>Ticket with line added</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/AddProductToTicketByPLU", Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult AddProductToTicketByPlu(string ticketId, string productCode, string quantity,
            string[] preparations);

        /// <summary>
        /// Cancel Product Line
        /// </summary>
        /// <returns>Ticket with line cancelled</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/CancelProductLine", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult CancelProductLine(string ticketId, string productLineId);

        /// <summary>
        /// Change Order Quantity
        /// </summary>
        /// <returns>Ticket with line increased / decreased</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/UpdateOrderQuantity", Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult UpdateOrderQuantity(string ticketId, string productLineId, string quantity);

        /// <summary>
        /// Add Order Preparation
        /// </summary>
        /// <returns>Ticket with preparation added</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/AddOrderPreparation", Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult AddOrderPreparation(string ticketId, string productLineId, string preparationId);

        /// <summary>
        /// Remove Order Preparation
        /// </summary>
        /// <returns>Ticket with preparation removed</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/RemoveOrderPreparation", Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult RemoveOrderPreparation(string ticketId, string productLineId, string preparationId);

        /// <summary>
        /// Update Order Preparation
        /// </summary>
        /// <returns>Ticket with preparation updated</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/UpdateOrderPreparation", Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult UpdateOrderPreparation(string ticketId, string productLineId, string[] preparationsId);

        /// <summary>
        /// add order comment
        /// </summary>
        /// <returns>Ticket with comment added</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/AddOrderComment", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult AddOrderComment(string ticketId, string productLineId, string comment);

        /// <summary>
        /// Set Table To Ticket
        /// </summary>
        /// <returns>Ticket with table set</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/SetTableToTicket", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult SetTableToTicket(string ticketId, string tableId);

        /// <summary>
        /// Clear Table From Ticket
        /// </summary>
        /// <returns>Ticket without table set</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/ClearTableFromTicket", Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult ClearTableFromTicket(string ticketId);

        /// <summary>
        /// Get All Reservation Info
        /// </summary>
        /// <returns>Get Reservations</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetReservations", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Obsolete("This method is deprecated, please use 'SearchReservations' instead.")]
        ValidationItemSource<ReservationRecord> GetReservations();
        
        /// <summary>
        /// Get Filtered Reservation Info
        /// </summary>
        /// <returns>Get Reservations</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SearchReservations", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<ReservationRecord> SearchReservations(ReservationFilterContract filter);

        [OperationContract]
        [WebInvoke(UriTemplate = "/GetPmsReservation", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<PmsReservationRecord> GetPmsReservation(PmsReservationFilterModel request);

        /// <summary>
        /// Get Filtered SPA Reservation Info
        /// </summary>
        /// <returns>Get SPA Reservations</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "SearchSpaReservations", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<SpaReservationSearchFromExternalRecord> SearchSpaReservations(SpaSearchReservationFilterModel filter);

        /// <summary>
        /// Gets a list of house uses
        /// </summary>
        /// <returns>A list of house use records</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetHouseUses/{standId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, 
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<HouseUseRecord> GetHouseUses(string standId);

        // /// <summary>
        // /// Add a Room Plan payment
        // ///
        // /// </summary>
        // /// <returns>A ticket contract</returns>
        // [Obsolete("Once the ValidateTicketPayments method gets done, remove it")]
        // [OperationContract]
        // [WebInvoke(UriTemplate = "/AddRoomPlanPayment", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
        //     RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        // ValidationContractResult AddRoomPlanPayment(Guid ticketId, ReservationSearchFromExternalRecord item);

        // /// <summary>
        // /// Add a Credit payment
        // /// </summary>
        // /// <param name="ticketId">Ticket id</param>
        // /// <param name="amount">Payment amount</param>
        // /// <param name="item">Reservation info</param>
        // /// <returns>A ticket contract</returns>
        // [Obsolete("Once the ValidateTicketPayments method gets done, remove it")]
        // [OperationContract]
        // [WebInvoke(UriTemplate = "/AddCreditPayment", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
        //     RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        // ValidationContractResult AddCreditPayment(Guid ticketId, decimal? amount, ReservationSearchFromExternalRecord item);
        
        // /// <summary>
        // /// Add a House Use payment
        // /// </summary>
        // /// <param name="ticketId">The id of the ticket</param>
        // /// <param name="item">The reservation object to be added</param>
        // /// <returns>A ticket contract</returns>
        // [Obsolete("Once the ValidateTicketPayments method gets done, remove it")]
        // [OperationContract]
        // [WebInvoke(UriTemplate = "/AddHouseUsePayment", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
        //     RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        // ValidationContractResult AddHouseUsePayment(Guid ticketId, HouseUseRecord item);

        // /// <summary>
        // /// Adds a Payment to a Ticket
        // /// </summary>
        // /// <returns>Ticket with payment added</returns>
        // [Obsolete("Once the ValidateTicketPayments method gets done, remove it")]
        // [OperationContract]
        // [WebInvoke(UriTemplate = "/AddPaymentToTicket", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
        //     RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        // ValidationContractResult AddPaymentToTicket(string ticketId, string paymentId, string creditCardId,
        //     string received, string value, string excessPaymentAsTip, string close);

        // /// <summary>
        // /// Remove all payments from Ticket
        // /// </summary>
        // /// <returns>Ticket with payments removed</returns>
        // [Obsolete("Once the ValidateTicketPayments method gets done, remove it")]
        // [OperationContract]
        // [WebInvoke(UriTemplate = "/ResetPaymentTickets", Method = "POST",
        //     BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
        //     ResponseFormat = WebMessageFormat.Json)]
        // ValidationContractResult ResetPaymentTickets(string ticketId);

        /// <summary>
        /// Close Ticket
        /// </summary>
        /// <returns>Closed Ticket</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/CloseTicket", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationStringResult CloseTicket(Guid ticketId, ClientRecord client, PersonalDocType docTypeSelected,
            bool saveClient, string signature = null);

        /// <summary>
        /// Close Ticket with fast payment method
        /// </summary>
        /// <returns>Closed Ticket</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/FastCloseTicket", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationStringResult FastCloseTicket(string ticketId);

        /// <summary>
        /// Close Ticket as invoice with fast payment method 
        /// </summary>
        [OperationContract]
        [WebInvoke(UriTemplate = "/FastCloseTicketAsInvoice", Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationStringResult FastCloseTicketAsInvoice(string ticketId, ClientRecord client, string docTypeSelected,
            bool saveClient);

        /// <summary>
        /// Close Ticket as invoice
        /// </summary>
        [OperationContract]
        [WebInvoke(UriTemplate = "/CloseTicketAsInvoice", Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationStringResult CloseTicketAsInvoice(string ticketId, ClientRecord client, string docTypeSelected,
            bool saveClient, string signature = null);

        /// <summary>
        /// Get Image based on Image Path
        /// </summary>
        /// <returns>Image on Base64</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/LoadImage", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationStringResult LoadImage(string imagePath);

        /// <summary>
        /// Print Table Ticket
        /// </summary>
        /// <returns>Ticket with Lookup Table Information</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/PrintTable", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult PrintTable(string ticketId);

        /// <summary>
        /// Get clients to fill invoice data
        /// </summary>
        /// <param name="client">Client record to search</param>
        /// <returns>Get Reservations</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetClientEntities", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<ClientRecord> GetClientEntities(ClientRecord client);

        /// <summary>
        /// Update ticket tip
        /// </summary>      
        /// <param name="ticketId">Ticket id</param> 
        /// <param name="tipPercent">Percent used to calculate the tip value</param>
        /// <param name="tipValue">Tip value calculated</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/UpdateTicketTip", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult UpdateTicketTip(string ticketId, decimal? tipPercent, decimal? tipValue);

        /// <summary>
        /// Apply or clear a discount to a list of products from a ticket
        /// </summary>      
        /// <param name="ticketId">Ticket id</param> 
        /// <param name="productsId">Products id to apply or clear the discount </param>
        /// <param name="discountTypeId">Type of discount</param>
        /// <param name="discountPercent">Percent to apply</param>
        /// <returns>Ticket with discounts applied</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/ApplyTicketDiscounts", Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult ApplyTicketDiscounts(string ticketId, string[] productsId, string discountTypeId,
            decimal? discountPercent);

        /// <summary>
        /// Create a new ticket with a list of products (NONIUS)
        /// </summary>
        /// <param name="room">Room number</param>
        /// <param name="external">Some description</param>
        /// <param name="productLines">List of product lines</param>
        /// <returns>A Contract Ticket</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/CreateTicketRoom", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult CreateTicketRoom(string room, string external, ProductLineDto[] productLines);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/SplitTicketByAlcoholicGroup/{ticketId}", Method = "GET",
            BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        SplitTicketResult SplitTicketByAlcoholicGroup(string ticketId);

        /// <summary>
        /// Change the separator to a list of products lines
        /// </summary>
        /// <param name="ticketId">The thicket id</param>
        /// <param name="productLineIds">The ids of the product lines</param>
        /// <param name="separatorId">The separator id</param>
        /// <returns>A Contract Ticket</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/ChangeProductLinesSeparator", Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult ChangeProductLinesSeparator(string ticketId, string[] productLineIds,
            string separatorId);

        /// <summary>
        /// Change the seat to a list of products lines
        /// </summary>
        /// <param name="ticketId">The thicket id</param>
        /// <param name="productLineIds">The ids of the product lines</param>
        /// <param name="seat">The seat number</param>
        /// <returns>A Contract Ticket</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/ChangeProductLinesSeat", Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult ChangeProductLinesSeat(string ticketId, string[] productLineIds, short seat);

        [OperationContract]
        [WebInvoke(UriTemplate = "/GetTableReservations/{state}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<LiteTablesReservationRecord> GetTableReservations(string state);

        [OperationContract]
        [WebInvoke(UriTemplate = "/CreateEmptyTicketToReservationTable", Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult CreateEmptyTicketToReservationTable(string[] tableIds, string reservationTableId, string name);
        

        [OperationContract]
        [WebInvoke(UriTemplate = "/CancelTableReservation", Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationResult CancelTableReservation(string id, string cancellationReasonId, string comments);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ClientInstallations/{openAccounts}&{clientId}&{room}&{fullName}", Method = "GET",
            BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<ClientInstallationRecord> GetClientInstallations(string openAccounts, string clientId, string room, string fullName);

        [OperationContract]
        [WebInvoke(UriTemplate = "/AddClientPositionToTicket", Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult AddClientPositionToTicket(string ticketId, short pax, ClientInstallationRecord client);

        [OperationContract]
        [WebInvoke(UriTemplate = "/RemoveClientPositionFromTicket", Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult RemoveClientPositionFromTicket(string ticketId, short pax);

        /// <summary>
        /// Get a list of pending orders in a stand
        /// </summary>
        /// <returns>List of tables with pending orders</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetPendingOrdersTables", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<Guid> GetPendingOrdersTables();

        /// <summary>
        /// Create a new ticket with a list of products
        /// </summary>
        /// <param name="tableId">Table id</param>
        /// <returns>A Contract Ticket</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/AttendTableOrder", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult AttendTableOrder(Guid tableId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/GetProductPrices/{standId}&{rateId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<ProductPriceRecord> GetProductPrices(string standId, string rateId);

        [OperationContract]
        [WebInvoke(UriTemplate = "/AddGuestToSeat", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult AddGuestToSeat(string ticketId, GuestContract guest, short seat);

        [OperationContract]
        [WebInvoke(UriTemplate = "/GetClientInfo/{clientId}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<ClientInfoContract> GetClientInfo(string clientId);

        /// <summary>
        /// Update order preparations
        /// </summary>
        /// <param name="ticketId">Id of the ticket</param>
        /// <param name="contract"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/UpdateOrderPreparations", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult UpdateOrderPreparations(string ticketId, ProductLinePreparationContract contract);

        /// <summary>
        /// Update order note
        /// </summary>
        /// <param name="ticketId">Id of the ticket</param>
        /// <param name="contract"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/UpdateOrderNote", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult UpdateOrderNote(string ticketId, ProductLineNoteContract contract);

        /// <summary>
        /// Update order note
        /// </summary>
        /// <param name="ticketId">Id of the ticket</param>
        /// <param name="pax"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/UpdateTicketInfo", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult UpdateTicketInfo(string ticketId, short pax, string description);

        /// <summary>
        /// Add reservation to ticket
        /// </summary>
        /// <param name="ticketId">Ticket id</param>
        /// <param name="reservation">Reservation info</param>
        /// <returns>A ticket contract</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/AddReservationToTicket", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult AddReservationToTicket(Guid ticketId, ReservationRecord reservation);

        /// <summary>
        /// Add reservation to ticket
        /// </summary>
        /// <param name="ticketId">Ticket id</param>
        /// <param name="reservation">Reservation info</param>
        /// <returns>A ticket contract</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/AddSpaReservationToTicket", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult AddSpaReservationToTicket(Guid ticketId, SpaReservationRecord reservation);

        [OperationContract]
        [WebInvoke(UriTemplate = "/UpdateClientsByPosition", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult UpdateClientsByPosition(Guid ticketId, IEnumerable<POSClientByPositionContract> clients);


        [OperationContract]
        [WebInvoke(UriTemplate = "/ControlAccounts", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<PmsControlAccountRecord> GetControlAccounts(PmsControlAccountFilterModel filter);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/ControlAccountMovements", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<ControlAccountMovementDto> GetControlAccountMovements(ControlAccountMovementsFilterModel filter);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ValidateTicketPayments", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<AddPaymentResponse> ValidateTicketPayments(AddPaymentsRequest request);

        /// <summary>
        /// Close Ticket as invoice
        /// </summary>
        [OperationContract]
        [WebInvoke(UriTemplate = "/NewCloseTicket", Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<CloseTicketResponse> NewCloseTicket(CloseTicketRequest request);

        /// <summary>
        /// Get list of open tables in a stand
        /// </summary>
        /// <param name="standId">Stand Id</param>
        /// <returns>List of Opened tables</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetTablesInfo", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<StandTableInfoDto> GetTablesInfo();

        /// <summary>
        /// Get list of spa services by a room
        /// </summary>
        /// <param name="room">Room number</param>
        /// <returns>List of SPA services</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetSpaServices/{room}", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<SpaServiceRecord> GetSpaServices(string room);

        #endregion

        #region Digital Menu

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hotelId">Hotel Id</param>
        /// <param name="standId">Stand Id</param>
        /// <param name="tableId"></param>
        /// <param name="langId">Language Id</param>
        /// <param name="saloonId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/Menu/GetEnvironment/{hotelId}/{standId}/{saloonId}/{tableId}/{langId}",
            Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationItemResult<MenuEnvDto> GetMenuStandEnvironment(string hotelId, string standId, string saloonId,
            string tableId, string langId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hotelId"></param>
        /// <param name="standId"></param>
        /// <param name="saloonId"></param>
        /// <param name="tableId"></param>
        /// <param name="langId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/Menu/GetOrders/{hotelId}/{standId}/{saloonId}/{tableId}/{langId}", Method = "GET",
            BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<TicketDto> GetMenuOrders(string hotelId, string standId, string saloonId, string tableId,
            string langId);

        /// <summary>
        /// Create a new ticket with a list of products
        /// </summary>
        /// <param name="tableId">Table if available, empty otherwise</param>
        /// <param name="productLines">List of product lines</param>
        /// <returns>A Contract Ticket</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/CreateMenuDigitalTicket", Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        ValidationContractResult CreateMenuDigitalTicket(Guid tableId, ProductLineDto[] productLines);

        /// <summary>
        /// Add a menu order
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/Menu/AddOrder", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<TicketDto> AddMenuOrder(MenuDataDto data);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/Menu/UpdateOrder", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<TicketDto> UpdateMenuOrder(MenuDataDto data);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/Menu/DeleteOrder", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<TicketDto> DeleteMenuOrder(MenuDataDto data);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/Menu/PlaceOrder", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<TicketDto> PlaceMenuOrder(MenuDataDto data);

        #endregion

        #region Signature Requests

        /// <summary>
        /// Get list of tickets that a signature was request
        /// </summary>
        /// <returns>List of tickets info</returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetSignatureRequests", Method = "GET", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<SignatureRequestDto> GetSignatureRequests();
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/ProcessSignature", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ValidationItemSource<SignatureRequestDto> ProcessSignature(string ticketId, string signature);

        #endregion
    }
}