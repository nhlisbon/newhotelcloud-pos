﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using NewHotel.Contracts;
using NewHotel.Pos.Hub.Common;
using NewHotel.Core;
using NewHotel.Pos.Hub.Model;
using NewHotel.Contracts.Pos.Records;
using NewHotel.Pos.Hub.Core;
using NewHotel.DataAnnotations;
using NewHotel.Pos.Communication.Behavior;
using NewHotel.Pos.Hub.Business.Business;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.IoC;
using NewHotel.Manager.Oracle;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Core;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Management;
using NewHotel.Pos.Hub.Contracts.Handheld.Contracts;
using NewHotel.Pos.Hub.Contracts.Handheld.DTOs;
using NewHotel.Pos.Hub.Model.WaitingListStand.Contract;
using NewHotel.Pos.PrinterDocument.Interfaces;
using NewHotel.Pos.PrinterDocument.Simple;
using NewHotel.Pos.Localization;
using NewHotel.Business;
using NewHotel.Pos.Hub.Contracts.Common.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.Records;
using NewHotel.Pos.Hub.Contracts.Common.Records.CurrentAccount;
using NewHotel.Pos.Hub.Contracts.Common.Records.Stands;
using NewHotel.Pos.Hub.Contracts.Handheld.DTOs.Requests;
using NewHotel.Pos.Hub.Contracts.Handheld.DTOs.Responses;
using NewHotel.Pos.Hub.Core.Logs;

namespace NewHotel.Pos.Hub.Handheld
{
    public class ServiceHandheld : CommonService, IServiceHandheld
    {
        private IServiceHandheld _serviceHandheldImplementation;

        #region Methodos

        #region Get Initial Information

        public ValidationItemResult<Model.UserRecord> GetUserByName(string username, string password)
        {
            var result = new ValidationItemResult<Model.UserRecord>();
            try
            {
                using var business = GetBusiness<IStandBusiness>();
                result = business.GetUserByName(username);
                if (result.Item == null)
                    result.Add(new ValidationResult(
                        new UnauthorizedAccessException("Username and/or password invalid")));
                //else if (result.Item.UserPassword != CryptUtils.Encrypt(password))
                else if (!result.Item.UserPassword.VerifyHashedPassword(password))
                    result.Add(new ValidationResult(
                        new UnauthorizedAccessException("Username and/or password invalid")));
            }
            catch (Exception ex)
            {
                result.AddError(ex.Message);
            }

            return result;
        }

        public ValidationItemResult<Model.UserRecord> GetUserByCode(string code)
        {
            var result = new ValidationItemResult<Model.UserRecord>();

            try
            {
                using var business = GetBusiness<IStandBusiness>();
                result = business.GetUserByCode(code);
                if (result.Item == null)
                {
                    result.Add(new ValidationResult(
                        new UnauthorizedAccessException("Username and/or password invalid")));
                }
            }
            catch (Exception ex)
            {
                result.AddError(ex.Message);
            }

            return result;
        }

        public ValidationItemSource<Model.HotelRecord> GetHotels(string userId)
        {
            var result = new ValidationItemSource<Model.HotelRecord>();

            try
            {
                using var business = GetBusiness<ISettingBusiness>();
                var hotels = business.GetHotels(userId.AsGuid());
                result.ItemSource = hotels;
                return result;
            }
            catch (Exception ex)
            {
                return new ValidationItemSource<Model.HotelRecord>(ex);
            }
        }

        public ValidationItemSource<CashierRecord> GetCashiers(string userId, string installationId, string language)
        {
            var result = new ValidationItemSource<CashierRecord>();

            try
            {
                using var business = GetBusiness<IStandBusiness>();
                var cashiers = business.GetCashiers(installationId.AsGuid(), userId.AsGuid(),
                    language.AsInteger());
                result.ItemSource = cashiers;
                return result;
            }
            catch (Exception ex)
            {
                return new ValidationItemSource<CashierRecord>(ex);
            }
        }

        public ValidationItemSource<POSStandRecord> GetStands(string cashierId, string language)
        {
            var result = new ValidationItemSource<POSStandRecord>();

            try
            {
                using var business = GetBusiness<IStandBusiness>();
                var stands = business.GetStands(cashierId.AsGuid(), language.AsInteger());
                result.ItemSource = stands;
                return result;
            }
            catch (Exception ex)
            {
                return new ValidationItemSource<POSStandRecord>(ex);
            }
        }

        public ValidationItemSource<CashierWithStandsRecord> GetCashiersWithStands(string userId, string installationId,
            string language)
        {
            var result = new ValidationItemSource<CashierWithStandsRecord>();

            try
            {
                using var business = GetBusiness<IStandBusiness>();
                var cashiers = business.GetCashiers(installationId.AsGuid(), userId.AsGuid(), language.AsInteger());
                var cs = cashiers.Select(cashier => new CashierWithStandsRecord
                    { Cashier = cashier, Stands = business.GetStands(cashier.Id, language.AsInteger()) }).ToList();
                result.ItemSource = cs;
                return result;
            }
            catch (Exception ex)
            {
                return new ValidationItemSource<CashierWithStandsRecord>(ex);
            }
        }

        public ValidationItemResult<Guid> SetSessionToken(string userId, string installationId)
        {
            var result = new ValidationItemResult<Guid>();
            try
            {
                var token = CreateSessionToken(installationId, userId);
                result.Item = token;
                if (token == Guid.Empty)
                    result.AddError("User does not exist");
            }
            catch (Exception ex)
            {
                result.Add(new ValidationResult(ex));
            }

            return result;
        }

        public ValidationStringResult SetSessionData(string cashierId, string standId, string language)
        {
            var result = new ValidationStringResult();
            try
            {
                var token = SetTokenContextData(standId, cashierId, Guid.Empty.ToString(), language);
                result.Description = token;
            }
            catch (Exception ex)
            {
                result.AddException(ex);
            }

            return result;
        }

        public ValidationItemResult<StandEnvironment> GetStandEnvironment(string standId)
        {
            using var business = GetBusiness<IStandBusiness>();
            var initial = DateTime.Now;
            var result = business.GetStandEnvironment(standId.AsGuid(), (int)BusinessContext.LanguageId, true);
            Console.WriteLine((DateTime.Now - initial).TotalMilliseconds.ToString(CultureInfo.CurrentCulture));
            return result;
        }

        public ValidationItemSource<AreasRecord> GetAreas()
        {
            var result = new ValidationItemSource<AreasRecord>();

            try
            {
                var business = GetBusiness<IProductBusiness>();
                var areas = business.GetAreasByStand(Guid.Empty, (int)BusinessContext.LanguageId);
                result.ItemSource = areas;
                return result;
            }
            catch (Exception ex)
            {
                return new ValidationItemSource<Model.AreasRecord>(ex);
            }
        }

        public ValidationItemSource<StandAreaRecord> GetStandAreas()
        {
            var result = new ValidationItemSource<StandAreaRecord>();

            try
            {
                var business = GetBusiness<IProductBusiness>();
                var areas = business.GetStandAreas((int)BusinessContext.LanguageId);
                result.ItemSource = areas;
                return result;
            }
            catch (Exception ex)
            {
                return new ValidationItemSource<StandAreaRecord>(ex);
            }
        }

        public ValidationItemSource<LiteStandRecord> GetAllStands()
        {
            var result = new ValidationItemSource<LiteStandRecord>();

            try
            {
                var business = GetBusiness<IStandBusiness>();
                var stands = business.GetStandsForTransfer((int)BusinessContext.LanguageId);
                result.ItemSource = stands;
                return result;
            }
            catch (Exception ex)
            {
                return new ValidationItemSource<LiteStandRecord>(ex);
            }
        }

        public ValidationStringResult LoadImage(string imagePath)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.LoadImage(imagePath);
        }

        public ValidationItemResult<POSGeneralSettingsRecord> GetGeneralSettings()
        {
            using (var business = GetBusiness<ISettingBusiness>())
            {
                try
                {
                    var record = business.GetGeneralSettings(BusinessContext.InstallationId);
                    var result = new ValidationItemResult<POSGeneralSettingsRecord>();
                    result.Item = record;
                    return result;
                }
                catch (Exception ex)
                {
                    return new ValidationItemResult<POSGeneralSettingsRecord>(ex);
                }
            }
        }

        #endregion

        #region Get Table Information

        public ValidationItemSource<Guid> GetOpenTables(string standId)
        {
            using (var business = GetBusiness<IStandBusiness>())
            {
                return business.GetOpenTables(standId.AsGuid(), null);
            }
        }

        public ValidationItemSource<Guid> GetOpenTablesBySaloon(string standId, string saloonId)
        {
            using (var business = GetBusiness<IStandBusiness>())
            {
                return business.GetOpenTables(standId.AsGuid(), saloonId.AsGuid());
            }
        }

        public ValidationItemSource<TableRecord> GetTablesBySaloon(string saloonId)
        {
            var result = new ValidationItemSource<TableRecord>();
            using var business = GetBusiness<IStandBusiness>();
            var tables = business.GetTablesBySaloon(saloonId.AsGuid());
            result.ItemSource = tables;
            return result;
        }

        #endregion

        #region Ticket Operations

        #region Product to Ticket

        public ValidationContractResult CreateTicket(
            string productId,
            string quantity,
            string manualPrice,
            string manualPriceDescription,
            string tableId,
            string[] selectedDetailed,
            string[] preparations,
            string description,
            short? paxs,
            ClientRecord? client,
            bool? digitalMenu,
            short? seat,
            string separatorId,
            TypedList<POSClientByPositionContract>? seatedClients,
            string observations = null
        )
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.NewTicket(
                productId.AsGuid(),
                quantity.AsInteger(),
                manualPrice.AsDecimalX(),
                manualPriceDescription,
                tableId.AsGuidX(),
                (selectedDetailed ?? []).Select(x => x.AsGuid()).ToArray(),
                (preparations ?? []).Select(x => x.AsGuid()).ToArray(),
                description,
                paxs ?? 0,
                client,
                digitalMenu ?? false,
                seat,
                separatorId.AsGuidX(),
                seatedClients,
                observations
            );
        }

        public ValidationContractResult NewTicket(NewTicketDto request)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.NewTicket(request);
        }

        public ValidationContractResult AddProductToTicket(
            string ticketId,
            string productId,
            string quantity,
            string manualPrice,
            string manualPriceDescription,
            string[] selectedDetailed,
            string[] preparations,
            short? seat,
            string separatorId = null,
            string observations = null
        )
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.AddProductToTicket(
                ticketId.AsGuid(),
                productId.AsGuid(),
                quantity.AsInteger(),
                manualPrice.AsDecimalX(),
                manualPriceDescription,
                false,
                (selectedDetailed ?? []).Select(x => x.AsGuid()).ToArray(),
                (preparations ?? []).Select(x => x.AsGuid()).ToArray(),
                seat,
                separatorId.AsGuidX(),
                observations
            );
        }

        public ValidationContractResult AddProductToTicketByPlu(string ticketId, string productCode, string quantity,
            string[] preparations)
        {
            using (var business = GetBusiness<ITicketBusiness>())
            {
                return business.AddProductToTicketByPlu(ticketId.AsGuid(), productCode, quantity.AsInteger(), null,
                    (preparations ?? new string[] { }).Select(x => x.AsGuid()).ToArray());
            }
        }

        public ValidationContractResult CancelProductLine(string ticketId, string productLineId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.CancelTicketProduct(ticketId.AsGuid(), productLineId.AsGuid());
        }

        #endregion

        #region Loads

        public ValidationItemSource<TicketInfo> GetOpenTickets()
        {
            ValidationItemSource<TicketInfo> result = new ValidationItemSource<TicketInfo>();

            using (var business = GetBusiness<ITicketBusiness>())
            {
                try
                {
                    HubSessionData hubSessionData = BaseService.GetHubSessionData(HubContextCache.Current.Token);
                    List<TicketInfo> tickets = business.GetTicketInfoByStandCashier(hubSessionData.InstallationId,
                        hubSessionData.StandId, hubSessionData.CashierId);
                    result.ItemSource = tickets;
                }
                catch (Exception ex)
                {
                    result.Add(new ValidationResult(ex));
                }

                return result;
            }
        }

        public ValidationItemSource<TicketInfo> GetOpenTicketsByTable(string tableId)
        {
            var result = new ValidationItemSource<TicketInfo>();

            using var business = GetBusiness<ITicketBusiness>();
            try
            {
                var tickets = business.GetTicketInfoByTable(tableId.AsGuid());
                result.ItemSource = tickets;
            }
            catch (Exception ex)
            {
                result.Add(new ValidationResult(ex));
            }

            return result;
        }

        private ValidationItemSource<TicketInfo> GetOpenTicketsByTable(string tableId, string standId,
            string installationId)
        {
            var result = new ValidationItemSource<TicketInfo>();

            using var business = GetBusiness<ITicketBusiness>();
            try
            {
                var tickets =
                    business.GetTicketInfoByTable(tableId.AsGuid(), standId.AsGuid(), installationId.AsGuid());
                result.ItemSource = tickets;
            }
            catch (Exception ex)
            {
                result.Add(new ValidationResult(ex));
            }

            return result;
        }

        public ValidationContractResult LoadTicketForEdition(string ticketId, string forceOpen)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.LoadTicketForEdition(ticketId.AsGuid(), forceOpen.AsBoolean());
        }

        public ValidationContractResult CloseTicketFromEdition(string ticketId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return string.IsNullOrEmpty(ticketId)
                ? new ValidationContractResult()
                : business.SaveTicketFromEdition(ticketId.AsGuid());
        }

        #endregion

        #region Voids

        public ValidationResult VoidTicket(string ticketId, string cancelReasonId, string comment, string fromMerge)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.CancelTicket(ticketId.AsGuid(), cancelReasonId.AsGuid(), comment,
                fromMerge.AsBoolean());
        }

        #endregion

        #region Orders Dispatch

        public ValidationContractResult DispatchOrders(string ticketId, DispatchContract[] orders)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.DispatchTicketOrders(ticketId.AsGuid(), orders, true);
        }

        public ValidationContractResult VoidDispatchOrders(string ticketId, DispatchContract[] orders)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.VoidDispatchTicketOrders(ticketId.AsGuid(), orders, true);
        }

        public ValidationContractResult AwayDispatchOrders(string ticketId, DispatchContract[] orders)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.AwayDispatchTicketOrders(ticketId.AsGuid(), orders, true);
        }

        #endregion

        #region Orders

        public ValidationContractResult UpdateOrderQuantity(string ticketId, string productLineId, string quantity)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.ChangeTicketProductQuantity(ticketId.AsGuid(), productLineId.AsGuid(),
                quantity.AsDecimal());
        }

        public ValidationContractResult AddOrderPreparation(string ticketId, string productLineId, string preparationId)
        {
            using (var business = GetBusiness<ITicketBusiness>())
            {
                return business.AddOrderPreparation(ticketId.AsGuid(), productLineId.AsGuid(), preparationId.AsGuid());
            }
        }

        public ValidationContractResult RemoveOrderPreparation(string ticketId, string productLineId,
            string preparationId)
        {
            using (var business = GetBusiness<ITicketBusiness>())
            {
                return business.RemoveOrderPreparation(ticketId.AsGuid(), productLineId.AsGuid(),
                    preparationId.AsGuid());
            }
        }

        public ValidationContractResult UpdateOrderPreparation(string ticketId, string productLineId,
            string[] preparationsId)
        {
            using (var business = GetBusiness<ITicketBusiness>())
            {
                return business.UpdateOrderPreparation(ticketId.AsGuid(), productLineId.AsGuid(),
                    (preparationsId ?? new string[] { }).Select(x => x.AsGuid()).ToList());
            }
        }

        public ValidationContractResult AddOrderComment(string ticketId, string productLineId, string comment)
        {
            using (var business = GetBusiness<ITicketBusiness>())
            {
                return business.AddOrderComment(ticketId.AsGuid(), productLineId.AsGuid(), comment);
            }
        }

        #endregion

        #region Tables

        public ValidationContractResult SetTableToTicket(string ticketId, string tableId)
        {
            using (var business = GetBusiness<ITicketBusiness>())
            {
                return business.SetTableToTicket(ticketId.AsGuid(), tableId.AsGuid());
            }
        }

        public ValidationContractResult ClearTableFromTicket(string ticketId)
        {
            using (var business = GetBusiness<ITicketBusiness>())
            {
                return business.SetTableToTicket(ticketId.AsGuid(), null);
            }
        }

        #endregion

        #region Payments

        // public ValidationContractResult AddRoomPlanPayment(Guid ticketId, ReservationSearchFromExternalRecord item)
        // {
        //     var hasPermission = CheckUserWritePermission(Permissions.PaymentsMethodsMealPlan);
        //     if (hasPermission)
        //     {
        //         using (var business = GetBusiness<ITicketBusiness>())
        //         {
        //             return business.AddRoomPlanPayment(ticketId, item);
        //         }
        //     }
        //
        //     var result = new ValidationContractResult();
        //     result.AddError("UserNotAuthorized".Translate());
        //
        //     return result;
        // }

        // public ValidationContractResult AddCreditPayment(Guid ticketId, decimal? amount, ReservationSearchFromExternalRecord item)
        // {
        //     var hasPermission = CheckUserWritePermission(Permissions.PaymentsMethodsCredit);
        //     if (hasPermission)
        //     {
        //         using var business = GetBusiness<ITicketBusiness>();
        //         return business.AddCreditPayment(ticketId, amount, item);
        //     }
        //
        //     var result = new ValidationContractResult();
        //     result.AddError("UserNotAuthorized".Translate());
        //
        //     return result;
        // }

        // public ValidationContractResult AddHouseUsePayment(Guid ticketId, HouseUseRecord item)
        // {
        //     var hasPermission = CheckUserWritePermission(Permissions.PaymentsMethodsHouseUse);
        //     if (hasPermission)
        //     {
        //         using var business = GetBusiness<ITicketBusiness>();
        //         return business.AddHouseUsePayment(ticketId, item);
        //     }
        //
        //     var result = new ValidationContractResult(); 
        //     result.AddError("userNotAuthorized".Translate());
        //
        //     return result;
        // }

        // public ValidationContractResult AddPaymentToTicket(string ticketId, string paymentId, string creditCardId,
        //     string received, string value, string excessPaymentAsTip, string close)
        // {
        //     using var business = GetBusiness<ITicketBusiness>();
        //     return business.AddPaymentToTicket(ticketId.AsGuid(), paymentId.AsGuid(), creditCardId.AsGuidX(), received.AsDecimal(), value.AsDecimal(),
        //         excessPaymentAsTip.AsBoolean(), close.AsBoolean());
        // }

        // public ValidationContractResult ResetPaymentTickets(string ticketId)
        // {
        //     using (var business = GetBusiness<ITicketBusiness>())
        //     {
        //         return business.ResetPaymentTickets(ticketId.AsGuid());
        //     }
        // }

        public ValidationStringResult CloseTicket(Guid ticketId, ClientRecord client, PersonalDocType docTypeSelected,
            bool saveClient, string signature)
        {
            if (CheckUserWritePermission(Permissions.CloseTickets))
            {
                using (var business = GetBusiness<ITicketBusiness>())
                {
                    var dSignature = signature != null ? new Blob(Convert.FromBase64String(signature)) : null;
                    return business.CloseTicket(ticketId, client, docTypeSelected, saveClient, dSignature);
                }
            }
            else
            {
                var result = new ValidationStringResult();
                result.AddError("UserNotAuthorized".Translate());
                return result;
            }
        }

        public ValidationStringResult FastCloseTicket(string ticketId)
        {
            using (var business = GetBusiness<ITicketBusiness>())
            {
                return business.FastCloseTicket(ticketId.AsGuid(), true);
            }
        }

        public ValidationStringResult FastCloseTicketAsInvoice(string ticketId, ClientRecord client,
            string docTypeSelected, bool saveClient)
        {
            if (CheckUserWritePermission(Permissions.CloseTickets))
            {
                using (var bussines = GetBusiness<ITicketBusiness>())
                {
                    var dDocTypeSelected = Serializer.Deserialize<PersonalDocType>(docTypeSelected);
                    return bussines.FastCloseTicketAsInvoice(ticketId.AsGuid(), client, dDocTypeSelected, saveClient);
                }
            }
            else
            {
                var result = new ValidationStringResult();
                result.AddError("UserNotAuthorized".Translate());
                return result;
            }
        }

        public ValidationStringResult CloseTicketAsInvoice(string ticketId, ClientRecord client, string docTypeSelected,
            bool saveClient, string signature)
        {
            if (CheckUserWritePermission(Permissions.CloseTickets))
            {
                using (var bussines = GetBusiness<ITicketBusiness>())
                {
                    var dDocTypeSelected = Serializer.Deserialize<PersonalDocType>(docTypeSelected);
                    var dSignature = signature != null ? new Blob(Convert.FromBase64String(signature)) : null;
                    return bussines.CloseTicketAsInvoice(ticketId.AsGuid(), client, dDocTypeSelected, saveClient,
                        dSignature);
                }
            }
            else
            {
                var result = new ValidationStringResult();
                result.AddError("UserNotAuthorized".Translate());
                return result;
            }
        }

        #endregion

        #region Print

        public ValidationContractResult PrintTable(string ticketId)
        {
            using (var business = GetBusiness<ITicketBusiness>())
                return business.CreateLookupTable(ticketId.AsGuid(), true);
        }

        #endregion

        #endregion

        #region Cloud Operations

        public ValidationItemSource<ReservationRecord> GetReservations()
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetReservations(new ReservationFilterContract());
        }

        public ValidationItemSource<ReservationRecord> SearchReservations(ReservationFilterContract filter)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetReservations(filter);
        }

        public ValidationItemSource<SpaReservationSearchFromExternalRecord> SearchSpaReservations(SpaSearchReservationFilterModel filter)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetSpaReservations(filter);
        }

        #endregion

        #endregion

        public ValidationItemSource<HouseUseRecord> GetHouseUses(string standId)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetHouseUses(standId);
        }

        public ValidationItemSource<ClientRecord> GetClientEntities(ClientRecord client)
        {
            using (var business = GetBusiness<IStandBusiness>())
            {
                var qr = new QueryRequest();
                qr.AddParameter("lang_pk", BusinessContext.LanguageId);

                if (client == null) return business.GetClientEntities(qr);

                if (!string.IsNullOrEmpty(client.Name))
                    qr.AddFilter(ClientsQuery.ClientNameFilter, client.Name);
                if (!string.IsNullOrEmpty(client.FiscalNumber))
                    qr.AddFilter(ClientsQuery.ClientFiscalNumberFilter, client.FiscalNumber);
                if (!string.IsNullOrEmpty(client.CountryName))
                    qr.AddFilter(ClientsQuery.ClientCountryFilter, client.CountryName);

                return business.GetClientEntities(qr);
            }
        }

        public ValidationContractResult UpdateTicketTip(string ticketId, decimal? tipPercent, decimal? tipValue)
        {
            using (var business = GetBusiness<ITicketBusiness>())
            {
                return business.UpdateTicketTip(ticketId.AsGuid(), tipPercent, tipValue);
            }
        }

        public ValidationContractResult ApplyTicketDiscounts(string ticketId, string[] productsId,
            string discountTypeId, decimal? discountPercent)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.ApplyTicketDiscounts(ticketId.AsGuid(), productsId, discountTypeId.AsGuidX(),
                discountPercent);
        }

        public ValidationContractResult CreateMenuDigitalTicket(Guid tableId, ProductLineDto[] productLines)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.CreateMenuDigitalTicket(tableId, productLines);
        }

        public ValidationContractResult CreateTicketRoom(string room, string external, ProductLineDto[] productLines)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.CreateTicketRoom(room, external, productLines);
        }

        public SplitTicketResult SplitTicketByAlcoholicGroup(string ticketId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.SplitTicketByAlcoholicGroup(ticketId.AsGuid());
        }

        public ValidationContractResult ChangeProductLinesSeparator(string ticketId, string[] productLineIds,
            string separatorId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.ChangeProductLinesSeparator(ticketId.AsGuid(),
                productLineIds.Select(productLineId => productLineId.AsGuid()), separatorId.AsGuidX());
        }

        public ValidationContractResult ChangeProductLinesSeat(string ticketId, string[] productLineIds, short seat)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.ChangeProductLinesSeat(ticketId.AsGuid(),
                productLineIds.Select(productLineId => productLineId.AsGuid()), seat);
        }

        public ValidationItemSource<LiteTablesReservationRecord> GetTableReservations(string state)
        {
            using var business = GetBusiness<ICloudBusiness>();

            var dState = Serializer.Deserialize<ReservationState>(state);
            var filter = new TablesReservationFilterContract()
            {
                InDate = BusinessContext.WorkDate,
                StandId = BusinessContext.StandId,
                StateReservation = dState
            };

            return business.GetTableReservations(filter);
        }

        public ValidationContractResult CreateEmptyTicketToReservationTable(string[] tableIds,
            string reservationTableId, string name)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var tables = tableIds.Select(id => id.AsGuid()).ToArray();
            var reservationTable = reservationTableId.AsGuid();
            return business.CreateEmptyTicketToReservationTable(tables, reservationTable, name);
        }

        public ValidationResult CancelTableReservation(string id, string cancellationReasonId, string comments)
        {
            using var business = GetBusiness<IReservationTableBusinessCloud>();
            var tableReservationId = id.AsGuid();
            var cancellationReason = cancellationReasonId.AsGuid();
            return business.CancelBookingTable(tableReservationId, cancellationReason, comments);
        }

        public ValidationItemSource<ClientInstallationRecord> GetClientInstallations(string openAccounts,
            string clientId, string room, string fullName)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetClientInstallations(
                openAccounts: bool.Parse(openAccounts),
                clientId: clientId != "<null>" ? clientId.AsGuidX() : null,
                room: room != "<null>" ? room : null,
                fullName: fullName != "<null>" ? fullName : null
            );
        }

        public ValidationContractResult AddClientPositionToTicket(string ticketId, short pax,
            ClientInstallationRecord client)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.AddClientPositionToTicket(ticketId.AsGuid(), pax, client);
        }

        public ValidationContractResult RemoveClientPositionFromTicket(string ticketId, short pax)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.RemoveClientPositionFromTicket(ticketId.AsGuid(), pax);
        }

        public ValidationItemSource<Guid> GetPendingOrdersTables()
        {
            using (var business = GetBusiness<IStandBusiness>())
            {
                return business.GetPendingOrdersTables();
            }
        }

        public ValidationContractResult AttendTableOrder(Guid tableId)
        {
            using (var business = GetBusiness<ITicketBusiness>())
            {
                return business.AttendTableOrder(tableId);
            }
        }

        public ValidationItemSource<ProductPriceRecord> GetProductPrices(string standId, string rateId)
        {
            using var business = GetBusiness<IProductBusiness>();
            var result = business.GetProductPrices(standId.AsGuid(), rateId.AsGuid());
            return result;
        }

        public ValidationContractResult AddGuestToSeat(string ticketId, GuestContract guest, short seat)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.AddGuestToSeat(ticketId.AsGuid(), guest, seat);
        }

        public ValidationItemResult<ClientInfoContract> GetClientInfo(string clientId)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetClientInfo(clientId.AsGuid());
        }

        public ValidationContractResult UpdateOrderPreparations(string ticketId,
            ProductLinePreparationContract contract)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.UpdateOrderPreparations(ticketId.AsGuid(), contract);
        }

        public ValidationContractResult UpdateOrderNote(string ticketId, ProductLineNoteContract contract)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.UpdateOrderNote(ticketId.AsGuid(), contract);
        }

        public ValidationContractResult UpdateTicketInfo(string ticketId, short pax, string description)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.UpdateTicketInfo(ticketId.AsGuid(), pax, description);
        }

        public ValidationContractResult AddReservationToTicket(Guid ticketId, ReservationRecord reservation)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.AddReservationToTicket(ticketId, reservation);
        }

        public ValidationContractResult AddSpaReservationToTicket(Guid ticketId, SpaReservationRecord reservation)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.AddSpaReservationToTicket(ticketId, reservation);
        }

        public ValidationContractResult UpdateClientsByPosition(Guid ticketId,
            IEnumerable<POSClientByPositionContract> clients)
        {
            using var business = GetBusiness<ITicketBusiness>();
            return business.UpdateClientsByPosition(ticketId, clients);
        }

        public ValidationItemSource<PmsControlAccountRecord> GetControlAccounts(PmsControlAccountFilterModel filter)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetControlAccounts(filter);
        }

        public ValidationItemSource<ControlAccountMovementDto> GetControlAccountMovements(
            ControlAccountMovementsFilterModel filter)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetControlAccountMovements(filter);
        }

        public ValidationItemResult<AddPaymentResponse> ValidateTicketPayments(AddPaymentsRequest request)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var result = business.ValidateTicketPayments(request);
            var response = new ValidationItemResult<AddPaymentResponse>(result)
            {
                Item = new AddPaymentResponse
                {
                    Ticket = result.Contract.As<POSTicketContract>(),
                    Request = request
                }
            };

            return response;
        }

        public ValidationItemResult<CloseTicketResponse> NewCloseTicket(CloseTicketRequest request)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var result = business.NewCloseTicket(request);
            var response = new ValidationItemResult<CloseTicketResponse>(result)
            {
                Item = new CloseTicketResponse
                {
                    Ticket = result.Contract.As<POSTicketContract>(),
                    Request = request
                }
            };
            return response;
        }

        public ValidationItemResult<StandTableInfoDto> GetTablesInfo()
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetTablesInfo();
        }

        public ValidationItemResult<SpaServiceRecord> GetSpaServices(string room)
        {
            using var business = GetBusiness<IStandBusiness>();

            var spaReservationResult = business.GetSpaReservations(new SpaSearchReservationFilterModel
            {
                Room = room
            });

            if (spaReservationResult.HasErrors)
                return new ValidationItemResult<SpaServiceRecord>(spaReservationResult);
            
            var spaReservation = spaReservationResult.ItemSource.FirstOrDefault();
            if (spaReservation != null)
            {
                if (spaReservation.ProductId != null)
                {
                    var spaServicesResult = business.GetSpaServices(null, (Guid)spaReservation.Id);
                    return spaServicesResult;
                }

                // Error, there is no product related to this reservation
                var result = new ValidationItemResult<SpaServiceRecord>();
                result.AddError("NoSpaServicesErrorMsg".Translate());
                return result;
            }
            else
            {
                // Error, there is no spa reservation for this room ...
                var result = new ValidationItemResult<SpaServiceRecord>();
                result.AddError("NoSpaReservationForRoomErrorMsg".Translate() + ' ' + room);
                return result;
            }
        }

        #region Digital Menu

        public ValidationItemResult<MenuEnvDto> GetMenuStandEnvironment(string hotelId, string standId, string saloonId,
            string tableId, string langId)
        {
            var menuResult = GetMenu(hotelId, standId, langId);
            if (menuResult.HasErrors) return new ValidationItemResult<MenuEnvDto>(menuResult);
            var menuList = menuResult.ItemSource;

            var ordersResult = GetMenuOrders(hotelId, standId, saloonId, tableId, langId);
            if (ordersResult.HasErrors) return new ValidationItemResult<MenuEnvDto>(ordersResult);
            var ticketList = ordersResult.ItemSource;

            return new ValidationItemResult<MenuEnvDto>
            {
                Item = new MenuEnvDto
                {
                    Menus = menuList,
                    Tickets = ticketList,
                    SocketIoUrl = BusinessContext.SocketIOUri
                }
            };
        }

        public ValidationItemSource<MenuDto> GetMenu(string hotelId, string standId, string langId)
        {
            using var business = GetBusiness<IStandBusiness>();
            var standEnvResult =
                business.GetMenuStandEnvironment(hotelId.AsGuid(), standId.AsGuid(), langId.AsInteger());
            if (standEnvResult.HasErrors) return new ValidationItemSource<MenuDto>(standEnvResult);
            var standEnv = standEnvResult.Item;

            var menuList = new List<MenuDto>();
            foreach (var category in standEnv.Categories)
            {
                var productList = new List<ProductDto>();
                foreach (var subCategory in category.SubCategories)
                {
                    foreach (var productColumn in subCategory.ProductColumns)
                    {
                        var product = standEnv.Products.FirstOrDefault(record => record.Id == productColumn.ProductId);

                        if (product != null)
                        {
                            var productDto = new ProductDto
                            {
                                Id = product.Id.AsString(),
                                Description = product.Description,
                                Image = product.Image != null ? LoadImage(product.Image).Description : null,
                                Price = product.StandardPrice ?? 0,
                                Preparations = product.Preparations.Select(record => new PreparationDto
                                {
                                    Id = record.Id.AsString(),
                                    Description = record.Description
                                })
                            };
                            productList.Add(productDto);
                        }
                    }
                }

                if (productList.Count > 0)
                {
                    var menuDto = new MenuDto
                    {
                        Id = category.Category.Id.AsString(),
                        Description = category.Category.Description,
                        Image = category.Category.ImagePath != null
                            ? LoadImage(category.Category.ImagePath).Description
                            : null,
                        Products = productList
                    };
                    menuList.Add(menuDto);
                }
            }


            // var menuList = (from category in standEnv.Categories
            //     let productList = (from subCategory in category.SubCategories
            //         from productColumn in subCategory.ProductColumns
            //         select standEnv.Products.First(record => record.Id == productColumn.ProductId)
            //         into product
            //         select new ProductDto
            //         {
            //             Id = product.Id.AsString(),
            //             Description = product.Description,
            //             Image = product.Image != null ? LoadImage(product.Image).Description : null,
            //             Price = product.StandardPrice ?? 0,
            //             Preparations = product.Preparations.Select(record => new PreparationDto
            //             {
            //                 Id = record.Id.AsString(),
            //                 Description = record.Description
            //             })
            //         }).ToList()
            //     select new MenuDto
            //     {
            //         Id = category.Category.Id.AsString(),
            //         Description = category.Category.Description,
            //         Image = category.Category.ImagePath != null
            //             ? LoadImage(category.Category.ImagePath).Description
            //             : null,
            //         Products = productList
            //     }).ToList();

            return new ValidationItemSource<MenuDto>
            {
                ItemSource = menuList
            };
        }

        public ValidationItemSource<TicketDto> GetMenuOrders(string hotelId, string standId, string saloonId,
            string tableId, string langId)
        {
            using var business = GetBusiness<ITicketBusiness>();
            var ticketsInfoResult = GetOpenTicketsByTable(tableId, standId, hotelId);
            if (ticketsInfoResult.HasErrors) return new ValidationItemSource<TicketDto>(ticketsInfoResult);

            var tickets = new List<TicketDto>();

            // Get created tickets for table
            var attendedTickets = (from ticketInfo in ticketsInfoResult.ItemSource
                let productLinesResult =
                    business.GetTicketProductLines(ticketInfo.Id, langId.AsInteger(), hotelId.AsGuid())
                where !productLinesResult.HasErrors
                let productLines = productLinesResult.ItemSource.Where(contract =>
                    contract.AnnulmentCode == ProductLineCancellationStatus.Active || contract.AnnulmentCode ==
                    ProductLineCancellationStatus.ActiveByTransfer)
                select new TicketDto
                {
                    Id = ticketInfo.Id.AsString(),
                    TicketLines = productLines.Select(contract => new TicketLineDto
                    {
                        Id = contract.Id.ToString(),
                        ProductId = contract.Product.AsString(),
                        ProductDescription = contract.ProductDescription,
                        Quantity = contract.ProductQtd,
                        ProductPrice = contract.ProductValueBeforeDiscount,
                        Placed = true,
                        Attended = true,
                        Preparations = contract.Preparations.Select(prepContract => prepContract.PreparationId)
                    })
                }).ToList();

            if (attendedTickets.Count > 0)
            {
                tickets.AddRange(attendedTickets);
            }

            // Get pending ticket for table
            var orderList = business.GetMenuOrdersByTable(standId.AsGuid(), tableId.AsGuid(), langId.AsLong());
            if (orderList.Count <= 0)
            {
                return new ValidationItemSource<TicketDto>()
                {
                    ItemSource = tickets
                };
            }

            var pendingTicket = new TicketDto
            {
                Id = null,
                TicketLines = orderList.Select(record => new TicketLineDto
                {
                    Id = record.Id.AsString(),
                    ProductId = record.ProductId.AsString(),
                    ProductDescription = record.ProductDescription,
                    ProductPrice = record.ProductPrice,
                    Quantity = record.Quantity,
                    Placed = record.Placed,
                    Attended = false,
                    Preparations = record.Preparations
                })
            };

            tickets.Add(pendingTicket);

            return new ValidationItemSource<TicketDto>()
            {
                ItemSource = tickets
            };
        }

        public ValidationItemSource<TicketDto> AddMenuOrder(MenuDataDto data)
        {
            var result = new ValidationItemSource<TicketDto>();
            using var business = GetBusiness<ITicketBusiness>();
            if (data.Quantity != null)
            {
                var addOrderResult = business.AddMenuOrder(data.HotelId, data.PosId, data.SaloonId, data.TableId,
                    data.ProductId, (decimal)data.Quantity, data.Preparations);
                if (!addOrderResult.HasErrors)
                {
                    result = GetMenuOrders(data.HotelId.ToString(), data.PosId.ToString(), data.SaloonId.ToString(),
                        data.TableId.ToString(), "1033");
                }
                else
                {
                    result.AddValidations(addOrderResult);
                }
            }
            else
            {
                result.AddException(new Exception("Quantity must not be null"));
            }

            return result;
        }

        public ValidationItemSource<TicketDto> UpdateMenuOrder(MenuDataDto data)
        {
            var result = new ValidationItemSource<TicketDto>();
            using var business = GetBusiness<ITicketBusiness>();
            var deleteMenuOrder = business.UpdateMenuOrder(data.Id, data.Quantity, data.Preparations);
            if (!deleteMenuOrder.HasErrors)
            {
                result = GetMenuOrders(data.HotelId.ToString(), data.PosId.ToString(), data.SaloonId.ToString(),
                    data.TableId.ToString(), "1033");
            }
            else
            {
                result.AddValidations(deleteMenuOrder);
            }

            return result;
        }

        public ValidationItemSource<TicketDto> DeleteMenuOrder(MenuDataDto data)
        {
            var result = new ValidationItemSource<TicketDto>();
            using var business = GetBusiness<ITicketBusiness>();
            var deleteMenuOrder = business.DeleteMenuOrder(data.Id);
            if (!deleteMenuOrder.HasErrors)
            {
                result = GetMenuOrders(data.HotelId.ToString(), data.PosId.ToString(), data.SaloonId.ToString(),
                    data.TableId.ToString(), "1033");
            }
            else
            {
                result.AddValidations(deleteMenuOrder);
            }

            return result;
        }

        public ValidationItemSource<TicketDto> PlaceMenuOrder(MenuDataDto data)
        {
            var result = new ValidationItemSource<TicketDto>();
            using var business = GetBusiness<ITicketBusiness>();
            var placeMenuOrder = business.PlaceMenuOrder(data.HotelId, data.PosId, data.SaloonId, data.TableId);
            if (!placeMenuOrder.HasErrors)
            {
                result = GetMenuOrders(data.HotelId.ToString(), data.PosId.ToString(), data.SaloonId.ToString(),
                    data.TableId.ToString(), "1033");
            }
            else
            {
                result.AddValidations(placeMenuOrder);
            }

            return result;
        }

        public ValidationItemSource<SignatureRequestDto> GetSignatureRequests()
        {
            var result = new ValidationItemSource<SignatureRequestDto>();

            using var manager = GetManager();
            manager.Open();
            try
            {
                using var command = manager.CreateCommand("Service.Handheld.SignatureRequest");
                command.CommandText = """
                                      SELECT VEND.VEND_PK,
                                             VEND.VEND_COAP,
                                             (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = IPOS.LITE_DESC AND MULT.LANG_PK = 1033) AS IPOS_DESC
                                      FROM TNHT_SIRE SIRE
                                               INNER JOIN TNHT_VEND VEND ON SIRE.VEND_PK = VEND.VEND_PK
                                               INNER JOIN TNHT_IPOS IPOS ON IPOS.IPOS_PK = VEND.IPOS_PK
                                      WHERE SIRE_PROC = 0
                                      ORDER BY VEND.VEND_COAP
                                      """;
                using var reader = manager.ExecuteReader(command);
                try
                {
                    var itemList = new List<SignatureRequestDto>();

                    while (reader.Read())
                    {
                        var info = new SignatureRequestDto
                        {
                            TicketId = reader.GetGuid(0),
                            TicketSaleNumber = reader.GetString(1),
                            PointOfSale = reader.GetString(2),
                        };
                        itemList.Add(info);
                    }

                    result.ItemSource = itemList;
                }
                finally
                {
                    reader.Close();
                }
            }
            finally
            {
                manager.Close();
            }

            return result;
        }

        public ValidationItemSource<SignatureRequestDto> ProcessSignature(string ticketId, string signature)
        {
            var dTicketId = ticketId.AsGuid();
            var dSignature = new Blob(Convert.FromBase64String(signature));

            using var manager = GetManager();
            manager.Open();
            try
            {
                using var command = manager.CreateCommand("Service.Handheld.UpdateSignature");
                command.CommandText = "UPDATE TNHT_SIRE SET SIRE_DISI = :DISI, SIRE_PROC = 1 WHERE VEND_PK = :VEND_PK";
                manager.CreateParameter(command, "DISI", manager.ConvertValueType(dSignature));
                manager.CreateParameter(command, "VEND_PK", manager.ConvertValueType(dTicketId));
                manager.ExecuteNonQuery(command);

                // BusinessContext.SocketIOClient?.Publish("SignatureDone", ticketId.AsGuid());
            }
            catch (Exception e)
            {
                return new ValidationItemSource<SignatureRequestDto>(e);
            }
            finally
            {
                manager.Close();
            }

            return GetSignatureRequests();
        }

        public ValidationItemResult<PmsReservationRecord> GetPmsReservation(PmsReservationFilterModel request)
        {
            using var business = GetBusiness<IStandBusiness>();
            return business.GetPmsReservation(request);
        }

        #endregion
    }
}