﻿using NewHotel.Pos.Hub.Core;
using System.ComponentModel.Composition;

namespace NewHotel.Pos.Hub.Handheld
{
	[Export(typeof(IHubHost)), PartCreationPolicy(CreationPolicy.NonShared)]
    public sealed class HubHandheldServiceHost : HubServiceHost, IHubHost
    {
        #region Constructor

        public HubHandheldServiceHost()
            : base("NewHotel.Pos.Hub", typeof(ServiceHandheld))
        {
        }

        #endregion
    }
}