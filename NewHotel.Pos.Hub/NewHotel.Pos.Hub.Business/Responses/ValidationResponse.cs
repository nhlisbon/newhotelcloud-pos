﻿using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class ValidationResponse
    {
        public int Code { get; set; }
        public string[] Errors { get; set; }
        public string[] Warnings { get; set; }

        public void AppendValidations(ValidationResult result)
        {
            if (Code != 200)
                result.AddError("Error: " + Code.ToString());

            if (Errors != null)
            {
                for (int index = 0; index < Errors.Length; index++)
                    result.AddError(Errors[index]);
            }

            if (Warnings != null)
            {
                for (int index = 0; index < Warnings.Length; index++)
                    result.AddWarning(Warnings[index]);
            }
        }
    }
}