﻿using System;
using System.Runtime.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Business.Responses
{
    [DataContract]
    public class ReservationSearchFromExternalResponse : BaseRecord<Guid>
    {
        #region Properties

        [DataMember]
        public string ReservationNumber { get; set; }
        [DataMember]
        public string Guest { get; set; }
        [DataMember]
        public string CountryCode { get; set; }
        [DataMember]
        public string Arrival { get; set; }
        [DataMember]
        public string Departure { get; set; }
        [DataMember]
        public short? PensionPk { get; set; }
        [DataMember]
        public string Pension { get; set; }
        [DataMember]
        public string Paxs { get; set; }
        [DataMember]
        public string StateDescription { get; set; }
        [DataMember]
        public ReservationState State { get; set; }
        [DataMember]
        public Guid? AccountId { get; set; }
        [DataMember]
        public bool IsLock { get; set; }
        [DataMember]
        public decimal? Balance { get; set; }
        [DataMember]
        public string RoomTypeReserved { get; set; }
        [DataMember]
        public string RoomTypeOccupied { get; set; }
        [DataMember]
        public string Room { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public Guid? RoomType { get; set; }
        [DataMember]
        public Guid? RoomId { get; set; }

        [DataMember]
        // ReSharper disable once InconsistentNaming
        public bool? AllowCreditPOS { get; set; } = true;
        
        /// <summary>
        /// Entity Description
        /// </summary>
        [DataMember]
        public string Company { get; set; }

        #endregion
        #region Methods

        public ReservationSearchFromExternalRecord GetRecord()
        {
            var record = new ReservationSearchFromExternalRecord
            {
                Id = Id,
                ReservationNumber = ReservationNumber,
                Guest = Guest,
                CountryCode = CountryCode,
                Arrival = Arrival,
                Departure = Departure,
                PensionPk = PensionPk,
                Pension = Pension,
                Paxs = Paxs,
                StateDescription = StateDescription,
                State = State,
                AccountId = AccountId,
                IsLock = IsLock,
                Balance = Balance,
                RoomTypeReserved = RoomTypeReserved,
                RoomTypeOccupied = RoomTypeOccupied,
                Room = Room,
                Comments = Comments,
                RoomType = RoomType,
                RoomId = RoomId,
                AllowCreditPOS = AllowCreditPOS ?? true,
                Company = Company
            };

            return record;
        }

        #endregion
    }
}