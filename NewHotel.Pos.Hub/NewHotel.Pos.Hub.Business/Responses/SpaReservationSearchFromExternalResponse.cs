﻿using System;
using System.Runtime.Serialization;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Pos.Hub.Business.Business
{
    [DataContract]
    public class SpaReservationSearchFromExternalResponse : BaseRecord<Guid>
    {
        #region Properties

    [DataMember]
    public string ReservationNumber { get; set; }
    [DataMember]
    public DateTime? OccupationDate { get; set; }
    [DataMember]
    public string FmtDate { get; set; }
    [DataMember]
    public ReservationState? ReservationStatus { get; set; }
    [DataMember]
    public string StatusDesc { get; set; }
    [DataMember]
    public Guid? GuestId { get; set; }
    [DataMember]
    public string Guest { get; set; }
    [DataMember]
    public short? Adults { get; set; }
    [DataMember]
    public short? Childs { get; set; }
    [DataMember]
    public string CountryCode { get; set; }
    [DataMember]
    public string CountryDesc { get; set; }
    [DataMember]
    public DateTime? InitialHour { get; set; }
    [DataMember]
    public string FmtInitialHour { get; set; }
    [DataMember]
    public DateTime? FinalHour { get; set; }
    [DataMember]
    public string FmtFinalHour { get; set; }
    [DataMember]
    public ReservationType? ReservationType { get; set; }
    [DataMember]
    public Guid? EntityId { get; set; }
    [DataMember]
    public string EntityDesc { get; set; }
    [DataMember]
    public short? Occupation { get; set; }
    [DataMember]
    public DateTime? CreationDate { get; set; }
    [DataMember]
    public Guid? PriceRateId { get; set; }
    [DataMember]
    public string PriceRateDescription { get; set; }
    [DataMember]
    public Guid? AccountId { get; set; }
    [DataMember]
    public bool? IsLock { get; set; }
    [DataMember]
    public Guid? DiscountId { get; set; }
    [DataMember]
    public decimal? DiscountValue { get; set; }
    [DataMember]
    public decimal? DiscountPercent { get; set; }
    [DataMember]
    public Guid? ServiceId { get; set; }
    [DataMember]
    public string ServiceDesc { get; set; }
    [DataMember]
    public string InitialResourceDesc { get; set; }
    [DataMember]
    public string MediumResourceDesc { get; set; }
    [DataMember]
    public string FinalResourceDesc { get; set; }
    [DataMember]
    public string InitialTherapistDesc { get; set; }
    [DataMember]
    public string MediumTherapistDesc { get; set; }
    [DataMember]
    public string FinalTherapistDesc { get; set; }
    [DataMember]
    public string BaseCurrency { get; set; }
    [DataMember]
    public decimal? NetPrice { get; set; }
    [DataMember]
    public decimal? GrossPrice { get; set; }
    [DataMember]
    public string FmtReservationPrice { get; set; }
    [DataMember]
    public string ForeignCurrency { get; set; }
    [DataMember]
    public decimal? ForeignValue { get; set; }
    [DataMember]
    public decimal? ExchangeRate { get; set; }
    [DataMember]
    public int ReservedServicesCount { get; set; }
    [DataMember]
    public int CheckinServicesCount { get; set; }
    [DataMember]
    public long Operations { get; set; }
    [DataMember]
    public Guid? ProductId { get; set; }
    [DataMember]
    public string Room { get; set; }
    [DataMember]
    public string UncommonAllergies { get; set; }

        #endregion
        #region Methods

        public SpaReservationSearchFromExternalRecord GetRecord()
        {
            var record = new SpaReservationSearchFromExternalRecord
            {
                Id = Id,
                ReservationNumber = ReservationNumber,
                StatusDesc = StatusDesc,
                Guest = Guest,
                CountryDesc = CountryDesc,
                FmtDate = FmtDate,
                FmtInitialHour = FmtInitialHour,
                FmtFinalHour = FmtFinalHour,
                OccupationDate = OccupationDate,
                InitialHour = InitialHour,
                FinalHour = FinalHour,
                ReservationType = ReservationType,
                EntityId = EntityId,
                EntityDesc = EntityDesc,
                Occupation = Occupation,
                CreationDate = CreationDate,
                Adults = Adults,
                Childs = Childs,
                CountryCode = CountryCode,
                PriceRateDescription = PriceRateDescription,
                AccountId = AccountId,
                IsLock = IsLock,
                DiscountValue = DiscountValue,
                DiscountPercent = DiscountPercent,
                DiscountId = DiscountId,
                Operations = Operations,
                ServiceDesc = ServiceDesc,
                ServiceId = ServiceId,
                InitialResourceDesc = InitialResourceDesc,
                MediumResourceDesc = MediumResourceDesc,
                FinalResourceDesc = FinalResourceDesc,
                InitialTherapistDesc = InitialTherapistDesc,
                MediumTherapistDesc = MediumTherapistDesc,
                FinalTherapistDesc = FinalTherapistDesc,
                BaseCurrency = BaseCurrency,
                NetPrice = NetPrice,
                GrossPrice = GrossPrice,
                ForeignCurrency = ForeignCurrency,
                ForeignValue = ForeignValue,
                FmtReservationPrice = FmtReservationPrice,
                ReservedServicesCount = ReservedServicesCount,
                CheckinServicesCount = CheckinServicesCount,
                ReservationStatus = ReservationStatus,
                ProductId = ProductId,
                Room = Room,
                UncommonAllergies = UncommonAllergies
            };

            return record;
        }

        #endregion
    }
}