﻿using System;
using System.Runtime.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Business.Business
{
    [DataContract]
    public class ClosedTicketResponse : BaseRecord<Guid>
    {
        #region Properties

        [DataMember]
        public POSDocumentType DocumentType { get; set; }
        [DataMember]
        public string OpeningNumber { get; set; }
        [DataMember]
        public string Number { get; set; }
        [DataMember]
        public DateTime CloseDate { get; set; }
        [DataMember]
        public DateTime CloseTime { get; set; }
        [DataMember]
        public decimal Total { get; set; }
        [DataMember]
        public string CurrencyId { get; set; }
        [DataMember]
        public string StandDescription { get; set; }
        [DataMember]
        public string CashierDescription { get; set; }
        [DataMember]
        public string InvoiceNumber { get; set; }
        [DataMember]
        public string CreditNoteNumber { get; set; }
        [DataMember]
        public string BallotNumber { get; set; }
        [DataMember]
        public string RoomNumber { get; set; }
        [DataMember]
        public bool Cancelled { get; set; }
        [DataMember]
        public long? FpInvoiceNumber { get; set; }
        [DataMember]
        public long? FpCreditNoteNumber { get; set; }
        [DataMember]
        public string FpSerialInvoice { get; set; }
        [DataMember]
        public string FpSerialCreditNote { get; set; }

        #endregion
        #region Methods

        public NewHotel.Contracts.Pos.Records.ClosedTicketRecord GetRecord()
        {
            var record = new NewHotel.Contracts.Pos.Records.ClosedTicketRecord();

            record.Id = Id;
            record.DocumentType = DocumentType;
            record.OpeningNumber = OpeningNumber;
            record.Number = Number;
            record.CloseDate = CloseDate;
            record.CloseTime = CloseTime;
            record.Total = Total;
            record.CurrencyId = CurrencyId;
            record.StandDescription = StandDescription;
            record.CashierDescription = CashierDescription;
            record.InvoiceNumber = InvoiceNumber;
            record.CreditNoteNumber = CreditNoteNumber;
            record.BallotNumber = BallotNumber;
            record.RoomNumber = RoomNumber;
            record.Cancelled = Cancelled;
            record.FpInvoiceNumber = FpInvoiceNumber;
            record.FpCreditNoteNumber = FpCreditNoteNumber;
            record.FpSerialInvoice = FpSerialInvoice;
            record.FpSerialCreditNote = FpSerialCreditNote;

            return record;
        }

        #endregion
    }
}