﻿using System;
using System.Runtime.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Business.Business
{
    [DataContract]
    public class EntityInstallationResponse : BaseRecord<Guid>
    {
        #region Properties

        [DataMember]
        public string Abbreviation { get; set; }
        [DataMember]
        public string CompanyName { get; set; }
        [DataMember]
        public string FiscalName { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string CountryId { get; set; }
        [DataMember]
        public Guid? EntityTypeId { get; set; }
        [DataMember]
        public string EntityType { get; set; }
        [DataMember]
        public string Category { get; set; }
        [DataMember]
        public Guid? AccountId { get; set; }
        [DataMember]
        public Guid? TaxSchemaId { get; set; }
        [DataMember]
        public Guid? MarketOriginId { get; set; }
        [DataMember]
        public string MarketOrigin { get; set; }
        [DataMember]
        public Guid? MarketSegmentId { get; set; }
        [DataMember]
        public string MarketSegment { get; set; }
        [DataMember]
        public string FiscalNumber { get; set; }
        [DataMember]
        public string InstallationCountry { get; set; }
        [DataMember]
        public string FiscalRegister { get; set; }
        [DataMember]
        public string FiscalAddress { get; set; }
        [DataMember]
        public string EconomicActivity { get; set; }
        [DataMember]
        public bool? TaxRetention { get; set; }
        [DataMember]
        public string FiscalCountry { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public InvoiceDestination? InvoiceMasterTo { get; set; }
        [DataMember]
        public InvoiceDestination? InvoceExtra1To { get; set; }
        [DataMember]
        public InvoiceDestination? InvoceExtra2To { get; set; }
        [DataMember]
        public InvoiceDestination? InvoceExtra3To { get; set; }
        [DataMember]
        public InvoiceDestination? InvoceExtra4To { get; set; }
        [DataMember]
        public InvoiceDestination? InvoceExtra5To { get; set; }
        [DataMember]
        public Guid? SalesmanId { get; set; }
        [DataMember]
        public Guid? RetentionSchemaId { get; set; }

        #endregion
        #region Methods

        public EntityInstallationRecord GetRecord()
        {
            var record = new EntityInstallationRecord();
            
            record.Id = Id;
            record.Abbreviation = Abbreviation;
            record.CompanyName = CompanyName;
            record.FiscalName = FiscalName;
            record.Address = Address;
            record.CountryId = CountryId;
            record.EntityTypeId = EntityTypeId;
            record.EntityType = EntityType;
            record.Category = Category;
            record.AccountId = AccountId;
            record.TaxSchemaId = TaxSchemaId;
            record.MarketOriginId = MarketOriginId;
            record.MarketOrigin = MarketOrigin;
            record.MarketSegmentId = MarketSegmentId;
            record.MarketSegment = MarketSegment;
            record.FiscalNumber = FiscalNumber;
            record.InstallationCountry = InstallationCountry;
            record.FiscalRegister = FiscalRegister;
            record.FiscalAddress = FiscalAddress;
            record.EconomicActivity = EconomicActivity;
            record.TaxRetention = TaxRetention;
            record.FiscalCountry = FiscalCountry;
            record.Country = Country;
            record.EmailAddress = EmailAddress;
            record.InvoiceMasterTo = InvoiceMasterTo;
            record.InvoceExtra1To = InvoceExtra1To;
            record.InvoceExtra2To = InvoceExtra2To;
            record.InvoceExtra3To = InvoceExtra3To;
            record.InvoceExtra4To = InvoceExtra4To;
            record.InvoceExtra5To = InvoceExtra5To;
            record.SalesmanId = SalesmanId;
            record.RetentionSchemaId = RetentionSchemaId;

            return record;
        }

        #endregion
    }
}