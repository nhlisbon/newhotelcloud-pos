﻿using System.Collections.Generic;
using NewHotel.Pos.Hub.Business.Business;

namespace NewHotel.Pos.Hub.Business.Responses;

public class ValidationContentListResponse<T> : ValidationResponse
    where T : class
{
    public List<T>? Content { get; set; }   
}