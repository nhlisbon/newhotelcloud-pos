﻿using System;
using System.Runtime.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Business.Business
{
    [DataContract]
    public class ClientInstallationResponse : BaseRecord<Guid>
    {
        #region Properties

        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string CountryId { get; set; }
        [DataMember]
        public string FiscalAddress { get; set; }
        [DataMember]
        public string FiscalCountry { get; set; }
        [DataMember]
        public string FiscalNumber { get; set; }
        [DataMember]
        public string FiscalRegister { get; set; }
        [DataMember]
        public DateTime? Birthday { get; set; }
        [DataMember]
        public string Age { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public string Gender { get; set; }
        [DataMember]
        public bool? Regular { get; set; }
        [DataMember]
        public bool? UnWanted { get; set; }
        [DataMember]
        public Guid? CurrentAccountId { get; set; }
        [DataMember]
        public string CivilState { get; set; }
        [DataMember]
        public string Profession { get; set; }
        [DataMember]
        public string JobFunction { get; set; }
        [DataMember]
        public string ClientAddress { get; set; }
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public string PostalCode { get; set; }
        [DataMember]
        public decimal? Balance { get; set; }
        [DataMember]
        public decimal? Pending { get; set; }
        [DataMember]
        public string InstallationCountry { get; set; }
        [DataMember]
        public string IdentityDoc { get; set; }
        [DataMember]
        public string ResidenceDoc { get; set; }
        [DataMember]
        public string DriverDoc { get; set; }
        [DataMember]
        public string PassportDoc { get; set; }
        [DataMember]
        public string FreeCode { get; set; }
        [DataMember]
        public string Room { get; set; }
        [DataMember]
        public string Reservation { get; set; }
        [DataMember]
        public string Preferences { get; set; }
        [DataMember]
        public string Diets { get; set; }
        [DataMember]
        public string Attentions { get; set; }
        [DataMember]
        public string Allergies { get; set; }

        #endregion
        #region Methods

        public ClientInstallationRecord GetRecord()
        {
            var record = new ClientInstallationRecord();

            record.Id = Id;
            record.Title = Title;
            record.Name = Name;
            record.Address = Address;
            record.CountryId = CountryId;
            record.FiscalAddress = FiscalAddress;
            record.FiscalCountry = FiscalCountry;
            record.FiscalNumber = FiscalNumber;
            record.FiscalRegister = FiscalRegister;
            record.Birthday = Birthday;
            record.Age = Age;
            record.Type = Type;
            record.Country = Country;
            record.Gender = Gender;
            record.Regular = Regular;
            record.UnWanted = UnWanted;
            record.CurrentAccountId = CurrentAccountId;
            record.CivilState = CivilState;
            record.Profession = Profession;
            record.JobFunction = JobFunction;
            record.ClientAddress = ClientAddress;
            record.EmailAddress = EmailAddress;
            record.PostalCode = PostalCode;
            record.Balance = Balance;
            record.Pending = Pending;
            record.InstallationCountry = InstallationCountry;
            record.IdentityDoc = IdentityDoc;
            record.ResidenceDoc = ResidenceDoc;
            record.DriverDoc = DriverDoc;
            record.PassportDoc = PassportDoc;
            record.FreeCode = FreeCode;
            record.Room = Room;
            record.Reservation = Reservation;
            record.Preferences = Preferences;
            record.Diets = Diets;
            record.Attentions = Attentions;
            record.Allergies = Allergies;

            return record;
        }

        #endregion
    }
}