﻿using System;
using System.Runtime.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Business.Business
{
    [DataContract]
    public class EventReservationSearchFromExternalResponse : BaseRecord<Guid>
    {
        #region Properties

        [DataMember]
        public string ReservationNumber { get; set; }
        [DataMember]
        public Guid? AccountId { get; set; }
        [DataMember]
        public bool IsLock { get; set; }
        [DataMember]
        public string EventName { get; set; }
        [DataMember]
        public string CountryId { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public string InitialDate { get; set; }
        [DataMember]
        public string FinalDate { get; set; }
        [DataMember]
        public Guid EventCategoryId { get; set; }
        [DataMember]
        public string EventCategory { get; set; }
        [DataMember]
        public long ConfirmationStatusId { get; set; }
        [DataMember]
        public string ConfirmationStatus { get; set; }
        [DataMember]
        public Guid? TaxSchemaId { get; set; }
        [DataMember]
        public string TaxSchema { get; set; }
        [DataMember]
        public Guid? SegmentSourceId { get; set; }
        [DataMember]
        public string SegmentSource { get; set; }
        [DataMember]
        public Guid? SegmentId { get; set; }
        [DataMember]
        public string Segment { get; set; }
        [DataMember]
        public ReservationState Status { get; set; }
        [DataMember]
        public string StatusDescription { get; set; }
        [DataMember]
        public string ContactName { get; set; }
        [DataMember]
        public short Adult { get; set; }
        [DataMember]
        public short Children { get; set; }
        [DataMember]
        public string Remarks { get; set; }
        [DataMember]
        public string InternalRemarks { get; set; }
        [DataMember]
        public string InvoiceRemarks { get; set; }
        [DataMember]
        public bool TaxIncluded { get; set; }
        [DataMember]
        public long Operations { get; set; }
        [DataMember]
        public decimal? TotalPrice { get; set; }
        [DataMember]
        public decimal? Launched { get; set; }
        [DataMember]
        public decimal? Invoiced { get; set; }
        [DataMember]
        public string Locations { get; set; }

        #endregion
        #region Methods

        public EventReservationSearchFromExternalRecord GetRecord()
        {
            var record = new EventReservationSearchFromExternalRecord();

            record.Id = Id;
            record.ReservationNumber = ReservationNumber;
            record.AccountId = AccountId;
            record.IsLock = IsLock;
            record.EventName = EventName;
            record.CountryId = CountryId;
            record.Country = Country;
            record.InitialDate = InitialDate;
            record.FinalDate = FinalDate;
            record.EventCategoryId = EventCategoryId;
            record.EventCategory = EventCategory;
            record.ConfirmationStatusId = ConfirmationStatusId;
            record.ConfirmationStatus = ConfirmationStatus;
            record.TaxSchemaId = TaxSchemaId;
            record.TaxSchema = TaxSchema;
            record.SegmentSourceId = SegmentSourceId;
            record.SegmentSource = SegmentSource;
            record.SegmentId = SegmentId;
            record.Segment = Segment;
            record.Status = Status;
            record.StatusDescription = StatusDescription;
            record.ContactName = ContactName;
            record.Adult = Adult;
            record.Children = Children;
            record.Remarks = Remarks;
            record.InternalRemarks = InternalRemarks;
            record.InvoiceRemarks = InvoiceRemarks;
            record.TaxIncluded = TaxIncluded;
            record.Operations = Operations;
            record.TotalPrice = TotalPrice;
            record.Launched = Launched;
            record.Invoiced = Invoiced;
            record.Locations = Locations;

            return record;
        }

        #endregion
    }
}