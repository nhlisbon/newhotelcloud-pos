﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Pos.Hub.Business.Business
{
    [DataContract]
    public class ReservationsGroupsResponse : BaseRecord<Guid>
    {
        #region Properties

        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public ARGBColor? Color { get; set; }
        [DataMember]
        public string CountryId { get; set; }
        [DataMember]
        public DateTime Created { get; set; }
        [DataMember]
        public DateTime Arrival { get; set; }
        [DataMember]
        public DateTime Departure { get; set; }
        [DataMember]
        public string Company { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public decimal Rooms { get; set; }
        [DataMember]
        public decimal Reservations { get; set; }
        [DataMember]
        public Guid? AccountId { get; set; }
        [DataMember]
        public string Contact { get; set; }
        [DataMember]
        public string ReservationGroupType { get; set; }
        [DataMember]
        public long Operations { get; set; }
        [DataMember]
        public List<ReservationGroupRoomTypeResponse> RoomTypes { get; set; }

        #endregion
        #region Methods

        public ReservationsGroupsRecord GetRecord()
        {
            var record = new ReservationsGroupsRecord();

            record.Id = Id;
            record.Name = Name;
            record.Description = Description;
            record.Color = Color;
            record.CountryId = CountryId;
            record.Created = Created;
            record.Arrival = Arrival;
            record.Departure = Departure;
            record.Company = Company;
            record.FirstName = FirstName;
            record.LastName = LastName;
            record.Rooms = Rooms;
            record.Reservations = Reservations;
            record.AccountId = AccountId;
            record.Contact = Contact;
            record.ReservationGroupType = ReservationGroupType;
            record.Operations = Operations;
            record.RoomTypes = RoomTypes.Select(x => x.GetRecord()).ToList();

            return record;
        }

        #endregion
        #region Constructor

        public ReservationsGroupsResponse()
        {
            RoomTypes = new List<ReservationGroupRoomTypeResponse>();
        }

        #endregion
    }

    [DataContract]
    public class ReservationGroupRoomTypeResponse : BaseRecord<Guid>
    {
        #region Properties

        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int Count { get; set; }
        [DataMember]
        public DateTime Arrival { get; set; }
        [DataMember]
        public DateTime Departure { get; set; }
        [DataMember]
        public ARGBColor? Color { get; set; }

        #endregion
        #region Methods

        public ReservationGroupRoomTypeRecord GetRecord()
        {
            var record = new ReservationGroupRoomTypeRecord();

            record.Id = Id;
            record.Description = Description;
            record.Count = Count;
            record.Arrival = Arrival;
            record.Departure = Departure;
            record.Color = Color;

            return record;
        }

        #endregion
    }
}