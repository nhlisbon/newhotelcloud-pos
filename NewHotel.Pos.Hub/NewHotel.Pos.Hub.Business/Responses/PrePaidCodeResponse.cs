﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.Responses
{
    public class PrePaidCodeResponse
    {
        public string Code { get; set; }
        public decimal Balance { get; set; }
        public string GuestName { get; set; }
    }
}
