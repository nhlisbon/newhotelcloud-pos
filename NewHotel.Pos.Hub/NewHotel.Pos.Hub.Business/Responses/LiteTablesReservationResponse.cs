﻿using System;
using System.Runtime.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Business.Responses
{
    [DataContract]
    public class LiteTablesReservationResponse : BaseRecord<Guid>
    {
        #region Properties

        [DataMember] public string SerieDocument { get; set; }
        [DataMember] public long? NumberDocument { get; set; }
        [DataMember] public string SerieName { get; set; }
        [DataMember] public string FullName { get; set; }
        [DataMember] public string CellPhone { get; set; }
        [DataMember] public string Email { get; set; }
        [DataMember] public long Status { get; set; }
        [DataMember] public short PaxsAdults { get; set; }
        [DataMember] public short PaxsChildren { get; set; }
        [DataMember] public short PaxsBabies { get; set; }
        [DataMember] public string CountryId { get; set; }
        [DataMember] public Guid? Stand { get; set; }
        [DataMember] public string StandDesc { get; set; }
        [DataMember] public Guid? Saloon { get; set; }
        [DataMember] public string SaloonDesc { get; set; }
        [DataMember] public Guid? BookingSlotId { get; set; }
        [DataMember] public string BookingSlotDesc { get; set; }
        [DataMember] public Guid? TableId { get; set; }
        [DataMember] public string TableDesc { get; set; }
        [DataMember] public bool Lock { get; set; }
        [DataMember] public bool? Smoke { get; set; }
        [DataMember] public string Comments { get; set; }
        [DataMember] public bool ConfirmationStatus { get; set; }
        [DataMember] public long? ConfirmationStatusId { get; set; }
        [DataMember] public string ReservationStatus { get; set; }
        [DataMember] public DateTime CreatedDate { get; set; }
        [DataMember] public DateTime Date { get; set; }
        [DataMember] public DateTime InitialTime { get; set; }
        [DataMember] public string InitialTimeFmt { get; set; }
        [DataMember] public DateTime? RealInitialTime { get; set; }
        [DataMember] public string RealInitialTimeFmt { get; set; }
        [DataMember] public DateTime? RealFinalTime { get; set; }
        [DataMember] public string RealFinalTimeFmt { get; set; }
        [DataMember] public DateTime FinalTime { get; set; }
        [DataMember] public string FinalTimeFmt { get; set; }
        [DataMember] public int NoTables { get; set; } = 1;
        [DataMember] public string QuickInformation { get; set; }
        [DataMember] public Guid? SlotId { get; set; }
        [DataMember] public Guid? StayId { get; set; }
        [DataMember] public string Room { get; set; }
        [DataMember] public string MealPlan { get; set; }
        [DataMember] public string SlotDuration { get; set; }
        [DataMember] public string Preferences { get; set; }
        [DataMember] public string Diets { get; set; }
        [DataMember] public string Attentions { get; set; }
        [DataMember] public string Allergies { get; set; }
        [DataMember] public DateTime LastModified { get; set; }

        #endregion

        #region Methods

        public LiteTablesReservationRecord GetRecord()
        {
            var record = new LiteTablesReservationRecord
            {
                Id = Id,
                SerieDocument = SerieDocument,
                NumberDocument = NumberDocument,
                SerieName = SerieName,
                FullName = FullName,
                CellPhone = CellPhone,
                Email = Email,
                Status = Status,
                PaxsAdults = PaxsAdults,
                PaxsChildren = PaxsChildren,
                PaxsBabies = PaxsBabies,
                CountryId = CountryId,
                Stand = Stand,
                StandDesc = StandDesc,
                Saloon = Saloon,
                SaloonDesc = SaloonDesc,
                TableId = TableId,
                TableDesc = TableDesc,
                Lock = Lock,
                Smoke = Smoke,
                Comments = Comments,
                ConfirmationStatus = ConfirmationStatus,
                ConfirmationStatusId = ConfirmationStatusId,
                ReservationStatus = ReservationStatus,
                CreatedDate = CreatedDate,
                Date = Date,
                InitialTime = InitialTime,
                InitialTimeFmt = InitialTimeFmt,
                RealInitialTime = RealInitialTime,
                RealInitialTimeFmt = RealInitialTimeFmt,
                RealFinalTime = RealFinalTime,
                RealFinalTimeFmt = RealFinalTimeFmt,
                FinalTime = FinalTime,
                FinalTimeFmt = FinalTimeFmt,
                NoTables = NoTables,
                QuickInformation = QuickInformation,
                SlotId = SlotId,
                StayId = StayId,
                Room = Room,
                MealPlan = MealPlan,
                SlotDuration = SlotDuration,
                Preferences = Preferences,
                Diets = Diets,
                Attentions = Attentions,
                Allergies = Allergies,
                LastModified = LastModified
            };

            return record;
        }

        #endregion
    }
}