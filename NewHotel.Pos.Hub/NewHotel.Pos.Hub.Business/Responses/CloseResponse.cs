﻿using System;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class CloseResponse : ValidationResponse
    {
        public long Turn { get; set; }
        public DateTime WorkDate { get; set; }
    }
}