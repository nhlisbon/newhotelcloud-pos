﻿using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.Responses
{
    [DataContract]
    public class StandTimeSlotsResponse : BaseRecord<Guid>
    {
        #region Properties
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public Guid StandId { get; set; }
        [DataMember]
        public string StartHours { get; set; }
        [DataMember]
        public string StartMinutes { get; set; }
        [DataMember]
        public int? PaxsMaxPercent { get; set; }
        [DataMember]
        public int ReservationDurationEstimated { get; set; }
        [DataMember]
        public int? PaxsMaxNumber { get; set; }
        #endregion

        #region Methods
        public StandTimeSlotRecord GetRecord()
        {
            var record = new StandTimeSlotRecord
            {
                Id = Id,
                Description = Description,
                StandId = StandId,
                StartHours = StartHours,
                StartMinutes = StartMinutes,
                PaxsMaxPercent = PaxsMaxPercent,
                ReservationDurationEstimated = ReservationDurationEstimated,
                PaxsMaxNumber = PaxsMaxNumber
            };
            return record;
        }
        #endregion
    }
}
