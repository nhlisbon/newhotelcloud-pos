﻿using System;
using System.Runtime.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Business.Business
{
    [DataContract]
    public class ControlAccountResponse : BaseRecord<Guid>
    {
        #region Properties

        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public object FiscalNumber { get; set; }
        [DataMember]
        public bool IsBalanceChecked { get; set; }
        [DataMember]
        public Guid? AccountId { get; set; }
        [DataMember]
        public bool Inactive { get; set; }
        [DataMember]
        public decimal? Balance { get; set; }
        [DataMember]
        public decimal? Pending { get; set; }
        [DataMember]
        public decimal? ForeignBalance { get; set; }
        [DataMember]
        public decimal? ForeignPending { get; set; }

        #endregion
        #region Methods

        public ControlAccountRecord GetRecord()
        {
            var record = new ControlAccountRecord();

            record.Id = Id;
            record.Address = Address;
            record.Description = Description;
            record.FiscalNumber = FiscalNumber;
            record.IsBalanceChecked = IsBalanceChecked;
            record.AccountId = AccountId;
            record.Inactive = Inactive;
            record.Balance = Balance;
            record.Pending = Pending;
            record.ForeignBalance = ForeignBalance;
            record.ForeignPending = ForeignPending;

            return record;
        }

        #endregion
    }
}