﻿using System;
using System.Runtime.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Business.Responses
{
    [DataContract]
    public class MealsControlResponse : BaseRecord<Guid?>
    {
        #region Properties

        [DataMember] public string Reservation { get; set; }
        [DataMember] public string GroupName { get; set; }
        [DataMember] public string GuestFullName { get; set; }
        [DataMember] public string Room { get; set; }
        [DataMember] public int? RoomSort { get; set; }
        [DataMember] public string ReservationStatus { get; set; }
        [DataMember] public string PensionMode { get; set; }
        [DataMember] public string Country { get; set; }
        [DataMember] public DateTime ArrivalDate { get; set; }
        [DataMember] public DateTime DepartureDate { get; set; }
        [DataMember] public bool? HasBreakfast { get; set; }
        [DataMember] public bool? HasLunch { get; set; }
        [DataMember] public bool? HasDinner { get; set; }
        [DataMember] public bool? Breakfast { get; set; }
        [DataMember] public bool? Lunch { get; set; }
        [DataMember] public bool? Dinner { get; set; }
        [DataMember] public string ConsumptionCardNumber { get; set; }

        #endregion

        #region Methods

        public MealsControlRecord GetRecord()
        {
            var record = new MealsControlRecord
            {
                Id = Id ?? Guid.Empty,
                Reservation = Reservation,
                GroupName = GroupName,
                GuestFullName = GuestFullName,
                Room = Room,
                RoomSort = RoomSort,
                ReservationStatus = ReservationStatus,
                PensionMode = PensionMode,
                Country = Country,
                ArrivalDate = ArrivalDate,
                DepartureDate = DepartureDate,
                HasBreakfast = HasBreakfast ?? false,
                HasLunch = HasLunch ?? false,
                HasDinner = HasDinner ?? false,
                Breakfast = Breakfast ?? false,
                Lunch = Lunch ?? false,
                Dinner = Dinner ?? false,
                ConsumptionCardNumber = ConsumptionCardNumber
            };

            return record;
        }

        #endregion
    }
}