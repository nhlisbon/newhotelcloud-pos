﻿using System.Collections.Generic;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class QueryDataResponse<T>
        where T : class
    {
        public long RecordCount { get; set; }
        public List<T> Data { get; set; }
    }
}
