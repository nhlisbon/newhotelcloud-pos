﻿namespace NewHotel.Pos.Hub.Business.Business
{
    public class ContentResponse<T>
        where T : class
    {
        public T Content { get; set; }
    }
}
