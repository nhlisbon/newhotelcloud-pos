﻿namespace NewHotel.Pos.Hub.Business.Business
{
    public class ValidationContentResponse<T> : ValidationResponse
    where T : class
    {
        public T Content { get; set; }
    }
}
