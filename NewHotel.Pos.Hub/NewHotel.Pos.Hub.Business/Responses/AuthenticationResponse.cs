﻿namespace NewHotel.Pos.Hub.Business.Business
{
    public class TokenData
    {
        public string Token { get; set; }
        public string[] errorCodes { get; set; }
        public string[] errors { get; set; }
    }

    public class AuthenticateResponse : ContentResponse<TokenData>
    {
    }
}