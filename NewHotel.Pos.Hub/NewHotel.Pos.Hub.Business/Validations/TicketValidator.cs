﻿using NewHotel.Contracts;
using NewHotel.Pos.Core.Ext;
using NewHotel.Pos.Core;
using NewHotel.Pos.Hub.Business.PersistentObjects.Tickets;
using System;
using System.Linq;
using System.Runtime.CompilerServices;

namespace NewHotel.Pos.Hub.Business.Validations
{
	public class TicketValidator : IValidator<Ticket>
	{
		private readonly Ticket _ticket;
		private readonly IValidatorFactory _validatorFactory;

		public Ticket Target => _ticket;
		public IValidatorFactory Factory => _validatorFactory;

		public TicketValidator(Ticket ticket, IValidatorFactory validatorFactory)
		{
			_ticket = ticket;
			_validatorFactory = validatorFactory;
		}

		#region Methods

		public ValidationResult Validate(string prefix = default)
		{
			var result = new ValidationResult();

			if (_ticket.IsAnul)
				return result;

			var (totalPayment, numberOfPayments, paymentRecived, change) = _ticket.PaymentLineByTicket
				.Aggregate(
					(pTotal: 0m, pNumber: 0, pReceived: 0m, pChange: 0m),
					(acc, pl) => (acc.pTotal + pl.PaymentAmount,
								  acc.pNumber + 1,
								  acc.pReceived + pl.PaymentReceived,
								  acc.pChange + pl.GetChangeValue()));

			var closed = (_ticket.ClosedTime ?? DateTime.MinValue) != DateTime.MinValue || (_ticket.ClosedDate ?? DateTime.MinValue) != DateTime.MinValue;
			var ticketTotal = _ticket.IsAnul ? 0M : _ticket.Total;

			result.Add(ValidateWithoutPaymentsAndIsNotClosed(_ticket, prefix, closed, ticketTotal, numberOfPayments));
			result.Add(ValidateWithPayments(_ticket, prefix, closed, totalPayment, paymentRecived, numberOfPayments, ticketTotal));
			result.Add(ValidateWithTransfers(_ticket, prefix, closed, numberOfPayments));

			return result;
		}

		private ValidationResult ValidateWithoutPaymentsAndIsNotClosed(Ticket ticket, string prefixed, bool closed, decimal ticketTotal, int numberOfPayments)
		{
			var result = new ValidationResult();

			if (closed || numberOfPayments > 0)
				return result;

			decimal total = 0;
			decimal tipTotal = 0;

			foreach (var productLine in ticket.ProductLineByTicket.Where(pl => pl.IsActive && !pl.IsTip))
			{
				var validator = _validatorFactory.GetValidator<ProductLine>(productLine);
				var validation = validator.Validate(prefixed);
				if (validation.HasErrors)
				{
					result.AddValidations(validation);
					return result;
				}
				total += productLine.GrossValue;
			}

			if (ticket.TipPercent > decimal.Zero)
				tipTotal = TipHelper.CalculateTipPercent(total, ticket.TipPercent ?? 0M);
			else if (ticket.TipValue > decimal.Zero)
				tipTotal = ticket.TipValue ?? 0M;

			(decimal realTotal, decimal adjustment) = ticket.ExtractTotals(total + tipTotal);

			result
				.Validate(() => realTotal.InRange(ticketTotal), 999, $"{prefixed} Total product value: {realTotal}, does not match the ticket total: {ticketTotal}.");

			return result;
		}

		private ValidationResult ValidateWithPayments(Ticket ticket, string prefixed, bool closed, decimal totalPayment, decimal paymentRecived, int numberOfPayments, decimal ticketTotal)
		{
			var result = new ValidationResult();

			var isTransfer = ticket.PaymentLineByTicket.Any() && ticket.PaymentLineByTicket.First().ReceivableType == ReceivableType.TicketTransfer;

			if ((numberOfPayments == 0 && !closed) || (closed && isTransfer))
				return result;

			decimal productTotal = 0;
			decimal totalTip = ticket.TipValue ?? 0M;
			bool hasProductTip = false;
			decimal totalToPay = 0M;

			foreach (var productLine in ticket.ProductLineByTicket)
			{
				var validator = _validatorFactory.GetValidator(productLine);
				var validation = validator.Validate(prefixed);
				if (validation.HasErrors)
				{
					result.AddValidations(validation);
					return result;
				}
				if (productLine.IsActive)
				{
					productTotal += productLine.GrossValue;
				}
				if (productLine.IsTip)
				{
					hasProductTip = true;
					totalTip -= productLine.GrossValue;
				}
			}

			totalToPay = productTotal + totalTip;

			result
				.Validate(() => (productTotal == 0 && numberOfPayments == 0) || (numberOfPayments > 0 && closed), 999, $"{prefixed} Invalid payments in a {(closed ? "closed" : "open")} ticket.")
				//.Validate(() => (productTotal == 0 && numberOfPayments == 0) || (numberOfPayments > 0 && closed) || (numberOfPayments == 0 && !closed) || (numberOfPayments == 0 && productTotal > 0 && closed), 999, $"{prefixed} Invalid payments in a {(closed ? "closed" : "open")} ticket.")
				.Validate(() => ticketTotal.InRange(productTotal), 999, $"{prefixed} Total value do not corresponds to total sales ({ticketTotal} / {productTotal}).")
				.Validate(() => !hasProductTip || (totalTip == 0M), 999, $"Tip value mistmatch")
				.Validate(() => (numberOfPayments == 0) || (closed && numberOfPayments > 0 && totalToPay == totalPayment), 999, $"{prefixed} Total value does not correspond to the total payment ({totalToPay} / {totalPayment}).");

			return result;
		}

		private ValidationResult ValidateWithTransfers(Ticket ticket, string prefixed, bool closed, int numberOfPayments)
		{
			var result = new ValidationResult();

			if (numberOfPayments != 1)
				return result;

			var isTransfer = ticket.PaymentLineByTicket.First().ReceivableType == ReceivableType.TicketTransfer;
			if (!isTransfer || !closed)
				return result;

			return result
				.Validate(() => !ticket.IsAnul && ticket.Total == 0M && closed && numberOfPayments == 1, 999, $"{prefixed} Invalid state in transfer reception. \n " +
					$"{(ticket.Total != 0M ? $"Ticket total is not 0, current total: {ticket.Total} \n" : "")}" +
					$"{(!ticket.IsAnul ? "Ticket is not cancelled \n" : "")}" +
					$"{(!closed ? "Ticket should be closed \n" : "")}" +
					$"{(numberOfPayments != 1 ? "Ticket can only have one payment which is the cancellation payment" : "")}"
				);
		}

		#endregion
	}
}
