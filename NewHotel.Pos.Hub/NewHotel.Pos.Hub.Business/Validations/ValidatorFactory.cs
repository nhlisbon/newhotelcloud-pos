﻿using NewHotel.Pos.Hub.Business.PersistentObjects.Tickets;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace NewHotel.Pos.Hub.Business.Validations
{
    public class ValidatorFactory : IValidatorFactory
    {
        private bool _enableValidations;
        private readonly Dictionary<Type, Delegate> _registry = new Dictionary<Type, Delegate>();
        
        public ValidatorFactory()
        {
            _enableValidations = bool.TryParse(ConfigurationManager.AppSettings["EnableDevValidations"], out bool isEnabled) && isEnabled;
        }

        public IValidator GetValidator<T>(T item) 
            where T : class
        {
            if(!_enableValidations)
                return new EmptyValidator();
            
            if (_registry.TryGetValue(typeof(T), out var validator))
                return ((Func<T, IValidatorFactory, IValidator<T>>)validator)(item, this);
            
            throw new InvalidOperationException($"No validator found for type {typeof(T).Name}");
        }

        public void Register<T>(Func<T, IValidatorFactory, IValidator<T>> factory)
            where T : class
        {
            if (_registry.ContainsKey(typeof(T)))
                throw new InvalidOperationException($"Validator for type {typeof(T).Name} is already registered");
           
            _registry[typeof(T)] = factory;
        }

        public bool IsValidatorRegistered<T>()
            where T : class
        {
            return _registry.ContainsKey(typeof(T));
        }
    }
}