﻿using NewHotel.Pos.Hub.Business.PersistentObjects.Tickets;
using NewHotel.Contracts;
using System;
using System.Runtime.CompilerServices;

namespace NewHotel.Pos.Hub.Business.Validations
{
    public class ProductLineValidator : IValidator<ProductLine>
    {
        private readonly ProductLine _productLine;
        private readonly IValidatorFactory _validatorFactory;
        
        public ProductLine Target => _productLine;
		public IValidatorFactory Factory => _validatorFactory;

        public ProductLineValidator(ProductLine productLine, IValidatorFactory validatorFactory)
        {
            _productLine = productLine;
            _validatorFactory = validatorFactory;
        }

        public ValidationResult Validate(string prefix = default)
        {
            var result = new ValidationResult();

            return result.Add(ValidateBeforeSave(_productLine, prefix));;
        }

        private ValidationResult ValidateBeforeSave(ProductLine productLine, string prefix = default)
        {
            var result = new ValidationResult();

            var total = productLine.NetValue + productLine.FirstIvaValue + (productLine.SecondIvaValue ?? 0M) + (productLine.ThirdIvaValue ?? 0M);

            // TODO: Review this code and if the validations are correct
            //var totalWithDiscount = total + productLine.DiscountValue; 
            // This check happens because...
            //var grossValueInBase = 
            //        productLine.GrossValueInBase > decimal.Zero && // ...when the ticket is created, the GrossValueInBase is not yet set 
            //        total >= productLine.GrossValueInBase  // ...when coming from the method SaveFromEdition, the recalculation is made in the loading of the ticket
            //            ? productLine.GrossValueInBase
            //            : total
            //            ;
            return result
                //.Validate(() => productLine.GrossValue == grossValueInBase, 999, $"[{prefix}] Conversion value in gross base {grossValueInBase}, does not match the gross value {productLine.GrossValue}, in product line position {productLine.ItemNumber}")
                //.Validate(() => productLine.ValueBeforeDiscount == totalWithDiscount, 999, $"[{prefix}] Total value with discount {totalWithDiscount} does not match the value before discount {productLine.ValueBeforeDiscount}, in product line position {productLine.ItemNumber}")
                .Validate(() => total == productLine.GrossValue, 999, $"[{prefix}] Gross value does not match the detail values: {total}, in product line position {productLine.ItemNumber} with value {productLine.GrossValue}");
        }
    }
}
