﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.Validations
{
    public class EmptyValidator : IValidator
    {
        public ValidationResult Validate(string prefix = default)
        {
            return new ValidationResult();
        }
    }
}
