﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.Validations
{
    public interface IValidatorFactory
    {
        void Register<T>(Func<T, IValidatorFactory, IValidator<T>> factory) where T : class;
        IValidator GetValidator<T>(T item) where T : class;
    }
}
