﻿using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Business.Validations
{
    public interface IValidator<T> : IValidator
    {
        T Target { get; }
        IValidatorFactory Factory { get; }
    }

    public interface IValidator
    {
        ValidationResult Validate(string prefix = default);
    }
}
