﻿namespace NewHotel.Pos.Hub.Business.IntegrationConfigs
{
    public class MxmTaskConfig
    {
        public string MxmTaskLogsPath { get; set; }
        public int MaxRetries { get; set; }
        public int MxmTaskIntervalInSeconds { get; set; }
        public string MxmUrl { get; set; }
        public bool EnableMxmBackgroundTask { get; set; }
    }
}
