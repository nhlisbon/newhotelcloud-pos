﻿namespace NewHotel.Pos.Hub.Business
{
    public class MxmConfig
    {
        public string CompanyCode { get; set; }
        public string CostCenterCode { get; set; }
        public string StockCode { get; set; }
        public string AlmoxarifadoCode { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string EnvironmentName { get; set; }
    }
}
