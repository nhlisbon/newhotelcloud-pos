﻿using System.Collections.Generic;
using NewHotel.Business;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Common
{
    public class QueryFactory : QueryContainer<QueryFactory>
    {
        public static T Get<T>(IDatabaseManager manager, params object[] args) where T : Query
        {
            var newArgs = new List<object> {manager};
            newArgs.AddRange(args);
            return Instance.Get<T>(newArgs.ToArray());
        }
    }
}