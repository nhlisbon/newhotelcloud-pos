﻿using System.Collections.Generic;
using NewHotel.Business;
using NewHotel.Core;
using NewHotel.Data.Commands;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Common
{
    public class FactoryCommand : CommandContainer<FactoryCommand>
    {
        public static T Get<T>(IDatabaseManager manager, params object[] args) where T : DbCommand
        {
            var newArgs = new List<object> { manager };
            newArgs.AddRange(args);
            return ((BaseCommandContainer)Instance).Get<T>(newArgs.ToArray());
        }
    }
}