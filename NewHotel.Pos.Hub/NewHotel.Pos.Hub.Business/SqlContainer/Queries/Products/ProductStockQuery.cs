﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products
{
	public class ProductStockQuery : Query
	{
		public ProductStockQuery(IDatabaseManager manager) : base(nameof(ProductStockQuery), manager)
		{
		}

		protected override string GetCommandText()
		{
            return @"
select arst.arst_pk,arst.artg_pk,arst.arst_qtds,arst.arst_datr,nvl(arve.arve_qtds,0) arve_qtds
from tnht_arst arst
left join (
    select sum(arve_qtds) arve_qtds,arve.artg_pk,vend.vend_dati
    from tnht_vend vend
    join tnht_arve arve on (vend.vend_pk=arve.vend_pk and arve.arve_anul='0')
    group by arve.artg_pk,vend.vend_dati) arve on (arst.artg_pk=arve.artg_pk and arst.arst_datr=arve.vend_dati)";
		}

		protected override void Initialize()
		{
			Filters.Add("arst_pk", "arst.arst_pk", typeof(Guid));
			Filters.Add("artg_pk", "arst.artg_pk", typeof(Guid));
			Filters.Add("arst_datr", "arst.arst_datr", typeof(DateTime));
		}
	}
}