﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products
{
    public class ProductSlaidersQuery : Query
    {
        public ProductSlaidersQuery(IDatabaseManager manager)
            : base("ProductSlaiders", manager)
        {
        }

        protected override string GetCommandText()
        {
            return @"SELECT ARIM.ARIM_PK, ARIM.ARTG_PK, ARIM.ARIM_PATH
                     FROM TNHT_ARIM ARIM
                     INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARIM.ARTG_PK
                     INNER JOIN TNHT_PVAR PVAR ON PVAR.ARTG_PK = ARTG.ARTG_PK
                     WHERE ARIM.DELETED = '0' AND ARTG.HOTE_PK = :HOTE_PK AND PVAR.IPOS_PK = :IPOS_PK";
        }
    }
}