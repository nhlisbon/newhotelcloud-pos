﻿using NewHotel.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products
{
    public class UpdateOrCreateProductStockQuery : Query
    {
        public UpdateOrCreateProductStockQuery(IDatabaseManager manager) 
            : base(nameof(UpdateProductStockQuery), manager)
        {
        }

        protected override string GetCommandText()
        {
            return @"
                     merge into tnht_arst arst
                     using (select :artg_pk as artg_pk, :arst_qtds as arst_qtds from dual) data
                     on (arst.artg_pk = data.artg_pk)
                     when matched then
                         update set arst_qtds = data.arst_qtds
                         where arst.arst_qtds != data.arst_qtds
                     when not matched then
                         insert (arst_pk, artg_pk, arst_qtds, arst_datr) values (:arst_pk, data.artg_pk, data.arst_qtds, :arst_datr)
            ";
        }

        protected override void Initialize()
        {
            Filters.Add("arst_pk", "arst_pk", typeof(Guid), FilterTypes.Like);
            Filters.Add("arst_datr", "arst_datr", typeof(DateTime), FilterTypes.Like);
            Filters.Add("artg_pk", "artg_pk", typeof(Guid), FilterTypes.Like);
            Filters.Add("arst_qtds", "arst_qtds", typeof(decimal), FilterTypes.Like);
        }
    }
}
