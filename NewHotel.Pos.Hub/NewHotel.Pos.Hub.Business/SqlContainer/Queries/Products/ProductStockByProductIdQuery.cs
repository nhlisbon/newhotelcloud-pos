﻿using NewHotel.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products
{
    public class ProductStockByProductIdQuery : Query
    {
        public ProductStockByProductIdQuery(IDatabaseManager manager)
            : base(nameof(ProductStockByProductIdQuery), manager)
        {
        }

        protected override string GetCommandText()
        {
            return @"
                     select arst_pk, artg_pk, arst_qtds, arst_datr
                     from tnht_arst
                     where artg_pk = :artg_pk
            ";
        }

        protected override void Initialize()
        {
            Filters.Add("artg_pk", "artg_pk", typeof(Guid), FilterTypes.Like);
        }
    }
}
