﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products;

public class AccompanyingProductsQuery(IDatabaseManager manager) : Query("AccompanyingProductsQuery", manager)
{
    protected override string GetCommandText()
    {
        return """
        SELECT
            ACOM.ACOM_ARPA AS ARPA_PK,
            ACOM.ACOM_ARCH AS ARCH_PK
        FROM TNHT_PVAR PVAR
                 INNER JOIN TNHT_ACOM ACOM ON PVAR.ARTG_PK = ACOM.ACOM_ARPA
                 INNER JOIN TNHT_ARTG ARTG ON ACOM.ACOM_ARCH = ARTG.ARTG_PK
        WHERE PVAR.IPOS_PK = :IPOS_PK
          AND ARTG.ARTG_INAC = 0
        """;
    }
    
    protected override void Initialize()
    {
        Filters.Add("ARTG_PK", "PVAR.ARTG_PK", typeof(Guid));
    }
}