﻿using System;
using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products
{
    public class AreasByProductsQuery : Query
    {
        public AreasByProductsQuery()
            : base("AreasByProducts") { }

        public AreasByProductsQuery(IDatabaseManager manager)
            : base("AreasByProducts", manager) { }

        // alex: dado un producto y un punto de venta, retorna la lista de areas de impresion asociadas
        protected override string GetCommandText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select area.area_pk,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = area.lite_pk and mult.lang_pk = :lang_pk) as area_desc,");
            sb.AppendLine("       area.area_colo, area.area_nuco, arar.arar_orde, arar.artg_pk, arar.arar_arma");
            sb.AppendLine("from tnht_arar arar");
            sb.AppendLine("inner join tnht_area area on area.area_pk = arar.area_pk");
            sb.AppendLine("where area.area_inac = :false and arar.ipos_pk = :ipos_pk");
            sb.AppendLine("order by arar.arar_orde");

            return sb.ToString();
        }

        protected override void Initialize()
        {
            Parameters["false"] = false;
            Filters.Add("artg_pk", "arar.artg_pk", typeof(Guid));
        }
    }
}