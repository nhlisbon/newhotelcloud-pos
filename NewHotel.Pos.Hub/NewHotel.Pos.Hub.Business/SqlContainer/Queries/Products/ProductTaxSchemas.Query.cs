﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products
{
    public class ProductTaxSchemasQuery : Query
    {
        public ProductTaxSchemasQuery(IDatabaseManager manager)
            : base("ProductTaxSchemas", manager)
        {
        }

        protected override string GetCommandText()
        {
            return @"SELECT ARTG.ARTG_PK,
                            IMSE.ESIM_PK,
                            ESIM.ESIM_DEFA,
                            IMSE.TIVA_COD1                               AS IVA1_PK,
                            T1.TIVA_COAX                                 AS IVA1_COAX,
                            T1.TIVA_PERC                                 AS IVA1_PERC,
                            (SELECT MULT.MULT_DESC
                             FROM VNHT_MULT MULT
                             WHERE MULT.LITE_PK = T1.LITE_PK
                               AND MULT.LANG_PK = :LANG_PK)              AS IVA1_DESC,
                            IMSE.TIVA_RET1                               AS IVA1_RETE,
                            IMSE.TIVA_COD2                               AS IVA2_PK,
                            T2.TIVA_COAX                                 AS IVA2_COAX,
                            T2.TIVA_PERC                                 AS IVA2_PERC,
                            CASE
                                WHEN T2.TIVA_PK IS NULL THEN NULL
                                ELSE (SELECT MULT.MULT_DESC
                                      FROM VNHT_MULT MULT
                                      WHERE MULT.LITE_PK = T2.LITE_PK
                                        AND MULT.LANG_PK = :LANG_PK) END AS IVA2_DESC,
                            IMSE.TIVA_APL2                               AS IVA2_APPL,
                            IMSE.TIVA_RET2                               AS IVA2_RETE,
                            IMSE.TIVA_COD3                               AS IVA3_PK,
                            T3.TIVA_COAX                                 AS IVA3_COAX,
                            T3.TIVA_PERC                                 AS IVA3_PERC,
                            CASE
                                WHEN T3.TIVA_PK IS NULL THEN NULL
                                ELSE (SELECT MULT.MULT_DESC
                                      FROM VNHT_MULT MULT
                                      WHERE MULT.LITE_PK = T3.LITE_PK
                                        AND MULT.LANG_PK = :LANG_PK) END AS IVA3_DESC,
                            IMSE.TIVA_APL3                               AS IVA3_APPL,
                            IMSE.TIVA_RET3                               AS IVA3_RETE
                    FROM TNHT_ARTG ARTG,
                         TNHT_IMSE IMSE,
                         TNHT_TIVA T1,
                         TNHT_TIVA T2,
                         TNHT_TIVA T3,
                         TNHT_ESIM ESIM
                    WHERE ARTG.ARTG_PK = :ARTG_PK
                      AND ARTG.DELETED = 0
                      AND ARTG.ARTG_PK = IMSE.ARTG_PK
                      AND IMSE.DELETED = 0
                      AND IMSE.TIVA_COD1 = T1.TIVA_PK
                      AND T1.DELETED = 0
                      AND IMSE.TIVA_COD2 = T2.TIVA_PK(+)
                      AND T1.DELETED = 0
                      AND IMSE.TIVA_COD3 = T3.TIVA_PK(+)
                      AND T1.DELETED = 0
                      AND IMSE.ESIM_PK = ESIM.ESIM_PK
                      AND ESIM.DELETED = 0";
        }
    }
}