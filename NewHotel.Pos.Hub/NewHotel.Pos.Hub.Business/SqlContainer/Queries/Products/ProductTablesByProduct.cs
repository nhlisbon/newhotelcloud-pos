﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products
{
    public class ProductTablesByProduct : Query
    {
        public ProductTablesByProduct(IDatabaseManager manager) : base("ProductTablesByProduct", manager)
        {
        }

        protected override string GetCommandText()
        {
            return @"
                SELECT ARTB.ARTB_PK,
                       ARTB.ARTG_PK,
                       ARTB.ARTB_CODI,
                       ARTB.ARTB_CANT,
                       (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = ARTG.LITE_ABRE AND MULT.LANG_PK = :LANG_PK) AS ARTG_ABRE,
                       (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = ARTG.LITE_DESC AND MULT.LANG_PK = :LANG_PK) AS ARTG_DESC,
                       ARTB.TBSP_PK,
                       ARTB.TBSP_ORDER,
                       ARTB.TBSP_MXSE,
                       (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = ARTB.TBSP_DESC AND MULT.LANG_PK = :LANG_PK) AS TBSP_DESC,
                       ARTB.TBSP_DESC AS TBSP_DESC_PK
                FROM TNHT_ARTB ARTB
                         INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARTB.ARTB_CODI
                WHERE ARTB.ARTG_PK = :ARTG_PK
                  AND ARTG.DELETED = 0
                  AND ARTB.DELETED = 0
            ";
        }
    }
}