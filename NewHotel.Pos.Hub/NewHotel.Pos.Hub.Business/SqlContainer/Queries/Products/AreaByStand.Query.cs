﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products
{
    public class AreasByStandQuery : Query
    {
        public AreasByStandQuery()
            : base("AreaByStand") { }

        public AreasByStandQuery(IDatabaseManager manager)
            : base("AreaByStand", manager) { }

        protected override string GetCommandText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select area.area_pk, area.area_colo, area.area_nuco, area.area_inac,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = area.lite_pk and mult.lang_pk = :lang_pk) as area_desc");
            sb.AppendLine("from tnht_area area");

            return sb.ToString();
        }
    }
}