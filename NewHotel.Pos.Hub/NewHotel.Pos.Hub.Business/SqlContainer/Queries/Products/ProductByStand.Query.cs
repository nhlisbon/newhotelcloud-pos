﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products
{
	public class ProductByStandQuery: Query
    {
        public ProductByStandQuery(IDatabaseManager manager)
            : base("ProductByStand", manager)
        {
        }

        protected override string GetCommandText()
        {
            return 
                @"
                    SELECT ARTG.ARTG_PK,
                           ARTG.HOTE_PK,
                           ARTG.ARTG_CODI,
                           (SELECT MULT.MULT_DESC
                            FROM VNHT_MULT MULT
                            WHERE MULT.LITE_PK = ARTG.LITE_ABRE
                              AND MULT.LANG_PK = :LANG_PK) AS ARTG_ABRE,
                           (SELECT MULT.MULT_DESC
                            FROM VNHT_MULT MULT
                            WHERE MULT.LITE_PK = ARTG.LITE_DESC
                              AND MULT.LANG_PK = :LANG_PK) AS ARTG_DESC,
                           (SELECT MULT.MULT_DESC
                            FROM VNHT_MULT MULT
                            WHERE MULT.LITE_PK = GRUP.LITE_PK
                              AND MULT.LANG_PK = :LANG_PK) AS GRUP_DESC,
                           (SELECT MULT.MULT_DESC
                            FROM VNHT_MULT MULT
                            WHERE MULT.LITE_PK = FAMI.LITE_PK
                              AND MULT.LANG_PK = :LANG_PK) AS FAMI_DESC,
                           CASE
                               WHEN ARTG.SFAM_PK IS NULL THEN NULL
                               ELSE
                                   (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = SFAM.LITE_PK AND MULT.LANG_PK = :LANG_PK)
                               END                         AS SFAM_DESC,
                           CASE
                               WHEN ARTG.SEPA_PK IS NULL THEN NULL
                               ELSE
                                   (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = SEPA.LITE_PK AND MULT.LANG_PK = :LANG_PK)
                               END                         AS SEPA_DESC,
                           ARTG.GRUP_PK,
                           ARTG.FAMI_PK,
                           ARTG.SFAM_PK,
                           ARTG.SEPA_PK,
                           ARTG.ARTG_STEP,
                           ARTG.ARTG_IMAG,
                           ARTG.ARTG_COLO,
                           ARTG.ARTG_TIPR,
                           PVAR.FAVO_ORDE,
                           PVAR.SERV_PK,
                           ARTG.ARTG_INAC,
                           ARTG.ARTG_TABL,
                           ARTG.ARTG_PDES
                    FROM VNHT_ARTG ARTG,
                         TNHT_PVAR PVAR,
                         VNHT_GRUP GRUP,
                         VNHT_FAMI FAMI,
                         VNHT_SFAM SFAM,
                         TNHT_SEPA SEPA
                    WHERE ARTG.ARTG_PK = :ARTG_PK
                      AND ARTG.ARTG_PK = PVAR.ARTG_PK
                      AND PVAR.IPOS_PK = :IPOS_PK
                      AND ARTG.GRUP_PK = GRUP.GRUP_PK
                      AND ARTG.FAMI_PK = FAMI.FAMI_PK
                      AND ARTG.SFAM_PK = SFAM.SFAM_PK(+)
                      AND ARTG.SEPA_PK = SEPA.SEPA_PK(+)
                ";
        }
    }
}