﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products
{
    public class PreparationsQuery : Query
    {
        public PreparationsQuery()
            : base("Preparations") { }

        public PreparationsQuery(IDatabaseManager manager)
            : base("Preparations", manager) { }

        /* alex: dado un producto, retorna la lista de preparaciones asociadas al grupo, familia o al producto en si */
        protected override string GetCommandText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select prep.prep_pk,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = prep.lite_pk and mult.lang_pk = :lang_pk) as prep_desc");
            sb.AppendLine("from tnht_prep prep where deleted = '0'");

            return sb.ToString();
        }
    }
}