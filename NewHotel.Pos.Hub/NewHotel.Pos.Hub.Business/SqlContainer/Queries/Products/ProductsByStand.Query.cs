﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products
{
    public class ProductsByStandQuery : Query
    {
        public ProductsByStandQuery()
            : base("ProductsByStand") { }

        public ProductsByStandQuery(IDatabaseManager manager)
            : base("ProductsByStand", manager) { }

        /* alex: productos que se venden en este stand */
        protected override string GetCommandText()
        {
            return @"
                SELECT ARTG.ARTG_PK,
                       ARTG.HOTE_PK,
                       ARTG.ARTG_CODI,
                       ARTG.ARTG_BARC,
                       (SELECT MULT.MULT_DESC
                        FROM VNHT_MULT MULT
                        WHERE MULT.LITE_PK = ARTG.LITE_ABRE
                          AND MULT.LANG_PK = :LANG_PK) AS ARTG_ABRE,
                       (SELECT MULT.MULT_DESC
                        FROM VNHT_MULT MULT
                        WHERE MULT.LITE_PK = ARTG.LITE_DESC
                          AND MULT.LANG_PK = :LANG_PK) AS ARTG_DESC,
                       (SELECT MULT.MULT_DESC
                        FROM VNHT_MULT MULT
                        WHERE MULT.LITE_PK = GRUP.LITE_PK
                          AND MULT.LANG_PK = :LANG_PK) AS GRUP_DESC,
                       (SELECT MULT.MULT_DESC
                        FROM VNHT_MULT MULT
                        WHERE MULT.LITE_PK = FAMI.LITE_PK
                          AND MULT.LANG_PK = :LANG_PK) AS FAMI_DESC,
                       CASE
                           WHEN ARTG.SFAM_PK IS NULL THEN NULL
                           ELSE
                               (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = SFAM.LITE_PK AND MULT.LANG_PK = :LANG_PK)
                           END                         AS SFAM_DESC,
                       CASE
                           WHEN ARTG.SEPA_PK IS NULL THEN NULL
                           ELSE
                               (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = SEPA.LITE_PK AND MULT.LANG_PK = :LANG_PK)
                           END                         AS SEPA_DESC,
                       ARTG.GRUP_PK,
                       ARTG.FAMI_PK,
                       ARTG.SFAM_PK,
                       ARTG.SEPA_PK,
                       ARTG.ARTG_STEP,
                       ARTG.ARTG_IMAG,
                       ARTG.ARTG_COLO,
                       ARTG.ARTG_TIPR,
                       PVAR.FAVO_ORDE,
                       PVAR.SERV_PK,
                       ARTG.ARTG_INAC,
                       ARTG.ARTG_TABL,
                       ARTG.ARTG_PDES,
                       ARTG.SERV_PROD,
                       ARTG.DESC_ISENTO,
                       ARTG.CODE_ISENTO
                FROM TNHT_PVAR PVAR
                         INNER JOIN VNHT_ARTG ARTG ON ARTG.ARTG_PK = PVAR.ARTG_PK
                         INNER JOIN VNHT_GRUP GRUP ON GRUP.GRUP_PK = ARTG.GRUP_PK
                         INNER JOIN VNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                         LEFT JOIN VNHT_SFAM SFAM ON SFAM.SFAM_PK = ARTG.SFAM_PK
                         LEFT JOIN TNHT_SEPA SEPA ON SEPA.SEPA_PK = ARTG.SEPA_PK
                WHERE ARTG.ARTG_INAC = 0
                  AND ARTG.HOTE_PK = :HOTE_PK
                  AND PVAR.IPOS_PK = :IPOS_PK
                  AND (:autoTicket = 0 OR PVAR.PVAR_AUTK = '1')
            ";
        }

        protected override void Initialize()
        {
            base.Initialize();

            Filters
                .Add("ARTG_PK", "ARTG.ARTG_PK", typeof(Guid))
                .Add("ARTG_BARC", "ARTG.ARTG_BARC", typeof(string));
        }
    }
}