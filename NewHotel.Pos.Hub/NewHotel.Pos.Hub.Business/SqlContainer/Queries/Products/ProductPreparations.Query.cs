﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products
{
    public class ProductPreparationsQuery : Query
    {
        public ProductPreparationsQuery(IDatabaseManager manager)
            : base("ProductPreparations", manager)
        {
        }

        protected override string GetCommandText()
        {
            return
                @"
                    SELECT ARTG_PK, PREP_PK, PREP_DESC
                    FROM (
                             (SELECT ARTP.ARTG_PK,
                                     ARTP.PREP_PK,
                                     (SELECT MULT_DESC FROM VNHT_MULT WHERE LITE_PK = PREP.LITE_PK AND LANG_PK = :LANG_PK) PREP_DESC
                              FROM TNHT_ARTG ARTG,
                                   TNHT_ARTP ARTP,
                                   TNHT_PREP PREP
                              WHERE ARTG.ARTG_PK = :ARTG_PK
                                AND ARTG.DELETED = 0
                                AND ARTG.ARTG_PK = ARTP.ARTG_PK
                                AND ARTP.DELETED = 0
                                AND ARTP.PREP_PK = PREP.PREP_PK
                                AND PREP.DELETED = 0)
                             UNION
                             (SELECT ARTG.ARTG_PK,
                                     FAPP.PREP_PK,
                                     (SELECT MULT_DESC FROM VNHT_MULT WHERE LITE_PK = PREP.LITE_PK AND LANG_PK = :LANG_PK) PREP_DESC
                              FROM TNHT_ARTG ARTG,
                                   TNHT_FAMI FAMI,
                                   TNHT_FAPP FAPP,
                                   TNHT_PREP PREP
                              WHERE ARTG.ARTG_PK = :ARTG_PK
                                AND ARTG.DELETED = 0
                                AND ARTG.FAMI_PK = FAMI.FAMI_PK
                                AND FAMI.DELETED = 0
                                AND FAMI.FAMI_PK = FAPP.FAMI_PK
                                AND FAPP.DELETED = 0
                                AND FAPP.PREP_PK = PREP.PREP_PK
                                AND PREP.DELETED = 0)
                             UNION
                             (SELECT ARTG.ARTG_PK,
                                     GRPP.PREP_PK,
                                     (SELECT MULT_DESC FROM VNHT_MULT WHERE LITE_PK = PREP.LITE_PK AND LANG_PK = :LANG_PK) PREP_DESC
                              FROM TNHT_ARTG ARTG,
                                   TNHT_FAMI FAMI,
                                   TNHT_GRUP GRUP,
                                   TNHT_GRPP GRPP,
                                   TNHT_PREP PREP
                              WHERE ARTG.ARTG_PK = :ARTG_PK
                                AND ARTG.DELETED = 0
                                AND ARTG.FAMI_PK = FAMI.FAMI_PK
                                AND FAMI.DELETED = 0
                                AND FAMI.GRUP_PK = GRUP.GRUP_PK
                                AND GRUP.DELETED = 0
                                AND GRUP.GRUP_PK = GRPP.GRUP_PK
                                AND GRPP.DELETED = 0
                                AND GRPP.PREP_PK = PREP.PREP_PK
                                AND PREP.DELETED = 0)
                         )
                    GROUP BY ARTG_PK, PREP_PK, PREP_DESC
                 ";
        }
    }
}