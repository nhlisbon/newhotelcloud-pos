﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products
{
    public class ProductGroupsByStandsQuery : Query
    {
        public ProductGroupsByStandsQuery()
            : base("ProductGroupsByStands") { }

        public ProductGroupsByStandsQuery(IDatabaseManager manager)
            : base("ProductGroupsByStands", manager) { }

        /* alex: grupos asociadas a productos que se venden en este stand */
        protected override string GetCommandText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select grup.grup_pk, grup.grup_imag,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = grup.lite_pk and mult.lang_pk = :lang_pk) as grup_desc");
            sb.AppendLine("from vnht_grup grup");
            sb.AppendLine("where exists (");
            sb.AppendLine("  select 1 from tnht_pvar pvar inner join vnht_artg artg on artg.artg_pk = pvar.artg_pk");
            sb.AppendLine("  where artg.grup_pk = grup.grup_pk and artg.hote_pk = :hote_pk and pvar.ipos_pk = :ipos_pk");
            sb.AppendLine(")");

            return sb.ToString();
        }
    }
}