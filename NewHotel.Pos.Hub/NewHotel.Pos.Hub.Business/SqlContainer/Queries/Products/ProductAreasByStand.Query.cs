﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products
{
    public class ProductAreasByStandQuery : Query
    {
        public ProductAreasByStandQuery(IDatabaseManager manager)
            : base("ProductAreasByStand", manager) { }

        protected override string GetCommandText()
        {
            return
                @"
                    SELECT ARTG.ARTG_PK,
                           AREA.AREA_PK,
                           AREA.AREA_COLO,
                           AREA.AREA_NUCO,
                           ARAR.ARAR_ORDE,
                           (SELECT MULT.MULT_DESC
                            FROM VNHT_MULT MULT
                            WHERE MULT.LITE_PK = AREA.LITE_PK
                              AND MULT.LANG_PK = :LANG_PK) AS AREA_DESC
                    FROM TNHT_ARTG ARTG,
                         TNHT_ARAR ARAR,
                         TNHT_AREA AREA
                    WHERE ARTG.ARTG_PK = :ARTG_PK
                      AND ARTG.DELETED = 0
                      AND ARTG.ARTG_PK = ARAR.ARTG_PK
                      AND ARAR.IPOS_PK = :IPOS_PK
                      AND ARAR.DELETED = 0
                      AND ARAR.AREA_PK = AREA.AREA_PK
                      AND AREA.DELETED = 0
                      AND AREA.AREA_INAC = 0
                    ORDER BY ARAR_ORDE
                  ";
        }       
    }
}