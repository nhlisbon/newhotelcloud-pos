﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products
{
    public class ProductFamilyByStandsQuery : Query
    {
        public ProductFamilyByStandsQuery()
            : base("ProductFamilyByStands") { }

        public ProductFamilyByStandsQuery(IDatabaseManager manager)
            : base("ProductFamilyByStands", manager) { }

        /* alex: familias asociadas a productos que se venden en este stand */
        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select fami.fami_pk, (select mult.mult_desc from vnht_mult mult where mult.lite_pk = fami.lite_pk and mult.lang_pk = :lang_pk) as fami_desc");
            sb.AppendLine("from vnht_fami fami");
            sb.AppendLine("where exists (");
            sb.AppendLine("   select 1 from tnht_pvar pvar, vnht_artg artg where pvar.artg_pk = artg.artg_pk");
            sb.AppendLine("   and artg.fami_pk = fami.fami_pk and artg.hote_pk = :hote_pk and pvar.ipos_pk = :ipos_pk)");

            return sb.ToString();
        }
    }
}
