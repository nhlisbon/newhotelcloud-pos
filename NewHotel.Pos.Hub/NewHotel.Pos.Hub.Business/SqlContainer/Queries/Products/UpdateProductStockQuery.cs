﻿using NewHotel.Core;
using System;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products
{
    public class UpdateProductStockQuery : Query
    {
        public UpdateProductStockQuery(IDatabaseManager manager) 
            : base(nameof(UpdateProductStockQuery), manager)
        {
        }

        protected override string GetCommandText()
        {
            return @"update tnht_arst set arst_qtds = :quantity where artg_pk = :productId";
        }

        protected override void Initialize()
        {
            Filters.Add("productId", "arst.artg_pk", typeof(Guid));
            Filters.Add("quantity", "arst.arst_qtds", typeof(decimal));
        }
    }
}
