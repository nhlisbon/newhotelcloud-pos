﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products
{
    public class ProductSubFamilyByStandsQuery : Query
    {
        public ProductSubFamilyByStandsQuery()
            : base("ProductSubFamilyByStands") { }

        public ProductSubFamilyByStandsQuery(IDatabaseManager manager)
            : base("ProductSubFamilyByStands", manager) { }

        /* alex: subfamilias asociadas a productos que se venden en este stand */
        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select sfam.sfam_pk, (select mult.mult_desc from vnht_mult mult where mult.lite_pk = sfam.lite_pk and mult.lang_pk = :lang_pk) as sfam_desc");
            sb.AppendLine("from vnht_sfam sfam");
            sb.AppendLine("where exists (");
            sb.AppendLine("   select 1 from tnht_pvar pvar, vnht_artg artg where pvar.artg_pk = artg.artg_pk");
            sb.AppendLine("   and artg.sfam_pk = sfam.sfam_pk and artg.hote_pk = :hote_pk and pvar.ipos_pk = :ipos_pk)");

            return sb.ToString();
        }
    }
}
