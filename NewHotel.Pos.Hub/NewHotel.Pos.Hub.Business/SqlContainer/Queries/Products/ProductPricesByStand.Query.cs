﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products
{
    public class ProductPricesByStandQuery(IDatabaseManager manager) : Query("ProductPricesByStand", manager)
    {
        protected override string GetCommandText()
        {
            return """
                   WITH TEMP_TPPE AS (SELECT TPPE.TPRG_PK,
                                             TPRL.ARTG_PK,
                                             TPPE.TPPE_DAIN,
                                             TPPE.TPPE_DAFI,
                                             TPRL.TPRL_VALOR
                                      FROM TNHT_TPPE TPPE
                                               INNER JOIN TNHT_TPRL TPRL ON TPRL.TPPE_PK = TPPE.TPPE_PK)
                   SELECT PVAR.ARTG_PK,
                          TPPE_STDR.TPRL_VALOR                                                                                        AS STDR_VALO,
                          TPPE_CINT.TPRL_VALOR                                                                                        AS CINT_VALO,
                          TPPE_PENS.TPRL_VALOR                                                                                        AS PENS_VALO,
                          TPPE_ACOM.TPRL_VALOR                                                                                        AS ACOM_VALOR
                   FROM TNHT_PVAR PVAR
                            INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = PVAR.ARTG_PK
                            INNER JOIN TNHT_IPOS IPOS ON IPOS.IPOS_PK = PVAR.IPOS_PK
                            LEFT JOIN TEMP_TPPE TPPE_STDR ON TPPE_STDR.ARTG_PK = PVAR.ARTG_PK
                       AND TPPE_STDR.TPRG_PK = IPOS.TPRG_STDR
                       AND TRUNC(IPOS.IPOS_FCTR) BETWEEN TPPE_STDR.TPPE_DAIN AND TPPE_STDR.TPPE_DAFI
                            LEFT JOIN TEMP_TPPE TPPE_CINT ON TPPE_CINT.ARTG_PK = PVAR.ARTG_PK
                       AND TPPE_CINT.TPRG_PK = IPOS.TPRG_CINT
                       AND TRUNC(IPOS.IPOS_FCTR) BETWEEN TPPE_CINT.TPPE_DAIN AND TPPE_CINT.TPPE_DAFI
                            LEFT JOIN TEMP_TPPE TPPE_PENS ON TPPE_PENS.ARTG_PK = PVAR.ARTG_PK
                       AND TPPE_PENS.TPRG_PK = IPOS.TPRG_PENS
                       AND TRUNC(IPOS.IPOS_FCTR) BETWEEN TPPE_PENS.TPPE_DAIN AND TPPE_PENS.TPPE_DAFI
                            LEFT JOIN TEMP_TPPE TPPE_ACOM ON TPPE_ACOM.ARTG_PK = PVAR.ARTG_PK
                       AND TPPE_ACOM.TPRG_PK = IPOS.TPRG_ACOM
                       AND TRUNC(IPOS.IPOS_FCTR) BETWEEN TPPE_ACOM.TPPE_DAIN AND TPPE_ACOM.TPPE_DAFI
                   WHERE ARTG.DELETED = 0
                     AND PVAR.DELETED = 0
                     AND IPOS.DELETED = 0
                     AND PVAR.IPOS_PK = :IPOS_PK
                   """;
        }
    }
}