﻿using System;
using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products
{
    public class PreparationsByProductsQuery : Query
    {
        public PreparationsByProductsQuery()
            : base("PreparationsByProducts") { }

        public PreparationsByProductsQuery(IDatabaseManager manager)
            : base("PreparationsByProducts", manager) { }

        /* alex: dado un producto, retorna la lista de preparaciones asociadas al grupo, familia o al producto en si */
        protected override string GetCommandText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select temp.artg_pk, temp.prep_pk,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = prep.lite_pk and mult.lang_pk = :lang_pk) as prep_desc");
            sb.AppendLine("from (");
            sb.AppendLine("  select artp.artg_pk, artp.prep_pk");
            sb.AppendLine("  from tnht_artp artp");
            sb.AppendLine("  union");
            sb.AppendLine("  select artg.artg_pk, grpp.prep_pk");
            sb.AppendLine("  from tnht_grpp grpp");
            sb.AppendLine("  inner join tnht_fami fami on fami.grup_pk = grpp.grup_pk");
            sb.AppendLine("  inner join tnht_artg artg on artg.fami_pk = fami.fami_pk");
            sb.AppendLine("  union");
            sb.AppendLine("  select artg.artg_pk, fapp.prep_pk");
            sb.AppendLine("  from tnht_fapp fapp");
            sb.AppendLine("  inner join tnht_artg artg on artg.fami_pk = fapp.fami_pk");
            sb.AppendLine(") temp inner join tnht_prep prep on prep.prep_pk = temp.prep_pk");

            return sb.ToString();
        }

        protected override void Initialize()
        {
            Filters.Add("artg_pk", "temp.artg_pk", typeof(Guid));
        }
    }
}