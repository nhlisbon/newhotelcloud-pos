﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Admin
{
    public class CurrencyExchangeQuery : Query
    {
        public new const string Name = "CurrencyExchange.Query";

        public const string DateWorkParam = "DATE_WORK";

        public CurrencyExchangeQuery()
            : base(Name) { }

        public CurrencyExchangeQuery(IDatabaseManager manager)
            : base(Name, manager) { }

        protected override string GetCommandText()
        {
            return @"SELECT CAMB_PK, CAMB_DATE, CAMB_VALO, HUNMO_PK
                     FROM   TNHT_CAMB
                     WHERE  CAMB_DATE = :DATE_WORK";
        }
    }
}
