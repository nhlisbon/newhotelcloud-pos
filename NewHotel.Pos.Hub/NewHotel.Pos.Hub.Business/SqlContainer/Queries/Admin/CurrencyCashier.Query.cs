﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Admin
{
    public class CurrencyCashierQuery : Query
    {
        public new const string Name = "CurrencyCashier.Query";
        public const string HoteIdParam = "HOTE_PK";

        public CurrencyCashierQuery()
            : base(Name) { }

        public CurrencyCashierQuery(IDatabaseManager manager)
            : base(Name, manager) { }

        protected override string GetCommandText()
        {
            return @"SELECT HUNMO_PK, UNMO_PK, UNMO_SYMB, CAMB_MULT, UNMO_BASE FROM TNHT_HUNMO WHERE HOTE_PK = :HOTE_PK AND UNMO_CAJA = '1'";
        }
    }
}