﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Lines.Ticket;

public class LoadPreparationsByTicketQuery: Query
{
    public LoadPreparationsByTicketQuery(string name, IDatabaseManager manager) : base(name, manager)
    {
    }

    public LoadPreparationsByTicketQuery(string name) : base(name)
    {
    }
    
    protected override string GetCommandText()
    {
        
        return """
               SELECT
                   ANUL_PAID,
                   ANUL_UTIL,
                   AREA_PK,
                   ARTG_PK,
                   ARVE_ACOM,
                   ARVE_ADIC,
                   ARVE_ANUL,
                   ARVE_COST,
                   ARVE_DACR,
                   ARVE_DEAU,
                   ARVE_DESC,
                   ARVE_EMAR,
                   ARVE_FIMA,
                   ARVE_HPPY,
                   ARVE_IMAR,
                   ARVE_IMPR,
                   ARVE_INMA,
                   ARVE_ITEM,
                   ARVE_IVA2,
                   ARVE_IVA3,
                   ARVE_IVAS,
                   ARVE_LMOD,
                   ARVE_MPRI,
                   ARVE_NPAX,
                   ARVE_OBSM,
                   ARVE_OBSV,
                   ARVE_PDES,
                   ARVE_PK,
                   ARVE_POR1,
                   ARVE_POR2,
                   ARVE_POR3,
                   ARVE_QTDS,
                   ARVE_RECA,
                   ARVE_TIPL,
                   ARVE_TOTL,
                   ARVE_VLIQ,
                   ARVE_VSDE,
                   CAJA_PK,
                   CODE_ISTO,
                   DESC_ISTO,
                   IPOS_PK,
                   IVAS_COD2,
                   IVAS_COD3,
                   IVAS_CODI,
                   MAPR_DESC,
                   SEPA_MANU,
                   SEPA_PK,
                   TIDE_PK,
                   UTIL_PK,
                   VEND_PK
               FROM
                   TNHT_ARVE
               WHERE
                   VEND_PK = :VEND_PK
               """;
    }
}