﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Stands
{
    public class StandsForTransferQuery: Query
    {
        public StandsForTransferQuery()
            : base("StandsForTransfer") { }

        public StandsForTransferQuery(IDatabaseManager manager)
            : base("StandsForTransfer", manager) { }

        /* alex: puntos de venta y sus propiedades que estan disponibles para una determinada caja */
        protected override string GetCommandText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select distinct ipos.ipos_pk,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = ipos.lite_desc and mult.lang_pk = :lang_pk) as ipos_desc");
            sb.AppendLine("from tnht_capv capv");
            sb.AppendLine("inner join tnht_ipos ipos on ipos.ipos_pk = capv.ipos_pk and ipos.ipos_inac = '0'");
            sb.AppendLine("inner join tnht_caja caja on caja.caja_pk = capv.caja_pk and caja.deleted = '0'");

            return sb.ToString();
        }
    }
}
