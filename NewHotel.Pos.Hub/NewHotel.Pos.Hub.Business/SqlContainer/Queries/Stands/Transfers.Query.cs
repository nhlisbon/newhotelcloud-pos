﻿using System;
using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Stands
{
    public class TransfersQuery : Query
    {
        public TransfersQuery() : base("Transfers")
        {
        }
        
        public TransfersQuery(IDatabaseManager manager) : base("Transfers", manager)
        {
        }
        
        protected override string GetCommandText()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("select titr.titr_pk, titr.caja_dest, titr.caja_orig, titr.ipos_dest, titr.ipos_orig, titr.vend_tran as vend_orig, titr.vend_acep as vend_dest,");
            builder.AppendLine("        titr.vend_tran, titr.vend_acep, titr.tran_date, titr.tran_time, titr.acep_date, titr.acep_time, titr.util_tran, titr.util_acep,");
            builder.AppendLine("        (select mult_desc from vnht_mult mult where mult.lang_pk = :lang_pk and mult.lite_pk = caja_orig.lite_pk) as caor_desc,");
            builder.AppendLine("        (select mult_desc from vnht_mult mult where mult.lang_pk = :lang_pk and mult.lite_pk = caja_dest.lite_pk) as cade_desc,");
            builder.AppendLine("        (select mult_desc from vnht_mult mult where mult.lang_pk = :lang_pk and mult.lite_pk = stand_orig.lite_desc) as stor_desc,");
            builder.AppendLine("        (select mult_desc from vnht_mult mult where mult.lang_pk = :lang_pk and mult.lite_pk = stand_dest.lite_desc) as stde_desc,");
            builder.AppendLine("        case when titr.acep_date is not null then 1 else 0 end as titr_acep,");
            builder.AppendLine("        arve.arve_totl as tick_valo, util_tran.util_desc as uttr_desc, util_acep.util_desc as utac_desc,");
            builder.AppendLine("        vend_orig.vend_seri || '/' || to_char(vend_orig.vend_codi) as vdor_desc,");
            builder.AppendLine("        vend_dest.vend_seri || '/' || to_char(vend_dest.vend_codi) as vdde_desc");
            builder.AppendLine("from    tnht_titr titr, tnht_caja caja_orig, tnht_ipos stand_orig, tnht_vend vend_orig, tnht_util util_tran,");
            builder.AppendLine("        tnht_caja caja_dest, tnht_ipos stand_dest, tnht_vend vend_dest, tnht_util util_acep,");
            builder.AppendLine("          (select arve.vend_pk, sum(arve_totl) arve_totl from  tnht_arve arve, tnht_vend vend");
            builder.AppendLine("           where arve.vend_pk = vend.vend_pk and vend.hote_pk = :hote_pk and arve.arve_anul in (0, 2)");
            builder.AppendLine("           group by arve.vend_pk) arve");
            builder.AppendLine("where   titr.caja_orig = caja_orig.caja_pk");
            builder.AppendLine("and     caja_orig.hote_pk = :hote_pk");
            builder.AppendLine("and     titr.ipos_orig = stand_orig.ipos_pk");
            builder.AppendLine("and     titr.vend_tran = vend_orig.vend_pk");
            builder.AppendLine("and     vend_orig.vend_pk = arve.vend_pk");
            builder.AppendLine("and     titr.util_tran = util_tran.util_pk");
            builder.AppendLine("and     titr.ipos_dest = stand_dest.ipos_pk(+)");
            builder.AppendLine("and     titr.caja_dest = caja_dest.caja_pk(+)");
            builder.AppendLine("and     titr.vend_acep = vend_dest.vend_pk(+)");
            builder.AppendLine("and     titr.util_acep = util_acep.util_pk(+)");
            return builder.ToString();
        }
        
        protected override void Initialize()
        {
            base.Filters.AddTranslation("stor_desc", "stand_orig.lite_desc").AddTranslation("stde_desc", "stand_dest.lite_desc").AddTranslation("caor_desc", "caja_orig.lite_pk").AddTranslation("cade_desc", "caja_dest.lite_pk").Add("caja_dest", "titr.caja_dest", typeof(Guid)).Add("caja_orig", "titr.caja_orig", typeof(Guid)).Add("ipos_dest", "titr.ipos_dest", typeof(Guid)).Add("ipos_orig", "titr.ipos_orig", typeof(Guid)).Add("titr_acep", "(case when(TITR.ACEP_DATE is not null) then 1 else 0 end)", typeof(bool)).Add("tran_date", "titr.tran_date", typeof(DateTime));
            base.Sorts.AddAsc("tran_date");
        }
    }
}
