﻿using System;
using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Stands
{
    public class TablesQuery : Query
    {
         public TablesQuery()
            : base("Tables") { }

         public TablesQuery(IDatabaseManager manager)
             : base("Tables", manager) { }

         protected override string GetCommandText()
         {
             StringBuilder sb = new StringBuilder();

             sb.AppendLine("select mesa.mesa_pk, mesa.mesa_desc, mesa.salo_pk, mesa.mesa_paxs, mesa.mesa_type, mesa.mesa_row, mesa.mesa_colu, mesa.mesa_widt, mesa.mesa_heig");
             sb.AppendLine("from tnht_mesa mesa");
             //sb.AppendLine("where mesa.salo_pk = :salo_pk");
             
             return sb.ToString();
         }

        protected override void Initialize()
        {
            base.Initialize();

			Filters
				.Add("mesa_desc", "mesa.mesa_desc", typeof(string))
				.Add("salo_pk", "mesa.salo_pk", typeof(Guid))
                ;

			Sorts.AddAsc("mesa_desc");
        }
    }
}
