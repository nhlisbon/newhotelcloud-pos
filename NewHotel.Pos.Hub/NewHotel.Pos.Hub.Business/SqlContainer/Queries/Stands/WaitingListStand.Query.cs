﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Stands
{
    public class WaitingListStandQuery : Query
    {
        public WaitingListStandQuery()
            : base("WaitingListStandQuery") { }

        public WaitingListStandQuery(IDatabaseManager manager)
            : base("WaitingListStandQuery", manager)
        {
        }

        protected override string GetCommandText()
        {
            return @"SELECT WLST.WLST_PK,   WLST.IPOS_PK,    WLST.SALO_PK, WLST.WLST_DARE, 
                            WLST.WLST_NAME, WLST.WLST_PHONE, WLST.WLST_PAXS
                     FROM TNHT_WLST WLST";
        }

        protected override void Initialize()
        {
            Filters
                .Add("ipos_pk", "WLST.IPOS_PK", typeof(Guid))
                .Add("salo_pk", "WLST.SALO_PK", typeof(Guid))
                .Add("wlst_paxs", "WLST.WLST_PAXS", typeof(int))
                .Add("wlst_name", "WLST.WLST_NAME", typeof(string), FilterTypes.Like)
                .Add("wlst_phone", "WLST.WLST_PHONE", typeof(string), FilterTypes.Like);

            Sorts.AddAsc("WLST_DARE");
        }
    }
}
