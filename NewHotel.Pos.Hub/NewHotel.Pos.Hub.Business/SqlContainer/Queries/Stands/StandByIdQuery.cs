﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Stands
{
	public class StandByIdQuery : Query
    {
        public const string StandIdParam = "IPOS_PK";
        public const string LanguageIdParam = "LANG_PK";
        public const string DescriptionColumn = "IPOS_DESC";

        public StandByIdQuery()
            : base("StandByID") { }

		public StandByIdQuery(IDatabaseManager manager)
            : base("StandByID", manager) { }

        protected override string GetCommandText()
		{
			return @"
				SELECT IPOS.IPOS_PK,
				       MULT1.MULT_DESC       AS IPOS_ABRE,
				       MULT2.MULT_DESC       AS IPOS_DESC,
				       TRUNC(IPOS.IPOS_FCTR) AS IPOS_FCTR,
				       IPOS.IPOS_TUTR,
				       IPOS.CTRL_PK,
				       IPOS.TPRG_STDR,
				       IPOS.TPRG_CINT,
				       IPOS.TPRG_CIPO,
				       IPOS.TPRG_PENS,
				       IPOS.TPRG_PEPO,
				       IPOS.CNFG_TICK,
				       IPOS.CNFG_BOLE,
				       IPOS.CNFG_FACT,
				       IPOS.CNFG_COMP,
				       IPOS.IPOS_CCOP,
				       IPOS.IPOS_CKAB,
				       IPOS.IPOS_KITI,
				       IPOS.IPOS_COMP,
				       IPOS.IPOS_TISE,
				       IPOS.IPOS_TIPE,
				       IPOS.IPOS_TIVL,
				       IPOS.IPOS_TIAT,
				       IPOS.IPOS_TION,
				       IPOS.MENU_MON,
				       IPOS.MENU_TUE,
				       IPOS.MENU_WED,
				       IPOS.MENU_THU,
				       IPOS.MENU_FRI,
				       IPOS.MENU_SAT,
				       IPOS.MENU_SUN,
				       IPOS.IPOS_SPTA,
				       IPOS.RESE_CHST,
                       IPOS.HOTE_PK,
                       IPOS.IPOS_MEAL,
                       IPOS.IPOS_DEPO,
                       IPOS.IPOS_ROOM,
                       IPOS.IPOS_HOUSE,
					   UNMO.UNMO_PK UNMO_BASE,
                       TPRG.UNMO_PK,
                       case UNMO.CAMB_MULT when '1' then CAMB.CAMB_VALO else (case CAMB.CAMB_VALO when 0 then 0 else round(1/CAMB.CAMB_VALO,12) end) end CAMB_VALO,
                       IPOS.ASK_CODE
				FROM TNHT_IPOS IPOS
						 LEFT JOIN (SELECT U.*, ROW_NUMBER() OVER (ORDER BY UNMO_PK) RN FROM TNHT_HUNMO U  WHERE UNMO_BASE='1') UNMO ON UNMO.HOTE_PK=IPOS.HOTE_PK AND UNMO.RN=1
						 LEFT JOIN VNHT_MULT MULT1 ON MULT1.LITE_PK = IPOS.LITE_ABRE AND MULT1.LANG_PK = :LANG_PK
						 LEFT JOIN VNHT_MULT MULT2 ON MULT2.LITE_PK = IPOS.LITE_DESC AND MULT2.LANG_PK = :LANG_PK
                         LEFT JOIN TNHT_TPRG TPRG ON TPRG.TPRG_PK = IPOS.TPRG_STDR
                         LEFT JOIN (
                            select ip.ipos_pk,hu.hunmo_pk,hu.hote_pk,hu.unmo_pk,hu.unmo_base, cm.camb_pk,cm.camb_date,cm.camb_valo, row_number() over (partition by hu.unmo_base order by cm.camb_date desc) rn
                              from tnht_hunmo hu
                              join tnht_camb cm on hu.hunmo_pk=cm.hunmo_pk
                              join tnht_ipos ip on ip.hote_pk=hu.hote_pk and ip.ipos_fctr>=cm.camb_date
                              where hu.unmo_base='0'
                             order by cm.camb_date desc) CAMB on CAMB.ipos_pk=IPOS.ipos_pk and CAMB.UNMO_PK = TPRG.UNMO_PK and CAMB.RN=1
				WHERE IPOS.IPOS_PK = :IPOS_PK
				  AND IPOS.IPOS_INAC = '0'
			";
		}
	}
}