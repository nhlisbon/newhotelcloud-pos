﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Stands
{
    public class SaloonsByStandQuery:Query
    {
          public SaloonsByStandQuery()
            : base("SaloonsByStand") { }

          public SaloonsByStandQuery(IDatabaseManager manager)
              : base("SaloonsByStand", manager) { }

         protected override string GetCommandText()
         {
             StringBuilder sb = new StringBuilder();

             sb.AppendLine("select salo.salo_pk, (select mult_desc from vnht_mult where lite_pk = salo.lite_pk and lang_pk = :lang_pk) as salo_desc,");
             sb.AppendLine("       salo.salo_widt, salo.salo_heig, salo.mesa_free, salo.mesa_ocup, salo.salo_imag, salo.salo_smok, salo.salo_kids, ipsl.ipsl_orde");
             sb.AppendLine("from   tnht_salo salo, tnht_ipsl ipsl");
             sb.AppendLine("where  ipsl.salo_pk = salo.salo_pk");
             sb.AppendLine("and    ipsl.ipos_pk = :ipos_pk");
             
             return sb.ToString();
         }

        protected override void Initialize()
        {
            base.Initialize();

            Sorts.AddAsc("ipsl_orde");
        }
    }
}
