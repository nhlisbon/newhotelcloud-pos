﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Stands
{
    public class UsersQuery : Query
    {
        public new const string Name = "Users";
        public const string UserCodeFilter = "USER_CODE";
        public const string UserIdFilter = "util_pk";
        public const string DescriptionColumn = "UTIL_DESC";

        public UsersQuery()
            : base(Name) { }

        public UsersQuery(IDatabaseManager manager)
            : base(Name, manager) { }

        /* alex: dame todos los usuarios, obligado aplicar uno de los filtros */
        protected override string GetCommandText()
        {
            return @"SELECT UTIL_PK, UTIL_DESC, UTIL_LOGIN, UTIL_PASS, UTIL_CODE, UTIL_INTE
                     FROM TNHT_UTIL WHERE  UTIL_INAC = '0'";
        }

        protected override void Initialize()
        {
            Filters
                .Add("util_login", "upper(util_login)", typeof(string))
                .Add(UserCodeFilter, "util_code", typeof(string))
                .Add("util_pk", "tnht_util.util_pk", typeof(Guid));
        }
    }
}