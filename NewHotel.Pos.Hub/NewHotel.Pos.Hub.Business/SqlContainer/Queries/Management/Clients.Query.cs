﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Management
{
    public class ClientsQuery : Query
    {
        public new const string Name = "Clients.Query";

        public const string ClientIdFilter = "CLIENT_ID_FILTER";
        public const string ClientNameFilter = "CLIENT_NAME_FILTER";
        public const string ClientFiscalNumberFilter = "CLIENT_FISCALNUM_FILTER";
        public const string ClientCountryFilter = "CLIENT_COUNTRY_FILTER";        

        public ClientsQuery()
            : base(Name) { }

        public ClientsQuery(IDatabaseManager manager)
            : base(Name, manager) { }

        protected override string GetCommandText()
        {
            return @"select clie.clie_pk, clie.clie_nome, clie.clie_caux, clie.fisc_numb, clie.home_phone,
                            clie.fisc_addr1, clie.fisc_addr2, clie.fisc_door, clie.fisc_loca, clie.fisc_copo,
                            dist.dist_caux, dist.dist_desc, comu.comu_caux, clie.regi_fisc, clie.email_addr,
                            clie.naci_pk, mult.mult_desc as naci_desc, clie.enti_fino, clie.clie_deau,
                            clie.clie_pass, clie.clie_iden, clie.clie_resd, clie.clie_driv,
                            clie.fisc_corf, clie.fisc_cotr,
                            case when clie.clie_type = 11 then :true else :false end as clie_clie
                     from   tnht_clie clie
                            left join tnht_naci naci on naci.naci_pk = clie.naci_pk
                            left join vnht_mult mult on mult.lite_pk = naci.lite_desc and mult.lang_pk = :lang_pk
                            left join tnht_dist dist on dist.dist_pk = clie.fisc_dist
                            left join tnht_comu comu on comu.comu_pk = dist.comu_pk";
        }

        protected override void Initialize()
        {
            base.Initialize();
            Parameters["true"] = true;
            Parameters["false"] = false;
            Filters.Add(ClientIdFilter, "clie.clie_pk", typeof(Guid));
            Filters.Add(ClientNameFilter, "clie.clie_nome", typeof(string), FilterTypes.Like);
            Filters.Add(ClientFiscalNumberFilter, "clie.fisc_numb", typeof(string), FilterTypes.Like);
            Filters.Add(ClientCountryFilter, "mult.mult_desc", typeof(string), FilterTypes.Like);
            Sorts.AddAsc("clie_nome");
        }
    }
}
