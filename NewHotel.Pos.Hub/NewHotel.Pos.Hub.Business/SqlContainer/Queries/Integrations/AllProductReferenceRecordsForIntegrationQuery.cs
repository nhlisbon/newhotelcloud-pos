﻿using NewHotel.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Integrations
{
    public class AllProductReferenceRecordsForIntegrationQuery : Query
    {
        public AllProductReferenceRecordsForIntegrationQuery()
            : base("AllProductReferenceRecordsForIntegrationQuery")
        {
        }
        public AllProductReferenceRecordsForIntegrationQuery(IDatabaseManager manager)
            : base("AllProductReferenceRecordsForIntegrationQuery", manager)
        {
        }
        protected override string GetCommandText()
        {
            return @"SELECT ARIN.ARIN_PK, ARIN.ARTG_PK, ARIN.INTE_PK, ARIN.ARIN_CODE, ARIN.ARIN_OPCD_ONE, ARIN.ARIN_OPCD_TWO
                     FROM TNHT_ARIN ARIN
                     WHERE ARIN.INTE_PK = :inte_pk
            ";
        }
        protected override void Initialize()
        {
            Filters.Add("inte_pk", "INTE_PK", typeof(Guid))
                   ;
            Sorts.AddAsc("ARIN_CODE");
        }
    }
}
