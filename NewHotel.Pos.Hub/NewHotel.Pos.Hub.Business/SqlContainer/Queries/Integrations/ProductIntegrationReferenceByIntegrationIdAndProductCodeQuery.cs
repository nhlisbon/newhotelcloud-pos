﻿using NewHotel.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Integrations
{
    public class ProductIntegrationReferenceByIntegrationIdAndProductCodeQuery : Query
    {
        public ProductIntegrationReferenceByIntegrationIdAndProductCodeQuery()
            : base("ProductIntegrationReferenceByIntegrationIdAndProductCodeQuery")
        {
        }

        public ProductIntegrationReferenceByIntegrationIdAndProductCodeQuery(IDatabaseManager manager)
            : base("ProductIntegrationReferenceByIntegrationIdAndProductCodeQuery", manager)
        {
        }

        protected override string GetCommandText()
        {
            return @"SELECT ARIN.ARIN_PK, ARIN.ARTG_PK, ARIN.INTE_PK, ARIN.ARIN_CODE, ARIN.ARIN_OPCD_ONE, ARIN.ARIN_OPCD_TWO
                     FROM TNHT_ARIN ARIN
                     WHERE ARIN.ARIN_CODE = :arin_code and ARIN.INTE_PK = :inte_pk
            ";
        }

        protected override void Initialize()
        {
            Filters.Add("arin_code", "ARIN_CODE", typeof(string))
                   .Add("inte_pk", "INTE_PK", typeof(Guid))
                   ;
            Sorts.AddAsc("ARIN_CODE");
        }
    }
}
