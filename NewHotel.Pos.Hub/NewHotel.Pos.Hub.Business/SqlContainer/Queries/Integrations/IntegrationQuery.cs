﻿using NewHotel.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Integrations
{
    public class IntegrationQuery : Query
    {
        public IntegrationQuery()
            : base("IntegrationQuery") { }

        public IntegrationQuery(IDatabaseManager manager)
            : base("IntegrationQuery", manager)
        {
        }

        protected override string GetCommandText()
        {
            return @"SELECT INTE_PK, INTE_NAME, INTE_TYPE, INTE_CONFIG, INTE_ACTI
                     FROM TNHT_INTE
                     WHERE INTE_TYPE = :inte_type
                    "
            ;
        }

        protected override void Initialize()
        {
            Filters.Add("inte_type", "INTE_TYPE", typeof(string), FilterTypes.Like);
            Sorts.AddAsc("INTE_NAME");
        }
    }
}
