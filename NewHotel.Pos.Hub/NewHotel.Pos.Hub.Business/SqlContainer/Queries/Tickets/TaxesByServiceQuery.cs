﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class TaxesByProductQuery : Query
    {
        public new const string Name = "TaxesByProduct.Query";
        public const string HotelIdParam = "HOTE_PK";
        public const string ProductIdFilter = "ARTG_PK";
        public const string TaxSchemaIdFilter = "ESIM_PK";
        public const string LanguageIdParam = "LANG_PK";

        public TaxesByProductQuery()
            : base(Name) { }

        public TaxesByProductQuery(IDatabaseManager manager)
            : base(Name, manager) { }

        protected override string GetCommandText()
        {
            return @"SELECT IMSE.IMSE_PK, IMSE.ARTG_PK, IMSE.ESIM_PK, ESIM.ESIM_DEFA,
                            (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = ESIM.LITE_PK AND MULT.LANG_PK = :LANG_PK) AS ESIM_DESC,
                            IMSE.TIVA_COD1, CASE WHEN SEIM1.SEIM_PK IS NOT NULL THEN SEIM1.SEIM_ABRE || ' ' || TO_CHAR(TIVA1.TIVA_PERC, '99.99') || '%'
                                                 WHEN TIVA1.TIVA_PK IS NOT NULL THEN (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = TIVA1.LITE_PK AND MULT.LANG_PK = :LANG_PK)
                            END AS TIVA_DESC1,
                            IMSE.TIVA_COD2, CASE WHEN SEIM2.SEIM_PK IS NOT NULL THEN SEIM2.SEIM_ABRE || ' ' || TO_CHAR(TIVA2.TIVA_PERC, '99.99') || '%'
                                                 WHEN TIVA2.TIVA_PK IS NOT NULL THEN (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = TIVA2.LITE_PK AND MULT.LANG_PK = :LANG_PK)
                            END AS TIVA_DESC2,
                            IMSE.TIVA_COD3, CASE WHEN SEIM3.SEIM_PK IS NOT NULL THEN SEIM3.SEIM_ABRE || ' ' || TO_CHAR(TIVA3.TIVA_PERC, '99.99') || '%'
                                                 WHEN TIVA3.TIVA_PK IS NOT NULL THEN (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = TIVA3.LITE_PK AND MULT.LANG_PK = :LANG_PK)
                            END AS TIVA_DESC3,
                            IMSE.TIVA_APL2, IMSE.TIVA_APL3, TIVA1.TIVA_PERC AS TIVA_PERC1, TIVA2.TIVA_PERC AS TIVA_PERC2, TIVA3.TIVA_PERC AS TIVA_PERC3,
                            TIVA1.TIVA_COAX AS TIVA_COAX1, TIVA2.TIVA_COAX AS TIVA_COAX2, TIVA3.TIVA_COAX AS TIVA_COAX3
                    FROM TNHT_ESIM ESIM INNER JOIN TNHT_IMSE IMSE ON IMSE.ESIM_PK = ESIM.ESIM_PK
                    LEFT JOIN TNHT_TIVA TIVA1 ON IMSE.TIVA_COD1 = TIVA1.TIVA_PK
                    LEFT JOIN TNHT_SEIM SEIM1 ON TIVA1.SEIM_PK = SEIM1.SEIM_PK 
                    LEFT JOIN TNHT_TIVA TIVA2 ON IMSE.TIVA_COD2 = TIVA2.TIVA_PK
                    LEFT JOIN TNHT_SEIM SEIM2 ON TIVA2.SEIM_PK = SEIM2.SEIM_PK 
                    LEFT JOIN TNHT_TIVA TIVA3 ON IMSE.TIVA_COD3 = TIVA3.TIVA_PK
                    LEFT JOIN TNHT_SEIM SEIM3 ON TIVA3.SEIM_PK = SEIM3.SEIM_PK
                    WHERE IMSE.DELETED = 0 AND ESIM.HOTE_PK = :HOTE_PK";
        }

        protected override void Initialize()
        {
            base.Initialize();

            Filters.Add("IMSE_PK", "IMSE.IMSE_PK", typeof(Guid))
                   .Add("ARTG_PK", "IMSE.ARTG_PK", typeof(Guid))
                   .Add("ESIM_PK", "IMSE.ESIM_PK", typeof(Guid));
            Sorts.AddDesc("ESIM_DEFA");
        }
    }
}
