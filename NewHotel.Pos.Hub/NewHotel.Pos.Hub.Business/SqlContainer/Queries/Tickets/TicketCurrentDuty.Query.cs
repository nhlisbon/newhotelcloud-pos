﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class TicketCurrentDutyQuery : Query
    {
        public TicketCurrentDutyQuery()
            : base("TicketCurrentDuty")
        { }

        public TicketCurrentDutyQuery(IDatabaseManager manager)
            : base("TicketCurrentDuty", manager)
        { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select  duty_pk ");
            sb.AppendLine("from ");
            sb.AppendLine("(");
            sb.AppendLine("    select      duty.duty_pk ");
            sb.AppendLine("    from        tnht_duty duty");
            sb.AppendLine("    where       :curr_datetime between (trunc(:curr_datetime) + (duty.duty_hori - trunc(duty.duty_hori))) ");
            sb.AppendLine("                                   and (case when duty.duty_horf < duty.duty_hori then trunc(:curr_datetime + 1) + (duty.duty_horf - trunc(duty.duty_hori))");
            sb.AppendLine("                                            else trunc(:curr_datetime) + (duty.duty_horf - trunc(duty.duty_hori)) ");
            sb.AppendLine("                                       end)");
            sb.AppendLine("    and         duty.hote_pk = :hote_pk");
            sb.AppendLine("    order by    duty.duty_hori");
            sb.AppendLine(")");
            sb.AppendLine("where rownum <= 1");

            return sb.ToString();
        }
    }
}