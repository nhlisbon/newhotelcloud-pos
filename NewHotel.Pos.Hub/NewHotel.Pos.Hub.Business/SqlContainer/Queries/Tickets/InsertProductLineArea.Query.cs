﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class InsertProductLineAreaQuery : Query
    {
        public InsertProductLineAreaQuery(IDatabaseManager manager) : base("AddProductLineAreasQuery", manager)
        {
        }

        public InsertProductLineAreaQuery() : base("AddProductLineAreasQuery")
        {
        }

        protected override string GetCommandText()
        {
            const string query = @"INSERT INTO TNHT_ARVA (ARVA_PK, ARVE_PK, AREA_PK) VALUES (:ARVA_PK, :ARVE_PK, :AREA_PK)";
            return query;
        }
    }
}