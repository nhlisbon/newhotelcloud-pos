﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class ClientsPositionAttentionsByTicketQuery : Query
    {
         public ClientsPositionAttentionsByTicketQuery()
            : base("ClientsPositionAttentionsByTicket") { }

         public ClientsPositionAttentionsByTicketQuery(IDatabaseManager manager)
             : base("ClientsPositionAttentionsByTicket", manager) { }

        // dame la información de atenciones del cliente por posición dado un ticket
        protected override string GetCommandText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select clat.clat_pk, clat.clve_pk, clat.tiat_pk, clat.tiat_desc");
            sb.AppendLine("from tnht_clat clat inner join tnht_clve clve on clve.clve_pk = clat.clve_pk");
            sb.AppendLine("where clve.vend_pk = :vend_pk");

            return sb.ToString();
        }
    }
}