﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class InvoiceSignQuery : Query
    {
        public InvoiceSignQuery(IDatabaseManager manager)
            : base("InvoiceSign", manager) { }

        public InvoiceSignQuery()
            : base("InvoiceSign") { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select vend.fact_sign,vend.fact_codi from tnht_vend vend ");
            sb.AppendLine("where ");
            sb.AppendLine("vend.fact_seri = :fact_seri and ");
            sb.AppendLine("vend.fact_codi = (select max(fact_codi) from tnht_vend where fact_seri = :fact_seri and fact_codi < :fact_codi) and");
            sb.AppendLine("vend.fact_sign is not null ");
            return sb.ToString();
        }
    }
}
