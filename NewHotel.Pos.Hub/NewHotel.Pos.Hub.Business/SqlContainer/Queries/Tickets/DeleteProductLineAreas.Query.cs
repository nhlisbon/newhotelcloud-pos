﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class DeleteProductLineAreasQuery : Query
    {
        public DeleteProductLineAreasQuery(IDatabaseManager manager) : base("DeleteProductLineAreasQuery", manager)
        {
        }

        public DeleteProductLineAreasQuery() : base("DeleteProductLineAreasQuery")
        {
        }

        protected override string GetCommandText()
        {
            const string query = @"DELETE FROM TNHT_ARVA ARVA WHERE ARVA.ARVE_PK = :ARVE_PK";
            return query;
        }
    }
}