﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets;

public class GetTicketDescriptionsQuery : Query
{
    public GetTicketDescriptionsQuery(IDatabaseManager manager) : base("GetTicketDescriptionsQuery", manager)
    {
    }

    public GetTicketDescriptionsQuery() : base("GetTicketDescriptionsQuery")
    {
    }

    protected override string GetCommandText()
    {
        return """
               SELECT (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = IPOS.LITE_DESC AND MULT.LANG_PK = :LANG_PK) AS IPOS_DESC,
                      (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = CAJA.LITE_PK AND MULT.LANG_PK = :LANG_PK)   AS CAJA_DESC,
                      (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = SALO.LITE_PK AND MULT.LANG_PK = :LANG_PK)   AS SALO_DESC,
                      MESA.MESA_DESC,
                      VEND.VEND_TOTA,
                      CLIE.CLIE_DEAU
               FROM TNHT_VEND VEND
                        INNER JOIN TNHT_IPOS IPOS ON IPOS.IPOS_PK = VEND.IPOS_PK
                        INNER JOIN TNHT_CAJA CAJA ON CAJA.CAJA_PK = VEND.CAJA_PK
                        LEFT JOIN TNHT_CLIE CLIE ON CLIE.CLIE_PK = VEND.CLIE_PK
                        LEFT JOIN TNHT_MESA MESA ON MESA.MESA_PK = VEND.VEND_MESA
                        LEFT JOIN TNHT_SALO SALO ON SALO.SALO_PK = MESA.SALO_PK
               WHERE VEND.VEND_PK = :VEND_PK
               """;
    }
}