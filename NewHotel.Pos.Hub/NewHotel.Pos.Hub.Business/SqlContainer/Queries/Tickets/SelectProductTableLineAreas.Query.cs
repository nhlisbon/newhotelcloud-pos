﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class SelectProductTableLineAreasQuery : Query
    {
        public SelectProductTableLineAreasQuery(IDatabaseManager manager) : base("SelectProductTableLineAreasQuery", manager)
        {
        }

        public SelectProductTableLineAreasQuery() : base("SelectProductTableLineAreasQuery")
        {
        }

        protected override string GetCommandText()
        {
            const string query = @"
                SELECT TABA.TABA_PK, TABA.TABL_PK, TABA.AREA_PK
                FROM TNHT_TABA TABA
                         INNER JOIN TNHT_TABL TABL ON TABA.TABL_PK = TABL.TABL_PK
                         INNER JOIN TNHT_ARVE ARVE ON TABL.ARVE_PK = ARVE.ARVE_PK
                WHERE ARVE.VEND_PK = :VEND_PK
            ";
            return query;
        }
    }
}