﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class ClientsPositionPreferencesByTicketQuery : Query
    {
         public ClientsPositionPreferencesByTicketQuery()
            : base("ClientsPositionPreferencesByTicket") { }

         public ClientsPositionPreferencesByTicketQuery(IDatabaseManager manager)
             : base("ClientsPositionPreferencesByTicket", manager) { }

        // dame la información de preferencias del cliente por posición dado un ticket
        protected override string GetCommandText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select clpr.clpr_pk, clpr.clve_pk, clpr.pref_pk, clpr.pref_desc");
            sb.AppendLine("from tnht_clpr clpr inner join tnht_clve clve on clve.clve_pk = clpr.clve_pk");
            sb.AppendLine("where clve.vend_pk = :vend_pk");

            return sb.ToString();
        }
    }
}