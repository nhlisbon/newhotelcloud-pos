﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class ClientsPositionAllergiesByTicketQuery : Query
    {
         public ClientsPositionAllergiesByTicketQuery()
            : base("ClientsPositionAllergiesByTicket") { }

         public ClientsPositionAllergiesByTicketQuery(IDatabaseManager manager)
             : base("ClientsPositionAllergiesByTicket", manager) { }

        // dame la información de alergias del cliente por posición dado un ticket
        protected override string GetCommandText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select clal.clal_pk, clal.clve_pk, clal.alle_pk, clal.alle_desc");
            sb.AppendLine("from tnht_clal clal inner join tnht_clve clve on clve.clve_pk = clal.clve_pk");
            sb.AppendLine("where clve.vend_pk = :vend_pk");

            return sb.ToString();
        }
    }
}