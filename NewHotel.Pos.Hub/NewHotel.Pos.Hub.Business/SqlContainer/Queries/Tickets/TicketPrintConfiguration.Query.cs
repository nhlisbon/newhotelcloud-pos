﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class TicketPrintConfigurationQuery : Query
    {
         public TicketPrintConfigurationQuery()
            : base("TicketPrintConfiguration") { }

         public TicketPrintConfigurationQuery(IDatabaseManager manager)
             : base("TicketPrintConfiguration", manager) { }

        // definiciones de como imprimir un determinado tipo de documento
        protected override string GetCommandText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select tick.tick_pk, (select mult.mult_desc from vnht_mult mult where mult.lite_pk = tick.lite_pk and mult.lang_pk = :lang_pk) as tick_desc,");
            sb.AppendLine("       tick.tick_ivaf, tick.tick_hora, tick.tick_hcie, tick.tick_sepa, tick.tick_msg1, tick.tick_msg2, tick.tick_sign, tick.tick_line, tick.tick_linp,");
            sb.AppendLine("       tick.tick_lpro, tick.tick_alig, tick.tick_shlo, tick.tick_colu, tick.tick_disc, tick.tick_reca, tick.tick_recv, tick.tick_paym, tick.tick_tips,");
            sb.AppendLine("       tick.tick_copr, tick.tick_coti, tick.tick_larg, tick.tick_logo, tick.tick_tsug, tick.tick_taxd, tick.tick_subt, tick.tick_tyst, tick.tick_ccop,");
            sb.AppendLine("       tick.tick_rcds, tick.tick_fisc,");
            sb.AppendLine("       tick.tick_asgo, tick.tick_arnu, tick.tick_anom, tick.tick_asig, tick.tick_pdde");
            sb.AppendLine("from tnht_tick tick");
            sb.AppendLine("where tick.tick_pk = :tick_pk");

            return sb.ToString();
        }
    }
}