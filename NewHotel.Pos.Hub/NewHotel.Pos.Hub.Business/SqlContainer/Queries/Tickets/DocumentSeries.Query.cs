﻿using System.Text;
using NewHotel.Contracts;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class DocumentSeriesQuery : Query
    {
        public DocumentSeriesQuery()
           : base("DocumentSeries") { }

        public DocumentSeriesQuery(IDatabaseManager manager)
            : base("DocumentSeries", manager) { }

        // dame las series de factura, tickets y mesas para esta caja/stand que esten activas o futuras
        protected override string GetCommandText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select sedo_pk, tido_pk, sedo_ptex, sedo_pnum, sedo_orde,");
            sb.AppendLine("       sedo_nufa, sedo_nufi, sedo_dafi, sedo_seac, sedo_saft");
            sb.AppendLine("from tnht_sedo");
            sb.AppendLine("where sedo_seac in (:active, :future)");
            sb.AppendLine("and ipos_pk = :ipos_pk and caja_pk = :caja_pk");

            return sb.ToString();
        }

        protected override void Initialize()
        {
            Parameters["active"] = SerieStatus.Active;
            Parameters["future"] = SerieStatus.Future;

            Filters.Add("tido_pk", "tido_pk",
                typeof(POSDocumentType), FilterTypes.In,
                new object[]
                {
                    (long)POSDocumentType.Invoice,
                    (long)POSDocumentType.Ballot,
                    (long)POSDocumentType.CashInvoice,
                    (long)POSDocumentType.CashCreditNote,
                    (long)POSDocumentType.Ticket,
                    (long)POSDocumentType.TableBalance
                })
                .Add("sedo_sctg", "sedo_sctg", typeof(bool));

            Sorts.AddAsc("sedo_seac");
            Sorts.AddDesc("sedo_pnum");
        }
    }

    public class SerieValidationCodeQuery : Query
    {
        public SerieValidationCodeQuery()
           : base("SerieValidationCode") { }

        public SerieValidationCodeQuery(IDatabaseManager manager)
            : base("SerieValidationCode", manager) { }

        protected override string GetCommandText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select sedo_cova from tnht_sedo");
            sb.AppendLine("where case when sedo_pnum is null then sedo_ptex when sedo_orde = 691 then sedo_ptex || sedo_pnum else sedo_pnum || sedo_ptex end = :serie");

            return sb.ToString();
        }
    }
}