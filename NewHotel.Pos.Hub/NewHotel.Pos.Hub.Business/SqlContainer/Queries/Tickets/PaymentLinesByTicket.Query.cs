﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class PaymentLinesByTicketQuery : Query
    {
         public PaymentLinesByTicketQuery()
            : base("PaymentLinesByTicket") { }

         public PaymentLinesByTicketQuery(IDatabaseManager manager)
             : base("PaymentLinesByTicket", manager) { }

        /* alex: dame las lineas de pago dado un ticket */
        protected override string GetCommandText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select pave.pave_pk, pave.tire_pk,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lang_pk = :lang_pk and mult.lite_pk = enum.lite_pk) as tire_desc,");
            sb.AppendLine("       pave.fore_pk, case when fore.fore_pk is null then null else nvl(tacr.cacr_desc, (select mult.mult_desc from vnht_mult mult where mult.lang_pk = :lang_pk and mult.lite_pk = fore.lite_desc)) end as fore_desc, fore_caux,");
            sb.AppendLine("       pave.tacr_pk, tacr.tacr_iden,");
            sb.AppendLine("       pave.ccco_pk, case when pave.ccco_pk is null then null else (select ccco_desc from tnht_vend where vend_pk = pave.vend_pk) end as ccco_desc,");
            sb.AppendLine("       pave.hote_pk,");
            sb.AppendLine("       pave.coin_pk, case when coin.coin_pk is null then null else (select mult.mult_desc from vnht_mult mult where mult.lang_pk = :lang_pk and mult.lite_pk = coin.lite_pk) end as coin_desc,");
            sb.AppendLine("       pave.pave_obsv, pave.pave_valo, pave.pave_reci, pave.pave_anul, pave.unmo_pk, pave.camb_online, pave.camb_mult");
            sb.AppendLine("from   tnht_pave pave, tnht_enum enum, tnht_fore fore, vnht_coin coin,");
            sb.AppendLine("           (select tacr.tacr_pk, tacr.cacr_pk, tacr.cacr_vano as tacr_vano, tacr.cacr_vmes as tacr_vmes,");
            sb.AppendLine("                   case");
            sb.AppendLine("                     when tacr.tacr_iden is not null and length(tacr.tacr_iden) > 5 then");
            sb.AppendLine("                        concat(concat(substr(tacr.tacr_iden, 1, 5),'****-****-**'), substr(tacr.tacr_iden, length(tacr.tacr_iden)-2, 2))");
            sb.AppendLine("                     else '****-****-****-****'");
            sb.AppendLine("                   end as tacr_iden,");
            sb.AppendLine("                  (select mult.mult_desc from vnht_mult mult where mult.lang_pk = :lang_pk and mult.lite_pk = cacr.lite_pk) as cacr_desc");
            sb.AppendLine("            from   tnht_tacr tacr, tnht_cacr cacr");
            sb.AppendLine("            where  tacr.cacr_pk = cacr.cacr_pk) tacr");
            sb.AppendLine("where  pave.tire_pk = enum.enum_pk");
            sb.AppendLine("and    pave.vend_pk = :vend_pk");
            sb.AppendLine("and    pave.fore_pk = fore.fore_pk(+)");
            sb.AppendLine("and    pave.tacr_pk = tacr.tacr_pk(+)");
            sb.AppendLine("and    pave.coin_pk = coin.coin_pk(+)");

            return sb.ToString();
        }
    }
}