﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class DocumentSignQuery : Query
    {
        public DocumentSignQuery(IDatabaseManager manager)
            : base("DocumentSign", manager) { }

        public DocumentSignQuery()
            : base("DocumentSign") { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select vend.vend_sign,vend.vend_codi from tnht_vend vend ");
            sb.AppendLine("where ");
            sb.AppendLine("vend.vend_seri = :vend_seri and ");
            sb.AppendLine("vend.vend_codi = (select max(vend_codi) from tnht_vend where vend_seri = :vend_seri and vend_codi < :vend_codi) and");
            sb.AppendLine("vend.vend_sign is not null ");

            return sb.ToString();
        }
    }
}
