﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class DeleteProductTableLineAreasQuery : Query
    {
        public DeleteProductTableLineAreasQuery(IDatabaseManager manager) : base("DeleteProductTableLineAreasQuery", manager)
        {
        }

        public DeleteProductTableLineAreasQuery() : base("DeleteProductTableLineAreasQuery")
        {
        }

        protected override string GetCommandText()
        {
            const string query = @"DELETE FROM TNHT_TABA TABA WHERE TABA.TABL_PK = :TABL_PK";
            return query;
        }
    }
}