﻿using System;
using NewHotel.Core;
using NewHotel.Pos.Hub.Business.Business.Common;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class LocalClosedTicketsQuery : Query
    {
        public new const string Name = "NEWHOTEL.POS.HUB.BUSINESS.QUERYS.LOCALCLOSEDTICKETSQUERY";
        public const string StandIdFilter = "IPOS_PK";
        public const string CashierIdFilter = "CAJA_PK";
        public const string CloseDateFilter = "VEND_DATF";
        public const string LanguageIdParam = "LANG_PK";

        public LocalClosedTicketsQuery()
           : base(Name) { }

        public LocalClosedTicketsQuery(IDatabaseManager manager)
            : base(Name, manager) { }

        protected override string GetCommandText()
        {
            return @"SELECT VEND.VEND_PK, VEND.UNMO_PK, NULL AS PAVE_ROOM, VEND.VEND_ANUL, VEND.VEND_NFFA, VEND.VEND_NFNC, VEND.VEND_IFFA, VEND.VEND_IFNC, VEND.VEND_COAP,
                            VEND.VEND_CODI || '/' || VEND.VEND_SERI AS VEND_NUMBER, VEND.VEND_DATF, VEND.VEND_HORF, VEND.VEND_TOTA AS VEND_VALO, FACT_TIDO AS VEND_TIDO,
                            (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = IPOS.LITE_DESC AND MULT.LANG_PK = :LANG_PK) AS IPOS_DESC,
                            (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = CAJA.LITE_PK AND MULT.LANG_PK = :LANG_PK) AS CAJA_DESC,
                            CASE WHEN VEND.FACT_TIDO = 26 AND VEND.FACT_SERI IS NOT NULL AND VEND.FACT_CODI IS NOT NULL THEN VEND.FACT_CODI || '/' || VEND.FACT_SERI END AS DOCU_BOLE,
                            CASE WHEN VEND.FACT_TIDO = 11 AND VEND.FACT_SERI IS NOT NULL AND VEND.FACT_CODI IS NOT NULL THEN VEND.FACT_CODI || '/' || VEND.FACT_SERI END AS DOCU_FACT,
                            CASE WHEN VEND.NCRE_SERI IS NOT NULL AND VEND.NCRE_CODI IS NOT NULL THEN VEND.NCRE_CODI || '/' || VEND.NCRE_SERI END AS DOCU_NCRE,
                            VEND.CCCO_PK, VEND.CCCO_DESC
                     FROM   TNHT_VEND VEND
                            INNER JOIN TNHT_IPOS IPOS ON IPOS.IPOS_PK = VEND.IPOS_PK
                            INNER JOIN TNHT_CAJA CAJA ON CAJA.CAJA_PK = VEND.CAJA_PK
                     WHERE  VEND.VEND_DATF IS NOT NULL";
        }

        protected override void Initialize()
        {
            base.Initialize();
            Parameters[LanguageIdParam] = BusinessContext.LanguageId;

            Filters.Add(StandIdFilter, "IPOS.IPOS_PK", typeof(Guid));
            Filters.Add(CashierIdFilter, "CAJA.CAJA_PK", typeof(Guid));
            Filters.Add(CloseDateFilter, "VEND.VEND_DATF", typeof(DateTime));
        }
    }
}