﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class InsertProductTableLineAreaQuery : Query
    {
        public InsertProductTableLineAreaQuery(IDatabaseManager manager) : base("InsertProductTableLineAreaQuery", manager)
        {
        }

        public InsertProductTableLineAreaQuery() : base("InsertProductTableLineAreaQuery")
        {
        }

        protected override string GetCommandText()
        {
            const string query = @"INSERT INTO TNHT_TABA (TABA_PK, TABL_PK, AREA_PK) VALUES (:TABA_PK, :TABL_PK, :AREA_PK)";
            return query;
        }
    }
}