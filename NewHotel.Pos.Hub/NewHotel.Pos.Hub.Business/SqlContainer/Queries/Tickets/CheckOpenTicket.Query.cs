﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class CheckOpenTicketQuery : Query
    {
        public CheckOpenTicketQuery()
            : base("CheckOpenTicketQuery") { }

        public CheckOpenTicketQuery(IDatabaseManager manager)
            : base("CheckOpenTicketQuery", manager) { }
        
        protected override string GetCommandText()
        {
            return """
                   SELECT VEND.VEND_PK
                   FROM TNHT_VEND VEND
                   WHERE VEND.VEND_PK = :VEND_PK
                     AND VEND.IPOS_PK = :IPOS_PK
                     AND VEND.CAJA_PK = :CAJA_PK
                     AND VEND.ANUL_DAAN IS NULL
                     AND VEND.VEND_DATF IS NULL
                   """;
        }
    }
}