﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class TicketByStandSaloon : Query
    {
         public TicketByStandSaloon()
            : base("TicketByStandSaloon") { }

         public TicketByStandSaloon(IDatabaseManager manager)
             : base("TicketByStandSaloon", manager) { }

         protected override string GetCommandText()
         {
             StringBuilder sb = new StringBuilder();

             sb.AppendLine("SELECT vend_pk FROM tnht_vend vend");
             sb.AppendLine("WHERE hote_pk = :hote_pk");
             sb.AppendLine("    and caja_pk = :caja_pk");
             sb.AppendLine("    and ipos_pk = :ipos_pk");
             sb.AppendLine("    and :salo_pk in (select salo_pk from tnht_mesa where mesa_pk = vend.vend_mesa)");

             
             return sb.ToString();
         }
    }
}
