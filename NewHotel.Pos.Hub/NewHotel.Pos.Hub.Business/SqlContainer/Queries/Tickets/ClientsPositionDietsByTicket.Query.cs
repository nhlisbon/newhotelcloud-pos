﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class ClientsPositionDietsByTicketQuery : Query
    {
         public ClientsPositionDietsByTicketQuery()
            : base("ClientsPositionDietsByTicket") { }

         public ClientsPositionDietsByTicketQuery(IDatabaseManager manager)
             : base("ClientsPositionDietsByTicket", manager) { }

        // dame la información de dietas del cliente por posición dado un ticket
        protected override string GetCommandText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select cldi.cldi_pk, cldi.clve_pk, cldi.diet_pk, cldi.diet_desc");
            sb.AppendLine("from tnht_cldi cldi inner join tnht_clve clve on clve.clve_pk = cldi.clve_pk");
            sb.AppendLine("where clve.vend_pk = :vend_pk");

            return sb.ToString();
        }
    }
}