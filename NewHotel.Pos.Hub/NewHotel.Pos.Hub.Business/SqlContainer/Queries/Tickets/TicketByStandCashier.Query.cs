﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class TicketByStandCashierQuery : Query
    {
        public TicketByStandCashierQuery()
            : base("TicketByStandCashier") { }

        public TicketByStandCashierQuery(IDatabaseManager manager)
            : base("TicketByStandCashier", manager) { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select      vend.vend_pk,vend.vend_mesa,vend.vend_codi,vend.turn_codi, mesa.mesa_desc, mesa.salo_pk, ");
            sb.AppendLine("            vend.vend_medi,vend.vend_name,vend.vend_seri,vend.vend_open,vend.vend_tota,vend.ipos_pk,vend.vend_datf,vend.caja_pk,");
			sb.AppendLine("            case when exists (select 1 from tnht_titr titr where titr.vend_acep = vend.vend_pk) then 1 else 0 end as vend_acep");
            sb.AppendLine("from        tnht_vend vend left join tnht_mesa mesa on vend.vend_mesa = mesa.mesa_pk");
            sb.AppendLine("where       vend.hote_pk = :hote_pk");
            sb.AppendLine("and         ipos_pk = :ipos_pk");
            sb.AppendLine("and         vend.anul_daan is null");
            sb.AppendLine("and         vend.vend_datf is null");

            return sb.ToString();
        }
    }
}
