﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class ClientsPositionByTicketQuery : Query
    {
        public ClientsPositionByTicketQuery(IDatabaseManager manager)
             : base("ClientsPositionByTicket", manager) { }

        protected override string GetCommandText()
        {
            return """
                   SELECT CLVE_PK, CLIE_PK, CLIE_NAME, CLIE_ROOM, CLIE_NPAX, CLIE_ALLE, CLIE_SEOP
                   FROM TNHT_CLVE
                   WHERE VEND_PK = :VEND_PK
                   """;
        }
    }
}