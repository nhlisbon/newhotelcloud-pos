﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class SelectProductLineAreasQuery : Query
    {
        public SelectProductLineAreasQuery(IDatabaseManager manager) : base("SelectProductLineAreasQuery", manager)
        {
        }

        public SelectProductLineAreasQuery() : base("SelectProductLineAreasQuery")
        {
        }

        protected override string GetCommandText()
        {
            const string query = @"SELECT * FROM TNHT_ARVA WHERE ARVE_PK = :ARVE_PK";
            return query;
        }
    }
}