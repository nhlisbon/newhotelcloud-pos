﻿using NewHotel.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class TicketWithProductQuery : Query
    {
        public TicketWithProductQuery()
            : base("TicketWithProducts") { }

        public TicketWithProductQuery(IDatabaseManager manager)
            : base("TicketWithProducts", manager) { }

        protected override string GetCommandText()
        {

            return @"
            select 
                vend.vend_pk, vend.vend_codi, vend.vend_medi, vend.vend_name, vend.vend_seri, vend.vend_open, vend.vend_tota, vend.ipos_pk,
                vend.vend_datf, vend.caja_pk, vend.turn_codi, mesa.salo_pk, mesa.mesa_desc, mesa.mesa_pk, arve.arve_pk,  artg.artg_codi,
                case when exists (select 1 from tnht_titr titr where titr.vend_acep = vend.vend_pk) then 1 else 0 end as vend_acep,
                arve.arve_qtds, arve.arve_totl, artg.artg_pk, mult.mult_desc
            from tnht_arve arve 
                inner join tnht_artg artg on artg.artg_pk = arve.artg_pk
                inner join vnht_mult mult on (mult.lite_pk = artg.lite_abre and mult.lang_pk = :lang_pk)
                inner join tnht_vend vend on vend.vend_pk = arve.vend_pk
                left join tnht_mesa mesa on mesa.mesa_pk = vend.vend_pk
            where arve.vend_pk = :vend_pk";
        }
    }
}
