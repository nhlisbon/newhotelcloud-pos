﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class ProductLinesByTicketQuery : Query
    {
        public new const string Name = "ProductLinesByTicket.Query";

        public ProductLinesByTicketQuery()
           : base(Name) { }

        public ProductLinesByTicketQuery(IDatabaseManager manager)
            : base(Name, manager) { }

        /* alex: dame las lineas de producto dado un ticket */
        protected override string GetCommandText()
        {
            return """
                   SELECT ARVE.ARVE_PK,
                          ARVE.CAJA_PK,
                          ARVE.IPOS_PK,
                          ARVE.ARVE_QTDS,
                          ARVE.ARVE_VLIQ,
                          ARVE.ARVE_COST,
                          ARVE.ARVE_TOTL,
                          ARVE.ARVE_VSDE,
                          ARVE.ARVE_DESC,
                          ARVE.ARVE_PDES,
                          ARVE.ARVE_RECA,
                          ARVE.UTIL_PK,
                          UTIL.UTIL_LOGIN,
                          ARVE.ARVE_ANUL,
                          ARVE.ANUL_PAID,
                          ARVE.ANUL_UTIL,
                          ARVE.ARVE_IMPR,
                          ARVE.ARVE_EMAR,
                          ARVE.ARVE_INMA,
                          ARVE.ARVE_FIMA,
                          ARVE.IVAS_CODI                                                                                                                                              AS IVA1_PK,
                          ARVE.ARVE_POR1                                                                                                                                              AS IVA1_PORC,
                          ARVE.ARVE_IVAS                                                                                                                                              AS IVA1_VALO,
                          (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LANG_PK = :LANG_PK AND MULT.LITE_PK = IVA1.LITE_PK)                                                   AS IVA1_DESC,
                          IVA1.TIVA_COAX                                                                                                                                                 IVA1_COAX,
                          ARVE.IVAS_COD2                                                                                                                                              AS IVA2_PK,
                          ARVE.ARVE_POR2                                                                                                                                              AS IVA2_PORC,
                          ARVE.ARVE_IVA2                                                                                                                                              AS IVA2_VALO,
                          ARVE.IVAS_COD3                                                                                                                                              AS IVA3_PK,
                          ARVE.ARVE_POR3                                                                                                                                              AS IVA3_PORC,
                          ARVE.ARVE_IVA3                                                                                                                                              AS IVA3_VALO,
                          CASE WHEN IVA2.TIVA_PK IS NULL THEN NULL ELSE (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LANG_PK = :LANG_PK AND MULT.LITE_PK = IVA2.LITE_PK) END AS IVA2_DESC,
                          CASE WHEN IVA3.TIVA_PK IS NULL THEN NULL ELSE (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LANG_PK = :LANG_PK AND MULT.LITE_PK = IVA3.LITE_PK) END AS IVA3_DESC,
                          ARVE.ARVE_HPPY,
                          ARVE.ARVE_ADIC,
                          ARVE.ARVE_ACOM,
                          ARVE.ARVE_OBSV,
                          ARVE.ARVE_OBSM,
                          ARVE.AREA_PK,
                          CASE
                              WHEN AREA.AREA_PK IS NOT NULL THEN
                                      (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LANG_PK = :LANG_PK AND MULT.LITE_PK = AREA.LITE_PK)
                              END                                                                                                                                                     AS AREA_DESC,
                          ARVE.SEPA_PK,
                          CASE
                              WHEN SEPA.SEPA_PK IS NOT NULL THEN
                                      (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LANG_PK = :LANG_PK AND MULT.LITE_PK = SEPA.LITE_PK)
                              END                                                                                                                                                     AS SEPA_DESC,
                          SEPA.SEPA_ORDE,
                          ARVE.ARTG_PK,
                          ARTG.ARTG_CODI,
                          MULT_ARTG_ABRE.MULT_DESC                                                                                                                                    AS ARTG_ABRE,
                          MULT_ARTG_DESC.MULT_DESC                                                                                                                                    AS ARTG_DESC,
                          ARVE.MAPR_DESC                                                                                                                                              AS MAPR_DESC,
                          ARVE.ARVE_DACR,
                          ARVE.TIDE_PK,

                          tide_desc.mult_desc                                                                                                                                         AS tide_desc,

                          ARTG.ARTG_COLO,
                          ARTG.ARTG_IMAG,
                          ARTG.ARTG_ALCO,
                          ARVE.SEPA_MANU,
                          ARVE.ARVE_MPRI,
                          ARVE.ARVE_TIPL,
                          ARVE.ARVE_DEAU,
                          ARVE.ARVE_ITEM,
                          ARVE.ARVE_NPAX,
                          ARVE.ARVE_LMOD,
                          ARVE.DESC_ISTO,
                          ARVE.CODE_ISTO,
                          ARVE.FROM_SPA
                   FROM TNHT_ARVE ARVE
                            LEFT JOIN tnht_tide tide on tide.tide_pk = arve.tide_pk
                            LEFT JOIN vnht_mult tide_desc on tide_desc.lite_pk = tide.lite_pk and tide_desc.lang_pk=:lang_pk
                            INNER JOIN VNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                            INNER JOIN TNHT_UTIL UTIL ON UTIL.UTIL_PK = ARVE.UTIL_PK
                            INNER JOIN TNHT_TIVA IVA1 ON IVA1.TIVA_PK = ARVE.IVAS_CODI
                            LEFT JOIN TNHT_TIVA IVA2 ON IVA2.TIVA_PK = ARVE.IVAS_COD2
                            LEFT JOIN TNHT_TIVA IVA3 ON IVA3.TIVA_PK = ARVE.IVAS_COD3
                            LEFT JOIN TNHT_SEPA SEPA ON SEPA.SEPA_PK = ARVE.SEPA_PK
                            LEFT JOIN TNHT_AREA AREA ON AREA.AREA_PK = ARVE.AREA_PK
                            LEFT JOIN VNHT_MULT MULT_ARTG_ABRE ON MULT_ARTG_ABRE.LITE_PK = ARTG.LITE_ABRE AND MULT_ARTG_ABRE.LANG_PK = :LANG_PK
                            LEFT JOIN VNHT_MULT MULT_ARTG_DESC ON MULT_ARTG_DESC.LITE_PK = ARTG.LITE_DESC AND MULT_ARTG_DESC.LANG_PK = :LANG_PK
                   WHERE ARVE.VEND_PK = :VEND_PK
                   """;
        }

        protected override void Initialize()
        {
            Sorts.AddAsc("ARVE_ITEM");
        }
    }
}