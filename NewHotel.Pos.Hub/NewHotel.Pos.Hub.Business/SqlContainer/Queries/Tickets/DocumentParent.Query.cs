﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets
{
    public class DocumentParentQuery : Query
    {
        public DocumentParentQuery()
           : base("DocumentParent") { }

        public DocumentParentQuery(IDatabaseManager manager)
            : base("DocumentParent", manager) { }

        protected override string GetCommandText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select tido_pare from tnht_tido where tido_pk = :tido_pk");

            return sb.ToString();
        }
    }
}