﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Settings;

public class GeneralSettingsQuery : Query
{
    public GeneralSettingsQuery(IDatabaseManager manager)
        : base("GeneralSettings", manager) { }

    protected override string GetCommandText()
    {
        return """
               SELECT NPOS.FAST_PAYM,
                      NPOS.PARA_FORE,
                      NPOS.PARA_IVIN, 
                      NPOS.DATR_DASY,
                      NPOS.PARA_ESIM,
                      NPOS.PARA_TICK,
                      NPOS.PARA_MTCO,
                      NPOS.PARA_TICO,
                      NPOS.PARA_NPAS,
                      NPOS.PARA_TISE,
                      NPOS.PARA_TIPE,
                      NPOS.PARA_TIVL,
                      NPOS.PARA_TIAT,
                      NPOS.DRAW_CODE,
                      NPOS.BLOB_PATH,
                      NPOS.OPEN_PLAN,
                      NPOS.NPOS_TIFI,
                      NPOS.COIN_VALO,
                      NPOS.DESC_VALO,
                      NPOS.TICK_FISC,
                      NPOS.NPOS_NCHC,
                      NPOS.NPOS_FAON,
                      NPOS.NPOS_HBOL,
                      NPOS.NPOS_HSCO,
                      NPOS.NPOS_DEAU,
                      NPOS.SIGN_TIPO,
                      NPOS.SIGN_VERS,
                      NPOS.SIGN_TEST,
                      NPOS.FISC_USER,
                      NPOS.FISC_PASW,
                      HOTE.FISC_NUMB,
                      HOTE.REGI_CAUX,
                      NPOS.ANUL_DATR,
                      NPOS.ANUL_MONTH,
                      NPOS.ANUL_DAYS,
                      NPOS.TICK_MAFN,
                      HOTE.NACI_PK,
                      NPOS.SYNC_VEND,
                      NPOS.FAST_PLAN,
                      NPOS.PLAN_ROOM,
                      NPOS.EMIT_SOTK,
                      NPOS.NPOS_TIME,
                      NPOS.NPOS_TIMF,
                      NPOS.PROD_FTQT,
                      NPOS.NPOS_IBTN,
                      NPOS.NPOS_ASGN,
                      NPOS.NPOS_ASEP,
                      NPOS.NPOS_FCON,
                      NPOS.TICK_VECA,
                      HOTE.PAYS_INTALL,
                      NPOS.NPOS_ATSK,
                      NPOS.TICK_NCRE
               FROM TNHT_HOTE HOTE
                        INNER JOIN TCFG_NPOS NPOS ON NPOS.HOTE_PK = HOTE.HOTE_PK
               WHERE HOTE.HOTE_PK = :HOTE_PK
               """;
    }
}