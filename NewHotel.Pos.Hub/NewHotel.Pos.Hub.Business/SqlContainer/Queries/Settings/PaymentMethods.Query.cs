﻿using System;
using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Settings
{
    public class PaymentMethodsQuery : Query
    {
        public PaymentMethodsQuery()
            : base("PaymentMethods") { }

        public PaymentMethodsQuery(IDatabaseManager manager)
            : base("PaymentMethods", manager) { }

        // alex: listado de tipos de pagos
        protected override string GetCommandText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select fore.fore_pk,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = fore.lite_abre and mult.lang_pk = :lang_pk) as fore_abre,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = fore.lite_desc and mult.lang_pk = :lang_pk) as fore_desc,");
            sb.AppendLine("       fore.unmo_pk, fore.fore_cash, fore.fore_cacr, fore.repo_cash, fore.fore_path, fore.fore_orde, fore.fore_cred, fore.fore_debi,");
            sb.AppendLine("       fore.fore_pcds, fore.fore_caux, fore.fore_invo, fore.fore_tick, fore.pays_origin");
            sb.AppendLine("from tnht_fore fore");
            sb.AppendLine("where fore.hote_pk = :hote_pk and fore.fore_inac = '0'");
            // sb.AppendLine("order by fore.fore_orde");

            return sb.ToString();
        }

		protected override void Initialize()
		{
			base.Initialize();

			Filters.Add("FORE_PK", "FORE.FORE_PK", typeof(Guid));
		}
	}
}