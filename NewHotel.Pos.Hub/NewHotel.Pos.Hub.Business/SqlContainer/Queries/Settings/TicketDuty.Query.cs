﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Settings
{
    public class TicketDutyQuery : Query
    {
        public TicketDutyQuery()
            : base("TicketDuty") { }

        public TicketDutyQuery(IDatabaseManager manager)
            : base("TicketDuty", manager) { }

        /* alex: listado de servicios para el ticket */
        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select duty.duty_pk, duty.duty_hori, duty.duty_horf, duty.duty_etme,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = duty.lite_pk and mult.lang_pk = :lang_pk) as duty_desc");
            sb.AppendLine("from tnht_duty duty");
            sb.AppendLine("where duty.hote_pk = :hote_pk");

            return sb.ToString();
        }
    }
}
