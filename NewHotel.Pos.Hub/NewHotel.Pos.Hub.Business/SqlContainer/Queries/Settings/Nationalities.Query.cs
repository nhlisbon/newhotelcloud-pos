﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Settings
{
    class NationalitiesQuery: Query
    {
        public NationalitiesQuery()
            : base("Nationalities") { }

        public NationalitiesQuery(IDatabaseManager manager)
            : base("Nationalities", manager) { }

        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select naci.naci_pk,");
            sb.AppendLine("(select mult.mult_desc from vnht_mult mult where mult.lite_pk = naci.lite_desc and mult.lang_pk = :lang_pk) as lite_desc,");
            sb.AppendLine("naci.remu_pk, naci.licl_pk, naci.naci_caco, naci.iso_3166, naci.pt_sef, naci.naci_caux");
            sb.AppendLine("from tnht_naci naci");
            sb.AppendLine("order by 2");

            return sb.ToString();
        }
    }
}
