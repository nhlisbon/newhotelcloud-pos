﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Settings
{
    public class TaxSchemaQuery : Query
    {
        public new const string Name = "TaxSchema.Query";
        public const string HotelIdParam = "HOTE_PK";
        public const string LanguageIdParam = "LANG_PK";

        public TaxSchemaQuery()
            : base(Name) { }

        public TaxSchemaQuery(IDatabaseManager manager)
            : base(Name, manager) { }

        protected override string GetCommandText()
        {
            return @"SELECT ESIM.ESIM_PK, ESIM.HOTE_PK, ESIM.ESIM_DEFA,
                            (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = ESIM.LITE_PK AND MULT.LANG_PK = :LANG_PK) AS ESIM_DESC
                     FROM   TNHT_ESIM ESIM
                     WHERE  ESIM.DELETED=0 AND ESIM.HOTE_PK=:HOTE_PK";
        }
    }
}
