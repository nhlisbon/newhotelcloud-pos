﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Settings
{
    public class CancellationTypesQuery : Query
    {
        public CancellationTypesQuery()
            : base("CancellationTypes") { }

        public CancellationTypesQuery(IDatabaseManager manager)
            : base("CancellationTypes", manager) { }

        /* alex: listado de motivos de anulacion */
        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select mtco.mtco_pk, mtco.mtco_tica,  ");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = mtco.lite_pk and mult.lang_pk = :lang_pk) as mtco_desc");
            sb.AppendLine("from tnht_mtco mtco");
            sb.AppendLine("where mtco.hote_pk = :hote_pk");

            return sb.ToString();
        }
    }
}
