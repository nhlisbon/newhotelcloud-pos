﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Settings
{
    class GetEmailHotelQuery : Query
    {
        public new const string Name = "GetEmailHotel.Query";

        public GetEmailHotelQuery(IDatabaseManager manager) : base(Name, manager)
        {
        }

        public GetEmailHotelQuery() : base(Name)
        {
        }

        protected override string GetCommandText()
        {
            return "select email_addr from tnht_hote";
        }
    }
}
