﻿using System.Text;
using NewHotel.Core;
using NewHotel.Pos.Hub.Business.Business;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Settings
{
    public class DiscountTypesQuery : Query
    {
        public DiscountTypesQuery()
            : base("DiscountTypes") { }

        public DiscountTypesQuery(IDatabaseManager manager)
            : base("DiscountTypes", manager) { }

        /* alex: listado de tipos de descuentos */
        protected override string GetCommandText()
        {
            return """
                   SELECT TIDE.TIDE_PK,
                          TIDE.APPL_PK,
                          TIDE.TIDE_FIXE,
                          TIDE.TIDE_VMIN,
                          TIDE.TIDE_VMAX,
                          MULT.MULT_DESC AS TIDE_DESC
                   FROM TNHT_TIDE TIDE
                            INNER JOIN VNHT_MULT MULT ON MULT.LITE_PK = TIDE.LITE_PK AND MULT.LANG_PK = :LANG_PK
                   WHERE TIDE.HOTE_PK = :HOTE_PK
                     AND (TIDE.APPL_PK IS NULL OR TIDE.APPL_PK = :APPL_PK)
                   ORDER BY MULT.MULT_DESC
                   """;
        }
    }
}