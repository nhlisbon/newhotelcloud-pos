﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Settings
{
    public class SeparatorsQuery : Query
    {
        public SeparatorsQuery()
            : base("Separators") { }

        public SeparatorsQuery(IDatabaseManager manager)
            : base("Separators", manager) { }

        /* alex: listado de separadores */
        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select sepa.sepa_pk, (select mult.mult_desc from vnht_mult mult where mult.lite_pk = sepa.lite_pk and mult.lang_pk = :lang_pk) as sepa_desc,");
            sb.AppendLine("       sepa.sepa_orde, sepa.sepa_maxp, sepa.sepa_acum, sepa.sepa_tick ");
            sb.AppendLine("from tnht_sepa sepa");
            sb.AppendLine("where sepa.hote_pk = :hote_pk");
            sb.AppendLine("order by sepa_orde");

            return sb.ToString();
        }
    }
}