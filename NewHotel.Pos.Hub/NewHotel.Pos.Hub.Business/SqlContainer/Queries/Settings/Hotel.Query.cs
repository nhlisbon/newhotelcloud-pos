﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Settings
{
    public class HotelQuery : Query
    {
        public HotelQuery()
            : base("Hotel") { }

        public HotelQuery(IDatabaseManager manager)
            : base("Hotel", manager) { }

        /* alex: listado de hoteles locales */
        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("select      distinct hote.hote_pk, hote.hote_abre, hote.hote_desc, hote.hote_caux, hote.naci_pk, hote.hote_logo, hote.hote_kali, hote.hote_expc, hote.hote_andr, hote.hote_sync");
            sb.AppendLine("from        tnht_hote hote inner join tnht_util util on hote.hote_pk = util.hote_pk");
            sb.AppendLine("where       (:util_pk is null or util.util_pk = :util_pk)");
            return sb.ToString();

        }
    }
}
