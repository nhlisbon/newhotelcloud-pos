﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Settings
{
    public class HouseUseQuery : Query
    {
        public HouseUseQuery()
            : base("HouseUse") { }

        public HouseUseQuery(IDatabaseManager manager)
            : base("HouseUse", manager) { }

        /* alex: listado de consumos internos autorizados en un punto de venta */
        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select pvci.pvci_pk, pvci.coin_pk, pvci.pvci_limi, pvci.pvci_ilim, pvci.pvci_porl, coin.coin_invi,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lang_pk = :lang_pk and mult.lite_pk = coin.lite_pk) as coin_desc");
            sb.AppendLine("from tnht_pvci pvci, vnht_coin coin");
            sb.AppendLine("where pvci.coin_pk = coin.coin_pk");
            sb.AppendLine("and pvci.ipos_pk = :ipos_pk");
            sb.AppendLine("and coin.coin_inac = 0");
            sb.AppendLine("order by coin_desc");

            return sb.ToString();
        }
    }
}
