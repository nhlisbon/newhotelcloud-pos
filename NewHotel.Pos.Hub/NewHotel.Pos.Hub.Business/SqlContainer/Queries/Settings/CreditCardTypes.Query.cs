﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Settings
{
    public class CreditCardTypesQuery : Query
    {
        public CreditCardTypesQuery()
            : base("CreditCardTypes") { }

        public CreditCardTypesQuery(IDatabaseManager manager)
            : base("CreditCardTypes", manager) { }

        /* alex: listado de tipos de tarjetas de crédito */
        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("select   cacr.cacr_pk, (select mult.mult_desc from vnht_mult mult where mult.lite_pk = cacr.lite_pk and mult.lang_pk = :lang_pk) as cacr_desc, cacr_cacr, cacr_cade");
            sb.AppendLine("from     tnht_cacr cacr");
            sb.AppendLine("where    cacr.hote_pk = :hote_pk");
            sb.AppendLine("and      cacr.cacr_inac = 0");

            return sb.ToString();
        }
    }
}
