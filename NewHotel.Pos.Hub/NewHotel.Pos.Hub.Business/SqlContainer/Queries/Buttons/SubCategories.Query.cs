﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Buttons
{
    public class SubCategoriesQuery : Query
    {
        public SubCategoriesQuery()
            : base("SubCategories") { }

        public SubCategoriesQuery(IDatabaseManager manager)
            : base("SubCategories", manager) { }

        /* alex: dada una categoria retorna la lista de sub-categorias */
        protected override string GetCommandText()
        {
            return @"SELECT BCAT.BCAT_PK,
                            BCAT.BCAT_IMAG,
                            BCAT.BCAT_ORDE,
                            (SELECT MULT.MULT_DESC
                             FROM VNHT_MULT MULT
                             WHERE MULT.LITE_PK = BCAT.LITE_PK
                             AND MULT.LANG_PK = :LANG_PK) AS BCAT_DESC
                FROM TNHT_BCAT BCAT
                WHERE BCAT.BCAT_CATE = :CATE_PK
                  AND BCAT.DELETED = 0";
        }

        protected override void Initialize()
        {
            Sorts.AddAsc("bcat_orde");
        }
    }
}