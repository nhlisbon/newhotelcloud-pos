﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Buttons
{
    public class CategoriesQuery : Query
    {
        public CategoriesQuery()
            : base("Categories") { }

        public CategoriesQuery(IDatabaseManager manager)
            : base("Categories", manager) { }

        /* alex: dame todas las categorias */
        protected override string GetCommandText()
        {
            return @"SELECT BCAT.BCAT_PK,
                            BCAT.BCAT_IMAG,
                            BCAT.BCAT_ORDE,
                            (SELECT MULT.MULT_DESC
                             FROM VNHT_MULT MULT
                             WHERE MULT.LITE_PK = BCAT.LITE_PK
                             AND MULT.LANG_PK = :LANG_PK) AS BCAT_DESC
                FROM TNHT_BCAT BCAT
                WHERE BCAT.HOTE_PK = :HOTE_PK
                  AND BCAT.BCAT_CATE IS NULL
                  AND BCAT.DELETED = 0";
        }

        protected override void Initialize()
        {
            Sorts.AddAsc("bcat_orde");
        }
    }
}