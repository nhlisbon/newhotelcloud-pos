﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Buttons
{
    public class ColumnsPlusProductsQuery : Query
    {
        public ColumnsPlusProductsQuery()
            : base("ColumnsPlusProducts") { }

        public ColumnsPlusProductsQuery(IDatabaseManager manager)
            : base("ColumnsPlusProducts", manager) { }


        /* alex: dado un stand y una subcategoria retorna las colunmas y sus productos */
        protected override string GetCommandText()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("select      bcat.bcat_pk, (select mult.mult_desc from vnht_mult mult where mult.lite_pk = bcat.lite_pk and mult.lang_pk = :lang_pk) as bcat_desc,");
            sb.AppendLine("            botv.artg_pk, (select mult.mult_desc from vnht_mult mult where mult.lite_pk = artg.lite_desc and mult.lang_pk = :lang_pk) artg_desc,");
            sb.AppendLine("            bcat.bcat_orde, botv.botv_orde");
            sb.AppendLine("from        tnht_bcat bcat, tnht_botv botv, vnht_artg artg ");
            sb.AppendLine("where       bcat.bcat_pk = botv.bcat_pk");
            sb.AppendLine("and         bcat.bcat_cate = :scat_pk ");
            sb.AppendLine("and         botv.artg_pk = artg.artg_pk ");
            sb.AppendLine("and         botv.ipos_pk = :ipos_pk");
            return sb.ToString();
        }

        protected override void Initialize()
        {
            base.Initialize();

            Sorts.AddAsc("bcat_orde");
            Sorts.AddAsc("botv_orde");
        }
    }
}
