﻿using NewHotel.Core;
using NewHotel.Pos.Hub.Business.SqlContainer.Common;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Buttons;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Cashier;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Integrations;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Lines.Ticket;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Prices;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Settings;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Stands;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries
{
    public sealed class CommonQueryFactory : QueryFactory
    {
        #region Products

        public static Query GetProductsByStand(IDatabaseManager manager) { return new ProductsByStandQuery(manager); }
        public static Query GetProductGroups(IDatabaseManager manager) { return new ProductGroupsByStandsQuery(manager); }
        public static Query GetProductFamilies(IDatabaseManager manager) { return new ProductFamilyByStandsQuery(manager); }
        public static Query GetProductSubFamilies(IDatabaseManager manager) { return new ProductSubFamilyByStandsQuery(manager); }
        public static Query GetPreparationsByProducts(IDatabaseManager manager) { return new PreparationsByProductsQuery(manager); }
        public static Query GetPreparations(IDatabaseManager manager) { return new PreparationsQuery(manager); }
        public static Query GetAreasByProducts(IDatabaseManager manager) { return new AreasByProductsQuery(manager); }
        public static Query GetAreasByStand(IDatabaseManager manager) { return new AreasByStandQuery(manager); }
        public static Query GetProductByStand(IDatabaseManager manager) { return new ProductByStandQuery(manager); }
        public static Query GetProductAreasByStand(IDatabaseManager manager) { return new ProductAreasByStandQuery(manager); }
        public static Query GetProductPreparations(IDatabaseManager manager) { return new ProductPreparationsQuery(manager); }
        public static Query GetProductTableProducts(IDatabaseManager manager) { return new ProductTableProductsQuery(manager); }
        public static Query GetProductTablesByProduct(IDatabaseManager manager) { return new ProductTablesByProduct(manager); }
        public static Query GetProductTaxSchemas(IDatabaseManager manager) { return new ProductTaxSchemasQuery(manager); }
        public static Query GetProductPricesByStand(IDatabaseManager manager) { return new ProductPricesByStandQuery(manager); }
        public static Query GetProductStockQuery(IDatabaseManager manager) { return new ProductStockQuery(manager); }
        public static Query UpdateProductStock(IDatabaseManager manager) { return new UpdateProductStockQuery(manager); }
        public static Query UpdateOrCreateProductStock(IDatabaseManager manager) { return new UpdateOrCreateProductStockQuery(manager); }

        #endregion
        #region Stands

        public static Query GetUsers(IDatabaseManager manager) { return new UsersQuery(manager); }
        public static Query GetCashierByUsers(IDatabaseManager manager) { return new CashiersByUsersQuery(manager); }
        public static Query GetStandById(IDatabaseManager manager)
        {
            return new StandByIdQuery(manager);
        }
        public static Query GetStandsByCashiers(IDatabaseManager manager)
        {
            return new StandsByCashiersQuery(manager);
        }
        public static Query GetStandsForTransfer(IDatabaseManager manager) { return new StandsForTransferQuery(manager); }
        public static Query GetTransfers(IDatabaseManager manager) { return new TransfersQuery(manager); }
        public static Query GetSaloonsByStand(IDatabaseManager manager) { return new SaloonsByStandQuery(manager); }
        public static Query GetTablesBySaloon(IDatabaseManager manager) { return new TablesQuery(manager); }
        public static Query GetWaitingList(IDatabaseManager manager) { return new WaitingListStandQuery(manager); }


        #endregion
        #region Tickets

        public static Query GetLocalClosedTickets(IDatabaseManager manager) { return new LocalClosedTicketsQuery(manager); }
        public static Query GetDocumentSign(IDatabaseManager manager) { return new DocumentSignQuery(manager); }
        public static Query GetInvoiceSign(IDatabaseManager manager) { return new InvoiceSignQuery(manager); }
        public static Query GetDocumentSeriesByStandCashier(IDatabaseManager manager) { return new DocumentSeriesQuery(manager); }
        public static Query GetSerieValidationCode(IDatabaseManager manager) { return new SerieValidationCodeQuery(manager); }        
        public static Query GetDocumentParent(IDatabaseManager manager) { return new DocumentParentQuery(manager); }
        public static Query GetTicketPrintConfiguration(IDatabaseManager manager) { return new TicketPrintConfigurationQuery(manager); }
        public static Query GetProductLinesByTicket(IDatabaseManager manager) { return new ProductLinesByTicketQuery(manager); } 
        public static Query GetPaymentLinesByTicket(IDatabaseManager manager) { return new PaymentLinesByTicketQuery(manager); }
        public static Query GetClientsPositionByTicket(IDatabaseManager manager) { return new ClientsPositionByTicketQuery(manager); }
        public static Query GetClientsPositionPreferencesByTicket(IDatabaseManager manager) { return new ClientsPositionPreferencesByTicketQuery(manager); }
        public static Query GetClientsPositionDietsByTicket(IDatabaseManager manager) { return new ClientsPositionDietsByTicketQuery(manager); }
        public static Query GetClientsPositionAttentionsByTicket(IDatabaseManager manager) { return new ClientsPositionAttentionsByTicketQuery(manager); }
        public static Query GetClientsPositionAllergiesByTicket(IDatabaseManager manager) { return new ClientsPositionAllergiesByTicketQuery(manager); }
        public static Query GetTaxesByProduct(IDatabaseManager manager) { return new TaxesByProductQuery(manager); }
        public static Query GetTicketByStandSaloon(IDatabaseManager manager) { return new TicketByStandSaloon(manager); }
        public static Query GetTicketByStandCashier(IDatabaseManager manager) { return new TicketByStandCashierQuery(manager); }
        public static Query GetTicketWithProducts(IDatabaseManager manager) { return new TicketWithProductQuery(manager); }
        public static Query GetTicketCurrentDuty(IDatabaseManager manager) { return new TicketCurrentDutyQuery(manager); }
        public static Query InsertProductLineAreaQuery(IDatabaseManager manager) { return new InsertProductLineAreaQuery(manager); }
        public static Query InsertProductTableLineAreaQuery(IDatabaseManager manager) { return new InsertProductTableLineAreaQuery(manager); }
        public static Query SelectProductLineAreasQuery(IDatabaseManager manager) { return new SelectProductLineAreasQuery(manager); }
        public static Query SelectProductTableLineAreasQuery(IDatabaseManager manager) { return new SelectProductTableLineAreasQuery(manager); }
        public static Query DeleteProductLineAreasQuery(IDatabaseManager manager) { return new DeleteProductLineAreasQuery(manager); }
        public static Query DeleteProductTableLineAreasQuery(IDatabaseManager manager) { return new DeleteProductTableLineAreasQuery(manager); }
        public static Query CheckOpenTicketQuery(IDatabaseManager manager) { return new CheckOpenTicketQuery(manager); }
        public static Query GetTicketDescriptionsQuery(IDatabaseManager manager) { return new GetTicketDescriptionsQuery(manager); }

        #endregion

        #region Prices

        public static Query GetHappyHourByStands(IDatabaseManager manager) { return new HappyHourQuery(manager); }
        public static Query GetPriceByStandAndRate(IDatabaseManager manager) { return new PriceByStandAndRateQuery(manager); }
        public static Query GetPricesByStand(IDatabaseManager manager) { return new PricesByStandQuery(manager); }
        public static Query GetDispatchAreaByProduct(IDatabaseManager manager) { return new DispatchAreaByProductQuery(manager); }

        #endregion
        #region Button Configuration

        public static Query GetCategories(IDatabaseManager manager) { return new CategoriesQuery(manager); }
        public static Query GetSubCategories(IDatabaseManager manager) { return new SubCategoriesQuery(manager); }
        public static Query GetColumnsPlusProducts(IDatabaseManager manager) { return new ColumnsPlusProductsQuery(manager); }

        #endregion
        #region Settings

        public static Query GetHotel(IDatabaseManager manager) { return new HotelQuery(manager); }
        public static Query GetNationalities(IDatabaseManager manager) { return new NationalitiesQuery(manager); }
        public static Query GetGeneralSettings(IDatabaseManager manager) { return new GeneralSettingsQuery(manager); }
        public static Query GetCancellationTypes(IDatabaseManager manager) { return new CancellationTypesQuery(manager); }
        public static Query GetCreditCardTypes(IDatabaseManager manager) { return new CreditCardTypesQuery(manager); }
        public static Query GetDiscountTypes(IDatabaseManager manager) { return new DiscountTypesQuery(manager); }
        public static Query GetHouseUses(IDatabaseManager manager) { return new HouseUseQuery(manager); }
        public static Query GetPaymentMethods(IDatabaseManager manager) { return new PaymentMethodsQuery(manager); }
        public static Query GetSeparators(IDatabaseManager manager) { return new SeparatorsQuery(manager); }
        public static Query GetTicketDuty(IDatabaseManager manager) { return new TicketDutyQuery(manager); }

        #endregion

        #region Lines

        public static Query LoadProductLinesByTicketQuery(IDatabaseManager manager) { return new LoadProductLinesByTicketQuery(manager); }

        #endregion

        #region Integrations

        public static Query GetProductIntegrationReferenceById(IDatabaseManager manager) { return new ProductIntegrationReferenceQuery(manager); }
        public static Query GetProductStockByProductId(IDatabaseManager manager) { return new ProductStockByProductIdQuery(manager); }
        public static Query GetProductIntegrationReference(IDatabaseManager manager) { return new ProductIntegrationReferenceQuery(manager); }
        public static Query GetProductIntegrationReferenceByIntegrationIdAndProductCode(IDatabaseManager manager) { return new ProductIntegrationReferenceByIntegrationIdAndProductCodeQuery(manager); }
        public static Query GetAllProductReferenceRecordsForIntegration(IDatabaseManager manager) { return new AllProductReferenceRecordsForIntegrationQuery(manager); }
        public static Query GetIntegration(IDatabaseManager manager) { return new IntegrationQuery(manager); }

        #endregion
    }
}