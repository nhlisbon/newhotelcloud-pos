﻿using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Reports
{
    public class SalesReportQuery : Query
    {
        public SalesReportQuery(IDatabaseManager manager)
            : base("SalesReport", manager)
        { }
        
        protected override string GetCommandText()
        {
            return """
                   SELECT CLAS.LITE_PK                  AS ARVE_PK,
                          CLAS_MULT.MULT_DESC           AS GRUP_DESC,
                          ARTG_MULT.MULT_DESC           AS ARTG_DESC,
                          SUM(ARVE.ARVE_QTDS)           AS QTD,
                          ROUND(SUM(ARVE.ARVE_TOTL), 2) AS IMPORTE,
                          TOTAL_PAX                     AS PAXS
                   FROM (SELECT VEND.VEND_PK, SUM(VEND.VEND_NPAX) OVER () AS TOTAL_PAX
                         FROM TNHT_VEND VEND
                         WHERE VEND.IPOS_PK = :IPOS_PK
                           AND VEND.CAJA_PK = :CAJA_PK
                           AND VEND.VEND_DATF = :VEND_DATF
                           AND VEND.VEND_ANUL = 0) VEND
                            INNER JOIN TNHT_ARVE ARVE ON ARVE.VEND_PK = VEND.VEND_PK AND ARVE.ARVE_ANUL IN (0, 2)
                            INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = ARVE.ARTG_PK
                            INNER JOIN VNHT_MULT ARTG_MULT ON ARTG_MULT.LITE_PK = ARTG.LITE_DESC AND ARTG_MULT.LANG_PK = :LANG_PK
                            INNER JOIN TNHT_FAMI FAMI ON FAMI.FAMI_PK = ARTG.FAMI_PK
                            INNER JOIN TNHT_CLAS CLAS ON CLAS.CLAS_PK = FAMI.GRUP_PK
                            INNER JOIN VNHT_MULT CLAS_MULT ON CLAS_MULT.LITE_PK = CLAS.LITE_PK AND CLAS_MULT.LANG_PK = :LANG_PK
                   GROUP BY CLAS.LITE_PK, CLAS_MULT.MULT_DESC, ARTG_MULT.MULT_DESC, VEND.TOTAL_PAX
                   ORDER BY CLAS.LITE_PK, ARTG_MULT.MULT_DESC
                   """;
        }
    }
}