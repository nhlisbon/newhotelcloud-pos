﻿using NewHotel.Core;
using NewHotel.Pos.Hub.Business.Business;
using NewHotel.Pos.Hub.Business.PersistentObjects.Menu;
using System.Reflection;
using System;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Reports
{
    public class PaymentsReportQuery : Query
    {
        public new const string Name = "PaymentsReport.Query";
        public const string LanguageIdParam = "lang_pk";
        public const string StandIdParam = "ipos_pk";
        public const string CashierIdParam = "caja_pk";
        public const string DateClosedParam = "vend_datf";
        public const string RoomCreditLabelParam = "room_credit";
        public const string PaymentTypeIdFilter = "tire_pk";

        public PaymentsReportQuery(IDatabaseManager manager)
            : base(Name, manager)
        {
        }

        protected override string GetCommandText()
        {
            return
                /*
                @"SELECT PAVE_PK, PAVE_VALO, VEND_PK, VEND_CODI, VEND_SERI, VEND_DATF, VEND_ANUL, FACT_CODI, FACT_SERI, NCRE_CODI, NCRE_SERI,
                         TURN_CODI, UTIL_DESC, PAVE_OBSV, TACR_DESC, TIRE_PK, PAVE_DESC, VEND_UNMO, PAVE_UNMO, CAMB_ONLINE, CAMB_MULT,
                         CASE WHEN TIRE_PK = 2131 THEN FORE_DESC ELSE PAVE_DESC END AS PAYMENT_TYPE_NAME, FORE_DESC AS PAYMENT_DESC,
                         CASE WHEN TIRE_PK = 2131 THEN CASE WHEN TACR_DESC IS NULL THEN FORE_DESC ELSE TACR_DESC END ELSE PAVE_DESC END AS PAYMENT_DESC_DETAILED
                  FROM (
                      WITH TEMP_VEND AS (
                        SELECT PAVE.PAVE_PK, PAVE.PAVE_VALO, PAVE.UNMO_PK AS PAVE_UNMO, PAVE.CAMB_ONLINE, PAVE.CAMB_MULT, 
                               VEND.VEND_PK, VEND.VEND_CODI, PAVE.PAVE_OBSV, VEND.VEND_SERI, VEND.VEND_DATF, VEND.TURN_CODI,
                               CASE WHEN UTIL_CLOS.UTIL_DESC IS NULL THEN UTIL.UTIL_DESC ELSE UTIL_CLOS.UTIL_DESC END AS UTIL_DESC,
                               VEND.VEND_ANUL, VEND.UNMO_PK AS VEND_UNMO,
                               CASE WHEN FORE.FORE_PK IS NOT NULL THEN
                                    (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = FORE.LITE_DESC AND MULT.LANG_PK = :LANG_PK)
                                    WHEN COIN.CLAS_PK IS NOT NULL THEN
                                    (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = COIN.LITE_PK AND MULT.LANG_PK = :LANG_PK)
                                    WHEN VEND.CCCO_PK IS NOT NULL THEN VEND.CCCO_DESC
                               ELSE PAVE.PAVE_OBSV END AS FORE_DESC,
                               (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = CACR.LITE_PK AND MULT.LANG_PK = :LANG_PK) AS TACR_DESC,
                               PAVE.TIRE_PK, 
                               CASE WHEN PAVE.TIRE_PK = 271 THEN :ROOM_CREDIT ELSE
                                    (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = ENUM.LITE_PK AND MULT.LANG_PK = :LANG_PK)
                               END AS PAVE_DESC, VEND.FACT_CODI, VEND.FACT_SERI, VEND.ANUL_DAAN, VEND.NCRE_DATR, VEND.NCRE_CODI, VEND.NCRE_SERI
                        FROM TNHT_VEND VEND
                             INNER JOIN TNHT_UTIL UTIL ON UTIL.UTIL_PK = VEND.UTIL_PK
                             INNER JOIN TNHT_PAVE PAVE ON PAVE.VEND_PK = VEND.VEND_PK
                             INNER JOIN TNHT_ENUM ENUM ON ENUM_PK = PAVE.TIRE_PK
                             LEFT JOIN TNHT_UTIL UTIL_CLOS ON UTIL_CLOS.UTIL_PK = VEND.UTIL_CLOS
                             LEFT JOIN TNHT_CLAS COIN ON COIN.CLAS_PK = PAVE.COIN_PK
                             LEFT JOIN TNHT_FORE FORE ON FORE.FORE_PK = PAVE.FORE_PK
                             LEFT JOIN TNHT_TACR TACR ON TACR.TACR_PK = PAVE.TACR_PK
                             LEFT JOIN TNHT_CACR CACR ON CACR.CACR_PK = TACR.CACR_PK
                        WHERE VEND.IPOS_PK = :IPOS_PK AND VEND.CAJA_PK = :CAJA_PK)
                      SELECT PAVE_PK, PAVE_VALO, PAVE_UNMO, TIRE_PK, PAVE_DESC, PAVE_OBSV, CAMB_ONLINE, CAMB_MULT,
                             VEND_PK, VEND_CODI, VEND_SERI, VEND_DATF, VEND_ANUL, VEND_UNMO, TURN_CODI, UTIL_DESC,
                             FORE_DESC, TACR_DESC, ANUL_DAAN, FACT_CODI, FACT_SERI, NULL AS NCRE_DATR, NULL AS NCRE_CODI, NULL AS NCRE_SERI
                      FROM TEMP_VEND WHERE VEND_DATF = :VEND_DATF
                      UNION ALL
                      SELECT PAVE_PK, -PAVE_VALO, PAVE_UNMO, TIRE_PK, PAVE_DESC, PAVE_OBSV, CAMB_ONLINE, CAMB_MULT,
                             VEND_PK, VEND_CODI, VEND_SERI, VEND_DATF, VEND_ANUL, VEND_UNMO, TURN_CODI, UTIL_DESC,
                             FORE_DESC, TACR_DESC, ANUL_DAAN, NULL AS FACT_CODI, NULL AS FACT_SERI, NULL AS NCRE_DATR, NULL AS NCRE_CODI, NULL AS NCRE_SERI
                      FROM TEMP_VEND WHERE VEND_ANUL = '1' AND ANUL_DAAN = :VEND_DATF
                      UNION ALL
                      SELECT PAVE_PK, -PAVE_VALO, PAVE_UNMO, TIRE_PK, PAVE_DESC, PAVE_OBSV, CAMB_ONLINE, CAMB_MULT,
                             VEND_PK, VEND_CODI, VEND_SERI, VEND_DATF, VEND_ANUL, VEND_UNMO, TURN_CODI, UTIL_DESC,
                             FORE_DESC, TACR_DESC, ANUL_DAAN, FACT_CODI, FACT_SERI, NCRE_DATR, NCRE_CODI, NCRE_SERI
                      FROM TEMP_VEND WHERE VEND_ANUL = '0' AND NCRE_DATR = :VEND_DATF
                  )";
                */
                //TODO JA: annadiendo usuario que cancelo ticket, borrar despues de las pruebas

                
                @"SELECT PAVE_PK, PAVE_VALO, VEND_PK, VEND_CODI, VEND_SERI, VEND_DATF, VEND_ANUL, FACT_CODI, FACT_SERI, NCRE_CODI, NCRE_SERI,
                         TURN_CODI, UTIL_DESC, PAVE_OBSV, TACR_DESC, TIRE_PK, PAVE_DESC, VEND_UNMO, PAVE_UNMO, CAMB_ONLINE, CAMB_MULT,
                         CASE WHEN TIRE_PK = 2131 THEN FORE_DESC ELSE PAVE_DESC END AS PAYMENT_TYPE_NAME, FORE_DESC AS PAYMENT_DESC,
                         CASE WHEN TIRE_PK = 2131 THEN CASE WHEN TACR_DESC IS NULL THEN FORE_DESC ELSE TACR_DESC END ELSE PAVE_DESC END AS PAYMENT_DESC_DETAILED
                  FROM(
                      WITH TEMP_VEND AS(
                        SELECT PAVE.PAVE_PK, PAVE.PAVE_VALO, PAVE.UNMO_PK AS PAVE_UNMO, PAVE.CAMB_ONLINE, PAVE.CAMB_MULT,
                               VEND.VEND_PK, VEND.VEND_CODI, PAVE.PAVE_OBSV, VEND.VEND_SERI, VEND.VEND_DATF, VEND.TURN_CODI,
                               CASE
                                    WHEN ANUL.UTIL_DESC IS NOT NULL THEN ANUL.UTIL_DESC
                                    WHEN UTIL_CLOS.UTIL_DESC IS NOT NULL THEN UTIL_CLOS.UTIL_DESC
                                    ELSE UTIL.UTIL_DESC
                               END AS UTIL_DESC,
                               VEND.VEND_ANUL, VEND.UNMO_PK AS VEND_UNMO,
                               CASE WHEN FORE.FORE_PK IS NOT NULL THEN
                                    (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = FORE.LITE_DESC AND MULT.LANG_PK = :LANG_PK)
                                    WHEN COIN.CLAS_PK IS NOT NULL THEN
                                    (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = COIN.LITE_PK AND MULT.LANG_PK = :LANG_PK)
                                    WHEN VEND.CCCO_PK IS NOT NULL THEN VEND.CCCO_DESC
                               ELSE PAVE.PAVE_OBSV END AS FORE_DESC,
                               (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = CACR.LITE_PK AND MULT.LANG_PK = :LANG_PK) AS TACR_DESC,
                  PAVE.TIRE_PK,
                  CASE WHEN PAVE.TIRE_PK = 271 THEN :ROOM_CREDIT ELSE
                                    (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = ENUM.LITE_PK AND MULT.LANG_PK = :LANG_PK)
                               END AS PAVE_DESC, VEND.FACT_CODI, VEND.FACT_SERI, VEND.ANUL_DAAN, VEND.NCRE_DATR, VEND.NCRE_CODI, VEND.NCRE_SERI
                        FROM TNHT_VEND VEND
                             INNER JOIN TNHT_UTIL UTIL ON UTIL.UTIL_PK = VEND.UTIL_PK
                             INNER JOIN TNHT_PAVE PAVE ON PAVE.VEND_PK = VEND.VEND_PK
                             INNER JOIN TNHT_ENUM ENUM ON ENUM_PK = PAVE.TIRE_PK
                             LEFT JOIN TNHT_UTIL UTIL_CLOS ON UTIL_CLOS.UTIL_PK = VEND.UTIL_CLOS
                             LEFT JOIN TNHT_CLAS COIN ON COIN.CLAS_PK = PAVE.COIN_PK
                             LEFT JOIN TNHT_FORE FORE ON FORE.FORE_PK = PAVE.FORE_PK
                             LEFT JOIN TNHT_TACR TACR ON TACR.TACR_PK = PAVE.TACR_PK
                             LEFT JOIN TNHT_CACR CACR ON CACR.CACR_PK = TACR.CACR_PK
                             LEFT JOIN TNHT_UTIL ANUL ON ANUL.UTIL_PK = VEND.ANUL_UTIL
                        WHERE VEND.IPOS_PK = :IPOS_PK AND VEND.CAJA_PK = :CAJA_PK)
                      SELECT PAVE_PK, PAVE_VALO, PAVE_UNMO, TIRE_PK, PAVE_DESC, PAVE_OBSV, CAMB_ONLINE, CAMB_MULT,
                             VEND_PK, VEND_CODI, VEND_SERI, VEND_DATF, VEND_ANUL, VEND_UNMO, TURN_CODI, UTIL_DESC,
                             FORE_DESC, TACR_DESC, ANUL_DAAN, FACT_CODI, FACT_SERI, NULL AS NCRE_DATR, NULL AS NCRE_CODI, NULL AS NCRE_SERI
                      FROM TEMP_VEND WHERE VEND_DATF = :VEND_DATF
                      UNION ALL
                      SELECT PAVE_PK, -PAVE_VALO, PAVE_UNMO, TIRE_PK, PAVE_DESC, PAVE_OBSV, CAMB_ONLINE, CAMB_MULT,
                             VEND_PK, VEND_CODI, VEND_SERI, VEND_DATF, VEND_ANUL, VEND_UNMO, TURN_CODI, UTIL_DESC,
                             FORE_DESC, TACR_DESC, ANUL_DAAN, NULL AS FACT_CODI, NULL AS FACT_SERI, NULL AS NCRE_DATR, NULL AS NCRE_CODI, NULL AS NCRE_SERI
                      FROM TEMP_VEND WHERE VEND_ANUL = '1' AND ANUL_DAAN = :VEND_DATF
                      UNION ALL
                      SELECT PAVE_PK, -PAVE_VALO, PAVE_UNMO, TIRE_PK, PAVE_DESC, PAVE_OBSV, CAMB_ONLINE, CAMB_MULT,
                             VEND_PK, VEND_CODI, VEND_SERI, VEND_DATF, VEND_ANUL, VEND_UNMO, TURN_CODI, UTIL_DESC,
                             FORE_DESC, TACR_DESC, ANUL_DAAN, FACT_CODI, FACT_SERI, NCRE_DATR, NCRE_CODI, NCRE_SERI
                      FROM TEMP_VEND WHERE VEND_ANUL = '0' AND NCRE_DATR = :VEND_DATF
                  )";                
        }

        protected override void Initialize()
        {
            base.Initialize();
            Filters.Add(PaymentTypeIdFilter, "tire_pk" , typeof(long));
            Sorts.AddAsc("payment_desc");
        }
    }
}