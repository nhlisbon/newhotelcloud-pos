﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Closing
{
    public class UnsyncedQuery : Query
    {
        public new const string Name = "UnsyncedQuery.Query";
        public const string CashierIdParam = "caja_pk";
        public const string StandIdParam = "ipos_pk";
        public const string ListIds = "LIST_PK";

        public UnsyncedQuery(): base(Name)
        {
        }

        public UnsyncedQuery(IDatabaseManager manager) : base(Name, manager)
        {

        }

        protected override string GetCommandText()
        {
            return """
                   SELECT VEND_CODI, VEND_SERI
                                        FROM   TNHT_VEND 
                                        WHERE  IPOS_PK = :IPOS_PK     AND 
                                               CAJA_PK = :CAJA_PK
                   """;
        }
    }
}
