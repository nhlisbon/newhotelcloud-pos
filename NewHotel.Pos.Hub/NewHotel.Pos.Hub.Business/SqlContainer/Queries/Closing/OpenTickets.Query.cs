﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Closing
{
    public class OpenTicketsQuery : Query
    {
        public new const string Name = "OpenTickets.Query";
        public const string CashierIdParam = "caja_pk";
        public const string StandIdParam = "ipos_pk";

        public OpenTicketsQuery(): base(Name)
        {
        }

        public OpenTicketsQuery(IDatabaseManager manager) : base(Name, manager)
        {

        }

        protected override string GetCommandText()
        {
            return @"SELECT COUNT(*)  
                     FROM   TNHT_VEND 
                     WHERE  VEND_DATF IS NULL AND 
                            IPOS_PK = :IPOS_PK AND 
                            CAJA_PK = :CAJA_PK";
        }
    }
}
