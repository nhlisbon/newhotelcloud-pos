﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Closing
{
    public class EmptyFiscalPrinterDataQuery : Query
    {
        public new const string Name = "EmptyFiscalPrinterDataTickets.Query";
        public const string CashierIdParam = "caja_pk";
        public const string StandIdParam = "ipos_pk";
        public const string CancelledParam = "vend_anul";

        public EmptyFiscalPrinterDataQuery(): base(Name)
        {
        }

        public EmptyFiscalPrinterDataQuery(IDatabaseManager manager) : base(Name, manager)
        {

        }

        protected override string GetCommandText()
        {
            // Invoices with empty fiscal printer data
            return @"SELECT COUNT(*)
                     FROM   TNHT_VEND 
                     WHERE  VEND_ANUL = :VEND_ANUL AND 
                            IPOS_PK   = :IPOS_PK   AND 
                            CAJA_PK   = :CAJA_PK   AND 
                            NOT FACT_SERI IS NULL  AND 
                            VEND_IFNC IS NULL      AND 
                            VEND_IFFA IS NULL      AND
                            VEND_NFFA IS NULL      AND
                            VEND_NFNC IS NULL";
        }
    }
}
