﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Closing
{
    public class UntransmittedTicketsQuery : Query
    {
        public new const string Name = "OpenedTickets.Query";
        public const string CashierIdParam = "caja_pk";
        public const string StandIdParam = "ipos_pk";
        public const string CancelledParam = "vend_anul";

        public UntransmittedTicketsQuery(): base(Name)
        {
        }

        public UntransmittedTicketsQuery(IDatabaseManager manager) : base(Name, manager)
        {

        }

        protected override string GetCommandText()
        {
            return """
                   SELECT VEND_CODI, VEND_SERI
                   FROM TNHT_VEND
                   WHERE VEND_SYNC IS NOT NULL
                     AND IPOS_PK = :IPOS_PK
                     AND CAJA_PK = :CAJA_PK
                   """;
            
            
            // SELECT VEND_CODI, VEND_SERI
            // FROM   TNHT_VEND 
            // WHERE  VEND_SYNC IS NOT NULL  AND 
            //     VEND_ANUL = :VEND_ANUL AND 
            //     IPOS_PK = :IPOS_PK     AND 
            //     CAJA_PK = :CAJA_PK
        }
    }
}
