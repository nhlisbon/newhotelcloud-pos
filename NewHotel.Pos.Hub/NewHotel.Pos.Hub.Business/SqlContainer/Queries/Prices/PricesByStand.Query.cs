﻿using System;
using System.Text;
using System.Web.UI.WebControls;
using System.Windows.Controls;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Prices
{
    public class PricesByStandQuery : Query
    {
        public PricesByStandQuery()
            : base("PricesByStand") { }

        public PricesByStandQuery(IDatabaseManager manager)
            : base("PricesByStand", manager) { }

        /* alex: dado una tarifa y un punto de venta, retorna la lista de productos y sus precios 
           util para saber los precios de un happy-hour, precios de consumo interno y meal plan
           cuando tengan una determinada tarifa asociada */
        protected override string GetCommandText()
        {
            return """
                   WITH TEMP_TPPE AS (SELECT TPPE.TPRG_PK,
                                             TPRL.ARTG_PK,
                                             TPPE.TPPE_DAIN,
                                             TPPE.TPPE_DAFI,
                                             TPRL.TPRL_VALOR
                                      FROM TNHT_TPPE TPPE
                                               INNER JOIN TNHT_TPRL TPRL ON TPRL.TPPE_PK = TPPE.TPPE_PK)
                   SELECT PVAR.ARTG_PK,
                          (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = ARTG.LITE_DESC AND MULT.LANG_PK = :LANG_PK) AS ARTG_DESC,
                          ARTG.ARTG_CODI,
                          ARTG.SEPA_PK,
                          ARTG.ARTG_TABL,
                          ARTG.ARTG_PDES,
                          TPPE_STDR.TPRL_VALOR                                                                                        AS STDR_VALO,
                          TPPE_CINT.TPRL_VALOR                                                                                        AS CINT_VALO,
                          TPPE_PENS.TPRL_VALOR                                                                                        AS PENS_VALO,
                          TPPE_ACOM.TPRL_VALOR                                                                                        AS ACOM_VALO
                   FROM TNHT_PVAR PVAR
                            INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = PVAR.ARTG_PK
                            INNER JOIN TNHT_IPOS IPOS ON IPOS.IPOS_PK = PVAR.IPOS_PK
                            LEFT JOIN TEMP_TPPE TPPE_STDR ON TPPE_STDR.ARTG_PK = PVAR.ARTG_PK
                       AND TPPE_STDR.TPRG_PK = IPOS.TPRG_STDR
                       AND IPOS.IPOS_FCTR BETWEEN TPPE_STDR.TPPE_DAIN AND TPPE_STDR.TPPE_DAFI
                            LEFT JOIN TEMP_TPPE TPPE_CINT ON TPPE_CINT.ARTG_PK = PVAR.ARTG_PK
                       AND TPPE_CINT.TPRG_PK = IPOS.TPRG_CINT
                       AND IPOS.IPOS_FCTR BETWEEN TPPE_CINT.TPPE_DAIN AND TPPE_CINT.TPPE_DAFI
                            LEFT JOIN TEMP_TPPE TPPE_PENS ON TPPE_PENS.ARTG_PK = PVAR.ARTG_PK
                       AND TPPE_PENS.TPRG_PK = IPOS.TPRG_PENS
                       AND IPOS.IPOS_FCTR BETWEEN TPPE_PENS.TPPE_DAIN AND TPPE_PENS.TPPE_DAFI
                            LEFT JOIN TEMP_TPPE TPPE_ACOM ON TPPE_ACOM.ARTG_PK = PVAR.ARTG_PK
                       AND TPPE_ACOM.TPRG_PK = IPOS.TPRG_ACOM
                       AND IPOS.IPOS_FCTR BETWEEN TPPE_ACOM.TPPE_DAIN AND TPPE_ACOM.TPPE_DAFI
                   WHERE PVAR.IPOS_PK = :IPOS_PK
                   """;
        }

        protected override void Initialize()
        {
            Filters.Add("artg_pk", "pvar.artg_pk", typeof(Guid));
        }
    }

    public class DispatchAreaByProductQuery : Query
    {
        public DispatchAreaByProductQuery()
            : base("DispatchAreaByProduct") { }

        public DispatchAreaByProductQuery(IDatabaseManager manager)
            : base("DispatchAreaByProduct", manager) { }

        protected override string GetCommandText()
        {
            return """

                   SELECT ARAR.AREA_PK, ARAR.ARAR_ARMA
                   FROM TNHT_ARAR ARAR
                   WHERE ARAR.IPOS_PK = :IPOS_PK
                     AND ARAR.ARAR_ARMA = :TRUE
                     AND ARAR.ARTG_PK = :ARTG_PK
                   UNION ALL
                   SELECT MIN(ARAR.AREA_PK) AS AREA_PK, :FALSE AS ARAR_ARMA
                   FROM TNHT_ARAR ARAR
                   WHERE ARAR.IPOS_PK = :IPOS_PK
                     AND ARAR.ARAR_ARMA = :FALSE
                     AND ARAR.ARTG_PK = :ARTG_PK
                     
                   """;
        }

        protected override void Initialize()
        {
            Parameters["true"] = true;
            Parameters["false"] = false;
        }
    }
}