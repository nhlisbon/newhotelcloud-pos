﻿using System;
using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Prices
{
    public class PriceByStandAndRateQuery : Query
    {
        public PriceByStandAndRateQuery()
            : base("PriceByStandAndRate") { }

        public PriceByStandAndRateQuery(IDatabaseManager manager)
            : base("PriceByStandAndRate", manager) { }

        /* alex: dado una tarifa y un punto de venta, retorna la lista de productos y sus precios 
           util para saber los precios de un happy-hour, precios de consumo interno y meal plan
           cuando tengan una determinada tarifa asociada */
        protected override string GetCommandText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select tprl.artg_pk, tprl.tprl_valor, tprg.unmo_pk,");
            sb.AppendLine("       tprg.ivas_incl, tprg.tprg_type, tprg.tprg_inac");
            sb.AppendLine("from tnht_tprl tprl");
            sb.AppendLine("inner join tnht_tppe tppe on tppe.tppe_pk = tprl.tppe_pk");
            sb.AppendLine("inner join tnht_tprg tprg on tprg.tprg_pk = tppe.tprg_pk");
            sb.AppendLine("inner join tnht_ipos ipos on ipos.ipos_fctr between tppe.tppe_dain and tppe.tppe_dafi");
            sb.AppendLine("where tprg.tprg_pk = :tprg_pk and ipos.ipos_pk = :ipos_pk");

            return sb.ToString();
        }

        protected override void Initialize()
        {
            Filters.Add("artg_pk", "tprl.artg_pk", typeof(Guid));
        }
    }
}