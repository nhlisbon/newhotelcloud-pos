﻿using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Prices
{

    public class HappyHourQuery : Query
    {
        public HappyHourQuery()
            : base("HappyHour") { }

        public HappyHourQuery(IDatabaseManager manager)
            : base("HappyHour", manager) { }

        protected override string GetCommandText()
        {
            return @"
                SELECT HAIP.HAIP_PK,
                       HOUR.HOUR_PK,
                       (SELECT MULT_DESC FROM VNHT_MULT WHERE LITE_PK = HOUR.LITE_PK AND LANG_PK = :LANG_PK) AS HOUR_DESC,
                       HOUR.HOUR_HORI,
                       HOUR.HOUR_HORF,
                       HOUR.HOUR_DIAS,
                       HOUR.TPRG_PK,
                       HOUR.ARTG_CANT,
                       HOUR.HOUR_INAC
                FROM TNHT_HAIP HAIP,
                     TNHT_HOUR HOUR
                WHERE IPOS_PK = :IPOS_PK
                  AND HAIP.HOUR_PK = HOUR.HOUR_PK
                  AND HOUR.HOUR_INAC = 0
            ";
        }
    }

}
