﻿using System;
using System.Text;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Cashier
{
    public class CashiersByUsersQuery : Query
    {
        public new const string Name = "CashiersByUsers.Query";
        public CashiersByUsersQuery()
            : base(Name) { }

        public CashiersByUsersQuery(IDatabaseManager manager)
            : base(Name, manager) { }


        /* alex: dame todas las cajas que no esten bloqueadas que puedan vender en puntos de venta autorizados para mi ususario */
        protected override string GetCommandText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select caja.caja_pk,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = caja.lite_pk and mult.lang_pk = :lang_pk) as caja_desc,");
            sb.AppendLine("       caja.caja_veca, caja.ipos_empc");
            sb.AppendLine("from tnht_caja caja");
            sb.AppendLine("where caja.deleted = '0' and caja.hote_pk = :hote_pk");

            return sb.ToString();
        }

        private string GetUserFilter()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select 1 from tnht_capv capv");
            sb.AppendLine("inner join tnht_iput iput on iput.ipos_pk = capv.ipos_pk");
            sb.AppendLine("where capv.capv_bloc = '0' and capv.caja_pk = caja.caja_pk");

            return sb.ToString();
        }

        protected override void Initialize()
        {
            Filters
                .AddQuery("util", "exists", GetUserFilter(), true)
                .Add("util_pk", "iput.util_pk", typeof(Guid));
        }
    }
}