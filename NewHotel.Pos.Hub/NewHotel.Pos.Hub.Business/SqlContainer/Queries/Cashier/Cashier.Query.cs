﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.SqlContainer.Queries.Cashier
{
    public class CashierQuery : Query
    {
        public new const string Name = "Cashier.Query";
        public const string CashierIdFilter = "CashierIdFilter";
        public const string LanguageIdParam = "lang_pk";

        public const string DescriptionColumn = "cashier_desc";


        public CashierQuery()
            : base(Name) { }

        public CashierQuery(IDatabaseManager manager)
            : base(Name, manager) { }


        protected override string GetCommandText()
        {
            return @"select cashier.caja_pk, (select mult.mult_desc from vnht_mult mult where mult.lite_pk = cashier.lite_pk and mult.lang_pk = :lang_pk) as cashier_desc,
                            cashier.caja_veca, cashier.ipos_empc
                     from   tnht_caja cashier";
        }

        protected override void Initialize()
        {
            base.Initialize();

            Filters.Add(CashierIdFilter, "cashier.caja_pk", typeof(Guid));
        }
    }
}
