﻿using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.Pos.Core;
using NewHotel.Pos.Hub.Business.Business;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Business.PersistentObjects.Tickets;
using NewHotel.Pos.Hub.Core.Logs;
using NewHotel.Pos.Hub.Model.Session;
using NewHotel.Pos.IoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business
{
	public static class BusinessConfiguration
	{
		public static void Configure(IServiceConfigurator configurator)
		{
			configurator
				.AddTransient<ITicketRepository, TicketRepository>()
				.AddTransient(() => HubLogsFactory.DefaultFactory)
				.AddTransient<ITicketBusiness, TicketBusiness>()
				.AddTransient<IStandBusiness, StandBusiness>()
				.AddTransient<IPaxWaitingListBusiness, StandBusiness>()
				.AddTransient<IProductBusiness, ProductBusiness>()
				.AddTransient<ISettingBusiness, SettingBusiness>()
				.AddTransient<IDocumentSerieBusiness, DocumentSerieBusiness>()
				.AddTransient<ICloudBusiness, CloudBusiness>()
				.AddTransient<IReservationTableBusinessCloud, CloudBusiness>()
				.AddTransient<ISettingManagementBusinessCloud, CloudBusiness>()
				.AddTransient<ITableBusiness, TableBusiness>()
				.AddTransient<ILogBusiness, LogBusiness>()
				.AddTransient<IFiscalBusiness, FiscalBusiness>()
				.AddTransient<IPaySystemBusiness, PaySystemBusiness>()
				.AddTransient<IIntegrationBusiness, IntegrationBusiness>()
                .AddTransient<ISignatureBusiness, SignatureBusiness>()
			;

			CwFactory.RegisterWithFactoryMethod<IDatabaseManager, IPosSessionContext, IOnlineExportBusiness>((manager, sessionContext) =>
			{
				return BusinessContext.Country switch
				{
					CountryCodes.Mexico when BusinessContext.SignatureMode == DocumentSign.FiscalizationMexicoCfdi => new MexOnlineExportBusiness(manager, sessionContext),
					CountryCodes.Peru when BusinessContext.SignatureMode == DocumentSign.FiscalizationPeruDFacture => new PeOnlineExportBusiness(manager, sessionContext),
					_ => throw new Exception("Sign Type of the corresponding country should be configured in PMS invoice section in order to use export of documents in POS")
				};
			});
		}
	}
}
