﻿using System;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class VoidTicketRequest
    {
        public Guid TicketId { get; set; }
        public Guid? CancellationReasonId { get; set; }
        public string Comments { get; set; }
    }
}