﻿using System;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class CloseTurnRequest : BaseQueryRequest
    {
        public Guid StandId { get; set; }
        public Guid? CashierId { get; set; }
    }
}