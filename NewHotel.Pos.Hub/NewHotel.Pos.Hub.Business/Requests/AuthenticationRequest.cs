﻿namespace NewHotel.Pos.Hub.Business.Business
{
    public class AuthenticationRequest
    {
        public string HotelId { get; set; }
        public string HashVerifier { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool Encrypted { get; set; }
        public int LanguageId { get; set; }
        public int ApplicationId { get; set; }

        public AuthenticationRequest(int language)
        {
            LanguageId = language;
            ApplicationId = 107;
            Encrypted = true;
        }
    }
}