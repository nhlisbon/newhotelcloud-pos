﻿using System;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class TableReservationFilterRequest
    {
        public int Page { get; set; }
        public int PageSize { get; set; }

        public FilterTypes? ArrivalDateFilterType { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public DateTime? ArrivalTime { get; set; }
        public string ClientName { get; set; }
        public string ClientPhone { get; set; }
        public string ClientEmail { get; set; }
        public string ReservationSerie { get; set; }

        public Guid? TableReservationId { get; set; }
        public DateTime? InDate { get; set; }
        public Guid? StandId { get; set; }
        public Guid? SlotId { get; set; }
        public Guid[] SaloonIds { get; set; }
        public Guid[] BookingSlots {  get; set; } 
        public long? KindReservation { get; set; }
        public ReservationState? StateReservation { get; set; }
    }
}