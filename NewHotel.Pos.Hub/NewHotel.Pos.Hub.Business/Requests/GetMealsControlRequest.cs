﻿using System;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class GetMealsControlRequest
    {
        public DateTime Date { get; set; }
        public Guid? InstallationId { get; set; }
        public bool JustCheckInReservation { get; set; }
    }
}