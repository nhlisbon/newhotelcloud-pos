﻿using System;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class UpdateVersionsRequest
    {
        public string DbVersion { get; set; }
        public string SyncVersion { get; set; }
        public string NewPosVersion { get; set; }
        public string HandheldVersion { get; set; }
        public string HousekeepingVersion { get; set; }
    }
}