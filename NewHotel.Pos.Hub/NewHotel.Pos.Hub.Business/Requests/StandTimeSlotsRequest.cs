﻿using NewHotel.Pos.Hub.Business.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.Requests
{
    public class StandTimeSlotsRequest : BaseQueryRequest
    {
        public Guid StandId { get; set; }
        public DateTime Date { get; set; }
    }
}
