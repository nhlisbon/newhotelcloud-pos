﻿namespace NewHotel.Pos.Hub.Business.Business
{
    public class BaseQueryRequest
    {
        public long Page { get; set; }
        public long PageSize { get; set; }
        public bool HasTotal { get; set; }
    }
}