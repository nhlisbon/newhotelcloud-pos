﻿using System;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class ClosedTicketsRequest : BaseQueryRequest
    {
        public Guid StandId { get; set; }
        public Guid CashierId { get; set; }
        public DateTime ClosedDate { get; set; }
        public long FilterType { get; set; }
        public bool? Transferred { get; set; }
    }
}