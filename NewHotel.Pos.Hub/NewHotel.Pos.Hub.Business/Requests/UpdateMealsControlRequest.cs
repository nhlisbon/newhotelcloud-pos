﻿using System;

namespace NewHotel.Pos.Hub.Business.Requests
{
    public class UpdateMealsControlRequest
    {
        public Guid Id { get; set; }
        public bool Breakfast { get; set; }
        public bool Lunch { get; set; }
        public bool Dinner { get; set; }
        public Guid? InstallationId { get; set; }
        public DateTime WorkDate { get; set; }
    }
}