﻿using System;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class CloseDayRequest : CloseTurnRequest
    {
        public DateTime? DateToClose { get; set; }
    }
}