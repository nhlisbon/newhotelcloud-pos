﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class CancelBookingTableRequest
    {
        public CancellationControlContract CancellationControlContract { get; set; }

        public bool IgnoreWarnings { get; set; }

        public Guid BookingTableId { get; set; }
    }
}