﻿using System;
using System.Text;
using System.Net;
using System.Diagnostics;
using System.Globalization;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Core;
using RestSharp;
using RestSharp.Serialization;

namespace NewHotel.Pos.Hub.Business.Business
{
    public sealed class RestInvoke
    {
        #region Members

        private const string AuthorizationHeader = "Authorization";
        private const string ClientTimezoneOffsetHeader = "ClientTimezoneOffset";

        private readonly string _url;
        private readonly Guid _hotelId;
        private readonly RestClient _client;

        private string _userName;
        private string _userPassword;
        private string _token;

        #endregion
        #region Private Methods

        private void CheckCredentials()
        {
            var userName = BusinessContext.UserName;
            var userPasssword = BusinessContext.UserPassword;
            if (userName != _userName || userPasssword != _userPassword)
            {
                _userName = userName;
                _userPassword = userPasssword;
            }
        }

        private void Authenticate(RestClient client, string resource, Guid hotelId)
        {
            CheckCredentials();

            var auth = new AuthenticationRequest((int)BusinessContext.LanguageId);
            auth.HotelId = hotelId.ToString();
            auth.UserName = _userName;
            auth.Password = _userPassword;

            var request = new RestRequest(resource, DataFormat.Json);
            AddTimezoneOffset(request);
            request.AddJsonBody(auth);
            var response = client.Post<AuthenticateResponse>(request);
            if (!response.IsSuccessful)
                throw new ApplicationException($"Error trying to authenticate on hotel {hotelId} {_userName} {_userPassword} ({response.StatusCode})");

            if (response.Data == null || response.Data.Content == null)
                throw new ApplicationException($"Empty response for hotel {hotelId}");
            else if (response.Data.Content.errorCodes != null && response.Data.Content.errorCodes.Length > 0)
            {
                var errors = new StringBuilder();
                for (int i = 0; i < response.Data.Content.errorCodes.Length; i++)
                    errors.AppendLine($"{response.Data.Content.errorCodes[i]}: {response.Data.Content.errors[i]}");
                throw new ApplicationException(errors.ToString());
            }

            _token = response.Data.Content.Token;
        }

        private void AddAuthorization(IRestRequest request)
        {
            request.AddHeader(AuthorizationHeader, $"Bearer {_token}");
        }

        private void AddTimezoneOffset(IRestRequest request)
        {
            var utcOffset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).TotalHours;
            request.AddHeader(ClientTimezoneOffsetHeader, utcOffset.ToString(CultureInfo.InvariantCulture));
        }

        private IRestResponse<T> Get<T>(RestClient client, string resource)
        {
            if (_token == null)
                Authenticate(_client, "token", _hotelId);

            var request = new RestRequest(resource, DataFormat.Json);
            AddAuthorization(request);
            AddTimezoneOffset(request);

            return client.Get<T>(request);
        }

        private IRestResponse<T> Post<T>(RestClient client, string resource, object content)
        {
            if (_token == null)
                Authenticate(_client, "token", _hotelId);

            var request = new RestRequest(resource, DataFormat.Json);
            AddAuthorization(request);
            AddTimezoneOffset(request);
            request.AddJsonBody(content);

            return client.Post<T>(request);
        }

        #endregion
        #region Public Methods

        public IRestResponse<T> Get<T>(string resource)
        {
            Trace.TraceInformation($"GET {resource}");

            var response = Get<T>(_client, resource);
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                _token = null;
                response = Get<T>(_client, resource);
            }

            if (response.StatusCode != HttpStatusCode.OK && response.Data == null)
                throw new ApplicationException($"Error response from {resource}, ({response.StatusCode})");

            return response;
        }

        public IRestResponse<T> Post<T>(string resource, object content)
        {
            Trace.TraceInformation($"POST {resource}: {Serializer.ConvertToJson(content)}");

            var response = Post<T>(_client, resource, content);
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                _token = null;
                response = Post<T>(_client, resource, content);
            }

            if (response.StatusCode != HttpStatusCode.OK && response.Data == null)
                throw new ApplicationException($"Error response from {resource}, ({response.StatusCode})");

            return response;
        }

        #endregion
        #region Constructor

        private sealed class JsonSerializer : IRestSerializer
        {
            private readonly string[] _supportedContentTypes = { "application/json", "text/json", "text/x-json", "text/javascript", "*+json" };
            private readonly DataFormat _dataFormat = DataFormat.Json;
            private string _contentType = "application/json";

            public string Serialize(object obj)
            { 
                return Serializer.ConvertToJson(obj);
            }

            public string Serialize(Parameter parameter)
            {
                return Serializer.ConvertToJson(parameter.Value);
            }

            public T Deserialize<T>(IRestResponse response)
            {
                return Serializer.ConvertFromJson<T>(response.Content);
            }

            public string[] SupportedContentTypes { get { return _supportedContentTypes; } }
            public DataFormat DataFormat { get { return _dataFormat; } }
            public string ContentType { get { return _contentType; } set { _contentType = value; } }
        }

        public RestInvoke(string url, Guid hotelId)
        {
            _url = url;
            _hotelId = hotelId;
            _client = new RestClient(_url);
            _client.ThrowOnAnyError = true;
            _client.UseSerializer(() => new JsonSerializer());
        }

        #endregion
    }
}