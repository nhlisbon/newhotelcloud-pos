﻿using System;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.Payments.Api.Models;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Contracts.PaySystem;

namespace NewHotel.Pos.Hub.Business.PersistentObjects
{
	/// <summary>
	/// TNHT_MEXT
	/// </summary>
	[PersistentTable("TNHT_MEXT", "MEXT_PK")]
	public class ExternalPaymentEntry : BasePersistentFullOperations, IExternalPaymentEntry
	{
		#region Constructor + Initialize
		public ExternalPaymentEntry(IDatabaseManager manager)
			: base(manager)
		{
		}

		public ExternalPaymentEntry(IDatabaseManager manager, object id)
			: base(manager, id)
		{
		}
		#endregion

		#region Persistent Properties
		private Guid _ticket;
		/// <summary>
		/// column="VEND_PK"
		/// Ticket
		/// </summary>
		[PersistentColumn("VEND_PK", Nullable = false)]
		public Guid Ticket
		{
			get => _ticket;
			set => Set(ref _ticket, value, nameof(Ticket));
		}

		ActionRequestType _type = ActionRequestType.None;
		/// <summary>
		/// column="MEXT_TYPE"
		/// <para>Operation type</para>
		/// </summary>
		[PersistentColumn("MEXT_TYPE", Nullable = false)]
		public ActionRequestType Type
		{
			get => _type;
			set => Set(ref _type, value, nameof(Type));
		}

		Guid _paymentMethodId;
		/// <summary>
		/// column="FORE_PK"
		/// <para>Payment type</para>
		/// </summary>
		[PersistentColumn("FORE_PK", Nullable = false)]
		public Guid PaymentMethodId
		{
			get => _paymentMethodId;
			set => Set(ref _paymentMethodId, value, nameof(PaymentMethodId));
		}

		Guid _originId;
		/// <summary>
		/// column="ORIGIN_ID"
		/// <para>Payment System Origin Identifier</para>
		/// </summary>
		[PersistentColumn("ORIGIN_ID", Nullable = false)]
		public Guid OriginId
		{
			get => _originId;
			set => Set(ref _originId, value, nameof(OriginId));
		}

		Guid? _terminalId;
		/// <summary>
		/// column="TERMINAL_ID"
		/// <para>Payment System Terminal Identifier</para>
		/// </summary>
		[PersistentColumn("TERMINAL_ID", Nullable = true)]
		public Guid? TerminalId
		{
			get => _terminalId;
			set => Set(ref _terminalId, value, nameof(TerminalId));
		}

		DateTime _workDate;
		/// <summary>
		/// column="MEXT_DATR"
		/// <para>Work date</para>
		/// </summary>
		[PersistentColumn("MEXT_DATR", Nullable = false)]
		public DateTime WorkDate
		{
			get => _workDate;
			set => Set(ref _workDate, value, nameof(WorkDate));
		}

		DateTime _date;
		/// <summary>
		/// column="MEXT_DATE"
		/// <para>Registry date</para>
		/// </summary>
		[PersistentColumn("MEXT_DATE", Nullable = false)]
		public DateTime Date
		{
			get => _date;
			set => Set(ref _date, value, nameof(Date));
		}

		decimal _amount;
		/// <summary>
		/// column="MEXT_VALO"
		/// <para>Value to pay</para>
		/// </summary>
		[PersistentColumn("MEXT_VALO", Nullable = false)]
		public decimal Amount
		{
			get => _amount;
			set => Set(ref _amount, value, nameof(Amount));
		}

		string _bodyOnDatabase;
		/// <summary>
		/// column="MEXT_BODY"
		/// <para>Local original payment request</para>
		/// </summary>
		[PersistentColumn("MEXT_BODY", Nullable = true, StringSize = int.MaxValue)]
		public string BodyOnDatabase
		{
			get => GetDatabaseValueFromBody(); // _bodyOnDatabase;
			set => Set(
				ref _bodyOnDatabase,
				value,
				nameof(BodyOnDatabase),
				(o, n) => _body = null);
		}
		// public RequestPaymentBody Body { get; set; }

		Guid? _operationId;
		/// <summary>
		/// column="OPER_ID"
		/// <para>Paymet System operation identifier</para>
		/// </summary>
		[PersistentColumn("OPER_ID", Nullable = true)]
		public Guid? OperationId
		{
			get => _operationId;
			set => Set(ref _operationId, value, nameof(OperationId));
		}

		Guid? _transactionId;
		/// <summary>
		/// column="MCAN_TRAN"
		/// <para>Paymet System resulting transaction identifier</para>
		/// </summary>
		[PersistentColumn("MCAN_TRAN", Nullable = true)]
		public Guid? TransactionId
		{
			get => _transactionId;
			set => Set(ref _transactionId, value, nameof(TransactionId));
		}

		Guid? _referencedEntryId;
		/// <summary>
		/// column="MEXT_REFE"
		/// <para>Operation to which this one references</para>
		/// </summary>
		[PersistentColumn("MEXT_REFE", Nullable = true)]
		public Guid? ReferencedEntryId
		{
			get => _referencedEntryId;
			set => Set(ref _referencedEntryId, value, nameof(ReferencedEntryId));
		}

		PaymentOperationStatus _status = PaymentOperationStatus.Pending;
		/// <summary>
		/// column="MEXT_STAT"
		/// <para>Operation status</para>
		/// </summary>
		[PersistentColumn("MEXT_STAT", Nullable = false)]
		public PaymentOperationStatus Status
		{
			get => _status;
			set => Set(ref _status, value, nameof(Status));
		}

		string _linkUrl;
		/// <summary>
		/// column="MEXT_LURL"
		/// <para>Payment link url</para>
		/// </summary>
		[PersistentColumn("MEXT_LURL", Nullable = true)]
		public string LinkUrl
		{
			get => _linkUrl;
			set => Set(ref _linkUrl, value, nameof(LinkUrl));
		}
		#endregion

		RequestPaymentBody GetBodyFromDatabaseValue()
		{
			try
			{
				if (_body != null)
					return _body;

				if (string.IsNullOrWhiteSpace(_bodyOnDatabase))
					return _body = new RequestPaymentBody();

				_body = _bodyOnDatabase.FromJSON<RequestPaymentBody>();
				_bodyOnDatabase = null;
			}
			catch (Exception ex)
			{
				_failLoadBody = ex;
				_body = new RequestPaymentBody();
			}
			return _body;
		}
		Exception _failLoadBody;
		string GetDatabaseValueFromBody()
		{
			if (_body == null)
				return null;

			return _body.ToJson(true);
		}

		RequestPaymentBody _body;
		public RequestPaymentBody Body { get => GetBodyFromDatabaseValue(); set => _body = value; }
	}
	public class RequestPaymentBody
	{
		public PaymentTransactionModel PaymentTransaction { get; set; }
		public DeviceMessageModel DeviceMessage { get; set; }
	}


}
