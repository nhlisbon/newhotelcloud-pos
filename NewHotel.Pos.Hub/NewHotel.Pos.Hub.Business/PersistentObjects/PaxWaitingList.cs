﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects
{
    [PersistentTable("TNHT_WLST", "WLST_PK")]
    public class PaxWaitingList : BasePersistentFullOperations
    {
        private string _phoneNumber;
        private string _name;
        private DateTime _registerDate;
        private Guid? _standId;
        private Guid? _salonId;
        private short _paxs;

        public PaxWaitingList(IDatabaseManager manager) : base(manager)
        {
        }

        public PaxWaitingList(IDatabaseManager manager, Guid id) : base(manager)
        {
            this.LoadObject(id);
        }

        [PersistentColumn("IPOS_PK", Nullable = true)]
        public Guid? StandId
        {
            get { return _standId; }
            set
            {
                if (value != _standId)
                {
                    _standId = value;
                    IsDirty = true;
                }
            }
        }

        [PersistentColumn("SALO_PK", Nullable = true)]
        public Guid? SaloonId
        {
            get { return _salonId; }
            set
            {
                if (value != _salonId)
                {
                    _salonId = value;
                    IsDirty = true;
                }
            }
        }

        [PersistentColumn("WLST_NAME", Nullable = false)]
        public string Name
        {
            get { return _name; }
            set
            {
                if (value != _name)
                {
                    _name = value;
                    IsDirty = true;
                }
            }
        }

        [PersistentColumn("WLST_PHONE", Nullable = true)]
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set
            {
                if (value != _phoneNumber)
                {
                    _phoneNumber = value;
                    IsDirty = true;
                }
            }
        }

        [PersistentColumn("WLST_DARE", Nullable = false)]
        public DateTime RegisterDate
        {
            get { return _registerDate; }
            set
            {
                if (value != _registerDate)
                {
                    _registerDate = value;
                    IsDirty = true;
                }
            }
        }

        [PersistentColumn("WLST_PAXS", Nullable = false)]
        public short Paxs
        {
            get { return _paxs; }
            set
            {
                if (value != _paxs)
                {
                    _paxs = value;
                    IsDirty = true;
                }
            }
        }
    }
}