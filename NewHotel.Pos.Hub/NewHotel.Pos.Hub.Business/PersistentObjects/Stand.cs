﻿using System;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Business.PersistentObjects
{
    [PersistentTable("TNHT_IPOS", "IPOS_PK")]
    public class Stand : BasePersistentFullOperations
    {
        private Guid? _compPrint;
        private bool _copyKitchenInTicketPrinter;
        private Guid? _factPrint;
        private int _shift;
        private DateTime _standWorkDate;
        private Guid? _ticketPrint;
        private Guid? _tipService;
        private TicketProductGrouping _productGrouping;
        private bool _isRoomService;
        private bool _allowChangeTableReservationStatus;

        public Stand(IDatabaseManager manager) 
            : base(manager)
        {
        }
        
        public Stand(IDatabaseManager manager, object id)
            : base(manager)
        {
            this.LoadObject(id);
        }

        [PersistentObject("LITE_ABRE")]
        public string Abbreviation { get; set; }

        [PersistentObject("LITE_DESC")]
        public string Description { get; set; }

        [PersistentColumn("CNFG_COMP")]
        public Guid? CompPrint
        {
            get => _compPrint;
            set
            {
                if (value == _compPrint) return;
                _compPrint = value;
                IsDirty = true;
            }
        }

        [PersistentColumn("IPOS_KITI")]
        public bool CopyKitchenInTicketPrinter
        {
            get => _copyKitchenInTicketPrinter;
            set
            {
                if (value == _copyKitchenInTicketPrinter) return;
                _copyKitchenInTicketPrinter = value;
                IsDirty = true;
            }
        }

        [PersistentColumn("CNFG_FACT")]
        public Guid? FactPrint
        {
            get => _factPrint;
            set
            {
                if (value == _factPrint) return;
                _factPrint = value;
                IsDirty = true;
            }
        }

        [PersistentColumn("IPOS_TUTR")]
        public int Shift
        {
            get => _shift;
            set
            {
                if (value == _shift) return;
                _shift = value;
                IsDirty = true;
            }
        }
        
        [PersistentColumn("IPOS_FCTR")]
        public DateTime StandWorkDate
        {
            get => _standWorkDate.Date;
            set
            {
                if (value == _standWorkDate) return;
                _standWorkDate = value.Date;
                IsDirty = true;
            }
        }

        [PersistentColumn("CNFG_TICK")]
        public Guid? TicketPrint
        {
            get => _ticketPrint;
            set
            {
                if (value == _ticketPrint) return;
                _ticketPrint = value;
                IsDirty = true;
            }
        }

        [PersistentColumn("IPOS_TISE")]
        public Guid? TipService
        {
            get => _tipService;
            set
            {
                if (value == _tipService) return;
                _tipService = value;
                IsDirty = true;
            }
        }

        [PersistentColumn("IPOS_TIGR")]
        public TicketProductGrouping ProductGrouping
        {
            get => _productGrouping;
            set
            {
                if (value == _productGrouping) return;
                _productGrouping = value;
                IsDirty = true;
            }
        }

        [PersistentColumn("IPOS_ROSE")]
        public bool IsRoomService
        {
            get => _isRoomService;
            set
            {
                if (value == _isRoomService) return;
                _isRoomService = value;
                IsDirty = true;
            }
        }
        
        /// <summary>
        /// RESE_CHST
        /// Allow change the status for a table reservation
        /// </summary>
        [PersistentColumn("RESE_CHST")]
        public bool AllowChangeTableReservationStatus {
            get => _allowChangeTableReservationStatus;
            set
            {
                if (value == _allowChangeTableReservationStatus) return;
                _allowChangeTableReservationStatus = value;
                IsDirty = true;
            }
        }
    }
}