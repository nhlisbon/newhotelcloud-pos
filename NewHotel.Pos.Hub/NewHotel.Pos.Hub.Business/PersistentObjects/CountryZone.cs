﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects
{
    /// <summary>
    /// table="TNHT_COMU"
    /// Comunidades/Zonas del País
    /// </summary>
    [PersistentTable("TNHT_COMU", "COMU_PK")]
    public class CountryZone : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public CountryZone(IDatabaseManager manager)
            : base(manager) { }

        public CountryZone(IDatabaseManager manager, object id)
            : this(manager)
        {
            this.LoadObject(id);
        }

        #endregion
        #region Public Properties

        /// <summary>
        /// NACI_PK
        /// Pais asociado
        /// </summary>
        private string _countryId;
        [PersistentColumn("NACI_PK")]
        public string CountryId
        {
            get { return _countryId; }
            set
            {
                if (value != _countryId)
                {
                    _countryId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// COMU_DESC
        /// Descripción de la comunidad autonoma o zona del país
        /// </summary>
        private string _description;
        [PersistentColumn("COMU_DESC")]
        public string Description
        {
            get { return _description; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// COMU_CAUX
        /// Código libre
        /// </summary>
        private string _freeCode;
        [PersistentColumn("COMU_CAUX")]
        public string FreeCode
        {
            get { return _freeCode; }
            set
            {
                if (value != _freeCode)
                {
                    _freeCode = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// COMU_LMOD
        /// </summary>
        [PersistentLastModifiedColumn("COMU_LMOD")]
        public DateTime LastModified { get; set; }

        #endregion
    }
}