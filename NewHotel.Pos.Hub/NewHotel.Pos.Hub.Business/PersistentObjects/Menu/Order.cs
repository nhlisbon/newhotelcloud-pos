using System;
using NewHotel.Contracts;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Menu
{
    /// <summary>
    /// Menu order
    /// </summary>
    [PersistentTable("TNHT_MEOR", "MEOR_PK")]
    public class Order : BasePersistent, IDataFullOperations
    {
        public Order(IDatabaseManager manager) : base(manager)
        {
            _preparations = GetList<Preparation>(Manager, x => x.OrderId);
        }

        public Order(IDatabaseManager manager, object id) : this(manager)
        {
            this.LoadObject(id);
        }

        public bool LoadObject(object id, bool throwNoDataFoundException)
        {
            return LoadObjectFromDB(id, throwNoDataFoundException);
        }

        public bool SaveObject()
        {
            if (!PersistObjectToDB()) return false;
            if (_preparations.Populated)
                _preparations.PersistOrDeleteObjects();

            return true;
        }

        public bool DeleteObject()
        {
            return DeleteObjectFromDB();
        }

        /// <summary>
        /// column="HOTE_PK"
        /// Hotel id
        /// </summary>
        private Guid _hotelId;

        [PersistentColumn("HOTE_PK")]
        public Guid HotelId
        {
            get => _hotelId;
            set
            {
                if (value == _hotelId) return;
                _hotelId = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="IPOS_PK"
        /// Pos id
        /// </summary>
        private Guid _posId;

        [PersistentColumn("IPOS_PK")]
        public Guid PosId
        {
            get => _posId;
            set
            {
                if (value == _posId) return;
                _posId = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="SALO_PK"
        /// Saloon id
        /// </summary>
        private Guid _saloonId;

        [PersistentColumn("SALO_PK")]
        public Guid SaloonId
        {
            get => _saloonId;
            set
            {
                if (value == _saloonId) return;
                _saloonId = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="MESA_PK"
        /// Table id
        /// </summary>
        private Guid _tableId;

        [PersistentColumn("MESA_PK")]
        public Guid TableId
        {
            get => _tableId;
            set
            {
                if (value == _tableId) return;
                _tableId = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="ARTG_PK"
        /// Product id
        /// </summary>
        private Guid _productId;

        [PersistentColumn("ARTG_PK")]
        public Guid ProductId
        {
            get => _productId;
            set
            {
                if (value == _productId) return;
                _productId = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="MEOR_CANT"
        /// Quantity
        /// </summary>
        private decimal _quantity;

        [PersistentColumn("MEOR_CANT")]
        public decimal Quantity
        {
            get => _quantity;
            set
            {
                if (value == _quantity) return;
                _quantity = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="MEOR_NOTA"
        /// Product id
        /// </summary>
        private string _note;

        [PersistentColumn("MEOR_NOTA")]
        public string Note
        {
            get => _note;
            set
            {
                if (value == _note) return;
                _note = value;
                IsDirty = true;
            }
        }

        private readonly IPersistentList<Preparation> _preparations;

        [ReflectionExclude]
        public IPersistentList<Preparation> Preparations
        {
            get
            {
                if (!_preparations.Populated)
                    _preparations.Execute(Id);

                return _preparations;
            }
        }
    }
}