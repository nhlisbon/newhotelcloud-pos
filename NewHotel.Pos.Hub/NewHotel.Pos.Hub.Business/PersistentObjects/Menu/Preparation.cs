using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Menu
{
    /// <summary>
    /// table="TNHT_MEPR"
    /// Menu order preparation
    /// </summary>
    [PersistentTable("TNHT_MEPR", "MEPR_PK")]
    public class Preparation : BasePersistentFullOperations
    {
        public Preparation(IDatabaseManager manager) : base(manager)
        {
        }

        /// <summary>
        /// column="MEOR_PK"
        /// Order id
        /// </summary>
        private Guid _orderId;

        [PersistentColumn("MEOR_PK")]
        public Guid OrderId
        {
            get => _orderId;
            set
            {
                if (value == _orderId) return;
                _orderId = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="PREP_PK"
        /// Preparation id
        /// </summary>
        private Guid _preparationId;

        [PersistentColumn("PREP_PK")]
        public Guid PreparationId
        {
            get => _preparationId;
            set
            {
                if (value == _preparationId) return;
                _preparationId = value;
                IsDirty = true;
            }
        }
    }
}