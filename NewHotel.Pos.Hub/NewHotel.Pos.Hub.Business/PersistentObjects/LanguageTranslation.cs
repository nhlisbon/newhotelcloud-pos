﻿using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using NewHotel.Core;
using NewHotel.Contracts;

namespace NewHotel.Business
{
    public class TranslationInfo
    {
        #region Constants

        private const int maxLength = 255;

        #endregion
        #region Members

        public readonly long LanguageId;
        public readonly string LanguageName;
        public readonly string Translation;

        #endregion
        #region Constructor

        public TranslationInfo(long languageId, string languageName, string translation)
        {
            LanguageId = languageId;
            LanguageName = languageName;
            Translation = translation != null && translation.Length > maxLength
                ? translation.Substring(0, maxLength) : translation;
        }

        public TranslationInfo(long languageId, string languageName)
            : this(languageId, languageName, string.Empty)
        {
        }

        #endregion
    }

    public class LanguageTranslation : BasePersistentFullOperations, IEnumerable<TranslationInfo>
    {
        #region Members

        private static readonly string LoadLanguagesCommandText;
        private static readonly string LoadTranslationsCommandText;
        private static readonly string DeleteLiteralCommandText;
        private static readonly string InsertLiteralCommandText;

        private static readonly IDictionary<string, string> _mergeTranslationCommandText =
            new Dictionary<string, string>();
        private static readonly IDictionary<long, Tuple<long, string>[]> _languages =
            new Dictionary<long, Tuple<long, string>[]>();

        private bool _initialized = false;
        private readonly IDictionary<long, TranslationInfo> _translations =
            new Dictionary<long, TranslationInfo>();

        #endregion
        #region Properties

        private IEnumerable<TranslationInfo> Translations
        {
            get
            {
                if (!_initialized)
                    Load();

                return _translations.Values;
            }
        }

        [ReflectionExclude(Hide = true)]
        public IEnumerable<long> Languages
        {
            get { return _languages.Keys; }
        }

        [ReflectionExclude(Hide = true)]
        public string this[long language]
        {
            get
            {
                if (!_initialized)
                    Load();

                TranslationInfo info;
                if (!_translations.TryGetValue(language, out info))
                    throw new ArgumentOutOfRangeException(string.Format("language ({0})", language));

                return info.Translation;
            }
            set
            {
                if (!_initialized)
                    Load();

                value = value ?? string.Empty;
                if (this[language] != value)
                {
                    TranslationInfo info;
                    if (!_translations.TryGetValue(language, out info))
                        throw new ArgumentOutOfRangeException(string.Format("language ({0})", language));

                    _translations[language] = new TranslationInfo(language, info.LanguageName, value);
                    IsDirty = true;
                }
            }
        }

        [ReflectionExclude(Hide = true)]
        public override bool IsEmpty
        {
            get { return !_translations.Values.Any(x => !string.IsNullOrEmpty(x.Translation)); }
        }

        #endregion
        #region Constructor + Initialize

        public LanguageTranslation(IDatabaseManager manager, bool initialize)
            : base(manager)
        {
            if (initialize)
                Load();
        }

        public LanguageTranslation(IDatabaseManager manager)
            : this(manager, true) { }

        public LanguageTranslation(IDatabaseManager manager, Guid id)
            : base(manager)
        {
            Id = id;
            Load();
        }

        #endregion
        #region Private Methods

        private void Load()
        {
            Manager.Open();
            try
            {
                // initialize languages
                var languages = GetLanguages(Manager, NewHotelAppContext.GetInstance().Language);

                _translations.Clear();
                for (int index = 0; index < languages.Length; index++)
                {
                    var language = languages[index];
                    _translations.Add(language.Item1, new TranslationInfo(language.Item1, language.Item2));
                }

                using (var command = Manager.CreateCommand("NewHotel.Load.Translations"))
                {
                    command.CommandText = LoadTranslationsCommandText;
                    Manager.CreateParameter(command, "lite_pk", Manager.ConvertValueType(Id));

                    using (var reader = Manager.ExecuteReader(command, CommandBehavior.SingleResult))
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(1))
                                {
                                    var languageId = reader.GetInt64(0);
                                    TranslationInfo info;
                                    if (_translations.TryGetValue(languageId, out info))
                                        _translations[languageId] = new TranslationInfo(info.LanguageId, info.LanguageName, reader.GetString(1));
                                }
                            }

                            IsDirty = false;
                            _initialized = true;
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }
                }
            }
            finally
            {
                Manager.Close();
            }
        }

        #endregion
        #region Static Methods

        static LanguageTranslation()
        {
            // load languages
            var sb = new StringBuilder();
            sb.AppendLine("select lang.lang_pk, mult.mult_desc lang_name, lang.lang_prio");
            sb.AppendLine("from tnht_lang lang inner join tnht_licl licl on licl.licl_pk = lang.lang_pk");
            sb.AppendLine("left join tnht_mult mult on mult.lite_pk = licl.lite_pk and mult.lang_pk = :lang_pk");
            sb.AppendLine("order by lang.lang_prio");
            LoadLanguagesCommandText = sb.ToString();

            // load translations
            LoadTranslationsCommandText = "select mult.lang_pk, mult.mult_desc from tnht_mult mult where mult.lite_pk = :lite_pk";

            // delete literal
            DeleteLiteralCommandText = "delete from tnht_lite where lite_pk = :lite_pk";

            // insert literal
            sb = new StringBuilder();
            sb.AppendLine("merge into tnht_lite lite");
            sb.AppendLine("using dual on (lite.lite_pk = :lite_pk)");
            sb.AppendLine("when not matched then");
            sb.AppendLine("insert (lite_pk) values (:lite_pk)");
            InsertLiteralCommandText = sb.ToString();
        }

        public static Tuple<long, string>[] GetLanguages(IDatabaseManager manager, long currentLanguageId)
        {
            lock (_languages)
            {
                Tuple<long, string>[] langs;
                if (!_languages.TryGetValue(currentLanguageId, out langs))
                {
                    using (var command = manager.CreateCommand("NewHotel.Load.Languages"))
                    {
                        command.CommandText = LoadLanguagesCommandText;
                        manager.CreateParameter(command, "lang_pk", manager.ConvertValueType(currentLanguageId));

                        using (var reader = manager.ExecuteReader(command, CommandBehavior.SingleResult))
                        {
                            try
                            {
                                var languages = new List<Tuple<long, string>>();
                                while (reader.Read())
                                {
                                    var languageId = reader.GetInt64(0);
                                    languages.Add(Tuple.Create(languageId, reader.IsDBNull(1) ? "{" + languageId.ToString() + "}" : reader.GetString(1)));
                                }

                                langs = languages.ToArray();
                                _languages.Add(currentLanguageId, langs);
                            }
                            finally
                            {
                                reader.Close();
                            }
                        }
                    }
                }

                return langs;
            }
        }

        private static string GetMergeTranslationCommandText(IEnumerable<long> translationIds)
        {
            var key = string.Join("+", translationIds);

            string mergeTranslationCommandText;
            if (!_mergeTranslationCommandText.TryGetValue(key, out mergeTranslationCommandText))
            {
                // insert/update/delete translations
                var sb = new StringBuilder();
                sb.AppendLine("merge into tnht_mult mult");
                sb.AppendLine("using (select lang_pk, case");
                foreach (var translationId in translationIds)
                    sb.AppendLine(string.Format("when lang_pk = :lang_pk_{0} then :mult_desc_{0}", translationId));
                sb.AppendLine("end as lang_desc from tnht_lang) lang");
                sb.AppendLine("on (mult.lite_pk = :lite_pk and mult.lang_pk = lang.lang_pk)");
                sb.AppendLine("when not matched then");
                sb.AppendLine("insert (lite_pk, lang_pk, mult_desc)");
                sb.AppendLine("values (:lite_pk, lang.lang_pk, lang.lang_desc)");
                sb.AppendLine("where trim(lang.lang_desc) is not null");
                sb.AppendLine("when matched then");
                sb.AppendLine("update set mult_desc = lang.lang_desc");
                sb.AppendLine("delete where trim(mult_desc) is null");

                mergeTranslationCommandText = sb.ToString();
                _mergeTranslationCommandText.Add(key, mergeTranslationCommandText);
            }

            return mergeTranslationCommandText;
        }

        public static implicit operator string(LanguageTranslation languageTranslation)
        {
            if (languageTranslation == null)
                return null;

            var translation = languageTranslation[NewHotelAppContext.GetInstance().Language];

            if (string.IsNullOrEmpty(translation))
            {
                var info = languageTranslation.FirstOrDefault(x => !string.IsNullOrEmpty(x.Translation));
                translation = info != null ? info.Translation : string.Empty;
            }

            return translation;
        }

        #endregion
        #region IEnumerable<TranslationInfo> Members

        public bool Contains(long language)
        {
            return _translations.ContainsKey(language);
        }

        public string TranslateTo(long language)
        {
            if (_translations.ContainsKey(language) && !string.IsNullOrEmpty(_translations[language].Translation))
                return _translations[language].Translation;
            else
            {
                string result = this;
                return result;
            }
        }

        public IEnumerator<TranslationInfo> GetEnumerator()
        {
            return Translations.GetEnumerator();
        }

        #endregion
        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Translations.GetEnumerator();
        }

        #endregion
        #region IData Operations

        public override bool SaveObject()
        {
            if (IsEmpty)
                return false;

            if (IsDirty)
            {
                Manager.Open();
                try
                {
                    // insert a literal if necessary
                    using (var command = Manager.CreateCommand("NewHotel.Insert.Literal.LanguageTranslation"))
                    {
                        command.CommandText = InsertLiteralCommandText;
                        Manager.CreateParameter(command, "lite_pk", Id);
                        Manager.ExecuteNonQuery(command);
                    }

                    // insert/update/delete translations
                    using (var command = Manager.CreateCommand("NewHotel.Merge.Translation.LanguageTranslation"))
                    {
                        command.CommandText = GetMergeTranslationCommandText(_translations.Keys);
                        foreach (var translation in _translations.Values)
                        {
                            Manager.CreateParameter(command, string.Format("lang_pk_{0}", translation.LanguageId), translation.LanguageId);
                            var desc = " ";
                            if (!string.IsNullOrEmpty(translation.Translation))
                                desc = translation.Translation;
                            Manager.CreateParameter(command, string.Format("mult_desc_{0}", translation.LanguageId), desc);
                        }
                        Manager.CreateParameter(command, "lite_pk", Id);
                        Manager.ExecuteNonQuery(command);
                    }

                    IsDirty = false;
                }
                finally
                {
                    Manager.Close();
                }
            }

            return true;
        }

        public override bool LoadObject(object id, bool throwNoDataFoundException)
        {
            Id = id;
            Load();
            return true;
        }

        public override bool DeleteObject()
        {
            Manager.Open();
            try
            {
                // delete a literal with all translations
                using (var command = Manager.CreateCommand("NewHotel.Delete.Literal.LanguageTranslation"))
                {
                    command.CommandText = DeleteLiteralCommandText;
                    Manager.CreateParameter(command, "lite_pk", Id);
                    Manager.ExecuteNonQuery(command);
                }

                return true;
            }
            finally
            {
                Manager.Close();
            }
        }

        #endregion
    }
}
