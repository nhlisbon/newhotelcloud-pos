﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects
{
    [PersistentTable("TNHT_HOTE", "HOTE_PK")]
    public class Hotel : BasePersistentFullOperations
    {
        #region Constructor + Initialize
        public Hotel(IDatabaseManager manager)
            : base(manager)
        {

        }
        public Hotel(IDatabaseManager manager, object id)
            : base(manager)
        {
            this.LoadObject(id);
        }

        #endregion
        #region Persistent Properties

        /// <summary>
        /// Abbreviature
        /// col=HOTE_ABRE
        /// </summary>
        private string _abbreviature;
        [PersistentColumn("HOTE_ABRE")]
        public string Abbreviature
        {
            get => _abbreviature;
            set
            {
                if (value != _abbreviature)
                {
                    _abbreviature = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// Description
        /// col=HOTE_DESC
        /// </summary>
        private string _description;
        [PersistentColumn("HOTE_DESC")]
        public string Description
        {
            get => _description;
            set
            {
                if (value != _description)
                {
                    _description = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// Auxiliar code
        /// col=HOTE_CAUX
        /// </summary>
        private string _auxiliarCode;
        [PersistentColumn("HOTE_CAUX")]
        public string AuxiliarCode
        {
            get => _auxiliarCode;
            set
            {
                if (value != _auxiliarCode)
                {
                    _auxiliarCode = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// Nationality
        /// col=NACI_PK
        /// </summary>
        private string _nationality;
        [PersistentColumn("NACI_PK")]
        public string Nationality
        {
            get => _nationality;
            set
            {
                if (value != _nationality)
                {
                    _nationality = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// Logotype
        /// col=HOTE_LOGO
        /// </summary>
        private string _logotype;
        [PersistentColumn("HOTE_LOGO")]
        public string Logotype
        {
            get => _logotype;
            set
            {
                if (value != _logotype)
                {
                    _logotype = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// Fiscal Number
        /// col=FISC_NUMB
        /// </summary>
        private string _fiscalNumber;
        [PersistentColumn("FISC_NUMB")]
        public string FiscalNumber
        {
            get => _fiscalNumber;
            set
            {
                if (value != _fiscalNumber)
                {
                    _fiscalNumber = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// HOME_ADDR1
        /// Direccion Hotel Linea 1
        /// </summary>
        private string _homeAddress1;
        [PersistentColumn("HOME_ADDR1")]
        public string HomeAddress1
        {
            get { return _homeAddress1; }
            set
            {
                if (value != _homeAddress1)
                {
                    _homeAddress1 = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// HOME_ADDR2
        /// Direccion Hotel Linea 2
        /// </summary>
        private string _homeAddress2;
        [PersistentColumn("HOME_ADDR2")]
        public string HomeAddress2
        {
            get { return _homeAddress2; }
            set
            {
                if (value != _homeAddress2)
                {
                    _homeAddress2 = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// HOME_DOOR
        /// Numero de puerta Direccion Hotel 
        /// </summary>
        private string _homeDoor;
        [PersistentColumn("HOME_DOOR")]
        public string HomeDoor
        {
            get { return _homeDoor; }
            set
            {
                if (value != _homeDoor)
                {
                    _homeDoor = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="HOME_LOCA"
        /// Localidad Direccion Hotel
        /// </summary>
        private string _homeLocation;
        [PersistentColumn("HOME_LOCA", Nullable = true)]
        public string HomeLocation
        {
            get { return _homeLocation; }
            set
            {
                if (value != _homeLocation)
                {
                    _homeLocation = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="HOME_COPO"
        /// Codigo Postal Direccion Hotel
        /// </summary>
        private string _homePostalCode;
        [PersistentColumn("HOME_COPO", Nullable = true)]
        public string HomePostalCode
        {
            get { return _homePostalCode; }
            set
            {
                if (value != _homePostalCode)
                {
                    _homePostalCode = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="HOME_DIST"
        /// Distrito Direccion Hotel
        /// </summary>
        private Guid? _homeDistrictId;
        [PersistentColumn("HOME_DIST", Nullable = true)]
        public Guid? HomeDistrictId
        {
            get { return _homeDistrictId; }
            set
            {
                if (value != _homeDistrictId)
                {
                    _homeDistrictId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// FISC_ADDR1
        /// Direccion Fiscal Hotel Linea 1
        /// </summary>
        private string _fiscalAddress1;
        [PersistentColumn("FISC_ADDR1")]
        public string FiscalAddress1
        {
            get { return _fiscalAddress1; }
            set
            {
                if (value != _fiscalAddress1)
                {
                    _fiscalAddress1 = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// FISC_ADDR2
        /// Direccion Fiscal Hotel Linea 2
        /// </summary>
        private string _fiscalAddress2;
        [PersistentColumn("FISC_ADDR2")]
        public string FiscalAddress2
        {
            get { return _fiscalAddress2; }
            set
            {
                if (value != _fiscalAddress2)
                {
                    _fiscalAddress2 = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// FISC_DOOR
        /// Numero Puerta Direccion Fiscal Hotel 
        /// </summary>
        private string _fiscalDoor;
        [PersistentColumn("FISC_DOOR")]
        public string FiscalDoor
        {
            get { return _fiscalDoor; }
            set
            {
                if (value != _fiscalDoor)
                {
                    _fiscalDoor = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="FISC_LOCA"
        /// Localidad Fiscal Direccion Hotel
        /// </summary>
        private string _fiscalLocation;
        [PersistentColumn("FISC_LOCA", Nullable = true)]
        public string FiscalLocation
        {
            get { return _fiscalLocation; }
            set
            {
                if (value != _fiscalLocation)
                {
                    _fiscalLocation = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="FISC_COPO"
        /// Codigo Postal Fiscal del Hotel
        /// </summary>
        private string _fiscalPostalCode;
        [PersistentColumn("FISC_COPO", Nullable = true)]
        public string FiscalPostalCode
        {
            get { return _fiscalPostalCode; }
            set
            {
                if (value != _fiscalPostalCode)
                {
                    _fiscalPostalCode = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="FISC_DIST"
        /// Distrito Direccion Hotel
        /// </summary>
        private Guid? _fiscalDistrictId;
        [PersistentColumn("FISC_DIST", Nullable = true)]
        public Guid? FiscalDistrictId
        {
            get { return _fiscalDistrictId; }
            set
            {
                if (value != _fiscalDistrictId)
                {
                    _fiscalDistrictId = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
    }
}