﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewHotel.Business;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects
{
    /// <summary>
    /// TNHT_CAJA
    /// Table cashiers
    /// </summary>
    [PersistentTable("TNHT_CAJA", "CAJA_PK")]
    public class Cashier : BasePersistentFullOperations
    {
        private Guid _hotelId;
        private bool _seeOnlyOwnTickets;
        private bool _activeUserSalesInCashierReport;

        public Cashier(IDatabaseManager manager) : base(manager)
        {
            Description = new LanguageTranslation(Manager);
        }

        public Cashier(IDatabaseManager manager, Guid cashierId) : this(manager)
        {
            LoadObjectFromDB(cashierId);
        }


        /// <summary>
        /// LITE_PK
        /// Cashier description
        /// </summary>
        [PersistentObject("LITE_PK")]
        public LanguageTranslation Description { get; set; }


        /// <summary>
        /// HOTE_PK
        /// Hotel Id
        /// </summary>
        [PersistentColumn("HOTE_PK")]
        public Guid HotelId
        {
            get => _hotelId;
            set
            {
                if (value != _hotelId)
                {
                    _hotelId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// CAJA_VECA
        /// Flag: Each cashier can only see the tickets opened by the
        /// </summary>
        [PersistentColumn("CAJA_VECA")]
        public bool SeeOnlyOwnTickets
        {
            get => _seeOnlyOwnTickets;
            set
            {
                if (value != _seeOnlyOwnTickets)
                {
                    _seeOnlyOwnTickets = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// IPOS_EMPC
        /// Flag: Cash reports only include active employee sales
        /// </summary>
        [PersistentColumn("IPOS_EMPC")]
        public bool ActiveUserSalesInCashierReport
        {
            get => _activeUserSalesInCashierReport;
            set
            {
                if (value != _activeUserSalesInCashierReport)
                {
                    _activeUserSalesInCashierReport = value;
                    IsDirty = true;
                }
            }
        }
    }
}
