﻿using NewHotel.Core;
using NewHotel.Contracts.Pos;
using System;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Tickets
{
	[PersistentTable("TNHT_VEND", "VEND_PK")]
	public class TicketGeneralInformation : BasePersistent, IDataLoad, ICanExchangeCurrency
	{
		public TicketGeneralInformation(IDatabaseManager manager) : base(manager)
		{
		}

		private string _baseCurrency;
		/// <summary>
		/// column="CAMB_UNMO"
		/// Moneda a cambiar
		/// </summary>
		[PersistentColumn("CAMB_UNMO")]
		public string BaseCurrency
		{
			get => _baseCurrency;
			set
			{
				if (value != _baseCurrency)
				{
					_baseCurrency = value;
					IsDirty = true;
				}
			}
		}

		private decimal _exchangeFactor = 1;
		/// <summary>
		/// column="CAMB_VALO"
		/// Factor de cambio
		/// </summary>
		[PersistentColumn("CAMB_VALO")]
		public decimal ExchangeFactor
		{
			get => _exchangeFactor;
			set
			{
				if (value != _exchangeFactor)
				{
					_exchangeFactor = value;
					IsDirty = true;
				}
			}
		}

		#region IDataLoad Members
		public bool LoadObject(object id, bool throwNoDataFoundException)
		{
			return LoadObjectFromDB(id, throwNoDataFoundException);
		}
		#endregion
	}

	public static class TicketGeneralInformationSupport
	{
		public static TicketGeneralInformation GetTicketGeneralInformation(this IDatabaseManager manager, Guid id)
		{
			var result = new TicketGeneralInformation(manager);
			result.LoadObject(id);

			return result;
		}
	}
}