﻿using System;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.DataAnnotations;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Tickets
{
    /// <summary>
    /// table="TNHT_CLDI"
    /// Dietas de Clientes por posición
    /// </summary>
    [PersistentTable("TNHT_CLDI", "CLDI_PK")]
    public class ClientByPositionDiet : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public ClientByPositionDiet(IDatabaseManager manager)
            : base(manager)
        {
        }

        public ClientByPositionDiet(IDatabaseManager manager, object id)
            : this(manager)
        {
            this.LoadObject(id);
        }

        #endregion     
        #region Persistent Properties

        /// <summary>
        /// column="CLVE_PK"
        /// Cliente por posición
        /// </summary>
        private Guid _clientByPositionId;
        [PersistentColumn("CLVE_PK")]
        public Guid ClientByPositionId
        {
            get { return _clientByPositionId; }
            set
            {
                if (value != _clientByPositionId)
                {
                    _clientByPositionId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="DIET_PK"
        /// Id de dieta
        /// </summary>
        private Guid _dietId;
        [PersistentColumn("DIET_PK")]
        public Guid DietId
        {
            get { return _dietId; }
            set
            {
                if (value != _dietId)
                {
                    _dietId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="DIET_DESC"
        /// Descripción de la dieta
        /// </summary>
        private string _description;
        [PersistentColumn("DIET_DESC")]
        public string Description
        {
            get { return _description; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
    }
}