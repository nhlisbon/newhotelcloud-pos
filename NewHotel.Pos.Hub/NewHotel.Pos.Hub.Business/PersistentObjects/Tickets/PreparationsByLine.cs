﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Tickets
{
    /// <summary>
    /// table="TNHT_ARVP
    /// Preparaciones por linea de Productos
    /// </summary>
    [PersistentTable("TNHT_ARVP", "ARVP_PK")]
    public class PreparationsByLine : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public PreparationsByLine(IDatabaseManager manager)
            : base (manager)
        {
        }

        public PreparationsByLine(IDatabaseManager manager, object id)
            : this (manager)
        {
            this.LoadObject(id);
        }

        #endregion

        #region Persistent Properties

        /// <summary>
        /// column="ARVE_PK"
        /// Linea de Producto
        /// </summary>
        private Guid _productLineId;
        [PersistentColumn("ARVE_PK")]
        public Guid ProductLineId
        {
            get { return _productLineId; }
            set
            {
                if (value != _productLineId)
                {
                    _productLineId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="PREP_PK"
        /// Preparacion
        /// </summary>
        private Guid _preparationId;
        [PersistentColumn("PREP_PK")]
        public Guid PreparationId
        {
            get { return _preparationId; }
            set
            {
                if (value != _preparationId)
                {
                    _preparationId = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
    }
}