﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Tickets
{
    /// <summary>
    /// table="TNHT_TITR"
    /// Transferencia de tickets
    /// </summary>
    [PersistentTable("TNHT_TITR", "TITR_PK")]
    public class TicketTransfer : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public TicketTransfer(IDatabaseManager manager)
            : base(manager)
        {
        }

        public TicketTransfer(IDatabaseManager manager, object id)
            : base(manager)
        {
            this.LoadObject(id);
        }

        #endregion     
        #region Persistent Properties

        /// <summary>
        /// column="VEND_ORIG"
        /// Ticket de donde se transfiere
        /// </summary>
        private Guid _ticketOrig;
        [PersistentColumn("VEND_TRAN")]
        public Guid TicketOrig
        {
            get { return _ticketOrig; }
            set
            {
                if (value != _ticketOrig)
                {
                    _ticketOrig = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="IPOS_ORIG"
        /// Punto de venta de donde se transfiere
        /// </summary>
        private Guid _standOrig;
        [PersistentColumn("IPOS_ORIG")]
        public Guid StandOrig
        {
            get { return _standOrig; }
            set
            {
                if (value != _standOrig)
                {
                    _standOrig = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="CAJA_ORIG"
        /// Caja de donde se transfiere
        /// </summary>
        private Guid _cajaOrig;
        [PersistentColumn("CAJA_ORIG")]
        public Guid CajaOrig
        {
            get { return _cajaOrig; }
            set
            {
                if (value != _cajaOrig)
                {
                    _cajaOrig = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="VEND_DEST"
        /// Ticket a donde se transfiere
        /// </summary>
        private Guid? _ticketDest;
        [PersistentColumn("VEND_ACEP")]
        public Guid? TicketDest
        {
            get { return _ticketDest; }
            set
            {
                if (value != _ticketDest)
                {
                    _ticketDest = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="IPOS_DEST"
        /// Punto de venta a donde se transfiere
        /// </summary>
        private Guid _standDest;
        [PersistentColumn("IPOS_DEST")]
        public Guid StandDest
        {
            get { return _standDest; }
            set
            {
                if (value != _standDest)
                {
                    _standDest = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="CAJA_DEST"
        /// Caja a donde se transfiere
        /// </summary>
        private Guid? _cajaDest;
        [PersistentColumn("CAJA_DEST")]
        public Guid? CajaDest
        {
            get { return _cajaDest; }
            set
            {
                if (value != _cajaDest)
                {
                    _cajaDest = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="UTIL_TRAN"
        /// Usuario que hace la transferencia
        /// </summary>
        private Guid _utilTran;
        [PersistentColumn("UTIL_TRAN")]
        public Guid UtilTran
        {
            get { return _utilTran; }
            set
            {
                if (value != _utilTran)
                {
                    _utilTran = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="TRAN_DATE"
        /// Fecha en que se hace la transferencia
        /// </summary>
        private DateTime _tranDate;
        [PersistentColumn("TRAN_DATE")]
        public DateTime TranDate
        {
            get { return _tranDate; }
            set
            {
                if (value != _tranDate)
                {
                    _tranDate = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="TRAN_TIME"
        /// Hora en que se hace la transferencia
        /// </summary>
        private DateTime _tranTime;
        [PersistentColumn("TRAN_TIME")]
        public DateTime TranTime
        {
            get { return _tranTime; }
            set
            {
                if (value != _tranTime)
                {
                    _tranTime = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="UTIL_ACEP"
        /// Usuario que acepta la transferencia
        /// </summary>
        private Guid? _utilAcep;
        [PersistentColumn("UTIL_ACEP")]
        public Guid? UtilAcep
        {
            get { return _utilAcep; }
            set
            {
                if (value != _utilAcep)
                {
                    _utilAcep = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ACEP_DATE"
        /// Fecha en que se acepta la transferencia
        /// </summary>
        private DateTime? _acepDate;
        [PersistentColumn("ACEP_DATE")]
        public DateTime? AcepDate
        {
            get { return _acepDate; }
            set
            {
                if (value != _acepDate)
                {
                    _acepDate = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ACEP_TIME"
        /// Hora en que se acepta la transferencia
        /// </summary>
        private DateTime? _acepTime;
        [PersistentColumn("ACEP_TIME")]
        public DateTime? AcepTime
        {
            get { return _acepTime; }
            set
            {
                if (value != _acepTime)
                {
                    _acepTime = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
    }
}