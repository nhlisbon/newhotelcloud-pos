﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Tickets
{
    /// <summary>
    /// table="TNHT_TABA
    /// Areas de despacho por linea de productos de tipo table
    /// </summary>
    [PersistentTable("TNHT_TABA", "TABA_PK")]
    public class ProductTableLineArea : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public ProductTableLineArea(IDatabaseManager manager)
            : base (manager)
        {
        }

        public ProductTableLineArea(IDatabaseManager manager, object id)
            : this (manager)
        {
            this.LoadObject(id);
        }

        #endregion
        
        #region Persistent Properties

        /// <summary>
        /// column="TABL_PK"
        /// Product table line
        /// </summary>
        private Guid _productTableLineId;
        [PersistentColumn("TABL_PK")]
        public Guid ProductTableLineId
        {
            get => _productTableLineId;
            set
            {
                if (value == _productTableLineId) return;
                _productTableLineId = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="AREA_PK"
        /// Area
        /// </summary>
        private Guid _areaId;
        [PersistentColumn("AREA_PK")]
        public Guid AreaId
        {
            get => _areaId;
            set
            {
                if (value == _areaId) return;
                _areaId = value;
                IsDirty = true;
            }
        }

        #endregion
    }
}