﻿using System;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.DataAnnotations;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Tickets
{
    /// <summary>
    /// table="TNHT_CLPR"
    /// Preferencias de Clientes por posición
    /// </summary>
    [PersistentTable("TNHT_CLPR", "CLPR_PK")]
    public class ClientByPositionPreference : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public ClientByPositionPreference(IDatabaseManager manager)
            : base(manager)
        {
        }

        public ClientByPositionPreference(IDatabaseManager manager, object id)
            : this(manager)
        {
            this.LoadObject(id);
        }

        #endregion     
        #region Persistent Properties

        /// <summary>
        /// column="CLVE_PK"
        /// Cliente por posición
        /// </summary>
        private Guid _clientByPositionId;
        [PersistentColumn("CLVE_PK")]
        public Guid ClientByPositionId
        {
            get { return _clientByPositionId; }
            set
            {
                if (value != _clientByPositionId)
                {
                    _clientByPositionId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="PREF_PK"
        /// Id de la preferencia
        /// </summary>
        private Guid _preferenceId;
        [PersistentColumn("PREF_PK")]
        public Guid PreferenceId
        {
            get { return _preferenceId; }
            set
            {
                if (value != _preferenceId)
                {
                    _preferenceId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="PREF_DESC"
        /// Descripción de la preferencia
        /// </summary>
        private string _description;
        [PersistentColumn("PREF_DESC")]
        public string Description
        {
            get { return _description; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
    }
}