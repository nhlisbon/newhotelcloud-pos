﻿using System;
using System.Linq;
using NewHotel.DataAnnotations;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.Pos.Localization;
using NewHotel.Contracts.Pos;
using NewHotel.Pos.Hub.Core.Logs;
using NewHotel.Pos.Core.Ext;
using NewHotel.Pos.Hub.Business.Validations;
using NewHotel.Pos.Hub.Model.Session;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Tickets
{
	/// <summary>
	/// table="TNHT_VEND"
	/// Ticket
	/// </summary>
	[PersistentTable("TNHT_VEND", "VEND_PK")]
	public class Ticket : BasePersistent, IDataBasicOperations, ICanExchangeCurrency
	{
		private readonly bool _lazy = true;
		private readonly IPosSessionContext _sessionContext;
		private readonly IValidatorFactory _validatorFactory;

		#region Constructors + Initialize

		public static Ticket Create(IDatabaseManager manager, IPosSessionContext sessionContext, IValidatorFactory validatorFactory)
		{
			return new Ticket(manager, sessionContext, validatorFactory);
		}

		public static Ticket Create(IDatabaseManager manager, IPosSessionContext sessionContext, Guid id, bool lazyLoading, IValidatorFactory validatorFactory)
		{
			return new Ticket(manager, sessionContext, id, lazyLoading, validatorFactory);
		}

		private Ticket(IDatabaseManager manager, IPosSessionContext sessionContext, IValidatorFactory validatorFactory) : base(manager)
		{
			_productLineByTicket = GetList<ProductLine>(Manager, x => x.Ticket);
			_paymentLineByTicket = GetList<PaymentLine>(Manager, x => x.Ticket);
			_lookupTableByTicket = GetList<LookupTable>(Manager, x => x.Ticket);
			_clientsByPosition = GetList<ClientByPosition>(Manager, x => x.Ticket);
			_sessionContext = sessionContext;
			_validatorFactory = validatorFactory;
		}

		private Ticket(IDatabaseManager manager, IPosSessionContext sessionContext, Guid id, bool lazyLoading, IValidatorFactory validatorFactory)
			: this(manager, sessionContext, validatorFactory)
		{
			this.LoadObject(id);
			_lazy = lazyLoading;
		}

		public override void Initialize()
		{
			Paxs = 1;
			ReceiveCons = 0;
			Shift = 1;
			IsAnul = false;
			Total = 0;
			GeneralDiscount = 0;
			Recharge = 0;
			Process = false;
			DocumentType = POSDocumentType.Ticket;
		}

		#endregion
		#region Persistent Properties

		/// <summary>
		/// column="ESIM_PK"
		/// Tax schema
		/// </summary>
		private Guid? _taxSchemaId;
		[PersistentColumn("ESIM_PK")]
		public Guid? TaxSchemaId
		{
			get => _taxSchemaId;
			set
			{
				if (value != _taxSchemaId)
				{
					_taxSchemaId = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="LITR_PK"
		/// Reservacion de mesa asociada
		/// </summary>
		private Guid? _reservationTableId;
		[PersistentColumn("LITR_PK")]
		public Guid? ReservationTableId
		{
			get => _reservationTableId;
			set
			{
				if (value != _reservationTableId)
				{
					_reservationTableId = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="HOTE_PK"
		/// Hotel
		/// </summary>
		private Guid _hotel;
		[PersistentColumn("HOTE_PK")]
		public Guid Hotel
		{
			get => _hotel;
			set
			{
				if (value != _hotel)
				{
					_hotel = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="IPOS_ORIG"
		/// Punto de venta que lo creo
		/// </summary>
		private Guid _stand;
		[PersistentColumn("IPOS_PK")]
		public Guid Stand
		{
			get => _stand;
			set
			{
				if (value != _stand)
				{
					_stand = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="CAJA_ORIG"
		/// Caja donde se abrió el ticket
		/// </summary>
		private Guid _caja;
		[PersistentColumn("CAJA_PK")]
		public Guid Caja
		{
			get => _caja;
			set
			{
				if (value != _caja)
				{
					_caja = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_MESA"
		/// Mesa del ticket
		/// </summary>
		private Guid? _mesa;
		[PersistentColumn("VEND_MESA")]
		public Guid? Mesa
		{
			get => _mesa;
			set
			{
				if (value != _mesa)
				{
					_mesa = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="UNMO_PK"
		/// Moneda con la que se cobro el ticket
		/// </summary>
		private string _currency;
		[PersistentColumn("UNMO_PK")]
		public string Currency
		{
			get => _currency;
			set
			{
				if (value != _currency)
				{
					_currency = value;
					IsDirty = true;
				}
			}
		}

		private string _baseCurrency;
		/// <summary>
		/// column="CAMB_UNMO"
		/// Moneda a cambiar
		/// </summary>
		[PersistentColumn("CAMB_UNMO")]
		public string BaseCurrency
		{
			get => _baseCurrency;
			set
			{
				if (value != _baseCurrency)
				{
					_baseCurrency = value;
					IsDirty = true;
				}
			}
		}

		private decimal _exchangeFactor = 1;
		/// <summary>
		/// column="CAMB_VALO"
		/// Factor de cambio
		/// </summary>
		[PersistentColumn("CAMB_VALO")]
		public decimal ExchangeFactor
		{
			get => _exchangeFactor;
			set
			{
				if (value != _exchangeFactor)
				{
					_exchangeFactor = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="CAJA_PAVE"
		/// Caja donde se cobro el ticket
		/// </summary>
		private Guid? _cajaPave;
		[PersistentColumn("CAJA_PAVE")]
		public Guid? CajaPave
		{
			get => _cajaPave;
			set
			{
				if (value != _cajaPave)
				{
					_cajaPave = value;
					IsDirty = true;
				}
			}
		}

		private string _serie;
		/// <summary>
		/// column="VEND_SERI"
		/// Serie o prefijo del ticket
		/// Un cheque transferido viajaria con la serie con la que fue creado
		/// </summary>
		[PersistentColumn("VEND_SERI")]
		public string Serie
		{
			get => _serie;
			set
			{
				if (value != _serie)
				{
					_serie = value;
					IsDirty = true;
				}
			}
		}

		private long? _number;
		/// <summary>
		/// column="VEND_CODI"
		/// Número dentro de la serie del ticket
		/// </summary>
		[PersistentColumn("VEND_CODI")]
		public long? Number
		{
			get => _number;
			set
			{
				if (value != _number)
				{
					_number = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_COAP"
		/// Numero de serie de apertura del ticket
		/// </summary>
		private long _openingNumber;
		[PersistentColumn("VEND_COAP")]
		public long OpeningNumber
		{
			get => _openingNumber;
			set
			{
				if (value != _openingNumber)
				{
					_openingNumber = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_DATI"
		/// Fecha de apertura del cheque
		/// </summary>
		private DateTime _openingDate;
		[PersistentColumn("VEND_DATI")]
		public DateTime OpeningDate
		{
			get => _openingDate;
			set
			{
				if (value != _openingDate)
				{
					_openingDate = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_HORI"
		/// Hora de apertura del cheque
		/// </summary>
		private DateTime _openingTime;
		[PersistentColumn("VEND_HORI")]
		public DateTime OpeningTime
		{
			get => _openingTime;
			set
			{
				if (value != _openingTime)
				{
					_openingTime = value;
					IsDirty = true;
				}
			}
		}

		private DateTime? _closedDate;
		/// <summary>
		/// column="VEND_DATF"
		/// Fecha de cierre de ticket
		/// </summary>
		[PersistentColumn("VEND_DATF")]
		public DateTime? ClosedDate
		{
			get => _closedDate;
			set
			{
				if (value != _closedDate)
				{
					_closedDate = value;
					IsDirty = true;
				}
			}
		}

		private DateTime? _closedTime;
		/// <summary>
		/// column="VEND_HORF"
		/// Hora del cierre del ticket
		/// </summary>
		[PersistentColumn("VEND_HORF")]
		public DateTime? ClosedTime
		{
			get => _closedTime;
			set
			{
				if (value != _closedTime)
				{
					_closedTime = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_NPAX"
		/// Numero de Pax o personas que consumieron en este ticket
		/// </summary>
		private short _paxs;
		[PersistentColumn("VEND_NPAX")]
		public short Paxs
		{
			get => _paxs;
			set
			{
				if (value != _paxs)
				{
					_paxs = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_COMP"
		/// Consecutivos de comprobantes emitidos para este ticket
		/// </summary>
		private int _receiveCons;
		[PersistentColumn("VEND_COMP")]
		public int ReceiveCons
		{
			get => _receiveCons;
			set
			{
				if (value != _receiveCons)
				{
					_receiveCons = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="UTIL_PK"
		/// Si se trabaja con tickets por cajero, el cajero propietario del ticket en este momento.
		/// Cuando se crea el ticket es el cajero que lo crea, si se transfiere, el cajero que lo acepta
		/// </summary>
		private Guid _userId;
		[PersistentColumn("UTIL_PK")]
		public Guid UserId
		{
			get => _userId;
			set
			{
				if (value != _userId)
				{
					_userId = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="COIN_PK"
		/// Concepto de consumo interno
		/// </summary>
		private Guid? _internalUse;
		[PersistentColumn("COIN_PK")]
		public Guid? InternalUse
		{
			get => _internalUse;
			set
			{
				if (value != _internalUse)
				{
					_internalUse = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_ANUL"
		/// flag: ticket anulado (1.si, 0.no)
		/// </summary>
		private bool _isAnul;
		[PersistentColumn("VEND_ANUL")]
		public bool IsAnul
		{
			get => _isAnul;
			set
			{
				if (value != _isAnul)
				{
					_isAnul = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="MTCO_PK"
		/// Clasificador Motivos de Anulacion y Correccion
		/// </summary>
		private Guid? _cancellationReasonId;
		[PersistentColumn("MTCO_PK")]
		public Guid? CancellationReasonId
		{
			get => _cancellationReasonId;
			set
			{
				if (value != _cancellationReasonId)
				{
					_cancellationReasonId = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="ANUL_UTIL"
		/// Usuario que anulo, ref. tnht_util (util_pk)
		/// </summary>
		private Guid? _userCancellationId;
		[PersistentColumn("ANUL_UTIL")]
		public Guid? UserCancellationId
		{
			get => _userCancellationId;
			set
			{
				if (value != _userCancellationId)
				{
					_userCancellationId = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="ANUL_DAAN"
		/// Fecha trabajo anulación
		/// </summary>
		private DateTime? _cancellationDate;
		[PersistentColumn("ANUL_DAAN")]
		public DateTime? CancellationDate
		{
			get => _cancellationDate;
			set
			{
				if (value != _cancellationDate)
				{
					_cancellationDate = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="ANUL_DARE"
		/// Fecha registro anulación
		/// </summary>
		private DateTime? _cancellationSystemDate;
		[PersistentColumn("ANUL_DARE")]
		public DateTime? CancellationSystemDate
		{
			get => _cancellationSystemDate;
			set
			{
				if (value != _cancellationSystemDate)
				{
					_cancellationSystemDate = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="ANUL_OBSE"
		/// Cancellation comment
		/// </summary>
		private string _cancellationComments;
		[PersistentColumn("ANUL_OBSE", Nullable = true)]
		public string CancellationComments
		{
			get => _cancellationComments;
			set
			{
				if (value != _cancellationComments)
				{
					_cancellationComments = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="CCCO_PK"
		/// Cuenta corriente
		/// </summary>
		private Guid? _account;
		[PersistentColumn("CCCO_PK")]
		public Guid? Account
		{
			get => _account;
			set
			{
				if (value != _account)
				{
					_account = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="CCCO_DESC"
		/// Descripcion de la cuenta corriente
		/// </summary>
		private string _accountDescription;
		[PersistentColumn("CCCO_DESC")]
		public string AccountDescription
		{
			get => _accountDescription;
			set
			{
				if (value != _accountDescription)
				{
					_accountDescription = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="CCCO_TIPO"
		/// Tipo de la cuenta corriente
		/// </summary>
		private CurrentAccountType? _accountType;
		[PersistentColumn("CCCO_TIPO")]
		public CurrentAccountType? AccountType
		{
			get => _accountType;
			set
			{
				if (value != _accountType)
				{
					_accountType = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="CCCO_VALO"
		/// Balance de la cuenta corriente
		/// </summary>
		private decimal? _accountBalance;
		[PersistentColumn("CCCO_VALO")]
		public decimal? AccountBalance
		{
			get => _accountBalance;
			set
			{
				if (value == _accountBalance) return;
				_accountBalance = value;
				IsDirty = true;
			}
		}

		/// <summary>
		/// column="CCCO_CNUM"
		/// Card number of the current account
		/// </summary>
		private string _cardNumber;
		[PersistentColumn("CCCO_CNUM",  Nullable = true)]
		public string CardNumber
        {
            get => _cardNumber;
            set
            {
                if(value == _cardNumber)
                    return;
				_cardNumber = value;
                IsDirty = true;
            }
        }

		/// <summary>
		/// column="TURN_CODI"
		/// Turno en el que se abrio el ticket
		/// </summary>
		private long _shift;
		[PersistentColumn("TURN_CODI")]
		public long Shift
		{
			get => _shift;
			set
			{
				if (value != _shift)
				{
					_shift = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_DESC"
		/// Valor del descuento general que se le aplica al ticket
		/// </summary>
		private decimal _generalDiscount;
		[PersistentColumn("VEND_DESC")]
		public decimal GeneralDiscount
		{
			get => _generalDiscount;
			set
			{
				if (value != _generalDiscount)
				{
					_generalDiscount = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_RECA"
		/// Valor del recargo que se le aplica al ticket
		/// </summary>
		private decimal _recharge;
		[PersistentColumn("VEND_RECA")]
		public decimal Recharge
		{
			get => _recharge;
			set
			{
				if (value != _recharge)
				{
					_recharge = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_OBSE"
		/// Observaciones del ticket
		/// </summary>
		private string _observations;
		[PersistentColumn("VEND_OBSE")]
		public string Observations
		{
			get => _observations;
			set
			{
				if (value != _observations)
				{
					_observations = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="DUTY_PK"
		/// Servicio asociado a este ticket
		/// </summary>
		private Guid? _duty;
		[PersistentColumn("DUTY_PK", Nullable = true)]
		public Guid? Duty
		{
			get => _duty;
			set
			{
				if (value != _duty)
				{
					_duty = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="TPRG_PK"
		/// Tarifa precios aplicada, ref. tnht_tprg (tprg_pk)
		/// </summary>
		private Guid? _taxRate;
		[PersistentColumn("TPRG_PK")]
		public Guid? TaxRate
		{
			get => _taxRate;
			set
			{
				if (value != _taxRate)
				{
					_taxRate = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_IVIN"
		/// flag:impuesto incluido en los precios
		/// </summary>
		private bool _taxIncluded;
		[PersistentColumn("VEND_IVIN")]
		public bool TaxIncluded
		{
			get => _taxIncluded;
			set
			{
				if (value != _taxIncluded)
				{
					_taxIncluded = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_TOTA"
		/// Total del Ticket
		/// </summary>
		private decimal _total;
		[PersistentColumn("VEND_TOTA")]
		public decimal Total
		{
			get => _total;
			set
			{
				if (value != _total)
				{
					_total = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_SIGN"
		/// Firma digital del ticket
		/// </summary>
		private string _signature;
		[PersistentColumn("VEND_SIGN")]
		public string Signature
		{
			get => _signature;
			set
			{
				if (value != _signature)
				{
					_signature = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_QRCO"
		/// Código QR del ticket
		/// </summary>
		private Blob? _qrCode;
		[PersistentColumn("VEND_QRCO")]
		public Blob? QrCode
		{
			get => _qrCode;
			set
			{
				if (value != _qrCode)
				{
					_qrCode = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_QRDT"
		/// Ticket QR Code Data
		/// </summary>
		private string _qrCodeData;
		[PersistentColumn("VEND_QRDT")]
		public string QrCodeData
		{
			get => _qrCodeData;
			set
			{
				if (value != _qrCodeData)
				{
					_qrCodeData = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_COVA"
		/// Código de validación del ticket: SAFT-PT ATCUD
		/// </summary>
		private string _validationCode;
		[PersistentColumn("VEND_COVA")]
		public string ValidationCode
		{
			get => _validationCode;
			set
			{
				if (value != _validationCode)
				{
					_validationCode = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_PRNT"
		/// Número de impresiones del ticket
		/// </summary>
		private short _prints;
		[PersistentColumn("VEND_PRNT")]
		public short Prints
		{
			get => _prints;
			set
			{
				if (value != _prints)
				{
					_prints = value;
					IsDirty = true;
				}
			}
		}

		private decimal? _tipPercent;
		/// <summary>
		/// column="VEND_TIPE"
		/// Tip percent
		/// </summary>
		[PersistentColumn("VEND_TIPE")]
		public decimal? TipPercent
		{
			get => _tipPercent;
			set
			{
				if (value != _tipPercent)
				{
					_tipPercent = value;
					IsDirty = true;
				}
			}
		}

		private decimal? _tipValue;
		/// <summary>
		/// column="VEND_TIVA"
		/// Tip value
		/// </summary>
		[PersistentColumn("VEND_TIVA")]
		public decimal? TipValue
		{
			get => _tipValue;
			set
			{
				if (value != _tipValue)
				{
					_tipValue = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_PROC"
		/// 1. si el ticket ya  fue procesado y subido a la cuenta del stand
		/// </summary>
		private bool _process;
		[PersistentColumn("VEND_PROC")]
		public bool Process
		{
			get => _process;
			set
			{
				if (value != _process)
				{
					_process = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="UTIL_OPEN"
		/// User that open ticket for edition
		/// </summary>
		private Guid? _openUserId;
		[PersistentColumn("UTIL_OPEN")]
		public Guid? OpenUserId
		{
			get => _openUserId;
			set
			{
				if (value != _openUserId)
				{
					_openUserId = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// User that open ticket for edition
		/// </summary>
		[PersistentRefColumn("TNHT_UTIL.UTIL_DESC", "UTIL_PK", "UTIL_OPEN")]
		public string OpenUserDescription { get; set; }

		/// <summary>
		/// column="VEND_OPEN"
		/// User that open ticket for edition
		/// </summary>c
		private bool _opened;
		[PersistentColumn("VEND_OPEN")]
		public bool Opened
		{
			get => _opened;
			set
			{
				if (value != _opened)
				{
					_opened = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="UTIL_CLOS"
		/// User that close ticket
		/// </summary>
		private Guid? _closeUserId;
		[PersistentColumn("UTIL_CLOS")]
		public Guid? CloseUserId
		{
			get => _closeUserId;
			set
			{
				if (value != _closeUserId)
				{
					_closeUserId = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// User that close ticket
		/// </summary>
		[PersistentRefColumn("TNHT_UTIL.UTIL_DESC", "UTIL_PK", "UTIL_CLOS")]
		public string CloseUserDescription { get; set; }

		/// <summary>
		/// column="CLIE_PK"
		/// Entity/Client holder of the ticket / invoice
		/// </summary>c
		private Guid? _baseEntityId;
		[PersistentColumn("CLIE_PK")]
		public Guid? BaseEntityId
		{
			get => _baseEntityId;
			set
			{
				if (value != _baseEntityId)
				{
					_baseEntityId = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FISC_PEND"
		/// Documento Fiscal Pendiente (no permite cancelar)
		/// </summary>
		private bool _fiscalDocumentPending;
		[PersistentColumn("FISC_PEND")]
		public bool FiscalDocumentPending
		{
			get => _fiscalDocumentPending;
			set
			{
				if (value != _fiscalDocumentPending)
				{
					_fiscalDocumentPending = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_IFFA"
		/// Numero de serie de la impresora fiscal que imprimio la factura';
		/// </summary>
		private string _fpSerialInvoice;
		[PersistentColumn("VEND_IFFA")]
		public string FpSerialInvoice
		{
			get => _fpSerialInvoice;
			set
			{
				if (value != _fpSerialInvoice)
				{
					_fpSerialInvoice = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_NFFA"
		/// Numero fiscal asignado por la impresora a la factura';
		/// </summary>
		private long? _fpInvoiceNumber;
		[PersistentColumn("VEND_NFFA")]
		public long? FpInvoiceNumber
		{
			get => _fpInvoiceNumber;
			set
			{
				if (value != _fpInvoiceNumber)
				{
					_fpInvoiceNumber = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_IFND"
		/// Numero de serie de la impresora fiscal que imprimio la nota de credito';
		/// </summary>
		private string _fpSerialCreditNote;
		[PersistentColumn("VEND_IFNC")]
		public string FpSerialCreditNote
		{
			get => _fpSerialCreditNote;
			set
			{
				if (value != _fpSerialCreditNote)
				{
					_fpSerialCreditNote = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_NFNC"
		/// Numero fiscal asignado por la impresora a la nota de credito
		/// </summary>c
		private long? _fpCreditNoteNumber;
		[PersistentColumn("VEND_NFNC")]
		public long? FpCreditNoteNumber
		{
			get => _fpCreditNoteNumber;
			set
			{
				if (value != _fpCreditNoteNumber)
				{
					_fpCreditNoteNumber = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_MEDI"
		/// flag: Ticket creado por el menu digital?
		/// </summary>c
		private bool _digitalMenu;
		[PersistentColumn("VEND_MEDI")]
		public bool DigitalMenu
		{
			get => _digitalMenu;
			set
			{
				if (value != _digitalMenu)
				{
					_digitalMenu = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_ESTR"
		/// Estación de trabajo
		/// </summary>
		private string _workStation;
		[PersistentColumn("VEND_ESTR", StringSize = 2000)]
		public string WorkStation
		{
			get => _workStation;
			set
			{
				if (value != _workStation)
				{
					_workStation = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_TIDO"
		/// Tipo de Documento
		/// </summary>
		private POSDocumentType _documentType;
		[PersistentColumn("FACT_TIDO")]
		public POSDocumentType DocumentType
		{
			get => _documentType;
			set
			{
				if (value != _documentType)
				{
					_documentType = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_TIDF"
		/// Tipo de Documento Fiscal
		/// </summary>c
		private long? _fiscalIdenType;
		[PersistentColumn("FACT_TIDF")]
		public long? FiscalIdenType
		{
			get => _fiscalIdenType;
			set
			{
				if (value != _fiscalIdenType)
				{
					_fiscalIdenType = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_CCCO"
		/// Cuenta corriente facturada
		/// </summary>
		private Guid? _invoiceAccount;
		[PersistentColumn("FACT_CCCO")]
		public Guid? InvoiceAccount
		{
			get => _invoiceAccount;
			set
			{
				if (value != _invoiceAccount)
				{
					_invoiceAccount = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_NACI"
		/// Código del País en el documento fiscal
		/// </summary>
		private string _fiscalDocNacionality;
		[PersistentColumn("FACT_NACI")]
		public string FiscalDocNacionality
		{
			get => _fiscalDocNacionality;
			set
			{
				if (value != _fiscalDocNacionality)
				{
					_fiscalDocNacionality = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_DAEM"
		/// Fecha emision documento fiscal 
		/// </summary>
		private DateTime? _fiscalDocEmiDate;
		[PersistentColumn("FACT_DAEM")]
		public DateTime? FiscalDocEmiDate
		{
			get => _fiscalDocEmiDate;
			set
			{
				if (value != _fiscalDocEmiDate)
				{
					_fiscalDocEmiDate = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_DARE"
		/// Fecha registro documento fiscal 
		/// </summary>
		private DateTime? _fiscalDocRegDate;
		[PersistentColumn("FACT_DARE")]
		public DateTime? FiscalDocRegDate
		{
			get => _fiscalDocRegDate;
			set
			{
				if (value != _fiscalDocRegDate)
				{
					_fiscalDocRegDate = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_GUEST"
		/// Documento fiscal a nombre de...
		/// </summary>
		private string _fiscalDocTo;
		[PersistentColumn("FACT_GUEST")]
		public string FiscalDocTo
		{
			get => _fiscalDocTo;
			set
			{
				if (value != _fiscalDocTo)
				{
					_fiscalDocTo = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_ADDR"
		/// Direccion en el documento fiscal
		/// </summary>
		private string _fiscalDocAddress;
		[PersistentColumn("FACT_ADDR")]
		public string FiscalDocAddress
		{
			get => _fiscalDocAddress;
			set
			{
				if (value != _fiscalDocAddress)
				{
					_fiscalDocAddress = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_MAIL"
		/// Email asociado al documento fiscal
		/// </summary>c
		private string _fiscalDocEmail;
		[PersistentColumn("FACT_MAIL")]
		public string FiscalDocEmail
		{
			get => _fiscalDocEmail;
			set
			{
				if (value != _fiscalDocEmail)
				{
					_fiscalDocEmail = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_NUCO"
		/// No. Fiscal en el documento fiscal
		/// </summary>
		private string _fiscalDocFiscalNumber;
		[PersistentColumn("FACT_NUCO")]
		public string FiscalDocFiscalNumber
		{
			get => _fiscalDocFiscalNumber;
			set
			{
				if (value != _fiscalDocFiscalNumber)
				{
					_fiscalDocFiscalNumber = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_TOTA"
		/// Total Documento fiscal
		/// </summary>
		private decimal? _fiscalDocTotal;
		[PersistentColumn("FACT_TOTA")]
		public decimal? FiscalDocTotal
		{
			get => _fiscalDocTotal;
			set
			{
				if (value != _fiscalDocTotal)
				{
					_fiscalDocTotal = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_SERI"
		/// Serie Fiscal de la factura o boleta
		/// </summary>
		private string _fiscalDocSerie;
		[PersistentColumn("FACT_SERI")]
		public string FiscalDocSerie
		{
			get => _fiscalDocSerie;
			set
			{
				if (value != _fiscalDocSerie)
				{
					_fiscalDocSerie = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_CODI"
		/// No. Consecutivo de la factura o boleta
		/// </summary>
		private long? _fiscalDocNumber;
		[PersistentColumn("FACT_CODI")]
		public long? FiscalDocNumber
		{
			get => _fiscalDocNumber;
			set
			{
				if (value != _fiscalDocNumber)
				{
					_fiscalDocNumber = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_SIGN"
		/// Firma digital de la factura o boleta
		/// </summary>
		private string _fiscalDocSignature;
		[PersistentColumn("FACT_SIGN")]
		public string FiscalDocSignature
		{
			get => _fiscalDocSignature;
			set
			{
				if (value != _fiscalDocSignature)
				{
					_fiscalDocSignature = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_QRCO"
		/// Código QR de la factura o boleta
		/// </summary>
		private Blob? _fiscalDocQrCode;
		[PersistentColumn("FACT_QRCO")]
		public Blob? FiscalDocQrCode
		{
			get => _fiscalDocQrCode;
			set
			{
				if (value != _fiscalDocQrCode)
				{
					_fiscalDocQrCode = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_QRDT"
		/// Invoice/Ballot QR Code Data
		/// </summary>
		private string _fiscalDocQrCodeData;
		[PersistentColumn("FACT_QRDT")]
		public string FiscalDocQrCodeData
		{
			get => _fiscalDocQrCodeData;
			set
			{
				if (value != _fiscalDocQrCodeData)
				{
					_fiscalDocQrCodeData = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_COVA"
		/// Código de validación de la factura o boleta
		/// SAFT-PT ATCUD
		/// </summary>
		private string _fiscalDocValidationCode;
		[PersistentColumn("FACT_COVA")]
		public string FiscalDocValidationCode
		{
			get => _fiscalDocValidationCode;
			set
			{
				if (value != _fiscalDocValidationCode)
				{
					_fiscalDocValidationCode = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_PRNT"
		/// Número de impresiones de la factura/boleta
		/// </summary>
		private short _fiscalDocPrints;
		[PersistentColumn("FACT_PRNT")]
		public short FiscalDocPrints
		{
			get => _fiscalDocPrints;
			set
			{
				if (value != _fiscalDocPrints)
				{
					_fiscalDocPrints = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_PHNE"
		/// Número de teléfono asociado a la factura
		/// </summary>
		private string _fiscalPhoneNumber;
		[PersistentColumn("FACT_PHNE")]
		public string FiscalPhoneNumber
		{
			get => _fiscalPhoneNumber;
			set
			{
				if (value != _fiscalPhoneNumber)
				{
					_fiscalPhoneNumber = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_CTCD"
		/// Código de la ciudad asociada a la factura
		/// </summary>
		private string _fiscalCityCode;
		[PersistentColumn("FACT_CTCD")]
		public string FiscalCityCode
		{
			get => _fiscalCityCode;
			set
			{
				if (value != _fiscalCityCode)
				{
					_fiscalCityCode = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_CTNM"
		/// Nombre de la ciudad asociada a la factura
		/// </summary>
		private string _fiscalCityName;
		[PersistentColumn("FACT_CTNM")]
		public string FiscalCityName
		{
			get => _fiscalCityName;
			set
			{
				if (value != _fiscalCityName)
				{
					_fiscalCityName = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_RTCD"
		/// Código de tipo de régimen asociado a la factura
		/// </summary>
		private string _fiscalRegimeTypeCode;
		[PersistentColumn("FACT_RTCD")]
		public string FiscalRegimeTypeCode
		{
			get => _fiscalRegimeTypeCode;
			set
			{
				if (value != _fiscalRegimeTypeCode)
				{
					_fiscalRegimeTypeCode = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_RPCD"
		/// Código de responsabilidad asociado a la factura
		/// </summary>
		private string _fiscalResponsabilityCode;
		[PersistentColumn("FACT_RPCD")]
		public string FiscalResponsabilityCode
		{
			get => _fiscalResponsabilityCode;
			set
			{
				if (value != _fiscalResponsabilityCode)
				{
					_fiscalResponsabilityCode = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="FACT_CUFE"
		/// Cufe devuelto por BTW asociado a la factura
		/// </summary>
		private string _cufe;
		[PersistentColumn("FACT_CUFE")]
		public string Cufe
		{
			get => _cufe;
			set
			{
				if (value != _cufe)
				{
					_cufe = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="NCRE_SERI"
		/// Serie Fiscal de la nota de crédito
		/// </summary>
		private string _creditNoteSerie;
		[PersistentColumn("NCRE_SERI")]
		public string CreditNoteSerie
		{
			get => _creditNoteSerie;
			set
			{
				if (value != _creditNoteSerie)
				{
					_creditNoteSerie = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="NCRE_CODI"
		/// No. Consecutivo de la nota de crédito
		/// </summary>
		private long? _creditNoteNumber;
		[PersistentColumn("NCRE_CODI")]
		public long? CreditNoteNumber
		{
			get => _creditNoteNumber;
			set
			{
				if (value != _creditNoteNumber)
				{
					_creditNoteNumber = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="NCRE_DARE"
		/// Fecha de registro de la nota de crédito
		/// </summary>
		private DateTime? _creditNoteSystemDate;
		[PersistentColumn("NCRE_DARE")]
		public DateTime? CreditNoteSystemDate
		{
			get => _creditNoteSystemDate;
			set
			{
				if (value != _creditNoteSystemDate)
				{
					_creditNoteSystemDate = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="NCRE_DATR"
		/// Fecha de trabajo de la nota de crédito
		/// </summary>
		private DateTime? _creditNoteWorkDate;
		[PersistentColumn("NCRE_DATR")]
		public DateTime? CreditNoteWorkDate
		{
			get => _creditNoteWorkDate;
			set
			{
				if (value != _creditNoteWorkDate)
				{
					_creditNoteWorkDate = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="NCRE_UTIL"
		/// Usuario de la nota de crédito
		/// </summary>
		private Guid? _creditNoteUserId;
		[PersistentColumn("NCRE_UTIL")]
		public Guid? CreditNoteUserId
		{
			get => _creditNoteUserId;
			set
			{
				if (value != _creditNoteUserId)
				{
					_creditNoteUserId = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="NCRE_SIGN"
		/// Firma digital de la nota de crédito
		/// </summary>
		private string _creditNoteSignature;
		[PersistentColumn("NCRE_SIGN")]
		public string CreditNoteSignature
		{
			get => _creditNoteSignature;
			set
			{
				if (value != _creditNoteSignature)
				{
					_creditNoteSignature = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="NCRE_QRCO"
		/// Código QR de la nota de crédito
		/// </summary>
		private Blob? _creditNoteQrCode;
		[PersistentColumn("NCRE_QRCO")]
		public Blob? CreditNoteQrCode
		{
			get => _creditNoteQrCode;
			set
			{
				if (value != _creditNoteQrCode)
				{
					_creditNoteQrCode = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="NCRE_QRDT"
		/// Credit Note QR Code Data
		/// </summary>
		private string _creditNoteQrCodeData;
		[PersistentColumn("NCRE_QRDT")]
		public string CreditNoteQrCodeData
		{
			get => _creditNoteQrCodeData;
			set
			{
				if (value != _creditNoteQrCodeData)
				{
					_creditNoteQrCodeData = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="NCRE_COVA"
		/// Código de validación de la nota de crédito
		/// SAFT-PT ATCUD
		/// </summary>
		private string _creditNoteValidationCode;
		[PersistentColumn("NCRE_COVA")]
		public string CreditNoteValidationCode
		{
			get => _creditNoteValidationCode;
			set
			{
				if (value != _creditNoteValidationCode)
				{
					_creditNoteValidationCode = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="NCRE_PRNT"
		/// Número de impresiones de la nota de crédito
		/// </summary>
		private short _creditNotePrints;
		[PersistentColumn("NCRE_PRNT")]
		public short CreditNotePrints
		{
			get => _creditNotePrints;
			set
			{
				if (value != _creditNotePrints)
				{
					_creditNotePrints = value;
					IsDirty = true;
				}
			}
		}

		[PersistentRefColumn("TNHT_UTIL.UTIL_DESC", "UTIL_PK", "UTIL_PK")]
		public string UserDescription { get; set; }

		[PersistentRefColumn("TNHT_UTIL.UTIL_DESC", "UTIL_PK", "NCRE_UTIL")]
		public string CreditNoteUserDescription { get; set; }

		/// <summary>
		/// column="VEND_ROOM"
		/// Número de habitación
		/// </summary>
		private string _room;
		[PersistentColumn("VEND_ROOM")]
		public string Room
		{
			get => _room;
			set
			{
				if (value != _room)
				{
					_room = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_PENS"
		/// Description of the pension
		/// </summary>
		private string _pension;
		[PersistentColumn("VEND_PENS")]
		public string Pension
		{
			get => _pension;
			set
			{
				if (value != _pension)
				{
					_pension = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_NAME"
		/// Nombre de la persona
		/// </summary>
		private string _name;
		[PersistentColumn("VEND_NAME")]
		public string Name
		{
			get => _name;
			set
			{
				if (value != _name)
				{
					_name = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_RSNU"
		/// Número de servicio habitación
		/// </summary>
		private string _roomServiceNumber;
		[PersistentColumn("VEND_RSNU")]
		public string RoomServiceNumber
		{
			get => _roomServiceNumber;
			set
			{
				if (value != _roomServiceNumber)
				{
					_roomServiceNumber = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_DISI"
		/// Firma digital del cliente
		/// </summary>
		private Blob? _digitalSignature;
		[PersistentColumn("VEND_DISI")]
		public Blob? DigitalSignature
		{
			get => _digitalSignature;
			set
			{
				if (value != _digitalSignature)
				{
					_digitalSignature = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_SYNC"
		/// Timestamp upload request
		/// Set to current timestamp to upload
		/// </summary>
		private DateTime? _sync;
		[PersistentTimestampColumn("VEND_SYNC")]
		public DateTime? Sync
		{
			get => _sync;
			set
			{
				if (value != _sync)
				{
					_sync = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// column="VEND_LMOD"
		/// </summary>
		[PersistentLastModifiedColumn("VEND_LMOD")]
		public DateTime LastModified { get; set; }

		/// <summary>
		/// column="VEND_APRC"
		/// Allow POS room credit
		/// </summary>
		private bool _allowRoomCredit = true;
		[PersistentColumn("VEND_APRC")]
		public bool AllowRoomCredit
		{
			get => _allowRoomCredit;
			set
			{
				if (value == _allowRoomCredit) return;
				_allowRoomCredit = value;
				IsDirty = true;
			}
		}
		
		/// <summary>
		/// column="SPA_SERV"
		/// Si está relacionado con algún servicio del Spa
		/// </summary>
		private Guid? _spaServiceId;
		[PersistentColumn("SPA_SERV")]
		public Guid? SpaServiceId
		{
			get => _spaServiceId;
			set
			{
				if (value == _spaServiceId) return;
				_spaServiceId = value;
				IsDirty = true;
			}
		}
		
		/// <summary>
		/// column="SPA_SIGN"
		/// If was created through rfid code
		/// </summary>
		private bool _isSpaServiceDigitallySigned;
		[PersistentColumn("SPA_SIGN")]
		public bool IsSpaServiceDigitallySigned
		{
			get => _isSpaServiceDigitallySigned;
			set
			{
				if (value == _isSpaServiceDigitallySigned) return;
				_isSpaServiceDigitallySigned = value;
				IsDirty = true;
			}
		}

		#endregion
		#region Persistent Lists

		private IPersistentList<ProductLine> _productLineByTicket;
		[ReflectionExclude]
		public IPersistentList<ProductLine> ProductLineByTicket
		{
			get
			{
				if (_lazy && !_productLineByTicket.Populated)
					_productLineByTicket.Execute(Id);

				return _productLineByTicket;
			}
		}

		private IPersistentList<PaymentLine> _paymentLineByTicket;
		[ReflectionExclude]
		public IPersistentList<PaymentLine> PaymentLineByTicket
		{
			get
			{
				if (_lazy && !_paymentLineByTicket.Populated)
					_paymentLineByTicket.Execute(Id);

				return _paymentLineByTicket;
			}
		}

		private IPersistentList<LookupTable> _lookupTableByTicket;

		[ReflectionExclude]
		public IPersistentList<LookupTable> LookupTableByTicket
		{
			get
			{
				if (_lazy && !_lookupTableByTicket.Populated)
					_lookupTableByTicket.Execute(Id);

				return _lookupTableByTicket;
			}
		}

		private IPersistentList<ClientByPosition> _clientsByPosition;

		[ReflectionExclude]
		public IPersistentList<ClientByPosition> ClientsByPosition
		{
			get
			{
				if (_lazy && !_clientsByPosition.Populated)
					_clientsByPosition.Execute(Id);

				return _clientsByPosition;
			}
		}

		#endregion
		#region IDataBasicOperations Operations

		public bool SaveObject(bool sync)
		{
			// Tiene datos de nota de crédito
			if (!string.IsNullOrEmpty(CreditNoteSerie) && CreditNoteNumber.HasValue)
			{
				if (IsAnul)
					throw new ApplicationException("NotCreditNoteAnulTicket".Translate());
				if (string.IsNullOrEmpty(FiscalDocSerie) || !FiscalDocNumber.HasValue)
					throw new ApplicationException("NotCreditNoteNonInvoicedTicket".Translate());
			}

			// Tiene datos de factura/boleta
			if (!string.IsNullOrEmpty(FiscalDocSerie) && FiscalDocNumber.HasValue)
			{
				var stand = new Stand(Manager, Stand);
				var tipService = stand.TipService ?? Guid.Empty;

				// Tiene que tener al menos una linea activa y no ser propina
				if (!ProductLineByTicket.Any(pl => pl.IsActive && pl.Product != tipService))
					throw new ApplicationException("NotInvoiceEmptyTicket".Translate());
			}

			var isNew = LastModified == DateTime.MinValue;
			if (isNew)
			{
				OpeningDate = _sessionContext.WorkDate;
				OpeningTime = DateTime.Now;
				if (string.IsNullOrEmpty(WorkStation))
					WorkStation = Environment.MachineName;
			}

			if (OpeningDate.Equals(DateTime.MinValue))
			{
				OpeningDate = _sessionContext.WorkDate;
				if (OpeningDate.Equals(DateTime.MinValue))
				{
					OpeningDate = DateTime.Today;
				}
			}

			if (OpeningTime.Equals(DateTime.MinValue))
			{
				OpeningTime = DateTime.Now;
			}


			if (sync)
				Sync = DateTime.Now;
			// One of them is: if the ticket has room credit as a payment, then it has to be saved with attr process = 1
			// This helps to avoid duplicated movements in the pms at the day close action
			if (IsChargeAccount)
				Process = true;

			if (PersistObjectToDB())
			{
				if (_productLineByTicket.Populated)
					ProductLineByTicket.PersistOrDeleteObjects();
				if (_paymentLineByTicket.Populated)
					PaymentLineByTicket.PersistOrDeleteObjects();
				if (_lookupTableByTicket.Populated)
					_lookupTableByTicket.PersistOrDeleteObjects();
				if (_clientsByPosition.Populated)
					_clientsByPosition.PersistOrDeleteObjects();

				return true;
			}

			return false;
		}

		public bool SaveObject()
		{
			return SaveObject(false);
		}

		public bool DeleteObject()
		{
			return DeleteObjectFromDB();
		}

		public bool LoadObject(object id, bool throwNoDataFoundException)
		{
			return LoadObjectFromDB(id, throwNoDataFoundException);
		}

		public static void UpdateSync(IDatabaseManager manager, Guid ticketId)
		{
			using (var command = manager.CreateCommand("UpdateSync"))
			{
				command.CommandText = "update tnht_vend set vend_sync = :vend_sync where vend_pk = :vend_pk";
				manager.CreateParameter(command, "vend_pk", manager.ConvertValueType(ticketId));
				manager.CreateParameter(command, "vend_sync", manager.ConvertValueType(DateTime.Now));
				manager.ExecuteNonQuery(command);
			}
		}

		#endregion

		#region Non Persistent Properties

		public bool ApplyTipOverNetValue { get; set; }
		
		public decimal AdjustingToTotal { get; private set; }

		public bool IsChargeAccount => PaymentLineByTicket.Any(x => x.AccountId.HasValue && x.ReceivableType == ReceivableType.Credit)
									|| PaymentLineByTicket.Any(x => x.AccountId.HasValue && x.ReceivableType == ReceivableType.UseAccountDeposit);

		public IValidatorFactory ValidatorFactory => _validatorFactory;

		#endregion

		#region Methods

		
		public (decimal, decimal) ExtractTotals(decimal total)
		{
			decimal realTotal = total.CommercialRound();
			decimal adjustment = realTotal - total;

			return (total, adjustment);
		}

		public void SetTotalValue(decimal total)
		{
			(Total, AdjustingToTotal) = ExtractTotals(total);

			// Total = total.RoundToNext();
		}
		#endregion
	}

	public static class TicketValidationExt
	{
		private static string FixPrefix(string prefix) => string.IsNullOrEmpty(prefix) ? string.Empty : $"[{prefix}] ";
		
		public static T SaveTicket<T>(this T result, Ticket ticket, bool sync, IValidatorFactory validatorFactory, string prefix = default)
		where T : ValidationResult
		{
			if (result.HasErrors)
			{
				return result;
			}

			if (validatorFactory != null)
			{
				var rePrefix = FixPrefix(prefix);

				var ticketValidator = validatorFactory.GetValidator<Ticket>(ticket);
				var validationResult = ticketValidator.Validate(prefix);
				if (validationResult.HasErrors)
				{
					result.AddValidations(validationResult);
					return result;
				}
			}

            result.Validate(() => ticket.SaveObject(sync), 999, $"[{prefix}] Error saving ticket.");
			prefix ??= "Generic-SaveTicket";
			string description = $"{ticket.Serie} / {ticket.Number}";
			using var log = HubLogsFactory.DefaultFactory.NewBusinessLog("__" + prefix);
			if (result.HasErrors)
				log.Error(result.ToString());
			else
				log.Information($"{description} saved");

			return result;
		}

		//public static T AndValidateTicket<T>(this T result, Ticket ticket, string prefix)
		//	where T : ValidationResult
		//{
		//	var validation = ticket.Validate(prefix);
		//	if (validation.HasErrors)
		//	{
		//		result.AddValidations(validation);
		//	}

		//	return result;
		//}
	}
}