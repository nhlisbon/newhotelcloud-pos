﻿using System;
using NewHotel.Contracts;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Tickets
{
    /// <summary>
    /// table="TNHT_PAVE
    /// Id Linea de Pago
    /// </summary>
    [PersistentTable("TNHT_PAVE", "PAVE_PK")]
    public class PaymentLine : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public PaymentLine(IDatabaseManager manager)
            : base(manager)
        {
        }

        public PaymentLine(IDatabaseManager manager, object id)
            : this (manager)
        {
            this.LoadObject(id);
        }

        public override void Initialize()
        {
            ReceivableType = ReceivableType.Payment;
            PaymentAmount = 0;
        }

        #endregion
        #region Persistent Properties

        private Guid _ticket;
        /// <summary>
        /// column="VEND_PK"
        /// Ticket
        /// </summary>
        [PersistentColumn("VEND_PK")]
        public Guid Ticket
        {
            get { return _ticket; }
            set
            {
                if (value != _ticket)
                {
                    _ticket = value;
                    IsDirty = true;
                }
            }
        }

        private ReceivableType _receivableType;
        /// <summary>
        /// column="TIRE_PK"
        /// Tipo de Pago (Payment, Room Plan, Ticket Transfer, HouseUse, Deposit Use)
        /// </summary>
        [PersistentColumn("TIRE_PK")]
        public ReceivableType ReceivableType
        {
            get { return _receivableType; }
            set
            {
                if (value != _receivableType)
                {
                    _receivableType = value;
                    IsDirty = true;
                }
            }
        }

        private Guid? _paymentId;
        /// <summary>
        /// column="FORE_PK"
        /// Forma de pago
        /// </summary>
        [PersistentColumn("FORE_PK")]
        public Guid? PaymentId
        {
            get { return _paymentId; }
            set
            {
                if (value != _paymentId)
                {
                    _paymentId = value;
                    IsDirty = true;
                }
            }
        }

        private decimal _paymentAmount;
        /// <summary>
        /// column="PAVE_VALO"
        /// Valor base, importe en la forma de pago
        /// </summary>
        [PersistentColumn("PAVE_VALO")]
        public decimal PaymentAmount
        {
            get { return _paymentAmount; }
            set
            {
                if (value != _paymentAmount)
                {
                    _paymentAmount = value;
                    IsDirty = true;
                }
            }
        }

        private decimal _paymentReceived;
        /// <summary>
        /// column="PAVE_RECI"
        /// Valor recibido, importe en la forma de pago
        /// </summary>
        [PersistentColumn("PAVE_RECI")]
        public decimal PaymentReceived
        {
            get { return _paymentReceived; }
            set
            {
                if (value != _paymentReceived)
                {
                    _paymentReceived = value;
                    IsDirty = true;
                }
            }
        }

        private Guid? _creditCard;
        /// <summary>
        /// column="TACR_PK"
        /// Tarjeta de credito
        /// </summary>
        [PersistentColumn("TACR_PK")]
        public Guid? CreditCard
        {
            get { return _creditCard; }
            set
            {
                if (value != _creditCard)
                {
                    _creditCard = value;
                    IsDirty = true;
                }
            }
        }

        private Guid? _accountId;
        /// <summary>
        /// column="CCCO_PK"
        /// Cuenta Corriente
        /// </summary>
        [PersistentColumn("CCCO_PK")]
        public Guid? AccountId
        {
            get { return _accountId; }
            set
            {
                if (value != _accountId)
                {
                    _accountId = value;
                    IsDirty = true;
                }
            }
        }

        private Guid? _internalConsumptionId;
        /// <summary>
        /// column="COIN_PK"
        /// Consumo Interno
        /// </summary>
        [PersistentColumn("COIN_PK")]
        public Guid? InternalConsumptionId
        {
            get { return _internalConsumptionId; }
            set
            {
                if (value != _internalConsumptionId)
                {
                    _internalConsumptionId = value;
                    IsDirty = true;
                }
            }
        }

        private string _observations;
        /// <summary>
        /// column="PAVE_OBSV"
        /// Observaciones en la linea de pago
        /// </summary>
        [PersistentColumn("PAVE_OBSV")]
        public string Observations
        {
            get { return _observations; }
            set
            {
                if (value != _observations)
                {
                    _observations = value;
                    IsDirty = true;
                }
            }
        }

        private short _annulmentCode;
        /// <summary>
        /// column="PAVE_ANUL"
        /// Indica si el pago esta anulado o es transferido de otro punto de venta
        /// 0 Activo, 1 Anulado, 2 Transferido
        /// </summary>
        [PersistentColumn("PAVE_ANUL")]
        public short AnnulmentCode
        {
            get { return _annulmentCode; }
            set
            {
                if (value != _annulmentCode)
                {
                    _annulmentCode = value;
                    IsDirty = true;
                }
            }
        }

        private string _currencyId;
        /// <summary>
        /// column="UNMO_PK"
        /// Moneda de pago en caja
        /// </summary>
        [PersistentColumn("UNMO_PK")]
        public string CurrencyId
        {
            get => _currencyId;
            set
            {
                if (value != _currencyId)
                {
                    _currencyId = value;
                    IsDirty = true;
                }
            }
        }

        private decimal? _exchangeCurrency;
        /// <summary>
        /// column="CAMB_ONLINE"
        /// Cambio online de moneda base para moneda de pago en caja
        /// </summary>
        [PersistentColumn("CAMB_ONLINE")]
        public decimal? ExchangeCurrency
        {
            get => _exchangeCurrency;
            set
            {
                if (value != _exchangeCurrency)
                {
                    _exchangeCurrency = value;
                    IsDirty = true;
                }
            }
        }

        private bool? _multCurrency;
        /// <summary>
        /// column="CAMB_MULT"
        /// Operador asociado al calcular el cambio de moneda base para moneda
        /// de pago en caja (1.multiplicar, 0. dividir)
        /// </summary>
        [PersistentColumn("CAMB_MULT")]
        public bool? MultCurrency
        {
            get => _multCurrency;
            set
            {
                if (value != _multCurrency)
                {
                    _multCurrency = value;
                    IsDirty = true;
                }
            }
        }

        private Guid? _accountInstallationId;
        /// <summary>
        /// column="HOTE_PK"
        /// Hotel para el cargo en cuenta
        /// </summary>
        [PersistentColumn("HOTE_PK")]
        public Guid? AccountInstallationId
        {
            get { return _accountInstallationId; }
            set
            {
                if (value != _accountInstallationId)
                {
                    _accountInstallationId = value;
                    IsDirty = true;
                }
            }
        }

		private decimal _paymentAmountInBase;
		/// <summary>
		/// column="PAVE_VAME"
		/// Valor en moneda base
		/// </summary>
		[PersistentColumn("PAVE_VAME")]
		public decimal PaymentAmountInBase
		{
			get { return _paymentAmountInBase; }
			set
			{
                Set(ref _paymentAmountInBase, value, nameof(PaymentAmountInBase));
			}
		}

		#endregion
	}

    public static class PaymentLineExtensions
	{
		public static decimal GetChangeValue(this PaymentLine paymentLine)
            => paymentLine.PaymentReceived - paymentLine.PaymentAmount;
	}
}
