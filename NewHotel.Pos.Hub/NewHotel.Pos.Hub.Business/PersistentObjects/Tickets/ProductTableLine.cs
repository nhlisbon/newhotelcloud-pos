﻿using System;
using NewHotel.Contracts;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Tickets
{
    /// <summary>
    /// table="TNHT_TABL
    /// Productos por Linea de Table
    /// </summary>
    [PersistentTable("TNHT_TABL", "TABL_PK")]
    public class ProductTableLine : BasePersistentFullOperations
    {
        private readonly bool _lazy = true;

        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public ProductTableLine(IDatabaseManager manager)
            : base (manager)
        {
            _tableProductsByLinePreparation = GetList<ProductsTableLinePreparation>(Manager, x => x.TableLineId);
            _areasByLine = GetList<ProductTableLineArea>(Manager, x => x.ProductTableLineId);
        }

        public ProductTableLine(IDatabaseManager manager, object id)
            : this (manager)
        {
            this.LoadObject(id);
        }
        
        public ProductTableLine(IDatabaseManager manager, bool lazyLoading)
            : this (manager)
        {
            _lazy = lazyLoading;
        }

        #endregion

        #region Persistent Properties

        /// <summary>
        /// column="ARVE_PK"
        /// Linea de Producto
        /// </summary>
        private Guid _productLineId;
        [PersistentColumn("ARVE_PK")]
        public Guid ProductLineId
        {
            get => _productLineId;
            set
            {
                if (value == _productLineId) return;
                _productLineId = value;
                IsDirty = true;
            }
        }
        
        /// <summary>
        /// column="ARTB_PK"
        /// Producto Detalle
        /// </summary>
        private Guid? _menuId;
        [PersistentColumn("ARTB_PK")]
        public Guid? MenuId
        {
            get => _menuId;
            set
            {
                if (value == _menuId) return;
                _menuId = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="ARTG_PK"
        /// Producto Detalle
        /// </summary>
        private Guid _productId;
        [PersistentColumn("ARTG_PK")]
        public Guid ProductId
        {
            get => _productId;
            set
            {
                if (value == _productId) return;
                _productId = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="SEPA_PK"
        /// Separator id
        /// </summary>
        private Guid? _separatorId;
        [PersistentColumn("SEPA_PK")]
        public Guid? SeparatorId
        {
            get => _separatorId;
            set
            {
                if (value == _separatorId) return;
                _separatorId = value;
                IsDirty = true;
            }
        }
        
        /// <summary>
        /// column="SEPA_ORDE"
        /// Separator order
        /// </summary>
        private short? _separatorOrder;
        [PersistentColumn("SEPA_ORDE")]
        public short? SeparatorOrder
        {
            get => _separatorOrder;
            set
            {
                if (value == _separatorOrder) return;
                _separatorOrder = value;
                IsDirty = true;
            }
        }
        
        /// <summary>
        /// column="SEPA_DESC"
        /// Separator description Id (LITE_PK) 
        /// </summary>
        private Guid? _separatorDescriptionId;
        [PersistentColumn("SEPA_DESC")]
        public Guid? SeparatorDescriptionId
        {
            get => _separatorDescriptionId;
            set
            {
                if (value == _separatorDescriptionId) return;
                _separatorDescriptionId = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="TABL_CANT"
        /// Quantity
        /// </summary>
        private decimal _quantity;
        [PersistentColumn("TABL_CANT")]
        public decimal Quantity
        {
            get => _quantity;
            set
            {
                if (value == _quantity) return;
                _quantity = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="TABL_COST"
        /// Cost
        /// </summary>
        private decimal _cost;
        [PersistentColumn("TABL_COST")]
        public decimal Cost
        {
            get => _cost;
            set
            {
                if (value == _cost) return;
                _cost = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="TABL_PREC"
        /// Price
        /// </summary>
        private decimal _price;
        [PersistentColumn("TABL_PREC")]
        public decimal Price
        {
            get => _price;
            set
            {
                if (value == _price) return;
                _price = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="TABL_PORC"
        /// Percent of the product in the table
        /// </summary>
        private decimal _percent;
        [PersistentColumn("TABL_PORC")]
        public decimal Percent
        {
            get => _percent;
            set
            {
                if (value == _percent) return;
                _percent = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="TABL_ADIC"
        /// Is an additional
        /// </summary>
        private bool _isAdditional;
        [PersistentColumn("TABL_ADIC")]
        public bool IsAdditional
        {
            get => _isAdditional;
            set
            {
                if (value == _isAdditional) return;
                _isAdditional = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="TABL_IMAR"
        /// flag: Indica si ha sido impreso en la impresora de comanda (1.si, 0.no)
        /// </summary>
        private bool _isSendedToArea;
        [PersistentColumn("TABL_IMAR")]
        public bool IsSendedToArea
        {
            get => _isSendedToArea;
            set
            {
                if (value == _isSendedToArea) return;
                _isSendedToArea = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="TABL_EMAR"
        /// Indica el estado del envio al area de despacho:
        /// 0.No Pedido, 1.Pedido, 2.En Procesamiento, 3.Terminado, 4.Despachado
        /// </summary>
        private short _sendToAreaStatus;
        [PersistentColumn("TABL_EMAR")]
        public short SendToAreaStatus
        {
            get => _sendToAreaStatus;
            set
            {
                if (value == _sendToAreaStatus) return;
                _sendToAreaStatus = value;
                // ESTO ES TEMPORAL HASTA ELIMINAR TABL_IMAR
                _isSendedToArea = _sendToAreaStatus > 0;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="TABL_IMPR"
        /// Impreso
        /// </summary>
        private bool _printed;
        [PersistentColumn("TABL_IMPR")]
        public bool Printed
        {
            get => _printed;
            set
            {
                if (value == _printed) return;
                _printed = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="TABL_OBSV"
        /// Kitchen Notes
        /// </summary>
        private string _observations;
        [PersistentColumn("TABL_OBSV")]
        public string Observations
        {
            get => _observations;
            set
            {
                if (value == _observations) return;
                _observations = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// TABL_LMOD
        /// </summary>
        [PersistentLastModifiedColumn("TABL_LMOD")]
        public DateTime LastModified { get; set; }

        #endregion

        #region Lists

        private readonly IPersistentList<ProductsTableLinePreparation> _tableProductsByLinePreparation;
        
        [ReflectionExclude]
        public IPersistentList<ProductsTableLinePreparation> TableProductsByLinePreparation
        {
            get
            {
                if (_lazy && !_tableProductsByLinePreparation.Populated)
                    _tableProductsByLinePreparation.Execute(Id);

                return _tableProductsByLinePreparation;
            }
        }
        
        private readonly IPersistentList<ProductTableLineArea> _areasByLine;
        [ReflectionExclude]
        public IPersistentList<ProductTableLineArea> AreasByLine
        {
            get
            {
                if (_lazy && !_areasByLine.Populated)
                    _areasByLine.Execute(Id);

                return _areasByLine;
            }
        }

        #endregion

        public override bool SaveObject()
        {
            if (!PersistObjectToDB()) return false;
            if (_tableProductsByLinePreparation.Populated)
                _tableProductsByLinePreparation.PersistOrDeleteObjects();

            return base.SaveObject();

        }
        
        /// <summary>
        /// column="AREA_PK"
        /// Area de despacho a la que fue enviado, ref. tnht_area(area_pk)
        /// </summary>
        private Guid? _areaId;
        [PersistentColumn("AREA_PK")]
        public Guid? AreaId
        {
            get => _areaId;
            set
            {
                if (value == _areaId) return;
                _areaId = value;
                IsDirty = true;
            }
        }
    }
}