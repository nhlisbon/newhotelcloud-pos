﻿using System;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.Pos.Hub.Model;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Tickets
{
    /// <summary>
    /// table="TNHT_ARVE
    /// Linea de Productos
    /// </summary>
    [PersistentTable("TNHT_ARVE", "ARVE_PK")]
    public class ProductLine : BasePersistent, IDataFullOperations
    {
        private readonly bool _lazy = true;
        
        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public ProductLine(IDatabaseManager manager)
            : base (manager)
        {
            _preparationsByLine = GetList<PreparationsByLine>(Manager, x => x.ProductLineId);
            _areasByLine = GetList<AreasByLine>(Manager, x => x.ProductLineId);
            _tableProductsByLine = GetList<ProductTableLine>(Manager, x => x.ProductLineId);
        }

        public ProductLine(IDatabaseManager manager, object id)
            : this (manager)
        {
            this.LoadObject(id);
        }
        
        public ProductLine(IDatabaseManager manager, bool lazyLoading)
            : this (manager)
        {
            _lazy = lazyLoading;
        }

        public override void Initialize()
        {
            ProductQtd = 1;
            //CreationDate = DateTime.Now;
        }

		#endregion

		#region Persistent Properties

		private Guid _ticket;
		/// <summary>
		/// column="VEND_PK"
		/// Ticket
		/// </summary>
		[PersistentColumn("VEND_PK")]
        public Guid Ticket
        {
            get => _ticket;
            set
            {
                if (value == _ticket) return;
                _ticket = value;
                IsDirty = true;
            }
        }

		private Guid _stand;
		/// <summary>
		/// column="IPOS_PK"
		/// Punto de venta que posteo el producto
		/// </summary>
		[PersistentColumn("IPOS_PK")]
        public Guid Stand
        {
            get { return _stand; }
            set
            {
                if (value != _stand)
                {
                    _stand = value;
                    IsDirty = true;
                }
            }
        }

		private Guid _caja;
		/// <summary>
		/// column="CAJA_PK"
		/// Caja
		/// </summary>
		[PersistentColumn("CAJA_PK")]
        public Guid Caja
        {
            get { return _caja; }
            set
            {
                if (value != _caja)
                {
                    _caja = value;
                    IsDirty = true;
                }
            }
        }

		private Guid _product;
		/// <summary>
		/// column="ARTG_PK"
		/// Producto
		/// </summary>
		[PersistentColumn("ARTG_PK")]
        public Guid Product
        {
            get { return _product; }
            set
            {
                if (value != _product)
                {
                    _product = value;
                    IsDirty = true;
                }
            }
        }

		private Guid? _separator;
		/// <summary>
		/// column="SEPA_PK"
		/// Separator de productos en el ticket
		/// </summary>
		[PersistentColumn("SEPA_PK")]
        public Guid? Separator
        {
            get { return _separator; }
            set
            {
                if (value != _separator)
                {
                    _separator = value;
                    IsDirty = true;
                }
            }
        }

		private decimal _productQtd;
		/// <summary>
		/// column="ARVE_QTDS"
		/// Cantidad de producto
		/// </summary>
		[PersistentColumn("ARVE_QTDS")]
        public decimal ProductQtd
        {
            get { return _productQtd; }
            set
            {
                if (value != _productQtd)
                {
                    _productQtd = value;
                    IsDirty = true;
                }
            }
        }

		private decimal _grossValue;
		/// <summary>
		/// column="ARVE_TOTL"
		/// Total de la linea
		/// </summary>
		[PersistentColumn("ARVE_TOTL")]
        public decimal GrossValue
        {
            get { return _grossValue; }
            set
            {
                if (value != _grossValue)
                {
                    _grossValue = value;
                    IsDirty = true;
                }
            }
        }

		private decimal _netValue;
		/// <summary>
		/// column="ARVE_VLIQ"
		/// Valor sin impuestos de la linea
		/// </summary>
		[PersistentColumn("ARVE_VLIQ")]
        public decimal NetValue
        {
            get { return _netValue; }
            set
            {
                if (value != _netValue)
                {
                    _netValue = value;
                    IsDirty = true;
                }
            }
        }

		private decimal _costValue;
		/// <summary>
		/// column="ARVE_COST"
		/// Costo
		/// </summary>
		[PersistentColumn("ARVE_COST")]
        public decimal CostValue
        {
            get { return _costValue; }
            set
            {
                if (value != _costValue)
                {
                    _costValue = value;
                    IsDirty = true;
                }
            }
        }

		private decimal _discountValue;
		/// <summary>
		/// column="ARVE_DESC"
		/// Valor del descuento
		/// </summary>
		[PersistentColumn("ARVE_DESC")]
        public decimal DiscountValue
        {
            get { return _discountValue; }
            set
            {
                if (value != _discountValue)
                {
                    _discountValue = value;
                    IsDirty = true;
                }
            }
        }

		private decimal? _discountPercent;
		/// <summary>
		/// column="ARVE_PDES"
		/// % descuento aplicado a la linea
		/// </summary>
		[PersistentColumn("ARVE_PDES")]
        public decimal? DiscountPercent
        {
            get { return _discountPercent; }
            set
            {
                if (value != _discountPercent)
                {
                    _discountPercent = value;
                    IsDirty = true;
                }
            }
        }

		private decimal _valueBeforeDiscount;
		/// <summary>
		/// column="ARVE_VSDE"
		/// Valor antes de aplicar descuento
		/// </summary>
		[PersistentColumn("ARVE_VSDE")]
        public decimal ValueBeforeDiscount
        {
            get { return _valueBeforeDiscount; }
            set
            {
                if (value != _valueBeforeDiscount)
                {
                    _valueBeforeDiscount = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_DEAU"
        /// Descuento automático aplicado?
        /// </summary>
        private bool _automaticDiscount;

        [PersistentColumn("ARVE_DEAU")]
        public bool AutomaticDiscount
        {
            get { return _automaticDiscount; }
            set
            {
                if (value != _automaticDiscount)
                {
                    _automaticDiscount = value;
                    IsDirty = true;
                }
            }
        }

		private decimal _recharge;
		/// <summary>
		/// column="ARVE_RECA"
		/// Valor del recargo
		/// </summary>
		[PersistentColumn("ARVE_RECA")]
        public decimal Recharge
        {
            get { return _recharge; }
            set
            {
                if (value != _recharge)
                {
                    _recharge = value;
                    IsDirty = true;
                }
            }
        }

		private decimal _grossValueInBase;
		/// <summary>
		/// column="ARVE_VAME"
		/// Total de la linea en moneda base
		/// </summary>
		[PersistentColumn("ARVE_VAME")]
        public decimal GrossValueInBase
		{
            get { return _grossValueInBase; }
            set
            {
                Set(ref _grossValueInBase, value, nameof(GrossValueInBase));
            }
        }

		private Guid _userId;
		/// <summary>
		/// column="UTIL_PK"
		/// Empleado o dependiente que posteo el producto
		/// </summary>
		[PersistentColumn("UTIL_PK")]
        public Guid UserId
        {
            get { return _userId; }
            set
            {
                if (value != _userId)
                {
                    _userId = value;
                    IsDirty = true;
                }
            }
        }

		private Guid? _annulUserId;
		/// <summary>
		/// column="ANUL_UTIL"
		/// User who canceled the product
		/// </summary>
		[PersistentColumn("ANUL_UTIL")]
        public Guid? AnnulUserId
        {
            get => _annulUserId;
            set
            {
                if (value == _annulUserId) return;
                _annulUserId = value;
                IsDirty = true;
            }
        }

		private Guid _firstIvaId;
		/// <summary>
		/// column="IVAS_CODI"
		/// Codigo del primer iva aplicado
		/// </summary>
		[PersistentColumn("IVAS_CODI")]
        public Guid FirstIvaId
        {
            get { return _firstIvaId; }
            set
            {
                if (value != _firstIvaId)
                {
                    _firstIvaId = value;
                    IsDirty = true;
                }
            }
        }

		private decimal _firstIvaPercent;
		/// <summary>
		/// column="ARVE_POR1"
		/// Porciento aplicado en el primer impuesto
		/// </summary>
		[PersistentColumn("ARVE_POR1")]
        public decimal FirstIvaPercent
        {
            get { return _firstIvaPercent; }
            set
            {
                if (value != _firstIvaPercent)
                {
                    _firstIvaPercent = value;
                    IsDirty = true;
                }
            }
        }

		private decimal _firstIvasValue;
		/// <summary>
		/// column="ARVE_IVAS"
		/// Valor del primer ivas
		/// </summary>
		[PersistentColumn("ARVE_IVAS")]
        public decimal FirstIvaValue
        {
            get { return _firstIvasValue; }
            set
            {
                if (value != _firstIvasValue)
                {
                    _firstIvasValue = value;
                    IsDirty = true;
                }
            }
        }

		private Guid? _secondIvaId;
		/// <summary>
		/// column="IVAS_COD2"
		/// Codigo del segundo iva aplicado
		/// </summary>
		[PersistentColumn("IVAS_COD2")]
        public Guid? SecondIvaId
        {
            get { return _secondIvaId; }
            set
            {
                if (value != _secondIvaId)
                {
                    _secondIvaId = value;
                    IsDirty = true;
                }
            }
        }

		private decimal? _secondIvaPercent;
		/// <summary>
		/// column="ARVE_POR2"
		/// Porciento aplicado en el segundo impuesto
		/// </summary>
		[PersistentColumn("ARVE_POR2")]
        public decimal? SecondIvaPercent
        {
            get { return _secondIvaPercent; }
            set
            {
                if (value != _secondIvaPercent)
                {
                    _secondIvaPercent = value;
                    IsDirty = true;
                }
            }
        }

		private decimal? _secondIvaValue;
		/// <summary>
		/// column="ARVE_IVA2"
		/// Valor del segundo ivas
		/// </summary>
		[PersistentColumn("ARVE_IVA2")]
        public decimal? SecondIvaValue
        {
            get { return _secondIvaValue; }
            set
            {
                if (value != _secondIvaValue)
                {
                    _secondIvaValue = value;
                    IsDirty = true;
                }
            }
        }

		private Guid? _thirdIvaId;
		/// <summary>
		/// column="IVAS_COD3"
		/// Codigo del tercer iva aplicado
		/// </summary>
		[PersistentColumn("IVAS_COD3")]
        public Guid? ThirdIvaId
        {
            get { return _thirdIvaId; }
            set
            {
                if (value != _thirdIvaId)
                {
                    _thirdIvaId = value;
                    IsDirty = true;
                }
            }
        }

		private decimal? _thirdIvaPercent;
		/// <summary>
		/// column="ARVE_POR3"
		/// Porciento aplicado en el tercer impuesto
		/// </summary>
		[PersistentColumn("ARVE_POR3")]
        public decimal? ThirdIvaPercent
        {
            get { return _thirdIvaPercent; }
            set
            {
                if (value != _thirdIvaPercent)
                {
                    _thirdIvaPercent = value;
                    IsDirty = true;
                }
            }
        }

		private decimal? _thirdIvaValue;
		/// <summary>
		/// column="ARVE_IVA3"
		/// Valor del tercer ivas
		/// </summary>
		[PersistentColumn("ARVE_IVA3")]
        public decimal? ThirdIvaValue
        {
            get { return _thirdIvaValue; }
            set
            {
                if (value != _thirdIvaValue)
                {
                    _thirdIvaValue = value;
                    IsDirty = true;
                }
            }
        }

		private short _annulmentCode;
		/// <summary>
		/// column="ARVE_ANUL"
		/// Indica el estado del producto
		/// 0. Activo
		/// 1. Anulado
		/// 2. Activo por transferencia desde otro ticket
		/// 3. Anulado después de impreso
		/// 4. Anulado por transferencia hacia otro ticket
		/// 5. Anulado por division del ticket
		/// </summary>
		[PersistentColumn("ARVE_ANUL")]
        public short AnnulmentCode
        {
            get { return _annulmentCode; }
            set
            {
                if (value != _annulmentCode)
                {
                    _annulmentCode = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ANUL_PAID"
        /// Indica si el producto esta anulado despues de estar cerrado el ticket
        /// </summary>
        private bool _anulAfterClosed;

        [PersistentColumn("ANUL_PAID")]
        public bool AnulAfterClosed
        {
            get { return _anulAfterClosed; }
            set
            {
                if (value != _anulAfterClosed)
                {
                    _anulAfterClosed = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_IMAR"
        /// flag: Indica si ha sido enviado al area de despacho (1.si, 0.no)
        /// </summary>
        private bool _isSendedToArea;

        [PersistentColumn("ARVE_IMAR")]
        public bool IsSendedToArea
        {
            get { return _isSendedToArea; }
            set
            {
                if (value != _isSendedToArea)
                {
                    _isSendedToArea = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_EMAR"
        /// Indica el estado del envio al area de despacho:
        /// 0.No Pedido, 1.Pedido, 2.En Procesamiento, 3.Terminado, 4.Despachado
        /// </summary>
        private short _sendToAreaStatus;

        [PersistentColumn("ARVE_EMAR")]
        public short SendToAreaStatus
        {
            get { return _sendToAreaStatus; }
            set
            {
                if (value != _sendToAreaStatus)
                {
                    _sendToAreaStatus = value;
                    // ESTO ES TEMPORAL HASTA ELIMINAR ARVE_IMAR
                    _isSendedToArea = _sendToAreaStatus > 0;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_INMA"
        /// Indica la fecha y hora de inicio del despacho
        /// </summary>
        private DateTime? _sendToAreaStartTime;

        [PersistentColumn("ARVE_INMA")]
        public DateTime? SendToAreaStartTime
        {
            get { return _sendToAreaStartTime; }
            set
            {
                if (value != _sendToAreaStartTime)
                {
                    _sendToAreaStartTime = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_FIMA"
        /// Indica la fecha y hora de fin del despacho
        /// </summary>
        private DateTime? _sendToAreaEndTime;

        [PersistentColumn("ARVE_FIMA")]
        public DateTime? SendToAreaEndTime
        {
            get { return _sendToAreaEndTime; }
            set
            {
                if (value != _sendToAreaEndTime)
                {
                    _sendToAreaEndTime = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_IMPR"
        /// Contador: Indica cuantas veces a sido impreso
        /// </summary>
        private short _printed;

        [PersistentColumn("ARVE_IMPR")]
        public short Printed
        {
            get { return _printed; }
            set
            {
                if (value != _printed)
                {
                    _printed = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="AREA_PK"
        /// Area de despacho a la que fue enviado, ref. tnht_area(area_pk)
        /// </summary>
        private Guid? _areaId;

        [PersistentColumn("AREA_PK")]
        public Guid? AreaId
        {
            get { return _areaId; }
            set
            {
                if (value != _areaId)
                {
                    _areaId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_HPPY"
        /// flag: producto cargado en happy hour (1.si, 0.no)
        /// </summary>
        private bool _isHappyHour;

        [PersistentColumn("ARVE_HPPY")]
        public bool IsHappyHour
        {
            get { return _isHappyHour; }
            set
            {
                if (value != _isHappyHour)
                {
                    _isHappyHour = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_ADIC"
        /// flag: Indica si el producto fue vendido como una adicion (1.si, 0.no)
        /// </summary>
        private bool _isAditional;

        [PersistentColumn("ARVE_ADIC")]
        public bool IsAditional
        {
            get { return _isAditional; }
            set
            {
                if (value != _isAditional)
                {
                    _isAditional = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_ACOM"
        /// Si es un producto acompannante de otro, producto principal
        /// </summary>
        private Guid? _mainProduct;

        [PersistentColumn("ARVE_ACOM")]
        public Guid? MainProduct
        {
            get { return _mainProduct; }
            set
            {
                if (value != _mainProduct)
                {
                    _mainProduct = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_OBSV"
        /// Observaciones a nivel de lina
        /// </summary>
        private string _observations;

        [PersistentColumn("ARVE_OBSV", NullStringAsEmpty = false)]
        public string Observations
        {
            get { return _observations; }
            set
            {
                if (value != _observations)
                {
                    _observations = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_OBSM"
        /// Observaciones para la marcha del producto
        /// </summary>
        private string _observationsMarch;

        [PersistentColumn("ARVE_OBSM", NullStringAsEmpty = false)]
        public string ObservationsMarch
        {
            get { return _observationsMarch; }
            set
            {
                if (value != _observationsMarch)
                {
                    _observationsMarch = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// TIDE_PK
        /// Discount Type
        /// </summary>
        private Guid? _discountTypeId;

        [PersistentColumn("TIDE_PK")]
        public Guid? DiscountTypeId
        {
            get { return _discountTypeId; }
            set
            {
                if (value != _discountTypeId)
                {
                    _discountTypeId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// SEPA_MANU
        /// Separador manual (Si 1, No 0)
        /// </summary>
        private bool _hasManualSeparator;

        [PersistentColumn("SEPA_MANU")]
        public bool HasManualSeparator
        {
            get { return _hasManualSeparator; }
            set
            {
                if (value != _hasManualSeparator)
                {
                    _hasManualSeparator = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// ARVE_MPRI
        /// Precio manual (Si 1, No 0)
        /// </summary>
        private bool _hasManualPrice;

        [PersistentColumn("ARVE_MPRI")]
        public bool HasManualPrice
        {
            get { return _hasManualPrice; }
            set
            {
                if (value != _hasManualPrice)
                {
                    _hasManualPrice = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_TIPL"
        /// Flag que indica si la linea es propina
        /// </summary>
        private bool _isTip;

        [PersistentColumn("ARVE_TIPL")]
        public bool IsTip
        {
            get { return _isTip; }
            set
            {
                if (value != _isTip)
                {
                    _isTip = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_NPAX"
        /// Número de paxs
        /// </summary>
        private short? _paxNumber;

        [PersistentColumn("ARVE_NPAX")]
        public short? PaxNumber
        {
            get { return _paxNumber; }
            set
            {
                if (value != _paxNumber)
                {
                    _paxNumber = value;
                    IsDirty = true;
                }
            }
        }

		private short _itemNumber;
		/// <summary>
		/// ARVE_ITEM
		/// Consecutivo de la linea en el ticket
		/// </summary>
		[PersistentColumn("ARVE_ITEM")]
        public short ItemNumber
        {
            get { return _itemNumber; }
            set
            {
                if (value != _itemNumber)
                {
                    _itemNumber = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// ARVE_DACR
        /// </summary>
        [PersistentColumn("ARVE_DACR")]
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// ARVE_LMOD
        /// </summary>
        [PersistentLastModifiedColumn("ARVE_LMOD")]
        public DateTime LastModified { get; set; }

        /// <summary>
        /// column="MAPR_DESC"
        /// Manual price description
        /// </summary>
        private string _manualPriceDesc;

        [PersistentColumn("MAPR_DESC")]
        public string ManualPriceDesc
        {
            get => _manualPriceDesc;
            set
            {
                if (value == _manualPriceDesc) return;
                _manualPriceDesc = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="DESC_ISTO"
        /// Descripcion impuesto 0% iva
        /// </summary>
        private string _descIsentoIva;

        [PersistentColumn("DESC_ISTO")]
        public string DescIsentoIva
        {
            get => _descIsentoIva;
            set
            {
                if (value == _descIsentoIva) return;
                _descIsentoIva = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// column="CODE_ISTO"
        ///  Codigo impuesto 0% iva
        /// </summary>
        private string _codeIsentoIva;

        [PersistentColumn("CODE_ISTO")]
        public string CodeIsentoIva
        {
            get => _codeIsentoIva;
            set
            {
                if (value == _codeIsentoIva) return;
                _codeIsentoIva = value;
                IsDirty = true;
            }
        }
        
        
        /// <summary>
        /// column="FROM_SPA"
        ///  If it was launched from a spa service
        /// </summary>
        private bool _isLaunchFromSpa;

        [PersistentColumn("FROM_SPA")]
        public bool IsLaunchFromSpa
        {
            get => _isLaunchFromSpa;
            set
            {
                if (value == _isLaunchFromSpa) return;
                _isLaunchFromSpa = value;
                IsDirty = true;
            }
        }

        public bool IsActive => AnnulmentCode is ProductLineCancellationStatus.Active or ProductLineCancellationStatus.ActiveByTransfer;

        #endregion

        #region Persistent Lists

        private readonly IPersistentList<PreparationsByLine> _preparationsByLine;

        [ReflectionExclude]
        public IPersistentList<PreparationsByLine> PreparationsByLine
        {
            get
            {
                if (_lazy && !_preparationsByLine.Populated)
                    _preparationsByLine.Execute(Id);

                return _preparationsByLine;
            }
        }

        private readonly IPersistentList<ProductTableLine> _tableProductsByLine;

        [ReflectionExclude]
        public IPersistentList<ProductTableLine> TableProductsByLine
        {
            get
            {
                if (_lazy && !_tableProductsByLine.Populated)
                    _tableProductsByLine.Execute(Id);

                return _tableProductsByLine;
            }
        }

        private readonly IPersistentList<AreasByLine> _areasByLine;

        [ReflectionExclude]
        public IPersistentList<AreasByLine> AreasByLine
        {
            get
            {
                if (_lazy && !_areasByLine.Populated)
                    _areasByLine.Execute(Id);

                return _areasByLine;
            }
        }

        #endregion

        public bool SaveObject()
        {
            if (CreationDate == DateTime.MinValue)
                CreationDate = DateTime.Now;

            if (PersistObjectToDB())
            {
                if (_preparationsByLine.Populated)
                    _preparationsByLine.PersistOrDeleteObjects();
                if (_tableProductsByLine.Populated)
                    _tableProductsByLine.PersistOrDeleteObjects();
                if (_areasByLine.Populated)
                    _areasByLine.PersistOrDeleteObjects();

                return true;
            }

            return false;
        }

        public bool DeleteObject()
        {
            return DeleteObjectFromDB();
        }

        public bool LoadObject(object id, bool throwNoDataFoundException)
        {
            return LoadObjectFromDB(id, throwNoDataFoundException);
        }
    }
}