﻿using System;
using NewHotel.Contracts;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Tickets
{
    /// <summary>
    /// table="TNHT_CLVE"
    /// Clientes del ticket por posición
    /// </summary>
    [PersistentTable("TNHT_CLVE", "CLVE_PK")]
    public class ClientByPosition : BasePersistentFullOperations
    {
        private readonly bool _lazy = true;

        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public ClientByPosition(IDatabaseManager manager)
            : base(manager)
        {
            _preferences = GetList<ClientByPositionPreference>(Manager, x => x.ClientByPositionId);
            _diets = GetList<ClientByPositionDiet>(Manager, x => x.ClientByPositionId);
            _attentions = GetList<ClientByPositionAttention>(Manager, x => x.ClientByPositionId);
            _allergies = GetList<ClientByPositionAllergy>(Manager, x => x.ClientByPositionId);
        }

        public ClientByPosition(IDatabaseManager manager, object id)
            : this(manager)
        {
            this.LoadObject(id);
        }
        
        public ClientByPosition(IDatabaseManager manager, bool lazyLoading)
            : this(manager)
        {
            _lazy = lazyLoading;
        }

        #endregion
        #region Persistent Properties

        /// <summary>
        /// column="VEND_PK"
        /// Ticket
        /// </summary>
        private Guid _ticket;
        [PersistentColumn("VEND_PK")]
        public Guid Ticket
        {
            get { return _ticket; }
            set
            {
                if (value != _ticket)
                {
                    _ticket = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="CLIE_NPAX"
        /// Pax
        /// </summary>
        private short _pax;
        [PersistentColumn("CLIE_NPAX")]
        public short Pax
        {
            get { return _pax; }
            set
            {
                if (value != _pax)
                {
                    _pax = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="CLIE_PK"
        /// Identificador de cliente
        /// </summary>
        private Guid? _clientId;
        [PersistentColumn("CLIE_PK")]
        public Guid? ClientId
        {
            get { return _clientId; }
            set
            {
                if (value != _clientId)
                {
                    _clientId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="CLIE_NAME"
        /// Nombre del cliente
        /// </summary>
        private string _clientName;
        [PersistentColumn("CLIE_NAME")]
        public string ClientName
        {
            get { return _clientName; }
            set
            {
                if (value != _clientName)
                {
                    _clientName = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="CLIE_ROOM"
        /// Número de habitación del cliente
        /// </summary>
        private string _clientRoom;
        [PersistentColumn("CLIE_ROOM")]
        public string ClientRoom
        {
            get { return _clientRoom; }
            set
            {
                if (value != _clientRoom)
                {
                    _clientRoom = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="CLIE_ALLE"
        /// Uncommon Allergies
        /// </summary>
        private string _uncommonAllergies;
        [PersistentColumn("CLIE_ALLE")]
        public string UncommonAllergies
        {
            get => _uncommonAllergies;
            set
            {
                if (value == _uncommonAllergies) return;
                _uncommonAllergies = value;
                IsDirty = true;
            }
        }
        
        /// <summary>
        /// column="CLIE_SEOP"
        /// Uncommon Allergies
        /// </summary>
        private string _segmentOperations;
        [PersistentColumn("CLIE_SEOP")]
        public string SegmentOperations
        {
            get => _segmentOperations;
            set
            {
                if (value == _segmentOperations) return;
                _segmentOperations = value;
                IsDirty = true;
            }
        }

        #endregion
        #region Persistent Lists

        private IPersistentList<ClientByPositionPreference> _preferences;
        [ReflectionExclude]
        public IPersistentList<ClientByPositionPreference> Preferences
        {
            get
            {
                if (_lazy && !_preferences.Populated)
                    _preferences.Execute(Id);

                return _preferences;
            }
        }

        private IPersistentList<ClientByPositionDiet> _diets;
        [ReflectionExclude]
        public IPersistentList<ClientByPositionDiet> Diets
        {
            get
            {
                if (_lazy && !_diets.Populated)
                    _diets.Execute(Id);

                return _diets;
            }
        }

        private IPersistentList<ClientByPositionAttention> _attentions;
        [ReflectionExclude]
        public IPersistentList<ClientByPositionAttention> Attentions
        {
            get
            {
                if (_lazy && !_attentions.Populated)
                    _attentions.Execute(Id);

                return _attentions;
            }
        }

        private IPersistentList<ClientByPositionAllergy> _allergies;
        [ReflectionExclude]
        public IPersistentList<ClientByPositionAllergy> Allergies
        {
            get
            {
                if (_lazy && !_allergies.Populated)
                    _allergies.Execute(Id);

                return _allergies;
            }
        }

        #endregion
        #region IDataBasicOperations Operations

        public override bool SaveObject()
        {
            if (base.SaveObject())
            {
                if (_preferences.Populated)
                    Preferences.PersistOrDeleteObjects();
                if (_diets.Populated)
                    Diets.PersistOrDeleteObjects();
                if (_attentions.Populated)
                    Attentions.PersistOrDeleteObjects();
                if (_allergies.Populated)
                    Allergies.PersistOrDeleteObjects();

                return true;
            }

            return false;
        }

        #endregion
    }
}