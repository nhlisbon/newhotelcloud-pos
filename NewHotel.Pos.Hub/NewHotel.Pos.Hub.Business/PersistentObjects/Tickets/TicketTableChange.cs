﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Tickets
{
    /// <summary>
    /// table="TNHT_TITC"
    /// Cambio de mesa
    /// </summary>
    [PersistentTable("TNHT_TITC", "TITC_PK")]
    public class TicketTableChange : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public TicketTableChange(IDatabaseManager manager)
            : base(manager)
        {
        }

        public TicketTableChange(IDatabaseManager manager, object id)
            : base(manager)
        {
            this.LoadObject(id);
        }

        #endregion
        #region Persistent Properties

        /// <summary>
        /// column="TITC_TIME"
        /// Fecha y hora de cambio de mesa
        /// </summary>
        private DateTime _changeTime;
        [PersistentColumn("TITC_TIME")]
        public DateTime ChangeTime
        {
            get { return _changeTime; }
            set
            {
                if (value != _changeTime)
                {
                    _changeTime = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="VEND_PK"
        /// Ticket
        /// </summary>
        private Guid _ticketId;
        [PersistentColumn("VEND_PK")]
        public Guid TicketId
        {
            get { return _ticketId; }
            set
            {
                if (value != _ticketId)
                {
                    _ticketId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="MESA_ORIG"
        /// Mesa origen
        /// </summary>
        private Guid? _tableOriginId;
        [PersistentColumn("MESA_ORIG")]
        public Guid? TableOriginId
        {
            get { return _tableOriginId; }
            set
            {
                if (value != _tableOriginId)
                {
                    _tableOriginId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="MESA_DEST"
        /// Mesa destino
        /// </summary>
        private Guid? _tableDestId;
        [PersistentColumn("MESA_DEST")]
        public Guid? TableDestId
        {
            get { return _tableDestId; }
            set
            {
                if (value != _tableDestId)
                {
                    _tableDestId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="UTIL_PK"
        /// Usuario que cambia la mesa
        /// </summary>
        private Guid _userId;
        [PersistentColumn("UTIL_PK")]
        public Guid UserId
        {
            get { return _userId; }
            set
            {
                if (value != _userId)
                {
                    _userId = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
    }
}