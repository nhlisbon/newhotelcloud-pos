﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Tickets
{
    /// <summary>
    /// table="TNHT_ARVA
    /// Areas de despacho por linea de Productos
    /// </summary>
    [PersistentTable("TNHT_ARVA", "ARVA_PK")]
    public class AreasByLine : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public AreasByLine(IDatabaseManager manager)
            : base (manager)
        {
        }

        public AreasByLine(IDatabaseManager manager, object id)
            : this (manager)
        {
            this.LoadObject(id);
        }

        #endregion
        #region Persistent Properties

        /// <summary>
        /// column="ARVE_PK"
        /// Linea de Producto
        /// </summary>
        private Guid _productLineId;
        [PersistentColumn("ARVE_PK")]
        public Guid ProductLineId
        {
            get { return _productLineId; }
            set
            {
                if (value != _productLineId)
                {
                    _productLineId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="AREA_PK"
        /// Area
        /// </summary>
        private Guid _areaId;
        [PersistentColumn("AREA_PK")]
        public Guid AreaId
        {
            get { return _areaId; }
            set
            {
                if (value != _areaId)
                {
                    _areaId = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
    }
}