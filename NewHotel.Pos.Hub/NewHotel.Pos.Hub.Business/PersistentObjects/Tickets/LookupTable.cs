﻿using System;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.DataAnnotations;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Tickets
{
    /// <summary>
    /// table="TNHT_LKTB"
    /// Documento Consulta de Mesas
    /// </summary>
    [PersistentTable("TNHT_LKTB", "LKTB_PK")]
    public class LookupTable : BasePersistentFullOperations
    {
        private readonly bool _lazy = true;

        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public LookupTable(IDatabaseManager manager)
            : base(manager)
        {
            _detailsByLookup = GetList<LookupTableDetails>(Manager, x => x.LookupTableId);
        }

        public LookupTable(IDatabaseManager manager, object id)
            : this(manager)
        {
            this.LoadObject(id);
        }

        public LookupTable(IDatabaseManager manager, bool lazyLoading)
            : this(manager)
        {
            _lazy = lazyLoading;
        }

        #endregion
        #region Persistent Properties

        /// <summary>
        /// column="VEND_PK"
        /// Ticket que genera la consulta de mesa
        /// </summary>
        private Guid _ticket;
        [PersistentColumn("VEND_PK")]
        public Guid Ticket
        {
            get { return _ticket; }
            set
            {
                if (value != _ticket)
                {
                    _ticket = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="UTIL_PK"
        /// Usuario asociado, ref. tnht_util (util_pk)
        /// </summary>
        private Guid _user;
        [PersistentColumn("UTIL_PK")]
        public Guid User
        {
            get { return _user; }
            set
            {
                if (value != _user)
                {
                    _user = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="LKTB_SERI"
        /// Serie
        /// </summary>
        private string _serie;
        [PersistentColumn("LKTB_SERI")]
        public string Serie
        {
            get { return _serie; }
            set
            {
                if (value != _serie)
                {
                    _serie = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="LKTB_CODI"
        /// Numero Documento
        /// </summary>
        private long _number;
        [PersistentColumn("LKTB_CODI")]
        public long Number
        {
            get { return _number; }
            set
            {
                if (value != _number)
                {
                    _number = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="LKTB_DATE"
        /// Fecha Trabajo
        /// </summary>
        private DateTime _workDate;
        [PersistentColumn("LKTB_DATE")]
        public DateTime WorkDate
        {
            get { return _workDate; }
            set
            {
                if (value != _workDate)
                {
                    _workDate = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="LKTB_STIM"
        /// Fecha Sistema
        /// </summary>
        private DateTime _systemDateTime;
        [PersistentColumn("LKTB_STIM")]
        public DateTime SystemDateTime
        {
            get { return _systemDateTime; }
            set
            {
                if (value != _systemDateTime)
                {
                    _systemDateTime = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="LKTB_TOTA"
        /// Valor
        /// </summary>
        private decimal _total;
        [PersistentColumn("LKTB_TOTA")]
        public decimal Total
        {
            get { return _total; }
            set
            {
                if (value != _total)
                {
                    _total = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="LKTB_SIGN"
        /// Firma digital
        /// </summary>
        private string _signature;
        [PersistentColumn("LKTB_SIGN")]
        public string Signature
        {
            get { return _signature; }
            set
            {
                if (value != _signature)
                {
                    _signature = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="LKTB_QRCO"
        /// Código QR de la consulta de mesa
        /// </summary>
        private Blob? _qrCode;
        [PersistentColumn("LKTB_QRCO")]
        public Blob? QrCode
        {
            get => _qrCode;
            set
            {
                if (value != _qrCode)
                {
                    _qrCode = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="LKTB_COVA"
        /// Código de validación de la consulta de mesa: SAFT-PT ATCUD
        /// </summary>
        private string _validationCode;
        [PersistentColumn("LKTB_COVA")]
        public string ValidationCode
        {
            get => _validationCode;
            set
            {
                if (value != _validationCode)
                {
                    _validationCode = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
        #region Persistent Lists

        private IPersistentList<LookupTableDetails> _detailsByLookup;
        [ReflectionExclude]
        public IPersistentList<LookupTableDetails> DetailsByLookup
        {
            get
            {
                if (_lazy && !_detailsByLookup.Populated)
                    _detailsByLookup.Execute(Id);

                return _detailsByLookup;
            }
        }

        #endregion
        #region IDataBasicOperations operations
        public override bool SaveObject()
        {
            PersistObjectToDB();

            if (_detailsByLookup.Populated)
                DetailsByLookup.PersistOrDeleteObjects();

            return true;
        }
        #endregion
    }
}
