﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Tickets
{
    /// <summary>
    /// table="TNHT_LKDE"
    /// Detalle Consulta de Mesas
    /// </summary>
    [PersistentTable("TNHT_LKDE", "LKDE_PK")]
    public class LookupTableDetails : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public LookupTableDetails(IDatabaseManager manager)
            : base(manager) { }

        public LookupTableDetails(IDatabaseManager manager, object id)
            : base(manager)
        {
            this.LoadObject(id);
        }

        #endregion
        #region Persistent Properties

        /// <summary>
        /// column="LKTB_PK"
        /// Consulta de Mesa
        /// </summary>
        private Guid _lookupTableId;
        [PersistentColumn("LKTB_PK")]
        public Guid LookupTableId
        {
            get { return _lookupTableId; }
            set
            {
                if (value != _lookupTableId)
                {
                    _lookupTableId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_QTDS"
        /// Cantidad de producto
        /// </summary>
        private decimal _quantity;
        [PersistentColumn("ARVE_QTDS")]
        public decimal Quantity
        {
            get { return _quantity; }
            set
            {
                if (value != _quantity)
                {
                    _quantity = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_TOTL"
        /// Total de la linea
        /// </summary>
        private decimal _totalPrice;
        [PersistentColumn("ARVE_TOTL")]
        public decimal TotalPrice
        {
            get { return _totalPrice; }
            set
            {
                if (value != _totalPrice)
                {
                    _totalPrice = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_VLIQ"
        /// Valor sin impuestos de la linea
        /// </summary>
        private decimal _netPrice;
        [PersistentColumn("ARVE_VLIQ")]
        public decimal NetPrice
        {
            get { return _netPrice; }
            set
            {
                if (value != _netPrice)
                {
                    _netPrice = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARTG_PK"
        /// Producto
        /// </summary>
        private Guid _productId;
        [PersistentColumn("ARTG_PK")]
        public Guid ProductId
        {
            get { return _productId; }
            set
            {
                if (value != _productId)
                {
                    _productId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_ANUL"
        /// Indica el estado del producto
        /// 0. Activo
        /// 1. Anulado
        /// 2. Activo por transferencia desde otro ticket
        /// 3. Anulado después de impreso
        /// 4. Anulado por transferencia hacia otro ticket
        /// 5. Anulado por division del ticket
        /// </summary>
        private short _annulmentCode;
        [PersistentColumn("ARVE_ANUL")]
        public short AnnulmentCode
        {
            get { return _annulmentCode; }
            set
            {
                if (value != _annulmentCode)
                {
                    _annulmentCode = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="IVAS_CODI"
        /// Codigo del primer iva aplicado
        /// </summary>
        private Guid? _firstIvaId;
        [PersistentColumn("IVAS_CODI")]
        public Guid? FirstIvaId
        {
            get { return _firstIvaId; }
            set
            {
                if (value != _firstIvaId)
                {
                    _firstIvaId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_POR1"
        /// Porciento aplicado en el primer impuesto
        /// </summary>
        private decimal? _firstIvaPercent;
        [PersistentColumn("ARVE_POR1")]
        public decimal? FirstIvaPercent
        {
            get { return _firstIvaPercent; }
            set
            {
                if (value != _firstIvaPercent)
                {
                    _firstIvaPercent = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_IVAS"
        /// Valor del primer iva
        /// </summary>
        private decimal? _firstIvasValue;
        [PersistentColumn("ARVE_IVAS")]
        public decimal? FirstIvaValue
        {
            get { return _firstIvasValue; }
            set
            {
                if (value != _firstIvasValue)
                {
                    _firstIvasValue = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="IVAS_COD2"
        /// Codigo del segundo iva aplicado
        /// </summary>
        private Guid? _secondIvaId;
        [PersistentColumn("IVAS_COD2")]
        public Guid? SecondIvaId
        {
            get { return _secondIvaId; }
            set
            {
                if (value != _secondIvaId)
                {
                    _secondIvaId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_POR2"
        /// Porciento aplicado en el segundo impuesto
        /// </summary>
        private decimal? _secondIvaPercent;
        [PersistentColumn("ARVE_POR2")]
        public decimal? SecondIvaPercent
        {
            get { return _secondIvaPercent; }
            set
            {
                if (value != _secondIvaPercent)
                {
                    _secondIvaPercent = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_IVA2"
        /// Valor del segundo iva
        /// </summary>
        private decimal? _secondIvaValue;
        [PersistentColumn("ARVE_IVA2")]
        public decimal? SecondIvaValue
        {
            get { return _secondIvaValue; }
            set
            {
                if (value != _secondIvaValue)
                {
                    _secondIvaValue = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="IVAS_COD3"
        /// Codigo del tercer iva aplicado
        /// </summary>
        private Guid? _thirdIvaId;
        [PersistentColumn("IVAS_COD3")]
        public Guid? ThirdIvaId
        {
            get { return _thirdIvaId; }
            set
            {
                if (value != _thirdIvaId)
                {
                    _thirdIvaId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_POR2"
        /// Porciento aplicado en el tercer impuesto
        /// </summary>
        private decimal? _thirdIvaPercent;
        [PersistentColumn("ARVE_POR3")]
        public decimal? ThirdIvaPercent
        {
            get { return _thirdIvaPercent; }
            set
            {
                if (value != _thirdIvaPercent)
                {
                    _thirdIvaPercent = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ARVE_IVA3"
        /// Valor del tercer iva
        /// </summary>
        private decimal? _thirdIvaValue;
        [PersistentColumn("ARVE_IVA3")]
        public decimal? ThirdIvaValue
        {
            get { return _thirdIvaValue; }
            set
            {
                if (value != _thirdIvaValue)
                {
                    _thirdIvaValue = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
    }
}