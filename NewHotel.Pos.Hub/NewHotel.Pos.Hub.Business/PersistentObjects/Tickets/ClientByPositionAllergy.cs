﻿using System;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.DataAnnotations;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Tickets
{
    /// <summary>
    /// table="TNHT_CLAL"
    /// Alergias de Clientes por posición
    /// </summary>
    [PersistentTable("TNHT_CLAL", "CLAL_PK")]
    public class ClientByPositionAllergy : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public ClientByPositionAllergy(IDatabaseManager manager)
            : base(manager)
        {
        }

        public ClientByPositionAllergy(IDatabaseManager manager, object id)
            : this(manager)
        {
            this.LoadObject(id);
        }

        #endregion     
        #region Persistent Properties

        /// <summary>
        /// column="CLVE_PK"
        /// Cliente por posición
        /// </summary>
        private Guid _clientByPositionId;
        [PersistentColumn("CLVE_PK")]
        public Guid ClientByPositionId
        {
            get { return _clientByPositionId; }
            set
            {
                if (value != _clientByPositionId)
                {
                    _clientByPositionId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ALLE_PK"
        /// Id de la alergia
        /// </summary>
        private Guid _allergyId;
        [PersistentColumn("ALLE_PK")]
        public Guid AllergyId
        {
            get { return _allergyId; }
            set
            {
                if (value != _allergyId)
                {
                    _allergyId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="ALLE_DESC"
        /// Descripción de la alergia
        /// </summary>
        private string _description;
        [PersistentColumn("ALLE_DESC")]
        public string Description
        {
            get { return _description; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
    }
}