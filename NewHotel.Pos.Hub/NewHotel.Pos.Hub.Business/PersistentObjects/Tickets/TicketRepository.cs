﻿using System;
using NewHotel.Core;
using NewHotel.Pos.Hub.Business.Validations;
using NewHotel.Pos.Hub.Model.Session;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Tickets
{
	public interface ITicketRepository
	{
		Ticket GetTicket(Guid id, bool lazyLoading = true);
		Ticket NewTicket();
	}

	public class TicketRepository : ITicketRepository
	{
		private readonly IDatabaseManager manager;
		private readonly IPosSessionContext sessionContext;
		private readonly IValidatorFactory validatorFactory;

		public TicketRepository(IDatabaseManager manager, IPosSessionContext sessionContext, IValidatorFactory validatorFactory)
		{
			this.manager = manager;
			this.sessionContext = sessionContext;
			this.validatorFactory = validatorFactory;
		}

		public Ticket GetTicket(Guid id, bool lazyLoading = true)
		{
			return Ticket.Create(manager, sessionContext, id, lazyLoading, validatorFactory);
		}

		public Ticket NewTicket()
		{
			return Ticket.Create(manager, sessionContext, validatorFactory);
		}
	}
}