﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Tickets
{
    /// <summary>
    /// table="TNHT_TABL
    /// Productos por Linea de Table
    /// </summary>
    [PersistentTable("TNHT_TABP", "TABP_PK")]
    public class ProductsTableLinePreparation : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public ProductsTableLinePreparation(IDatabaseManager manager)
            : base (manager)
        {
        }

        public ProductsTableLinePreparation(IDatabaseManager manager, object id)
            : this (manager)
        {
            this.LoadObject(id);
        }

        #endregion
        #region Persistent Properties

        /// <summary>
        /// column="TABL_PK"
        /// Linea del Table
        /// </summary>
        private Guid _tableLineId;
        [PersistentColumn("TABL_PK")]
        public Guid TableLineId
        {
            get { return _tableLineId; }
            set
            {
                if (value != _tableLineId)
                {
                    _tableLineId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="PREP_PK"
        /// Preparacion
        /// </summary>
        private Guid _preparationId;
        [PersistentColumn("PREP_PK")]
        public Guid PreparationId
        {
            get { return _preparationId; }
            set
            {
                if (value != _preparationId)
                {
                    _preparationId = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
    }
}
