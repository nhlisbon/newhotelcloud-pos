﻿using System;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.DataAnnotations;

namespace NewHotel.Pos.Hub.Business.PersistentObjects.Tickets
{
    /// <summary>
    /// table="TNHT_CLAT"
    /// Atenciones de Clientes por posición
    /// </summary>
    [PersistentTable("TNHT_CLAT", "CLAT_PK")]
    public class ClientByPositionAttention : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public ClientByPositionAttention(IDatabaseManager manager)
            : base(manager)
        {
        }

        public ClientByPositionAttention(IDatabaseManager manager, object id)
            : this(manager)
        {
            this.LoadObject(id);
        }

        #endregion     
        #region Persistent Properties

        /// <summary>
        /// column="CLVE_PK"
        /// Cliente por posición
        /// </summary>
        private Guid _clientByPositionId;
        [PersistentColumn("CLVE_PK")]
        public Guid ClientByPositionId
        {
            get { return _clientByPositionId; }
            set
            {
                if (value != _clientByPositionId)
                {
                    _clientByPositionId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="TIAT_PK"
        /// Id de la atención
        /// </summary>
        private Guid _attentionId;
        [PersistentColumn("TIAT_PK")]
        public Guid AttentionId
        {
            get { return _attentionId; }
            set
            {
                if (value != _attentionId)
                {
                    _attentionId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// column="TIAT_DESC"
        /// Descripción de la atención
        /// </summary>
        private string _description;
        [PersistentColumn("TIAT_DESC")]
        public string Description
        {
            get { return _description; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
    }
}