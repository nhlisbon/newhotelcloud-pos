﻿using System;
using NewHotel.Contracts;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects
{
    [PersistentTable("TNHT_MESA", "MESA_PK")]
    public class Table : BasePersistentFullOperations
    {
        #region Constructor + Initialize
        public Table(IDatabaseManager manager)
            : base(manager)
        {

        }
        public Table(IDatabaseManager manager, object id)
            : base(manager)
        {
            this.LoadObject(id);
        }

        public override void Initialize()
        {
            base.Initialize();
            Type = EPosMesaType.Standard;
        }


        #endregion
        #region Persistent Properties

        /// <summary>
        ///Descripcion
        /// </summary>
        private string _description;
        [PersistentColumn("MESA_DESC")]
        public string Description
        {
            get { return _description; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        ///Salon
        /// </summary>
        private Guid _saloon;
        [PersistentColumn("SALO_PK")]
        public Guid Saloon
        {
            get { return _saloon; }
            set
            {
                if (value != _saloon)
                {
                    _saloon = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        ///Cantidad maxima de pax permitidos en esa mesa
        /// </summary>
        private short _maxPaxs;
        [PersistentColumn("MESA_PAXS")]
        public short MaxPaxs
        {
            get { return _maxPaxs; }
            set
            {
                if (value != _maxPaxs)
                {
                    _maxPaxs = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        ///Tipo de Mesa (Standard,Bar,Reserved)
        /// </summary>
        private EPosMesaType _type;
        [PersistentColumn("MESA_TYPE")]
        public EPosMesaType Type
        {
            get { return _type; }
            set
            {
                if (value != _type)
                {
                    _type = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        ///Row
        /// </summary>
        private short? _row;
        [PersistentColumn("MESA_ROW")]
        public short? Row
        {
            get { return _row; }
            set
            {
                if (value != _row)
                {
                    _row = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        ///Column
        /// </summary>
        private short? _column;
        [PersistentColumn("MESA_COLU")]
        public short? Column
        {
            get { return _column; }
            set
            {
                if (value != _column)
                {
                    _column = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        ///Width
        /// </summary>
        private short? _width;
        [PersistentColumn("MESA_WIDT")]
        public short? Width
        {
            get { return _width; }
            set
            {
                if (value != _width)
                {
                    _width = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        ///Height
        /// </summary>
        private short? _height;
        [PersistentColumn("MESA_HEIG")]
        public short? Height
        {
            get { return _height; }
            set
            {
                if (value != _height)
                {
                    _height = value;
                    IsDirty = true;
                }
            }
        }

        #endregion
    }
}
