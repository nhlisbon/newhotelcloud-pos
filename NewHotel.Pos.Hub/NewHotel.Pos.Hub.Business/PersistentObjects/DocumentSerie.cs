﻿using System;
using NewHotel.Contracts;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects
{
    /// <summary>
    /// TNHT_SEDO
    /// </summary>
    [PersistentTable("TNHT_SEDO", "SEDO_PK")]
    public class DocumentSerie : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        public DocumentSerie(IDatabaseManager manager)
            : base(manager) { }

        public DocumentSerie(IDatabaseManager manager, Guid id)
            : this(manager)
        {
            this.LoadObject(id);
        }

        public DocumentSerie(IDatabaseManager manager, Guid id, bool lockRecord)
            : this(manager)
        {
            ForUpdate = lockRecord;
            this.LoadObject(id);
        }

        public override void Initialize()
        {
            NextNumber = 1;
            SerieStatus = SerieStatus.Future;
        }

        #endregion

        [ReflectionExclude]
        public bool CanAutocreate
        {
            get { return PrefixNumber.HasValue; }
        }

        public string SerieName
        {
            get
            {
                return (PrefixOrder == SerieOrder.TextPlusNumber)
                    ? (PrefixText + ((PrefixNumber.HasValue) ? PrefixNumber.ToString() : string.Empty))
                    : (((PrefixNumber.HasValue) ? PrefixNumber.ToString() : string.Empty) + PrefixText);
            }
        }
        
        #region Persistent Properties

        /// <summary>
        /// column=HOTE_PK"
        /// Hotel/Instalación asociada 
        /// </summary>
        private Guid _installationId;
        [PersistentColumn("HOTE_PK")]
        public Guid InstallationId
        {
            get { return _installationId; }
            set
            {
                if (value != _installationId)
                {
                    _installationId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// SEDO_PTEX, SEDO_PNUM, SEDO_ORDE
        /// prefijo texto, prefijo numerico, orden
        /// </summary>
        private string _prefixText;
        [PersistentColumn("SEDO_PTEX", Nullable = true)]
        public string PrefixText
        {
            get { return _prefixText; }
            set
            {
                if (value != _prefixText)
                {
                    _prefixText = value;
                    IsDirty = true;
                }
            }
        }
        
        private long? _prefixNumber;
        [PersistentColumn("SEDO_PNUM", Nullable = true)]
        public long? PrefixNumber
        {
            get { return _prefixNumber; }
            set
            {
                if (value != _prefixNumber)
                {
                    _prefixNumber = value;
                    IsDirty = true;
                }
            }
        }
        
        private SerieOrder _prefixOrder;
        [PersistentColumn("SEDO_ORDE")]
        public SerieOrder PrefixOrder
        {
            get { return _prefixOrder; }
            set
            {
                if (value != _prefixOrder)
                {
                    _prefixOrder = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// TIDO_PK
        /// Código tipo documento 
        /// ref. tnht_tido(tido_pk)
        /// </summary>
        private POSDocumentType _docTypeId;
        [PersistentColumn("TIDO_PK")]
        public POSDocumentType DocTypeId
        {
            get { return _docTypeId; }
            set
            {
                if (value != _docTypeId)
                {
                    _docTypeId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        ///Punto de Venta
        /// </summary>
        private Guid? _stand;
        [PersistentColumn("IPOS_PK")]
        public Guid? Stand
        {
            get { return _stand; }
            set
            {
                if (value != _stand)
                {
                    _stand = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// CAJA_PK
        /// </summary>
        private Guid? _caja;
        [PersistentColumn("CAJA_PK")]
        public Guid? Caja
        {
            get { return _caja; }
            set
            {
                if (value != _caja)
                {
                    _caja = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// SEDO_NUFA
        /// proximo numero documento
        /// </summary>
        private long _nextNumber;
        [PersistentColumn("SEDO_NUFA")]
        public long NextNumber
        {
            get { return _nextNumber; }
            set
            {
                if (value != _nextNumber)
                {
                    _nextNumber = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// SEDO_CONT
        /// proximo numero del contador auxiliar
        /// </summary>
        private long _nextAuxiliarCode;
        [PersistentColumn("SEDO_CONT")]
        public long NextAuxiliarCode
        {
            get { return _nextAuxiliarCode; }
            set
            {
                if (value != _nextAuxiliarCode)
                {
                    _nextAuxiliarCode = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// SEDO_NUFI
        /// numero final de la serie
        /// </summary>
        private long? _endNumber;
        [PersistentColumn("SEDO_NUFI", Nullable = true)]
        public long? EndNumber
        {
            get { return _endNumber; }
            set
            {
                if (value != _endNumber)
                {
                    _endNumber = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// SEDO_DAFI
        /// fecha final de la serie
        /// </summary>
        private DateTime? _endDate;
        [PersistentColumn("SEDO_DAFI", Nullable = true)]
        public DateTime? EndDate
        {
            get { return _endDate; }
            set
            {
                if (value != _endDate)
                {
                    _endDate = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// SEDO_SEAC
        /// status (activa, inactiva, futura) de la serie
        /// </summary>
        private  SerieStatus _serieStatus;
        [PersistentColumn("SEDO_SEAC")]
        public  SerieStatus SerieStatus
        {
            get { return _serieStatus; }
            set
            {
                if (value != _serieStatus)
                {
                    _serieStatus = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// SEDO_SAFT
        /// Firma Digital SAFT-PT
        /// </summary>
        private string _signature;
        [PersistentColumn("SEDO_SAFT", Nullable = true)]
        public string Signature
        {
            get { return _signature; }
            set
            {
                if (value != _signature)
                {
                    _signature = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// SEDO_COVA
        /// Código de Validación: SAFT-PT ATCUD
        /// </summary>
        private string _validationCode;
        [PersistentColumn("SEDO_COVA", Nullable = true)]
        public string ValidationCode
        {
            get { return _validationCode; }
            set
            {
                if (value != _validationCode)
                {
                    _validationCode = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// SEDO_LMOD
        /// </summary>
        [PersistentLastModifiedColumn("SEDO_LMOD")]
        public DateTime LastModified { get; set; }

        #endregion

        #region Public methods

        /// <summary>
        /// Poner la serie a inactiva
        /// </summary>
        public void CloseSerie()
        {
            SerieStatus =  SerieStatus.Inactive;
        }

        public DocumentSerie Clone(DateTime workDate)
        {
            var newSerie = new DocumentSerie(Manager);
            newSerie.SetValues(this.GetValues());

            newSerie.Id = Guid.NewGuid();
            newSerie.PrefixText = PrefixText;
            newSerie.PrefixNumber = PrefixNumber + 1;
            newSerie.PrefixOrder = PrefixOrder;
            newSerie.NextNumber = 1;
            newSerie.NextAuxiliarCode = 1;

            if (EndNumber.HasValue)
                newSerie.EndNumber = EndNumber.Value;
            else if (EndDate.HasValue)
            {
                var date = new DateTime(workDate.Year, EndDate.Value.Month, EndDate.Value.Day);
                newSerie.EndDate = date < workDate ? date.AddYears(1) : date;
            }

            newSerie.SerieStatus = SerieStatus.Future;
            return newSerie;
        }

        /// <summary>
        /// Retorna el próximo nº documento si es posible y actualiza la serie
        /// 
        /// </summary>
        public long? NextDocNumber(bool increment, DateTime workDate)
        {
            if (SerieStatus ==  SerieStatus.Active)
            {
                if (IsExhausted(workDate))
                    SerieStatus = SerieStatus.Inactive;
                else
                {
                    if (increment)
                    {
                        NextNumber++;
                        return NextNumber - 1;
                    }
                    return NextNumber;
                }
            }

            return null;
        }

        public long? NextDocAuxiliarCode(bool increment, DateTime workDate)
        {
            if (SerieStatus == SerieStatus.Active)
            {
                if (IsExhausted(workDate))
                    SerieStatus = SerieStatus.Inactive;
                else
                {
                    if (increment)
                    {
                        NextAuxiliarCode++;
                        return NextAuxiliarCode - 1;
                    }
                    else
                        return NextAuxiliarCode;
                }
            }

            return null;
        }

        /// <summary>
        /// Determina si una serie puede ser auto-generada
        /// </summary>
        public bool IsExhausted(DateTime workDate)
        {
            if (EndNumber.HasValue)
                return (NextNumber > EndNumber.Value);
            else if (EndDate.HasValue)
                return workDate > EndDate.Value;

            return false;
        }

        #endregion
    }
}
