﻿using NewHotel.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.PersistentObjects
{
    [PersistentTable("TNHT_INTE", "INTE_PK")]
    public class IntegrationModel : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        public IntegrationModel(IDatabaseManager manager)
            : base(manager)
        {
        }

        public IntegrationModel(IDatabaseManager manager, object id)
            : base(manager, id)
        {
        }

        #endregion

        #region Persistent Properties

        private string _name;
        /// <summary>
        /// column="INTE_NAME"
        /// </summary>
        public string Name
        {
            get => _name;
            set => Set(ref _name, value, nameof(Name));
        }

        private string _type;
        /// <summary>
        /// column="INTE_TYPE"
        /// </summary>
        public string Type
        {
            get => _type;
            set => Set(ref _type, value, nameof(Type));
        }

        private string _config;
        /// <summary>
        /// column="INTE_CONFIG"
        /// This is a JSON string that contains the configuration of the integration
        /// </summary>
        public string Config
        {
            get => _config;
            set => Set(ref _config, value, nameof(Config));
        }

        private bool _active;
        /// <summary>
        /// column="INTE_ACTI"
        /// </summary>
        public bool Active
        {
            get => _active;
            set => Set(ref _active, value, nameof(Active));
        }

        private DateTime _lastModification;
        /// <summary>
        /// column="INTE_LMOD"
        /// </summary>
        public DateTime LastModification
        {
            get => _lastModification;
            set => Set(ref _lastModification, value, nameof(LastModification));
        }

        #endregion

    }
}
