﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects
{
    /// <summary>
    /// table="TNHT_DIST"
    /// Distritos/Regiones
    /// </summary>
    [PersistentTable("TNHT_DIST", "DIST_PK")]
    public class Region : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        /// <summary>
        /// Default constructor
        /// </summary>
        public Region(IDatabaseManager manager)
            : base(manager)
        {
        }

        public Region(IDatabaseManager manager, object id)
            : this(manager)
        {
            this.LoadObject(id);
        }

        #endregion
        #region Public Properties

        /// <summary>
        /// NACI_PK
        /// Pais asociado
        /// </summary>
        private string _countryId;
        [PersistentColumn("NACI_PK")]
        public string CountryId
        {
            get { return _countryId; }
            set
            {
                if (value != _countryId)
                {
                    _countryId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// DIST_DESC
        /// Descripción del distrito/región
        /// </summary>
        private string _description;
        [PersistentColumn("DIST_DESC")]
        public string Description
        {
            get { return _description; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// COMU_PK
        /// Zona País a la que pertence
        /// </summary>
        private Guid? _countryZoneId;
        [PersistentColumn("COMU_PK", Nullable = true)]
        public Guid? CountryZoneId
        {
            get { return _countryZoneId; }
            set
            {
                if (value != _countryZoneId)
                {
                    _countryZoneId = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// DIST_CAUX
        /// Código libre
        /// </summary>
        private string _freeCode;
        [PersistentColumn("DIST_CAUX")]
        public string FreeCode
        {
            get { return _freeCode; }
            set
            {
                if (value != _freeCode)
                {
                    _freeCode = value;
                    IsDirty = true;
                }
            }
        }

        /// <summary>
        /// DIST_LMOD
        /// </summary>
        [PersistentLastModifiedColumn("DIST_LMOD")]
        public DateTime LastModified { get; set; }

        #endregion
    }
}