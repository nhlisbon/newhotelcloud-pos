﻿using NewHotel.Core;
using NewHotel.Pos.Hub.Contracts.Common.Records.Integrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.PersistentObjects
{
    [PersistentTable("TNHT_ARIN", "ARIN_PK")]
    public class ProductIntegrationReference : BasePersistentFullOperations
    {
        #region Constructor + Initialize

        public ProductIntegrationReference(IDatabaseManager manager)
            : base(manager)
        {
        }

        public ProductIntegrationReference(IDatabaseManager manager, object id)
            : base(manager, id)
        {
        }

        #endregion

        #region Persistent Properties

        private Guid _productId;
        /// <summary>
        /// column="ARST_PK"
        /// Id of the product in our system
        /// </summary>
        [PersistentColumn("ARST_PK", Nullable = false)]
        public Guid ProductId
        {
            get => _productId;
            set => Set(ref _productId, value, nameof(ProductId));
        }

        private Guid _integrationId;
        /// <summary>
        /// The integration that the product is associated with
        /// </summary>
        [PersistentColumn("INTE_PK", Nullable = false)]
        public Guid IntegrationId
        {
            get => _integrationId;
            set => Set(ref _integrationId, value, nameof(IntegrationId));
        }

        private string _productCode;
        /// <summary>
        /// The code of the product in the external system
        /// </summary>
        [PersistentColumn("ARIN_CODE", Nullable = false, StringSize = 50)]
        public string ProductCode
        {
            get => _productCode;
            set => Set(ref _productCode, value, nameof(ProductCode));
        }

        private DateTime _lastModified;
        /// <summary>
        /// Timestamp of the creation of the reference
        /// </summary>
        [PersistentColumn("ARIN_LMOD", Nullable = false)]
        public DateTime LastModified
        {
            get => _lastModified;
            set => Set(ref _lastModified, value, nameof(LastModified));
        }

        #endregion
    }
}
