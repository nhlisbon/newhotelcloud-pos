﻿using System;
using NewHotel.Core;

namespace NewHotel.Pos.Hub.Business.PersistentObjects
{
	/// <summary>
	/// TNHT_TACR
	/// </summary>
	[PersistentTable("TNHT_TACR", "TACR_PK")]
	public class CreditCard : BasePersistentFullOperations
	{
		#region Constructor + Initialize

		public CreditCard(IDatabaseManager manager)
			: base(manager)
		{
		}

		public CreditCard(IDatabaseManager manager, Guid id)
			: this(manager)
		{
			this.LoadObject(id);
		}

		#endregion

		#region Persistent Properties

		/// <summary>
		/// CACR_PK
		/// Código Tipo Tarjeta Crédito 
		/// </summary>
		private Guid _creditCardTypeId;
		[PersistentColumn("CACR_PK")]
		public Guid CreditCardTypeId
		{
			get { return _creditCardTypeId; }
			set
			{
				if (value != _creditCardTypeId)
				{
					_creditCardTypeId = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// TACR_IDEN
		/// Nº tarjeta credito
		/// </summary>
		private string _creditCardNumber;
		[PersistentColumn("TACR_IDEN")]
		public string CreditCardNumber
		{
			get { return _creditCardNumber; }
			set
			{
				if (value != _creditCardNumber)
				{
					_creditCardNumber = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// CACR_VMES
		/// Mes Validación
		/// </summary>
		private short? _validationMonth;
		[PersistentColumn("CACR_VMES")]
		public short? ValidationMonth
		{
			get { return _validationMonth; }
			set
			{
				if (value != _validationMonth)
				{
					_validationMonth = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// CACR_VANO
		/// Año Validación
		/// </summary>
		private short? _validationYear;
		[PersistentColumn("CACR_VANO")]
		public short? ValidationYear
		{
			get { return _validationYear; }
			set
			{
				if (value != _validationYear)
				{
					_validationYear = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// CACR_CODE
		/// Código Validación
		/// </summary>
		private string _validationCode;
		[PersistentColumn("CACR_CODE", Nullable = true)]
		public string ValidationCode
		{
			get { return _validationCode; }
			set
			{
				if (value != _validationCode)
				{
					_validationCode = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// TACR_NSU
		/// Transaction Number (NSU)
		/// </summary>
		private string _transactionNumber;
		[PersistentColumn("TACR_NSU", Nullable = true)]
		public string TransactionNumber
		{
			get { return _transactionNumber; }
			set
			{
				if (value != _transactionNumber)
				{
					_transactionNumber = value;
					IsDirty = true;
				}
			}
		}

		/// <summary>
		/// TACR_AUTH
		/// Authorization Code
		/// </summary>
		private string _authorizationCode;
		[PersistentColumn("TACR_AUTH", Nullable = true)]
		public string AuthorizationCode
		{
			get { return _authorizationCode; }
			set
			{
				if (value != _authorizationCode)
				{
					_authorizationCode = value;
					IsDirty = true;
				}
			}
		}

		#endregion

		public override bool SaveObject()
		{
			if (string.IsNullOrEmpty(CreditCardNumber))
				CreditCardNumber = "0000000000000000";

			return base.SaveObject();
		}
	}
}
