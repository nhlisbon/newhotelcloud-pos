﻿using Microsoft.Extensions.Options;
using NewHotel.Business;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.Payments.Api;
using NewHotel.Payments.Api.Models;
using NewHotel.Payments.Core.Definitions;
using NewHotel.Payments.Core.Values;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Business.PersistentObjects;
using NewHotel.Pos.Hub.Contracts.PaySystem;
using NewHotel.Pos.Hub.Model.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.Business
{
	public class PaySystemConfig
	{
		public string Url { get; set; }
		public string FunctionKey { get; set; }
	}

	public class PaySystemBusiness : BaseBusiness, IPaySystemBusiness
	{
		private readonly IPosSessionContext sessionContext;
		private readonly ISettingBusiness settingsBussines;
		private readonly IOptions<PaySystemConfig> options;
		private Payments.Api.PaySystemApiClient apiClient;
		private NewHotel.Contracts.Pos.Records.POSGeneralSettingsRecord generalSettings;

		public PaySystemBusiness(
			IDatabaseManager manager,
			IPosSessionContext sessionContext,
			ISettingBusiness settingsBussines,
			IOptions<PaySystemConfig> options) : base(manager)
		{
			//IOptions

			this.sessionContext = sessionContext;
			this.settingsBussines = settingsBussines;
			this.options = options;
		}

		NewHotel.Contracts.Pos.Records.POSGeneralSettingsRecord GeneralSettings => generalSettings ??= settingsBussines.GetGeneralSettings(sessionContext.InstallationId);

		Payments.Api.PaySystemApiClient ApiClient => apiClient ??= CreateClient();


		Payments.Api.PaySystemApiClient CreateClient()
		{
			return new(new Payments.Api.PaySystemApiClientConfig
			{
				InstallationId = GeneralSettings.PaySystemInstallationId.Value,
				Url = options.Value.Url,
				FunctionKey = options.Value.FunctionKey,
				//Url = "http://localhost:7073",
			});
		}

		ExternalPaymentEntry CreateNewExternalPaymentEntry(Contracts.PaySystem.ActionRequestType type, Guid ticketId, Guid paymentMethodId, Guid originId)
		{
			using (Manager.OpenSession())
			{
				return new ExternalPaymentEntry(Manager)
				{
					Ticket = ticketId,
					Type = type,
					PaymentMethodId = paymentMethodId,
					OriginId = originId,
					WorkDate = sessionContext.WorkDate,
					Date = DateTime.Now,
					Status = PaymentOperationStatus.Pending,
					// Body = new RequestPaymentBody(),
				};
			}
		}

		static HashSet<string> SystemsThatMustBeProcessedLocally = ["SITEF"];

		public async Task<ValidationResult<ExternalPaymentResponseContract>> RequestPayment(ExternalPaymentRequestContract request, CancellationToken cancellation)
		{
			var result = new ValidationResult<ExternalPaymentResponseContract>();
			ValidationResult<Model.PaymentMethodsRecord> paymethodResult = null;

			try
			{
				using var _ = Manager.OpenSession();

				if (result
					.Validate(() => paymethodResult = settingsBussines.GetPaymentMethod(sessionContext.InstallationId, request.PaymentMethodId, sessionContext.LanguageId))
					.Validate(() => paymethodResult.Result.PaySystemOriginId.HasValue, 999, "Not a valid method for external payments")
					.HasErrors)
					return result;

				Model.PaymentMethodsRecord paymentMethod = paymethodResult.Result;

				Payments.Api.PayApiFunctionResult<PayOriginInfo[]> originResult = await ApiClient.ListOrigins(paymethodResult.Result.PaySystemOriginId.Value);

				if (result
					.Apply(originResult)
					.Validate(() => originResult.Value?.Any() ?? false, 999, "No valid origins found")
					.HasErrors)
					return result;

				PayOriginInfo payOrigin = originResult.Value.First();


				ExternalPaymentEntry entry = CreateNewExternalPaymentEntry(
					Contracts.PaySystem.ActionRequestType.AuthorizeAndCapture,
					request.TicketId,
					paymentMethod.Id,
					payOrigin.Id);
				entry.Body.ToString();
				PayExtraInfo extraInfo = request.ToPayExtraInfo(entry);
				entry.Amount = extraInfo.Value;
				entry.TerminalId = request.PaySystemTerminalId;
				entry.SaveObject();
				entry = Manager.LoadExternalPaymentEntry((Guid)entry.Id);

				try
				{

					var newTransaction = new NewTransactionModel
					{
						RequestType = (Payments.Core.Definitions.ActionRequestType)entry.Type,
						UserId = sessionContext.UserId,
						UserCode = sessionContext.UserName,
						RequestId = (Guid)entry.Id,
						OriginId = entry.OriginId,
						PayExtraInfo = extraInfo,
						CanProcessDevice = SystemsThatMustBeProcessedLocally.Contains(payOrigin.SystemType),
						TerminalId = entry.TerminalId,
					};

					if (newTransaction.CanProcessDevice && request.PaySystemTerminalId.HasValue)
					{
						Payments.Api.PayApiFunctionResult<PaymentTransactionResultModel> syncResult = await ApiClient.NewSyncTransaction(
							newTransaction, cancellation);

						if (syncResult.IsSuccess)
						{
							PaymentTransactionModel transaction = syncResult.Value.Transactions.First();
							entry
								.UpdateFromPayment(transaction)
								.UpdateFromDevice(syncResult.Value.DeviceMessage)
								;
							string payload = syncResult.Value.DeviceMessage != null ? PaySystemApiClientSupport.ToJson(syncResult.Value.DeviceMessage?.Payload) : null;
							result.Result = new ExternalPaymentResponseContract
							{
								Id = entry.Id,
								OperationId = transaction.OperationId,
								RequestId = (Guid)entry.Id,
								TicketId = request.TicketId,
								Value = request.Values.Value,
								CurrencyCode = request.Values.CurrencyCode,
								SystemType = payOrigin.SystemType,
								DeviceRequest = payload,
							};
						}
						else
						{
							entry.Status = PaymentOperationStatus.Cancelled;
							result.AddError($"[{syncResult.Error?.Code}] {syncResult.Error?.Message ?? "Payment unknown failure"}");
						}
					}
					else
					{
						Payments.Api.PayApiFunctionResult<NewTransactionResultModel> callResult = await ApiClient.NewTransaction(
								newTransaction, cancellation);

						if (callResult.IsSuccess)
						{
							entry.OperationId = callResult.Value.OperationId;
							entry.Amount = request.Values.Value;
							result.Result = new ExternalPaymentResponseContract
							{
								Id = entry.Id,
								OperationId = callResult.Value.OperationId,
								RequestId = (Guid)entry.Id,
								TicketId = request.TicketId,
								Value = request.Values.Value,
								CurrencyCode = request.Values.CurrencyCode,
								SystemType = payOrigin.SystemType,
							};
						}
						else
						{
							entry.Status = PaymentOperationStatus.Cancelled;
							result.AddError($"[{callResult.Error?.Code}] {callResult.Error?.Message ?? "Payment unknown failure"}");
						}
					}

					/* */
					/* */
					/*
					Payments.Api.PayApiFunctionResult<PaymentTransactionResultModel> syncResult = await ApiClient.NewSyncTransaction(
						new Payments.Api.Models.NewTransactionModel
						{
							RequestType = (Payments.Core.Definitions.ActionRequestType)entry.Type,
							UserId = sessionContext.UserId,
							UserCode = sessionContext.UserName,
							RequestId = (Guid)entry.Id,
							OriginId = paymethodResult.Result.PaySystemOriginId.Value,
							PayExtraInfo = extraInfo,
							CanProcessDevice = !request.PaySystemTerminalId.HasValue,
							TerminalId = request.PaySystemTerminalId,
						});

					if (syncResult.IsSuccess)
					{
						PaymentTransactionModel transaction = syncResult.Value.Transactions.First();

						entry.OperationId = transaction.OperationId;
						result.Result = new ExternalPaymentResponseContract
						{
							OperationId = transaction.OperationId,
							RequestId = (Guid)entry.Id,
							TicketId = request.TicketId,
							Value = request.Values.Value,
							CurrencyCode = request.Values.CurrencyCode,
						};
					}
					else
					{
						entry.Status = PaymentOperationStatus.Cancelled;
						result.AddError($"[{syncResult.Error?.Code}] {syncResult.Error?.Message ?? "Payment unknown failure"}");
					}
					*/
					entry.SaveObject();
				}
				catch (Exception ex)
				{
					result.AddException(ex);
					entry.Status = PaymentOperationStatus.Cancelled;
					entry.SaveObject();
				}
			}
			catch (Exception mainEx)
			{
				result.AddException(mainEx);
			}

			return result;
		}

		public async Task<ValidationResult<ExternalPaymentEntryContract>> UpdateDevicePayment(ExternalPaymentDeviceCompleteRequest request, CancellationToken cancellation)
		{
			ValidationResult<ExternalPaymentEntryContract> result = [];
			//Payments.Device.Messages.DeviceResponseModel
			try
			{
				using var _ = Manager.OpenSession();
				ExternalPaymentEntry entry = Manager.LoadExternalPaymentEntry(request.EntryId);

				Payments.Device.Messages.DeviceResponseModel model = new()
				{
					Id = entry.Body.DeviceMessage.Id,
					OperationId = entry.OperationId.Value,
					ResponseAction = entry.Body.DeviceMessage.ResponseAction,
					Payload = request.DeviceActionResult,
					Interactive = false,
					RequestId = entry.Body.PaymentTransaction.RequestId,
				};

				PayApiFunctionResult<PaymentTransactionResultModel> updateResult = await ApiClient.UpdateFromDevice(model, cancellation);
				if (result.Apply(updateResult).HasErrors)
				{
					entry.Status = PaymentOperationStatus.Cancelled;
					entry.SaveObject();
					return result;
				}

				result = UpdateEntryFromTransactionResult(result, entry, updateResult.Value);
			}
			catch (Exception ex)
			{
				result.AddException(ex);
			}

			return result;
		}

		public async Task<ValidationResult<ExternalPaymentEntryContract>> UpdatePendingPayment(Guid entryId, CancellationToken cancellation)
		{
			ValidationResult<ExternalPaymentEntryContract> result = [];

			try
			{
				using var _ = Manager.OpenSession();
				ExternalPaymentEntry entry = Manager.LoadExternalPaymentEntry(entryId);

				if (result
					.Validate(() => entry.OperationId.HasValue, 999, "Invalid state. Operation identifier is missing")
					.HasErrors)
					return result;

				if (entry.Status != PaymentOperationStatus.Pending)
					return result.Set(entry.ToExternalPaymentEntryContract());

				PayApiFunctionResult<PaymentTransactionResultModel> callResult = await ApiClient.GetTransaction(entry.OperationId.Value, cancellation);

				if (result.Apply(callResult).HasErrors)
					return result;

				return UpdateEntryFromTransactionResult(result, entry, callResult.Value);
			}
			catch (Exception ex)
			{
				return result.AddException(ex);
			}
		}

		static ValidationResult<ExternalPaymentEntryContract> UpdateEntryFromTransactionResult(
			ValidationResult<ExternalPaymentEntryContract> result,
			ExternalPaymentEntry entry,
			PaymentTransactionResultModel resultModel)
		{
			foreach (var payment in resultModel.Transactions)
			{
				entry.UpdateFromPayment(payment);
			}

			entry.SaveObject();

			return result.Set(entry.ToExternalPaymentEntryContract());
		}

		public ValidationResult<TicketExternalPayments> GetTicketExternalPayments(Guid ticketId)
		{
			//Query query = new ExternalPaymentsForTicketQuery(Manager);
			//query.Parameters.Add("TICKET_ID", ticketId);
			//query.ExecuteList();

			List<ExternalPaymentEntry> entries = [];

			using var _ = Manager.OpenSession();
			using (var command = Manager.CreateCommand("ExternalPaymentsForTicketQuery"))
			{
				var type = typeof(ExternalPaymentEntry);
				var query = Manager.GetLoadCommandText(type);
				//query = query.Clone();
				query.ClearWhere();
				query.AppendWhere("TNHT_MEXT.VEND_PK = :VEND_PK{1}");
				command.CommandText = string.Format(query.ToString(), string.Empty, string.Empty);
				Manager.CreateParameter(command, "VEND_PK", ticketId);
				using (var reader = Manager.ExecuteReader(command))
				{
					var data = Manager.GetQueryReader(reader);
					while (data.Read())
					{
						var entry = new ExternalPaymentEntry(Manager);
						entry.LoadObjectFromDB(data);
						if (entry.Status == PaymentOperationStatus.Completed)
							entries.Add(entry);
					}
				}
			}

			var result = new TicketExternalPayments
			{
				TicketId = ticketId,
				Entries = entries.Select(e => e.ToExternalPaymentEntryModel()).ToArray(),
			};

			return new ValidationResult<TicketExternalPayments>(result);
		}

		public async Task<ValidationResult<RequestTerminalsContract>> GetPayTerminals(Guid? originId)
		{
			ValidationResult<RequestTerminalsContract> result = [];
			Payments.Api.PayApiFunctionResult<Payments.Core.Values.PayTerminalInfo[]> terminals = await ApiClient.ListTerminals(originId);

			if (!terminals.IsSuccess)
			{
				return result.Apply(terminals);
			}

			var items = terminals.Value.Select(t => new PayTerminalModel
			{
				Id = t.Id,
				OriginId = t.OriginId,
				Name = t.Name,
				Workstation = t.Workstation,
			}).ToArray();

			result.Result = new RequestTerminalsContract { Terminals = items };

			return result;
		}
	}

	public static class PaySystemBusinessEx
	{
		public static ExternalPaymentEntryContract ToExternalPaymentEntryContract(this ExternalPaymentEntry entry)
		{
			var result = new ExternalPaymentEntryContract
			{
				Id = entry.Id,
				RequestId = (Guid)entry.Id,
				OriginId = entry.OriginId,
				PaymentMethodId = entry.PaymentMethodId,
				ReferencedEntryId = entry.ReferencedEntryId,
				Ticket = entry.Ticket,
				TransactionId = entry.TransactionId,
				Type = entry.Type,
				Status = entry.Status,
				Date = entry.Date,
				WorkDate = entry.WorkDate,
				Amount = entry.Amount,
				TerminalId = entry.TerminalId,
				OperationId = entry.OperationId,
				LinkUrl = entry.LinkUrl,
				CardExternalType = entry.Body?.PaymentTransaction?.CardType,
				CardNumber = entry.Body?.PaymentTransaction?.CardNumber,
				CardHolder = entry.Body?.PaymentTransaction?.CardHolderName,
				Authorization = entry.Body?.PaymentTransaction?.AuthorizationCode,
				Nsu = entry.Body?.PaymentTransaction?.Nsu,
				Token = entry.Body?.PaymentTransaction?.Token,
				SysResultMessage = entry.Body?.PaymentTransaction?.SysResultMessage,
				SysResultCode = entry.Body?.PaymentTransaction?.SysResultCode
			};
			return result;
		}

		public static ExternalPaymentEntryModel ToExternalPaymentEntryModel(this ExternalPaymentEntry entry)
		{
			var result = new ExternalPaymentEntryModel
			{
				Id = (Guid)entry.Id,
				RequestId = (Guid)entry.Id,
				OriginId = entry.OriginId,
				PaymentMethodId = entry.PaymentMethodId,
				ReferencedEntryId = entry.ReferencedEntryId,
				Ticket = entry.Ticket,
				TransactionId = entry.TransactionId,
				Type = entry.Type,
				Status = entry.Status,
				Date = entry.Date,
				WorkDate = entry.WorkDate,
				Amount = entry.Amount,
				TerminalId = entry.TerminalId,
				OperationId = entry.OperationId,
				LinkUrl = entry.LinkUrl,
				CardExternalType = entry.Body?.PaymentTransaction?.CardType,
				CardNumber = entry.Body?.PaymentTransaction?.CardNumber,
				CardHolder = entry.Body?.PaymentTransaction?.CardHolderName,
				Authorization = entry.Body?.PaymentTransaction?.AuthorizationCode,
				Nsu = entry.Body?.PaymentTransaction?.Nsu,
				Token = entry.Body?.PaymentTransaction?.Token,
				SysResultMessage = entry.Body?.PaymentTransaction?.SysResultMessage,
				SysResultCode = entry.Body?.PaymentTransaction?.SysResultCode
			};
			return result;
		}

		public static PaymentOperationStatus ToPaymentOperationStatus(this PayOperationResult result)
		{
			return result switch
			{
				PayOperationResult.Approved => PaymentOperationStatus.Completed,
				PayOperationResult.AuthorizationNeeded => PaymentOperationStatus.AuthRequired,
				PayOperationResult.Declined => PaymentOperationStatus.Declined,
				PayOperationResult.Failed => PaymentOperationStatus.Cancelled,
				PayOperationResult.Unknown => PaymentOperationStatus.Pending,
				_ => PaymentOperationStatus.Pending,
			};
		}

		public static ExternalPaymentEntry LoadExternalPaymentEntry(this IDatabaseManager manager, Guid entryId)
		{
			using var _ = manager.OpenSession();
			var entry = new ExternalPaymentEntry(manager, entryId);
			entry.Body.ToString();
			return entry;
		}

		public static ExternalPaymentEntry UpdateFromPayment(this ExternalPaymentEntry entry, PaymentTransactionModel payment)
		{
			entry.Status = payment.Result.ToPaymentOperationStatus();
			entry.OperationId = payment.OperationId;
			entry.LinkUrl = payment.WebUIUrl;
			entry.TransactionId = payment.TransactionId;
			entry.Date = payment.Date != DateTime.MinValue ? payment.Date : entry.Date;
			entry.Amount = payment.Value != 0M ? payment.Value : entry.Amount;
			entry.Body.PaymentTransaction = payment;
			return entry;
		}

		public static ExternalPaymentEntry UpdateFromDevice(this ExternalPaymentEntry entry, DeviceMessageModel device)
		{
			entry.Body.DeviceMessage = device ?? entry.Body.DeviceMessage;
			return entry;
		}

		public static T Apply<T>(this T result, Payments.Api.PayApiFunctionResult functionResult)
			where T : ValidationResult
		{
			if (!functionResult.IsSuccess)
				result.AddError($"[{functionResult.Error?.Code}] {functionResult.Error?.Message ?? "Payment unknown failure"}");
			return result;
		}

		public static ValidationResult<T> AsValidationResult<T>(this Payments.Api.PayApiFunctionResult<T> value)
		{
			var result = new ValidationResult<T>(value.Value);
			result.Apply(value);
			return result;
		}

		public static PayExtraInfo ToPayExtraInfo(this ExternalPaymentRequestContract request, ExternalPaymentEntry entry)
		{
			return new PayExtraInfo
			{
				Invoice = entry.Id.ToString(),
				UserReference = request.TicketId.ToString(),
				Workstation = request.Values.Workstation,

				Value = request.Values.Value,
				Tax = request.Values.Tax,
				CurrencyCode = request.Values.CurrencyCode,
				Quota = (double)request.Values.Quota,
				Tip = request.Values.Tip,

				Reason = Payments.Core.Definitions.PayReason.NoReason, //request.PayInformation.Reason,
				AuthorizationCode = request.AuthorizationCode,

				// HotelInfo = request.PayInformation.HotelInfo,
				// Merchant = getHotelContact(),
				// Buyer = request.PayInformation.Buyer,
				// Products = request.PayInformation.Products,
				// ForTokenization = request.ForTokenization ?? false,
			};
		}
	}
}
