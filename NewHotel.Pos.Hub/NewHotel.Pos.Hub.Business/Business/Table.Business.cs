﻿using System;
using System.Collections.Generic;
using NewHotel.Business;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.DataAnnotations;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Business.PersistentObjects;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries;

namespace NewHotel.Pos.Hub.Business.Business
{
	public class TableBusiness : BaseBusiness, ITableBusiness
    {
        #region Constructors

        public TableBusiness(IDatabaseManager manager) : base(manager)
        { }

        #endregion
        #region ITableBusiness

        public ValidationContractResult LoadTable(Guid tableId)
        {
            var result = new ValidationContractResult();
            Manager.Open();
            try
            {
                var contract = new MesaContract();
                var mesa = new Table(Manager, tableId);
                contract.SetValues(mesa.GetValues());
                result.Contract = contract;
            }
            catch (Exception e)
            {
                result.AddError(e.Message);
            }
            finally
            {
                Manager.Close();
            }
            return result;
        }


        #endregion
    }
    
    public static class TableBusinessExtensions
	{
        public static Model.TableRecord GetTableRecord(this IRecord record)
		{
			var tableRecord = new Model.TableRecord
			{
				Id = record.ValueAs<Guid>("MESA_PK"),
				Description = record.ValueAs<string>("MESA_DESC"),
				MaxPaxs = record.ValueAs<short>("MESA_PAXS"),
				Type = record.ValueAs<EPosMesaType>("MESA_TYPE"),
				Height = record.ValueAs<short?>("MESA_HEIG"),
				Width = record.ValueAs<short?>("MESA_WIDT"),
				Column = record.ValueAs<short>("MESA_COLU"),
				Row = record.ValueAs<short>("MESA_ROW"),

                Reservations = [],
			};

            return tableRecord;
		}

        public static IEnumerable<Model.TableRecord> QueryTable(this IDatabaseManager manager, Action<Query> modifyQuery)
        {
			manager.Open();
			try
			{
				var query = CommonQueryFactory.GetTablesBySaloon(manager);
				modifyQuery?.Invoke(query);

				var records = query.Execute();
				foreach (IRecord record in records)
				{
					var tableRecord = record.GetTableRecord();
					yield return tableRecord;
				}
			}
			finally
			{
				manager.Close();
			}
		}
	}
}