﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewHotel.Business;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos;
using NewHotel.Contracts.Pos.Records;
using NewHotel.Core;
using NewHotel.DataAnnotations;
using NewHotel.Fiscal.Saft;
using NewHotel.Pos.Core.Ext;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Business.PersistentObjects;
using NewHotel.Pos.Hub.Business.PersistentObjects.Menu;
using NewHotel.Pos.Hub.Business.PersistentObjects.Tickets;
using NewHotel.Pos.Hub.Business.SpoolerService;
using NewHotel.Pos.Hub.Business.SqlContainer.Common;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Stands;
using NewHotel.Pos.Hub.Business.Validations;
using NewHotel.Pos.Hub.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.DTOs.Request;
using NewHotel.Pos.Hub.Contracts.Common.Records;
using NewHotel.Pos.Hub.Contracts.Common.Records.Products;
using NewHotel.Pos.Hub.Contracts.Common.Records.Tickets;
using NewHotel.Pos.Hub.Contracts.Handheld.Contracts;
using NewHotel.Pos.Hub.Contracts.Handheld.DTOs;
using NewHotel.Pos.Hub.Contracts.Handheld.DTOs.Requests;
using NewHotel.Pos.Hub.Contracts.Handheld.Records;
using NewHotel.Pos.Hub.Core;
using NewHotel.Pos.Hub.Core.Logs;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Hub.Model.Session;
using NewHotel.Pos.Hub.Model.Validator;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Localization;
using NewHotel.Pos.Notifications;
using NewHotel.Pos.Printer.Interfaces;
using NewHotel.Pos.PrinterDocument.Fiscal;
using ARGBColor = NewHotel.DataAnnotations.ARGBColor;
using Blob = NewHotel.DataAnnotations.Blob;
using DocumentSerieRecord = NewHotel.Pos.Hub.Model.DocumentSerieRecord;
using ProductRecord = NewHotel.Pos.Hub.Contracts.Common.Records.Products;
using TableLinePreparationContract = NewHotel.Contracts.TableLinePreparationContract;

namespace NewHotel.Pos.Hub.Business.Business
{

    public partial class TicketBusiness : BaseBusiness, ITicketBusiness
    {
        #region Constants

        const string IvaNormal = "NOR";
        const string IvaIntermediate = "INT";
        const string IvaReduced = "RED";
        const string IvaExempted = "ISE";
        const string IvaOther = "OUT";

        #endregion

        #region Members

        private POSGeneralSettingsRecord? _settingsRecord;
		private readonly IHubLogsFactory logsFactory;
		private readonly IPosSessionContext sessionContext;
        private readonly ITicketRepository ticketRepository;
        private readonly ISettingBusiness settingBusiness;
        private readonly IValidatorFactory _validatorFactory;
        private readonly IStocksManager _stocksManager;

        #endregion

        #region Constructor

        public TicketBusiness(
            IDatabaseManager manager,
			IHubLogsFactory logsFactory,
			IPosSessionContext sessionContext,
            ITicketRepository ticketRepository,
            ISettingBusiness settingBusiness,
            IValidatorFactory validatorFactory,
            IStocksManager stocksManager
        ) : base(manager)
        {
			this.logsFactory = logsFactory;
			this.sessionContext = sessionContext;
            this.ticketRepository = ticketRepository;
            this.settingBusiness = settingBusiness;
            _validatorFactory = validatorFactory;
            _stocksManager = stocksManager;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Hotel General Settings <br/><br/>
        /// <b>Unsafe</b>: Call only from methods that guaranty the user is authenticated
        /// </summary>
        private POSGeneralSettingsRecord GeneralSetting
        {
            get
            {
                if (_settingsRecord != null) return _settingsRecord;
                _settingsRecord = settingBusiness.GetGeneralSettings(sessionContext.InstallationId);
                return _settingsRecord;
            }
        }


        public bool UseContingencySeries { get; set; }

        public bool BlockOnlineDocExportation { get; set; }

        #endregion

        #region Print

        private void LoadSuggestions(Guid ticketId, IList<TipSuggestionContract> contracts)
        {
            var query = new BaseQuery(Manager, "Suggestions",
                "select tisu_pk, tick_pk, tisu_desc, tisu_perc, tisu_auto, tisu_onet from tnht_tisu",
                "where tick_pk = :tick_pk order by tisu_perc asc");
            query.Parameters["tick_pk"] = ticketId;

            var records = query.Execute();
            foreach (IRecord record in records)
            {
                var contract = new TipSuggestionContract();
                contract.Id = record.ValueAs<Guid>("tisu_pk");
                contract.TicketConfiguration = record.ValueAs<Guid>("tick_pk");
                contract.Description = record.ValueAs<string>("tisu_desc");
                contract.Percent = record.ValueAs<decimal>("tisu_perc");
                contract.Autogenerate = record.ValueAs<bool>("tisu_auto");
                contract.OverNet = record.ValueAs<bool>("tisu_onet");

                contracts.Add(contract);
            }
        }

        public POSTicketPrintConfigurationRecord GetTicketPrintConfiguration(Guid configurationId, long language)
        {
            var configRecord = new POSTicketPrintConfigurationRecord();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetTicketPrintConfiguration(Manager);
                query.Parameters["tick_pk"] = configurationId;
                query.Parameters["lang_pk"] = language;
                var records = query.Execute();

                foreach (IRecord record in records)
                {
                    var ticketConfiguration = new POSTicketPrintConfigurationRecord
                    {
                        Id = record.ValueAs<Guid>("TICK_PK"),
                        Description = record.ValueAs<string>("TICK_DESC"),
                        PrintTaxDetails = record.ValueAs<bool>("TICK_IVAF"),
                        PrintOpeningTime = record.ValueAs<bool>("TICK_HORA"),
                        PrintCloseDateTime = record.ValueAs<bool>("TICK_HCIE"),
                        PrintSeparators = record.ValueAs<bool>("TICK_SEPA"),
                        PrintMessage1 = record.ValueAs<string>("TICK_MSG1"),
                        PrintMessage2 = record.ValueAs<string>("TICK_MSG2"),
                        PrintSignature = record.ValueAs<bool>("TICK_SIGN"),
                        HeaderLinesEmpty = record.ValueAs<short>("TICK_LINE"),
                        MinLines = record.ValueAs<short?>("TICK_LINP"),
                        PrintTipsLines = record.ValueAs<bool>("TICK_LPRO"),
                        Align = record.ValueAs<short?>("TICK_ALIG"),
                        ShowLogo = record.ValueAs<bool>("TICK_SHLO"),
                        ColumnCaptions = record.ValueAs<bool>("TICK_COLU"),
                        SupressDiscount = record.ValueAs<bool>("TICK_DISC"),
                        SupressRecharge = record.ValueAs<bool>("TICK_RECA"),
                        PaymentInReceipts = record.ValueAs<bool>("TICK_RECV"),
                        PaymentDetailed = record.ValueAs<bool>("TICK_PAYM"),
                        PrintTips = record.ValueAs<bool>("TICK_TIPS"),
                        ShowCommentsProducts = record.ValueAs<bool>("TICK_COPR"),
                        ShowCommentsTicket = record.ValueAs<bool>("TICK_COTI"),
                        IsLarge = record.ValueAs<bool>("TICK_LARG"),
                        Logo = record.ValueAs<Blob?>("TICK_LOGO"),
                        TipSuggestion = record.ValueAs<bool>("TICK_TSUG"),
                        RoomChargeDigitalSignature = record.ValueAs<bool>("TICK_RCDS"),
                        PrintTaxDetailsPerLine = record.ValueAs<bool>("TICK_TAXD"),
                        PrintSubtotal = record.ValueAs<bool>("TICK_SUBT"),
                        SubTotalToPrint = record.ValueAs<KindSubTotal?>("TICK_TYST"),
                        Copies = record.ValueAs<short>("TICK_CCOP"),
                        FiscalDescription = record.ValueAs<string>("TICK_FISC"),
                        ShowSignatureOptions = record.ValueAs<bool>("TICK_ASGO"),
                        AlternateRoomNumber = record.ValueAs<string>("TICK_ARNU"),
                        AlternateName = record.ValueAs<string>("TICK_ANOM"),
                        AlternateSignature = record.ValueAs<string>("TICK_ASIG"),
                        PrintDiscountDescription = record.ValueAs<bool>("TICK_PDDE"),
                    };

                    if (ticketConfiguration.TipSuggestion)
                    {
                        ticketConfiguration.TipSuggestions = new List<TipSuggestionContract>();
                        LoadSuggestions(record.ValueAs<Guid>("TICK_PK"), ticketConfiguration.TipSuggestions);
                    }

                    return ticketConfiguration;
                }
            }
            finally
            {
                Manager.Close();
            }

            return configRecord;
        }

        public ExportOfficialDocumentResult GetPdfInvoiceReport(Guid ticketId)
        {
            var result = new ExportOfficialDocumentResult();

            try
            {
                var cloudBusiness = new CloudBusiness(Manager, sessionContext);
                result = cloudBusiness.GetExportOfficialDocument(ticketId);
            }
            catch (Exception ex)
            {
                logsFactory.Error(ex);
                result.AddException(ex);
            }

            return result;
        }

        #endregion

        #region Ticket Operations

        public List<POSTicketContract> GetTicketByStandSaloon(Guid hotelId, Guid cashierId, Guid standId, Guid saloonId)
        {
            var list = new List<POSTicketContract>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetTicketByStandSaloon(Manager);
                query.Parameters["hote_pk"] = hotelId;
                query.Parameters["caja_pk"] = cashierId;
                query.Parameters["ipos_pk"] = standId;
                query.Parameters["salo_pk"] = saloonId;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var result = LoadTicket(record.ValueAs<Guid>("vend_pk"));
                    if (result.IsEmpty)
                    {
                        var contract = result.Contract.As<POSTicketContract>();
                        if (contract != null)
                            list.Add(contract);
                    }
                }
            }
            finally
            {
                Manager.Close();
            }

            return list;
        }

        public List<POSTicketContract> GetTicketByStandCashier(Guid hotelId, Guid standId, Guid cashierId)
        {
            var list = new List<POSTicketContract>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetTicketByStandCashier(Manager);
                query.Parameters["hote_pk"] = hotelId;
                query.Parameters["ipos_pk"] = standId;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var result = LoadTicket(record.ValueAs<Guid>("vend_pk"));
                    if (result.IsEmpty)
                    {
                        var contract = result.Contract.As<POSTicketContract>();
                        if (contract != null)
                        {
                            contract.IsAcceptedAsTransfer = record.ValueAs<bool>("vend_acep");
                            list.Add(contract);
                        }
                    }
                }
            }
            finally
            {
                Manager.Close();
            }

            return list;
        }

        public List<TicketInfo> GetTicketInfoByStandCashier(Guid hotelId, Guid standId, Guid cashierId)
        {
            var list = new List<TicketInfo>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetTicketByStandCashier(Manager);
                query.Parameters["hote_pk"] = hotelId;
                query.Parameters["ipos_pk"] = standId;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var ticket = new TicketInfo()
                    {
                        Id = record.ValueAs<Guid>("vend_pk"),
                        IsAcceptedTransfer = record.ValueAs<bool>("vend_acep"),
                        MesaId = record.ValueAs<Guid?>("vend_mesa"),
                        Description = record.ValueAs<string>("vend_codi"),
                        Name = record.ValueAs<string>("vend_name"),
                        Number = record.ValueAs<long?>("vend_codi"),
                        DigitalMenu = record.ValueAs<bool>("vend_medi"),
                        Serie = record.ValueAs<string>("vend_seri"),
                        Opened = record.ValueAs<bool>("vend_open"),
                        Total = record.ValueAs<decimal>("vend_tota"),
                        Stand = record.ValueAs<Guid>("ipos_pk"),
                        ClosedDate = record.ValueAs<DateTime?>("vend_datf"),
                        Caja = record.ValueAs<Guid>("caja_pk"),
                        Shift = record.ValueAs<long>("turn_codi"),
                        SaloonId = record.ValueAs<Guid?>("salo_pk"),
                        MesaDescription = record.ValueAs<string>("mesa_desc"),
                        IsPersisted = true,
                    };

                    list.Add(ticket);
                }
            }
            finally
            {
                Manager.Close();
            }

            return list;
        }

        public TicketWithProducts GetTicketWithProducts(Guid ticketId)
        {
            Manager.Open();
            var list = new List<TicketProduct>();
            TicketWithProducts ticket = null;
            var language = (int)sessionContext.LanguageId != 0 ? sessionContext.LanguageId : 1033;
            try 
            {
                var query = CommonQueryFactory.GetTicketWithProducts(Manager);
                query.Parameters["vend_pk"] = ticketId;
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();
                if (records.Count == 0)
                    return ticket;

                foreach (IRecord record in records) 
                {
                    var product = new TicketProduct
                    {
                        Id = record.ValueAs<Guid>("artg_pk"),
                        Code = record.ValueAs<string>("artg_codi"),
                        Name = record.ValueAs<string>("mult_desc"),
                        Quantity = record.ValueAs<decimal?>("arve_qtds"),
                        Value = record.ValueAs<decimal?>("arve_totl")
                    };
                    list.Add(product);
    
                }
              
                
                ticket = new TicketWithProducts
                {
                    Id = records.ValueAs<Guid>("vend_pk"),
                    IsAcceptedTransfer = records.ValueAs<bool>("vend_acep"),
                    MesaId = records.ValueAs<Guid?>("mesa_pk"),
                    Description = records.ValueAs<string>("vend_codi"),
                    Name = records.ValueAs<string>("vend_name"),
                    Number = records.ValueAs<long?>("vend_codi"),
                    DigitalMenu = records.ValueAs<bool>("vend_medi"),
                    Serie = records.ValueAs<string>("vend_seri"),
                    Opened = records.ValueAs<bool>("vend_open"),
                    Total = records.ValueAs<decimal>("vend_tota"),
                    Stand = records.ValueAs<Guid>("ipos_pk"),
                    ClosedDate = records.ValueAs<DateTime?>("vend_datf"),
                    Caja = records.ValueAs<Guid>("caja_pk"),
                    Shift = records.ValueAs<long>("turn_codi"),
                    SaloonId = records.ValueAs<Guid?>("salo_pk"),
                    MesaDescription = records.ValueAs<string>("mesa_desc"),
                    IsPersisted = true,
                    Products = list.ToArray()
                };
            }
            finally
            {
                Manager.Close();
            }

            return ticket;
        }




        public List<TicketInfo> GetTicketInfoByTable(Guid tableId, Guid? standId = null, Guid? installationId = null)
        {
            var list = new List<TicketInfo>();

            Manager.Open();
            try
            {
                BaseQuery query;
                if (standId != null && installationId != null)
                {
                    // The call was made by Menu Digital
                    // TODO: Implement a better way
                    const string sb = """
                                      SELECT VEND.VEND_PK,
                                             VEND.VEND_MESA,
                                             VEND.VEND_CODI,
                                             CASE WHEN EXISTS(SELECT 1 FROM TNHT_TITR TITR WHERE TITR.VEND_ACEP = VEND.VEND_PK) THEN 1 ELSE 0 END AS VEND_ACEP
                                      FROM TNHT_VEND VEND
                                      WHERE VEND.VEND_MESA = :MESA_PK
                                        AND VEND.IPOS_PK = :IPOS_PK
                                        AND VEND.HOTE_PK = :HOTE_PK
                                        AND VEND.VEND_DATF IS NULL
                                      """;

                    query = new BaseQuery(Manager, "GetTicketInfoByTable", sb)
                    {
                        Parameters =
                        {
                            ["MESA_PK"] = tableId,
                            ["IPOS_PK"] = standId,
                            ["HOTE_PK"] = installationId
                        }
                    };
                }
                else
                {
                    const string sb = """
                                      SELECT VEND.VEND_PK,
                                             VEND.VEND_MESA,
                                             VEND.VEND_CODI,
                                             CASE WHEN EXISTS(SELECT 1 FROM TNHT_TITR TITR WHERE TITR.VEND_ACEP = VEND.VEND_PK) THEN 1 ELSE 0 END AS VEND_ACEP
                                      FROM TNHT_VEND VEND
                                      WHERE VEND.VEND_MESA = :MESA_PK
                                        AND VEND.IPOS_PK = :IPOS_PK
                                        AND VEND.CAJA_PK = :CAJA_PK
                                        AND VEND.HOTE_PK = :HOTE_PK
                                        AND VEND.VEND_DATF IS NULL
                                      """;

                    query = new BaseQuery(Manager, "GetTicketInfoByTable", sb)
                    {
                        Parameters =
                        {
                            ["MESA_PK"] = tableId,
                            ["IPOS_PK"] = sessionContext.StandId,
                            ["CAJA_PK"] = sessionContext.CashierId,
                            ["HOTE_PK"] = sessionContext.InstallationId
                        }
                    };
                }

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var ticketInfo = new TicketInfo()
                    {
                        Id = record.ValueAs<Guid>("vend_pk"),
                        IsAcceptedTransfer = record.ValueAs<bool>("vend_acep"),
                        MesaId = record.ValueAs<Guid?>("vend_mesa"),
                        Description = record.ValueAs<string>("vend_codi")
                    };

                    list.Add(ticketInfo);
                }
            }
            finally
            {
                Manager.Close();
            }

            return list;
        }

        public ValidationContractResult NewTicket(Guid standId, Guid cashierId, Guid hotelId)
        {
            Manager.Open();
            try
            {
                var ticket = ticketRepository.NewTicket();
                ticket.Stand = standId;
                ticket.Caja = cashierId;
                ticket.Hotel = hotelId;
                ticket.OpeningDate = sessionContext.WorkDate;
                ticket.OpeningTime = DateTime.Now;

                ticket.Duty = GetCurrentDuty(hotelId);

                var contract = new POSTicketContract();
                contract.SetValues(ticket.GetValues());

                return new ValidationContractResult(contract);
            }
            finally
            {
                Manager.Close();
            }
        }

        private ValidationContractResult NewTicketFromContract(POSTicketContract ticket)
        {
            var settingBusiness = new SettingBusiness(Manager);
            var settings = settingBusiness.GetGeneralSettings(sessionContext.InstallationId);

            var result = NewTicket((Guid)settings.Id, ticket.Stand, ticket.Caja, ticket.Mesa, ticket.UserId, null, null, ticket.Paxs);

            if (result.HasErrors || !ticket.SpaServiceId.HasValue) return result;
            // Do check-in if ticket has a spa service
            var cloudBusiness = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
            result.Add(cloudBusiness.CheckInSpaService(ticket.SpaServiceId.Value));

            return result;
        }

        public ValidationContractResult NewTicket(Guid hotelId, Guid standId, Guid cashierId, Guid? tableId, Guid userId,
            TablesReservationLineContract? reservationTable = null, string name = null, short? paxs = null)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var standBusiness = new StandBusiness(Manager, sessionContext, null, ticketRepository, _validatorFactory);
                    var stand = standBusiness.GetStandById(standId, sessionContext.LanguageId);

                    var currency = standBusiness.GetStandardCurrencyByStand(standId);

                    var ticket = ticketRepository.NewTicket();
                    ticket.ReservationTableId = (Guid?)reservationTable?.Id;
                    ticket.Hotel = hotelId;
                    ticket.Stand = standId;
                    ticket.Caja = cashierId;
                    ticket.TaxIncluded = GeneralSetting.TaxesIncluded;
                    ticket.TaxSchemaId = GeneralSetting.DefaultTaxSchemaId;
                    ticket.OpeningDate = sessionContext.WorkDate;
                    ticket.OpeningTime = DateTime.Now;
                    ticket.Mesa = tableId;
                    ticket.Currency = currency;
                    ticket.UserId = userId;
                    ticket.Name = name;
                    ticket.Paxs = (short?)reservationTable?.TotalPaxs ?? (paxs ?? 1);
                    ticket.BaseCurrency = stand.BaseCurrency;
                    ticket.ExchangeFactor = stand.ExchangeFactor ?? 1M;

                    // Check if stand is configured to add automatically tips, leave it served for the calculation of tips later
                    if (stand is { ApplyTipAutomatically: true, TipServiceId: not null } && stand.TipServiceId != Guid.Empty)
                    {
                        // Check if product to add tip exists
                        var tipProduct = GetProductRecordForTicketCreation(stand.TipServiceId.Value);
                        if (tipProduct != null)
                        {
                            ticket.TipPercent = stand.TipPercent;
                            ticket.TipValue = stand.TipValue;
                            ticket.ApplyTipOverNetValue = stand.ApplyTipOverNetValue;
                        }
                    }

                    var documentSeries = new DocumentSerieBusiness(Manager, sessionContext, _validatorFactory);
                    var nextAxContResult = documentSeries.GetNextAuxiliarCount(ticket.Caja, ticket.Stand, false, (long)POSDocumentType.Ticket, true);
                    result.Add(nextAxContResult);

                    if (result.IsEmpty)
                    {
                        ticket.Serie = string.Empty;
                        ticket.OpeningNumber = nextAxContResult.DocumentNumber.GetValueOrDefault();
                        ticket.Number = ticket.OpeningNumber;
                        //ticket.Process = ticket.Account.HasValue;
                        ticket.Duty = GetCurrentDuty(hotelId);

                        var settings = settingBusiness.GetGeneralSettings(sessionContext.InstallationId);

                        if (result.SaveTicket(ticket, settings.RealTimeTicketSync, _validatorFactory, nameof(NewTicket)).HasErrors)
                            return result;

                        var contract = new POSTicketContract();
                        contract.SetValues(ticket.GetValues());

                        if (ticket.ReservationTableId.HasValue)
                        {
                            contract.ReservationTable = reservationTable;

                            // Put the table reservation in check-in status. 
                            if (stand.AllowChangeTableReservationStatus && reservationTable is { IsReserved: true })
                            {
                                var cloudBusiness = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
                                var vr1 = cloudBusiness.PersistTablesReservationLine(reservationTable);
                                if (!vr1.IsEmpty)
                                    result.AddValidations(vr1);
                                if (result.IsEmpty)
                                {
                                    var vr2 = cloudBusiness.CheckInBookingTable(ticket.ReservationTableId.Value);
                                    if (!vr2.IsEmpty)
                                        result.AddValidations(vr2);
                                }
                            }
                        }

                        #region Automatic products
                        var productBusiness = new ProductBusiness(Manager, sessionContext);
                        var products = productBusiness.GetProductsByStandRate(hotelId, standId, (int)sessionContext.LanguageId, false, true);

                        foreach (var product in products)
                        {
                            var productLineContractResult = NewProductLine(contract, product, 1);
                            if (productLineContractResult.IsEmpty)
                            {
                                contract.ProductLineByTicket.Add((POSProductLineContract)productLineContractResult.Contract);
                            }

                        }
                        #endregion

                        result.Contract = contract;
                    }
                }
                catch (Exception ex)
                {
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
                finally
                {
                    Manager.EndTransaction(result);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult NewTicket(NewTicketDto request)
        {
            ValidationContractResult result;

            Manager.Open();
            try
            {
                var installationId = sessionContext.InstallationId;
                var standId = sessionContext.StandId;
                var cashierId = sessionContext.CashierId;
                var userId = sessionContext.UserId;

                result = NewTicket(installationId, standId, cashierId, request.TableId, userId);

                if (result.HasErrors) return result;

                var contract = result.Contract.As<POSTicketContract>();

                // Add basic values
                contract.Name = request.Description;
                contract.Paxs = request.Paxs ?? 1;
                contract.CurrentPaxNumber = request.Product.Seat ?? 1;
                contract.DigitalMenu = false;

                // Add client
                if (request.Client != null)
                {
                    contract.BaseEntityId = request.Client.Id;
                    contract.FiscalDocTo = request.Client.Name;
                    contract.FiscalDocFiscalNumber = request.Client.FiscalNumber;
                    contract.FiscalDocAddress = request.Client.Address;
                    contract.FiscalDocNacionality = request.Client.Country;
                    contract.FiscalDocEmail = request.Client.EmailAddress;
                    contract.ApplyAutomaticProductDiscount = request.Client.ApplyAutomaticProductDiscount;
                }

                // Add reservation
                if (request.Reservation != null)
                {
                    contract.Room = request.Reservation.Room;
                    contract.Name = request.Reservation.Guest;
                    contract.Account = request.Reservation.AccountId;
                    contract.AccountType = request.Reservation.Type.HasValue ? (CurrentAccountType) request.Reservation.Type : CurrentAccountType.Reservation;
                    contract.AccountInstallationId = sessionContext.InstallationId;
                    contract.FiscalDocTo = request.Reservation.Guest;
                    contract.AccountDescription = ConcatenateAccountDescription(new ReservationRecord
                    {
                        ReservationNumber = request.Reservation.ReservationNumber,
                        Room = request.Reservation.Room,
                        Guests = request.Reservation.Guest,
                        Company = request.Reservation.Company
                    });
                    contract.AllowRoomCredit = request.Reservation.AllowCreditPos;
                    contract.AccountBalance = request.Reservation.Balance;
                    contract.CardNumber = request.Reservation.CardNumber;
                }

                // Add Spa Service
                if (request.SpaService != null)
                {
                    var spaService = request.SpaService;
                    contract.SpaServiceId = spaService.ServiceId;

                    // Add guest
                    var client = new POSClientByPositionContract
                    {
                        Id = Guid.NewGuid(),
                        ClientName = spaService.GuestName,
                        Pax = request.Product.Seat ?? 0,
                    };

                    var clientAllergies = spaService.Allergies.Where(x => !string.IsNullOrEmpty(x));
                    if(clientAllergies.Count() > 0)
                        client.Allergies.AddRange(spaService.Allergies.Select(allergy =>
                            new POSClientByPositionAllergyContract(Guid.NewGuid(), Guid.Empty, allergy)));

                    var clientDiets = spaService.Diets.Where(x => !string.IsNullOrEmpty(x));
                    if (clientAllergies.Count() > 0)
                        client.Diets.AddRange(spaService.Diets.Select(diet =>
                            new POSClientByPositionDietContract(Guid.NewGuid(), Guid.Empty, diet)));

                    var clientUncommonAllergies = spaService.UncommonAllergies;
                    if(!string.IsNullOrEmpty(clientUncommonAllergies))
                        client.UncommonAllergies = spaService.UncommonAllergies;

                    contract.ClientsByPosition.Add(client);
                }

                // Add product
                var product = GetProductRecordForTicketCreation(request.Product.ProductId);
                if (product != null)
                {
                    if (request.Product.HasAdditionalProducts())
                    {
                        if (product.IsProductMenu())
                        {
                            foreach (var record in product.TableProducts)
                                record.Selected = request.Product.ProductIds!.Contains(record.Id);
                        }
                        else if (product.HasAccompanyingProducts())
                        {
                            product.AccompanyingProductsSelected = request.Product.ProductIds!.ToList();
                        }
                    }

                    result = AddProductToTicket(contract, product, request.Product.Quantity,
                        request.Product.ManualPrice, request.Product.ManualPriceDescription,
                        request.Product.PreparationsIds, observations: request.Product.Observations);

                    if (request.Product.SeparatorId.HasValue && !result.HasErrors)
                    {
                        contract = result.Contract.As<POSTicketContract>();
                        var productLine = contract.ProductLineByTicket.FirstOrDefault(lineContract =>
                            lineContract.Product == request.Product.ProductId);
                        if (productLine != null)
                        {
                            result = ChangeProductLinesSeparator(contract.Id.ToString().AsGuid(),
                                new[] { productLine.Id.ToString().AsGuid() }, request.Product.SeparatorId);
                        }
                    }
                }
                else result.AddError("Product not found");

                if (result.HasErrors) return result;

                if (contract.SpaServiceId.HasValue)
                {
                    // Do check-in if ticket has a spa service
                    var cloudBusiness = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
                    result.Add(cloudBusiness.CheckInSpaService(contract.SpaServiceId.Value));
                }
            }
            finally
            {
                Manager.Close();
            }
            return result;
        }

        public ValidationContractResult NewTicket(Guid productId, int quantity, decimal? manualPrice, string manualPriceDescription, Guid? tableId,
            Guid[]? selectedDetailed, Guid[]? preparations, string description, short? paxs, ClientRecord? client, bool digitalMenu, short? seat, Guid? separatorId, TypedList<POSClientByPositionContract>? seatedClients, string observations = null)
        {
            ValidationContractResult result;

            Manager.Open();
            try
            {
                var installationId = sessionContext.InstallationId;
                var standId = sessionContext.StandId;
                var cashierId = sessionContext.CashierId;
                var userId = sessionContext.UserId;

                result = NewTicket(installationId, standId, cashierId, tableId, userId);
                if (!result.HasErrors)
                {
                    var contract = result.Contract.As<POSTicketContract>();
                    contract.Name = description;
                    contract.Paxs = paxs ?? 1;
                    contract.CurrentPaxNumber = seat ?? 1;
                    contract.DigitalMenu = digitalMenu;
                    contract.ClientsByPosition = seatedClients ?? [];

                    if (client != null)
                    {
                        contract.BaseEntityId = client.Id;
                        contract.FiscalDocTo = client.Name;
                        contract.FiscalDocFiscalNumber = client.FiscalNumber;
                        contract.FiscalDocAddress = client.Address;
                        contract.FiscalDocNacionality = client.Country;
                        contract.FiscalDocEmail = client.EmailAddress;
                        contract.ApplyAutomaticProductDiscount = client.ApplyAutomaticProductDiscount;
                    }

                    // Add product
                    if (!result.HasErrors)
                    {
                        contract = result.Contract.As<POSTicketContract>();

                        var product = GetProductRecordForTicketCreation(productId);
                        if (product != null)
                        {
                            if (selectedDetailed != null && selectedDetailed.Any())
                            {
                                foreach (var record in product.TableProducts)
                                    record.Selected = selectedDetailed.Contains(record.Id);
                            }

                            result = AddProductToTicket(contract, product, quantity, manualPrice, manualPriceDescription, preparations, observations: observations);

                            if (separatorId.HasValue && !result.HasErrors)
                            {
                                contract = result.Contract.As<POSTicketContract>();
                                var productLine = contract.ProductLineByTicket.FirstOrDefault(lineContract =>
                                    lineContract.Product == productId);
                                if (productLine != null)
                                {
                                    result = ChangeProductLinesSeparator(contract.Id.ToString().AsGuid(),
                                        new[] { productLine.Id.ToString().AsGuid() }, separatorId);
                                }
                            }
                        }
                        else result.AddError("Product not found");
                    }
                }
            }
            finally
            {
                Manager.Close();
            }
            return result;
        }

        public ValidationContractResult CreateEmptyTicketToReservationTable(Guid[] tableIds, Guid reservationTableId, string name)
        {
            Manager.Open();
            try
            {
                var installationId = sessionContext.InstallationId;
                var standId = sessionContext.StandId;
                var cashierId = sessionContext.CashierId;
                var userId = sessionContext.UserId;

                var cloudBusiness = BusinessContext.GetBusiness<ICloudBusiness>(Manager);

                var result = cloudBusiness.LoadTablesReservation(reservationTableId);
                if (!result.IsEmpty)
                    return new ValidationContractResult(result);

                var reservation = result.Contract.As<TablesReservationLineContract>();
                if (reservation.IsReserved)
                {
                    reservation.Tables.Clear();
                    foreach (var tableId in tableIds)
                    {
                        var table = new PersistentObjects.Table(Manager, tableId);
                        reservation.Tables.Add(new TableReservationTableRelationContract(Guid.NewGuid(), tableId, table.MaxPaxs));
                    }
                }

                return NewTicket(installationId, standId, cashierId, tableIds[0], userId, result.Contract as TablesReservationLineContract, name);
            }
            finally
            {
                Manager.Close();
            }
        }

        public ValidationContractResult CreateEmptyTicketToPaxWaitingList(Guid tableId, Guid paxWaitingListId)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var hotelId = sessionContext.InstallationId;
                    var standId = sessionContext.StandId;
                    var cashierId = sessionContext.CashierId;
                    var utilId = sessionContext.UserId;

                    var newTicketResult = NewTicket(hotelId, standId, cashierId, tableId, utilId);
                    if (newTicketResult.IsEmpty)
                    {
                        var waitingListBusiness = new StandBusiness(Manager, sessionContext, null, ticketRepository, _validatorFactory);

                        var deleteResult = waitingListBusiness.DeletePaxWaitingList(paxWaitingListId);
                        if (deleteResult.IsEmpty)
                            result.Contract = newTicketResult.Contract;
                    }

                    Manager.CommitTransaction();
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        private ValidationContractResult NewProductLine(POSTicketContract ticket, ProductRecord.ProductRecord product, int quantity, decimal? manualPrice = null, string manualPriceDesc = null, bool isTip = false, string observations = null, bool asAccompanying = false)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var productLine = new ProductLine(Manager);
                    var contract = new POSProductLineContract();
                    contract.SetValues(productLine.GetValues());

                    contract.Ticket = (Guid)ticket.Id;
                    contract.IsAditional = false;
                    contract.Caja = ticket.Caja;
                    contract.CostValue = decimal.Zero;
                    contract.DiscountValue = decimal.Zero;
                    contract.DiscountPercent = decimal.Zero;
                    contract.FirstIvaId = Guid.Empty;
                    contract.SecondIvaId = null;
                    contract.ThirdIvaId = null;
                    contract.IsHappyHour = false;
                    contract.MainProduct = null;
                    contract.Observations = observations;
                    contract.ObservationsMarch = null;
                    contract.FirstIvaPercent = decimal.Zero;
                    contract.SecondIvaPercent = null;
                    contract.ThirdIvaPercent = null;
                    contract.Product = product.Id;
                    contract.ProductCode = product.ProductCode;
                    contract.ProductDescription = product.Description;
                    contract.ProductQtd = 1;
                    contract.Separator = product.SeparatorId;
                    contract.Stand = ticket.Stand;
                    contract.UserId = ticket.UserId;
                    contract.LastModified = DateTime.Now;
                    contract.ItemNumber = (short)ticket.ProductLineByTicket.Count;
                    contract.IsTip = isTip;
                    contract.IsLaunchFromSpa = product.IsLaunchFromSpaService;

                    var basePrice = (manualPrice ?? ((asAccompanying ? product.AccompanyingPrice : product.StandardPrice) ?? 0)) * quantity;

                    contract.ProductQtd = quantity;
                    contract.NetValue = basePrice;
                    contract.GrossValue = basePrice;
                    contract.ValueBeforeDiscount = basePrice;
                    contract.HasManualPrice = manualPrice.HasValue;
                    contract.ManualPriceDesc = !string.IsNullOrEmpty(manualPriceDesc) ? manualPriceDesc : null;

                    contract.DescIsentoIva = product.DescIsentoIva;
                    contract.CodeIsentoIva = product.CodeIsentoIva;

                    if (ticket.ApplyAutomaticProductDiscount)
                    {
                        contract.AutomaticDiscount = product.DiscountPercent.HasValue;
                        if (contract.AutomaticDiscount)
                            contract.DiscountPercent = product.DiscountPercent;
                    }

                    if (product is { ProductTableType: not null, TableProducts.Count: > 0 })
                    {
                        switch (product.ProductTableType.Value)
                        {
                            case TableType.FixedMenu:
                                {
                                    // Fill table product lines with all info
                                    foreach (var record in product.TableProducts)
                                    {
                                        var tableLine = new TableLineContract
                                        {
                                            Id = Guid.NewGuid(),
                                            MenuId = record.Id,
                                            ProductId = record.ProductId,
                                            ProductLineId = (Guid)contract.Id,
                                            Quantity = quantity * record.Quantity,
                                            SeparatorId = record.MenuSeparatorId,
                                            SeparatorOrden = record.MenuSeparatorOrder,
                                            SeparatorDescription = record.MenuSeparatorDescription,
                                            SeparatorDescriptionId = record.MenuSeparatorDescriptionId,
                                            Price = record.ProductStandardPrice ?? decimal.Zero,
                                            ProductAbbreviation = record.ProductAbbreviation,
                                            ProductDescription = record.ProductDescription,
                                            TableLinePreparations = new TypedList<TableLinePreparationContract>()
                                        };

                                        if (record.Preparations != null)
                                        {
                                            foreach (var item in record.Preparations.Where(x => x.Selected))
                                            {
                                                var preparation = new TableLinePreparationContract
                                                {
                                                    Id = Guid.NewGuid(),
                                                    LineDescription = tableLine.ProductDescription,
                                                    PreparationDescription = item.Description,
                                                    PreparationId = item.Id,
                                                    TableLineId = (Guid)tableLine.Id
                                                };
                                                tableLine.TableLinePreparations.Add(preparation);
                                            }
                                        }

                                        contract.TableLines.Add(tableLine);
                                    }

                                    break;
                                }
                            case TableType.DynamicMenu:
                                {
                                    // Fill table product lines with all info
                                    foreach (var record in product.TableProducts.Where(x => x.Selected))
                                    {
                                        var tableLine = new TableLineContract
                                        {
                                            Id = Guid.NewGuid(),
                                            MenuId = record.Id,
                                            ProductId = record.ProductId,
                                            ProductLineId = (Guid)contract.Id,
                                            Quantity = quantity * record.Quantity,
                                            SeparatorId = record.MenuSeparatorId,
                                            SeparatorOrden = record.MenuSeparatorOrder,
                                            SeparatorDescription = record.MenuSeparatorDescription,
                                            SeparatorDescriptionId = record.MenuSeparatorDescriptionId,
                                            Price = record.ProductStandardPrice ?? decimal.Zero,
                                            ProductAbbreviation = record.ProductAbbreviation,
                                            ProductDescription = record.ProductDescription
                                        };

                                        if (record.Preparations != null)
                                        {
                                            foreach (var item in record.Preparations.Where(x => x.Selected))
                                            {
                                                var preparation = new TableLinePreparationContract
                                                {
                                                    Id = Guid.NewGuid(),
                                                    LineDescription = tableLine.ProductDescription,
                                                    PreparationDescription = item.Description,
                                                    PreparationId = item.Id,
                                                    TableLineId = (Guid)tableLine.Id
                                                };
                                                tableLine.TableLinePreparations.Add(preparation);
                                            }
                                        }

                                        contract.TableLines.Add(tableLine);
                                    }

                                    break;
                                }
                            case TableType.RoundMenu:
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }

                    var taxSchemaId = ticket.TaxSchemaId ?? sessionContext.TaxSchemaId;
                    result.Add(LoadTaxesData(contract, sessionContext.Country, taxSchemaId, sessionContext.InstallationId, ticket.TaxIncluded, ticket, isTip));
                    if (result.IsEmpty)
                        result.Contract = contract;

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult PersistProductLine(POSProductLineContract contract, Guid hotelId, Guid taxSchemaId, bool included)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var productLine = new ProductLine(Manager);

                    if (contract.Id != null)
                        productLine.LoadObject(contract.Id, false);

                    TicketGeneralInformation ticketInto = Manager.GetTicketGeneralInformation(productLine.Ticket);

                    result.AddValidations(LoadTaxesData(contract, sessionContext.Country, taxSchemaId, hotelId, included, ticketInto, contract.IsTip));
                    if (result.IsEmpty)
                    {
                        productLine.ValueBeforeDiscount = contract.ValueBeforeDiscount;
                        productLine.SetValues(contract.GetValues());

                        productLine.SaveObject();
                        contract.LastModified = productLine.LastModified;

                        result.Contract = contract;
                        result.Extra = productLine.Id.ToString();

                        var settings = settingBusiness.GetGeneralSettings(sessionContext.InstallationId);

                        if (settings.RealTimeTicketSync)
                            Ticket.UpdateSync(Manager, productLine.Ticket);

                        Manager.CommitTransaction();
                    }
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult MoveTicketToTable(POSTicketContract ticket, Guid? tableId)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                #region Move ticket

                var oldTableId = ticket.Mesa;
                ticket.Mesa = tableId;
                result = PersistTicket(ticket);

                #endregion

                #region Move reservation tables

                if (result.IsEmpty)
                {
                    if (ticket.ReservationTable != null)
                    {
                        var tables = ticket.ReservationTable.Tables;
                        if (oldTableId.HasValue)
                            tables.Remove(x => x.MesaId == oldTableId);

                        var persist = true;
                        if (tableId.HasValue)
                        {
                            if (tables.All(t => t.MesaId != tableId.Value))
                            {
                                tables.Add(new TableReservationTableRelationContract
                                {
                                    Id = Guid.NewGuid(),
                                    MesaId = tableId.Value
                                });
                            }
                            else
                                persist = false;
                        }
                        else
                            tables.Clear();

                        if (persist)
                        {
                            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);

                            var persistTablesReservation = business.PersistTablesReservationLine(ticket.ReservationTable);
                            if (persistTablesReservation.IsEmpty)
                            {
                                if (persistTablesReservation.Contract is TablesReservationLineContract tablesReservationLineContract)
                                {
                                    if (result.Contract is POSTicketContract contract)
                                        contract.ReservationTable = tablesReservationLineContract;
                                }
                            }
                            else
                                result.AddValidations(persistTablesReservation);
                        }
                    }

                    BusinessContext.Publish("TicketMoved", (Guid)ticket.Id);
                }

                #endregion

                Manager.EndTransaction(result);
            }
            catch (Exception ex)
            {
                Manager.RollbackTransaction();
                logsFactory.Error(ex);
                result.AddException(ex);
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult MoveTicketToTable(Guid ticketId, Guid? tableId)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                #region Move ticket

                var loadResult = LoadTicket(ticketId);
                if (loadResult.HasErrors)
                    return loadResult;

                var ticket = loadResult.Contract.As<POSTicketContract>();

                var oldTableId = ticket.Mesa;
                ticket.Mesa = tableId;
                result = PersistTicket(ticket);

                #endregion

                #region Move reservation tables

                if (result.IsEmpty)
                {
                    if (ticket.ReservationTable != null)
                    {
                        var tables = ticket.ReservationTable.Tables;
                        if (oldTableId.HasValue)
                            tables.Remove(x => x.MesaId == oldTableId);

                        var persist = true;
                        if (tableId.HasValue)
                        {
                            if (tables.All(t => t.MesaId != tableId.Value))
                            {
                                tables.Add(new TableReservationTableRelationContract
                                {
                                    Id = Guid.NewGuid(),
                                    MesaId = tableId.Value
                                });
                            }
                            else
                                persist = false;
                        }
                        else
                            tables.Clear();

                        if (persist)
                        {
                            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);

                            var persistTablesReservation = business.PersistTablesReservationLine(ticket.ReservationTable);
                            if (persistTablesReservation.IsEmpty)
                            {
                                if (persistTablesReservation.Contract is TablesReservationLineContract tablesReservationLineContract)
                                {
                                    if (result.Contract is POSTicketContract contract)
                                        contract.ReservationTable = tablesReservationLineContract;
                                }
                            }
                            else
                                result.AddValidations(persistTablesReservation);
                        }
                    }

                    BusinessContext.Publish("TicketMoved", (Guid)ticket.Id);
                }

                #endregion

                Manager.EndTransaction(result);
            }
            catch (Exception ex)
            {
                Manager.RollbackTransaction();
                logsFactory.Error(ex);
                result.AddException(ex);
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        private ValidationContractResult LoadTicket(Guid ticketId, long languageId, Guid? hotelId = null)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                var ticket = ticketRepository.GetTicket(ticketId);
                var contract = new POSTicketContract();
                contract.SetValues(ticket.GetValues());

                if (result.IsEmpty)
                {
                    // Ignore call communication errors
                    //if (contract.ReservationTableId.HasValue) contract.ReservationTable = GetReservation(contract.ReservationTableId.Value);

                    #region Descriptions

                    var descriptions = GetDescriptions(ticketId, languageId);
                    contract.StandDescription = descriptions.ValueAs<string>("ipos_desc");
                    contract.CashierDescription = descriptions.ValueAs<string>("caja_desc");
                    contract.SaloonDescription = descriptions.ValueAs<string>("salo_desc");
                    contract.TableDescription = descriptions.ValueAs<string>("mesa_desc");
                    contract.Total = descriptions.ValueAs<decimal>("vend_tota");

                    #endregion

                    var settings = settingBusiness.GetGeneralSettings(ticket.Hotel);
                    if (settings.ApplyAutomaticProductDiscount)
                        contract.ApplyAutomaticProductDiscount = descriptions.ValueAs<bool?>("clie_deau") ?? false;

                    var preparationList = GetProductLinesPreparations(ticketId, languageId);
                    var areaList = GetProductLinesAreas(ticketId, languageId);

                    var tableProductList = GetProductLinesTableDetails(ticketId, languageId);
                    var tableProductPreparationList = GetProductLinesTablePreparations(ticketId, languageId);
                    var tableProductAreaList = GetProductTableLinesAreas(ticketId);

                    #region Product Lines

                    var defaultColor = new ARGBColor(255, 218, 173, 63, "Oro de la sierra");

                    var productLines = GetProductLines(ticketId, languageId);
                    foreach (IRecord productLine in productLines)
                    {
                        var productLineContract = new POSProductLineContract
                        {
                            Id = productLine.ValueAs<Guid>("arve_pk"),
                            Ticket = ticketId,
                            ProductQtd = productLine.ValueAs<decimal>("arve_qtds"),
                            NetValue = productLine.ValueAs<decimal>("arve_vliq"),
                            CostValue = productLine.ValueAs<decimal>("arve_cost"),
                            GrossValue = productLine.ValueAs<decimal>("arve_totl"),
                            ValueBeforeDiscount = productLine.ValueAs<decimal>("arve_vsde"),
                            DiscountValue = productLine.ValueAs<decimal>("arve_desc"),
                            DiscountPercent = productLine.ValueAs<decimal?>("arve_pdes"),
                            DiscountTypeDescription = productLine.ValueAs<string>("tide_desc"),
                            Recharge = productLines.ValueAs<decimal>("arve_reca"),
                            AnnulmentCode = productLine.ValueAs<short>("arve_anul"),
                            AnnulUserId = productLine.ValueAs<Guid?>("ANUL_UTIL"),
                            AnulAfterClosed = productLine.ValueAs<bool>("anul_paid"),
                            SendToAreaStatus = productLine.ValueAs<short>("arve_emar"),
                            SendToAreaStartTime = productLine.ValueAs<DateTime?>("arve_inma"),
                            SendToAreaEndTime = productLine.ValueAs<DateTime?>("arve_fima"),
                            Printed = productLine.ValueAs<short>("arve_impr"),
                            IsHappyHour = productLine.ValueAs<bool>("arve_hppy"),
                            IsAditional = productLine.ValueAs<bool>("arve_adic"),
                            MainProduct = productLine.ValueAs<Guid?>("arve_acom"),
                            FirstIvaId = productLine.ValueAs<Guid>("iva1_pk"),
                            FirstIvaPercent = productLine.ValueAs<decimal>("iva1_porc"),
                            FirstIvaValue = productLine.ValueAs<decimal>("iva1_valo"),
                            SecondIvaId = productLine.ValueAs<Guid?>("iva2_pk"),
                            SecondIvaPercent = productLine.ValueAs<decimal?>("iva2_porc"),
                            SecondIvaValue = productLine.ValueAs<decimal?>("iva2_valo"),
                            ThirdIvaId = productLine.ValueAs<Guid?>("iva3_pk"),
                            ThirdIvaPercent = productLine.ValueAs<decimal?>("iva3_porc"),
                            ThirdIvaValue = productLine.ValueAs<decimal?>("iva3_valo"),
                            AreaId = productLine.ValueAs<Guid?>("area_pk"),
                            //productLineContract.AreaDescription = productLine.ValueAs<string>("area_desc");
                            Separator = productLine.ValueAs<Guid?>("sepa_pk"),
                            SeparatorDescription = productLine.ValueAs<string>("sepa_desc"),
                            Product = productLine.ValueAs<Guid>("artg_pk"),
                            ProductCode = productLine.ValueAs<string>("artg_codi"),
                            ProductAbbreviation = productLine.ValueAs<string>("artg_abre"),
                            ProductDescription = productLine.ValueAs<string>("artg_desc"),
                            ManualPriceDesc = productLine.ValueAs<string>("mapr_desc"),
                            Caja = productLine.ValueAs<Guid>("caja_pk"),
                            Stand = productLine.ValueAs<Guid>("ipos_pk"),
                            DiscountTypeId = productLine.ValueAs<Guid?>("tide_pk"),
                            HasManualSeparator = productLine.ValueAs<bool>("sepa_manu"),
                            HasManualPrice = productLine.ValueAs<bool>("arve_mpri"),
                            AutomaticDiscount = productLine.ValueAs<bool>("arve_deau"),
                            IsTip = productLine.ValueAs<bool>("arve_tipl"),
                            PaxNumber = productLine.ValueAs<short?>("arve_npax"),
                            Observations = productLine.ValueAs<string>("arve_obsv"),
                            ObservationsMarch = productLine.ValueAs<string>("arve_obsm"),
                            UserId = productLine.ValueAs<Guid>("util_pk"),
                            UserDescription = productLine.ValueAs<string>("util_login"),
                            SeparatorOrder = productLine.ValueAs<short?>("sepa_orde"),
                            ProductImage = productLine.ValueAs<string>("artg_imag"),
                            ProductColor = productLine.IsNull("artg_colo")
                                ? defaultColor
                                : new ARGBColor(productLine.ValueAs<int>("artg_colo")),
                            Alcoholic = productLine.ValueAs<bool?>("artg_alco"),
                            ItemNumber = productLine.ValueAs<short>("arve_item"),
                            LastModified = productLine.ValueAs<DateTime>("arve_lmod"),
                            CodeIsentoIva = productLine.ValueAs<string>("code_isto"),
                            DescIsentoIva = productLine.ValueAs<string>("desc_isto"),
                            IsLaunchFromSpa = productLine.ValueAs<bool>("FROM_SPA"),
                        };

                        if (ticket.TaxSchemaId.HasValue)
                            result.AddValidations(LoadTaxesData(productLineContract, settings.Country,
                                ticket.TaxSchemaId.Value, ticket.Hotel, ticket.TaxIncluded, ticket, productLineContract.IsTip));

                        if (result.IsEmpty)
                        {
                            // Preparations
                            var preparations = preparationList.Where(x => x.ValueAs<Guid>("arve_pk") == (Guid)productLineContract.Id);
                            foreach (var record in preparations)
                            {
                                var preparationContract = new POSProductLinePreparationContract
                                {
                                    Id = record.ValueAs<Guid>("arvp_pk"),
                                    PreparationDescription = record.ValueAs<string>("prep_desc"),
                                    PreparationId = record.ValueAs<Guid>("prep_pk"),
                                    ProductDescription = record.ValueAs<string>("artg_desc"),
                                    ProductLineId = record.ValueAs<Guid>("arve_pk")
                                };

                                productLineContract.Preparations.Add(preparationContract);
                            }

                            {
                                // Areas
                                var areas = areaList.Where(x => x.ValueAs<Guid>("arve_pk") == (Guid)productLineContract.Id);
                                foreach (var record in areas)
                                {
                                    var areaContract = new POSProductLineAreaContract
                                    {
                                        Id = record.ValueAs<Guid>("arva_pk"),
                                        AreaId = record.ValueAs<Guid>("area_pk")
                                    };

                                    productLineContract.Areas.Add(areaContract);
                                }
                            }

                            // Table Details
                            var tableProducts = tableProductList.Where(x => x.ValueAs<Guid>("arve_pk") == (Guid)productLineContract.Id);
                            foreach (var tableLineRecord in tableProducts)
                            {
                                var tableLineContract = new TableLineContract
                                {
                                    Id = tableLineRecord.ValueAs<Guid>("tabl_pk"),
                                    MenuId = tableLineRecord.ValueAs<Guid>("ARTB_PK"),
                                    IsAdditional = tableLineRecord.ValueAs<bool>("tabl_adic"),
                                    Cost = tableLineRecord.ValueAs<decimal>("tabl_cost"),
                                    SendToAreaStatus = tableLineRecord.ValueAs<short>("tabl_emar"),
                                    Observations = tableLineRecord.ValueAs<string>("tabl_obsv"),
                                    Price = tableLineRecord.ValueAs<decimal>("tabl_prec"),
                                    Percent = tableLineRecord.ValueAs<decimal>("tabl_porc"),
                                    Printed = tableLineRecord.ValueAs<bool>("tabl_impr"),
                                    ProductId = tableLineRecord.ValueAs<Guid>("artg_pk"),
                                    ProductLineId = tableLineRecord.ValueAs<Guid>("arve_pk"),
                                    Quantity = tableLineRecord.ValueAs<decimal>("tabl_cant"),
                                    SeparatorId = tableLineRecord.ValueAs<Guid?>("SEPA_PK"),
                                    SeparatorOrden = tableLineRecord.ValueAs<short?>("SEPA_ORDE"),
                                    SeparatorDescription = tableLineRecord.ValueAs<string>("SEPA_DESC"),
                                    SeparatorDescriptionId = tableLineRecord.ValueAs<Guid>("SEPA_LITE_PK"),
                                    ProductAbbreviation = tableLineRecord.ValueAs<string>("artg_abre"),
                                    ProductDescription = tableLineRecord.ValueAs<string>("artg_desc"),
                                    AreaId = tableLineRecord.ValueAs<Guid>("area_pk"),
                                    LastModified = tableLineRecord.ValueAs<DateTime>("tabl_lmod")
                                };

                                {
                                    // Preparations Details
                                    var tableProductPreparations = tableProductPreparationList
                                        .Where(x => x.ValueAs<Guid>("tabl_pk") == (Guid)tableLineContract.Id);
                                    foreach (var preparationRecord in tableProductPreparations)
                                    {
                                        var preparationContract = new TableLinePreparationContract
                                        {
                                            Id = preparationRecord.ValueAs<Guid>("tabp_pk"),
                                            LineDescription = tableLineContract.ProductDescription,
                                            PreparationDescription = preparationRecord.ValueAs<string>("prep_desc"),
                                            PreparationId = preparationRecord.ValueAs<Guid>("prep_pk"),
                                            TableLineId = (Guid)tableLineContract.Id
                                        };

                                        tableLineContract.TableLinePreparations.Add(preparationContract);
                                    }
                                }

                                {
                                    // Areas
                                    var areas = tableProductAreaList.Where(x => x.ValueAs<Guid>("TABL_PK") == (Guid)tableLineContract.Id);
                                    foreach (var record in areas)
                                    {
                                        var areaContract = new POSProductLineAreaContract
                                        {
                                            Id = record.ValueAs<Guid>("TABA_PK"),
                                            AreaId = record.ValueAs<Guid>("AREA_PK")
                                        };

                                        tableLineContract.Areas.Add(areaContract);
                                    }
                                }

                                productLineContract.TableLines.Add(tableLineContract);
                            }

                            contract.ProductLineByTicket.Add(productLineContract);
                        }
                        else
                            break;
                    }

                    #endregion

                    if (result.IsEmpty)
                    {
                        // Cargar las lineas de Pagos
                        var listPayments = GetPaymentLines(ticketId, languageId);
                        if (!listPayments.IsEmpty)
                        {
                            do
                            {
                                if (listPayments.ValueAs<short>("pave_anul") != 1)
                                {
                                    var paymentLineContract = new POSPaymentLineContract(
                                        listPayments.ValueAs<Guid>("pave_pk"),
                                        listPayments.ValueAs<long>("tire_pk"),
                                        listPayments.ValueAs<string>("tire_desc"),
                                        listPayments.ValueAs<Guid?>("fore_pk"),
                                        listPayments.ValueAs<string>("fore_desc"),
                                        listPayments.ValueAs<Guid?>("tacr_pk"),
                                        listPayments.ValueAs<string>("tacr_iden"),
                                        listPayments.ValueAs<Guid?>("ccco_pk"),
                                        listPayments.ValueAs<Guid?>("coin_pk"),
                                        listPayments.ValueAs<string>("coin_desc"), listPayments.ValueAs<string>("pave_obsv"),
                                        listPayments.ValueAs<decimal>("pave_valo"),
                                        listPayments.ValueAs<decimal>("pave_reci"),
                                        listPayments.ValueAs<short>("pave_anul"),
                                        listPayments.ValueAs<string>("unmo_pk"),
                                        listPayments.ValueAs<decimal?>("camb_online"),
                                        listPayments.ValueAs<bool?>("camb_mult")
                                    )
                                    {
                                        AccountInstallationId = listPayments.ValueAs<Guid?>("hote_pk")
                                    };

                                    paymentLineContract.PaymentTypeAuxCode = listPayments.ValueAs<string>("fore_caux");

                                    if (listPayments.ValueAs<Guid?>("tacr_pk").HasValue)
                                    {
                                        var creditCardContract = new POSCreditCardContract();
                                        var creditCard = new CreditCard(Manager, listPayments.ValueAs<Guid>("tacr_pk"));
                                        creditCardContract.SetValues(creditCard.GetValues());
                                        paymentLineContract.CreditCardContract = creditCardContract;
                                    }

                                    var paymentMethodDesc = listPayments.ValueAs<string>("fore_desc");
                                    var internalMethdDesc = listPayments.ValueAs<string>("coin_desc");
                                    var accountDesc = listPayments.ValueAs<string>("ccco_desc");

                                    paymentLineContract.PaymentDescription = paymentMethodDesc + internalMethdDesc + accountDesc;
                                    paymentLineContract.Ticket = ticketId;
                                    paymentLineContract.PaymentReceived = listPayments.ValueAs<decimal>("pave_reci");

                                    contract.PaymentLineByTicket.Add(paymentLineContract);
                                }
                            } while (listPayments.Next());
                        }

                        // Cargar las consultas de mesa
                        var listLookupTables = GetLookupTables(ticketId);
                        var listLookupTableDetails = GetLookupTableDetails(ticketId);

                        if (!listLookupTables.IsEmpty)
                        {
                            do
                            {
                                var lookupTableContract = new LookupTableContract();
                                lookupTableContract.Caja = ticket.Caja;
                                lookupTableContract.Id = listLookupTables.ValueAs<Guid>("lktb_pk");
                                lookupTableContract.Number = listLookupTables.ValueAs<long>("lktb_codi");
                                lookupTableContract.Serie = listLookupTables.ValueAs<string>("lktb_seri");
                                lookupTableContract.Signature = listLookupTables.ValueAs<string>("lktb_sign");
                                lookupTableContract.Stand = ticket.Stand;
                                lookupTableContract.SystemDateTime = listLookupTables.ValueAs<DateTime>("lktb_stim");
                                lookupTableContract.Ticket = (Guid)ticket.Id;
                                lookupTableContract.Total = listLookupTables.ValueAs<decimal>("lktb_tota");
                                lookupTableContract.User = listLookupTables.ValueAs<Guid>("util_pk");
                                lookupTableContract.WorkDate = listLookupTables.ValueAs<DateTime>("lktb_date");
                                lookupTableContract.ValidationCode = listLookupTables.ValueAs<string>("lktb_cova");
                                lookupTableContract.QrCode = listLookupTables.ValueAs<Blob?>("lktb_qrco");
                                lookupTableContract.QrCodeData = listLookupTables.ValueAs<string>("lktb_qrdt");

                                contract.LookupTableByTicket.Add(lookupTableContract);

                                // Details
                                var lookupTableDetails = listLookupTableDetails.Where(x => x.ValueAs<Guid>("lktb_pk") == (Guid)lookupTableContract.Id);
                                foreach (IRecord record in lookupTableDetails)
                                {
                                    var detailContract = new LookupTableDetailsContract
                                    {
                                        Id = record.ValueAs<Guid>("lkde_pk"),
                                        LookupTableId = record.ValueAs<Guid>("lktb_pk"),
                                        NetPrice = record.ValueAs<decimal>("arve_vliq"),
                                        TotalPrice = record.ValueAs<decimal>("arve_totl"),
                                        ProductId = record.ValueAs<Guid>("artg_pk"),
                                        Quantity = record.ValueAs<decimal>("arve_qtds"),
                                        AnnulmentCode = record.ValueAs<short>("arve_anul"),
                                        FirstIvaId = record.ValueAs<Guid>("ivas_codi"),
                                        FirstIvaPercent = record.ValueAs<decimal>("arve_por1"),
                                        FirstIvaValue = record.ValueAs<decimal>("arve_ivas"),
                                        SecondIvaId = record.ValueAs<Guid>("ivas_cod2"),
                                        SecondIvaPercent = record.ValueAs<decimal>("arve_por2"),
                                        SecondIvaValue = record.ValueAs<decimal>("arve_iva2"),
                                        ThirdIvaId = record.ValueAs<Guid>("ivas_cod3"),
                                        ThirdIvaPercent = record.ValueAs<decimal>("arve_por3"),
                                        ThirdIvaValue = record.ValueAs<decimal>("arve_iva3")
                                    };

                                    lookupTableContract.Details.Add(detailContract);
                                }
                            } while (listLookupTables.Next());
                        }

                        // Cargar los clientes por posición
                        var listClientsByPosition = GetClientsByPosition(ticketId);
                        if (!listClientsByPosition.IsEmpty)
                        {
                            do
                            {
                                var clientByPositionContract = new POSClientByPositionContract(
                                    listClientsByPosition.ValueAs<Guid>("clve_pk"),
                                    listClientsByPosition.ValueAs<Guid>("clie_pk"),
                                    listClientsByPosition.ValueAs<string>("clie_name"),
                                    listClientsByPosition.ValueAs<string>("clie_room"),
                                    listClientsByPosition.ValueAs<short>("clie_npax"),
                                    listClientsByPosition.ValueAs<string>("clie_alle"),
                                    listClientsByPosition.ValueAs<string>("clie_seop")
                                );

                                contract.ClientsByPosition.Add(clientByPositionContract);
                            } while (listClientsByPosition.Next());
                        }

                        if (contract.ClientsByPosition.Count > 0)
                        {
                            // Cargar las preferencias
                            var dictPreferences = new Dictionary<Guid, List<POSClientByPositionPreferenceContract>>();
                            var listClientsByPositionPreferences = GetClientsByPositionPreferences(ticketId);
                            foreach (IRecord listClientsByPositionPreference in listClientsByPositionPreferences)
                            {
                                var clientByPositionId = listClientsByPositionPreference.ValueAs<Guid>("clve_pk");

                                if (!dictPreferences.TryGetValue(clientByPositionId, out List<POSClientByPositionPreferenceContract> contracts))
                                {
                                    contracts = new List<POSClientByPositionPreferenceContract>();
                                    dictPreferences.Add(clientByPositionId, contracts);
                                }

                                contracts.Add(new POSClientByPositionPreferenceContract(
                                    listClientsByPositionPreference.ValueAs<Guid>("clpr_pk"),
                                    listClientsByPositionPreference.ValueAs<Guid>("pref_pk"),
                                    listClientsByPositionPreference.ValueAs<string>("pref_desc")));
                            }

                            // Cargar las dietas
                            var dictDiets = new Dictionary<Guid, List<POSClientByPositionDietContract>>();
                            var listClientsByPositionDiets = GetClientsByPositionDiets(ticketId);
                            foreach (IRecord listClientsByPositionDiet in listClientsByPositionDiets)
                            {
                                var clientByPositionId = listClientsByPositionDiet.ValueAs<Guid>("clve_pk");

                                if (!dictDiets.TryGetValue(clientByPositionId, out List<POSClientByPositionDietContract> contracts))
                                {
                                    contracts = new List<POSClientByPositionDietContract>();
                                    dictDiets.Add(clientByPositionId, contracts);
                                }

                                contracts.Add(new POSClientByPositionDietContract(
                                    listClientsByPositionDiet.ValueAs<Guid>("cldi_pk"),
                                    listClientsByPositionDiet.ValueAs<Guid>("diet_pk"),
                                    listClientsByPositionDiet.ValueAs<string>("diet_desc")));
                            }

                            // Cargar las atenciones
                            var dictAttentions = new Dictionary<Guid, List<POSClientByPositionAttentionContract>>();
                            var listClientsByPositionAttentions = GetClientsByPositionAttentions(ticketId);
                            foreach (IRecord listClientsByPositionAttention in listClientsByPositionAttentions)
                            {
                                var clientByPositionId = listClientsByPositionAttention.ValueAs<Guid>("clve_pk");

                                if (!dictAttentions.TryGetValue(clientByPositionId, out List<POSClientByPositionAttentionContract> contracts))
                                {
                                    contracts = new List<POSClientByPositionAttentionContract>();
                                    dictAttentions.Add(clientByPositionId, contracts);
                                }

                                contracts.Add(new POSClientByPositionAttentionContract(
                                    listClientsByPositionAttention.ValueAs<Guid>("clat_pk"),
                                    listClientsByPositionAttention.ValueAs<Guid>("tiat_pk"),
                                    listClientsByPositionAttention.ValueAs<string>("tiat_desc")));
                            }

                            // Cargar las alergias
                            var dictAllergies = new Dictionary<Guid, List<POSClientByPositionAllergyContract>>();
                            var listClientsByPositionAllergies = GetClientsByPositionAllergies(ticketId);
                            foreach (IRecord listClientsByPositionAllergy in listClientsByPositionAllergies)
                            {
                                var clientByPositionId = listClientsByPositionAllergy.ValueAs<Guid>("clve_pk");

                                if (!dictAllergies.TryGetValue(clientByPositionId, out List<POSClientByPositionAllergyContract> contracts))
                                {
                                    contracts = new List<POSClientByPositionAllergyContract>();
                                    dictAllergies.Add(clientByPositionId, contracts);
                                }

                                contracts.Add(new POSClientByPositionAllergyContract(
                                    listClientsByPositionAllergy.ValueAs<Guid>("clal_pk"),
                                    listClientsByPositionAllergy.ValueAs<Guid>("alle_pk"),
                                    listClientsByPositionAllergy.ValueAs<string>("alle_desc")));
                            }

                            foreach (var clientByPosition in contract.ClientsByPosition)
                            {
                                if (dictPreferences.TryGetValue((Guid)clientByPosition.Id, out List<POSClientByPositionPreferenceContract> preferences))
                                    clientByPosition.Preferences.AddRange(preferences);

                                if (dictDiets.TryGetValue((Guid)clientByPosition.Id, out List<POSClientByPositionDietContract> diets))
                                    clientByPosition.Diets.AddRange(diets);

                                if (dictAttentions.TryGetValue((Guid)clientByPosition.Id, out List<POSClientByPositionAttentionContract> attentions))
                                    clientByPosition.Attentions.AddRange(attentions);

                                if (dictAllergies.TryGetValue((Guid)clientByPosition.Id, out List<POSClientByPositionAllergyContract> allergies))
                                    clientByPosition.Allergies.AddRange(allergies);
                            }
                        }

                        // Trasfered
                        contract.IsAcceptedAsTransfer = contract.ProductLineByTicket.Any(x => x.AnnulmentCode == ProductLineCancellationStatus.ActiveByTransfer);

                        #region Validation code (ATCUD)

                        var signType = (hotelId.HasValue ? GetGeneralSetting(hotelId.Value) : GeneralSetting).SignType;
                        switch (signType)
                        {
                            case DocumentSign.FiscalizationPortugal:
                                // Ticket
                                if (!string.IsNullOrEmpty(contract.Serie) && contract is { ClosedDate: not null, Number: not null })
                                {
                                    var validationCode = GetSerieValidationCode(contract.Serie);
                                    if (validationCode != null && validationCode != contract.ValidationCode)
                                    {
                                        contract.ValidationCode = validationCode;

                                        var buildQrCodeResult = BuildPortugalTicketQRCode(contract, GeneralSetting);
                                        if (buildQrCodeResult.IsEmpty)
                                        {
                                            // byte[] qrCodeImage = new QRCode(buildQrCodeResult.Result);
                                            //contract.QrCode = qrCodeImage;
                                            ticket.ValidationCode = contract.ValidationCode;
                                            //ticket.QrCode = contract.QrCode;
                                            ticket.FiscalDocQrCodeData = buildQrCodeResult.Result;
                                        }
                                        else
                                            result.Add(buildQrCodeResult);
                                    }
                                }

                                // Invoice / Boleta
                                if (result.IsEmpty && !string.IsNullOrEmpty(contract.FiscalDocSerie))
                                {
                                    var fiscalDocValidationCode = GetSerieValidationCode(contract.FiscalDocSerie);
                                    if (fiscalDocValidationCode != null && fiscalDocValidationCode != contract.FiscalDocValidationCode)
                                    {
                                        contract.FiscalDocValidationCode = fiscalDocValidationCode;

                                        var buildQrCodeResult = BuildPortugalInvoiceQRCode(contract, GeneralSetting);
                                        if (buildQrCodeResult.IsEmpty)
                                        {
                                            //byte[] qrCodeImage = new QRCode(buildQrCodeResult.Result);
                                            //contract.FiscalDocQrCode = qrCodeImage;
                                            ticket.FiscalDocValidationCode = contract.FiscalDocValidationCode;
                                            //ticket.FiscalDocQrCode = contract.FiscalDocQrCode;
                                            ticket.FiscalDocQrCodeData = buildQrCodeResult.Result;
                                        }
                                        else
                                            result.Add(buildQrCodeResult);
                                    }
                                }

                                // Credit Note
                                if (result.IsEmpty && !string.IsNullOrEmpty(contract.CreditNoteSerie))
                                {
                                    var creditNoteValidationCode = GetSerieValidationCode(contract.CreditNoteSerie);
                                    if (creditNoteValidationCode != null && creditNoteValidationCode != contract.CreditNoteValidationCode)
                                    {
                                        contract.CreditNoteValidationCode = creditNoteValidationCode;

                                        var buildQrCodeResult = BuildPortugalCreditNoteQRCode(contract, GeneralSetting);
                                        if (buildQrCodeResult.IsEmpty)
                                        {
                                            //byte[] qrCodeImage = new QRCode(buildQrCodeResult.Result);
                                            //contract.CreditNoteQrCode = qrCodeImage;
                                            ticket.CreditNoteValidationCode = contract.CreditNoteValidationCode;
                                            //ticket.CreditNoteQrCode = contract.CreditNoteQrCode;
                                            ticket.CreditNoteQrCodeData = buildQrCodeResult.Result;
                                        }
                                        else
                                            result.Add(buildQrCodeResult);
                                    }
                                }

                                break;
                            case DocumentSign.FiscalizationCaboVerde:
                                // Ticket
                                if (contract is { Serie: not null and not "", ClosedDate: not null, Number: not null })
                                {
                                    var validationCode = GetSerieValidationCode(contract.Serie);
                                    if (validationCode != null && validationCode != contract.ValidationCode)
                                        contract.ValidationCode = validationCode;
                                }
                                // Invoice / Boleta
                                if (contract is { FiscalDocSerie: not null and not "" })
                                {
                                    var fiscalDocValidationCode = GetSerieValidationCode(contract.FiscalDocSerie);
                                    if (fiscalDocValidationCode != null && fiscalDocValidationCode != contract.FiscalDocValidationCode)
                                        contract.FiscalDocValidationCode = fiscalDocValidationCode;
                                }
                                // Credit Note
                                if (contract is { CreditNoteSerie: not null and not "" })
                                {
                                    var creditNoteValidationCode = GetSerieValidationCode(contract.CreditNoteSerie);
                                    if (creditNoteValidationCode != null && creditNoteValidationCode != contract.CreditNoteValidationCode)
                                        contract.CreditNoteValidationCode = creditNoteValidationCode;
                                }
                                break;
                        }

                        contract.FiscalDocQrCodeText = ticket.FiscalDocQrCodeData;


                        if (result.IsEmpty && ticket.IsDirty)
                        {
                            Manager.BeginTransaction();
                            try
                            {
                                ticket.SaveObject(true);
                                contract.LastModified = ticket.LastModified;
                                Manager.CommitTransaction();
                            }
                            catch
                            {
                                Manager.RollbackTransaction();
                                throw;
                            }
                        }

                        #endregion

                        #region Load Currency Symbol
                        var hotelCurrencies = Manager.GetCurrencies(hotelId ?? sessionContext.InstallationId);
                        //var query = QueryFactory.Get<CurrencyCashierQuery>(Manager);
                        //query.Parameters["HOTE_PK"] = hotelId ?? BusinessContext.InstallationId;
                        //var hotelCurrencies = query.ExecuteList<CurrencyCashierRecord>();
                        foreach (var currency in hotelCurrencies)
                        {
                            if (currency.CurrencyId != contract.Currency) continue;
                            contract.CurrencySymbol = currency.Symbol;
                            break;
                        }

                        #endregion

                        result.Contract = contract;
                    }
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult LoadTicket(Guid ticketId)
        {
            return LoadTicket(ticketId, sessionContext.LanguageId);
        }

        private TablesReservationLineContract GetReservation(Guid reservationId)
        {
            var tableReservation = new TablesReservationLineContract();
            var cloudBusiness = BusinessContext.GetBusiness<ICloudBusiness>(Manager);

            var loadTableReservationResult = cloudBusiness.LoadTablesReservation(reservationId);
            if (loadTableReservationResult.IsEmpty)
            {
                var tableReservationLine = loadTableReservationResult.Contract.As<TablesReservationLineContract>();
                if (tableReservationLine != null) tableReservation = tableReservationLine;
            }
            else cloudBusiness.TraceComunicationErrors(loadTableReservationResult);

            return tableReservation;
        }

        public ValidationContractResult LoadTicketForEdition(Guid ticketId, bool forceUnlock)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var ticketBlockedIssue = false;
                    var ticket = ticketRepository.GetTicket(ticketId);

                    if (ticket.ClosedDate.HasValue)
                        throw new ApplicationException("Ticket Closed");

                    if (forceUnlock || !ticket.Opened)
                    {
                        ticket.OpenUserId = sessionContext.UserId;
                        ticket.Opened = true;

                        var settings = settingBusiness.GetGeneralSettings(sessionContext.InstallationId);

                        ticket.SaveObject(settings.RealTimeTicketSync);
                    }
                    else if (ticket.Opened)
                        ticketBlockedIssue = true;

                    if (!result.HasErrors)
                    {
                        result = LoadTicket(ticketId);
                        var contract = result.Contract.As<POSTicketContract>();

                        // Ignore call communication errors
                        if (contract.ReservationTableId.HasValue)
                            contract.ReservationTable = GetReservation(contract.ReservationTableId.Value);

                        contract.TicketBlockedIssue = ticketBlockedIssue;

                        BusinessContext.Publish("OpeningTicket", (Guid)contract.Id);
                    }

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult SaveTicketFromEdition(TicketIdentification ticketInfo, short? paxs = null)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var ticket = ticketRepository.GetTicket(ticketInfo.Id);
                    ticket.Opened = false;
                    ticket.OpenUserId = null;

                    if (ticketInfo.LastModified == DateTime.MinValue)
                        ticketInfo.LastModified = ticket.LastModified;

                    if (result.ValidateTicketStaleData(ticket, ticketInfo.LastModified).HasErrors)
                        return result;

                    if (paxs.HasValue) ticket.Paxs = paxs.Value;

                    var settings = settingBusiness.GetGeneralSettings(sessionContext.InstallationId);

                    if (result.SaveTicket(ticket, settings.RealTimeTicketSync, _validatorFactory, nameof(SaveTicketFromEdition)).HasErrors)
                        return result;

                    result = LoadTicket(ticketInfo.Id);

                    BusinessContext.Publish("ClosingTicket", ticketInfo.Id);
                }
                catch (Exception ex)
                {
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
                finally
                {
                    Manager.EndTransaction(result);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult SaveTicketFromEdition(Guid ticketId, short? paxs = null)
        {
            return SaveTicketFromEdition(new TicketIdentification { Id = ticketId }, paxs);
        }

        public ValidationTicketsResult AcceptTransferedTickets(List<TransferContract> contracts)
        {
            var result = new ValidationTicketsResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var stand = new Stand(Manager, sessionContext.StandId);

                    var tickets = new List<POSTicketContract>();
                    var transferLabel = LocalizationMgr.Translation("Transfer");
                    foreach (var contract in contracts)
                    {
                        var loadTicketResult = LoadTicket(contract.TicketOrig);
                        if (loadTicketResult.IsEmpty)
                        {
                            var originalTicket = loadTicketResult.Contract.As<POSTicketContract>();

                            #region Pagar ticket como transferido

                            var paymentContract = new POSPaymentLineContract(Guid.NewGuid(),
                                (long)ReceivableType.TicketTransfer, null, null, null, null,
                                null, null, null, null, transferLabel, originalTicket.Total,
                                originalTicket.PaymentReceived, 0, null, null, null);

                            originalTicket.PaymentLineByTicket.Add(paymentContract);

                            #endregion

                            #region Marcar lineas como transferidas

                            foreach (var productLine in originalTicket.ProductLineByTicket)
                            {
                                if (productLine.AnnulmentCode is not (ProductLineCancellationStatus.Active or ProductLineCancellationStatus.ActiveByTransfer))
                                    continue;

                                productLine.AnnulmentCode = ProductLineCancellationStatus.CancelledByTransfer;
                                productLine.AnnulUserId ??= sessionContext.UserId;
                            }

                            #endregion

                            var persistOriginalTicketResult = PersistAndClose(originalTicket);
                            if (persistOriginalTicketResult.IsEmpty)
                            {
                                #region Create new ticket

                                var ticketResult = NewTicket(sessionContext.InstallationId, sessionContext.StandId,
                                    sessionContext.CashierId, null, sessionContext.UserId);
                                if (ticketResult.IsEmpty)
                                {
                                    var newTicket = ticketResult.Contract.As<POSTicketContract>();
                                    newTicket.OpeningDate = originalTicket.OpeningDate;
                                    newTicket.OpeningTime = originalTicket.OpeningTime;

                                    foreach (var productLine in originalTicket.ProductLineByTicket
                                                 .Where(pl => pl.IsActive || pl.AnnulmentCode == ProductLineCancellationStatus.CancelledByTransfer))
                                    {
                                        var newLine = productLine.Clone<POSProductLineContract>();
                                        newLine.Id = Guid.NewGuid();
                                        newLine.Ticket = (Guid)newTicket.Id;
                                        newLine.AnnulmentCode = ProductLineCancellationStatus.ActiveByTransfer;
                                        newLine.ItemNumber = (short)newTicket.ProductLineByTicket.Count;

                                        newTicket.ProductLineByTicket.Add(newLine);
                                    }

                                    var persistNewTicketResult = PersistTicket(newTicket);
                                    if (persistNewTicketResult.IsEmpty)
                                    {
                                        tickets.Add(persistNewTicketResult.Contract.As<POSTicketContract>());

                                        #region Completar información de transferencia

                                        var ticketTransfer = new TicketTransfer(Manager, contract.Id);
                                        ticketTransfer.TicketDest = (Guid)newTicket.Id;
                                        ticketTransfer.CajaDest = sessionContext.CashierId;
                                        ticketTransfer.UtilAcep = sessionContext.UserId;
                                        ticketTransfer.AcepDate = stand.StandWorkDate;
                                        ticketTransfer.AcepTime = DateTime.Now;
                                        ticketTransfer.SaveObject();

                                        #endregion
                                    }
                                    else
                                        result.Add(persistNewTicketResult);
                                }
                                else
                                    result.Add(ticketResult);

                                #endregion
                            }
                            else
                                result.Add(persistOriginalTicketResult);
                        }
                        else
                            result.Add(loadTicketResult);
                    }

                    if (result.IsEmpty)
                        result.Tickets.AddRange(tickets);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult AddProductLineToTicket(NewTicketItemModel newTicketItem)
        {
            ValidationContractResult result = Guid.Empty.Equals(newTicketItem.TicketId) ? null : LoadTicketForEdition(newTicketItem.TicketId, true);
			POSTicketContract ticket
				= result?.Contract.As<POSTicketContract>()
                ?? new POSTicketContract { Stand = sessionContext.StandId, Caja = sessionContext.CashierId, UserId = sessionContext.UserId, Mesa = newTicketItem.TableId };
            POSProductLineContract productLine = null;


			var productRecord = GetProductRecordForTicketCreation(newTicketItem.ProductCode);
            if (productRecord != null)
            {
                var newLine = NewProductLine(ticket, productRecord, newTicketItem.Quantity);
                if (newLine.HasErrors)
                {
                    result.Add(newLine);
                    return result;
                }
                productLine = newLine.Contract.As<POSProductLineContract>();
            }
            else
                return result.AddError(999, $"Product {newTicketItem.ProductCode} not found");

            return AddProductLineToTicket(ticket, productLine);
		}


		public ValidationContractResult AddProductLineToTicket(POSTicketContract ticket, POSProductLineContract productLine, bool notifyTicket = true)
        {
            return AddProductLineToTicket(null!, ticket, productLine, [], notifyTicket);
        }

        private ValidationContractResult AddProductLineToTicket(
            Ticket persistentTicket,
            POSTicketContract ticket,
            POSProductLineContract productLine,
            IEnumerable<POSProductLineContract> accompanying,
            bool notifyTicket = true, bool persistProductLine = true)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    if (result.Validate(() => Manager
                        .GetLocalStockRecord(productLine.Product, sessionContext.WorkDate)
                        .CanConsume(productLine.ProductQtd), 999, "Stock insuficiente")
                        .HasErrors)
                        return result;

                    if (ticket.ClosedDate.HasValue)
                        result.AddError("Ticket already closed and payed");
                    else if (!ticket.IsPersisted)
                    {
                        var ticketResult = NewTicketFromContract(ticket);
                        if (ticketResult.IsEmpty)
                        {
                            var newTicket = ticketResult.Contract.As<POSTicketContract>();
                            newTicket.Opened = true;
                            newTicket.OpenUserId = sessionContext.UserId;
                            newTicket.BaseEntityId = ticket.BaseEntityId;
                            newTicket.FiscalDocTo = ticket.FiscalDocTo;
                            newTicket.FiscalDocFiscalNumber = ticket.FiscalDocFiscalNumber;
                            newTicket.FiscalDocAddress = ticket.FiscalDocAddress;
                            newTicket.FiscalDocNacionality = ticket.FiscalDocNacionality;
                            newTicket.FiscalDocEmail = ticket.FiscalDocEmail;
                            newTicket.ClientsByPosition.AddRange(ticket.ClientsByPosition);
                            newTicket.AccountDescription = ticket.AccountDescription;
                            newTicket.Account = ticket.Account;
                            newTicket.Room = ticket.Room;
                            newTicket.Pension = ticket.Pension;
                            newTicket.AccountType = ticket.AccountType;
                            newTicket.AllowRoomCredit = ticket.AllowRoomCredit;

                            // newTicket.ProductLineByTicket = ticket.ProductLineByTicket; // The ticket contract can come with products
                            // if ticket contract came with product lines to be added
                            foreach (var ticketProductLine in ticket.ProductLineByTicket)
                                newTicket.ProductLineByTicket.Add(ticketProductLine);

                            newTicket.SpaServiceId = ticket.SpaServiceId;
                            ticket = newTicket;
                        }
                        else
                            result.Add(ticketResult);
                    }

                    if (result.IsEmpty)
                    {
                        #region Join Order with 3 seconds

                        var joinTimespan = TimeSpan.FromSeconds(3);

                        var similar = ticket.ProductLineByTicket
                            .Where(pl => pl.AnnulmentCode == ProductLineCancellationStatus.Active &&
                                         pl.SendToAreaStatus == 0 && productLine.TableLines.Count == 0)
                            .OrderByDescending(pl => pl.LastModified).FirstOrDefault();

                        if (similar != null && !productLine.HasManualPrice &&
                            similar.Product == productLine.Product &&
                            similar.DiscountPercent == productLine.DiscountPercent &&
                            similar.AutomaticDiscount == productLine.AutomaticDiscount &&
                            similar.PaxNumber == productLine.PaxNumber &&
                            productLine.LastModified - similar.LastModified <= joinTimespan &&
                            CompareProductLinePreparations(productLine, similar))
                        {
                            similar.ProductQtd += productLine.ProductQtd;
                            similar.ValueBeforeDiscount += productLine.ValueBeforeDiscount;
                            similar.DiscountValue += productLine.DiscountValue;
                            similar.GrossValue += productLine.GrossValue;
                            similar.NetValue += productLine.NetValue;
                            similar.FirstIvaValue += productLine.FirstIvaValue;
                            similar.SecondIvaValue += productLine.SecondIvaValue;
                            similar.ThirdIvaValue += productLine.ThirdIvaValue;
                        }
                        else
                        {
                            productLine.Ticket = (Guid)ticket.Id;
                            ticket.ProductLineByTicket.Add(productLine);
                        }

                        #endregion

                        foreach (var accProductLine in accompanying ?? [])
                        {
                            accProductLine.Ticket = (Guid)ticket.Id;
                            ticket.ProductLineByTicket.Add(accProductLine);
                        }

                        var standBusiness = new StandBusiness(Manager, sessionContext, null, ticketRepository, _validatorFactory);
                        var stand = standBusiness.GetStandById(sessionContext.StandId, sessionContext.LanguageId);
                        // If account balance is:
                        // Negative: It's debit
                        // Positive: It's credit
                        if (stand.AskCode && result.Validate(
                                () => ticket.AccountBalance == null ||
                                      ticket.CalculateTotal(stand.ApplyTipOverNetValue) <= ticket.AccountBalance * decimal.MinusOne, 999,
                                "AccountBalanceError".TranslateMsg()).HasErrors)
                            return result;

                        if (persistProductLine)
                            result = PersistTicket(persistentTicket, ticket, notifyTicket);
                        else
                            result.Contract = ticket;
                    }

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        private static bool CompareProductLinePreparations(POSProductLineContract productLine, POSProductLineContract similar)
        {
            var flag =
                productLine.Preparations.Count == similar.Preparations.Count &&
                productLine.Preparations.All(productLinePreparation =>
                {
                    return similar.Preparations.FirstOrDefault(similarPreparation =>
                        similarPreparation.PreparationId == productLinePreparation.PreparationId) != null;
                });

            return flag;
        }

        private ValidationContractResult AddProductToTicket(Ticket persistentTicket, POSTicketContract ticket, ProductRecord.ProductRecord product, int quantity, decimal? manualPrice, string manualPriceDesc,
            Guid[]? preparations, bool isTip = false, bool notifyTicket = true, string observations = null, bool persistProductLine = true)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var productLine = ValidateResult<POSProductLineContract>(NewProductLine(ticket, product, quantity, manualPrice, manualPriceDesc, isTip, observations));

                    if (preparations != null)
                    {
                        foreach (var preparationId in preparations)
                        {
                            var preparation = new POSProductLinePreparationContract(Guid.NewGuid(), (Guid)productLine.Id, string.Empty, preparationId, string.Empty);
                            productLine.Preparations.Add(preparation);
                        }
                    }

                    if (ticket.CurrentPaxNumber >= 0 && !isTip)
                        productLine.PaxNumber = ticket.CurrentPaxNumber;

                    List<POSProductLineContract> accompanying = [];

                    if (product.AccompanyingProductsSelected.Count > 0)
                    {
                        foreach (var productId in product.AccompanyingProductsSelected)
                        {
                            var accProduct = GetProductRecordForTicketCreation(productId);
                            if (accProduct != null)
                            {
                                var pr = ValidateResult<POSProductLineContract>(NewProductLine(ticket, accProduct, quantity, asAccompanying: true));
                                pr.MainProduct = (Guid)productLine.Id;

                                // If everything is ok, add the product line to the ticket contract
                                // ticket.ProductLineByTicket.Add(pr);
                                accompanying.Add(pr);
                            }
                            else result.AddError($"Product not found: {productId}");
                            // If there is an error, stop the process
                            if (!result.IsEmpty) break;
                        }
                    }
                    if (result.IsEmpty)
                        result = AddProductLineToTicket(persistentTicket, ticket, productLine, accompanying, notifyTicket, persistProductLine);
                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult AddProductToTicket(POSTicketContract ticket, ProductRecord.ProductRecord product, int quantity, decimal? manualPrice, string manualPriceDesc, Guid[]? preparations, bool isTip = false, bool notifyTicket = true, string observations = null, bool persistProductLine = true)
        {
            return AddProductToTicket(null!, ticket, product, quantity, manualPrice, manualPriceDesc, preparations, isTip, notifyTicket, observations, persistProductLine);
        }

        public ValidationContractResult AddProductToTicketByPlu(Guid ticketId, string productCode, int quantity, decimal? manualPrice, Guid[] preparations, bool isTip = false)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var loadTicketResult = LoadTicket(ticketId);
                    if (loadTicketResult.IsEmpty)
                    {
                        var ticket = loadTicketResult.Contract.As<POSTicketContract>();
                        var product = GetProductRecordForTicketCreation(productCode);

                        if (product != null)
                        {
                            var productLine = NewProductLine(ticket, product, quantity, manualPrice, null, isTip).Contract.As<POSProductLineContract>();

                            if (preparations != null)
                            {
                                foreach (Guid preparationId in preparations)
                                {
                                    var preparation = new POSProductLinePreparationContract(Guid.NewGuid(), (Guid)productLine.Id, string.Empty, preparationId, string.Empty);
                                    productLine.Preparations.Add(preparation);
                                }
                            }

                            result = AddProductLineToTicket(null!, ticket, productLine, [], true);
                        }
                        else
                            result.AddError("Product missing or invalid");
                    }
                    else
                        result.Add(loadTicketResult);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult AddProductToTicket(
            Guid ticketId,
            Guid productId,
            int quantity,
            decimal? manualPrice,
            string manualPriceDescription,
            bool isTip,
            Guid[]? selectedDetailed,
            Guid[]? preparations,
            short? seat,
            Guid? separatorId,
            string observations = null
        )
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var ticket = LoadTicketForPersist(ticketId);
                    var contract = ParseTicket(ticket);
                    var initialProductCount = contract.ProductLines.Count();
                    contract.CurrentPaxNumber = seat ?? 0;

                    var product = GetProductRecordForTicketCreation(productId);
                    if (product != null)
                    {
                        if (selectedDetailed is { Length: > 0 })
                        {
                            if (product.IsProductMenu())
                            {
                                foreach (var record in product.TableProducts)
                                    record.Selected = selectedDetailed.Contains(record.Id);
                            }
                            else if (product.HasAccompanyingProducts())
                            {
                                product.AccompanyingProductsSelected = selectedDetailed.ToList();
                            }
                        }

                        result = AddProductToTicket(ticket, contract, product, quantity, manualPrice, manualPriceDescription, preparations, isTip, true, observations);
                        contract = result.Contract.As<POSTicketContract>();

                        if (separatorId.HasValue && !result.HasErrors && contract.ProductLines.Count() != initialProductCount)
                        {
                            var productLine = contract.ProductLineByTicket.FirstOrDefault(lineContract => lineContract.Id.ToString().AsGuid() == contract.ProductLinesIds.Last());
                            if (productLine != null)
                            {
                                result = ChangeProductLinesSeparator(ticketId, new[] { productLine.Id.ToString().AsGuid() }, separatorId);
                            }
                        }
                    }
                    else
                    {
                        result.AddError("Product missing or invalid");
                    }

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        // public ValidationContractResult AddPaymentToTicket(Guid ticketId, Guid paymentId, Guid? creditCardId, decimal received, decimal value, bool excessPaymentAsTip, bool close)
        // {
        //     var result = new ValidationContractResult();
        //
        //     Manager.Open();
        //     try
        //     {
        //         Manager.BeginTransaction();
        //         try
        //         {
        //             var loadTicketResult = LoadTicket(ticketId);
        //             if (loadTicketResult.IsEmpty)
        //             {
        //                 var ticket = loadTicketResult.Contract.As<POSTicketContract>();
        //
        //                 var contract = new POSPaymentLineContract()
        //                 {
        //                     Id = Guid.NewGuid(),
        //                     Ticket = ticketId,
        //                     PaymentAmount = value,
        //                     PaymentId = paymentId,
        //                     PaymentReceived = received,
        //                     ReceivableType = ReceivableType.Payment,
        //                     CurrencyId = ticket.Currency,
        //                     MultCurrency = true,
        //                     ExchangeCurrency = 1
        //                 };
        //
        //                 // Validate if the payment received is greater than ticket value and should be added as a tip
        //                 if (excessPaymentAsTip && received > value)
        //                 {
        //                     // Update payment
        //                     contract.PaymentAmount = received;
        //                     // Add tip to ticket
        //                     ticket.TipPercent = null;
        //                     ticket.TipValue = received - value;
        //                 }
        //
        //                 if (creditCardId.HasValue)
        //                 {
        //                     var creditCard = new POSCreditCardContract
        //                     {
        //                         CreditCardTypeId = creditCardId.Value,
        //                         Id = Guid.NewGuid()
        //                     };
        //
        //                     contract.CreditCardContract = creditCard;
        //                     contract.IsCreditCard = true;
        //                     contract.CreditCard = (Guid)creditCard.Id;
        //                 }
        //
        //                 if (ticket.PaymentTotal >= ticket.TicketValue + ticket.TipValue)
        //                 {
        //                     result.AddError("There is not debt to add a new payment");
        //                 }
        //                 else
        //                 {
        //                     ticket.PaymentLineByTicket.Add(contract);
        //
        //                     result = close ? CloseTicket(ticket, CheckProductTip(ticket)) : PersistTicket(ticket);
        //                 }
        //             }
        //             else
        //                 result.Add(loadTicketResult);
        //
        //             Manager.EndTransaction(result);
        //         }
        //         catch (Exception ex)
        //         {
        //             Manager.RollbackTransaction();
        //             Trace.TraceError(ex.Message);
        //             result.AddException(ex);
        //         }
        //     }
        //     finally
        //     {
        //         Manager.Close();
        //     }
        //
        //     return result;
        // }

        public ValidationContractResult AddClientPositionToTicket(Guid ticketId, Guid clientId, string clientName, string clientRoom, short pax, string allergies)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var loadTicketResult = LoadTicket(ticketId);
                    if (loadTicketResult.IsEmpty)
                    {
                        var ticket = loadTicketResult.Contract.As<POSTicketContract>();

                        var contract = ticket.ClientsByPosition.FirstOrDefault(cp => cp.Pax == pax);
                        if (contract == null)
                        {
                            contract = new POSClientByPositionContract();
                            ticket.ClientsByPosition.Add(contract);
                        }

                        contract.ClientId = clientId;
                        contract.ClientName = clientName;
                        contract.ClientRoom = clientRoom;
                        contract.Pax = pax;

                        PersistTicket(ticket);
                    }
                    else
                        result.Add(loadTicketResult);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult AddClientPositionToTicket(Guid ticketId, short pax, ClientInstallationRecord client)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var loadTicketResult = LoadTicket(ticketId);
                    if (loadTicketResult.IsEmpty)
                    {
                        var ticket = loadTicketResult.Contract.As<POSTicketContract>();

                        var clientByPosition = ticket.ClientsByPosition.FirstOrDefault(cp => cp.Pax == pax);
                        if (clientByPosition == null)
                        {
                            clientByPosition = new POSClientByPositionContract
                            {
                                Pax = pax
                            };

                            ticket.ClientsByPosition.Add(clientByPosition);
                        }

                        var clientId = client.Id.ToString().AsGuid();
                        clientByPosition.ClientId = clientId;
                        clientByPosition.ClientName = client.Name;
                        clientByPosition.ClientRoom = client.Room;

                        using var business = BusinessContext.GetBusiness<IStandBusiness>(Manager);
                        var clientInfoResult = business.GetClientInfo(clientId);
                        if (clientInfoResult.IsEmpty)
                        {
                            clientByPosition.Preferences.Clear();
                            clientByPosition.Preferences.AddRange(clientInfoResult.Item.Preferences
                                .Select(p => new POSClientByPositionPreferenceContract(Guid.NewGuid(), p.Id, p.Description)));

                            clientByPosition.Diets.Clear();
                            clientByPosition.Diets.AddRange(clientInfoResult.Item.Diets
                                .Select(p => new POSClientByPositionDietContract(Guid.NewGuid(), p.Id, p.Description)));

                            clientByPosition.Attentions.Clear();
                            clientByPosition.Attentions.AddRange(clientInfoResult.Item.Attentions
                                .Select(p => new POSClientByPositionAttentionContract(Guid.NewGuid(), p.Id, p.Description)));

                            clientByPosition.Allergies.Clear();
                            clientByPosition.Allergies.AddRange(clientInfoResult.Item.Allergies
                                .Select(p => new POSClientByPositionAllergyContract(Guid.NewGuid(), p.Id, p.Description)));

                            result = PersistTicket(ticket);
                        }
                        else
                        {
                            result.Add(clientInfoResult);
                        }
                    }
                    else
                        result.Add(loadTicketResult);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult RemoveClientPositionFromTicket(Guid ticketId, short pax)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var loadTicketResult = LoadTicket(ticketId);
                    if (loadTicketResult.IsEmpty)
                    {
                        var ticket = loadTicketResult.Contract.As<POSTicketContract>();
                        ticket.ClientsByPosition.Remove(cp => cp.Pax == pax);
                        result = PersistTicket(ticket);
                    }
                    else
                        result.Add(loadTicketResult);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        // public ValidationContractResult ResetPaymentTickets(Guid ticketId)
        // {
        //     var result = new ValidationContractResult();
        //
        //     Manager.Open();
        //     try
        //     {
        //         Manager.BeginTransaction();
        //         try
        //         {
        //             var ticket = LoadTicket(ticketId).Contract.As<POSTicketContract>();
        //             if (ticket.ClosedDate.HasValue)
        //                 result.AddError("Ticket already closed");
        //             else
        //             {
        //                 ticket.PaymentLineByTicket = new TypedList<POSPaymentLineContract>();
        //                 result = PersistTicket(ticket);
        //             }
        //
        //             Manager.EndTransaction(result);
        //         }
        //         catch (Exception ex)
        //         {
        //             Manager.RollbackTransaction();
        //             Trace.TraceError(ex.Message);
        //             result.AddException(ex);
        //         }
        //     }
        //     finally
        //     {
        //         Manager.Close();
        //     }
        //
        //     return result;
        // }

        public ValidationContractResult CreateLookupTable(Guid ticketId, bool handlePrint = false)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    result = LoadTicket(ticketId);
                    if (result.IsEmpty)
                    {
                        var ticket = result.Contract.As<POSTicketContract>();

                        result = CreateLookupTable(ticket);
                        if (result.IsEmpty && handlePrint)
                            PrintBySpoolerAsync((POSTicketContract)result.Contract, DocumentType.Receipt);
                    }

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult CreateLookupTable(POSTicketContract ticket)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var lookupTable = new LookupTableContract
                    {
                        Id = Guid.NewGuid(),
                        SystemDateTime = DateTime.Now,
                        WorkDate = sessionContext.WorkDate,
                        Ticket = (Guid)ticket.Id,
                        Total = ticket.Total,
                        User = sessionContext.UserId
                    };

                    #region Serie & Signature

                    var documentSerie = new DocumentSerieBusiness(Manager, sessionContext, _validatorFactory);
                    var netxSerieResult = documentSerie.GetNextSerialNumber(ticket.Caja, ticket.Stand, false, (long)POSDocumentType.TableBalance, true);
                    lookupTable.Number = netxSerieResult.DocumentNumber ?? 0;
                    lookupTable.Serie = netxSerieResult.DocumentSerie;

                    var lookupSerie = new DocumentSerie(Manager, netxSerieResult.SerieId.Value);
                    if (GeneralSetting.SignType == DocumentSign.FiscalizationPortugal && lookupSerie is { ValidationCode: null or "" or " " })
                    {
                        result.AddError("Validation code not configured");
                    }
                    else
                    {
                        var signature = SAFTPT.Instance.SignLookupTable(lookupTable.SystemDateTime, lookupTable.WorkDate, lookupTable.Number, lookupTable.Serie, lookupTable.Total,
                            lookupSerie.Signature);
                        lookupTable.Signature = signature;
                        lookupSerie.Signature = signature;
                        lookupSerie.SaveObject();
                    }

                    #endregion

                    if (result.IsEmpty)
                    {
                        #region Details

                        foreach (var productLine in ticket.ProductLineByTicket)
                        {
                            if (productLine.AnnulmentCode != ProductLineCancellationStatus.Cancelled)
                                productLine.Printed++;

                            foreach (var tableLine in productLine.TableLines)
                                tableLine.Printed = true;

                            var detailContract = new LookupTableDetailsContract()
                            {
                                LookupTableId = (Guid)lookupTable.Id,
                                NetPrice = productLine.NetValue,
                                ProductId = productLine.Product,
                                Quantity = productLine.ProductQtd,
                                TotalPrice = productLine.GrossValue,
                                AnnulmentCode = productLine.AnnulmentCode,
                                FirstIvaId = productLine.FirstIvaId,
                                FirstIvaPercent = productLine.FirstIvaPercent,
                                FirstIvaValue = productLine.FirstIvaValue,
                                SecondIvaId = productLine.SecondIvaId,
                                SecondIvaPercent = productLine.SecondIvaPercent,
                                SecondIvaValue = productLine.SecondIvaValue,
                                ThirdIvaId = productLine.ThirdIvaId,
                                ThirdIvaPercent = productLine.ThirdIvaPercent,
                                ThirdIvaValue = productLine.ThirdIvaValue
                            };

                            lookupTable.Details.Add(detailContract);
                        }

                        #endregion

                        #region Validation Code (ATCUD)

                        switch (GeneralSetting.SignType)
                        {
                            case DocumentSign.FiscalizationPortugal:
                                lookupTable.ValidationCode = lookupSerie.ValidationCode;
                                if (!string.IsNullOrEmpty(lookupTable.Serie))
                                {
                                    var buildQrCodeResult = BuildPortugalLookupQRCode(lookupTable, GeneralSetting);
                                    if (buildQrCodeResult.IsEmpty)
                                    {
                                        //byte[] qrCodeImage = new QRCode(buildQrCodeResult.Result);
                                        //lookupTable.QrCode = qrCodeImage;
                                        lookupTable.QrCodeData = buildQrCodeResult.Result;
                                    }
                                    else
                                        result.Add(buildQrCodeResult);
                                }
                                break;
                            case DocumentSign.FiscalizationCaboVerde:
                                lookupTable.ValidationCode = lookupSerie.ValidationCode;
                                break;
                        }

                        #endregion

                        if (result.IsEmpty)
                        {
                            ticket.LookupTableByTicket.Add(lookupTable);
                            result = PersistTicket(ticket);
                        }
                    }

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationStringResult FastCloseTicket(Guid ticketId, bool handlePrint = false)
        {
            var result = new ValidationStringResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    #region Validations

                    var settings = settingBusiness.GetGeneralSettings(sessionContext.InstallationId);
                    if (settings.FiscalType != FiscalPOSType.None)
                    {
                        // Has fiscal printer, android not have implemented invoice
                        result.AddError("Cannot use fast close ticket from tablet using fiscal printer");
                    }

                    #endregion

                    if (result.IsEmpty)
                    {
                        var ticket = LoadTicket(ticketId).Contract.As<POSTicketContract>();
                        ticket.PaymentLineByTicket = new TypedList<POSPaymentLineContract>();
                        var paymentId = GetFastPaymentMethod();

                        if (paymentId.HasValue)
                        {
                            var payment = new POSPaymentLineContract()
                            {
                                Id = Guid.NewGuid(),
                                IsCreditCard = false,
                                PaymentAmount = ticket.Total,
                                PaymentId = paymentId,
                                PaymentReceived = ticket.Total,
                                ReceivableType = ReceivableType.Payment,
                                Ticket = ticketId,
                                CurrencyId = ticket.Currency,
                                MultCurrency = true,
                                ExchangeCurrency = 1
                            };

                            ticket.PaymentLineByTicket.Add(payment);
                        }

                        var persistResult = CloseTicket(ticket, CheckProductTip(ticket));

                        if (persistResult.IsEmpty)
                        {
                            var contract = persistResult.Contract.As<POSTicketContract>();
                            result.Description = contract.SerieName;
                        }
                        else
                            result.Add(persistResult);

                        if (result.IsEmpty && handlePrint)
                            PrintBySpoolerAsync(ticket, DocumentType.Ticket);
                    }

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationStringResult CloseTicket(Guid ticketId, ClientRecord client, PersonalDocType docTypeSelected, bool save, Blob signature)
        {
            var result = new ValidationStringResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var loadResult = LoadTicket(ticketId);
                    if (!loadResult.HasErrors)
                    {
                        var ticket = loadResult.Contract.As<POSTicketContract>();
                        // Add digital signature
                        ticket.DigitalSignature = signature;

                        var persistResult = CloseAndPrintTicket(ticket, CheckProductTip(ticket), client, docTypeSelected, save);
                        if (persistResult.IsEmpty)
                        {
                            var contract = persistResult.Contract.As<POSTicketContract>();
                            result.Description = contract.SerieName;
                        }
                        else
                            result.Add(persistResult);
                    }

                    Manager.EndTransaction(result);
                    return result;
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    return new ValidationStringResult(ex);
                }
            }
            finally
            {
                Manager.Close();
            }
        }

        public ValidationStringResult FastCloseTicketAsInvoice(Guid ticketId, ClientRecord client,
            PersonalDocType docTypeSelected, bool saveClient)
        {
            var result = new ValidationStringResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var ticket = LoadTicket(ticketId).Contract.As<POSTicketContract>();

                    #region Validations

                    if (ticket.ProductLineByTicket.IsEmpty)
                        result.AddError("NotPayEmptyTicket".Translate());
                    else if (ticket.PaymentLineByTicket.Count > 0)
                        result.AddError(string.Format("YouMustRemovePreviousPayments".Translate(), "FAST".Translate()), "Warning".Translate());

                    #endregion

                    if (result.IsEmpty)
                    {
                        var paymentId = GetFastPaymentMethod();
                        if (paymentId.HasValue)
                        {
                            var payment = new POSPaymentLineContract()
                            {
                                Id = Guid.NewGuid(),
                                IsCreditCard = false,
                                PaymentAmount = ticket.Total,
                                PaymentId = paymentId,
                                PaymentReceived = ticket.Total,
                                ReceivableType = ReceivableType.Payment,
                                Ticket = ticketId,
                                CurrencyId = ticket.Currency,
                                MultCurrency = true,
                                ExchangeCurrency = 1
                            };

                            ticket.PaymentLineByTicket ??= new TypedList<POSPaymentLineContract>();
                            ticket.PaymentLineByTicket.Add(payment);

                            result = CloseTicketAsInvoice(ticket, CheckProductTip(ticket), client, docTypeSelected, saveClient);
                        }
                        else
                            result.AddError("There is no fast payment method configured");
                    }

                    Manager.EndTransaction(result);

                    return result;
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    return new ValidationStringResult(ex);
                }
            }
            finally
            {
                Manager.Close();
            }
        }

        public ValidationStringResult CloseTicketAsInvoice(Guid ticketId, ClientRecord client,
            PersonalDocType docTypeSelected, bool saveClient, Blob signature)
        {
            var result = new ValidationStringResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var ticket = LoadTicket(ticketId).Contract.As<POSTicketContract>();

                    #region Validations

                    if (ticket.ProductLineByTicket.IsEmpty)
                        result.AddError("NotPayEmptyTicket".Translate());

                    #endregion


                    if (result.IsEmpty)
                    {
                        // Add digital signature
                        ticket.DigitalSignature = signature;

                        result = CloseTicketAsInvoice(ticket, CheckProductTip(ticket), client, docTypeSelected, saveClient);
                    }

                    Manager.EndTransaction(result);

                    return result;
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    return new ValidationStringResult(ex);
                }
            }
            finally
            {
                Manager.Close();
            }
        }

        /**
         * Check the availability of the tip in the ticket and return the product that reference the tip
         */
        private ProductRecord.ProductRecord? CheckProductTip(POSTicketContract ticket)
        {
            // Get tip product if ticket has tip value > 0
            ProductRecord.ProductRecord? tipProduct = null;
            if (ticket.TipValue is not > decimal.Zero) return tipProduct;

            var standBusiness = new StandBusiness(Manager, sessionContext, null, ticketRepository, _validatorFactory);
            var productBusiness = new ProductBusiness(Manager, sessionContext);
            var stand = standBusiness.GetStandById(sessionContext.StandId, sessionContext.LanguageId);

            if (stand.TipServiceId.HasValue)
                tipProduct = productBusiness.GetProductByStand(stand.TipServiceId.Value, (Guid)stand.Id);

            return tipProduct;
        }

        public ValidationContractResult CloseTicket(POSTicketContract ticket, ProductRecord.ProductRecord? tipProduct = null, bool saveClient = false)
        {
            var result = new ValidationContractResult();

            try
            {
                result = CloseTicketInternal(ticket, tipProduct, saveClient);
                if (result.ContainsError(ErrorCodePos.DocExportationCommunication))
                {
                    if (GeneralSetting.AllowContingencySerie)
                    {
                        BlockOnlineDocExportation = true;
                        UseContingencySeries = true;

                        try
                        {
                            result = CloseTicketInternal(ticket, tipProduct, saveClient);
                            if (result.IsEmpty)
                            {
                                var translation = LocalizationMgr.TranslationMessage(sessionContext.LanguageId, "ErrorExpContSerie");
                                result.AddWarning(translation);
                            }
                        }
                        finally
                        {
                            BlockOnlineDocExportation = false;
                            UseContingencySeries = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logsFactory.Error(ex);
                result.AddException(ex);
            }

            #region StockIntegration

            if (result.IsEmpty)
            {
                var standBusiness = BusinessContext.GetBusiness<IStandBusiness>(Manager);
                var currentStand = standBusiness.GetStands(sessionContext.CashierId, (int)sessionContext.LanguageId);
                var isEnabled = currentStand.FirstOrDefault(s => (Guid)s.Id == sessionContext.StandId).ActivateIntegration;
                if (isEnabled)
                {
                    // Unfortunately, we need to wait for the stock integration to finish that is why we are using Task.Run and .Result
                    // And since the method CloseTicket is critical the refactoring will take a very long time  
                    var stockResult = Task.Run(async () => await _stocksManager.ConsumeStocks(ticket)).Result;
                    // We log instead of adding errors because the ticket was closed successfully and this is just an extra feature that should not block the ticket closing
                    if (stockResult is not null && stockResult.HasErrors)
                    {
                        foreach(var error in stockResult.Errors)
                        {
                            // TODO: Log to a file not to the console
                            Console.WriteLine($"Could not update \n {error}");
                        }
                    }
                }
            }

            #endregion

            return result;
        }

        public ValidationContractResult LoadCloseTicket(Guid ticketId, bool local)
        {
            try
            {
                // Ticket from local
                if (local)
                    return LoadTicket(ticketId);

                // Ticket from cloud
                var cloudBusiness = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
                return cloudBusiness.LoadTicket(ticketId);
            }
            catch (Exception ex)
            {
                logsFactory.Error(ex);
                var result = new ValidationContractResult();
                result.AddException(ex);
                return result;
            }
        }

        private string GetUserName(Guid userId)
        {
            var qr = new QueryRequest();
            qr.AddFilter(UsersQuery.UserIdFilter, userId);

            var query = QueryFactory.Get<UsersQuery>(Manager);
            query.SetRequest(qr);
            return query.Execute().FirstOrDefault()?.ValueAs<string>(UsersQuery.DescriptionColumn);
        }

        private ValidationResult CreateCreditNote(POSTicketContract contract, DateTime workDate, Guid cancellationReasonId, Guid userId)
        {
            var result = new ValidationResult();

            // Ticket con información de facturación
            if (!string.IsNullOrEmpty(contract.FiscalDocSerie) && contract.FiscalDocNumber.HasValue)
            {
                contract.CreditNoteSystemDate = DateTime.Now;
                contract.CreditNoteWorkDate = workDate;
                contract.CancellationReasonId = cancellationReasonId;
                contract.CreditNoteUserId = userId;
                contract.CreditNoteUserDescription = GetUserName(userId);

                result.Add(GenerateCreditNoteSerie(contract, GeneralSetting.SignType));
                if (result.IsEmpty)
                {
                    #region Generate QR Code

                    var qrCodeStr = string.Empty;
                    switch (GeneralSetting.SignType)
                    {
                        case DocumentSign.FiscalizationPortugal:
                            var buildQrCodeResult = BuildPortugalCreditNoteQRCode(contract, GeneralSetting);

                            if (buildQrCodeResult.IsEmpty)
                                qrCodeStr = buildQrCodeResult.Result;
                            else
                                result.Add(buildQrCodeResult);
                            break;
                    }

                    #endregion

                    if (result.IsEmpty)
                    {
                        // Save QR code
                        if (!string.IsNullOrEmpty(qrCodeStr))
                        {
                            //byte[] qrCodeImage = new QRCode(qrCodeStr);
                            //contract.CreditNoteQrCode = qrCodeImage;
                            contract.CreditNoteQrCodeData = qrCodeStr;
                        }
                    }
                }
            }

            return result;
        }

        public ValidationContractResult CreateQuickTicketFromClosedTicket(Guid closedTicketId)
        {
            var result = new ValidationContractResult();
            Manager.Open();

            try
            {
                var newTicketResult = NewTicket(sessionContext.InstallationId, sessionContext.StandId, sessionContext.CashierId, null, sessionContext.UserId);
                if (!newTicketResult.IsEmpty)
                    return result;

                var newTicketContract = newTicketResult.Contract.As<POSTicketContract>();
                var closedTicketResult = LoadTicket(closedTicketId);
                if (closedTicketResult.HasErrors)
                    return (ValidationContractResult)
                        result.Add(closedTicketResult);

                var ticket = (POSTicketContract)closedTicketResult.Contract;
                var oldTicketProducts = ticket.ProductLineByTicket.Where(
                    pl => pl.IsAnul == ProductLineCancellationStatus.Active 
                    || pl.IsAnul == ProductLineCancellationStatus.ActiveByTransfer 
                    || pl.AnulAfterClosed
                ).ToList();

                if (!oldTicketProducts.Any())
                    return result;
                else
                {
                    foreach (var oldTicketProductLine in oldTicketProducts)
                    {
                        // If it does not exist, continue
                        var product = GetProductRecordForTicketCreation(oldTicketProductLine.Product);
                        if (product == null) continue;
                        // If passes the validation, create the product line for the ticket
                        var pr = ValidateResult<POSProductLineContract>(NewProductLine(newTicketContract, product, (int)oldTicketProductLine.ProductQtd, oldTicketProductLine.HasManualPrice ? oldTicketProductLine.ValueBeforeDiscount : null, oldTicketProductLine.HasManualPrice ? oldTicketProductLine.ManualPriceDesc : null));
                        newTicketContract.ProductLineByTicket.Add(pr);
                    }
                    result.Add(PersistTicket(newTicketContract));
                    result.Contract = newTicketContract;
                }
                return result;
            }
            finally
            {
                Manager.Close();
            }
        }

        public ValidationContractResult CancelTicketById(Guid ticketId, Guid cancellationReasonId, string comments, bool voidTicket, bool fromMerge = false, bool createQuickTicket = false)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                //var ticket = new Ticket(Manager, ticketId);
                //if (ticket.ClosedDate.HasValue)
                //	throw new ApplicationException("Ticket Closed");

                var loadResult = LoadTicket(ticketId);
                if (loadResult.HasErrors)
                    return (ValidationContractResult)result.Add(loadResult);

                var ticket = (POSTicketContract)loadResult.Contract;

                // var newContract = ticket.Clone<POSTicketContract>();
                // Yep, this is horrible
                var serialized = ticket.SerializeContract();
                var newContract = serialized.DeserializeContract<POSTicketContract>();

                result = CancelTicket(ticket, cancellationReasonId, comments, voidTicket, fromMerge, createQuickTicket);
                if (!result.HasErrors)
                    result.Contract = newContract;

                return result;

            }
            finally
            {
                Manager.Close();
            }
        }

        /// <summary>
        /// Cancels a ticket and creates a new one with the same products, clients, etc.
        /// The new ticket will increment the ticket number by 1.
        /// </summary>
        /// <param name="contract">The ticket/contract to be cancelled and recreated</param>
        /// <param name="cancellationReasonId">The id of the cancellation enum</param>
        /// <param name="comments">The comments/reason of the cancellation</param>
        /// <param name="voidTicket">If it's a void ticket</param>
        /// <param name="fromMerge">If it comes from a merge</param>
        /// <param name="createQuickTicket">If it should be recreated has a quick ticket</param>
        /// <returns>A empty result if everything is okay otherwise returns a result with an error</returns>
        public ValidationContractResult CancelTicket(POSTicketContract contract, Guid cancellationReasonId, string comments, bool voidTicket, bool fromMerge = false, bool createQuickTicket = false)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var stand = new Stand(Manager, sessionContext.StandId);
                    // Save the old ticket in memory to use it later
                    var oldTicket = contract.Clone<POSTicketContract>();
                    var fromClosed = contract.ClosedDate.HasValue;
                    if (!fromClosed)
                    {
                        contract.ClosedDate = stand.StandWorkDate.Date;
                        contract.ClosedTime = DateTime.Now;
                    }
                    else if (sessionContext.SignatureMode == DocumentSign.FiscalizationBrazilNFE)
                    {
                        if (contract.FiscalDocumentPending) result.AddError("NFCe ainda não foi emitida");
                    }
                    else if (contract.HasTransferredOrders()) result.AddError("You cannot void a ticket with transferred orders");

                    if (result.IsEmpty)
                    {
                        // If it's a closed ticket
                        if (fromClosed)
                        {

                            var business = BusinessContext.GetBusiness<IFiscalBusiness>(Manager);
                            if (voidTicket)
                            {
                                contract.IsAnul = true;
                                contract.UserCancellationId = sessionContext.UserId;
                                contract.CancellationReasonId = cancellationReasonId;
                                contract.CancellationDate = stand.StandWorkDate.Date;
                                contract.CancellationSystemDate = DateTime.Now;
                                contract.CancellationComments = comments;

                                if (!contract.IsInvoice)
                                    CancelProductLines(contract.ProductLineByTicket, true, fromMerge);

                                if (!string.IsNullOrEmpty(contract.InvoiceSerie) && contract.InvoiceNumber.HasValue)
                                {
                                    #region Cancelar Factura/Boleta Electrónica

                                    if (!UseContingencySeries)
                                    {
                                        switch (contract.DocumentType)
                                        {
                                            case POSDocumentType.CashInvoice:
                                                result.Add(business.CancelInvoice((Guid)contract.Id, contract.InvoiceSerie, contract.InvoiceNumber.Value));
                                                break;
                                            case POSDocumentType.Ballot:
                                                result.Add(business.CancelTicket((Guid)contract.Id, contract.InvoiceSerie, contract.InvoiceNumber.Value));
                                                break;
                                        }
                                    }

                                    #endregion
                                }
                            }
                            else
                            {
                                #region Crear Nota de Crédito

                                result.Add(CreateCreditNote(contract, stand.StandWorkDate.Date, cancellationReasonId, sessionContext.UserId));
                                if (result.IsEmpty)
                                {
                                    if (!string.IsNullOrEmpty(contract.CreditNoteSerie) && contract.CreditNoteNumber.HasValue)
                                    {
                                        #region Nota de Crédito Electrónica

                                        if (!UseContingencySeries)
                                            result.Add(business.ProcessCreditNote((Guid)contract.Id, contract.CreditNoteSerie, contract.CreditNoteNumber.Value));

                                        #endregion
                                    }
                                }

                                #endregion
                            }

                            if (result.IsEmpty)
                                result.Add(PersistTicket(contract));
                        }
                        else
                        {
                            contract.IsAnul = true;
                            contract.UserCancellationId = sessionContext.UserId;
                            contract.CancellationReasonId = cancellationReasonId;
                            contract.CancellationDate = stand.StandWorkDate.Date;
                            contract.CancellationSystemDate = DateTime.Now;
                            contract.CancellationComments = comments;

                            if (!contract.IsInvoice)
                                CancelProductLines(contract.ProductLineByTicket, false, fromMerge);

                            result.Add(PersistAndClose(contract));
                        }

                        //Create new quick ticket
                        if (result.IsEmpty && createQuickTicket)
                        {
                            var newTicketResult = NewTicket(sessionContext.InstallationId, sessionContext.StandId, sessionContext.CashierId, null, sessionContext.UserId);
                            if (newTicketResult.IsEmpty)
                            {
                                var newTicketContract = newTicketResult.Contract.As<POSTicketContract>();

                                foreach (
                                    var oldTicketProductLine in oldTicket.ProductLineByTicket.Where(
                                        pl => pl.IsAnul == ProductLineCancellationStatus.Active 
                                        || pl.IsAnul == ProductLineCancellationStatus.ActiveByTransfer 
                                        || pl.AnulAfterClosed
                                    )
                                )
                                {
                                    // If it does not exist, continue
                                    var product = GetProductRecordForTicketCreation(oldTicketProductLine.Product);
                                    if (product == null) continue;
                                    // If passes the validation, create the product line for the ticket
                                    var pr = ValidateResult<POSProductLineContract>(NewProductLine(newTicketContract, product, (int)oldTicketProductLine.ProductQtd, oldTicketProductLine.HasManualPrice ? oldTicketProductLine.ValueBeforeDiscount : null, oldTicketProductLine.HasManualPrice ? oldTicketProductLine.ManualPriceDesc : null));
                                    newTicketContract.ProductLineByTicket.Add(pr);
                                }
                                result.Add(PersistTicket(newTicketContract));
                            }
                        }

                        if (result.IsEmpty)
                        {
                            if (stand.AllowChangeTableReservationStatus && contract.ReservationTableId.HasValue && contract.ReservationTable.IsCheckin)
                            {
                                // Puts the reservation in a checkout status and ignores communication errors with the cloud
                                var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
                                var vr = business.CheckOutBookingTable(contract.ReservationTableId.Value);
                                if (!vr.IsEmpty)
                                {
                                    business.TraceComunicationErrors(vr);
                                    result.AddValidations(business.GetBusinessValidations(vr));
                                }
                            }
                        }
                    }

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationResult CancelTicket(Guid ticketId, Guid cancellationReasonId, string comments, bool fromMerge = false)
        {
            var result = new ValidationResult();

            try
            {
                var tickedResult = LoadTicket(ticketId);
                if (tickedResult.IsEmpty)
                {
                    var ticket = tickedResult.Contract.As<POSTicketContract>();

                    // Void product lines
                    var productLines = ticket.ProductLineByTicket.Where(x => x.IsActive && (x.HasAreas || x.TableLines.Any(t => t.HasAreas))).ToList();
                    if (productLines.Any())
                    {
                        var dispatchContract = productLines.Select(productLine => new DispatchContract
                        {
                            OrderId = (Guid)productLine.Id,
                            AreaIds = productLine.Areas.Select(area => area.AreaId).ToArray(),
                            OrderIds = productLine.TableLines.Select(tableLine => new DispatchContract
                            {
                                OrderId = (Guid)tableLine.Id,
                                AreaIds = tableLine.Areas.Select(area => area.AreaId).ToArray()
                            }).ToArray()
                        }
                        ).ToArray();

                        result = VoidDispatchTicketOrders(ticket, dispatchContract, true, true, false);
                    }

                    if (result.IsEmpty)
                    {
                        result.Add(CancelTicket(ticket, cancellationReasonId, comments, fromMerge));
                    }
                }
                else
                    result.Add(tickedResult);
            }
            catch (Exception ex)
            {
                logsFactory.Error(ex);
                result.AddException(ex);
            }

            return result;
        }

        public ValidationContractResult CancelClosedTicket(Guid ticketId, Guid cancellationReasonId, string comments, bool voidTicket, bool local, bool createQuickTicket)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var loadTicketResult = LoadTicket(ticketId);
                    if (loadTicketResult.IsEmpty)
                    {
                        var contract = (POSTicketContract)loadTicketResult.Contract;

                        if (GeneralSetting is { TicketCancelWindow: not null })
                        {
                            var workDate = sessionContext.WorkDate;
                            var closedDate = contract.ClosedDate?.Add(contract.ClosedTime?.TimeOfDay ?? TimeSpan.Zero);
                            var time = GeneralSetting.TicketCancelWindow ?? TimeSpan.Zero;

                            if (closedDate.HasValue && closedDate.Value.Add(time) < workDate)
                            {
                                result.AddError("CancelTicketExceededDays".TranslateMsg());
                            }

                            if (!result.HasErrors)
                            {
                                result.Add(CancelTicket(contract, cancellationReasonId, comments, voidTicket, createQuickTicket: createQuickTicket));
                                if (!result.IsEmpty)
                                    return result;
                                else
                                {
                                    // if (!string.IsNullOrEmpty(contract.CreditNoteSerie) && contract.CreditNoteNumber.HasValue)
                                    //     result.Contract = contract;
                                    // else
                                    // {
                                    //     var cloudBusiness = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
                                    //     result.Add(cloudBusiness.CancelTicket(ticketId, cancellationReasonId, comments));
                                    // }

                                    // When the ticket is processed it-s already in PMS so this is only called to guarantee that all
                                    // movements are recorded on the cloud/api
                                    //if (contract.Process)
                                    {
                                        var cloudBusiness = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
                                        result.Add(cloudBusiness.CancelTicket(ticketId, cancellationReasonId, comments));
                                    }
                                }

                                if (Manager.EndTransaction(result))
                                {
                                    // Notification
                                    BusinessContext.Publish("TicketCanceled", ticketId);
                                }
                            }
                        }
                    }
                    else
                        result.Add(loadTicketResult);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult CancelTicketProduct(POSTicketContract contract, Guid productLineId, bool printVoid = false)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var productLines = contract.ProductLineByTicket.Where(x => ((Guid)x.Id == productLineId || x.MainProduct == productLineId) && x.IsActive);

                    var productLinesToVoid = new List<DispatchContract>();
                    if (printVoid)
                    {
                        foreach (var productLineContract in productLines)
                        {
                            if (!productLineContract.HasAreas && !productLineContract.TableLines.Any(x => x.HasAreas))
                            {
                                var dispatchContract = new DispatchContract
                                {
                                    OrderId = (Guid)productLineContract.Id,
                                    AreaIds = [],
                                    OrderIds = []
                                };
                                productLinesToVoid.Add(dispatchContract);
                            }
                            else
                            {
                                var dispatchContract = new DispatchContract
                                {
                                    OrderId = (Guid)productLineContract.Id,
                                    AreaIds = productLineContract.Areas.Select(x => x.AreaId).ToArray(),
                                    OrderIds = productLineContract.TableLines.Select(x => new DispatchContract
                                    {
                                        OrderId = (Guid)x.Id,
                                        AreaIds = x.Areas.Select(x => x.AreaId).ToArray()
                                    }).ToArray()
                                };
                                productLinesToVoid.Add(dispatchContract);
                            }
                        }

                        result = VoidDispatchTicketOrders((Guid)contract.Id, productLinesToVoid.ToArray(), true, true);
                    }
                    else
                    {
                        foreach (var productLineContract in productLines)
                        {
                            // In the product was cancelled before, we don't need to cancel it again otherwise we will have duplicated movements
                            if (productLineContract.AnnulmentCode != ProductLineCancellationStatus.Cancelled)
                            {
                                productLineContract.AnnulmentCode = productLineContract.Printed > 0 ? ProductLineCancellationStatus.CancelledAfterPrint : ProductLineCancellationStatus.Cancelled;
                                productLineContract.AnnulUserId ??= sessionContext.UserId;
                            }
                        }

                        result = PersistTicket(contract);
                    }

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult CancelTicketProduct(Guid ticketId, Guid productLineId)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();

                try
                {
                    var ticket = LoadTicket(ticketId).Contract.As<POSTicketContract>();
                    result = CancelTicketProduct(ticket, productLineId, true);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult MergeTicketsById(Guid mergeTicketId, Guid voidTicketId)
        {
            Manager.Open();
            try
            {
                var loadResult = LoadTicket(mergeTicketId);
                if (loadResult.HasErrors)
                    return loadResult;

                var mergeTicket = loadResult.Contract.As<POSTicketContract>();

                loadResult = LoadTicket(voidTicketId);
                if (loadResult.HasErrors)
                    return loadResult;

                var voidTicket = loadResult.Contract.As<POSTicketContract>();

                return MergeTickets(mergeTicket, voidTicket);
            }
            finally
            {
                Manager.Close();
            }
        }

        public ValidationContractResult MergeTickets(POSTicketContract mergeTicket, POSTicketContract voidTicket)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var settingBusiness = new SettingBusiness(Manager);
                    var settings = settingBusiness.GetGeneralSettings(sessionContext.InstallationId);

                    // Move all orders to first ticket
                    foreach (var productLine in voidTicket.ProductLineByTicket)
                    {
                        var newLine = productLine.Clone<POSProductLineContract>();
                        newLine.Id = Guid.NewGuid();
                        newLine.Ticket = (Guid)mergeTicket.Id;

                        newLine.TableLines = new TypedList<TableLineContract>();
                        foreach (var tableLine in productLine.TableLines)
                        {
                            var newTableLine = tableLine.Clone<TableLineContract>();
                            newTableLine.Id = Guid.NewGuid();
                            newTableLine.ProductLineId = (Guid)newLine.Id;

                            // if (tableLine.TableLinePreparations != null)
                            // {
                            //     foreach (var prep in tableLine.TableLinePreparations)
                            //     {
                            //         var newPrep = new TableLinePreparationContract
                            //         {
                            //             Id = Guid.NewGuid(),
                            //             LineDescription = newTableLine.ProductDescription,
                            //             PreparationDescription = prep.PreparationDescription,
                            //             PreparationId = prep.PreparationId,
                            //             TableLineId = (Guid)newTableLine.Id
                            //         };
                            //         newTableLine.TableLinePreparations.Add(newPrep);
                            //     }
                            // }
                            //
                            // if (tableLine.Areas != null)
                            // {
                            //     foreach (var area in tableLine.Areas)
                            //     {
                            //         var newArea = new POSProductLineAreaContract()
                            //         {
                            //             Id = Guid.NewGuid(),
                            //             AreaId = area.AreaId
                            //         };
                            //         newTableLine.Areas.Add(newArea);
                            //     }
                            // }

                            newLine.TableLines.Add(newTableLine);
                        }

                        mergeTicket.ProductLineByTicket.Add(newLine);
                    }

                    // Cancel Second Ticket
                    if (settings.DefaultCancellationReasonId != null)
                        result.Add(CancelTicket(voidTicket, settings.DefaultCancellationReasonId.Value, string.Empty, true));

                    // Save First Ticket
                    if (result.IsEmpty)
                        result = PersistTicket(mergeTicket);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult SplitTicketProduct(POSTicketContract ticket, Guid productLineId, int quantity)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var originalLine = ticket.ProductLineByTicket.Where(x => (Guid)x.Id == productLineId).FirstOrDefault();
                    if (originalLine == null)
                        result.AddError(-1, "Invalid Split Quantity");
                    if (originalLine != null && originalLine.ProductQtd <= quantity)
                        result.AddError(-1, "Invalid Split Quantity");

                    if (result.IsEmpty)
                    {

                        // Previously, we were using Clone instead of CreateCopy.
                        // If you need to revert to using Clone, you may still use
                        // both New Line Adjustments and Original Line Adjustments                       

                        // New Line Creation
                        //POSProductLineContract newLine = originalLine.Clone<POSProductLineContract>();
                        POSProductLineContract newLine = originalLine.CreateCopy((Guid)ticket.Id, (newLine, oldLine) =>
                        {
                            #region New Line Adjustemnts

                            newLine.LastModified = DateTime.MinValue;
                            newLine.Id = Guid.NewGuid();
                            newLine.ProductQtd = quantity;
                            ticket.ProductLineByTicket.Add(newLine);

                            // New Line Price Adjustments
                            newLine.ProductQtd = quantity;
                            newLine.CostValue = (newLine.CostValue / originalLine.ProductQtd * quantity);
                            newLine.DiscountValue = (newLine.DiscountValue / originalLine.ProductQtd * quantity);
                            newLine.FirstIvaValue = (newLine.FirstIvaValue / originalLine.ProductQtd * quantity);
                            newLine.GrossValue = (newLine.GrossValue / originalLine.ProductQtd * quantity);
                            newLine.NetValue = (newLine.NetValue / originalLine.ProductQtd * quantity);
                            newLine.SecondIvaValue = (newLine.SecondIvaValue ?? 0 / originalLine.ProductQtd * quantity);
                            newLine.ThirdIvaValue = (newLine.ThirdIvaValue ?? 0 / originalLine.ProductQtd * quantity);
                            newLine.ValueBeforeDiscount = (newLine.ValueBeforeDiscount / originalLine.ProductQtd * quantity);

                            #endregion

                            #region Original Line Adjustments

                            originalLine.ProductQtd -= quantity;
                            originalLine.CostValue = originalLine.CostValue - newLine.CostValue;
                            originalLine.DiscountValue = originalLine.DiscountValue - newLine.DiscountValue;
                            originalLine.FirstIvaValue = originalLine.FirstIvaValue - newLine.FirstIvaValue;
                            originalLine.GrossValue = originalLine.GrossValue - newLine.GrossValue;
                            originalLine.NetValue = originalLine.NetValue - newLine.NetValue;
                            originalLine.SecondIvaValue = originalLine.SecondIvaValue - newLine.SecondIvaValue;
                            originalLine.ThirdIvaValue = originalLine.ThirdIvaValue - newLine.ThirdIvaValue;
                            originalLine.ValueBeforeDiscount = originalLine.ValueBeforeDiscount - newLine.ValueBeforeDiscount;

                            #endregion
                        });
                    }

                    result = PersistTicket(ticket);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult ChangeTicketProductQuantity(Guid ticketId, Guid productLineId, decimal quantity)
        {
            var result = new ValidationContractResult();

            try
            {
                result = LoadTicket(ticketId);
                if (result.HasErrors)
                    return result;

                var ticketContract = result.Contract.As<POSTicketContract>();
                var productLine = ticketContract.ProductLineByTicket.FirstOrDefault(pl => (Guid)pl.Id == productLineId);

                if (productLine != null)
                {
                    if (productLine.ProductQtd != quantity)
                    {
                        #region Add more items

                        if (quantity > productLine.ProductQtd)
                        {
                            if (productLine.SendToAreaStatus != 0 || productLine.TableLines.Any(x => x.SendToAreaStatus != 0))
                            {
                                var newLine = productLine.CreateCopy((Guid)ticketContract.Id, (newLine, oldLine) =>
                                {
                                    // Clear the area status
                                    newLine.SendToAreaStatus = 0;
                                    newLine.SendToAreaStartTime = null;
                                    newLine.SendToAreaEndTime = null;
                                    newLine.AreaId = null;
                                    newLine.Areas = [];

                                    // Clear the area status for its table lines
                                    foreach (var newTableLine in newLine.TableLines)
                                    {
                                        newTableLine.SendToAreaStatus = 0;
                                        newTableLine.AreaId = null;
                                        newTableLine.Areas = [];
                                    }
                                });

                                newLine.ProductLineAdjustmentByQuantity(quantity - productLine.ProductQtd);
                                ticketContract.ProductLineByTicket.Add(newLine);
                            }
                            // No dispatch, just adjust quantity
                            else
                                productLine.ProductLineAdjustmentByQuantity(quantity);
                        }

                        #endregion

                        #region Remove some items

                        else
                        {
                            // Quantity 0, Anul line
                            if (quantity == 0)
                            {
                                productLine.AnnulmentCode = productLine.Printed > 0 ? ProductLineCancellationStatus.CancelledAfterPrint : ProductLineCancellationStatus.Cancelled;
                                productLine.AnnulUserId ??= sessionContext.UserId;
                            }
                            else
                            {
                                // Create Annul line
                                var cancelLine = productLine.CreateCopy((Guid)ticketContract.Id, (newLineContract, _) =>
                                {
                                    newLineContract.AnnulmentCode = productLine.Printed > 0 ? ProductLineCancellationStatus.CancelledAfterPrint : ProductLineCancellationStatus.Cancelled;
                                    newLineContract.AnnulUserId ??= sessionContext.UserId;
                                });

                                cancelLine.ProductLineAdjustmentByQuantity(productLine.ProductQtd - quantity);
                                productLine.ProductLineAdjustmentByQuantity(quantity);

                                ticketContract.ProductLineByTicket.Add(cancelLine);

                                if (cancelLine.HasAreas || cancelLine.TableLines.Any(x => x.HasAreas))
                                {
                                    var dispatchContract = new DispatchContract
                                    {
                                        OrderId = (Guid)cancelLine.Id,
                                        AreaIds = cancelLine.Areas.Select(area => area.AreaId).ToArray(),
                                        OrderIds = cancelLine.TableLines.Select(cancelTableLine => new DispatchContract
                                        {
                                            OrderId = (Guid)cancelTableLine.Id,
                                            AreaIds = cancelTableLine.Areas.Select(area => area.AreaId).ToArray()
                                        }).ToArray()
                                    };

                                    result = VoidDispatchTicketOrders(ticketContract, [dispatchContract], true, true, false);
                                }
                            }
                        }

                        #endregion
                    }

                    var standBusiness = new StandBusiness(Manager, sessionContext, null, ticketRepository, _validatorFactory);
                    var stand = standBusiness.GetStandById(sessionContext.StandId, sessionContext.LanguageId);
                    // If account balance is:
                    // Negative: It's debit
                    // Positive: It's credit
                    if (stand.AskCode && result.Validate(
                            () => ticketContract.AccountBalance == null ||
                            ticketContract.CalculateTotal(stand.ApplyTipOverNetValue) <= ticketContract.AccountBalance * decimal.MinusOne, 999,
                            "AccountBalanceError".TranslateMsg()).HasErrors)
                        return result;

                    if (result.IsEmpty)
                        result = PersistTicket(LoadTicketForPersist(ticketId), ticketContract);
                }
                else
                {
                    result.Contract = ticketContract;
                    result.AddError("Product not found");
                }
            }
            catch (Exception ex)
            {
                logsFactory.Error(ex);
                result.AddException(ex);
            }

            return result;
        }

        public ValidationContractResult AddOrderComment(Guid ticketId, Guid productLineId, string comments)
        {
            var result = new ValidationContractResult();

            try
            {
                var ticket = LoadTicket(ticketId).Contract.As<POSTicketContract>();
                var line = LoadProductLine(productLineId).Contract.As<POSProductLineContract>();
                line.Observations = comments;

                return ChangeTicketProductDetails(ticket, productLineId, line);
            }
            catch (Exception ex)
            {
                logsFactory.Error(ex);
                result.AddException(ex);
            }

            return result;
        }

        public ValidationContractResult AddOrderPreparation(Guid ticketId, Guid productLineId, Guid preparationId)
        {
            var result = new ValidationContractResult();

            try
            {
                var ticket = LoadTicket(ticketId).Contract.As<POSTicketContract>();
                var line = LoadProductLine(productLineId).Contract.As<POSProductLineContract>();
                var preparation = new POSProductLinePreparationContract(Guid.NewGuid(), productLineId, string.Empty, preparationId, string.Empty);
                line.Preparations.Add(preparation);

                return ChangeTicketProductDetails(ticket, productLineId, line);
            }
            catch (Exception ex)
            {
                logsFactory.Error(ex);
                result.AddException(ex);
            }

            return result;
        }

        public ValidationContractResult UpdateOrderPreparation(Guid ticketId, Guid productLineId, List<Guid> preparationsIds)
        {
            var result = new ValidationContractResult();

            try
            {
                var ticket = LoadTicket(ticketId).Contract.As<POSTicketContract>();
                var line = LoadProductLine(productLineId).Contract.As<POSProductLineContract>();
                line.Preparations.Clear();

                foreach (var preparationId in preparationsIds)
                {
                    var preparation = new POSProductLinePreparationContract(Guid.NewGuid(), productLineId, string.Empty, preparationId, string.Empty);
                    line.Preparations.Add(preparation);
                }

                return ChangeTicketProductDetails(ticket, productLineId, line);
            }
            catch (Exception ex)
            {
                logsFactory.Error(ex);
                result.AddException(ex);
            }

            return result;
        }

        public ValidationContractResult RemoveOrderPreparation(Guid ticketId, Guid productLineId, Guid preparationId)
        {
            var result = new ValidationContractResult();

            try
            {
                var ticket = LoadTicket(ticketId).Contract.As<POSTicketContract>();
                var line = LoadProductLine(productLineId).Contract.As<POSProductLineContract>();
                line.Preparations.Remove(x => x.PreparationId == preparationId);

                return ChangeTicketProductDetails(ticket, productLineId, line);
            }
            catch (Exception ex)
            {
                logsFactory.Error(ex);
                result.AddException(ex);
            }

            return result;
        }

        public ValidationContractResult ChangeTicketProductDetails(POSTicketContract ticket, Guid productLineId, POSProductLineContract editedLine, bool voidCanceledOrder = false)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var productLine = ticket.ProductLineByTicket.First(pl => (Guid)pl.Id == productLineId);

                    #region Quantity Adjustment

                    var quantity = editedLine.ProductQtd;
                    if (productLine.ProductQtd != quantity)
                    {
                        #region Add more items

                        if (quantity > productLine.ProductQtd)
                        {
                            if (productLine.SendToAreaStatus != 0 || productLine.TableLines.Any(x => x.SendToAreaStatus != 0))
                            {
                                var newLine = productLine.CreateCopy((Guid)ticket.Id, (newLine, oldLine) =>
                                {
                                    // Clear the area status
                                    newLine.SendToAreaStatus = 0;
                                    newLine.SendToAreaStartTime = null;
                                    newLine.SendToAreaEndTime = null;
                                    newLine.AreaId = null;
                                    newLine.Areas = [];
                                });

                                // Clear the area status from table lines
                                foreach (var newTableLine in newLine.TableLines)
                                {
                                    newTableLine.Id = Guid.NewGuid();
                                    newTableLine.SendToAreaStatus = 0;
                                    newTableLine.AreaId = null;
                                    newTableLine.Areas = [];

                                    foreach (var tablePreparationLine in newTableLine.TableLinePreparations)
                                    {
                                        tablePreparationLine.Id = Guid.NewGuid();
                                        tablePreparationLine.TableLineId = (Guid)newTableLine.Id;
                                    }
                                }

                                newLine.ProductLineAdjustmentByQuantity(quantity - productLine.ProductQtd);
                                ticket.ProductLineByTicket.Add(newLine);
                            }
                            // No dispatch, just adjust quantity
                            else
                                productLine.ProductLineAdjustmentByQuantity(quantity);
                        }

                        #endregion

                        #region Remove some items

                        else
                        {
                            // Quantity 0, Anul line
                            if (quantity == 0)
                            {
                                productLine.AnnulmentCode = productLine.Printed > 0 ? ProductLineCancellationStatus.CancelledAfterPrint : ProductLineCancellationStatus.Cancelled;
                                productLine.AnnulUserId ??= sessionContext.UserId;
                            }
                            else
                            {
                                // Create annul line
                                var cancelLine = productLine.CreateCopy((Guid)ticket.Id, (newLineContract, _) =>
                                {
                                    newLineContract.AnnulmentCode = productLine.Printed > 0 ? ProductLineCancellationStatus.CancelledAfterPrint : ProductLineCancellationStatus.Cancelled;
                                    newLineContract.AnnulUserId ??= sessionContext.UserId;
                                });

                                cancelLine.ProductLineAdjustmentByQuantity(productLine.ProductQtd - quantity);
                                productLine.ProductLineAdjustmentByQuantity(quantity);

                                ticket.ProductLineByTicket.Add(cancelLine);
                            }
                        }

                        #endregion
                    }

                    #endregion

                    #region Comments Adjustments

                    if (editedLine.Observations != productLine.Observations)
                        productLine.Observations = editedLine.Observations;

                    foreach (var tableLine in productLine.TableLines)
                    {
                        var editedTableLine = editedLine.TableLines.FirstOrDefault(x => (Guid)x.Id == (Guid)tableLine.Id);
                        if (editedTableLine != null)
                        {
                            tableLine.Observations = editedTableLine.Observations;
                        }
                    }

                    #endregion

                    #region Preparations

                    productLine.Preparations = editedLine.Preparations;
                    foreach (var tableLine in productLine.TableLines)
                    {
                        var editedTableLine = editedLine.TableLines.FirstOrDefault(x => (Guid)x.Id == (Guid)tableLine.Id);
                        if (editedTableLine != null)
                        {
                            tableLine.TableLinePreparations = editedTableLine.TableLinePreparations;
                        }
                    }

                    #endregion

                    #region Discount

                    if (productLine.DiscountTypeId != editedLine.DiscountTypeId || productLine.DiscountValue != editedLine.DiscountValue)
                    {
                        productLine.DiscountTypeId = editedLine.DiscountTypeId;
                        productLine.DiscountPercent = editedLine.DiscountPercent;
                        var taxSchemaId = ticket.TaxSchemaId ?? sessionContext.TaxSchemaId;
                        result.Add(LoadTaxesData(productLine, sessionContext.Country, taxSchemaId, sessionContext.InstallationId, ticket.TaxIncluded, ticket));
                    }

                    #endregion

                    #region Separator

                    productLine.Separator = editedLine.Separator;
                    productLine.SeparatorDescription = editedLine.SeparatorDescription;
                    productLine.SeparatorOrder = editedLine.SeparatorOrder;

                    #endregion

                    if (result.IsEmpty)
                    {
                        result = PersistTicket(ticket);
                    }

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();

                var canceledProductLines = ticket.ProductLineByTicket.Where(x =>
                    (x.AnnulmentCode == ProductLineCancellationStatus.CancelledAfterPrint || x.AnnulmentCode == ProductLineCancellationStatus.Cancelled) && x.HasAreas);

                if (canceledProductLines.Any())
                {
                    var contract = result.Contract.As<POSTicketContract>();

                    foreach (var canceledProductLine in canceledProductLines)
                    {
                        var productLine = contract.ProductLineByTicket.FirstOrDefault(x => (Guid)x.Id == (Guid)canceledProductLine.Id);
                        if (productLine == null) continue;
                        productLine.Areas = canceledProductLine.Areas;
                    }

                    if (voidCanceledOrder)
                    {
                        if (canceledProductLines.Any())
                        {
                            var dispatchContracts = canceledProductLines.Select(cancelLine => new DispatchContract
                            {
                                OrderId = (Guid)cancelLine.Id,
                                AreaIds = cancelLine.Areas.Select(x => x.AreaId).ToArray(),
                                OrderIds = cancelLine.TableLines.Select(x => new DispatchContract
                                {
                                    OrderId = (Guid)x.Id,
                                    AreaIds = x.Areas.Select(x => x.AreaId).ToArray()
                                }).ToArray()
                            }).ToArray();

                            result = VoidDispatchTicketOrders((Guid)ticket.Id, dispatchContracts, true, true, true);
                        }
                    }
                }
            }

            return result;
        }

        public ValidationContractResult SetTableToTicket(Guid ticketId, Guid? tableId)
        {
            var result = new ValidationContractResult();

            try
            {
                var ticket = LoadTicket(ticketId).Contract.As<POSTicketContract>();
                result = MoveTicketToTable(ticket, tableId);
            }
            catch (Exception ex)
            {
                logsFactory.Error(ex);
                result.AddException(ex);
            }

            return result;
        }

        #region Orders Dispatch

        public ValidationContractResult DispatchTicketOrders(POSTicketContract ticket, List<Guid> orderIds)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    foreach (var productLine in ticket.ProductLineByTicket.Where(x => orderIds.Contains((Guid)x.Id)))
                    {
                        if (!productLine.TableLines.IsEmpty)
                        {
                            foreach (var tableLine in productLine.TableLines)
                            {
                                if (!tableLine.HasAreas) continue;
                                {
                                    var query = CommonQueryFactory.DeleteProductTableLineAreasQuery(Manager);
                                    query.Parameters["TABL_PK"] = tableLine.Id;
                                    query.Execute();
                                }

                                foreach (var area in tableLine.Areas)
                                {
                                    var query = CommonQueryFactory.InsertProductTableLineAreaQuery(Manager);
                                    query.Parameters["TABA_PK"] = area.Id;
                                    query.Parameters["TABL_PK"] = tableLine.Id;
                                    query.Parameters["AREA_PK"] = area.AreaId;
                                    query.Execute();
                                }

                                // Put table line as dispatched
                                tableLine.SendToAreaStatus = (short)(tableLine.SendToAreaStatus == 0 ? 1 : tableLine.SendToAreaStatus);
                            }
                        }
                        else if (productLine.HasAreas)
                        {
                            {
                                var query = CommonQueryFactory.DeleteProductLineAreasQuery(Manager);
                                query.Parameters["ARVE_PK"] = productLine.Id;
                                query.Execute();
                            }

                            foreach (var area in productLine.Areas)
                            {
                                var query = CommonQueryFactory.InsertProductLineAreaQuery(Manager);
                                query.Parameters["ARVA_PK"] = area.Id;
                                query.Parameters["ARVE_PK"] = productLine.Id;
                                query.Parameters["AREA_PK"] = area.AreaId;
                                query.Execute();
                            }

                            // Put product line as dispatched
                            if (productLine.SendToAreaStatus == 0)
                                productLine.SendToAreaStartTime = DateTime.UtcNow;
                            productLine.SendToAreaStatus = 1;
                        }
                        else
                        {
                            result.AddError("No area defined for: " + productLine.ProductDescription);
                            break;
                        }
                    }

                    if (result.IsEmpty)
                        result = PersistTicket(ticket);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult DispatchTicketOrders(Guid ticketId, DispatchContract[] orders, bool handlePrint = false)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var loadTicketResult = LoadTicket(ticketId);
                    if (loadTicketResult.IsEmpty)
                    {
                        var ticket = loadTicketResult.Contract.As<POSTicketContract>();

                        if (handlePrint)
                            result.AddValidations(PrintOrdersBySpooler(ticket, orders, "ORDER"));

                        if (result.IsEmpty)
                        {
                            foreach (var order in orders)
                            {
                                // Get the product line for the dispatch order
                                var productLine = ticket.ProductLineByTicket.FirstOrDefault(x => (Guid)x.Id == order.OrderId);

                                if (productLine == null) continue;

                                if (order.OrderIds.Length == 0 && productLine.TableLines.IsEmpty)
                                {
                                    {
                                        var query = CommonQueryFactory.DeleteProductLineAreasQuery(Manager);
                                        query.Parameters["ARVE_PK"] = productLine.Id;
                                        query.Execute();
                                    }
                                    foreach (var areaId in order.AreaIds)
                                    {
                                        productLine.Areas.Add(new POSProductLineAreaContract
                                        {
                                            AreaId = areaId
                                        });

                                        var query = CommonQueryFactory.InsertProductLineAreaQuery(Manager);
                                        query.Parameters["ARVA_PK"] = Guid.NewGuid();
                                        query.Parameters["ARVE_PK"] = productLine.Id;
                                        query.Parameters["AREA_PK"] = areaId;
                                        query.Execute();
                                    }

                                    if (productLine.Areas.IsEmpty) continue;

                                    // Put product line as dispatched
                                    if (productLine.SendToAreaStatus == 0)
                                        productLine.SendToAreaStartTime = DateTime.UtcNow;
                                    productLine.SendToAreaStatus = 1;
                                    productLine.AreaId = productLine.Areas[0].AreaId;
                                }
                                else
                                {
                                    foreach (var orderId in order.OrderIds)
                                    {
                                        var tableLine = productLine.TableLines.FirstOrDefault(x => (Guid)x.Id == orderId.OrderId);

                                        if (tableLine == null) continue;

                                        {
                                            var query = CommonQueryFactory.DeleteProductTableLineAreasQuery(Manager);
                                            query.Parameters["TABL_PK"] = tableLine.Id;
                                            query.Execute();
                                        }

                                        foreach (var areaId in orderId.AreaIds)
                                        {
                                            tableLine.Areas.Add(new POSProductLineAreaContract
                                            {
                                                AreaId = areaId
                                            });

                                            var query = CommonQueryFactory.InsertProductTableLineAreaQuery(Manager);
                                            query.Parameters["TABA_PK"] = Guid.NewGuid();
                                            query.Parameters["TABL_PK"] = tableLine.Id;
                                            query.Parameters["AREA_PK"] = areaId;
                                            query.Execute();
                                        }

                                        if (tableLine.Areas.IsEmpty) continue;

                                        // Put table line as dispatched
                                        tableLine.SendToAreaStatus = 1;
                                        tableLine.AreaId = tableLine.Areas[0].AreaId;
                                    }
                                }
                            }

                            result = PersistTicket(ticket);
                        }
                    }
                    else
                        result.Add(loadTicketResult);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult VoidDispatchTicketOrders(POSTicketContract ticket, List<Guid> orderIds)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    foreach (var productLine in ticket.ProductLineByTicket.Where(x => orderIds.Contains((Guid)x.Id)))
                    {
                        if (productLine.TableLines.IsEmpty)
                        {
                            // Clear product line dispatch
                            productLine.AreaId = null;
                            productLine.Areas.Clear();
                            productLine.SendToAreaStatus = 0;
                            productLine.SendToAreaStartTime = null;
                            productLine.SendToAreaEndTime = null;

                            var query = CommonQueryFactory.DeleteProductLineAreasQuery(Manager);
                            query.Parameters["ARVE_PK"] = productLine.Id;
                            query.Execute();
                        }
                        else
                        {
                            foreach (var tableLine in productLine.TableLines)
                            {
                                // Clear table line dispatch
                                if (tableLine.HasAreas) continue;

                                tableLine.AreaId = null;
                                tableLine.Areas.Clear();
                                tableLine.SendToAreaStatus = 0;

                                var query = CommonQueryFactory.DeleteProductTableLineAreasQuery(Manager);
                                query.Parameters["TABL_PK"] = tableLine.Id;
                                query.Execute();
                            }
                        }
                    }

                    result = PersistTicket(ticket);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult VoidDispatchTicketOrders(Guid ticketId, DispatchContract[] orders, bool handlePrint = false, bool cancel = false, bool persist = true)
        {
            var result = new ValidationContractResult();

            var loadTicketResult = LoadTicket(ticketId);
            if (loadTicketResult.IsEmpty)
            {
                var ticket = loadTicketResult.Contract.As<POSTicketContract>();
                result = VoidDispatchTicketOrders(ticket, orders, handlePrint, cancel, persist);
            }
            else
                result.Add(loadTicketResult);

            return result;
        }

        private ValidationContractResult VoidDispatchTicketOrders(POSTicketContract ticket, DispatchContract[] orders, bool handlePrint = false, bool cancel = false, bool persist = true)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {

                    if (handlePrint)
                        result.AddValidations(PrintOrdersBySpooler(ticket, orders, "VOID"));

                    if (result.IsEmpty)
                    {
                        foreach (var productLineOrder in orders)
                        {
                            // Get the product line for the dispatch order
                            var productLine = ticket.ProductLineByTicket.FirstOrDefault(x => (Guid)x.Id == productLineOrder.OrderId);

                            if (productLine == null) continue;

                            if (productLineOrder.OrderIds.Length == 0 && productLine.TableLines.IsEmpty)
                            {
                                // Clear product line dispatch
                                productLine.AreaId = null;
                                productLine.Areas.Clear();
                                productLine.SendToAreaStatus = 0;
                                productLine.SendToAreaStartTime = null;
                                productLine.SendToAreaEndTime = null;

                                var query = CommonQueryFactory.DeleteProductLineAreasQuery(Manager);
                                query.Parameters["ARVE_PK"] = productLine.Id;
                                query.Execute();
                            }
                            else
                            {
                                foreach (var tableLineOrder in productLineOrder.OrderIds)
                                {
                                    var tableLine = productLine.TableLines.FirstOrDefault(x => (Guid)x.Id == tableLineOrder.OrderId);

                                    if (tableLine == null) continue;

                                    // Clear table line dispatch
                                    tableLine.AreaId = null;
                                    tableLine.Areas.Clear();
                                    tableLine.SendToAreaStatus = 0;

                                    var query = CommonQueryFactory.DeleteProductTableLineAreasQuery(Manager);
                                    query.Parameters["TABL_PK"] = tableLine.Id;
                                    query.Execute();
                                }
                            }

                            if (!cancel) continue;
                            productLine.AnnulmentCode = productLine.Printed > 0 ? ProductLineCancellationStatus.CancelledAfterPrint : ProductLineCancellationStatus.Cancelled;
                            productLine.AnnulUserId ??= sessionContext.UserId;
                        }

                        if (persist)
                        {
                            result = PersistTicket(ticket);
                        }
                    }

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult AwayDispatchTicketOrders(POSTicketContract ticket, List<DispatchContract> orderIds)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    foreach (var orderId in orderIds)
                    {
                        var productLine = ticket.ProductLineByTicket.FirstOrDefault(x => (Guid)x.Id == orderId.OrderId);
                        if (productLine == null) continue;
                        {
                            foreach (var orderTableId in orderId.OrderIds)
                            {
                                var tableLine = productLine.TableLines.FirstOrDefault(x => (Guid)x.Id == orderTableId.OrderId);
                                if (tableLine == null) continue;
                                // Put table line as away
                                tableLine.SendToAreaStatus = 5;
                            }

                            // Put product line as away
                            productLine.SendToAreaStatus = 5;
                        }
                    }

                    if (result.IsEmpty)
                        result = PersistTicket(ticket);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult AwayDispatchTicketOrders(Guid ticketId, DispatchContract[] orders, bool handlePrint = false, bool cancel = false, bool persist = true)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var loadTicketResult = LoadTicket(ticketId);
                    if (loadTicketResult.IsEmpty)
                    {
                        var ticket = loadTicketResult.Contract.As<POSTicketContract>();

                        if (handlePrint)
                            result.AddValidations(PrintOrdersBySpooler(ticket, orders, "AWAY"));

                        if (result.IsEmpty)
                        {
                            foreach (var order in orders)
                            {
                                // Get the product line for the dispatch order
                                var productLine = ticket.ProductLineByTicket.FirstOrDefault(x => (Guid)x.Id == order.OrderId);

                                if (productLine == null) continue;

                                if (order.OrderIds.Length == 0 && productLine.TableLines.IsEmpty)
                                {
                                    {
                                        var query = CommonQueryFactory.DeleteProductLineAreasQuery(Manager);
                                        query.Parameters["ARVE_PK"] = productLine.Id;
                                        query.Execute();
                                    }
                                    foreach (var areaId in order.AreaIds)
                                    {
                                        productLine.Areas.Add(new POSProductLineAreaContract
                                        {
                                            AreaId = areaId
                                        });

                                        var query = CommonQueryFactory.InsertProductLineAreaQuery(Manager);
                                        query.Parameters["ARVA_PK"] = Guid.NewGuid();
                                        query.Parameters["ARVE_PK"] = productLine.Id;
                                        query.Parameters["AREA_PK"] = areaId;
                                        query.Execute();
                                    }

                                    if (productLine.Areas.IsEmpty) continue;

                                    // Put product line as away
                                    if (productLine.SendToAreaStatus == 0)
                                        productLine.SendToAreaStartTime = DateTime.UtcNow;
                                    productLine.SendToAreaStatus = 5;
                                    productLine.AreaId = productLine.Areas[0].AreaId;
                                }
                                else
                                {
                                    foreach (var orderId in order.OrderIds)
                                    {
                                        var tableLine = productLine.TableLines.FirstOrDefault(x => (Guid)x.Id == orderId.OrderId);

                                        if (tableLine == null) continue;

                                        {
                                            var query = CommonQueryFactory.DeleteProductTableLineAreasQuery(Manager);
                                            query.Parameters["TABL_PK"] = tableLine.Id;
                                            query.Execute();
                                        }

                                        foreach (var areaId in orderId.AreaIds)
                                        {
                                            tableLine.Areas.Add(new POSProductLineAreaContract
                                            {
                                                AreaId = areaId
                                            });

                                            var query = CommonQueryFactory.InsertProductTableLineAreaQuery(Manager);
                                            query.Parameters["TABA_PK"] = Guid.NewGuid();
                                            query.Parameters["TABL_PK"] = tableLine.Id;
                                            query.Parameters["AREA_PK"] = areaId;
                                            query.Execute();
                                        }

                                        if (tableLine.Areas.IsEmpty) continue;

                                        // Put table line as away
                                        tableLine.SendToAreaStatus = 5;
                                        tableLine.AreaId = tableLine.Areas[0].AreaId;
                                    }
                                }
                            }

                            result = PersistTicket(ticket);
                        }
                    }
                    else
                        result.Add(loadTicketResult);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        #endregion

        /// <summary>
        /// Manually splits a ticket by separating selected orders into a new ticket.
        /// </summary>
        /// <param name="ticketId">The ID of the ticket to split.</param>
        /// <param name="ordersToLeave">A collection of key-value pairs where each key is an order ID and the value is the decimal quantity to leave on the original ticket.</param>
        /// <returns>A SplitTicketResult object containing the original and new ticket details, or error information if the split failed.</returns>
        /// <remarks>
        /// This method involves several key steps:
        /// - Opening a database connection and beginning a transaction to ensure data consistency.
        /// - Loading the original ticket and checking if it is valid.
        /// - Creating a new ticket based on the original one's settings.
        /// - Iterating over the provided orders to leave, and splitting each as specified:
        ///     - Orders that match the specified quantity are left on the original ticket.
        ///     - Orders with differing quantities are split between the original and new tickets.
        ///     - Annulment codes are adjusted to reflect the split.
        ///     - Quantity adjustments are made to both the original and new ticket lines.
        /// - Persisting the changes to both the original and the new tickets.
        /// - Handling exceptions and rolling back the transaction in case of failure.
        /// - Finally, closing the database connection.
        /// </remarks>
        public SplitTicketResult SplitTicketManual(Guid ticketId, KeyValuePair<Guid, decimal>[] ordersToLeave)
        {
            var result = new SplitTicketResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var settingBusiness = new SettingBusiness(Manager);
                    var generalSettings = settingBusiness.GetGeneralSettings(sessionContext.InstallationId);

                    var loadTicketResult = LoadTicket(ticketId);
                    if (loadTicketResult.IsEmpty)
                    {
                        var originalTicket = loadTicketResult.Contract.As<POSTicketContract>();

                        var standBusiness = new StandBusiness(Manager, sessionContext, null, ticketRepository, _validatorFactory);
                        var stand = standBusiness.GetStandById(sessionContext.StandId, sessionContext.LanguageId);

                        // If the bottom parameter is active, then keep the new ticket with same table as the original ticket 
                        Guid? tableId = null;
                        if (stand.KeepTicketsInTableOnManualSplit)
                            tableId = originalTicket.Mesa;

                        var newTicketResult = NewTicket((Guid)generalSettings.Id, originalTicket.Stand, originalTicket.Caja, tableId, originalTicket.UserId);
                        if (newTicketResult.IsEmpty)
                        {
                            var newTicketContract = newTicketResult.Contract.As<POSTicketContract>();
                            newTicketContract.OpeningDate = sessionContext.WorkDate;
                            newTicketContract.OpeningTime = DateTime.Now;

                            foreach (var order in ordersToLeave)
                            {
                                var line = originalTicket.ProductLineByTicket.FirstOrDefault(x => (Guid)x.Id == order.Key);
                                if (line == null) continue;

                                #region New Ticket Line

                                // Create a copy from the original product line and then annul it
                                var newTicketNewLine = line.CreateCopy((Guid)newTicketContract.Id, (newLineContract, oldLineContract) =>
                                {
                                    // Change prop to new product line
                                    newLineContract.AnnulmentCode = ProductLineCancellationStatus.Active;
                                    // Change props to original prod line
                                    oldLineContract.AnnulmentCode = ProductLineCancellationStatus.CancelledBySplit;
                                    oldLineContract.AnnulUserId ??= sessionContext.UserId;
                                });
                                newTicketContract.ProductLineByTicket.Add(newTicketNewLine);

                                #endregion

                                // If the quantity is the same is not needed to create a new product line as a replacement of the old one
                                if (line.ProductQtd == order.Value) continue;

                                #region Original Ticket Line

                                var originalNewLine = line.CreateCopy((Guid)originalTicket.Id, (newProductLine, _) =>
                                {
                                    newProductLine.AnnulmentCode = ProductLineCancellationStatus.Active;
                                });
                                originalTicket.ProductLineByTicket.Add(originalNewLine);

                                #endregion

                                #region New Ticket Calculation

                                newTicketNewLine.ProductLineAdjustmentByQuantity(order.Value);
                                newTicketNewLine.AnnulmentCode = ProductLineCancellationStatus.Active;

                                #endregion

                                #region Original Ticket Adjustments

                                originalNewLine.ProductLineAdjustmentByQuantity(originalNewLine.ProductQtd - order.Value);
                                originalNewLine.AnnulmentCode = ProductLineCancellationStatus.Active;

                                #endregion
                            }

                            var persistOriginalTicketResult = PersistTicket(originalTicket);
                            if (persistOriginalTicketResult.IsEmpty)
                            {
                                originalTicket = persistOriginalTicketResult.Contract.As<POSTicketContract>();

                                var persistNewTicketResult = PersistTicket(newTicketContract);
                                if (persistNewTicketResult.IsEmpty)
                                {
                                    newTicketContract = persistNewTicketResult.Contract.As<POSTicketContract>();

                                    result.Tickets.Add(originalTicket);
                                    result.Tickets.Add(newTicketContract);
                                }
                                else
                                    result.Add(persistNewTicketResult);
                            }
                            else
                                result.Add(persistOriginalTicketResult);
                        }
                        else
                            result.Add(newTicketResult);
                    }
                    else
                        result.Add(loadTicketResult);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public SplitTicketResult SplitTicketAuto(Guid ticketId, int quantity)
        {
            var result = new SplitTicketResult();

            Manager.Open();
            Manager.BeginTransaction();
            try
            {
                var loadTicketResult = LoadTicket(ticketId);
                if (loadTicketResult.IsEmpty)
                {
                    var originalTicket = loadTicketResult.Contract.As<POSTicketContract>();
                    var productLines = originalTicket.ProductLineByTicket.Where(pl => pl.IsActive).ToList();

                    if (productLines.Any())
                    {
                        var taxSchemaId = originalTicket.TaxSchemaId ?? sessionContext.TaxSchemaId;

                        #region Create New Tickets

                        if (result.IsEmpty)
                        {
                            var standBusiness = new StandBusiness(Manager, sessionContext, null, ticketRepository, _validatorFactory);
                            var stand = standBusiness.GetStandById(sessionContext.StandId, sessionContext.LanguageId);

                            Guid? tableId = null;
                            // Si está activado el parámetro de mantener el nuevo ticket en la mesa
                            if (stand.KeepTicketsInTableOnManualSplit)
                                tableId = originalTicket.Mesa;

                            for (var i = 0; i < quantity - 1; i++)
                            {
                                var newTicketResult = NewTicket(sessionContext.InstallationId, originalTicket.Stand, originalTicket.Caja, tableId, originalTicket.UserId);

                                if (newTicketResult.IsEmpty)
                                {
                                    var newTicket = newTicketResult.Contract.As<POSTicketContract>();
                                    newTicket.OpeningDate = sessionContext.WorkDate;
                                    newTicket.OpeningTime = DateTime.Now;

                                    foreach (var productLine in productLines)
                                    {
                                        var newProductLine = productLine.CreateCopy((Guid)newTicket.Id, (newLine, oldLine) =>
                                        {
                                            // Active new product line
                                            newLine.AnnulmentCode = ProductLineCancellationStatus.Active;

                                            // TODO: See it and fix it, cause it's not calculating the taxes.
                                            // newLine.AdjustProductLineValuesForSplit(quantity);

                                            newLine.ProductQtd = (oldLine.ProductQtd / quantity).Round2();
                                            newLine.ValueBeforeDiscount = (oldLine.ValueBeforeDiscount / quantity).Round2();
                                            newLine.Recharge = (oldLine.Recharge / quantity).Round2();
                                            newLine.DiscountPercent = oldLine.DiscountPercent;
                                        });

                                        var taxResult = LoadTaxesData(
                                            newProductLine, 
                                            sessionContext.Country, 
                                            taxSchemaId, 
                                            sessionContext.InstallationId, 
                                            originalTicket.TaxIncluded, 
                                            originalTicket, 
                                            newProductLine.IsTip
                                        );

                                        if(!taxResult.IsEmpty)
                                        {
                                            foreach (var error in taxResult.Errors)
                                            {
                                                result.AddError(error.Message);
                                            }
                                            return result;
                                        }
                                        else
                                        {
                                            newTicket.ProductLineByTicket.Add(newProductLine);
                                        }
                                    }

                                    var persistNewTicketResult = PersistTicket(newTicket);
                                    if (persistNewTicketResult.IsEmpty)
                                        result.Tickets.Add(newTicket);
                                    else
                                        result.Add(persistNewTicketResult);
                                }
                                else
                                    result.Add(newTicketResult);

                                if (!result.IsEmpty)
                                    break;
                            }
                        }

                        #endregion

                        #region Update Original Ticket

                        foreach (var productLine in productLines)
                        {
                            var newProductLine = productLine.CreateCopy((Guid)originalTicket.Id, (newLine, oldLine) =>
                            {
                                // Active new product line
                                newLine.AnnulmentCode = ProductLineCancellationStatus.Active;

                                // TODO: See it and fix it, cause it's not calculating the taxes.
                                // newLine.AdjustProductLineValuesForSplit(quantity, true);

                                newLine.ProductQtd = oldLine.ProductQtd - (oldLine.ProductQtd / quantity).Round2() * (quantity - 1);
                                newLine.ValueBeforeDiscount =  oldLine.ValueBeforeDiscount - (oldLine.ValueBeforeDiscount / quantity).Round2() * (quantity - 1);
                                newLine.Recharge = oldLine.Recharge - (oldLine.Recharge / quantity).Round2() * (quantity - 1);
                                newLine.DiscountPercent = oldLine.DiscountPercent;

                                // Annul old product line
                                oldLine.AnnulmentCode = ProductLineCancellationStatus.CancelledBySplit;
                                oldLine.AnnulUserId ??= sessionContext.UserId;
                            });

                            var taxResult = LoadTaxesData(
                                newProductLine,
                                sessionContext.Country,
                                taxSchemaId,
                                sessionContext.InstallationId,
                                originalTicket.TaxIncluded,
                                originalTicket,
                                newProductLine.IsTip
                            );

                            if (!taxResult.IsEmpty)
                            {
                                foreach (var error in taxResult.Errors)
                                {
                                    result.AddError(error.Message);
                                }
                                return result;
                            }
                            else
                            {
                                originalTicket.ProductLineByTicket.Add(newProductLine);
                            }
                        }

                        var persistOriginalTicketResult = PersistTicket(originalTicket);
                        if (persistOriginalTicketResult.IsEmpty)
                        {
                            originalTicket = persistOriginalTicketResult.Contract.As<POSTicketContract>();
                            result.Tickets.Add(originalTicket);
                        }
                        else
                            result.Add(persistOriginalTicketResult);

                        #endregion
                    }
                }
                else
                    result.Add(loadTicketResult);

                Manager.EndTransaction(result);
            }
            catch (Exception ex)
            {
                Manager.RollbackTransaction();
                logsFactory.Error(ex);
                result.AddException(ex);
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public SplitTicketResult SplitTicketByAlcoholicGroup(Guid ticketId)
        {
            var result = new SplitTicketResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var loadTicketResult = LoadTicket(ticketId);
                    if (loadTicketResult.IsEmpty)
                    {
                        var originalTicket = loadTicketResult.Contract.As<POSTicketContract>();
                        var newTicketResult = NewTicket(sessionContext.InstallationId, originalTicket.Stand, originalTicket.Caja, originalTicket.Mesa, originalTicket.UserId);
                        if (newTicketResult.IsEmpty)
                        {
                            var newTicket = newTicketResult.Contract.As<POSTicketContract>();
                            newTicket.OpeningDate = sessionContext.WorkDate;
                            newTicket.OpeningTime = DateTime.Now;

                            var productLines = originalTicket.ProductLineByTicket
                                .Where(pl => (pl.Alcoholic ?? false) && pl.IsActive);

                            if (productLines.Any())
                            {
                                foreach (var productLine in productLines)
                                {
                                    #region Create New Line

                                    var newProductLine = productLine.Clone<POSProductLineContract>();
                                    newProductLine.Id = Guid.NewGuid();
                                    newProductLine.AnnulmentCode = ProductLineCancellationStatus.Active;
                                    newTicket.ProductLineByTicket.Add(newProductLine);

                                    #endregion

                                    #region Void Original Line

                                    productLine.AnnulmentCode = ProductLineCancellationStatus.CancelledBySplit;
                                    productLine.AnnulUserId = sessionContext.UserId;

                                    #endregion
                                }

                                var persistNewTicketResult = PersistTicket(newTicket);
                                if (persistNewTicketResult.IsEmpty)
                                {
                                    var persistOriginalTicketResult = PersistTicket(originalTicket);
                                    if (persistOriginalTicketResult.IsEmpty)
                                    {
                                        result.Tickets.Add(persistOriginalTicketResult.Contract.As<POSTicketContract>());
                                        result.Tickets.Add(persistNewTicketResult.Contract.As<POSTicketContract>());
                                    }
                                    else
                                        result.Add(persistOriginalTicketResult);
                                }
                                else
                                    result.Add(persistNewTicketResult);
                            }
                            else
                                result.AddError("Not found alcoholic orders to split");
                        }
                        else
                            result.Add(newTicketResult);
                    }
                    else
                        result.Add(loadTicketResult);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationResult PersistentPaymentLine(PaymentLineContract contract)
        {
            var result = new ValidationResult();
            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var paymentLine = new PaymentLine(Manager);

                    if (contract.Id != null && contract.Id.Equals(Guid.Empty))
                        paymentLine.LoadObject(contract.Id);

                    paymentLine.SetValues(contract.GetValues());

                    paymentLine.SaveObject();
                    Manager.CommitTransaction();

                    result = new ValidationResult();
                    result.Extra = paymentLine.Id.ToString();
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult LoadProductLine(Guid productLineId)
        {
            var result = new ValidationContractResult();

            try
            {
                var productLine = new ProductLine(Manager, productLineId);
                var contract = new POSProductLineContract();
                contract.SetValues(productLine.GetValues());

                var preparationList = GetProductLinePreparations(productLineId, sessionContext.LanguageId);
                foreach (IRecord record in preparationList)
                {
                    var preparationContract = new POSProductLinePreparationContract
                    {
                        Id = record.ValueAs<Guid>("arvp_pk"),
                        PreparationDescription = record.ValueAs<string>("prep_desc"),
                        PreparationId = record.ValueAs<Guid>("prep_pk"),
                        ProductDescription = record.ValueAs<string>("artg_desc"),
                        ProductLineId = record.ValueAs<Guid>("arve_pk")
                    };
                    contract.Preparations.Add(preparationContract);
                }

                result.Contract = contract;
            }
            catch (Exception ex)
            {
                logsFactory.Error(ex);
                result.AddException(ex);
            }

            return result;
        }

        public ValidationContractResult LoadPaymentLine(Guid paymentLineId)
        {
            PaymentLineContract contract;
            if (!ContractFactory.TryGet(paymentLineId, out contract))
            {
                Manager.Open();
                try
                {
                    var paymentLine = new PaymentLine(Manager, paymentLineId);
                    contract = new PaymentLineContract();
                    contract.SetValues(paymentLine.GetValues());
                    ContractFactory.Add(contract);

                    return new ValidationContractResult(contract);
                }
                finally
                {
                    Manager.Close();
                }
            }
            else
                return new ValidationContractResult(contract);
        }

        public ValidationResult DeleteProductLine(string productLineId)
        {
            var result = new ValidationResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var productLine = new ProductLine(Manager, productLineId);
                    productLine.DeleteObject();

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationResult CancelProductLine(string productLineId)
        {
            var result = new ValidationResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var productLine = new ProductLine(Manager, productLineId);
                    productLine.AnnulmentCode = ProductLineCancellationStatus.Cancelled;
                    productLine.AnnulUserId ??= sessionContext.UserId;
                    productLine.SaveObject();
                    result.Extra = productLine.LastModified.Ticks.ToString();

                    var settings = settingBusiness.GetGeneralSettings(sessionContext.InstallationId);

                    if (settings.RealTimeTicketSync)
                        Ticket.UpdateSync(Manager, productLine.Ticket);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationResult DeletePaymentLine(string paymentLineId)
        {
            var result = new ValidationResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var paymentLine = new PaymentLine(Manager, paymentLineId);
                    paymentLine.DeleteObject();

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationResult CancelPaymentLine(string paymentLineId)
        {
            var result = new ValidationResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var paymentLine = new PaymentLine(Manager, paymentLineId);
                    paymentLine.AnnulmentCode = ProductLineCancellationStatus.Cancelled;
                    paymentLine.SaveObject();

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        private ValidationContractResult SetRateType(POSTicketContract ticket, RateType rateType, decimal discountPercent = 0)
        {
            var result = new ValidationContractResult();

            try
            {
                foreach (var productLineContract in ticket.ProductLineByTicket)
                {
                    if (productLineContract.IsHappyHour || productLineContract.HasManualPrice || productLineContract.IsTip ||
                        productLineContract.AnnulmentCode != 0 && productLineContract.AnnulmentCode != 2)
                        continue;

                    var product = GetProductRecordForTicketCreation(productLineContract.Product);

                    var basePrice = rateType switch
                    {
                        RateType.Standard => product.StandardPrice ?? decimal.Zero,
                        RateType.InternalUse => product.HouseUsePrice ?? product.StandardPrice ?? decimal.Zero,
                        RateType.PensionMode => GeneralSetting.LeaveMealPlanOpened
                            ? product.MealPlanPrice ?? product.StandardPrice ?? decimal.Zero
                            : product.StandardPrice ?? decimal.Zero,
                        _ => product.StandardPrice ?? decimal.Zero
                    } * productLineContract.ProductQtd;

                    productLineContract.NetValue = basePrice * (1 - discountPercent / 100);
                    productLineContract.GrossValue = basePrice * (1 - discountPercent / 100);
                    productLineContract.ValueBeforeDiscount = basePrice * (1 - discountPercent / 100);

                    // Recalculate Discount and Taxs;
                    var taxSchemaId = ticket.TaxSchemaId ?? sessionContext.TaxSchemaId;
                    result.Add(LoadTaxesData(productLineContract, sessionContext.Country, taxSchemaId,
                        sessionContext.InstallationId, ticket.TaxIncluded, ticket));
                }

                ticket.Total = ticket.ProductLineByTicket.Where(pl => pl.IsActive).Sum(pl => pl.GrossValue);
                ticket.Total += ticket.TipValue ?? 0;

                result.Contract = ticket;
            }
            catch (Exception ex)
            {
                logsFactory.Error(ex);
                result.AddException(ex);
            }

            return result;
        }

        public ValidationResult Sync(Guid ticketId)
        {
            var result = new ValidationResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var ticket = ticketRepository.GetTicket(ticketId);
                    ticket.Sync = DateTime.Now;

                    var settings = settingBusiness.GetGeneralSettings(sessionContext.InstallationId);

                    ticket.SaveObject(settings.RealTimeTicketSync);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationResult UpdateFiscalPrinterDataAsync(Guid ticketId, string fpSerialInvoice,
            long? fpInvoiceNumber, string fpSerialCreditNote, long? fpCreditNoteNumber, bool sync = true)
        {
            var result = new ValidationResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var ticket = ticketRepository.GetTicket(ticketId);
                    ticket.FpSerialInvoice = fpSerialInvoice;
                    ticket.FpInvoiceNumber = fpInvoiceNumber;
                    ticket.FpSerialCreditNote = fpSerialCreditNote;
                    ticket.FpCreditNoteNumber = fpCreditNoteNumber;

                    result.SaveTicket(ticket, sync, _validatorFactory, nameof(UpdateFiscalPrinterDataAsync));

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationResult ValidateTicket(POSTicketContract ticket)
        {
            var result = new ValidationResult();

            ClientRecord client = null;
            if (ticket.BaseEntityId.HasValue)
            {
                var standBusiness = BusinessContext.GetBusiness<IStandBusiness>(Manager);

                var request = new QueryRequest();
                request.AddFilter("client_id_filter", ticket.BaseEntityId.Value);

                var clientsResult = standBusiness.GetClientEntities(request);
                if (clientsResult.IsEmpty)
                    client = clientsResult.ItemSource.FirstOrDefault();
                else
                    result.Add(clientsResult);
            }

            if (result.IsEmpty)
            {
                var validator = new DocValidator(GeneralSetting.MandatoryValueForFiscalNumber)
                {
                    Client = client,
                    Country = ticket.FiscalDocNacionality,
                    DocumentSign = sessionContext.SignatureMode
                };

                result.Add(validator.Validate(ticket));
            }

            return result;
        }

        public ValidationResult UpdateDispatchAreaStatus(Guid ticketId, Guid orderId,
            short orderStatus, long languageId, Guid hotelId, Guid cashierId, Guid standId)
        {
            var result = new ValidationResult();

            Manager.Open();
            try
            {
                int count = 0;

                Manager.BeginTransaction();
                try
                {
                    using (var command = Manager.CreateCommand("UpdateDispatchAreaStatus"))
                    {
                        command.CommandText = "UPDATE TNHT_ARVE SET ARVE_EMAR = :ARVE_EMAR, ARVE_FIMA = :ARVE_FIMA WHERE ARVE_PK = :ARVE_PK";
                        Manager.CreateParameter(command, "ARVE_EMAR", Manager.ConvertValueType(orderStatus));
                        if (orderStatus == 4)
                            Manager.CreateParameter(command, "ARVE_FIMA", Manager.ConvertValueType(DateTime.UtcNow));
                        else
                            Manager.CreateParameter(command, "ARVE_FIMA", Manager.ConvertValueType(DBNull.Value));
                        Manager.CreateParameter(command, "ARVE_PK", Manager.ConvertValueType(orderId));
                        count = Manager.ExecuteNonQuery(command);
                    }

                    var settings = settingBusiness.GetGeneralSettings(hotelId);

                    if (settings.RealTimeTicketSync)
                        Ticket.UpdateSync(Manager, ticketId);

                    Manager.CommitTransaction();
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }

                if (count > 0)
                {
                    var loadTicketResult = LoadTicket(ticketId, languageId, hotelId);
                    if (loadTicketResult.IsEmpty)
                    {
                        var contract = loadTicketResult.Contract.As<POSTicketContract>();
                        if (contract != null)
                        {
                            BusinessContext.Publish("TicketPersisted",
                                new NotificationContract<Guid>
                                {
                                    TokenID = Guid.Empty,
                                    HotelId = hotelId,
                                    CashierId = cashierId,
                                    StandId = standId,
                                    DataContext = (Guid)contract.Id
                                });
                        }
                    }
                    else
                        result.Add(loadTicketResult);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult CreateMenuDigitalTicket(Guid tableId, ProductLineDto[] productLines)
        {
            var installationId = sessionContext.InstallationId;
            var standId = sessionContext.StandId;
            var cashierId = sessionContext.CashierId;
            var userId = sessionContext.UserId;

            var result = NewTicket(installationId, standId, cashierId, tableId, userId);
            if (result.IsEmpty)
            {
                ((POSTicketContract)result.Contract).Paxs = 1;
                ((POSTicketContract)result.Contract).DigitalMenu = true;

                foreach (var line in productLines)
                {
                    var product = GetProductRecordForTicketCreation(line.ProductId);
                    if (product != null)
                    {
                        result = AddProductToTicket(null, (POSTicketContract)result.Contract, product, line.Quantity, null, null, line.Preparations);
                    }
                    else
                        result.AddError("Product not found: " + line.ProductId);
                }
            }
            else
                result.Add(result);

            return result;
        }

        private ValidationResult NotifyRoomService(POSTicketContract contract, short applicationId)
        {
            var result = new ValidationResult();

            if (BusinessContext.ServiceMonitor == null) return result;
            var details = new List<string>();
            details.Add($"COMANDA NUMERO: {contract.SerieName}");
            if (!string.IsNullOrEmpty(contract.Room))
                details.Add($"HABITAÇÃO: {contract.Room}");
            if (!string.IsNullOrEmpty(contract.Name))
                details.Add($"HOSPEDE: {contract.Name}");
            details.AddRange(from productLine in contract.ProductLineByTicket where productLine.AnnulmentCode is ProductLineCancellationStatus.Active or ProductLineCancellationStatus.ActiveByTransfer select $"{productLine.ProductQtd} {productLine.ProductDescription}");

            var response = BusinessContext.ServiceMonitor.Notify(contract.Room, applicationId, string.Join(";", details));
            if (response.serviceNumber > 0)
            {
                contract.RoomServiceNumber = response.serviceNumber.ToString();

                // Update the ticket with response from the Service Monitor
                using (var command = Manager.CreateCommand("UpdateRoomServiceNumber"))
                {
                    command.CommandText = "update tnht_vend set vend_room = :vend_room, vend_rsnu = :vend_rsnu where vend_pk = :vend_pk";
                    Manager.CreateParameter(command, "vend_pk", Manager.ConvertValueType(contract.Id));
                    Manager.CreateParameter(command, "vend_room", Manager.ConvertValueType(contract.Room));
                    Manager.CreateParameter(command, "vend_rsnu", Manager.ConvertValueType(contract.RoomServiceNumber));
                    Manager.ExecuteNonQuery(command);
                }
            }
            else if (!string.IsNullOrEmpty(response.message))
                result.AddError(response.message);
            else
                result.AddError($"Error: {"RoomService".Translate()}");

            return result;
        }

        public ValidationResult<string> NotifyRoomServiceMonitor(POSTicketContract contract)
        {
            var result = new ValidationResult<string>();

            Manager.Open();
            try
            {
                try
                {
                    ReservationRecord? reservation;

                    if (!string.IsNullOrEmpty(contract.Room))
                    {
                        var standBusiness = BusinessContext.GetBusiness<IStandBusiness>(Manager);
                        var reservationsResult = standBusiness.GetReservations(new ReservationFilterContract { RoomNumber = contract.Room });

                        if (reservationsResult.HasErrors)
                        {
                            result.Add(reservationsResult);
                            return result;
                        }

                        reservation = reservationsResult.ItemSource.FirstOrDefault(x => x.Room == contract.Room && x.State == ReservationState.CheckIn);
                    }
                    else
                    {
                        result.AddError("No room specified");
                        return result;
                    }

                    if (reservation == null)
                    {
                        result.AddError($"There is no reservation associated to room: {contract.Room}");
                        return result;
                    }

                    result.Add(NotifyRoomService(contract, 2));

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddError(ex.Message);
                }

                if (result.IsEmpty && contract != null)
                    result.Result = contract.RoomServiceNumber;
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        /// <summary>
        /// Creates a ticket with a list of products and preparations for a room from a given external source
        /// </summary>
        /// <param name="room">The room that ordered</param>
        /// <param name="external">The external source</param>
        /// <param name="productLines">The list of products to be ordered</param>
        /// <returns>A contract with the success or not of the operation</returns>
        public ValidationContractResult CreateTicketRoom(string room, string external, ProductLineDto[] productLines)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                ReservationRecord? reservation;

                if (!string.IsNullOrEmpty(room))
                {
                    var standBusiness = BusinessContext.GetBusiness<IStandBusiness>(Manager);
                    var reservationsResult = standBusiness.GetReservations(new ReservationFilterContract { RoomNumber = room });

                    if (reservationsResult.HasErrors)
                    {
                        result.Add(reservationsResult);
                        return result;
                    }
                    reservation = reservationsResult.ItemSource.FirstOrDefault(x => x.Room == room && x.State == ReservationState.CheckIn);
                }
                else
                {
                    result.AddError("No room specified");
                    return result;
                }

                if (reservation == null)
                {
                    result.AddError($"There is no reservation associated to room: {room}");
                    return result;
                }

                var installationId = sessionContext.InstallationId;
                var standId = sessionContext.StandId;
                var cashierId = sessionContext.CashierId;
                var userId = sessionContext.UserId;

                Manager.BeginTransaction();
                try
                {
                    POSTicketContract? contract = null;
                    var dispatches = new List<DispatchContract>();

                    result = NewTicket(installationId, standId, cashierId, null, userId);
                    if (result.IsEmpty)
                    {
                        contract = result.Contract as POSTicketContract;
                        if (contract != null)
                        {
                            contract.Paxs = 1;
                            contract.Room = room;
                            contract.Name = reservation.Guests;
                            if (!string.IsNullOrEmpty(external)) contract.Name += $" ({external})";
                            contract.Account = reservation.AccountId;
                            contract.AccountType = CurrentAccountType.Reservation;
                            contract.AccountInstallationId = sessionContext.InstallationId;
                            contract.FiscalDocTo = reservation.Guests;

                            contract.AccountDescription = ConcatenateAccountDescription(reservation);
                            contract.AllowRoomCredit = reservation.AllowCreditPos;

                            var newTicketContract = result.Contract.As<POSTicketContract>();
                            foreach (var productLine in productLines)
                            {
                                var product = GetProductRecordForTicketCreation(productLine.ProductId);
                                if (product != null)
                                {
                                    var pr = ValidateResult<POSProductLineContract>(NewProductLine(newTicketContract, product, productLine.Quantity));

                                    // Add the preparations to the product line
                                    foreach (var preparationId in productLine.Preparations)
                                    {
                                        pr.Preparations.Add(new POSProductLinePreparationContract
                                        {
                                            Id = Guid.NewGuid(),
                                            PreparationId = preparationId,
                                            ProductLineId = pr.Product
                                        });
                                    }

                                    // Assign areas
                                    var areasId = GetProductAreasForTicketCreation(productLine.ProductId, standId);
                                    foreach (var areaId in areasId) pr.Areas.Add(new POSProductLineAreaContract(Guid.NewGuid(), areaId));
                                    pr.AreaId = areasId.FirstOrDefault();
                                    pr.SendToAreaStartTime = DateTime.UtcNow;
                                    pr.SendToAreaStatus = 1;
                                    pr.Observations = productLine.Observations;

                                    // If everything is ok, add the product line to the ticket contract
                                    newTicketContract.ProductLineByTicket.Add(pr);
                                }
                                else result.AddError($"Product not found: {productLine.ProductId}");
                                // If there is an error, stop the process
                                if (!result.IsEmpty) break;
                            }
                            // Add a persistent ticket to the result
                            result.Add(PersistTicket(newTicketContract));

                            if (result.IsEmpty)
                            {
                                foreach (var productLine in contract.ProductLineByTicket)
                                {
                                    var areasId = productLine.Areas.Select(a => a.AreaId).ToList();
                                    if (productLine.AreaId.HasValue)
                                        areasId.Add(productLine.AreaId.Value);

                                    if (areasId.Count > 0)
                                        dispatches.Add(new DispatchContract { OrderId = (Guid)productLine.Id, AreaIds = areasId.ToArray() });
                                }

                                result.Add(NotifyRoomService(contract, 1));
                            }
                        }
                    }

                    if (Manager.EndTransaction(result))
                    {
                        // Cashier notification
                        if (contract?.Id != null) BusinessContext.Publish("TicketPersisted", (Guid)contract.Id);

                        // Dispatch all orders calling spooler
                        if (contract != null && dispatches.Count > 0)
                        {
                            using var spoolerClient = new SpoolerServiceClient();
                            spoolerClient.PrintOrders("ORDER", contract, dispatches.ToArray(), false);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    logsFactory.Error(ex);
                    result.AddError(ex.Message);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult ChangeProductLinesSeparator(Guid ticketId, IEnumerable<Guid> productLineIds,
            Guid? separatorId)
        {
            var result = LoadTicket(ticketId);

            if (result.HasErrors) return result;

            // Get ticket contract
            var ticket = result.Contract.As<POSTicketContract>();
            // Get separator
            var separators = settingBusiness.GetSeparators(sessionContext.InstallationId,
                sessionContext.LanguageId.ToString().AsInteger());
            var separator = separators.FirstOrDefault(record => record.Id == separatorId);
            // Change productLines separator info
            foreach (var id in productLineIds)
            {
                var productLine = ticket.ProductLineByTicket.FirstOrDefault(productLine =>
                    productLine.Id.ToString().AsGuid() == id);

                if (productLine == null) continue;

                productLine.Separator = separator?.Id;
                productLine.SeparatorDescription = separator?.Description;
                productLine.SeparatorOrder = separator?.Order;
                productLine.HasManualSeparator = separator != null;
            }

            result = PersistTicket(ticket);

            return result;
        }

        public ValidationContractResult ChangeProductLinesSeat(Guid ticketId, IEnumerable<Guid> productLineIds, short seat)
        {
            var result = LoadTicket(ticketId);

            if (result.HasErrors) return result;

            // Get ticket contract
            var ticket = result.Contract.As<POSTicketContract>();
            // Change productLines seat number
            foreach (var id in productLineIds)
            {
                var productLine = ticket.ProductLineByTicket.FirstOrDefault(productLine =>
                    productLine.Id.ToString().AsGuid() == id);

                if (productLine == null) continue;

                productLine.PaxNumber = seat;
            }

            result = PersistTicket(ticket);

            return result;
        }

        public ValidationContractResult IncrementPrints(Guid ticketId, int title)
        {
            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var sb = new StringBuilder();
                    sb.Append("UPDATE TNHT_VEND SET ");
                    switch (title)
                    {
                        // Invoice
                        case 2:
                        // Ballot
                        case 4:
                            sb.AppendLine("FACT_PRNT = FACT_PRNT + 1");
                            break;
                        // Credit Note
                        case 3:
                            sb.AppendLine("NCRE_PRNT = NCRE_PRNT + 1");
                            break;
                        // Ticket
                        case 1:
                            sb.AppendLine("VEND_PRNT = VEND_PRNT + 1");
                            break;
                        default:
                            return LoadTicket(ticketId);
                    }

                    sb.AppendLine("WHERE VEND_PK = :VEND_PK");

                    using (var command = Manager.CreateCommand("IncrementPrints"))
                    {
                        command.CommandText = sb.ToString();
                        Manager.CreateParameter(command, "VEND_PK", Manager.ConvertValueType(ticketId));
                        Manager.ExecuteNonQuery(command);
                    }

                    var result = LoadTicket(ticketId);
                    Manager.EndTransaction(result);

                    return result;
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
            finally
            {
                Manager.Close();
            }
        }

        public ValidationContractResult CheckOpenTicket(Guid ticketId, Guid standId, Guid cashierId)
        {
            var query = CommonQueryFactory.CheckOpenTicketQuery(Manager);
            query.Parameters["VEND_PK"] = ticketId;
            query.Parameters["IPOS_PK"] = standId;
            query.Parameters["CAJA_PK"] = cashierId;

            var record = query.Execute().FirstOrDefault();
            return record != null
                ? LoadTicket(ticketId)
                : new ValidationContractResult
                {
                    Contract = null
                };
        }

        #endregion

        #region Signature

        /// <summary>
        /// Gets all the signatures records from the table
        /// </summary>
        /// <returns>A list with all the entries of the table</returns>
        public List<SignatureRecord> GetAllSignatures()
        {
            var list = new List<SignatureRecord>();
            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var query = new BaseQuery(Manager, "GetAllSignatures", @"SELECT * FROM TNHT_SIRE");

                    foreach (IRecord record in query.Execute())
                    {
                        list.Add(new SignatureRecord
                        {
                            TicketId = record.ValueAs<Guid>("VEND_PK"),
                            IsSigned = record.ValueAs<bool>("SIRE_PROC"),
                            Signature = record.ValueAs<Blob>("SIRE_DISI")
                        });
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Manager.RollbackTransaction();
                }
            }
            catch (Exception ex)
            {
                logsFactory.Error(ex);
            }
            finally { Manager.Close(); }
            return list;
        }

        /// <summary>
        /// Gets one signature record from the table
        /// </summary>
        /// <param name="ticketId">The ticket id of the record</param>
        /// <returns>A signature record with the given ticket Id</returns>
        public SignatureRecord GetSignature(Guid ticketId)
        {
            var signatureRecord = new SignatureRecord();
            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var query = new BaseQuery(Manager, "GetSignature", "SELECT * FROM TNHT_SIRE WHERE VEND_PK = :VEND_PK")
                    {
                        Parameters =
                        {
                            ["VEND_PK"] = ticketId
                        }
                    };
                    var record = query.Execute().FirstOrDefault();

                    signatureRecord.TicketId = record.ValueAs<Guid>("VEND_PK");
                    signatureRecord.Signature = record.ValueAs<Blob>("SIRE_DISI");
                    signatureRecord.IsSigned = record.ValueAs<bool>("SIRE_PROC");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Manager.RollbackTransaction();
                }
            }
            catch (Exception ex) { logsFactory.Error(ex); }
            finally { Manager.Close(); }
            return signatureRecord;
        }

        /// <summary>
        /// Creates a new signature record in the table
        /// </summary>
        /// <param name="signature">The record to be created</param>
        /// <returns>A result with the success or not of the operation</returns>
        public ValidationContractResult AddSignature(SignatureRecord signature)
        {
            var result = new ValidationContractResult();
            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    const string command = """
                                           MERGE INTO TNHT_SIRE SIRE
                                           USING DUAL
                                           ON (SIRE.VEND_PK = :VEND_PK)
                                           WHEN MATCHED THEN
                                               UPDATE
                                               SET SIRE.SIRE_PROC = '0',
                                                   SIRE.SIRE_DISI = NULL
                                           WHEN NOT MATCHED THEN
                                               INSERT (SIRE.VEND_PK)
                                               VALUES (:VEND_PK)
                                           """;
                    var query = new BaseQuery(Manager, "AddSignature", command)
                    {
                        Parameters =
                            {
                                ["VEND_PK"] = signature.TicketId
                            }
                    };
                    query.Execute();
                    Manager.CommitTransaction();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Manager.RollbackTransaction();
                }
            }
            catch (Exception ex) { logsFactory.Error(ex); }
            finally { Manager.Close(); }
            return result;
        }

        /// <summary>
        /// Updates the signature of a ticket
        /// </summary>
        /// <param name="ticketId">The ticket Id to be searched for</param>
        /// <param name="signature">The new signature</param>
        /// <returns>A result with the success or not of the operation</returns>
        public ValidationContractResult UpdateSignature(Guid ticketId, SignatureRecord signature)
        {
            var result = new ValidationContractResult();
            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    // var query = new BaseQuery(Manager, "UpdateSignature", "UPDATE TNHT_SIRE SIRE SET SIRE.SIRE_DISI = :SIRE_DISI, SIRE.SIRE_PROC = :SIRE_PROC WHERE SIRE.VEND_PK = :VEND_PK")
                    //     {
                    //         Parameters =
                    //         {
                    //             ["SIRE_DISI"] = signature.Signature ?? DBNull.Value,
                    //             ["SIRE_PROC"] = signature.IsSigned,
                    //             ["VEND_PK"] = ticketId
                    //         }
                    //     };

                    using (var command = Manager.CreateCommand("UpdateSignature"))
                    {
                        command.CommandText = "UPDATE TNHT_SIRE SIRE SET SIRE.SIRE_DISI = :SIRE_DISI, SIRE.SIRE_PROC = :SIRE_PROC WHERE SIRE.VEND_PK = :VEND_PK";
                        Manager.CreateParameter(command, "SIRE_DISI", Manager.ConvertValueType(signature.Signature));
                        Manager.CreateParameter(command, "SIRE_PROC", Manager.ConvertValueType(signature.IsSigned));
                        Manager.CreateParameter(command, "VEND_PK", Manager.ConvertValueType(signature.TicketId));
                        Manager.ExecuteNonQuery(command);
                    }
                    //query.Execute();
                    Manager.CommitTransaction();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Manager.RollbackTransaction();
                }
            }
            catch (Exception ex) { logsFactory.Error(ex); }
            finally { Manager.Close(); }
            return result;
        }

        /// <summary>
        /// Deletes a signature from the table
        /// We use the ticketId to identify the signature to be deleted
        /// </summary>
        /// <param name="ticketId">The id of the signature to be deleted</param>
        /// <returns>A result with the success or not of the operation</returns>
        public ValidationContractResult DeleteSignature(Guid ticketId)
        {
            var result = new ValidationContractResult();
            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var query = new BaseQuery(Manager, "DeleteSignature", @"DELETE FROM TNHT_SIRE WHERE VEND_PK = :VEND_PK") { Parameters = { ["VEND_PK"] = ticketId } };
                    query.Execute();
                    Manager.CommitTransaction();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Manager.RollbackTransaction();
                }
            }
            catch (Exception ex) { logsFactory.Error(ex); }
            finally { Manager.Close(); }
            return result;
        }


        public ValidationItemResult<Guid> RequestNHSignatureCapture(Guid ticketId)
        {
            var result = new ValidationItemResult<Guid>();

            var contractResult = LoadTicket(ticketId);
            result.AddValidations(contractResult);

            if (contractResult.HasErrors) return result;

            // Parse details
            var contract = contractResult.Contract.As<POSTicketContract>();

            var details = new List<SignatureDetails>
            {
                new()
                {
                    Description = contract.SerieName
                }
            };
            details.AddRange(contract.ProductLineByTicket.Select(pl => new SignatureDetails { Description = $"{pl.ProductQtd} {pl.ProductDescription}", Value = $"{pl.CostValue}" }));
            details.Add(new SignatureDetails()
            {
                Description = "Total".Translate(),
                Value = $"{contract.Total}"
            });

            var request = new SignatureRequest
            {
                HotelId = contract.AccountInstallationId ?? sessionContext.InstallationId,
                Description = contract.SerieName,
                Value = $"{contract.Total}",
                Details = details.ToArray()
            };


            var business = BusinessContext.GetBusiness<ISignatureBusiness>(Manager);
            var pendingSignatureIdResult = business.RequestSignature(request);
            result.AddValidations(pendingSignatureIdResult);

            if (result.HasErrors) return result;

            result.Item = pendingSignatureIdResult.Item;

            return result;
        }

        public ValidationContractResult RetrieveNHSignatureCapture(Guid ticketId, Guid signatureId)
        {
            return new ValidationContractResult();
        }

        public ValidationResult RemoveNHSignatureCapture(Guid signatureId)
        {
            return new ValidationResult();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Hotel General Settings
        /// </summary>
        private POSGeneralSettingsRecord GetGeneralSetting(Guid hotelId)
        {
            if (_settingsRecord != null) return _settingsRecord;
            _settingsRecord = settingBusiness.GetGeneralSettings(hotelId);

            return _settingsRecord;
        }

        private Guid? GetCurrentDuty(Guid hotelId)
        {
            var query = CommonQueryFactory.GetTicketCurrentDuty(Manager);
            query.Parameters["hote_pk"] = hotelId;
            query.Parameters["curr_datetime"] = DateTime.Now;

            return query.ExecuteScalar<Guid?>();
        }

        private ValidationContractResult CloseAndPrintTicket(POSTicketContract ticket, ProductRecord.ProductRecord? tipProduct, IClient? client, PersonalDocType docTypeSelected, bool saveClient)
        {
            var result = new ValidationContractResult();

            if (!ticket.AllowPrintTicket(GeneralSetting))
                result.AddError("InvoiceTicketWhenPayCashCard".Translate());
            else
            {
                ticket.DocumentType = POSDocumentType.Ticket;

                if (client != null)
                {
                    ticket.BaseEntityId = client.Id;
                    ticket.FiscalDocAddress = client.Address;
                    ticket.FiscalDocFiscalNumber = docTypeSelected switch
                    {
                        PersonalDocType.FiscalNumber => client.FiscalNumber != string.Empty ? client.FiscalNumber : null,
                        PersonalDocType.IdentityDocument => client.Identity != string.Empty ? client.Identity : null,
                        PersonalDocType.Passport => client.Passport != string.Empty ? client.Passport : null,
                        PersonalDocType.ResidenceCertificate => client.Residence != string.Empty ? client.Residence : null,
                        PersonalDocType.DriverLicence => client.DriverLicense != string.Empty ? client.DriverLicense : null,
                        _ => client.FiscalNumber != string.Empty ? client.FiscalNumber : null
                    };
                    ticket.FiscalDocNacionality = client.Country;
                    ticket.FiscalDocTo = client.Name != "" ? client.Name : null;
                    ticket.FiscalDocEmail = client.EmailAddress;
                    ticket.FiscalIdenType = (long?)docTypeSelected;
                }

                result.Add(ValidateTicket(ticket, client));
                if (result.HasErrors) return result;

                // Solo asignar la entidad base si no hubo errores
                ticket.BaseEntityId = client?.Id ?? ticket.BaseEntityId;

                var persistResult = CloseTicket(ticket, tipProduct, saveClient);
                if (persistResult.IsEmpty)
                {
                    var contract = persistResult.Contract.As<POSTicketContract>();
                    result.Contract = contract;
                    // Print persisted ticket
                    PrintBySpoolerAsync(contract, DocumentType.Ticket);
                }
                else
                    result.Add(persistResult);
            }

            return result;
        }

        /// <summary>
        /// Specifies the type of document to be printed.
        /// </summary>
        private enum DocumentType
        {
            Receipt = 1,
            Ticket = 2,
            Invoice = 3
        }

        // <summary>
        /// Asynchronously prints a document using the spooler service based on the specified document type.
        /// </summary>
        /// <param name="ticket">The ticket contract containing details to be printed.</param>
        /// <param name="docType">The type of document to print. Defaults to <see cref="DocumentType.Ticket"/>.</param>
        private void PrintBySpoolerAsync(POSTicketContract ticket, DocumentType docType = DocumentType.Ticket)
        {
            try
            {
                // Prepare needed variables
                var business = CwFactory.Resolve<IStandBusiness>();
                var stand = business.GetStands(sessionContext.CashierId, (int)sessionContext.LanguageId).FirstOrDefault(x => (Guid)x.Id == sessionContext.StandId);

                Guid? printCnfgId = docType switch
                {
                    DocumentType.Receipt => stand.ReceiptPrintConfiguration,
                    DocumentType.Ticket => stand.TicketPrintConfiguration,
                    DocumentType.Invoice => stand.InvoicePrintConfiguration,
                    _ => stand.TicketPrintConfiguration,
                };

                var printConfig = GetTicketPrintConfiguration(printCnfgId ?? Guid.Empty, sessionContext.LanguageId);

                var settings = new Settings
                {
                    GeneralSettings = GeneralSetting,
                    Print = printConfig,
                    Stand = stand
                };
                ticket.ClearImages();

                // Create async task to print ticket
                var task = new Task(() =>
                {
                    var proxy = new SpoolerServiceClient();
                    proxy.PrintTicket(ticket, settings, docType == DocumentType.Invoice);
                });
                task.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                // result.AddError("Printing error, check spooler or contact administrator");
                // result.AddException(ex);
            }
        }

        private ValidationStringResult CloseTicketAsInvoice(POSTicketContract ticket, ProductRecord.ProductRecord? tipProduct, ClientRecord? client, PersonalDocType docTypeSelected,
            bool saveClient)
        {
            var result = new ValidationStringResult();

            if (ticket.AllowPrintInvoice(GeneralSetting))
            {
                var settings = settingBusiness.GetGeneralSettings(sessionContext.InstallationId);

                if (client == null)
                {
                    client = new ClientRecord
                    {
                        Id = ticket.BaseEntityId ?? Guid.Empty,
                        Name = FiscalDocContract.GetFinalCustomerName(settings.SignType),
                        Address = ticket.FiscalDocAddress,
                        EmailAddress = ticket.FiscalDocEmail,
                        FiscalNumber = FiscalDocContract.GetFinalCustomerNumber(settings.SignType),
                        Country = settingBusiness.GetNationalities((int)sessionContext.LanguageId).FirstOrDefault(x =>
                            x.CountryCode == GetHotel(sessionContext.InstallationId).Nationality)?.CountryCode
                    };

                    docTypeSelected = PersonalDocType.FiscalNumber;
                }

                if (settings.FiscalType != FiscalPOSType.None)
                {
                    var fiscalPrinter = CwFactory.Resolve<IFiscalPrinter>();
                    var buyer = new BuyerInfo
                    {
                        FiscalNumber = client.FiscalNumber,
                        Name = client.Name,
                        IsDefaultCustomer = settings.SignType == DocumentSign.FiscalizationPortugal
                    };

                    result.AddValidations(fiscalPrinter.ValidateBuyerInfo(buyer));
                }

                if (result.IsEmpty)
                {
                    // Invoice Information -> Preparing for Invoice
                    ticket.BaseEntityId = client.Id != Guid.Empty ? client.Id : ticket.BaseEntityId;
                    ticket.DocumentType = POSDocumentType.CashInvoice;
                    ticket.FiscalDocAddress = client.Address;
                    ticket.FiscalDocNacionality = client.Country;
                    ticket.FiscalDocTo = client.Name != "" ? client.Name : null;
                    ticket.FiscalDocEmail = client.EmailAddress;
                    ticket.FiscalIdenType = (long?)docTypeSelected;
                    ticket.FiscalDocFiscalNumber = docTypeSelected switch
                    {
                        PersonalDocType.FiscalNumber => client.FiscalNumber != string.Empty ? client.FiscalNumber : null,
                        PersonalDocType.IdentityDocument => client.Identity != string.Empty ? client.Identity : null,
                        PersonalDocType.Passport => client.Passport != string.Empty ? client.Passport : null,
                        PersonalDocType.ResidenceCertificate => client.Residence != string.Empty ? client.Residence : null,
                        _ => client.FiscalNumber != string.Empty ? client.FiscalNumber : null
                    };
                }

                if (result.IsEmpty)
                {
                    var persistResult = CloseTicket(ticket, tipProduct, saveClient);
                    if (persistResult.IsEmpty)
                    {
                        var contract = persistResult.Contract.As<POSTicketContract>();
                        result.Description = contract.SerieName;
                        // Print persisted ticket from the CloseTicket action
                        PrintBySpoolerAsync(contract, DocumentType.Invoice);
                    }
                    else
                        result.Add(persistResult);
                }
                else
                    result.AddError("CanOnlyInvoiceTicketWhenPayCashCard".Translate());
            }
            else
            {
                result.AddError("Print invoice not allowed");
            }

            return result;
        }

        private ValidationContractResult ValidateTicket(POSTicketContract ticket, IClient client)
        {
            var result = new ValidationContractResult();

            var settings = settingBusiness.GetGeneralSettings(sessionContext.InstallationId);

            // Validate if there is a client and has a fiscal printer
            if (client != null && settings.FiscalType != FiscalPOSType.None)
            {
                var country = GetHotel(sessionContext.InstallationId).Nationality;
                var buyer = new BuyerInfo
                {
                    FiscalNumber = ticket.FiscalDocFiscalNumber,
                    Name = ticket.FiscalDocTo,
                    IsDefaultCustomer = ticket.FiscalDocTo == FiscalValidations.GetFinalCustomerName(country) &&
                                        ticket.FiscalDocFiscalNumber == FiscalValidations.GetFinalCustomerNumber(country)
                };

                var fiscalPrinter = CwFactory.Resolve<IFiscalPrinter>();
                result.AddValidations(fiscalPrinter.ValidateBuyerInfo(buyer));
            }

            var validator = new DocValidator(settings.MandatoryValueForFiscalNumber)
            {
                Client = client,
                Country = ticket.FiscalDocNacionality,
                DocumentSign = settings.SignType
            };

            result.Add(validator.Validate(ticket));

            return result;
        }

        private ValidationContractResult CloseTicketInternal(POSTicketContract ticket, ProductRecord.ProductRecord? tipProduct = null, bool saveClient = false)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    #region Tip Treatment

                    if (ticket.TipValue is > decimal.Zero)
                    {
                        if (tipProduct != null)
                        {
                            var tipResult = AddProductToTicket(ticket, tipProduct, 1, ticket.TipValue.Value, null, null, true, true, null, false);
                            if (tipResult.IsEmpty)
                                ticket = tipResult.Contract.As<POSTicketContract>();
                            else
                                result.Add(tipResult);
                        }
                        else
                            result.AddError("UndefinedTipService".Translate());
                    }

                    #endregion

                    if (result.IsEmpty)
                    {
                        #region Close

                        var stand = new Stand(Manager, sessionContext.StandId);
                        ticket.CajaPave = sessionContext.CashierId;
                        ticket.ClosedDate = sessionContext.WorkDate;
                        ticket.ClosedTime = DateTime.Now;
                        ticket.Shift = stand.Shift;
                        ticket.Opened = false;
                        ticket.OpenUserId = null;

                        #region Validations

                        result.Add(ValidateTicket(ticket));

                        #endregion

                        if (result.IsEmpty)
                        {
                            if (!ticket.BaseEntityId.HasValue && ticket.FiscalDocFiscalNumber != "999999990" && saveClient)
                            {
                                var standBusiness = BusinessContext.GetBusiness<IStandBusiness>(Manager);
                                var newClientId = standBusiness.CreateClient(ticket.FiscalDocTo, ticket.FiscalDocFiscalNumber,
                                    ticket.FiscalDocEmail, ticket.FiscalDocAddress, ticket.FiscalDocNacionality);

                                if (newClientId.HasErrors)
                                    return result;

                                if (newClientId.ItemSource.Count > 0)
                                {
                                    var clientId = newClientId.ItemSource[0];

                                    var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
                                    var clientContract = new ClientContract(DocumentSign.None, string.Empty, DateTime.Now);

                                    clientContract.Id = clientId;
                                    var fullNameSplit = ticket.FiscalDocTo.Split(' ');
                                    if (fullNameSplit.Length == 0)
                                        result.AddError("First name can't be empty");

                                    if (result.IsEmpty)
                                    {
                                        clientContract.FirstName = fullNameSplit[0];
                                        if (fullNameSplit.Length == 1)
                                            result.AddError("Last name can't be empty");
                                        else
                                        {
                                            clientContract.LastName = ticket.FiscalDocTo.Substring(ticket.FiscalDocTo.IndexOf(' ') + 1);
                                            clientContract.FiscalNumber = ticket.FiscalDocFiscalNumber;
                                            clientContract.EmailAddress = ticket.FiscalDocEmail;
                                            clientContract.FiscalAddressContract = new TypeAddressContract();
                                            clientContract.FiscalAddressContract.Address1 = ticket.FiscalDocAddress;
                                            clientContract.CountryId = ticket.FiscalDocNacionality;
                                            clientContract.Gender = GenderType.Male;
                                            clientContract.CivilStatus = CivilState.Single;
                                            clientContract.RegistrationDate = DateTime.Now;
                                        }
                                    }
                                }
                            }
                            else if (ticket.BaseEntityId.HasValue)
                            {
                                var standBusiness = BusinessContext.GetBusiness<IStandBusiness>(Manager);
                                var newClientId = standBusiness.CreateClient(ticket.FiscalDocTo, ticket.FiscalDocFiscalNumber,
                                    ticket.FiscalDocEmail, ticket.FiscalDocAddress, ticket.FiscalDocNacionality, ticket.BaseEntityId);
                                result.AddValidations(newClientId);
                            }
                        }

                        // There are too many process implied on this.
                        // When I moved it below the cloud business actions, appeared tons of pms erros
                        // As far as I know, you have to close the ticket locally first
                        // Then pray to not have duplicated movements in the pms
                        // One of them is: if the ticket has room credit as a payment, then it has to be saved with attr process = 1
                        if (result.IsEmpty)
                            result = PersistAndClose(ticket);

                        #endregion

                        #region Cloud Business

                        //var chargeAccount = ticket.PaymentLineByTicket != null &&
                        //                    (ticket.PaymentLineByTicket.Any(x => x.AccountId.HasValue && x.ReceivableType == ReceivableType.Credit)
                        //                     || ticket.PaymentLineByTicket.Any(x => x is { IsAccount: true, ReceivableType: ReceivableType.UseAccountDeposit }));

                        if (ticket.MustChargeAccount())
                        {
                            // Charge room account
                            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
                            result.Add(business.ChargeAccount(ticket));
                        }

                        if (ticket.HasBookingTableInCheckIn() && stand.AllowChangeTableReservationStatus)
                        {
                            var cloudBusiness = BusinessContext.GetBusiness<ICloudBusiness>(Manager);

                            var vr = cloudBusiness.CheckOutBookingTable(ticket.ReservationTableId.Value);
                            if (!vr.IsEmpty)
                            {
                                cloudBusiness.TraceComunicationErrors(vr);
                                result.AddValidations(cloudBusiness.GetBusinessValidations(vr));
                            }
                        }

                        if (ticket.HasSpaService())
                        {
                            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
                            result.Add(business.CheckOutSpaService(ticket.SpaServiceId.Value));
                        }

                        #endregion

                        if (result.IsEmpty)
                        {

                            if (result.IsEmpty)
                            {
                                if (IsExportingDoc())
                                {
                                    #region Online exportation

                                    var business = BusinessContext.GetBusiness<IOnlineExportBusiness>(Manager);
                                    if (business != null)
                                        result = business.Export(result.Contract as POSTicketContract);
                                    else
                                        result.AddError("Online exportation not configurated correctly");

                                    #endregion
                                }
                                else
                                {
                                    #region Factura electrónica

                                    if (!UseContingencySeries && !ticket.MustChargeAccount() &&
                                        !string.IsNullOrEmpty(ticket.InvoiceSerie) && ticket.InvoiceNumber.HasValue)
                                    {
                                        var business = BusinessContext.GetBusiness<IFiscalBusiness>(Manager);
                                        switch (ticket.DocumentType)
                                        {
                                            case POSDocumentType.CashInvoice:
                                                result.Add(business.ProcessInvoice((Guid)ticket.Id, ticket.InvoiceSerie, ticket.InvoiceNumber.Value));
                                                break;
                                            case POSDocumentType.Ballot:
                                                result.Add(business.ProcessTicket((Guid)ticket.Id, ticket.InvoiceSerie, ticket.InvoiceNumber.Value));
                                                break;
                                        }
                                    }

                                    #endregion
                                }
                            }
                        }
                    }

                    Manager.EndTransaction(result);
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        private bool IsExportingDoc()
        {
            return !BlockOnlineDocExportation && GeneralSetting.OnlineDocExportation;
        }

        private ValidationResult GenerateFiscalDocSerie(POSTicketContract ticket, DocumentSign documentSign)
        {
            var result = new ValidationResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var business = BusinessContext.GetBusiness<IDocumentSerieBusiness>(Manager);

                    var nextSerieResult = business.GetNextSerialNumber(ticket.Caja, ticket.Stand,
                        false, (long)ticket.DocumentType, true, UseContingencySeries);

                    if (nextSerieResult.IsEmpty)
                    {
                        ticket.FiscalDocSerie = nextSerieResult.DocumentSerie;
                        ticket.FiscalDocNumber = nextSerieResult.DocumentNumber;
                        ticket.FiscalDocRegDate = DateTime.Now;
                        ticket.FiscalDocEmiDate = sessionContext.WorkDate;
                        ticket.FiscalDocTotal = ticket.Total;

                        switch (documentSign)
                        {
                            case DocumentSign.FiscalizationPortugal:
                            case DocumentSign.FiscalizationAngola:
                            case DocumentSign.FiscalizationCaboVerde:
                                var documentSerie = new DocumentSerie(Manager, nextSerieResult.SerieId.Value);
                                if (string.IsNullOrWhiteSpace(documentSerie.ValidationCode) && documentSign == DocumentSign.FiscalizationPortugal)
                                {
                                    result.AddError("Validation code not configured");
                                    break;
                                }

                                var signature = SAFTPT.Instance.SignInvoice(ticket.FiscalDocRegDate.Value, ticket.FiscalDocEmiDate.Value.Date,
                                    ticket.FiscalDocNumber.Value, ticket.FiscalDocSerie,
                                    ticket.FiscalDocTotal.Value, documentSerie.Signature);

                                ticket.FiscalDocSignature = signature;
                                ticket.FiscalDocValidationCode = documentSerie.ValidationCode;

                                documentSerie.Signature = signature;
                                documentSerie.SaveObject();
                                break;
                        }
                    }
                    else
                        result.Add(nextSerieResult);

                    Manager.EndTransaction(result);
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        private ValidationResult GenerateCreditNoteSerie(POSTicketContract ticket, DocumentSign documentSign)
        {
            var result = new ValidationResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var business = BusinessContext.GetBusiness<IDocumentSerieBusiness>(Manager);

                    var nextSerieResult = business.GetNextSerialNumber(ticket.Caja, ticket.Stand, false, (long)POSDocumentType.CashCreditNote, true, UseContingencySeries);
                    if (nextSerieResult.IsEmpty)
                    {
                        ticket.CreditNoteSerie = nextSerieResult.DocumentSerie;
                        ticket.CreditNoteNumber = nextSerieResult.DocumentNumber;

                        switch (documentSign)
                        {
                            case DocumentSign.FiscalizationPortugal:
                            case DocumentSign.FiscalizationAngola:
                            case DocumentSign.FiscalizationCaboVerde:
                                var documentSerie = new DocumentSerie(Manager, nextSerieResult.SerieId.Value);
                                if (string.IsNullOrWhiteSpace(documentSerie.ValidationCode) && documentSign == DocumentSign.FiscalizationPortugal)
                                {
                                    result.AddError("Validation code not configured");
                                    break;
                                }

                                var signature = SAFTPT.Instance.SignCreditNote(ticket.CreditNoteSystemDate.Value, ticket.CreditNoteWorkDate.Value.Date,
                                    ticket.CreditNoteNumber.Value, ticket.CreditNoteSerie,
                                    ticket.FiscalDocTotal.Value, documentSerie.Signature);

                                ticket.CreditNoteSignature = signature;
                                ticket.CreditNoteValidationCode = documentSerie.ValidationCode;

                                documentSerie.Signature = signature;
                                documentSerie.SaveObject();
                                break;
                        }
                    }
                    else
                        result.Add(nextSerieResult);

                    Manager.EndTransaction(result);
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        private ValidationResult GenerateTicketSerie(POSTicketContract ticket, DocumentSign documentSign)
        {
            var result = new ValidationResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var business = BusinessContext.GetBusiness<IDocumentSerieBusiness>(Manager);

                    var nextSerieResult = business.GetNextSerialNumber(ticket.Caja, ticket.Stand, false, (long)POSDocumentType.Ticket, true);
                    if (nextSerieResult.IsEmpty)
                    {
                        ticket.Serie = nextSerieResult.DocumentSerie;
                        ticket.Number = nextSerieResult.DocumentNumber;

                        switch (documentSign)
                        {
                            case DocumentSign.FiscalizationPortugal:
                            case DocumentSign.FiscalizationAngola:
                            case DocumentSign.FiscalizationCaboVerde:
                                var documentSerie = new DocumentSerie(Manager, nextSerieResult.SerieId.Value);
                                if (string.IsNullOrWhiteSpace(documentSerie.ValidationCode) && documentSign == DocumentSign.FiscalizationPortugal)
                                {
                                    result.AddError("Validation code not configured");
                                    break;
                                }

                                var signature = SAFTPT.Instance.SignTicket(ticket.OpeningTime, ticket.OpeningDate,
                                    ticket.Number.Value, ticket.Serie,
                                    ticket.Total, documentSerie.Signature);

                                ticket.Signature = signature;
                                ticket.ValidationCode = documentSerie.ValidationCode;

                                documentSerie.Signature = signature;
                                documentSerie.SaveObject();
                                break;
                        }
                    }
                    else
                        result.Add(nextSerieResult);

                    Manager.EndTransaction(result);
                }
                catch
                {
                    Manager.RollbackTransaction();
                    throw;
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        private ValidationResult GenerateSeries(POSTicketContract contract, DocumentSign documentSign)
        {
            // Ticket serie
            var ticketSerieResult = GenerateTicketSerie(contract, documentSign);
            if (!ticketSerieResult.IsEmpty)
                return ticketSerieResult;

            // Invoice or Ballot serie
            if (!contract.IsOnlyTicket)
            {
                // Fiscal serie
                var fiscalDocSerieResult = GenerateFiscalDocSerie(contract, documentSign);
                if (!fiscalDocSerieResult.IsEmpty)
                    return fiscalDocSerieResult;
            }

            return ValidationResult.Empty;
        }

        private static void AddQRItem(IDictionary<string, string> items, string key, string value, int length)
        {
            if (value == null)
                throw new ApplicationException(string.Format("Undefined QR code parameter {0}", key));

            items.Add(key, value.Length > length ? value.Substring(0, length) : value);
        }

        private static string GetTaxCodePT(decimal percent)
        {
            if (percent >= 18)
                return IvaNormal;
            else if (percent >= 9)
                return IvaIntermediate;
            else if (percent >= 4)
                return IvaReduced;
            else if (percent == 0)
                return IvaExempted;

            return IvaOther;
        }

        private static ValidationResult<string> BuildPortugalQRCode(POSGeneralSettingsRecord settings,
            string docFiscalNumber, string docCountry, bool isAnul, string docType, string docSerie,
            long? docNumber, DateTime? emissionDate, decimal docTotal, string docValidationCode,
            string docSignature, IEnumerable<IProductLineTax> productLines, string docComments)
        {
            var result = new ValidationResult<string>();

            var items = new Dictionary<string, string>();

            // número fiscal del hotel
            if (string.IsNullOrEmpty(settings.FiscalNumber))
                result.AddError("NIF do hotel não definido");
            else
                AddQRItem(items, "A", settings.FiscalNumber, 9);
            // número fiscal del adquiriente
            if (string.IsNullOrEmpty(docFiscalNumber))
                AddQRItem(items, "B", "999999990", 30);
            else
                AddQRItem(items, "B", docFiscalNumber, 30);
            // país del adquiriente
            if (string.IsNullOrEmpty(docCountry))
                AddQRItem(items, "C", "PT", 12);
            else
                AddQRItem(items, "C", docCountry, 12);
            // tipo de documento fiscal
            if (string.IsNullOrEmpty(docType))
                result.AddError("Tipo de documento não e válido");
            else
                AddQRItem(items, "D", docType, 2);
            // estado del documento
            AddQRItem(items, "E", isAnul ? "A" : "N", 1);
            // fecha de emisión del documento
            if (emissionDate.HasValue)
                AddQRItem(items, "F", emissionDate.Value.Date.ToString("yyyyMMdd"), 8);
            else
                result.AddError("Data de emissão não definida");
            // identificador del documento
            if (string.IsNullOrEmpty(docSerie) || !docNumber.HasValue)
                result.AddError("Serie de documento não definida");
            else
            {
                AddQRItem(items, "G", string.Format("{0} {1}/{2}", docType, docSerie, docNumber.Value), 60);
                // ATCUD
                AddQRItem(items, "H", string.IsNullOrEmpty(docValidationCode)
                    ? "0"
                    : string.Format("{0}-{1}", docValidationCode, docNumber.Value), 70);
            }

            // region de impuesto del hotel
            if (string.IsNullOrEmpty(settings.FiscalRegionCode))
                result.AddError("Região de imposto não definida");
            else
                AddQRItem(items, "I1", settings.FiscalRegionCode, 5);

            if (result.IsEmpty)
            {
                var totalTax = decimal.Zero;

                var exemptedTaxValue = Math.Abs(productLines
                    .Where(pl => GetTaxCodePT(pl.FirstIvaPercent) == IvaExempted)
                    .Sum(pl => pl.FirstIvaValue));

                // iva exento
                if (exemptedTaxValue > decimal.Zero)
                {
                    // valor base
                    AddQRItem(items, "I2", exemptedTaxValue.ToString("0.00"), 16);
                }

                var baseTaxValue = Math.Abs(productLines
                    .Sum(pl => pl.NetPrice));

                // iva reducido
                if (baseTaxValue > decimal.Zero)
                {
                    var reducedTaxValue = Math.Abs(productLines
                        .Where(pl => GetTaxCodePT(pl.FirstIvaPercent) == IvaReduced)
                        .Sum(pl => pl.FirstIvaValue));

                    var intermediateTaxValue = Math.Abs(productLines
                        .Where(pl => GetTaxCodePT(pl.FirstIvaPercent) == IvaIntermediate)
                        .Sum(pl => pl.FirstIvaValue));

                    var normalTaxValue = Math.Abs(productLines
                        .Where(pl => GetTaxCodePT(pl.FirstIvaPercent) == IvaNormal)
                        .Sum(pl => pl.FirstIvaValue));

                    // valor base iva reducido
                    AddQRItem(items, "I3", baseTaxValue.ToString("0.00"), 16);

                    // valor de impuesto iva reducido
                    totalTax += reducedTaxValue;
                    AddQRItem(items, "I4", reducedTaxValue.ToString("0.00"), 16);

                    // valor base iva intermedio
                    AddQRItem(items, "I5", baseTaxValue.ToString("0.00"), 16);

                    // valor de impuesto iva intermedio
                    totalTax += intermediateTaxValue;
                    AddQRItem(items, "I6", intermediateTaxValue.ToString("0.00"), 16);

                    // valor base iva normal
                    AddQRItem(items, "I7", baseTaxValue.ToString("0.00"), 16);

                    // valor de impuesto iva normal
                    totalTax += normalTaxValue;
                    AddQRItem(items, "I8", normalTaxValue.ToString("0.00"), 16);
                }

                // valor total de impuesto
                AddQRItem(items, "N", totalTax.ToString("0.00"), 16);
                // valor total bruto;
                AddQRItem(items, "O", docTotal.ToString("0.00"), 16);
                // valor total de retenciones: P (no implementada)
                // hash del documento
                if (!string.IsNullOrEmpty(docSignature))
                    AddQRItem(items, "Q", new string(new char[] { docSignature[0], docSignature[10], docSignature[20], docSignature[30] }), 4);
                // número de certificado
                AddQRItem(items, "R", SAFTPT.NewHotelCloudCertificateNumber, 4);
                // comentarios del documento
                if (!string.IsNullOrEmpty(docComments))
                    AddQRItem(items, "S", docComments.Replace('*', ' '), 65);

                result.Result = string.Join("*", items.Select(kv => kv.Key + ":" + kv.Value));
            }

            return result;
        }

        private static ValidationResult<string> BuildPortugalTicketQRCode(POSTicketContract contract, POSGeneralSettingsRecord settings)
        {
            var productLines = contract.IsAnul
                ? contract.ProductLineByTicket.Where(pl => pl.AnulAfterClosed)
                : contract.ProductLineByTicket.Where(pl => pl.IsActive);

            return BuildPortugalQRCode(settings,
                contract.FiscalDocFiscalNumber, contract.FiscalDocNacionality, contract.IsAnul,
                "CM", contract.Serie, contract.Number.Value, contract.OpeningDate,
                contract.Total, contract.ValidationCode, contract.Signature,
                productLines, contract.Observations);
        }

        private static ValidationResult<string> BuildPortugalLookupQRCode(LookupTableContract contract, POSGeneralSettingsRecord settings)
        {
            return BuildPortugalQRCode(settings,
                string.Empty, string.Empty, false, "CM",
                contract.Serie, contract.Number, contract.WorkDate,
                contract.Total, contract.ValidationCode, contract.Signature,
                contract.Details, string.Empty);
        }

        private static ValidationResult<string> BuildPortugalInvoiceQRCode(POSTicketContract contract, POSGeneralSettingsRecord settings)
        {
            var productLines = contract.IsAnul
                ? contract.ProductLineByTicket.Where(pl => pl.AnulAfterClosed)
                : contract.ProductLineByTicket.Where(pl => pl.IsActive);

            return BuildPortugalQRCode(settings,
                contract.FiscalDocFiscalNumber, contract.FiscalDocNacionality, contract.IsAnul,
                "FR", contract.FiscalDocSerie, contract.FiscalDocNumber, contract.FiscalDocEmiDate,
                contract.FiscalDocTotal ?? decimal.Zero, contract.FiscalDocValidationCode,
                contract.FiscalDocSignature, productLines, contract.Observations);
        }

        private static ValidationResult<string> BuildPortugalCreditNoteQRCode(POSTicketContract contract, POSGeneralSettingsRecord settings)
        {
            var productLines = contract.IsAnul
                ? contract.ProductLineByTicket.Where(pl => pl.AnulAfterClosed)
                : contract.ProductLineByTicket.Where(pl => pl.IsActive);

            return BuildPortugalQRCode(settings,
                contract.FiscalDocFiscalNumber, contract.FiscalDocNacionality, contract.IsAnul,
                "NC", contract.CreditNoteSerie, contract.CreditNoteNumber, contract.CreditNoteWorkDate,
                contract.FiscalDocTotal ?? decimal.Zero, contract.CreditNoteValidationCode,
                contract.CreditNoteSignature, productLines, contract.Observations);
        }

        private ValidationResult PrintOrdersBySpooler(POSTicketContract ticket, DispatchContract[] orders, string headerTitle)
        {
            var result = new ValidationResult();

            try
            {
                ticket.ClearImages();
                var client = new SpoolerServiceClient();
                var stand = new Stand(Manager, ticket.Stand);

                var userDesc = GetUserName(sessionContext.UserId);
                ticket.LastUserDispatched = userDesc;

                result.AddValidations(client.PrintOrders(headerTitle, ticket, orders, stand.CopyKitchenInTicketPrinter));
            }
            catch (Exception ex)
            {
                result.AddError("Printing error, check spooler or contact administrator");
                result.AddException(ex);
            }

            return result;
        }


        private static void ClearDispatch(POSProductLineContract productLine)
        {
            productLine.AreaId = null;
            productLine.Areas.Clear();
            productLine.SendToAreaStatus = 0;
            productLine.SendToAreaStartTime = null;
            productLine.SendToAreaEndTime = null;
            foreach (var tableLine in productLine.TableLines)
            {
                if (!tableLine.HasAreas)
                {
                    tableLine.SendToAreaStatus = 0;
                }
            }
        }

        private void CancelProductLines(IEnumerable<POSProductLineContract> productLines, bool fromClosed, bool fromMerge)
        {
            foreach (var productLine in productLines)
            {
                ClearDispatch(productLine);

                if(productLine.AnnulmentCode == ProductLineCancellationStatus.Cancelled)
                    continue;

                if (fromMerge)
                    productLine.AnnulmentCode = ProductLineCancellationStatus.CancelledByTransfer;
                else
                {
                    if (fromClosed)
                        productLine.AnulAfterClosed = productLine.IsActive;
                    productLine.AnnulmentCode = productLine.Printed > 0
                        ? ProductLineCancellationStatus.CancelledAfterPrint
                        : ProductLineCancellationStatus.Cancelled;
                }

                productLine.AnnulUserId ??= sessionContext.UserId;
            }
        }

        private void CalculateIncidence(TaxContract taxContract)
        {
            var totalTax = taxContract.Taxes.Sum(x => x.TaxValue);
            foreach (var tax in taxContract.Taxes)
            {
                if (totalTax > decimal.Zero)
                {
                    var incidence = (tax.TaxValue / totalTax * taxContract.InformedPrice) - tax.TaxValue;
                    tax.TaxIncidence = incidence;
                }
                else
                    tax.TaxIncidence = decimal.Zero;
            }
        }

        private bool StaleDataControl(DateTime contract, DateTime persistent)
        {
            return (int)Math.Truncate((contract - persistent).TotalMilliseconds * 1000) != 0;
        }

        private T ValidateResult<T>(ValidationContractResult validation, bool throwExceptionWithWarnings = false) where T : BaseContract
        {
            ValidateErrorAndWarning(validation, throwExceptionWithWarnings);
            return validation.Contract.As<T>();
        }

        private void ValidateErrorAndWarning(ValidationResult validation, bool throwExceptionWithWarnings)
        {
            var message = string.Empty;

            if (validation.HasErrors)
                message = validation.Errors.Aggregate(message, (current, v) => current + $"{v.Message}\n");

            if (validation.HasWarnings && throwExceptionWithWarnings)
                message = validation.Warnings.Aggregate(message, (current, v) => current + $"{v.Message}\n");

            if (!string.IsNullOrEmpty(message))
                throw new Exception(message);
        }

        private HotelContract GetHotel(Guid hotelId)
        {
            var hotel = new Hotel(Manager, hotelId);
            var contract = new HotelContract();
            contract.SetValues(hotel.GetValues());

            return contract;
        }

        private ListData GetProductLines(Guid ticketId, long languageId)
        {
            var query = CommonQueryFactory.GetProductLinesByTicket(Manager);
            query.Parameters["vend_pk"] = ticketId;
            query.Parameters["lang_pk"] = languageId;

            return query.Execute();
        }

        private static decimal SumProductsValue(POSTicketContract ticket, bool overNet)
        {
            // Check if product line is annulled or is a tip
            int Annul(POSProductLineContract p) => p.AnnulmentCode == ProductLineCancellationStatus.Active || p.AnnulmentCode == ProductLineCancellationStatus.ActiveByTransfer && p.IsTip == false ? 1 : 0;

            var sum = overNet
                ? ticket.ProductLineByTicket.Sum(p => p.NetValue * Annul(p))
                : ticket.ProductLineByTicket.Sum(p => p.GrossValue * Annul(p));

            return sum;
        }

        private string GetOnlyDigits(string input)
        {
            return string.IsNullOrEmpty(input) ? string.Empty : string.Concat(input.Where(c => char.IsDigit(c)));
        }

        private bool ValidNITColombia(string nit)
        {
            nit = GetOnlyDigits(nit);
            if (!string.IsNullOrEmpty(nit))
            {
                for (int i = 0; i < nit.Length; i++)
                {
                    if (!char.IsDigit(nit[i]))
                        return false;
                }
                return true;
            }

            return false;
        }

        private bool ValidDNIColombia(string dni)
        {
            dni = GetOnlyDigits(dni);
            if (string.IsNullOrEmpty(dni))
                return false;
            return dni.All(c => char.IsDigit(c));

        }

        #endregion

        #region Public Methods

        public List<DocumentSerieRecord> GetTicketSerieByStandCashier(Guid standId, Guid cajaId)
        {
            var list = new List<DocumentSerieRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetDocumentSeriesByStandCashier(Manager);
                query.Parameters["ipos_pk"] = standId;
                query.Parameters["caja_pk"] = cajaId;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var documentSerieRecord = new DocumentSerieRecord();

                    documentSerieRecord.Id = record.ValueAs<Guid>("sedo_pk");
                    documentSerieRecord.DocType = record.ValueAs<POSDocumentType>("tido_pk");
                    documentSerieRecord.SerieTex = record.ValueAs<string>("sedo_ptex");
                    documentSerieRecord.SerieNum = record.ValueAs<long>("sedo_pnum");
                    documentSerieRecord.SerieOrder = record.ValueAs<long>("sedo_orde");

                    documentSerieRecord.StandId = standId;
                    documentSerieRecord.CajaId = cajaId;
                    documentSerieRecord.NextNumber = record.ValueAs<long>("sedo_nufa");
                    documentSerieRecord.EndNumber = record.ValueAs<long?>("sedo_nufi");
                    documentSerieRecord.EndDate = record.ValueAs<DateTime?>("sedo_dafi");
                    documentSerieRecord.SerieStatus = record.ValueAs<SerieStatus>("sedo_seac");
                    documentSerieRecord.Signature = record.ValueAs<string>("sedo_saft");

                    list.Add(documentSerieRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return list;
        }

        public ListData GetProductLines(Guid ticketId)
        {
            return GetProductLines(ticketId, sessionContext.LanguageId);
        }

        public ListData GetDescriptions(Guid ticketId, long languageId)
        {
            var sb = new StringBuilder();
            sb.AppendLine("select (select mult.mult_desc from vnht_mult mult where mult.lite_pk = ipos.lite_desc and mult.lang_pk = :lang_pk) as ipos_desc,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = caja.lite_pk and mult.lang_pk = :lang_pk) as caja_desc,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = salo.lite_pk and mult.lang_pk = :lang_pk) as salo_desc,");
            sb.AppendLine("       mesa.mesa_desc, vend.vend_tota, clie.clie_deau");
            sb.AppendLine("from tnht_vend vend");
            sb.AppendLine("inner join tnht_ipos ipos on ipos.ipos_pk = vend.ipos_pk");
            sb.AppendLine("inner join tnht_caja caja on caja.caja_pk = vend.caja_pk");
            sb.AppendLine("left join tnht_clie clie on clie.clie_pk = vend.clie_pk");
            sb.AppendLine("left join tnht_mesa mesa on mesa.mesa_pk = vend.vend_mesa");
            sb.AppendLine("left join tnht_salo salo on salo.salo_pk = mesa.salo_pk");
            sb.AppendLine("where vend.vend_pk = :vend_pk");

            var query = new BaseQuery(Manager, "Descriptions", sb.ToString());
            query.Parameters["vend_pk"] = ticketId;
            query.Parameters["lang_pk"] = languageId;

            return query.Execute();
        }

        public ListData GetProductLinesPreparations(Guid ticketId, long languageId)
        {
            var sb = new StringBuilder();
            sb.AppendLine("select arvp.arvp_pk, arve.arve_pk, prep.prep_pk,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = artg.lite_desc and mult.lang_pk = :lang_pk) as artg_desc,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = prep.lite_pk and mult.lang_pk = :lang_pk) as prep_desc");
            sb.AppendLine("from tnht_arvp arvp");
            sb.AppendLine("inner join tnht_arve arve on arve.arve_pk = arvp.arve_pk");
            sb.AppendLine("inner join tnht_prep prep on prep.prep_pk = arvp.prep_pk");
            sb.AppendLine("inner join tnht_artg artg on artg.artg_pk = arve.artg_pk");
            sb.AppendLine("where arve.vend_pk = :vend_pk");

            var query = new BaseQuery(Manager, "TicketPreparations", sb.ToString());
            query.Parameters["vend_pk"] = ticketId;
            query.Parameters["lang_pk"] = languageId;

            return query.Execute();
        }

        public ListData GetProductLinesAreas(Guid ticketId, long languageId)
        {
            var sb = new StringBuilder();
            sb.AppendLine("select arva.arva_pk, arve.arve_pk, area.area_pk");
            //sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = area.lite_pk and mult.lang_pk = :lang_pk) as area_desc");
            sb.AppendLine("from tnht_arva arva");
            sb.AppendLine("inner join tnht_arve arve on arve.arve_pk = arva.arve_pk");
            sb.AppendLine("inner join tnht_area area on area.area_pk = arva.area_pk");
            sb.AppendLine("where arve.vend_pk = :vend_pk");

            var query = new BaseQuery(Manager, "TicketPreparations", sb.ToString());
            query.Parameters["vend_pk"] = ticketId;
            //query.Parameters["lang_pk"] = languageId;

            return query.Execute();
        }

        public ListData GetProductTableLinesAreas(Guid ticketId)
        {
            var query = CommonQueryFactory.SelectProductTableLineAreasQuery(Manager);
            query.Parameters["VEND_PK"] = ticketId;
            return query.Execute();
        }

        public ListData GetProductLinePreparations(Guid productLineId, long languageId)
        {
            var sb = new StringBuilder();
            sb.AppendLine("select arvp.arvp_pk, arve.arve_pk, prep.prep_pk,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = artg.lite_desc and mult.lang_pk = :lang_pk) as artg_desc,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = prep.lite_pk and mult.lang_pk = :lang_pk) as prep_desc");
            sb.AppendLine("from tnht_arvp arvp");
            sb.AppendLine("inner join tnht_arve arve on arve.arve_pk = arvp.arve_pk");
            sb.AppendLine("inner join tnht_prep prep on prep.prep_pk = arvp.prep_pk");
            sb.AppendLine("inner join tnht_artg artg on artg.artg_pk = arve.artg_pk");
            sb.AppendLine("where arve.arve_pk = :arve_pk");

            var query = new BaseQuery(Manager, "TicketPreparations", sb.ToString());
            query.Parameters["arve_pk"] = productLineId;
            query.Parameters["lang_pk"] = languageId;

            return query.Execute();
        }

        private ListData GetProductLinesTableDetails(Guid ticketId, long languageId)
        {
            const string sb = @"
                SELECT TABL.TABL_PK,
                       TABL.ARVE_PK,
                       TABL.ARTG_PK,
                       TABL.ARTB_PK,
                       TABL.AREA_PK,
                       TABL.SEPA_PK,
                       TABL.SEPA_DESC AS SEPA_LITE_PK,
                       TABL.SEPA_ORDE,
                       TABL.TABL_CANT,
                       TABL.TABL_COST,
                       TABL.TABL_PREC,
                       TABL.TABL_PORC,
                       TABL.TABL_ADIC,
                       TABL.TABL_IMPR,
                       TABL.TABL_EMAR,
                       TABL.TABL_OBSV,
                       TABL.TABL_LMOD,
                       M1.MULT_DESC    AS SEPA_DESC,
                       M2.MULT_DESC    AS ARTG_ABRE,
                       M3.MULT_DESC    AS ARTG_DESC
                FROM TNHT_TABL TABL
                         INNER JOIN TNHT_ARVE ARVE ON ARVE.ARVE_PK = TABL.ARVE_PK
                         INNER JOIN TNHT_ARTG ARTG ON ARTG.ARTG_PK = TABL.ARTG_PK
                         LEFT JOIN VNHT_MULT M1 ON M1.LANG_PK = :LANG_PK AND M1.LITE_PK = TABL.SEPA_DESC
                         LEFT JOIN VNHT_MULT M2 ON M2.LANG_PK = :LANG_PK AND M2.LITE_PK = ARTG.LITE_ABRE
                         LEFT JOIN VNHT_MULT M3 ON M3.LANG_PK = :LANG_PK AND M3.LITE_PK = ARTG.LITE_DESC
                WHERE ARVE.VEND_PK = :VEND_PK
                ORDER BY SEPA_ORDE, ARTG_DESC
            ";

            var query = new BaseQuery(Manager, "TicketPreparationsQuery", sb)
            {
                Parameters =
                {
                    ["VEND_PK"] = ticketId,
                    ["LANG_PK"] = languageId
                }
            };

            return query.Execute();
        }

        private ListData GetProductLinesTablePreparations(Guid ticketId, long languageId)
        {
            var sb = new StringBuilder();
            sb.AppendLine("select tabp.tabp_pk, tabp.tabl_pk, tabp.prep_pk,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = prep.lite_pk and mult.lang_pk = :lang_pk) as prep_desc");
            sb.AppendLine("from tnht_tabp tabp");
            sb.AppendLine("inner join tnht_tabl tabl on tabl.tabl_pk = tabp.tabl_pk");
            sb.AppendLine("inner join tnht_arve arve on arve.arve_pk = tabl.arve_pk");
            sb.AppendLine("inner join tnht_prep prep on prep.prep_pk = tabp.prep_pk");
            sb.AppendLine("where arve.vend_pk = :vend_pk");

            var query = new BaseQuery(Manager, "TicketPreparations", sb.ToString());
            query.Parameters["vend_pk"] = ticketId;
            query.Parameters["lang_pk"] = languageId;

            return query.Execute();
        }

        private ListData GetPaymentLines(Guid ticketId, long languageId)
        {
            var query = CommonQueryFactory.GetPaymentLinesByTicket(Manager);
            query.Parameters["vend_pk"] = ticketId;
            query.Parameters["lang_pk"] = languageId;

            return query.Execute();
        }

        private ListData GetClientsByPosition(Guid ticketId)
        {
            var query = CommonQueryFactory.GetClientsPositionByTicket(Manager);
            query.Parameters["vend_pk"] = ticketId;

            return query.Execute();
        }

        private ListData GetClientsByPositionPreferences(Guid ticketId)
        {
            var query = CommonQueryFactory.GetClientsPositionPreferencesByTicket(Manager);
            query.Parameters["vend_pk"] = ticketId;

            return query.Execute();
        }

        private string GetSerieValidationCode(string serie)
        {
            var query = CommonQueryFactory.GetSerieValidationCode(Manager);
            query.Parameters["serie"] = serie;

            var records = query.Execute();
            if (!records.IsEmpty)
                return records.ValueAs<string>("sedo_cova") ?? string.Empty;

            return null;
        }

        private ListData GetClientsByPositionDiets(Guid ticketId)
        {
            var query = CommonQueryFactory.GetClientsPositionDietsByTicket(Manager);
            query.Parameters["vend_pk"] = ticketId;

            return query.Execute();
        }

        private ListData GetClientsByPositionAttentions(Guid ticketId)
        {
            var query = CommonQueryFactory.GetClientsPositionAttentionsByTicket(Manager);
            query.Parameters["vend_pk"] = ticketId;

            return query.Execute();
        }

        public ListData GetClientsByPositionAllergies(Guid ticketId)
        {
            var query = CommonQueryFactory.GetClientsPositionAllergiesByTicket(Manager);
            query.Parameters["vend_pk"] = ticketId;

            return query.Execute();
        }

        public ListData GetLookupTables(Guid ticketId)
        {
            var sb = new StringBuilder();
            sb.AppendLine("select lktb_pk, vend_pk, util_pk, lktb_seri, lktb_codi, lktb_date,");
            sb.AppendLine("       lktb_stim, lktb_tota, lktb_sign, lktb_cova, lktb_qrco, lktb_qrdt");
            sb.AppendLine("from tnht_lktb where vend_pk = :vend_pk");
            sb.AppendLine("order by lktb_stim");

            var query = new BaseQuery(Manager, "LookupTables", sb.ToString());
            query.Parameters["vend_pk"] = ticketId;

            return query.Execute();
        }

        public Guid? GetFastPaymentMethod()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select para_fore from tcfg_npos where fast_paym = :true");

            var query = new BaseQuery(Manager, "FastPayment", sb.ToString());
            query.Parameters["true"] = true;

            var record = query.Execute();
            if (!record.IsEmpty)
                return record.ValueAs<Guid?>("para_fore");

            return null;
        }

        private ProductRecord.ProductRecord? GetProductRecordForTicketCreation(Guid productId, Guid? standId = null, long? langId = null)
        {
            var product = new ProductRecord.ProductRecord();

            var query = CommonQueryFactory.GetPricesByStand(Manager);
            query.Parameters["lang_pk"] = langId ?? sessionContext.LanguageId;
            query.Parameters["ipos_pk"] = standId ?? sessionContext.StandId;
            query.Filters["artg_pk"].Value = productId;

            var records = query.Execute();
            if (records.IsEmpty) return null;

            product.Id = records.ValueAs<Guid>("artg_pk");
            product.Description = records.ValueAs<string>("artg_desc");
            product.ProductCode = records.ValueAs<string>("artg_codi");
            product.ProductTableType = records.ValueAs<TableType?>("artg_tabl");
            product.SeparatorId = records.ValueAs<Guid?>("sepa_pk");
            product.StandardPrice = records.ValueAs<decimal?>("stdr_valo");
            product.HouseUsePrice = records.ValueAs<decimal?>("cint_valo");
            product.MealPlanPrice = records.ValueAs<decimal?>("pens_valo");
            product.AccompanyingPrice = records.ValueAs<decimal?>("ACOM_VALO");
            product.DiscountPercent = records.ValueAs<decimal?>("artg_pdes");

            if (product.ProductTableType.HasValue)
            {
                product.TableProducts = [];

                var tableProductQuery = CommonQueryFactory.GetProductTablesByProduct(Manager);
                tableProductQuery.SetParameter("LANG_PK", sessionContext.LanguageId);
                tableProductQuery.SetParameter("ARTG_PK", productId);

                records = tableProductQuery.Execute();
                foreach (IRecord record in records)
                {
                    var table = new ProductTableRecord
                    {
                        Id = record.ValueAs<Guid>("artb_pk"),
                        TableId = record.ValueAs<Guid>("artg_pk"),
                        ProductAbbreviation = record.ValueAs<string>("artg_abre"),
                        ProductDescription = record.ValueAs<string>("artg_desc"),
                        ProductId = record.ValueAs<Guid>("artb_codi"),
                        Quantity = record.ValueAs<decimal>("artb_cant"),
                        MenuSeparatorSelection = record.ValueAs<int>("TBSP_MXSE"),
                        MenuSeparatorId = record.ValueAs<Guid?>("TBSP_PK"),
                        MenuSeparatorOrder = record.ValueAs<short?>("TBSP_ORDER"),
                        MenuSeparatorDescription = record.ValueAs<string>("TBSP_DESC"),
                        MenuSeparatorDescriptionId = record.ValueAs<Guid?>("TBSP_DESC_PK"),
                    };

                    product.TableProducts.Add(table);
                }
            }
            else
            {
                var accompanyingProductsQuery = QueryFactory.Get<AccompanyingProductsQuery>(Manager);
                accompanyingProductsQuery.Parameters["IPOS_PK"] = standId ?? sessionContext.StandId;
                accompanyingProductsQuery.Filters["ARTG_PK"].Value = productId;

                records = accompanyingProductsQuery.Execute();
                foreach (IRecord record in records)
                {
                    product.AccompanyingProducts.Add(record.ValueAs<Guid>("ARCH_PK"));
                }
            }

            return product;
        }

        private List<Guid> GetProductAreasForTicketCreation(Guid productId, Guid standId)
        {
            var query = CommonQueryFactory.GetDispatchAreaByProduct(Manager);
            query.Parameters["ARTG_PK"] = productId;
            query.Parameters["IPOS_PK"] = standId;

            var areas = new List<Guid>();

            var records = query.Execute();
            if (records.IsEmpty) return areas;
            foreach (IRecord record in records)
                areas.Add(record.ValueAs<Guid>("AREA_PK"));

            return areas;
        }

        public ProductRecord.ProductRecord GetProductRecordForTicketCreation(string productCode)
        {
            var product = new ProductRecord.ProductRecord();

            var sb = new StringBuilder();
            sb.AppendLine("select artg.artg_pk,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = artg.lite_desc and mult.lang_pk = :lang_pk) as artg_desc,");
            sb.AppendLine("       artg.artg_codi, artg.sepa_pk, tprl.tprl_valor as stdr_valo");
            sb.AppendLine("from tnht_pvar pvar");
            sb.AppendLine("inner join tnht_ipos ipos on ipos.ipos_pk = pvar.ipos_pk");
            sb.AppendLine("inner join tnht_tprg tprg on tprg.tprg_pk = ipos.tprg_stdr");
            sb.AppendLine("inner join tnht_tppe tppe on tppe.tprg_pk = tprg.tprg_pk");
            sb.AppendLine("and ipos.ipos_fctr between tppe.tppe_dain and tppe.tppe_dafi");
            sb.AppendLine("inner join tnht_tprl tprl on tprl.tppe_pk = tppe.tppe_pk");
            sb.AppendLine("inner join tnht_artg artg on artg.artg_pk = tprl.artg_pk");
            sb.AppendLine("where ipos.ipos_pk = :ipos_pk and artg.artg_codi = :artg_codi");

            var query = new BaseQuery(Manager, "GetMinimalProductInfo", sb.ToString());
            query.Parameters["lang_pk"] = sessionContext.LanguageId;
            query.Parameters["ipos_pk"] = sessionContext.StandId;
            query.Parameters["artg_codi"] = productCode;

            var records = query.Execute();
            if (!records.IsEmpty)
            {
                product.Id = records.ValueAs<Guid>("artg_pk");
                product.Description = records.ValueAs<string>("artg_desc");
                product.ProductCode = records.ValueAs<string>("artg_codi");
                product.SeparatorId = records.ValueAs<Guid?>("sepa_pk");
                product.StandardPrice = records.ValueAs<decimal>("stdr_valo");

                return product;
            }

            return null;
        }

        public ListData GetLookupTableDetails(Guid ticketId)
        {
            var sb = new StringBuilder();
            sb.AppendLine("select lkde.lkde_pk, lkde.lktb_pk, lkde.arve_qtds,");
            sb.AppendLine("       lkde.arve_totl, lkde.arve_vliq, lkde.artg_pk, lkde.arve_anul,");
            sb.AppendLine("       lkde.ivas_codi, lkde.arve_por1, lkde.arve_ivas,");
            sb.AppendLine("       lkde.ivas_cod2, lkde.arve_por2, lkde.arve_iva2,");
            sb.AppendLine("       lkde.ivas_cod3, lkde.arve_por3, lkde.arve_iva3");
            sb.AppendLine("from tnht_lktb lktb inner join tnht_lkde lkde on lkde.lktb_pk = lktb.lktb_pk");
            sb.AppendLine("where lktb.vend_pk = :vend_pk");

            var query = new BaseQuery(Manager, "LookupTableDetails", sb.ToString());
            query.Parameters["vend_pk"] = ticketId;

            return query.Execute();
        }

        public ValidationResult LoadTaxesData(POSProductLineContract productLine, string country,
            Guid taxSchemaId, Guid hotelId, bool taxesIncluded, ICanExchangeCurrency canExchangeCurrency, bool isTip = false)
        {
            var result = new ValidationResult();

            // Discount
            productLine.DiscountValue = productLine.DiscountPercent.HasValue ? productLine.ValueBeforeDiscount.GetDiscountValue(productLine.DiscountPercent.Value) : decimal.Zero;
            productLine.GrossValue = productLine.NetValue = productLine.ValueBeforeDiscount - productLine.DiscountValue;
            productLine.CostValue = productLine.ValueBeforeDiscount - productLine.DiscountValue + productLine.Recharge;

            var productBusiness = BusinessContext.GetBusiness<IProductBusiness>(Manager);

            var included = isTip || taxesIncluded;
            var value = included ? productLine.GrossValue : productLine.NetValue;

            var defResult = productBusiness.CalculateTaxRates(productLine.Product, country, value, taxSchemaId, hotelId, included);
            if (defResult.IsEmpty)
            {
                if (defResult.Contract is TaxContract taxes)
                {
                    productLine.GrossValue = taxes.GrossValue;
                    productLine.NetValue = taxes.NetValue;

                    foreach (var taxDetail in taxes.Taxes)
                    {
                        switch (taxDetail.TaxLevel)
                        {
                            case 0:
                                if (taxDetail.TaxRateId.HasValue)
                                {
                                    productLine.FirstIvaId = taxDetail.TaxRateId.Value;
                                    productLine.FirstIvaAuxCode = taxDetail.TaxAuxCode;
                                    productLine.FirstIvaDescription = taxDetail.TaxRateDescription;
                                    productLine.FirstIvaPercent = taxDetail.Percent;
                                    productLine.FirstIvaBase = taxDetail.TaxBase;
                                    productLine.FirstIvaValue = taxDetail.TaxValue;
                                }
                                else
                                {
                                    productLine.FirstIvaPercent = decimal.Zero;
                                    productLine.FirstIvaValue = decimal.Zero;
                                    productLine.FirstIvaBase = decimal.Zero;
                                }

                                break;
                            case 1:
                                if (taxDetail.TaxRateId.HasValue)
                                {
                                    productLine.SecondIvaId = taxDetail.TaxRateId.Value;
                                    productLine.SecondIvaAuxCode = taxDetail.TaxAuxCode;
                                    productLine.SecondIvaDescription = taxDetail.TaxRateDescription;
                                    productLine.SecondIvaPercent = taxDetail.Percent;
                                    productLine.SecondIvaBase = taxDetail.TaxBase;
                                    productLine.SecondIvaValue = taxDetail.TaxValue;
                                }
                                else
                                {
                                    productLine.SecondIvaId = null;
                                    productLine.SecondIvaPercent = decimal.Zero;
                                    productLine.SecondIvaValue = decimal.Zero;
                                    productLine.SecondIvaBase = decimal.Zero;
                                }

                                break;
                            case 2:
                                if (taxDetail.TaxRateId.HasValue)
                                {
                                    productLine.ThirdIvaId = taxDetail.TaxRateId.Value;
                                    productLine.ThirdIvaAuxCode = taxDetail.TaxAuxCode;
                                    productLine.ThirdIvaDescription = taxDetail.TaxRateDescription;
                                    productLine.ThirdIvaPercent = taxDetail.Percent;
                                    productLine.ThirdIvaValue = taxDetail.TaxValue;
                                    productLine.ThirdIvaBase = taxDetail.TaxBase;
                                }
                                else
                                {
                                    productLine.ThirdIvaId = null;
                                    productLine.ThirdIvaPercent = decimal.Zero;
                                    productLine.ThirdIvaValue = decimal.Zero;
                                    productLine.ThirdIvaBase = decimal.Zero;
                                }

                                break;
                        }
                    }
                }
                productLine.GrossValueInBase = canExchangeCurrency.ExchangeFactor * productLine.GrossValue;
            }
            else
                result.AddValidations(defResult);

            return result;
        }

        public ValidationContractResult UpdateTicketTip(Guid ticketId, decimal? tipPercent, decimal? tipValue)
        {
            var ticket = LoadTicket(ticketId).Contract.As<POSTicketContract>();

            // Add tip to ticket
            ticket.TipPercent = tipPercent;
            ticket.TipValue = tipValue;

            return PersistTicket(ticket);
        }

        public ValidationContractResult ApplyTicketDiscounts(Guid ticketId,
            IEnumerable<string> productsId, Guid? discountTypeId, decimal? discountPercent)
        {
            var result = new ValidationContractResult();

            var loadTicketResult = LoadTicket(ticketId);
            if (loadTicketResult.IsEmpty)
            {
                var ticket = loadTicketResult.Contract.As<POSTicketContract>();
                foreach (var id in productsId)
                {
                    var productLine = ticket.ProductLineByTicket
                        .FirstOrDefault(productLine => (Guid)productLine.Id == id.AsGuid());

                    if (productLine == null)
                        continue;

                    productLine.DiscountTypeId = discountTypeId;
                    productLine.DiscountPercent = discountPercent;
                    var taxSchemaId = ticket.TaxSchemaId ?? sessionContext.TaxSchemaId;
                    result.Add(LoadTaxesData(productLine, sessionContext.Country,
                        taxSchemaId, sessionContext.InstallationId, ticket.TaxIncluded, ticket));

                    if (!result.IsEmpty)
                        break;
                }

                if (!result.IsEmpty) return result;

                ticket.GeneralDiscount = ticket.ProductLineByTicket.Sum(x => x.AnnulmentCode == 0 && !x.IsTip ? x.DiscountValue : decimal.Zero);

                result = PersistTicket(ticket);
            }
            else
                result.Add(loadTicketResult);

            return result;
        }

        #endregion

        #region Handheld

        public ValidationContractResult AttendTableOrder(Guid tableId)
        {
            var result = new ValidationContractResult();

            try
            {
                var placedOrders = GetMenuOrdersByTable(sessionContext.StandId, tableId, sessionContext.LanguageId)
                    .Where(record => record.Placed).ToList();
                if (placedOrders.Count > 0)
                {
                    var productLines = placedOrders.Select(record => new ProductLineDto
                    {
                        ProductId = record.ProductId,
                        Quantity = (int)record.Quantity,
                        Preparations = record.Preparations
                    }).ToList();

                    result = CreateMenuDigitalTicket(tableId, productLines.ToArray());

                    if (!result.HasErrors)
                    {
                        foreach (var order in placedOrders)
                        {
                            DeleteMenuOrder(order.Id);
                        }
                    }

                    // Digital menu notification
                    BusinessContext.SocketIOClient?.Publish("OrderAttended", tableId);
                }
                else
                {
                    throw new Exception("There are no orders to attend");
                }
            }
            catch (Exception e)
            {
                result.AddException(e);
            }

            return result;
        }

        public ValidationContractResult AddGuestToSeat(Guid ticketId, GuestContract guest, short pax)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var loadTicketResult = LoadTicket(ticketId);
                    if (loadTicketResult.IsEmpty)
                    {
                        var ticket = loadTicketResult.Contract.As<POSTicketContract>();

                        var clientByPosition = ticket.ClientsByPosition.FirstOrDefault(cp => cp.Pax == pax);
                        if (clientByPosition == null)
                        {
                            clientByPosition = new POSClientByPositionContract { Pax = pax };
                            ticket.ClientsByPosition.Add(clientByPosition);
                        }

                        clientByPosition.ClientId = guest.Id.ToString().AsGuid();
                        clientByPosition.ClientName = guest.FirstName + guest.LastName;
                        clientByPosition.ClientRoom = guest.Room;

                        clientByPosition.Preferences.Clear();
                        clientByPosition.Preferences.AddRange(guest.TableReservationPreferences
                            .Select(p => new POSClientByPositionPreferenceContract(Guid.NewGuid(), p.PreferenceId, p.PreferenceDesc)));

                        clientByPosition.Diets.Clear();
                        clientByPosition.Diets.AddRange(guest.TableReservationDiets
                            .Select(p => new POSClientByPositionDietContract(Guid.NewGuid(), p.DietId, p.DietDescription)));

                        clientByPosition.Attentions.Clear();
                        clientByPosition.Attentions.AddRange(guest.TableReservationAttentions
                            .Select(p => new POSClientByPositionAttentionContract(Guid.NewGuid(), p.AttentionType, p.AttentionTypeDesc)));

                        clientByPosition.Allergies.Clear();
                        clientByPosition.Allergies.AddRange(guest.TableReservationAllergies
                            .Select(p => new POSClientByPositionAllergyContract(Guid.NewGuid(), p.AllergyId, p.AllergyDescription)));

                        result = PersistTicket(ticket);
                    }
                    else
                        result.Add(loadTicketResult);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
					logsFactory.Error(ex);
					result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult UpdateOrderPreparations(Guid ticketId, ProductLinePreparationContract contract)
        {
            var result = new ValidationContractResult();

            try
            {
                result = LoadTicket(ticketId);
                var ticket = result.Contract.As<POSTicketContract>();

                var productLineId = contract.ProductLineId;
                var productLine = ticket.ProductLineByTicket.FirstOrDefault(x => (Guid)x.Id == productLineId);

                if (productLine == null) return result;

                productLine.Preparations.Clear();

                foreach (var preparationId in contract.PreparationIds)
                {
                    var preparation = new POSProductLinePreparationContract(
                        Guid.NewGuid(),
                        productLineId,
                        string.Empty,
                        preparationId,
                        string.Empty
                    );
                    productLine.Preparations.Add(preparation);
                }

                foreach (var tableLineContract in contract.TableLineIds)
                {
                    var tableLineId = tableLineContract.TableLineId;
                    var tableLine = productLine.TableLines.FirstOrDefault(x => (Guid)x.Id == tableLineId);

                    if (tableLine == null) continue;
                    tableLine.TableLinePreparations.Clear();

                    foreach (var preparationId in tableLineContract.PreparationIds)
                    {
                        var preparation = new TableLinePreparationContract(
                            Guid.NewGuid(),
                            preparationId,
                            tableLineId,
                            string.Empty,
                            string.Empty
                        );
                        tableLine.TableLinePreparations.Add(preparation);
                    }
                }

                return ChangeTicketProductDetails(ticket, productLineId, productLine);
            }
            catch (Exception ex)
            {
				logsFactory.Error(ex);
				result.AddException(ex);
            }

            return result;
        }

        public ValidationContractResult UpdateOrderNote(Guid ticketId, ProductLineNoteContract contract)
        {
            var result = new ValidationContractResult();

            try
            {
                result = LoadTicket(ticketId);
                var ticket = result.Contract.As<POSTicketContract>();

                var productLineId = contract.ProductLineId;
                var productLine = ticket.ProductLineByTicket.FirstOrDefault(x => (Guid)x.Id == productLineId);

                if (productLine == null) return result;

                productLine.Observations = contract.Note;

                foreach (var tableLineContract in contract.TableLineIds)
                {
                    var tableLineId = tableLineContract.TableLineId;
                    var tableLine = productLine.TableLines.FirstOrDefault(x => (Guid)x.Id == tableLineId);

                    if (tableLine == null) continue;

                    tableLine.Observations = tableLineContract.Note;
                }

                return ChangeTicketProductDetails(ticket, productLineId, productLine);
            }
            catch (Exception ex)
            {
				logsFactory.Error(ex);
				result.AddException(ex);
            }

            return result;
        }

        public ValidationContractResult UpdateTicketInfo(Guid ticketId, short pax, string description)
        {
            var result = new ValidationContractResult();

            try
            {
                result = LoadTicket(ticketId);
                if (!result.IsEmpty) return result;

                var ticket = result.Contract.As<POSTicketContract>();
                ticket.Paxs = pax;
                ticket.Name = description;

                result = PersistTicket(ticket);
            }
            catch (Exception ex)
            {
				logsFactory.Error(ex);
				result.AddException(ex);
            }

            return result;
        }

        public ValidationContractResult AddReservationToTicket(Guid ticketId, ReservationRecord reservation)
        {
            var result = LoadTicket(ticketId);

            if (result.HasErrors) return result;

            var ticket = result.Contract.As<POSTicketContract>();

            if (reservation.IsLock)
            {
                result = new ValidationContractResult();
                result.AddError("InvalidData".Translate());
                return result;
            }

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    ticket.Account = reservation.AccountId;
                    ticket.AccountType = CurrentAccountType.Reservation;
                    ticket.AccountInstallationId = sessionContext.InstallationId;

                    ticket.FiscalDocTo = reservation.Guests;
                    ticket.Room = reservation.Room;
                    ticket.Pension = reservation.Pension;

                    var accountDescription = $"{reservation.ReservationNumber} ({reservation.Room})";
                    accountDescription += $"\n{reservation.Guests}";
                    if (!string.IsNullOrEmpty(reservation.Company))
                        accountDescription += $"\n{reservation.Company}";
                    ticket.AccountDescription = accountDescription;

                    ticket.AllowRoomCredit = reservation.AllowCreditPos;

                    result = PersistTicket(ticket);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
					logsFactory.Error(ex);
					result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult AddSpaReservationToTicket(Guid ticketId, SpaReservationRecord reservation)
        {
            var result = LoadTicket(ticketId);

            if (result.HasErrors) return result;

            var ticket = result.Contract.As<POSTicketContract>();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    ticket.Account = reservation.AccountId;
                    ticket.AccountType = CurrentAccountType.SpaReservation;
                    ticket.AccountInstallationId = sessionContext.InstallationId;


                    ticket.Account = reservation.AccountId;
                    ticket.Room = reservation.Room;
                    ticket.FiscalDocTo = reservation.Guest;

                    var accountDescription = "";
                    accountDescription += $"{reservation.ReservationNumber} ({reservation.Room})";
                    accountDescription += $"\n{reservation.Guest}";
                    ticket.AccountDescription = accountDescription;

                    result = PersistTicket(ticket);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    Trace.TraceError(ex.Message);
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult UpdateClientsByPosition(Guid ticketId, IEnumerable<POSClientByPositionContract> clients)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var loadTicketResult = LoadTicket(ticketId);
                    if (loadTicketResult.IsEmpty)
                    {
                        var ticket = loadTicketResult.Contract.As<POSTicketContract>();

                        // Add or update clients
                        foreach (var client in clients)
                        {
                            var clientByPosition = ticket.ClientsByPosition.FirstOrDefault(cp => cp.Pax == client.Pax);
                            if (clientByPosition == null)
                            {
                                clientByPosition = new POSClientByPositionContract { Pax = client.Pax };
                                ticket.ClientsByPosition.Add(clientByPosition);
                            }

                            clientByPosition.ClientId = client.ClientId;
                            clientByPosition.ClientName = client.ClientName;
                            clientByPosition.ClientRoom = client.ClientRoom;
                            clientByPosition.UncommonAllergies = client.UncommonAllergies;
                            clientByPosition.SegmentOperations = client.SegmentOperations;

                            clientByPosition.Preferences.Clear();
                            clientByPosition.Preferences.AddRange(client.Preferences
                                .Select(p => new POSClientByPositionPreferenceContract(Guid.NewGuid(), p.PreferenceId, p.Description)));

                            clientByPosition.Diets.Clear();
                            clientByPosition.Diets.AddRange(client.Diets
                                .Select(p => new POSClientByPositionDietContract(Guid.NewGuid(), p.DietId, p.Description)));

                            clientByPosition.Attentions.Clear();
                            clientByPosition.Attentions.AddRange(client.Attentions
                                .Select(p => new POSClientByPositionAttentionContract(Guid.NewGuid(), p.AttentionId, p.Description)));

                            clientByPosition.Allergies.Clear();
                            clientByPosition.Allergies.AddRange(client.Allergies
                                .Select(p => new POSClientByPositionAllergyContract(Guid.NewGuid(), p.AllergyId, p.Description)));
                        }

                        // Remove if not exits
                        ticket.ClientsByPosition.Remove(cp => clients.FirstOrDefault(x => x.Pax == cp.Pax) == null);

                        result = PersistTicket(ticket);
                    }
                    else
                        result.Add(loadTicketResult);

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
					logsFactory.Error(ex);
					result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        private Ticket LoadTicketForPersist(Guid ticketId)
        {
            try
            {
                Manager.Open();

                var ticket = ticketRepository.GetTicket(ticketId, false);

                // Load product lines
                Dictionary<Guid, ProductLine> productLines = new();
                using (var command = Manager.CreateCommand("NewHotel.Load.ProductLinesByTicket"))
                {
                    var type = typeof(ProductLine);
                    var query = Manager.GetLoadCommandText(type);
                    query.ClearWhere();
                    query.AppendWhere("TNHT_ARVE.VEND_PK = :VEND_PK{1}");
                    command.CommandText = string.Format(query.ToString(), string.Empty, string.Empty);
                    Manager.CreateParameter(command, "VEND_PK", ticketId);
                    using (var reader = Manager.ExecuteReader(command))
                    {
                        var data = Manager.GetQueryReader(reader);
                        while (data.Read())
                        {
                            var productLine = new ProductLine(Manager, false);
                            productLine.LoadObjectFromDB(data);

                            productLines.Add((Guid)productLine.Id, productLine);
                            ticket.ProductLineByTicket.Add(productLine);
                        }
                    }
                }

                // Load preparations lines
                using (var command = Manager.CreateCommand("NewHotel.Load.PreparationLinesByTicket"))
                {
                    var type = typeof(PreparationsByLine);
                    var queryStart = Manager.GetLoadCommandText(type);
                    queryStart.ClearWhere();
                    const string queryEnd = """
                                            INNER JOIN TNHT_ARVE ON TNHT_ARVE.ARVE_PK = TNHT_ARVP.ARVE_PK
                                            WHERE TNHT_ARVE.VEND_PK = :VEND_PK{1}
                                            """;
                    command.CommandText = string.Format(queryStart + queryEnd, string.Empty, string.Empty);
                    Manager.CreateParameter(command, "VEND_PK", ticketId);
                    using (var reader = Manager.ExecuteReader(command))
                    {
                        var data = Manager.GetQueryReader(reader);
                        while (data.Read())
                        {
                            var preparationLine = new PreparationsByLine(Manager);
                            preparationLine.LoadObjectFromDB(data);

                            if (productLines.TryGetValue(preparationLine.ProductLineId, out var productLine))
                            {
                                productLine.PreparationsByLine.Add(preparationLine);
                            }
                        }
                    }
                }

                // Load areas lines
                using (var command = Manager.CreateCommand("NewHotel.Load.AreaLinesByTicket"))
                {
                    var type = typeof(AreasByLine);
                    var queryStart = Manager.GetLoadCommandText(type);
                    queryStart.ClearWhere();
                    const string queryEnd = """
                                            INNER JOIN TNHT_ARVE ON TNHT_ARVE.ARVE_PK = TNHT_ARVA.ARVE_PK
                                            WHERE TNHT_ARVE.VEND_PK = :VEND_PK{1}
                                            """;
                    command.CommandText = string.Format(queryStart + queryEnd, string.Empty, string.Empty);
                    Manager.CreateParameter(command, "VEND_PK", ticketId);
                    using (var reader = Manager.ExecuteReader(command))
                    {
                        var data = Manager.GetQueryReader(reader);
                        while (data.Read())
                        {
                            var areaLine = new AreasByLine(Manager);
                            areaLine.LoadObjectFromDB(data);

                            if (productLines.TryGetValue(areaLine.ProductLineId, out var productLine))
                            {
                                productLine.AreasByLine.Add(areaLine);
                            }
                        }
                    }
                }

                // Load table lines
                Dictionary<Guid, ProductTableLine> tableLines = new();
                using (var command = Manager.CreateCommand("NewHotel.Load.TableLinesByTicket"))
                {
                    var type = typeof(ProductTableLine);
                    var queryStart = Manager.GetLoadCommandText(type);
                    queryStart.ClearWhere();
                    const string queryEnd = """
                                            INNER JOIN TNHT_ARVE ON TNHT_ARVE.ARVE_PK = TNHT_TABL.ARVE_PK
                                            WHERE TNHT_ARVE.VEND_PK = :VEND_PK{1}
                                            """;
                    command.CommandText = string.Format(queryStart + queryEnd, string.Empty, string.Empty);
                    Manager.CreateParameter(command, "VEND_PK", ticketId);
                    using (var reader = Manager.ExecuteReader(command))
                    {
                        var data = Manager.GetQueryReader(reader);
                        while (data.Read())
                        {
                            var tableLine = new ProductTableLine(Manager, false);
                            tableLine.LoadObjectFromDB(data);

                            tableLines.Add((Guid)tableLine.Id, tableLine);
                            if (productLines.TryGetValue(tableLine.ProductLineId, out var productLine))
                            {
                                productLine.TableProductsByLine.Add(tableLine);
                            }
                        }
                    }
                }


                // Load table line preparations
                using (var command = Manager.CreateCommand("NewHotel.Load.TablePreparationLinesByTicket"))
                {
                    var type = typeof(ProductsTableLinePreparation);
                    var queryStart = Manager.GetLoadCommandText(type);
                    queryStart.ClearWhere();
                    const string queryEnd = """
                                            INNER JOIN TNHT_TABL ON TNHT_TABL.TABL_PK = TNHT_TABP.TABL_PK
                                            INNER JOIN TNHT_ARVE ON TNHT_ARVE.ARVE_PK = TNHT_TABL.ARVE_PK
                                            WHERE TNHT_ARVE.VEND_PK = :VEND_PK{1}
                                            """;
                    command.CommandText = string.Format(queryStart + queryEnd, string.Empty, string.Empty);
                    Manager.CreateParameter(command, "VEND_PK", ticketId);
                    using (var reader = Manager.ExecuteReader(command))
                    {
                        var data = Manager.GetQueryReader(reader);
                        while (data.Read())
                        {
                            var tablePreparationLine = new ProductsTableLinePreparation(Manager);
                            tablePreparationLine.LoadObjectFromDB(data);

                            if (tableLines.TryGetValue(tablePreparationLine.TableLineId, out var tableLine))
                            {
                                tableLine.TableProductsByLinePreparation.Add(tablePreparationLine);
                            }
                        }
                    }
                }

                // Load table line areas
                using (var command = Manager.CreateCommand("NewHotel.Load.TableAreaLinesByTicket"))
                {
                    var type = typeof(ProductTableLineArea);
                    var queryStart = Manager.GetLoadCommandText(type);
                    queryStart.ClearWhere();
                    const string queryEnd = """
                                            INNER JOIN TNHT_TABL ON TNHT_TABL.TABL_PK = TNHT_TABA.TABL_PK
                                            INNER JOIN TNHT_ARVE ON TNHT_ARVE.ARVE_PK = TNHT_TABL.ARVE_PK
                                            WHERE TNHT_ARVE.VEND_PK = :VEND_PK{1}
                                            """;
                    command.CommandText = string.Format(queryStart + queryEnd, string.Empty, string.Empty);
                    Manager.CreateParameter(command, "VEND_PK", ticketId);
                    using (var reader = Manager.ExecuteReader(command))
                    {
                        var data = Manager.GetQueryReader(reader);
                        while (data.Read())
                        {
                            var tableAreaLine = new ProductTableLineArea(Manager);
                            tableAreaLine.LoadObjectFromDB(data);

                            if (tableLines.TryGetValue(tableAreaLine.ProductTableLineId, out var tableLine))
                            {
                                tableLine.AreasByLine.Add(tableAreaLine);
                            }
                        }
                    }
                }

                // Load payment lines
                using (var command = Manager.CreateCommand("NewHotel.Load.PaymentLinesByTicket"))
                {
                    var type = typeof(PaymentLine);
                    var query = Manager.GetLoadCommandText(type);
                    query.ClearWhere();
                    query.AppendWhere("TNHT_PAVE.VEND_PK = :VEND_PK{1}");
                    command.CommandText = string.Format(query.ToString(), string.Empty, string.Empty);
                    Manager.CreateParameter(command, "VEND_PK", ticketId);
                    using (var reader = Manager.ExecuteReader(command))
                    {
                        var data = Manager.GetQueryReader(reader);
                        while (data.Read())
                        {
                            var paymentLine = new PaymentLine(Manager);
                            paymentLine.LoadObjectFromDB(data);

                            ticket.PaymentLineByTicket.Add(paymentLine);
                        }
                    }
                }

                // Load lookup table lines
                Dictionary<Guid, LookupTable> lookupTableLines = new();
                using (var command = Manager.CreateCommand("NewHotel.Load.LookupTableLinesByTicket"))
                {
                    var type = typeof(LookupTable);
                    var query = Manager.GetLoadCommandText(type);
                    query.ClearWhere();
                    query.AppendWhere("TNHT_LKTB.VEND_PK = :VEND_PK{1}");
                    command.CommandText = string.Format(query.ToString(), string.Empty, string.Empty);
                    Manager.CreateParameter(command, "VEND_PK", ticketId);
                    using (var reader = Manager.ExecuteReader(command))
                    {
                        var data = Manager.GetQueryReader(reader);
                        while (data.Read())
                        {
                            var lookupTableLine = new LookupTable(Manager, false);
                            lookupTableLine.LoadObjectFromDB(data);

                            lookupTableLines.Add((Guid)lookupTableLine.Id, lookupTableLine);
                            ticket.LookupTableByTicket.Add(lookupTableLine);
                        }
                    }
                }

                // Load lookup table detail lines
                using (var command = Manager.CreateCommand("NewHotel.Load.LookupTableDetailLinesByTicket"))
                {
                    var type = typeof(LookupTableDetails);
                    var queryStart = Manager.GetLoadCommandText(type);
                    queryStart.ClearWhere();
                    const string queryEnd = """
                                            INNER JOIN TNHT_LKTB ON TNHT_LKTB.LKTB_PK = TNHT_LKDE.LKTB_PK
                                            WHERE TNHT_LKTB.VEND_PK = :VEND_PK{1}
                                            """;
                    command.CommandText = string.Format(queryStart + queryEnd, string.Empty, string.Empty);
                    Manager.CreateParameter(command, "VEND_PK", ticketId);
                    using (var reader = Manager.ExecuteReader(command))
                    {
                        var data = Manager.GetQueryReader(reader);
                        while (data.Read())
                        {
                            var lookupTableDetailsLine = new LookupTableDetails(Manager);
                            lookupTableDetailsLine.LoadObjectFromDB(data);

                            if (lookupTableLines.TryGetValue(lookupTableDetailsLine.LookupTableId, out var lookupTableLine))
                            {
                                lookupTableLine.DetailsByLookup.Add(lookupTableDetailsLine);
                            }
                        }
                    }
                }

                // Load clients by position lines
                Dictionary<Guid, ClientByPosition> clientsByPositionLines = new();
                using (var command = Manager.CreateCommand("NewHotel.Load.ClientsByPositionLinesByTicket"))
                {
                    var type = typeof(ClientByPosition);
                    var query = Manager.GetLoadCommandText(type);
                    query.ClearWhere();
                    query.AppendWhere("TNHT_CLVE.VEND_PK = :VEND_PK{1}");
                    command.CommandText = string.Format(query.ToString(), string.Empty, string.Empty);
                    Manager.CreateParameter(command, "VEND_PK", ticketId);
                    using (var reader = Manager.ExecuteReader(command))
                    {
                        var data = Manager.GetQueryReader(reader);
                        while (data.Read())
                        {
                            var clientByPositionLine = new ClientByPosition(Manager, false);
                            clientByPositionLine.LoadObjectFromDB(data);

                            clientsByPositionLines.Add((Guid)clientByPositionLine.Id, clientByPositionLine);
                            ticket.ClientsByPosition.Add(clientByPositionLine);
                        }
                    }
                }

                // Load allergy lines
                using (var command = Manager.CreateCommand("NewHotel.Load.AllergyLinesByTicket"))
                {
                    var type = typeof(ClientByPositionAllergy);
                    var queryStart = Manager.GetLoadCommandText(type);
                    queryStart.ClearWhere();
                    const string queryEnd = """
                                            INNER JOIN TNHT_CLVE ON TNHT_CLVE.CLVE_PK = TNHT_CLAL.CLVE_PK
                                            WHERE TNHT_CLVE.VEND_PK = :VEND_PK{1}
                                            """;
                    command.CommandText = string.Format(queryStart + queryEnd, string.Empty, string.Empty);
                    Manager.CreateParameter(command, "VEND_PK", ticketId);
                    using (var reader = Manager.ExecuteReader(command))
                    {
                        var data = Manager.GetQueryReader(reader);
                        while (data.Read())
                        {
                            var allergyLine = new ClientByPositionAllergy(Manager);
                            allergyLine.LoadObjectFromDB(data);

                            if (clientsByPositionLines.TryGetValue(allergyLine.ClientByPositionId, out var clientByPositionLine))
                            {
                                clientByPositionLine.Allergies.Add(allergyLine);
                            }
                        }
                    }
                }

                // Load attention lines
                using (var command = Manager.CreateCommand("NewHotel.Load.AttentionLinesByTicket"))
                {
                    var type = typeof(ClientByPositionAttention);
                    var queryStart = Manager.GetLoadCommandText(type);
                    queryStart.ClearWhere();
                    const string queryEnd = """
                                            INNER JOIN TNHT_CLVE ON TNHT_CLVE.CLVE_PK = TNHT_CLAT.CLVE_PK
                                            WHERE TNHT_CLVE.VEND_PK = :VEND_PK{1}
                                            """;
                    command.CommandText = string.Format(queryStart + queryEnd, string.Empty, string.Empty);
                    Manager.CreateParameter(command, "VEND_PK", ticketId);
                    using (var reader = Manager.ExecuteReader(command))
                    {
                        var data = Manager.GetQueryReader(reader);
                        while (data.Read())
                        {
                            var attentionLine = new ClientByPositionAttention(Manager);
                            attentionLine.LoadObjectFromDB(data);

                            if (clientsByPositionLines.TryGetValue(attentionLine.ClientByPositionId, out var clientByPositionLine))
                            {
                                clientByPositionLine.Attentions.Add(attentionLine);
                            }
                        }
                    }
                }

                // Load diets lines
                using (var command = Manager.CreateCommand("NewHotel.Load.DietLinesByTicket"))
                {
                    var type = typeof(ClientByPositionDiet);
                    var queryStart = Manager.GetLoadCommandText(type);
                    queryStart.ClearWhere();
                    const string queryEnd = """
                                            INNER JOIN TNHT_CLVE ON TNHT_CLVE.CLVE_PK = TNHT_CLDI.CLVE_PK
                                            WHERE TNHT_CLVE.VEND_PK = :VEND_PK{1}
                                            """;
                    command.CommandText = string.Format(queryStart + queryEnd, string.Empty, string.Empty);
                    Manager.CreateParameter(command, "VEND_PK", ticketId);
                    using (var reader = Manager.ExecuteReader(command))
                    {
                        var data = Manager.GetQueryReader(reader);
                        while (data.Read())
                        {
                            var dietLine = new ClientByPositionDiet(Manager);
                            dietLine.LoadObjectFromDB(data);

                            if (clientsByPositionLines.TryGetValue(dietLine.ClientByPositionId, out var clientByPositionLine))
                            {
                                clientByPositionLine.Diets.Add(dietLine);
                            }
                        }
                    }
                }

                // Load preference lines
                using (var command = Manager.CreateCommand("NewHotel.Load.PreferenceLinesByTicket"))
                {
                    var type = typeof(ClientByPositionPreference);
                    var queryStart = Manager.GetLoadCommandText(type);
                    queryStart.ClearWhere();
                    const string queryEnd = """
                                            INNER JOIN TNHT_CLVE ON TNHT_CLVE.CLVE_PK = TNHT_CLPR.CLVE_PK
                                            WHERE TNHT_CLVE.VEND_PK = :VEND_PK{1}
                                            """;
                    command.CommandText = string.Format(queryStart + queryEnd, string.Empty, string.Empty);
                    Manager.CreateParameter(command, "VEND_PK", ticketId);
                    using (var reader = Manager.ExecuteReader(command))
                    {
                        var data = Manager.GetQueryReader(reader);
                        while (data.Read())
                        {
                            var preferenceLine = new ClientByPositionPreference(Manager);
                            preferenceLine.LoadObjectFromDB(data);

                            if (clientsByPositionLines.TryGetValue(preferenceLine.ClientByPositionId, out var clientByPositionLine))
                            {
                                clientByPositionLine.Preferences.Add(preferenceLine);
                            }
                        }
                    }
                }

                return ticket;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                Manager.Close();
            }
        }

        private static POSTicketContract ParseTicket(Ticket ticket)
        {
            var contract = new POSTicketContract();
            contract.SetValues(ticket.GetValues());

            // Product lines
            foreach (var productLine in ticket.ProductLineByTicket)
            {
                var productLineContract = new POSProductLineContract();
                productLineContract.SetValues(productLine.GetValues());

                // Preparation lines
                foreach (var preparationLine in productLine.PreparationsByLine)
                {
                    var preparationLineContract = new POSProductLinePreparationContract();
                    preparationLineContract.SetValues(preparationLine.GetValues());

                    productLineContract.Preparations.Add(preparationLineContract);
                }

                // Area lines
                foreach (var areaLine in productLine.AreasByLine)
                {
                    var areaLineContract = new POSProductLineAreaContract();
                    areaLineContract.SetValues(areaLine.GetValues());

                    productLineContract.Areas.Add(areaLineContract);
                }

                // Table lines
                foreach (var tableLine in productLine.TableProductsByLine)
                {
                    var tableLineContract = new TableLineContract();
                    tableLineContract.SetValues(tableLine.GetValues());

                    // Table preparation lines
                    foreach (var tablePreparationLine in tableLine.TableProductsByLinePreparation)
                    {
                        var tablePreparationLineContract = new TableLinePreparationContract();
                        tablePreparationLineContract.SetValues(tablePreparationLine.GetValues());

                        tableLineContract.TableLinePreparations.Add(tablePreparationLineContract);
                    }

                    // Table Area lines
                    foreach (var tableAreaLine in tableLine.AreasByLine)
                    {
                        var tableAreaLineContract = new POSProductLineAreaContract();
                        tableAreaLineContract.SetValues(tableAreaLine.GetValues());

                        tableLineContract.Areas.Add(tableAreaLineContract);
                    }

                    productLineContract.TableLines.Add(tableLineContract);
                }

                contract.ProductLineByTicket.Add(productLineContract);
            }

            // Client lines
            foreach (var clientLine in ticket.ClientsByPosition)
            {
                var clientContract = new POSClientByPositionContract();
                clientContract.SetValues(clientLine.GetValues());

                // Allergy lines
                foreach (var allergyLine in clientLine.Allergies)
                {
                    var allergyContract = new POSClientByPositionAllergyContract();
                    allergyContract.SetValues(allergyLine.GetValues());

                    clientContract.Allergies.Add(allergyContract);
                }

                // Attention lines
                foreach (var attentionLine in clientLine.Attentions)
                {
                    var attentionContract = new POSClientByPositionAttentionContract();
                    attentionContract.SetValues(attentionLine.GetValues());

                    clientContract.Attentions.Add(attentionContract);
                }

                // Diet lines
                foreach (var dietLine in clientLine.Diets)
                {
                    var dietContract = new POSClientByPositionDietContract();
                    dietContract.SetValues(dietLine.GetValues());

                    clientContract.Diets.Add(dietContract);
                }

                // Preference lines
                foreach (var preferenceLine in clientLine.Preferences)
                {
                    var preferenceContract = new POSClientByPositionPreferenceContract();
                    preferenceContract.SetValues(preferenceLine.GetValues());

                    clientContract.Preferences.Add(preferenceContract);
                }

                contract.ClientsByPosition.Add(clientContract);
            }

            return contract;
        }

        #endregion

        #region MenuDigital

        public ValidationItemSource<POSProductLineContract> GetTicketProductLines(Guid ticketId, int langId, Guid installationId)
        {
            var loadTicketResult = LoadTicket(ticketId, langId, installationId);
            if (loadTicketResult.HasErrors) return new ValidationItemSource<POSProductLineContract>(loadTicketResult);
            var contract = loadTicketResult.Contract as POSTicketContract;
            return new ValidationItemSource<POSProductLineContract>()
            {
                ItemSource = contract?.ProductLineByTicket != null ? new List<POSProductLineContract>(contract.ProductLineByTicket) : new List<POSProductLineContract>()
            };
        }

        public List<OrderRecord> GetMenuOrdersByTable(Guid standId, Guid tableId, long langId)
        {
            var list = new List<OrderRecord>();

            Manager.Open();
            try
            {
                var query = new BaseQuery(
                    Manager,
                    "GetMenuOrdersByTable",
                    @"SELECT * FROM TNHT_MEOR WHERE MESA_PK = :MESA_PK"
                )
                {
                    Parameters = { ["MESA_PK"] = tableId }
                };

                foreach (IRecord record in query.Execute())
                {
                    var product = GetProductRecordForTicketCreation(record.ValueAs<Guid>("ARTG_PK"), standId, langId);
                    var ticket = new OrderRecord
                    {
                        Id = record.ValueAs<Guid>("MEOR_PK"),
                        TableId = record.ValueAs<Guid>("MESA_PK"),
                        ProductId = record.ValueAs<Guid>("ARTG_PK"),
                        ProductDescription = product.Description,
                        ProductPrice = product.StandardPrice ?? 0,
                        Quantity = record.ValueAs<decimal>("MEOR_CANT"),
                        Notes = record.ValueAs<string>("MEOR_NOTA"),
                        Placed = record.ValueAs<bool>("MEOR_PEDI"),
                        Preparations = new BaseQuery(
                                Manager,
                                "GetMenuOrderPreparationsBy",
                                @"SELECT * FROM TNHT_MEPR WHERE MEOR_PK = :MEOR_PK"
                            )
                        {
                            Parameters = { ["MEOR_PK"] = record.ValueAs<Guid>("MEOR_PK") }
                        }
                            .Execute()
                            .Select(prepRecord => prepRecord.ValueAs<Guid>("PREP_PK"))
                            .ToArray()
                    };

                    list.Add(ticket);
                }
            }
            finally
            {
                Manager.Close();
            }

            return list;
        }

        public ValidationResult AddMenuOrder(Guid hotelId, Guid standId, Guid saloonId, Guid tableId, Guid productId, decimal quantity,
            IEnumerable<Guid> preparations)
        {
            var result = new ValidationResult();
            Manager.Open();
            try
            {
                Manager.BeginTransaction();

                var order = new Order(Manager)
                {
                    HotelId = hotelId,
                    PosId = standId,
                    SaloonId = saloonId,
                    TableId = tableId,
                    ProductId = productId,
                    Quantity = quantity
                };
                order.SaveObject();

                if (preparations != null)
                {
                    foreach (var preparationId in preparations)
                    {
                        var preparation = new Preparation(Manager)
                        {
                            OrderId = order.Id.ToString().AsGuid(),
                            PreparationId = preparationId
                        };
                        preparation.SaveObject();
                    }
                }

                Manager.EndTransaction(result);
            }
            catch (Exception e)
            {
                result.AddException(e);
                Manager.RollbackTransaction();
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationResult UpdateMenuOrder(Guid orderId, decimal? quantity, IEnumerable<Guid> preparations)
        {
            var result = new ValidationResult();
            Manager.Open();
            try
            {
                Manager.BeginTransaction();

                var order = new Order(Manager, orderId);
                if (quantity != null)
                {
                    order.Quantity = (decimal)quantity;
                    order.SaveObject();
                }

                if (preparations != null)
                {
                    // Remove previous preparations
                    var query = new BaseQuery(
                        Manager,
                        "RemovePreviousPreparations",
                        @"DELETE FROM TNHT_MEPR WHERE MEOR_PK = :MEOR_PK"
                    )
                    {
                        Parameters =
                        {
                            ["MEOR_PK"] = orderId
                        }
                    };
                    query.Execute();
                    // Add new preparations
                    foreach (var preparationId in preparations)
                    {
                        var preparation = new Preparation(Manager)
                        {
                            OrderId = order.Id.ToString().AsGuid(),
                            PreparationId = preparationId
                        };
                        preparation.SaveObject();
                    }
                }

                Manager.EndTransaction(result);
            }
            catch (Exception e)
            {
                result.AddException(e);
                Manager.RollbackTransaction();
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationResult DeleteMenuOrder(Guid orderId)
        {
            var result = new ValidationResult();
            Manager.Open();
            try
            {
                Manager.BeginTransaction();

                var order = new Order(Manager, orderId);
                order.DeleteObject();

                Manager.EndTransaction(result);
            }
            catch (Exception e)
            {
                result.AddException(e);
                Manager.RollbackTransaction();
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationResult PlaceMenuOrder(Guid hotelId, Guid standId, Guid saloonId, Guid tableId)
        {
            var result = new ValidationResult();

            try
            {
                var query = new BaseQuery(
                    Manager,
                    "PlaceMenuOrder",
                    @"
                        UPDATE TNHT_MEOR
                        SET MEOR_PEDI = 1
                        WHERE HOTE_PK = :HOTE_PK
                          AND IPOS_PK = :IPOS_PK
                          AND SALO_PK = :SALO_PK
                          AND MESA_PK = :MESA_PK
                    "
                )
                {
                    Parameters =
                    {
                        ["HOTE_PK"] = hotelId,
                        ["IPOS_PK"] = standId,
                        ["SALO_PK"] = saloonId,
                        ["MESA_PK"] = tableId
                    }
                };
                query.Execute();

                // Digital menu notification
                BusinessContext.SocketIOClient?.Publish("OrderPlaced", new MenuDataDto
                {
                    HotelId = hotelId,
                    PosId = standId,
                    SaloonId = saloonId,
                    TableId = tableId
                });
            }
            catch (Exception e)
            {
                result.AddException(e);
            }

            return result;
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Method to concatenate the account description from a reservation record
        /// by using its properties.
        /// </summary>
        /// <param name="reservation">The reservation to be used</param>
        /// <returns>An string with the proper format to be used in the ticket headers </returns>
        private static string ConcatenateAccountDescription(ReservationRecord reservation)
        {
            var accountDescription = $"{reservation.ReservationNumber} ({reservation.Room})";
            accountDescription += $"\n{reservation.Guests}";
            if (!string.IsNullOrEmpty(reservation.Company)) accountDescription += $"\n{reservation.Company}";

            return accountDescription;
        }

        #endregion

        public ValidationContractResult NewCloseTicket(CloseTicketRequest request)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    result = LoadTicket(request.TicketId);

                    if (result.HasErrors) return result;
                    var ticket = result.Contract.As<POSTicketContract>();

                    #region Validations

                    if (ticket.ProductLineByTicket.IsEmpty)
                        result.AddError("NotPayEmptyTicket".Translate());

                    #endregion


                    result = ValidateTicketPayments(ticket, request.Payments);


                    if (result.IsEmpty)
                    {
                        // Add digital signature
                        if (request.Signature != null)
                            ticket.DigitalSignature = new Blob(Convert.FromBase64String(request.Signature));

                        var tip = CheckProductTip(ticket);

                        var clientRecord = request.Client != null ? new ClientRecord
                        {
                            Id = request.Client.Id,
                            Name = request.Client.Name,
                            Address = request.Client.Address,
                            EmailAddress = request.Client.EmailAddress,
                            FiscalNumber = request.Client.FiscalNumber,
                            Country = request.Client.Country,
                            Identity = request.Client.Identity,
                            Passport = request.Client.Passport,
                            Residence = request.Client.Residence
                        } : null;

                        result.AddValidations(
                            request.CloseAsInvoice
                                ? CloseTicketAsInvoice(ticket, tip, clientRecord, request.DocTypeSelected, request.SaveClient)
                                : CloseAndPrintTicket(ticket, tip, clientRecord, request.DocTypeSelected, request.SaveClient)
                        );
                    }

                    Manager.EndTransaction(result);

                    return result;
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
					logsFactory.Error(ex);
					result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult ValidateTicketPayments(AddPaymentsRequest request)
        {
            var result = new ValidationContractResult();
            try
            {
                result = LoadTicket(request.TicketId);
                if (result.HasErrors) return result;
                var ticket = result.Contract.As<POSTicketContract>();

                ValidateTicketPayments(ticket, request.Payments);
            }
            catch (Exception ex)
            {
				logsFactory.Error(ex);
				result.AddException(ex);
            }

            return result;
        }

        private ValidationContractResult ValidateTicketPayments(POSTicketContract ticket, PaymentsDto[] payments)
        {
            var result = new ValidationContractResult();

            foreach (var payment in payments)
            {
                if (payment.CashPayment != null)
                    result.Add(AddPaymentToTicket(ticket, payment.CashPayment));

                if (payment.CreditRoomPayment != null)
                    result.Add(AddCreditRoomPayment(ticket, payment.CreditRoomPayment));

                if (payment.RoomPlanPayment != null)
                    result.Add(AddRoomPlanPayment(ticket, payment.RoomPlanPayment.Reservation));

                if (payment.HouseUsePayment != null)
                    result.Add(AddHouseUsePayment(ticket, payment.HouseUsePayment));
            }

            return result;
        }

        private static ValidationContractResult AddPaymentToTicket(POSTicketContract ticket, CashPaymentDto payment)
        {
            var result = new ValidationContractResult();

            var contract = new POSPaymentLineContract()
            {
                Id = Guid.NewGuid(),
                Ticket = (Guid)ticket.Id,
                PaymentDescription = payment.PaymentDescription,
                PaymentAmount = payment.Value,
                PaymentId = payment.PaymentId,
                PaymentReceived = payment.Received,
                ReceivableType = ReceivableType.Payment,
                CurrencyId = ticket.Currency,
                MultCurrency = true,
                ExchangeCurrency = 1
            };

            // Validate if the payment received is greater than ticket value and should be added as a tip
            if (payment.ExcessPaymentAsTip && payment.Received > payment.Value)
            {
                // Update payment
                contract.PaymentAmount = payment.Received;
                // Add tip to ticket
                ticket.TipPercent = null;
                ticket.TipValue = payment.Received - payment.Value;
            }

            if (payment.CreditCardId.HasValue)
            {
                var creditCard = new POSCreditCardContract
                {
                    CreditCardTypeId = payment.CreditCardId.Value,
                    Id = Guid.NewGuid()
                };

                contract.CreditCardContract = creditCard;
                contract.IsCreditCard = true;
                contract.CreditCard = (Guid)creditCard.Id;
            }

            if (ticket.PaymentTotal >= ticket.TicketValue + ticket.TipValue)
            {
                result.AddError("There is not debt to add a new payment");
            }
            else
            {
                ticket.PaymentLineByTicket.Add(contract);
            }

            return result;
        }

        private static ValidationContractResult AddCreditRoomPayment(POSTicketContract ticket, CreditRoomPaymentDto payment)
        {
            var amount = payment.Amount;
            var item = payment.Reservation;

            var result = new ValidationContractResult();
            amount ??= ticket.Total - ticket.PaymentReceived;
            if (amount < 0 && amount + ticket.PaymentTotal > ticket.TicketValue + ticket.TipValue)
            {
                result.AddError("InvalidAmountMoney".Translate());
                return result;
            }

            if (item.IsLock)
            {
                result = new ValidationContractResult();
                result.AddError("InvalidData".Translate());
                return result;
            }

            ticket.Account = item.AccountId;
            ticket.AccountType = CurrentAccountType.Reservation;

            var accountDescription = $"{item.ReservationNumber} ({item.Room})";
            accountDescription += $"\n{item.Guest}";
            if (!string.IsNullOrEmpty(item.Company))
                accountDescription += $"\n{item.Company}";
            ticket.AccountDescription = accountDescription;

            ticket.Room = item.Room;
            ticket.FiscalDocTo = item.Guest;

            var contract = new POSPaymentLineContract
            {
                Id = Guid.NewGuid(),
                Ticket = (Guid)ticket.Id,
                PaymentDescription = CurrentAccountType.Reservation.ToString(),
                PaymentAmount = (decimal)amount,
                PaymentReceived = (decimal)amount,
                AccountId = ticket.Account,
                AccountInstallationId = ticket.AccountInstallationId,
                ReceivableType = ReceivableType.Credit,
                CreditType = CurrentAccountType.Reservation,
                CurrencyId = ticket.Currency,
                MultCurrency = true,
                ExchangeCurrency = 1,
                Observations = CurrentAccountType.Reservation.ToString()
            };

            ticket.PaymentLineByTicket ??= [];
            ticket.PaymentLineByTicket.Add(contract);

            ticket.DocumentType = POSDocumentType.Ticket;

            return result;
        }

        private ValidationContractResult AddRoomPlanPayment(POSTicketContract ticket, ReservationDto item)
        {
            var result = new ValidationContractResult();

            if (ticket.HasMealPlanPayment || ticket.HasHouseUsePayment)
            {
                var msg = "HaveAnotherPaymentResetPayments".Translate();
                var pay = ticket.HasMealPlanPayment ? "MealPlan".Translate() : "Houseuse".Translate();
                result.AddError(string.Format(msg, pay));
            }

            if (result.HasErrors) return result;

            var standBusiness = new StandBusiness(Manager, sessionContext, null, ticketRepository, _validatorFactory);
            var stand = standBusiness.GetStandById(sessionContext.StandId, sessionContext.LanguageId);
            if (stand.PensionRateType.HasValue)
                ticket = SetRateType(ticket, RateType.PensionMode).Contract.As<POSTicketContract>();

            ticket.Account = item.AccountId;
            ticket.AccountType = CurrentAccountType.Reservation;

            var accountDescription = $"{item.ReservationNumber} ({item.Room})";
            accountDescription += $"\n{item.Guest}";
            if (!string.IsNullOrEmpty(item.Company))
                accountDescription += $"\n{item.Company}";
            ticket.AccountDescription = accountDescription;

            var amount = decimal.Zero;
            if (!GeneralSetting.LeaveMealPlanOpened || ticket.Total == decimal.Zero)
                amount = ticket.Total;

            var payment = new POSPaymentLineContract()
            {
                Id = Guid.NewGuid(),
                Ticket = (Guid)ticket.Id,
                PaymentDescription = "MealPlan".Translate(),
                PaymentAmount = amount,
                PaymentReceived = amount,
                AccountId = item.AccountId,
                ReceivableType = ReceivableType.RoomPlan,
                CurrencyId = ticket.Currency,
                MultCurrency = true,
                ExchangeCurrency = 1
            };

            ticket.PaymentLineByTicket ??= [];
            ticket.PaymentLineByTicket.Add(payment);

            ticket.DocumentType = POSDocumentType.Ticket;

            return result;
        }

        private ValidationContractResult AddHouseUsePayment(POSTicketContract ticket, HouseUsePaymentDto item)
        {
            var result = new ValidationContractResult();

            if (ticket.HasMealPlanPayment || ticket.HasHouseUsePayment)
            {
                var msg = "HaveAnotherPaymentResetPayments".Translate();
                var pay = ticket.HasHouseUsePayment ? "Houseuse".Translate() : "MealPlan".Translate();
                result.AddError(string.Format(msg, pay));
            }

            if (result.HasErrors)
                return result;

            var standBusiness = BusinessContext.GetBusiness<IStandBusiness>(Manager);
            var stands = standBusiness.GetStands(sessionContext.CashierId, (int)sessionContext.LanguageId);
            var stand = stands.FirstOrDefault(s => s.Id.ToString().AsGuid() == ticket.Stand);

            var payment = new POSPaymentLineContract
            {
                Id = Guid.NewGuid(),
                Ticket = (Guid)ticket.Id,
                InternalConsumptionId = item.Id,
                PaymentDescription = item.Description,
                PaymentAmount = ticket.Total,
                PaymentReceived = ticket.Total,
                ReceivableType = ReceivableType.HouseUse,
                CurrencyId = ticket.Currency,
                MultCurrency = true,
                ExchangeCurrency = 1
            };

            ticket.InternalUse = item.Id;

            var internalUsePercent = decimal.Zero;
            if (item.Unlimited)
                internalUsePercent = decimal.MinusOne;
            else if (item.LimitPercent.HasValue)
                internalUsePercent = item.LimitPercent.Value;
            else
                internalUsePercent = decimal.Zero;

            if (internalUsePercent < decimal.Zero)
                ticket = SetRateType(ticket, RateType.InternalUse, stand.ConsIntRateType.HasValue ? 0 : stand.ConsIntPercentValue ?? 0)
                      .Contract.As<POSTicketContract>();

            if (internalUsePercent is < decimal.Zero or 100)
                ticket.PaymentLineByTicket.Clear();

            payment.PaymentAmount = ticket.Total.GetDiscountValue(
                internalUsePercent < decimal.Zero
                    ? decimal.Zero
                    : internalUsePercent
            );

            if (item.LimitPercent != 100 && ticket.Total - payment.PaymentAmount < GeneralSetting.AddedValueOnHouseUse)
                payment.PaymentAmount -= GeneralSetting.AddedValueOnHouseUse;

            payment.PaymentReceived = payment.PaymentAmount;
            ticket.PaymentLineByTicket ??= [];
            ticket.PaymentLineByTicket.Add(payment);

            return result;
        }

        private sealed class BuyerInfo : IPrinteableBuyerInfo
        {
            public string FiscalNumber { get; set; }
            public string Name { get; set; }
            public bool IsDefaultCustomer { get; set; }
        }
    }
}
