﻿using System;
using System.Configuration;
using System.Diagnostics;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.Pos.Core;
using NewHotel.Pos.Hub.Core;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Notifications;
using NewHotel.Pos.Hub.Business.Interfaces;
using System.Collections.Generic;
using NewHotel.Pos.Hub.Model.Session;

namespace NewHotel.Pos.Hub.Business.Business.Common
{
    public static class BusinessContext
    {
        #region SignalR

        public static string HubName => ConfigurationManager.AppSettings["SignalRHubName"];
        public static string SignalRServer => ConfigurationManager.AppSettings["SignalRServer"];
        public static SignalRClientWrapper SignalRClient { get; set; }

        #endregion

        #region SocketIO

        public static string SocketIOUri => ConfigurationManager.AppSettings["SocketIOUri"];
        public static SocketIOClientWrapper? SocketIOClient { get; set; }

        #endregion

        #region Room Service Monitor

        public static string RoomServiceMonitorUri => ConfigurationManager.AppSettings["RoomServiceMonitorUri"];
        public static string RoomServiceMonitorUsername => ConfigurationManager.AppSettings["RoomServiceMonitorUsername"];
        public static string RoomServiceMonitorPassword => ConfigurationManager.AppSettings["RoomServiceMonitorPassword"];
        public static string RoomServiceMonitorHotel => ConfigurationManager.AppSettings["RoomServiceMonitorHotel"];

        public static RoomServiceMonitor? ServiceMonitor { get; set; }

        #endregion

        public static Guid InstallationId => (Guid)HubContextCache.Current[HubContextCache.NEWHOTEL_HUB_INSTALLATIONID];
        public static Guid UserId => (Guid)HubContextCache.Current[HubContextCache.NEWHOTEL_HUB_USERID];
        public static string UserName => (string)HubContextCache.Current[HubContextCache.NEWHOTEL_HUB_USERNAME];
        public static string UserPassword => (string)HubContextCache.Current[HubContextCache.NEWHOTEL_HUB_USERPASSWORD];
        public static Guid StandId => (Guid)HubContextCache.Current[HubContextCache.NEWHOTEL_HUB_STANDID];
        public static Guid CashierId => (Guid)HubContextCache.Current[HubContextCache.NEWHOTEL_HUB_CASHIERID];
        public static Guid TaxSchemaId => (Guid)HubContextCache.Current[HubContextCache.NEWHOTEL_HUB_TAXSCHEMA];
        public static DocumentSign SignatureMode => (DocumentSign)HubContextCache.Current[HubContextCache.NEWHOTEL_HUB_SIGNMODE];
        public static DateTime WorkDate => (DateTime)HubContextCache.Current[HubContextCache.NEWHOTEL_HUB_WORKDATE];
        public static string Country => (string)HubContextCache.Current[HubContextCache.NEWHOTEL_HUB_COUNTRY];
        public static long LanguageId => HubContextCache.Current != null ? (long)HubContextCache.Current[HubContextCache.NEWHOTEL_HUB_LANGUAGE] : 1033;
        public static IEnumerable<Validation> Validations => HubContextCache.Current[HubContextCache.NEWHOTEL_HUB_VALIDATIONS] as IEnumerable<Validation>;
        
		public static void InitializeServiceMonitor()
        {
            if (!string.IsNullOrEmpty(RoomServiceMonitorUri) &&
                !string.IsNullOrEmpty(RoomServiceMonitorUsername) &&
                !string.IsNullOrEmpty(RoomServiceMonitorPassword) &&
                !string.IsNullOrEmpty(RoomServiceMonitorHotel))
            {
                Trace.WriteLine($"Monitor: {RoomServiceMonitorUri}, {RoomServiceMonitorUsername}, {RoomServiceMonitorPassword}, {RoomServiceMonitorHotel}");
                ServiceMonitor = new RoomServiceMonitor(RoomServiceMonitorUri, RoomServiceMonitorUsername, RoomServiceMonitorPassword, RoomServiceMonitorHotel);
            }
        }

        private static void SubscribeToEvents()
        {
            if (SocketIOClient != null)
            {
                SocketIOClient.Subscribe((name, data) =>
                {
                    try
                    {
                        Trace.WriteLine($"Notified {name}: {data}");

                        switch (name)
                        {
                            #region Dispatch area status changed

                            case "DispatchAreaStatusChanged":
                                {
                                    var contract = Serializer.Deserialize<NotificationContract<DispatchAreaStatusChanged>>(data);
                                    if (contract != null && contract.DataContext != null)
                                    {
                                        using (var manager = BaseService.GetManager())
                                        {
                                            var business = GetBusiness<ITicketBusiness>(manager);
                                            business.UpdateDispatchAreaStatus(contract.DataContext.TicketId,
                                                contract.DataContext.OrderId, contract.DataContext.OrderStatus,
                                                contract.DataContext.LanguageId, contract.HotelId,
                                                contract.CashierId, contract.StandId);
                                        }
                                    }
                                }
                                break;

                                #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        Trace.WriteLine(ex.Message);
                    }
                });
            }
        }

        public static void InitializeNotifications()
        {
            if (SignalRClient == null && !string.IsNullOrEmpty(HubName) && !string.IsNullOrEmpty(SignalRServer))
            {
                SignalRClient = new SignalRClientWrapper(HubName, SignalRServer);

                try
                {
                    SignalRClient.Initialize();
                    Trace.WriteLine($"SignalR {HubName} initialized on {SignalRServer}");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine($"SignalR {HubName} failed on {SignalRServer}: {ex.Message}");
                }
            }

            if (SocketIOClient == null && !string.IsNullOrEmpty(SocketIOUri))
            {
                SocketIOClient = new SocketIOClientWrapper(SocketIOUri);

                SocketIOClient.Error += (e) =>
                {
                    Trace.WriteLine(e.Message);
                };

                SocketIOClient.StateChanged += (sc) =>
                {
                    Trace.WriteLine($"SocketIO on {SocketIOUri} from {sc.OldState.ToString()} to {sc.NewState.ToString()}");
                };

                try
                {
                    SocketIOClient.Initialize();
                    Trace.WriteLine($"SocketIO initialized on {SocketIOUri}");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine($"SocketIO failed on {SocketIOUri}: {ex.Message}");
                }
            }

            SubscribeToEvents();
        }

        public static void Publish<T>(string eventName, NotificationContract<T> contract)
        {
            SignalRClient?.Publish(eventName, contract);
        }

        public static void Publish<T>(string eventName, T dataContext)
        {
            var session = CwFactory.Resolve<IPosSessionContext>();
            SignalRClient?.Publish(eventName,
                new NotificationContract<T>
                {
                    TokenID = session.SessionId,
                    HotelId = session.InstallationId,
                    CashierId = session.CashierId,
                    StandId = session.StandId,
                    DataContext = dataContext
                });
            //SignalRClient?.Publish(eventName,
            //    new NotificationContract<T>
            //    {
            //        TokenID = HubContextCache.Current.Token,
            //        HotelId = InstallationId,
            //        CashierId = CashierId,
            //        StandId = StandId,
            //        DataContext = dataContext
            //    });
        }

        public static T GetBusiness<T>(IDatabaseManager manager)
        {
            return CwFactory.Resolve<T>(("manager", manager));
        }
    }
}