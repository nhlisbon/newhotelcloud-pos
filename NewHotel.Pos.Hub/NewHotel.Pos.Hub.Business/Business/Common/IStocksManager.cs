﻿using NewHotel.Contracts;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.Business.Common
{
    public interface IStocksManager
    {
        Task<ValidationResult> ConsumeStocks(POSTicketContract ticket);
        Task<ValidationResult> CheckStocks();
        Task<ValidationResult<string>> RefreshStocks();
    }
}