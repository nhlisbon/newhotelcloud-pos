﻿using System;
using System.Linq;
using System.Net.Sockets;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using NewHotel.Pos.Core.Ext;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Business.PersistentObjects.Tickets;
using NewHotel.Pos.Hub.Model;

namespace NewHotel.Pos.Hub.Business.Business.Common;

    public static class TicketBusinessActions
    {
        /// <summary>
        /// Determines whether the specified contract has any transferred orders.
        /// </summary>
        /// <param name="contract">The POSTicketContract instance to check for transferred orders.</param>
        /// <returns>
        /// <c>true</c> if the contract contains any product lines that are marked as 
        /// 'ActiveByTransfer' in the annulment code; otherwise, <c>false</c>.
        /// </returns>
        /// <remarks>
        /// This method utilizes the null-conditional operator to safely check if the 
        /// 'ProductLineByTicket' property is not null and contains any elements with 
        /// the specified annulment code. If 'ProductLineByTicket' is null, the method 
        /// gracefully returns false.
        /// </remarks>
        public static bool HasTransferredOrders(this POSTicketContract contract)
            => contract.ProductLineByTicket?.Any(x => x.AnnulmentCode == ProductLineCancellationStatus.ActiveByTransfer) ?? false;

        /// <summary>
        /// Creates a copy of the specified POSProductLineContract.
        /// </summary>
        /// <param name="productLine">The POSProductLineContract instance to be copied.</param>
        /// <param name="ticketId">The new ticket ID to be assigned to the copied product line.</param>
        /// <param name="afterCopyProductLine">An optional action to perform after the copy operation. It takes two parameters: the new product line and the original product line.</param>
        /// <returns>A new POSProductLineContract instance that is a copy of the original, with updated properties.</returns>
        /// <remarks>
        /// This method clones the provided POSProductLineContract and then updates specific properties such as Id, Ticket, and collections like TableLines, Preparations, and Areas. Each element in these collections is also copied to create a deep copy of the product line. The optional afterCopyProductLine action allows for additional custom processing on the newly created copy using both the new and original product lines.
        /// </remarks>
        public static POSProductLineContract CreateCopy(
            this POSProductLineContract productLine,
            Guid ticketId,
            Action<POSProductLineContract, POSProductLineContract>? afterCopyProductLine = null
        )
        {
            var newProductLine = productLine.Clone<POSProductLineContract>();
            newProductLine.Id = Guid.NewGuid();
            newProductLine.Ticket = ticketId;
            newProductLine.TableLines = new TypedList<TableLineContract>(productLine.TableLines.Select(x => x.CreateCopy((Guid)newProductLine.Id)));
            newProductLine.Preparations = new TypedList<POSProductLinePreparationContract>(productLine.Preparations.Select(x => x.CreateCopy((Guid)newProductLine.Id)));
            newProductLine.Areas = new TypedList<POSProductLineAreaContract>(productLine.Areas.Select(x => x.CreateCopy()));
            afterCopyProductLine?.Invoke(newProductLine, productLine);
            return newProductLine;
        }

        private static POSProductLinePreparationContract CreateCopy(
            this POSProductLinePreparationContract productLinePreparation,
            Guid productLineId)
        {
            var newProductLinePreparation = productLinePreparation.Clone<POSProductLinePreparationContract>();
            newProductLinePreparation.Id = Guid.NewGuid();
            newProductLinePreparation.ProductLineId = productLineId;
            return newProductLinePreparation;
        }

        private static TableLineContract CreateCopy(
            this TableLineContract tableLine,
            Guid productLineId,
            Action<TableLineContract, TableLineContract>? afterCopyTableLine = null)
        {
            var newTableLine = tableLine.Clone<TableLineContract>();
            newTableLine.Id = Guid.NewGuid();
            newTableLine.ProductLineId = productLineId;
            newTableLine.Areas = new TypedList<POSProductLineAreaContract>(tableLine.Areas.Select(x => x.CreateCopy()));
            afterCopyTableLine?.Invoke(newTableLine, tableLine);
            return newTableLine;
        }

        private static POSProductLineAreaContract CreateCopy(this POSProductLineAreaContract lineArea)
        {
            var newLineArea = lineArea.Clone<POSProductLineAreaContract>();
            newLineArea.Id = Guid.NewGuid();
            return newLineArea;
        }

        public static void ProductLineAdjustmentByQuantity(this POSProductLineContract contract, decimal quantity)
        {
            contract.CostValue = (contract.CostValue / contract.ProductQtd * quantity).Round2();
            contract.DiscountValue = (contract.DiscountValue / contract.ProductQtd * quantity).Round2();
            contract.GrossValue = (contract.GrossValue / contract.ProductQtd * quantity).Round2();
            contract.GrossValueInBase = (contract.GrossValueInBase / contract.ProductQtd * quantity).Round2();
            contract.NetValue = (contract.NetValue / contract.ProductQtd * quantity).Round2();
            contract.FirstIvaValue = (contract.FirstIvaValue / contract.ProductQtd * quantity).Round2();
            contract.SecondIvaValue = (contract.SecondIvaValue ?? 0 / contract.ProductQtd * quantity).Round2();
            contract.ThirdIvaValue = (contract.ThirdIvaValue ?? 0 / contract.ProductQtd * quantity).Round2();
            contract.ValueBeforeDiscount = (contract.ValueBeforeDiscount / contract.ProductQtd * quantity).Round2();

            foreach (var tableLine in contract.TableLines)
            {
                tableLine.Quantity = tableLine.Quantity / (contract.ProductQtd != 0 ? contract.ProductQtd : 1) * quantity;
            }

            contract.ProductQtd = quantity;
        }

    /// <summary>
    /// Adjusts the values of a product line based on a splitting factor,
    /// taking into account whether it is the last product line in a split.
    /// </summary>
    /// <param name="contract">The product line to be adjusted</param>
    /// <param name="splitFactor">The factor by which the product line quantity is divided for the split. Must be greater than 1.</param>
    /// <param name="isLast">Indicates whether the product line is the last one in the split. Defaults to false.</param>
        public static void AdjustProductLineValuesForSplit(this POSProductLineContract contract, int splitFactor, bool isLast = false)
        {
            if (splitFactor is 0 or 1) return;

            var newProductQtd = (contract.ProductQtd / splitFactor).RoundTo(2);
            
            if (isLast)
            {
                // Adjust the quantity for last product line in the ticket split
                newProductQtd += contract.ProductQtd - newProductQtd * splitFactor;
                // Adjust the values for last product line in the ticket split
                contract.CostValue -= (contract.CostValue / contract.ProductQtd * newProductQtd).Round2() * (splitFactor - 1);
                contract.DiscountValue -= (contract.DiscountValue / contract.ProductQtd * newProductQtd).Round2() * (splitFactor - 1);
                contract.GrossValue -= (contract.GrossValue / contract.ProductQtd * newProductQtd).Round2() * (splitFactor - 1);
                contract.NetValue -= (contract.NetValue / contract.ProductQtd * newProductQtd).Round2() * (splitFactor - 1);
                contract.FirstIvaValue -= (contract.FirstIvaValue / contract.ProductQtd * newProductQtd).Round2() * (splitFactor - 1);
                contract.SecondIvaValue -= (contract.SecondIvaValue ?? 0 / contract.ProductQtd * newProductQtd).Round2() * (splitFactor - 1);
                contract.ThirdIvaValue -= (contract.ThirdIvaValue ?? 0 / contract.ProductQtd * newProductQtd).Round2() * (splitFactor - 1);
                contract.ValueBeforeDiscount -= (contract.ValueBeforeDiscount / contract.ProductQtd * newProductQtd).Round2() * (splitFactor - 1);
                contract.ProductQtd = newProductQtd;

                foreach (var tableLine in contract.TableLines)
                {
                    tableLine.Quantity -= (tableLine.Quantity / splitFactor).Truncate(5) * (splitFactor - 1);
                }
            }
            else
            {
                ProductLineAdjustmentByQuantity(contract, newProductQtd);
            }
        }

    public static bool AllowPrintInvoice(this POSTicketContract contract, POSGeneralSettingsRecord settings)
        {
            var totalAmount = contract.Total;
            var paymentTotalAmount = contract.PaymentLineByTicket.Sum(x => x.PaymentReceived);

            if (totalAmount > paymentTotalAmount)
                return false;

            if (contract.HasAccountDepositPayment)
                return true;

            if (settings.FiscalType != FiscalPOSType.None)
                return contract is
                {
                    HasCashPayment: true,
                    HasMealPlanPayment: false,
                    HasCreditRoomPayment: false,
                    HasHouseUsePayment: false
                };

            if (contract.IsMealPlanPayment && totalAmount <= paymentTotalAmount)
                return false;

            return settings.SignType switch
            {
                DocumentSign.None => settings.EmitOnlyTickets == false && totalAmount <= paymentTotalAmount,
                DocumentSign.FiscalizationColombiaBtw or DocumentSign.FiscalizationPanamaHKA => !contract
                    .HasCreditRoomPayment,
                DocumentSign.FiscalizationEcuador => contract.IsCashPayment,
                DocumentSign.FiscalizationPortugal or DocumentSign.FiscalizationAngola
                    or DocumentSign.FiscalizationMalta => contract is
                    {
                        HasCreditRoomPayment: false, IsHouseUsePayment: false
                    },
                DocumentSign.FiscalizationBrazilCMFLEX => false,
                DocumentSign.FiscalPrinter or DocumentSign.FiscalizationBrazilNFE or DocumentSign.FiscalizationCroatia
                    or DocumentSign.FiscalizationChile or DocumentSign.FiscalizationPeruDFacture
                    or DocumentSign.FiscalizationBrazilProsyst or DocumentSign.FiscalizationCuracao
                    or DocumentSign.FiscalizationMexicoCfdi
                    or DocumentSign.FiscalizationUruguayExFactura => contract.IsCashPayment ||
                                                                     contract.IsHouseUseAndCashCombinedPayments,
                _ => contract.HasPayments
            };
        }

    public static bool AllowPrintTicket(this POSTicketContract contract, POSGeneralSettingsRecord settings)
    {
        var totalAmount = contract.Total;
        var paymentTotalAmount = contract.PaymentLineByTicket.Sum(x => x.PaymentReceived);

        if (totalAmount > paymentTotalAmount)
            return false;

        if (contract.HasAccountDepositPayment)
            return false;

        if (settings.FiscalType != FiscalPOSType.None)
            return !contract.HasCashPayment;

        return settings.SignType switch
        {
            DocumentSign.None => totalAmount <= paymentTotalAmount,
            DocumentSign.FiscalizationEcuador => contract.HasCreditRoomPayment || contract.HasMealPlanPayment ||
                                                 contract.IsHouseUsePayment ||
                                                 contract.IsHouseUseAndCashCombinedPayments,
            DocumentSign.FiscalizationPortugal or DocumentSign.FiscalizationAngola
                or DocumentSign.FiscalizationMalta => !contract.HasCashPayment,
            DocumentSign.FiscalPrinter or DocumentSign.FiscalizationBrazilNFE or DocumentSign.FiscalizationCroatia
                or DocumentSign.FiscalizationChile or DocumentSign.FiscalizationBrazilCMFLEX
                or DocumentSign.FiscalizationPeruDFacture or DocumentSign.FiscalizationColombiaBtw
                or DocumentSign.FiscalizationBrazilProsyst or DocumentSign.FiscalizationCuracao
                or DocumentSign.FiscalizationMexicoCfdi or DocumentSign.FiscalizationUruguayExFactura
                or DocumentSign.FiscalizationPanamaHKA => contract.IsCashPayment || contract.HasCreditRoomPayment ||
                                                          contract.HasMealPlanPayment || contract.HasHouseUsePayment,
            _ => contract.HasPayments
        };
    }
}