﻿using System;
using System.Net;
using RestSharp;

namespace NewHotel.Pos.Hub.Business.Business.Common
{
    public class RoomServiceResponse
    {
        public string message { get; set; }
        public long serviceNumber { get; set; }
        public string utcNow { get; set; }
    }

    public sealed class RoomServiceMonitor
    {
        #region Constants

        private const string AuthorizationHeader = "Authorization";

        #endregion

        #region Members

        private readonly string _url;
        private readonly RestClient _client;

        private readonly string _userName;
        private readonly string _userPassword;
        private readonly string _hotelId;
        private string _token;

        #endregion

        #region Private Methods

        private class AuthResponse
        {
            public string token { get; set; }
        }

        private void Authenticate(IRestClient client, string resource)
        {
            var auth = new
            {
                Username = _userName,
                Password = _userPassword
            };

            var request = new RestRequest(resource, DataFormat.Json);
            request.AddJsonBody(auth);
            var response = client.Post<AuthResponse>(request);
            if (!response.IsSuccessful)
                throw new ApplicationException($"Error trying to authenticate {resource} ({response.StatusCode})");

            if (response.Data == null || string.IsNullOrEmpty(response.Data.token))
                throw new ApplicationException($"Empty response for {resource}");

            _token = response.Data.token;
        }

        private void AddAuthorization(IRestRequest request)
        {
            request.AddHeader(AuthorizationHeader, $"Bearer {_token}");
        }

        private IRestResponse<T> Post<T>(IRestClient client, string resource, object content)
        {
            if (_token == null)
                Authenticate(_client, "/api/authenticate");

            var request = new RestRequest(resource, DataFormat.Json);
            AddAuthorization(request);
            request.AddJsonBody(content);

            return client.Post<T>(request);
        }

        #endregion

        #region Public Methods

        public RoomServiceResponse Notify(string roomNumber, short applicationId, string details)
        {
            const string resource = "/api/service";

            var content = new
            {
                HotelId = _hotelId,
                RoomNumber = roomNumber,
                ServiceType = "1",
                ServiceDate = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.000Z"),
                ApplicationId = applicationId.ToString(),
                Details = details
            };

            var response = Post<RoomServiceResponse>(_client, resource, content);
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                _token = null;
                response = Post<RoomServiceResponse>(_client, resource, content);
            }

            if (response.IsSuccessful)
                return response.Data;
            throw new ApplicationException($"Error response from {_url}{resource}, {response.ErrorMessage} ({response.StatusCode})\n{response.Content}");
        }

        #endregion

        #region Constructor

        public RoomServiceMonitor(string url, string userName, string userPassword, string hotelId)
        {
            _url = url;
            _userName = userName;
            _userPassword = userPassword;
            _hotelId = hotelId;
            _client = new RestClient(_url)
            {
                ThrowOnAnyError = true
            };
        }

        #endregion
    }
}