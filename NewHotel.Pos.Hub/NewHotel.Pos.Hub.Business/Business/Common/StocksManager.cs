﻿using NewHotel.Contracts;
using NewHotel.Pos.Core;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Core.Logs;
using NewHotel.Pos.IoC;
using System;
using System.Linq;
using System.Threading.Tasks;
using ZXing;

namespace NewHotel.Pos.Hub.Business.Business.Common
{
    public class StocksManager : IStocksManager
    {
        private readonly IIntegrationBusiness _integrationBusiness;
        private readonly IMxmBusiness _mxmBusiness;
		private readonly IHubLogsFactory logsFactory;

        public StocksManager(
            IIntegrationBusiness integrationBusiness,
            IMxmBusiness mxmBusiness,
			IHubLogsFactory logsFactory

		)
        {
            _integrationBusiness = integrationBusiness;
            _mxmBusiness = mxmBusiness;
			this.logsFactory = logsFactory;
		}

        #region Entry Points

        public async Task<ValidationResult> ConsumeStocks(POSTicketContract ticket)
        {
            var result = new ValidationResult<string>();
            try
            {
                var (config, integrationId) = GetConfig();
                if (config.SettingMxm != null)
                {
                    var syncResult = await _mxmBusiness.ConsumeStock(ticket, integrationId, config.SettingMxm);
                    if (syncResult.HasErrors)
                        result.AddError(syncResult.Errors.ToString());
                    else
                        result.Result = syncResult.Result;
                }
            }
            catch (Exception ex)
            {
                var message = $"Error trying to consume the local stock \n {ex}";
                logsFactory.Error(ex);
                result.AddError(message);
            }
            return result;
        }

        public async Task<ValidationResult> CheckStocks()
        {
            var result = new ValidationResult<string>();
            try
            {
                var config = GetConfig().Item1;   
                if (config.SettingMxm != null)
                {
                    var syncResult = await _mxmBusiness.CheckStockDataFromMxmApi(Guid.Empty, Guid.Empty, config.SettingMxm);
                    if (syncResult.HasErrors)
                        result.AddError(syncResult.Errors.ToString());
                    else
                        result.Result = syncResult.Result;
                }
            }
            catch (Exception ex)
            {
                var message = $"Error trying to check the local stock \n {ex}";
				logsFactory.Error(ex);
				result.AddError(message);
            }
            return result;
        }

        public async Task<ValidationResult<string>> RefreshStocks()
        {
            var result = new ValidationResult<string>();
            try
            {
                var (config, integrationId) = GetConfig();
                if (config.SettingMxm != null)
                {
                    var syncResult = await _mxmBusiness.RefreshStock(config.SettingMxm, integrationId);
                    if(syncResult.HasErrors)
                        result.AddError(syncResult.Errors.ToString());
                    else
                        result.Result = syncResult.Result;
                }
            }
            catch(Exception ex)
            {
                var message = $"Error trying to sync the local stock \n {ex}";
                logsFactory.Error(ex);
                result.AddError(message);
            }
            return result;
        }

        #endregion

        #region Helpers

        private (MultiConfig, Guid) GetConfig()
        {
            var integrationResult = _integrationBusiness.GetActiveIntegrationByType(IntegrationType.Stocks);
            if (integrationResult.HasErrors)
            {
                //result.AddError(integrationResult.Errors.ToString());
                //return result;
            }
            var integrationId = integrationResult.Result.Id;
            var config = _integrationBusiness.DeserializeConfig(integrationResult.Result).Result;
            return (config, integrationId);
        }

        #endregion
    }
}
