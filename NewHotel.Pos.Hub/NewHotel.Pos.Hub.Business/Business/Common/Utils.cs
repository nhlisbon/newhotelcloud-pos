﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;

namespace NewHotel.Pos.Hub.Business.Business.Common;

public static class Utils
{
    private static string _imageRootPath;
    private static bool _pathLoaded;

    public static readonly Dictionary<string, string> ImagePaths = new();

    public static string BuildImagePath(string path)
    {
        EnsureImageRootPathLoaded();
        return (_imageRootPath == null) ? path : Path.Combine(_imageRootPath, path);
    }

    private static void EnsureImageRootPathLoaded()
    {
        lock (typeof(Utils))
        {
            if (_pathLoaded) return;
            try
            {
                var rootPath = ConfigurationManager.AppSettings.Get("ImageRootPath");
                if (!string.IsNullOrEmpty(rootPath))
                    rootPath = rootPath.Trim();

                if (string.IsNullOrEmpty(rootPath) || !Directory.Exists(rootPath))
                    _imageRootPath = Path.Combine(Environment.CurrentDirectory, "Images");
                else
                    _imageRootPath = rootPath;

                if (!Directory.Exists(_imageRootPath))
                {
                    try
                    {
                        Directory.CreateDirectory(_imageRootPath);
                        Trace.TraceInformation("Looking for images in \"{0}\"", _imageRootPath);
                    }
                    catch
                    {
                        Trace.TraceInformation("Invalid images path \"{0}\"", _imageRootPath);
                    }
                }
                else
                    Trace.TraceInformation("Looking for images in \"{0}\"", _imageRootPath);
            }
            finally
            {
                _pathLoaded = true;
            }
        }
    }

    public static string Base64Image(string path)
    {
        try
        {
            EnsureImageRootPathLoaded();
            var fullPath = Path.Combine(_imageRootPath!, path);
            if (File.Exists(fullPath))
            {
                var imageArray = File.ReadAllBytes(fullPath);
                return Convert.ToBase64String(imageArray);
            }
            Trace.TraceInformation("Image file not found: {0}", fullPath);
        }
        catch (Exception ex)
        {
            Trace.TraceInformation("Error looking for images: {0}", ex.Message);
        }

        return string.Empty;
    }

    public static byte[]? LoadImage(string path)
    {
        try
        {
            EnsureImageRootPathLoaded();
            var fullPath = Path.Combine(_imageRootPath!, path);
            if (File.Exists(fullPath))
                return File.ReadAllBytes(fullPath);
            Trace.TraceInformation("Image file not found: {0}", fullPath);
        }
        catch (Exception ex)
        {
            Trace.TraceInformation("Error looking for images: {0}", ex.Message);
        }

        return null;
    }
}