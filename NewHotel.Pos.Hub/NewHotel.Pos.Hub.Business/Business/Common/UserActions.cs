﻿using System;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.Pos.Hub.Model;

namespace NewHotel.Pos.Hub.Business.Business.Common;

public static class UserActions
{
	public static Model.UserRecord GetUserRecord(this ListData records)
	{
		var user = new Model.UserRecord
		{
			Id = records.ValueAs<Guid>("UTIL_PK"),
			Description = records.ValueAs<string>("UTIL_DESC"),
			UserLogin = records.ValueAs<string>("UTIL_LOGIN"),
			UserPassword = records.ValueAs<string>("UTIL_PASS"),
			UserCode = records.ValueAs<string>("UTIL_CODE"),
			UserInternal = records.ValueAs<bool>("UTIL_INTE")
		};

		return user;
	}

	public static ValidationItemResult<Model.UserRecord> GetUserRecord(this ValidationItemResult<Model.UserRecord> result, Query query, Func<Model.UserRecord, ValidationResult> afterLoad = default)
	{
		var records = query.Execute();
		if (!records.IsEmpty)
		{
			var user = records.GetUserRecord();

			result.Item = user;
			if (afterLoad != null)
			{
				result.Add(afterLoad(user));
			}
		}

		return result;
	}
}
