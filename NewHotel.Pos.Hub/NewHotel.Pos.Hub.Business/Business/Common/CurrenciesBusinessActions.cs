﻿using System;
using System.Collections.Generic;
using NewHotel.Pos.Hub.Contracts.Cashier.Records;
using NewHotel.Core;
using NewHotel.Pos.Hub.Business.SqlContainer.Common;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Admin;

namespace NewHotel.Pos.Hub.Business.Business.Common;

public static class CurrenciesBusinessActions
{
    public static IList<CurrencyCashierRecord> GetCurrencies(this IDatabaseManager manager, Guid hotelId)
	{
		manager.Open();
		try
		{
			var query = QueryFactory.Get<CurrencyCashierQuery>(manager);
			query.Parameters[CurrencyCashierQuery.HoteIdParam] = hotelId;

			var result = query.ExecuteList<CurrencyCashierRecord>();

			foreach (var currency in result)
			{
				currency.IsBase = currency.IsHotelBase;
			}

			return result;
		}
		finally
		{
			manager.Close();
		}
	}
}
