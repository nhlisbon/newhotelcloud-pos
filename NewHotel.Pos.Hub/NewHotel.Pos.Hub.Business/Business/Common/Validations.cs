﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.Business
{
	public static class ValidationSupport
	{
		public static T ValidateStaleData<T>(this T result, DateTime contract, DateTime persistent, string errorMessage)
			where T : ValidationResult
		{
			if ((int)Math.Truncate((contract - persistent).TotalMilliseconds * 1000) != 0)
			{
				result.AddError(errorMessage);
			}
			return result;
		}

		public static T ValidateTicketStaleData<T>(this T result, NewHotel.Pos.Hub.Business.PersistentObjects.Tickets.Ticket ticket, DateTime contractModified)
			where T : ValidationResult
			=> result.ValidateStaleData(contractModified, ticket.LastModified, "Ticket modified by another cashier");
	}
}
