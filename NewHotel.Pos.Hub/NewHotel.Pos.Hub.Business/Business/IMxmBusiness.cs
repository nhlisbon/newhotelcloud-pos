﻿using NewHotel.Contracts;
using System;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.Business
{
    public interface IMxmBusiness
    {
        /// <summary>
        /// This method is used to consume the stock from the Mxm integration API. This happens when a product is sold.
        /// This is the method to be called when trying to do the action consume stock.
        /// </summary>
        /// <param name="ticket">The ticket contract which contains the products the method wants to affect</param>
        /// <param name="integrationId">The Guid of the integration to help the search of the product in the reference table TNHT_ARIN</param>
        /// <param name="config">The config of the integration</param>
        /// <returns>A success message or an error message if something went wrong</returns>
        Task<ValidationResult<string>> ConsumeStock(POSTicketContract ticket, Guid integrationId, MxmConfig config);

        /// <summary>
        /// This happens by either a manual action or by a scheduled task. This method is used to refresh the stock from the Mxm integration API.
        /// So it updates the local stock with the data from the integration. 
        /// Unfortunately, the integration does not have a specific endpoint for this, so we have to do it one by one, which means that this method is not optimized and may take a long time to complete.
        /// Since it makes a request for each product in the integration, in case one fails it does not rollback the whole process and it will skip the one that failed and will keep going until the end.
        /// Use with due CAUTION.
        /// </summary>
        /// <param name="config">The MxmConfig to be used</param>
        /// <returns>If nothing went wrong it will return a success message otherwise, it will return an error message</returns>
        Task<ValidationResult<string>> RefreshStock(MxmConfig config, Guid integrationId);

        /// <summary>
        /// This method is used to check the the stock consume process from the Mxm integration API. This happens when a product is sold.
        /// And due to they're system, we need to check if the stock was consumed correctly or not.
        /// </summary>
        /// <param name="productId">The product id from the table TNHT_ARTG</param>
        /// <returns>A sucess message or a error message if anything went wrong</returns>
        Task<ValidationResult<string>> CheckStockDataFromMxmApi(Guid productId, Guid integrationId, MxmConfig config);
    }
}
