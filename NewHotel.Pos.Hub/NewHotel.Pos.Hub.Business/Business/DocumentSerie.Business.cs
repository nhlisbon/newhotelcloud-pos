﻿using System;
using NewHotel.Business;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.Pos.Core.Ext;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Business.PersistentObjects;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries;
using NewHotel.Pos.Hub.Business.Validations;
using NewHotel.Pos.Hub.Model.Session;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class DocumentSerieBusiness : BaseBusiness, IDocumentSerieBusiness
    {
		private readonly IPosSessionContext _sessionContext;
		private readonly IValidatorFactory _validatorFactory;

        #region Constructor

        public DocumentSerieBusiness(
            IDatabaseManager manager,
            IPosSessionContext sessionContext,
            IValidatorFactory validatorFactory)
            : base(manager) 
            {
			this._sessionContext = sessionContext;
			_validatorFactory = validatorFactory;
            }


        #endregion
        #region IDocumentSerieBusiness

        #region Get Next Number

        public SerialNumberResult GetNextSerialNumber(Guid cajaId, Guid standId, bool justSerie, long docType, bool searchInParent, bool isAltenativeSerie = false)
        {
            var result = GetCurrentActiveSerie(cajaId, standId, justSerie, docType, searchInParent, false, isAltenativeSerie);
            if (!result.DocumentNumber.HasValue || string.IsNullOrEmpty(result.DocumentSerie))
                result.AddError("Cannot get any serie for '{0}'", Enum.GetName(typeof(POSDocumentType), docType));

            return result;
        }

        public SerialNumberResult GetNextAuxiliarCount(Guid cajaId, Guid standId, bool justSerie, long docType, bool searchInParent, bool isAltenativeSerie = false)
        {
            var result = GetCurrentActiveSerie(cajaId, standId, justSerie, docType, searchInParent, true, isAltenativeSerie);
            if (!result.DocumentNumber.HasValue || string.IsNullOrEmpty(result.DocumentSerie))
                result.AddError("Cannot get any serie for '{0}'", Enum.GetName(typeof(POSDocumentType), docType));

            return result;
        }

        private SerialNumberResult GetCurrentActiveSerie(Guid cajaId, Guid standId, bool justSerie,
            long type, bool searchInParent, bool auxiliarCountInstead, bool? isAltenativeSerie)
        {
            var result = new SerialNumberResult();

            var query = CommonQueryFactory.GetDocumentSeriesByStandCashier(Manager);
            query.Parameters["ipos_pk"] = standId;
            query.Parameters["caja_pk"] = cajaId;
            query.Filters["tido_pk"].Value = new object[] { type };

            if (isAltenativeSerie.HasValue)
                query.Filters["sedo_sctg"].Value = isAltenativeSerie.Value;

            // existen series del mismo tipo documento
            // en caso de multiples series activas o futuras
            // se toma la primera ordenado por activas primero
            // y prefijo numerico descendiente
            var records = query.Execute();
            if (records.Count > 0)
            {
                var standBusiness = BusinessContext.GetBusiness<IStandBusiness>(Manager); //new StandBusiness(Manager, _sessionContext, null, _validatorFactory);
                var workDate = standBusiness.GetWorkDateByStand(standId);
                DocumentSerie activeDocSerie = null;

                if (records.ValueAs<SerieStatus>("sedo_seac") == SerieStatus.Active)
                {
                    // tratar de retornar el próximo número de la serie activa
                    // en caso de ser retornado, actualizar el próximo número en la serie 
                    activeDocSerie = new DocumentSerie(Manager, records.ValueAs<Guid>("sedo_pk"), true);

                    var docNumber = auxiliarCountInstead
                        ? activeDocSerie.NextDocAuxiliarCode(!justSerie, workDate)
                        : activeDocSerie.NextDocNumber(!justSerie, workDate);

                    // salvar la serie para el caso en que queda inactiva
                    // o incrementó el consecutivo
                    activeDocSerie.SaveObject();

                    // retornar si se pudo obtener un consecutivo
                    if (docNumber.HasValue)
                    {
                        result.SerieId = (Guid)activeDocSerie.Id;
                        result.DocumentSerie = activeDocSerie.SerieName;
                        result.DocumentNumber = docNumber.Value;

                        return result;
                    }

                    if (records.Count > 1)
                        records.Goto(1);
                }

                // estoy aqui porque itemSerieControl no esta activa ó
                // o estaba activa, no me dio un próximo número pero tenia una futura
                // preparada en la base de datos
                if (records.ValueAs<SerieStatus>("sedo_seac") == SerieStatus.Future)
                {
                    var futureDocSerie = new DocumentSerie(Manager, records.ValueAs<Guid>("sedo_pk"), true);
                    futureDocSerie.SerieStatus = SerieStatus.Active;

                    var docNumber = auxiliarCountInstead
                        ? futureDocSerie.NextDocAuxiliarCode(!justSerie, workDate)
                        : futureDocSerie.NextDocNumber(!justSerie, workDate);

                    if (docNumber.HasValue)
                    {
                        futureDocSerie.SaveObject();
                        result.SerieId = (Guid)futureDocSerie.Id;
                        result.DocumentSerie = futureDocSerie.SerieName;
                        result.DocumentNumber = docNumber.Value;

                        return result;
                    }
                }

                // estoy en este punto porque la serie que encontré
                // no fue capaz de generar un nuevo numero de serie
                if (string.IsNullOrEmpty(result.DocumentSerie))
                {
                    // comprobar si puedo autogenear
                    if (activeDocSerie != null && activeDocSerie.CanAutocreate && string.IsNullOrEmpty(activeDocSerie.ValidationCode))
                    {
                        // clonar la serie documento, pasar la nueva a estado: activa 
                        // y poner la anterior a estado: inactiva
                        var clonedDocSerie = activeDocSerie.Clone(workDate);
                        clonedDocSerie.SerieStatus = SerieStatus.Active;

                        var docNumber = auxiliarCountInstead
                            ? clonedDocSerie.NextDocAuxiliarCode(!justSerie, workDate)
                            : clonedDocSerie.NextDocNumber(!justSerie, workDate);

                        // poner la serie como inactiva
                        activeDocSerie.CloseSerie();

                        // guardar la información
                        activeDocSerie.SaveObject();
                        clonedDocSerie.SaveObject();

                        // información de retorno
                        result.SerieId = (Guid)clonedDocSerie.Id;
                        result.DocumentNumber = docNumber.Value;
                        result.DocumentSerie = clonedDocSerie.SerieName;

                        return result;
                    }
                }

                // estoy aqui porque no encontré manejadores de series activos para el tipo de documento
                // ó si encontré pero estos no pudieron darme un nº serie válido           
                if (searchInParent)
                {
                    // primero ver si el tipo de documento tiene padre...
                    // true: tratar de pedirle el nº documento a la serie de documentos del padre
                    // false: tratar de crear nuevas series en caso de que se puedan auto-generar
                    var querySearchInParent = CommonQueryFactory.GetDocumentParent(Manager);
                    querySearchInParent.Parameters["tido_pk"] = (long)type;
                    var list = querySearchInParent.Execute();

                    // aqui viene 1 sola linea
                    if (list.Count > 0 && !list.IsNull("tido_pare"))
                    {
                        var parentDocType = (long)list.ValueAs<POSDocumentType>("tido_pare");
                        result = GetCurrentActiveSerie(cajaId, standId, justSerie, parentDocType,
                            searchInParent, auxiliarCountInstead, isAltenativeSerie);
                    }
                }
            }

            return result;
        }

        public ValidationResult<string> GetSerieValidationCode(string serie)
        {
            var query = CommonQueryFactory.GetSerieValidationCode(Manager);
            query.Parameters["serie"] = serie;

            var result = new ValidationResult<string>();
            result.Result = query.ExecuteScalar<string>();
            return result;
        }

        #endregion

        #endregion
    }
}