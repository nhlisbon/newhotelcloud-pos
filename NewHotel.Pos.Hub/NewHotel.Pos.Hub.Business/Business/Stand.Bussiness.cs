using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using NewHotel.Business;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using NewHotel.Core;
using NewHotel.DataAnnotations;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Business.PersistentObjects;
using NewHotel.Pos.Hub.Business.PersistentObjects.Tickets;
using NewHotel.Pos.Hub.Business.SqlContainer.Common;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Admin;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Closing;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Management;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Reports;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Stands;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets;
using NewHotel.Pos.Hub.Business.Validations;
using NewHotel.Pos.Hub.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.Records;
using NewHotel.Pos.Hub.Contracts.Common.Records.CurrentAccount;
using NewHotel.Pos.Hub.Contracts.Common.Records.Stands;
using NewHotel.Pos.Hub.Contracts.Common.Records.Tickets;
using NewHotel.Pos.Hub.Contracts.Handheld.DTOs;
using NewHotel.Pos.Hub.Core;
using NewHotel.Pos.Hub.Core.Logs;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Hub.Model.Session;
using NewHotel.Pos.Hub.Model.WaitingListStand.Contract;
using NewHotel.Pos.Hub.Model.WaitingListStand.Record;
using NewHotel.Pos.Localization;
using ZXing;
using ClosedTicketRecord = NewHotel.Pos.Hub.Model.ClosedTicketRecord;
using CurrencyExchangeRecord = NewHotel.Pos.Hub.Contracts.Cashier.Records.CurrencyExchangeRecord;
using SaloonRecord = NewHotel.Pos.Hub.Model.SaloonRecord;
using UserRecord = NewHotel.Pos.Hub.Model.UserRecord;

namespace NewHotel.Pos.Hub.Business.Business
{
	public class StandBusiness : BaseBusiness, IStandBusiness, IPaxWaitingListBusiness
    {
		readonly IHubLogsFactory logsFactory;
		private readonly ITicketRepository _ticketRepository;
		private readonly IPosSessionContext _sessionContext;
        private readonly IValidatorFactory _validatorFactory;

        #region Constructors

        public StandBusiness(
            IDatabaseManager manager,
            IPosSessionContext sessionContext,
            IHubLogsFactory logsFactory,
            ITicketRepository ticketRepository,
            IValidatorFactory validatorFactory)
            : base(manager)
        {
			_sessionContext = sessionContext;
			this.logsFactory = logsFactory;
			_ticketRepository = ticketRepository;
			_validatorFactory = validatorFactory;
        }

        #endregion

        #region IStandBusiness

        public string GetStandardCurrencyByStand(Guid standId)
        {
            var result = string.Empty;

            Manager.Open();
            try
            {
                using (var command = Manager.CreateCommand("Context.GetStandardCurrencyByStand"))
                {
                    command.CommandText = "select tprg.unmo_pk from tnht_tprg tprg, tnht_ipos ipos where tprg.tprg_pk = ipos.tprg_stdr and ipos.ipos_pk = :ipos_pk";
                    Manager.CreateParameter(command, "ipos_pk", standId);

                    var reader = Manager.ExecuteReader(command);
                    if (reader.Read())
                        result = reader.GetString(0);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationItemResult<StandEnvironment> GetStandEnvironment(Guid standId, int language, bool base64Image = false)
        {
            using var log = logsFactory.NewBusinessLog(nameof(GetStandEnvironment));
            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var result = new ValidationItemResult<StandEnvironment>();

                    var sb = new StringBuilder();
                    sb.AppendLine("merge into tnht_vers tbl using (select :vers_numb vers_numb, :vers_date vers_date from dual) t2 on (tbl.vers_numb = t2.vers_numb) ");
                    sb.AppendLine("when not matched then insert values (:vers_numb, :vers_date)");

                    // Version Control Included
                    using (var command = Manager.CreateCommand("VersionControl"))
                    {
                        command.CommandText = sb.ToString();
                        Manager.CreateParameter(command, "vers_numb", Manager.ConvertValueType(Assembly.GetEntryAssembly().GetName().Version.ToString()));
                        Manager.CreateParameter(command, "vers_date", Manager.ConvertValueType(DateTime.Today));
                        Manager.ExecuteNonQuery(command);
                    }

                    var lcid = language;
                    var hotelId = _sessionContext.InstallationId;

                    var settingBusiness = BusinessContext.GetBusiness<ISettingBusiness>(Manager);
                    var ticketBusiness = BusinessContext.GetBusiness<ITicketBusiness>(Manager);
                    var productBusiness = BusinessContext.GetBusiness<IProductBusiness>(Manager);

                    var settings = settingBusiness.GetGeneralSettings(_sessionContext.InstallationId);

                    var environment = new StandEnvironment
                    {
                        Hotel = GetHotel(hotelId),
                        IsHtml5 = CloudBusiness.IsCloudApi,
                        CreditCardTypes = settingBusiness.GetCreditCardTypes(hotelId, lcid),
                        CancellationReasons = settingBusiness.GetCancellationTypes(hotelId, lcid),
                        Separators = settingBusiness.GetSeparators(hotelId, lcid),
                        PaymentMethods = settingBusiness.GetPaymentMethods(hotelId, lcid),
                        DiscountTypes = settingBusiness.GetDiscountTypes(hotelId, lcid),
                        TicketDuties = settingBusiness.GetTicketDuty(hotelId, lcid),
                        TaxSchemas = settingBusiness.GetTaxSchemas(hotelId, lcid).ToList(),
                        HouseUses = settingBusiness.GetHouseUses(standId, lcid),
                        HappyHours = settingBusiness.GetHappyHoursByStand(standId, lcid),
                        Nationalities = settingBusiness.GetNationalities(lcid),
                        AppVersion = Assembly.GetEntryAssembly()?.GetName().Version.ToString() ?? string.Empty
                    };

                    // if (environment.IsHtml5)
                    // {
                    //     var cloudBusiness = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
                    //     var updateVersionsResult = UpdateVersions(cloudBusiness);
                    //     if (!updateVersionsResult.IsEmpty)
                    //     {
                    //         Trace.TraceError($"Error updating versions: {updateVersionsResult.ToString()}");
                    //         var vres = cloudBusiness.TraceComunicationErrors(updateVersionsResult);
                    //         if (!vres.IsEmpty)
                    //             Trace.TraceError(vres.ToString());
                    //     }
                    // }

                    foreach (var category in settingBusiness.GetButtonCategories(hotelId, lcid))
                    {
                        var categoryOption = new CategoryOption
                        {
                            Category = category,
                            SubCategories = settingBusiness.GetButtonSubCategories(category.Id, lcid)
                                .Select(s => new SubCategoryOption
                                {
                                    SubCategory = s,
                                    ProductColumns = settingBusiness.GetColumnsPlusProducts(standId, s.Id, lcid)
                                })
                                .ToList()
                        };

                        environment.Categories.Add(categoryOption);
                    }

                    var stand = GetStandById(standId, lcid);
                    var saloons = GetSaloonsByStand(standId, lcid);
                    // var workDate = GetWorkDateByStand(standId);

                    // List<LiteTablesReservationRecord> reservations = null;
                    // var saloonReservationsValidation = GetTableReservations(cloudBusiness, standId, workDate);
                    // if (saloonReservationsValidation.IsEmpty)
                    //     reservations = saloonReservationsValidation.ItemSource;
                    // else
                    // {
                    //     Trace.TraceError($"Error reading reservations: {saloonReservationsValidation.ToString()}");
                    //     var vres = cloudBusiness.TraceComunicationErrors(saloonReservationsValidation);
                    //     if (!vres.IsEmpty)
                    //         Trace.TraceError(vres.ToString());
                    // }

                    if (result.IsEmpty)
                    {
                        foreach (var saloon in saloons)
                        {
                            var saloonEnvironment = new SaloonEnvironment
                            {
                                Saloon = saloon
                            };

                            #region Load tables environment

                            var tables = GetTablesBySaloon(saloon.Id);
                            foreach (var table in tables)
                            {
                                var tableEnvironment = new TableEnvironment
                                {
                                    Table = table
                                };

                                #region Reservations

                                // if (reservations != null)
                                // {
                                //     foreach (var reservation in reservations.Where(r => r.Saloon.HasValue && r.Saloon.Value == saloon.Id))
                                //     {
                                //         if (reservation.TablesDescription.Contains(table.Description))
                                //             tableEnvironment.Reservations.Add(reservation);
                                //     }
                                // }

                                #endregion

                                saloonEnvironment.Tables.Add(tableEnvironment);
                            }

                            #endregion

                            environment.Saloons.Add(saloonEnvironment);
                        }

                        #region Print Settings

                        if (stand.TicketPrintConfiguration.HasValue)
                            environment.TicketPrintSettings = ticketBusiness.GetTicketPrintConfiguration(stand.TicketPrintConfiguration.Value, lcid);
                        if (stand.BallotPrintConfiguration.HasValue)
                            environment.BallotPrintSettings = ticketBusiness.GetTicketPrintConfiguration(stand.BallotPrintConfiguration.Value, lcid);
                        if (stand.InvoicePrintConfiguration.HasValue)
                            environment.InvoicePrintSettings = ticketBusiness.GetTicketPrintConfiguration(stand.InvoicePrintConfiguration.Value, lcid);
                        if (stand.ReceiptPrintConfiguration.HasValue)
                            environment.ReceiptPrintSettings = ticketBusiness.GetTicketPrintConfiguration(stand.ReceiptPrintConfiguration.Value, lcid);

                        #endregion

                        environment.Areas = productBusiness.GetAreasByStand(standId, lcid);
                        environment.ProductGroups = productBusiness.GetProductGroups(hotelId, standId, lcid);
                        environment.ProductFamilies = productBusiness.GetProductFamilies(hotelId, standId, lcid);
                        environment.ProductSubFamilies = productBusiness.GetProductSubFamilies(hotelId, standId, lcid);
                        environment.Preparations = productBusiness.GetPreparations(lcid);

                        #region Currencies

                        // Moneda del punto de venta
                        var standCurrencyId = GetStandardCurrencyByStand(standId);
                        environment.Currency = standCurrencyId;

                        // Definiciones de monedas usables en caja
                        var currencies = Manager.GetCurrencies(_sessionContext.InstallationId); //GetCurrencies();
                        environment.Currencys = currencies;

                        // Conversiones de monedas. Deberian ser relativas a la moneda del stand
                        var currencyExchanges = GetCurrencyExchanges();
                        environment.CurrencyExchanges = currencyExchanges;

                        // Tratar de poner la moneda base como la que tiene el stand configurado. 
                        // Si no se puede prevalece la base del hotel
                        foreach (var currency in currencies)
                        {
                            if (currency.CurrencyId == standCurrencyId)
                            {
                                currency.IsBase = true;
                                foreach (var item in currencies.Where(x => x != currency))
                                    item.IsBase = false;
                                break;
                            }
                        }

                        #endregion

                        if (!currencies.Any(c => c.IsBase))
                            result.AddError($"Undefined base currency for {stand.Description}");
                        else
                        {
                            if (stand.StandardRateType.HasValue)
                            {
                                if (settings.DefaultTaxSchemaId.HasValue)
                                {
                                    environment.Products = productBusiness.GetProductsByStandRate(hotelId, standId, lcid, base64Image);
                                }
                                else
                                    result.AddError("Undefined default tax schema");
                            }
                            else
                                result.AddError($"Standard rate not defined for {stand.Description}");
                        }

                        result.Item = environment;
                    }

                    Manager.EndTransaction(result);
                    return result;
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    return new ValidationItemResult<StandEnvironment>(ex);
                }
            }
            finally
            {
                Manager.Close();
            }
        }

        public ValidationStringResult LoadImage(string imagePath)
        {
            try
            {
                var result = new ValidationStringResult
                {
                    Description = Utils.Base64Image(imagePath)
                };

                return result;
            }
            catch (Exception ex)
            {
                return new ValidationStringResult(ex);
            }
        }

        public ValidationItemResult<UserRecord> GetUserByName(string loginUser)
        {
            var result = new ValidationItemResult<UserRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetUsers(Manager);
                query.Filters["util_login"].Value = (loginUser ?? string.Empty).ToUpper();

                result = result.GetUserRecord(query, FillUserPermissions);
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationItemResult<UserRecord> GetUserByCode(string userCode)
        {
            var result = new ValidationItemResult<UserRecord>();

            if (string.IsNullOrEmpty(userCode))
                result.AddError("Code can not be empty");

            if (!result.IsEmpty) return result;

            Manager.Open();

			try
            {
                var query = QueryFactory.Get<UsersQuery>(Manager);
                var filter = query.Filters[UsersQuery.UserCodeFilter];
                filter.Enabled = true;
                filter.Value = userCode;

                result = result.GetUserRecord(query, FillUserPermissions);
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationItemResult<UserRecord> GetUserById(Guid userId)
        {
            var result = new ValidationItemResult<UserRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetUsers(Manager);
                query.Filters["util_pk"].Value = userId;

				result = result.GetUserRecord(query);
			}
            catch (Exception e)
            {
                result.AddException(e);
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationBooleanResult CheckUserPermission(Guid userId, Permissions permission)
        {
            var result = new ValidationBooleanResult();

            Manager.Open();
            try
            {
                var sb = new StringBuilder();
                sb.AppendLine("select peap.perm_pk, peut.secu_code");
                sb.AppendLine("from tnht_peut peut inner join tnht_peap peap on peap.peap_pk = peut.peap_pk");
                sb.AppendLine("where peut.util_pk = :util_pk and peap.appl_pk = :appl_pk");

                var query = new BaseQuery(Manager, "UserPermissions", sb.ToString());
                query.Parameters["util_pk"] = userId;
                query.Parameters["appl_pk"] = CloudBusiness.ApplicationId;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var currentPermission = NewHotelPermissions.GetPermissionsEnum(record.ValueAs<Guid>("perm_pk"));
                    if (currentPermission.HasValue && currentPermission.Value == permission)
                    {
                        var securityCode = record.ValueAs<SecurityCode>("secu_code");
                        result.Value = securityCode == SecurityCode.FullAccess || securityCode == SecurityCode.ReadWrite;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Value = false;
                result.AddException(ex);
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public List<CashierRecord> GetCashiers()
        {
            //_sessionContext.InstallationId

            return GetCashiers(_sessionContext.InstallationId, _sessionContext.IsInternal ? Guid.Empty : _sessionContext.UserId, (int)_sessionContext.LanguageId);
        }


		public List<CashierRecord> GetCashiers(Guid hotelId, Guid userId, int language)
        {
			var results = new List<CashierRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetCashierByUsers(Manager);
                query.Parameters["hote_pk"] = hotelId;
                query.Parameters["lang_pk"] = language;
                if (userId != Guid.Empty)
                    query.Filters["util_pk"].Value = userId;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var cajaRecord = new CashierRecord();

                    cajaRecord.Id = record.ValueAs<Guid>("CAJA_PK");
                    cajaRecord.Description = record.ValueAs<string>("CAJA_DESC");
                    cajaRecord.TicketsByCashiers = record.ValueAs<bool>("CAJA_VECA");
                    cajaRecord.ReportsByCashiers = record.ValueAs<bool>("IPOS_EMPC");

                    results.Add(cajaRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        public List<POSStandRecord> GetStands(Guid cajaId, int language)
        {
            language = language == 0 ? (int)_sessionContext.LanguageId : language;

            var results = new List<POSStandRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetStandsByCashiers(Manager);
                query.Parameters["caja_pk"] = cajaId;
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var standRecord = new POSStandRecord
                    {
                        Id = record.ValueAs<Guid>("IPOS_PK"),
                        Abbreviation = record.ValueAs<string>("IPOS_ABRE"),
                        Description = record.ValueAs<string>("IPOS_DESC"),
                        StandWorkDate = record.ValueAs<DateTime>("IPOS_FCTR").Date,
                        StandWorkShift = record.ValueAs<long>("IPOS_TUTR"),
                        ControlAccount = record.ValueAs<Guid?>("CTRL_PK"),
                        StandardRateType = record.ValueAs<Guid?>("TPRG_STDR"),
                        ConsIntRateType = record.ValueAs<Guid?>("TPRG_CINT"),
                        ConsIntPercentValue = record.ValueAs<decimal?>("TPRG_CIPO"),
                        PensionRateType = record.ValueAs<Guid?>("TPRG_PENS"),
                        PensionRatePercentValue = record.ValueAs<decimal?>("TPRG_PEPO"),
                        TicketPrintConfiguration = record.ValueAs<Guid?>("CNFG_TICK"),
                        InvoicePrintConfiguration = record.ValueAs<Guid?>("CNFG_FACT"),
                        ReceiptPrintConfiguration = record.ValueAs<Guid?>("CNFG_COMP"),
                        NightAuditorReportCopies = record.ValueAs<short>("IPOS_CCOP"),
                        AllowCloseDayWithOpenTickets = record.ValueAs<bool>("IPOS_CKAB"),
                        CopyKitchenInTicketPrinter = record.ValueAs<bool>("IPOS_KITI"),
                        KeepTicketsInTableOnManualSplit = record.ValueAs<bool>("IPOS_SPTA"),
                        AllowPrintTableBalance = record.ValueAs<bool>("IPOS_COMP"),
                        TipServiceId = record.ValueAs<Guid?>("IPOS_TISE"),
                        ApplyTipAutomatically = record.ValueAs<bool>("IPOS_TIAT"),
                        TipPercent = record.ValueAs<decimal?>("IPOS_TIPE"),
                        TipValue = record.ValueAs<decimal?>("IPOS_TIVL"),
                        ApplyTipOverNetValue = record.ValueAs<bool>("IPOS_TION"),
                        ProductGrouping = record.ValueAs<TicketProductGrouping>("IPOS_TIGR"),
                        MenuMonday = record.ValueAs<Guid?>("MENU_MON"),
                        MenuTuesday = record.ValueAs<Guid?>("MENU_TUE"),
                        MenuWednesday = record.ValueAs<Guid?>("MENU_WED"),
                        MenuThusday = record.ValueAs<Guid?>("MENU_THU"),
                        MenuFriday = record.ValueAs<Guid?>("MENU_FRI"),
                        MenuSaturday = record.ValueAs<Guid?>("MENU_SAT"),
                        MenuSunday = record.ValueAs<Guid?>("MENU_SUN"),
                        IsRoomService = record.ValueAs<bool>("IPOS_ROSE"),
                        PromptProductNote = record.ValueAs<bool>("IPOS_PRNO"),
                        AllowAndroidPayments = record.ValueAs<bool>("ANDR_PAYM"),
                        AskPax = record.ValueAs<bool>("ASK_PAX"),
                        BaseCurrency = record.ValueAs<string>("UNMO_BASE"),
                        AllowMealPlanPayment = record.ValueAs<bool>("IPOS_MEAL"),
                        AllowDepositPayment = record.ValueAs<bool>("IPOS_DEPO"),
                        AllowCreditRoomPayment = record.ValueAs<bool>("IPOS_ROOM"),
                        AllowHouseUsePayment = record.ValueAs<bool>("IPOS_HOUSE"),
                        AskDescription = record.ValueAs<bool>("ASK_DESC"),
                        AskRoom = record.ValueAs<bool>("ASK_ROOM"),
                        AskCode = record.ValueAs<bool>("ASK_CODE"),
                        ActivateIntegration = record.ValueAs<bool>("ACTI_INTE"),
                        AskInvoiceData = record.ValueAs<bool>("IPOS_AIVD")
                    };

                    results.Add(standRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        public POSStandRecord GetStandById(Guid standId, long language)
        {
            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetStandById(Manager);
                query.Parameters["ipos_pk"] = standId;
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();

                if (records.Count == 1)
                {
                    return new POSStandRecord
                    {
                        Id = records.ValueAs<Guid>("IPOS_PK"),
                        Abbreviation = records.ValueAs<string>("IPOS_ABRE"),
                        Description = records.ValueAs<string>("IPOS_DESC"),
                        StandWorkDate = records.ValueAs<DateTime>("IPOS_FCTR").Date,
                        StandWorkShift = records.ValueAs<long>("IPOS_TUTR"),
                        ControlAccount = records.ValueAs<Guid>("CTRL_PK"),
                        StandardRateType = records.ValueAs<Guid>("TPRG_STDR"),
                        ConsIntRateType = records.ValueAs<Guid>("TPRG_CINT"),
                        ConsIntPercentValue = records.ValueAs<decimal?>("TPRG_CIPO"),
                        PensionRateType = records.ValueAs<Guid?>("TPRG_PENS"),
                        PensionRatePercentValue = records.ValueAs<decimal?>("TPRG_PEPO"),

                        TicketPrintConfiguration = records.ValueAs<Guid>("CNFG_TICK"),
                        BallotPrintConfiguration = records.ValueAs<Guid?>("CNFG_BOLE"),
                        InvoicePrintConfiguration = records.ValueAs<Guid?>("CNFG_FACT"),
                        ReceiptPrintConfiguration = records.ValueAs<Guid?>("CNFG_COMP"),

                        NightAuditorReportCopies = records.ValueAs<short>("IPOS_CCOP"),
                        AllowCloseDayWithOpenTickets = records.ValueAs<bool>("IPOS_CKAB"),
                        CopyKitchenInTicketPrinter = records.ValueAs<bool>("IPOS_KITI"),
                        KeepTicketsInTableOnManualSplit = records.ValueAs<bool>("IPOS_SPTA"),
                        AllowPrintTableBalance = records.ValueAs<bool>("IPOS_COMP"),
                        TipServiceId = records.ValueAs<Guid?>("IPOS_TISE"),
                        ApplyTipAutomatically = records.ValueAs<bool>("IPOS_TIAT"),
                        TipPercent = records.ValueAs<decimal?>("IPOS_TIPE"),
                        TipValue = records.ValueAs<decimal?>("IPOS_TIVL"),
                        ApplyTipOverNetValue = records.ValueAs<bool>("IPOS_TION"),
                        MenuMonday = records.ValueAs<Guid>("MENU_MON"),
                        MenuTuesday = records.ValueAs<Guid>("MENU_TUE"),
                        MenuWednesday = records.ValueAs<Guid>("MENU_WED"),
                        MenuThusday = records.ValueAs<Guid>("MENU_THU"),
                        MenuFriday = records.ValueAs<Guid>("MENU_FRI"),
                        MenuSaturday = records.ValueAs<Guid>("MENU_SAT"),
                        MenuSunday = records.ValueAs<Guid>("MENU_SUN"),

                        AllowChangeTableReservationStatus = records.ValueAs<bool>("RESE_CHST"),
						BaseCurrency = records.ValueAs<string>("UNMO_BASE"),
						Currency = records.ValueAs<string>("UNMO_PK"),
						ExchangeFactor = records.ValueAs<decimal?>("CAMB_VALO"),

                        AllowMealPlanPayment = records.ValueAs<bool>("IPOS_MEAL"),
                        AllowDepositPayment = records.ValueAs<bool>("IPOS_DEPO"),
                        AllowCreditRoomPayment = records.ValueAs<bool>("IPOS_ROOM"),
                        AllowHouseUsePayment = records.ValueAs<bool>("IPOS_HOUSE"),

                        AskCode = records.ValueAs<bool>("ASK_CODE")
					};
                }
            }
            finally
            {
                Manager.Close();
            }

            return null;
        }

        public List<LiteStandRecord> GetStandsForTransfer(int language)
        {
            var results = new List<LiteStandRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetStandsForTransfer(Manager);
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var standRecord = new LiteStandRecord();
                    standRecord.Id = record.ValueAs<Guid>("IPOS_PK");
                    standRecord.Description = record.ValueAs<string>("IPOS_DESC");

                    results.Add(standRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        public ValidationResult PersistentTransfer(TransferContract contract)
        {
            var result = new ValidationResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var ticketTransfer = new TicketTransfer(Manager);
                    ticketTransfer.SetValues(contract.GetValues());
                    ticketTransfer.SaveObject();

                    Manager.EndTransaction(result);
                }
                catch (Exception ex)
                {
                    Trace.TraceError(ex.Message);
                    Manager.RollbackTransaction();
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationTicketsResult ReturnTransferedTickets(List<TransferContract> transfers)
        {
            var result = new ValidationTicketsResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var settingBusiness = BusinessContext.GetBusiness<ISettingBusiness>(Manager);
                    var settings = settingBusiness.GetGeneralSettings(_sessionContext.InstallationId);

                    foreach (var transferContract in transfers)
                    {
                        // Return Ticket (delete closing date and time)
                        var ticket = _ticketRepository.GetTicket(transferContract.TicketOrig);

						ticket.ClosedDate = null;
                        ticket.ClosedTime = null;
                        ticket.Process = false;
						
                        if (result.SaveTicket(ticket, settings.RealTimeTicketSync, _validatorFactory, nameof(ReturnTransferedTickets)).HasErrors)
							return result;

                        // Delete transfer line                    
                        var ticketTransfer = new TicketTransfer(Manager, transferContract.Id);
                        ticketTransfer.DeleteObject();
                    }

                    var business = BusinessContext.GetBusiness<ITicketBusiness>(Manager);
                    foreach (var transfer in transfers)
                    {
                        var loadTicketResult = business.LoadTicket(transfer.TicketOrig);
                        if (loadTicketResult.IsEmpty)
                        {
                            var contract = loadTicketResult.Contract.As<POSTicketContract>();
                            BusinessContext.Publish("TicketPersisted", (Guid)contract.Id);
                            result.Tickets.Add(loadTicketResult.Contract.As<POSTicketContract>());
                        }
                        else
                            result.Add(loadTicketResult);

                        if (!result.IsEmpty)
                            break;
                    }

                }
                catch (Exception ex)
                {
                    // Manager.RollbackTransaction();
                    result.AddException(ex);
                    return result;
                }
                finally
				{
					Manager.EndTransaction(result);
				}
			}
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult ReturnTransferTicket(TransferContract contract)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    // Reabrir el ticket
                    var ticket = _ticketRepository.NewTicket();
                    ticket.LoadObject(contract.TicketOrig, false);

                    var productLines = ticket.ProductLineByTicket;
                    ticket = ticket.Clone(Manager);
                    ticket.Id = Guid.NewGuid();
                    ticket.Signature = null;
                    ticket.ClosedDate = null;
                    ticket.ClosedTime = null;
                    ticket.Process = false;
                    ticket.Mesa = null;
                    ticket.Number = 0;

                    // Eliminar los pagos
                    ticket.PaymentLineByTicket.Clear();

                    var settingBusiness = BusinessContext.GetBusiness<ISettingBusiness>(Manager);
                    var settings = settingBusiness.GetGeneralSettings(_sessionContext.InstallationId);


                    if (result.SaveTicket(ticket, settings.RealTimeTicketSync, _validatorFactory, nameof(ReturnTransferTicket)).HasErrors)
                        return result;

                    foreach (var productLine in productLines)
                    {
                        var clonedProductLine = productLine.Clone(Manager);
                        clonedProductLine.Ticket = (Guid)ticket.Id;
                        clonedProductLine.Id = Guid.NewGuid();
                        clonedProductLine.SaveObject();
                    }

                    // Eliminar la transferencia
                    var ticketTransfer = new TicketTransfer(Manager, contract.Id);
                    ticketTransfer.DeleteObject();

                    // Manager.CommitTransaction();

                    var ticketBusiness = BusinessContext.GetBusiness<ITicketBusiness>(Manager);
                    var loadTicketResult = ticketBusiness.LoadTicket((Guid)ticket.Id);
                    if (loadTicketResult.IsEmpty)
                    {
                        var ticketContract = loadTicketResult.Contract.As<POSTicketContract>();
                        BusinessContext.Publish("TicketPersisted", (Guid)ticketContract.Id);

                        return new ValidationContractResult(ticketContract);
                    }
                    else
                        result.Add(loadTicketResult);
                }
                catch (Exception ex)
                {
                    Trace.TraceError(ex.Message);
                    // Manager.RollbackTransaction();
                    result.AddException(ex);
                }
                finally
                {
                    Manager.EndTransaction(result);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationResult TransferTicket(POSTicketContract ticket, TransferContract transfer)
        {
            var result = new ValidationResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var stand = new Stand(Manager, ticket.Stand);

                    // Close Ticket (without payment)
                    ticket.ClosedDate = _sessionContext.WorkDate;
                    ticket.ClosedTime = DateTime.Now;
                    ticket.Shift = stand.Shift;
                    ticket.Process = true;
                    ticket.Opened = false;
                    ticket.OpenUserId = null;
                    ticket.Mesa = null;
                    ticket.AllowPersistInvalid = true;
                    //ticket.Sync = DateTime.Now;

                    var ticketBusiness = BusinessContext.GetBusiness<ITicketBusiness>(Manager);
                    result.Add(ticketBusiness.PersistTicket(ticket));
                    if (result.IsEmpty)
                        result.Add(PersistentTransfer(transfer));

                    Manager.EndTransaction(result);
                    return result;
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationItemResult<StandTableInfoDto> GetTablesInfo()
        {
            var result = new ValidationItemResult<StandTableInfoDto>();

            var tablesWithOpenOrders = GetOpenTables(_sessionContext.StandId, null);
            if (tablesWithOpenOrders.HasErrors)
            {
                result.AddValidations(tablesWithOpenOrders);
                return result;
            }

            var tablesWithPendingMenuOrders = GetPendingOrdersTables();
            if (tablesWithPendingMenuOrders.HasErrors)
            {
                result.AddValidations(tablesWithPendingMenuOrders);
                return result;
            }

            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
            var filter = new TablesReservationFilterContract()
            {
                InDate = _sessionContext.WorkDate,
                StandId = _sessionContext.StandId,
                StateReservation = ReservationState.CheckIn
            };
            var tableReservations = business.GetTableReservations(filter);
            if (tableReservations.HasErrors)
            {
                result.AddValidations(tableReservations);
                return result;
            }

            result.Item = new StandTableInfoDto
            {
                TablesWithOpenOrders = tablesWithOpenOrders.ItemSource,
                TablesWithPendingMenuOrders = tablesWithPendingMenuOrders.ItemSource,
                TablesWithCheckInReservations = tableReservations.ItemSource
            };

            return result;
        }

        public ValidationContractResult AcceptTransferTicket(TransferContract contract, Guid cajaId, Guid userId, Guid hoteId)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    Guid? ticketId = null;
                    var business = BusinessContext.GetBusiness<ITicketBusiness>(Manager);

                    var newTicketResult = business.NewTicket(contract.StandDest, cajaId, hoteId);
                    if (!newTicketResult.HasErrors)
                    {
                        var ticket = _ticketRepository.NewTicket();
                        ticket.LoadObject(newTicketResult.Contract.Id, false);
                        var serieRecord = business.GetTicketSerieByStandCashier(contract.StandDest, cajaId)
                            .First(x => x.DocType == POSDocumentType.Ticket);
                        var stand = new Stand(Manager);
                        stand.LoadObject(contract.StandDest, false);

                        // Crear el nuevo ticket en la caja donde lo acepté
                        ticket.OpeningDate = stand.StandWorkDate;
                        ticket.OpeningTime = DateTime.Now;
                        ticket.Caja = cajaId;
                        ticket.Stand = contract.StandDest;
                        ticket.UserId = userId;
                        ticket.Serie = serieRecord.SerieTex;
                        ticket.Number = 0;

                        var loadOriginTicketResult = business.LoadTicket(contract.TicketOrig);
                        if (loadOriginTicketResult.IsEmpty)
                        {
                            var ticketTransfered = loadOriginTicketResult.Contract.As<POSTicketContract>();

                            ticket.Currency = ticketTransfered.Currency;
                            ticket.Paxs = ticketTransfered.Paxs;
                            ticket.TaxIncluded = ticketTransfered.TaxIncluded;
                            ticket.TaxSchemaId = ticketTransfered.TaxSchemaId;
                            ticket.ReceiveCons = 0;
                            ticket.Recharge = ticketTransfered.Recharge;

                            // Crear las nuevas lineas de productos al nuevo ticket
                            var records = business.GetProductLines(contract.TicketOrig);
                            if (!records.IsEmpty)
                            {
                                do
                                {
                                    var loadProductLineResult = business.LoadProductLine(records.ValueAs<Guid>("arve_pk"));
                                    if (loadProductLineResult.IsEmpty)
                                    {
                                        var productLine = new ProductLine(Manager);
                                        productLine.LoadObject(loadProductLineResult.Contract.Id, false);
                                        if (productLine.AnnulmentCode == ProductLineCancellationStatus.Cancelled)
                                            continue;

                                        var newProductLine = productLine.Clone(Manager);
                                        newProductLine.Id = Guid.NewGuid();
                                        newProductLine.AnnulmentCode = ProductLineCancellationStatus.ActiveByTransfer;
                                        newProductLine.Ticket = (Guid)ticket.Id;
                                        newProductLine.Stand = (Guid)stand.Id;

                                        ticket.ProductLineByTicket.Add(newProductLine);
                                    }
                                    else
                                        result.Add(loadProductLineResult);

                                    if (!result.IsEmpty)
                                        break;
                                } while (records.Next());
                            }
                        }

                        if (result.IsEmpty)
                        {
                            var settingBusiness = BusinessContext.GetBusiness<ISettingBusiness>(Manager);
                            var settings = settingBusiness.GetGeneralSettings(_sessionContext.InstallationId);

                            if (result.SaveTicket(ticket, settings.RealTimeTicketSync, _validatorFactory, nameof(AcceptTransferTicket)).HasErrors)
                            {
                                Manager.EndTransaction(result);
								return result;
                            }

                            ticketId = (Guid)ticket.Id;

                            // Actualizar la linea de transferencia
                            var loadTransferTicketResult = LoadTransfer((Guid)contract.Id);
                            if (loadTransferTicketResult.IsEmpty)
                            {
                                var ticketTransfer = loadTransferTicketResult.Contract.As<TransferContract>();
                                ticketTransfer.TicketDest = (Guid)ticket.Id;
                                ticketTransfer.UtilAcep = userId;
                                ticketTransfer.CajaDest = cajaId;
                                ticketTransfer.AcepDate = stand.StandWorkDate;
                                ticketTransfer.AcepTime = DateTime.Now;

                                result.Add(PersistentTransfer(ticketTransfer));
                            }
                            else
                                result.Add(loadTransferTicketResult);
                        }
                    }

                    if (Manager.EndTransaction(result))
                    {
                        if (ticketId.HasValue)
                        {
                            var loadTicketResult = business.LoadTicket(ticketId.Value);
                            if (loadTicketResult.IsEmpty)
                            {
                                var ticketContract = loadTicketResult.Contract.As<POSTicketContract>();
                                return new ValidationContractResult(ticketContract);
                            }
                            else
                                result.Add(loadTicketResult);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Trace.TraceError(ex.Message);
                    Manager.RollbackTransaction();
                    result.AddException(ex);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult VerifyTransferToAccept(Guid standId, Guid installationId, int language)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetTransfers(Manager);
                query.Parameters["hote_pk"] = installationId;
                query.Filters["ipos_dest"].Value = standId;
                query.Filters["titr_acep"].Value = false;
                query.Parameters["lang_pk"] = language;

                var containerContract = new ContainerTransferContract();

                var records = query.Execute();
                if (!records.IsEmpty)
                {
                    do
                    {
                        var contract = new TransferContract(records.ValueAs<Guid>("titr_pk"), records.ValueAs<Guid>("vend_orig"), records.ValueAs<Guid>("ipos_orig"),
                            records.ValueAs<Guid>("caja_orig"),
                            records.ValueAs<Guid?>("vend_dest"), records.ValueAs<Guid>("ipos_dest"), records.ValueAs<Guid?>("caja_dest"),
                            records.ValueAs<DateTime>("tran_date"), records.ValueAs<DateTime>("tran_time"), records.ValueAs<DateTime?>("acep_date"),
                            records.ValueAs<DateTime?>("acep_time"), records.ValueAs<Guid>("util_tran"), records.ValueAs<Guid?>("util_acep"),
                            records.ValueAs<string>("uttr_desc"), records.ValueAs<string>("utac_desc"),
                            records.ValueAs<string>("vdor_desc"), records.ValueAs<string>("stor_desc"), records.ValueAs<string>("caor_desc"),
                            records.ValueAs<string>("vdde_desc"), records.ValueAs<string>("stde_desc"), records.ValueAs<string>("cade_desc"),
                            records.ValueAs<decimal>("tick_valo"));

                        containerContract.Transfers.Add(contract);
                    } while (records.Next());
                }

                result.Contract = containerContract;
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult VerifyTransferToReturn(Guid standId, Guid installationId, int language)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetTransfers(Manager);
                query.Parameters["hote_pk"] = installationId;
                query.Filters["ipos_orig"].Value = standId;
                query.Filters["titr_acep"].Value = false;
                query.Parameters["lang_pk"] = language;

                var containerContract = new ContainerTransferContract();

                var records = query.Execute();
                if (!records.IsEmpty)
                {
                    do
                    {
                        var contract = new TransferContract(records.ValueAs<Guid>("titr_pk"), records.ValueAs<Guid>("vend_orig"), records.ValueAs<Guid>("ipos_orig"),
                            records.ValueAs<Guid>("caja_orig"),
                            records.ValueAs<Guid?>("vend_dest"), records.ValueAs<Guid>("ipos_dest"), records.ValueAs<Guid?>("caja_dest"),
                            records.ValueAs<DateTime>("tran_date"), records.ValueAs<DateTime>("tran_time"), records.ValueAs<DateTime?>("acep_date"),
                            records.ValueAs<DateTime?>("acep_time"), records.ValueAs<Guid>("util_tran"), records.ValueAs<Guid?>("util_acep"),
                            records.ValueAs<string>("uttr_desc"), records.ValueAs<string>("utac_desc"),
                            records.ValueAs<string>("vdor_desc"), records.ValueAs<string>("stor_desc"), records.ValueAs<string>("caor_desc"),
                            records.ValueAs<string>("vdde_desc"), records.ValueAs<string>("stde_desc"), records.ValueAs<string>("cade_desc"),
                            records.ValueAs<decimal>("tick_valo"));

                        containerContract.Transfers.Add(contract);
                    } while (records.Next());
                }

                result.Contract = containerContract;
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult LoadTransfer(Guid transferId)
        {
            Manager.Open();
            try
            {
                var ticketTransfer = new TicketTransfer(Manager, transferId);
                var contract = new TransferContract();
                contract.SetValues(ticketTransfer.GetValues());

                return new ValidationContractResult(contract);
            }
            finally
            {
                Manager.Close();
            }
        }

        public List<POSStandRecord> GetStandsFromCloud(int language)
        {
            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);

            return business.GetStandsFromCloud(language).ItemSource;
        }

        public StandTurnDateContract GetStandTurnDate(Guid standId)
        {
            var result = new StandTurnDateContract();

            Manager.Open();
            try
            {
                using (var command = Manager.CreateCommand("Context.GetWorkDateByStand"))
                {
                    command.CommandText = "select trunc(ipos.ipos_fctr), ipos.ipos_tutr from tnht_ipos ipos where ipos.ipos_pk = :ipos_pk";
                    Manager.CreateParameter(command, "ipos_pk", standId);
                    using (var reader = Manager.ExecuteReader(command))
                    {
                        if (reader.Read())
                        {
                            result.WorkDate = reader.GetDateTime(0);
                            result.Turn = reader.GetInt64(1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                throw ex;
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationResult CloseDayValidations(Guid standId, Guid cashierId, DateTime closeDay)
        {
            var result = new ValidationResult();

            try
            {
                Manager.Open();
                try
                {
                    var settingBusiness = BusinessContext.GetBusiness<ISettingBusiness>(Manager);
                    var settings = settingBusiness.GetGeneralSettings(_sessionContext.InstallationId);

                    #region WorkDate & SystemDate

                    {
                        if (settings.WorkDateOverSystemDate)
                        {
                            if (_sessionContext.WorkDate > DateTime.Today)
                            {
                                result.AddError("Stand workdate already ahead of today");
                                return result;
                            }

                            if (closeDay > DateTime.Today)
                            {
                                result.AddError("Stand workdate already ahead of today");
                                return result;
                            }
                        }
                    }

                    #endregion

                    #region Open Tickets

                    {
                        var openTicketsQuery = QueryFactory.Get<OpenTicketsQuery>(Manager);
                        openTicketsQuery.Parameters[OpenTicketsQuery.StandIdParam] = standId;
                        openTicketsQuery.Parameters[OpenTicketsQuery.CashierIdParam] = cashierId;
                        if (openTicketsQuery.ExecuteScalar<int>() > 0)
                        {
                            result.AddError("Close all tickets before closing day");
                            return result;
                        }
                    }

                    #endregion

                    #region Untransmitted Tickets

                    {
                        // Verify if there are no synced tickets but marked as synced
                        var request = new QueryRequest();
                        request.AddFilter("vend_close", 1);
                        request.AddFilter("ipos_pk", standId);
                        request.AddFilter("caja_pk", cashierId);
                        request.AddFilter("vend_datf", _sessionContext.WorkDate);
                        var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
                        var cloudResult = business.GetClosedTickets(request);
                        if (cloudResult.IsEmpty)
                        {
                            var parsedIds = cloudResult.ItemSource
                                .Select(x => (Guid) x.Id)
                                .Aggregate("", (current, id) => current + ('\'' + id.AsOracleRaw() + '\'' + ','))
                                .TrimEnd(',');

                            if (parsedIds.Length > 0)
                            {
                                var commandText =
                                    $"UPDATE TNHT_VEND SET VEND_SYNC = SYSTIMESTAMP WHERE VEND_DATF = :WORK_DATE AND CAJA_PK = :CAJA_PK AND IPOS_PK = :IPOS_PK AND VEND_SYNC IS NULL AND VEND_PK NOT IN ( {parsedIds} )";

                                using var command = Manager.CreateCommand("UpdateVendSyncCommand");
                                command.CommandText = commandText;
                                Manager.CreateParameter(command, "WORK_DATE", Manager.ConvertValueType(_sessionContext.WorkDate));
                                Manager.CreateParameter(command, "IPOS_PK", Manager.ConvertValueType(standId));
                                Manager.CreateParameter(command, "CAJA_PK", Manager.ConvertValueType(cashierId));
                                Manager.ExecuteNonQuery(command);
                            }

                            var untransmittedTicketsQuery = QueryFactory.Get<UntransmittedTicketsQuery>(Manager);
                            untransmittedTicketsQuery.Parameters[UntransmittedTicketsQuery.StandIdParam] = standId;
                            untransmittedTicketsQuery.Parameters[UntransmittedTicketsQuery.CashierIdParam] = cashierId;
                            // untransmittedTicketsQuery.Parameters[UntransmittedTicketsQuery.CancelledParam] = false;

                            var records = untransmittedTicketsQuery.Execute();
                            if (!records.IsEmpty)
                            {
                                var sb = new StringBuilder();
                                sb.AppendLine("Tickets not uploaded found. Please wait and try again in a few minutes for the synchronization to occur.");
                                foreach (IRecord record in records)
                                {
                                    var ticket = $"{record.ValueAs<string>("vend_codi")}/{record.ValueAs<string>("vend_seri")}";
                                    sb.AppendLine(ticket);
                                }

                                result.AddError(sb.ToString());
                                return result;
                            }
                        }
                    }

                    #endregion

                    #region Empty Fiscal Printer Data

                    if (settings.FiscalType != FiscalPOSType.None)
                    {
                        var emptyFiscalPrinterDataQuery = QueryFactory.Get<EmptyFiscalPrinterDataQuery>(Manager);
                        emptyFiscalPrinterDataQuery.Parameters[EmptyFiscalPrinterDataQuery.StandIdParam] = standId;
                        emptyFiscalPrinterDataQuery.Parameters[EmptyFiscalPrinterDataQuery.CashierIdParam] = cashierId;
                        emptyFiscalPrinterDataQuery.Parameters[EmptyFiscalPrinterDataQuery.CancelledParam] = false;
                        if (emptyFiscalPrinterDataQuery.ExecuteScalar<int>() > 0)
                        {
                            result.AddError("There are closed invoiced tickets with empty fiscal printer data. Try print or cancel tickets");
                            return result;
                        }
                    }

                    #endregion
                }
                finally
                {
                    Manager.Close();
                }
            }
            catch (Exception e)
            {
                result.AddError(e.Message);
            }

            return result;
        }

        public ValidationContractResult CloseDay(Guid standId, Guid cashierId, DateTime closeDay)
        {
            var result = new ValidationContractResult();

            try
            {
                result.AddValidations(CloseDayValidations(standId, cashierId, closeDay));
                if (result.IsEmpty)
                {
                    var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
                    var cloudResult = business.CloseDay(standId, cashierId, closeDay);

                    if (cloudResult == null)
                        result.AddError("Cloud communication failed");
                    else if (!cloudResult.IsEmpty)
                        result.Add(cloudResult);
                    else
                    {
                        var contract = cloudResult.Contract as StandTurnDateContract;
                        if (contract != null)
                        {
                            result.Contract = contract;

                            Manager.Open();
                            try
                            {
                                Manager.BeginTransaction();
                                try
                                {
                                    var stand = new Stand(Manager);
                                    stand.LoadObject(standId, false);
                                    stand.Shift = (int)contract.Turn;
                                    stand.StandWorkDate = contract.WorkDate;
                                    stand.SaveObject();

                                    contract.Id = stand.Id;

                                    Manager.CommitTransaction();
                                    BusinessContext.Publish("DayClosed", contract);
                                }
                                catch (Exception ex)
                                {
                                    Trace.TraceError(ex.Message);
                                    Manager.RollbackTransaction();
                                    throw ex;
                                }
                            }
                            finally
                            {
                                Manager.Close();
                            }
                        }
                        else
                            result.AddError("Failed to close day");
                    }
                }
            }
            catch (Exception ex)
            {
                result.AddException(ex);
            }

            return result;
        }

        public ValidationContractResult CloseTurn(Guid standId, Guid cajaId)
        {
            var result = new ValidationContractResult();

            try
            {
                var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
                var cloudResult = business.CloseTurn(standId, cajaId);

                if (cloudResult == null)
                    result.AddError("Cloud communication failed");
                else if (!cloudResult.IsEmpty)
                    result.Add(cloudResult);
                else
                {
                    var contract = cloudResult.Contract.As<StandTurnDateContract>();
                    if (contract != null)
                    {
                        result.Contract = contract;

                        Manager.Open();
                        try
                        {
                            Manager.BeginTransaction();
                            try
                            {
                                var stand = new Stand(Manager);
                                stand.LoadObject(standId, false);
                                stand.Shift = (int)contract.Turn;
                                stand.StandWorkDate = contract.WorkDate;
                                stand.SaveObject();

                                contract.Id = standId;

                                Manager.CommitTransaction();
                                BusinessContext.Publish("TurnClosed", contract);
                            }
                            catch (Exception ex)
                            {
                                Trace.TraceError(ex.Message);
                                Manager.RollbackTransaction();
                                throw ex;
                            }
                        }
                        finally
                        {
                            Manager.Close();
                        }
                    }
                    else
                        result.AddError("Failed to close turn");
                }
            }
            catch (Exception ex)
            {
                result.AddException(ex);
                return result;
            }

            return result;
        }

        public ValidationDateResult UndoCloseDay(Guid standId, Guid cajaId)
        {
            var result = new ValidationDateResult();

            Manager.Open();
            try
            {
                var stand = new Stand(Manager);
                stand.LoadObject(standId, false);

                // Verifications
                var query = new BaseQuery(Manager, "OpenedTickets",
                    "select 1 from tnht_vend where vend_dati = :vend_datr and ipos_pk = :ipos_pk and caja_pk = :caja_pk");
                query.Parameters["ipos_pk"] = standId;
                query.Parameters["caja_pk"] = cajaId;
                query.Parameters["vend_datr"] = stand.StandWorkDate;

                if (!query.Execute().IsEmpty)
                    result.AddError("Tickets already opened on this day, unable to undo.");
                else
                {
                    var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
                    var cloudResult = business.UndoCloseDay(standId, cajaId, _sessionContext.WorkDate.AddDays(-1));

                    if (cloudResult.IsEmpty)
                    {
                        var contract = cloudResult.Contract.As<StandTurnDateContract>();
                        stand.Shift = (int)contract.Turn;
                        stand.StandWorkDate = contract.WorkDate;
                        stand.SaveObject();
                        result.Date = stand.StandWorkDate;
                    }
                    else
                    {
                        result.AddValidations(cloudResult);
                    }
                }
            }
            catch (Exception ex)
            {
                result.AddException(ex);
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public DateTime GetWorkDateByStand(Guid standId)
        {
            Manager.Open();
            try
            {
                using (var command = Manager.CreateCommand("Context.GetWorkDateByStand"))
                {
                    command.CommandText = "select trunc(ipos.ipos_fctr) from tnht_ipos ipos where ipos.ipos_pk = :ipos_pk";
                    Manager.CreateParameter(command, "ipos_pk", standId);
                    using (var reader = Manager.ExecuteReader(command))
                    {
                        if (reader.Read())
                            return reader.GetDateTime(0);
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                throw ex;
            }
            finally
            {
                Manager.Close();
            }

            return DateTime.Now.Date;
        }

        public List<SaloonRecord> GetSaloonsByStand(Guid standId, int language)
        {
			language = language == 0 ? (int)_sessionContext.LanguageId : language;

			var results = new List<SaloonRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetSaloonsByStand(Manager);
                query.Parameters["ipos_pk"] = standId;
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var saloonRecord = new SaloonRecord
                    {
                        Id = record.ValueAs<Guid>("SALO_PK"),
                        Description = record.ValueAs<string>("SALO_DESC"),
                        SaloonWidth = record.ValueAs<short?>("SALO_WIDT"),
                        SaloonHeight = record.ValueAs<short?>("SALO_HEIG"),
                        ImageTableVacantState = record.ValueAs<string>("MESA_FREE"),
                        ImageTableBusyState = record.ValueAs<string>("MESA_OCUP"),
                        ImageSaloonFloorPlan = record.ValueAs<string>("SALO_IMAG")
                    };

                    results.Add(saloonRecord);
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                throw ex;
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        public List<ProductInStandRecord> GetProductsByStand(Guid standId, int language)
        {
            language = language == 0 ? (int)_sessionContext.LanguageId : language;
            var result = new List<ProductInStandRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetProductsByStand(Manager);
                query.Parameters["hote_pk"] = _sessionContext.InstallationId;
                query.Parameters["ipos_pk"] = standId;
                query.Parameters["lang_pk"] = language;
                query.Parameters["autoTicket"] = '0';

                var records = query.Execute();
                foreach(IRecord record in records)
                {
                    var ProductInStandRecord = new ProductInStandRecord
                    {
                        Id = record.ValueAs<Guid>("artg_pk"),
                        Product = record.ValueAs<string>("artg_desc"),
                        ProductCode = record.ValueAs<string>("artg_codi")

                    };
                    result.Add(ProductInStandRecord);
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                throw ex;
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }


        public List<TableRecord> GetTablesBySaloon(Guid saloonId)
        {
            try
            {
				return Manager.QueryTable(query => query.SetFilter("salo_pk",saloonId)).ToList();
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                throw ex;
            }
        }
        
        public ValidationItemSource<HouseUseRecord> GetHouseUses(string standId)
        {
            var result = new ValidationItemSource<HouseUseRecord>();
            var id = standId.AsGuid();
            var environment = GetStandEnvironment(id, (int)_sessionContext.LanguageId);

            result.ItemSource = environment.Item.HouseUses.ToList();

            return result;
        }

        #region Get Cloud Accounts

        public ValidationItemSource<ReservationRecord> GetReservations(ReservationFilterContract filter)
        {
            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
            return business.GetReservations(filter);
        }

        public ValidationItemResult<PmsReservationRecord> GetPmsReservation(PmsReservationFilterModel filter)
        {
            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
            return business.GetPmsReservation(filter);
        }
        
        public ValidationItemSource<EventReservationSearchFromExternalRecord> GetEventReservations(Guid? installationId = null)
        {
            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
            return business.GetEventReservations(installationId);
        }

        public ValidationItemSource<SpaReservationSearchFromExternalRecord> GetSpaReservations(SpaSearchReservationFilterModel filter)
        {
            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
            return business.GetSpaReservations(filter);
        }

        public ValidationItemResult<SpaServiceRecord> GetSpaServices(Guid? installationId, Guid spaId)
        {
            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
            return business.GetSpaServices(installationId, spaId);
        }

        public ValidationItemSource<PmsCompanyRecord> GetEntityInstallations(PmsCompanyFilterModel filter)
        {
            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
            return business.GetEntityInstallations(filter);
        }

        public ValidationItemSource<ReservationsGroupsRecord> GetReservationsGroups(Guid? installationId = null)
        {
            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
            return business.GetReservationsGroups(installationId);
        }

        public ValidationItemSource<PmsControlAccountRecord> GetControlAccounts(PmsControlAccountFilterModel filter)
        {
            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
            return business.GetControlAccounts(filter);
        }

        public ValidationItemSource<ControlAccountMovementDto> GetControlAccountMovements(ControlAccountMovementsFilterModel filter)
        {
            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
            return business.GetControlAccountMovements(filter);
        }

        public ValidationItemSource<ClientInstallationRecord> GetClientInstallations(bool openAccounts, Guid? clientId = null, string room = null, string fullName = null, Guid? installationId = null)
        {
            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
            return business.GetClientInstallations(openAccounts, clientId, room, fullName, installationId);
        }

        #endregion

        #region Meals Control

        public ValidationItemSource<MealsControlRecord> GetMealsControl(MealsFilterModel model)
        {
            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
            return business.GetMealsControl(model);
        }

        public ValidationResult UpdateMealsControl(Guid guestId, bool breakfast, bool lunch, bool dinner, Guid? installationId = null)
        {
            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
            return business.UpdateMealsControl(guestId, breakfast, lunch, dinner, installationId);
        }

        public ValidationItemResult<ClientInfoContract> GetClientInfo(Guid clientId)
        {
            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
            return business.GetClientInfo(clientId);
        }

        #endregion

        public ValidationItemSource<ClosedTicketRecord> GetOldTickets(Guid? standId, Guid? cashierId, DateTime? date)
        {
            var result = new ValidationItemSource<ClosedTicketRecord>();

            Manager.Open();
            try
            {
                #region Cloud tickets

                var request = new QueryRequest();
                request.AddFilter("vend_close", 1);
                request.AddFilter("vend_trans", 0);
                if (standId.HasValue)
                    request.AddFilter("ipos_pk", standId.Value);
                if (cashierId.HasValue)
                    request.AddFilter("caja_pk", cashierId.Value);
                if (date.HasValue)
                    request.AddFilter("vend_datf", date.Value);

                var cloudBusiness = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
                var cloudValidation = cloudBusiness.GetClosedTickets(request);
                result.AddValidations(cloudBusiness.GetOfflineModeValidations(cloudValidation));

                #endregion

                #region Merge closed tickets from cloud into local tickets

                if (result.IsEmpty)
                {
                    #region Local tickets

                    var query = QueryFactory.Get<LocalClosedTicketsQuery>(Manager);

                    request = new QueryRequest();
                    if (standId.HasValue)
                        request.AddFilter(LocalClosedTicketsQuery.StandIdFilter, standId.Value);
                    if (cashierId.HasValue)
                        request.AddFilter(LocalClosedTicketsQuery.CashierIdFilter, cashierId.Value);
                    if (date.HasValue)
                        request.AddFilter(LocalClosedTicketsQuery.CloseDateFilter, date.Value);
                    query.SetRequest(request);

                    var tickets = query.ExecuteList<ClosedTicketRecord>();

                    #endregion

                    var localTickets = new List<ClosedTicketRecord>(tickets);
                    localTickets.ForEach(lt =>
                    {
                        lt.LocalTicket = true;
                        {
                            lt.LocalCreditNote = !string.IsNullOrEmpty(lt.CreditNoteNumber);
                        }
                    });

                    if (cloudValidation.ItemSource != null)
                    {
                        foreach (var remoteTicket in cloudValidation.ItemSource.OrderByDescending(x => x.CloseTime))
                        {
                            var localTicket = localTickets.FirstOrDefault(x => x.Id.Equals(remoteTicket.Id));
                            if (localTicket == null)
                                localTickets.Add(remoteTicket);
                            else
                            {
                                localTicket.RoomNumber = remoteTicket.RoomNumber;

                                if (remoteTicket.Cancelled || localTicket.Cancelled)
                                    localTicket.Cancelled = true;

                                if (!string.IsNullOrEmpty(remoteTicket.CreditNoteNumber) && string.IsNullOrEmpty(localTicket.CreditNoteNumber))
                                    localTicket.CreditNoteNumber = remoteTicket.CreditNoteNumber;
                            }
                        }
                    }

                    result.ItemSource = localTickets.OrderByDescending(x => x.CloseTime).ToList();
                }

                #endregion
            }
            catch (Exception ex)
            {
                result.AddException(ex);
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public List<SalesCashierRecord> GetSalesReport(QueryRequest queryRequest, bool isTurnReport)
        {
            var results = new List<SalesCashierRecord>();

            try
            {
                var query = new SalesReportQuery(Manager)
                {
                    Parameters =
                    {
                        ["LANG_PK"] = _sessionContext.LanguageId,
                        ["IPOS_PK"] = new Guid(queryRequest.GetParameter("ipos_pk").Value.ToString()),
                        ["CAJA_PK"] = new Guid(queryRequest.GetParameter("caja_pk").Value.ToString()),
                        ["VEND_DATF"] = new DateTime((long)queryRequest.GetParameter("workDate").Value)
                    }
                };

                if (isTurnReport)
                    query.Parameters.Add("TURN_CODI", long.Parse(queryRequest.GetParameter("turn_codi").Value.ToString()));
                
                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var sale = new SalesCashierRecord
                    {
                        GroupDescription = record.ValueAs<string>("GRUP_DESC"),
                        Id = record.ValueAs<Guid>("ARVE_PK"),
                        Importe = record.ValueAs<decimal>("IMPORTE"),
                        Paxs = record.ValueAs<short>("PAXS"),
                        ProductDescription = record.ValueAs<string>("ARTG_DESC"),
                        Quantity = record.ValueAs<short>("QTD")
                    };

                    results.Add(sale);
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                throw;
            }

            return results;
        }

        public ValidationItemSource<SalePaymentRecord> GetPaymentsReport(QueryRequest request)
        {
            var result = new ValidationItemSource<SalePaymentRecord>();

            try
            {
                var query = QueryFactory.Get<PaymentsReportQuery>(Manager);
                query.Parameters[PaymentsReportQuery.LanguageIdParam] = _sessionContext.LanguageId;
                query.Parameters[PaymentsReportQuery.StandIdParam] = new Guid(request.GetParameter("ipos_pk").Value.ToString());
                query.Parameters[PaymentsReportQuery.CashierIdParam] = new Guid(request.GetParameter("caja_pk").Value.ToString());
                query.Parameters[PaymentsReportQuery.DateClosedParam] = new DateTime((long)request.GetParameter("vend_datf").Value);
                query.Parameters[PaymentsReportQuery.RoomCreditLabelParam] = LocalizationMgr.Translation(_sessionContext.LanguageId, "RoomCredit");

                if (request.HasFilter(PaymentsReportQuery.PaymentTypeIdFilter))
                {
                    var value = request.GetFilter(PaymentsReportQuery.PaymentTypeIdFilter).Value;
                    var filter = query.Filters[PaymentsReportQuery.PaymentTypeIdFilter];
                    filter.Value = value;
                }

                result.ItemSource = query.ExecuteList<SalePaymentRecord>().ToList();
            }
            catch (Exception e)
            {
                Trace.TraceError(e.Message);
                result.AddError(e.Message);
            }

            return result;
        }

        public TicketRContract LoadTicketReal(Guid ticketId)
        {
            var business = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
            return business.LoadTicketReal(ticketId).Contract as TicketRContract;
        }

        public ValidationResult UpdateMesa(MesaContract contract)
        {
            try
            {
                Manager.Open();
                try
                {
                    Manager.BeginTransaction();
                    try
                    {
                        var table = new Table(Manager);
                        table.SetValues(contract.GetValues());
                        table.SaveObject();

                        Manager.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        Trace.TraceError(ex.Message);
                        Manager.RollbackTransaction();
                        var result = new ValidationResult();
                        result.AddException(ex);
                        return result;
                    }
                }
                finally
                {
                    Manager.Close();
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                throw ex;
            }

            return null;
        }

        public CurrencyRecord GetCurrencyByStand(Guid standId, int language)
        {
            var result = new CurrencyRecord();

            Manager.Open();
            try
            {
                using (var command = Manager.CreateCommand("GetStandardCurrencyByStand"))
                {
                    command.CommandText =
                        "select unmo.unmo_pk,(select mult_desc from vnht_mult where lite_pk = unmo.lite_pk and lang_pk = :lang_pk),unmo.unmo_ndec from tnht_unmo unmo where unmo_pk in (select tprg.unmo_pk from  tnht_tprg tprg, tnht_ipos ipos where tprg.tprg_pk = ipos.tprg_stdr and ipos.ipos_pk = :ipos_pk)";
                    Manager.CreateParameter(command, "ipos_pk", standId);
                    Manager.CreateParameter(command, "lang_pk", language);

                    using (var reader = Manager.ExecuteReader(command))
                    {
                        if (reader.Read())
                        {
                            result.Id = reader.GetString(0);
                            result.Description = reader.GetString(1);
                            result.SetDecimalPlaces = reader.GetInt16(2);
                        }
                    }
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationItemSource<ClientRecord> GetClientEntities(QueryRequest request)
        {
            var result = new ValidationItemSource<ClientRecord>();

            result.ItemSource = new List<ClientRecord>();
            try
            {
                var query = QueryFactory.Get<ClientsQuery>(Manager);
                if (request != null)
                    query.SetRequest(request);

                foreach (IRecord record in query.Execute())
                {
                    var client = new ClientRecord
                    {
                        Id = record.ValueAs<Guid>("clie_pk"),
                        Name = record.ValueAs<string>("clie_nome"),
                        AuxiliarCode = record.ValueAs<string>("clie_caux"),
                        FiscalNumber = record.ValueAs<string>("fisc_numb"),
                        Country = record.ValueAs<string>("naci_pk"),
                        CountryName = record.ValueAs<string>("naci_desc"),
                        PhoneNumber = record.ValueAs<string>("home_phone"),
                        Address = record.ValueAs<string>("fisc_addr1"),
                        DoorNumber = record.ValueAs<string>("fisc_door"),
                        AddressComplement = record.ValueAs<string>("fisc_addr2"),
                        Location = record.ValueAs<string>("fisc_loca"),
                        PostalCode = record.ValueAs<string>("fisc_copo"),
                        DistrictCode = record.ValueAs<string>("dist_caux"),
                        District = record.ValueAs<string>("dist_desc"),
                        StateCode = record.ValueAs<string>("comu_caux"),
                        FiscalRegister = record.ValueAs<string>("regi_fisc"),
                        EmailAddress = record.ValueAs<string>("email_addr"),
                        Passport = record.ValueAs<string>("clie_pass"),
                        Identity = record.ValueAs<string>("clie_iden"),
                        Residence = record.ValueAs<string>("clie_resd"),
                        DriverLicense = record.ValueAs<string>("clie_driv"),
                        IsClient = record.ValueAs<bool>("clie_clie"),
                        FiscalNotePreferred = record.ValueAs<bool>("enti_fino"),
                        ApplyAutomaticProductDiscount = record.ValueAs<bool>("clie_deau"),
                        FiscalRegimeCode = record.ValueAs<string>("fisc_cotr"),
                        FiscalResponsibilityCode = record.ValueAs<string>("fisc_corf"),
                    };

                    result.ItemSource.Add(client);
                }
            }
            catch (Exception ex)
            {
                result.AddException(ex);
            }

            return result;
        }

        public ValidationItemSource<Guid> CreateClient(string name, string fiscalNumber, string email, string fiscalAddress, string country, Guid? clientId = null)
        {
            var result = new ValidationItemSource<Guid>
            {
                ItemSource = []
            };

            var newClientId = clientId ?? Guid.NewGuid();
            try
            {
                const string command = """
                                       merge
                                       into tnht_clie cl01
                                       using (select clie_pk
                                              from tnht_clie where clie_pk=:clie_pk) cl02
                                       on (cl01.clie_pk = cl02.clie_pk)
                                       when matched then
                                           update
                                           set clie_nome  = :clie_nome,
                                               fisc_numb  = :fisc_numb,
                                               email_addr = :email_addr,
                                               fisc_addr1 = :fisc_addr1,
                                               naci_pk    = :naci_pk
                                       when not matched then
                                           insert (clie_pk, clie_nome, fisc_numb, email_addr, fisc_addr1, naci_pk, clie_lock, clie_type)
                                           values (:clie_pk, :clie_nome, :fisc_numb, :email_addr, :fisc_addr1, :naci_pk, '0', 11)
                                       """;
                var query = new BaseQuery(Manager, "CreateClient", command)
                {
                    Parameters =
                    {
                        ["clie_pk"] = newClientId,
                        ["clie_nome"] = name ?? string.Empty,
                        ["fisc_numb"] = fiscalNumber ?? string.Empty,
                        ["email_addr"] = email ?? string.Empty,
                        ["fisc_addr1"] = fiscalAddress ?? string.Empty,
                        ["naci_pk"] = country
                    }
                };
                query.Execute();
            }
            catch (Exception ex)
            {
                result.AddException(ex);
            }

            result.ItemSource.Add(newClientId);
            return result;
        }

        public ValidationItemSource<Guid> GetOpenTables(Guid standId, Guid? saloonId)
        {
            var result = new ValidationItemSource<Guid>();

            try
            {
                const string sb = """
                                  SELECT MESA.MESA_PK, MESA.MESA_DESC, MESA.SALO_PK
                                  FROM TNHT_MESA MESA,
                                       TNHT_SALO SALO,
                                       TNHT_IPSL IPSL
                                  WHERE MESA.SALO_PK = SALO.SALO_PK
                                    AND SALO.SALO_PK = IPSL.SALO_PK
                                    AND IPSL.IPOS_PK = :IPOS_PK
                                    AND EXISTS(
                                          SELECT 1
                                          FROM TNHT_VEND VEND
                                          WHERE VEND.VEND_MESA = MESA.MESA_PK
                                            AND VEND.IPOS_PK = :IPOS_PK
                                            AND VEND.CAJA_PK = :CAJA_PK
                                            AND VEND.HOTE_PK = :HOTE_PK
                                            AND VEND.VEND_DATF IS NULL
                                      )
                                  """;

                var query = new BaseQuery(Manager, "OpenTables", sb)
                {
                    Parameters =
                    {
                        ["IPOS_PK"] = standId,
                        ["CAJA_PK"] = _sessionContext.CashierId,
                        ["HOTE_PK"] = _sessionContext.InstallationId,
                    }
                };
                query.Filters.Add("salo_pk", "salo.salo_pk", typeof(Guid));
                query.Filters["salo_pk"].Value = saloonId;

                var tables = new List<Guid>();
                foreach (IRecord record in query.Execute())
                    tables.Add(record.ValueAs<Guid>("mesa_pk"));

                result.ItemSource = tables;
            }
            catch (Exception ex)
            {
                result.AddException(ex);
            }

            return result;
        }

        #region Private Methods

        private ValidationItemSource<LiteTablesReservationRecord> GetSaloonReservations(ICloudBusiness business, IEnumerable<Guid> saloonIds, DateTime date)
        {
            var filter = new TablesReservationFilterContract
            {
                InDate = date,
                SaloonIds = saloonIds.ToArray(),
                StateReservation = ReservationState.Reserved
            };

            return business.GetTableReservations(filter);
        }

        private static ValidationItemSource<LiteTablesReservationRecord> GetTableReservations(IReservationTableBusinessCloud business, Guid standId, DateTime date)
        {
            var filter = new TablesReservationFilterContract
            {
                InDate = date,
                StandId = standId,
                StateReservation = ReservationState.Reserved
            };

            return business.GetTableReservations(filter);
        }


        private ValidationResult UpdateVersions(ICloudBusiness business)
        {
            Manager.Open();
            try
            {
                var query = new BaseQuery(Manager, "GetVersions", "select date_vers, sync_vers from tcfg_gene");

                var record = query.Execute();
                if (!record.IsEmpty)
                {
                    var dbVersion = record.ValueAs<string>("date_vers");
                    var syncVersion = record.ValueAs<string>("sync_vers");
                    var assembly = Assembly.GetEntryAssembly();
                    var version = assembly.GetName().Version;
                    var appVersion = version.Major.ToString("0000") + version.Minor.ToString("00") + version.Build.ToString("00") + "-" + version.Revision.ToString();

                    return business.UpdateVersions(dbVersion, syncVersion, appVersion);
                }
            }
            finally
            {
                Manager.Close();
            }

            return ValidationResult.Empty;
        }

        /*
        private Task<ValidationResult> UpdateVersionsAsync(ICloudBusiness business)
        {
            Manager.Open();
            try
            {
                var query = new BaseQuery(Manager, "GetVersions", "select date_vers, sync_vers from tcfg_gene");

                var record = query.Execute();
                if (!record.IsEmpty)
                {
                    var dbVersion = record.ValueAs<string>("date_vers");
                    var syncVersion = record.ValueAs<string>("sync_vers");
                    var assembly = Assembly.GetEntryAssembly();
                    var version = assembly.GetName().Version;
                    var appVersion = version.Major.ToString("0000") + version.Minor.ToString("00") + version.Build.ToString("00") + "-" + version.Revision.ToString();

                    return business.UpdateVersionsAsync(dbVersion, syncVersion, appVersion);
                }
            }
            finally
            {
                Manager.Close();
            }

            return Task.FromResult(ValidationResult.Empty);
        }
        */

        private IList<CurrencyExchangeRecord> GetCurrencyExchanges()
        {
            Manager.Open();
            try
            {
                var query = QueryFactory.Get<CurrencyExchangeQuery>(Manager);
                query.Parameters[CurrencyExchangeQuery.DateWorkParam] = _sessionContext.WorkDate;

                return query.ExecuteList<CurrencyExchangeRecord>();
            }
            finally
            {
                Manager.Close();
            }
        }

        //private IList<CurrencyCashierRecord> GetCurrencies(Guid? hotelId = null)
        //{
        //    Manager.Open();
        //    try
        //    {
        //        var query = QueryFactory.Get<CurrencyCashierQuery>(Manager);
        //        query.Parameters[CurrencyCashierQuery.HoteIdParam] = hotelId ?? BusinessContext.InstallationId;

        //        return query.ExecuteList<CurrencyCashierRecord>();
        //    }
        //    finally
        //    {
        //        Manager.Close();
        //    }
        //}

        private ValidationResult FillUserPermissions(UserRecord user)
        {
            var result = new ValidationResult();

            user.Permissions = new Dictionary<Permissions, Tuple<SecurityCode, string>>();
            try
            {
                var sb = new StringBuilder();
                sb.AppendLine("select perm.perm_pk, peut.secu_code,");
                sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = perm.lite_pk and mult.lang_pk = :lang_pk) as perm_desc");
                sb.AppendLine("from tnht_peut peut");
                sb.AppendLine("inner join tnht_peap peap on peap.peap_pk = peut.peap_pk");
                sb.AppendLine("inner join tnht_perm perm on perm.perm_pk = peap.perm_pk");
                sb.AppendLine("where peut.util_pk = :util_pk and peap.appl_pk = :appl_pk");

                var query = new BaseQuery(Manager, "UserPermissions", sb.ToString());
                query.Parameters["util_pk"] = user.Id;
                query.Parameters["appl_pk"] = CloudBusiness.ApplicationId;
                query.Parameters["lang_pk"] = _sessionContext.LanguageId;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var permission = NewHotelPermissions.GetPermissionsEnum(record.ValueAs<Guid>("perm_pk"));
                    if (permission.HasValue && !user.Permissions.ContainsKey(permission.Value))
                    {
                        user.Permissions.Add(permission.Value,
                            Tuple.Create(record.ValueAs<SecurityCode>("secu_code"),
                                record.ValueAs<string>("perm_desc")));
                    }
                }
            }
            catch (Exception ex)
            {
                result.AddException(ex);
            }

            return result;
        }

        private HotelContract GetHotel(Guid hotelId)
        {
            var hotel = new Hotel(Manager, hotelId);
            var contract = new HotelContract();
            contract.SetValues(hotel.GetValues());

            return contract;
        }

        #endregion

        #endregion

        #region IPaxWaitingList

        public ValidationContractResult NewPaxWaitingList()
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                var paxWaitingList = new PaxWaitingList(Manager);
                var contract = new PaxWaitingListContract();
                contract.SetValues(paxWaitingList.GetValues());
                result.Contract = contract;
            }
            catch (Exception e)
            {
                result.AddError(e.Message);
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult LoadPaxWaitingList(Guid id)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                var paxWaitingList = new PaxWaitingList(Manager, id);
                var contract = new PaxWaitingListContract();
                contract.SetValues(paxWaitingList.GetValues());
                result.Contract = contract;
            }
            catch (Exception e)
            {
                result.AddError(e.Message);
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult PersistPaxWaitingList(PaxWaitingListContract contract)
        {
            var result = new ValidationContractResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var paxWaitingList = new PaxWaitingList(Manager);
                    paxWaitingList.SetValues(contract.GetValues());
                    if (!paxWaitingList.IsPersisted)
                        paxWaitingList.RegisterDate = DateTime.Now;

                    paxWaitingList.SaveObject();

                    result.Contract = contract;

                    Manager.CommitTransaction();
                }
                catch (Exception e)
                {
                    result.AddError(e.Message);
                    Manager.RollbackTransaction();
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationResult DeletePaxWaitingList(Guid id)
        {
            var result = new ValidationResult();

            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var paxWaitingList = new PaxWaitingList(Manager, id);
                    paxWaitingList.DeleteObject();

                    Manager.CommitTransaction();
                }
                catch (Exception e)
                {
                    result.AddError(e.Message);
                    Manager.RollbackTransaction();
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationItemSource<PaxWaitingListRecord> GetWaitingList(WaitingListFilterContract filter)
        {
            var result = new ValidationItemSource<PaxWaitingListRecord>();

            Manager.Open();
            try
            {
                var request = new Request();
                if (!string.IsNullOrEmpty(filter.Name))
                    request.AddFilter("wlst_name", filter.Name);
                if (!string.IsNullOrEmpty(filter.PhoneNumber))
                    request.AddFilter("wlst_phone", filter.PhoneNumber);
                if (filter.StandId.HasValue)
                    request.AddFilter("ipos_pk", filter.StandId.Value);
                if (filter.SaloonId.HasValue)
                    request.AddFilter("salo_pk", filter.SaloonId.Value);
                if (filter.Paxs.HasValue)
                    request.AddFilter("wlst_paxs", filter.Paxs.Value);

                var query = CommonQueryFactory.GetWaitingList(Manager);
                query.SetRequest(request);

                result.ItemSource = query.ExecuteList<PaxWaitingListRecord>().ToList();
            }
            catch (Exception e)
            {
                result.AddError(e.Message);
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        #endregion

        #region Handheld

        public ValidationItemSource<Guid> GetPendingOrdersTables()
        {
            var result = new ValidationItemSource<Guid>();
            var list = new List<Guid>();

            Manager.Open();
            try
            {
                var query = new BaseQuery(
                    Manager,
                    "GetPendingOrdersTable",
                    @"SELECT DISTINCT MESA_PK FROM TNHT_MEOR WHERE IPOS_PK = :POS_ID AND MEOR_PEDI = 1"
                )
                {
                    Parameters = { ["POS_ID"] = _sessionContext.StandId }
                };

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    list.Add(record.ValueAs<Guid>("MESA_PK"));
                }

                result.ItemSource = list;
            }
            catch (Exception e)
            {
                result.AddException(e);
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        #endregion

        #region Digital Menu

        public ValidationItemResult<StandEnvironment> GetMenuStandEnvironment(Guid hotelId, Guid standId, int langId)
        {
            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    var result = new ValidationItemResult<StandEnvironment>();

                    var lcid = langId;

                    var settingBusiness = BusinessContext.GetBusiness<ISettingBusiness>(Manager);
                    var productBusiness = BusinessContext.GetBusiness<IProductBusiness>(Manager);

                    var settings = settingBusiness.GetGeneralSettings(hotelId);

                    var environment = new StandEnvironment
                    {
                        Hotel = GetHotel(hotelId)
                    };

                    foreach (var categoryOption in settingBusiness.GetButtonCategories(hotelId, lcid).Select(category =>
                                 new CategoryOption
                                 {
                                     Category = category,
                                     SubCategories = settingBusiness.GetButtonSubCategories(category.Id, lcid)
                                         .Select(s => new SubCategoryOption
                                         {
                                             SubCategory = s,
                                             ProductColumns =
                                                 settingBusiness.GetColumnsPlusProducts(standId, s.Id, lcid)
                                         })
                                         .ToList()
                                 }))
                    {
                        environment.Categories.Add(categoryOption);
                    }

                    var stand = GetStandById(standId, lcid);

                    if (result.IsEmpty)
                    {
                        environment.ProductGroups = productBusiness.GetProductGroups(hotelId, standId, lcid);
                        environment.ProductFamilies = productBusiness.GetProductFamilies(hotelId, standId, lcid);
                        environment.ProductSubFamilies = productBusiness.GetProductSubFamilies(hotelId, standId, lcid);
                        environment.Preparations = productBusiness.GetPreparations(lcid);

                        #region Currencies

                        // Moneda del punto de venta
                        var standCurrencyId = GetStandardCurrencyByStand(standId);
                        environment.Currency = standCurrencyId;

                        // Definiciones de monedas usables en caja
                        var currencies = Manager.GetCurrencies(hotelId); // GetCurrencies(hotelId);
                        environment.Currencys = currencies;


                        // Tratar de poner la moneda base como la que tiene el stand configurado. 
                        // Si no se puede prevalece la base del hotel
                        foreach (var currency in currencies)
                        {
                            if (currency.CurrencyId != standCurrencyId) continue;

                            currency.IsBase = true;
                            foreach (var item in currencies.Where(x => x != currency))
                                item.IsBase = false;
                            break;
                        }

                        #endregion

                        if (!currencies.Any(c => c.IsBase))
                            result.AddError($"Undefined base currency for {stand.Description}");
                        else
                        {
                            if (stand.StandardRateType.HasValue)
                            {
                                if (settings.DefaultTaxSchemaId.HasValue)
                                {
                                    environment.Products = productBusiness.GetProductsByStandRate(hotelId, standId, lcid);
                                }
                                else
                                    result.AddError("Undefined default tax schema");
                            }
                            else
                                result.AddError($"Standard rate not defined for {stand.Description}");
                        }

                        result.Item = environment;
                    }

                    Manager.EndTransaction(result);
                    return result;
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    return new ValidationItemResult<StandEnvironment>(ex);
                }
            }
            finally
            {
                Manager.Close();
            }
        }


        #endregion
    }
}