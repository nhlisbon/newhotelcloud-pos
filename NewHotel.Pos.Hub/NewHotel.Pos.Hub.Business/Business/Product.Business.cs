﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using NewHotel.Business;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.DataAnnotations;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Business.SqlContainer.Common;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Products;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Tickets;
using NewHotel.Pos.Hub.Contracts.Common.Records.Products;
using NewHotel.Pos.Hub.Contracts.Common.Records.Stands;
using NewHotel.Pos.Hub.Core;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Hub.Model.Session;
using ProductRecord = NewHotel.Pos.Hub.Contracts.Common.Records.Products.ProductRecord;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class ProductBusiness : BaseBusiness, IProductBusiness
    {
		private readonly IPosSessionContext sessionContext;
		#region Constructor

		public ProductBusiness(IDatabaseManager manager, IPosSessionContext sessionContext)
            : base(manager)
        {
			this.sessionContext = sessionContext;
		}

        #endregion
        #region IProductBusiness

        public List<ProductRecord> GetProductsByStandRate(Guid hotelId, Guid standId, int language, bool base64Image = false, bool autoToTicket = false)
        {
            var results = new Dictionary<Guid, ProductRecord>();

            Manager.Open();
            try
            {
                var areasByProduct = new Dictionary<Guid, List<AreasRecord>>();
                var preparationsByProduct = new Dictionary<Guid, List<PreparationRecord>>();
                var tableProductsByProduct = new Dictionary<Guid, List<ProductTableRecord>>();
                var taxSchemasByProduct = new Dictionary<Guid, List<TaxSchemaProductRecord>>();
                var productTables = new Dictionary<Guid, ProductTableRecord>();
                var slidersByProduct = new Dictionary<Guid, List<string>>();

                #region Load Areas for all products

                var areaQuery = CommonQueryFactory.GetAreasByProducts(Manager);
                areaQuery.SetParameter("lang_pk", language);
                areaQuery.SetParameter("ipos_pk", standId.ToHex());

                var records = areaQuery.Execute();
                // Do not use foreach to avoid possible object allocation
                // ReSharper disable once ForCanBeConvertedToForeach
                for (var i = 0; i < records.Count; i++)
                {
                    var record = (IRecord)records[i];
                    var area = new AreasRecord
                    {
                        Id = record.ValueAs<Guid>("area_pk"),
                        Description = record.ValueAs<string>("area_desc"),
                        NumberCopies = record.ValueAs<short>("area_nuco"),
                        Order = record.ValueAs<short>("arar_orde"),
                        Mandatory = record.ValueAs<bool>("arar_arma")
                    };
                    var color = record.ValueAs<int?>("area_colo");
                    if (color.HasValue)
                        area.AreaColor = new ARGBColor(color.Value);

                    var productId = record.ValueAs<Guid>("artg_pk");

                    if (!areasByProduct.TryGetValue(productId, out var areasRecordList))
                    {
                        areasRecordList = new List<AreasRecord>();
                        areasByProduct.Add(productId, areasRecordList);
                    }

                    areasRecordList.Add(area);
                }

                #endregion

                #region Load Preparations for all products

                var preparationQuery = CommonQueryFactory.GetPreparationsByProducts(Manager);
                preparationQuery.SetParameter("lang_pk", language);

                records = preparationQuery.Execute();
                // Do not use foreach to avoid possible object allocation
                // ReSharper disable once ForCanBeConvertedToForeach
                for (var i = 0; i < records.Count; i++)
                {
                    var record = (IRecord)records[i];
                    var preparation = new PreparationRecord
                    {
                        Id = record.ValueAs<Guid>("prep_pk"),
                        Description = record.ValueAs<string>("prep_desc")
                    };

                    var productId = record.ValueAs<Guid>("artg_pk");

                    if (!preparationsByProduct.TryGetValue(productId, out var preparationRecordList))
                    {
                        preparationRecordList = new List<PreparationRecord>();
                        preparationsByProduct.Add(productId, preparationRecordList);
                    }

                    preparationRecordList.Add(preparation);
                }

                #endregion

                #region Load Table Products for all products

                var tableProductQuery = CommonQueryFactory.GetProductTableProducts(Manager);
                tableProductQuery.SetParameter("lang_pk", language);

                records = tableProductQuery.Execute();
                // Do not use foreach to avoid possible object allocation
                // ReSharper disable once ForCanBeConvertedToForeach
                for (var i = 0; i < records.Count; i++)
                {
                    var record = (IRecord)records[i];
                    var table = new ProductTableRecord
                    {
                        Id = record.ValueAs<Guid>("artb_pk"),
                        ProductAbbreviation = record.ValueAs<string>("artg_abre"),
                        ProductDescription = record.ValueAs<string>("artg_desc"),
                        ProductId = record.ValueAs<Guid>("artb_codi"),
                        Quantity = record.ValueAs<decimal>("artb_cant"),
                        MenuSeparatorId = record.ValueAs<Guid?>("TBSP_PK"),
                        MenuSeparatorOrder = record.ValueAs<short?>("tbsp_order"),
                        MenuSeparatorSelection = record.ValueAs<int?>("tbsp_mxse"),
                        MenuSeparatorDescription = record.ValueAs<string>("tbsp_desc"),
                        MenuSeparatorDescriptionId = record.ValueAs<Guid?>("TBSP_DESC_PK"),

                    };

                    var productId = record.ValueAs<Guid>("artg_pk");
                    table.TableId = productId;

                    if (!areasByProduct.TryGetValue(table.ProductId, out var areaRecordList))
                        areaRecordList = new List<AreasRecord>();
                    table.Areas = areaRecordList;
                    
                    if (!preparationsByProduct.TryGetValue(table.ProductId, out var preparationRecordList))
                        preparationRecordList = new List<PreparationRecord>();
                    table.Preparations = preparationRecordList;

                    if (!tableProductsByProduct.TryGetValue(productId, out var productTableRecordList))
                    {
                        productTableRecordList = new List<ProductTableRecord>();
                        tableProductsByProduct.Add(productId, productTableRecordList);
                    }

                    productTableRecordList.Add(table);

                    productTables[table.ProductId] = table;
                }

                #endregion

                #region Load all Tax Schema and Rates for all products

                var taxSchemasProductsQuery = QueryFactory.Get<TaxSchemasProductsQuery>(Manager);
                taxSchemasProductsQuery.SetParameter("lang_pk", language);

                records = taxSchemasProductsQuery.Execute();
                foreach (var productTaxSchemasGroup in records.GroupBy(x => x.ValueAs<Guid>("artg_pk")))
                {
                    var productId = productTaxSchemasGroup.Key;
                    if (!taxSchemasByProduct.TryGetValue(productId, out var taxSchemas))
                    {
                        taxSchemas = new List<TaxSchemaProductRecord>();
                        taxSchemasByProduct.Add(productId, taxSchemas);
                    }

                    foreach (var productTaxSchema in productTaxSchemasGroup)
                    {
                        var tax = new TaxSchemaProductRecord
                        {
                            TaxSchemaId = productTaxSchema.ValueAs<Guid>("esim_pk"),
                            Default = productTaxSchema.ValueAs<bool>("esim_defa")
                        };

                        var taxRateId1 = productTaxSchema.ValueAs<Guid?>("iva1_pk");
                        if (taxRateId1.HasValue)
                        {
                            tax.TaxRates.Add(new TaxRateProductRecord
                            {
                                TaxRateId = taxRateId1.Value,
                                TaxRateAuxCode = productTaxSchema.ValueAs<string>("iva1_coax"),
                                Percent = productTaxSchema.ValueAs<decimal>("iva1_perc"),
                                TaxRateDesc = productTaxSchema.ValueAs<string>("iva1_desc"),
                                ApplyRetention = productTaxSchema.ValueAs<bool>("iva1_rete")
                            });
                        }

                        var taxRateId2 = productTaxSchema.ValueAs<Guid?>("iva2_pk");
                        if (taxRateId2.HasValue)
                        {
                            tax.TaxRates.Add(new TaxRateProductRecord
                            {
                                TaxRateId = taxRateId2.Value,
                                TaxRateAuxCode = productTaxSchema.ValueAs<string>("iva2_coax"),
                                Percent = productTaxSchema.ValueAs<decimal>("iva2_perc"),
                                TaxRateDesc = productTaxSchema.ValueAs<string>("iva2_desc"),
                                ApplyRetention = productTaxSchema.ValueAs<bool>("iva2_rete"),
                                Mode2 = productTaxSchema.ValueAs<ApplyTax2?>("iva2_appl")
                            });
                        }

                        var taxRateId3 = productTaxSchema.ValueAs<Guid?>("iva3_pk");
                        if (taxRateId3.HasValue)
                        {
                            tax.TaxRates.Add(new TaxRateProductRecord
                            {
                                TaxRateId = taxRateId3.Value,
                                TaxRateAuxCode = productTaxSchema.ValueAs<string>("iva3_coax"),
                                Percent = productTaxSchema.ValueAs<decimal>("iva3_perc"),
                                TaxRateDesc = productTaxSchema.ValueAs<string>("iva3_desc"),
                                ApplyRetention = productTaxSchema.ValueAs<bool>("iva3_rete"),
                                Mode3 = productTaxSchema.ValueAs<ApplyTax3?>("iva3_appl")
                            });
                        }

                        taxSchemas.Add(tax);
                    }
                }

                #endregion

                #region Load all Sliders for all products

                var productSlidersQuery = QueryFactory.Get<ProductSlaidersQuery>(Manager);
                productSlidersQuery.SetParameter("ipos_pk", standId.ToHex());
                productSlidersQuery.SetParameter("hote_pk", hotelId.ToHex());

                records = productSlidersQuery.Execute();
                foreach (var productSlidersGroup in records.GroupBy(x => x.ValueAs<Guid>("artg_pk")))
                {
                    var productId = productSlidersGroup.Key;
                    if (!slidersByProduct.TryGetValue(productId, out var sliders))
                    {
                        sliders = new List<string>();
                        slidersByProduct.Add(productId, sliders);
                    }

                    foreach (var productSlider in productSlidersGroup)
                        sliders.Add(productSlider.ValueAs<string>("arim_path"));
                }

                #endregion

                #region Load accompanying products

                var accompanyingProducts = new Dictionary<Guid, List<Guid>>();
                {
                    var accompanyingProductsQuery = QueryFactory.Get<AccompanyingProductsQuery>(Manager);
                    accompanyingProductsQuery.SetParameter("IPOS_PK", standId.ToHex());

                    var accompanyingRecords = accompanyingProductsQuery.Execute();
                    foreach (var accompanyingRecord in accompanyingRecords)
                    {
                        var record = (IRecord)accompanyingRecord;
                        var parentProductId = record.ValueAs<Guid>("ARPA_PK");
                        var childProductId = record.ValueAs<Guid>("ARCH_PK");

                        if (!accompanyingProducts.TryGetValue(parentProductId, out var accompanyingProductList))
                        {
                            accompanyingProductList = [];
                            accompanyingProducts.Add(parentProductId, accompanyingProductList);
                        }
                        accompanyingProductList.Add(childProductId);
                    }
                }

                #endregion

                #region Load Products by Stand

                var query = CommonQueryFactory.GetProductsByStand(Manager);
                query.SetParameter("ipos_pk", standId.ToHex());
                query.SetParameter("lang_pk", language);
                query.SetParameter("hote_pk", hotelId.ToHex());
                query.SetParameter("autoTicket", autoToTicket ? '1' : '0');

                var defaultColor = new ARGBColor(255, 218, 173, 63, "Oro de la sierra");

                records = query.Execute();
                // Do not use foreach to avoid possible object allocation
                // ReSharper disable once ForCanBeConvertedToForeach
                for (var i = 0; i < records.Count; i++)
                {
                    var record = (IRecord)records[i];
                    var imagePath = record.ValueAs<string>("artg_imag");
                    var productRecord = new ProductRecord
                    {
                        Id = record.ValueAs<Guid>("artg_pk"),
                        HotelId = record.ValueAs<Guid>("hote_pk"),
                        ProductCode = record.ValueAs<string>("artg_codi"),
                        Abbreviation = record.ValueAs<string>("artg_abre"),
                        Description = record.ValueAs<string>("artg_desc"),
                        Step = record.ValueAs<decimal>("artg_step"),
                        GroupId = record.ValueAs<Guid>("grup_pk"),
                        GroupDescription = record.ValueAs<string>("grup_desc"),
                        FamilyId = record.ValueAs<Guid>("fami_pk"),
                        FamilyDescription = record.ValueAs<string>("fami_desc"),
                        SubFamilyId = record.ValueAs<Guid>("sfam_pk"),
                        SubFamilyDescription = record.ValueAs<string>("sfam_desc"),
                        SeparatorId = record.ValueAs<Guid?>("sepa_pk"),
                        SeparatorDescription = record.ValueAs<string>("sepa_desc"),
                        ProductTableType = record.ValueAs<TableType?>("artg_tabl"),
                        Image = base64Image && !string.IsNullOrEmpty(imagePath) ? Utils.Base64Image(imagePath) : imagePath,
                        DiscountPercent = record.ValueAs<decimal?>("artg_pdes"),
                        BarCode = record.ValueAs<string>("artg_barc"),
                        Inactive = record.ValueAs<bool>("artg_inac"),
                        IsProduct = record.ValueAs<bool>("SERV_PROD"),
                        DescIsentoIva = record.ValueAs<string>("desc_isento"),
                        CodeIsentoIva = record.ValueAs<string>("code_isento")
                    };

                    var color = record.ValueAs<int?>("artg_colo");
                    productRecord.Color = color.HasValue ? new ARGBColor(color.Value) : defaultColor;

                    productRecord.PriceType = record.ValueAs<ProductPriceType>("artg_tipr");
                    productRecord.PmsServiceId = record.ValueAs<Guid>("serv_pk");
                    productRecord.OrderInFavorites = record.ValueAs<long?>("favo_orde");

                    if (!areasByProduct.TryGetValue(productRecord.Id, out var areaRecordList))
                        areaRecordList = new List<AreasRecord>();
                    productRecord.Areas = areaRecordList;

                    if (!preparationsByProduct.TryGetValue(productRecord.Id, out var preparationRecordList))
                        preparationRecordList = new List<PreparationRecord>();
                    productRecord.Preparations = preparationRecordList.OrderBy(x => x.Description).ToList();

                    if (!tableProductsByProduct.TryGetValue(productRecord.Id, out var productTableRecordList))
                        productTableRecordList = new List<ProductTableRecord>();
                    productRecord.TableProducts = productTableRecordList;

                    if (!slidersByProduct.TryGetValue(productRecord.Id, out var productSliders))
                        productSliders = new List<string>();
                    productRecord.Sliders = productSliders;

                    if (!taxSchemasByProduct.TryGetValue(productRecord.Id, out var taxSchemas))
                        taxSchemas = new List<TaxSchemaProductRecord>();
                    productRecord.TaxSchemas = taxSchemas;

                    if (!accompanyingProducts.TryGetValue(productRecord.Id, out var accompanyingProductList))
                        accompanyingProductList = [];
                    productRecord.AccompanyingProducts = accompanyingProductList;

                    if (!results.ContainsKey(productRecord.Id))
                        results.Add(productRecord.Id, productRecord);
                }

                #endregion

                #region Load Prices by Stand

                var pricesQuery = CommonQueryFactory.GetProductPricesByStand(Manager);
                pricesQuery.SetParameter("IPOS_PK", standId.ToHex());

                records = pricesQuery.Execute();
                // Do not use foreach to avoid possible object allocation
                // ReSharper disable once ForCanBeConvertedToForeach
                for (var i = 0; i < records.Count; i++)
                {
                    var record = (IRecord)records[i];
                    var productId = record.ValueAs<Guid>("ARTG_PK");

                    if (results.TryGetValue(productId, out var productRecord))
                    {
                        productRecord.StandardPrice = record.ValueAs<decimal?>("STDR_VALO");
                        productRecord.HouseUsePrice = record.ValueAs<decimal?>("CINT_VALO");
                        productRecord.MealPlanPrice = record.ValueAs<decimal?>("PENS_VALO");
                        productRecord.AccompanyingPrice = record.ValueAs<decimal?>("ACOM_VALOR");
                    }
                }

                #endregion
            }
            finally
            {
                Manager.Close();
            }

            return results.Values.ToList();
        }

        public List<GroupRecord> GetProductGroups(Guid hotelId, Guid standId, int language)
        {
            var results = new List<GroupRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetProductGroups(Manager);
                query.Parameters["hote_pk"] = hotelId;
                query.Parameters["ipos_pk"] = standId;
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var groupRecord = new GroupRecord();
                    groupRecord.Id = record.ValueAs<Guid>("grup_pk");
                    groupRecord.Description = record.ValueAs<string>("grup_desc");
                    groupRecord.Image = record.ValueAs<string>("grup_imag");

                    results.Add(groupRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        public List<FamilyRecord> GetProductFamilies(Guid hotelId, Guid standId, int language)
        {
            var results = new List<FamilyRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetProductFamilies(Manager);
                query.Parameters["hote_pk"] = hotelId;
                query.Parameters["ipos_pk"] = standId;
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var familyRecord = new FamilyRecord();
                    familyRecord.Id = record.ValueAs<Guid>("fami_pk");
                    familyRecord.Description = record.ValueAs<string>("fami_desc");

                    results.Add(familyRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        public List<SubFamilyRecord> GetProductSubFamilies(Guid hotelId, Guid standId, int language)
        {
            var results = new List<SubFamilyRecord>();
            Manager.Open();

            try
            {
                var query = CommonQueryFactory.GetProductSubFamilies(Manager);
                query.Parameters["hote_pk"] = hotelId;
                query.Parameters["ipos_pk"] = standId;
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var subFamilyRecord = new SubFamilyRecord();

                    subFamilyRecord.Id = record.ValueAs<Guid>("sfam_pk");
                    subFamilyRecord.Description = record.ValueAs<string>("sfam_desc");

                    results.Add(subFamilyRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        public List<PreparationRecord> GetPreparationsByProducts(Guid productId, int language)
        {
            var results = new List<PreparationRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetPreparationsByProducts(Manager);
                query.SetParameter("lang_pk", language);
                query.SetFilter("artg_pk", productId);

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var preparationRecord = new PreparationRecord();
                    preparationRecord.Id = record.ValueAs<Guid>("prep_pk");
                    preparationRecord.Description = record.ValueAs<string>("prep_desc");

                    results.Add(preparationRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results.OrderBy(x => x.Description).ToList();
        }

        public List<PreparationRecord> GetPreparations(int language)
        {
            var results = new List<PreparationRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetPreparations(Manager);
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var preparationRecord = new PreparationRecord();
                    preparationRecord.Id = record.ValueAs<Guid>("prep_pk");
                    preparationRecord.Description = record.ValueAs<string>("prep_desc");

                    results.Add(preparationRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results.OrderBy(x => x.Description).ToList();
        }

        public List<AreasRecord> GetAreasByProducts(Guid standId, Guid productId, int language)
        {
            var results = new List<AreasRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetAreasByProducts(Manager);
                query.SetParameter("lang_pk", language);
                query.SetParameter("ipos_pk", standId);
                query.SetFilter("artg_pk", productId);

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var areaRecord = new AreasRecord();
                    areaRecord.Id = record.ValueAs<Guid>("area_pk");
                    areaRecord.Description = record.ValueAs<string>("area_desc");
                    areaRecord.NumberCopies = record.ValueAs<short>("area_nuco");
                    var color = record.ValueAs<int?>("area_colo");
                    if (color.HasValue)
                        areaRecord.AreaColor = new ARGBColor(color.Value);
                    areaRecord.Order = record.ValueAs<short>("arar_orde");
                    areaRecord.Mandatory = record.ValueAs<bool>("arar_arma");

                    results.Add(areaRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        public List<AreasRecord> GetAreasByStand(Guid standId, int language)
        {
            var results = new List<AreasRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetAreasByStand(Manager);
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var areaRecord = new AreasRecord();

                    areaRecord.Id = record.ValueAs<Guid>("area_pk");
                    areaRecord.Description = record.ValueAs<string>("area_desc");
                    areaRecord.NumberCopies = record.ValueAs<short>("area_nuco");
                    var color = record.ValueAs<int?>("area_colo");
                    if (color.HasValue)
                        areaRecord.AreaColor = new ARGBColor(color.Value);

                    results.Add(areaRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        public List<StandAreaRecord> GetStandAreas(int language)
        {
            var result = new List<StandAreaRecord>();

            const string sb = @"
                SELECT IPOS.IPOS_PK,
                       (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = IPOS.LITE_DESC AND MULT.LANG_PK = :LANG_PK) AS IPOS_DESC,
                       AREA.AREA_PK,
                       (SELECT MULT.MULT_DESC FROM VNHT_MULT MULT WHERE MULT.LITE_PK = AREA.LITE_PK AND MULT.LANG_PK = :LANG_PK)   AS AREA_DESC,
                       AREA.AREA_COLO,
                       AREA.AREA_NUCO
                FROM TNHT_IPOS IPOS
                         INNER JOIN TNHT_ARAR ARAR ON ARAR.IPOS_PK = IPOS.IPOS_PK
                         INNER JOIN TNHT_AREA AREA ON AREA.AREA_PK = ARAR.AREA_PK
                GROUP BY IPOS.IPOS_PK, AREA.AREA_PK, IPOS.LITE_DESC, AREA.LITE_PK, AREA.AREA_COLO, AREA.AREA_NUCO
                ORDER BY IPOS_DESC, AREA_DESC
            ";

            var query = new BaseQuery(Manager, "GetStandAreas", sb);
            query.SetParameter("lang_pk", language.ToString());
            
            foreach (var record in query.Execute().ToList())
            {
                var standAreaRecord = new StandAreaRecord
                {
                    StandId = record.ValueAs<Guid>("IPOS_PK"),
                    StandDescription = record.ValueAs<string>("IPOS_DESC"),
                    AreaId = record.ValueAs<Guid>("AREA_PK"),
                    AreaDescription = record.ValueAs<string>("AREA_DESC"),
                    NumberOfCopies = record.ValueAs<int>("AREA_NUCO")
                };

                var color = record.ValueAs<int?>("AREA_COLO");
                if (color.HasValue)
                    standAreaRecord.AreaColor = new ARGBColor(color.Value);

                result.Add(standAreaRecord);
            }

            return result;
        }

        public decimal? GetProductPrice(Guid standId, Guid productId, Guid rateId)
        {
            Manager.Open();
            try
            {
                var queryPrices = CommonQueryFactory.GetPriceByStandAndRate(Manager);
                queryPrices.Parameters["ipos_pk"] = standId;
                queryPrices.Parameters["tprg_pk"] = rateId;
                queryPrices.Filters["artg_pk"].Value = productId;

                var record = queryPrices.Execute();
                if (record.IsEmpty)
                    return record.ValueAs<decimal?>("tprl_valor");
            }
            finally
            {
                Manager.Close();
            }

            return null;
        }

        public ProductRecord GetProductByStand(Guid productId, Guid standId)
        {
            ProductRecord result = null;

            Manager.Open();
            try
            {
                var prodRecord = CommonQueryFactory.GetProductByStand(Manager)
                    .SetParameter("ARTG_PK", productId)
                    .SetParameter("IPOS_PK", standId)
                    .SetParameter("LANG_PK", sessionContext.LanguageId)
                    .Execute()
                    .FirstOrDefault();

                if (prodRecord != null)
                {
                    result = new ProductRecord
                    {
                        Id = prodRecord.ValueAs<Guid>("ARTG_PK"),
                        HotelId = prodRecord.ValueAs<Guid>("HOTE_PK"),
                        ProductCode = prodRecord.ValueAs<string>("ARTG_CODI"),
                        Abbreviation = prodRecord.ValueAs<string>("ARTG_ABRE"),
                        Description = prodRecord.ValueAs<string>("ARTG_DESC"),
                        Step = prodRecord.ValueAs<decimal>("ARTG_STEP"),
                        GroupId = prodRecord.ValueAs<Guid>("GRUP_PK"),
                        GroupDescription = prodRecord.ValueAs<string>("GRUP_DESC"),
                        FamilyId = prodRecord.ValueAs<Guid>("FAMI_PK"),
                        FamilyDescription = prodRecord.ValueAs<string>("FAMI_DESC"),
                        SubFamilyId = prodRecord.ValueAs<Guid>("SFAM_PK"),
                        SubFamilyDescription = prodRecord.ValueAs<string>("SFAM_DESC"),
                        SeparatorId = prodRecord.ValueAs<Guid?>("SEPA_PK"),
                        SeparatorDescription = prodRecord.ValueAs<string>("SEPA_DESC"),
                        Inactive = prodRecord.ValueAs<bool>("ARTG_INAC"),
                        ProductTableType = prodRecord.ValueAs<TableType?>("ARTG_TABL"),
                        Image = prodRecord.ValueAs<string>("ARTG_IMAG"),
                        DiscountPercent = prodRecord.ValueAs<decimal?>("ARTG_PDES"),
                        Color = prodRecord.ValueAs<int?>("ARTG_COLO").HasValue
                            ? new ARGBColor(prodRecord.ValueAs<int>("ARTG_COLO"))
                            : new ARGBColor(255, 218, 173, 63, "Oro de la sierra"),
                        PriceType = prodRecord.ValueAs<ProductPriceType>("ARTG_TIPR"),
                        PmsServiceId = prodRecord.ValueAs<Guid>("SERV_PK"),
                        OrderInFavorites = prodRecord.ValueAs<long?>("FAVO_ORDE"),
                        Areas = CommonQueryFactory.GetProductAreasByStand(Manager)
                            .SetParameter("ARTG_PK", productId)
                            .SetParameter("IPOS_PK", standId)
                            .SetParameter("LANG_PK", sessionContext.LanguageId)
                            .Execute().ToList()
                            .ConvertAll(
                                areaRecord => new AreasRecord
                                {
                                    Id = areaRecord.ValueAs<Guid>("AREA_PK"),
                                    AreaColor = areaRecord.IsNull("AREA_COLO")
                                        ? ARGBColor.AntiqueWhite
                                        : new ARGBColor(areaRecord.ValueAs<int>("AREA_COLO")),
                                    Description = areaRecord.ValueAs<string>("AREA_DESC"),
                                    NumberCopies = areaRecord.ValueAs<short>("AREA_NUCO"),
                                    Order = areaRecord.ValueAs<short>("ARAR_ORDE")
                                }),
                        Preparations = CommonQueryFactory.GetProductPreparations(Manager)
                            .SetParameter("ARTG_PK", productId)
                            .SetParameter("LANG_PK", sessionContext.LanguageId)
                            .Execute().ToList().ConvertAll(prepRecord => new PreparationRecord
                            {
                                Id = prepRecord.ValueAs<Guid>("PREP_PK"),
                                Description = prepRecord.ValueAs<string>("PREP_DESC")
                            }),
                        TableProducts = CommonQueryFactory.GetProductTableProducts(Manager)
                            .SetParameter("LANG_PK", sessionContext.LanguageId)
                            .SetFilter("ARTG_PK", productId)
                            .Execute().ToList().ConvertAll(
                                tableProdRecord =>
                                {
                                    var tableProd = new ProductTableRecord
                                    {
                                        Id = tableProdRecord.ValueAs<Guid>("ARTB_PK"),
                                        ProductAbbreviation = tableProdRecord.ValueAs<string>("ARTG_ABRE"),
                                        ProductDescription = tableProdRecord.ValueAs<string>("ARTG_DESC"),
                                        ProductId = tableProdRecord.ValueAs<Guid>("ARTB_CODI"),
                                        Quantity = tableProdRecord.ValueAs<decimal>("ARTB_CANT"),
                                        MenuSeparatorId = tableProdRecord.ValueAs<Guid?>("TBSP_PK"),
                                        MenuSeparatorOrder = tableProdRecord.ValueAs<short?>("TBSP_ORDER"),
                                        MenuSeparatorSelection = tableProdRecord.ValueAs<int?>("TBSP_MXSE"),
                                        MenuSeparatorDescription = tableProdRecord.ValueAs<string>("TBSP_DESC"),
                                        MenuSeparatorDescriptionId = tableProdRecord.ValueAs<Guid?>("TBSP_DESC_PK"),
                                        TableId = tableProdRecord.ValueAs<Guid>("ARTG_PK"),
                                        Preparations = CommonQueryFactory.GetProductPreparations(Manager)
                                            .SetParameter("ARTG_PK", tableProdRecord.ValueAs<Guid>("ARTB_PK"))
                                            .SetParameter("LANG_PK", sessionContext.LanguageId)
                                            .Execute().ToList().ConvertAll(prepRecord => new PreparationRecord
                                            {
                                                Id = prepRecord.ValueAs<Guid>("PREP_PK"),
                                                Description = prepRecord.ValueAs<string>("PREP_DESC")
                                            })
                                    };

                                    var pricesRecord = CommonQueryFactory.GetProductPricesByStand(Manager)
                                        .SetParameter("IPOS_PK", standId)
                                        .SetParameter("LANG_PK", sessionContext.LanguageId)
                                        .Execute()
                                        .FirstOrDefault(record => record.ValueAs<Guid>("ARTG_PK") == tableProd.Id);

                                    tableProd.ProductStandardPrice = pricesRecord.ValueAs<decimal>("STDR_VALO");
                                    tableProd.ProductHouseUsePrice = pricesRecord.ValueAs<decimal>("CINT_VALO");
                                    tableProd.ProductMealPlanPrice = pricesRecord.ValueAs<decimal>("PENS_VALO");

                                    return tableProd;
                                }),
                        TaxSchemas = CommonQueryFactory.GetProductTaxSchemas(Manager)
                            .SetParameter("ARTG_PK", productId)
                            .SetParameter("LANG_PK", sessionContext.LanguageId)
                            .Execute().ToList().ConvertAll(
                                taxSchemaRecord =>
                                {
                                    var tax = new TaxSchemaProductRecord
                                    {
                                        TaxSchemaId = taxSchemaRecord.ValueAs<Guid>("ESIM_PK"),
                                        Default = taxSchemaRecord.ValueAs<bool>("ESIM_DEFA")
                                    };

                                    var taxRateId1 = taxSchemaRecord.ValueAs<Guid?>("IVA1_PK");
                                    if (taxRateId1.HasValue)
                                    {
                                        tax.TaxRates.Add(new TaxRateProductRecord
                                        {
                                            TaxRateId = taxRateId1.Value,
                                            TaxRateAuxCode = taxSchemaRecord.ValueAs<string>("IVA1_COAX"),
                                            Percent = taxSchemaRecord.ValueAs<decimal>("IVA1_PERC"),
                                            TaxRateDesc = taxSchemaRecord.ValueAs<string>("IVA1_DESC"),
                                            ApplyRetention = taxSchemaRecord.ValueAs<bool>("IVA1_RETE")
                                        });
                                    }

                                    var taxRateId2 = taxSchemaRecord.ValueAs<Guid?>("IVA2_PK");
                                    if (taxRateId2.HasValue)
                                    {
                                        tax.TaxRates.Add(new TaxRateProductRecord
                                        {
                                            TaxRateId = taxRateId2.Value,
                                            TaxRateAuxCode = taxSchemaRecord.ValueAs<string>("IVA2_COAX"),
                                            Percent = taxSchemaRecord.ValueAs<decimal>("IVA2_PERC"),
                                            TaxRateDesc = taxSchemaRecord.ValueAs<string>("IVA2_DESC"),
                                            ApplyRetention = taxSchemaRecord.ValueAs<bool>("IVA2_RETE"),
                                            Mode2 = taxSchemaRecord.ValueAs<ApplyTax2?>("IVA2_APPL")
                                        });
                                    }

                                    var taxRateId3 = taxSchemaRecord.ValueAs<Guid?>("IVA3_PK");
                                    if (taxRateId3.HasValue)
                                    {
                                        tax.TaxRates.Add(new TaxRateProductRecord
                                        {
                                            TaxRateId = taxRateId3.Value,
                                            TaxRateAuxCode = taxSchemaRecord.ValueAs<string>("IVA3_COAX"),
                                            Percent = taxSchemaRecord.ValueAs<decimal>("IVA3_PERC"),
                                            TaxRateDesc = taxSchemaRecord.ValueAs<string>("IVA3_DESC"),
                                            ApplyRetention = taxSchemaRecord.ValueAs<bool>("IVA3_RETE"),
                                            Mode3 = taxSchemaRecord.ValueAs<ApplyTax3?>("IVA3_APPL")
                                        });
                                    }

                                    return tax;
                                })
                    };

                    #region Prices

                    var pricesRecord = CommonQueryFactory.GetProductPricesByStand(Manager)
                        .SetParameter("IPOS_PK", standId)
                        .Execute()
                        .FirstOrDefault(record => record.ValueAs<Guid>("ARTG_PK") == productId);

                    if (pricesRecord != null)
                    {
                        result.StandardPrice = pricesRecord.ValueAs<decimal>("STDR_VALO");
                        result.HouseUsePrice = pricesRecord.ValueAs<decimal>("CINT_VALO");
                        result.MealPlanPrice = pricesRecord.ValueAs<decimal>("PENS_VALO");
                    }

                    #endregion
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationItemResult<ImageRecord> GetImage(string path)
        {
            var result = new ValidationItemResult<ImageRecord>();

            try
            {
                if (!File.Exists(path))
                    path = Utils.ImagePaths[path];

                using var fs = new FileStream(path, FileMode.Open, FileAccess.Read);
                var length = (int)fs.Length;
                var buffer = new byte[length];
                fs.Read(buffer, 0, length);

                var record = new ImageRecord();
                var fileTimestamp = File.GetLastWriteTimeUtc(path).ToFileTimeUtc();
                record.Id = path;
                record.Timestamp = fileTimestamp;
                record.Image = Convert.ToBase64String(buffer, Base64FormattingOptions.None);

                result.Item = record;
                return result;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                result.AddError(string.Format("Error reading image from path {0}", path));
                return result;
            }
        }

        public ValidationItemSource<ProductPriceRecord> GetProductPrices(Guid standId, Guid rateId)
        {
            var result = new ValidationItemSource<ProductPriceRecord>();
            result.ItemSource = new List<ProductPriceRecord>();

            Manager.Open();
            try
            {
                var queryPrices = CommonQueryFactory.GetPriceByStandAndRate(Manager);
                queryPrices.Parameters["ipos_pk"] = standId;
                queryPrices.Parameters["tprg_pk"] = rateId;

                var pricesRecords = queryPrices.Execute();
                foreach (IRecord record in pricesRecords)
                {
                    var productPrice = new ProductPriceRecord
                    {
                        Price = record.ValueAs<decimal?>("tprl_valor"),
                        ProductId = record.ValueAs<Guid>("artg_pk"),
                        RateId = rateId
                    };

                    result.ItemSource.Add(productPrice);
                }
            }
            finally
            {
                Manager.Close();
            }

            return result;
        }

        public ValidationContractResult CalculateTaxRates(Guid productId,
            string country, decimal value, Guid schemaId, Guid hotelId, bool included)
        {
            var tax = GetTaxRuleByProduct(hotelId, schemaId, productId);
            if (tax == null)
            {
                // Lets assume that the reason there is no tax rules is because the sync is not finished updating yet
                for (int retry = 0; retry < 3 && tax == null; retry++)
                {
                    System.Threading.Thread.Sleep(1000);
                    tax = GetTaxRuleByProduct(hotelId, schemaId, productId);
                }
			}

            if (tax != null)
            {
                var taxCalculation = new TaxCalculation(country, tax.TaxRateId1, tax.Percent1,
                    tax.TaxRateId2, tax.Percent2, tax.Mode2, tax.TaxRateId3, tax.Percent3, tax.Mode3)
                {
                    RoundMode = RoundMode.Close,
                    Decimals = 8
                };

                if (included)
                    taxCalculation.DeduceTax(value);
                else
                    taxCalculation.AddTax(value);

                var contract = new TaxContract(schemaId, included, value)
                {
                    NetValue = taxCalculation.NetValue,
                    GrossValue = taxCalculation.GrossValue
                };

                #region Tax 1 Definition

                if (taxCalculation.TaxRateId1.HasValue)
                {
                    contract.Taxes.Add(
                        new MovementTaxDetailContract(taxCalculation.TaxRateId1.Value, taxCalculation.Percent1 ?? 0, 0)
                        {
                            TaxAuxCode = tax.TaxRateAuxCode1,
                            TaxRateDescription = tax.TaxRateDesc1,
                            TaxValue = taxCalculation.TaxValue1 ?? decimal.Zero,
                            TaxBase = taxCalculation.TaxBase1 ?? decimal.Zero,
                            TaxIncidence = taxCalculation.TaxIncidence1 ?? decimal.Zero,
                        });
                }

                #endregion

                #region Tax 2 Definition

                if (taxCalculation.TaxRateId2.HasValue)
                {
                    contract.Taxes.Add(
                        new MovementTaxDetailContract(taxCalculation.TaxRateId2.Value, taxCalculation.Percent2 ?? 0, 1)
                        {
                            TaxAuxCode = tax.TaxRateAuxCode2,
                            TaxRateDescription = tax.TaxRateDesc2,
                            TaxValue = taxCalculation.TaxValue2 ?? decimal.Zero,
                            TaxBase = taxCalculation.TaxBase2 ?? decimal.Zero,
                            TaxIncidence = taxCalculation.TaxIncidence2 ?? decimal.Zero,
                        });
                }

                #endregion

                #region Tax 3 Definition

                if (taxCalculation.TaxRateId3.HasValue)
                {
                    contract.Taxes.Add(
                        new MovementTaxDetailContract(taxCalculation.TaxRateId3.Value, taxCalculation.Percent3 ?? 0, 2)
                        {
                            TaxAuxCode = tax.TaxRateAuxCode3,
                            TaxRateDescription = tax.TaxRateDesc3,
                            TaxValue = taxCalculation.TaxValue3 ?? decimal.Zero,
                            TaxBase = taxCalculation.TaxBase3 ?? decimal.Zero,
                            TaxIncidence = taxCalculation.TaxIncidence3 ?? decimal.Zero,
                        });
                }

                #endregion

                return new ValidationContractResult(contract);
            }

            var result = new ValidationContractResult();
            result.AddError(Res.TaxRulesNotDefined, "Tax rules by schema not defined");

            return result;
        }

        private ServiceTaxContract GetTaxRuleByProduct(Guid hotelId, Guid schemaId, Guid productId)
        {
            var query = QueryFactory.Get<TaxesByProductQuery>(Manager);
            query.Parameters[TaxesByProductQuery.HotelIdParam] = hotelId;
            query.Filters[TaxesByProductQuery.TaxSchemaIdFilter].Value = schemaId;
            query.Filters[TaxesByProductQuery.ProductIdFilter].Value = productId;
            query.Filters[TaxesByProductQuery.ProductIdFilter].Value = productId;
            query.Parameters[TaxesByProductQuery.LanguageIdParam] = sessionContext.LanguageId;

            var list = query.First();
            if (list.Count > 0)
            {
                var contract = new ServiceTaxContract
                {
                    TaxSchemaId = list.ValueAs<Guid?>("esim_pk"),
                    TaxSchemaName = list.ValueAs<string>("esim_desc"),
                    ServiceId = list.ValueAs<Guid>("artg_pk"),
                    TaxRateId1 = list.ValueAs<Guid?>("tiva_cod1"),
                    TaxRateAuxCode1 = list.ValueAs<string>("tiva_coax1"),
                    TaxRateDesc1 = list.ValueAs<string>("tiva_desc1"),
                    Percent1 = list.ValueAs<decimal?>("tiva_perc1"), 
                    TaxRateId2 = list.ValueAs<Guid?>("tiva_cod2"),
                    TaxRateAuxCode2 = list.ValueAs<string>("tiva_coax2"),
                    TaxRateDesc2 = list.ValueAs<string>("tiva_desc2"),
                    Percent2 = list.ValueAs<decimal?>("tiva_perc2"),
                    TaxRateId3 = list.ValueAs<Guid?>("tiva_cod3"),
                    TaxRateAuxCode3 = list.ValueAs<string>("tiva_coax3"),
                    TaxRateDesc3 = list.ValueAs<string>("tiva_desc3"),
                    Percent3 = list.ValueAs<decimal?>("tiva_perc3"),
                    Mode2 = list.ValueAs<ApplyTax2?>("tiva_apl2"),
                    Mode3 = list.ValueAs<ApplyTax3?>("tiva_apl3")
                };

                return contract;
            }

            return null;
        }

        #endregion
    }

    public static class ProductBusinessEx
    {
		public static ProductStockRecord GetLocalStockRecord(this IDatabaseManager manager, Guid productId, DateTime workDate)
		{
			return CommonQueryFactory
				.GetProductStockQuery(manager)
				.SetFilter("artg_pk", productId)
				.SetFilter("arst_datr", workDate)
				.ExecuteList<ProductStockRecord>()
				.FirstOrDefault()
				;
		}

        public static bool CanConsume(this ProductStockRecord stockRecord, decimal quantity)
        {
            if (stockRecord is null)
                return true;
            return (stockRecord.Quantity - stockRecord.Used) >= quantity;
        }
	}
}
