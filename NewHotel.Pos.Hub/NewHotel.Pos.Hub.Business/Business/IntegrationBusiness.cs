﻿using NewHotel.Business;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.DataAnnotations;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Business.PersistentObjects;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries;
using NewHotel.Pos.Hub.Contracts.Common.Records.Integrations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class IntegrationBusiness : BaseBusiness, IIntegrationBusiness
    {

        public IntegrationBusiness(
            IDatabaseManager manager
        ) : base(manager)
        {
        }

        #region ProductIntegrationReference

        public ValidationResult<List<ProductIntegrationReferenceRecord>> GetProductIntegrationReferenceRecords()
        {
            using (Manager.OpenSession())
            {
                var result = new ValidationResult<List<ProductIntegrationReferenceRecord>>();
                try
                {
                    var query = CommonQueryFactory.GetProductIntegrationReference(Manager);
                    var records = query.Execute();
                    var productIntegrationReferences = records.Select(record => new ProductIntegrationReferenceRecord
                    {
                        Id = records.FirstOrDefault().ValueAs<Guid>("arin_pk"),
                        ProductId = records.FirstOrDefault().ValueAs<Guid>("arst_pk"),
                        IntegrationId = records.FirstOrDefault().ValueAs<Guid>("inte_pk"),
                        ProductCode = records.FirstOrDefault().ValueAs<string>("arin_code"),
                        OptionalCodeOne = records.FirstOrDefault().ValueAs<string>("arin_opcd_one"),
                        OptionalCodeTwo = records.FirstOrDefault().ValueAs<string>("arin_opcd_two")
                    }).ToList();
                    result.Result = productIntegrationReferences;
                }
                catch (Exception ex)
                {
                    var message = "Error trying to get ProductIntegrationReferences from the database";
                    result.AddError(message, ex);
                }
                return result;
            }
        }
        
        public ValidationResult<ProductIntegrationReferenceRecord> GetProductIntegrationReferenceById(Guid id)
        {
            using (Manager.OpenSession())
            {
                var result = new ValidationResult<ProductIntegrationReferenceRecord>();
                try
                {
                    var query = CommonQueryFactory.GetProductIntegrationReferenceById(Manager);
                    query.Parameters["arin_pk"] = id;

                    var records = query.Execute();
                    if (records == null)
                        result.AddError("ProductIntegrationReference not found");
                    else if(records.Count() > 1)
                        result.AddError("More than one ProductIntegrationReference found");
                    else
                    {
                        var productIntegrationReference = new ProductIntegrationReferenceRecord
                        {
                            Id = records.FirstOrDefault().ValueAs<Guid>("arin_pk"),
                            ProductId = records.FirstOrDefault().ValueAs<Guid>("arst_pk"),
                            IntegrationId = records.FirstOrDefault().ValueAs<Guid>("inte_pk"),
                            ProductCode = records.FirstOrDefault().ValueAs<string>("arin_code"),
                            OptionalCodeOne = records.FirstOrDefault().ValueAs<string>("arin_opcd_one"),
                            OptionalCodeTwo = records.FirstOrDefault().ValueAs<string>("arin_opcd_two")
                        };
                        result.Result = productIntegrationReference;
                    }
                }
                catch (Exception ex)
                {
                    var message = "Error trying to get a ProductIntegrationReference from the database";
                    result.AddError(message, ex);
                }
                return result;
            }
        }

        public ValidationResult<ProductIntegrationReferenceRecord> GetProductIntegrationReferenceByIntegrationIdAndProductCode(Guid integrationId, string productCode)
        {
            using(Manager.OpenSession())
            {
                var result = new ValidationResult<ProductIntegrationReferenceRecord>();
                try
                {
                    var query = CommonQueryFactory.GetProductIntegrationReferenceByIntegrationIdAndProductCode(Manager);
                    query.Parameters["inte_pk"] = integrationId;
                    query.Parameters["arin_code"] = productCode;

                    var records = query.Execute();
                    if (records == null)
                        result.AddError("ProductIntegrationReference not found");
                    else if (records.Count() > 1)
                        result.AddError("More than one ProductIntegrationReference found");
                    else
                    {
                        var productIntegrationReference = new ProductIntegrationReferenceRecord
                        {
                            Id = records.FirstOrDefault().ValueAs<Guid>("arin_pk"),
                            ProductId = records.FirstOrDefault().ValueAs<Guid>("artg_pk"),
                            IntegrationId = records.FirstOrDefault().ValueAs<Guid>("inte_pk"),
                            ProductCode = records.FirstOrDefault().ValueAs<string>("arin_code"),
                            OptionalCodeOne = records.FirstOrDefault().ValueAs<string>("arin_opcd_one"),
                            OptionalCodeTwo = records.FirstOrDefault().ValueAs<string>("arin_opcd_two")
                        };
                        result.Result = productIntegrationReference;
                    }
                }
                catch (Exception ex)
                {
                    var message = "Error trying to get a ProductIntegrationReference from the database";
                    result.AddError(message, ex);
                }
                return result;
            }
        }

        public ValidationResult<ProductIntegrationReferenceRecord> GetProductIntegrationReferenceByProductIdAndIntegrationId(Guid productId, Guid integrationId)
        {
            using (Manager.OpenSession())
            {
                var result = new ValidationResult<ProductIntegrationReferenceRecord>();
                try
                {
                    var query = CommonQueryFactory.GetProductIntegrationReferenceById(Manager);
                    query.Parameters["artg_pk"] = productId;
                    query.Parameters["inte_pk"] = integrationId;

                    var records = query.Execute();
                    if (records == null || records.Count == 0)
                        result.AddError("ProductIntegrationReference not found");
                    else if(records.Count() > 1)
                        result.AddError("More than one ProductIntegrationReference found");
                    else
                    { 
                        var productIntegrationReference = new ProductIntegrationReferenceRecord
                        {
                            Id = records.FirstOrDefault().ValueAs<Guid>("arin_pk"),
                            ProductId = records.FirstOrDefault().ValueAs<Guid>("artg_pk"),
                            IntegrationId = records.FirstOrDefault().ValueAs<Guid>("inte_pk"),
                            ProductCode = records.FirstOrDefault().ValueAs<string>("arin_code"),
                            OptionalCodeOne = records.FirstOrDefault().ValueAs<string>("arin_opcd_one"),
                            OptionalCodeTwo = records.FirstOrDefault().ValueAs<string>("arin_opcd_two")
                        };
                        result.Result = productIntegrationReference;
                    }
                }
                catch (Exception ex)
                {
                    var message = "Error trying to get a ProductIntegrationReference from the database";
                    result.AddError(message, ex);
                }
                return result;
            }
        }

        /// <summary>
        /// This method creates a new ProductIntegrationReference record in the database
        /// And it's just for testing purposes the official method is through the backoffice, which is then synced from the Sync Project 
        /// </summary>
        /// <param name="integrationId">The id of the integration from the table TNHT_INTE</param>
        /// <param name="productCode">The code of the product that it's in the external system</param>
        /// <param name="productId">The id of the product from the table TNHT_ARTG</param>
        /// <returns>A new product integration record if everything went well otherwise will return with error(s)</returns>
        public ValidationResult<ProductIntegrationReferenceRecord> CreateProductIntegrationReference(Guid integrationId, string productCode, Guid productId)
        {
            using(Manager.OpenSession())
            {
                var result = new ValidationResult<ProductIntegrationReferenceRecord>();
                Manager.BeginTransaction();
                try
                {
                    var productIntegrationReference = new ProductIntegrationReference(Manager)
                    {
                        Id = Guid.NewGuid(),
                        ProductId = productId,
                        IntegrationId = integrationId,
                        ProductCode = productCode,
                        LastModified = DateTime.UtcNow
                    };
                    productIntegrationReference.SaveObject();
                    
                    result.Result = new ProductIntegrationReferenceRecord
                    {
                        Id = (Guid)productIntegrationReference.Id,
                        ProductId = productIntegrationReference.ProductId,
                        IntegrationId = productIntegrationReference.IntegrationId,
                        ProductCode = productIntegrationReference.ProductCode,
                    };
                    Manager.CommitTransaction();
                }
                catch(Exception ex)
                {
                    Manager.RollbackTransaction();
                    var message = "Error trying to save a new ProductIntegrationReference to the database";
                    result.AddError(message, ex);
                }
                
                return result;
            }
        }

        #endregion

        #region Integration

        /// <summary>
        /// Deserializes the config found in the given record and returns a MultiConfig object.
        /// Do note that the MultiConfig object can hold multiple configs in it's properties but only one of them must hold values at any given time 
        /// </summary>
        /// <param name="record">The IntegrationRecord that contains the config we want to deserialize </param>
        /// <returns>A MultiConfig object with the property holding values, otherwise it will return an error in the ValidationResult object</returns>
        public ValidationResult<MultiConfig> DeserializeConfig(IntegrationRecord record)
        {
            var result = new ValidationResult<MultiConfig>();
            try
            {
                var config = JsonSerialization.FromJson<MultiConfig>(record.Config);
                // TODO: Refactor the validation to take in to account which configs need to be null or not
                if (config != null)
                    result.Result = config;
                else if (config.SettingMxm == null)
                    result.AddError("The Mxm config null is please check if the record's config SettingMxm property holds any value");
                else
                    result.AddError("The config was null please check if the record's config holds any value");
            }
            catch (Exception ex)
            {
                var message = "Error trying to deserialize the config of the Integration";
                result.AddError(message, ex);
            }
            return result;
        }

        public ValidationResult<IntegrationRecord> GetIntegrationRecordById(Guid id)
        {
            using (Manager.OpenSession())
            {
                var result = new ValidationResult<IntegrationRecord>();
                try
                {
                    var query = CommonQueryFactory.GetProductIntegrationReferenceById(Manager);
                    query.Parameters["inte_pk"] = id;

                    var records = query.Execute();
                    if (records == null)
                        result.AddError("IntegrationRecord not found");
                    else if(records.Count() > 1)
                        result.AddError("More than one IntegrationRecord found");
                    else
                    {
                        var integration = new IntegrationRecord
                        {
                            Id = records.FirstOrDefault().ValueAs<Guid>("inte_pk"),
                            Name = records.FirstOrDefault().ValueAs<string>("inte_name"),
                            Type = records.FirstOrDefault().ValueAs<string>("inte_type"),
                            Config = records.FirstOrDefault().ValueAs<string>("inte_config"),
                            Active = records.FirstOrDefault().ValueAs<bool>("inte_acti"),
                        };
                        result.Result = integration;
                    }
                }
                catch (Exception ex)
                {
                    var message = "Error trying to get a IntegrationRecord from the database";
                    result.AddError(message, ex);
                }
                return result;
            }
        }

        /// <summary>
        /// Gets the active integration by the given type. It searches the table "TNHT_INTE" for a record with the given
        /// type ("INTE_TYPE") and if it has a 1 in the column "INTE_ACTI"  
        /// </summary>
        /// <param name="type">The type that we are trying to search for</param>
        /// <returns>A record if it finds it with the given conditions otherwise returns an error in the ValidationResult object</returns>
        public ValidationResult<IntegrationRecord> GetActiveIntegrationByType(IntegrationType type)
        {
            using (Manager.OpenSession())
            {
                var result = new ValidationResult<IntegrationRecord>();
                var val = (int)type;
                try
                {
                    var query = CommonQueryFactory.GetIntegration(Manager);
                    query.Parameters["inte_type"] = val.ToString();

                    var records = query.Execute();
                    if (records == null)
                        result.AddError($"Integrations not found for the type: {type}");
                    else if (
                        records.Where(
                            r => r.ValueAs<bool>("inte_acti") == true &&
                            r.ValueAs<string>("inte_type") == val.ToString()
                        ).Count() > 1
                    )
                        result.AddError($"More than one active Integration found for the type: {type}");
                    else
                    {
                        var record = records.FirstOrDefault(
                            r => r.ValueAs<bool>("inte_acti") == true &&
                            r.ValueAs<string>("inte_type") == val.ToString()
                        );
                        var integration = new IntegrationRecord
                        {
                            Id = record.ValueAs<Guid>("inte_pk"),
                            Name = record.ValueAs<string>("inte_name"),
                            Type = record.ValueAs<string>("inte_type"),
                            Config = record.ValueAs<Clob>("inte_config"),
                            Active = record.ValueAs<bool>("inte_acti"),
                        };
                        result.Result = integration;
                    }
                }
                catch (Exception ex)
                {
                    var message = "Error trying to get a IntegrationRecord from the database";
                    result.AddError(message, ex);
                }
                return result;
            }
        }
        
        public ValidationResult<IntegrationRecord> GetIntegrationByTypeAndName(IntegrationType type, string name)
        {
            var result = new ValidationResult<IntegrationRecord>();
            var val = (int)type;
            try
            {
                using (Manager.OpenSession())
                {
                    var query = CommonQueryFactory.GetIntegration(Manager);
                    query.Parameters["inte_type"] = val.ToString();
                    query.Parameters["inte_name"] = name;

                    var records = query.Execute();
                    if (records == null)
                        result.AddError($"Integrations not found for the type: {type}, with name: {name}");
                    else if (
                        records.Where(
                            r => r.ValueAs<bool>("inte_acti") == true &&
                            r.ValueAs<string>("inte_type") == val.ToString()
                        ).Count() > 1
                    )
                        result.AddError($"More than one active Integration found for the type: {type}");
                    else
                    {
                        var integration = new IntegrationRecord
                        {
                            Id = records.FirstOrDefault().ValueAs<Guid>("inte_pk"),
                            Name = records.FirstOrDefault().ValueAs<string>("inte_name"),
                            Type = records.FirstOrDefault().ValueAs<string>("inte_type"),
                            Config = records.FirstOrDefault().ValueAs<Clob>("inte_config"),
                            Active = records.FirstOrDefault().ValueAs<bool>("inte_acti"),
                        };
                        result.Result = integration;
                    }
                }
            }
            catch (Exception ex)
            {
                var message = $"Error trying to get a IntegrationRecord from the database with parameter values: type {type} and name: {name}";
                result.AddError(message, ex);
            }
            return result;
        }
         
        public ValidationResult<IntegrationRecord> CreateIntegration(string name, IntegrationType type, object config)
        {
            using(Manager.OpenSession())
            {
                var val = (int)type;
                var result = new ValidationResult<IntegrationRecord>();
                Manager.BeginTransaction();
                try
                {
                    var configJson = JsonSerialization.ToJson(config);
                    var integration = new IntegrationModel(Manager)
                    {
                        Id = Guid.NewGuid(),
                        Name = name,
                        Type = val.ToString(),
                        Config = configJson,
                        Active = false,
                        LastModification = DateTime.UtcNow
                    };
                    integration.SaveObject();
                    
                    result.Result = new IntegrationRecord
                    {
                        Id = (Guid)integration.Id,
                        Name = integration.Name,
                        Type = integration.Type,
                        Config = integration.Config,
                        Active = integration.Active,
                    };
                    Manager.CommitTransaction();
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    var message = $"Error trying to save a new Integration to the database with param values: name {name}, type {type}";
                    result.AddError(message, ex);
                }
                return result;
            }
        }

        #endregion
    }
}
