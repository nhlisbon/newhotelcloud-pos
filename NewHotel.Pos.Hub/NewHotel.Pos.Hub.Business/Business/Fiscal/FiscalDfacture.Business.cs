﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.ServiceModel;
using System.Diagnostics;
using NewHotel.Core;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;
using NewHotel.Business;
using NewHotel.Fiscal.Dfacture;
using NewHotel.Pos.Hub.Business.PersistentObjects;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Hub.Model.Session;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class FiscalDfactureBusiness21 : FiscalBaseBusiness, IFiscalBusiness
    {
        #region Constants

        private const string IGVTaxed = "10";
        private const string IGVFree = "16";
        private const string IGVExonerated = "20";
        private const string IGVInactive = "30";
        private const string IGVExport = "40";

        private const string yes = "SI";
        private const string no = "NO";
        private const string unitOfMeasure = "NIU";
        private const string CashPaymentMethod = "Contado";
        private const string OtherPaymentCode = "999";
        private const string InternalCode = "0101";
        private const string ExportCode = "0202";
        private const string InvoiceCode = "01";
        private const string CreditNoteCode = "07";

        private const int ResponseDocSucceed = 0;
        private const int ResponseDocInProcess = 95;
        private const int ResponseDocAlreadyCanceled = 229;

        #endregion
        #region Members

        private static readonly string _documentHeadersCommandText;
        private static readonly string _documentDetailsCommandText;
        private static readonly HashSet<string> _creditNoteReasonCodes;

		#endregion
		#region Constructors

		static FiscalDfactureBusiness21()
        {
            _creditNoteReasonCodes = new HashSet<string>();
            _creditNoteReasonCodes.Add("01");
            _creditNoteReasonCodes.Add("02");
            _creditNoteReasonCodes.Add("03");
            _creditNoteReasonCodes.Add("04");
            _creditNoteReasonCodes.Add("05");
            _creditNoteReasonCodes.Add("06");
            _creditNoteReasonCodes.Add("07");
            _creditNoteReasonCodes.Add("08");
            _creditNoteReasonCodes.Add("09");
            _creditNoteReasonCodes.Add("10");
            _creditNoteReasonCodes.Add("11");
            _creditNoteReasonCodes.Add("12");
            _creditNoteReasonCodes.Add("13");

            StringBuilder sb;

            sb = new StringBuilder();
            sb.AppendLine("select vend.vend_pk as docf_pk, vend.fact_seri as docf_sedo, vend.fact_codi as docf_codi,");
            sb.AppendLine("       vend.fact_nuco as docf_nuco, vend.fact_tidf as docf_tidf, vend.fact_tota as docf_valo,");
            sb.AppendLine("       vend.vend_datf as docf_daem, vend.vend_horf as docf_hoem, vend.unmo_pk as docf_unmo,");
            sb.AppendLine("       clie.fisc_dist as docf_dist, vend.fact_naci as docf_naci, vend.vend_obse as docf_obse,");
            sb.AppendLine("       case when clie.clie_type = 211 then :true else :false end as docf_faen, vend.fact_guest as docf_titu,");
            sb.AppendLine("       vend.fact_addr as docf_addr, clie.fisc_loca as docf_loca, clie.fisc_copo as docf_copo,");
            sb.AppendLine("       case when vend.fact_mail is null then clie.email_addr else vend.fact_mail end as docf_mail,");
            sb.AppendLine("       case when clie.home_phone is null then clie.cell_phone else clie.home_phone end as docf_telf,");
            sb.AppendLine("       case when vend.vend_anul = :true then 7 when vend.fact_tido = 11 then 1 when vend.fact_tido = 26 then 3 else 0 end as docf_tido,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = mtco.lite_pk and mult.lang_pk = :lang_pk) as mtco_desc,");
            sb.AppendLine("       vend.ncre_seri as ncre_sedo, vend.ncre_codi, mtco.mtco_caux,");
            sb.AppendLine("       vend.anul_daan as ncre_daem, vend.anul_dare as ncre_hoem,");
            sb.AppendLine("       (select case when min(fore.fore_caux) = max(fore.fore_caux) then min(fore.fore_caux) end");
            sb.AppendLine("        from tnht_pave pave inner join tnht_fore fore on fore.fore_pk = pave.fore_pk");
            sb.AppendLine("        where pave.vend_pk = vend.vend_pk");
            sb.AppendLine("       ) as docf_fore,");
            sb.AppendLine("       case when not exists (");
            sb.AppendLine("         select 1 from tnht_pave pave where pave.vend_pk = vend.vend_pk");
            sb.AppendLine("       ) then :true else :false end as docf_free");
            sb.AppendLine("from tnht_vend vend");
            sb.AppendLine("     left join tnht_clie clie on clie.clie_pk = vend.clie_pk");
            sb.AppendLine("     left join tnht_mtco mtco on mtco.mtco_pk = vend.mtco_pk");
            sb.AppendLine("where vend.vend_pk = :vend_pk");
            _documentHeadersCommandText = sb.ToString();

            sb = new StringBuilder();
            sb.AppendLine("select artg.artg_coax as movi_code,");
            sb.AppendLine("       (select mult.mult_desc from vnht_mult mult where mult.lite_pk = artg.lite_desc and mult.lang_pk = :lang_pk_es) as movi_desc,");
            sb.AppendLine("       arve.arve_tipl as movi_tipl, trunc(arve.arve_qtds, 3) as movi_cant,");
            sb.AppendLine("       round(arve.arve_vliq, 2) as movi_vliq, round(arve.arve_totl, 2) as movi_valo,");
            sb.AppendLine("       case when arve.arve_iva2 is null then 0 else round(arve.arve_iva2, 2) end +");
            sb.AppendLine("       case when arve.arve_iva3 is null then 0 else round(arve.arve_iva3, 2) end as movi_voca,");
            sb.AppendLine("       round(arve.arve_vsde, 2) as movi_vsde, round(arve.arve_ivas, 2) as movi_vigv,");
            sb.AppendLine("       case when tiva.tiva_coax = :taxed or arve.arve_por1 > 0 then arve.arve_por1");
            sb.AppendLine("            when tiva.tiva_coax = :inactive then null");
            sb.AppendLine("            when tiva.tiva_coax = :exonerated or arve.arve_por1 = 0 then 0");
            sb.AppendLine("       end as movi_pigv");
            sb.AppendLine("from tnht_arve arve");
            sb.AppendLine("     inner join tnht_artg artg on artg.artg_pk = arve.artg_pk");
            sb.AppendLine("     inner join tnht_tiva tiva on tiva.tiva_pk = arve.ivas_codi");
            sb.AppendLine("where arve.vend_pk = :vend_pk");
            sb.AppendLine("order by artg.artg_codi");
            _documentDetailsCommandText = sb.ToString();
        }

        public FiscalDfactureBusiness21(IDatabaseManager manager, IPosSessionContext sessionContext, string ruc, string user, string password, bool testMode)
            : base(manager, sessionContext, ruc, user, password, testMode)
        {
		}

        #endregion
        #region Protected Methods

        protected override ValidationResult Validate()
        {
            var result = new ValidationResult();
            if (string.IsNullOrEmpty(FiscalNumber))
                result.AddError("RUC del hotel no definido");
            else if (string.IsNullOrEmpty(FiscalUser) || string.IsNullOrEmpty(FiscalPassword))
                result.AddError("Usuario/Contraseña para el servicio fiscal no definida");

            return result;
        }

        #endregion
        #region Private Methods

        private static ListData GetDocumentHeader(IDatabaseManager manager, Guid documentId)
        {
            var query = new BaseQuery(manager, "Dfacture.GetDocumentHeader", _documentHeadersCommandText);
            query.Parameters["true"] = true;
            query.Parameters["false"] = false;
            query.Parameters["lang_pk"] = 3082;
            query.Parameters["vend_pk"] = documentId;

            return query.Execute();
        }

        private static ListData GetDocumentDetails(IDatabaseManager manager, Guid documentId)
        {
            var query = new BaseQuery(manager, "Dfacture.GetDocumentDetails", _documentDetailsCommandText);
            query.Parameters["lang_pk_es"] = 3082;
            query.Parameters["taxed"] = IGVTaxed;
            query.Parameters["exonerated"] = IGVExonerated;
            query.Parameters["inactive"] = IGVInactive;
            query.Parameters["vend_pk"] = documentId;

            return query.Execute();
        }

        private static string Truncate(string s, int maxLength, bool removeSpaces = false)
        {
            if (!string.IsNullOrEmpty(s))
            {
                s = s.Replace(Environment.NewLine, " ").Replace("|", " ").Trim();
                if (removeSpaces)
                    s = s.Replace(" ", string.Empty);
                if (s.Length > maxLength)
                    return s.Substring(0, maxLength);
            }

            return s;
        }

        private static string GetDocumentType(long docType)
        {
            switch (docType)
            {
                // DNI
                case 2:
                    return "1";
                // Carnet de Extranjería
                case 5:
                    return "4";
                // RUC
                case 8:
                    return "6";
                // Pasaporte
                case 3:
                    return "7";
                // Documento fiscal no domiciliado
                case 7:
                    return "0";
            }

            return string.Empty;
        }

        private ValidationResult ProcessDocument(Guid documentId, string document)
        {
            var result = Validate();

            if (result.IsEmpty)
            {
                try
                {
                    var localDateTime = DateTime.Now;
                    string respCode = null;
                    string respXML = null;
                    string respMessage = null;

                    var id = Guid.NewGuid();
                    var status = DocumentFiscalStatus.Pending;

                    var settingsBussines = BusinessContext.GetBusiness<ISettingBusiness>(Manager);
                    var settings = settingsBussines.GetGeneralSettings(sessionContext.InstallationId);

                    var standBussines = BusinessContext.GetBusiness<IStandBusiness>(Manager);
                    var workDate = standBussines.GetWorkDateByStand(sessionContext.StandId);

                    InsertIntegration(Manager, workDate, id, documentId, document);

                    var records = GetDocumentHeader(Manager, documentId);

                    var fiscalDoc = records.ValueAs<string>("docf_nuco");
                    if (!string.IsNullOrEmpty(fiscalDoc) && fiscalDoc.Length > 13)
                        result.AddError("Documento de indentidad inválido. Max 13 caracteres");

                    var fiscalDocType = string.Empty;
                    if (!records.IsNull("docf_tidf"))
                    {
                        fiscalDocType = GetDocumentType(records.ValueAs<long>("docf_tidf"));
                        if (fiscalDocType == "1")
                        {
                            if (fiscalDoc != null && fiscalDoc.Length == 9)
                                fiscalDoc = fiscalDoc.Substring(0, 8);
                        }
                    }

                    var req = new NewHotel.Fiscal.Dfacture.DfactureService.DocumentoElectronico();

                    var cancellationDate = records.ValueAs<DateTime?>("ncre_daem");
                    var cancellationTime = records.ValueAs<DateTime?>("ncre_hoem");
                    var emissionDate = cancellationDate.HasValue ? cancellationDate.Value : records.ValueAs<DateTime>("docf_daem");
                    var emissionTime = cancellationTime.HasValue ? cancellationTime.Value : records.ValueAs<DateTime>("docf_hoem");
                    var serie = cancellationDate.HasValue ? records.ValueAs<string>("ncre_sedo") : records.ValueAs<string>("docf_sedo");
                    var number = (cancellationDate.HasValue ? records.ValueAs<long>("ncre_codi") : records.ValueAs<long>("docf_codi")).ToString("00000000");
                    var freeOfCharge = records.ValueAs<bool>("docf_free");
                    var expirationDate = emissionDate.AddDays(1);

                    var reasonCode = string.Empty;
                    var reasonDescription = string.Empty;
                    var applyRetention = false;

                    var invoiceRetentionPercent = decimal.Zero;
                    var invoiceExpirationDate = DateTime.MinValue;
                    decimal? invoiceTotalValue = null;

                    var docType = records.ValueAs<int>("docf_tido");
                    switch (docType)
                    {
                        // factura
                        case 1:
                            invoiceExpirationDate = expirationDate;
                            req.facturaNegociable = new NewHotel.Fiscal.Dfacture.DfactureService.ModoPago();
                            break;
                        // boleta
                        case 3:
                            break;
                        // nota de crédito
                        case 7:
                            reasonDescription = records.ValueAs<string>("mtco_desc");
                            reasonCode = records.ValueAs<string>("mtco_caux");
                            if (string.IsNullOrEmpty(reasonCode))
                                result.AddError("Nota de crédito sin motivo");
                            else if (!_creditNoteReasonCodes.Contains(reasonCode))
                                result.AddError($"Motivo de nota de crédito no es válido: {reasonCode}");

                            if (reasonCode == "13")
                            {
                                req.facturaNegociable = new NewHotel.Fiscal.Dfacture.DfactureService.ModoPago();
                                invoiceTotalValue = records.ValueAs<decimal?>("docf_valo");
                            }
                            break;
                        default:
                            result.AddError("Tipo de documento no soportado");
                            break;
                    }

                    if (result.IsEmpty)
                    {
                        #region Datos Generales

                        req.idTransaccion = id.ToString("N").ToUpperInvariant();
                        req.tipoDocumento = docType.ToString("00");
                        req.serie = serie;
                        req.correlativo = number;
                        req.fechaEmision = emissionDate.ToString("yyyy-MM-dd");
                        req.horaEmision = emissionTime.ToString("HH:mm:ss");
                        req.fechaVencimiento = expirationDate.ToString("yyyy-MM-dd");
                        var isCompanyInvoice = records.ValueAs<bool>("docf_faen");

                        #endregion

                        #region Emisor

                        var hotel = new Hotel(Manager, sessionContext.InstallationId);

                        var address = new StringBuilder();
                        if (!string.IsNullOrEmpty(hotel.FiscalDoor))
                            address.AppendLine(hotel.FiscalDoor);
                        if (!string.IsNullOrEmpty(hotel.FiscalAddress1))
                            address.AppendLine(hotel.FiscalAddress1);
                        if (!string.IsNullOrEmpty(hotel.FiscalAddress2))
                            address.AppendLine(hotel.FiscalAddress2);

                        req.emisor = new NewHotel.Fiscal.Dfacture.DfactureService.Emisor();
                        req.emisor.nombreComercial = Truncate(hotel.Description, 100);
                        req.emisor.ruc = hotel.FiscalNumber;
                        req.emisor.domicilioFiscal = Truncate(address.ToString().Replace(Environment.NewLine, " "), 200);
                        req.emisor.urbanizacion = Truncate(hotel.FiscalLocation, 25);
                        req.emisor.distrito = string.Empty;
                        req.emisor.provincia = string.Empty;
                        if (hotel.FiscalDistrictId.HasValue)
                        {
                            var region = new Region(Manager, hotel.FiscalDistrictId.Value);
                            req.emisor.distrito = Truncate(region.Description, 30);
                            if (region.CountryZoneId.HasValue)
                            {
                                var countryZone = new CountryZone(Manager, region.CountryZoneId.Value);
                                req.emisor.provincia = Truncate(countryZone.Description, 30);
                            }
                        }
                        req.emisor.departamento = string.Empty;
                        req.emisor.codigoPais = hotel.Nationality;
                        req.emisor.lugarExpedicion = "0000";

                        #endregion

                        #region Receptor

                        var holderFullName = records.ValueAs<string>("docf_titu");
                        var holderCountry = records.ValueAs<string>("docf_naci");

                        req.receptor = new NewHotel.Fiscal.Dfacture.DfactureService.Receptor();
                        req.receptor.numDocumento = fiscalDoc;
                        req.receptor.tipoDocumento = fiscalDocType;
                        req.receptor.razonSocial = Truncate(holderFullName, 100);
                        req.receptor.direccion = Truncate(records.ValueAs<string>("docf_addr"), 100);
                        req.receptor.distrito = string.Empty;
                        req.receptor.provincia = string.Empty;
                        var districtId = records.ValueAs<Guid?>("docf_dist");
                        if (districtId.HasValue)
                        {
                            var region = new Region(Manager, districtId.Value);
                            req.receptor.distrito = Truncate(region.Description, 100);
                            if (region.CountryZoneId.HasValue)
                            {
                                var countryZone = new CountryZone(Manager, region.CountryZoneId.Value);
                                req.receptor.provincia = Truncate(countryZone.Description, 100);
                            }
                        }
                        req.receptor.departamento = string.Empty;
                        req.receptor.pais = holderCountry;
                        req.receptor.telefono = Truncate(records.ValueAs<string>("docf_telf"), 50);
                        req.receptor.email = Truncate(records.ValueAs<string>("docf_mail"), 100);
                        req.receptor.notificar = string.IsNullOrEmpty(req.receptor.email) ? no : yes;

                        #endregion

                        var totalIgvValue = decimal.Zero;
                        var totalNetIgvValue = decimal.Zero;
                        var totalChargeValue = decimal.Zero;
                        var totalDiscountValue = decimal.Zero;
                        var totalTipValue = decimal.Zero;
                        var totalGrossValue = decimal.Zero;
                        var totalNetValue = decimal.Zero;

                        #region Pagos

                        req.pago = new NewHotel.Fiscal.Dfacture.DfactureService.Pago();

                        if (!freeOfCharge)
                            req.pago.metodoPago = records.ValueAs<string>("docf_fore");

                        if (string.IsNullOrEmpty(req.pago.metodoPago))
                            req.pago.metodoPago = OtherPaymentCode;

                        req.pago.fechaInicio = emissionDate.ToString("yyyy-MM-dd");
                        req.pago.fechaFin = emissionDate.AddDays(1).ToString("yyyy-MM-dd");
                        req.pago.moneda = records.ValueAs<string>("docf_unmo");

                        #endregion

                        #region Informaciones

                        var pdf = new List<NewHotel.Fiscal.Dfacture.DfactureService.PersonalizacionPDF>();
                        var productos = new List<NewHotel.Fiscal.Dfacture.DfactureService.Producto>();

                        req.codigoTipoOperacion = InternalCode;

                        if (result.IsEmpty)
                        {
                            #region Datos del Documento

                            if (freeOfCharge)
                            {
                                pdf.Add(new NewHotel.Fiscal.Dfacture.DfactureService.PersonalizacionPDF()
                                {
                                    seccion = "Documento",
                                    titulo = "Formas de pago",
                                    valor = "Gratuito"
                                });
                            }
                            else
                            {
                                var payments = records.ValueAs<string>("docf_fore");
                                if (!string.IsNullOrEmpty(payments))
                                {
                                    foreach (var payment in payments.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries))
                                    {
                                        pdf.Add(new NewHotel.Fiscal.Dfacture.DfactureService.PersonalizacionPDF()
                                        {
                                            seccion = "Documento",
                                            titulo = "Formas de pago",
                                            valor = payment.Trim()
                                        });
                                    }
                                }
                            }

                            var comments = records.ValueAs<string>("docf_obse");
                            if (!string.IsNullOrEmpty(comments))
                            {
                                pdf.Add(new NewHotel.Fiscal.Dfacture.DfactureService.PersonalizacionPDF()
                                {
                                    seccion = "Documento",
                                    titulo = "Comentarios",
                                    valor = comments
                                });
                            }

                            #endregion

                            #region Productos

                            if (result.IsEmpty)
                            {
                                records = GetDocumentDetails(Manager, documentId);

                                int itemNumber = 0;
                                foreach (IRecord record in records)
                                {
                                    var grossValue = Math.Abs(record.ValueAs<decimal>("movi_valo"));
                                    totalGrossValue += grossValue;

                                    if (record.ValueAs<bool>("movi_tipl"))
                                        totalTipValue += grossValue;
                                    else
                                    {
                                        var quantity = Math.Abs(record.ValueAs<decimal>("movi_cant"));
                                        if (quantity == decimal.Zero)
                                            quantity = decimal.One;

                                        var igvValue = record.ValueAs<decimal?>("movi_vigv") ?? decimal.Zero;
                                        var netValue = Math.Abs(record.ValueAs<decimal>("movi_vliq"));
                                        var chargeValue = Math.Abs(record.ValueAs<decimal>("movi_voca"));
                                        var undiscountValue = Math.Abs(record.ValueAs<decimal>("movi_vsde"));
                                        var discountValue = undiscountValue - grossValue;

                                        totalNetValue += netValue;
                                        totalDiscountValue += discountValue;
                                        totalIgvValue += igvValue;
                                        if (igvValue > decimal.Zero)
                                            totalNetIgvValue += netValue;

                                        #region IGV

                                        var igvPercent = record.ValueAs<decimal?>("movi_pigv");
                                        var typeOfIGV = IGVFree;

                                        if (!freeOfCharge)
                                        {
                                            if (igvPercent.HasValue)
                                            {
                                                // gravado
                                                if (igvPercent.Value > decimal.Zero)
                                                    typeOfIGV = IGVTaxed;
                                                // exonerado
                                                else
                                                    typeOfIGV = IGVExonerated;
                                            }
                                            // inafecto
                                            else
                                                typeOfIGV = IGVInactive;
                                        }

                                        var igv = new NewHotel.Fiscal.Dfacture.DfactureService.ProductoIGV();
                                        igv.tipo = typeOfIGV;
                                        igv.baseImponible = netValue.ToString("F2", Dfacture.Culture);
                                        igv.monto = igvValue.ToString("F2", Dfacture.Culture);
                                        igv.porcentaje = (igvPercent ?? decimal.Zero).ToString("F2", Dfacture.Culture);

                                        #endregion

                                        #region Descuento de Producto

                                        NewHotel.Fiscal.Dfacture.DfactureService.ProductoDescuento productoDescuento = null;
                                        if (discountValue > decimal.Zero)
                                        {
                                            productoDescuento = new NewHotel.Fiscal.Dfacture.DfactureService.ProductoDescuento();
                                            productoDescuento.codigo = "00";
                                            productoDescuento.baseImponible = (undiscountValue / quantity).ToString("F2", Dfacture.Culture);
                                            productoDescuento.porcentaje = (discountValue / undiscountValue).ToString("F5", Dfacture.Culture);
                                            productoDescuento.monto = (discountValue / quantity).ToString("F2", Dfacture.Culture);
                                        }

                                        #endregion

                                        #region Cargo de Producto

                                        NewHotel.Fiscal.Dfacture.DfactureService.ProductoCargo productoCargo = null;
                                        if (chargeValue > decimal.Zero)
                                        {
                                            productoCargo = new NewHotel.Fiscal.Dfacture.DfactureService.ProductoCargo();
                                            productoCargo.codigo = "48";
                                            productoCargo.baseImponible = netValue.ToString("F2", Dfacture.Culture);
                                            productoCargo.porcentaje = (chargeValue / netValue).ToString("F5", Dfacture.Culture);
                                            productoCargo.monto = chargeValue.ToString("F2", Dfacture.Culture);
                                            totalChargeValue += chargeValue;
                                        }

                                        #endregion

                                        var itemCode = record.ValueAs<string>("movi_code");
                                        var itemDescription = record.ValueAs<string>("movi_desc");
                                        var itemGrossValue = grossValue - chargeValue;

                                        var producto = new NewHotel.Fiscal.Dfacture.DfactureService.Producto();
                                        producto.numeroOrden = (++itemNumber).ToString();
                                        producto.cantidad = quantity.ToString("F3", Dfacture.Culture);
                                        producto.descripcion = Truncate(itemDescription, 250);
                                        producto.unidadMedida = unitOfMeasure;
                                        producto.codigoPLUSunat = itemCode;
                                        producto.valorVentaItemQxBI = netValue.ToString("F2", Dfacture.Culture);
                                        producto.valorUnitarioBI = (netValue / quantity).ToString("F2", Dfacture.Culture);
                                        producto.precioVentaUnitarioItem = (grossValue / quantity).ToString("F2", Dfacture.Culture);
                                        producto.montoTotalImpuestoItem = igv.monto;
                                        producto.descuento = productoDescuento;
                                        producto.cargo = productoCargo;
                                        producto.IGV = igv;

                                        if (freeOfCharge)
                                            producto.valorReferencialUnitario = producto.valorUnitarioBI;

                                        productos.Add(producto);
                                    }
                                }

                                req.producto = productos.ToArray();
                            }

                            #endregion

                            // si no fue definido un tipo
                            if (string.IsNullOrEmpty(req.codigoTipoOperacion))
                            {
                                switch (req.tipoDocumento)
                                {
                                    case InvoiceCode:
                                        // si es gravado y de empresa
                                        if (isCompanyInvoice && totalIgvValue != decimal.Zero)
                                            req.codigoTipoOperacion = InternalCode;
                                        else
                                            req.codigoTipoOperacion = ExportCode;
                                        break;
                                    case CreditNoteCode:
                                        // si es gravado
                                        if (totalIgvValue != decimal.Zero)
                                            req.codigoTipoOperacion = InternalCode;
                                        else
                                            req.codigoTipoOperacion = ExportCode;
                                        break;
                                    default:
                                        req.codigoTipoOperacion = InternalCode;
                                        break;
                                }
                            }

                            // exportación
                            if (req.codigoTipoOperacion == ExportCode)
                            {
                                foreach (var producto in productos)
                                {
                                    if (producto.IGV != null)
                                        producto.IGV.tipo = IGVExport;
                                }
                            }

                            if (pdf.Count > 0)
                                req.personalizacionPDF = pdf.ToArray();
                        }

                        #endregion

                        if (result.IsEmpty)
                        {
                            #region Validaciones IGV

                            var typesOfIGV = productos.Select(p => p.IGV.tipo).Distinct();
                            if (typesOfIGV.Count() > 1)
                                result.AddError(string.Format("No esta soportado multiples tipos de IGV: {0}", string.Join("/", typesOfIGV)));

                            if (result.IsEmpty)
                            {
                                if (!freeOfCharge && req.codigoTipoOperacion == ExportCode)
                                {
                                    if (totalIgvValue != decimal.Zero)
                                        result.AddError("Total de IGV debe ser 0 para exportación");
                                }
                            }

                            #endregion

                            if (result.IsEmpty)
                            {
                                totalIgvValue = Math.Round(totalIgvValue, 2);
                                totalNetIgvValue = Math.Round(totalNetIgvValue, 2);
                                totalChargeValue = Math.Round(totalChargeValue, 2);
                                totalGrossValue = Math.Round(totalGrossValue, 2);
                                totalNetValue = Math.Round(totalNetValue, 2);
                                totalTipValue = Math.Round(totalTipValue, 2);

                                #region Cargos Globales

                                if (totalTipValue > decimal.Zero)
                                {
                                    var cargosGlobales = new NewHotel.Fiscal.Dfacture.DfactureService.CargosGlobales();
                                    cargosGlobales.motivo = "46";
                                    cargosGlobales.baseImponible = (totalGrossValue - totalTipValue).ToString("F2", Dfacture.Culture);
                                    cargosGlobales.monto = totalTipValue.ToString("F2", Dfacture.Culture);
                                    cargosGlobales.porcentaje = (totalTipValue / (totalGrossValue - totalTipValue)).ToString("F5", Dfacture.Culture);

                                    req.cargosGlobales = cargosGlobales;
                                }

                                #endregion

                                #region Totales + SubTotales

                                req.totales = new NewHotel.Fiscal.Dfacture.DfactureService.Totales();
                                req.totales.subtotal = new NewHotel.Fiscal.Dfacture.DfactureService.Subtotal();
                                req.totales.montoTotalImpuestos = totalIgvValue.ToString("F2", Dfacture.Culture);

                                if (freeOfCharge)
                                {
                                    req.totales.importeTotalVenta = 0.ToString("F2", Dfacture.Culture);
                                    req.totales.importeTotalPagar = req.totales.importeTotalVenta;
                                    req.totales.sumatoriaImpuestosOG = req.totales.montoTotalImpuestos;
                                    req.totales.subtotal.gratuitas = totalNetValue.ToString("F2", Dfacture.Culture);
                                }
                                else
                                {
                                    req.totales.importeTotalVenta = (totalGrossValue - totalChargeValue).ToString("F2", Dfacture.Culture);
                                    req.totales.importeTotalPagar = totalGrossValue.ToString("F2", Dfacture.Culture);
                                }

                                req.totales.subtotalValorVenta = totalNetValue.ToString("F2", Dfacture.Culture);

                                switch (typesOfIGV.First())
                                {
                                    case "40":
                                        req.totales.subtotal.exportacion = (totalGrossValue - totalChargeValue - totalTipValue).ToString("F2", Dfacture.Culture);
                                        break;
                                    case "30":
                                        req.totales.subtotal.inafectas = (totalGrossValue - totalChargeValue - totalTipValue).ToString("F2", Dfacture.Culture);
                                        break;
                                    case "20":
                                        req.totales.subtotal.exoneradas = (totalGrossValue - totalChargeValue - totalTipValue).ToString("F2", Dfacture.Culture);
                                        break;
                                    case "10":
                                        req.totales.subtotal.IGV = totalNetIgvValue.ToString("F2", Dfacture.Culture);
                                        req.totales.totalIGV = totalIgvValue.ToString("F2", Dfacture.Culture);
                                        break;
                                }

                                #endregion

                                #region Factura Negociable

                                if (req.facturaNegociable != null)
                                {
                                    var totalValue = totalGrossValue;
                                    if (invoiceTotalValue.HasValue)
                                        totalValue = Math.Round(invoiceTotalValue.Value, 2);

                                    #region Retenciones

                                    var pendingValue = totalValue;
                                    if (applyRetention)
                                    {
                                        var retentionValue = Math.Round(totalValue * invoiceRetentionPercent / 100, 2);
                                        pendingValue -= retentionValue;

                                        var retencionFactura = new NewHotel.Fiscal.Dfacture.DfactureService.retencionFactura
                                        {
                                            importeOperacion = totalValue.ToString("F2", Dfacture.Culture),
                                            porcentajeRetencion = (invoiceRetentionPercent / 100).ToString("F2", Dfacture.Culture),
                                            montoImporte = retentionValue.ToString("F2", Dfacture.Culture)
                                        };

                                        req.facturaNegociable.retencionFactura = new NewHotel.Fiscal.Dfacture.DfactureService.retencionFactura[] { retencionFactura };
                                    }

                                    #endregion

                                    #region Cuotas

                                    var cuotasFactura = new List<NewHotel.Fiscal.Dfacture.DfactureService.cuotaFactura>();

                                    req.facturaNegociable.modoPago = CashPaymentMethod;
                                    req.facturaNegociable.cuotasFactura = cuotasFactura.ToArray();

                                    #endregion
                                }

                                #endregion
                            }
                        }

                        if (result.IsEmpty)
                        {
                            using (var client = Dfacture.GetProxy21(FiscalTestMode))
                            {
                                try
                                {
                                    var reqXML = req.ToXml();

                                    var reqDate = localDateTime;
                                    UpdateIntegrationRequest(Manager, id, reqDate, status, reqXML);

                                    var resp = client.Enviar(FiscalNumber, FiscalUser, FiscalPassword, req);

                                    if (resp.codigo != ResponseDocSucceed &&
                                        resp.codigo != ResponseDocInProcess)
                                    {
                                        status = DocumentFiscalStatus.Error;
                                        respMessage = resp.codigo + ": " + resp.mensaje;

                                        var log = new StringBuilder();
                                        log.AppendLine(respMessage);
                                        log.AppendLine(document);
                                        result.AddError(log.ToString());
                                        
                                        log.Append(req.ToXml());
                                        log.AppendLine();
                                        log.AppendLine(client.Endpoint.Address.Uri.ToString());
                                        log.AppendFormat("RUC: {0}", FiscalNumber);
                                        log.AppendLine();
                                        log.AppendFormat("User: {0}", FiscalUser);
                                        log.AppendLine();
                                        log.AppendFormat("Password: {0}", FiscalPassword);
                                        log.AppendLine();

                                        Trace.TraceInformation(log.ToString());
                                    }

                                    respCode = resp.codigo.ToString();
                                    if (!string.IsNullOrEmpty(resp.xml))
                                        respXML = Encoding.UTF8.GetString(Convert.FromBase64String(resp.xml));
                                }
                                catch (TimeoutException ex)
                                {
                                    Trace.TraceError(ex.Message);
                                    result.AddError(Res.CommunicationError, ex.Message);
                                }
                                catch (CommunicationException ex)
                                {
                                    Trace.TraceError(ex.Message);
                                    result.AddError(Res.CommunicationError, ex.Message);
                                }
                            }
                        }
                    }

                    if (result.IsEmpty)
                    {
                        var respDate = localDateTime;
                        UpdateIntegrationResponse(Manager, id, respDate, status, respXML, respMessage, respCode);
                        UpdateDocumentStatus(Manager, documentId, status);
                    }
                }
                catch (Exception ex)
                {
                    Trace.TraceError(ex.Message);
                    result.AddError(ex.Message);
                }
            }

            return result;
        }

        private ValidationResult ProcessCancel(Guid documentId, string document)
        {
            var result = Validate();

            if (result.IsEmpty)
            {
                try
                {
                    var localDateTime = DateTime.Now;
                    string respCode = null;
                    string respXML = null;
                    string respMessage = null;

                    var id = Guid.NewGuid();
                    var status = GetDocumentStatus(Manager, documentId) ?? DocumentFiscalStatus.Manual;

                    if (status == DocumentFiscalStatus.Successful)
                    {
                        var standBussines = BusinessContext.GetBusiness<IStandBusiness>(Manager);
                        var workDate = standBussines.GetWorkDateByStand(sessionContext.StandId);

                        InsertIntegration(Manager, workDate, id, documentId, document);

                        var reqDate = localDateTime;
                        status = DocumentFiscalStatus.Processing;
                        UpdateIntegrationRequest(Manager, id, reqDate, status);

                        var records = GetDocumentHeader(Manager, documentId);
                        var reason = records.ValueAs<string>("mtco_desc");

                        using (var client = Dfacture.GetProxy21(FiscalTestMode))
                        {
                            try
                            {
                                var resp = client.ComunicacionBaja(FiscalNumber, FiscalUser, FiscalPassword, document, reason);

                                if (resp.codigo != ResponseDocSucceed &&
                                    resp.codigo != ResponseDocInProcess &&
                                    resp.codigo != ResponseDocAlreadyCanceled)
                                {
                                    status = DocumentFiscalStatus.Error;
                                    respMessage = resp.codigo + ": " + resp.mensaje;

                                    var log = new StringBuilder();
                                    log.AppendLine(respMessage);
                                    log.AppendLine(document);

                                    result.AddError(log.ToString());

                                    log.AppendLine(client.Endpoint.Address.Uri.ToString());
                                    log.AppendFormat("RUC: {0}", FiscalNumber);
                                    log.AppendLine();
                                    log.AppendFormat("User: {0}", FiscalUser);
                                    log.AppendLine();
                                    log.AppendFormat("Password: {0}", FiscalPassword);
                                    log.AppendLine();

                                    Trace.TraceInformation(log.ToString());
                                }

                                respCode = resp.codigo.ToString();
                                if (!string.IsNullOrEmpty(resp.archivo))
                                    respXML = resp.archivo;
                            }
                            catch (TimeoutException ex)
                            {
                                Trace.TraceError(ex.Message);
                                result.AddError(Res.CommunicationError, ex.Message);
                            }
                            catch (CommunicationException ex)
                            {
                                Trace.TraceError(ex.Message);
                                result.AddError(Res.CommunicationError, ex.Message);
                            }
                        }

                        if (result.IsEmpty)
                        {
                            if (!string.IsNullOrEmpty(respXML))
                            {
                                var respDate = localDateTime;
                                UpdateIntegrationResponse(Manager, id, respDate, status, respXML, respMessage, respCode);
                            }

                            UpdateDocumentStatus(Manager, documentId,
                                status == DocumentFiscalStatus.Successful
                                ? DocumentFiscalStatus.Canceled : status);
                        }
                    }
                    else if (status != DocumentFiscalStatus.Manual)
                        result.AddError("No es posible cancelar documento no integrado con exito");
                }
                catch (Exception ex)
                {
                    Trace.TraceError(ex.Message);
                    result.AddError(ex.Message);
                }
            }

            return result;
        }

        #endregion
        #region Public Methods

        public bool AllowCancel(Guid documentId, bool isInvoiced)
        {
            return true;
        }

        public ValidationResult ProcessInvoice(Guid documentId, string serie, long number)
        {
            // FACTURA: 01-FXXX-00000000
            return ProcessDocument(documentId, string.Format("01-{0}-{1}", serie, number.ToString("00000000")));
        }

        public ValidationResult ProcessTicket(Guid documentId, string serie, long number)
        {
            // BOLETA: 03-BXXX-00000000
            return ProcessDocument(documentId, string.Format("03-{0}-{1}", serie, number.ToString("00000000")));
        }

        public ValidationResult ProcessCreditNote(Guid documentId, string serie, long number)
        {
            // NOTA DE CREDITO DE FACTURA: 07-FCXX-00000000
            // NOTA DE CREDITO DE BOLETA: 07-BCXX-00000000
            return ProcessDocument(documentId, string.Format("07-{0}-{1}", serie, number.ToString("00000000")));
        }

        public ValidationResult CancelInvoice(Guid documentId, string serie, long number)
        {
            // CANCELAMIENTO DE FACTURA: 01-FXXX-00000000
            return ProcessCancel(documentId, string.Format("01-{0}-{1}", serie, number.ToString("00000000")));
        }

        public ValidationResult CancelTicket(Guid documentId, string serie, long number)
        {
            // CANCELAMIENTO DE BOLETA: 03-BXXX-00000000
            return ProcessCancel(documentId, string.Format("03-{0}-{1}", serie, number.ToString("00000000")));
        }

        #endregion
    }
}