﻿using System;
using System.Text;
using System.Globalization;
using NewHotel.Core;
using NewHotel.Contracts;
using NewHotel.Business;
using NewHotel.DataAnnotations;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Hub.Model.Session;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class FiscalBaseBusiness : BaseBusiness
    {
        #region Constants

        private const int MaxStringLength = 2000;

        #endregion
        #region Members

        //private static readonly string _documentEmissionDateCommandText;
        private static readonly string _insertIntegrationCommandText;
        private static readonly string _updateIntegrationRequestCommandText;
        private static readonly string _updateIntegrationResponseCommandText;
        private static readonly string _updateIntegrationDocumentCommandText;
        private static readonly string _updateIntegrationRequestReferenceCommandText;

        private static readonly string _getDocumentStatusCommandText;
        private static readonly string _updateDocumentStatusCommandText;
		protected readonly IPosSessionContext sessionContext;
		protected string FiscalNumber;
        protected string FiscalUser;
        protected string FiscalPassword;
        protected bool FiscalTestMode;
        protected bool AsyncMode;

        #endregion
        #region Constructor

        static FiscalBaseBusiness()
        {
            //_documentEmissionDateCommandText = "select vend_horf as docf_daem from tnht_vend where vend_pk = :vend_pk";
            StringBuilder sb;

            #region Insert Integration

            sb = new StringBuilder();
            sb.AppendLine("insert into tnht_pinf (pinf_pk, pinf_corq, pinf_dare, pinf_daop, hote_pk, util_pk, vend_pk, fdes_pk)");
            sb.Append("values (:pinf_pk, :pinf_corq, :pinf_dare, :pinf_daop, :hote_pk, :util_pk, :vend_pk, :fdes_pk)");
            _insertIntegrationCommandText = sb.ToString();

            #endregion

            #region Update Integration Request

            sb = new StringBuilder();
            sb.AppendLine("update tnht_pinf set");
            sb.AppendLine("pinf_merr = :pinf_merr, pinf_esta = :pinf_esta,");
            sb.AppendLine("pinf_darq = :pinf_darq, pinf_reqt = :pinf_reqt");
            sb.Append("where pinf_pk = :pinf_pk");
            _updateIntegrationRequestCommandText = sb.ToString();

            #endregion

            #region Update Integration Response

            sb = new StringBuilder();
            sb.AppendLine("update tnht_pinf set");
            sb.AppendLine("pinf_esta = :pinf_esta, pinf_merr = :pinf_merr,");
            sb.AppendLine("pinf_darp = :pinf_darp, pinf_resp = :pinf_resp, pinf_corp = :pinf_corp,");
            sb.AppendLine("pinf_durl = :pinf_durl, pinf_xurl = :pinf_xurl ");
            sb.Append("where pinf_pk = :pinf_pk");
            _updateIntegrationResponseCommandText = sb.ToString();

            #endregion

            #region Update Integration Document (PDF + XML + BIN)

            sb = new StringBuilder();
            sb.AppendLine("update tnht_pinf set pinf_dpdf = :pinf_dpdf,");
            sb.AppendLine("pinf_dbin = :pinf_dbin, pinf_dxml = :pinf_dxml");
            sb.Append("where pinf_pk = :pinf_pk");
            _updateIntegrationDocumentCommandText = sb.ToString();

            #endregion

            #region Update Integration Request Reference

            sb = new StringBuilder();
            sb.Append("update tnht_pinf set pinf_corq = :pinf_corq where pinf_pk = :pinf_pk");
            _updateIntegrationRequestReferenceCommandText = sb.ToString();

            #endregion

            #region Get Document Status

            sb = new StringBuilder();
            sb.Append("select vend_fies from tnht_vend where vend_pk = :vend_pk");
            _getDocumentStatusCommandText = sb.ToString();

            #endregion

            #region Update Document Status

            sb = new StringBuilder();
            sb.Append("update tnht_vend set vend_fies = :vend_fies where vend_pk = :vend_pk");
            _updateDocumentStatusCommandText = sb.ToString();

            #endregion
        }

        public FiscalBaseBusiness(IDatabaseManager manager, IPosSessionContext sessionContext)
            : base(manager)
        {
			this.sessionContext = sessionContext;
		}

        public FiscalBaseBusiness(IDatabaseManager manager, IPosSessionContext sessionContext,
            string fiscalNumber, string fiscalUser, string fiscalPassword, bool fiscalTestMode)
            : this(manager, sessionContext)
        {
            FiscalNumber = fiscalNumber;
            FiscalUser = fiscalUser;
            FiscalPassword = fiscalPassword;
            FiscalTestMode = fiscalTestMode;
        }

        #endregion
        #region Protected Methods

        protected virtual ValidationResult Validate()
        {
            return ValidationResult.Empty;
        }

        protected static string ReplaceDiacritics(string s, int maxLength)
        {
            var result = s;
            if (!string.IsNullOrEmpty(s))
            {
                var source = s.Normalize(NormalizationForm.FormD);
                var index = 0;
                var buffer = new char[s.Length];
                for (int i = 0; i < source.Length; i++)
                {
                    var ch = source[i];
                    if (CharUnicodeInfo.GetUnicodeCategory(ch) != UnicodeCategory.NonSpacingMark)
                        buffer[index++] = ch;
                }

                result = (new string(buffer)).Normalize(NormalizationForm.FormC);
            }

            if (!string.IsNullOrEmpty(result))
            {
                result = result.Replace("\r", string.Empty).Replace("\n", " ").Trim();
                if (result.Length > maxLength)
                    result = result.Substring(0, maxLength).Trim();
            }

            return result.Replace(Environment.NewLine, " ");
        }

        //protected static DateTime? GetDocumentEmissionDate(IDatabaseManager manager, Guid documentId)
        //{
        //    var query = new BaseQuery(manager, "Dfacture.GetDocumentEmissionDate", _documentEmissionDateCommandText);
        //    query.Parameters["vend_pk"] = documentId;

        //    return query.ExecuteScalar<DateTime?>();
        //}

        protected void InsertIntegration(IDatabaseManager manager, DateTime workDate, Guid reqId, Guid? documentId, string document, long fdes_pk = 0)
        {
            using (var command = manager.CreateCommand("Fiscal.InsertIntegrationRequest"))
            {
                command.CommandText = _insertIntegrationCommandText;
                manager.CreateParameter(command, "pinf_pk", manager.ConvertValueType(reqId));
                manager.CreateParameter(command, "pinf_corq", manager.ConvertValueType(document));
                manager.CreateParameter(command, "pinf_dare", manager.ConvertValueType(DateTime.Now));
                manager.CreateParameter(command, "pinf_daop", manager.ConvertValueType(workDate));
                manager.CreateParameter(command, "hote_pk", manager.ConvertValueType(sessionContext.InstallationId));
                manager.CreateParameter(command, "util_pk", manager.ConvertValueType(sessionContext.UserId));
                if (documentId.HasValue)
                    manager.CreateParameter(command, "vend_pk", manager.ConvertValueType(documentId.Value));
                else
                    manager.CreateParameter(command, "vend_pk", manager.ConvertValueType(DBNull.Value));

                manager.CreateParameter(command, "fdes_pk", manager.ConvertValueType(fdes_pk));
                manager.ExecuteNonQuery(command);
            }
        }

        protected static void UpdateIntegrationRequest(IDatabaseManager manager, Guid id, DateTime date,
            DocumentFiscalStatus status, string req = null, string error = null)
        {
            manager.BeginTransaction();
            try
            {
                using (var command = manager.CreateCommand("Fiscal.UpdateIntegrationRequest"))
                {
                    command.CommandText = _updateIntegrationRequestCommandText;
                    manager.CreateParameter(command, "pinf_pk", manager.ConvertValueType(id));
                    manager.CreateParameter(command, "pinf_esta", manager.ConvertValueType(status));
                    if (string.IsNullOrEmpty(error))
                        manager.CreateParameter(command, "pinf_merr", manager.ConvertValueType(DBNull.Value));
                    else
                    {
                        if (error.Length > MaxStringLength)
                            error = error.Substring(0, MaxStringLength);
                        manager.CreateParameter(command, "pinf_merr", manager.ConvertValueType(error));
                    }
                    manager.CreateParameter(command, "pinf_darq", manager.ConvertValueType(date));
                    manager.CreateParameter(command, "pinf_reqt", manager.ConvertValueType(req));

                    manager.ExecuteNonQuery(command);
                }

                manager.CommitTransaction();
            }
            catch
            {
                manager.RollbackTransaction();
                throw;
            }
        }

        protected static void UpdateIntegrationResponse(IDatabaseManager manager, Guid id, DateTime date,
            DocumentFiscalStatus status, string resp = null, string error = null,
            string respCode = null, string respURL = null, string respXML = null)
        {
            manager.BeginTransaction();
            try
            {
                using (var command = manager.CreateCommand("Fiscal.UpdateIntegrationResponse"))
                {
                    command.CommandText = _updateIntegrationResponseCommandText;
                    manager.CreateParameter(command, "pinf_pk", manager.ConvertValueType(id));
                    manager.CreateParameter(command, "pinf_esta", manager.ConvertValueType(status));
                    if (string.IsNullOrEmpty(error))
                        manager.CreateParameter(command, "pinf_merr", manager.ConvertValueType(DBNull.Value));
                    else
                    {
                        if (error.Length > MaxStringLength)
                            error = error.Substring(0, MaxStringLength);
                        manager.CreateParameter(command, "pinf_merr", manager.ConvertValueType(error));
                    }
                    manager.CreateParameter(command, "pinf_darp", manager.ConvertValueType(date));
                    if (string.IsNullOrEmpty(resp))
                        manager.CreateParameter(command, "pinf_resp", manager.ConvertValueType(DBNull.Value));
                    else
                        manager.CreateParameter(command, "pinf_resp", manager.ConvertValueType(resp));
                    if (string.IsNullOrEmpty(respCode))
                        manager.CreateParameter(command, "pinf_corp", manager.ConvertValueType(DBNull.Value));
                    else
                        manager.CreateParameter(command, "pinf_corp", manager.ConvertValueType(respCode));
                    if (string.IsNullOrEmpty(respURL))
                        manager.CreateParameter(command, "pinf_durl", manager.ConvertValueType(DBNull.Value));
                    else
                        manager.CreateParameter(command, "pinf_durl", manager.ConvertValueType(respURL));
                    if (string.IsNullOrEmpty(respXML))
                        manager.CreateParameter(command, "pinf_xurl", manager.ConvertValueType(DBNull.Value));
                    else
                        manager.CreateParameter(command, "pinf_xurl", manager.ConvertValueType(respXML));
                    manager.ExecuteNonQuery(command);
                }

                manager.CommitTransaction();
            }
            catch
            {
                manager.RollbackTransaction();
                throw;
            }
        }

        protected static void UpdateIntegrationDocument(IDatabaseManager manager, Guid id, byte[] pdf = null, byte[] doc = null, string xml = null)
        {
            manager.BeginTransaction();
            try
            {
                using (var command = manager.CreateCommand("Fiscal.UpdateIntegrationDocument"))
                {
                    command.CommandText = _updateIntegrationDocumentCommandText;
                    manager.CreateParameter(command, "pinf_pk", manager.ConvertValueType(id));
                    if (pdf != null)
                        manager.CreateParameter(command, "pinf_dpdf", manager.ConvertValueType(new Blob(pdf)));
                    else
                        manager.CreateParameter(command, "pinf_dpdf", manager.ConvertValueType(DBNull.Value));
                    if (doc != null)
                        manager.CreateParameter(command, "pinf_dbin", manager.ConvertValueType(new Blob(doc)));
                    else
                        manager.CreateParameter(command, "pinf_dbin", manager.ConvertValueType(DBNull.Value));
                    if (!string.IsNullOrEmpty(xml))
                        manager.CreateParameter(command, "pinf_dxml", manager.ConvertValueType(new Clob(xml)));
                    else
                        manager.CreateParameter(command, "pinf_dxml", manager.ConvertValueType(DBNull.Value));
                    manager.ExecuteNonQuery(command);
                }

                manager.CommitTransaction();
            }
            catch
            {
                manager.RollbackTransaction();
                throw;
            }
        }

        protected static void UpdateIntegrationRequestReference(IDatabaseManager manager, Guid id, string document)
        {
            manager.BeginTransaction();
            try
            {
                using (var command = manager.CreateCommand("Fiscal.UpdateIntegrationRequestReference"))
                {
                    command.CommandText = _updateIntegrationRequestReferenceCommandText;
                    manager.CreateParameter(command, "pinf_corq", manager.ConvertValueType(document));
                    manager.CreateParameter(command, "pinf_pk", manager.ConvertValueType(id));

                    manager.ExecuteNonQuery(command);
                }

                manager.CommitTransaction();
            }
            catch
            {
                manager.RollbackTransaction();
                throw;
            }
        }

        protected static DocumentFiscalStatus? GetDocumentStatus(IDatabaseManager manager, Guid documentId)
        {
            using (var command = manager.CreateCommand("Fiscal.GetDocumentStatus"))
            {
                command.CommandText = _getDocumentStatusCommandText;
                manager.CreateParameter(command, "vend_pk", manager.ConvertValueType(documentId));
                var obj = manager.ExecuteScalar(command);
                if (obj != null)
                    return (DocumentFiscalStatus)Enum.ToObject(typeof(DocumentFiscalStatus), obj);

                return null;
            }
        }

        protected static void UpdateDocumentStatus(IDatabaseManager manager, Guid documentId, DocumentFiscalStatus status)
        {
            manager.BeginTransaction();
            try
            {
                using (var command = manager.CreateCommand("Fiscal.UpdateDocumentStatus"))
                {
                    command.CommandText = _updateDocumentStatusCommandText;
                    manager.CreateParameter(command, "vend_fies", manager.ConvertValueType(status));
                    manager.CreateParameter(command, "vend_pk", manager.ConvertValueType(documentId));

                    manager.ExecuteNonQuery(command);
                }

                manager.CommitTransaction();
            }
            catch
            {
                manager.RollbackTransaction();
                throw;
            }
        }

        #endregion
    }
}