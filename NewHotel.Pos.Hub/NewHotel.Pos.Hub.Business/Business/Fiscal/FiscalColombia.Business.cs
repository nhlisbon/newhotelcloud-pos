﻿using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.Pos.Hub.Model.Session;
using System;


namespace NewHotel.Pos.Hub.Business.Business
{
    public class FiscalColombiaBusiness : FiscalBaseBusiness, IFiscalBusiness
    {
        public FiscalColombiaBusiness(IDatabaseManager manager, IPosSessionContext sessionContext) : base(manager, sessionContext)
        {
        }

        public FiscalColombiaBusiness(IDatabaseManager manager, IPosSessionContext sessionContext, string fiscalNumber, string fiscalUser, string fiscalPassword, bool fiscalTestMode)
            : base(manager, sessionContext, fiscalNumber, fiscalUser, fiscalPassword, fiscalTestMode)
        {
        }

        protected override ValidationResult Validate()
        {
            throw new NotImplementedException();
        }

        public bool AllowCancel(Guid documentId, bool isInvoiced)
        {
            throw new NotImplementedException();
        }

        public ValidationResult CancelInvoice(Guid documentId, string serie, long number)
        {
            throw new NotImplementedException();
        }

        public ValidationResult CancelTicket(Guid documentId, string serie, long number)
        {
            throw new NotImplementedException();
        }

        public ValidationResult ProcessCreditNote(Guid documentId, string serie, long number)
        {
            throw new NotImplementedException();
        }

        public ValidationResult ProcessInvoice(Guid documentId, string serie, long number)
        {
            throw new NotImplementedException();
        }

        public ValidationResult ProcessTicket(Guid documentId, string serie, long number)
        {
            throw new NotImplementedException();
        }
    }
}
