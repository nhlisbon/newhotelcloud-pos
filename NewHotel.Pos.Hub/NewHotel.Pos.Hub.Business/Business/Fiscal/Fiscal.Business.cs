﻿using System;
using NewHotel.Core;
using NewHotel.Contracts;
using NewHotel.Business;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Hub.Model.Session;

namespace NewHotel.Pos.Hub.Business.Business
{
    public interface IFiscalBusiness
    {
        ValidationResult ProcessInvoice(Guid documentId, string serie, long number);
        ValidationResult ProcessTicket(Guid documentId, string serie, long number);
        ValidationResult ProcessCreditNote(Guid documentId, string serie, long number);

        bool AllowCancel(Guid documentId, bool isInvoiced);

        ValidationResult CancelInvoice(Guid documentId, string serie, long number);
        ValidationResult CancelTicket(Guid documentId, string serie, long number);
    }

    public class FiscalBusiness : BaseBusiness, IFiscalBusiness
    {
        #region Members

        private static readonly string _documentEmissionDateCommandText;
        private readonly IFiscalBusiness _business;
		private readonly IPosSessionContext _sessionContext;

		#endregion
		#region Constructor

		static FiscalBusiness()
        {
            _documentEmissionDateCommandText = "select vend_horf as docf_daem from tnht_vend where vend_pk = :vend_pk";
        }

        public FiscalBusiness(IDatabaseManager manager, IPosSessionContext sessionContext)
            : base(manager)
        {
			_sessionContext = sessionContext;
			var settingsBussines = BusinessContext.GetBusiness<ISettingBusiness>(Manager);
            var settings = settingsBussines.GetGeneralSettings(_sessionContext.InstallationId);

            switch (settings.SignType)
            {
                case DocumentSign.FiscalizationPeruDFacture:
                    if (settings.SignVersion.HasValue)
                    {
                        switch (settings.SignVersion.Value)
                        {
                            case 21:
                                _business = new FiscalDfactureBusiness21(manager, _sessionContext, settings.FiscalNumber,
                                    settings.FiscalServiceUser, settings.FiscalServicePassword,
                                    settings.SignTestMode);
                                break;
                        }
                    }
                    break;
                case DocumentSign.FiscalizationPanamaHKA:
                    _business = new FiscalHKABusiness(manager, sessionContext);
                    break;
            }
		}

        #endregion
        #region Private Methods

        private static DateTime? GetDocumentEmissionDate(IDatabaseManager manager, Guid documentId)
        {
            var query = new BaseQuery(manager, "Dfacture.GetDocumentEmissionDate", _documentEmissionDateCommandText);
            query.Parameters["vend_pk"] = documentId;

            return query.ExecuteScalar<DateTime?>();
        }

        private bool AllowCancel(Guid documentId, DateTime emissionDate, bool isInvoiced)
        {
            var settingsBussines = BusinessContext.GetBusiness<ISettingBusiness>(Manager);
            var settings = settingsBussines.GetGeneralSettings(_sessionContext.InstallationId);

            var now = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Utc);

            var daysToCancelInvoice = settings.CancelInvoicesOnlyEmissionDate ? 0 : settings.DaysToCancelInvoice;
            if (isInvoiced)
            {
                if (daysToCancelInvoice.HasValue)
                    return (now - emissionDate).Days <= daysToCancelInvoice.Value;
                else if (settings.CancelInvoicesOnlyEmissionMonth)
                    return emissionDate.Date.Month == now.Date.Month && emissionDate.Date.Year == now.Date.Year;
            }
            else if (settings.TicketCancelWindow.HasValue)
                return emissionDate.Date.Add(settings.TicketCancelWindow.Value) <= now;

            return true;
        }

        #endregion
        #region Public Methods

        public bool AllowCancel(Guid documentId, bool isInvoiced)
        {
            var emissionDate = GetDocumentEmissionDate(Manager, documentId);
            if (emissionDate.HasValue)
                return AllowCancel(documentId, emissionDate.Value, isInvoiced);

            return true;
        }

        public ValidationResult ProcessInvoice(Guid documentId, string serie, long number)
        {
            var result = new ValidationResult();
            if (_business != null)
                result.Add(_business.ProcessInvoice(documentId, serie, number));

            return result;
        }

        public ValidationResult ProcessTicket(Guid documentId, string serie, long number)
        {
            var result = new ValidationResult();
            if (_business != null)
                result.Add(_business.ProcessTicket(documentId, serie, number));

            return result;
        }

        public ValidationResult ProcessCreditNote(Guid documentId, string serie, long number)
        {
            var result = new ValidationResult();
            if (_business != null)
                result.Add(_business.ProcessCreditNote(documentId, serie, number));

            return result;
        }

        public ValidationResult CancelInvoice(Guid documentId, string serie, long number)
        {
            var result = new ValidationResult();
            if (_business != null)
                result.Add(_business.CancelInvoice(documentId, serie, number));

            return result;
        }

        public ValidationResult CancelTicket(Guid documentId, string serie, long number)
        {
            var result = new ValidationResult();
            if (_business != null)
                result.Add(_business.CancelTicket(documentId, serie, number));

            return result;
        }

        #endregion
    }
}