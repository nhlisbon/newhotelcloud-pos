﻿using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Model.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class FiscalHKABusiness : FiscalBaseBusiness, IFiscalBusiness
    {
        public FiscalHKABusiness(IDatabaseManager manager, IPosSessionContext sessionContext) : base(manager, sessionContext)
        {
        }

        public FiscalHKABusiness(IDatabaseManager manager, IPosSessionContext sessionContext, string fiscalNumber, string fiscalUser, string fiscalPassword, bool fiscalTestMode)
            : base(manager, sessionContext, fiscalNumber, fiscalUser, fiscalPassword, fiscalTestMode)
        {
        }

        protected override ValidationResult Validate()
        {
            var result = new ValidationResult();
            if (string.IsNullOrEmpty(FiscalNumber))
                result.AddError("RUC del hotel no definido");
            return result;
        }

        public bool AllowCancel(Guid documentId, bool isInvoiced)
        {
            throw new NotImplementedException();
        }

        public ValidationResult CancelInvoice(Guid documentId, string serie, long number)
        {
            throw new NotImplementedException();
        }

        public ValidationResult CancelTicket(Guid documentId, string serie, long number)
        {
            throw new NotImplementedException();
        }

        public ValidationResult ProcessCreditNote(Guid documentId, string serie, long number)
        {
            throw new NotImplementedException();
        }

        public ValidationResult ProcessInvoice(Guid documentId, string serie, long number)
        {
            var document = number.ToString() + "/" + serie;
            var standBussines = BusinessContext.GetBusiness<IStandBusiness>(Manager);
            var workDate = standBussines.GetWorkDateByStand(sessionContext.StandId);
            long fdes_pk = 13;

            var id = Guid.NewGuid();
            InsertIntegration(Manager, workDate, id, documentId, document, fdes_pk);
            return new ValidationResult();
        }

        public ValidationResult ProcessTicket(Guid documentId, string serie, long number)
        {
            throw new NotImplementedException();
        }
    }
}
