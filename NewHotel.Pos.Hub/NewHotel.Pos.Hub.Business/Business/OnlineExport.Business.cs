﻿using System;
using NewHotel.Business;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Contracts;
using NewHotel.Pos.Hub.Model.Session;

namespace NewHotel.Pos.Hub.Business.Business
{
    #region Contingency Exportation 

    public class ContingencyExportBusiness : BaseBusiness, IOnlineExportBusiness
    {
		private readonly IPosSessionContext _sessionContext;

		public ContingencyExportBusiness(IDatabaseManager manager, IPosSessionContext sessionContext) : base(manager)
        {
			_sessionContext = sessionContext;
		}

        public virtual ValidationContractResult Export(POSTicketContract ticket)
        {
            var result = new ValidationContractResult();

            try
            {
                var cloudBusiness = BusinessContext.GetBusiness<ICloudBusiness>(Manager);

                // Export ticket to cloud
                var exportValidations = cloudBusiness.PersistTicket(ticket);
                if (exportValidations.IsEmpty)
                {
                    // Not need upload to cloud from sync
                    if (ticket.Sync.HasValue)
                        ticket.Sync = null;
                }
                else
                {
                    // Log comunication errors
                    cloudBusiness.TraceComunicationErrors(exportValidations);

                    result.AddValidations(cloudBusiness.GetComunicationValidations(exportValidations));
                    if (!result.IsEmpty)
                    {
                        var settingBusiness = BusinessContext.GetBusiness<ISettingBusiness>(Manager);
                        var settings = settingBusiness.GetGeneralSettings(_sessionContext.InstallationId);

                        if (!settings.AllowContingencySerie)
                            result.Add(exportValidations);
                        else
                            result.AddError(ErrorCodePos.DocExportationCommunication, string.Empty);
                    }
                    else
                        result.AddValidations(cloudBusiness.GetBusinessValidations(exportValidations));
                }

                if (result.IsEmpty)
                {
                    var ticketBusiness = BusinessContext.GetBusiness<ITicketBusiness>(Manager);
                    var persistTicketResult = ticketBusiness.PersistTicket(ticket);
                    if (!persistTicketResult.IsEmpty)
                        result.AddValidations(persistTicketResult);
                    else
                        result.Contract = persistTicketResult.Contract;
                }
            }
            catch (Exception ex)
            {
                result.AddError(ex.Message);
            }

            return result;
        }
    }

    #endregion
    #region Mex

    public class MexOnlineExportBusiness : ContingencyExportBusiness
    {
        public MexOnlineExportBusiness(IDatabaseManager manager, IPosSessionContext sessionContext) : base(manager, sessionContext)
        {
        }

        public override ValidationContractResult Export(POSTicketContract ticket)
        {
            return ticket.IsInvoice ? base.Export(ticket) : new ValidationContractResult { Contract = ticket };
        }
    }

    #endregion
    #region Peru
    public class PeOnlineExportBusiness : ContingencyExportBusiness
    {
        public PeOnlineExportBusiness(IDatabaseManager manager, IPosSessionContext sessionContext) : base(manager, sessionContext)
        {
        }

        public override ValidationContractResult Export(POSTicketContract ticket)
        {
            return !ticket.IsOnlyTicket ? base.Export(ticket) : new ValidationContractResult { Contract = ticket };
        }
    }

    #endregion
}