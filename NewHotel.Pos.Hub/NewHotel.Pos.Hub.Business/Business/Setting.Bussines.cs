﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NewHotel.Business;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using NewHotel.Core;
using NewHotel.DataAnnotations;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Business.SqlContainer.Common;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Settings;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Hub.Model.Cashier.Contracts.Settings;
using ZXing;
using HotelRecord = NewHotel.Pos.Hub.Model.HotelRecord;
using TaxSchemaRecord = NewHotel.Pos.Hub.Contracts.Cashier.Records.TaxSchemaRecord;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class SettingBusiness : BaseBusiness, ISettingBusiness
    {
        #region Constructor

        public SettingBusiness(
            IDatabaseManager manager
        )
        : base(manager) 
        {
        }

        #endregion
        #region ISettingBusiness

        public IEnumerable<TaxSchemaRecord> GetTaxSchemas(Guid hotelId, int language)
        {
            Manager.Open();
            try
            {
                var query = QueryFactory.Get<TaxSchemaQuery>(Manager);
                query.Parameters[TaxSchemaQuery.HotelIdParam] = hotelId;
                query.Parameters[TaxSchemaQuery.LanguageIdParam] = language;

                return query.ExecuteList<TaxSchemaRecord>();
            }
            finally
            {
                Manager.Close();
            }
        }

        public List<NationalityRecord> GetNationalities(int language)
        {
            var results = new List<NationalityRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetNationalities(Manager);
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var nationalityRecord = new NationalityRecord
                    {
                        CountryCode = record.ValueAs<string>("NACI_PK"),
                        Description = record.ValueAs<string>("LITE_DESC"),
                        RegionCode = record.ValueAs<int>("REMU_PK"),
                        LanguageCode = record.ValueAs<int>("LICL_PK"),
                        CallingCode = record.ValueAs<string>("NACI_CACO"),
                        ISO3166 = record.ValueAs<string>("ISO_3166"),
                        SEFCode = record.ValueAs<string>("PT_SEF"),
                        CountryAuxiliaryCode = record.ValueAs<string>("NACI_CAUX")
                    };

                    results.Add(nationalityRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        public List<HotelRecord> GetHotels(Guid userId)
        {
            var results = new List<HotelRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetHotel(Manager);
                query.Parameters["UTIL_PK"] = userId;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var hotelRecord = new HotelRecord
                    {
                        Id = record.ValueAs<Guid>("HOTE_PK"),
                        Abbreviation = record.ValueAs<string>("HOTE_ABRE"),
                        Description = record.ValueAs<string>("HOTE_DESC"),
                        FreeCode = record.ValueAs<string>("HOTE_CAUX"),
                        CountryID = record.ValueAs<string>("NACI_PK"),
                        KeppAliveTime = record.ValueAs<short>("HOTE_KALI"),
                        KeppAliveCount = record.ValueAs<short>("HOTE_EXPC"),
                        Synced = record.ValueAs<DateTime?>("HOTE_SYNC"),
                        AndroidEnabled = record.ValueAs<bool>("HOTE_ANDR")
                    };

                    results.Add(hotelRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        static AsyncLocal<Dictionary<Guid, POSGeneralSettingsRecord>> _cachedSettings = new();

        /// <summary>
        /// Gets the general settings from the database and maps them to the POSGeneralSettingsRecord's properties.
        /// </summary>
        /// <param name="hotelId">The id of the hotel to get the settings from</param>
        /// <param name="fromCloud">If the data comes from the cloud or not</param>
        /// <returns>A result with errors or not</returns>
        public POSGeneralSettingsRecord GetGeneralSettings(Guid hotelId, bool fromCloud = false)
        {
            _cachedSettings.Value ??= [];
            if (_cachedSettings.Value.TryGetValue(hotelId, out var cachedSettingsValue) && cachedSettingsValue is not null)
                return cachedSettingsValue;

            POSGeneralSettingsRecord generalSettingRecord = null;

            if (fromCloud)
            {
                var cloudBusiness = BusinessContext.GetBusiness<ICloudBusiness>(Manager);
                return _cachedSettings.Value[hotelId] = cloudBusiness.GetGeneralSettings().Item;
            }

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetGeneralSettings(Manager);
                query.Parameters["HOTE_PK"] = hotelId;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    generalSettingRecord = new POSGeneralSettingsRecord
                    {
                        Id = hotelId,
                        FastPayment = record.ValueAs<bool>("FAST_PAYM"),
                        FastPaymentFormId = record.ValueAs<Guid>("PARA_FORE"),
                        TaxesIncluded = record.ValueAs<bool>("PARA_IVIN"),
                        DefaultTaxSchemaId = record.ValueAs<Guid?>("PARA_ESIM"),
                        CloseWithOpenTickets = record.ValueAs<bool>("PARA_TICK"),
                        DefaultCancellationReasonId = record.ValueAs<Guid>("PARA_MTCO"),
                        AccountFolder = record.ValueAs<DailyAccountType>("PARA_TICO"),
                        UseNumericLogin = record.ValueAs<bool>("PARA_NPAS"),
                        ServiceTipId = record.ValueAs<Guid>("PARA_TISE"),
                        TipPercent = record.ValueAs<decimal?>("PARA_TIPE"),
                        TipValue = record.ValueAs<decimal?>("PARA_TIVL"),
                        AutomaticTip = record.ValueAs<bool>("PARA_TIAT"),
                        CashDrawerCode = record.ValueAs<string>("DRAW_CODE"),
                        LeaveMealPlanOpened = record.ValueAs<bool>("OPEN_PLAN"),
                        AddedValueOnHouseUse = record.ValueAs<decimal>("COIN_VALO"),
                        AddedValueOnDiscount = record.ValueAs<decimal>("DESC_VALO"),
                        FiscalNumberOnTickets = record.ValueAs<bool>("TICK_FISC"),
                        WorkDateOverSystemDate = record.ValueAs<bool>("DATR_DASY"),
                        FiscalType = CheckEnum(record.ValueAs<FiscalPOSType>("NPOS_TIFI"), FiscalPOSType.None),
                        CribNumberHotel = record.ValueAs<int?>("NPOS_NCHC"),
                        OnlineDocExportation = record.ValueAs<bool>("NPOS_FAON"),
                        AllowBallotDocument = record.ValueAs<bool>("NPOS_HBOL"),
                        AllowContingencySerie = record.ValueAs<bool>("NPOS_HSCO"),
                        ApplyAutomaticProductDiscount = record.ValueAs<bool>("NPOS_DEAU"),
                        SignType = CheckEnum(record.ValueAs<DocumentSign?>("SIGN_TIPO") ?? DocumentSign.None, DocumentSign.None),
                        SignVersion = record.ValueAs<long?>("SIGN_VERS"),
                        SignTestMode = record.ValueAs<bool>("SIGN_TEST"),
                        FiscalRegionCode = record.ValueAs<string>("REGI_CAUX"),
                        FiscalNumber = record.ValueAs<string>("FISC_NUMB"),
                        FiscalServiceUser = record.ValueAs<string>("FISC_USER"),
                        FiscalServicePassword = record.ValueAs<string>("FISC_PASW"),
                        Country = record.ValueAs<string>("NACI_PK"),
                        CancelInvoicesOnlyEmissionDate = record.ValueAs<bool>("ANUL_DATR"),
                        CancelInvoicesOnlyEmissionMonth = record.ValueAs<bool>("ANUL_MONTH"),
                        DaysToCancelInvoice = record.ValueAs<short?>("ANUL_DAYS"),
                        TicketCancelWindow = record.ValueAs<TimeSpan>("TICK_VECA"),
                        MandatoryValueForFiscalNumber = record.ValueAs<decimal?>("TICK_MAFN"),
                        RealTimeTicketSync = record.ValueAs<bool>("SYNC_VEND"),
                        FastPlan = record.ValueAs<bool>("FAST_PLAN"),
                        IsMandatoryFastPlanRoomInfo = record.ValueAs<bool>("PLAN_ROOM"),
                        EmitOnlyTickets = record.ValueAs<bool>("EMIT_SOTK"),
                        AutoLogOutTimerValue = record.ValueAs<float>("NPOS_TIME"),
                        UsingAutoLogOutTimer = record.ValueAs<bool>("NPOS_TIMF"),
                        ShowQuantityDecimalCase = record.ValueAs<bool>("PROD_FTQT"),
                        ShowInvoiceButton = record.ValueAs<bool>("NPOS_IBTN"),
                        AllowDigitalSign = record.ValueAs<bool>("NPOS_ASGN"),
                        AllowSelectSeparators = record.ValueAs<bool>("NPOS_ASEP"),
                        SkipFillFiscalDataAndPutFinalConsumer = record.ValueAs<bool>("NPOS_FCON"),
                        PaySystemInstallationId = record.ValueAs<Guid?>("PAYS_INTALL"),
                        AutoTableAfterAllToKitchen = record.ValueAs<bool>("NPOS_ATSK"),
                        AllowCreditNoteTicketReversal = record.ValueAs<bool>("TICK_NCRE"),
                    };

                    var path = record.ValueAs<string>("BLOB_PATH");
                    if (string.IsNullOrEmpty(path)) return generalSettingRecord;
                    generalSettingRecord.ImagePath = new Guid(MD5.Create().ComputeHash(Encoding.Default.GetBytes(path))).ToHex();

                    if (!Utils.ImagePaths.ContainsKey(generalSettingRecord.ImagePath))
                        Utils.ImagePaths.Add(generalSettingRecord.ImagePath, Utils.BuildImagePath(path));

                    return generalSettingRecord;
                }
            }
            finally
            {
                Manager.Close();
                _cachedSettings.Value[hotelId] = generalSettingRecord;
            }

            return generalSettingRecord ?? new POSGeneralSettingsRecord();
        }

        public List<CancellationTypesRecord> GetCancellationTypes(Guid hotelId, int language)
        {
            var results = new List<CancellationTypesRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetCancellationTypes(Manager);
                query.Parameters["hote_pk"] = hotelId;
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();

                foreach (IRecord record in records)
                {
                    var cancellationRecord = new CancellationTypesRecord
                    {
                        Id = record.ValueAs<Guid>("MTCO_PK"),
                        Description = record.ValueAs<string>("MTCO_DESC"),
                        Type = record.ValueAs<CancellationType>("MTCO_TICA")
                    };

                    results.Add(cancellationRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        public List<CreditCardTypesRecord> GetCreditCardTypes(Guid hotelId, int language)
        {
            var results = new List<CreditCardTypesRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetCreditCardTypes(Manager);
                query.Parameters["hote_pk"] = hotelId;
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var creditCardTypeRecord = new CreditCardTypesRecord
                    {
                        Id = record.ValueAs<Guid>("CACR_PK"),
                        Description = record.ValueAs<string>("CACR_DESC"),
                        CreditCard = record.ValueAs<bool>("CACR_CACR"),
                        DebitCard = record.ValueAs<bool>("CACR_CADE")
                    };

                    results.Add(creditCardTypeRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        public List<DiscountTypesRecord> GetDiscountTypes(Guid hotelId, int language)
        {
            var results = new List<DiscountTypesRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetDiscountTypes(Manager);
                query.Parameters["appl_pk"] = CloudBusiness.ApplicationId;
                query.Parameters["hote_pk"] = hotelId;
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var discountTypeRecord = new DiscountTypesRecord
                    {
                        Id = record.ValueAs<Guid>("TIDE_PK"),
                        Description = record.ValueAs<string>("TIDE_DESC"),
                        FixedDiscount = record.ValueAs<bool>("TIDE_FIXE"),
                        MinPercent = record.ValueAs<decimal>("TIDE_VMIN"),
                        MaxPercent = record.ValueAs<decimal>("TIDE_VMAX")
                    };

                    results.Add(discountTypeRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        public List<HouseUseRecord> GetHouseUses(Guid standId, int language)
        {
            var results = new List<HouseUseRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetHouseUses(Manager);
                query.Parameters["ipos_pk"] = standId;
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var houseUseRecord = new HouseUseRecord
                    {
                        Id = record.ValueAs<Guid>("COIN_PK"),
                        Description = record.ValueAs<string>("COIN_DESC"),
                        MonthlyLimit = record.ValueAs<decimal?>("PVCI_LIMI"),
                        Unlimited = record.ValueAs<bool>("PVCI_ILIM"),
                        LimitPercent = record.ValueAs<decimal?>("PVCI_PORL"),
                        IsInvitation = record.ValueAs<bool>("COIN_INVI")
                    };

                    results.Add(houseUseRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }


        public ValidationResult<PaymentMethodsRecord> GetPaymentMethod(Guid hotelId, Guid paymentMethodId, long language)
        {
            using (Manager.OpenSession())
            {
                var query = CommonQueryFactory.GetPaymentMethods(Manager);
                query.Parameters["hote_pk"] = hotelId;
                query.Parameters["lang_pk"] = language;
                query.Filters["FORE_PK"].Value = paymentMethodId;

                var record = query.BrowsePaymentMethods().FirstOrDefault();

                return record == null
                    ? new ValidationResult<PaymentMethodsRecord>().AddError(999, "Payment method not found")
                    : new ValidationResult<PaymentMethodsRecord>(record);
            }
        }


		public List<PaymentMethodsRecord> GetPaymentMethods(Guid hotelId, int language)
        {
            using (Manager.OpenSession())
            {
                var query = CommonQueryFactory.GetPaymentMethods(Manager);
                query.Parameters["hote_pk"] = hotelId;
                query.Parameters["lang_pk"] = language;
                query.Sorts.AddAsc("FORE_ORDE");

                return query.BrowsePaymentMethods().ToList();
            }
        }

        public List<SeparatorsRecord> GetSeparators(Guid hotelId, int language)
        {
            var results = new List<SeparatorsRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetSeparators(Manager);
                query.Parameters["hote_pk"] = hotelId;
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var separatorRecord = new SeparatorsRecord
                    {
                        Id = record.ValueAs<Guid>("SEPA_PK"),
                        Description = record.ValueAs<string>("SEPA_DESC"),
                        Order = record.ValueAs<short>("SEPA_ORDE"),
                        MaxCoversAllowed = record.ValueAs<short>("SEPA_MAXP"),
                        CumulativeRound = record.ValueAs<bool>("SEPA_ACUM"),
                        ShowedInTickets = record.ValueAs<bool>("SEPA_TICK")
                    };

                    results.Add(separatorRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        public List<TicketDutyRecord> GetTicketDuty(Guid hotelId, int language)
        {
            var results = new List<TicketDutyRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetTicketDuty(Manager);
                query.Parameters["hote_pk"] = hotelId;
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var ticketDutyRecord = new TicketDutyRecord
                    {
                        Id = record.ValueAs<Guid>("DUTY_PK"),
                        Description = record.ValueAs<string>("DUTY_DESC"),
                        InitialTime = record.ValueAs<DateTime>("DUTY_HORI"),
                        FinalTime = record.ValueAs<DateTime>("DUTY_HORF"),
                        TimeDelay = record.ValueAs<short>("DUTY_ETME")
                    };

                    results.Add(ticketDutyRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        public List<CategoriesRecord> GetButtonCategories(Guid hotelId, int language)
        {
            var results = new List<CategoriesRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetCategories(Manager);
                query.Parameters["hote_pk"] = hotelId;
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var buttonCategoryRecord = new CategoriesRecord
                    {
                        Id = record.ValueAs<Guid>("BCAT_PK"),
                        Description = record.ValueAs<string>("BCAT_DESC"),
                        Order = record.ValueAs<short>("BCAT_ORDE")
                    };

                    if (record.ValueAs<string>("BCAT_IMAG") != null)
                    {
                        buttonCategoryRecord.ImagePath = new Guid(MD5.Create().ComputeHash(Encoding.Default.GetBytes(record.ValueAs<string>("BCAT_IMAG")))).ToHex();

                        if (!Utils.ImagePaths.ContainsKey(buttonCategoryRecord.ImagePath))
                            Utils.ImagePaths.Add(buttonCategoryRecord.ImagePath, Utils.BuildImagePath(record.ValueAs<string>("BCAT_IMAG")));
                    }

                    results.Add(buttonCategoryRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        public List<SubCategoriesRecord> GetButtonSubCategories(Guid categoryId, int language)
        {
            var results = new List<SubCategoriesRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetSubCategories(Manager);
                query.Parameters["cate_pk"] = categoryId;
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var buttonSubCategoryRecord = new SubCategoriesRecord
                    {
                        Id = record.ValueAs<Guid>("BCAT_PK"),
                        Description = record.ValueAs<string>("BCAT_DESC"),
                        Order = record.ValueAs<short>("BCAT_ORDE")
                    };

                    if (record.ValueAs<string>("BCAT_IMAG") != null)
                    {
                        buttonSubCategoryRecord.ImagePath = new Guid(MD5.Create().ComputeHash(
                            Encoding.Default.GetBytes(record.ValueAs<string>("BCAT_IMAG")))).ToHex();

                        if (!Utils.ImagePaths.ContainsKey(buttonSubCategoryRecord.ImagePath))
                            Utils.ImagePaths.Add(buttonSubCategoryRecord.ImagePath, Utils.BuildImagePath(record.ValueAs<string>("BCAT_IMAG")));
                    }

                    results.Add(buttonSubCategoryRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        public List<ColumnsPlusProductsRecord> GetColumnsPlusProducts(Guid standId, Guid subCategoryId, int language)
        {
            var results = new List<ColumnsPlusProductsRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetColumnsPlusProducts(Manager);
                query.Parameters["ipos_pk"] = standId;
                query.Parameters["scat_pk"] = subCategoryId;
                query.Parameters["lang_pk"] = language;

                var products = new List<ColumnsPlusProductsRecord>();

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var columnPlusButtonRecord = new ColumnsPlusProductsRecord
                    {
                        ColumnId = record.ValueAs<Guid>("BCAT_PK"),
                        ColumnDescription = record.ValueAs<string>("BCAT_DESC"),
                        ProductId = record.ValueAs<Guid>("ARTG_PK"),
                        ProductDescription = record.ValueAs<string>("ARTG_DESC"),
                        CategoryOrden = record.ValueAs<short>("BCAT_ORDE"),
                        ProductOrder = record.ValueAs<short>("BOTV_ORDE")
                    };

                    products.Add(columnPlusButtonRecord);
                }

                // Grouping and Sorting 
                if (products.Any())
                {
                    var grouped = products.OrderBy(p => p.CategoryOrden).GroupBy(p => p.ColumnId);
                    foreach (var categoryGroup in grouped)
                        results.AddRange(categoryGroup.OrderBy(x => x.ProductOrder));
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        public List<HappyHourByStandRecord> GetHappyHoursByStand(Guid standId, int language)
        {
            var results = new List<HappyHourByStandRecord>();

            Manager.Open();
            try
            {
                var query = CommonQueryFactory.GetHappyHourByStands(Manager);
                query.Parameters["ipos_pk"] = standId;
                query.Parameters["lang_pk"] = language;

                var records = query.Execute();
                foreach (IRecord record in records)
                {
                    var happyHourRecord = new HappyHourByStandRecord
                    {
                        Id = record.ValueAs<Guid>("HAIP_PK"),
                        PeriodId = record.ValueAs<Guid>("HOUR_PK"),
                        Description = record.ValueAs<string>("HOUR_DESC"),
                        InitialTime = record.ValueAs<DateTime>("HOUR_HORI"),
                        FinalTime = record.ValueAs<DateTime>("HOUR_HORF"),
                        DaysOfWeek = record.ValueAs<string>("HOUR_DIAS"),
                        PriceRateId = record.ValueAs<Guid>("TPRG_PK"),
                        Quantity = record.ValueAs<short>("ARTG_CANT"),
                        Inactive = record.ValueAs<bool>("HOUR_INAC")
                    };

                    results.Add(happyHourRecord);
                }
            }
            finally
            {
                Manager.Close();
            }

            return results;
        }

        public ValidationResult ResetSyncStatus()
        {
            Manager.Open();
            try
            {
                Manager.BeginTransaction();
                try
                {
                    using (var command = Manager.CreateCommand("ResetSyncStatus"))
                    {
                        command.CommandText = "update tnht_hote set hote_sync = null";
                        Manager.ExecuteNonQuery(command);
                    }

                    Manager.CommitTransaction();
                    return new ValidationResult();
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    var result = new ValidationResult();
                    result.AddException(ex);
                    return result;
                }
            }
            finally
            {
                Manager.Close();
            }
        }

        public async Task<ValidationResult<string>> SyncLocalStock()
        {
            using(Manager.OpenSession())
            {
                
                Manager.BeginTransaction();
                try
                {
                    var result = new ValidationResult<string>();
                    var stocksManager = BusinessContext.GetBusiness<IStocksManager>(Manager);
                    var syncStockResult = await stocksManager.RefreshStocks();
                    if(syncStockResult.HasErrors)
                    {
                        Manager.RollbackTransaction();
                        result.AddError(syncStockResult.Errors.ToString());
                        return syncStockResult;
                    }
                    result.Result = syncStockResult.Result;

                    return result;
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    return new ValidationResult<string>().AddException(ex);
                }
            }
        }

        public ValidationItemSource<DownloadedImageContact> DownloadImages(KeyValuePair<string, DateTime>[] paths)
        {
            // var images = new List<DownloadedImageContact>();
            // var result = new ValidationItemSource<DownloadedImageContact> { ItemSource = images };

            var images = new ConcurrentBag<DownloadedImageContact>();
            var trace = new ConcurrentBag<string>();

            Parallel.ForEach(paths, path =>
            {
                var imgPath = Utils.BuildImagePath(path.Key);
                var fileInfo = new FileInfo(imgPath);
                if (fileInfo.Exists)
                {
                    try
                    {
                        if (path.Value == fileInfo.LastWriteTimeUtc) return;
                        var bytes = File.ReadAllBytes(imgPath);
                        //lock (images)
                        //{
                        images.Add(new DownloadedImageContact
                        {
                            PathHub = path.Key,
                            Image = bytes,
                            LastWriteTimeUtc = fileInfo.LastWriteTimeUtc
                        });
                        //}

                        // Trace.TraceInformation($"Image file {imgPath} loaded");
                        trace.Add($"Image file {imgPath} loaded");
                    }
                    catch (Exception ex)
                    {
                        // Trace.TraceInformation($"Error loading file {imgPath}: {ex.Message}");
                        trace.Add($"Error loading file {imgPath}: {ex.Message}");
                    }
                }
                else
                    trace.Add($"Image file {imgPath} not found");
            });

            var result = new ValidationItemSource<DownloadedImageContact> { ItemSource = images.ToList() };

            Trace.TraceInformation(string.Join("\n", trace));

            return result;
        }

        private static T CheckEnum<T>(T value, T defaultValue)
        {
            if (value != null && Enum.IsDefined(typeof(T), value))
            {
                return value;
            }

            return defaultValue;
        }

        #endregion
    }

    public static class SettingBusinessEx
    {
		public static IEnumerable<PaymentMethodsRecord> BrowsePaymentMethods(this Query query)
		{
			var records = query.Execute();
			foreach (IRecord record in records)
			{
				var paymentMethodRecord = new PaymentMethodsRecord
				{
					Id = record.ValueAs<Guid>("FORE_PK"),
					Abbreviation = record.ValueAs<string>("FORE_ABRE"),
					Description = record.ValueAs<string>("FORE_DESC"),
					CurrencyId = record.ValueAs<string>("UNMO_PK"),
					ConsideredCash = !record.ValueAs<bool>("FORE_CACR"),
					ConsideredCreditCard = record.ValueAs<bool>("FORE_CACR"),
					ShowedInCashierReport = record.ValueAs<bool>("REPO_CASH"),
					AllowCreditCard = record.ValueAs<bool>("FORE_CRED"),
					AllowDebitCard = record.ValueAs<bool>("FORE_DEBI"),
					PaymentChargeDigitalSignature = record.ValueAs<bool>("FORE_PCDS"),
					ImagePath = record.ValueAs<string>("FORE_PATH"),
					Order = record.ValueAs<short>("FORE_ORDE"),
					AuxiliarCode = record.ValueAs<string>("FORE_CAUX"),
					AllowInvoiceClick = record.ValueAs<bool>("FORE_INVO"),
					AllowTicketClick = record.ValueAs<bool>("FORE_TICK"),
					PaySystemOriginId = record.ValueAs<Guid?>("PAYS_ORIGIN"),
				};

				yield return paymentMethodRecord;
			}
		}
	}
}