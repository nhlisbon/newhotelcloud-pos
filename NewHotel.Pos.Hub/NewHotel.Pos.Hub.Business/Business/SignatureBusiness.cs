﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using NewHotel.Contracts;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Contracts.Common.DTOs.Request;
using NewHotel.Pos.Hub.Model.Session;

namespace NewHotel.Pos.Hub.Business.Business;

public class SignatureBusiness : ISignatureBusiness
{
    private readonly SignatureApiService _rest;

    public SignatureBusiness()
    {
        var baseUrl = ConfigurationManager.AppSettings["SignatureApiLocation"];
        if (!string.IsNullOrEmpty(baseUrl))
            _rest = SignatureApiService.Build(baseUrl);
    }


    public ValidationItemResult<Guid> RequestSignature(SignatureRequest request)
    {
        if (_rest == null) throw new NotImplementedException("Signature API location not configured.");
        {
            var result = new ValidationItemResult<Guid>();

            var response = _rest.RequestSignature(request).Result;
            if (response.IsSuccess)
            {
                result.Item = response.Data.Id;
                return result;
            }

            result.AddError(response.Error.Message);

            return result;
        }
    }

    public async Task<ValidationItemResult<string>> RetrieveSignature(Guid id)
    {
        if (_rest == null) throw new NotImplementedException("Signature API location not configured.");
        {
            var response = await _rest.RetrieveSignature(id);
            var result = new ValidationItemResult<string>
            {
                Item = response.Data.Image
            };

            return result;
        }
    }
}

internal class SignatureApiService
{
    private static readonly JsonSerializerOptions JsonOptions = new()
    {
        PropertyNameCaseInsensitive = true,
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
        Converters = { new JsonStringEnumConverter() }
    };

    private readonly Lazy<HttpClientHandler> _handler = new(() => new HttpClientHandler());
    private readonly HttpClient? _client;

    private SignatureApiService()
    {
    }

    private SignatureApiService(string baseUrl)
    {
        _client = new HttpClient(_handler.Value);
        _client.BaseAddress = new Uri(baseUrl.TrimEnd('/'));
    }

    private static string ToJson<T>(T obj)
    {
        return JsonSerializer.Serialize(obj, JsonOptions);
    }

    private static T? FromJson<T>(string json)
    {
        return JsonSerializer.Deserialize<T>(json, JsonOptions);
    }

    private async Task<R2> Post<R1, R2>(string uri, R1 body)
    {
        var httpContent = new StringContent(ToJson(body), Encoding.UTF8, "application/json");
        var response = await _client!.PostAsync(uri, httpContent);
        var content = await response.Content.ReadAsStringAsync();
        var result = FromJson<R2>(content);
        return result;
    }

    private async Task<R2> Get<R1, R2>(string uri, R1 path)
    {
        var response = await _client!.GetAsync($"api/signature/{path}");
        var content = await response.Content.ReadAsStringAsync();
        var result = FromJson<R2>(content);
        return result;
    }

    public static SignatureApiService Build(string baseUrl)
    {
        return new SignatureApiService(baseUrl);
    }

    public async Task<SignatureResponse> RequestSignature(SignatureRequest request)
    {
        return await Post<SignatureRequest, SignatureResponse>("api/signature", request);
    }

    public async Task<SignatureResponse> RetrieveSignature(Guid id)
    {
        return await Get<Guid, SignatureResponse>("api/signature", id);
    }
}

public class SignatureResponse
{
    public bool IsSuccess { get; set; }

    public ErrorResponse Error { get; set; }

    public DataResponse Data { get; set; }
}

public class ErrorResponse
{
    public string Code { get; set; }
    public string Message { get; set; }
}

public class DataResponse
{
    public Guid Id { get; set; }
    public string Description { get; set; }
    public string Value { get; set; }
    public string Image { get; set; }
}