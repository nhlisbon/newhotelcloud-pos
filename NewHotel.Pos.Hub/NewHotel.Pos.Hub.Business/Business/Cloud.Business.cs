﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.ServiceModel;
using System.Configuration;
using NewHotel.Business;
using NewHotel.Communication.Client;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using NewHotel.Core;
using NewHotel.DataAnnotations;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Business.PersistentObjects;
using NewHotel.Pos.Hub.Business.Requests;
using NewHotel.Pos.Hub.Business.Responses;
using NewHotel.Pos.Hub.Business.SqlContainer.Common;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Cashier;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries.Stands;
using NewHotel.Pos.Hub.Contracts;
using NewHotel.Pos.Hub.Core;
using NewHotel.Pos.Hub.Model.WaitingListStand.Contract;
using NewHotel.Pos.IoC;
using ClosedTicketRecord = NewHotel.Pos.Hub.Model.ClosedTicketRecord;
using System.Threading.Tasks;
using NewHotel.Pos.Hub.Contracts.Common.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.Records;
using NewHotel.Pos.Hub.Contracts.Common.Records.CurrentAccount;
using NewHotel.Contracts.DataProvider;
using NewHotel.Pos.Hub.Model.Session;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class CloudBusiness : BaseBusiness, ICloudBusiness
    {
        #region Members

        private readonly IPosSessionContext _sessionContext;

        private static string CloudLocation { get; set; }
        public static bool IsCloudApi { get; private set; }

        public static long ApplicationId => IsCloudApi ? 107 : 2;

        private readonly RestInvoke _rest;

        #endregion

        #region Constructors

        static CloudBusiness()
        {
            CloudLocation = ConfigurationManager.AppSettings["CloudApiLocation"];
            if (!string.IsNullOrEmpty(CloudLocation))
                IsCloudApi = true;
            else
                CloudLocation = ConfigurationManager.AppSettings["CloudLocation"];
        }

		public CloudBusiness(IDatabaseManager manager, IPosSessionContext sessionContext)
			: base(manager)
		{
            _sessionContext = sessionContext;
			if (!string.IsNullOrEmpty(CloudLocation) && IsCloudApi)
				_rest = new RestInvoke(CloudLocation, _sessionContext.InstallationId);
			else
				throw new ArgumentException("Expected CloudLocation parameter");
		}

		#endregion

		#region Validations

		public IEnumerable<Validation> GetBusinessValidations(ValidationResult validationResult)
        {
            return validationResult.Where(v => v.Code != ErrorCodePos.Communication);
        }

        public IEnumerable<Validation> GetComunicationValidations(ValidationResult validationResult)
        {
            return validationResult.Where(v => v.Code == ErrorCodePos.Communication);
        }

        public ValidationResult TraceComunicationErrors(ValidationResult validation)
        {
            return CwFactory.Resolve<ILogBusiness>().TraceError(GetComunicationValidations(validation));
        }

        public ValidationResult GetOfflineModeValidations(ValidationResult cloudResult,
            bool logOnCommunicationError = true)
        {
            var result = new ValidationResult();
            if (!cloudResult.IsEmpty)
            {
                if (logOnCommunicationError)
                {
                    // Log comunications errors
                    TraceComunicationErrors(cloudResult);
                }

                // Only check business errors
                result.AddValidations(GetBusinessValidations(cloudResult));
            }

            return result;
        }

        #endregion

        #region Private Methods

        private string GetUserName(Guid userId)
        {
            var qr = new QueryRequest();
            qr.AddFilter(UsersQuery.UserIdFilter, userId);

            var query = QueryFactory.Get<UsersQuery>(Manager);
            query.SetRequest(qr);
            return query.Execute().FirstOrDefault()?.ValueAs<string>(UsersQuery.DescriptionColumn);
        }

        private string GetCashierDescription(Guid cajaId)
        {
            var qr = new QueryRequest();
            qr.AddParameter(CashierQuery.LanguageIdParam, _sessionContext.LanguageId);
            qr.AddFilter(CashierQuery.CashierIdFilter, cajaId);

            var query = QueryFactory.Get<CashierQuery>(Manager);
            query.SetRequest(qr);
            return query.Execute().FirstOrDefault()?.ValueAs<string>(CashierQuery.DescriptionColumn);
        }

        private string GetStandDescription(Guid standId)
        {
            var qr = new QueryRequest();
            qr.AddParameter(StandByIdQuery.LanguageIdParam, _sessionContext.LanguageId);
            qr.AddParameter(StandByIdQuery.StandIdParam, standId);

            var query = QueryFactory.Get<StandByIdQuery>(Manager);
            query.SetRequest(qr);
            return query.Execute().FirstOrDefault()?.ValueAs<string>(StandByIdQuery.DescriptionColumn);
        }

        private POSTicketContract TicketContractFromCloud(TicketRContract ticket)
        {
            var result = new POSTicketContract
            {
                DocumentType = ticket.DocumentType,
                Account = ticket.Account,
                Caja = ticket.Caja,
                CajaPave = ticket.CajaPave,
                CancellationDate = ticket.AnulDate,
                CancellationSystemDate = ticket.AnulTime,
                ClosedDate = ticket.CloseDate,
                ClosedTime = ticket.CloseTime,
                Currency = ticket.Unmo,
                Duty = ticket.Duty,
                GeneralDiscount = ticket.GeneralDiscount,
                Id = ticket.Id,
                InternalUse = ticket.InternalUse,
                IsAnul = ticket.Anul.HasValue,
                Mesa = ticket.Mesa,
                Number = ticket.Serie,
                OpeningNumber = ticket.OpeningSerie,
                Observations = ticket.Observations,
                OpeningDate = ticket.CheckOpeningDate,
                OpeningTime = ticket.CheckOpeningTime,
                Paxs = ticket.Paxs,
                Process = ticket.Process,
                ReceiveCons = ticket.ReceiveCons,
                Recharge = ticket.Recharge,
                Serie = ticket.SeriePrex,
                Shift = ticket.Shift,
                Signature = ticket.Signature,
                Stand = ticket.Stand,
                TaxIncluded = ticket.TaxIncluded,
                TaxSchemaId = ticket.TaxSchemaId,
                Total = ticket.Valor,
                UserId = ticket.Util,
                FiscalDocAddress = ticket.FiscalDocInformationContract?.Address,
                FiscalDocEmiDate = ticket.FiscalDocInformationContract?.DocDate,
                FiscalDocFiscalNumber = ticket.FiscalDocInformationContract?.NIF,
                FiscalDocNacionality = ticket.FiscalDocInformationContract?.Nationality,
                FiscalDocNumber = ticket.FiscalDocInformationContract?.FactNumber,
                FiscalDocRegDate = ticket.FiscalDocInformationContract?.DocSysDateTime,
                FiscalDocSerie = ticket.FiscalDocInformationContract?.FactSerie,
                FiscalDocTo = ticket.FiscalDocInformationContract?.Holder,
                FiscalDocTotal = ticket.FiscalDocInformationContract?.Value,
                FiscalDocSignature = ticket.FiscalDocInformationContract?.FiscalSignature,
                FiscalDocValidationCode = ticket.ValidationCode,
                FiscalDocQrCode = ticket.FiscalDocQrCode,
                FiscalDocEmail = ticket.FiscalDocInformationContract?.Email,
                FpInvoiceNumber = ticket.FpInvoiceNumber,
                FpCreditNoteNumber = ticket.FpCreditNoteNumber,
                FpSerialCreditNote = ticket.FpSerialCreditNote,
                FpSerialInvoice = ticket.FpSerialInvoice,
                ValidationCode = ticket.ValidationCode,
                QrCode = ticket.QrCode,
                DigitalMenu = ticket.DigitalMenu,
                BaseCurrency = ticket.BaseCurrency,
                ExchangeFactor = ticket.ExchangeFactor,
            };

            // Remove nationality prefix code from invoice fiscal number.
            // Compatibility with old POS
            var invNacionality = result.FiscalDocNacionality;
            var fiscalNumber = result.FiscalDocFiscalNumber;
            if (!string.IsNullOrEmpty(invNacionality) &&
                !string.IsNullOrEmpty(fiscalNumber) &&
                fiscalNumber.IndexOf(invNacionality, StringComparison.Ordinal) == 0)
                result.FiscalDocFiscalNumber = fiscalNumber.Substring(invNacionality.Length);

            #region Cashier/Stand/User Description

            result.CashierDescription = GetCashierDescription(ticket.Caja);
            result.StandDescription = GetStandDescription(ticket.Stand);
            result.UserDescription = GetUserName(ticket.Util);
            if (ticket.CreditNoteUserId.HasValue)
                result.CreditNoteUserDescription = GetUserName(ticket.CreditNoteUserId.Value);

            #endregion

            #region Product Lines

            var ticketBussines = BusinessContext.GetBusiness<ITicketBusiness>(Manager);
            foreach (var productLine in ticket.ProductLines)
            {
                var contract = new POSProductLineContract();
                contract.Id = (Guid)productLine.Id;
                contract.ProductQtd = productLine.ProductQtd;
                contract.NetValue = productLine.LiqValue;
                contract.CostValue = productLine.Cost;
                contract.GrossValue = productLine.Importe;
                contract.ValueBeforeDiscount = productLine.ValueBeforeDiscount;
                contract.DiscountValue = productLine.Discount ?? decimal.Zero;
                contract.DiscountPercent = productLine.DiscountPercent;
                contract.DiscountTypeDescription = productLine.DiscountTypeDescription;
                contract.Recharge = productLine.Recharge;
                contract.AnnulmentCode = (short)productLine.IsAnul;
                contract.AnulAfterClosed = productLine.AnulAfterClosed;
                contract.SendToAreaStatus = productLine.SendToAreaStatus;
                contract.SendToAreaStartTime = productLine.SendToAreaStartTime;
                contract.SendToAreaEndTime = productLine.SendToAreaEndTime;
                contract.Printed = productLine.Printed;
                contract.IsHappyHour = productLine.IsHappyHour;
                contract.IsAditional = productLine.Aditional;
                contract.MainProduct = productLine.MainProduct;
                contract.FirstIvaId = productLine.FirstIva;
                contract.FirstIvaPercent = productLine.Percent1;
                contract.FirstIvaValue = productLine.FirstIvas;
                contract.SecondIvaId = productLine.SecodIva;
                contract.SecondIvaPercent = productLine.Percent2;
                contract.SecondIvaValue = productLine.SecondIvas;
                contract.ThirdIvaId = productLine.ThirdIva;
                contract.ThirdIvaPercent = productLine.Percent3;
                contract.ThirdIvaValue = productLine.ThirdIvas;
                contract.Separator = productLine.Separator;
                contract.SeparatorDescription = null;
                contract.AreaId = null;
                contract.Areas.Clear();
                contract.Product = productLine.Product;
                contract.ProductCode = productLine.ProductCode;
                contract.ProductAbbreviation = null;
                contract.ProductDescription = productLine.ProductDesc;
                contract.Caja = productLine.Caja;
                contract.Stand = productLine.Stand;
                contract.DiscountTypeId = null;
                contract.HasManualSeparator = false;
                contract.HasManualPrice = false;
                contract.AutomaticDiscount = productLine.AutomaticDiscount;
                contract.IsTip = false;
                contract.PaxNumber = productLine.PaxNumber;
                contract.Observations = productLine.Observations;
                contract.ObservationsMarch = productLine.ObservationsMarch;
                contract.UserId = productLine.Util;
                contract.UserDescription = null;
                contract.Alcoholic = productLine.Alcoholic;
                contract.ItemNumber = productLine.ItemNumber;
                contract.LastModified = productLine.LastModified;

                var taxSchemaId = ticket.TaxSchemaId ?? _sessionContext.TaxSchemaId;
                ticketBussines.LoadTaxesData(contract, _sessionContext.Country, taxSchemaId, _sessionContext.InstallationId, ticket.TaxIncluded, ticket);

                result.ProductLineByTicket.Add(contract);
            }

            #endregion

            #region Payment Lines

            foreach (var item in ticket.PaymentLines)
            {
                var line = new POSPaymentLineContract(
                    (Guid)item.Id,
                    item.Tire,
                    item.TireDesc,
                    item.Payment,
                    item.PaymentDesc,
                    item.CreditCard,
                    null,
                    item.Account,
                    item.Coin,
                    null,
                    item.Observations,
                    item.Valo,
                    item.Received == 0 ? item.Valo : item.Received, 0,
                    item.CurrencyId, item.ExchangeCurrency, item.MultCurrency);

                result.PaymentLineByTicket.Add(line);
            }

            #endregion

            return result;
        }

        private T CommunicationError<T>() where T : ValidationResult, new()
        {
            return new T().AddError(ErrorCodePos.Communication, "Cloud response unavailable");
        }

        private T Call<T>(Func<T> function) where T : ValidationResult, new()
        {
            var result = new T();

            try
            {
                var response = function();
                return response ?? CommunicationError<T>();
            }
            catch (TargetInvocationException e)
            {
                if (e.InnerException is EndpointNotFoundException notFound)
                    result.AddError(ErrorCodePos.Communication, notFound.Message);
                else if (e.InnerException != null)
                {
                    if (e.InnerException.InnerException != null)
                        result.AddError(ErrorCodePos.General, e.InnerException.InnerException.Message);
                    else
                        result.AddError(ErrorCodePos.General, e.InnerException.Message);
                }
                else
                    result.AddError(ErrorCodePos.General, e.Message);

                CwFactory.Resolve<ILogBusiness>().TraceError(e.Message);
            }
            catch (CommunicationException e)
            {
                result.AddError(ErrorCodePos.Communication, e.Message);
                CwFactory.Resolve<ILogBusiness>().TraceError(e.Message);
            }
            catch (Exception e)
            {
                result.AddError(ErrorCodePos.General, e.Message);
                CwFactory.Resolve<ILogBusiness>().TraceError(e.Message);
            }

            return result;
        }

        private NewHotelWPFPosClient GetSilverlightProxy()
        {
            var hubSessionData = BaseService.GetHubSessionData(HubContextCache.Current.Token);

            var userId = hubSessionData.UserId;
            var userLogin = hubSessionData.UserLogin;
            var userPassword = CryptUtils.Decrypt(hubSessionData.UserPassword);
            var installationId = hubSessionData.InstallationId;

            var sessionData = new HubSessionData(userId, userLogin, userPassword, installationId);

            var contextData = new ContextData(0, true);
            return new NewHotelWPFPosClient(sessionData, contextData, CloudLocation);
        }

        private static QueryRequest GetRequest(TablesReservationFilterContract filter)
        {
            var qr = new QueryRequest();

            if (filter.TableReservationId.HasValue)
                qr.AddFilter("litr_pk", filter.TableReservationId.Value);

            if (filter.SaloonIds != null && filter.SaloonIds.Length > 0)
                qr.AddFilter("salo_pk", filter.SaloonIds.Cast<object>().ToArray(), FilterTypes.In);

            if (filter.StandId.HasValue)
                qr.AddFilter("ipos_pk", filter.StandId.Value);

            if (filter.InDate.HasValue)
                qr.AddFilter("lire_daen", filter.InDate.Value);

            if (filter.KindReservation.HasValue)
                qr.AddFilter("lire_tres", filter.KindReservation.Value);

            if (filter.StateReservation.HasValue)
                qr.AddFilter("litr_esta", (long)filter.StateReservation.Value);

            if (!string.IsNullOrEmpty(filter.Email))
                qr.AddFilter("clie_emai", filter.Email);

            if (!string.IsNullOrEmpty(filter.Name))
                qr.AddFilter("clie_name", filter.Name);

            if (!string.IsNullOrEmpty(filter.Phone))
                qr.AddFilter("clie_phone", filter.Phone);

            return qr;
        }

        #endregion

        #region ITicketBusinessCloud

        public ValidationResult CancelTicket(Guid ticketId, Guid? cancellationReasonId, string comments)
        {
            return Call<ValidationResult>(() =>
            {
                if (_rest != null)
                {
                    var request = new VoidTicketRequest();
                    request.TicketId = ticketId;
                    request.CancellationReasonId = cancellationReasonId;
                    request.Comments = comments;
                    var response = _rest.Post<ValidationResponse>("posintegration/voidticket", request);
                    var result = new ValidationResult();
                    response.Data.AppendValidations(result);
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    return proxy.VoidTicketSync(ticketId, cancellationReasonId, comments) as ValidationResult;
                }
            });
        }

        public ValidationContractResult LoadTicket(Guid ticketId)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var response = _rest.Get<ValidationContentResponse<TicketRContract>>("posintegration/loadticket?id=" + ticketId.ToString());
                    var result = new ValidationContractResult();
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors)
                        result.Contract = TicketContractFromCloud(response.Data.Content);
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    var cloudResult = proxy.LoadTicketRAsync(ticketId) as ValidationContractResult;
                    if (cloudResult != null)
                    {
                        var result = new ValidationContractResult();
                        if (cloudResult.IsEmpty)
                            result.Contract = TicketContractFromCloud((TicketRContract)cloudResult.Contract);
                        else
                            result.Add(cloudResult);
                        return result;
                    }
                }

                return null;
            });
        }

        public ValidationContractResult LoadTicketReal(Guid ticketId)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var response = _rest.Get<ValidationContentResponse<TicketRContract>>("posintegration/loadticket?id=" + ticketId.ToString());
                    var result = new ValidationContractResult();
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors)
                        result.Contract = response.Data.Content;
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    return proxy.LoadTicketRAsync(ticketId) as ValidationContractResult;
                }
            });
        }

        public ValidationResult PersistTicket(POSTicketContract ticket)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var response = _rest.Post<ValidationResponse>("posintegration/updateticket", ticket);
                    var result = new ValidationResult();
                    response.Data.AppendValidations(result);
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    return proxy.UpdatePOSTicketSync(ticket) as ValidationResult;
                }
            });
        }

        public ValidationResult ChargeAccount(POSTicketContract ticket)
        {
            return Call<ValidationResult>(() =>
            {
                if (_rest != null)
                {
                    var response = _rest.Post<ValidationResponse>("posintegration/chargeticket", ticket);
                    var result = new ValidationResult();
                    response.Data.AppendValidations(result);
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    return proxy.ChargePOSTicketAccountSync(ticket) as ValidationResult;
                }
            });
        }

        public ValidationItemSource<ClosedTicketRecord> GetClosedTickets(QueryRequest request)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var queryRequest = new ClosedTicketsRequest();
                    queryRequest.StandId = (Guid)request.GetFilter("ipos_pk").Value;
                    queryRequest.CashierId = (Guid)request.GetFilter("caja_pk").Value;
                    queryRequest.ClosedDate = (DateTime)request.GetFilter("vend_datf").Value;
                    var response = _rest.Post<QueryResponse<ClosedTicketResponse>>("posintegration/closedtickets", queryRequest);
                    var result = new ValidationItemSource<ClosedTicketRecord>();
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors)
                        result.ItemSource = response.Data.Content.Data.Select(x => new ClosedTicketRecord(x.GetRecord())).ToList();
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    if (proxy.GetClosedTicketsSync(request) is QueryResponse cloudResponse)
                    {
                        return new ValidationItemSource<ClosedTicketRecord>
                        {
                            ItemSource = cloudResponse.Cast<NewHotel.Contracts.Pos.Records.ClosedTicketRecord>()
                                .Select(x => new ClosedTicketRecord(x)).ToList()
                        };
                    }
                }

                return null;
            });
        }

        #endregion

        #region IReservationTableBusinessCloud

        public ValidationItemSource<StandTimeSlotRecord> GetStandTimeSlots(Guid standId, DateTime date)
        {
            return Call(() =>
            {
                var result = new ValidationItemSource<StandTimeSlotRecord>();
                var response = _rest.Get<QueryResponse<StandTimeSlotsResponse>>("posintegration/timeslots?id=" + standId);
                response.Data.AppendValidations(result);
                if (!result.HasErrors)
                {
                    result.ItemSource = response.Data.Content.Data.Select(x => x.GetRecord()).ToList();
                }
                return result;
            });
        }

        /// <summary>
        /// searchtable
        /// </summary>
        public ValidationItemSource<LiteTablesReservationRecord> GetTableReservations(TablesReservationFilterContract filter)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var request = new TableReservationFilterRequest
                    {
                        TableReservationId = filter.TableReservationId,
                        InDate = filter.InDate,
                        StandId = filter.StandId,
                        SaloonIds = filter.SaloonIds,
                        KindReservation = filter.KindReservation,
                        StateReservation = filter.StateReservation,
                        ClientName = filter.Name,
                        ClientPhone = filter.Phone,
                        ClientEmail = filter.Email,
                        SlotId = filter.SlotId
                    };
                    var response = _rest.Post<QueryResponse<LiteTablesReservationResponse>>("posintegration/searchtable", request);
                    var result = new ValidationItemSource<LiteTablesReservationRecord>();
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors)
                        result.ItemSource = response.Data.Content.Data.Select(x => x.GetRecord()).ToList();
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    var request = GetRequest(filter);
                    var cloudResponse = proxy.GetLiteTablesReservationsSync(request) as QueryResponse;
                    if (cloudResponse != null)
                    {
                        var result = new ValidationItemSource<LiteTablesReservationRecord>();
                        if (!cloudResponse.IsEmpty)
                            result.AddValidations(cloudResponse.Errors.Values.Select(e => new ValidationError(e)));
                        else
                            result.ItemSource = cloudResponse.Cast<LiteTablesReservationRecord>().ToList();
                        return result;
                    }

                    return null;
                }
            });
        }

        /// <summary>
        /// loadtablesreservationline
        /// </summary>
        public ValidationContractResult LoadTablesReservation(Guid reservationTableId)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var response = _rest.Get<ValidationContentResponse<TablesReservationLineContract>>("posintegration/loadtablesreservationline?id=" + reservationTableId);
                    var result = new ValidationContractResult();
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors)
                        result.Contract = response.Data.Content;
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    return proxy.LoadTablesReservationLineSync(reservationTableId) as ValidationContractResult;
                }
            });
        }

        /// <summary>
        /// updatetable
        /// </summary>
        public ValidationContractResult PersistTablesReservationLine(TablesReservationLineContract request)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var response = _rest.Post<ValidationContentResponse<TablesReservationLineContract>>("posintegration/updatetable", request);
                    var result = new ValidationContractResult();
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors)
                        result.Contract = response.Data.Content;
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    return proxy.UpdateTablesReservationLine(request);
                }
            });
        }

        public ValidationContractResult LoadTableInfo(Guid table)
        {
            return Call(() =>
            {
                var response = _rest.Get<ValidationContentResponse<TablesReservationLineContract>>("posintegration/loadtableinfo?id=");
                var result = new ValidationContractResult();
                response.Data.AppendValidations(result);
                if (!result.HasErrors)
                    result.Contract = response.Data.Content;
                return result;
            });
        }

        /// <summary>
        /// checkintable
        /// </summary>
        public ValidationResult CheckInBookingTable(Guid bookingTableId)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var response = _rest.Get<ValidationResponse>("posintegration/checkintable?id=" + bookingTableId.ToString());
                    var result = new ValidationResult();
                    response.Data.AppendValidations(result);
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    return proxy.CheckInBookingTableSync(bookingTableId) as ValidationResult;
                }
            });
        }

        /// <summary>
        /// undocheckintable
        /// </summary>
        public ValidationResult UndoCheckInBookingTable(Guid bookingTableId)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var response = _rest.Get<ValidationResponse>("posintegration/undocheckintable?id=" + bookingTableId.ToString());
                    var result = new ValidationResult();
                    response.Data.AppendValidations(result);
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    return proxy.UndoCheckInBookingTableSync(bookingTableId) as ValidationResult;
                }
            });
        }

        /// <summary>
        /// checkouttable
        /// </summary>
        public ValidationResult CheckOutBookingTable(Guid bookingTableId)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var response = _rest.Get<ValidationResponse>("posintegration/checkouttable?id=" + bookingTableId.ToString());
                    var result = new ValidationResult();
                    response.Data.AppendValidations(result);
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    return proxy.CheckOutBookingTableSync(bookingTableId) as ValidationResult;
                }
            });
        }

        /// <summary>
        /// undocheckouttable
        /// </summary>
        public ValidationResult UndoCheckOutBookingTable(Guid bookingTableId)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var response = _rest.Get<ValidationResponse>("posintegration/undocheckouttable?id=" + bookingTableId.ToString());
                    var result = new ValidationResult();
                    response.Data.AppendValidations(result);
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    return proxy.UndoCheckOutBookingTableSync(bookingTableId) as ValidationResult;
                }
            });
        }

        /// <summary>
        /// cancelbookingtable
        /// </summary>
        public ValidationResult CancelBookingTable(Guid bookingTableId, Guid cancellationReasonId, string comment)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var request = new CancelBookingTableRequest();
                    request.BookingTableId = bookingTableId;
                    request.CancellationControlContract = new CancellationControlContract();
                    request.CancellationControlContract.CancellationReasonId = cancellationReasonId;
                    request.CancellationControlContract.Comment = comment;
                    var response = _rest.Post<ValidationResponse>("posintegration/cancelbookingtable", request);
                    var result = new ValidationResult();
                    response.Data.AppendValidations(result);
                    return result;
                }
                else
                {
                    var stand = new Stand(Manager, _sessionContext.StandId);
                    var contract = new CancellationControlContract(stand.StandWorkDate,
                        DateTime.Now, _sessionContext.UserId, null, comment);
                    contract.CancellationReasonId = cancellationReasonId;
                    var proxy = GetSilverlightProxy();
                    var cloudResponse = proxy.CancelTablesReservationSync(bookingTableId, contract) as ValidationResult[];
                    if (cloudResponse != null)
                    {
                        var result = new ValidationResult();
                        foreach (var response in cloudResponse)
                            result.Add(response);
                        return result;
                    }

                    return null;
                }
            });
        }

        /// <summary>
        /// undocancelbookingtable
        /// </summary>
        public ValidationResult UndoCancelBookingTable(Guid bookingTableId)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var response = _rest.Get<ValidationResponse>("posintegration/undocancelbookingtable?id=" + bookingTableId.ToString());
                    var result = new ValidationResult();
                    response.Data.AppendValidations(result);
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    return proxy.UndoCancelTablesReservationSync(bookingTableId) as ValidationResult;
                }
            });
        }

        /// <summary>
        /// loadtablesgroup
        /// </summary>
        public ValidationContractResult LoadTablesGroup(Guid tablesGroupId)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var response = _rest.Get<ValidationContentResponse<TablesGroupContract>>("posintegration/loadtablesgroup?id=" + tablesGroupId.ToString());
                    var result = new ValidationContractResult();
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors)
                        result.Contract = response.Data.Content;
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    return proxy.LoadTablesGroupSync(tablesGroupId) as ValidationContractResult;
                }
            });
        }

        #endregion

        #region ISettingManagementBusinessCloud

        /// <summary>
        /// no implementado en HTML5
        /// </summary>
        public ExportOfficialDocumentResult GetExportOfficialDocument(Guid officialDocId)
        {
            return Call(() =>
            {
                if (_rest != null)
                    throw new NotImplementedException("GetExportOfficialDocument");
                else
                {
                    var proxy = GetSilverlightProxy();
                    return proxy.GetExportOfficialDocumentSync(officialDocId);
                }
            });
        }

        public ValidationItemResult<POSGeneralSettingsRecord> GetGeneralSettings()
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var response = _rest.Get<QueryResponse<POSGeneralSettingsRecord>>("posintegration/settings");
                    var result = new ValidationItemResult<POSGeneralSettingsRecord>();
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors)
                        result.Item = response.Data.Content.Data?.FirstOrDefault() as POSGeneralSettingsRecord;
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    if (proxy.GetGeneralSettingsSync() is QueryResponse queryResponse)
                    {
                        return new ValidationItemResult<POSGeneralSettingsRecord>
                        {
                            Item = queryResponse.Data?.FirstOrDefault() as POSGeneralSettingsRecord
                        };
                    }
                }

                return null;
            });
        }

        public ValidationItemSource<POSTicketPrintConfigurationRecord> GetTicketPrintConfigurations()
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var response = _rest.Get<QueryResponse<POSTicketPrintConfigurationRecord>>("posintegration/setting/ticket");
                    var result = new ValidationItemSource<POSTicketPrintConfigurationRecord>();
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors)
                        result.ItemSource = response.Data.Content.Data;
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    if (proxy.GetTicketPrintConfigurationsSync() is QueryResponse response)
                    {
                        var result = new ValidationItemSource<POSTicketPrintConfigurationRecord>();
                        if (response.Errors == null || response.Errors.Count == 0)
                            result.ItemSource = response.Cast<POSTicketPrintConfigurationRecord>().ToList();
                        else
                            result.AddValidations(response.Errors.Values.Select(x => new ValidationError(x)));
                        return result;
                    }
                }

                return null;
            });
        }

        public ValidationItemSource<POSStandRecord> GetStandsFromCloud(int language)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var response = _rest.Get<QueryResponse<POSStandRecord>>("posintegration/stands?languageId=" + language.ToString());
                    var result = new ValidationItemSource<POSStandRecord>();
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors)
                        result.ItemSource = response.Data.Content.Data;
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    if (proxy.GetStandsFromCloudSync(language) is QueryResponse response)
                    {
                        var result = new ValidationItemSource<POSStandRecord>();
                        if (response.Errors == null || response.Errors.Count == 0)
                            result.ItemSource = response.Cast<POSStandRecord>().ToList();
                        else
                            result.AddValidations(response.Errors.Values.Select(x => new ValidationError(x)));
                        return result;
                    }
                }

                return null;
            });
        }

        public ValidationContractResult CloseDay(Guid standId, Guid? cajaId, DateTime? closeDay)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var queryRequest = new CloseDayRequest();
                    queryRequest.StandId = standId;
                    queryRequest.CashierId = cajaId;
                    queryRequest.DateToClose = closeDay;
                    var response = _rest.Post<ValidationContentResponse<CloseResponse>>("posintegration/close/day", queryRequest);
                    var result = new ValidationContractResult();
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors && response.Data.Content != null)
                    {
                        var contract = new StandTurnDateContract();
                        contract.WorkDate = response.Data.Content.WorkDate;
                        contract.Turn = response.Data.Content.Turn;
                        result.Contract = contract;
                    }

                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    return proxy.CloseDaySync(standId, cajaId, closeDay) as ValidationContractResult;
                }
            });
        }

        public ValidationContractResult UndoCloseDay(Guid standId, Guid? cajaId, DateTime? closeDay)
        {
            return Call(() =>
            {
                if (_rest == null) throw new NotImplementedException("UndoCloseDay");
                {
                    var queryRequest = new CloseDayRequest
                    {
                        StandId = standId,
                        CashierId = cajaId,
                        DateToClose = closeDay
                    };
                    var response = _rest.Post<ValidationContentResponse<CloseResponse>>("posintegration/close/day/undo", queryRequest);
                    var result = new ValidationContractResult();
                    response.Data.AppendValidations(result);
                    if (result.HasErrors) return result;

                    var contract = new StandTurnDateContract
                    {
                        WorkDate = response.Data.Content.WorkDate,
                        Turn = response.Data.Content.Turn
                    };

                    result.Contract = contract;

                    return result;
                }
            });
        }

        public ValidationContractResult CloseTurn(Guid standId, Guid cajaId)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var queryRequest = new CloseTurnRequest();
                    queryRequest.StandId = standId;
                    queryRequest.CashierId = cajaId;
                    var response = _rest.Post<ValidationContentResponse<CloseResponse>>("posintegration/close/turn", queryRequest);
                    var result = new ValidationContractResult();
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors && response.Data.Content != null)
                    {
                        var contract = new StandTurnDateContract();
                        contract.WorkDate = response.Data.Content.WorkDate;
                        contract.Turn = response.Data.Content.Turn;
                        result.Contract = contract;
                    }

                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    return proxy.CloseTurnSync(standId, cajaId) as ValidationContractResult;
                }
            });
        }

        public ValidationResult UpdateVersions(string dbVersion, string syncVersion, string appVersion)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var request = new UpdateVersionsRequest();
                    request.DbVersion = dbVersion;
                    request.SyncVersion = syncVersion;
                    request.NewPosVersion = appVersion;
                    var response = _rest.Post<ValidationResponse>("posintegration/version/update", request);
                    var result = new ValidationContractResult();
                    response.Data.AppendValidations(result);
                    return result;
                }
                else
                    throw new NotImplementedException("UpdateVersions");
            });
        }

        public Task<ValidationResult> UpdateVersionsAsync(string dbVersion, string syncVersion, string appVersion)
        {
            var taskResult = Task.Run(() =>
            {
                return Call(() =>
                {
                    if (_rest != null)
                    {
                        var request = new UpdateVersionsRequest();
                        request.DbVersion = dbVersion;
                        request.SyncVersion = syncVersion;
                        request.NewPosVersion = appVersion;
                        var response = _rest.Post<ValidationResponse>("posintegration/version/update", request);
                        var result = new ValidationContractResult();
                        response.Data.AppendValidations(result);
                        return (ValidationResult)result;
                    }
                    else
                        throw new NotImplementedException("UpdateVersions");
                });
            });

            return taskResult;
        }

        #endregion

        #region IReservationAccountBusinessCloud

        public ValidationItemSource<ReservationRecord> GetReservations(ReservationFilterContract filter)
        {
            return Call(() =>
            {
                var response = _rest.Post<QueryResponse<ReservationRecord>>($"posintegration/account/reservations", filter);
                var result = new ValidationItemSource<ReservationRecord>();
                response.Data.AppendValidations(result);
                if (!result.HasErrors)
                    result.ItemSource = response.Data.Content.Data;
                return result;
            });
        }

        public ValidationItemResult<PmsReservationRecord> GetPmsReservation(PmsReservationFilterModel filter)
        {
            return Call(() =>
            {
                var result = new ValidationItemResult<PmsReservationRecord>();
                
                var response = _rest.Post<QueryResponse<PmsReservationRecord>>($"posintegration/account/reservations", filter);
                response.Data.AppendValidations(result);
                if (!result.HasErrors)
                {
                    if(response.Data.Content.Data.Count == 0)
                        result.AddError("More than one reservation found");
                    else
                        result.Item = response.Data.Content.Data.FirstOrDefault();
                }

                return result;
            });
        }

        public ValidationItemSource<EventReservationSearchFromExternalRecord> GetEventReservations(Guid? installationId)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var response = _rest.Get<QueryResponse<EventReservationSearchFromExternalResponse>>($"posintegration/account/events?hotel={installationId.AsString()}");
                    var result = new ValidationItemSource<EventReservationSearchFromExternalRecord>();
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors)
                        result.ItemSource = response.Data.Content.Data.Select(x => x.GetRecord()).ToList();
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    if (proxy.GetEventReservationsSync(new QueryRequest()) is QueryResponse response)
                    {
                        var result = new ValidationItemSource<EventReservationSearchFromExternalRecord>();
                        if (response.Errors == null || response.Errors.Count == 0)
                            result.ItemSource = response.Cast<EventReservationSearchFromExternalRecord>().ToList();
                        else
                            result.AddValidations(response.Errors.Values.Select(x => new ValidationError(x)));
                        return result;
                    }
                }

                return null;
            });
        }

        public ValidationItemSource<SpaReservationSearchFromExternalRecord> GetSpaReservations(SpaSearchReservationFilterModel filter)
        {
            return Call(() =>
            {
                if (_rest == null) throw new NotImplementedException("Cloud API location not configured.");
                {
                    var response = _rest.Post<QueryResponse<SpaReservationSearchFromExternalResponse>>("posintegration/account/spa", filter);
                    var result = new ValidationItemSource<SpaReservationSearchFromExternalRecord>();
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors)
                        result.ItemSource = response.Data.Content.Data.Select(x => x.GetRecord()).ToList();
                    return result;
                }
            });
        }

        public ValidationItemSource<PmsControlAccountRecord> GetControlAccounts(PmsControlAccountFilterModel filter)
        {
            return Call(() =>
            {
                // TODO: I have to separate this call in two methods
                if (filter.GetDeposits)
                {
                    var response = _rest.Post<ValidationContentListResponse<PmsControlAccountRecord>>("posintegration/account/control/deposits", filter);
                    var result = new ValidationItemSource<PmsControlAccountRecord>();
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors)
                        result.ItemSource = response.Data.Content;
                    return result;

                }
                else
                {
                    var response = _rest.Post<QueryResponse<PmsControlAccountRecord>>($"posintegration/account/control", filter);
                    var result = new ValidationItemSource<PmsControlAccountRecord>();
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors)
                        result.ItemSource = response.Data.Content.Data;
                    return result;
                }
            });
        }
        
        public ValidationItemSource<ControlAccountMovementDto> GetControlAccountMovements(ControlAccountMovementsFilterModel filter)
        {
            return Call(() =>
            {
                var response = _rest.Post<QueryResponse<ControlAccountMovementDto>>($"posintegration/account/control/movements", filter);
                var result = new ValidationItemSource<ControlAccountMovementDto>();
                response.Data.AppendValidations(result);
                if (!result.HasErrors)
                    result.ItemSource = response.Data.Content.Data;
                return result;
            });
        }

        public ValidationItemSource<ReservationsGroupsRecord> GetReservationsGroups(Guid? installationId)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var response = _rest.Get<QueryResponse<ReservationsGroupsResponse>>($"posintegration/account/groups?hotel={installationId.AsString()}");
                    var result = new ValidationItemSource<ReservationsGroupsRecord>();
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors)
                        result.ItemSource = response.Data.Content.Data.Select(x => x.GetRecord()).ToList();
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    var request = new QueryRequest();
                    request.AddFilter("ccco_not_null", ArgumentValue.Null);
                    if (proxy.GetReservationsGroupsSync(request) is QueryResponse response)
                    {
                        var result = new ValidationItemSource<ReservationsGroupsRecord>();
                        if (response.Errors == null || response.Errors.Count == 0)
                            result.ItemSource = response.Cast<ReservationsGroupsRecord>().ToList();
                        else
                            result.AddValidations(response.Errors.Values.Select(x => new ValidationError(x)));
                        return result;
                    }
                }

                return null;
            });
        }

        public ValidationItemSource<PmsCompanyRecord> GetEntityInstallations(PmsCompanyFilterModel filter)
        {
            return Call(() =>
            {
                var response = _rest.Post<QueryResponse<PmsCompanyRecord>>($"posintegration/account/companies", filter);
                var result = new ValidationItemSource<PmsCompanyRecord>();
                response.Data.AppendValidations(result);
                if (!result.HasErrors)
                    result.ItemSource = response.Data.Content.Data;
                return result;
            });
        }

        public ValidationItemSource<ClientInstallationRecord> GetClientInstallations(bool openAccounts, Guid? clientId = null, string room = null, string fullName = null,
            Guid? installationId = null)
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var response = _rest.Get<QueryResponse<ClientInstallationResponse>>(
                        $"posintegration/account/clients?openaccounts={openAccounts.ToString()}&clientid={(clientId.AsString())}&room={room}&fullname={fullName}&hotel={installationId.AsString()}");
                    var result = new ValidationItemSource<ClientInstallationRecord>();
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors)
                        result.ItemSource = response.Data.Content.Data.Select(x => x.GetRecord()).ToList();
                    return result;
                }
                else
                {
                    var proxy = GetSilverlightProxy();
                    var request = new QueryRequest();
                    if (openAccounts)
                        request.AddFilter("ccco_not_null", ArgumentValue.Null);
                    if (proxy.GetClientInstallationsSync(request) is QueryResponse response)
                    {
                        var result = new ValidationItemSource<ClientInstallationRecord>();
                        if (response.Errors == null || response.Errors.Count == 0)
                            result.ItemSource = response.Cast<ClientInstallationRecord>().ToList();
                        else
                            result.AddValidations(response.Errors.Values.Select(x => new ValidationError(x)));
                        return result;
                    }
                }

                return null;
            });
        }

        public ValidationItemSource<ChargeHotelRecord> GetChargeHotels()
        {
            return Call(() =>
            {
                if (_rest != null)
                {
                    var response = _rest.Get<QueryResponse<ChargeHotelRecord>>("posintegration/getchargehotels");
                    var result = new ValidationItemSource<ChargeHotelRecord>();
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors)
                        result.ItemSource = response.Data.Content.Data;
                    return result;
                }
                else
                    throw new NotImplementedException("GetChargeHotels");
            });
        }

        #endregion

        #region MealsControlCloud

        public ValidationItemSource<MealsControlRecord> GetMealsControl(MealsFilterModel model)
        {
            return Call(() =>
            {
                if (_rest == null) throw new NotImplementedException("Cloud API location not configured.");
                
                var response = _rest.Post<QueryResponse<MealsControlResponse>>("posintegration/getmealscontrol", model);
                var result = new ValidationItemSource<MealsControlRecord>();
                response.Data.AppendValidations(result);
                if (!result.HasErrors)
                    result.ItemSource = response.Data.Content.Data
                        .Where(x => x.Id != null)
                        .Select(x => x.GetRecord())
                        .ToList();
                return result;
            });
        }

        public ValidationResult UpdateMealsControl(Guid guestId, bool breakfast, bool lunch, bool dinner, Guid? installationId = null)
        {
            return Call(() =>
            {
                if (_rest == null) throw new NotImplementedException("UpdateMealsControl");

                var request = new UpdateMealsControlRequest
                {
                    Id = guestId,
                    Breakfast = breakfast,
                    Lunch = lunch,
                    Dinner = dinner,
                    InstallationId = installationId,
                    WorkDate = _sessionContext.WorkDate
                };

                var response = _rest.Post<ValidationResponse>("posintegration/updatemealscontrol", request);
                var result = new ValidationResult();
                response.Data.AppendValidations(result);
                return result;
            });
        }

        // public ValidationItemResult<List<ClientInfo>> GetClientsCardex()
        // {
        //     return Call()
        // }
        
        public ValidationItemResult<ClientInfoContract> GetClientInfo(Guid clientId)
        {
            return Call(() =>
            {
                var result = new ValidationItemResult<ClientInfoContract>();
                if (_rest != null)
                {
                    var response = _rest.Get<ValidationContentResponse<ClientInfoContract>>($"posintegration/client/info?id={clientId.ToString()}");
                    response.Data.AppendValidations(result);
                    if (!result.HasErrors)
                        result.Item = response.Data.Content;
                }
                else
                    result.Item = new ClientInfoContract();

                return result;
            });
        }

        #endregion

        #region ClientsCloud

        public ValidationResult UpdateClientCloud(ClientContract clientContract)
        {
            return Call(() =>
            {
                if (_rest != null)
                    return new ValidationResult();
                else
                {
                    var proxy = GetSilverlightProxy();
                    return proxy.UpdateClientSync(clientContract) as ValidationResult;
                }
            });
        }

        #endregion ClientsCloud

        #region SpaBussinessCloud
        
        /// <summary>
        /// Get the service for a SPA reservation
        /// </summary>
        public ValidationItemResult<SpaServiceRecord> GetSpaServices(Guid? hotelId, Guid spaId)
        {
            return Call(() =>
            {
                if (_rest == null) throw new NotImplementedException("Cloud API location not configured.");

                var response = _rest.Post<ValidationContentResponse<SpaServiceRecord>>($"posintegration/account/spa/products",
                    new { HotelId = hotelId, SpaReservationId = spaId });
                var result = new ValidationItemResult<SpaServiceRecord>();
                response.Data.AppendValidations(result);
                if (!result.HasErrors)
                    result.Item = response.Data.Content;
                return result;
            });
        }

        /// <summary>
        /// Checkin a service from a SPA reservation
        /// </summary>
        public ValidationResult CheckInSpaService(Guid spaServiceId)
        {
            return Call(() =>
            {
                if (_rest == null) throw new NotImplementedException("Cloud API location not configured.");

                var response = _rest.Post<ValidationResponse>($"posintegration/account/spa/service/checkin?id={spaServiceId}", new { });
                var result = new ValidationResult();
                response.Data.AppendValidations(result);
                return result;

            });
        }

        /// <summary>
        /// Checkout a service from a SPA reservation
        /// </summary>
        public ValidationResult CheckOutSpaService(Guid spaServiceId)
        {
            return Call(() =>
            {
                if (_rest == null) throw new NotImplementedException("Cloud API location not configured.");

                var response = _rest.Post<ValidationResponse>($"posintegration/account/spa/service/checkout?id={spaServiceId}", new {});
                var result = new ValidationResult();
                response.Data.AppendValidations(result);
                return result;

            });
        }

        #endregion

        public ValidationContractResult AuditDocument(POSTicketContract ticket)
        {
            return Call(() =>
            {
                if (_rest == null) throw new NotImplementedException("Cloud API location not configured.");
                {
                    var response = _rest.Post<ValidationContentResponse<POSTicketContract>>("posintegration/chargeticket", ticket);
                    var result = new ValidationContractResult
                    {
                        Contract = response.Data.Content
                    };
                    response.Data.AppendValidations(result);
                    return result;
                }
            });
        }
    }
}