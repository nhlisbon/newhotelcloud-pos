﻿using System;
using System.Diagnostics;
using System.Linq;
using NewHotel.Business;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.DataAnnotations;
using NewHotel.Pos.Core;
using NewHotel.Pos.Core.Ext;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Business.PersistentObjects;
using NewHotel.Pos.Hub.Business.PersistentObjects.Tickets;
using NewHotel.Pos.Integration.Mxm.Models.Estoque;
using RestSharp.Extensions;

namespace NewHotel.Pos.Hub.Business.Business
{
	partial class TicketBusiness
	{
		private ValidationContractResult PersistTicket(Ticket persistentTicket, POSTicketContract contract, bool notifyTicket = true)
		{
			return PersistTicket(persistentTicket, contract, sessionContext.InstallationId, false, notifyTicket);
		}

		public ValidationContractResult PersistTicket(POSTicketContract contract, bool notifyTicket = true)
		{
			return PersistTicket(contract, sessionContext.InstallationId, false, notifyTicket);
		}

		public ValidationContractResult PersistAndClose(POSTicketContract contract, bool notifyTicket = true)
		{
			return PersistTicket(contract, sessionContext.InstallationId, true, notifyTicket);
		}

		public ValidationContractResult PersistTicket(POSTicketContract contract, Guid installationId, bool closeTicket, bool notifyTicket = true)
		{
			return PersistTicket(null, contract, installationId, closeTicket, notifyTicket);
		}

		private ValidationContractResult PersistTicket(Ticket persistentTicket, POSTicketContract contract, Guid installationId, bool closeTicket, bool notifyTicket = true)
		{
			var result = new ValidationContractResult();

			#region Validations

			if (!contract.IsAnul && contract.ProductLineByTicket.Count == 0)
			{
				// Do not allow persist ticket without products when not in reservation mode
				if (contract.ReservationTableId == null)
					result.AddError("Ticket without products");
			}

			if (result.IsEmpty)
			{
				if (contract.PaymentLineByTicket is { Count: > 0 })
				{
					// Remove 0 amount payment with cash or credit card
					contract.PaymentLineByTicket.Remove(pl => pl.PaymentAmount <= decimal.Zero && (pl.IsCash || pl.IsCreditCard));

					// Do not allow account charge with payments other than room plan
					var chargeAccount = contract.PaymentLineByTicket.Any(pl => pl.ReceivableType == ReceivableType.Credit);
					if (chargeAccount)
					{
						if (contract.PaymentLineByTicket.Any(pl => pl.ReceivableType != ReceivableType.Credit && pl.ReceivableType != ReceivableType.RoomPlan))
							result.AddError("Credit payments cannot be used with payments other than room plan");
					}
				}
			}

			#endregion

			if (result.IsEmpty)
			{
				Manager.Open();
				try
				{
					Manager.BeginTransaction();
					try
					{
						var ticket = persistentTicket ?? ticketRepository.GetTicket((Guid)contract.Id);
						ticket.Hotel = installationId;

						var tableId = contract.Mesa;

						#region Check Last Modified

						result = result.ValidateTicketStaleData(ticket, contract.LastModified);

						#endregion

						if (result.IsEmpty)
						{
							contract.ProductLineByTicket ??= [];

							if (closeTicket)
							{
								contract.CloseUserId = sessionContext.UserId;

								#region Generate series

								result.Add(GenerateSeries(contract, GeneralSetting.SignType));

								#endregion

								#region Sync request

								if (result.IsEmpty)
								{
									contract.FiscalDocumentPending = sessionContext.SignatureMode switch
									{
										DocumentSign.FiscalizationBrazilNFE => true,
										_ => false
									};

									if (!ticket.Sync.HasValue)
									{
										if (contract.IsOnlyTicket)
										{
											// Tickets charged to the room go directly to the cloud
											var isRoomCharge = contract.PaymentLineByTicket != null &&
															   contract.PaymentLineByTicket.Any(pl => pl.AccountId.HasValue && pl.ReceivableType == ReceivableType.Credit);

											// Sync ticket if not room charge y tiene serie
											if (!isRoomCharge && !string.IsNullOrEmpty(contract.Serie))
												contract.Sync = DateTime.Now;
										}
										else
										{
											// Sync ticket if not exist fiscal printer
											if (GeneralSetting.FiscalType == FiscalPOSType.None)
											{
												// Ticket abierto que se cierra
												if (contract.ClosedDate.HasValue && !string.IsNullOrEmpty(contract.Serie))
												{
													if (!ticket.ClosedDate.HasValue)
														contract.Sync = DateTime.Now;
												}
											}
										}
									}
								}

								#endregion
							}
							else
							{
								#region Sync request

								if (!ticket.Sync.HasValue)
								{
									// Ticket cerrado que se anula o crea nota de crédito
									if (contract.ClosedDate.HasValue &&
										!string.IsNullOrEmpty(contract.Serie))
									{
										if (!ticket.IsAnul && contract.IsAnul)
											contract.Sync = DateTime.Now;
										else if (string.IsNullOrEmpty(ticket.CreditNoteSerie) &&
												 !string.IsNullOrEmpty(contract.CreditNoteSerie))
											contract.Sync = DateTime.Now;
									}
								}

								#endregion
							}

							if (result.IsEmpty)
							{
								ticket.SetValues(contract.GetValues());
								decimal totalOnBaseCurrency = 0M;

								#region Product Lines + Preparations + Areas

								contract.ProductLineByTicket.Syncronize(
									Manager,
									ticket.ProductLineByTicket,
									(productLine, productLineContract, _) =>
									{
										productLine.Ticket = (Guid)ticket.Id;
										productLine.MainProduct = productLineContract.MainProduct;
										totalOnBaseCurrency += productLineContract.GrossValueInBase;

										productLineContract.Areas.Syncronize(
											Manager,
											productLine.AreasByLine,
											(areaLine, _, _) => areaLine.ProductLineId = (Guid)productLineContract.Id
										);

										productLineContract.Preparations.Syncronize(
											Manager,
											productLine.PreparationsByLine,
											(preparationLine, _, _) => preparationLine.ProductLineId = (Guid)productLineContract.Id
										);

										productLineContract.TableLines.Syncronize(
											Manager,
											productLine.TableProductsByLine,
											(productTableLine, productTableLineContract, _) =>
											{
												productTableLine.ProductLineId = (Guid)productLineContract.Id;
												if (productTableLineContract.SeparatorId != null && productTableLineContract.SeparatorId != Guid.Empty)
												{
													productTableLine.SeparatorId = productTableLineContract.SeparatorId;
													productTableLine.SeparatorOrder = productTableLineContract.SeparatorOrden;
													productTableLine.SeparatorDescriptionId = productTableLineContract.SeparatorDescriptionId;
												}

												productTableLineContract.TableLinePreparations.Syncronize(
													Manager,
													productTableLine.TableProductsByLinePreparation,
													(productTablePreparationLine, _, _) => productTablePreparationLine.TableLineId = (Guid)productTableLineContract.Id
												);
											});
									});

								#endregion

								#region Payment Lines

								contract.PaymentLineByTicket.Syncronize(Manager, ticket.PaymentLineByTicket,
									(paymentLine, _, _) =>
									{
										paymentLine.Ticket = (Guid)ticket.Id;

										paymentLine.PaymentAmountInBase = (paymentLine.PaymentAmount * contract.ExchangeFactor).Round2();

										if (paymentLine.PaymentAmountInBase <= totalOnBaseCurrency)
											totalOnBaseCurrency -= paymentLine.PaymentAmountInBase;
										else if (paymentLine.PaymentAmountInBase > totalOnBaseCurrency)
										{
											paymentLine.PaymentAmountInBase = totalOnBaseCurrency;
											totalOnBaseCurrency = 0;
										}
									});
								//contract.PaymentLineByTicket.Syncronize(Manager, ticket.PaymentLineByTicket,
								//    (paymentLine, _, _) => paymentLine.Ticket = (Guid)ticket.Id);

								if (contract.PaymentLineByTicket != null)
									foreach (var paymentLine in contract.PaymentLineByTicket)
									{
										if (paymentLine.CreditCardContract is { IsPersisted: false })
										{
											var creditCard = new CreditCard(Manager);
											creditCard.SetValues(paymentLine.CreditCardContract.GetValues());
											creditCard.SaveObject();
										}

										if (paymentLine is { IsCreditCard: true, CreditCardContract: null })
											result.AddError("Missing credit card information");

										if (!result.IsEmpty)
											break;
									}

								#endregion

								if (result.IsEmpty)
								{
									#region Lookup Table

									contract.LookupTableByTicket.Syncronize(Manager, ticket.LookupTableByTicket,
										(lookupTable, lookupTableContract, f) =>
										{
											lookupTable.Ticket = (Guid)ticket.Id;
											lookupTableContract.Details.Syncronize(Manager, lookupTable.DetailsByLookup,
												(lookupTableDetail, lookupTableDetailContract, f1) => lookupTableDetail.LookupTableId = (Guid)lookupTableContract.Id);
										});

									#endregion

									#region Clients Position

									contract.ClientsByPosition.Syncronize(Manager, ticket.ClientsByPosition,
										(clientByPosition, clientByPositionContract, f) =>
										{
											clientByPosition.Ticket = (Guid)ticket.Id;

											clientByPositionContract.Allergies.Syncronize(Manager, clientByPosition.Allergies,
												(clientByPositionAllergy, _, _) => clientByPositionAllergy.ClientByPositionId = (Guid)clientByPosition.Id);

											clientByPositionContract.Attentions.Syncronize(Manager, clientByPosition.Attentions,
												(clientByPositionAttention, _, _) => clientByPositionAttention.ClientByPositionId = (Guid)clientByPosition.Id);

											clientByPositionContract.Diets.Syncronize(Manager, clientByPosition.Diets,
												(clientByPositionDiets, _, _) => clientByPositionDiets.ClientByPositionId = (Guid)clientByPosition.Id);

											clientByPositionContract.Preferences.Syncronize(Manager, clientByPosition.Preferences,
												(clientByPositionPreference, _, _) => clientByPositionPreference.ClientByPositionId = (Guid)clientByPosition.Id);
										});

									#endregion
								}
							}
						}

						if (result.IsEmpty)
						{
							// Recalculate only if is not being cancelled to keep value
							if (!ticket.IsAnul)
							{
								#region Calculate tip

								var standBusiness = new StandBusiness(Manager, sessionContext, null, ticketRepository, _validatorFactory);
								var stand = standBusiness.GetStandById(sessionContext.StandId, sessionContext.LanguageId);

								if (ticket.TipPercent is > decimal.Zero)
								{
									var subTotal = ticket.ProductLineByTicket
										.Where(pl => pl.IsActive && !pl.IsTip)
										.Sum(pl => stand.ApplyTipOverNetValue ? pl.NetValue : pl.GrossValue);

									ticket.ApplyTipOverNetValue = stand.ApplyTipOverNetValue;
									ticket.TipValue = TipHelper.CalculateTipPercent(subTotal, ticket.TipPercent.Value);
								}

								#endregion

								//var productLineValue = Math.Round(
								//	ticket.ProductLineByTicket
								//	.Where(pl => pl.IsActive && !pl.IsTip)
								//	.Sum(pl => pl.GrossValue), 2, MidpointRounding.AwayFromZero);

								var productLineValue = ticket.ProductLineByTicket
									.Where(pl => pl.IsActive && !pl.IsTip)
									.Sum(pl => pl.GrossValue);

								var currentTipValue = (ticket.TipValue ?? decimal.Zero).Round2();
								//var currentTipValue = ticket.TipValue ?? decimal.Zero;

								// Tomas: In case the two variables above are giving wrong values, we can use the following line
								//ticket.Total = (ticket.ProductLineByTicket.Where(pl => pl.IsActive && !pl.IsTip).Sum(pl => pl.GrossValue).Round2() + (ticket.TipValue ?? decimal.Zero).Round2()).Round2();
								var fullTotal = productLineValue + currentTipValue;
								ticket.SetTotalValue(fullTotal);
							}

							if (closeTicket)
							{
								if (ticket.AdjustingToTotal != 0M)
								{
									var adjustProductLine = ticket.ProductLineByTicket.LastOrDefault(x => x.IsActive && !x.IsTip);
									if (adjustProductLine != null)
									{
										adjustProductLine.GrossValue += ticket.AdjustingToTotal;
										adjustProductLine.GrossValueInBase += ticket.AdjustingToTotal;
										adjustProductLine.NetValue += ticket.AdjustingToTotal;
										adjustProductLine.DiscountValue -= ticket.AdjustingToTotal;
                                    }
								}

								#region Generate QR Code
								// Tomas and Yordan: Hope the other change in the line 6926 is not necessary
								// if (contract.IsOnlyTicket && (contract.HasCreditRoomPayment || contract.HasMealPlanPayment)) 
								//     ticket.Process = true;

								//if (!ticket.FiscalDocQrCode.HasValue)
								if (string.IsNullOrEmpty(ticket.FiscalDocQrCodeData))
								{
									string qrCodeStr = null;
									string fiscalDocQrCodeStr = null;

									switch (GeneralSetting.SignType)
									{
										case DocumentSign.FiscalizationPortugal:
											var productLines = contract.IsAnul
												? contract.ProductLineByTicket.Where(pl => pl.AnulAfterClosed)
												: contract.ProductLineByTicket.Where(pl => pl.IsActive);

											var buildQrCodeResult = BuildPortugalQRCode(GeneralSetting,
												ticket.FiscalDocFiscalNumber, ticket.FiscalDocNacionality, ticket.IsAnul,
												"CM", ticket.Serie, ticket.Number, ticket.OpeningDate, ticket.Total,
												ticket.ValidationCode, ticket.Signature, productLines, ticket.Observations);

											if (buildQrCodeResult.IsEmpty)
												qrCodeStr = buildQrCodeResult.Result;
											else
												result.Add(buildQrCodeResult);

											if (result.IsEmpty && contract.IsInvoice)
											{
												buildQrCodeResult = BuildPortugalQRCode(GeneralSetting,
													ticket.FiscalDocFiscalNumber, ticket.FiscalDocNacionality, ticket.IsAnul,
													"FR", ticket.FiscalDocSerie, ticket.FiscalDocNumber, ticket.FiscalDocEmiDate,
													ticket.FiscalDocTotal ?? decimal.Zero, ticket.FiscalDocValidationCode,
													ticket.FiscalDocSignature, productLines, ticket.Observations);

												if (buildQrCodeResult.IsEmpty)
													fiscalDocQrCodeStr = buildQrCodeResult.Result;
												else
													result.Add(buildQrCodeResult);
											}

											break;
									}

									// Save QR code
									if (result.IsEmpty)
									{
										if (!string.IsNullOrEmpty(qrCodeStr))
										{
											//byte[] qrCodeImage = new QRCode(qrCodeStr);
											//ticket.QrCode = qrCodeImage;
											ticket.QrCodeData = qrCodeStr;

										}

										if (!string.IsNullOrEmpty(fiscalDocQrCodeStr))
										{
											//byte[] qrCodeImage = new QRCode(fiscalDocQrCodeStr);
											//ticket.FiscalDocQrCode = qrCodeImage;
											ticket.FiscalDocQrCodeData = fiscalDocQrCodeStr;
										}
									}
								}

								// Only enter in the switch if the ticket is an invoice
								if (ticket.DocumentType is POSDocumentType.Invoice or POSDocumentType.CashInvoice)
								{
									// fiscalization type/nationality switch
									switch (GeneralSetting.SignType)
									{
										case DocumentSign.FiscalizationColombiaBtw:
											switch (ticket.FiscalIdenType)
											{
												case (long)PersonalDocType.FiscalNumber:
													{
														if (!ValidNITColombia(ticket.FiscalDocFiscalNumber))
															result.AddError(string.Format("NIT del adquiriente inválido: {0}", ticket.FiscalDocFiscalNumber));
														break;
													}
												case (long)PersonalDocType.IdentityDocument:
													{
														if (!ValidDNIColombia(ticket.FiscalDocFiscalNumber))
															result.AddError(string.Format("Cédula de ciudadanía del adquiriente inválida: {0}", ticket.FiscalDocFiscalNumber));
														break;
													}
												default:
													{
														result.AddError(string.Format("Fiscalización en Colombia requiere NIT o DNI", ticket.FiscalDocFiscalNumber));
														break;
													}
											}
											if (string.IsNullOrEmpty(ticket.FiscalDocTo))
												result.AddError("Nombre del adquiriente no definida");
											if (string.IsNullOrEmpty(ticket.FiscalDocAddress))
												result.AddError("Dirección del adquiriente no definida");
											if (string.IsNullOrEmpty(ticket.FiscalDocEmail))
												result.AddError("E-mail del adquiriente no definido");


											if (string.IsNullOrEmpty(ticket.FiscalPhoneNumber))
												result.AddError("Teléfono del adquiriente no definido");
											if (string.IsNullOrEmpty(ticket.FiscalDocNacionality))
												result.AddError("País del adquiriente no definido");
											if (string.IsNullOrEmpty(ticket.FiscalCityName))
												result.AddError("Nombre de ciudad del adquiriente no definida");
											if (string.IsNullOrEmpty(ticket.FiscalCityCode))
												result.AddError("Código de ciudad del adquiriente no definida");
											if (string.IsNullOrEmpty(ticket.FiscalRegimeTypeCode))
												result.AddError("Código del tipo de regimen del adquiriente no definido");
											if (string.IsNullOrEmpty(ticket.FiscalResponsabilityCode))
												result.AddError("Código de responsabilidad del adquiriente no definido");

											break;
									}
								}

								if ((GeneralSetting.SignType is DocumentSign.FiscalizationColombiaBtw or DocumentSign.FiscalizationPanamaHKA or DocumentSign.FiscalizationCaboVerde) && (contract.IsInvoice || contract.IsCreditNote))
								{
									var cloudBusiness = BusinessContext.GetBusiness<ICloudBusiness>(Manager);

									var pmsResult = cloudBusiness.AuditDocument(contract);
									if (pmsResult.IsEmpty)
									{
										contract = (POSTicketContract)pmsResult.Contract;
										ticket.Process = true;
										ticket.Cufe = contract.Cufe;
										//var fiscalDocQrCodeStr = contract.FiscalDocQrCodeText;   FiscalDocQrCodeText renamed FiscalDocQrCodeData
										//if (!string.IsNullOrEmpty(fiscalDocQrCodeStr))
										//{
										//	byte[] qrCodeImage = new QRCode(fiscalDocQrCodeStr);
										//	ticket.FiscalDocQrCode = qrCodeImage;
										//}
										if (!string.IsNullOrEmpty(contract.FiscalDocQrCodeText))
										{
											ticket.FiscalDocQrCodeData = contract.FiscalDocQrCodeText;
										}
									}
								}
								#endregion
							}

							if (result.IsEmpty)
							{
								ticket.CardNumber = contract.CardNumber;
								
								var settingBusiness = BusinessContext.GetBusiness<ISettingBusiness>(Manager);
								var settings = settingBusiness.GetGeneralSettings(sessionContext.InstallationId);

								// Persist ticket
								if (result.SaveTicket(ticket, settings.RealTimeTicketSync, !contract.AllowPersistInvalid ?  _validatorFactory : null, nameof(PersistTicket)).HasErrors)
									return result;

								// ticket.SaveObject(settings.RealTimeTicketSync);

								#region Ticket Table Change

								if (tableId != ticket.Mesa)
								{
									var ticketTableChange = new TicketTableChange(Manager);
									ticketTableChange.ChangeTime = DateTime.Now;
									ticketTableChange.TicketId = (Guid)ticket.Id;
									ticketTableChange.TableOriginId = tableId;
									ticketTableChange.TableDestId = ticket.Mesa;
									ticketTableChange.UserId = BusinessContext.UserId;
									ticketTableChange.SaveObject();
								}

								#endregion

								// Reload from db
								var loadTicketResult = LoadTicket((Guid)ticket.Id);
								if (loadTicketResult.IsEmpty)
									result = loadTicketResult;
								else
									result.AddValidations(loadTicketResult);
							}
						}

						Manager.EndTransaction(result);
					}
					catch (DatabaseCustomException ex)
					{
						Manager.RollbackTransaction();
						Trace.TraceError(ex.Message);
						switch (ex.ErrorCode)
						{
							case 20002:
								result.AddError("Ticket series is already used. Please close ticket and try again");
								break;
							case 20003:
								result.AddError("Ticket series consecutive not valid. Please close ticket and try again");
								break;
							case 20004:
								result.AddError("Invoice series is not unique. Please close ticket and try again");
								break;
							case 20005:
								result.AddError("Invoice series consecutive not valid. Please close ticket and try again");
								break;
							case 20006:
								result.AddError("The code of ticket can not be modified after the series is assigned");
								break;
							case 20007:
								result.AddError("The code of invoice can not be modified after the series is assigned");
								break;
							case 20008:
								result.AddError("Ticket series cannot be changed");
								break;
							case 20009:
								result.AddError("Invoice series cannot be changed");
								break;
							default:
								result.AddException(ex);
								break;
						}
					}
					catch (Exception ex)
					{
						Manager.RollbackTransaction();
						Trace.TraceError(ex.Message);
						result.AddException(ex);
					}
				}
				finally
				{
					Manager.Close();
				}
			}

			#region SignalR Notifications

			if (!result.IsEmpty || !notifyTicket) return result;
			var ticketContract = result.Contract.As<POSTicketContract>();
			if (closeTicket)
			{
				// Cashier notification
				BusinessContext.Publish("TicketClosed", (Guid)ticketContract.Id);

				// Digital menu notification
				BusinessContext.SocketIOClient?.Publish("TicketClosed", (Guid)ticketContract.Id);
			}
			else
			{
				// Cashier notification
				BusinessContext.Publish("TicketPersisted", (Guid)ticketContract.Id);

				// Digital menu notification
				if (ticketContract.Mesa != null)
				{
					BusinessContext.SocketIOClient?.Publish("TicketPersisted", ticketContract.Mesa);
				}
			}

            #endregion

            return result;
		}
	}
}
