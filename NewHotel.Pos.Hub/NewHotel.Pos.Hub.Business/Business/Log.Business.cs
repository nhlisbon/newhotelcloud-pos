﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using NewHotel.Business;
using NewHotel.Contracts;
using NewHotel.Core;
using NewHotel.Pos.Hub.Business.Interfaces;

namespace NewHotel.Pos.Hub.Business.Business
{ 
    public class LogBusiness : BaseBusiness, ILogBusiness
    {
        #region Constructors

        public LogBusiness(IDatabaseManager manager) : base(manager)
        {
        }

        public LogBusiness() : base(null)
        {
        }

        #endregion
        #region ILogBusiness

        public ValidationResult TraceError(string message)
        {
            var result = new ValidationResult();
            try
            {
                Trace.TraceError(message);
            }
            catch (Exception e)
            {
                result.AddException(e);
            }
            return result;
        }

        public ValidationResult TraceError(IEnumerable<Validation> validations)
        {
            var result = new ValidationResult();
            try
            {
                foreach (var validation in validations)
                    Trace.TraceError(validation.Message);
            }
            catch (Exception e)
            {
                result.AddException(e);
            }
            return result;
        }

        #endregion
    } 
}