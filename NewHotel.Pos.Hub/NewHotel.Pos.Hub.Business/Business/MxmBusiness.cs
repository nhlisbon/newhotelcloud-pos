﻿using NewHotel.Contracts;
using NewHotel.Pos.Hub.Business.Business.Common;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Business.SqlContainer.Queries;
using NewHotel.Pos.Hub.Contracts.Common.Records.Integrations;
using NewHotel.Pos.Integration.Mxm.Models.Estoque;
using NewHotel.Pos.Integration.Mxm.Models.Produto;
using NewHotel.Pos.Integration.Mxm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NewHotel.Business;
using NewHotel.Core;
using Microsoft.Extensions.Options;
using NewHotel.Pos.Hub.Business.IntegrationConfigs;
using System.Globalization;

namespace NewHotel.Pos.Hub.Business.Business
{
    public class MxmBusiness : BaseBusiness, IMxmBusiness
    {
        private readonly IIntegrationBusiness _integrationBusiness;
        private readonly IOptions<MxmTaskConfig> _options;

        public MxmBusiness(
            IDatabaseManager manager, 
            IIntegrationBusiness integrationBusiness,
          IOptions<MxmTaskConfig> options
        ) : base(manager)
        {
            _integrationBusiness = integrationBusiness;
            _options = options;
        }

        #region Entry Points

        /// <summary>
        /// This method is used to consume the stock from the Mxm integration API. This happens when a product is sold.
        /// This is the method to be called when trying to do the action consume stock.
        /// </summary>
        /// <param name="ticket">The ticket contract which contains the products the method wants to affect</param>
        /// <param name="integrationId">The Guid of the integration to help the search of the product in the reference table TNHT_ARIN</param>
        /// <param name="config">The config of the integration</param>
        /// <returns>A success message or an error message if something went wrong</returns>
        public async Task<ValidationResult<string>> ConsumeStock(POSTicketContract ticket, Guid integrationId, MxmConfig config)
        {
            var result = new ValidationResult<string>();
            try
            {
                var stockIntegrationData = new MxmStockIntegrationData();
                var processList = new List<InterfacedoEstoque>();

                var stockProduct = MapTicketToInterfaceDoEstoque(ticket, config);
                var productList = new List<InterfaceItemdoEstoque>();
                for (int i = 0; i < ticket.ProductLineByTicket.Count; i++)
                {
                    POSProductLineContract productLine = ticket.ProductLineByTicket[i];
                    var referenceResult = _integrationBusiness.GetProductIntegrationReferenceByProductIdAndIntegrationId(productLine.Product, integrationId);
                    if (referenceResult.HasErrors || referenceResult.Result == null)
                    {
                        result.AddError($"Product reference not found for product: {productLine.ProductDescription} in Mxm");
                        
                        // Skip this product and go to the next one
                        continue; 
                    }

                    var item = MapProductAndReferenceRecordToInterfaceItemdoEstoque(productLine, referenceResult.Result, i);
                    productList.Add(item);
                }

                if(productList.Count == 0)
                {
                    result.AddError("No products found to consume stock");
                    return result;
                }

                stockProduct.InterfaceItemdoEstoque = [.. productList];
                processList.Add(stockProduct);
                stockIntegrationData.DataSaveProcessStock.InterfacedoEstoque = [.. processList];

                var standBusiness = BusinessContext.GetBusiness<IStandBusiness>(Manager);
                var workdate = standBusiness.GetWorkDateByStand(ticket.Stand);

                var consumeStockResult = await ConsumeStockInMxmApiAndLocal(stockIntegrationData, workdate, config, integrationId);
                if(consumeStockResult.HasErrors)
                    result.AddError(consumeStockResult.Errors[0].Message);
                else
                    result.Result = "Stock consumed successfully";
            }
            catch (Exception ex)
            {
                var message = "Error trying to check product availability";
                result.AddError(message, ex);
            }
            return result;
        }

        /// <summary>
        /// This happens by either a manual action or by a scheduled task. This method is used to refresh the stock from the Mxm integration API.
        /// So it updates the local stock with the data from the integration. 
        /// Unfortunately, the integration does not have a specific endpoint for this, so we have to do it one by one, which means that this method is not optimized and may take a long time to complete.
        /// Since it makes a request for each product in the integration, in case one fails it does not rollback the whole process and it will skip the one that failed and will keep going until the end.
        /// Use with due CAUTION.
        /// </summary>
        /// <param name="config">The MxmConfig to be used</param>
        /// <returns>If nothing went wrong it will return a success message otherwise, it will return an error message</returns>
        public async Task<ValidationResult<string>> RefreshStock(MxmConfig config, Guid integrationId)
        {
            var result = new ValidationResult<string>();
            try
            {
                var records = GetAllProductReferenceRecordsForIntegration(integrationId);
                if (records.HasErrors)
                {
                    result.AddError(records.Errors[0].Message);
                    return result;
                }
                var referenceRecords = records.Result;
                foreach (var record in referenceRecords)
                {
                    var request = MapProductReferenceToMxmDataCheckProductAvailabilityRequest(record, config, DateTime.Now);
                    var stockProductResult = await CheckProductAvailabilityFromMxmApi(request, config);
                    if (stockProductResult.HasErrors)
                    {
                        result.AddError($"Error trying to check product availability for product with id: {record.ProductId} in Mxm. Check if the product has the external code and optional code 1 filled", stockProductResult.Errors[0].Message);
                        continue;
                    }
                    var qty = decimal.TryParse(stockProductResult.Result.InterfacedoProduto.FirstOrDefault().Saldo, out var quantity) ? quantity : 0;
                    var localStockResult = UpdateOrCreateLocalStock(record.ProductId, qty);
                    if (localStockResult.HasErrors)
                        result.AddError($"Error updating the local stock for the product with id: {record.ProductId} in Mxm");
                    else
                        result.Result = "Stock refreshed successfully";
                }
            }
            catch(Exception ex)
            {
                var message = "Error trying to refresh the stock";
                result.AddError(message, ex);
            }
            return result;
        }

        /// <summary>
        /// This method is used to check the the stock consume process from the Mxm integration API. This happens when a product is sold.
        /// And due to they're system, we need to check if the stock was consumed correctly or not.
        /// </summary>
        /// <param name="productId">The product id from the table TNHT_ARTG</param>
        /// <returns>A sucess message or a error message if anything went wrong</returns>
        public async Task<ValidationResult<string>> CheckStockDataFromMxmApi(Guid productId, Guid integrationId, MxmConfig config)
        {
            var result = new ValidationResult<string>();
            try
            {
                var referenceResult = _integrationBusiness.GetProductIntegrationReferenceByProductIdAndIntegrationId(productId, integrationId);
                if (referenceResult.HasErrors)
                {
                    result.AddError("Product Integration Reference not found");
                    return result;
                }
                else
                {
                    var reference = referenceResult.Result;
                    
                    var endpoint = new Endpoints(config.Username, config.Password, config.EnvironmentName, _options.Value.MxmUrl);
                    int.TryParse(reference.ProductCode, out var processNumber);
                    var response = await endpoint.CheckStockProcess(processNumber);
                    if(response.Success)
                    {
                        // This is the success message from the integration
                        // and we need to check it EXACTLY like this because it does not have a specific code for it
                        if (response.Messages.Any(s => s.Message.Contains("Registro Processado com Sucesso")))
                            result.Result = "Stock data was processed succesfully";
                        else
                            result.Result = "Stock is yet to be processed";
                    }
                    else
                        result.AddError("Error trying to get stock data from the integration", response.Messages.FirstOrDefault().Message);
                }
            }
            catch (Exception ex)
            {
                var message = "Error trying to get stock data from the integration";
                result.AddError(message, ex);
            }
            return result;
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Gets all the product reference records from the table TNHT_ARIN
        /// </summary>
        /// <param name="integrationId">The id of the integration from the table TNHT_INTE</param>
        /// <returns></returns>
        private ValidationResult<List<ProductIntegrationReferenceRecord>> GetAllProductReferenceRecordsForIntegration(Guid integrationId)
        {
            var result = new ValidationResult<List<ProductIntegrationReferenceRecord>>();
            using (Manager.OpenSession())
            {
                try
                {
                    var query = CommonQueryFactory.GetAllProductReferenceRecordsForIntegration(Manager);
                    query.Parameters["inte_pk"] = integrationId;
                    var records = query.Execute();
                    var productReferenceRecords = records.Select(record => new ProductIntegrationReferenceRecord
                    {
                        Id = record.ValueAs<Guid>("arin_pk"),
                        ProductId = record.ValueAs<Guid>("artg_pk"),
                        IntegrationId = record.ValueAs<Guid>("inte_pk"),
                        ProductCode = record.ValueAs<string>("ARIN_CODE"),
                        OptionalCodeOne = record.ValueAs<string>("ARIN_OPCD_ONE"),
                        OptionalCodeTwo = record.ValueAs<string>("ARIN_OPCD_TWO")
                    }).ToList();
                    result.Result = productReferenceRecords;
                }
                catch (Exception ex)
                {
                    var message = "Error trying to get ProductStocks from the database";
                    result.AddError(message, ex);
                }
            }
            return result;
        }

        /// <summary>
        /// Gets all the stock records from the table TNHT_ARST
        /// </summary>
        /// <returns>A list of ProductStockRecord or an error with said list in null and the error(s) in the Error property</returns>
        private ValidationResult<ProductStockRecord> GetStockRecordFromProductId(Guid productId)
        {
            var result = new ValidationResult<ProductStockRecord>();
            using (Manager.OpenSession())
            {
                try
                {
                    var query = CommonQueryFactory.GetProductStockByProductId(Manager);
                    query.Parameters["artg_pk"] = productId;
                    var records = query.Execute();
                    if(records.Count() == 0)
                    {
                        result.AddError("No records found for the given product id");
                        return result;
                    }
                    else if (records.Count() > 1)
                    {
                        result.AddError("More than one stock record found for the given product id");
                        return result;
                    }
                    result.Result = new ProductStockRecord
                    {
                        Id = records.FirstOrDefault().ValueAs<Guid>("arst_pk"),
                        ProductId = records.FirstOrDefault().ValueAs<Guid>("artg_pk"),
                        Quantity = records.FirstOrDefault().ValueAs<decimal>("arst_qtds"),
                        Date = records.FirstOrDefault().ValueAs<DateTime>("arst_datr")
                    };
                }
                catch (Exception ex)
                {
                    var message = "Error trying to get ProductStocks from the database";
                    result.AddError(message, ex);
                }
            }
            return result;
        }

        /// <summary>
        /// Updates the local stock with the data from the Mxm integration
        /// </summary>
        /// <param name="productId">The id of the product to be updated</param>
        /// <param name="quantity">The new quantity of the product found by the given id</param>
        /// <returns>A record from the table TNHT_ARIN with the given data by the params</returns>
        private ValidationResult<ProductStockRecord> UpdateOrCreateLocalStock(Guid productId, decimal quantity)
        {
            var result = new ValidationResult<ProductStockRecord>();
            using (Manager.OpenSession())
            {
                Manager.BeginTransaction();
                try
                {
                    var upsertQuery = CommonQueryFactory.UpdateOrCreateProductStock(Manager);
                    upsertQuery.Parameters["arst_pk"] = Guid.NewGuid();
                    upsertQuery.Parameters["artg_pk"] = productId;
                    upsertQuery.Parameters["arst_qtds"] = quantity;
                    upsertQuery.Parameters["arst_datr"] = DateTime.Now.Date;

                    upsertQuery.Execute();
                    Manager.CommitTransaction();

                    var record = GetStockRecordFromProductId(productId).Result;
                    if (record == null)
                        result.AddError("Product not found");
                    else
                        result.Result = record;

                    return result;
                }
                catch (Exception ex)
                {
                    Manager.RollbackTransaction();
                    var message = $"Error trying to update the local stock with productId: {productId} and quantity: {quantity}";
                    result.AddError(message, ex);
                    return result;
                }
            }
        }

        /// <summary>
        /// Checks if the product is available in the Mxm integration
        /// </summary>
        /// <param name="data">The data to be used in the body of the request</param>
        /// <param name="config">The config of the integration</param>
        /// <returns>A success message or a error message</returns>
        private async Task<ValidationResult<MxmDataCheckProductStock>> CheckProductAvailabilityFromMxmApi(MxmDataCheckProductAvailabilityRequest data, MxmConfig config)
        {
            var result = new ValidationResult<MxmDataCheckProductStock>();
            try
            {
                var endpoint = new Endpoints(config.Username, config.Password, config.EnvironmentName, _options.Value.MxmUrl);
                var response = await endpoint.CheckProductAvailability(data);
                if (!response.Success)
                    result.AddError("Error trying to check product availability", response.Messages.FirstOrDefault().Message);
                else
                {
                    if(response.Messages.Any(msg => msg.ErrorLevel == 2))
                        result.AddError("No records were found for the given data");
                    else
                        result.Result = response.Data;
                }
            }
            catch (Exception ex)
            {
                var message = "Error trying to check product availability";
                result.AddError(message, ex);
            }
            return result;
        }

        /// <summary>
        /// This method is used to consume and update the stock data to the Mxm integration API. This happens when a product is bought.
        /// The stock is consumed and we need to update the local stock as well.
        /// </summary>
        /// <param name="data">The data we want to sent to the api</param>
        /// <param name="workdate">The date fo the current work day</param>
        /// <param name="config">The config of the integration</param>
        /// <param name="integrationId">The id of the integration to be used to get the ids of the products from the table TNHT_ARIN to update them in the local stock table TNHT_ARST</param>
        /// <returns>The response object from the Mxm API or an error in the Error property</returns>
        private async Task<ValidationResult<MxmDataSaveProcessStock>> ConsumeStockInMxmApiAndLocal(MxmStockIntegrationData data, DateTime workdate, MxmConfig config, Guid integrationId)
        {
            var result = new ValidationResult<MxmDataSaveProcessStock>();
            
            try
            {
                var endpoint = new Endpoints(config.Username, config.Password, config.EnvironmentName, _options.Value.MxmUrl);
                var response = await endpoint.CreateStockProcess(data.DataSaveProcessStock);
                if (response.Success) 
                {
                    result.Result = data.DataSaveProcessStock;
                    var stocks = data.DataSaveProcessStock.InterfacedoEstoque[0].InterfaceItemdoEstoque;
                    foreach (var stock in stocks)
                    {
                        var request = MapInterfacedoEstoqueAndInterfaceItemdoEstoqueToMxmDataCheckProductAvailabilityRequest(stock, data.DataSaveProcessStock.InterfacedoEstoque.FirstOrDefault(), workdate);
                        var stockProductResult = await CheckProductAvailabilityFromMxmApi(request, config);
                        if(stockProductResult.HasErrors)
                        {
                            result.AddError("Error trying to check product availability", stockProductResult.Errors[0].Message);
                            continue;
                        }
                        var qty = decimal.TryParse(stockProductResult.Result.InterfacedoProduto.FirstOrDefault().Saldo, out var quantity) ? quantity : 0;
                        var referenceResult = _integrationBusiness.GetProductIntegrationReferenceByIntegrationIdAndProductCode(integrationId, stock.CodigodoItem);
                        if(referenceResult.HasErrors)
                        {
                            result.AddError($"Error getting the reference for the product with code: {stock.CodigodoItem} for Mxm integration");
                            continue;
                        }
                        var localStockResult = UpdateOrCreateLocalStock(referenceResult.Result.ProductId, qty);
                        if(localStockResult.HasErrors)
                            result.AddError($"Error updating the local stock for the product with code: {stock.CodigodoItem} for Mxm integration");
                    }
                }
                else
                    result.AddError("Error trying to send stock data to the integration", response.Messages[0].Message);
            }
            catch (Exception ex)
            {
                var message = "Error trying to send stock data to the integration";
                result.AddError(message, ex);
            }
            return result;
        }

        #endregion

        #region Mappers

        private static InterfacedoEstoque MapTicketToInterfaceDoEstoque(POSTicketContract ticket, MxmConfig config)
        {
            return new InterfacedoEstoque
            {
                SequenciadoRegistro = 1,
                CodigodaEmpresa = config.CompanyCode,
                CodigodoEstoque = config.StockCode,
                CodigodoAlmoxarifado = config.AlmoxarifadoCode,
                NumerodoDocumento = $"NH00105567/{ticket.Number}", 
                TipodeMovimentacao = "S",
                CodigoTipodeNota = "03",
                IndicadordeClienteouFornecedor = "C",
                CodigodoClienteouFornecedor = ticket.FiscalDocFiscalNumber,
                TipodeOperacao = "054",
                ValorTotaldaNota = ticket.FiscalDocTotal.ToString(),
                DatadeEmissaodaNota = ticket.FiscalDocEmiDate.ToMxmDate(),
                // They expect the date in the format "ddmmyyyy" that is why we are replacing the "/" with ""
                DatadeEntradadaNota = ticket.ClosedDate.ToMxmDate(), // ticket.ClosedDate.Value.ToShortDateString().Replace("/", ""),
				DatadeDigitacao = ticket.ClosedDate.ToMxmDate(),// ticket.ClosedDate.Value.ToShortDateString().Replace("/", ""),
                DatadeCompetencia = ticket.ClosedDate.ToMxmDate(),// ticket.ClosedDate.Value.ToShortDateString().Replace("/", ""), 
                InterfaceItemdoEstoque = []
            };
        }

        private static InterfaceItemdoEstoque MapProductAndReferenceRecordToInterfaceItemdoEstoque(POSProductLineContract productLine, ProductIntegrationReferenceRecord record, int count)
        {
            return new InterfaceItemdoEstoque
            {
                SequenciadoItemnaNota = count,
                CodigodoItem = record.ProductCode,
                QuantidadedoItem = productLine.ProductQtd.ToMxmDecimal(),
                CodigodaUnidadeAdquirida = "UN",//record.ProductCode,
                QuantidadedoAdquirida = productLine.ProductQtd.ToMxmDecimal(),
                ValorBruto = productLine.GrossValue.ToMxmDecimal(),
                Destinacao = record.OptionalCodeOne,
                CentrodeCusto = record.OptionalCodeTwo, //costCenterCode,
                ValordoItem = productLine.CostValue.ToMxmDecimal(), 
                TipodeOperacao = "054",
				//UnidadeDeMedidaDeControle = "UN",
            };
        }

        private static MxmDataCheckProductAvailabilityRequest MapProductReferenceToMxmDataCheckProductAvailabilityRequest(ProductIntegrationReferenceRecord record, MxmConfig config, DateTime date)
        {
            return new MxmDataCheckProductAvailabilityRequest
            {
                Empresa = config.CompanyCode,
                Estoque = config.StockCode,
                Produto = record.ProductCode,
                Almoxarifado = config.AlmoxarifadoCode,
                ExibeProdutoSaldoZerado = "S",
                DesconsiderarSaldoEmpenhado = "S",
                PeriodoDe = date.ToMxmDate(), //ToString("yyyy-MM-dd"),
                PeriodoAte = date.ToMxmDate(),//ToString("yyyy-MM-dd"),
                SaldoEstoqueLivre = "S"
            };
        }

        private static MxmDataCheckProductAvailabilityRequest MapInterfacedoEstoqueAndInterfaceItemdoEstoqueToMxmDataCheckProductAvailabilityRequest(InterfaceItemdoEstoque stockItem, InterfacedoEstoque stock, DateTime date)
        {
            return new MxmDataCheckProductAvailabilityRequest
            {
                Empresa = stock.CodigodaEmpresa,
                Estoque = stock.CodigodoEstoque,
                Produto = stockItem.CodigodoItem,
                Almoxarifado = stock.CodigodoAlmoxarifado, // optional
                ExibeProdutoSaldoZerado = "S",
                DesconsiderarSaldoEmpenhado = "S",
                PeriodoDe = date.ToMxmDate(),//ToString("yyyy-MM-dd"), // optional
                PeriodoAte = date.ToMxmDate(),//ToString("yyyy-MM-dd"), // optional
                SaldoEstoqueLivre = "S"
            };
        }

        #endregion
    }

	public static class MxmBusinessExtensions
	{
        static readonly CultureInfo MxmCulture = new("pt-BR") { NumberFormat = new NumberFormatInfo { NumberDecimalSeparator = "." } };

        public static string ToMxmDate(this DateTime? date) => (date ?? DateTime.Today).ToMxmDate();

		public static string ToMxmDate(this DateTime date)
		{
			return date.ToString("ddMMyyyy", MxmCulture);
		}

		public static string ToMxmDecimal(this decimal value)
		{
			return value.ToString("#0.0#######", MxmCulture);
		}
	}
}
