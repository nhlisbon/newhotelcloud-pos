﻿using NewHotel.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.Records.Integrations;
using NewHotel.Pos.Hub.Business;
using System;

namespace NewHotel.Pos.Hub.Business.Business
{
    public interface IIntegrationBusiness
    {
        ValidationResult<IntegrationRecord> GetIntegrationRecordById(Guid id);
        ValidationResult<IntegrationRecord> GetActiveIntegrationByType(IntegrationType type);
        ValidationResult<IntegrationRecord> GetIntegrationByTypeAndName(IntegrationType type, string name);
        ValidationResult<ProductIntegrationReferenceRecord> GetProductIntegrationReferenceByIntegrationIdAndProductCode(Guid integrationId, string productCode);
        ValidationResult<ProductIntegrationReferenceRecord> GetProductIntegrationReferenceByProductIdAndIntegrationId(Guid productId, Guid integrationId);
        ValidationResult<IntegrationRecord> CreateIntegration(string name, IntegrationType type, object config);
        ValidationResult<MultiConfig> DeserializeConfig(IntegrationRecord record);   
    }
}
