﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Threading.Tasks;
using System.ServiceModel.Description;

namespace POSComunication
{
    public class ProxyMgr
    {
        public static Proxy<NewHotel.Pos.Hub.Cashier.IService> GetCashierClientInstance()
        {
            ContractDescription contract = new ContractDescription("NewHotel.Pos.Hub.Cashier.IService");
            NetTcpBinding binding = new NetTcpBinding(SecurityMode.None);
            EndpointAddress address = new EndpointAddress("net.tcp://localhost:13301/NewHotel.Pos.Hub.Cashier/Service/");
            ServiceEndpoint endpoint = new ServiceEndpoint(contract, binding, address);
            ChannelFactory<NewHotel.Pos.Hub.Cashier.IService> channelFactory = new ChannelFactory<NewHotel.Pos.Hub.Cashier.IService>(binding, address);
            NewHotel.Pos.Hub.Cashier.IService proxy = channelFactory.CreateChannel();

            return new Proxy<NewHotel.Pos.Hub.Cashier.IService>(proxy);
        }
    }



    public sealed class Proxy<T> : IDisposable
    {
        private T _obj;
        private bool _disposed = false;

        public Proxy(T obj)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");
            _obj = obj;
        }

        public event EventHandler Opening
        {
            add { CommunicationObject.Opening += value; }
            remove { CommunicationObject.Opening -= value; }
        }

        public event EventHandler Opened
        {
            add { CommunicationObject.Opened += value; }
            remove { CommunicationObject.Opened -= value; }
        }

        public event EventHandler Closing
        {
            add { CommunicationObject.Closing += value; }
            remove { CommunicationObject.Closing -= value; }
        }

        public event EventHandler Closed
        {
            add { CommunicationObject.Closed += value; }
            remove { CommunicationObject.Closed -= value; }
        }

        public event EventHandler Faulted
        {
            add { CommunicationObject.Faulted += value; }
            remove { CommunicationObject.Faulted -= value; }
        }

        private ICommunicationObject CommunicationObject
        {
            get { return _obj as ICommunicationObject; }
        }

        public CommunicationState State
        {
            get { return CommunicationObject.State; }
        }

        public T Channel
        {
            get { return _obj; }
        }

        public IClientChannel InnerChannel
        {
            get { return _obj as IClientChannel; }
        }

        private void Dispose(bool close)
        {
            if (!_disposed)
            {
                _disposed = true;
                try
                {
                    if (CommunicationObject != null)
                    {
                        try
                        {
                            if (!close || CommunicationObject.State == CommunicationState.Faulted)
                                CommunicationObject.Abort();
                            else if (CommunicationObject.State == CommunicationState.Opened)
                            {
                                try
                                {
                                    CommunicationObject.Close(TimeSpan.FromSeconds(3));
                                }
                                catch (CommunicationException)
                                {
                                    CommunicationObject.Abort();
                                }
                            }
                        }
                        finally
                        {
                            var disposable = _obj as IDisposable;
                            if (disposable != null)
                                disposable.Dispose();
                        }
                    }
                }
                finally
                {
                    GC.SuppressFinalize(this);
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        ~Proxy()
        {
            Dispose(false);
        }
    }
}

