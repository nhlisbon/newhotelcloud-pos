﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NewHotel.Contracts;
using NewHotel.Pos.Core;
using NewHotel.Pos.Hub.Contracts.Common.Records.Tickets;
using NewHotel.Pos.Hub.Contracts.Handheld.Contracts;
using NewHotel.Pos.Hub.Core;
using NewHotel.Pos.Hub.Model;
using DocumentSerieRecord = NewHotel.Pos.Hub.Model.DocumentSerieRecord;
using ProductRecord = NewHotel.Pos.Hub.Contracts.Common.Records.Products.ProductRecord;

namespace NewHotel.Pos.Communication.Cashier
{
	public interface ITicketBusinessProxy
    {
        #region Members

        Task<ValidationResult> UpdateFiscalPrinterDataAsync(POSTicketContract ticket, bool sync = true);

        Task<ValidationResult> UpdateFiscalPrinterDataAsync(Guid tickedId, string fpSerialInvoice, long? fpInvoiceNumber, string fpSerialCreditNote, long? fpCreditNoteNumber, bool sync = true);

        Task<ValidationContractResult> CreateLookupTableAsync(POSTicketContract ticket);

        Task<ValidationContractResult> MergeTicketsAsync(POSTicketContract originalTicket, POSTicketContract voidTicket);

        Task<ValidationContractResult> SplitTicketProductAsync(POSTicketContract ticket, Guid productLineId, int quantity);

        Task<ValidationContractResult> ChangeTicketProductDetailsAsync(POSTicketContract ticket, Guid productLineId,
            POSProductLineContract editedLine);

        Task<ValidationContractResult> DispatchTicketOrdersAsync(POSTicketContract ticket, List<Guid> productLineIds);

        Task<ValidationContractResult> VoidDispatchTicketOrdersAsync(POSTicketContract ticket, List<Guid> productLineIds);

        Task<ValidationContractResult> AwayDispatchTicketOrdersAsync(POSTicketContract ticket, List<DispatchContract> productLineIds);

        Task<ValidationContractResult> PersistentProductLineAsync(POSProductLineContract contract, Guid hotelId,
            Guid schemaId, bool included);

        Task<ValidationContractResult> AddProductToTicketAsync(POSTicketContract ticket, ProductRecord product, int quantity, decimal? manualPrice, string manualPriceDesc);

        Task<ValidationContractResult> CloseTicketAsync(POSTicketContract contract, ProductRecord tipProduct, bool saveTicket);

        Task<ValidationContractResult> LoadClosedTicketAsync(Guid ticketId, bool local);

        Task<ValidationContractResult> CancelClosedTicketAsync(Guid ticketId, Guid cancellationReasonId, string comment, bool voidTicket, bool local, bool createQuickTicket);

        Task<POSTicketContract[]> GetTicketByStandCashierAsync(Guid standId, Guid cashierId);
        Task<TicketInfo[]> GetTicketInfoByStandCashierAsync(Guid standId, Guid cashierId);


		Task<SplitTicketResult> SplitTicketAutoAsync(Guid ticketId, int quantity);

        Task<SplitTicketResult> SplitTicketByAlcoholicGroupAsync(Guid ticketId);

        Task<SplitTicketResult> SplitTicketManualAsync(Guid ticketId, KeyValuePair<Guid, decimal>[] orderIds);

        Task<ValidationContractResult> CancelTicketProductAsync(POSTicketContract contract, Guid productLineId);

        Task<ValidationContractResult> CancelTicketAsync(POSTicketContract contract, Guid cancellationReasonId, string comment, bool createQuickTicket);

        Task<ValidationContractResult> PersistTicketAsync(POSTicketContract contract);

        Task<ValidationContractResult> SaveTicketFromEditionAsync(Guid ticketId, short paxs);

        Task<ValidationContractResult> LoadTicketForEditionAsync(Guid ticketId, bool forceUnlock);

        Task<ValidationContractResult> CreateEmptyTicketToPaxWaitingListAsync(Guid tableId, Guid paxWaitingListId);

        Task<ValidationContractResult> CreateEmptyTicketToReservationTableAsync(Guid[] tableIds, Guid reservationTableId, string name);

        Task<ValidationContractResult> NewTicketAsync(Guid hotelId, Guid cajaId, Guid standId, Guid? tableId, Guid utilId);

        Task<DocumentSerieRecord[]> GetTicketSerieByStandCashierAsync(Guid standId, Guid cashierId);

        Task<ValidationContractResult> MoveTicketToTable(POSTicketContract ticketContract, Guid? tableId);

        Task<ValidationResult<string>> NotifyRoomServiceMonitorAsync(POSTicketContract contract);

        Task<ValidationContractResult> IncrementPrintsAsync(Guid ticketId, int title);

        Task<ValidationContractResult> CheckOpenTicket(Guid ticketId, Guid standId, Guid cashierId);
		Task<ValidationContractResult> MoveTicketToTableById(Guid ticketId, Guid? tableId);
		Task<ValidationContractResult> CancelTicketByIdAsync(Guid ticketId, Guid cancellationReasonId, string comment, bool createQuickTicket);
		Task<ValidationContractResult> MergeTicketsByIdAsync(Guid originalTicketId, Guid voidTicketId);

        Task<ValidationContractResult> ChangeOrdersSeparatorAsync(POSTicketContract ticket, IEnumerable<Guid> productLineIds, Guid? separatorId);
        Task<ValidationContractResult> ChangeOrdersSeatAsync(POSTicketContract ticket, IEnumerable<Guid> productLineIds, short seat);
        Task<ValidationContractResult> CreateQuickTicketFromClosedTicketAsync(string closedTicketId);
        
        #endregion

        #region Signatures

        Task<SignatureRecord[]> GetAllSignaturesAsync();
        Task<SignatureRecord> GetSignatureAsync(string ticketId);
        Task<ValidationContractResult> AddSignatureAsync(SignatureRecord signature);
        Task<ValidationContractResult> UpdateSignatureAsync(string ticketId, SignatureRecord signature);
        Task<ValidationContractResult> DeleteSignatureAsync(string ticketId);
        
        #endregion

    }

    public class TicketBusinessProxy : CashierBaseBusinessProxy, ITicketBusinessProxy
    {
        #region ITicketBusinessProxy Implementation

        public Task<ValidationResult> UpdateFiscalPrinterDataAsync(POSTicketContract ticked, bool sync = true)
        {
            return UpdateFiscalPrinterDataAsync((Guid)ticked.Id, ticked.FpSerialInvoice, ticked.FpInvoiceNumber,
                ticked.FpSerialCreditNote, ticked.FpCreditNoteNumber, sync);
        }

        public Task<ValidationResult> UpdateFiscalPrinterDataAsync(Guid tickedId, string fpSerialInvoice,
            long? fpInvoiceNumber, string fpSerialCreditNote, long? fpCreditNoteNumber, bool sync = true)
        {
			var ticketIdSerialized = Serializer.Serialize(tickedId);
			var fpInvoiceNumberSerialized = Serializer.Serialize(fpInvoiceNumber);
			var fpCreditNoteNumberSerialized = Serializer.Serialize(fpCreditNoteNumber);
            return ExecuteAsync(sc => sc.UpdateFiscalPrinterDataAsync(ticketIdSerialized, fpSerialInvoice, fpInvoiceNumberSerialized, fpSerialCreditNote, fpCreditNoteNumberSerialized, sync));
		}

		public Task<ValidationContractResult> CreateLookupTableAsync(POSTicketContract ticket)
        {
			var ticketSerialized = Serializer.SerializeJson(ticket, typeof(POSTicketContract));
			return ExecuteAsync(sc => sc.CreateLookupTableAsync(ticketSerialized));
		}

		public Task<ValidationContractResult> MergeTicketsAsync(POSTicketContract originalTicket, POSTicketContract voidTicket)
        {
			var originalTicketSerialized = Serializer.SerializeJson(originalTicket, typeof(POSTicketContract));
			var voidTicketSerialized = Serializer.SerializeJson(voidTicket, typeof(POSTicketContract));
			return ExecuteAsync(sc => sc.MergeTicketsAsync(originalTicketSerialized, voidTicketSerialized));
		}

		public Task<ValidationContractResult> MergeTicketsByIdAsync(Guid originalTicketId, Guid voidTicketId)
         {
			var originalTicketIdSerialized = Serializer.SerializeJson(originalTicketId, typeof(Guid));
			var voidTicketIdSerialized = Serializer.SerializeJson(voidTicketId, typeof(Guid));
			return ExecuteAsync(sc => sc.MergeTicketsByIdAsync(originalTicketIdSerialized, voidTicketIdSerialized));
		}

		public Task<ValidationContractResult> SplitTicketProductAsync(POSTicketContract ticket, Guid productLineId, int quantity)
        {
			var ticketSerialized = Serializer.SerializeJson(ticket, typeof(POSTicketContract));
			var productLineIdSerialized = Serializer.SerializeJson(productLineId, typeof(Guid));
			var quantitySerialized = Serializer.SerializeJson(quantity, typeof(int));
			return ExecuteAsync(sc => sc.SplitTicketProductAsync(ticketSerialized, productLineIdSerialized, quantitySerialized));
		}

		public Task<ValidationContractResult> ChangeTicketProductDetailsAsync(POSTicketContract ticket, Guid productLineId, POSProductLineContract editedLine)
        {
			var ticketSerialized = Serializer.SerializeJson(ticket, typeof(POSTicketContract));
			var productLineIdSerialized = Serializer.SerializeJson(productLineId, typeof(Guid));
			var editedLineSerialized = Serializer.SerializeJson(editedLine, typeof(POSProductLineContract));
			return ExecuteAsync(sc => sc.ChangeTicketProductDetailsAsync(ticketSerialized, productLineIdSerialized, editedLineSerialized));
		}

		public Task<ValidationContractResult> DispatchTicketOrdersAsync(POSTicketContract ticket, List<Guid> productLineIds)
        {
			var ticketSerialized = Serializer.SerializeJson(ticket, typeof(POSTicketContract));
			var productLineIdSerialized = Serializer.SerializeJson(productLineIds, typeof(List<Guid>));
			return ExecuteAsync(sc => sc.DispatchTicketOrdersAsync(ticketSerialized, productLineIdSerialized));
		}

		public Task<ValidationContractResult> VoidDispatchTicketOrdersAsync(POSTicketContract ticket, List<Guid> productLineIds)
        {
			var ticketSerialized = Serializer.SerializeJson(ticket, typeof(POSTicketContract));
			var productLineIdSerialized = Serializer.SerializeJson(productLineIds, typeof(List<Guid>));
			return ExecuteAsync(sc => sc.VoidDispatchTicketOrdersAsync(ticketSerialized, productLineIdSerialized));
		}

        public Task<ValidationContractResult> AwayDispatchTicketOrdersAsync(POSTicketContract ticket, List<DispatchContract> productLineIds)
        {
            var ticketSerialized = Serializer.SerializeJson(ticket, typeof(POSTicketContract));
            var productLineIdSerialized = Serializer.SerializeJson(productLineIds, typeof(List<DispatchContract>));
            return ExecuteAsync(sc => sc.AwayDispatchTicketOrdersAsync(ticketSerialized, productLineIdSerialized));
        }

        public Task<ValidationContractResult> PersistentProductLineAsync(POSProductLineContract contract, Guid hotelId, Guid schemaId, bool included)
        {
			var serializedContract = Serializer.SerializeJson(contract, typeof(POSProductLineContract));
			return ExecuteAsync(sc => sc.PersistentProductLineAsync(serializedContract, schemaId.AsString(), hotelId.AsString(), included));
		}

        public Task<ValidationContractResult> AddProductToTicketAsync(POSTicketContract ticket, ProductRecord product, int quantity, decimal? manualPrice,  string manualPriceDesc)
        {
			var ticketSerialized = Serializer.SerializeJson(ticket, typeof(POSTicketContract));
			var productSerialized = Serializer.SerializeJson(product, typeof(ProductRecord));
			var quantitySerialized = Serializer.SerializeJson(quantity, typeof(int));
			var manualPriceSerialized = Serializer.SerializeJson(manualPrice, typeof(decimal?));

			return ExecuteAsync(sc => sc.AddProductToTicketAsync(ticketSerialized, productSerialized, quantitySerialized, manualPriceSerialized, manualPriceDesc, ""));
		}

		public Task<ValidationContractResult> CloseTicketAsync(POSTicketContract contract, ProductRecord tipProduct, bool saveClient)
        {
			var serializedContract = Serializer.SerializeJson(contract, typeof(POSTicketContract));
			var serializedTipProduct = Serializer.SerializeJson(tipProduct, typeof(ProductRecord));
			var serializedSaveClient = Serializer.SerializeJson(saveClient, typeof(bool));
			return ExecuteAsync(sc => sc.CloseTicketAsync(serializedContract, serializedTipProduct, serializedSaveClient));
		}

		public Task<ValidationContractResult> LoadClosedTicketAsync(Guid ticketId, bool local)
        {
			var serializedId = Serializer.SerializeJson(ticketId, typeof(Guid));
			var serializedLocal = Serializer.SerializeJson(local, typeof(bool));
			return ExecuteAsync(sc => sc.LoadClosedTicketAsync(serializedId, serializedLocal));
		}

		public Task<ValidationContractResult> CancelClosedTicketAsync(Guid ticketId, Guid cancellationReasonId, string comment, bool voidTicket, bool local, bool createQuickTicket)
        {
			var ticketIdSerialized = Serializer.SerializeJson(ticketId, typeof(Guid));
			var cancellationReasonIdSerialized = Serializer.SerializeJson(cancellationReasonId, typeof(Guid));
			var localSerialized = Serializer.SerializeJson(local, typeof(bool));
			var voidTicketSerialized = Serializer.SerializeJson(voidTicket, typeof(bool));
            var createQuickTicketSerialized = Serializer.SerializeJson(createQuickTicket, typeof(bool));
            return ExecuteAsync(sc => sc.CancelClosedTicketAsync(ticketIdSerialized, cancellationReasonIdSerialized, comment, voidTicketSerialized, localSerialized, createQuickTicketSerialized));
		}

		public Task<POSTicketContract[]> GetTicketByStandCashierAsync(Guid standId, Guid cashierId)
        {
			return ExecuteAsync(sc => sc.GetTicketByStandCashierAsync(standId.AsString(), cashierId.AsString()));
		}

        public Task<TicketInfo[]> GetTicketInfoByStandCashierAsync(Guid standId, Guid cashierId)
        {
			return ExecuteAsync(sc => sc.GetTicketInfoByStandCashierAsync(standId.AsString(), cashierId.AsString()));
		}

        public Task<SplitTicketResult> SplitTicketAutoAsync(Guid ticketId, int quantity)
        {
			var ticketIdSerialized = Serializer.SerializeJson(ticketId, typeof(Guid));
			var quantitySerialized = Serializer.SerializeJson(quantity, typeof(int));
			return ExecuteAsync(sc => sc.SplitTicketAutoAsync(ticketIdSerialized, quantitySerialized));
		}

		public Task<SplitTicketResult> SplitTicketByAlcoholicGroupAsync(Guid ticketId)
        {
			var ticketIdSerialized = Serializer.SerializeJson(ticketId, typeof(Guid));
			return ExecuteAsync(sc => sc.SplitTicketByAlcoholicGroupAsync(ticketIdSerialized));
		}

		public Task<SplitTicketResult> SplitTicketManualAsync(Guid ticketId, KeyValuePair<Guid, decimal>[] orderIds)
        {
			return ExecuteAsync(sc => sc.SplitTicketManualAsync(Serializer.SerializeJson(ticketId, typeof(Guid)), Serializer.SerializeJson(orderIds, typeof(KeyValuePair<Guid, decimal>[]))));
		}

		public Task<ValidationContractResult> CancelTicketProductAsync(POSTicketContract contract, Guid productLineId)
        {
			return ExecuteAsync(sc => sc.CancelTicketProductAsync(Serializer.SerializeJson(contract, typeof(POSTicketContract)), Serializer.SerializeJson(productLineId, typeof(Guid))));
		}

		public Task<ValidationContractResult> CancelTicketAsync(POSTicketContract contract, Guid cancellationReasonId, string comment, bool createQuickTicket)
        {
			return ExecuteAsync(sc => sc.CancelTicketAsync(Serializer.SerializeJson(contract, typeof(POSTicketContract)), Serializer.SerializeJson(cancellationReasonId, typeof(Guid)), comment, Serializer.Serialize(createQuickTicket)));
		}

		public Task<ValidationContractResult> CancelTicketByIdAsync(Guid ticketId, Guid cancellationReasonId, string comment, bool createQuickTicket)
        {
			return ExecuteAsync(sc => sc.CancelTicketByIdAsync(
				Serializer.SerializeJson(ticketId, typeof(Guid)), Serializer.SerializeJson(cancellationReasonId, typeof(Guid)), comment, Serializer.Serialize(createQuickTicket)));
		}

		public Task<ValidationContractResult> PersistTicketAsync(POSTicketContract contract)
        {
			var serializedContract = Serializer.SerializeJson(contract, typeof(POSTicketContract));
			return ExecuteAsync(sc => sc.PersistTicketAsync(serializedContract));
		}

		public Task<ValidationContractResult> SaveTicketFromEditionAsync(Guid ticketId, short paxs)
        {
			var ticketIdSerialized = Serializer.SerializeJson(ticketId, typeof(Guid));
			var paxsSerialized = Serializer.SerializeJson(paxs, typeof(short));
			return ExecuteAsync(sc => sc.SaveTicketFromEditionAsync(ticketIdSerialized, paxsSerialized));
		}

		public Task<ValidationContractResult> LoadTicketForEditionAsync(Guid ticketId, bool forceUnlock)
        {
			var ticketIdSerialized = Serializer.SerializeJson(ticketId, typeof(Guid));
			return ExecuteAsync(sc => sc.LoadTicketForEditionAsync(ticketIdSerialized, forceUnlock));
		}

		public Task<ValidationContractResult> CreateEmptyTicketToPaxWaitingListAsync(Guid tableId, Guid paxWaitingListId)
        {
			var table = Serializer.Serialize(tableId);
			var paxWaitingList = Serializer.Serialize(paxWaitingListId);
			return ExecuteAsync(sc => sc.CreateEmptyTicketToPaxWaitingListAsync(table, paxWaitingList));
		}

		public Task<ValidationContractResult> CreateEmptyTicketToReservationTableAsync(Guid[] tableIds, Guid reservationTableId, string name)
        {
			var table = Serializer.Serialize(tableIds);
			var reservationTable = Serializer.Serialize(reservationTableId);
			return ExecuteAsync(sc => sc.CreateEmptyTicketToReservationTableAsync(table, reservationTable, name));
		}

		public Task<ValidationContractResult> NewTicketAsync(Guid hotelId, Guid cajaId, Guid standId, Guid? tableId, Guid utilId)
        {
			return ExecuteAsync(sc => sc.NewTicketAsync(hotelId.AsString(), cajaId.AsString(), standId.AsString(), tableId.AsString(), utilId.AsString()));
		}
         
        public Task<DocumentSerieRecord[]> GetTicketSerieByStandCashierAsync(Guid standId, Guid cashierId)
        {
			return ExecuteAsync(sc => sc.GetTicketSerieByStandCashierAsync(standId.AsString(), cashierId.AsString()));
		}

        public Task<ValidationContractResult> MoveTicketToTable(POSTicketContract ticketContract, Guid? tableId)
        {
			var ticket = Serializer.Serialize(ticketContract);
			var table = Serializer.Serialize(tableId);
			return ExecuteAsync(sc => sc.MoveTicketToTableAsync(ticket, table));
		}

		public Task<ValidationContractResult> MoveTicketToTableById(Guid ticketId, Guid? tableId)
        {
			return ExecuteAsync(sc => sc.MoveTicketToTableByIdAsync(ticketId.AsString(), tableId.AsString()));
		}

        public Task<ValidationResult<string>> NotifyRoomServiceMonitorAsync(POSTicketContract contract)
        {
			var ticket = Serializer.Serialize(contract);
			return ExecuteAsync(sc => sc.NotifyRoomServiceMonitorAsync(ticket));
		}

		public Task<ValidationContractResult> IncrementPrintsAsync(Guid ticketId, int title)
        {
			return ExecuteAsync(sc => sc.IncrementPrintsAsync(ticketId.AsString(), title.AsString()));
		}

        public Task<ValidationContractResult> CheckOpenTicket(Guid ticketId, Guid standId, Guid cashierId)
        {
			return ExecuteAsync(sc => sc.CheckOpenTicketAsync(ticketId.AsString(), standId.AsString(), cashierId.AsString()));
		}
        
        public Task<ValidationContractResult> ChangeOrdersSeparatorAsync(POSTicketContract ticket, IEnumerable<Guid> productLineIds, Guid? separatorId)
        {
	        return ExecuteAsync(sc => sc.ChangeProductLinesSeparatorAsync(ticket.Id.ToString(), productLineIds.Select(x => x.AsString()).ToArray(), separatorId != null ? separatorId.ToString() : null));
        }

        public Task<ValidationContractResult> ChangeOrdersSeatAsync(POSTicketContract ticket, IEnumerable<Guid> productLineIds, short seat)
        {
	        return ExecuteAsync(sc => sc.ChangeProductLinesSeatAsync(ticket.Id.ToString(), productLineIds.Select(x => x.AsString()).ToArray(), seat));
        }
        
        public Task<ValidationContractResult> CreateQuickTicketFromClosedTicketAsync(string closedTicketId)
        {
	        return ExecuteAsync(sc => sc.CreateQuickTicketFromClosedTicketAsync(closedTicketId));
        }

        #endregion
        
        #region Signatures
        
        public Task<SignatureRecord[]> GetAllSignaturesAsync()
        {
	        return ExecuteAsync(sc => sc.GetAllSignaturesAsync());
        }

        public Task<SignatureRecord> GetSignatureAsync(string ticketId)
        {
	        return ExecuteAsync(sc => sc.GetSignatureAsync(ticketId));
        }

        public Task<ValidationContractResult> AddSignatureAsync(SignatureRecord signature)
        {
	        return ExecuteAsync(sc => sc.AddSignatureAsync(signature));
        }

        public Task<ValidationContractResult> UpdateSignatureAsync(string ticketId, SignatureRecord signature)
        {
	        return ExecuteAsync(sc => sc.UpdateSignatureAsync(ticketId, signature));
        }

        public Task<ValidationContractResult> DeleteSignatureAsync(string ticketId)
        {
	        return ExecuteAsync(sc => sc.DeleteSignatureAsync(ticketId));
        }
        
        #endregion
    }
}