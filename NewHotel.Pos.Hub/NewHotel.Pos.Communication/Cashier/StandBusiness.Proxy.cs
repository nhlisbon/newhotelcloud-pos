﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using NewHotel.Pos.Core;
using NewHotel.Pos.Hub.Contracts.Common.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.Records;
using NewHotel.Pos.Hub.Contracts.Common.Records.CurrentAccount;
using NewHotel.Pos.Hub.Contracts.Common.Records.Stands;
using NewHotel.Pos.Hub.Contracts.Common.Records.Tickets;
using NewHotel.Pos.Hub.Core;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Hub.Model.WaitingListStand.Contract;
using NewHotel.Pos.Hub.Model.WaitingListStand.Record;
using ClosedTicketRecord = NewHotel.Pos.Hub.Model.ClosedTicketRecord;
using UserRecord = NewHotel.Pos.Hub.Model.UserRecord;

namespace NewHotel.Pos.Communication.Cashier
{
	public class StandBusinessProxy : CashierBaseBusinessProxy, IStandBusinessProxy
    {
        #region IStandBusinessProxy

        public Task<string> SetStandCashierSessionTokenAsync(Guid standId, Guid cashierId, Guid schemaId, long language)
        {
            return ExecuteAsync(sc => InternalSetStandCashierSessionTokenAsync(sc, standId, cashierId,schemaId, language));
        }

        public Task<ValidationResult> CloseDayValidationsAsync(Guid standId, Guid cashierId, DateTime closeDay)
        {
			var stand = Serializer.Serialize(standId);
			var cashier = Serializer.Serialize(cashierId);
			var date = Serializer.Serialize(closeDay);
            return ExecuteAsync(sc => sc.CloseDayValidationsAsync(stand, cashier, date));
		}

		public Task<ValidationContractResult> CloseDayAsync(Guid standId, Guid cashierId, DateTime closeDay)
        {
			var stand = Serializer.Serialize(standId);
			var cashier = Serializer.Serialize(cashierId);
			var date = Serializer.Serialize(closeDay);
            return ExecuteAsync(sc => sc.CloseDayAsync(stand, cashier, date));
		}

		public Task<ValidationDateResult> UndoCloseDayAsync(Guid standId, Guid cashierId)
        {
			var stand = Serializer.Serialize(standId);
			var cashier = Serializer.Serialize(cashierId);
			return ExecuteAsync(sc => sc.UndoCloseDayAsync(stand, cashier));
		}

		public Task<ValidationContractResult> CloseTurnAsync(Guid standId, Guid cashierId)
        {
			return ExecuteAsync(sc => sc.CloseTurnAsync(standId.AsString(), cashierId.AsString()));
		}

        public Task<ValidationTicketsResult> AcceptTransferedTicketsAsync(List<TransferContract> contracts)
        {
			return ExecuteAsync(sc => sc.AcceptTransferedTicketsAsync(Serializer.Serialize(contracts)));
		}

        public Task<ValidationTicketsResult> ReturnTransferedTicketAsync(List<TransferContract> contract)
        {
			return ExecuteAsync(sc => sc.ReturnTransferedTicketsAsync(Serializer.Serialize(contract)));
		}

        public Task<ValidationContractResult> VerifyTransferToReturnAsync(Guid standId, int language)
        {
			return ExecuteAsync(sc => sc.VerifyTransferToReturnAsync(standId.AsString(), language.ToString()));
		}

        public Task<ValidationContractResult> VerifyTransferToAcceptAsync(Guid standId, int language)
        {
			return ExecuteAsync(sc => sc.VerifyTransferToAcceptAsync(standId.AsString(), language.ToString()));
		}

        public Task<ValidationResult> TransferTicketAsync(POSTicketContract ticket, TransferContract transfer)
        {
			return ExecuteAsync(sc => sc.TransferTicketAsync(Serializer.Serialize(ticket), Serializer.Serialize(transfer)));
		}

        public Task<LiteStandRecord[]> GetStandsForTransferAsync(string language)
        {
			return ExecuteAsync(sc => sc.GetStandsForTransferAsync(language));
		}

        public Task<POSStandRecord[]> GetStandsAsync(Guid cajaId, int language)
        {
			return ExecuteAsync(sc => sc.GetStandsAsync(cajaId.AsString(), language.ToString()));
		}

        public Task<CashierRecord[]> GetCashiersAsync(Guid hotelId, Guid userId, int language)
        {
			return ExecuteAsync(sc => sc.GetCashiersAsync(hotelId.AsString(), userId.AsString(), language.ToString()));
		}

        public async Task<ValidationItemResult<UserRecord>> GetUserByCodeAsync(string userCode)
        {
			return await InternalExecuteAsync(sc => sc.GetUserByCodeAsync(userCode));
		}

        public async Task<ValidationItemResult<UserRecord>> GetUserByNameAsync(string loginUser)
        {
			return await InternalExecuteAsync(sc => sc.GetUserByNameAsync(loginUser));
		}

        public Task<ValidationItemResult<StandEnvironment>> GetStandEnvironmentAsync(Guid standId, int language)
        {
			return ExecuteAsync(sc => sc.GetStandEnvironmentAsync(standId.AsString(), language.ToString()));
		}
	
        public Task<ValidationItemSource<ReservationRecord>> GetReservationsAsync(ReservationFilterContract filter)
        {
			return ExecuteAsync(sc => sc.GetReservationsAsync(filter));
		}

        public Task<ValidationItemSource<EventReservationSearchFromExternalRecord>> GetEventReservationsAsync(Guid? installationId)
        {
			return ExecuteAsync(sc => sc.GetEventReservationsAsync(installationId.AsString()));
		}

        public Task<ValidationItemSource<SpaReservationSearchFromExternalRecord>> GetSpaReservationsAsync(SpaSearchReservationFilterModel filter)
        {
			return ExecuteAsync(sc => sc.GetSpaReservationsV2Async(filter));
		}

        public Task<ValidationItemResult<SpaServiceRecord>> GetSpaServicesAsync(Guid? installationId, Guid spaId)
        { 
	        return ExecuteAsync(sc => sc.GetSpaServicesAsync(installationId.AsString(), spaId.AsString()));
        }

        public Task<ValidationItemSource<PmsCompanyRecord>> GetEntityInstallationsAsync(PmsCompanyFilterModel filter)
        {
			return ExecuteAsync(sc => sc.GetEntityInstallationsAsync(filter));
		}

        public Task<ValidationItemSource<ReservationsGroupsRecord>> GetReservationsGroupsAsync(Guid? installationId)
        {
			return ExecuteAsync(sc => sc.GetReservationsGroupsAsync(installationId.AsString()));
		}

        public Task<ValidationItemSource<PmsControlAccountRecord>> GetControlAccountsAsync(PmsControlAccountFilterModel filter)
        { 
	        return ExecuteAsync(sc => sc.GetControlAccountsAsync(filter));
        }

        public Task<ValidationItemSource<ControlAccountMovementDto>> GetControlAccountMovementAsync(ControlAccountMovementsFilterModel filter)
        { 
	        return ExecuteAsync(sc => sc.GetControlAccountMovementsAsync(filter));
        }

        public Task<ValidationItemSource<ClientInstallationRecord>> GetClientInstallationsAsync(bool openAccount, string room, string fullName, Guid? clientId = null, Guid? installationId = null)
        {
			var openAccountSerialized = Serializer.SerializeJson(openAccount, typeof(bool));
			return ExecuteAsync(sc => sc.GetClientInstallationsAsync(openAccountSerialized, clientId.AsString(), room, fullName, installationId.AsString()));
		}

		public Task<ValidationItemSource<ClosedTicketRecord>> GetOldTicketsAsync(Guid? standId, Guid? cashierId, DateTime? date)
        {
			var standSerialized = Serializer.SerializeJson(standId, typeof(Guid?));
			var cashierSerialized = Serializer.SerializeJson(cashierId, typeof(Guid?));
			var dateSerialized = Serializer.SerializeJson(date, typeof(DateTime?));
			return ExecuteAsync(sc => sc.GetOldTicketsAsync(standSerialized, cashierSerialized, dateSerialized));
		}

		public Task<SalesCashierRecord[]> GetSalesReportAsync(QueryRequest queryRequest, bool isTurnReport)
        {
			var serializedQueryRequest = Serializer.SerializeJson(queryRequest, typeof(QueryRequest));
            var serializedIsTurnReport = Serializer.SerializeJson(isTurnReport, typeof(bool));
			return ExecuteAsync(sc => sc.GetSalesReportAsync(serializedQueryRequest, serializedIsTurnReport));
		}

		public Task<ValidationItemSource<SalePaymentRecord>> GetPaymentsReportAsync(QueryRequest queryRequest)
        {
			var serializedQueryRequest = Serializer.SerializeJson(queryRequest, typeof(QueryRequest));
			return ExecuteAsync(sc => sc.GetPaymentsReportAsync(serializedQueryRequest));
		}

        public Task<ValidationResult> UpdateMesaAsync(MesaContract contract)
        {
			return ExecuteAsync(sc => sc.UpdateMesaAsync(Serializer.SerializeJson(contract, typeof(MesaContract))));
		}

        public Task<ValidationItemResult<PmsReservationRecord>> GetPmsReservationAsync(PmsReservationFilterModel filter)
        {
            return ExecuteAsync(sc => sc.GetPmsReservationAsync(filter));
        }

        #region Reservation Table

        public Task<ValidationItemSource<LiteTablesReservationRecord>> GetTableReservationsAsync(TablesReservationFilterContract filterContract)
        {
			var filterSerialized = Serializer.Serialize(filterContract);
			return ExecuteAsync(sc => sc.GetTableReservationsAsync(filterSerialized));
		}

		public Task<ValidationResult> CancelTableReservationAsync(Guid reservationTableId, Guid cancellationReasonId, string comments)
        {
			var id = Serializer.Serialize(reservationTableId);
			var cancellationReason = Serializer.Serialize(cancellationReasonId);
			return ExecuteAsync(sc => sc.CancelTableReservationAsync(id, cancellationReason, comments));
		}

        public Task<ValidationItemSource<StandTimeSlotRecord>> GetStandTimeSlotsAsync(Guid standId, DateTime date)
        {
            var stand = Serializer.Serialize(standId);
            var dateSerialized = Serializer.Serialize(date);
            return ExecuteAsync(sc => sc.GetStandTimeSlotsAsync(stand, dateSerialized));
        }

		#endregion

		#region Waiting List

		public Task<ValidationItemSource<PaxWaitingListRecord>> GetWaitingListAsync(WaitingListFilterContract filter)
        {
			return ExecuteAsync(sc => sc.GetWaitingListAsync(Serializer.Serialize(filter)));
		}

        public Task<ValidationContractResult> PersistPaxWaitingListAsync(PaxWaitingListContract contract)
        {
			return ExecuteAsync(sc => sc.PersistPaxWaitingListAsync(Serializer.Serialize(contract)));
		}

        public Task<ValidationResult> DeletePaxWaitingListAsync(Guid paxWaintingListId)
        {
			return ExecuteAsync(sc => sc.DeletePaxWaitingListAsync(Serializer.Serialize(paxWaintingListId)));
		}

        #endregion

        #region Meals Control

        public Task<ValidationItemSource<MealsControlRecord>> GetMealsControl(MealsFilterModel model)
        {
			return ExecuteAsync(sc => sc.GetMealsControlAsync(model));
		}

        public Task<ValidationResult> UpdateMealsControl(Guid guestId, bool breakfast, bool lunch, bool dinner, Guid? installationId = null)
        {
			return ExecuteAsync(sc => sc.UpdateMealsControlAsync(guestId.ToString(), breakfast.ToString(), lunch.ToString(), dinner.ToString(), installationId.ToString()));
		}

        public Task<ValidationItemResult<ClientInfoContract>> GetClientInfo(Guid clientId)
        {
			return ExecuteAsync(sc => sc.GetClientInfoAsync(clientId.ToString()));
		}

        #endregion

        #region Multi Hotel

        public Task<ValidationItemSource<ChargeHotelRecord>> GetChargeHotelsAsync()
        {
			return ExecuteAsync(sc => sc.GetChargeHotelsAsync());
		}

        #endregion

        #endregion
    }

    public interface IStandBusinessProxy
    {
        #region Members

        Task<ValidationDateResult> UndoCloseDayAsync(Guid standId, Guid cashierId);
        Task<string> SetStandCashierSessionTokenAsync(Guid standId, Guid cashierId, Guid schemaId, long language);
        Task<ValidationResult> CloseDayValidationsAsync(Guid standId, Guid cashierId, DateTime closeDay);
        Task<ValidationContractResult> CloseDayAsync(Guid standId, Guid cashierId, DateTime closeDay);
        Task<ValidationContractResult> CloseTurnAsync(Guid standId, Guid cashierId);
        Task<ValidationTicketsResult> AcceptTransferedTicketsAsync(List<TransferContract> contracts);
        Task<ValidationContractResult> VerifyTransferToReturnAsync(Guid standId, int language);
        Task<ValidationContractResult> VerifyTransferToAcceptAsync(Guid standId, int language);
        Task<ValidationResult> TransferTicketAsync(POSTicketContract ticket, TransferContract transfer);
        Task<LiteStandRecord[]> GetStandsForTransferAsync(string language);
        Task<POSStandRecord[]> GetStandsAsync(Guid cajaId, int language);
        Task<CashierRecord[]> GetCashiersAsync(Guid hotelId, Guid userId, int language);
        Task<ValidationItemResult<UserRecord>> GetUserByCodeAsync(string userCode);
        Task<ValidationItemResult<UserRecord>> GetUserByNameAsync(string loginUser);
        Task<ValidationItemResult<StandEnvironment>> GetStandEnvironmentAsync(Guid standId, int language);
        Task<ValidationItemSource<ReservationRecord>> GetReservationsAsync(ReservationFilterContract filter);
        Task<ValidationItemSource<EventReservationSearchFromExternalRecord>> GetEventReservationsAsync(Guid? installationId);
        Task<ValidationItemSource<SpaReservationSearchFromExternalRecord>> GetSpaReservationsAsync(SpaSearchReservationFilterModel filter);
        Task<ValidationItemResult<SpaServiceRecord>> GetSpaServicesAsync(Guid? installationId, Guid spaId);
        Task<ValidationItemSource<PmsCompanyRecord>> GetEntityInstallationsAsync(PmsCompanyFilterModel filter);
        Task<ValidationItemSource<ReservationsGroupsRecord>> GetReservationsGroupsAsync(Guid? installationId);
        Task<ValidationItemSource<PmsControlAccountRecord>> GetControlAccountsAsync(PmsControlAccountFilterModel filter);
        Task<ValidationItemSource<ClientInstallationRecord>> GetClientInstallationsAsync(bool openAccounts, string room, string fullName, Guid? clientId = null, Guid? installationId = null);
        Task<ValidationItemSource<ClosedTicketRecord>> GetOldTicketsAsync(Guid? standId, Guid? cashierId, DateTime? date);
        Task<SalesCashierRecord[]> GetSalesReportAsync(QueryRequest queryRequest, bool isTurnReport);
        Task<ValidationItemSource<SalePaymentRecord>> GetPaymentsReportAsync(QueryRequest queryRequest);
        Task<ValidationResult> UpdateMesaAsync(MesaContract contract);
        Task<ValidationItemSource<LiteTablesReservationRecord>> GetTableReservationsAsync(TablesReservationFilterContract filterContract);
        Task<ValidationResult> CancelTableReservationAsync(Guid reservationTableId, Guid cancellationReasonId, string comments);
        Task<ValidationItemSource<PaxWaitingListRecord>> GetWaitingListAsync(WaitingListFilterContract filter);
        Task<ValidationContractResult> PersistPaxWaitingListAsync(PaxWaitingListContract contract);
        Task<ValidationResult> DeletePaxWaitingListAsync(Guid paxWaintingListId);
        Task<ValidationTicketsResult> ReturnTransferedTicketAsync(List<TransferContract> contract);
        Task<ValidationItemSource<MealsControlRecord>> GetMealsControl(MealsFilterModel model);
        Task<ValidationResult> UpdateMealsControl(Guid guestId, bool breakfast, bool lunch, bool dinner, Guid? installationId = null);
        Task<ValidationItemResult<ClientInfoContract>> GetClientInfo(Guid clientId);
        Task<ValidationItemSource<ChargeHotelRecord>> GetChargeHotelsAsync();
        Task<ValidationItemSource<StandTimeSlotRecord>> GetStandTimeSlotsAsync(Guid standId, DateTime date);
        Task<ValidationItemResult<PmsReservationRecord>> GetPmsReservationAsync(PmsReservationFilterModel filter);

        #endregion
    }
}