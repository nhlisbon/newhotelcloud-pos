﻿using System;
using System.Threading.Tasks;
using NewHotel.Contracts;
using NewHotel.Pos.Hub.Contracts.PaySystem;
using NewHotel.Pos.Integration.Http.Http;

namespace NewHotel.Pos.Communication.Cashier
{
	public interface IPaySystemBusinessProxy
	{
		Task<ValidationResult<RequestTerminalsContract>> GetPayTerminalsAsync(Guid? originId);
		Task<ValidationResult<ExternalPaymentResponseContract>> RequestExternalPaymentAsync(ExternalPaymentRequestContract request);
		Task<ValidationResult<ExternalPaymentEntryContract>> UpdatePendingExternalPaymentAsync(Guid entryId);
	}

	public sealed class PaySystemBusinessProxy : CashierBaseBusinessProxy, IPaySystemBusinessProxy
	{
		public Task<ValidationResult<RequestTerminalsContract>> GetPayTerminalsAsync(Guid? originId)
		{
			return ExecuteAsync(sc => sc.GetPayTerminalsAsync(originId.ToString()));
		}

		public async Task<ValidationResult<ExternalPaymentResponseContract>> RequestExternalPaymentAsync(ExternalPaymentRequestContract request)
		{
			var result = await ExecuteAsync(sc => sc.RequestExternalPaymentAsync(request.Serialize()));
			return ClientExtension.Deserialize<ValidationResult<ExternalPaymentResponseContract>>(result);
		}

		public Task<ValidationResult<ExternalPaymentEntryContract>> UpdatePendingExternalPaymentAsync(Guid entryId)
		{
			return ExecuteAsync(sc => sc.UpdatePendingExternalPaymentAsync(entryId.ToString()));
		}
	}
}
