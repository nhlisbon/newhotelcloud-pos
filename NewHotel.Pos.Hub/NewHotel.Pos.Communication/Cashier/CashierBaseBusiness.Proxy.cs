﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.Threading.Tasks;
using MilestoneTG.TransientFaultHandling;
using NewHotel.Pos.Hub.Core;
using NewHotel.Diagnostics;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace NewHotel.Pos.Communication.Cashier
{
	public class CashierBaseBusinessProxy : ICashierBusinessProxy
	{
		#region Events

		public static event EventHandler DatabaseSyncErrorEvent;

		public static event EventHandler VersionErrorEvent;

		public static event EventHandler<Exception> DatabaseVersionErrorEvent;

		public static event EventHandler<DateTime> WorkDateErrorEvent;

		#endregion
		#region Properties

		public static string Version
		{
			get
			{
				var version = Assembly.GetEntryAssembly().GetName().Version;
				return $"{version.Major}.{version.Minor}.{version.Build}";
			}
		}

		public static Guid Token { get; set; } = Guid.Empty;

		public static string StandWorkDate { get; set; } = String.Empty;

		public static Guid InstallationId { get; set; } = Guid.Empty;

		public static Guid UserId { get; set; } = Guid.Empty;

		public static Guid CashierId { get; set; } = Guid.Empty;

		public static DateTime WorkDate { get; set; } = DateTime.Today;

		public static Guid StandId { get; set; } = Guid.Empty;

		public static Guid SchemaId { get; set; } = Guid.Empty;

		public static long Language { get; set; } = 0;

		#endregion
		#region ICashierBaseBusinessProxy Members

		public async Task<Guid> CreateSessionTokenAsync(Guid installationId, Guid userId)
		{
			return await ExecuteAsync(sc => InternalCreateSessionTokenAsync(sc, installationId, userId));
		}

		private async Task<Guid> InternalCreateSessionTokenAsync(ServiceClient sc, Guid installationId, Guid userId)
		{
			InstallationId = installationId;
			UserId = userId;
			return Token = await sc.CreateSessionTokenAsync(installationId.AsString(), userId.AsString());
		}

		protected async Task<string> InternalSetStandCashierSessionTokenAsync(ServiceClient sc, Guid standId, Guid cashierId, Guid schemaId, long language)
		{
			StandId = standId;
			CashierId = cashierId;
			SchemaId = schemaId;
			Language = language;
			var result = await sc.SetTokenContextDataAsync(standId.AsString(), cashierId.AsString(), schemaId.AsString(), language.ToString());
			WorkDate = DateTime.ParseExact(result, "yyyyMMdd", CultureInfo.InvariantCulture);
			StandWorkDate = result;
			return result;
		}


		public bool CheckCommunication()
		{
			bool ret;

			using (var sc = GetCashierServiceClient())
			{
				try
				{
					sc.Open();
					ret = sc.State == System.ServiceModel.CommunicationState.Opened;
					sc.Close();
				}
				catch
				{
					sc.Abort();
					ret = false;
				}
			}
			return ret;
		}

		public Guid GetToken(bool force = false)
		{
			if (Guid.Empty == Token || force)
			{
				Token = InternalExecute(sc => sc.CreateSessionToken(InstallationId.AsString(), UserId.AsString()));
			}
			return Token;
		}

		public Task<Guid> GetTokenAsync(bool force = false)
		{
			if (Guid.Empty == Token || force)
			{
				return CreateSessionTokenAsync(InstallationId, UserId);
			}
			return Task.FromResult(Token);
		}

		public Task<Stream> GetCashierFilesAsync()
		{
			return ExecuteAsync(sc => sc.GetCashierFilesAsync());
		}


		#endregion
		#region Aux

		protected ServiceClient GetCashierServiceClient()
		{
			var result = new ServiceClient("CashierService");
			result.Endpoint.EndpointBehaviors.Add(GetCustomEndpointBehavior());
			return result;
		}


		public class CustomMessageInspector : IClientMessageInspector
		{
			Action onNeedReload;

			public CustomMessageInspector(Action onNeedReload)
			{
				this.onNeedReload = onNeedReload;
			}

			public void AfterReceiveReply(ref Message reply, object correlationState)
			{
				// Inspect the incoming message headers
				foreach (var header in reply.Headers)
				{
					if (header.Name == "NeedReload")
					{
						onNeedReload?.Invoke();
						break;
					}
				}
			}

			public object BeforeSendRequest(ref Message request, IClientChannel channel)
			{
				// Optionally, you can add custom headers here
				return null;
			}
		}

		public class CustomEndpointBehavior : IEndpointBehavior
		{
			CustomMessageInspector messageInspector;

			public CustomEndpointBehavior(Action onNeedReload)
			{
				messageInspector = new CustomMessageInspector(onNeedReload);
			}

			public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters) { }

			public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
			{
				clientRuntime.MessageInspectors.Add(messageInspector);
			}

			public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher) { }

			public void Validate(ServiceEndpoint endpoint) { }
		}

		private CustomEndpointBehavior endpointBehavior;
		private bool needReload = false;
		CustomEndpointBehavior GetCustomEndpointBehavior()
		{
			return endpointBehavior ??= new CustomEndpointBehavior(() => needReload = true);
		}

		protected async Task<R> InternalExecuteAsync<R>(Func<ServiceClient, Task<R>> action)
		{
			ServiceClient sc = GetCashierServiceClient();
			try
			{
				R result = await action(sc);
				sc.Close();
				if (needReload)
				{
					needReload = false;
					DatabaseSyncErrorEvent?.Invoke(null, null);
				}
				return result;
			}
			catch
			{
				sc.Abort();
				throw;
			}
		}

		protected R InternalExecute<R>(Func<ServiceClient, R> action)
		{
			var sc = GetCashierServiceClient();
			try
			{
				R result = action(sc);
				sc.Close();
				return result;
			}
			catch
			{
				sc.Abort();
				throw;
			}
		}

		protected async Task<R> ExecuteAsync<R>(Func<ServiceClient, Task<R>> action, [CallerMemberName] string actionName = null)
		{
			bool retry = false;
			int times = 3;
			do
			{
				try
				{
					return await InternalExecuteAsync(action);
				}
				catch (CommunicationException commFail)
				{
					(bool isTransient, bool requestToken) = IsErrorTransient(this, commFail);

					if (isTransient && requestToken)
					{
						await InternalExecuteAsync(sc => InternalCreateSessionTokenAsync(sc, InstallationId, UserId));
						await InternalExecuteAsync(sc => InternalSetStandCashierSessionTokenAsync(sc, StandId, CashierId, SchemaId, Language));
						times--;
						retry = times > 0;
					}
					else
					{
						retry = false;
					}

					if (!retry)
						throw;
				}
			} while (retry);

			return default;
		}

		protected Task<T> ExecuteAsync<T>(Func<Task<T>> taskAction, [CallerMemberName] string taskCallerName = null)
		{
			var retryPolicy = GetRetryPolicy();
			int retryCount = 0;
			retryPolicy.Retrying += (sender, args) => { retryCount = args.CurrentRetryCount; };

			return retryPolicy.ExecuteAsync(async () =>
			{
				if (retryCount > 0)
					taskCallerName += " (" + retryCount + ")";

				using (TraceDebug.TraceScope(taskCallerName, Guid.NewGuid()))
				{
					return await taskAction.Invoke();
				}
			});
		}

		private RetryPolicy GetRetryPolicy()
		{
			return new RetryPolicy(new CashierErrorDetectionStrategy(), 3, TimeSpan.FromTicks(100));
		}

		internal static (bool, bool) IsErrorTransient(object source, Exception ex)
		{
			(bool isTransient, bool requestToken) failure = (false, false);
			if (ex is FaultException faultException)
			{
				Debug.Write(ex.Message);
				switch (faultException.Code.Name)
				{
					//Database Version
					case "5":
						DatabaseVersionErrorEvent?.Invoke(source, ex);
						return failure;
					//Client Version
					case "4":
						{
							if (WorkDateErrorEvent != null)
							{
								DateTime workDate = DateTime.ParseExact(faultException.Message, "yyyyMMdd", CultureInfo.InvariantCulture);
								WorkDate = workDate;
								StandWorkDate = faultException.Message;
								WorkDateErrorEvent(faultException, workDate);
							}
							return (true, false);
						}
					case "3":
						VersionErrorEvent?.Invoke(faultException, null);
						return failure;
					//Database Sync
					case "2":
						DatabaseSyncErrorEvent?.Invoke(null, null);
						return (true, false);
					//Token invalid
					case "1":
						if (Guid.Empty != InstallationId)
						{
							return (true, true);
						}
						break;
				}
			}
			return failure;
		}

		private class CashierErrorDetectionStrategy : ITransientErrorDetectionStrategy
		{
			public bool IsTransient(Exception ex)
			{
				(bool isTransient, bool requestToken) = IsErrorTransient(this, ex);
				if (isTransient && requestToken)
				{
					Task.Run(async () => await BusinessProxyRepository.Cashier.GetTokenAsync(true)).Wait();
					Task.Run(async () => await BusinessProxyRepository.Stand.SetStandCashierSessionTokenAsync(StandId, CashierId, SchemaId, Language)).Wait();
				}
				return isTransient;
			}
		}

		#endregion
	}

	public interface ICashierBusinessProxy
	{
		#region Members

		bool CheckCommunication();
		Task<Guid> CreateSessionTokenAsync(Guid installationId, Guid userId);
		Guid GetToken(bool force = false);
		Task<Guid> GetTokenAsync(bool force = false);
		Task<Stream> GetCashierFilesAsync();

		#endregion
	}
}
