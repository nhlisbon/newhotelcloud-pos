﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using NewHotel.Pos.Hub.Core;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Hub.Model.Cashier.Contracts.Settings;
using HotelRecord = NewHotel.Pos.Hub.Model.HotelRecord;
using NewHotel.Pos.Core;

namespace NewHotel.Pos.Communication.Cashier
{
    public class SettingsBusinessProxy : CashierBaseBusinessProxy, ISettingsBusinessProxy
    {
        #region ISettingsBusinessProxy Implementation

        public Task<ValidationItemSource<ClientRecord>> GetClientEntitiesAsync(QueryRequest queryRequest)
        {
            return ExecuteAsync(sc => sc.GetClientEntitiesAsync(Serializer.SerializeJson(queryRequest, typeof(QueryRequest))));
        }

        public Task<HotelRecord[]> GetHotelsAsync(Guid userId)
        {
            return ExecuteAsync(sc => sc.GetHotelsAsync(userId.AsString()));
        }

        public Task<POSGeneralSettingsRecord> GetGeneralSettingsAsync(bool fromCloud = false)
        {
            return ExecuteAsync(sc => sc.GetGeneralSettingsAsync(fromCloud));
        }

        public Task<ValidationResult> ResetSyncStatusAsync()
        {
            return ExecuteAsync(sc => sc.ResetSyncStatusAsync());
		}

        public Task<ValidationItemSource<DownloadedImageContact>> DownloadImages(KeyValuePair<string, DateTime>[] paths)
        {
            return ExecuteAsync(sc => sc.DownloadImagesAsync(paths));
		}

        public async Task<ValidationResult<string>> SyncLocalStockAsync()
        {
            return await ExecuteAsync(sc => sc.SyncLocalStockAsync());
        }

        #endregion
    }


    public interface ISettingsBusinessProxy 
    {
        #region Members

        Task<ValidationItemSource<ClientRecord>> GetClientEntitiesAsync(QueryRequest request);
        Task<HotelRecord[]> GetHotelsAsync(Guid userId);
        Task<POSGeneralSettingsRecord> GetGeneralSettingsAsync(bool fromCloud = false);
        Task<ValidationResult> ResetSyncStatusAsync();
        Task<ValidationItemSource<DownloadedImageContact>> DownloadImages(KeyValuePair<string, DateTime>[] paths);
        Task<ValidationResult<string>> SyncLocalStockAsync();

        #endregion
    }
}
