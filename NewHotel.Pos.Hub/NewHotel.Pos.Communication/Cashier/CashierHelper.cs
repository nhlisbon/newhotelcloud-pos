﻿using System;
using System.Collections.Generic;
using System.Reflection;
using NewHotel.Pos.Hub.Contracts.Common.Records.Products;
using NewHotel.Pos.Hub.Contracts.Common.Records.Stands;
using NewHotel.Pos.Hub.Model;

namespace NewHotel.Pos.Communication.Cashier
{
    /// <summary>
    /// Helper class for services
    /// </summary>
	static class CashierHelper
	{
        /// <summary>
        /// 
        /// </summary>
		static readonly List<Type> KnownTypes;

		static CashierHelper()
		{
			KnownTypes = new List<Type>
			{
				typeof(NewHotel.Contracts.ApplyTax2),
				typeof(NewHotel.Contracts.ApplyTax3),
				typeof(NewHotel.Contracts.BaseContract<System.Guid>),
				typeof(NewHotel.Contracts.BaseObject),
				typeof(NewHotel.Contracts.BaseRecord),
				typeof(NewHotel.Contracts.CancellationControlContract),
				typeof(NewHotel.Contracts.CancellationType),
				typeof(NewHotel.Contracts.ClientInstallationRecord),
				typeof(NewHotel.Contracts.ContainerTransferContract),
				typeof(NewHotel.Contracts.ControlAccountRecord),
				typeof(NewHotel.Contracts.CreditCardContract),
				typeof(NewHotel.Contracts.CurrentAccountType),
				typeof(NewHotel.Contracts.DailyAccountType),
				typeof(NewHotel.Contracts.DocumentContract),
				typeof(NewHotel.Contracts.DocumentSign),
				typeof(NewHotel.Contracts.EPosMesaType),
				typeof(NewHotel.Contracts.EntityInstallationRecord),
				typeof(NewHotel.Contracts.FactInformationContract),
				typeof(NewHotel.Contracts.InvoiceDestination),
				typeof(NewHotel.Contracts.LogTicketContract),
				typeof(NewHotel.Contracts.LookupTableContract),
				typeof(NewHotel.Contracts.POSDocumentType),
				typeof(NewHotel.Contracts.POSPaymentLineContract),
				typeof(NewHotel.Contracts.POSProductLineContract),
				typeof(NewHotel.Contracts.POSTicketContract),
				typeof(NewHotel.Contracts.PaymentLineContract),
				typeof(NewHotel.Contracts.PaymentServiceProvider),
				typeof(NewHotel.Contracts.PersonalDocContract),
				typeof(NewHotel.Contracts.Pos.Records.POSGeneralSettingsRecord),
				typeof(NewHotel.Contracts.Pos.Records.POSStandRecord),
				typeof(NewHotel.Contracts.Pos.Records.POSTicketPrintConfigurationRecord),
				typeof(NewHotel.Contracts.ProductLineContract),
				typeof(NewHotel.Contracts.ProductPriceType),
				typeof(NewHotel.Contracts.ReceivableType),
				typeof(NewHotel.Contracts.ReservationSearchFromExternalRecord),
				typeof(NewHotel.Contracts.ReservationState),
				typeof(NewHotel.Contracts.ReservationsGroupsRecord),
				typeof(NewHotel.Contracts.SalesCajaRecord),
				typeof(NewHotel.Contracts.SalesCashierRecord),
				typeof(NewHotel.Contracts.SerialNumberResult),
				typeof(NewHotel.Contracts.SerieStatus),
				typeof(NewHotel.Contracts.StandTurnDateContract),
				typeof(NewHotel.Contracts.TicketRContract),
				typeof(NewHotel.Contracts.TicketRealRecord),
				typeof(NewHotel.Contracts.TransferContract),
				typeof(NewHotel.Contracts.Validation),
				typeof(NewHotel.Contracts.ValidationError),
				typeof(NewHotel.Contracts.ValidationResult),
				typeof(NewHotel.Contracts.ValidationWarning),
				typeof(NewHotel.DataAnnotations.ARGBColor),
				typeof(NewHotel.DataAnnotations.Blob),
				typeof(AreasRecord),
				typeof(CancellationTypesRecord),
				typeof(CashierRecord),
				typeof(CategoriesRecord),
				typeof(ColumnsPlusProductsRecord),
				typeof(CreditCardTypesRecord),
				typeof(DiscountTypesRecord),
				typeof(DocumentSerieRecord),
				typeof(FamilyRecord),
				typeof(GroupRecord),
				typeof(HotelEnvironment),
				typeof(HotelRecord),
				typeof(HouseUseRecord),
				typeof(ImageRecord),
				typeof(LiteStandRecord),
				typeof(NationalityRecord),
				typeof(StandEnvironment),
				typeof(CategoryOption),
				typeof(PaymentMethodsRecord),
				typeof(PreparationRecord),
				typeof(ProductRecord),
				typeof(SaloonEnvironment),
				typeof(SaloonRecord),
				typeof(SeparatorsRecord),
				typeof(SubCategoryOption),
				typeof(SubCategoriesRecord),
				typeof(SubFamilyRecord),
				typeof(TableRecord),
				typeof(TicketDutyRecord),
				typeof(UserRecord),
                typeof(ClosedTicketRecord)
			};
		}
        /// <summary>
        /// Get all Known Types (Statically defined)
        /// </summary>
        /// <param name="provider">Provider</param>
        /// <returns>IEnumerable with all Know Types</returns>
		public static IEnumerable<Type> GetKnownTypes(ICustomAttributeProvider provider)
		{
			return KnownTypes;
		}
	}
}
