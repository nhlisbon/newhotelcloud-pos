﻿using System;
using System.Threading.Tasks;
using NewHotel.Contracts;
using NewHotel.Pos.Hub.Core;
using NewHotel.Pos.Hub.Model;

namespace NewHotel.Pos.Communication.Cashier
{
    internal class ProductBusinessProxy : CashierBaseBusinessProxy, IProductBusinessProxy
    {
        #region IProductBusinessProxy Implementation

        public Task<PreparationRecord[]> GetPreparationsByProductsAsync(Guid productId, int language)
        {
            return ExecuteAsync(sc => sc.GetPreparationsByProductsAsync(productId.AsString(), language.ToString()));
        }

        public Task<ValidationItemSource<ProductPriceRecord>> GetProductPricesAsync(Guid standId, Guid rateId)
        {
            return ExecuteAsync(sc => sc.GetProductPricesAsync(standId.AsString(), rateId.AsString()));
		}

        public Task<ValidationItemResult<ImageRecord>> GetImageAsync(string path)
        {
            return ExecuteAsync(sc => sc.GetImageAsync(path));
		}

        #endregion
    }

    public interface IProductBusinessProxy 
    {
        #region Members

        Task<PreparationRecord[]> GetPreparationsByProductsAsync(Guid productId, int language);
        Task<ValidationItemSource<ProductPriceRecord>> GetProductPricesAsync(Guid standId, Guid rateId);
        Task<ValidationItemResult<ImageRecord>> GetImageAsync(string path);

        #endregion
    }
}
