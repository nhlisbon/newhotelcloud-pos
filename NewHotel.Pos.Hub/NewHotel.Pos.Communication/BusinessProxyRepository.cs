﻿using NewHotel.Pos.Communication.Api;
using NewHotel.Pos.Communication.Cashier;
using NewHotel.Pos.IoC;

namespace NewHotel.Pos.Communication
{
    public static class BusinessProxyRepository
    {
        static BusinessProxyRepository()
        {
            CwFactory.Instance.RegisterSingleton<IPosHubApiClientFactory, PosHubApiClientFactory>();
            CwFactory.Instance.RegisterSingletonUsingFactory<IPosHubApiClient, IPosHubApiClientFactory>(f => f.Create());

            CwFactory.Register<IProductBusinessProxy, ProductBusinessProxy>();
            CwFactory.Register<ISettingsBusinessProxy, SettingsBusinessProxy>();
            CwFactory.Register<ITicketBusinessProxy, TicketBusinessProxy>();
            CwFactory.Register<IStandBusinessProxy, StandBusinessProxy>();
            CwFactory.Register<ICashierBusinessProxy, CashierBaseBusinessProxy>();
            CwFactory.Register<IPaySystemBusinessProxy, PaySystemBusinessProxy>();
        }

        public static ICashierBusinessProxy Cashier => CwFactory.Resolve<ICashierBusinessProxy>();
        public static ISettingsBusinessProxy Settings => CwFactory.Resolve<ISettingsBusinessProxy>();
        public static IProductBusinessProxy Product => CwFactory.Resolve<IProductBusinessProxy>();
        public static ITicketBusinessProxy Ticket => CwFactory.Resolve<ITicketBusinessProxy>();
        public static IStandBusinessProxy Stand => CwFactory.Resolve<IStandBusinessProxy>();
        public static IPaySystemBusinessProxy PaySystem => CwFactory.Resolve<IPaySystemBusinessProxy>();
        public static IPosHubApiClient ApiClient => CwFactory.Resolve<IPosHubApiClient>();
	}
}
