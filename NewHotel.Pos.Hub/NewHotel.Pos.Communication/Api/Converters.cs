﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Threading.Tasks;

namespace NewHotel.Pos.Communication.Api
{
	public class GuidConverter : JsonConverter<object>
	{
		const string GuidPrefix = "\xE2\x90\x90G";

		public override object Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			switch (reader.TokenType)
			{
				case JsonTokenType.String:
					var asString = reader.GetString();
					if (asString.StartsWith(GuidPrefix) && Guid.TryParse(asString.Substring(4), out var guid))
						return guid;
					return asString;
			}
			return JsonSerializer.Deserialize(ref reader, typeToConvert, options);
		}

		public override void Write(Utf8JsonWriter writer, object value, JsonSerializerOptions options)
		{
			if (value.GetType() == typeof(Guid))
			{
				writer.WriteStringValue($"{GuidPrefix}{value:n}");
			}
			else
			{
				JsonSerializer.SerializeToElement(value, value.GetType(), options).WriteTo(writer);
			}
		}
	}

	public class TypeConverter : JsonConverter<Type>
	{
		const string TypePrefix = "\xE2\x90\x90T";

		public override Type Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			switch (reader.TokenType)
			{
				case JsonTokenType.String:
					var asString = reader.GetString();

					if (asString.StartsWith(TypePrefix) && Type.GetType(asString) is Type type)
						return type;
					return null;
			}
			return null;
		}

		public override void Write(Utf8JsonWriter writer, Type value, JsonSerializerOptions options)
		{
			writer.WriteStringValue($"{TypePrefix}{value.AssemblyQualifiedName}");
		}

		public override bool CanConvert(Type typeToConvert)
		{
			return base.CanConvert(typeToConvert);
		}
	}
}
