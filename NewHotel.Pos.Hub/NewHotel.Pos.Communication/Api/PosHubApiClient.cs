﻿using NewHotel.Contracts.Pos.Records;
using NewHotel.Pos.Communication.Cashier;
using NewHotel.Pos.Hub.Api.Models;
using NewHotel.Pos.Hub.Contracts.Common.Records.Stands;
using NewHotel.Pos.Hub.Contracts.PaySystem;
using NewHotel.Pos.Hub.Core;
using NewHotel.Pos.Hub.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.ServiceModel.Configuration;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace NewHotel.Pos.Communication.Api
{
	public class PosHubApiClientFactory : IPosHubApiClientFactory
	{
		public IPosHubApiClient Create(string baseAddress = default)
		{
			if (string.IsNullOrEmpty(baseAddress))
			{
				var ss = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
				var sg = ss.GetSectionGroup("system.serviceModel");//System.ServiceModel.Configuration.ServiceModelSectionGroup
				var cl = sg.Sections.Get("client") as System.ServiceModel.Configuration.ClientSection;
				var address = cl.Endpoints.Cast<ChannelEndpointElement>().FirstOrDefault(x => x.Name == "CashierService").Address;
				var host = address.Host;
				var port = address.Port;
				baseAddress = $"http://{host}:{port + 1}";
			}

			return new PosHubApiClient(baseAddress);
		}
	}

	public class PosHubApiClient : IPosHubApiClient
	{
		static readonly Lazy<HttpClientHandler> handler = new Lazy<HttpClientHandler>(() => new HttpClientHandler());

		private Guid savedSessionId;
		private string token;
		private string baseAddress;

		public PosHubApiClient(string baseAddress)
		{
			this.baseAddress = baseAddress;
			savedSessionId = CashierBaseBusinessProxy.Token;
		}

		private HttpClient httpClient;
		HttpClient GetClient()
		{
			var client = new HttpClient(handler.Value)
			{
				BaseAddress = new Uri(baseAddress.TrimEnd('/'))
			};
			return client;
		}
		HttpClient HttpClient => httpClient ??= GetClient();

		void SetToken(HttpRequestMessage request)
		{
			if (!string.IsNullOrEmpty(token))
				request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
		}

		async Task VerifyAndResolveToken(CancellationToken cancellation)
		{
			bool noToken = string.IsNullOrEmpty(token);
			bool hasSession = CashierBaseBusinessProxy.Token != Guid.Empty;
			bool invalidSession = savedSessionId != CashierBaseBusinessProxy.Token;

			if (hasSession && (noToken || invalidSession))
			{
				LoginModel loginModel = new()
				{
					InstallationId = CashierBaseBusinessProxy.InstallationId,
					UserId = CashierBaseBusinessProxy.UserId,
					LanguageId = CashierBaseBusinessProxy.Language,
					CurrentSessionId = CashierBaseBusinessProxy.Token,
				};

				token = null;
				await LoginAsync(loginModel, cancellation);
			}
			else if (!hasSession)
			{
				token = null;
			}
		}

		async Task<HttpResponseMessage> Get(string url, Dictionary<string, string> parameters, CancellationToken cancellation)
		{
			await VerifyAndResolveToken(cancellation);

			url = url.AddParametersToUrl(parameters);

			HttpRequestMessage request = new(HttpMethod.Get, url);
			SetToken(request);
			return await HttpClient
				.SendAsync(request, cancellation)
				;
		}

		async Task<HttpResponseMessage> BasicPost<TReq>(string url, TReq data, Dictionary<string, string> parameters, CancellationToken cancellation)
		{
			if (parameters != null)
				url = url.AddParametersToUrl(parameters);

			HttpRequestMessage request = new(HttpMethod.Post, url)
			{
				Content = new StringContent(PosHubApiClientSupport.ToJson(data), Encoding.UTF8, "application/json")
			};
			SetToken(request);
			return await HttpClient
				.SendAsync(request, cancellation)
				;
		}

		async Task<HttpResponseMessage> Post<TReq>(string url, TReq data, Dictionary<string, string> parameters, CancellationToken cancellation)
		{
			await VerifyAndResolveToken(cancellation);
			return await BasicPost(url, data, parameters, cancellation);
		}

		public async Task<ApiValidationResult<UserRecord>> GetUserByCodeAsync(string userCode, CancellationToken cancellation = default)
			=> await Get("api/auth/user", new() { { "userCode", userCode } }, cancellation)
			.AsApiValidationResult<UserRecord>()
			;

		public Task<ApiValidationResult<HotelRecord[]>> GetHotelsAsync(Guid userId, CancellationToken cancellation = default)
			=> Get("api/settings/hotels", new() { { "userId", userId.ToString() } }, cancellation)
			.AsApiValidationResult<HotelRecord[]>()
			;

		public async Task<ApiValidationResult<LoginResultModel>> LoginAsync(LoginModel loginModel, CancellationToken cancellation = default)
		{
			var loginResult = await BasicPost("api/auth/login", loginModel, null, cancellation)
				.AsApiValidationResult<LoginResultModel>();

			token = loginResult.Result?.Token;

			return loginResult;
		}

		public Task<ApiValidationResult<CashierRecord[]>> GetCashiersAsync(CancellationToken cancellation = default)
			=> Get("api/stand/cashiers", null, cancellation)
			.AsApiValidationResult<CashierRecord[]>()
			;

		public Task<ApiValidationResult<POSStandRecord[]>> GetStandsAsync(Guid cashierId, CancellationToken cancellation = default)
			=> Get("api/stand/list", new() { { "cashierId", cashierId.ToString() } }, cancellation)
			.AsApiValidationResult<POSStandRecord[]>()
			;

		public Task<ApiValidationResult<DocumentSerieRecord[]>> GetTicketSerieByStandCashierAsync(Guid standId, Guid cashierId, CancellationToken cancellation = default)
			=> Get("api/ticket/series", new() { { "standId", standId.ToString() }, { "cashierId", cashierId.ToString() } }, cancellation)
			.AsApiValidationResult<DocumentSerieRecord[]>()
			;

		public Task<ApiValidationResult<POSGeneralSettingsRecord>> GetGeneralSettingsAsync(bool fromCloud, CancellationToken cancellation = default)
			=> Get("api/settings", new() { { "fromCloud", fromCloud.ToString() } }, cancellation)
			.AsApiValidationResult<POSGeneralSettingsRecord>()
			;
		
		public Task<ApiValidationResult<RequestTerminalsContract>> GetPaymentTerminalsAsync(Guid originId, CancellationToken cancellation = default)
			=> Get("api/onlinepayment/terminals", new() { { "originId", originId.ToString() } }, cancellation)
			.AsApiValidationResult<RequestTerminalsContract>()
			;

		public Task<ApiValidationResult<ExternalPaymentResponseContract>> RequestPaymentAsync(ExternalPaymentRequestContract request, CancellationToken cancellation = default)
			=> Post("api/onlinepayment/pay", request, null, cancellation)
			.AsApiValidationResult<ExternalPaymentResponseContract>()
			;

		public Task<ApiValidationResult<ExternalPaymentEntryContract>> ConfirmPaymentAsync(Guid entryId, CancellationToken cancellation = default)
			=> Post<object>("api/onlinepayment/confirm", null, new() { { "entryId" , entryId.ToString() } }, cancellation)
			.AsApiValidationResult<ExternalPaymentEntryContract>()
			;

		public Task<ApiValidationResult<ExternalPaymentEntryContract>> UpdateDeviceAsync(ExternalPaymentDeviceCompleteRequest request, CancellationToken cancellation = default)
			=> Post<object>("api/onlinepayment/updatedevice", request, null, cancellation)
			.AsApiValidationResult<ExternalPaymentEntryContract>()
			;

		public Task<ApiValidationResult<TicketExternalPayments>> GetTicketExternalPaymentsAsync(Guid ticketId, CancellationToken cancellation = default)
			=> Get($"api/onlinepayment/ticket/{ticketId}", null, cancellation)
			.AsApiValidationResult<TicketExternalPayments>()
			;
	}

	public static class PosHubApiClientSupport
	{
		public static string ToUrlParameters(this Dictionary<string, string> parameters)
		{
			return parameters?
				.Select(x => $"{UrlEncoder.Default.Encode(x.Key)}={UrlEncoder.Default.Encode(x.Value)}")
				.Aggregate((a, b) => $"{a}&{b}")
				;
		}

		public static string AddParametersToUrl(this string url, Dictionary<string, string> parameters)
		{
			var @params = parameters.ToUrlParameters();
			return url + (string.IsNullOrWhiteSpace(@params) ? string.Empty : "?" + @params);
		}

		static JsonSerializerOptions jsonOptions = new JsonSerializerOptions
		{
			PropertyNameCaseInsensitive = true,
			PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
			DefaultIgnoreCondition = System.Text.Json.Serialization.JsonIgnoreCondition.WhenWritingNull,
			Converters = { new GuidConverter(), new TypeConverter() },
			//Converters = {
			//	new JsonStringEnumConverter()
			//},
		};

		public static string ToJson<T>(T obj)
		{
			return JsonSerializer.Serialize(obj, jsonOptions);
		}

		static T FromJson<T>(string json)
		{
			return JsonSerializer.Deserialize<T>(json, jsonOptions);
		}

		public static async Task<ApiValidationResult<TResp>> AsApiValidationResult<TResp>(this Task<HttpResponseMessage> response)
		{
			string stResult = await (await response).Content.ReadAsStringAsync();
			return FromJson<ApiValidationResult<TResp>>(stResult);
		}

		public static async Task<ApiValidationResult> AsApiValidationResult(this Task<HttpResponseMessage> response)
		{
			string stResult = await (await response).Content.ReadAsStringAsync();
			return FromJson<ApiValidationResult>(stResult);
		}
	}
}
