﻿using NewHotel.Contracts.Pos.Records;
using NewHotel.Pos.Hub.Api.Models;
using NewHotel.Pos.Hub.Contracts.Common.Records.Stands;
using NewHotel.Pos.Hub.Contracts.PaySystem;
using NewHotel.Pos.Hub.Model;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NewHotel.Pos.Communication.Api
{
	public interface IPosHubApiClientFactory
	{
		IPosHubApiClient Create(string baseAddress = null);
	}

	public interface IPosHubApiClient
	{
		Task<ApiValidationResult<ExternalPaymentEntryContract>> ConfirmPaymentAsync(Guid entryId, CancellationToken cancellation = default);
		Task<ApiValidationResult<CashierRecord[]>> GetCashiersAsync(CancellationToken cancellation = default);
		Task<ApiValidationResult<POSGeneralSettingsRecord>> GetGeneralSettingsAsync(bool fromCloud, CancellationToken cancellation = default);
		Task<ApiValidationResult<HotelRecord[]>> GetHotelsAsync(Guid userId, CancellationToken cancellation = default);
		Task<ApiValidationResult<RequestTerminalsContract>> GetPaymentTerminalsAsync(Guid originId, CancellationToken cancellation = default);
		Task<ApiValidationResult<POSStandRecord[]>> GetStandsAsync(Guid cashierId, CancellationToken cancellation = default);
		Task<ApiValidationResult<TicketExternalPayments>> GetTicketExternalPaymentsAsync(Guid ticketId, CancellationToken cancellation = default);
		Task<ApiValidationResult<DocumentSerieRecord[]>> GetTicketSerieByStandCashierAsync(Guid standId, Guid cashierId, CancellationToken cancellation = default);
		Task<ApiValidationResult<UserRecord>> GetUserByCodeAsync(string userCode, CancellationToken cancellation = default);
		Task<ApiValidationResult<LoginResultModel>> LoginAsync(LoginModel loginModel, CancellationToken cancellation = default);
		Task<ApiValidationResult<ExternalPaymentResponseContract>> RequestPaymentAsync(ExternalPaymentRequestContract request, CancellationToken cancellation = default);
		Task<ApiValidationResult<ExternalPaymentEntryContract>> UpdateDeviceAsync(ExternalPaymentDeviceCompleteRequest request, CancellationToken cancellation = default);
	}
}