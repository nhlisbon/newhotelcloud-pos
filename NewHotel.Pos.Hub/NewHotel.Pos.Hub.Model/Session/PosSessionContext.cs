﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Model.Session
{
	public interface IPosSessionContext
	{
		Guid SessionId { get; }
		long ApplicationId { get; }
		string UserName { get; }
		string UserLogin { get; }
		Guid UserId { get; }
		bool IsInternal { get; }
		Guid InstallationId { get; }
		Guid StandId { get; }
		Guid CashierId { get; }
		Guid TaxSchemaId { get; }
		DocumentSign SignatureMode { get; }
		DateTime WorkDate { get; }
		long LanguageId { get; }
		string Country { get; }
	}

	public class PosSessionContext : IPosSessionContext
	{
		public Guid SessionId { get; set; }
		public long ApplicationId { get; set; }
		public string UserName { get; set; }
		public string UserLogin { get; set; }
		public Guid UserId { get; set; }
		public bool IsInternal { get; set; }
		public Guid InstallationId { get; set; }
		public Guid StandId { get; set; }
		public Guid CashierId { get; set; }
		public Guid TaxSchemaId { get; set; }
		public DocumentSign SignatureMode { get; set; }
		public DateTime WorkDate { get; set; }
		public long LanguageId { get; set; }
		public string Country { get; set; }
	}
}
