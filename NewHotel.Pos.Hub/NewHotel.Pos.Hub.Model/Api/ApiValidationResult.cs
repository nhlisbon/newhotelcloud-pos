﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Api.Models
{
	public class ApiValidationItem
	{
		public int Code { get; set; }
		public string Message { get; set; }
		public virtual string TraceMessage { get; set; }
	}

	public class ApiValidationResult
	{
		public ApiValidationItem[] Errors { get; set; }
		public ApiValidationItem[] Warnings { get; set; }
		public int? HttpResult { get; set; }
	}

	public class ApiValidationResult<T> : ApiValidationResult
	{
		public T Result { get; set; }

		public static implicit operator ApiValidationResult<T>(T value)
		{
			return new ApiValidationResult<T>
			{
				Result = value,
				Errors = new ApiValidationItem[0],
				Warnings = new ApiValidationItem[0]
			};
		}

		public static implicit operator ApiValidationResult<T>(ApiValidationItem[] errors)
		{
			return new ApiValidationResult<T>
			{
				Errors = errors.Select(x => x).ToArray()
			};
		}

		public static implicit operator ApiValidationResult<T>(ValidationResult validations)
		{
			return new ApiValidationResult<T>
			{
				Errors = validations.Errors.ToApiItems(),
				Warnings = validations.Warnings.ToApiItems()
			};
		}

		public static implicit operator ApiValidationResult<T>(ValidationResult<T> validations)
		{
			return new ApiValidationResult<T>
			{
				Result = validations.Result,
				Errors = validations.Errors.ToApiItems(),
				Warnings = validations.Warnings.ToApiItems()
			};
		}

		public static implicit operator ApiValidationResult<T>(ValidationItemResult<T> validations)
		{
			return new ApiValidationResult<T>
			{
				Result = validations.Item,
				Errors = validations.Errors.ToApiItems(),
				Warnings = validations.Warnings.ToApiItems()
			};
		}
	}

	public static class ApiValidationResultEx
	{
		public static ApiValidationResult<T> ToApiResult<T>(this NewHotel.Contracts.ValidationContractResult validations)
			where T : class
		{
			return new ApiValidationResult<T>
			{
				Result = validations.Contract as T,
				Errors = validations.Errors.ToApiItems(),
				Warnings = validations.Warnings.ToApiItems()
			};
		}

		public static ApiValidationItem ToApiItem(this NewHotel.Contracts.Validation validation)
		{
			return new ApiValidationItem
			{
				Code = validation.Code,
				Message = validation.Message,
				TraceMessage = validation.TraceMessage
			};
		}

		public static ValidationError ToValidationError(this ApiValidationItem item)
		{
			return new ValidationError(item.Code, item.Message, item.TraceMessage);
		}

		public static ValidationWarning ToValidationWarning(this ApiValidationItem item)
		{
			return new ValidationWarning(item.Code, item.Message, item.TraceMessage);
		}

		public static ApiValidationItem[] ToApiItems<T>(this IEnumerable<T> validations)
			where T : NewHotel.Contracts.Validation
		{
			if (validations == null)
				return null;
			return validations.Select(v => v.ToApiItem()).ToArray();
		}

		public static ApiValidationResult<T> ToApiResult<T>(this IEnumerable<NewHotel.Contracts.ValidationError> errors)
		{
			return new ApiValidationResult<T>
			{
				Errors = errors.ToApiItems()
			};
		}

		public static bool IsSuccess(this ApiValidationResult result)
		{
			return result.Errors == null || result.Errors.Length == 0;
		}

		public static ValidationResult<T> ToValidationResult<T>(this ApiValidationResult<T> result)
		{
			ValidationResult<T> validations = [];
			validations.Set(result.Result);
			validations.AddValidations(result.Errors?.Select(x => x.ToValidationError()).ToArray() ?? []);
			validations.AddValidations(result.Warnings?.Select(x => x.ToValidationWarning()).ToArray() ?? []);

			return validations;
		}
	}
}
