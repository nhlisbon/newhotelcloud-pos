﻿using System;

namespace NewHotel.Pos.Hub.Api.Models
{
	public sealed class LoginModel
	{
		public Guid UserId { get; set; }
		public Guid InstallationId { get; set; }
		public long LanguageId { get; set; }
		public Guid CurrentSessionId { get; set; }
	}

	public sealed class LoginResultModel
	{
		public Guid SessionId { get; set; }
		public string Token { get; set; }
		public string UserName { get; set; }
		public string[] Roles { get; set; }
		public string[] Permissions { get; set; }
	}

	public sealed class UpdateSessionModel
	{ 	
		public Guid StandId { get; set; }
		public Guid CashierId { get; set; }
		public Guid SchemaId { get; set; }
		public long LanguageId { get; set; }
	}
}
