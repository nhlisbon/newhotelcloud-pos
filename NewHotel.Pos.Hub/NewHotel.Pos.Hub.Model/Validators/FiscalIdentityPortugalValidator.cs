﻿using System.Linq;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Model.Validator
{
    public class FiscalIdentityPortugalValidator : SingletonValidator<FiscalIdentityPortugalValidator>
    {
        #region IValidator Implementation

        public override ValidationResult Validate(object obj)
        {
            var result = new ValidationResult();

            var identityNumber = (string)obj;
            if (!ValidatePortugueseNIF(identityNumber))
                result.AddError("Invalid fiscal number format");

            return result;
        }

        #endregion
        #region Methods

        private bool ValidatePortugueseNIF(string nif)
        {
            if (!string.IsNullOrEmpty(nif) && nif.Length == 9 && nif.All(char.IsDigit))
            {
                // Si el NIF empieza con:
                //1 a 3: Pessoa singular, o 3 ainda não está atribuido;[2]
                //45: Pessoa singular.Os algarismos iniciais "45" correspondem aos cidadãos não residentes que apenas obtenham em território português rendimentos sujeitos a retenção na fonte a título definitivo.[2]
                //5: pessoa coletiva obrigada a registo no Registo Nacional de Pessoas Coletivas;[3]
                //6: Organismo da Administração Pública Central, Regional ou Local;
                //70, 74 e 75: Herança Indivisa, em que o autor da sucessão não era empresário individual, ou Herança Indivisa em que o cônjuge sobrevivo tem rendimentos comerciais;
                //71: Não residentes coletivos sujeitos a retenção na fonte a título definitivo.
                //72: Fundos de investimento.
                //77: Atribuição Oficiosa de NIF de sujeito passivo(entidades que não requerem NIF junto do RNPC).
                //78: Atribuição oficiosa a não residentes abrangidos pelo processo VAT REFUND.
                //79: Regime excepcional - Expo 98.
                //8: "empresário em nome individual" (deixou de ser utilizado, já não é válido);
                //90 e 91: Condomínios, Sociedade Irregulares, Heranças Indivisas cujo autor da sucessão era empresário individual.
                //98: Não residentes sem estabelecimento estável.
                //99: Sociedades civis sem personalidade jurídica.

                var validFirstDigits = new[] { '1', '2', '3', '4', '5', '6', '7', '8', '9' };
                if (validFirstDigits.Contains(nif[0]))
                {
                    var checkDigit = 0;
                    for (int i = 1; i <= 8; i++)
                        checkDigit += int.Parse(nif[i - 1].ToString()) * (10 - i);
                    checkDigit = 11 - (checkDigit % 11);

                    // si es mayor o igual que 10 ponerlo a cero
                    if (checkDigit >= 10) checkDigit = 0;

                    // comparar el digito de verificación con el último digito
                    // del NIF, si son iguales entonces es válido
                    if (nif.EndsWith(checkDigit.ToString()))
                        return true;
                }
            }

            return false;
        }

        #endregion 
    }
}
