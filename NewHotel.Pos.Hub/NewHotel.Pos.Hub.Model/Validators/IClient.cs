﻿using System;

namespace NewHotel.Pos.Hub.Model.Validator
{
    public interface IClient
    {
        /// <summary>
        /// CLIE_PK
        /// Primary key
        /// </summary>
        Guid Id { get; set; }
        /// <summary>
        /// column="CLIE_NOME"
        /// Name / Description
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// column="CLIE_CAUX"
        /// Auxiliar Code
        /// </summary>
        string AuxiliarCode { get; set; }
        /// <summary>
        /// column="FISC_NUMB"
        /// Fiscal Number
        /// </summary>
        string FiscalNumber { get; set; }
        /// <summary>
        /// column="FISC_ADDR1"
        /// Address
        /// </summary>
        string Address { get; set; }
        /// <summary>
        /// column="FISC_ADDR2"
        /// Address
        /// </summary>
        string AddressComplement { get; set; }
        /// <summary>
        /// column="NACI_PK"
        /// Country
        /// </summary>
        string Country { get; set; }
        /// <summary>
        /// column="ENTI_FINO"
        /// Fiscal Note Preferred
        /// </summary>
        bool FiscalNotePreferred { get; set; }
        /// <summary>
        /// column="HOME_PHONE"
        /// Phone
        /// </summary>
        string PhoneNumber { get; set; }
        /// <summary>
        /// column="FISC_DOOR"
        /// Door Number
        /// </summary>
        string DoorNumber { get; set; }
        /// <summary>
        /// column="FISC_LOCA"
        /// Door Number
        /// </summary>
        string Location { get; set; }
        /// <summary>
        /// column="FISC_COPO"
        /// Postal Code
        /// </summary>
        string PostalCode { get; set; }
        /// <summary>
        /// column="REGI_FISC"
        /// Fiscal Register (IE)
        /// </summary>
        string FiscalRegister { get; set; }
        /// <summary>
        /// column="CLIE_TYPE" 11 Client, 211 Entity
        /// </summary>
        bool IsClient { get; set; }
        /// <summary>
        /// column="DIST_CAUX"
        /// </summary>
        string DistrictCode { get; set; }
        /// <summary>
        /// column="DIST_DESC"
        /// </summary>
        string District { get; set; }
        /// <summary>
        /// column="COMU_CAUX"
        /// </summary>
        string StateCode { get; set; }
        /// <summary>
        /// column="EMAIL_ADDR"
        /// </summary>
        string EmailAddress { get; set; }
        /// <summary>
        /// column="CLIE_PASS"
        /// </summary>
        string Passport { get; set; }
        /// <summary>
        /// column="CLIE_IDEN"
        /// </summary>
        string Identity { get; set; }
        /// <summary>
        /// column="CLIE_RESD"
        /// </summary>
        string Residence { get; set; }
        /// <summary>
        /// column="CLIE_DRIV"
        /// </summary>
        string DriverLicense { get; set; }
    }
}