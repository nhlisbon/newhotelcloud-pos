﻿using NewHotel.Contracts;
using NewHotel.Pos.Core;
using NewHotel.Pos.Hub.Model.Validator.PosDocument;

namespace NewHotel.Pos.Hub.Model.Validator
{
    public sealed class TicketValidator : BaseDocValidator
    {
        public TicketValidator(decimal? limitValue)
            : base(limitValue)
        {
        }

        public override bool IsFiscalNumberMandatory(POSTicketContract contract)
        {
            switch (Country)
            {
                case CountryCodes.Brazil:
                    var validator = new BrTicketValidator(LimitValue)
                    {
                        Country = Country,
                        DocumentSign = DocumentSign,
                        Client = Client
                    };

                    return validator.IsFiscalNumberMandatory(contract);
            }

            return true;
        }

        protected override IValidator<POSTicketContract> GetValidator(POSTicketContract contract)
        {
            switch (Country)
            {
                case CountryCodes.Brazil:
                    return new BrTicketValidator(LimitValue)
                    {
                        Country = Country,
                        DocumentSign = DocumentSign,
                        Client = Client
                    };
                case CountryCodes.Ecuador:
                    return new EcTicketValidator();
            }

            return null;
        }
    }
}