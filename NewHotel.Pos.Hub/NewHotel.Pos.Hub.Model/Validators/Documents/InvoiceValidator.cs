﻿using NewHotel.Contracts;
using NewHotel.Pos.Core;
using NewHotel.Pos.Hub.Model.Validator.PosDocument;

namespace NewHotel.Pos.Hub.Model.Validator
{
    public sealed class InvoiceValidator : BaseDocValidator
    {
        public InvoiceValidator(decimal? limitValue)
            : base(limitValue)
        {
        }

        protected override IValidator<POSTicketContract> GetValidator(POSTicketContract contract)
        {
            switch (Country)
            {
                case CountryCodes.Portugal:
                    return new PtInvoiceValidator()
                    {
                        Country = Country,
                        DocumentSign = DocumentSign,
                        Client = Client
                    };
                case CountryCodes.Brazil:
                    return new BrInvoiceValidator(LimitValue)
                    {
                        Country = Country,
                        DocumentSign = DocumentSign,
                        Client = Client
                    };
                case CountryCodes.Ecuador:
                    return new EcInvoiceValidator()
                    {
                        Country = Country,
                        DocumentSign = DocumentSign,
                        Client = Client
                    };
                case CountryCodes.Peru:
                    return new PeInvoiceValidator()
                    {
                        Country = Country,
                        DocumentSign = DocumentSign,
                        Client = Client
                    };
                default:
                    return new FiscalDocValidator()
                    {
                        Country = Country,
                        DocumentSign = DocumentSign,
                        Client = Client,
                        CheckEmail = false,
                        CheckAddress = false,
                        CheckFiscalNumber = false,
                        CheckName = false
                    };
            }
        }
    }
}