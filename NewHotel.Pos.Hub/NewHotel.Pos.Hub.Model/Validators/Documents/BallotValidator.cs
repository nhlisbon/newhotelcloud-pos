﻿using NewHotel.Contracts;
using NewHotel.Pos.Core;
using NewHotel.Pos.Hub.Model.Validator.PosDocument;

namespace NewHotel.Pos.Hub.Model.Validator
{
    public sealed class BallotValidator : BaseDocValidator
    {
        public BallotValidator(decimal? limitValue)
            : base(limitValue)
        {
        }

        protected override IValidator<POSTicketContract> GetValidator(POSTicketContract contract)
        {
            switch (Country)
            {
                case CountryCodes.Peru:
                    return new PeBallotValidator
                    {
                        Country = Country,
                        DocumentSign = DocumentSign
                    };
            }

            return null;
        }
    }
}