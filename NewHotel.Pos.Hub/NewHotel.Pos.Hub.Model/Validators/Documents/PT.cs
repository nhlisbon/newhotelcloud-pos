﻿using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Model.Validator.PosDocument
{
    #region PT

    public class PtInvoiceValidator : FiscalDocValidator
    {
        public PtInvoiceValidator()
        {
            CheckName = false;
            CheckEmail = false;
            CheckAddress = false;
        }

        protected override ValidationResult ValidateFiscalNumber(string number, string nationality)
        {
            var result = base.ValidateFiscalNumber(number, nationality);

            if (result.IsEmpty)
            {
                if (nationality == "PT")
                    result.Add(FiscalIdentityPortugalValidator.Instance.Validate(number));
            }

            return result;
        }
    }

    #endregion
}