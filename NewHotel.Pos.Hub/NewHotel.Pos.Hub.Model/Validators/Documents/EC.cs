﻿using System.Linq;
using System.Collections.Generic;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Model.Validator.PosDocument
{
    #region EC

    public class EcInvoiceValidator : FiscalDocValidator
    {
        public EcInvoiceValidator()
        {
            CheckEmail = false;
            CheckAddress = false;
        }

        public override ValidationResult Validate(POSTicketContract contract)
        {
            const decimal MaxFinalCustomerTotal = 50;

            var validation = base.Validate(contract);
            if (validation.IsEmpty)
            {
                // Pagos
                if (contract.PaymentLineByTicket.Any(x => x.ReceivableType != ReceivableType.Payment))
                    validation.AddError("Invoice only allowed in cash payments.");

                // Nacionalidad
                string nationality = contract.FiscalDocNacionality;
                if (string.IsNullOrEmpty(nationality))
                    validation.AddError("The Nationality field cannot be empty.");

                if (IsFinalConsumer(contract))
                {
                    if (contract.Total > MaxFinalCustomerTotal)
                        validation.AddError("Final consumer invoice can't exceed {MaxFinalCustomerTotal}.");
                }
            }

            return validation;
        }

        protected override ValidationResult ValidateIdentityDocument(string number, string nationality)
        {
            var result = base.ValidateIdentityDocument(number, nationality);
            if (result.IsEmpty)
            {
                if (!IdentityValidatorEcuador.Instance.IsNumberIdentity(number))
                    result.AddError("Invalid DNI.");
            }

            return result;
        }

        protected override ValidationResult ValidateFiscalNumber(string number, string nationality)
        {
            var result = base.ValidateFiscalNumber(number, nationality);
            if (result.IsEmpty)
            {
                if (!IdentityValidatorEcuador.Instance.IsRuc(number))
                    result.AddError("Invalid RUC.");
            }

            return result;
        }
    }

    public sealed class EcTicketValidator : IValidator<POSTicketContract>
    {
        public ValidationResult Validate(POSTicketContract contract)
        {
            var validation = new ValidationResult();

            // Si es ticket transferido entonces no contine pagos y es válido
            if (IsTransferedTicket(contract))
                return validation;

            // El unico pago no permitido para imprimir ticket es en dinero
            if (contract.PaymentLineByTicket.All(x => x.ReceivableType == ReceivableType.Payment))
                validation.AddError("Ticket no permitido");

            return validation;
        }

        private static bool IsTransferedTicket(POSTicketContract contract)
        {
            return contract.Process &&
                   !contract.Opened &&
                   !contract.Mesa.HasValue &&
                   contract.ClosedTime.HasValue &&
                   contract.ClosedDate.HasValue &&
                   contract.PaymentLineByTicket.Count == 1 &&
                   contract.PaymentLineByTicket.Any(x => x.ReceivableType == ReceivableType.TicketTransfer);
        }
    }

    #endregion
}