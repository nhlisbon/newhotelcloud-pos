﻿using NewHotel.Contracts;
using NewHotel.Pos.Localization;

namespace NewHotel.Pos.Hub.Model.Validator.PosDocument
{
    public abstract class BaseDocValidator : IValidator<POSTicketContract>
    {
        protected readonly decimal? LimitValue;

        public string Country { get; set; }
        public DocumentSign DocumentSign { get; set; }
        public IClient Client { get; set; }

        public bool IsFinalConsumer(POSTicketContract contract)
        {
            return !contract.IsOnlyTicket && FiscalDocContract.GetFinalCustomerNumber(DocumentSign) == contract.FiscalDocFiscalNumber;
        }

        public virtual bool IsFiscalNumberMandatory(POSTicketContract contract)
        {
            return true;
        }

        protected virtual IValidator<POSTicketContract> GetValidator(POSTicketContract contract)
        {
            return null;
        }

        public virtual ValidationResult Validate(POSTicketContract contract)
        {
            var result = new ValidationResult();

            var validator = GetValidator(contract);
            if (validator != null)
                result.Add(validator.Validate(contract));

            return result;
        }

        public BaseDocValidator(decimal? limitValue = null)
        {
            LimitValue = limitValue;
        }
    }

    public class FiscalDocValidator : BaseDocValidator
    {
        public FiscalDocValidator(decimal? limitValue = null)
            : base(limitValue)
        {
        }

        public bool CheckName { get; set; } = true;
        public bool CheckFiscalNumber { get; set; } = true;
        public bool CheckAddress { get; set; } = true;
        public bool CheckEmail { get; set; } = true;
        public bool CheckDocIdenType { get; set; } = true;

        public override ValidationResult Validate(POSTicketContract contract)
        {
            var result = new ValidationResult();

            if (DocumentSign != DocumentSign.None)
            {
                if (CheckDocIdenType)
                {
                    var docIdentity = (PersonalDocType?)contract.FiscalIdenType;
                    if (!docIdentity.HasValue)
                        result.AddError("Identification doc can't be empty");
                    else if (Client != null && !Client.IsClient && docIdentity.Value != PersonalDocType.FiscalNumber)
                        result.AddError("Invalid identification doc type");
                    else if (CheckFiscalNumber)
                    {
                        if (!string.IsNullOrEmpty(contract.FiscalDocFiscalNumber))
                        {
                            result.Add(docIdentity switch
                            {
                                PersonalDocType.IdentityDocument => ValidateIdentityDocument(contract.FiscalDocFiscalNumber, contract.FiscalDocNacionality),
                                PersonalDocType.FiscalNumber => ValidateFiscalNumber(contract.FiscalDocFiscalNumber, contract.FiscalDocNacionality),
                                PersonalDocType.Passport => ValidatePassport(contract.FiscalDocFiscalNumber, contract.FiscalDocNacionality),
                                PersonalDocType.DriverLicence => ValidateDriverLicence(contract.FiscalDocFiscalNumber, contract.FiscalDocNacionality),
                                PersonalDocType.ResidenceCertificate => ValidateResidenceCertificate(contract.FiscalDocFiscalNumber, contract.FiscalDocNacionality),
                            });
                        }
                        else if (IsFiscalNumberMandatory(contract))
                            result.AddError("Mandatory fiscal number");
                    }
                }

                if (CheckName && string.IsNullOrEmpty(contract.FiscalDocTo))
                    result.AddError("Name can't be empty");

                if (CheckAddress && string.IsNullOrEmpty(contract.FiscalDocAddress))
                    result.AddError("Fiscal address can't be empty");

                var emptyEmail = string.IsNullOrEmpty(contract.FiscalDocEmail);
                if (CheckEmail)
                {
                    if (emptyEmail)
                        result.AddError("Fiscal e-mail can't be empty");
                }

                if (!emptyEmail)
                    result.Add(EmailValidator.Instance.Validate(contract.FiscalDocEmail));

                var finalCustomerName = FiscalDocContract.GetFinalCustomerName(DocumentSign);
                if (IsFinalConsumer(contract))
                {
                    if (contract.FiscalDocTo != finalCustomerName)
                        result.AddError("InvalidData".Translate());
                }
                else if (contract.FiscalDocTo == finalCustomerName)
                    result.AddError("InvalidName".Translate());
            }

            return result;
        }

        protected virtual ValidationResult ValidateFiscalNumber(string number, string nationality)
        {
            var result = new ValidationResult();
            if (CheckFiscalNumber && string.IsNullOrEmpty(number))
                result.AddError("Fiscal number can't be empty");

            return result;
        }

        protected virtual ValidationResult ValidateIdentityDocument(string number, string nationality)
        {
            return ValidationResult.Empty;
        }

        protected virtual ValidationResult ValidateResidenceCertificate(string number, string nationality)
        {
            return ValidationResult.Empty;
        }

        protected virtual ValidationResult ValidateDriverLicence(string number, string nationality)
        {
            return ValidationResult.Empty;
        }

        protected virtual ValidationResult ValidatePassport(string number, string nationality)
        {
            return InternationalPassportValidator.Instance.Validate(number);
        }
    }
}