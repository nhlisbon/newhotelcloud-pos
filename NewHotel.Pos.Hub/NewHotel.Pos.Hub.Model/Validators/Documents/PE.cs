﻿using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Model.Validator.PosDocument
{
    #region PE

    public class PeBallotValidator : FiscalDocValidator
    {
        public PeBallotValidator()
        {
            CheckEmail = false;
        }

        public override ValidationResult Validate(POSTicketContract contract)
        {
            var result = base.Validate(contract);
            if (result.IsEmpty)
            {
                switch (DocumentSign)
                {
                    case DocumentSign.FiscalizationPeruDFacture:
                        if (contract.FiscalDocTotal > 200)
                            result.AddError("Boleta no permitida. El total de la boleta es mayor que 200");
                        break;
                }
            }

            return result;
        }
    }

    public class PeInvoiceValidator : FiscalDocValidator
    {
        public PeInvoiceValidator()
        {
            CheckEmail = false;
        }
    }

    #endregion
}