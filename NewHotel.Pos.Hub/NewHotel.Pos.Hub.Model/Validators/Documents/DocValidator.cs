﻿using NewHotel.Contracts;
using NewHotel.Pos.Hub.Model.Validator.PosDocument;

namespace NewHotel.Pos.Hub.Model.Validator
{
    public class DocValidator : BaseDocValidator
    {
        public DocValidator(decimal? limitValue)
            : base(limitValue)
        {
        }

        public override bool IsFiscalNumberMandatory(POSTicketContract contract)
        {
            BaseDocValidator validator = null;

            if (contract.IsOnlyTicket)
                validator = new TicketValidator(LimitValue)
                {
                    Client = Client,
                    Country = Country,
                    DocumentSign = DocumentSign
                };
            else if (contract.IsBallot)
                validator = new BallotValidator(LimitValue)
                {
                    Client = Client,
                    Country = Country,
                    DocumentSign = DocumentSign
                };
            else if (contract.IsInvoice)
                validator = new InvoiceValidator(LimitValue)
                {
                    Client = Client,
                    Country = Country,
                    DocumentSign = DocumentSign
                };

            if (validator != null)
                return validator.IsFiscalNumberMandatory(contract);

            return base.IsFiscalNumberMandatory(contract);
        }

        public override ValidationResult Validate(POSTicketContract contract)
        {
            var result = new ValidationResult();

            if (!contract.IsAnul)
            {
                if (contract.ClosedDate.HasValue)
                {
                    // Tiene pago y cubre el valor total?
                    var diff = contract.Total - contract.PaymentTotal;
                    if (diff > 0)
                        result.AddError($"Ticket not payed completely. Ticket total: {contract.Total}, total payment: {contract.PaymentTotal}"); 
                }

                if (result.IsEmpty)
                {
                    BaseDocValidator validator = null;

                    if (contract.IsOnlyTicket)
                        validator = new TicketValidator(LimitValue);
                    else if (contract.IsBallot)
                        validator = new BallotValidator(LimitValue);
                    else if (contract.IsInvoice)
                        validator = new InvoiceValidator(LimitValue);

                    if (validator != null)
                    {
                        validator.Country = Country;
                        validator.DocumentSign = DocumentSign;
                        validator.Client = Client;

                        result.Add(validator.Validate(contract));
                    }
                }
            }

            return result;
        }
    }
}