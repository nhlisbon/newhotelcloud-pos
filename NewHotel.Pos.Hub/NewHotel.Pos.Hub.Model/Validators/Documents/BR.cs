﻿using System.Linq;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Model.Validator.PosDocument
{
    #region BR

    public class BrInvoiceValidator : FiscalDocValidator
    {
        public BrInvoiceValidator(decimal? limitValue)
            : base(limitValue)
        {
            CheckAddress = false;
        }

        public override ValidationResult Validate(POSTicketContract contract)
        {
            switch (DocumentSign)
            {
                case DocumentSign.None:
                    return base.Validate(contract);
                case DocumentSign.FiscalizationBrazilNFE:
                case DocumentSign.FiscalizationBrazilCMFLEX:
                    return ValidateClientForNfCe(Client, contract);
            }

            return ValidationResult.Empty;
        }

        protected override ValidationResult ValidateIdentityDocument(string number, string nationality)
        {
            var result = base.ValidateIdentityDocument(number, nationality);
            result.Add(FiscalIdentityBrazilValidator.Instance.Validate(number));
            return result;
        }

        private static bool EmptyExtraFields(IClient client)
        {
            return string.IsNullOrEmpty(client.Country) &&
                   string.IsNullOrEmpty(client.PhoneNumber) &&
                   string.IsNullOrEmpty(client.Name) &&
                   string.IsNullOrEmpty(client.Address) &&
                   string.IsNullOrEmpty(client.DoorNumber) &&
                   string.IsNullOrEmpty(client.AddressComplement) &&
                   string.IsNullOrEmpty(client.Location) &&
                   string.IsNullOrEmpty(client.PostalCode) &&
                   string.IsNullOrEmpty(client.DistrictCode) &&
                   string.IsNullOrEmpty(client.District) &&
                   string.IsNullOrEmpty(client.StateCode) &&
                   string.IsNullOrEmpty(client.EmailAddress) &&
                   string.IsNullOrEmpty(client.FiscalRegister);
        }

        private ValidationResult ValidateClientForNfCe(IClient client, POSTicketContract contract)
        {
            var result = new ValidationResult();

            if (IsFiscalNumberMandatory(contract))
            {
                if (client != null)
                {
                    #region CPF/CNPJ

                    if (string.IsNullOrEmpty(client.FiscalNumber))
                        result.AddError("CPF/CNPJ obrigatorio");
                    else
                    {
                        var validationResult = FiscalIdentityBrazilValidator.Instance.Validate(client.FiscalNumber);
                        if (!validationResult.IsEmpty)
                            result.Add(validationResult);
                    }

                    #endregion

                    #region Name

                    if (result.IsEmpty)
                    {
                        if (string.IsNullOrEmpty(client.Name))
                            result.AddError("Cliente/Entidade não pode ter nome em branco");
                        else if (client.Name.Length > 60)
                            result.AddError("Nome do cliente maior que 60 caracteres");
                    }

                    #endregion

                    const decimal TicketTotalLimit = 10000;

                    if (result.IsEmpty)
                    {
                        #region Ticket over limit

                        if (contract.Total >= TicketTotalLimit)
                        {
                            // Can´t be client, must be entity
                            if (client.IsClient)
                                result.AddError($"Pessoa física não pode ter notas acima de {TicketTotalLimit} reais");
                            else
                            {
                                // Need to have everything filled (according to view)
                                if (string.IsNullOrEmpty(client.Country))
                                    result.AddError($"Entidade não pode ter Pais em branco em nota por cima de {TicketTotalLimit} reais");
                                if (string.IsNullOrEmpty(client.PhoneNumber))
                                    result.AddError($"Entidade não pode ter telefone em branco em nota por cima de {TicketTotalLimit} reais");
                                if (string.IsNullOrEmpty(client.Name))
                                    result.AddError($"Entidade não pode ter Nome em branco em nota por cima de {TicketTotalLimit} reais");
                                if (string.IsNullOrEmpty(client.Address))
                                    result.AddError($"Entidade não pode ter Logradouro em branco em nota por cima de {TicketTotalLimit} reais");
                                if (string.IsNullOrEmpty(client.DoorNumber))
                                    result.AddError($"Entidade não pode ter Numero de Porta em branco em nota por cima de {TicketTotalLimit} reais");
                                if (string.IsNullOrEmpty(client.AddressComplement))
                                    result.AddError($"Entidade não pode ter Complemento em branco em nota por cima de {TicketTotalLimit} reais");
                                if (string.IsNullOrEmpty(client.Location))
                                    result.AddError($"Entidade não pode ter Barrio em branco em nota por cima de {TicketTotalLimit} reais");
                                if (string.IsNullOrEmpty(client.PostalCode))
                                    result.AddError($"Entidade não pode ter CEP em branco em nota por cima de {TicketTotalLimit} reais");
                                if (string.IsNullOrEmpty(client.DistrictCode))
                                    result.AddError($"Entidade não pode ter IBGE em branco em nota por cima de {TicketTotalLimit} reais");
                                if (string.IsNullOrEmpty(client.District))
                                    result.AddError($"Entidade não pode ter a Cidade em branco em nota por cima de {TicketTotalLimit} reais");
                                if (string.IsNullOrEmpty(client.StateCode))
                                    result.AddError($"Entidade não pode ter UF em branco em nota por cima de {TicketTotalLimit} reais");
                                if (string.IsNullOrEmpty(client.FiscalRegister))
                                    result.AddError($"Entidade não pode ter IE em branco em nota por cima de {TicketTotalLimit} reais");
                                if (string.IsNullOrEmpty(client.EmailAddress))
                                    result.AddError($"Entidade não pode ter Email em branco em nota por cima de {TicketTotalLimit} reais");
                            }
                        }

                        #endregion

                        #region Ticket under limit

                        // At least one extra field has value, then all must have value
                        else if (!EmptyExtraFields(client))
                        {
                            if (string.IsNullOrEmpty(client.Country))
                                result.AddError("Cliente/Entidade não pode ter país em branco");
                            if (string.IsNullOrEmpty(client.PhoneNumber))
                                result.AddError("Cliente/Entidade não pode ter telefone em branco");
                            if (string.IsNullOrEmpty(client.Name))
                                result.AddError("Cliente/Entidade não pode ter nome em branco");
                            if (string.IsNullOrEmpty(client.Address))
                                result.AddError("Cliente/Entidade não pode ter logradouro em branco");
                            if (string.IsNullOrEmpty(client.DoorNumber))
                                result.AddError("Cliente/Entidade não pode ter numero de porta em branco");
                            if (string.IsNullOrEmpty(client.AddressComplement))
                                result.AddError("Cliente/Entidade não pode ter complemento em branco");
                            if (string.IsNullOrEmpty(client.Location))
                                result.AddError("Cliente/Entidade não pode ter bairro em branco");
                            if (string.IsNullOrEmpty(client.PostalCode))
                                result.AddError("Cliente/Entidade não pode ter CEP em branco");
                            if (string.IsNullOrEmpty(client.DistrictCode))
                                result.AddError("Cliente/Entidade não pode ter IBGE em branco");
                            if (string.IsNullOrEmpty(client.District))
                                result.AddError("Cliente/Entidade não pode ter a cidade em branco");
                            if (string.IsNullOrEmpty(client.StateCode))
                                result.AddError("Cliente/Entidade não pode ter UF em branco");
                            if (!client.IsClient && string.IsNullOrEmpty(client.FiscalRegister))
                                result.AddError("Entidade não pode ter IE em branco");
                            if (string.IsNullOrEmpty(client.EmailAddress))
                                result.AddError("Cliente/Entidade não pode ter e-mail em branco");
                        }

                        #endregion

                        // Phone Number
                        if (client.PhoneNumber != null && client.PhoneNumber.Length > 12)
                            result.AddError("Telefone com mais de 12 caracteres");

                        // Location
                        if (client.Location != null && client.Location.Length < 2)
                            result.AddError("Bairro não pode ter menos de 2 caracteres");
                    }
                }
                else
                    result.AddError("Cliente/Entidade obrigatorio");
            }

            return result;
        }
    }

    public class BrTicketValidator : FiscalDocValidator
    {
        public BrTicketValidator(decimal? limitValue)
            : base(limitValue)
        {
            CheckAddress = false;
        }

        public override ValidationResult Validate(POSTicketContract contract)
        {
            switch (DocumentSign)
            {
                case DocumentSign.None:
                    return ValidateFiscalNumber(contract);
                case DocumentSign.FiscalizationBrazilNFE:
                case DocumentSign.FiscalizationBrazilCMFLEX:
                    return ValidateClientForNfCe(Client, contract);
            }

            return ValidationResult.Empty;
        }

        public override bool IsFiscalNumberMandatory(POSTicketContract contract)
        {
            if (LimitValue.HasValue)
                return contract.ProductLineByTicket.Where(pl => pl.IsActive).Sum(pl => pl.GrossValue) >= LimitValue.Value;

            return base.IsFiscalNumberMandatory(contract);
        }

        protected override ValidationResult ValidateIdentityDocument(string number, string nationality)
        {
            var result = base.ValidateIdentityDocument(number, nationality);
            result.Add(FiscalIdentityBrazilValidator.Instance.Validate(number));
            return result;
        }

        private ValidationResult ValidateClientForNfCe(IClient client, POSTicketContract contract)
        {
            var result = new ValidationResult();

            if (IsFiscalNumberMandatory(contract))
            {
                if (client != null)
                {
                    #region CPF/CNPJ

                    if (string.IsNullOrEmpty(client.FiscalNumber))
                        result.AddError("CPF/CNPJ obrigatorio");
                    else
                    {
                        var validationResult = FiscalIdentityBrazilValidator.Instance.Validate(client.FiscalNumber);
                        if (!validationResult.IsEmpty)
                            result.Add(validationResult);
                    }

                    #endregion

                    #region Name

                    if (result.IsEmpty)
                    {
                        if (string.IsNullOrEmpty(client.Name))
                            result.AddError("Cliente/Entidade não pode ter nome em branco");
                        else if (client.Name.Length > 60)
                            result.AddError("Nome do cliente maior que 60 caracteres");
                    }

                    #endregion

                    const decimal TicketTotalLimit = 10000;

                    if (result.IsEmpty)
                    {
                        #region Ticket over limit

                        if (contract.Total >= TicketTotalLimit)
                        {
                            // Can´t be client, must be entity
                            if (client.IsClient)
                                result.AddError($"Pessoa física não pode ter notas acima de {TicketTotalLimit} reais");
                        }

                        #endregion

                        // Phone Number
                        if (client.PhoneNumber != null && client.PhoneNumber.Length > 12)
                            result.AddError("Telefone com mais de 12 caracteres");

                        // Location
                        if (client.Location != null && client.Location.Length < 2)
                            result.AddError("Bairro não pode ter menos de 2 caracteres");
                    }
                }
            }

            return result;
        }

        private ValidationResult ValidateFiscalNumber(POSTicketContract contract)
        {
            var result = new ValidationResult();

            if (contract.FiscalDocNacionality == "BR")
            {
                if (IsFiscalNumberMandatory(contract))
                {
                    #region CPF/CNPJ

                    if (string.IsNullOrEmpty(contract.FiscalDocFiscalNumber))
                        result.AddError("CPF/CNPJ obrigatorio");
                    else
                    {
                        var validationResult = FiscalIdentityBrazilValidator.Instance.Validate(contract.FiscalDocFiscalNumber);
                        if (!validationResult.IsEmpty)
                            result.Add(validationResult);
                    }

                    #endregion
                }
            }

            return result;
        }
    }

    #endregion
}