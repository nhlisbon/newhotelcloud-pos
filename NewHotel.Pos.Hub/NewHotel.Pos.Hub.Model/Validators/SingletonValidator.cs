﻿using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Model.Validator
{
    public abstract class SingletonValidator<I> : IValidator<object> where I : class, new()
    {
        public abstract ValidationResult Validate(object obj);

        #region Singleton

        private static I _instance;

        public static I Instance
        {
            get { return _instance ?? (_instance = new I()); }
        }

        #endregion
    }
}