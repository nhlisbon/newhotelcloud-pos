﻿using System.Text.RegularExpressions;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Model.Validator
{
    public class FiscalIdentityBrazilValidator : SingletonValidator<FiscalIdentityBrazilValidator>
    {
        #region IValidator Implementation

        public override ValidationResult Validate(object obj)
        {
            var result = new ValidationResult();
            var fiscalNumber = (string)obj;
            if (!ValidCPF(fiscalNumber) && !ValidCNPJ(fiscalNumber))
                result.AddError("Invalid fiscal number format");

            return result;
        }

        #endregion
        #region Methods

        private bool ValidCPF(string cpf)
        {
            if (cpf == null || cpf.Length != 11)
                return false;

            var regexObj = new Regex(@"[^\d]");
            cpf = regexObj.Replace(cpf, string.Empty);

            switch (cpf)
            {
                case "":
                case "00000000000":
                case "11111111111":
                case "22222222222":
                case "33333333333":
                case "44444444444":
                case "55555555555":
                case "66666666666":
                case "77777777777":
                case "88888888888":
                case "99999999999":
                    return false;
            }

            // Valida 1ro digito
            int add = 0;
            for (int i = 0; i < 9; i++)
                add += int.Parse(cpf[i].ToString()) * (10 - i);
            int rev = 11 - (add % 11);
            if (rev == 10 || rev == 11)
                rev = 0;
            if (rev != int.Parse(cpf[9].ToString())) return false;

            // Valida 2do digito
            add = 0;
            for (int i = 0; i < 10; i++)
                add += int.Parse(cpf[i].ToString()) * (11 - i);
            rev = 11 - (add % 11);
            if (rev == 10 || rev == 11)
                rev = 0;
            if (rev != int.Parse(cpf[10].ToString())) return false;

            return true;
        }

        private bool ValidCNPJ(string cnpj)
        {
            if (cnpj == null || cnpj.Length != 14)
                return false;

            var regexObj = new Regex(@"[^\d]");
            cnpj = regexObj.Replace(cnpj, string.Empty);

            switch (cnpj)
            {
                case "":
                case "00000000000000":
                case "11111111111111":
                case "22222222222222":
                case "33333333333333":
                case "44444444444444":
                case "55555555555555":
                case "66666666666666":
                case "77777777777777":
                case "88888888888888":
                case "99999999999999":
                    return false;
            }

            // Valida DVs
            var tamanho = cnpj.Length - 2;
            var numeros = cnpj.Substring(0, tamanho);
            var digitos = cnpj.Substring(tamanho);
            int soma = 0;
            int pos = tamanho - 7;

            for (int i = tamanho; i >= 1; i--)
            {
                soma += int.Parse(numeros[tamanho - i].ToString()) * pos--;
                if (pos < 2)
                    pos = 9;
            }

            int resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != int.Parse(digitos[0].ToString()))
                return false;

            tamanho = tamanho + 1;
            numeros = cnpj.Substring(0, tamanho);
            soma = 0;
            pos = tamanho - 7;

            for (int i = tamanho; i >= 1; i--)
            {
                soma += int.Parse(numeros[tamanho - i].ToString()) * pos--;
                if (pos < 2)
                    pos = 9;
            }

            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != int.Parse(digitos[1].ToString()))
                return false;

            return true;
        }

        #endregion 
    }
}
