﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Model.Validator
{
    public class InternationalPassportValidator : SingletonValidator<InternationalPassportValidator>
    {
        #region IValidator Implementation

        public override ValidationResult Validate(object passport)
        {
            var result = new ValidationResult();
            var sPassport = (string)passport;
            var pFormat = IsTd3(sPassport) || IsTd1(sPassport) || IsTd2(sPassport) || IsDocumentNumber(sPassport);
            if (!pFormat)
                result.AddError("Invalid passport format");

            return result;
        }

        #endregion
        #region Properties

        private static Dictionary<char, int> _values;
        private static Dictionary<char, int> Values
        {
            get
            {
                if (_values == null)
                {
                    _values = new Dictionary<char, int>();
                    _values.Add('<', 0);

                    for (int i = 0; i < 10; i++)
                        _values.Add(i.ToString().First(), i);

                    string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                    int start = 10;
                    foreach (var letter in alphabet)
                        _values.Add(letter, start++);
                }
                return _values;
            }
        }


        #endregion
        #region Methods

        public bool IsTd2(string passport)
        {
            int pStartRead;
            string pattern;
            if (passport.Length == 72)
            {
                pStartRead = 36;
                pattern =
                    "^[IAC]{1}[A-Z<]{1}[A-Z<]{3}[A-Z<]{31}[A-Z0-9<]{9}[0-9<]{1}[A-Z<]{3}[0-9<]{6}[0-9]{1}[FM<]{1}[0-9<]{6}[0-9]{1}[A-Z<0-9]{7}[0-9]{1}$";
            }
            else if (passport.Length == 36)
            {
                pStartRead = 0;
                pattern =
                    "^[A-Z0-9<]{9}[0-9<]{1}[A-Z<]{3}[0-9<]{6}[0-9]{1}[FM<]{1}[0-9<]{6}[0-9]{1}[A-Z<0-9]{7}[0-9]{1}$";
            }
            else return false;

            if (!Regex.IsMatch(passport, pattern))
                return false;

            // Check digit over digits 1–9 
            int start = pStartRead;
            int length = 9;
            int posCheckDigit = start + length;
            if (!CheckDigit(passport, start, length, posCheckDigit))
                return false;

            // Check digit over digits 14–19 
            start = pStartRead + 13;
            length = 6;
            posCheckDigit = start + length;
            if (!CheckDigit(passport, start, length, posCheckDigit))
                return false;

            // Check digit over digits 22–27
            start = pStartRead + 21;
            length = 6;
            posCheckDigit = start + length;
            if (!CheckDigit(passport, start, length, posCheckDigit))
                return false;

            // Check digit over digits 6–30 (upper line), 1–7, 9–15, 19–29 (middle line)
            var intervals = new[]
            {
                new Tuple<int, int>(pStartRead, 10),
                new Tuple<int, int>(pStartRead + 13, 7),
                new Tuple<int, int>(pStartRead + 21, 14)
            };
            if (!CheckDigit(passport, intervals, passport.Length - 1))
                return false;

            return true;
        }

        public bool IsTd1(string passport)
        {
            string pattern;
            if (passport.Length == 60)
            {
                pattern =
                    "^[IAC]{1}[A-Z<]{1}[A-Z<]{3}[A-Z0-9<]{9}[0-9<]{1}[A-Z0-9<]{15}[0-9]{6}[0-9]{1}[A-Z<]{1}[0-9]{6}[0-9]{1}[A-Z<]{3}[A-Z0-9<]{11}[0-9]{1}$";
            }
            else if (passport.Length == 90)
            {
                pattern =
                    "^[IAC]{1}[A-Z<]{1}[A-Z<]{3}[A-Z0-9<]{9}[0-9<]{1}[A-Z0-9<]{15}[0-9]{6}[0-9]{1}[A-Z<]{1}[0-9]{6}[0-9]{1}[A-Z<]{3}[A-Z0-9<]{11}[0-9]{1}[A-Z<]{30}$";
            }
            else return false;

            if (!Regex.IsMatch(passport, pattern))
                return false;

            int start = 5;
            int length = 9;
            int posCheckDigit = start + length;
            // Check digit over digits 6–14
            if (!CheckDigit(passport, start, length, posCheckDigit))
                return false;

            // Second row
            int pSecondRow = 30;
            // Check digit over digits 1-6
            start = pSecondRow;
            length = 6;
            posCheckDigit = start + length;
            if (!CheckDigit(passport, start, length, posCheckDigit))
                return false;

            // Check digit over digits 9-14
            start = pSecondRow + 8;
            length = 6;
            posCheckDigit = start + length;
            if (!CheckDigit(passport, start, length, posCheckDigit))
                return false;

            // Check digit over digits 6–30 (upper line), 1–7, 9–15, 19–29 (middle line)
            var intervals = new[]
            {
                new Tuple<int, int>(5, 25),
                new Tuple<int, int>(pSecondRow, 7),
                new Tuple<int, int>(pSecondRow + 8, 7),
                new Tuple<int, int>(pSecondRow + 18, 11)
            };
            if (!CheckDigit(passport, intervals, 59))
                return false;

            return true;
        }

        public bool IsTd3(string passport)
        {
            int pStartRead;
            string pattern;
            if (passport.Length == 88)
            {
                pStartRead = 44;
                pattern =
                    "^P[A-Z<]{1}[A-Z<]{3}[A-Z<]{39}[A-Z0-9<]{9}[0-9]{1}[A-Z<]{3}[0-9]{6}[0-9]{1}[A-Z<]{1}[0-9]{6}[0-9]{1}[A-Z0-9<]{14}[0-9<]{1}[0-9]{1}$";
            }
            else if (passport.Length == 44)
            {
                pStartRead = 0;
                pattern =
                    "^[A-Z0-9<]{9}[0-9]{1}[A-Z<]{3}[0-9]{6}[0-9]{1}[A-Z<]{1}[0-9]{6}[0-9]{1}[A-Z0-9<]{14}[0-9<]{1}[0-9]{1}$";
            }
            else return false;

            if (!Regex.IsMatch(passport, pattern))
                return false;

            // Check digit over digits 1–9
            int start = pStartRead;
            int length = 9;
            int posCheckDigit = start + length;
            if (!CheckDigit(passport, start, length, posCheckDigit))
                return false;

            // Check digit over digits 14–19 
            start = pStartRead + 13;
            length = 6;
            posCheckDigit = start + length;
            if (!CheckDigit(passport, start, length, posCheckDigit))
                return false;

            // Check digit over digits 22–27
            start = pStartRead + 21;
            length = 6;
            posCheckDigit = start + length;
            if (!CheckDigit(passport, start, length, posCheckDigit))
                return false;

            // Check digit over digits 29–42 (may be < if all characters are <) 
            start = pStartRead + 28;
            length = 14;
            posCheckDigit = start + length;
            if (!CheckDigit(passport, start, length, posCheckDigit))
                return false;

            // Composite check digit for characters of machine readable 
            // data of the lower line in positions 1 to 10, 14 to 20 and 22 to 43
            var intervals = new[]
            {
                new Tuple<int, int>(pStartRead, 10),
                new Tuple<int, int>(pStartRead + 13, 7),
                new Tuple<int, int>(pStartRead + 21, 22)
            };
            if (!CheckDigit(passport, intervals, passport.Length - 1))
                return false;

            return true;
        }

        public bool IsDocumentNumber(string number)
        {
            if (number.Length != 9)
                return false;

            string pattern = "^[A-Z0-9<]+$";
            return Regex.IsMatch(number, pattern);
        }

        #region Aux

        private int GetDigit(string passport, int pos)
        {
            return int.Parse(passport[pos].ToString());
        }

        private bool CheckDigit(string passport, int start, int length, int posCheckDigit)
        {
            int accumulate = Acumulate(GetCharacters(passport, new[] { new Tuple<int, int>(start, length) }));
            return GetDigit(passport, posCheckDigit) == accumulate % 10;
        }

        private bool CheckDigit(string passport, IEnumerable<Tuple<int, int>> intervals, int posCheckDigit)
        {
            int accumulate = Acumulate(GetCharacters(passport, intervals));
            return GetDigit(passport, posCheckDigit) == accumulate % 10;
        }

        private IEnumerable<char> GetCharacters(string passport, IEnumerable<Tuple<int, int>> intervals)
        {
            foreach (var interval in intervals)
            {
                int start = interval.Item1;
                int length = interval.Item2;
                for (int i = start; i < start + length; i++)
                    yield return passport[i];
            }
        }

        private int Acumulate(IEnumerable<char> characters)
        {
            int result = 0;
            int[] weights = { 7, 3, 1 };
            int w = 0;
            foreach (var c in characters)
            {
                result += weights[w % weights.Length] * Values[c];
                w++;
            }
            return result;
        }

        #endregion

        #endregion
    }
}
