﻿using System.Text.RegularExpressions;
using System.Net.Mail;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Model.Validator
{
    public sealed class EmailValidator : SingletonValidator<EmailValidator>
    {
        #region IValidator Implementation

        public override ValidationResult Validate(object obj)
        {
            var result = new ValidationResult();

            var email = (string)obj;
            if (!IsValidEmailAddress(email))
                result.AddError($"Invalid email: {email}");

            return result;
        }

        #endregion
        #region Methods

        private static bool IsValidEmailAddress(string address)
        {
            // Blank Validation
            if (string.IsNullOrEmpty(address))
                return false;

            // Net Validation
            try
            { 
                new MailAddress(address);
            }
            catch
            { 
                return false; 
            }

            // Regex Validation
            if (!Regex.IsMatch(address, @"^(?("")(""[^""]+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" + @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$"))
                return false;

            return true;
        }

        #endregion 
    }
}