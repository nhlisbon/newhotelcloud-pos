﻿using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Model.Validator
{
    public interface IValidator<T>
    {
        ValidationResult Validate(T contract);
    }
}