﻿using System;
using System.Linq;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Model.Validator
{
    public class IdentityValidatorEcuador : SingletonValidator<IdentityValidatorEcuador>
    {
        #region IValidator Implementation

        public override ValidationResult Validate(object obj)
        {
            var result = new ValidationResult();
            var identity = (string)obj;
            if (!IsRuc(identity) && !IsNumberIdentity(identity))
                result.AddError("Invalid identity number");
            return result;
        }

        #endregion
        #region Methods

        #region Public

        public bool IsRuc(string ruc)
        {
            return IsRucEcuador(ruc);
        }

        public bool IsNumberIdentity(string numberIdentity)
        {
            return IsNumberIdentityEcuador(numberIdentity);
        }

        //public bool IsNumberIdentity(int[] numberIdentity)
        //{
        //    return IsNumberIdentityEcuador(numberIdentity);
        //}

        #endregion
        #region Aux

        private static bool CheckDigit(string s, int[] coeficients)
        {
            int acum = 0;
            for (int index = 0; index < coeficients.Length; index++)
                acum += int.Parse(s.Substring(index, 1)) * coeficients[index];

            var dv = 11 - (acum % 11);
            if (dv == 11) dv = 0;

            return s.Substring(coeficients.Length, 1) == dv.ToString();
        }

        private static readonly int[] coeficients6 = new int[] { 3, 2, 7, 6, 5, 4, 3, 2 };
        private static readonly int[] coeficients9 = new int[] { 4, 3, 2, 7, 6, 5, 4, 3, 2 };

        private bool IsRucEcuador(string ruc)
        {
            const string FinalCustomerNumberEC = "9999999999999";
            if (!string.IsNullOrEmpty(ruc))
            {
                if (ruc == FinalCustomerNumberEC)
                    return true;
                else
                {
                    if (ruc.Length != 13)
                        return false;

                    if (ruc.Any(c => !char.IsDigit(c)))
                        return false;

                    // personas naturales
                    if (int.Parse(ruc.Substring(2, 1)) <= 5)
                        return IsNumberIdentityEcuador(ruc.Substring(0, 10)) && ruc.Substring(10, 3) == "001";
                    else
                    {
                        // provincia de expedición
                        var ruc01 = int.Parse(ruc.Substring(0, 2));
                        if (ruc01 < 1 || ruc01 > 24)
                            return false;

                        switch (ruc[2])
                        {
                            // publicos
                            case '6':
                                return CheckDigit(ruc, coeficients6);
                            // jurídicos y extranjeros sin cédula
                            case '9':
                                return CheckDigit(ruc, coeficients9) && ruc.Substring(10, 3) == "001";
                        }
                    }
                }
            }

            return false;
        }

        public bool ValidRUCEcuador(string ruc)
        {
            const string FinalCustomerNumberEC = "9999999999999";
            if (!string.IsNullOrEmpty(ruc))
            {
                if (ruc == FinalCustomerNumberEC)
                    return true;
                else
                {
                    if (ruc.Length != 13)
                        return false;

                    if (ruc.Any(c => !char.IsDigit(c)))
                        return false;

                    // personas naturales
                    if (int.Parse(ruc.Substring(2, 1)) <= 5)
                        return IsNumberIdentityEcuador(ruc.Substring(0, 10)) && ruc.Substring(10, 3) == "001";
                    else
                    {
                        // provincia de expedición
                        var ruc01 = int.Parse(ruc.Substring(0, 2));
                        if (ruc01 < 1 || ruc01 > 24)
                            return false;

                        switch (ruc[2])
                        {
                            // publicos
                            case '6':
                                return CheckDigit(ruc, coeficients6);
                            // jurídicos y extranjeros sin cédula
                            case '9':
                                return CheckDigit(ruc, coeficients9) && ruc.Substring(10, 3) == "001";
                        }
                    }
                }
            }

            return false;
        }

        public bool IsNumberIdentityEcuador(string dni)
        {
            if (!string.IsNullOrEmpty(dni))
            {
                if (dni.Length != 10)
                    return false;

                if (dni.Any(c => !char.IsDigit(c)))
                    return false;

                if (int.Parse(dni.Substring(0, 2)) > 24)
                    return false;

                if (int.Parse(dni.Substring(2, 1)) > 5)
                    return false;

                var coeficients = new int[] { 2, 1, 2, 1, 2, 1, 2, 1, 2 };
                int acum = 0;
                for (int index = 0; index < dni.Length - 1; index++)
                {
                    var v = int.Parse(dni.Substring(index, 1)) * coeficients[index];
                    acum += (v >= 10 ? v - 9 : v);
                }

                var dv = 10 - (acum % 10);
                if (dv == 10) dv = 0;

                return dni.Substring(9, 1) == dv.ToString();
            }

            return false;
        }

        //private bool IsRucEcuador(string ruc)
        //{
        //    const string finalCustomerNumberEc = "9999999999999";

        //    if (!string.IsNullOrEmpty(ruc))
        //    {
        //        if (ruc != finalCustomerNumberEc)
        //        {
        //            if (ruc.Length != 13)
        //                return false;

        //            var digits = new int[ruc.Length];
        //            for (int i = 0; i < ruc.Length; i++)
        //            {
        //                if (!char.IsDigit(ruc[i]))
        //                    return false;
        //                digits[i] = int.Parse(ruc.Substring(i, 1));
        //            }

        //            int p = BuildNumberFromDigits(digits[0], digits[1]);
        //            if (p < 1 || p > 24 && p != 30)
        //                return false;

        //            switch (digits[2])
        //            {
        //                case 6:
        //                    {
        //                        #region Publics

        //                        if (BuildNumberFromDigits(digits[9], digits[10], digits[11], digits[12]) == 0)
        //                            return false;

        //                        int[] coeficients = { 3, 2, 7, 6, 5, 4, 3, 2 };

        //                        int acum = 0;
        //                        for (int i = 0; i <= 7; i++)
        //                            acum += digits[i] * coeficients[i];

        //                        int dv = 11 - acum % 11;
        //                        if (dv == 11)
        //                            dv = 0;

        //                        return digits[8] == dv;

        //                        #endregion
        //                    }
        //                case 9:
        //                    {
        //                        #region Juridical and foreigners without identification

        //                        if (BuildNumberFromDigits(digits[10], digits[11], digits[12]) == 0)
        //                            return false;
        //                        int[] coeficients = { 4, 3, 2, 7, 6, 5, 4, 3, 2 };
        //                        int acum = 0;
        //                        for (int i = 0; i <= 8; i++)
        //                            acum += digits[i] * coeficients[i];

        //                        int dv = 11 - acum % 11;
        //                        if (dv == 11)
        //                            dv = 0;

        //                        return digits[9] == dv;

        //                        #endregion
        //                    }
        //                default:
        //                    {
        //                        #region Natural Person

        //                        if (digits[2] >= 0 && digits[2] <= 5)
        //                        {
        //                            if (BuildNumberFromDigits(digits[10], digits[11], digits[12]) == 0)
        //                                return false;
        //                            return IsNumberIdentityEcuador(digits.Take(10).ToArray());
        //                        }
        //                        return false;

        //                        #endregion
        //                    }
        //            }
        //        }
        //        return true;
        //    }
        //    return false;
        //}

        //private bool IsNumberIdentityEcuador(string numberIdentity)
        //{
        //    const int length = 10;
        //    if (string.IsNullOrEmpty(numberIdentity) || numberIdentity.Length != length)
        //        return false;

        //    var digits = new int[numberIdentity.Length];
        //    for (int i = 0; i < numberIdentity.Length; i++)
        //    {
        //        if (!char.IsDigit(numberIdentity[i]))
        //            return false;
        //        digits[i] = int.Parse(numberIdentity.Substring(i, 1));
        //    }
        //    return IsNumberIdentityEcuador(digits);
        //}

        //private bool IsNumberIdentityEcuador(int[] digits)
        //{
        //    const int length = 10;
        //    if (digits == null || digits.Length != length)
        //        return false;

        //    // Provincia
        //    int p = BuildNumberFromDigits(digits[0], digits[1]);
        //    if (p < 1 || p > 24 && p != 30)
        //        return false;

        //    if (digits[2] > 5)
        //        return false;

        //    int[] coefficients = { 2, 1, 2, 1, 2, 1, 2, 1, 2 };
        //    int acum = 0;
        //    for (int i = 0; i < digits.Length - 1; i++)
        //    {
        //        int v = digits[i] * coefficients[i];
        //        acum += v > 9 ? v - 9 : v;
        //    }

        //    int dv = 10 - acum % 10;
        //    if (dv == 10)
        //        dv = 0;
        //    return digits[9] == dv;
        //}

        //private int BuildNumberFromDigits(params int[] digits)
        //{
        //    int result = 0;
        //    for (int i = 0; i < digits.Length; i++)
        //        result += digits[digits.Length - i - 1] * (int)Math.Pow(10, i);
        //    return result;
        //}

        #endregion

        #endregion 
    }
}
