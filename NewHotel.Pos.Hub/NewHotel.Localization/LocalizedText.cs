﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Resources;
using System.Reflection;

namespace NewHotel.Localization
{
    public enum LocalizedSuffix { Label, Column, Message };

    public sealed class LocalizedText
    {
        #region Constants

        private const char LeftKeyDelimiter = '[';
        private const char RightKeyDelimiter = ']';
        private const string LeftTagDelimiter = "/*";
        private const string RightTagDelimiter = "*/";
        private const string NullTagFormat = "{0}";

        #endregion
        #region Members

        private static readonly string _pattern;
        private static readonly char[] _delimiters = new char[] { ' ', '.', ',', '_' };
        private static readonly ResourceManager _manager;

        private readonly CultureInfo _culture;
        private readonly string _format;
        private readonly string[] _tags;
        private readonly object[] _args;
        private readonly LocalizedSuffix _suffix;

        private string _localized;

        #endregion
        #region Properties

        public static ResourceManager Manager
        {
            get { return _manager; }
        }

        private string GetId(string key)
        {
            switch (_suffix)
            {
                case LocalizedSuffix.Label: return key + "Lbl";
                case LocalizedSuffix.Column: return key + "Col";
                case LocalizedSuffix.Message: return key + "Msg";
            }

            return string.Empty;
        }

        #endregion
        #region Constructors

        static LocalizedText()
        {
            _pattern = string.Format(@"\{0}\{1}(.*?)\{2}\{3}",
                LeftTagDelimiter[0], LeftTagDelimiter[1],
                RightTagDelimiter[0], RightTagDelimiter[1]);

            var assembly = Assembly.GetExecutingAssembly();
            var resource = assembly.GetManifestResourceNames().FirstOrDefault(x => x.EndsWith("LocalizedText.resources"));
            if (resource != null)
            {
                _manager = new ResourceManager(resource.Substring(0, resource.LastIndexOf('.')), assembly);
                _manager.IgnoreCase = true;
            }
        }

        public LocalizedText(CultureInfo culture, string format, LocalizedSuffix suffix, params object[] args)
        {
            _culture = culture;
            _format = format;
            _args = args;
            _suffix = suffix;

            try
            {
                var matches = Regex.Matches(_format, _pattern);
                if (matches.Count > 0)
                {
                    _tags = new string[matches.Count];
                    int index = 0;
                    foreach (Match match in matches)
                    {
                        var tag = match.Value;
                        _tags[index++] = tag.Substring(2, tag.Length - 4);
                    }
                }
            }
            catch (ArgumentException)
            {
                _tags = null;
            }
        }

        public LocalizedText(CultureInfo culture, string key, LocalizedSuffix suffix)
            : this(culture, key, suffix, null)
        {
        }

        public LocalizedText(string key, LocalizedSuffix suffix)
            : this(CultureInfo.CurrentUICulture, key, suffix, null)
        {
        }

        public LocalizedText(CultureInfo culture)
            : this(culture, string.Empty, LocalizedSuffix.Message, null)
        {
        }

        #endregion
        #region Private Methods

        private string GetString(string name)
        {
            return Manager.GetString(name, _culture);
        }

        #endregion
        #region Public Methods

        public static string KeyForText(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                var start = text.IndexOf(LeftKeyDelimiter);
                if (start >= 0)
                {
                    int end = text.IndexOf(RightKeyDelimiter, start + 1);
                    if (end >= 0)
                        return text.Substring(start + 1, end - start - 1);
                }

                var values = text.Split(_delimiters, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < values.Length; i++)
                {
                    var value = values[i];
                    var chars = new char[value.Length];
                    int index = 0;
                    for (int j = 0; j < value.Length; j++)
                        if (char.IsLetterOrDigit(value[j]))
                            chars[index++] = value[j];

                    if (index > 0)
                    {
                        chars[0] = char.ToUpper(chars[0]);
                        Array.Resize(ref chars, index);
                    }

                    values[i] = new string(chars);
                }

                return string.Join(string.Empty, values);
            }

            return text;
        }

        public override string ToString()
        {
            if (_localized == null)
            {
                if (_tags != null)
                {
                    var lsb = new StringBuilder(_format);
                    for (int index = 0; index < _tags.Length; index++)
                    {
                        var tag = _tags[index];
                        lsb = lsb.Replace(LeftTagDelimiter + tag + RightTagDelimiter,
                            GetString(GetId(tag)) ?? string.Format(NullTagFormat, tag));
                    }

                    _localized = lsb.ToString();
                }
                else
                    _localized = GetString(GetId(_format)) ?? string.Format(NullTagFormat, _format);

                if (_args != null && _args.Length > 0)
                    _localized = string.Format(_localized, _args);
            }

            return _localized;
        }

        public static implicit operator string(LocalizedText localizedText)
        {
            return localizedText.ToString();
        }

        #endregion
    }
}
