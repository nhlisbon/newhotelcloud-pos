﻿using System;
using System.Security.Cryptography;

namespace NewHotel.Security
{
    public static class MD5Extensions
    {
        public static string ComputeHashToString(this MD5 md5, byte[] bytes)
        {
            try
            {
                var hash = md5.ComputeHash(bytes);
                var res = new char[hash.Length * 2];

                int j = 0;
                foreach (var b in hash)
                {
                    var hex = b.ToString("x2");
                    res[j] = hex[0];
                    res[j + 1] = hex[1];
                    j += 2;
                }

                return new string(res);
            }
            catch (ArgumentNullException ex)
            {
                throw new ApplicationException("Hash has not been generated.", ex);
            }
        }
    }
}
