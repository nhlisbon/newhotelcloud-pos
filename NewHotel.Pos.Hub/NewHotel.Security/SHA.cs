﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace NewHotel.Security
{
    public sealed class SHASignature
    {
        #region Members

        private static readonly SHA1CryptoServiceProvider _sha1 = new SHA1CryptoServiceProvider();
        private static readonly SHA256CryptoServiceProvider _sha256 = new SHA256CryptoServiceProvider();

        #endregion
        #region Private Methods

        private static byte[] SignSHA(X509Certificate2 certificate, HashAlgorithm halg, byte[] bytes)
        {
            if (certificate == null)
                throw new ArgumentNullException("certificate");
            if (halg == null)
                throw new ArgumentNullException("halg");

            try
            {
                var provider = (RSACryptoServiceProvider)certificate.PrivateKey;
                return provider.SignData(bytes, halg);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("SHA signing error", ex);
            }
        }

        private static byte[] SignSHA(X509Certificate2 certificate,
            HashAlgorithm halg, Encoding encoding, string s)
        {
            if (encoding == null)
                throw new ArgumentNullException("encoding");

            return SignSHA(certificate, halg, encoding.GetBytes(s));
        }

        #endregion
        #region Public Methods

        #region SHA 1

        public static byte[] SignSHA1(X509Certificate2 certificate, byte[] bytes)
        {
            return SignSHA(certificate, _sha1, bytes);
        }

        public static byte[] SignSHA1(X509Certificate2 certificate, Encoding encoding, string s)
        {
            return SignSHA(certificate, _sha1, encoding, s);
        }

        public static byte[] SignSHA1(X509Certificate2 certificate, Encoding encoding, string separator, params object[] values)
        {
            return SignSHA(certificate, _sha1, encoding, string.Join(separator ?? string.Empty, values));
        }

        public static byte[] ComputeHashSHA1(byte[] bytes)
        {
            return _sha1.ComputeHash(bytes);
        }

        public static byte[] ComputeHashSHA1(string s, Encoding encoding)
        {
            return ComputeHashSHA1(encoding.GetBytes(s));
        }

        #endregion

        #region SHA 256

        public static byte[] SignSHA256(X509Certificate2 certificate, byte[] bytes)
        {
            return SignSHA(certificate, _sha256, bytes);
        }

        public static byte[] SignSHA256(X509Certificate2 certificate, Encoding encoding, string s)
        {
            return SignSHA(certificate, _sha256, encoding, s);
        }

        public static byte[] SignSHA256(X509Certificate2 certificate, Encoding encoding, string separator, params object[] values)
        {
            return SignSHA(certificate, _sha256, encoding, string.Join(separator ?? string.Empty, values));
        }

        public static byte[] ComputeHashSHA256(byte[] bytes)
        {
            return _sha256.ComputeHash(bytes);
        }

        public static byte[] ComputeHashSHA256(string s, Encoding encoding)
        {
            return ComputeHashSHA256(encoding.GetBytes(s));
        }

        #endregion

        #endregion
    }
}