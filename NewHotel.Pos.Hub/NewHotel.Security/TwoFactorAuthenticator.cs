﻿using System;
using System.Net.Http;

namespace NewHotel.Security
{
    public class TwoFactorAuthenticator
    {
        private readonly Google.Authenticator.TwoFactorAuthenticator _authenticator;

        public TwoFactorAuthenticator()
        {
            _authenticator = new Google.Authenticator.TwoFactorAuthenticator()
            {
                DefaultClockDriftTolerance = TimeSpan.FromMinutes(1)
            };
        }

        public SetupCode GenerateSetupCode(string appName, string account, string accountSecretKey)
        {
            var setup = _authenticator.GenerateSetupCode(appName, account, accountSecretKey, 300, 300, false);
            return new SetupCode
            {
                Account = setup.Account,
                AccountSecretKey = setup.AccountSecretKey,
                ManualEntryKey = setup.ManualEntryKey,
                QrCodeSetupImageUrl = setup.QrCodeSetupImageUrl,
                QrCodeSetupImageBytes = DownloadImageBytes(setup.QrCodeSetupImageUrl)
            };
        }

        public bool ValidateTwoFactorPin(string accountSecretKey, string twoFactorCodeFromClient)
        {
            return _authenticator.ValidateTwoFactorPIN(accountSecretKey, twoFactorCodeFromClient);
        }

        private byte[] DownloadImageBytes(string url)
        {
            var httpClient = new HttpClient();
            var result = httpClient.GetAsync(url).GetAwaiter().GetResult();
            return result.Content.ReadAsByteArrayAsync().GetAwaiter().GetResult();
        }
    }
}