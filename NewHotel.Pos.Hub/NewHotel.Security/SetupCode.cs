﻿namespace NewHotel.Security
{
    public class SetupCode
    {
        public string Account { get; set; }
        public string AccountSecretKey { get; set; }
        public string ManualEntryKey { get; set; }
        public byte[] QrCodeSetupImageBytes { get; set; }
        public string QrCodeSetupImageUrl { get; set; }
    }
}