﻿using System;
using System.Text;
using System.IO;
using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Runtime.InteropServices;
using System.Reflection;

namespace NewHotel.Security
{
    public sealed class RSASignature
    {
        #region Members

        private readonly string _key;
        private readonly RSACryptoServiceProvider _rsa;

        private static readonly SHA1CryptoServiceProvider _sha1 = new SHA1CryptoServiceProvider();
        private static readonly SHA256CryptoServiceProvider _sha256 = new SHA256CryptoServiceProvider();

        #endregion
        #region Properties

        public string Key
        {
            get { return _key; }
        }

        private static string GetFromResource(Assembly assembly, string path)
        {
            using (var stream = assembly.GetManifestResourceStream(path))
            {
                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        #endregion
        #region Constructor

        public RSASignature(string key, string password = null)
        {
            _key = key;
            _rsa = DecodeRSAPrivateKey(DecodeOpenSSLPrivateKey(_key, password));
        }

        public RSASignature(byte[] file, string password = null)
            : this(Encoding.UTF8.GetString(file), password)
        {
        }

        public RSASignature(Assembly assembly, string path, string password = null)
            : this(GetFromResource(assembly, path), password)
        {
        }

        #endregion
        #region Private Methods

        /// <summary>
        /// OpenSSL PBKD uses only one hash cycle (count)
        /// miter is number of iterations required to build sufficient bytes
        /// </summary>
        private static byte[] GetOpenSSL3deskey(byte[] salt, string password, int count, int miter)
        {
            var unmanagedPswd = IntPtr.Zero;
            // MD5 bytes
            int HASHLENGTH = 16;
            // to store contatenated Mi hashed results
            var keymaterial = new byte[HASHLENGTH * miter];

            var psbytes = new byte[password.Length];
            //unmanagedPswd = Marshal.SecureStringToGlobalAllocAnsi(password);
            unmanagedPswd = Marshal.StringToHGlobalAnsi(password);
            Marshal.Copy(unmanagedPswd, psbytes, 0, psbytes.Length);
            Marshal.ZeroFreeGlobalAllocAnsi(unmanagedPswd);

            // contatenate salt and pswd bytes into fixed data array
            var data00 = new byte[psbytes.Length + salt.Length];
            // copy the pswd bytes
            Array.Copy(psbytes, data00, psbytes.Length);
            // concatenate the salt bytes
            Array.Copy(salt, 0, data00, psbytes.Length, salt.Length);

            // do multi-hashing and contatenate results  D1, D2 ... into keymaterial bytes
            var md5 = new MD5CryptoServiceProvider();
            // fixed length initial hashtarget
            var hashtarget = new byte[HASHLENGTH + data00.Length];

            byte[] result = null;
            for (int j = 0; j < miter; j++)
            {
                // Now hash consecutively for count times
                if (j == 0)
                    // initialize 
                    result = data00;
                else
                {
                    Array.Copy(result, hashtarget, result.Length);
                    Array.Copy(data00, 0, hashtarget, result.Length, data00.Length);
                    result = hashtarget;
                }

                for (int i = 0; i < count; i++)
                    result = md5.ComputeHash(result);

                // contatenate to keymaterial
                Array.Copy(result, 0, keymaterial, j * HASHLENGTH, result.Length);
            }

            var deskey = new byte[24];
            Array.Copy(keymaterial, deskey, deskey.Length);

            Array.Clear(psbytes, 0, psbytes.Length);
            Array.Clear(data00, 0, data00.Length);
            Array.Clear(result, 0, result.Length);
            Array.Clear(hashtarget, 0, hashtarget.Length);
            Array.Clear(keymaterial, 0, keymaterial.Length);

            return deskey;
        }

        /// <summary>
        /// Decrypt the 3DES encrypted RSA private key
        /// </summary>
        private static byte[] DecryptKey(byte[] cipherData, byte[] desKey, byte[] IV)
        {
            var stream = new MemoryStream();
            var alg = TripleDES.Create();
            alg.Key = desKey;
            alg.IV = IV;

            try
            {
                var cs = new CryptoStream(stream, alg.CreateDecryptor(), CryptoStreamMode.Write);
                cs.Write(cipherData, 0, cipherData.Length);
                cs.Close();
            }
            catch
            {
                return null;
            }

            return stream.ToArray();
        }

        /// <summary>
        /// Get the binary RSA PRIVATE key, decrypting if necessary
        /// </summary>
        private static byte[] DecodeOpenSSLPrivateKey(string instr, string password)
        {
            const string pemprivheader = "-----BEGIN RSA PRIVATE KEY-----";
            const string pemprivfooter = "-----END RSA PRIVATE KEY-----";

            var pemstr = instr.Trim();            
            if (!pemstr.StartsWith(pemprivheader) || !pemstr.EndsWith(pemprivfooter))
                return null;

            var sb = new StringBuilder(pemstr);
            // remove headers/footers, if present
            sb.Replace(pemprivheader, string.Empty);
            sb.Replace(pemprivfooter, string.Empty);

            // get string after removing leading/trailing whitespace
            var pvkstr = sb.ToString().Trim();

            try
            {
                // if there are no PEM encryption info lines, this is an Unencrypted PEM private key
                return Convert.FromBase64String(pvkstr);
            }
            catch (FormatException)
            {
                // if can't b64 decode, it must be an encrypted private key
            }

            var str = new StringReader(pvkstr);

            // read PEM encryption info. lines and extract salt
            if (!str.ReadLine().StartsWith("Proc-Type: 4,ENCRYPTED"))
                return null;

            var saltline = str.ReadLine();
            if (!saltline.StartsWith("DEK-Info: DES-EDE3-CBC,"))
                return null;

            var saltstr = saltline.Substring(saltline.IndexOf(",") + 1).Trim();
            var salt = new byte[saltstr.Length / 2];
            for (int i = 0; i < salt.Length; i++)
                salt[i] = Convert.ToByte(saltstr.Substring(i * 2, 2), 16);

            if (!(str.ReadLine() == string.Empty))
                return null;

            // remaining b64 data is encrypted RSA key
            var encryptedstr = str.ReadToEnd();

            byte[] binkey;
            try
            {
                // should have b64 encrypted RSA key now
                binkey = Convert.FromBase64String(encryptedstr);
            }
            catch (FormatException)
            {
                // bad b64 data
                return null;
            }

            if (password == null || password.Length == 0)
            {
                // No password supplied for an encrypted private key
                return null;
            }

            // get the 3DES 24 byte key using PDK used by OpenSSL
            // secureString despswd = GetSecPswd("Enter password to derive 3DES key==>");
            // count=1 (for OpenSSL implementation); 2 iterations to get at least 24 bytes
            var deskey = GetOpenSSL3deskey(salt, password, 1, 2);
            if (deskey == null)
                return null;

            // Decrypt the encrypted 3des-encrypted RSA private key
            // OpenSSL uses salt value in PEM header also as 3DES IV
            var rsakey = DecryptKey(binkey, deskey, salt);
            if (rsakey != null)
                // We have a decrypted RSA private key
                return rsakey;

            // Failed to decrypt RSA private key; probably wrong password
            return null;
        }

        private static int GetIntegerSize(BinaryReader reader)
        {
            byte bt = 0;
            byte lowbyte = 0x00;
            byte highbyte = 0x00;
            int count = 0;

            bt = reader.ReadByte();
            // expect integer
            if (bt != 0x02)
                return 0;
            bt = reader.ReadByte();

            if (bt == 0x81)
                // data size in next byte
                count = reader.ReadByte();
            else if (bt == 0x82)
            {
                // data size in next 2 bytes
                highbyte = reader.ReadByte();
                lowbyte = reader.ReadByte();
                var modint = new byte[] { lowbyte, highbyte, 0x00, 0x00 };
                count = BitConverter.ToInt32(modint, 0);
            }
            else
                // we already have the data size
                count = bt;	

            while (reader.ReadByte() == 0x00)
            {
                // remove high order zeros in data
                count -= 1;
            }

            // last ReadByte wasn't a removed zero, so back up a byte
            reader.BaseStream.Seek(-1, SeekOrigin.Current);

            return count;
        }

        /// <summary>
        /// Get the binary RSA PUBLIC key
        /// </summary>
        private static byte[] DecodeOpenSSLPublicKey(string instr)
        {
            const string pempubheader = "-----BEGIN PUBLIC KEY-----";
            const string pempubfooter = "-----END PUBLIC KEY-----";

            var pemstr = instr.Trim();
            if (!pemstr.StartsWith(pempubheader) || !pemstr.EndsWith(pempubfooter))
                return null;

            var sb = new StringBuilder(pemstr);
            // remove headers/footers, if present
            sb.Replace(pempubheader, string.Empty);
            sb.Replace(pempubfooter, string.Empty);
            
            // get string after removing leading/trailing whitespace
            var pubstr = sb.ToString().Trim();

            try
            {
                return Convert.FromBase64String(pubstr);
            }
            catch (FormatException)
            {
                // if can't b64 decode, data is not valid
                return null;
            }
        }

        private static bool CompareByteArrays(byte[] a, byte[] b)
        {
            if (a.Length != b.Length)
                return false;

            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] != b[i])
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Encoded OID sequence for  PKCS #1 rsaEncryption szOID_RSA_RSA = "1.2.840.113549.1.1.1"
        /// </summary>
        private static readonly byte[] SeqOID = { 0x30, 0x0D, 0x06, 0x09, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x01, 0x05, 0x00 };

        /// <summary>
        /// Parses binary asn.1 X509 SubjectPublicKeyInfo; returns RSACryptoServiceProvider
        /// </summary>
        private static RSACryptoServiceProvider DecodeX509PublicKey(byte[] x509key)
        {
            var seq = new byte[15];

            // set up stream to read the asn.1 encoded SubjectPublicKeyInfo blob
            var mem = new MemoryStream(x509key);
            var binr = new BinaryReader(mem); // wrap Memory Stream with BinaryReader for easy reading
            byte bt = 0;
            ushort twobytes = 0;

            try
            {
                twobytes = binr.ReadUInt16();
                if (twobytes == 0x8130)	// data read as little endian order (actual data order for Sequence is 30 81)
                    binr.ReadByte(); // advance 1 byte
                else if (twobytes == 0x8230)
                    binr.ReadInt16(); // advance 2 bytes
                else
                    return null;

                seq = binr.ReadBytes(15); // read the Sequence OID
                if (!CompareByteArrays(seq, SeqOID)) // make sure Sequence for OID is correct
                    return null;

                twobytes = binr.ReadUInt16();
                if (twobytes == 0x8103) // data read as little endian order (actual data order for Bit string is 03 81)
                    binr.ReadByte(); // advance 1 byte
                else if (twobytes == 0x8203)
                    binr.ReadInt16(); // advance 2 bytes
                else
                    return null;

                bt = binr.ReadByte();
                if (bt != 0x00) // expect null byte next
                    return null;

                twobytes = binr.ReadUInt16();
                if (twobytes == 0x8130)	// data read as little endian order (actual data order for Sequence is 30 81)
                    binr.ReadByte(); // advance 1 byte
                else if (twobytes == 0x8230)
                    binr.ReadInt16(); // advance 2 bytes
                else
                    return null;

                twobytes = binr.ReadUInt16();
                byte lowbyte = 0x00;
                byte highbyte = 0x00;

                if (twobytes == 0x8102)	// data read as little endian order (actual data order for Integer is 02 81)
                    lowbyte = binr.ReadByte(); // read next bytes which is bytes in modulus
                else if (twobytes == 0x8202)
                {
                    highbyte = binr.ReadByte();	// advance 2 bytes
                    lowbyte = binr.ReadByte();
                }
                else
                    return null;

                var modint = new byte[] { lowbyte, highbyte, 0x00, 0x00 }; // reverse byte order since asn.1 key uses big endian order
                var modsize = BitConverter.ToInt32(modint, 0);

                var firstbyte = binr.ReadByte();
                binr.BaseStream.Seek(-1, SeekOrigin.Current);

                if (firstbyte == 0x00)
                {
                    // if first byte (highest order) of modulus is zero, don't include it
                    binr.ReadByte(); // skip this null byte
                    modsize -= 1; // reduce modulus buffer size by 1
                }

                var modulus = binr.ReadBytes(modsize); // read the modulus bytes

                if (binr.ReadByte() != 0x02) // expect an Integer for the exponent data
                    return null;
                var expbytes = (int)binr.ReadByte(); // should only need one byte for actual exponent data (for all useful values)
                var exponent = binr.ReadBytes(expbytes);

                // create RSACryptoServiceProvider instance and initialize with public key
                var RSA = new RSACryptoServiceProvider();
                var RSAKeyInfo = new RSAParameters();
                RSAKeyInfo.Modulus = modulus;
                RSAKeyInfo.Exponent = exponent;
                RSA.ImportParameters(RSAKeyInfo);

                return RSA;
            }
            catch
            {
                return null;
            }
            finally
            {
                binr.Close();
            }
        }

        private static byte[] SignSHA(X509Certificate2 certificate, Encoding encoding, HashAlgorithm halg, string s)
        {
            if (certificate == null)
                throw new ArgumentNullException("certificate");
            if (encoding == null)
                throw new ArgumentNullException("encoding");
            if (halg == null)
                throw new ArgumentNullException("halg");

            try
            {
                var provider = (RSACryptoServiceProvider)certificate.PrivateKey;
                return provider.SignData(encoding.GetBytes(s), halg);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("SHA signing error", ex);
            }
        }

        private static byte[] SignSHA(X509Certificate2 certificate, Encoding encoding, HashAlgorithm halg, string separator, params object[] values)
        {
            return SignSHA(certificate, encoding, halg, string.Join(separator ?? string.Empty, values));
        }

        #endregion
        #region Public Methods

        /// <summary>
        /// Parses binary ans.1 RSA private key
        /// returns RSACryptoServiceProvider
        /// </summary>
        public static RSACryptoServiceProvider DecodeRSAPrivateKey(byte[] privatekey)
        {
            // Set up stream to decode the asn.1 encoded RSA private key
            using (var stream = new MemoryStream(privatekey))
            {
                // wrap Memory Stream with BinaryReader for easy reading
                using (var reader = new BinaryReader(stream))
                {
                    byte bt = 0;
                    ushort twobytes = 0;
                    int elems = 0;

                    try
                    {
                        twobytes = reader.ReadUInt16();
                        // data read as little endian order (actual data order for Sequence is 30 81)
                        if (twobytes == 0x8130)
                            // advance 1 byte
                            reader.ReadByte();
                        else if (twobytes == 0x8230)
                            // advance 2 bytes
                            reader.ReadInt16();
                        else
                            return null;

                        twobytes = reader.ReadUInt16();
                        // version number
                        if (twobytes != 0x0102)
                            return null;
                        bt = reader.ReadByte();
                        if (bt != 0x00)
                            return null;

                        // all private key components are Integer sequences
                        elems = GetIntegerSize(reader);
                        var MODULUS = reader.ReadBytes(elems);

                        elems = GetIntegerSize(reader);
                        var E = reader.ReadBytes(elems);

                        elems = GetIntegerSize(reader);
                        var D = reader.ReadBytes(elems);

                        elems = GetIntegerSize(reader);
                        var P = reader.ReadBytes(elems);

                        elems = GetIntegerSize(reader);
                        var Q = reader.ReadBytes(elems);

                        elems = GetIntegerSize(reader);
                        var DP = reader.ReadBytes(elems);

                        elems = GetIntegerSize(reader);
                        var DQ = reader.ReadBytes(elems);

                        elems = GetIntegerSize(reader);
                        var IQ = reader.ReadBytes(elems);

                        // create RSACryptoServiceProvider instance and initialize with public key
                        var rsa = new RSACryptoServiceProvider();
                        var rsaParams = new RSAParameters();
                        rsaParams.Modulus = MODULUS;
                        rsaParams.Exponent = E;
                        rsaParams.D = D;
                        rsaParams.P = P;
                        rsaParams.Q = Q;
                        rsaParams.DP = DP;
                        rsaParams.DQ = DQ;
                        rsaParams.InverseQ = IQ;
                        rsa.ImportParameters(rsaParams);

                        return rsa;
                    }
                    catch
                    {
                        return null;
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
        }

        public RSACryptoServiceProvider RSA
        {
            get { return _rsa; }
        }

        public byte[] Sign(Encoding encoding, string s)
        {
            if (encoding == null)
                throw new ArgumentNullException("encoding");

            try
            {
                if (RSA != null)
                    return RSA.SignData(encoding.GetBytes(s), _sha1);

                return null;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("RSA signing error", ex);
            }
        }

        public byte[] SignSHA256(Encoding encoding, string s)
        {
            if (encoding == null)
                throw new ArgumentNullException("encoding");

            try
            {
                if (RSA != null)
                    return RSA.SignData(encoding.GetBytes(s), _sha256);

                return null;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("RSA signing error", ex);
            }
        }

        public byte[] Sign(string separator, Encoding encoding, params object[] values)
        {
            return Sign(encoding, string.Join(separator ?? string.Empty, values));
        }

        public string SignToBase64String(string separator, Encoding encoding, params object[] values)
        {
            return Convert.ToBase64String(Sign(separator, encoding, values));
        }

        #endregion
    }
}