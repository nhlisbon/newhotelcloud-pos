﻿using System;
using System.Xml;
using System.Security.Cryptography.Xml;
using System.Security.Cryptography.X509Certificates;

namespace NewHotel.Security
{
    public static class Certificate
    {
        #region Private Methods

        private static X509Store GetCertificateStore(StoreName name, StoreLocation location)
        {
            var store = new X509Store(name, location);
            store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
            return store;
        }

        #endregion
        #region Public Methods

        public static X509Certificate2 GetCertificateBySubject(string certificateSubject,
            StoreName storeName = StoreName.Root, StoreLocation storeLocation = StoreLocation.LocalMachine)
        {
            var store = GetCertificateStore(storeName, storeLocation);

            X509Certificate2 x509Cert = null;
            foreach (var item in store.Certificates)
            {
                if (item.Subject.Contains(string.Format("CN={0}", certificateSubject)))
                {
                    x509Cert = item;
                    break;
                }
            }

            return x509Cert;
        }

        public static X509Certificate2 GetCertificateBySerialNumber(string certificateSerialNumber,
            StoreName storeName = StoreName.Root, StoreLocation storeLocation = StoreLocation.LocalMachine)
        {
            var store = GetCertificateStore(storeName, storeLocation);

            X509Certificate2 x509Cert = null;
            var certificates = store.Certificates.Find(X509FindType.FindBySerialNumber, certificateSerialNumber, false);

            if (certificates.Count > 0)
                x509Cert = certificates[0];

            return x509Cert;
        }

        public static XmlDocument GetSignedXmlDoc(string xml, X509Certificate2 x509Cert, string tagName, string parentTagName)
        {
            try
            {
                var keyAlgorithm = x509Cert.GetKeyAlgorithm().ToString();

                // Create a new XML document.
                var doc = new XmlDocument();

                // Format the document to ignore white spaces.
                doc.PreserveWhitespace = false;
                doc.LoadXml(xml);

                // cheching the element to be signed
                int qtdeRefUri = doc.GetElementsByTagName(tagName).Count;

                if (qtdeRefUri == 0)
                    throw new Exception(string.Format("Sign tag {0} not found.", tagName));
                else
                {
                    if (qtdeRefUri > 1)
                        throw new Exception(string.Format("Sign tag {0} not unique.", tagName));
                    else
                    {
                        // Create a SignedXml object.
                        var signedXml = new SignedXml(doc);

                        // Add the key to the SignedXml document
                        signedXml.SigningKey = x509Cert.PrivateKey;

                        // Create a reference to be signed
                        var reference = new Reference(string.Empty);

                        if (!string.IsNullOrEmpty(tagName))
                        {
                            var attrs = doc.GetElementsByTagName(tagName).Item(0).Attributes;
                            if (attrs != null)
                            {
                                foreach (XmlAttribute attr in attrs)
                                {
                                    if (attr.Name == "Id")
                                        reference.Uri = "#" + attr.InnerText;
                                }
                            }
                        }

                        // Add an enveloped transformation to the reference.
                        var env = new XmlDsigEnvelopedSignatureTransform();
                        reference.AddTransform(env);

                        var c14 = new XmlDsigC14NTransform();
                        reference.AddTransform(c14);

                        // Add the reference to the SignedXml object.
                        signedXml.AddReference(reference);

                        // Create a new KeyInfo object
                        var keyInfo = new KeyInfo();

                        // Load the certificate into a KeyInfoX509Data object
                        // and add it to the KeyInfo object.
                        keyInfo.AddClause(new KeyInfoX509Data(x509Cert));

                        // Add the KeyInfo object to the SignedXml object.
                        signedXml.KeyInfo = keyInfo;
                        signedXml.ComputeSignature();

                        // Get the XML representation of the signature and save
                        // it to an XmlElement object.
                        var xmlDigitalSignature = signedXml.GetXml();

                        // save element on XML 
                        if (string.IsNullOrEmpty(parentTagName))
                            doc.DocumentElement.AppendChild(doc.ImportNode(xmlDigitalSignature, true));
                        else
                            doc.GetElementsByTagName(parentTagName).Item(0).AppendChild(doc.ImportNode(xmlDigitalSignature, true));
                        var xmlDoc = new XmlDocument();
                        xmlDoc.PreserveWhitespace = false;
                        xmlDoc = doc;

                        // Return signed XML document
                        return xmlDoc;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Sign exception.", ex);
            }
        }

        public static XmlDocument GetSignedXmlDoc(string xml, string certificateSerialNumber, string tagName, string parentTagName)
        {
            var x509Cert = GetCertificateBySerialNumber(certificateSerialNumber);

            if (x509Cert != null)
                return GetSignedXmlDoc(xml, x509Cert, tagName, parentTagName);
            else
                throw new Exception(string.Format("Certificate serial number {0} not found.", certificateSerialNumber));
        }

        public static string GetSignedXml(string xml, X509Certificate2 x509Cert, string tagName, string parentTagName)
        {
            return GetSignedXmlDoc(xml, x509Cert, tagName, parentTagName).OuterXml;
        }

        public static string GetSignedXml(string xml, string certificateSerialNumber, string tagName, string parentTagName)
        {
            return GetSignedXmlDoc(xml, certificateSerialNumber, tagName, parentTagName).OuterXml;
        }

        public static void SignXmlDocument(XmlDocument document, X509Certificate2 certificate, string tagName)
        {
            try
            {
                var keyInfoData = new KeyInfoX509Data();
                keyInfoData.AddCertificate(certificate);
                keyInfoData.AddIssuerSerial(certificate.Issuer, certificate.GetSerialNumberString());

                var keyInfo = new KeyInfo();
                keyInfo.AddClause(keyInfoData);

                var reference = new Reference(string.Empty);
                reference.AddTransform(new XmlDsigEnvelopedSignatureTransform(false));
                reference.AddTransform(new XmlDsigExcC14NTransform(false));
                reference.Uri = "#" + tagName;

                var xml = new SignedXml(document);
                xml.SigningKey = certificate.PrivateKey;
                xml.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigExcC14NTransformUrl;
                xml.KeyInfo = keyInfo;
                xml.AddReference(reference);
                xml.ComputeSignature();

                var element = xml.GetXml();
                document.DocumentElement.AppendChild(element);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Error signing XML document", ex);
            }
        }

        #endregion
    }
}