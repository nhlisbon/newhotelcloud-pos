﻿using System;
using System.Text;
using System.Reflection;
using NewHotel.Security;

namespace NewHotel.Fiscal.Saft
{
    public static class SAFTPT
    {
        #region Constants

        public const string NewHotelCloudCertificateNumber = "1437";
        /// <summary>
        /// Portaria: 1192/2009
        /// </summary>
        public const string Version101 = "1.01_01";
        /// <summary>
        /// Portaria: 160/2013
        /// </summary>
        public const string Version102 = "1.02_01";
        /// <summary>
        /// Portaria: 274/2013
        /// </summary>
        public const string Version103 = "1.03_01";
        /// <summary>
        /// Portaria: 302/2016
        /// </summary>
        public const string Version104 = "1.04_01";

        #endregion
        #region Members

        public static readonly RSASignature Instance;

        #endregion
        #region Constructor

        static SAFTPT()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var fullName = assembly.FullName;
            Instance = new RSASignature(assembly, fullName.Substring(0, fullName.IndexOf(',')) + "." + "NHCloudSAFT.pem");
        }

        #endregion
        #region Private Methods

        private static string Sign(this RSASignature signature, DateTime docSysDateTime, DateTime docDate, string docCode, long docNum, string docSerie, decimal docValue, string hash)
        {
            var value = decimal.ToInt64(Math.Truncate(docValue * 100)).ToString("000");
            var intValue = value.Substring(0, value.Length - 2);
            var decValue = value.Substring(value.Length - 2, 2);
            return signature.SignToBase64String(";", Encoding.UTF8,
                docDate.Date.ToString("yyyy-MM-dd"), docSysDateTime.ToString("yyyy-MM-ddTHH:mm:ss"),
                string.Format("{0} {1}/{2}", docCode, docSerie, docNum),
                string.Format("{0}.{1}", intValue, decValue), hash ?? string.Empty);
        }

        #endregion
        #region Public Methods

        public static string SignTicket(this RSASignature signature, DateTime docSysDateTime, DateTime docDate, long docNum, string docSerie, decimal docValue, string hash)
        {
            return Sign(signature, docSysDateTime, docDate, "T", docNum, docSerie, docValue, hash);
        }

        public static string SignInvoice(this RSASignature signature, DateTime docSysDateTime, DateTime docDate, long docNum, string docSerie, decimal docValue, string hash)
        {
            return Sign(signature, docSysDateTime, docDate, "F", docNum, docSerie, docValue, hash);
        }

        public static string SignProforma(this RSASignature signature, DateTime docSysDateTime, DateTime docDate, long docNum, string docSerie, decimal docValue, string hash)
        {
            return Sign(signature, docSysDateTime, docDate, "P", docNum, docSerie, docValue, hash);
        }

        public static string SignCreditNote(this RSASignature signature, DateTime docSysDateTime, DateTime docDate, long docNum, string docSerie, decimal docValue, string hash)
        {
            return Sign(signature, docSysDateTime, docDate, "N", docNum, docSerie, docValue, hash);
        }

        public static string SignDebitNote(this RSASignature signature, DateTime docSysDateTime, DateTime docDate, long docNum, string docSerie, decimal docValue, string hash)
        {
            return Sign(signature, docSysDateTime, docDate, "D", docNum, docSerie, docValue, hash);
        }

        public static string SignLookupTable(this RSASignature signature, DateTime docSysDateTime, DateTime docDate, long docNum, string docSerie, decimal docValue, string hash)
        {
            return Sign(signature, docSysDateTime, docDate, "C", docNum, docSerie, docValue, hash);
        }

        public static string GetVersion(DateTime date)
        {
            if (date.Date < new DateTime(2013, 7, 1))
                return Version101;
            else if (date.Date < new DateTime(2013, 10, 1))
                return Version102;
            else
                return Version103;
        }

        public static string GetLabel(string hash)
        {
            if (!string.IsNullOrEmpty(hash) && hash.Length > 30)
                return string.Format("{0}{1}{2}{3}-Processado por programa certificado no. {4}/AT",
                    hash.Substring(0, 1), hash.Substring(10, 1), hash.Substring(20, 1), hash.Substring(30, 1),
                    NewHotelCloudCertificateNumber);

            return string.Empty;
        }

        #endregion
    }
}