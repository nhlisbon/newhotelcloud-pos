﻿using System;
using System.Collections.Generic;
using NewHotel.Pos.Printer.Interfaces;

namespace NewHotel.Pos.Printer
{
    public class PrintableTableLine : IPrintableTableLine
    {
        public Guid Id { get; set; }
        public string ProductQuantity { get; set; }
        public string ProductDescription { get; set; }
        public IPrinteableSeparator Separator { get; set; }
        public ICollection<IPrinteablePreparation> Preparations { get; set; }
        public string Notes { get; set; }
    }
}