﻿using NewHotel.Pos.Printer.Interfaces;

namespace NewHotel.Pos.Printer
{
    public class TipSuggestionSetting : ITipSuggestionSetting
    {
        public string Description { get; set; }
        public decimal Percent { get; set; }
        public bool Autogenerate { get; set; }
        public bool OverNet { get; set; }
    }
}