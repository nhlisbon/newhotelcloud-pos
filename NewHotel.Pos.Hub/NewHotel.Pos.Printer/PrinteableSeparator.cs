﻿using System;
using NewHotel.Pos.Printer.Interfaces;

namespace NewHotel.Pos.Printer
{
    public class PrinteableSeparator : IPrinteableSeparator
    {
        #region Members

        public Guid? Id { get; set; }
        public string Description { get; set; }
        public short ImpresionOrden { get; set; }

        public override string ToString()
        {
            return Description;
        }

        public override bool Equals(object obj)
        {
            return Description == ((IPrinteableSeparator)obj)?.Description;
        }

        public int CompareTo(object obj)
        {
            return ImpresionOrden.CompareTo(((IPrinteableSeparator)obj).ImpresionOrden);
        }

        #endregion
    }
}