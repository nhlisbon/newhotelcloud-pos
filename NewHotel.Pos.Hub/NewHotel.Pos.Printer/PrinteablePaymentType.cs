﻿using System;
using NewHotel.Pos.Printer.Interfaces;

namespace NewHotel.Pos.Printer
{
    public class PrinteablePaymentType : IPrinteablePaymentType
    {
        public Guid Id { get; set; }
        public string AuxiliarCode { get; set; }
    }
}