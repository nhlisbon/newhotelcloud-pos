﻿using System;
using NewHotel.Pos.Printer.Interfaces;

namespace NewHotel.Pos.Printer
{
    public class PrinteableArea : IPrinteableArea
    {
        public PrinteableArea(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}