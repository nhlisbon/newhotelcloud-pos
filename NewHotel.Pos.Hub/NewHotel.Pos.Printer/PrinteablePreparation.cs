﻿using System;
using NewHotel.Pos.Printer.Interfaces;

namespace NewHotel.Pos.Printer
{
    public class PrinteablePreparation : IPrinteablePreparation
    {
        public PrinteablePreparation(Guid id, string description)
        {
            Id = id;
            Description = description;
        }

        public Guid Id { get; }
        public string Description { get; }
    }
}