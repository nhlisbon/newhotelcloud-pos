﻿using NewHotel.DataAnnotations;

namespace NewHotel.Pos.Printer.Interfaces
{
    public interface IPrinteableLookupTable
    {
        string Serie { get;}
        string Signature { get; }
        long? Number { get; }
        string SignedLabel { get; }
        string ValidationCode { get; }
        Blob? QrCode { get; }
    }
}
