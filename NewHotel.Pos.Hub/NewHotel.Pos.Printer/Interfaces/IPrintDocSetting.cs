﻿using System.Collections.Generic;
using System.Windows;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Pos.Printer.Interfaces
{ 
    public interface IPrintDocSetting
    {
        DocumentSign SignType { get; set; }

        string Description { get; }

        bool PrintTaxDetails { get; }

        bool PrintOpeningTime { get; }

        bool PrintCloseDateTime { get; }

        bool PrintSeparators { get; }

        bool PrintLogo { get; }

        bool PrintColumnCaptions { get; }

        bool SuppressDiscountWhenCero { get; }

        bool SuppressRechargeWhenCero { get; }

        bool PrintPaymentOptions { get; }

        bool PrintPaymentDetails { get; }

        bool PrintTips { get; }

        string PrintMessage1 { get; }

        string PrintMessage2 { get; }

        bool PrintCustomerSignature { get; }

        short HeaderLinesEmpty { get; }

        short? MinLines { get; }

        Blob? LogoImage { get; }

        HorizontalAlignment? LogoHorizontalAlignment { get; }

        bool ShowCommentsProducts { get; }

        bool ShowCommentsTicket { get; }

        bool IsLarge { get; }

        bool TipSuggestion { get; }

        bool PrintTaxDetailsPerLine { get; }

        bool PrintSubtotal { get; }

        KindSubTotal? SubTotalToPrint { get; }

        short Copies { get; }

        List<ITipSuggestionSetting> Suggestions { get; }

        string Country { get; }

        TicketProductGrouping ProductGrouping { get; }

        string FiscalDescription { get; }
        
        public bool ShowSignatureOptions { get; }
        
        public string AlternateRoomNumber { get;  }
        
        public string AlternateName { get;  }
        
        public string AlternateSignature { get;  }

        public bool PrintDiscountDescription { get; }
    }
}