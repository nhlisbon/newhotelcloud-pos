﻿using System;

namespace NewHotel.Pos.Printer.Interfaces
{
    public interface IPrinteableSeparator : IComparable
    {
        Guid? Id { get; }
        string Description { get;}
        short ImpresionOrden { get; }
    }
}
