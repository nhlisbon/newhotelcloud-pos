﻿using System;
using System.Collections.Generic;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Pos.Printer.Interfaces
{
    public interface IPrintableDoc
    {
        decimal TotalAmount { get; }

        long OpeningNumber { get; }
        long? Serie { get; }
        string SeriePrex { get; }
        string ValidationCode { get; }
        Blob? QrCode { get; }
        string TableDescription { get; }
        decimal TaxAmount { get; }
        decimal TipAmount { get; }
        string UserDescription { get; }

        string FiscalDocFiscalNumber { get; }
        string FiscalDocTo { get; }
        string FiscalIdenTypeName { get; }
        string FiscalDocAddress { get; }
        string FiscalDocEmail { get; }

        string FiscalDocSerie { get; }
        string FiscalDocSeriePrex { get; }
        long? FiscalDocNumber { get; }
        string FiscalDocValidationCode { get; }
        Blob? FiscalDocQrCode { get; }
        string FiscalDocSignature { get; }
        short FiscalDocPrints { get; }
        
        string Cufe { get; }

        string CreditNoteSerie { get; }
        string CreditNoteSeriePrex { get; }
        long? CreditNoteNumber { get; }
        string CreditNoteValidationCode { get; }
        Blob? CreditNoteQrCode { get; }
        string CreditNoteSignature { get; }
        string CreditNoteUserDescription { get; }
        short CreditNotePrints { get; }

        string SignedLabel { get; }
        string TicketSignature { get; }
        Blob? DigitalSignature { get; }
        DateTime? CloseDate { get; }
        DateTime? CloseTime { get; }
        decimal GeneralDiscount { get; }
        decimal Recharge { get; }
        bool TaxIncluded { get; }
        string Observations { get; }
        Guid? Account { get; }

        int VisibleOrdersCount { get; }
        DateTime CheckOpeningDate { get; }
        DateTime CheckOpeningTime { get; }
        string CashierDescription { get; }
        bool HasCashPayment { get; }
        bool HasMealPlanPayment { get; }
        bool HasCreditRoomPayment { get; }
        bool HasHouseUsePayment { get; }
        bool HasAccountDepositPayment { get; }
        CurrentAccountType? AccountType { get; }
        bool IsAnul { get; }
        string StandDescription { get; }
        string Room { get; }
        string Name { get; }
        short Paxs { get; }
        int Shift { get; }
        short Prints { get; }
        decimal GrossSubTotalAmount { get; }
        decimal NetSubTotalAmount { get; }
        string TipDescription { get; }
        decimal PaymentTotalAmount { get; }
        decimal ChangeAmount { get; }
        long? FpInvoiceNumber { get; }
        IEnumerable<IPrintableOrder> ProductLines { get; }
        IEnumerable<IPrinteableLookupTable> LookupTables { get; }
        IEnumerable<IPrintablePayment> Payments { get; }
        IEnumerable<IPrintableClient> Clients { get; }
        string SaloonDescription { get; }

        bool IsBallot { get; }
        bool IsTicket { get; }
        bool IsInvoice { get; }
        bool IsCreditNote { get; }

        string AccountDescription { get; }
        bool AllowRoomCredit { get; }
        string CurrencySymbol { get; }
        string LastUserDispatched { get; }
        string Pension { get; }
    }
}