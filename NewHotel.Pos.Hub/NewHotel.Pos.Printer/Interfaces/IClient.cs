﻿using System;

namespace NewHotel.Pos.Printer.Interfaces
{
    public interface IPrintableClient
    {
        string ClientInfo { get; }
        string Preferences { get; }
        string Diets { get; }
        string Attentions { get; }
        string Allergies { get; }
        string UncommonAllergies { get; }
        string SegmentOperations { get; }
        short Pax { get; }
    }
}