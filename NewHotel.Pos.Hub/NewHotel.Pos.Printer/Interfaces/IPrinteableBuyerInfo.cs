﻿namespace NewHotel.Pos.Printer.Interfaces
{
    public interface IPrinteableBuyerInfo
    {
        string FiscalNumber { get; }
        string Name { get; }
        bool IsDefaultCustomer { get; }
    }
}
