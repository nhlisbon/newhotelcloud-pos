﻿namespace NewHotel.Pos.Printer.Interfaces
{
    public interface ITipSuggestionSetting
    {
        string Description { get; set; }
        decimal Percent { get; set; }
        bool Autogenerate { get; set; }
        bool OverNet { get; set; }
    }
}
