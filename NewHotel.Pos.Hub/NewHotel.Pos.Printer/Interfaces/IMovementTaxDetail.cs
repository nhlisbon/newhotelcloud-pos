﻿using System;

namespace NewHotel.Pos.Printer.Interfaces
{
    public interface IPrinteableMovementTaxDetail
    {
        Guid TaxRateId { get; set; }

        decimal TaxPercent { get; set; }

        string TaxRateDescription { get; set; }

        decimal TaxBase { get; set; }

        decimal TaxValue { get; set; }
    }
}