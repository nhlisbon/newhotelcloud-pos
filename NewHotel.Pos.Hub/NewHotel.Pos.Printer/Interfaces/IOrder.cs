﻿using System;
using System.Collections.Generic;

namespace NewHotel.Pos.Printer.Interfaces
{
    public interface IPrintableOrder
    {
        Guid Id { get; }
        short ItemNumber { get; }
        string ProductDescription { get; }
        decimal Quantity { get; }
        Guid? DiscountType { get; }
        string DiscountDescription { get; }
        decimal DiscountPercent { get; }
        decimal DiscountValue { get; }
        string DiscountTypeDescription {  get; }
        bool ShowDiscountInfo { get; }
        bool AutomaticDiscount { get; }
        decimal ProductPriceBeforeDiscount { get; }
        decimal CostBeforeDiscount { get; }
        decimal Cost { get; }
        short CancellationStatus { get; }
        bool IsActive { get; }
        bool AnulAfterClosed { get; }
        bool IsVisible { get; }
        bool IsTip { get; }
        short? PaxNumber { get; }
        string PaxName { get; }
        bool HasManualPrice { get; }
        bool HasManualSeparator { get; }
        Guid ProductId { get; }
        Guid? AreaId { get; }
        string Notes { get; }

        IPrinteableSeparator Separator { get; }

        Guid FirstIvaCode { get; }
        string FirstIvaDescription { get; }
        decimal FirstIvaBase { get; }
        decimal FirstIvaPercent { get; }
        decimal FirstIvas { get; }
        string FirstIvaAuxCode { get; }

        Guid? SecondIvaCode { get; }
        string SecondIvaDescription { get; }
        decimal? SecondIvaBase { get; }
        decimal? SecondIvaPercent { get; }
        decimal? SecondIvas { get; }
        string SecondIvaAuxCode { get; }

        Guid? ThirdIvaCode { get; }
        string ThirdIvaDescription { get; }
        decimal? ThirdIvaBase { get; }
        decimal? ThirdIvaPercent { get; }
        decimal? ThirdIvas { get; }
        string ThirdIvaAuxCode { get; }

        ICollection<IPrinteablePreparation> Preparations { get; }
        ICollection<IPrinteableArea> Areas { get; }
        ICollection<IPrintableTableLine> TableLines { get; }

        string CodeIsentoIva {  get; }
        string DescIsentoIva { get; }
        bool ShowQuantityDecimalCase { get; }
    }

    public interface IPrintableTableLine
    {
        Guid Id { get; }
        IPrinteableSeparator? Separator { get; }
        string ProductQuantity { get; }
        string ProductDescription { get; }
        ICollection<IPrinteablePreparation> Preparations { get; }
        string Notes { get; }
    }

    public interface IPrinteablePreparation
    {
        Guid Id { get; }
        string Description { get; }
    }

    public interface IPrinteableArea
    {
        Guid Id { get; }
    }
}