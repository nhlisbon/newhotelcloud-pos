﻿namespace NewHotel.Pos.PrinterDocument.Interfaces
{
    public interface IPrinteableBuyerInfo
    {
        string Name { get; set; }

        string FiscalNumber { get; set; }

        string FiscalDocSerie { get; set; }

        string FactSignature { get; set; }

        bool IsDefaultCustomer { get;}

        string Text { get;}
    }
}

