﻿using System;

namespace NewHotel.Pos.Printer.Interfaces
{
    public interface IPrintablePayment
    {
        bool IsCash { get; }
        decimal ReceiveValue { get; }
        string TireDescription { get; }
        bool WideDetails { get; }
        string Details { get; set; }
        string Description { get; }
        IPrinteablePaymentType Type { get; }
    }

    public interface IPrinteablePaymentType
    {
        Guid Id { get; set; }
        string AuxiliarCode { get; }
    }
}
