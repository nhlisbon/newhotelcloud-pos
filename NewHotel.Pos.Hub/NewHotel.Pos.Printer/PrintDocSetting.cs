﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using NewHotel.DataAnnotations;
using NewHotel.Pos.Printer.Interfaces;

namespace NewHotel.Pos.Printer
{
    public class PrintDocSetting : IPrintDocSetting
    {
        #region Constructors

        public PrintDocSetting()
        {
            Suggestions = new List<ITipSuggestionSetting>();
        }

        #endregion
        
        #region Properties

        public DocumentSign SignType { get; set; }
        public string Description { get; set; }
        public bool PrintTaxDetails { get; set; }
        public bool PrintOpeningTime { get; set; }
        public bool PrintCloseDateTime { get; set; }
        public bool PrintSeparators { get; set; }
        public bool PrintLogo { get; set; }
        public bool PrintColumnCaptions { get; set; }
        public bool SuppressDiscountWhenCero { get; set; }
        public bool SuppressRechargeWhenCero { get; set; }
        public bool PrintPaymentOptions { get; set; }
        public bool PrintPaymentDetails { get; set; }
        public bool PrintTips { get; set; }
        public string PrintMessage1 { get; set; }
        public string PrintMessage2 { get; set; }
        public bool PrintCustomerSignature { get; set; }
        public short HeaderLinesEmpty { get; set; }
        public short? MinLines { get; set; }
        public Blob? LogoImage { get; set; }
        public HorizontalAlignment? LogoHorizontalAlignment { get; set; }
        public bool ShowCommentsProducts { get; set; }
        public bool ShowCommentsTicket { get; set; }
        public bool IsLarge { get; set; }
        public bool TipSuggestion { get; set; }
        public bool PrintTaxDetailsPerLine { get; set; }
        public bool PrintSubtotal { get; set; }
        public KindSubTotal? SubTotalToPrint { get; set; }
        public short Copies { get; set; }
        public List<ITipSuggestionSetting> Suggestions { get; }
        public string Country { get; set; }
        public TicketProductGrouping ProductGrouping { get; set; } = TicketProductGrouping.SeatsSeparators;
        public string FiscalDescription { get; set; }
        public bool ShowSignatureOptions { get; set; }
        public string AlternateRoomNumber { get; set; }
        public string AlternateName { get; set; }
        public string AlternateSignature { get; set; }
        public bool PrintDiscountDescription { get; set; }
        
        #endregion
       
        #region Methods

        public static PrintDocSetting Create(POSTicketPrintConfigurationRecord record, DocumentSign signType, string country, POSStandRecord stand)
        {
            var ticketSettings = new PrintDocSetting()
            {
                SignType = signType,
                Description = record.Description,
                HeaderLinesEmpty = record.HeaderLinesEmpty,
                IsLarge = record.IsLarge,
                LogoImage = record.Logo,
                MinLines = record.MinLines,
                PrintCloseDateTime = record.PrintCloseDateTime,
                PrintColumnCaptions = record.ColumnCaptions,
                PrintCustomerSignature = record.PrintSignature,
                PrintLogo = record.ShowLogo,
                PrintMessage1 = record.PrintMessage1,
                PrintMessage2 = record.PrintMessage2,
                PrintOpeningTime = record.PrintOpeningTime,
                PrintPaymentDetails = record.PaymentDetailed,
                PrintPaymentOptions = record.PaymentInReceipts,
                PrintSeparators = record.PrintSeparators,
                PrintTaxDetails = record.PrintTaxDetails,
                PrintTips = record.PrintTips,
                ShowCommentsProducts = record.ShowCommentsProducts,
                ShowCommentsTicket = record.ShowCommentsTicket,
                SuppressDiscountWhenCero = record.SupressDiscount,
                SuppressRechargeWhenCero = record.SupressRecharge,
                TipSuggestion = record.TipSuggestion,
                PrintTaxDetailsPerLine = record.PrintTaxDetailsPerLine,
                PrintSubtotal = record.PrintSubtotal,
                SubTotalToPrint = record.SubTotalToPrint ?? KindSubTotal.Net,
                Copies = record.Copies,
                ProductGrouping = stand.ProductGrouping,
                FiscalDescription = record.FiscalDescription,
                ShowSignatureOptions = record.ShowSignatureOptions,
                AlternateRoomNumber = record.AlternateRoomNumber,
                AlternateName = record.AlternateName,
                AlternateSignature = record.AlternateSignature,
                PrintDiscountDescription = record.PrintDiscountDescription
            };

            if (ticketSettings.TipSuggestion)
            {
                foreach (var settings in record.TipSuggestions.Select(tipSuggestions => new TipSuggestionSetting
                         {
                             Autogenerate = tipSuggestions.Autogenerate,
                             Description = tipSuggestions.Description,
                             Percent = tipSuggestions.Percent,
                             OverNet = tipSuggestions.OverNet
                         }))
                {
                    ticketSettings.Suggestions.Add(settings);
                }
            }

            if (record.Align.HasValue)
            {
                ticketSettings.LogoHorizontalAlignment = record.Align.Value switch
                {
                    0 => HorizontalAlignment.Left,
                    1 => HorizontalAlignment.Center,
                    2 => HorizontalAlignment.Right,
                    3 => HorizontalAlignment.Stretch,
                    _ => ticketSettings.LogoHorizontalAlignment
                };
            }

            ticketSettings.Country = country;
            return ticketSettings;
        }

        #endregion
    }
}