﻿using System;
using System.Threading.Tasks;
using NewHotel.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.DTOs.Request;

namespace NewHotel.Pos.Hub.Business.Interfaces;

public interface ISignatureBusiness
{
    public ValidationItemResult<Guid> RequestSignature(SignatureRequest request);
    public Task<ValidationItemResult<string>> RetrieveSignature(Guid id);
}