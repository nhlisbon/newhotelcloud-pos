﻿using System;
using NewHotel.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.Records;
using NewHotel.Pos.Hub.Contracts.Common.Records.CurrentAccount;

namespace NewHotel.Pos.Hub.Business.Interfaces
{
    public interface IReservationAccountBusinessCloud
    {
        #region Members

        ValidationItemSource<ReservationRecord> GetReservations(ReservationFilterContract filter);
        ValidationItemResult<PmsReservationRecord> GetPmsReservation(PmsReservationFilterModel filter);
        ValidationItemSource<EventReservationSearchFromExternalRecord> GetEventReservations(Guid? installationId);
        ValidationItemSource<SpaReservationSearchFromExternalRecord> GetSpaReservations(SpaSearchReservationFilterModel filter);
        ValidationItemSource<PmsCompanyRecord> GetEntityInstallations(PmsCompanyFilterModel filter);
        ValidationItemSource<ReservationsGroupsRecord> GetReservationsGroups(Guid? installationId);
        ValidationItemSource<PmsControlAccountRecord> GetControlAccounts(PmsControlAccountFilterModel filter);
        ValidationItemSource<ControlAccountMovementDto> GetControlAccountMovements(ControlAccountMovementsFilterModel filter);
        ValidationItemSource<ClientInstallationRecord> GetClientInstallations(bool openAccounts = true, Guid? clientId = null, string room = null, string fullName = null, Guid? installationId = null);

        ValidationItemSource<MealsControlRecord> GetMealsControl(MealsFilterModel model);
        ValidationResult UpdateMealsControl(Guid guestId, bool breakfast, bool lunch, bool dinner, Guid? installationId = null);

        ValidationItemResult<ClientInfoContract> GetClientInfo(Guid clientId);

        #endregion
    }
}