﻿using System;
using System.Threading.Tasks;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;

namespace NewHotel.Pos.Hub.Business.Interfaces
{
    public interface ISettingManagementBusinessCloud
    {
        #region Members

        ExportOfficialDocumentResult GetExportOfficialDocument(Guid officialDocId);
        ValidationItemResult<POSGeneralSettingsRecord> GetGeneralSettings();
        ValidationItemSource<POSTicketPrintConfigurationRecord> GetTicketPrintConfigurations();
        ValidationItemSource<POSStandRecord> GetStandsFromCloud(int language);
        ValidationContractResult CloseDay(Guid standId, Guid? cajaId, DateTime? closeDay);
        ValidationContractResult UndoCloseDay(Guid standId, Guid? cajaId, DateTime? closeDay);
        ValidationContractResult CloseTurn(Guid standId, Guid cajaId);

        ValidationResult UpdateVersions(string dbVersion, string syncVersion, string appVersion);
        Task<ValidationResult> UpdateVersionsAsync(string dbVersion, string syncVersion, string appVersion);

		#endregion
	}
}