﻿using System;
using NewHotel.Contracts;
using NewHotel.Pos.Hub.Model.WaitingListStand.Contract;
using NewHotel.Pos.Hub.Model.WaitingListStand.Record;

namespace NewHotel.Pos.Hub.Business.Interfaces
{
    public interface IPaxWaitingListBusiness : IDisposable
    {
        #region Members

        ValidationContractResult NewPaxWaitingList();

        ValidationContractResult LoadPaxWaitingList(Guid id);

        ValidationContractResult PersistPaxWaitingList(PaxWaitingListContract contract);

        ValidationResult DeletePaxWaitingList(Guid id);

        ValidationItemSource<PaxWaitingListRecord> GetWaitingList(WaitingListFilterContract filter);

        #endregion
    }
}