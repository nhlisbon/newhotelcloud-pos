﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Business.Interfaces
{
    public interface ILogBusiness
    {
        #region Members
        ValidationResult TraceError(string message);
        ValidationResult TraceError(IEnumerable<Validation> validations);
        #endregion
    }
}
