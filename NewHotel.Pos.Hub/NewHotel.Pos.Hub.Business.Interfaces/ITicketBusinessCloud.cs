﻿using System;
using NewHotel.Contracts;
using NewHotel.Pos.Hub.Model;

namespace NewHotel.Pos.Hub.Business.Interfaces
{
    public interface ITicketBusinessCloud
    {
        #region Members

        ValidationResult ChargeAccount(POSTicketContract ticket);
        ValidationResult CancelTicket(Guid ticketId, Guid? cancellationReasonId, string comments);
        ValidationContractResult LoadTicket(Guid ticketId);
        ValidationContractResult LoadTicketReal(Guid ticketId);
        ValidationResult PersistTicket(POSTicketContract ticket);
        ValidationItemSource<ClosedTicketRecord> GetClosedTickets(QueryRequest request);

        #endregion
    }
}