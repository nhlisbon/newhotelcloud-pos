﻿using System;
using System.Collections.Generic;
using NewHotel.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.Records.Stands;
using NewHotel.Pos.Hub.Model;
using ProductRecord = NewHotel.Pos.Hub.Contracts.Common.Records.Products.ProductRecord;

namespace NewHotel.Pos.Hub.Business.Interfaces
{
    public interface IProductBusiness : IDisposable
    {
        #region Members

        ValidationContractResult CalculateTaxRates(Guid productId, string country, decimal value, Guid schemaId, Guid hotelId, bool included);

        ValidationItemResult<ImageRecord> GetImage(string id);

        List<ProductRecord> GetProductsByStandRate(Guid hotelId, Guid standId, int language, bool base64Image = false, bool autoToTicket = false);

        List<GroupRecord> GetProductGroups(Guid hotelId, Guid standId, int language);

        List<FamilyRecord> GetProductFamilies(Guid hotelId, Guid standId, int language);

        List<SubFamilyRecord> GetProductSubFamilies(Guid hotelId, Guid standId, int language);

        List<PreparationRecord> GetPreparationsByProducts(Guid productId, int language);

        List<PreparationRecord> GetPreparations(int language);

        List<AreasRecord> GetAreasByProducts(Guid standId, Guid productId, int language);

        List<AreasRecord> GetAreasByStand(Guid standId, int language);

        List<StandAreaRecord> GetStandAreas(int language);

        decimal? GetProductPrice(Guid standId, Guid productId, Guid rateId);

        ValidationItemSource<ProductPriceRecord> GetProductPrices(Guid standId, Guid rateId);

        #endregion
    }
}