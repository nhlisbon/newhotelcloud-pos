﻿using System;
using System.Collections.Generic;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos;
using NewHotel.Contracts.Pos.Records;
using NewHotel.Core;
using NewHotel.DataAnnotations;
using NewHotel.Pos.Hub.Contracts.Common.Records;
using NewHotel.Pos.Hub.Contracts.Common.Records.Tickets;
using NewHotel.Pos.Hub.Contracts.Handheld.Contracts;
using NewHotel.Pos.Hub.Contracts.Handheld.DTOs;
using NewHotel.Pos.Hub.Contracts.Handheld.DTOs.Requests;
using NewHotel.Pos.Hub.Contracts.Handheld.Records;
using NewHotel.Pos.Hub.Model;
using DocumentSerieRecord = NewHotel.Pos.Hub.Model.DocumentSerieRecord;
using ProductRecord = NewHotel.Pos.Hub.Contracts.Common.Records.Products;

namespace NewHotel.Pos.Hub.Business.Interfaces
{
    public interface ITicketBusiness : IDisposable
    {
        ValidationResult Sync(Guid ticketId);

        ValidationResult UpdateFiscalPrinterDataAsync(Guid tficketId, string fpSerialInvoice, long? fpInvoiceNumber, string fpSerialCreditNote, long? fpCreditNoteNumber, bool sync = true);

        ValidationResult LoadTaxesData(POSProductLineContract productLine, string country, Guid taxSchemaId, Guid hotelId, bool taxesIncluded, ICanExchangeCurrency canExchangeCurrency, bool isTip = false);

        ValidationContractResult CloseTicket(POSTicketContract ticketContract, ProductRecord.ProductRecord tipProduct = null, bool saveClient = false);

        List<DocumentSerieRecord> GetTicketSerieByStandCashier(Guid standId, Guid cajaId);

        POSTicketPrintConfigurationRecord GetTicketPrintConfiguration(Guid configurationId, long language);

        ExportOfficialDocumentResult GetPdfInvoiceReport(Guid ticketId);

        ValidationContractResult NewTicket(Guid standId, Guid cashierId, Guid hotelId);

        ValidationContractResult NewTicket(
            Guid productId,
            int quantity,
            decimal? manualPrice,
            string manualPriceDescription,
            Guid? tableId,
            Guid[] selectedDetailed,
            Guid[] preparations,
            string description,
            short? paxs,
            ClientRecord? client,
            bool digitalMenu,
            short? seat,
            Guid? separatorId,
            TypedList<POSClientByPositionContract>? seatedClients,
            string observations = null
        );

        public ValidationContractResult NewTicket(NewTicketDto request);

        ValidationContractResult NewTicket(Guid hotelId, Guid standId, Guid cashierId, Guid? tableId, Guid userId, TablesReservationLineContract reservationTableLine = null, string name = null, short? paxs = null);

        ValidationContractResult CreateEmptyTicketToReservationTable(Guid[] tableIds, Guid reservationTableId, string name);

        ValidationContractResult CreateEmptyTicketToPaxWaitingList(Guid tableId, Guid paxWaitingListId);

        ValidationContractResult LoadTicket(Guid ticketId);

        ValidationContractResult MoveTicketToTable(POSTicketContract ticket, Guid? tableId);

        ValidationContractResult LoadTicketForEdition(Guid ticketId, bool forceUnlock);

        ValidationContractResult SaveTicketFromEdition(Guid ticketId, short? paxs = null);

        ValidationContractResult PersistTicket(POSTicketContract contract, bool notifyTicket = true);

        ValidationResult ValidateTicket(POSTicketContract ticket);

        ValidationContractResult PersistAndClose(POSTicketContract contract, bool notifyTicket = true);

        ValidationContractResult PersistTicket(POSTicketContract contract, Guid installationId, bool closeTicket, bool notifyTicket = true);

        ValidationContractResult CancelTicket(POSTicketContract contract, Guid cancellationReasonId, string comments, bool voidTicket, bool fromMerge = false, bool createQuickTicket = false);

        ValidationResult CancelTicket(Guid ticketId, Guid cancellationReasonId, string comments, bool fromMerge = false);

        ValidationContractResult CancelTicketProduct(POSTicketContract contract, Guid productLineId, bool printVoid = false);

        SplitTicketResult SplitTicketManual(Guid ticketId, KeyValuePair<Guid, decimal>[] ordersToLeave);

        SplitTicketResult SplitTicketAuto(Guid ticketId, int quantity);

        SplitTicketResult SplitTicketByAlcoholicGroup(Guid ticketId);

        List<POSTicketContract> GetTicketByStandSaloon(Guid hotelId, Guid cashierId, Guid standId, Guid saloonId);

        List<POSTicketContract> GetTicketByStandCashier(Guid hotelId, Guid standId, Guid cashierId);

        ValidationContractResult PersistProductLine(POSProductLineContract contract, Guid hotelId, Guid taxSchemaId, bool included);

        ValidationContractResult MergeTickets(POSTicketContract orignalTicket, POSTicketContract voidTicket);

        ValidationContractResult CancelClosedTicket(Guid ticketIdObj, Guid cancellationReasonIdObj, string comments, bool voidTicket, bool localObj, bool createQuickTicket);

        ValidationContractResult LoadCloseTicket(Guid ticketId, bool local);

        ValidationContractResult SplitTicketProduct(POSTicketContract ticket, Guid productLineId, int quantity);

        ValidationContractResult ChangeTicketProductDetails(POSTicketContract ticket, Guid productLineId, POSProductLineContract editedLine, bool voidCanceledOrder = false);

        ValidationContractResult DispatchTicketOrders(POSTicketContract ticket, List<Guid> productLineIds);

        ValidationContractResult DispatchTicketOrders(Guid ticketId, DispatchContract[] orders, bool handlePrint = false);

        ValidationContractResult VoidDispatchTicketOrders(POSTicketContract ticket, List<Guid> productLineIds);

        ValidationContractResult VoidDispatchTicketOrders(Guid ticketId, DispatchContract[] orders, bool handlePrint = false, bool cancel = false, bool persist = true);

        ValidationContractResult AwayDispatchTicketOrders(POSTicketContract ticket, List<DispatchContract> productLineIds);

        ValidationContractResult AwayDispatchTicketOrders(Guid ticketId, DispatchContract[] orders, bool handlePrint = false, bool cancel = false, bool persist = true);

        ValidationContractResult CancelTicketProduct(Guid ticketId, Guid productLineId);

        List<TicketInfo> GetTicketInfoByStandCashier(Guid hotelId, Guid standId, Guid cashierId);
        TicketWithProducts GetTicketWithProducts(Guid ticketId);

        ValidationResult PersistentPaymentLine(PaymentLineContract paymentLine);

        List<TicketInfo> GetTicketInfoByTable(Guid tableId, Guid? standId = null, Guid? installationId = null);

        ValidationContractResult SetTableToTicket(Guid ticketId, Guid? tableId);

        // ValidationContractResult AddRoomPlanPayment(Guid ticketId, ReservationSearchFromExternalRecord item);

        // ValidationContractResult AddCreditPayment(Guid ticketId, decimal? amount, ReservationSearchFromExternalRecord item);
        
        // ValidationContractResult AddHouseUsePayment(Guid ticketId, HouseUseRecord item);

        // ValidationContractResult AddPaymentToTicket(Guid ticketId, Guid paymentId, Guid? creditCardId, decimal received, decimal value, bool excessPaymentAsTip, bool close);

        ValidationContractResult ValidateTicketPayments(AddPaymentsRequest request);
        
        ValidationContractResult NewCloseTicket(CloseTicketRequest request);

        ValidationContractResult AddClientPositionToTicket(Guid ticketId, Guid clientId, string clientName, string clientRoom, short pax, string allergies);
        
        ValidationContractResult AddClientPositionToTicket(Guid ticketId, short pax, ClientInstallationRecord client);
        
        ValidationContractResult RemoveClientPositionFromTicket(Guid ticketId, short pax);

        ValidationContractResult AddOrderComment(Guid ticketId, Guid productLineId, string comments);

        ValidationContractResult ChangeTicketProductQuantity(Guid ticketId, Guid productLineId, decimal quantity);

        ValidationContractResult AddOrderPreparation(Guid ticketId, Guid productLineId, Guid preparationId);

        ValidationContractResult RemoveOrderPreparation(Guid ticketId, Guid productLineId, Guid preparationId);

        ValidationContractResult UpdateOrderPreparation(Guid ticketId, Guid productLineId, List<Guid> preparationId); 
        
        ValidationContractResult AddProductLineToTicket(POSTicketContract ticket, POSProductLineContract productLine, bool notifyTicket = true);

        ValidationContractResult CreateLookupTable(Guid ticketId, bool handlePrint = false);

        // ValidationContractResult ResetPaymentTickets(Guid ticketId);

        ValidationStringResult FastCloseTicket(Guid ticketId, bool handlePrint = false);

        ValidationStringResult CloseTicket(Guid ticketId, ClientRecord client, PersonalDocType docTypeSelected, bool save, Blob signature );

        ValidationStringResult FastCloseTicketAsInvoice(Guid ticketId, ClientRecord client, PersonalDocType docTypeSelected, bool saveClient);

        ValidationStringResult CloseTicketAsInvoice(Guid ticketId, ClientRecord client, PersonalDocType docTypeSelected, bool saveClient, Blob signature);
       
        ValidationContractResult AddProductToTicket(POSTicketContract ticket, ProductRecord.ProductRecord product, int quantity, decimal? manualPrice, string manualPriceDesc, Guid[] preparations, bool isTip = false, bool notifyTicket = true, string observations = null, bool persistProductLine = true);

        ValidationContractResult AddProductToTicket(Guid ticketId, Guid productId, int quantity, decimal? manualPrice, string manualPriceDescription, bool isTip, Guid[]? selectedDetailed, Guid[]? preparations, short? seat, Guid? separatorId, string observations);

        ValidationContractResult AddProductToTicketByPlu(Guid ticketId, string productCode, int quantity, decimal? manualPrice, Guid[] preparations, bool isTip = false);

        ValidationResult DeleteProductLine(string productLineId);

        ValidationResult CancelProductLine(string productLineId);

        ValidationContractResult CreateLookupTable(POSTicketContract ticket);

        ValidationResult DeletePaymentLine(string paymentLineId);

        ValidationResult CancelPaymentLine(string paymentLineId);

        ListData GetProductLines(Guid ticketId);

        ValidationContractResult LoadProductLine(Guid productLineId);

        ValidationContractResult UpdateTicketTip(Guid ticketId, decimal? tipPercent, decimal? tipValue);

        ValidationContractResult ApplyTicketDiscounts(Guid ticketId,
            IEnumerable<string> productsId, Guid? discountTypeId,
            decimal? discountPercent);

        ValidationResult UpdateDispatchAreaStatus(Guid ticketId,
            Guid orderId, short orderStatus, long languageId,
            Guid hotelId, Guid cashierId, Guid standId);

        ValidationContractResult CreateMenuDigitalTicket(Guid tableId, ProductLineDto[] productLines);

        ValidationContractResult CreateTicketRoom(string room, string external, ProductLineDto[] productLines);

        ValidationResult<string> NotifyRoomServiceMonitor(POSTicketContract contract);

        ValidationContractResult ChangeProductLinesSeparator(Guid ticketId, IEnumerable<Guid> productLineIds, Guid? separatorId);

        ValidationContractResult ChangeProductLinesSeat(Guid ticketId, IEnumerable<Guid> productLineIds, short seat);

        ValidationContractResult IncrementPrints(Guid ticketId, int title);

        ValidationContractResult CheckOpenTicket(Guid ticketId, Guid standId, Guid cashierId);
        
        ValidationContractResult CreateQuickTicketFromClosedTicket(Guid closedTicketId);
        
        #region Signatures
        
        List<SignatureRecord> GetAllSignatures();
        SignatureRecord GetSignature(Guid ticketId);
        ValidationContractResult AddSignature(SignatureRecord signature);
        ValidationContractResult UpdateSignature(Guid ticketId, SignatureRecord signature);
        ValidationContractResult DeleteSignature(Guid ticketId);

        public ValidationItemResult<Guid> RequestNHSignatureCapture(Guid ticketId);
        public ValidationContractResult RetrieveNHSignatureCapture(Guid ticketId, Guid signatureId);
        public ValidationResult RemoveNHSignatureCapture(Guid signatureId);

        #endregion

        #region Handheld

        ValidationContractResult AttendTableOrder(Guid tableId);

        ValidationContractResult AddGuestToSeat(Guid ticketId, GuestContract client, short pax);

        ValidationContractResult UpdateOrderPreparations(Guid ticketId, ProductLinePreparationContract contract);
        ValidationContractResult UpdateOrderNote(Guid ticketId, ProductLineNoteContract contract);
        ValidationContractResult UpdateTicketInfo(Guid ticketId, short pax, string description);

        ValidationContractResult AddReservationToTicket(Guid ticketId, ReservationRecord reservation);
        ValidationContractResult AddSpaReservationToTicket(Guid ticketId, SpaReservationRecord reservation);

        public ValidationContractResult UpdateClientsByPosition(Guid ticketId, IEnumerable<POSClientByPositionContract> clients);

        #endregion

        #region Digital Menu

        ValidationItemSource<POSProductLineContract> GetTicketProductLines(Guid ticketId, int langId, Guid installationId);

        List<OrderRecord> GetMenuOrdersByTable(Guid standId, Guid tableId, long langId);

        ValidationResult AddMenuOrder(Guid hotelId, Guid standId, Guid saloonId, Guid tableId,
            Guid productId, decimal quantity, IEnumerable<Guid> preparations);

        ValidationResult UpdateMenuOrder(Guid orderId, decimal? quantity, IEnumerable<Guid> preparations);

        ValidationResult DeleteMenuOrder(Guid orderId);

        ValidationResult PlaceMenuOrder(Guid hotelId, Guid standId,Guid saloonId, Guid tableId);
		ValidationContractResult MoveTicketToTable(Guid ticketId, Guid? tableId);
		ValidationContractResult CancelTicketById(Guid ticketId, Guid cancellationReasonId, string comments, bool voidTicket, bool fromMerge = false,  bool createQuickTicket = false);
		ValidationContractResult MergeTicketsById(Guid mergeTicketId, Guid voidTicketId);
		ValidationTicketsResult AcceptTransferedTickets(List<TransferContract> contracts);
		ValidationContractResult AddProductLineToTicket(NewTicketItemModel newTicketItem);

		#endregion
	}
}