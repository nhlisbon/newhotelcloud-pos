﻿using NewHotel.Contracts;
using NewHotel.Pos.Hub.Contracts.PaySystem;
using System;

namespace NewHotel.Pos.Hub.Business.Interfaces
{
	public interface IExternalPaymentEntry
	{
		decimal Amount { get; set; }
		DateTime Date { get; set; }
		string LinkUrl { get; set; }
		Guid? OperationId { get; set; }
		Guid OriginId { get; set; }
		Guid PaymentMethodId { get; set; }
		Guid? ReferencedEntryId { get; set; }
		PaymentOperationStatus Status { get; set; }
		Guid? TerminalId { get; set; }
		Guid Ticket { get; set; }
		Guid? TransactionId { get; set; }
		ActionRequestType Type { get; set; }
		DateTime WorkDate { get; set; }
	}
}