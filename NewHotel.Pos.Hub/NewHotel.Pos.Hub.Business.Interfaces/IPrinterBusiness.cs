﻿using System;
using System.Runtime.Serialization;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using NewHotel.Pos.Hub.Contracts.Handheld.Contracts;

namespace NewHotel.Pos.Hub.Business.Interfaces
{
    public interface IPrinterBusiness  
    {
        ValidationResult PrintOrders(string headerTitle, POSTicketContract ticket, DispatchContract[] orders, bool standPrint);
        ValidationResult PrintTicket(POSTicketContract ticket, Settings settings, bool isInvoice);
    }

    [DataContract]
    public class Settings
    {
        [DataMember]
        public POSTicketPrintConfigurationRecord Print { get; set; }

        [DataMember]
        public POSGeneralSettingsRecord GeneralSettings { get; set; }

        [DataMember]
        public POSStandRecord Stand { get; set; }
    }
}