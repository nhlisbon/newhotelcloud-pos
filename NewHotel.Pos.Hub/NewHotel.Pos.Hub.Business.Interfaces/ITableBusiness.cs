﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Business.Interfaces
{
    public interface ITableBusiness
    {
        #region Members

        ValidationContractResult LoadTable(Guid tableId);

        #endregion
    }
}