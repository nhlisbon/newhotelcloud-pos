﻿using System;
using System.Collections.Generic;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;

namespace NewHotel.Pos.Hub.Business.Interfaces
{
    public interface ICloudBusiness : IReservationTableBusinessCloud, ISettingManagementBusinessCloud, IReservationAccountBusinessCloud, ITicketBusinessCloud
    {
        #region Members

        IEnumerable<Validation> GetBusinessValidations(ValidationResult validationResult);
        IEnumerable<Validation> GetComunicationValidations(ValidationResult validationResult);
        ValidationResult TraceComunicationErrors(ValidationResult validation);
        ValidationResult GetOfflineModeValidations(ValidationResult cloudResult, bool logOnCommunicationError = true);
        ValidationResult UpdateClientCloud(ClientContract clientContract);
        ValidationItemSource<ChargeHotelRecord> GetChargeHotels();
        ValidationContractResult AuditDocument(POSTicketContract ticket);

        ValidationItemResult<SpaServiceRecord> GetSpaServices(Guid? hotelId, Guid spaId);
        ValidationResult CheckInSpaService(Guid spaServiceId);
        ValidationResult CheckOutSpaService(Guid spaServiceId);

        #endregion
    }
}