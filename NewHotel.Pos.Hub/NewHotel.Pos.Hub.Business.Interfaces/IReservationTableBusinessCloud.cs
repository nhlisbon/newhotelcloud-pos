﻿using System;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using NewHotel.Pos.Hub.Model.WaitingListStand.Contract;

namespace NewHotel.Pos.Hub.Business.Interfaces
{
    public interface IReservationTableBusinessCloud : IDisposable
    {
        #region Members

        ValidationItemSource<LiteTablesReservationRecord> GetTableReservations(TablesReservationFilterContract filter);

        ValidationItemSource<StandTimeSlotRecord> GetStandTimeSlots(Guid standId, DateTime date);

        ValidationResult CheckInBookingTable(Guid bookingTableId);

        ValidationResult UndoCheckInBookingTable(Guid bookingTableId);

        ValidationResult CheckOutBookingTable(Guid bookingTableId);

        ValidationResult UndoCheckOutBookingTable(Guid bookingTableId);

        ValidationResult CancelBookingTable(Guid bookingTableId, Guid cancellationReasonId, string comments);

        ValidationResult UndoCancelBookingTable(Guid bookingTableId);

        ValidationContractResult LoadTablesGroup(Guid tablesGroupId);

        ValidationContractResult LoadTablesReservation(Guid reservationTableId);

        ValidationContractResult PersistTablesReservationLine(TablesReservationLineContract line);
        
        ValidationContractResult LoadTableInfo(Guid table);

        #endregion
    }
}
