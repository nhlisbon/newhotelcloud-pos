﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Hub.Model.Cashier.Contracts.Settings;
using HotelRecord = NewHotel.Pos.Hub.Model.HotelRecord;
using TaxSchemaRecord = NewHotel.Pos.Hub.Contracts.Cashier.Records.TaxSchemaRecord;

namespace NewHotel.Pos.Hub.Business.Interfaces
{
    public interface ISettingBusiness : IDisposable
    {
        #region Members
        List<HotelRecord> GetHotels(Guid userId);
        POSGeneralSettingsRecord GetGeneralSettings(Guid hotelId, bool fromCloud = false);
        IEnumerable<TaxSchemaRecord> GetTaxSchemas(Guid hotelId, int language);
        List<CancellationTypesRecord> GetCancellationTypes(Guid hotelId, int language);
        List<CreditCardTypesRecord> GetCreditCardTypes(Guid hotelId, int language);
        List<DiscountTypesRecord> GetDiscountTypes(Guid hotelId, int language);
        List<HouseUseRecord> GetHouseUses(Guid standId, int language);
        List<PaymentMethodsRecord> GetPaymentMethods(Guid hotelId, int language);
        List<SeparatorsRecord> GetSeparators(Guid hotelId, int language);
        List<TicketDutyRecord> GetTicketDuty(Guid hotelId, int language);
        List<CategoriesRecord> GetButtonCategories(Guid hotelId, int language);
        List<SubCategoriesRecord> GetButtonSubCategories(Guid categoryId, int language);
        List<ColumnsPlusProductsRecord> GetColumnsPlusProducts(Guid standId, Guid subCategoryId, int language);
        ValidationResult ResetSyncStatus();
        List<NationalityRecord> GetNationalities(int language);
        List<HappyHourByStandRecord> GetHappyHoursByStand(Guid standId, int language);
        ValidationItemSource<DownloadedImageContact> DownloadImages(KeyValuePair<string, DateTime>[] paths);
		ValidationResult<PaymentMethodsRecord> GetPaymentMethod(Guid hotelId, Guid paymentMethodId, long language);
        Task<ValidationResult<string>> SyncLocalStock();

		#endregion
	}
}