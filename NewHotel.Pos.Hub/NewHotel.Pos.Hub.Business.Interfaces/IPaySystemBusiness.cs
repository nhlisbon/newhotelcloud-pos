﻿using NewHotel.Contracts;
using NewHotel.Payments.Core.Values;
using NewHotel.Pos.Hub.Contracts.PaySystem;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Business.Interfaces
{
	public interface IPaySystemBusiness : IDisposable
	{
		Task<ValidationResult<RequestTerminalsContract>> GetPayTerminals(Guid? originId);
		ValidationResult<TicketExternalPayments> GetTicketExternalPayments(Guid ticketId);
		Task<ValidationResult<ExternalPaymentResponseContract>> RequestPayment(ExternalPaymentRequestContract request, CancellationToken cancellation = default);
		Task<ValidationResult<ExternalPaymentEntryContract>> UpdateDevicePayment(ExternalPaymentDeviceCompleteRequest request, CancellationToken cancellation = default);
		Task<ValidationResult<ExternalPaymentEntryContract>> UpdatePendingPayment(Guid entryId, CancellationToken cancellation = default);
	}
}
