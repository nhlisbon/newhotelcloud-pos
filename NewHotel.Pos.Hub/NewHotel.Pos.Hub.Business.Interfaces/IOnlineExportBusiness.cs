﻿using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Business.Interfaces
{
    public interface IOnlineExportBusiness
    {
        ValidationContractResult Export(POSTicketContract ticket);
    }
}
