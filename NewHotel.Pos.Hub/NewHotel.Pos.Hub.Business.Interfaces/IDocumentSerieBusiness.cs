﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Contracts.Ticket;

namespace NewHotel.Pos.Hub.Business.Interfaces
{
    public interface IDocumentSerieBusiness : IDisposable
    {
        #region Members

        SerialNumberResult GetNextSerialNumber(Guid cajaId, Guid standId, bool justSerie, long docType, bool searchInParent, bool isAltenativeSerie = false);

        SerialNumberResult GetNextAuxiliarCount(Guid cajaId, Guid standId, bool justSerie, long docType, bool searchInParent, bool isAltenativeSerie = false);

        ValidationResult<string> GetSerieValidationCode(string serie);

        #endregion
    }
}
