﻿using System;
using System.Collections.Generic;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using NewHotel.Pos.Hub.Contracts.Common.Contracts;
using NewHotel.Pos.Hub.Contracts.Common.Records;
using NewHotel.Pos.Hub.Contracts.Common.Records.CurrentAccount;
using NewHotel.Pos.Hub.Contracts.Common.Records.Stands;
using NewHotel.Pos.Hub.Contracts.Common.Records.Tickets;
using NewHotel.Pos.Hub.Contracts.Handheld.DTOs;
using NewHotel.Pos.Hub.Model;
using ClosedTicketRecord = NewHotel.Pos.Hub.Model.ClosedTicketRecord;
using SaloonRecord = NewHotel.Pos.Hub.Model.SaloonRecord;
using UserRecord = NewHotel.Pos.Hub.Model.UserRecord;

namespace NewHotel.Pos.Hub.Business.Interfaces
{
    public interface IStandBusiness : IDisposable
    {
        #region Members

        ValidationStringResult LoadImage(string imagePath);
        string GetStandardCurrencyByStand(Guid standId);
        ValidationItemResult<StandEnvironment> GetStandEnvironment(Guid standId, int language,  bool base64Image = false);
        ValidationItemResult<UserRecord> GetUserByName(string loginUser);
        ValidationItemResult<UserRecord> GetUserByCode(string userCode);
        ValidationItemResult<UserRecord> GetUserById(Guid userId);
        List<CashierRecord> GetCashiers(Guid hotelId, Guid userId, int language);
        List<POSStandRecord> GetStands(Guid cajaId, int language);
        List<LiteStandRecord> GetStandsForTransfer(int language);
        ValidationResult PersistentTransfer(TransferContract contract);
        ValidationResult TransferTicket(POSTicketContract ticket, TransferContract transfer);
        ValidationContractResult VerifyTransferToAccept(Guid standId, Guid installationId, int language);
        ValidationContractResult VerifyTransferToReturn(Guid standId, Guid installationId, int language);
        ValidationContractResult AcceptTransferTicket(TransferContract contract, Guid cajaId, Guid userId, Guid hoteId);
        ValidationTicketsResult ReturnTransferedTickets(List<TransferContract> transfers);
        ValidationContractResult ReturnTransferTicket(TransferContract contract);
        List<SaloonRecord> GetSaloonsByStand(Guid standId, int language);
        List<TableRecord> GetTablesBySaloon(Guid saloonId);
        ValidationItemSource<ReservationRecord> GetReservations(ReservationFilterContract filter);
        ValidationItemResult<PmsReservationRecord> GetPmsReservation(PmsReservationFilterModel filter);
        ValidationItemSource<HouseUseRecord> GetHouseUses(string standId);
        ValidationItemSource<EventReservationSearchFromExternalRecord> GetEventReservations(Guid? installationId = null);
        ValidationItemSource<SpaReservationSearchFromExternalRecord> GetSpaReservations(SpaSearchReservationFilterModel filter);
        ValidationItemResult<SpaServiceRecord> GetSpaServices(Guid? installationId, Guid spaId);
        ValidationItemSource<PmsCompanyRecord> GetEntityInstallations(PmsCompanyFilterModel filter);
        ValidationItemSource<ReservationsGroupsRecord> GetReservationsGroups(Guid? installationId = null);
        ValidationItemSource<PmsControlAccountRecord> GetControlAccounts(PmsControlAccountFilterModel filter);
        ValidationItemSource<ControlAccountMovementDto> GetControlAccountMovements(ControlAccountMovementsFilterModel filter);
        ValidationItemSource<ClientInstallationRecord> GetClientInstallations(bool openAccounts = true, Guid? clientId = null, string room = null, string fullName = null, Guid? installationId = null);
        ValidationItemSource<ClosedTicketRecord> GetOldTickets(Guid? standId, Guid? cashierId, DateTime? date);
        List<SalesCashierRecord> GetSalesReport(QueryRequest queryRequest, bool isTurnReport);
        ValidationItemSource<SalePaymentRecord> GetPaymentsReport(QueryRequest queryRequest);
        TicketRContract LoadTicketReal(Guid ticketId);
        ValidationResult UpdateMesa(MesaContract contract);
        ValidationItemSource<Guid> GetOpenTables(Guid standId, Guid? saloonId);
        ValidationItemResult<StandTableInfoDto> GetTablesInfo();

        List<POSStandRecord> GetStandsFromCloud(int language);

        ValidationItemSource<ClientRecord> GetClientEntities(QueryRequest request);

        ValidationItemSource<Guid> CreateClient(string name, string fiscalNumber, string email, string fiscalAddress,
            string country, Guid? clientId = null);

        ValidationBooleanResult CheckUserPermission(Guid userIdObj, Permissions permissionObj);
        ValidationContractResult CloseTurn(Guid standId, Guid cashierId);
        ValidationDateResult UndoCloseDay(Guid standId, Guid cashierId);
        StandTurnDateContract GetStandTurnDate(Guid standId);
        ValidationResult CloseDayValidations(Guid standId, Guid cashierId, DateTime closeDay);
        ValidationContractResult CloseDay(Guid standId, Guid cashierId, DateTime date);

        DateTime GetWorkDateByStand(Guid standId);

        ValidationItemSource<MealsControlRecord> GetMealsControl(MealsFilterModel model);
        ValidationResult UpdateMealsControl(Guid guestId, bool breakfast, bool lunch, bool dinner, Guid? installationId = null);
        
        ValidationItemResult<ClientInfoContract> GetClientInfo(Guid clientId);
        List<ProductInStandRecord> GetProductsByStand(Guid standId, int language);

        #region Handheld

        ValidationItemSource<Guid> GetPendingOrdersTables();

        #endregion

        #region Digital Menu

        ValidationItemResult<StandEnvironment> GetMenuStandEnvironment(Guid hotelId, Guid standId, int langId);
		List<CashierRecord> GetCashiers();

		#endregion

		#endregion
	}
}