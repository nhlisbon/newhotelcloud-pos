﻿//#define NHC
using System;
using System.Text;
using System.Linq;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using NewHotel.Core;
using NewHotel.DataAnnotations;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;

namespace NewHotel.Manager.Oracle
{
    public class OracleDatabaseManager : DatabaseManager
    {
        #region Constants

        private const string LogKey = "NewHotel.Manager.Oracle";

        #if (NHC)
        private const int MinPoolSize = 25;
        private const int MaxPoolSize = 100;
        #else
        private const int MinPoolSize = 1;
        private const int MaxPoolSize = 50;
        #endif
        private const string SystemConnectionStringFormat = "User Id={0};Password={1};Data Source={2};Pooling=false;Connection Timeout=180";
        private const string ConnectionStringFormat = "User Id={0};Password={1};Data Source={2};Min Pool Size={3};Max Pool Size={4};Pooling={5};Connection Timeout=30;Connection Lifetime=120;Incr Pool Size=2;Decr Pool Size=10";

        #endregion
        #region Private

        private static readonly string _commandTextFirstPage;
        private static readonly string _commandTextFirstPage12;
        private static readonly string _commandTextNextPages;
        private static readonly string _commandTextNextPages12;

        private static readonly string _commandTextFirstPageWithColumns;
        private static readonly string _commandTextFirstPageWithColumns12;
        private static readonly string _commandTextNextPagesWithColumns;
        private static readonly string _commandTextNextPagesWithColumns12;

        private OracleConnection _connection;
        private OracleTransaction _transaction;
        private bool _version12 = false;
        private readonly string _drcp;
        private readonly NumberFormatInfo _numberFormat;

        #endregion
        #region Constructors

        private static bool IsPooling(string drcp, string user)
        {
            return string.IsNullOrEmpty(drcp) && !IsSysUser(user);
        }

        private static bool IsSysUser(string user)
        {
            return string.Equals(user, "SYSTEM", StringComparison.InvariantCultureIgnoreCase) ||
                   string.Equals(user, "SYS", StringComparison.InvariantCultureIgnoreCase);
        }

        private static bool IsMgrUser(string user)
        {
            return string.Equals(user, "MGR", StringComparison.InvariantCultureIgnoreCase);
        }

        private OracleDatabaseManager(string connectionString, string module, string drcp, string user, string password, string dataSource)
            : base(connectionString, module, user, password, dataSource)
        {
            _drcp = drcp;
            _numberFormat = (NumberFormatInfo)NumberFormatInfo.InvariantInfo.Clone();
            _numberFormat.NumberDecimalSeparator = ".";
            _numberFormat.NumberDecimalDigits = 2;
        }

        public OracleDatabaseManager(string module, string drcp, string user, string password, string dataSource, byte poolSize)
        #if (NHC)
            : this(IsSysUser(user)
                   ? string.Format(SystemConnectionStringFormat, user, password, dataSource)
                   : string.Format(ConnectionStringFormat, "NHC", "NHC", dataSource, MinPoolSize, MaxPoolSize,
                   poolSize > 0 ? "true" : "false"),
                   module, drcp, user, password, dataSource)
        #else
            : this(string.Format(ConnectionStringFormat, user, password, dataSource,
                   IsMgrUser(user) ? MinPoolSize + 2 : MinPoolSize, MaxPoolSize,
                   IsPooling(drcp, user) ? "true" : "false"),
                   module, drcp, user, password, dataSource)
        #endif
        {
        }

        static OracleDatabaseManager()
        {
            StringBuilder sql;

            sql = new StringBuilder();
            sql.AppendLine("{0}");
            sql.AppendFormat("FETCH FIRST (:{0}) ROWS ONLY", ROWCOUNT);
            _commandTextFirstPage12 = sql.ToString();

            sql = new StringBuilder();
            sql.AppendFormat("SELECT {0}.*, ROWNUM AS {1}", TABLEALIAS, ROWNUM);
            sql.AppendLine();
            sql.AppendLine("FROM (");
            sql.AppendLine("{0}");
            sql.AppendFormat(") {0}", TABLEALIAS);
            sql.AppendLine();
            sql.AppendFormat("WHERE ROWNUM <= :{0}", ENDROW);
            _commandTextFirstPage = sql.ToString();

            sql = new StringBuilder();
            sql.AppendLine("{0}");
            sql.AppendFormat("OFFSET (:{0}) ROWS FETCH NEXT (:{1}) ROWS ONLY", ROWOFFSET, ROWCOUNT);
            _commandTextNextPages12 = sql.ToString();

            sql = new StringBuilder();
            sql.AppendFormat("SELECT {0}__.*", TABLEALIAS);
            sql.AppendLine();
            sql.AppendLine("FROM (");
            sql.AppendFormat("SELECT {0}_.*, ROWNUM AS {1}", TABLEALIAS, ROWNUM);
            sql.AppendLine();
            sql.AppendLine("FROM (");
            sql.AppendLine("{0}");
            sql.AppendFormat(") {0}_", TABLEALIAS);
            sql.AppendLine();
            sql.AppendFormat(") {0}__", TABLEALIAS);
            sql.AppendLine();
            sql.AppendFormat("WHERE {0} >= :{1} AND ROWNUM <= :{2}", ROWNUM, STARTROW, ENDROW);
            _commandTextNextPages = sql.ToString();

            sql = new StringBuilder();
            sql.AppendFormat("SELECT {0}.*,", TABLEALIAS);
            sql.AppendLine();
            sql.AppendLine("{0}");
            sql.AppendLine("FROM (");
            sql.AppendLine("{1}");
            sql.AppendFormat("FETCH FIRST :{0} ROWS ONLY", ROWCOUNT);
            sql.AppendLine();
            sql.AppendFormat(") {0}", TABLEALIAS);
            _commandTextFirstPageWithColumns12 = sql.ToString();

            sql = new StringBuilder();
            sql.AppendFormat("SELECT {0}.*, ROWNUM AS {1},", TABLEALIAS, ROWNUM);
            sql.AppendLine();
            sql.AppendLine("{0}");
            sql.AppendLine("FROM (");
            sql.AppendLine("{1}");
            sql.AppendFormat(") {0}", TABLEALIAS);
            sql.AppendLine();
            sql.AppendFormat("WHERE ROWNUM <= :{0}", ENDROW);
            _commandTextFirstPageWithColumns = sql.ToString();

            sql = new StringBuilder();
            sql.AppendFormat("SELECT {0}.*,", TABLEALIAS);
            sql.AppendLine();
            sql.AppendLine("{0}");
            sql.AppendLine("FROM (");
            sql.AppendLine("{1}");
            sql.AppendFormat("OFFSET (:{0}) ROWS FETCH NEXT (:{1}) ROWS ONLY", ROWOFFSET, ROWCOUNT);
            sql.AppendLine();
            sql.AppendFormat(") {0}", TABLEALIAS);
            _commandTextNextPagesWithColumns12 = sql.ToString();

            sql = new StringBuilder();
            sql.AppendFormat("SELECT {0}__.*,", TABLEALIAS);
            sql.AppendLine();
            sql.AppendLine("{0}");
            sql.AppendLine("FROM (");
            sql.AppendFormat("SELECT {0}_.*, ROWNUM AS {1}", TABLEALIAS, ROWNUM);
            sql.AppendLine();
            sql.AppendLine("FROM (");
            sql.AppendLine("{1}");
            sql.AppendFormat(") {0}", TABLEALIAS);
            sql.AppendLine();
            sql.AppendFormat(") {0}__", TABLEALIAS);
            sql.AppendLine();
            sql.AppendFormat("WHERE {0} >= :{1} AND ROWNUM <= :{2}", ROWNUM, STARTROW, ENDROW);
            _commandTextNextPagesWithColumns = sql.ToString();
        }

        #endregion

        public override string StatementLock
        {
            get { return " FOR UPDATE"; }
        }

        public override string TableLock
        {
            get { return string.Empty; }
        }

        #region Type Conversion

        private static Type InternalGetTypeFromMetadata(Type type, string dataTypeName, object[] metadata, out bool isNullable)
        {
            // ColumnSize
            var size = (int)metadata[2];

            // NumericPrecision
            var numPrecision = metadata[3];
            short precision = 0;
            if (numPrecision != DBNull.Value)
                precision = (short)numPrecision;

            // NumericScale
            var numScale = metadata[4];
            short scale = 0;
            if (numScale != DBNull.Value)
                scale = (short)numScale;

            // AllowDBNull
            var allowDBNull = metadata[13];
            isNullable = true;
            if (allowDBNull != DBNull.Value)
                isNullable = (bool)allowDBNull;

            switch (dataTypeName)
            {
                case "RAW":
                    if (size == 16)
                        type = typeof(Guid);
                    break;
                case "CHAR":
                    type = typeof(bool);
                    break;
                case "NUMBER":
                    if (scale == 0 && precision > 0)
                    {
                        if (precision <= 5)
                            type = typeof(short);
                        else
                            type = typeof(long);
                    }
                    else
                        type = typeof(decimal);
                    break;
                case "BLOB":
                    type = typeof(Blob);
                    break;
                case "CLOB":
                    type = typeof(Clob);
                    break;
            }

            return type;
        }

        public override Type GetTypeFromMetadata(Type type, string dataTypeName, object[] metadata, out bool isNullable)
        {
            return InternalGetTypeFromMetadata(type, dataTypeName, metadata, out isNullable);
        }

        public override object ConvertValueType(object value)
        {
            if (value != null)
            {
                var type = value.GetType();
                if (type == typeof(Guid))
                {
                    var guid = (Guid)value;
                    return guid.ToByteArray();
                }
                else
                    return base.ConvertValueType(value);
            }

            return value;
        }

        #endregion

        protected override IDbConnection Connection
        {
            get
            {
                if (_connection == null)
                {
                    _connection = new OracleConnection(ConnectionString);
                    if (!string.IsNullOrEmpty(_drcp))
                        _connection.DRCPConnectionClass = _drcp;
                    Log.Source[MANAGERLOGKEY].TraceInformation("Connection created: {0}", ToString());
                }

                return _connection;
            }
        }

        protected override void CreateTransaction()
        {
            _transaction = _connection.BeginTransaction(IsolationLevel.ReadCommitted);
        }

        protected override void ClearTransaction(bool dispose)
        {
            if (_transaction != null)
            {
                _transaction.Dispose();
                _transaction = null;
            }

            base.ClearTransaction(dispose);
        }

        public override IDbTransaction Transaction
        {
            get { return _transaction; }
        }

        public override bool IsConnected
        {
            get
            {
                if (_connection != null)
                    return _connection.State == ConnectionState.Open;

                return false;
            }
        }

        protected override void ClearConnection(bool dispose)
        {
            ClearTransaction(dispose);
            if (_connection != null)
            {
                if (dispose)
                {
                    Log.Source[MANAGERLOGKEY].TraceInformation("Connection disposed: {0}", ToString());
                    _connection.Dispose();
                }

                _connection = null;
            }
        }

        private bool UpdateSessionInfo(OracleGlobalization globalization)
        {
            const string comparison = "LINGUISTIC";
            const string sort = "GENERIC_BASELETTER";
            const string lengthSemantics = "CHAR";

            if (globalization.Comparison != comparison || globalization.Sort != sort || globalization.LengthSemantics != lengthSemantics)
            {
                globalization.Comparison = comparison;
                globalization.Sort = sort;
                globalization.LengthSemantics = lengthSemantics;
                return true;
            }

            return false;
        }

        protected override void AfterOpenConnection()
        {
            if (_connection != null && _connection.State == ConnectionState.Open)
            {
                _connection.ModuleName = _moduleName;
                int serverVersion;
                if (!string.IsNullOrEmpty(_connection.ServerVersion))
                {
                    var version = _connection.ServerVersion.Substring(0, _connection.ServerVersion.IndexOf('.'));
                    if (int.TryParse(version, out serverVersion))
                        _version12 = serverVersion >= 12;
                }

                var globalization = _connection.GetSessionInfo();
                if (UpdateSessionInfo(globalization))
                    _connection.SetSessionInfo(globalization);

                #if (NHC)
                using (var command = _connection.CreateCommand())
                {
                    command.CommandText = string.Format("ALTER SESSION SET CURRENT_SCHEMA={0}", User);
                    command.ExecuteNonQuery();
                }
                #endif
            }
        }

        private IDbCommand CreateCommand(string commandName, int pageNumber, short pageSize, int arrayBindCount, string commandLabel)
        {
            var command = new OracleDbCommand(_connection, pageSize, commandName, commandLabel);
            command.ArrayBindCount = arrayBindCount;
            command.Transaction = Transaction;

            if (pageNumber > 0)
            {
                IDbDataParameter parameter;
                if (pageNumber > 1)
                {
                    parameter = CreateParameter(command);
                    parameter.Direction = ParameterDirection.Input;

                    var pageOffset = (pageNumber - 1) * pageSize;
                    if (_version12)
                        parameter.ParameterName = RowOffsetParamName;
                    else
                    {
                        parameter.ParameterName = StartRowParamName;
                        pageOffset++;
                    }
                    parameter.Value = pageOffset;
                }

                parameter = CreateParameter(command);
                parameter.Direction = ParameterDirection.Input;
                if (_version12)
                    parameter.ParameterName = RowCountParamName;
                else
                    parameter.ParameterName = EndRowParamName;
                parameter.Value = pageSize;
            }

            return command;
        }

        public override IDbCommand CreateCommand(string commandName, int pageNumber, short pageSize, string commandLabel = null)
        {
            return CreateCommand(commandName, pageNumber, pageSize, 0, commandLabel);
        }

        public override IDbCommand CreateCommand(string commandName, int arrayBindCount, string commandLabel = null)
        {
            return CreateCommand(commandName, 0, 0, arrayBindCount, commandLabel);
        }

        public override IDbCommand CreateCommand(string commandName, string commandLabel = null)
        {
            return CreateCommand(commandName, 0, 0, commandLabel);
        }

        public override void ExecuteDataSet(IDbCommand command, DataSet dataSet, string tableName)
        {
            var adapter = new OracleDataAdapter();
            IDbDataAdapter dataAdapter = adapter;
            dataAdapter.SelectCommand = command;
            adapter.Fill(dataSet, tableName);
        }

        #region Connection Class
        /*
        private class OracleDbConnection : IDbConnection, IDbTransaction
        {
            #region Members

            private OracleConnection _connection;
            private OracleTransaction _transaction;
            private bool _disposed = false;
            private string _connectionString;

            #endregion
            #region Properties

            private OracleConnection GetConnection()
            {
                if (_connection == null)
                    _connection = new OracleConnection(_connectionString);

                return _connection;
            }

            private IDbConnection Connection
            {
                get
                {
                    if (_connection == null)
                        GetConnection();

                    return _connection;
                }
            }

            private IDbTransaction Transaction
            {
                get
                {
                    if (_transaction != null)
                        return _transaction;

                    throw new InvalidOperationException("Not in transaction.");
                }
            }

            #endregion
            #region Constructor

            public OracleDbConnection(string connectionString)
            {
                _connectionString = connectionString;
            }

            #endregion
            #region IDbConnection Members

            public IDbTransaction BeginTransaction(IsolationLevel isolationLevel)
            {
                if (_transaction != null)
                {
                    var connection = GetConnection();
                    _transaction = connection.BeginTransaction(isolationLevel);
                }

                return _transaction;
            }

            public IDbTransaction BeginTransaction()
            {
                if (_transaction != null)
                {
                    var connection = GetConnection();
                    _transaction = connection.BeginTransaction();
                }

                return _transaction;
            }

            public void ChangeDatabase(string databaseName)
            {
                Connection.ChangeDatabase(databaseName);
            }

            public string ConnectionString
            {
                get { return _connectionString; }
                set 
                { 
                    _connectionString = value;
                    if (_connection != null)
                        Connection.ConnectionString = _connectionString;
                }
            }

            public int ConnectionTimeout
            {
                get { return Connection.ConnectionTimeout; }
            }

            public IDbCommand CreateCommand()
            {
                var connection = GetConnection();
                var command = connection.CreateCommand();

                command.BindByName = true;
                if (_transaction != null)
                    command.Transaction = _transaction;
                return command;
            }

            public string Database
            {
                get { return Connection.Database; }
            }

            public void Open()
            {
                Connection.Open();
            }

            public void Close()
            {
                Connection.Close();
                Dispose();
            }

            public ConnectionState State
            {
                get { return Connection.State; }
            }

            #endregion
            #region IDbTransaction Members

            IDbConnection IDbTransaction.Connection
            {
                get { return Transaction.Connection; }
            }

            public IsolationLevel IsolationLevel
            {
                get { return Transaction.IsolationLevel; }
            }

            public void Commit()
            {
                Transaction.Commit();
            }

            public void Rollback()
            {
                Transaction.Rollback();
            }

            #endregion
            #region IDisposable Members

            public void Dispose()
            {
                if (!_disposed)
                {
                    _disposed = true;
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }
                    if (_connection != null)
                    {
                        _connection.Dispose();
                        _connection = null;
                    }
                }
            }

            #endregion
        }
        */
        #endregion

        #region Parameters Collection Class

        private class OracleDbParameterCollection : List<OracleDbParameter>, IDataParameterCollection
        {
            private OracleDbParameter GetParameter(string parameterName)
            {
                return this.FirstOrDefault(x => string.Compare(x.ParameterName, parameterName, true) == 0);
            }

            public bool Contains(string parameterName)
            {
                return GetParameter(parameterName) != null;
            }

            public int IndexOf(string parameterName)
            {
                return this.FindIndex(x => string.Compare(x.ParameterName, parameterName, true) == 0);
            }

            public void RemoveAt(string parameterName)
            {
                var index = IndexOf(parameterName);
                if (index >= 0)
                    this.RemoveAt(index);
            }

            public object this[string parameterName]
            {
                get { return GetParameter(parameterName); }
                set
                {
                    var index = IndexOf(parameterName);
                    if (index >= 0)
                    {
                        var parameter = value as OracleDbParameter;
                        if (parameter != null)
                            this[index] = parameter;
                        else
                            throw new ArgumentException("Expected OracleDbParameter", "value");
                    }
                }
            }
        }

        #endregion

        #region Command Class

        private class OracleDbCommand : IDbCommand
        {
            #region Constants

            private const string ArrayParamPrefix = "NH___";

            #endregion
            #region Members

            private readonly short _pageSize = 0;
            private OracleConnection _connection;
            private OracleTransaction _transaction;
            private int _arrayBindCount;
            private int _commandTimeout = 0;
            private CommandType _commandType = CommandType.Text;
            private UpdateRowSource _updateRowSource = UpdateRowSource.None;
            private bool _prepare = false;
            private bool _disposed = false;

            private string _commandName = string.Empty;
            private string _commandLabel;
            private StringBuilder _commandText = new StringBuilder();
            private OracleDbParameterCollection _parameters = new OracleDbParameterCollection();

            #endregion
            #region Constructor

            public OracleDbCommand(OracleConnection connection, short pageSize, string commandName, string commandLabel)
            {
                _connection = connection;
                _pageSize = pageSize;
                _commandName = ReplaceDiacritics(commandName);
                _commandLabel = ReplaceDiacritics(commandLabel);
            }

            public OracleDbCommand(OracleConnection connection, string commandName, string commandLabel)
                : this(connection, 0, commandName, commandLabel) { }

            #endregion
            #region Properties

            public int ArrayBindCount
            {
                get { return _arrayBindCount; }
                set { _arrayBindCount = value; }
            }

            #endregion
            #region Private Methods

            private static string ReplaceDiacritics(string str)
            {
                var result = str;
                if (!string.IsNullOrEmpty(str))
                {
                    var source = str.Normalize(NormalizationForm.FormD);
                    var index = 0;

                    var buffer = new char[str.Length];
                    for (int i = 0; i < source.Length; i++)
                    {
                        var ch = source[i];
                        if (CharUnicodeInfo.GetUnicodeCategory(ch) != UnicodeCategory.NonSpacingMark)
                            buffer[index++] = ch;
                    }

                    if (buffer.Length != index)
                        Array.Resize(ref buffer, index);

                    result = new string(buffer).Normalize(NormalizationForm.FormC);
                }

                return result;
            }

            private void SetConnectionInfo()
            {
                if (_connection != null)
                {
                    _connection.ActionName = _commandName;
                    _connection.ClientInfo = _commandLabel;
                }
            }

            private void ClearConnectionInfo()
            {
                if (_connection != null)
                {
                    _connection.ActionName = null;
                    _connection.ClientInfo = null;
                }
            }

            private OracleCommand GetCommand()
            {
                var command = _connection.CreateCommand();
                command.ArrayBindCount = _arrayBindCount;
                command.Transaction = _transaction;
                command.CommandType = _commandType;
                command.CommandTimeout = _commandTimeout;
                command.UpdatedRowSource = _updateRowSource;
                command.BindByName = true;

                var commandText = _commandText.Replace(Environment.NewLine, "\n");

                var plTables = new List<Tuple<string, byte[]>>();

                for (int i = 0; i < _parameters.Count; i++)
                {
                    var parameter = _parameters[i];
                    var paramName = parameter.ParameterName;
                    if (paramName.StartsWith(ArrayParamPrefix, StringComparison.CurrentCultureIgnoreCase))
                    {
                        var values = parameter.Value as IEnumerable;
                        if (values != null)
                        {
                            var array = values.Cast<Guid>().ToArray();
                            var bytes = new byte[array.Length * 16];
                            for (int index = 0; index < array.Length; index++)
                                Array.Copy(array[index].ToByteArray(), 0, bytes, index * 16, 16);
                            plTables.Add(Tuple.Create(paramName, bytes));
                        }
                    }
                    else
                    {
                        var param = command.CreateParameter();
                        param.Direction = parameter.Direction;
                        param.ParameterName = paramName;
                        if (parameter.IsTimestamp)
                            param.OracleDbType = OracleDbType.TimeStamp;
                        param.Value = parameter.Value;
                        command.Parameters.Add(param);
                    }
                }

                if (plTables.Count > 0)
                {
                    foreach (var plTable in plTables)
                    {
                        var param = command.CreateParameter();
                        param.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                        param.ParameterName = ArrayParamPrefix + plTable.Item1;
                        param.Size = plTable.Item2.Length / 16;
                        var arrayBindSize = new int[param.Size];
                        var arrayValues = new byte[param.Size][];
                        var bytes = new byte[16];
                        for (int index = 0; index < arrayBindSize.Length; index++)
                        {
                            Array.Copy(plTable.Item2, index * 16, bytes, 0, 16);
                            arrayValues[index] = bytes;
                            arrayBindSize[index] = 16;
                        }
                        param.ArrayBindSize = arrayBindSize;
                        param.Value = arrayValues;
                        command.Parameters.Add(param);
                    }
                }
                else
                    command.CommandText = commandText.ToString();

                if (_prepare)
                    command.Prepare();

                return command;
            }

            private void DisposeCommand(OracleCommand command, OracleDbParameterCollection parameters)
            {
                ClearConnectionInfo();
                try
                {
                    for (int i = 0; i < parameters.Count; i++)
                    {
                        var param = command.Parameters[i];
                        if (param.Direction != ParameterDirection.Input &&
                            param.OracleDbType != OracleDbType.RefCursor)
                            parameters[i].Value = param.Value;

                        param.Dispose();
                    }
                }
                finally
                {
                    command.Parameters.Clear();
                    command.Transaction = null;
                    command.Connection = null;
                }
            }

            #endregion
            #region Public Methods

            public OracleDbParameter CreateParameter(string name)
            {
                var param = new OracleDbParameter();
                param.ParameterName = name;
                _parameters.Add(param);

                return param;
            }

            public OracleDbParameter CreateParameter(string name, OracleDbType dbType)
            {
                var param = new OracleDbParameter(name, dbType);
                _parameters.Add(param);

                return param;
            }

            #endregion
            #region IDbCommand Members

            public void Cancel()
            {
                throw new NotImplementedException();
            }

            public string CommandText
            {
                get { return _commandText.ToString(); }
                set
                {
                    _commandText.Clear();
                    if (!string.IsNullOrEmpty(value))
                        _commandText.Append(value.TrimStart());
                }
            }

            public int CommandTimeout
            {
                get { return _commandTimeout; }
                set { _commandTimeout = value; }
            }

            public CommandType CommandType
            {
                get { return _commandType; }
                set { _commandType = value; }
            }

            public UpdateRowSource UpdatedRowSource
            {
                get { return _updateRowSource; }
                set { _updateRowSource = value; }
            }

            public IDbConnection Connection
            {
                get { return _connection; }
                set
                {
                    if (value != null)
                    {
                        var connection = value as OracleConnection;
                        if (connection != null)
                            _connection = connection;
                        else
                            throw new ArgumentException("Expected OracleConnection", "value");
                    }
                    else
                        _connection = null;
                }
            }

            public IDbTransaction Transaction
            {
                get { return _transaction; }
                set
                {
                    if (value != null)
                    {
                        var transaction = value as OracleTransaction;
                        if (transaction != null)
                            _transaction = transaction;
                        else
                            throw new ArgumentException("Expected OracleTransaction", "value");
                    }
                    else
                        _transaction = null;
                }
            }

            public IDbDataParameter CreateParameter()
            {
                var param = new OracleDbParameter();
                _parameters.Add(param);

                return param;
            }

            public int ExecuteNonQuery()
            {
                SetConnectionInfo();
                using (var command = GetCommand())
                {
                    try
                    {
                        return command.ExecuteNonQuery();
                    }
                    finally
                    {
                        DisposeCommand(command, _parameters);
                        _prepare = false;
                    }
                }
            }

            public object ExecuteScalar()
            {
                SetConnectionInfo();
                using (var command = GetCommand())
                {
                    try
                    {
                        return command.ExecuteScalar();
                    }
                    finally
                    {
                        DisposeCommand(command, _parameters);
                        _prepare = false;
                    }
                }
            }

            public IDataReader ExecuteReader(CommandBehavior behavior)
            {
                SetConnectionInfo();
                using (var command = GetCommand())
                {
                    try
                    {
                        var reader = command.ExecuteReader(behavior);
                        long fetchSize = 0;
                        if (reader.RowSize > 0 && _pageSize > 0)
                        {
                            var rowSize = reader.RowSize;
                            long maxPageSize = 1024 * 1024 * 8 / rowSize;
                            var pageSize = Math.Min(_pageSize, maxPageSize);
                            fetchSize = rowSize * pageSize;
                        }

                        return new OracleDbReader(reader, fetchSize);
                    }
                    finally
                    {
                        DisposeCommand(command, _parameters);
                        _prepare = false;
                    }
                }
            }

            public IDataReader ExecuteReader()
            {
                return ExecuteReader(CommandBehavior.SingleResult);
            }

            public IDataParameterCollection Parameters
            {
                get { return _parameters; }
            }

            public void Prepare()
            {
                _prepare = true;
            }

            public override string ToString()
            {
                return _commandName;
            }

            #endregion
            #region IDisposable Members

            public void Dispose()
            {
                if (!_disposed)
                {
                    _disposed = true;
                    _parameters = null;
                    _transaction = null;
                    _connection = null;
                }
            }

            #endregion
        }

        #endregion

        #region Parameter Class

        private class OracleDbParameter : IDbParameter, IDisposable
        {
            #region Members

            private bool _disposed;
            private OracleParameter _parameter;
            private object _value;

            #endregion
            #region Properties

            private IDbDataParameter Parameter
            {
                get { return _parameter; }
            }

            #endregion
            #region Constructor

            public OracleDbParameter()
            {
                _parameter = new OracleParameter();
            }

            public OracleDbParameter(string name, OracleDbType oracleDbType)
            {
                _parameter = new OracleParameter(name, oracleDbType);
            }

            #endregion
            #region IDbDataParameter Members

            public byte Precision
            {
                get { return Parameter.Precision; }
                set { Parameter.Precision = value; }
            }

            public byte Scale
            {
                get { return Parameter.Scale; }
                set { Parameter.Scale = value; }
            }

            public int Size
            {
                get { return _parameter.Size; }
                set { _parameter.Size = value; }
            }

            #endregion
            #region IDataParameter Members

            public DbType DbType
            {
                get { return Parameter.DbType; }
                set { Parameter.DbType = value; }
            }

            public ParameterDirection Direction
            {
                get { return Parameter.Direction; }
                set { Parameter.Direction = value; }
            }

            public bool IsNullable
            {
                get { return Parameter.IsNullable; }
            }

            public string ParameterName
            {
                get { return Parameter.ParameterName; }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        Parameter.ParameterName = value.Trim().ToUpperInvariant();
                    else
                        Parameter.ParameterName = value;
                }
            }

            public string SourceColumn
            {
                get { return Parameter.SourceColumn; }
                set { Parameter.SourceColumn = value; }
            }

            public DataRowVersion SourceVersion
            {
                get { return Parameter.SourceVersion; }
                set { Parameter.SourceVersion = value; }
            }

            public virtual object Value
            {
                get
                {
                    if (_value != null)
                        return _value;

                    return Parameter.Value;
                }
                set
                {
                    if (value != null)
                    {
                        var type = value.GetType();
                        if (type.Equals(typeof(Guid)))
                            Parameter.Value = ((Guid)value).ToByteArray();
                        else if (type.Equals(typeof(object[])))
                        {
                            Parameter.Value = DBNull.Value;
                            Parameter.DbType = DbType.String;
                            _value = value;
                        }
                        else
                        {
                            if (type.Equals(typeof(char)))
                                _parameter.OracleDbType = OracleDbType.Char;
                            Parameter.Value = value;
                        }
                    }
                    else
                        Parameter.Value = DBNull.Value;
                }
            }

            #endregion
            #region IDbParameter

            public bool IsTimestamp
            {
                get
                {
                    return _parameter.OracleDbType == OracleDbType.TimeStamp;
                }
                set
                {
                    if (_parameter.DbType == System.Data.DbType.DateTime)
                        _parameter.OracleDbType = OracleDbType.TimeStamp;
                }
            }

            #endregion
            #region IDisposable Members

            public void Dispose()
            {
                if (!_disposed)
                {
                    _disposed = true;
                    _parameter.Dispose();
                    _parameter = null;
                }
            }

            #endregion
        }

        #endregion

        #region Reader Class

        private class OracleDbReader : IDataReader
        {
            #region Constants

            private const int MaxPrecision = 26;

            #endregion
            #region Members

            private bool _disposed = false;
            private OracleDataReader _reader;
            private DataTable _schema;

            #endregion
            #region Constructor

            public OracleDbReader(OracleDataReader reader, long fetchSize)
            {
                if (reader == null)
                    throw new ArgumentNullException("reader");

                _reader = reader;
                if (fetchSize > 0)
                    _reader.FetchSize = fetchSize;
            }

            #endregion
            #region Properties

            private OracleDataReader Reader
            {
                get
                {
                    if (_reader == null)
                        throw new ObjectDisposedException("reader");

                    return _reader;
                }
            }

            #endregion
            #region IDataReader Members

            public void Close()
            {
                if (_reader != null && !_reader.IsClosed)
                    _reader.Close();
                _reader = null;
                _schema = null;
            }

            public int Depth
            {
                get { return Reader.Depth; }
            }

            public DataTable GetSchemaTable()
            {
                if (_schema == null)
                {
                    try
                    {
                        _schema = Reader.GetSchemaTable();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Reader error reading schema table", ex);
                    }
                }

                return _schema;
            }

            public bool IsClosed
            {
                get { return Reader.IsClosed; }
            }

            public bool NextResult()
            {
                return Reader.NextResult();
            }

            public bool Read()
            {
                return Reader.Read();
            }

            public int RecordsAffected
            {
                get { return Reader.RecordsAffected; }
            }

            #endregion
            #region IDataRecord Members

            public int FieldCount
            {
                get { return Reader.FieldCount; }
            }

            public Type GetFieldType(int i)
            {
                return Reader.GetFieldType(i);
            }

            public string GetDataTypeName(int i)
            {
                return Reader.GetDataTypeName(i);
            }

            public bool GetBoolean(int i)
            {
                var obj = Reader.GetValue(i);
                return obj == null ? false : obj.ToString().StartsWith("1");
            }

            public byte GetByte(int i)
            {
                return decimal.ToByte(GetDecimal(i));
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                return Reader.GetBytes(i, fieldOffset, buffer, bufferoffset, length);
            }

            public char GetChar(int i)
            {
                return Reader.GetChar(i);
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                return Reader.GetChars(i, fieldoffset, buffer, bufferoffset, length);
            }

            public DateTime GetDateTime(int i)
            {
                return Reader.GetDateTime(i);
            }

            public decimal GetDecimal(int i)
            {
                var oraDecimal = Reader.GetOracleDecimal(i);
                try
                {
                    return oraDecimal.Value;
                }
                catch (OverflowException)
                {
                    return AdjustToDecimalPrecision(oraDecimal);
                }
            }

            public double GetDouble(int i)
            {
                return decimal.ToDouble(GetDecimal(i));
            }

            public float GetFloat(int i)
            {
                return decimal.ToSingle(GetDecimal(i));
            }

            public Guid GetGuid(int i)
            {
                var buffer = new byte[16];
                Reader.GetBytes(i, 0, buffer, 0, 16);
                return new Guid(buffer);
            }

            public short GetInt16(int i)
            {
                return decimal.ToInt16(GetDecimal(i));
            }

            public int GetInt32(int i)
            {
                return decimal.ToInt32(GetDecimal(i));
            }

            public long GetInt64(int i)
            {
                return decimal.ToInt64(GetDecimal(i));
            }

            public string GetString(int i)
            {
                return Reader.IsDBNull(i) ? null : Reader.GetString(i);
            }

            public object GetValue(int i)
            {
                return this[i];
            }

            public int GetValues(object[] values)
            {
                return Reader.GetValues(values);
            }

            public bool IsDBNull(int i)
            {
                return Reader.IsDBNull(i);
            }

            public IDataReader GetData(int i)
            {
                return Reader.GetData(i);
            }

            public object this[string name]
            {
                get { return this[GetOrdinal(name)]; }
            }

            public object this[int i]
            {
                get
                {
                    if (GetFieldType(i) == typeof(Decimal))
                    {
                        if (IsDBNull(i))
                            return DBNull.Value;
                        else
                            return GetDecimal(i);
                    }
                    else
                        return Reader[i];
                }
            }

            public string GetName(int i)
            {
                return Reader.GetName(i);
            }

            public int GetOrdinal(string name)
            {
                return Reader.GetOrdinal(name);
            }

            #endregion
            #region Private Methods

            private static decimal AdjustToDecimalPrecision(OracleDecimal oraDecimal)
            {
                return OracleDecimal.SetPrecision(oraDecimal, MaxPrecision).Value;
            }

            #endregion
            #region IDisposable Members

            public void Dispose()
            {
                if (!_disposed)
                {
                    _disposed = true;
                    if (_reader != null)
                    {
                        _reader.Dispose();
                        _reader = null;
                    }
                }
            }

            #endregion
        }

        #endregion

        public override IDbParameter CreateParameter(IDbCommand command)
        {
            return (IDbParameter)command.CreateParameter();
        }

        public override IDbParameter CreateParameter(IDbCommand command, string paramName, object paramValue, DbType dbType, ParameterDirection direction)
        {
            var param = CreateParameter(command);
            param.ParameterName = paramName;
            param.Direction = direction;
            param.DbType = dbType;
            param.Value = paramValue;

            return param;
        }

        public override string GetCommandText(string commandText, string commandCols)
        {
            if (string.IsNullOrEmpty(commandCols))
                return commandText;

            return string.Format("SELECT {0}.*,\r\n{2} FROM (\r\n{1}\r\n) {0}",
                TableAliasParamName, commandText, commandCols);
        }

        public override string GetCountCommandText(string commandText)
        {
            return string.Format("SELECT COUNT(1) FROM\r\n({0})", commandText);
        }

        public override string GetPagedCommandText(string commandText, string commandCols, int pageNumber, short pageSize)
        {
            if (pageNumber == 1)
            {
                if (string.IsNullOrEmpty(commandCols))
                {
                    if (_version12)
                        return string.Format(_commandTextFirstPage12, commandText.TrimEnd());
                    else
                        return string.Format(_commandTextFirstPage, commandText.Trim());
                }
                else
                {
                    if (_version12)
                        return string.Format(_commandTextFirstPageWithColumns12, commandCols, commandText.Trim());
                    else
                        return string.Format(_commandTextFirstPageWithColumns, commandCols, commandText.Trim());
                }
            }
            else if (pageNumber > 1)
            {
                if (string.IsNullOrEmpty(commandCols))
                {
                    if (_version12)
                        return string.Format(_commandTextNextPages12, commandText.TrimEnd());
                    else
                        return string.Format(_commandTextNextPages, commandText.Trim());
                }
                else
                {
                    if (_version12)
                        return string.Format(_commandTextNextPagesWithColumns12, commandCols, commandText.Trim());
                    else
                        return string.Format(_commandTextNextPagesWithColumns, commandCols, commandText.Trim());
                }
            }

            return commandText;
        }

        public override string GetUnionCommandText(string commandText1, string commandText2)
        {
            return string.Format("SELECT * FROM (\r\n{0})\r\nUNION ALL\r\n{1}", commandText1, commandText2);
        }

        private class OracleSqlQueryBuilder : SqlQueryBuilder
        {
            private readonly bool _version12;

            public OracleSqlQueryBuilder(bool version12) : base()
            {
                _version12 = version12;
            }

            protected override string GetPagedStatement(string query)
            {
                if (_version12)
                    return string.Format("{0}\r\nOFFSET (:{1}) ROWS FETCH NEXT (:{2}) ROWS", query, ROWOFFSET, ROWCOUNT);
                else
                {
                    var sb = new StringBuilder();

                    sb.AppendLine("SELECT * FROM (SELECT {3}.*, ROWNUM AS {0} FROM (");
                    sb.Append(query);
                    sb.AppendLine(") {3} WHERE ROWNUM <= :{2}) WHERE {1} > :{1}");

                    return string.Format(sb.ToString(), ROWNUM, STARTROW, ENDROW, TABLEALIAS);
                }
            }
        }

        #region Query Statement

        protected sealed class OracleQueryStatement : QueryStatement
        {
            private const string SelectToken = "SELECT";
            private const string FromToken = "FROM";
            private const string OnToken = "ON";
            private const string WhereToken = "WHERE";
            private const string GroupByToken = "GROUP BY";
            private const string OrderByToken = "ORDER BY";

            public override string ToString()
            {
                var sql = new StringBuilder();

                if (!Select.IsEmpty)
                {
                    sql.AppendLine(SelectToken);
                    sql.Append(Select.ToString());
                }
                if (!From.IsEmpty)
                {
                    sql.AppendLine(FromToken);
                    sql.Append(From.ToString());
                }
                if (!Where.IsEmpty)
                {
                    sql.AppendLine(WhereToken);
                    sql.Append(Where.ToString());
                }
                if (!GroupBy.IsEmpty)
                {
                    sql.AppendLine(GroupByToken);
                    sql.Append(GroupBy.ToString());
                }
                if (!OrderBy.IsEmpty)
                {
                    sql.AppendLine(OrderByToken);
                    sql.Append(OrderBy.ToString());
                }

                return sql.ToString();
            }

            public override IQueryStatement Clone()
            {
                var query = new OracleQueryStatement();
                query.Select.CopyFrom(Select);
                query.From.CopyFrom(From);
                query.Where.CopyFrom(Where);
                query.GroupBy.CopyFrom(GroupBy);
                query.OrderBy.CopyFrom(OrderBy);

                return query;
            }
        }

        #endregion

        protected override QueryStatement GetQueryStatement()
        {
            return new OracleQueryStatement();
        }

        #region Merge Statement

        protected sealed class OracleMergeStatement : MergeStatement
        {
            private const string MergeIntoToken = "MERGE INTO ";
            private const string UsingOnToken = "USING DUAL ON ";
            private const string WhenMatchedToken = "WHEN MATCHED THEN";
            private const string UpdateSetToken = "UPDATE SET";
            private const string WhereToken = "WHERE ";
            private const string WhenNotMatchedToken = "WHEN NOT MATCHED THEN";
            private const string InsertToken = "INSERT";
            private const string ValuesToken = "VALUES";

            public override string ToString()
            {
                var sql = new StringBuilder();

                sql.Append(MergeIntoToken);
                sql.Append(MergeInto.ToString());
                sql.Append(UsingOnToken);
                sql.Append(UsingOn.ToString());
                if (!UpdateSet.IsEmpty)
                {
                    sql.AppendLine(WhenMatchedToken);
                    sql.AppendLine(UpdateSetToken);
                    sql.Append(UpdateSet.ToString());
                    if (!UpdateWhere.IsEmpty)
                    {
                        sql.Append(WhereToken);
                        sql.Append(UpdateWhere.ToString());
                    }
                }
                if (!Insert.IsEmpty)
                {
                    sql.AppendLine(WhenNotMatchedToken);
                    sql.AppendLine(InsertToken);
                    sql.Append(Insert.ToString());
                    sql.AppendLine(ValuesToken);
                    sql.Append(Values.ToString());
                }

                return sql.ToString();
            }

            public override string FormatTimestamp(DateTime date)
            {
                return string.Format("{0:D2}-{1:D2}-{2:D4} {3:D2}:{4:D2}:{5:D2}.{6:D9}",
                    date.Day, date.Month, date.Year, date.Hour, date.Minute, date.Second,
                    (int)Math.Truncate((date.TimeOfDay.TotalSeconds - Math.Truncate(date.TimeOfDay.TotalSeconds)) * 1000000000));
            }

            public override string GetTimestampFormatExpression(string paramName, string colName)
            {
                return string.Format("TRUNC(ABS(EXTRACT(SECOND FROM ({0} - TO_TIMESTAMP({1}, 'DD-MM-YYYY HH24:MI:SS.FF')))), 7) <= 0.0000005", colName, paramName);
            }

            public override IMergeStatement Clone()
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        protected override MergeStatement GetMergeStatement()
        {
            return new OracleMergeStatement();
        }

        #region Delete Statement

        protected sealed class OracleDeleteStatement : DeleteStatement
        {
            private const string DeleteFromToken = "DELETE FROM";
            private const string WhereToken = "WHERE";

            public override string ToString()
            {
                var sql = new StringBuilder();

                sql.AppendLine(DeleteFromToken);
                sql.Append(DeleteFrom.ToString());
                sql.AppendLine(WhereToken);
                sql.Append(Where.ToString());

                return sql.ToString();
            }

            public override IDeleteStatement Clone()
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        protected override DeleteStatement GetDeleteStatement()
        {
            return new OracleDeleteStatement();
        }

        #region Lock Statement

        protected sealed class OracleLockStatement : LockStatement
        {
            private const string LockTableToken = "LOCK TABLE";
            private const string LockModeToken = "IN EXCLUSIVE MODE NOWAIT";

            public override string ToString()
            {
                var sql = new StringBuilder();

                sql.AppendLine(LockTableToken);
                sql.Append(Table.ToString());
                sql.Append(LockModeToken);

                return sql.ToString();
            }

            public override ILockStatement Clone()
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        protected override LockStatement GetLockStatement()
        {
            return new OracleLockStatement();
        }

        protected override string ParameterToString(object value)
        {
            if (value.GetType().Equals(typeof(byte[])))
            {
                var bytes = (byte[])value;
                if (bytes.Length == 16)
                    return bytes.ToHex();
            }

            return value.ToString();
        }

        private void ExceptionHandler(OracleException ex, string commandText, IEnumerable<KeyValuePair<string, object>> commandParams)
        {
            // Here convert oracle exception to custom exceptions
            if (ex != null)
            {
                var errorCode = ex.Number;
                switch (errorCode)
                {
                    // Unique violation
                    case 1:
                        throw new UniqueViolationException(this, ex, commandText, commandParams);
                    // Resource locked
                    case 54:
                        throw new ResourceLockedException(this, ex, commandText, commandParams);
                    // Child record found
                    case 2292:
                        throw new ChildRecordFoundException(this, ex, commandText, commandParams);
                    default:
                        if (errorCode >= 20000 && errorCode <= 20999)
                            throw new DatabaseCustomException(ex, errorCode);
                        else
                            throw new DatabaseException(this, ex, ex.Message, commandText, commandParams);
                }
            }
        }

        private static IDictionary<string, Stream> GetLobStreams(
            OracleConnection connection,
            OracleTransaction transaction,
            OracleDbType lobType, IDictionary<string, byte[]> buffers)
        {
            var lobs = new Dictionary<string, Stream>(buffers.Count);
            foreach (var buffer in buffers)
            {
                Stream stream = null;
                switch (lobType)
                {
                    case OracleDbType.Blob:
                        stream = new OracleBlob(connection);
                        break;
                    case OracleDbType.Clob:
                        stream = new OracleClob(connection);
                        break;
                }

                if (stream != null)
                {
                    stream.Write(buffer.Value, 0, buffer.Value.Length);
                    lobs.Add(buffer.Key, stream);
                }
            }

            return lobs;
        }

        private static Stream GetLobStream(
            OracleConnection connection,
            OracleTransaction transaction,
            OracleDbType lobType, byte[] buffer)
        {
            var buffers = new Dictionary<string, byte[]>(1);
            buffers.Add("TEMP", buffer);
            return GetLobStreams(connection, transaction, lobType, buffers).First().Value;
        }

        public override IDbParameter CreateParameter(IDbCommand command, string name, object value)
        {
            if (value != null)
            {
                var type = value.GetType();

                OracleDbType oracleLobType = 0;
                if (type.Equals(typeof(Blob)))
                    oracleLobType = OracleDbType.Blob;
                else if (type.Equals(typeof(Clob)))
                    oracleLobType = OracleDbType.Clob;

                byte[] buffer = null;
                switch (oracleLobType)
                {
                    case OracleDbType.Blob:
                        buffer = (Blob)value;
                        break;
                    case OracleDbType.Clob:
                        buffer = (Clob)value;
                        break;
                }

                if (buffer != null)
                {
                    var lob = GetLobStream(_connection, _transaction, oracleLobType, buffer);
                    var param = ((OracleDbCommand)command).CreateParameter(name, oracleLobType);
                    param.Value = lob;
                    return param;
                }
            }

            var parameter = CreateParameter(command);
            parameter.ParameterName = name;
            parameter.Value = value;
            return parameter;
        }

        public override void CreateParameter(IDbCommand command, string name, int count, object[] value)
        {
            for (int i = 0; i < count; i++)
                CreateParameter(command, name + i.ToString(), ConvertValueType(value[i]));
        }

        #region OracleQueryReader

        private class OracleQueryReader : QueryReader
        {
            #region Constructor

            public OracleQueryReader(IDataReader reader)
                : base(reader) { }

            #endregion
            #region Protected Methods

            protected override Type GetTypeFromMetadata(Type type, string name, object[] metadata, out bool isNullable)
            {
                return InternalGetTypeFromMetadata(type, name, metadata, out isNullable);
            }

            #endregion
            #region Public Methods

            public override IRecord Clone()
            {
                return new OracleQueryReader(Reader);
            }

            #endregion
        }

        #endregion

        public override QueryReader GetQueryReader(IDataReader reader)
        {
            return new OracleQueryReader(reader);
        }

        public override IDataReader ExecuteReader(string commandText, string commandName,
            IEnumerable<KeyValuePair<string, object>> parameters,
            int pageNumber, short pageSize, out long recordCount)
        {
            recordCount = 0;
            var paging = pageNumber > 0 && pageSize > 0;

            var command = CreateCommand(commandName, pageNumber, pageSize);
            if (paging)
                command.CommandText = GetPagedCommandText(commandText, null, pageNumber, pageSize);
            else
                command.CommandText = commandText;

            foreach (var param in parameters)
                CreateParameter(command, param.Key, param.Value);

            return ExecuteReader(command);
        }

        public override IDataReader ExecuteReader(IDbCommand command, CommandBehavior behavior)
        {
            try
            {
                var commandStopwatch = StartCommand(command);
                try
                {
                    return command.ExecuteReader(behavior);
                }
                finally
                {
                    if (commandStopwatch != null)
                    {
                        commandStopwatch.Executed();
                        EndCommand(commandStopwatch);
                    }
                }
            }
            catch (DbException ex)
            {
                var commandText = command.CommandText;
                var commandParams = command.Parameters.Cast<IDataParameter>()
                    .Select(x => new KeyValuePair<string, object>(x.ParameterName, x.Value));
                ExceptionHandler(ex.GetBaseException() as OracleException, commandText, commandParams);
            }
            catch (Exception ex)
            {
                var commandText = command.CommandText;
                var commandParams = command.Parameters.Cast<IDataParameter>()
                    .Select(x => new KeyValuePair<string, object>(x.ParameterName, x.Value));
                throw new DatabaseException(this, ex, commandText, commandParams);
            }

            return null;
        }

        public override object ExecuteScalar(IDbCommand command)
        {
            try
            {
                var commandStopwatch = StartCommand(command);
                try
                {
                    return command.ExecuteScalar();
                }
                finally
                {
                    if (commandStopwatch != null)
                    {
                        commandStopwatch.Executed();
                        EndCommand(commandStopwatch);
                    }
                }
            }
            catch (DbException ex)
            {
                var commandText = command.CommandText;
                var commandParams = command.Parameters.Cast<IDataParameter>()
                    .Select(x => new KeyValuePair<string, object>(x.ParameterName, x.Value));
                ExceptionHandler(ex.GetBaseException() as OracleException, commandText, commandParams);
            }
            catch (Exception ex)
            {
                var commandText = command.CommandText;
                var commandParams = command.Parameters.Cast<IDataParameter>()
                    .Select(x => new KeyValuePair<string, object>(x.ParameterName, x.Value));
                throw new DatabaseException(this, ex, commandText, commandParams);
            }

            return null;
        }

        public override int ExecuteNonQuery(IDbCommand command)
        {
            try
            {
                var commandStopwatch = StartCommand(command);
                try
                {
                    return command.ExecuteNonQuery();
                }
                finally
                {
                    if (commandStopwatch != null)
                    {
                        commandStopwatch.Executed();
                        EndCommand(commandStopwatch);
                    }
                }
            }
            catch (DbException ex)
            {
                var commandText = command.CommandText;
                var commandParams = command.Parameters.Cast<IDataParameter>()
                    .Select(x => new KeyValuePair<string, object>(x.ParameterName, x.Value));
                ExceptionHandler(ex.GetBaseException() as OracleException, commandText, commandParams);
            }
            catch (Exception ex)
            {
                var commandText = command.CommandText;
                var commandParams = command.Parameters.Cast<IDataParameter>()
                    .Select(x => new KeyValuePair<string, object>(x.ParameterName, x.Value));
                throw new DatabaseException(this, ex, commandText, commandParams);
            }

            return 0;
        }

        public override long GetSequenceNextValue(string name)
        {
            var command = CreateCommand("NewHotel.Sequence." + name);
            command.CommandText = string.Format("SELECT {0}.NEXTVAL FROM DUAL", name);
            return decimal.ToInt64((decimal)command.ExecuteScalar());
        }

        public override long GetSequenceCurrentValue(string name)
        {
            var command = CreateCommand("NewHotel.Sequence." + name);
            command.CommandText = string.Format("SELECT {0}.CURVAL FROM DUAL", name);
            return decimal.ToInt64((decimal)command.ExecuteScalar());
        }

        private static string GetUnicode(string value)
        {
            var unicodeArray = new char[value.Length * 5];
            int index = 0;
            foreach (var ch in value)
            {
                unicodeArray[index] = '\\';
                Array.Copy(((int)ch).ToString("X4").ToArray(), 0, unicodeArray, index + 1, 4);
                index += 5;
            }

            return new string(unicodeArray);
        }

        public override string GetSqlExpression(object value, Type type)
        {
            var typeCode = Type.GetTypeCode(type);
            switch (typeCode)
            {
                case TypeCode.DateTime:
                    var dateValue = (DateTime)Convert.ChangeType(value, type);
                    if (dateValue.TimeOfDay == TimeSpan.Zero)
                        return string.Format("TO_DATE('{0}-{1}-{2}', 'DD-MM-YYYY')",
                            dateValue.Day.ToString("D2"), dateValue.Month.ToString("D2"), dateValue.Year.ToString("D4"));
                    else
                        return string.Format("TO_DATE('{0}-{1}-{2} {3}:{4}:{5}', 'DD-MM-YYYY HH24:MI:SS')",
                            dateValue.Day.ToString("D2"), dateValue.Month.ToString("D2"), dateValue.Year.ToString("D4"),
                            dateValue.Hour.ToString("D2"), dateValue.Minute.ToString("D2"), dateValue.Second.ToString("D2"));
                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                    var longValue = Convert.ToInt64(value);
                    return longValue.ToString();
                case TypeCode.String:
                    var strValue = (string)Convert.ChangeType(value, type);
                    return string.IsNullOrEmpty(strValue) ? "''" : "TO_CHAR(UNISTR('" + GetUnicode(strValue) + "'))";
                case TypeCode.Boolean:
                    var boolValue = (bool)Convert.ChangeType(value, type);
                    return boolValue ? "'1'" : "'0'";
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Decimal:
                    var decimalValue = Convert.ToDecimal(value);
                    return decimalValue.ToString("0.###############", _numberFormat);
                case TypeCode.Object:
                    if (value.GetType().Equals(typeof(Guid)))
                    {
                        var guidValue = (Guid)value;
                        return "HEXTORAW('" + guidValue.ToHex() + "')";
                    }
                    break;
            }

            return string.Empty;
        }

        public override string GetSqlConcatOperator()
        {
            return "||";
        }

        public override ISqlQueryBuilder GetSqlQueryBuilder()
        {
            return new OracleSqlQueryBuilder(_version12);
        }

        public override object Clone()
        {
            return new OracleDatabaseManager(_connectionString, _moduleName, _drcp, _user, _password, _dataSource);
        }
    }
}