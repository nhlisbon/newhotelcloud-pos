﻿using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewHotel.Pos.Hub.Models;
using NewHotel.Pos.Hub.Core;
using NewHotel.Pos.Hub.Api.Models;
using NewHotel.Pos.Hub.Model.Session;

namespace NewHotel.Pos.Hub.Services
{
	public interface ISessionService
	{
		ValidationResult<NewSessionModel> CreateSession(Guid userId, Guid installationId);
		ValidationResult<NewSessionModel> ExtractSession(Guid sessionId);
		ValidationItemResult<Model.UserRecord> GetUserByCode(string userCode);
		ValidationItemResult<Model.UserRecord> GetUserById(Guid userId);
		ValidationResult<NewSessionModel> Login(LoginModel loginModel);
		ValidationResult<IPosSessionContext> UpdateSession(UpdateSessionModel sessionModel);
	}

	public class SessionService : ISessionService
	{
		private readonly IStandBusiness standBusiness;
		private readonly IPosSessionContext sessionContext;
		private readonly ISettingBusiness settingBusiness;
		private readonly IPosSessionFactory sessionFactory;

		public SessionService(
			IStandBusiness standBusiness,
			IPosSessionContext sessionContext,
			ISettingBusiness settingBusiness,
			IPosSessionFactory sessionFactory)
		{
			this.standBusiness = standBusiness;
			this.sessionContext = sessionContext;
			this.settingBusiness = settingBusiness;
			this.sessionFactory = sessionFactory;
		}

		public ValidationItemResult<Model.UserRecord> GetUserByCode(string userCode)
		{
			return standBusiness.GetUserByCode(userCode);
		}

		public ValidationItemResult<Model.UserRecord> GetUserById(Guid userId)
		{
			return standBusiness.GetUserById(userId);
		}

		public ValidationResult<NewSessionModel> Login(LoginModel loginModel)
		{
			ValidationResult<NewSessionModel> result = new ValidationResult<NewSessionModel>();

			if (loginModel.CurrentSessionId != Guid.Empty)
			{
				result = ExtractSession(loginModel.CurrentSessionId);
			}

			if (result.HasValue())
				return result;

			result = CreateSession(loginModel.UserId, loginModel.InstallationId);

			return result;
		}

		public ValidationResult<NewSessionModel> CreateSession(Guid userId, Guid installationId)
		{
			ValidationResult<NewSessionModel> result = new ValidationResult<NewSessionModel>();

			var userResult = standBusiness.GetUserById(userId);
			if (result.Validate(userResult).HasErrors)
				return result;

			Model.UserRecord user = userResult.Item;
			Guid sessionId = BaseService.CreateSessionToken(user, installationId);

			return result.Set(new NewSessionModel
			{
				Id = sessionId,
				InstallationId = installationId,
				User = user
			});
		}

		public ValidationResult<NewSessionModel> ExtractSession(Guid sessionId)
		{
			var result = new ValidationResult<NewSessionModel>();

			HubSessionData sessionData = BaseService.GetHubSessionData(sessionId);
			if (sessionData == null)
				return result.AddError(999, "Session not found");

			var user = standBusiness.GetUserById(sessionData.UserId);

			return result.Set(new NewSessionModel
			{
				Id = sessionId,
				InstallationId = sessionData.InstallationId,
				User = user.Item
			});
		}

		public ValidationResult<IPosSessionContext> UpdateSession(UpdateSessionModel sessionModel)
		{
			Guid schemaId = sessionModel.SchemaId;
			if (schemaId == Guid.Empty)
			{
				NewHotel.Contracts.Pos.Records.POSGeneralSettingsRecord settings = settingBusiness.GetGeneralSettings(sessionContext.InstallationId);
				schemaId = settings.DefaultTaxSchemaId ?? Guid.Empty;
			}

			var workDateStr = BaseService.SetTokenContext(
				sessionContext.SessionId,
				sessionModel.StandId.ToString(),
				sessionModel.CashierId.ToString(),
				schemaId.ToString(),
				sessionModel.LanguageId.ToString());

			var sessionData = BaseService.GetHubSessionData(sessionContext.SessionId);
			IPosSessionContext result = sessionFactory.Create(sessionContext.SessionId, sessionData, sessionContext.IsInternal);
			return new ValidationResult<IPosSessionContext>(result);
		}
	}
}
