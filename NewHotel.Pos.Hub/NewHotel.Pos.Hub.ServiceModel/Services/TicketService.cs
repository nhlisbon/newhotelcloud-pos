﻿using NewHotel.Pos.Hub.Business.Interfaces;
using System;
using NewHotel.Pos.Hub.Api.Models;
using NewHotel.Contracts;
using NewHotel.Pos.Hub.Model.Session;
using System.Collections.Generic;
using NewHotel.Core;
using NewHotel.Pos.Hub.Business.Business;
using System.Linq;
using NewHotel.Pos.Hub.Model;

namespace NewHotel.Pos.Hub.Services
{
	public interface ITicketService
	{
		ValidationResult<POSTicketContract> AddTicketItem(NewTicketItemModel ticketItem);
		ValidationResult<Model.DocumentSerieRecord[]> GetTicketSerieByStandCashier(Guid standId, Guid cashierId);
		ValidationResult<Model.TicketInfo[]> ListTickets();
		ValidationResult<Model.TicketWithProducts> ListTicketsWithProducts(Guid ticketId);
	}

	public class TicketService : ITicketService
	{
		private readonly IPosSessionContext sessionContext;
		private readonly IDatabaseManager maneger;
		private readonly ITableBusiness tableBusiness;
		private readonly ITicketBusiness ticketBusiness;

		public TicketService(
			IPosSessionContext sessionContext,
			IDatabaseManager maneger,
			ITableBusiness tableBusiness,
			ITicketBusiness ticketBusiness)
		{
			this.sessionContext = sessionContext;
			this.maneger = maneger;
			this.tableBusiness = tableBusiness;
			this.ticketBusiness = ticketBusiness;
		}

		public ValidationResult<Model.DocumentSerieRecord[]> GetTicketSerieByStandCashier(Guid standId, Guid cashierId)
		{
			return new ValidationResult<Model.DocumentSerieRecord[]>(ticketBusiness.GetTicketSerieByStandCashier(standId, cashierId).ToArray());
		}

		public ValidationResult<Model.TicketInfo[]> ListTickets()
		{
			Model.TicketInfo[] result = ticketBusiness.GetTicketInfoByStandCashier(
				sessionContext.InstallationId,
				sessionContext.StandId,
				sessionContext.CashierId)
				.ToArray();

			return new ValidationResult<Model.TicketInfo[]>(result);
		}

		public ValidationResult<POSTicketContract> AddTicketItem(NewHotel.Contracts.NewTicketItemModel ticketItem)
		{
			var result = new ValidationResult<POSTicketContract>();

			if (ticketItem.TicketId == Guid.Empty && !ticketItem.TableId.HasValue && !string.IsNullOrEmpty(ticketItem.TableDescription))
			{
				if (result.Validate(() => ticketItem.SaloonId.HasValue, 1003, "Saloon not found").HasErrors)
					return result;

				var tables = maneger
					.QueryTable(query => query.SetFilter("mesa_desc", ticketItem.TableDescription).SetFilter("salo_pk", ticketItem.SaloonId.Value))
					;

				if (result
					.Validate(() => tables.Any(), 1001, "Table not found")
					.Validate(() => tables.Count() == 1, 1002, "Multiple tables found")
					.HasErrors)
					return result;

				var table = tables.First();

				ticketItem.TableId = table.Id;
			}

			return ticketBusiness
				.AddProductLineToTicket(ticketItem)
				.ToValidationResult<POSTicketContract>();
		}

        public ValidationResult<TicketWithProducts> ListTicketsWithProducts(Guid ticketId)
        {
            Model.TicketWithProducts result = ticketBusiness.GetTicketWithProducts(ticketId);

            return new ValidationResult<Model.TicketWithProducts>(result);
        }
    }
}
