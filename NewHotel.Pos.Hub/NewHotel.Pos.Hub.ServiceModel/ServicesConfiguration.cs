﻿using NewHotel.Pos.Hub.Services;
using NewHotel.Pos.IoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub
{
	public static class ServicesConfiguration
	{
		public static void Configure(IServiceConfigurator configurator)
		{
			// Configure services here
			configurator
				.AddScoped<ISessionService, SessionService>()
				.AddScoped<ITicketService, TicketService>()
				;
		}
	}
}
