﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Models
{
	public class NewSessionModel
	{
		public Guid Id { get; set; }
		public Guid InstallationId { get; set; }
		public Model.UserRecord User { get; set; }
	}
}
