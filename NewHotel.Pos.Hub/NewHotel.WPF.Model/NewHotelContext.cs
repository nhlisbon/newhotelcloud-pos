﻿using NewHotel.Contracts;
using NewHotel.DataAnnotations;
using System;
using System.ComponentModel;

namespace NewHotel.WPF.App.Pos
{
    public class NewHotelContext : IRequestSessionData, INotifyPropertyChanged
    {
        #region Constants

        public const string DEFAULT_USER = "NHT";
        public const string DEFAULT_PASSWORD = "654C45F49B724BF0B5AC112718596CA4";

        #endregion
        #region Members

        //private static NewHotelContext _instance = null;
        private NewHotelContextData _data;
        private string _url;
        private Func<string> _password;

        #endregion
        #region Properties

        public static NewHotelContextStorage NewHotelContextStorage { get; set; }

        public static NewHotelContext Current
        {
            get
            {
                if (NewHotelContextStorage.Instance == null)
                    NewHotelContextStorage.Instance = new NewHotelContext();

                return NewHotelContextStorage.Instance;
            }
        }

        public NewHotelContextData Data
        {
            get
            {
                if (_data == null)
                    _data = new NewHotelContextData();

                return _data;
            }
        }

        public string LastUserName { get; set; }
        public string UserName { get; set; }
        public string Version { get; set; }

        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }

        public long ApplicationId
        {
            get { return Data.ApplicationId; }
            set { Data.ApplicationId = value; }
        }

        public string Password
        {
            get { return _password(); }
        }

        public Guid UserId
        {
            get { return Data.UserId; }
            set { Data.UserId = value; }
        }

        public Guid InstallationId
        {
            get { return Data.InstallationId; }
        }

        public DateTime Workdate
        {
            get { return Data.WorkDate; }
        }

        public DateTime SysDate
        {
            get { return Data.SysDate; }
        }

        public long Language
        {
            get { return Data.LanguageId; }
            set { Data.LanguageId = value; }
        }

        public string CultureName
        {
            get { return Data.CultureName; }
            set { Data.CultureName = value; }
        }

        public KeyDescRecord[] Languages
        {
            get { return Data.LanguageData; }
        }

        #endregion
        #region Constructor

        public NewHotelContext()
        {
            UserName = DEFAULT_USER;
            _password = () => { return DEFAULT_PASSWORD; };
        }

        static NewHotelContext()
        {
            NewHotelContextStorage = new NewHotelContextStorage();
        }

        #endregion
        #region Public Methods

        public KeyDescRecord[] GetEnums(Type type)
        {
            KeyDescRecord[] values;
            if (Data.EnumData.TryGetValue(type.Name.ToString(), out values))
                return values;

            return new KeyDescRecord[0];
        }

        public static void Clear()
        {
            NewHotelContextStorage.Instance = null;
        }

        public void SetContextData(NewHotelContextData data)
        {
            Data.CultureName = data.CultureName;

            if (data.CultureNameFormat != null) Data.CultureNameFormat = data.CultureNameFormat;

            Data.LanguageId = data.LanguageId;
            Data.MaxTabCount = data.MaxTabCount;
            Data.UserId = data.UserId;
            Data.Name = data.Name;
            Data.InstallationId = data.InstallationId;
            Data.InstallationName = data.InstallationName;
            Data.EnumData = data.EnumData;
            Data.LanguageData = data.LanguageData;
            Data.SysDate = data.SysDate;
            Data.WorkDate = data.WorkDate;
            Data.WorkShift = data.WorkShift;
            Data.FirstOpenedDate = data.FirstOpenedDate;
            Data.LastOpenedYear = data.LastOpenedYear;
            Data.CurrencyId = data.CurrencyId;
            Data.CurrencySymbol = data.CurrencySymbol;
            Data.CurrencyDecInPercents = data.CurrencyDecInPercents;
            Data.CurrencyDecInPrices = data.CurrencyDecInPrices;
            Data.CurrencyDecInExchange = data.CurrencyDecInExchange;
            Data.FirstTimeRun = data.FirstTimeRun;
            Data.Country = data.Country;
            Data.Limits = data.Limits;
            Data.UserValidUntil = data.UserValidUntil;
            Data.UserPermissions = data.UserPermissions;
            Data.UserPermissionsDetails = data.UserPermissionsDetails;
            Data.IsInternalUser = data.IsInternalUser;
            Data.ValidLoginPeriod = data.ValidLoginPeriod;
            Data.AllowMultiSchemaInvoicing = data.AllowMultiSchemaInvoicing;
            Data.IsMultiProperty = data.IsMultiProperty;
            Data.IsUserMultiProperty = data.IsUserMultiProperty;
            Data.LocalStock = data.LocalStock;
        }

        public void SetSecurityData(string username, string password)
        {
            UserName = username;
            _password = () => { return password; };
        }

        public void SetRequestSessionData(ISessionData data)
        {
            if (Data != null)
            {
                Data.SysDate = data.SysDate;
                Data.WorkDate = data.WorkDate;
                Data.WorkShift = data.WorkShift;
                Data.FirstOpenedDate = data.FirstOpenedDate;
                Data.LastOpenedYear = data.LastOpenedYear;
            }
        }

        public ISessionData GetRequestSessionData()
        {
            if (Data != null)
            {
                var data = new SessionData()
                {
                    ApplicationId = Data.ApplicationId,
                    ModuleName = Data.ModuleName,
                    InstallationId = Data.InstallationId,
                    UserId = Data.UserId,
                    LanguageId = Data.LanguageId,
                    SysDate = Data.SysDate,
                    WorkDate = Data.WorkDate,
                    WorkShift = Data.WorkShift,
                    FirstOpenedDate = Data.FirstOpenedDate,
                    LastOpenedYear = Data.LastOpenedYear
                };

                data.Version = Version;
                return data;
            }

            return null;
        }

        public void InitContext()
        {
            UserName = DEFAULT_USER;
            _password = () => { return DEFAULT_PASSWORD; };
        }

        #endregion
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
        #region Private Methods

        protected void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }

        #endregion
    }

    public class NewHotelContextStorage
    {
        static NewHotelContext instance;
        public virtual NewHotelContext Instance
        {
            get { return instance; }
            set { instance = value; }
        }
    }
}
