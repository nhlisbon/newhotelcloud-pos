﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Ionic.Zip;
using NewHotel.Pos.Communication;

namespace NewHotel.WPF.Model
{
    public class UpdaterCashier
    {
        public string UpdaterAppName
        {
            get
            {
                string name = MgrServices.GetConfigurationSetting("UpdaterAssemblyName");
                if (string.IsNullOrEmpty(name))
                {
                    name = "NewHotel.Deploy.WPF.Updater";
                    MgrServices.SetConfigurationSetting("UpdaterAssemblyName", name);
                }
                return name + ".exe";
            }
        }
        public string PkgName => "pos.zip";
        public string FolderPkgName => "NewPosUpdate";
        public string PosFolder => "Pos";

        public enum Status
        {
            ResetDirectory, DownloadPkgFile, ExtractPkgFile, StartUpdaterProcess
        }

        public event Action<Status> StatusChange;
        public event Action<Exception> Error;

        public async Task Update()
        {
            try
            {
                // Clean directory of previous updates
                OnStatusChange(Status.ResetDirectory);
                string pathUpdate = Path.Combine(Path.GetTempPath(), FolderPkgName);
                await ResetDirectory(pathUpdate);

                // Copy pos zip 
                OnStatusChange(Status.DownloadPkgFile);
                string pathPkg = Path.Combine(pathUpdate, PkgName);
                await CreatePkgFile(pathPkg);

                // Extract pos zip 
                OnStatusChange(Status.ExtractPkgFile);
                string updatePosDirectory = Path.Combine(pathUpdate, PosFolder);
                await ExtractPkgFile(pathPkg, updatePosDirectory);

                // Kill current process and start udpater
                OnStatusChange(Status.StartUpdaterProcess);
                StartUpdateProcess(updatePosDirectory);
            }
            catch (Exception e)
            {
                OnError(e);
            }
        }

        private async Task ResetDirectory(string directory)
        {
            await Task.Run(() =>
            {
                if (Directory.Exists(directory))
                    Directory.Delete(directory, true);
                Directory.CreateDirectory(directory);
            });
        }

        private async Task CreatePkgFile(string pathPkg)
        {
            Stream data = await BusinessProxyRepository.Cashier.GetCashierFilesAsync();
            var filePkg = new FileStream(pathPkg, FileMode.Create);
            data.CopyTo(filePkg);
            filePkg.Close();
        }

        private async Task ExtractPkgFile(string cashierPkgPath, string targetDirectory)
        {
            await Task.Run(() =>
            {
                var zip = ZipFile.Read(cashierPkgPath);
                zip.ExtractAll(targetDirectory, ExtractExistingFileAction.OverwriteSilently);
                zip.Dispose();
            });
        }

        private void StartUpdateProcess(string updatedPosDirectory)
        {
            var currentProcess = Process.GetCurrentProcess();
            var currentDirectory = new DirectoryInfo(Directory.GetCurrentDirectory());
            string sourceCashierDirectory = updatedPosDirectory;
            string targetCashierDirectory = currentDirectory.FullName;
            string backupDirectory = Path.Combine(currentDirectory.Parent.FullName, $"{currentDirectory.Name}-Backups");
            string cashierProcessName = currentProcess.ProcessName;
            string cashierAssemblyName = Assembly.GetEntryAssembly().GetName().Name;

            string updaterPath = Path.Combine(updatedPosDirectory, UpdaterAppName);
            string args =
                $"559E7373DC6A8B4B84AD084926F5E098 " +
                $"\"{sourceCashierDirectory}\" " +
                $"\"{targetCashierDirectory}\" " +
                $"\"{backupDirectory}\" " +
                $"\"{cashierProcessName}\" " +
                $"\"{cashierAssemblyName}\"";
            Process.Start(updaterPath, args);
            currentProcess.Kill();
        }

        protected virtual void OnStatusChange(Status obj)
        {
            StatusChange?.Invoke(obj);
        }

        protected virtual void OnError(Exception obj)
        {
            Error?.Invoke(obj);
        }
    }
}
