﻿using NewHotel.Payments.Device.Processors;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace NewHotel.WPF.App.Pos.Model
{
	internal struct ProcessorResponse
	{
		public ProcessorResponse(string value, bool cancelled)
		{
			Value = value;
			Cancelled = cancelled;
		}

		public string Value { get; }
		public bool Cancelled { get; }

		public static ProcessorResponse CancelledResponse => new(string.Empty, true);
		public static ProcessorResponse EmptyResponse => new(string.Empty, false);
		public static ProcessorResponse FromValue(string value) => new(value, false);
	}

	internal sealed class InternalInteractivityProcessor : IDeviceInteractiveActionsProcessor, IDisposable
	{
		private readonly ExternalPayment externalPayment;
		private readonly Channel<ProcessorResponse> channel;
		private readonly ChannelWriter<ProcessorResponse> writer;
		private readonly ChannelReader<ProcessorResponse> reader;

		public InternalInteractivityProcessor(ExternalPayment externalPayment)
		{
			this.externalPayment = externalPayment;
			channel = Channel.CreateBounded<ProcessorResponse>(1);
			writer = channel.Writer;
			reader = channel.Reader;
		}

		public async Task AcceptResponse(ProcessorResponse value)
		{
			await writer.WriteAsync(value);
		}

		public void Dispose()
		{
			writer.Complete();
		}

		private async Task<ProcessorActionResponse> WaitForRespons(ProcessorActionRequest request, CancellationToken cancellation = default)
		{
			var value = await reader.ReadAsync(cancellation);
			var response = new ProcessorActionResponse(value.Value, value.Cancelled);
			return response;
		}

		public async Task<ProcessorActionResponse> ProcessAction(ProcessorActionRequest request, CancellationToken cancellation = default)
		{
			switch (request.Type)
			{
				case ProcessorActionType.ShowToUser:
					externalPayment.IntUserMessage = request.Value;
					break;
				case ProcessorActionType.ShowToClient:
					externalPayment.IntClientMessage = request.Value;
					break;
				case ProcessorActionType.ShowDetail:
					externalPayment.IntDetailMessage = request.Value;
					break;
				case ProcessorActionType.MenuTitle:
					externalPayment.IntMenuTitle = request.Value;
					break;
				case ProcessorActionType.MenuContent:
					if (request.HasItems())
					{
						externalPayment.IntMenuContent = request.Items
							.Select(x => new InteractivityMenuItem { Key = x.Key, Value = x.Value })
							.ToArray();
						return await WaitForRespons(request, cancellation);
					}
					externalPayment.IntMenuContent = null;
					break;
				case ProcessorActionType.BooleanRequest:
					{
						externalPayment.IntBoolPrompt = request.Value;
						if (request.HasValue())
						{
							externalPayment.IntBoolResponse = "1".Equals(request.Value);
							externalPayment.SetActionItemFromBool();
							return await WaitForRespons(request, cancellation);
						}
					}
					break;
				case ProcessorActionType.KeyRequest:
					externalPayment.IntKeyPrompt = request.Value;
					if (request.HasValue())
					{
						externalPayment.SetActionItem();
						return await WaitForRespons(request, cancellation);
					}
					externalPayment.IntKeyPrompt = string.Empty;
					break;
				case ProcessorActionType.InputValue:
					{
						if (request.HasValue())
						{
							externalPayment.IntInputValue = string.Empty;
							externalPayment.IntInputMaxSize = request.MaxSize;
							externalPayment.IntInputPrompt = request.Value;
							externalPayment.SetActionItem();
							return await WaitForRespons(request, cancellation);
						}
						externalPayment.IntInputPrompt = string.Empty;
					}
					break;
			}

			return ProcessorActionResponse.EmptyResponse;
		}
	}
}
