﻿using NewHotel.Contracts;
using NewHotel.Payments.Device.Processors;
using NewHotel.Pos.Communication.Api;
using NewHotel.Pos.Communication.Cashier;
using NewHotel.Pos.Hub.Api.Models;
using NewHotel.Pos.Hub.Contracts.PaySystem;
using NewHotel.WPF.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace NewHotel.WPF.App.Pos.Model
{
	public class ExternalPaymentTerminal
	{
		PayTerminalModel payTerminalModel;

		public ExternalPaymentTerminal(PayTerminalModel payTerminalModel)
		{
			this.payTerminalModel = payTerminalModel;
		}

		public Guid Id { get => payTerminalModel.Id; set { } }
		public string Description { get => payTerminalModel.Name; set { } }
	}

	public class InteractivityMenuItem
	{
		public string Key { get; set; }
		public string Value { get; set; }
	}

	public enum ExternalPaymentState
	{
		None,
		Start,
		Waiting,
		TerminalSelection,
		TerminalSelected,
		Failure,
		Interactivity,
		Succeeded,
	}

	public enum PaymentSubState
	{
		None,
		RequestTerminals,
		RequestPayment,
	}

	public class ExternalPayment : BindableBase, IDisposable
	{
		private IPosHubApiClient apiClient;
		private PayClient.ITerminalSystems terminalSystems;
		Guid originId;

		string _description;
		string _waitingMessage;
		string _failureMessage;

		string _intUserMessage;
		string _intClientMessage;
		string _intDetailMessage;
		string _intMenuTitle;
		InteractivityMenuItem[] _intMenuContent;

		string _intBoolPrompt;
		bool _intBoolResponse;

		string _intKeyPrompt;

		string _intInputPrompt;
		string _intInputValue;

		ObservableCollection<ExternalPaymentTerminal> _terminals = [];
		Action _afterSuccess;

		public void Dispose()
		{
			interactiveActionsProcessor?.Dispose();
		}

		public static bool IsValidPaymentMethod(PaymentMethod paymentMethod)
		{
			return paymentMethod.IsCreditCardPayment || paymentMethod.IsCashPayment;
		}

		public void Initialize(IPosHubApiClient apiClient, PayClient.ITerminalSystems terminalSystems , Guid originId, Action afterSuccess)
		{
			if (!IsValidPaymentMethod(PaymentMethod))
				throw new InvalidOperationException("Invalid Payment Method");

			this.apiClient = apiClient;
			this.terminalSystems = terminalSystems;
			this.originId = originId;
			_afterSuccess = afterSuccess;
			NextState();
		}

		#region State Management
		ExternalPaymentState paymentState = ExternalPaymentState.Start;
		PaymentSubState paymentSubState = PaymentSubState.None;

		private void NextState()
		{ 
			switch (PaymentState, paymentSubState, Terminals, selectedTerminal, requestedPayment)
			{
				case (ExternalPaymentState.Start, _, _, _, _):
					SetWaiting(subState: PaymentSubState.RequestTerminals);
					ReadTerminals();
					break;
				case (ExternalPaymentState.Waiting, PaymentSubState.RequestTerminals, { Count: > 1 }, _, _):
					SetTerminalSelection();
					break;

				case (ExternalPaymentState.Waiting, PaymentSubState.RequestTerminals, { Count: 1 }, _, _):
					SetChecked(Terminals[0], SelectTerminal);
					break;

				//case (ExternalPaymentState.Waiting, PaymentSubState.RequestTerminals, { Count: 0 }, _, _):
				case (ExternalPaymentState.TerminalSelected, _, _, _, _):
					SetWaiting(subState: PaymentSubState.RequestPayment);
					StartPayment();
					break;

				case (ExternalPaymentState.Interactivity, _, _, _, _):
					StartInteractivity();
					break;

				case (ExternalPaymentState.Succeeded, PaymentSubState.RequestPayment, _, _, not null):
					if (completedPayment != null)
					{
						// Payment completed
						// Do something with the completed payment
						_afterSuccess?.Invoke();
						break;
					}
					break;
			}
		}

		private void UpdateState(ExternalPaymentState newState)
		{
			void CheckStateChange(ExternalPaymentState state)
			{
				switch (state)
				{
					case ExternalPaymentState.TerminalSelection:
						OnPropertyChanged(nameof(IsTerminalSelection));
						break;
					case ExternalPaymentState.Waiting:
						OnPropertyChanged(nameof(IsWaiting));
						break;
					case ExternalPaymentState.Failure:
						OnPropertyChanged(nameof(IsFailure));
						break;
					case ExternalPaymentState.Interactivity:
						OnPropertyChanged(nameof(IsInteractivity));
						break;
				}
			}

			var prevState = PaymentState;
			if (SetProperty(ref paymentState, newState, nameof(PaymentState)))
			{
				CheckStateChange(prevState);
				CheckStateChange(newState);
			}
		}

		private void SetFailure(string failureMessage)
		{
			FailureMessage = failureMessage;
			PaymentState = ExternalPaymentState.Failure;
		}

		private void SetChecked<T>(T value, Action<T> forChange)
		{
			try
			{
				forChange(value);
			}
			catch (Exception ex)
			{
				SetFailure(ex.Message);
			}

			NextState();
		}

		private void SetChecked<T>(ValidationResult<T> result, Action<T> forChange)
		{
			try
			{
				if (result.HasErrors)
				{
					SetFailure(result.Errors.FirstOrDefault()?.Message ?? "Generic Failure");
				}
				else
				{
					forChange(result.Result);
				}
			}
			catch (Exception ex)
			{
				SetFailure(ex.Message);
			}

			NextState();
		}

		private void SetWaiting(string waitingMessage = "Waiting", PaymentSubState subState = PaymentSubState.None)
		{
			WaitingMessage = waitingMessage;
			paymentSubState = subState;
			PaymentState = ExternalPaymentState.Waiting;
		}

		private void SetTerminalSelection()
		{
			paymentState = ExternalPaymentState.TerminalSelection;
			paymentSubState = PaymentSubState.None;
		}

		private void SetTerminalSelected()
		{
			paymentState = ExternalPaymentState.TerminalSelected;
			paymentSubState = PaymentSubState.None;
		}

		#endregion

		private void ReadTerminals()
		{
			apiClient
				.GetPaymentTerminalsAsync(originId)
				.ContinueWith(t =>
				{
					ValidationResult<RequestTerminalsContract> result = t.Result.ToValidationResult();
					try
					{
						SetChecked(result, SetTerminals);
					}
					catch (Exception ex)
					{
						SetFailure(ex.Message);
					}
				});
		}

		private void SetTerminals(RequestTerminalsContract terminals)
		{
			selectedTerminal = null;
			if (!terminals?.Terminals?.Any() ?? true)
			{
				SetTerminalSelected();
				return;
			}
			Terminals = new ObservableCollection<ExternalPaymentTerminal>(terminals.Terminals.Select(x => new ExternalPaymentTerminal(x)));

			//if (Terminals.Count == 1)
			//	OnSelectTerminal(Terminals[0]);
		}

		private void SelectTerminal(ExternalPaymentTerminal terminal)
		{
			selectedTerminal = terminal;
			SetTerminalSelected();
		}

		void OnSelectTerminal(ExternalPaymentTerminal terminal)
		{
			// Do something with the selected terminal
			SetChecked(terminal, SelectTerminal);
		}

		public ICommand SelectTerminalCommand => new RelayCommand(terminal => OnSelectTerminal((ExternalPaymentTerminal)terminal));

		public bool IsTerminalSelection => PaymentState == ExternalPaymentState.TerminalSelection;
		public bool IsWaiting => PaymentState == ExternalPaymentState.Waiting;
		public bool IsFailure => PaymentState == ExternalPaymentState.Failure;
		public bool IsInteractivity => PaymentState == ExternalPaymentState.Interactivity;
		public bool IsSuccess => PaymentState == ExternalPaymentState.Succeeded;
		public bool IsDeviceSuccess => ActionResult == Payments.Device.Values.ActionResult.Success;

		Payments.Device.Values.DeviceActionResult processResult = new() { Result = Payments.Device.Values.ActionResult.NotImplemented };
		ExternalPaymentTerminal selectedTerminal;

		public decimal Amount { get; set; }
		public decimal ReceivedAmount { get; set; }
		public string Currency { get; set; }
		public string Reference { get; set; }
		public string User { get; set; }
		public PaymentMethod PaymentMethod { get; set; }
		public WPF.Model.Model.Ticket Ticket { get; set; }
		public Payments.Device.Values.ActionResult ActionResult => processResult.Result;

		public void CompleteDevicePayment(Payment pay, Func<string, string, string, string, CreditCard> createCreditCard)
		{

			//pay.Details = typeComboBox.Text;
			pay.CurrencyId = Currency;
			pay.ExchangeCurrency = 1;
			pay.MultCurrency = true;
			pay.Tire = ReceivableType.Payment;
			pay.Description += "/" + processResult.CardExternaType;

			var creditCard = createCreditCard(processResult.CardNumber, $"[{processResult.CardExternaType}] {processResult.CardType}", processResult.Authorization, processResult.DeviceReferfence);
			creditCard.IdentNumber = processResult.CardNumber;
			creditCard.Id = Guid.NewGuid();

			pay.CreditCardModel = creditCard;
			pay.CreditCard = creditCard.Id;
			pay.SetCreditCard(creditCard, Amount, (creditCard.IdentNumber == "0000000000000000"));
		}

		public void CompletePayment(Payment pay, Func<string, string, string, string, CreditCard> createCreditCard)
		{
			switch (pay.Type)
			{
				case { IsCashPayment: true }:
					CompleteCashPayment(pay);
					break;
				case { IsCreditCardPayment: true }:
					CompleteCreditCardPayment(pay, createCreditCard);
					break;
			}
		}

		private void CompleteCashPayment(Payment pay)
		{
			pay.Id = (Guid)completedPayment.Id;
			pay.CurrencyId = Currency;
			pay.ExchangeCurrency = 1;
			pay.MultCurrency = true;
			pay.Tire = ReceivableType.Payment;
			pay.Details = Currency;
			pay.Description += "/" + completedPayment.CardExternalType;
		}

		private void CompleteCreditCardPayment(Payment pay, Func<string, string, string, string, CreditCard> createCreditCard)
		{
			pay.Id = (Guid)completedPayment.Id;
			pay.CurrencyId = Currency;
			pay.ExchangeCurrency = 1;
			pay.MultCurrency = true;
			pay.Tire = ReceivableType.Payment;
			pay.Description += "/" + completedPayment.CardExternalType;

			var creditCard = createCreditCard(completedPayment.CardNumber, $"[{completedPayment.CardExternalType}]", completedPayment.Authorization, completedPayment.Token);
			creditCard.IdentNumber = completedPayment.CardNumber;
			creditCard.Id = Guid.NewGuid();

			pay.CreditCardModel = creditCard;
			pay.CreditCard = creditCard.Id;
			pay.SetCreditCard(creditCard, Amount, (creditCard.IdentNumber == "0000000000000000"));
		}

		#region View Model Properties
		public string Description
		{
			get => _description;
			set => SetProperty(ref _description, value);
		}

		public string WaitingMessage
		{
			get => _waitingMessage;
			set => SetProperty(ref _waitingMessage, value);
		}

		public string FailureMessage
		{
			get => _failureMessage;
			set => SetProperty(ref _failureMessage, value);
		}

		public string SuccessMessage
		{
			get => _successMessage;
			set => SetProperty(ref _successMessage, value);
		}

		public ObservableCollection<ExternalPaymentTerminal> Terminals
		{
			get => _terminals;
			set => SetProperty(ref _terminals, value);
		}

		public ExternalPaymentState PaymentState
		{
			get => paymentState;
			set => UpdateState(value);
		}
		#endregion

		ExternalPaymentResponseContract requestedPayment;
		ExternalPaymentEntryContract completedPayment;

		void SetRequestedPayment(ExternalPaymentResponseContract contract)
		{
			requestedPayment = contract;
			if (requestedPayment.DeviceRequest != null)
			{
				PaymentState = ExternalPaymentState.Interactivity;
			}
		}

		private void StartPayment()
		{
			/* */
			apiClient
				.RequestPaymentAsync(new ExternalPaymentRequestContract
				{
					Values = new ExternalPaymentValues
					{
						CurrencyCode = Currency,
						Value = Amount,
						UserReference = Reference,
						Quota = 1,
						Workstation = Environment.MachineName,
					},
					PaymentMethodId = this.PaymentMethod.Id,
					TicketId = this.Ticket.Id,
					PaySystemTerminalId = selectedTerminal?.Id,
				})
				.ContinueWith(t =>
				{
					ValidationResult<ExternalPaymentResponseContract> result = t.Result.ToValidationResult();
					SetChecked(result, SetRequestedPayment);
					//try
					//{
					//	SetChecked(result, SetInteractivity);
					//}
					//catch (Exception ex)
					//{
					//	SetFailure(ex.Message);
					//}
				});
			/* */
		}

		private void Refresh()
		{
			if (requestedPayment == null)
				return;

			apiClient.ConfirmPaymentAsync((Guid)requestedPayment.Id)
				.ContinueWith(t =>
				{
					ValidationResult<ExternalPaymentEntryContract> result = t.Result.ToValidationResult();
					SetChecked(result, Confirm);
				});
		}

		private void UpdateDevice()
		{
			if (processResult == null)
			{
				SetFailure("No process result");
				return;
			}
			ExternalPaymentDeviceCompleteRequest request = new()
			{
				EntryId = (Guid)requestedPayment.Id,
				DeviceActionResult = processResult,
			};
			apiClient.UpdateDeviceAsync(request)
				.ContinueWith(t =>
				{
					ValidationResult<ExternalPaymentEntryContract> result = t.Result.ToValidationResult();
					SetChecked(result, Confirm);
				});
		}

		private void Confirm(ExternalPaymentEntryContract entry)
		{
			completedPayment = entry;
			switch (entry)
			{
				case { Status: PaymentOperationStatus.Completed }:
					SuccessMessage = "Payment Completed " + entry.SysResultMessage;
					PaymentState = ExternalPaymentState.Succeeded;
					ReceivedAmount = entry.Amount;
					break;
				case { Status: PaymentOperationStatus.Cancelled }:
					SetFailure("Payment Cancelled" + entry.SysResultMessage);
					break;
				case { Status: PaymentOperationStatus.Declined }:
					SetFailure("Payment Declined " + entry.SysResultMessage);
					break;
			}
		}

		public ICommand RefreshCommand => new RelayCommand((_) => Refresh());

		#region Interactivity
		private void SetInteractivity(ProcessorActionRequest request)
		{
			Description = request.Value;
			PaymentState = ExternalPaymentState.Interactivity;
		}


		private void StartInteractivity()
		{
			IntUserMessage = "User Message";
			PaymentState = ExternalPaymentState.Interactivity;

			Task.Run(async () =>
			{
				try
				{
					IPayMessageProcessor messageProcessor = terminalSystems.GetSystem(requestedPayment.SystemType);

					if (messageProcessor is null)
					{
						SetFailure($"System not found [{requestedPayment.SystemType}]");
						return;
					}

					if (messageProcessor is not IHasInteractivity interactivity)
					{
						SetFailure($"System does not support interactivity [{requestedPayment.SystemType}]");
						return;
					}

					interactiveActionsProcessor = new InternalInteractivityProcessor(this);
					interactivity.InteractivityProcessor = interactiveActionsProcessor;

					Description = messageProcessor.SystemName;

					var stPayload = Encoding.UTF8.GetBytes(requestedPayment.DeviceRequest);
					var natPayload = PayClient.Serializers.Json.Deserialize(stPayload, typeof(object));

					var payload = PayClient.Serializers.Json.DeserializeNative(natPayload, messageProcessor.MessageType);
					processResult = await messageProcessor.Process(payload, CancellationToken.None);
					UpdateDevice();
					if (IsSuccess)
						return;

					//var processor = new InternalInteractivityProcessor(this);
					//interactiveActionsProcessor = processor;
					//var messageProcessor = new STMessageProcessor();

					//var request = new STDeviceRequest
					//{
					//	//Configuration = new SiTef.Shared.Values.STDeviceConfiguration
					//	//{
					//	//	AgentUrl = "http://localhost:8080",
					//	//	CnpjInstall = "12345678912345",
					//	//	CnpjSoftware = "12345678912345",
					//	//	IdLoja = "00000000",
					//	//	SiTefIP = "localhost",
					//	//	TerminalId = "SE000325",
					//	//},
					//	Action = new Payments.Device.Values.DeviceAction
					//	{
					//		Ammount = new Payments.Device.Values.ActionMoney { CurrencyCode = Currency, Value = Amount },
					//		WorkDate = DateTime.Today,
					//		Type = Payments.Device.Values.ActionType.Sale,
					//		LocalId = (int)DateTime.Now.Ticks,
					//		UserId = User,
					//	}
					//};

					//Description = "SiTef";

					//var path = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
					//path = System.IO.Path.Combine(path, "sitefSettings.json");
					//if (!System.IO.File.Exists(path))
					//	return;
					//string json = System.IO.File.ReadAllText(path);
					//request.Configuration = JsonSerialization.FromJson<SiTef.Shared.Values.STDeviceConfiguration>(json);
					///* */
					//messageProcessor.InteractivityProcessor = processor;
					//processResult = await messageProcessor.Process(request, CancellationToken.None);

					//if (IsSuccess)
					//	return;
					///* */


					/*
					await processor.ShowToUser(processResult.ResponseMessage ?? "User Message");
					await Task.Delay(1000);

					await processor.ShowDetail(processResult.FailDetails ?? "Detail Message");
					await Task.Delay(1000);

					await processor.MenuTitle("Menu Title");

					var items = new[]
					{
						new ProcessorActionItem("Key1", "Value1"),
						new ProcessorActionItem ("Key2", "Value2"),
						new ProcessorActionItem ("Key3", "Value3"),
					};
					var mres = await processor.MenuContent(items);
					await processor.ShowToUser($"MRes => {mres.Response}");
					await processor.MenuContent(null);

					await Task.Delay(1000);
					var bres = await processor.BooleanRequest("Boolean Request");

					await processor.ShowToUser($"BRes => {bres.Response}");
					await processor.BooleanRequest(string.Empty);

					var ires = await processor.InputValue("Input Value", 10);
					await processor.ShowToUser($"IRes => {ires.Response}");
					await processor.InputValue(string.Empty, 4);

					var kres = await processor.KeyRequest("Key Request");
					await processor.ShowToUser($"KRes => {kres.Response}");
					await processor.KeyRequest(string.Empty);
					/* */
				}
				catch (Exception ex)
				{
					SetFailure(ex.Message);
				}
			});
		}

		InteractivityMenuItem _actionItem;
		InternalInteractivityProcessor interactiveActionsProcessor;
		bool _cancelRequested;
		private string _successMessage;

		internal void ClearActionItem()
		{
			_actionItem = null;
			OnPropertyChanged(nameof(ActionItem));
		}

		internal void SetActionItem(string value)
		{
			_actionItem ??= new InteractivityMenuItem();
			_actionItem.Value = value;
			OnPropertyChanged(nameof(ActionItem));
		}

		internal void SetActionItem(InteractivityMenuItem item = default)
		{
			_actionItem = item ?? new InteractivityMenuItem();
			OnPropertyChanged(nameof(ActionItem));
		}

		internal void SetActionItemFromBool()
		{
			SetActionItem(IntBoolResponse ? "1" : "0");
		}

		public ICommand IntCancelCommand =>
			new RelayCommand(item =>
			{
				if (HasActionItem)
					interactiveActionsProcessor.AcceptResponse(ProcessorResponse.CancelledResponse).ContinueWith(_ => ClearActionItem());
				else
					_cancelRequested = true;
			});

		public ICommand IntActionCommand =>
			new RelayCommand<InteractivityMenuItem>(item =>
			{
				interactiveActionsProcessor.AcceptResponse(ProcessorResponse.FromValue(item.Value)).ContinueWith(_ => ClearActionItem());
			});

		public ICommand IntMenuCommand =>
			new RelayCommand<InteractivityMenuItem>(item =>
			{
				interactiveActionsProcessor.AcceptResponse(ProcessorResponse.FromValue(item.Key)).ContinueWith(_ => ClearActionItem());
			});

		public InteractivityMenuItem ActionItem
		{
			get => _actionItem;
			set => SetActionItem(value);
		}

		public bool HasActionItem => ActionItem != null;

		public string IntUserMessage
		{
			get => _intUserMessage;
			set => SetProperty(ref _intUserMessage, value);
		}

		public string IntClientMessage
		{
			get => _intClientMessage;
			set => SetProperty(ref _intClientMessage, value);
		}

		public string IntDetailMessage
		{
			get => _intDetailMessage;
			set => SetProperty(ref _intDetailMessage, value);
		}

		public string IntMenuTitle
		{
			get => _intMenuTitle;
			set => SetProperty(ref _intMenuTitle, value);
		}

		public InteractivityMenuItem[] IntMenuContent
		{
			get => _intMenuContent;
			set => SetProperty(ref _intMenuContent, value);
		}

		public string IntBoolPrompt
		{
			get => _intBoolPrompt;
			set => SetProperty(ref _intBoolPrompt, value);
		}

		public bool IntBoolResponse
		{
			get => _intBoolResponse;
			set
			{
				if (SetProperty(ref _intBoolResponse, value))
					SetActionItemFromBool();
			}
		}

		public string IntKeyPrompt
		{
			get => _intKeyPrompt;
			set => SetProperty(ref _intKeyPrompt, value);
		}

		public string IntInputPrompt
		{
			get => _intInputPrompt;
			set => SetProperty(ref _intInputPrompt, value);
		}

		public int IntInputMaxSize { get; set; }
		public string IntInputValue
		{
			get => _intInputValue;
			set
			{
				if (SetProperty(ref _intInputValue, value))
					SetActionItem(value);
			}
		}
		#endregion
	}

	public class RelayCommand : ICommand
	{
		private readonly Action<object> _execute;
		private readonly Func<object, bool> _canExecute;
		public RelayCommand(Action<object> execute, Func<object, bool> canExecute = null)
		{
			_execute = execute;
			_canExecute = canExecute;
		}
		public event EventHandler CanExecuteChanged;
		public bool CanExecute(object parameter)
		{
			return _canExecute == null || _canExecute(parameter);
		}
		public void Execute(object parameter)
		{
			_execute(parameter);
		}
	}

	public class RelayCommand<T> : ICommand
	{
		private readonly Action<T> _execute;
		private readonly Func<object, bool> _canExecute;
		public RelayCommand(Action<T> execute, Func<object, bool> canExecute = null)
		{
			_execute = execute;
			_canExecute = canExecute;
		}
		public event EventHandler CanExecuteChanged;
		public bool CanExecute(object parameter)
		{
			return _canExecute == null || _canExecute(parameter);
		}
		public void Execute(object parameter)
		{
			_execute((T)parameter);
		}
	}
}
