﻿using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.App.Pos.Model
{

	public enum DownUpStatus { Unknown, Ok, Old}

    public class DownUpLoadData : BindableBase
	{
        public string Description 
        {
            get { return DescriptionProperty; }
            set { SetProperty(ref DescriptionProperty, value); }
        }

        private string DescriptionProperty;

        public object Tag
        {
            get { return TagProperty; }
            set { SetProperty(ref TagProperty, value); }
        }

		private object TagProperty;

        public DownUpStatus Status
        {
            get { return StatusProperty; }
            set { SetProperty(ref StatusProperty, value); }
        }

        private DownUpStatus StatusProperty;



        public bool IsSelected
        {
            get { return IsSelectedProperty; }
            set { SetProperty(ref IsSelectedProperty, value); }
        }

        private bool IsSelectedProperty;

    }
}
