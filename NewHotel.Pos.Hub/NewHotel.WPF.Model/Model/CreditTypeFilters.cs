﻿using System.Windows.Data;
using NewHotel.Contracts;
using NewHotel.Contracts.DataProvider;
using NewHotel.Pos.Hub.Contracts.Common.Records;
using NewHotel.WPF.Common;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.Model.Model
{
    public class CreditTypeFilters : BindableBase
    {
        public CollectionView? View
        {
            get => _viewProperty;
            set => SetProperty(ref _viewProperty, value);
        }

        private CollectionView? _viewProperty;
    }

    public class ReservationFilters : CreditTypeFilters
    {
        public string ReservationNumber
        {
            get => _reservationProperty;
            set => SetProperty(ref _reservationProperty, value);
        }

        private string _reservationProperty;

        public string Room
        {
            get => _roomProperty;
            set => SetProperty(ref _roomProperty, value);
        }

        private string _roomProperty;

        public string Guest
        {
            get => _guestProperty;
            set => SetProperty(ref _guestProperty, value);
        }

        private string _guestProperty;

        public string CardNumber
        {
            get => _cardNumberProperty;
            set => SetProperty(ref _cardNumberProperty, value);
        }

        private string _cardNumberProperty;

        public bool IsCheckIn
        {
            get => _isCheckInProperty;
            set => SetProperty(ref _isCheckInProperty, value);
        }

        private bool _isCheckInProperty;

        public bool ShowAccountBalance
        {
            get => _showAccountBalanceProperty;
            set => SetProperty(ref _showAccountBalanceProperty, value);
        }

        private bool _showAccountBalanceProperty;

        public bool ShowAllGuests
        {
            get => _showAllGuestsProperty;
            set => SetProperty(ref _showAllGuestsProperty, value);
        }

        private bool _showAllGuestsProperty;

        public string Code
        {
            get => _codeProperty;
            set => SetProperty(ref _codeProperty, value);
        }

        private string _codeProperty;

        protected override void OnPropertyChanged(string propertyName = null)
        {
            if (View != null)
            {
                View.Filter = x =>
                {
                    var value = (ReservationRecord)x;

                    if (IsCheckIn && value.State != ReservationState.CheckIn)
                        return false;

                    if (!string.IsNullOrEmpty(ReservationNumber) && !value.ReservationNumber.ToLower().Contains(ReservationNumber!.ToLower()))
                        return false;

                    if (!string.IsNullOrEmpty(Room) && !value.Room.ToLower().Contains(Room!.ToLower()))
                        return false;

                    if (!string.IsNullOrEmpty(Guest) && !value.Guests.ToLower().Contains(Guest!.ToLower()))
                        return false;

                    if (!string.IsNullOrEmpty(CardNumber) && !value.CardNumbers.ToLower().Contains(CardNumber!.ToLower()))
                        return false;

                    return true;
                };
            }

            base.OnPropertyChanged(propertyName);
        }
    }

    public class EntityFilters : CreditTypeFilters
    {
        public string Name
        {
            get => _nameProperty;
            set => SetProperty(ref _nameProperty, value);
        }

        private string _nameProperty;

        public string FiscalNumber
        {
            get => _fiscalNumberProperty;
            set => SetProperty(ref _fiscalNumberProperty, value);
        }

        private string _fiscalNumberProperty;

        public string Abbreviation
        {
            get => _abbreviationProperty;
            set => SetProperty(ref _abbreviationProperty, value);
        }

        private string _abbreviationProperty;

        protected override void OnPropertyChanged(string propertyName = null)
        {
            if (View != null)
            {
                View.Filter = x =>
                {
                    var value = (PmsCompanyRecord)x;
                    if (!string.IsNullOrEmpty(Name) && !value.CompanyName.ToLower().Contains(Name!.ToLower()))
                        return false;
                    if (!string.IsNullOrEmpty(FiscalNumber) && !value.FiscalNumber.ToLower().Contains(FiscalNumber!.ToLower()))
                        return false;
                    if (!string.IsNullOrEmpty(Abbreviation) && !value.Abbreviation.ToLower().Contains(Abbreviation!.ToLower()))
                        return false;

                    return true;
                };
            }

            base.OnPropertyChanged(propertyName);
        }
    }

    public class GroupFilters : CreditTypeFilters
    {
        public string GroupName
        {
            get { return GroupNameProperty; }
            set { SetProperty(ref GroupNameProperty, value); }
        }

        private string GroupNameProperty;

        public string Room
        {
            get { return RoomProperty; }
            set { SetProperty(ref RoomProperty, value); }
        }

        private string RoomProperty;

        protected override void OnPropertyChanged(string propertyName)
        {
            if (View != null)
            {
                if (!string.IsNullOrEmpty(GroupName) || !string.IsNullOrEmpty(Room))
                {
                    View.Filter = (object x) =>
                    {
                        if (!string.IsNullOrEmpty(GroupName))
                        {
                            var value = x.GetPropertyValue<object>("Description");
                            if (value == null || !value.ToString().ToLower().Contains(GroupName.ToLower()))
                                return false;
                        }

                        if (!string.IsNullOrEmpty(Room))
                        {
                            var value = x.GetPropertyValue<object>("Rooms");
                            if (value == null || !value.ToString().ToLower().Contains(Room.ToLower()))
                                return false;
                        }

                        return true;
                    };
                }
                else
                    View.Filter = null;

                if (View.NeedsRefresh)
                    View.Refresh();
            }

            base.OnPropertyChanged(propertyName);
        }
    }

    public class ClientFilters : CreditTypeFilters
    {
        public string Name
        {
            get { return NameProperty; }
            set { SetProperty(ref NameProperty, value); }
        }

        private string NameProperty;

        public string Reservation
        {
            get { return ReservationProperty; }
            set { SetProperty(ref ReservationProperty, value); }
        }

        private string ReservationProperty;

        public string Room
        {
            get { return RoomProperty; }
            set { SetProperty(ref RoomProperty, value); }
        }

        private string RoomProperty;


        protected override void OnPropertyChanged(string propertyName)
        {
            if (View != null)
            {
                if (!string.IsNullOrEmpty(Name) || !string.IsNullOrEmpty(Reservation) || !string.IsNullOrEmpty(Room))
                {
                    View.Filter = (object x) =>
                    {
                        if (!string.IsNullOrEmpty(Name))
                        {
                            var value = x.GetPropertyValue<object>("Name");
                            if (value == null || !value.ToString().ToLower().Contains(Name.ToLower()))
                                return false;
                        }

                        if (!string.IsNullOrEmpty(Reservation))
                        {
                            var value = x.GetPropertyValue<object>("Reservation");
                            if (value == null || !value.ToString().ToLower().Contains(Reservation.ToLower()))
                                return false;
                        }

                        if (!string.IsNullOrEmpty(Room))
                        {
                            var value = x.GetPropertyValue<object>("Room");
                            if (value == null || !value.ToString().ToLower().Contains(Room.ToLower()))
                                return false;
                        }

                        return true;
                    };
                }
                else
                    View.Filter = null;

                if (View.NeedsRefresh)
                    View.Refresh();
            }

            base.OnPropertyChanged(propertyName);
        }
    }

    public class ControlFilters : CreditTypeFilters
    {
        public string Name
        {
            get => _nameProperty;
            set => SetProperty(ref _nameProperty, value);
        }

        private string _nameProperty;

        public string Email
        {
            get => _emailProperty;
            set => SetProperty(ref _emailProperty, value);
        }

        private string _emailProperty;

        public string FreeCode
        {
            get => _freeCodeProperty;
            set => SetProperty(ref _freeCodeProperty, value);
        }

        private string _freeCodeProperty;

        public string FiscalNumber
        {
            get => _fiscalNumberProperty;
            set => SetProperty(ref _fiscalNumberProperty, value);
        }

        private string _fiscalNumberProperty;

        protected override void OnPropertyChanged(string propertyName = null)
        {
            if (View != null)
            {
                View.Filter = x =>
                {
                    var value = (PmsControlAccountRecord)x;

                    if (!string.IsNullOrEmpty(Name) && !value.AccountName.ToLower().Contains(Name!.ToLower()))
                        return false;

                    if (!string.IsNullOrEmpty(Email) && !value.AccountEmail.ToLower().Contains(Email!.ToLower()))
                        return false;

                    if (!string.IsNullOrEmpty(FreeCode) && !value.FreeCode.ToLower().Contains(FreeCode!.ToLower()))
                        return false;

                    if (!string.IsNullOrEmpty(FiscalNumber) && !value.FiscalNumber.ToLower().Contains(FiscalNumber!.ToLower()))
                        return false;

                    return true;
                };
            }

            base.OnPropertyChanged(propertyName);
        }
    }

    public class GuestFilters : CreditTypeFilters
    {
        public string Reservation
        {
            get { return ReservationProperty; }
            set { SetProperty(ref ReservationProperty, value); }
        }

        private string ReservationProperty;

        public string Room
        {
            get { return RoomProperty; }
            set { SetProperty(ref RoomProperty, value); }
        }

        private string RoomProperty;

        public string Guest
        {
            get { return GuestProperty; }
            set { SetProperty(ref GuestProperty, value); }
        }

        private string GuestProperty;

        protected override void OnPropertyChanged(string propertyName)
        {
            if (View != null)
            {
                if (!string.IsNullOrEmpty(Reservation) || !string.IsNullOrEmpty(Room) || !string.IsNullOrEmpty(Guest))
                {
                    View.Filter = (object x) =>
                    {
                        if (!string.IsNullOrEmpty(Reservation))
                        {
                            var value = x.GetPropertyValue<object>("ReservationNumber");
                            if (value == null || !value.ToString().ToLower().Contains(Reservation.ToLower()))
                                return false;
                        }

                        if (!string.IsNullOrEmpty(Room))
                        {
                            var value = x.GetPropertyValue<object>("Room");
                            if (value == null || !value.ToString().ToLower().Contains(Room.ToLower()))
                                return false;
                        }

                        if (!string.IsNullOrEmpty(Guest))
                        {
                            var value = x.GetPropertyValue<object>("Guest");
                            if (value == null || !value.ToString().ToLower().Contains(Guest.ToLower()))
                                return false;
                        }

                        return true;
                    };
                }
                else
                    View.Filter = null;

                if (View.NeedsRefresh)
                    View.Refresh();
            }

            base.OnPropertyChanged(propertyName);
        }
    }

    public class EventFilters : CreditTypeFilters
    {
        public string Name
        {
            get { return NameProperty; }
            set { SetProperty(ref NameProperty, value); }
        }

        private string NameProperty;

        public string Reservation
        {
            get { return ReservationProperty; }
            set { SetProperty(ref ReservationProperty, value); }
        }

        private string ReservationProperty;

        protected override void OnPropertyChanged(string propertyName)
        {
            if (View != null)
            {
                if (!string.IsNullOrEmpty(Name) || !string.IsNullOrEmpty(Reservation))
                {
                    View.Filter = (object x) =>
                    {
                        if (!string.IsNullOrEmpty(Name))
                        {
                            var value = x.GetPropertyValue<object>("EventName");
                            if (value == null || !value.ToString().ToLower().Contains(Name.ToLower()))
                                return false;
                        }

                        if (!string.IsNullOrEmpty(Reservation))
                        {
                            var value = x.GetPropertyValue<object>("ReservationNumber");
                            if (value == null || !value.ToString().ToLower().Contains(Reservation.ToLower()))
                                return false;
                        }

                        return true;
                    };
                }
                else
                    View.Filter = null;

                if (View.NeedsRefresh)
                    View.Refresh();
            }

            base.OnPropertyChanged(propertyName);
        }
    }

    public class SpaFilters : CreditTypeFilters
    {
        public string Guest
        {
            get { return GuestProperty; }
            set { SetProperty(ref GuestProperty, value); }
        }

        private string GuestProperty;

        public string Reservation
        {
            get { return ReservationProperty; }
            set { SetProperty(ref ReservationProperty, value); }
        }

        private string ReservationProperty;
        public string Room
        {
            get { return RoomProperty; }
            set { SetProperty(ref RoomProperty, value); }
        }

        private string RoomProperty;

        public string CardNumber
        {
            get => CardNumberProperty;
            set => SetProperty(ref CardNumberProperty, value);
        }

        private string CardNumberProperty;

        protected override void OnPropertyChanged(string propertyName)
        {
            if (View != null)
            {
                if (!string.IsNullOrEmpty(Guest) || !string.IsNullOrEmpty(Reservation) || !string.IsNullOrEmpty(Room) || !string.IsNullOrEmpty(CardNumber))
                {
                    View.Filter = x =>
                    {
                        if (!string.IsNullOrEmpty(Guest))
                        {
                            var value = x.GetPropertyValue<object>("Guest");
                            if (value == null || !value.ToString().ToLower().Contains(Guest.ToLower()))
                                return false;
                        }

                        if (!string.IsNullOrEmpty(Reservation))
                        {
                            var value = x.GetPropertyValue<object>("ReservationNumber");
                            if (value == null || !value.ToString().ToLower().Contains(Reservation.ToLower()))
                                return false;
                        }

                        if (!string.IsNullOrEmpty(Room))
                        {
                            var value = x.GetPropertyValue<object>("Room");
                            if (value == null || !value.ToString().ToLower().Contains(Room.ToLower()))
                                return false;
                        }

                        if (!string.IsNullOrEmpty(CardNumber))
                        {
                            var value = x.GetPropertyValue<object>("CardNumber");
                            if (value == null || !value.ToString().ToLower().Contains(CardNumber.ToLower()))
                                return false;
                        }

                        return true;
                    };
                }
                else
                    View.Filter = null;

                if (View.NeedsRefresh)
                    View.Refresh();
            }

            base.OnPropertyChanged(propertyName);
        }
    }
}