﻿using NewHotel.WPF.MVVM;
using System;

namespace NewHotel.WPF.App.Pos.Model
{
	public class CancellationReason : BindableBase
	{

        #region Dependency Properties

        private string DescriptionProperty;

        private long TypeProperty;

        #endregion

        #region Constructors

        public CancellationReason()
        {

        }

        #endregion

        #region Properties

        public Guid Id { get; set; }

        public string Description
        {
            get { return DescriptionProperty; }
            set { SetProperty(ref DescriptionProperty, value); }
        }

        public long Type
        {
            get { return TypeProperty; }
            set { SetProperty(ref TypeProperty, value); }
        }


        #endregion

    }
}
