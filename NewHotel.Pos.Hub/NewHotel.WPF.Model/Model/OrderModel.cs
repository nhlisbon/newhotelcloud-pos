﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using NewHotel.Contracts;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Core;
using NewHotel.Pos.Core.Ext;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Localization;
using NewHotel.Pos.Printer;
using NewHotel.Pos.Printer.Interfaces;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using NewHotel.WPF.MVVM;
using DiscountType = NewHotel.WPF.App.Pos.Model.DiscountType;

namespace NewHotel.WPF.Model.Model
{
    public class Order : BindableBase, IPrintableOrder
    {
        #region Variables

        private POSProductLineContract _prodLine;
        private decimal? _productPriceBeforeDiscountOverride;
        private decimal? _productPriceOverride;
        private ObservableCollection<Preparation> _orderPreparations;
        private ObservableCollection<Area> _orderAreas;
        private Discount _discountType;
        private int _noRaiseEventsModeActivated;
        private Product _product;
        private bool _isSelected;
        private bool _isMenuExpanded;
        private decimal? _incrementProductQuantity;
        private Separator _separatorModel;
        private ObservableCollection<OrderTable>? _orderTables;

        #endregion

        #region Constructors

        public Order(Ticket ticket, POSProductLineContract prodLine)
        {
            Ticket = ticket;
            Initialize(prodLine);
        }

        public Order()
        {
            Initialize(new POSProductLineContract());
        }

        public Order(Guid newId)
        {
            Initialize(new POSProductLineContract { Id = newId });
        }

        #region Tip

        public static Order CreateTipOrder(Stand stand, Ticket ticket, Product tipProduct)
        {
            var userId = ticket.UserId != Guid.Empty ? ticket.UserId : Hotel.Instance.UserId;
            var order = new Order(ticket, userId, tipProduct, stand.TipSeparator);
            return order.AsContract != null ? order : null;
        }

        private Order(Ticket ticket, Guid userId, Product tipProduct, Separator tipSeparator)
        {
            Ticket = ticket;

            var standardPrice = tipProduct.StandardPrice ?? decimal.Zero;
            var taxDefinition = tipProduct.GetTaxLocal(ticket.TaxSchemaId, ticket.TaxIncluded, standardPrice);
            if (taxDefinition != null)
            {
                var contract = new POSProductLineContract();
                contract.Id = Guid.NewGuid();
                contract.ProductQtd = decimal.One;
                contract.NetValue = decimal.Zero;
                contract.CostValue = decimal.Zero;
                contract.GrossValue = standardPrice;
                contract.ValueBeforeDiscount = standardPrice;
                contract.DiscountValue = decimal.Zero;
                contract.DiscountPercent = null;
                contract.Recharge = decimal.Zero;
                contract.AnnulmentCode = ProductLineCancellationStatus.Active;
                contract.AnulAfterClosed = false;
                contract.SendToAreaStatus = 0;
                contract.SendToAreaStartTime = null;
                contract.SendToAreaEndTime = null;
                contract.Printed = 0;
                contract.IsHappyHour = tipProduct.IsHappyHour;
                contract.IsAditional = false;
                contract.MainProduct = tipProduct.Id;
                contract.FirstIvaId = taxDefinition.TaxDetailIva1.TaxRateId;
                contract.FirstIvaPercent = taxDefinition.TaxDetailIva1.TaxPercent;
                contract.FirstIvaValue = taxDefinition.TaxDetailIva1.TaxValue;
                contract.SecondIvaId = taxDefinition.TaxDetailIva2?.TaxRateId;
                contract.SecondIvaPercent = taxDefinition.TaxDetailIva2?.TaxPercent;
                contract.SecondIvaValue = taxDefinition.TaxDetailIva2?.TaxValue;
                contract.ThirdIvaId = taxDefinition.TaxDetailIva3?.TaxRateId;
                contract.ThirdIvaPercent = taxDefinition.TaxDetailIva3?.TaxPercent;
                contract.ThirdIvaValue = taxDefinition.TaxDetailIva2?.TaxValue;
                contract.Separator = tipSeparator.Id;
                contract.SeparatorDescription = tipSeparator.Description;
                contract.AreaId = null;
                contract.Areas.Clear();
                contract.Product = tipProduct.Id;
                contract.ProductCode = tipProduct.Code;
                contract.ProductAbbreviation = tipProduct.Abre;
                contract.ProductDescription = tipProduct.Description;
                contract.Caja = ticket.Cashier;
                contract.Stand = ticket.Stand;
                contract.DiscountTypeId = null;
                contract.HasManualSeparator = false;
                contract.HasManualPrice = false;
                contract.AutomaticDiscount = false;
                contract.IsTip = false;
                contract.PaxNumber = null;
                contract.Observations = string.Empty;
                contract.ObservationsMarch = string.Empty;
                contract.UserId = userId;
                contract.UserDescription = string.Empty;
                contract.Alcoholic = null;
                contract.ItemNumber = 0;
                contract.LastModified = tipProduct.LastModified;

                Initialize(contract);
            }
        }

        #endregion

        #endregion

        #region Properties

        #region Contract Context

        public override object DataContext
        {
            get
            {
                if (_prodLine != null)
                {
                    CopyPreparationsToDataContext();
                    //CopyAreasToDataContext();
                }

                return _prodLine;
            }
            set
            {
                _prodLine = (POSProductLineContract)value;
                InitializeModels(_prodLine);
                // RaiseAllPropertyChanged();
            }
        }

        public POSProductLineContract AsContract => DataContext as POSProductLineContract;

        public Guid? AreaId
        {
            get => _prodLine.AreaId;
            set
            {
                SetProperty(_prodLine.AreaId, value, x => _prodLine.AreaId = x);
                //_prodLine.AreaId = value;
                //OnPropertyChanged(nameof(AreaId));
            }
        }

        public TypedList<POSProductLineAreaContract> Areas
        {
            get => _prodLine.Areas;
            set
            {
                SetProperty(_prodLine.Areas, value, x => _prodLine.Areas = x);
                //_prodLine.Areas = value;
                //OnPropertyChanged(nameof(Areas));
            }
        }

        public IEnumerable<Guid> AreasId
        {
            get
            {
                var areas = new List<Guid>(Areas.Select(a => a.AreaId));
                if (AreaId.HasValue)
                    areas.Add(AreaId.Value);

                return areas;
            }
        }

        public bool HasAreas => AreaId.HasValue || Areas.Count > 0;

        private DateTime LastModified
        {
            get => _prodLine.LastModified;
            set => _prodLine.LastModified = value;
        }

        public short ItemNumber
        {
            get => _prodLine.ItemNumber;
            set
            {
                SetProperty(_prodLine.ItemNumber, value, x => _prodLine.ItemNumber = x);
                //_prodLine.ItemNumber = value;
                //OnPropertyChanged(nameof(ItemNumber));
            }
        }

        public Guid Id
        {
            get => (Guid)_prodLine.Id;
            // set => _prodLine.Id = value;
        }

        private Guid TicketId
        {
            get => _prodLine.Ticket;
            set
            {
                var refValue = _prodLine.Ticket;
                SetProperty(ref refValue, value, () => _prodLine.Ticket = refValue);
            }
        }

        private Guid Stand
        {
            get => _prodLine.Stand;
            set
            {
                var refValue = _prodLine.Stand;
                SetProperty(ref refValue, value, () => _prodLine.Stand = refValue);
            }
        }

        private Guid Cashier
        {
            get => _prodLine.Caja;
            set
            {
                var refValue = _prodLine.Caja;
                SetProperty(ref refValue, value, () => _prodLine.Caja = refValue);
            }
        }

        public Guid ProductId
        {
            get => _prodLine.Product;
            private set
            {
                var refValue = _prodLine.Product;
                SetProperty(ref refValue, value, () => _prodLine.Product = refValue);
            }
        }
        /// <summary>
        /// Property to show or hide the decimal case in the quantity of the product in the order/ticket
        /// </summary>
        public bool ShowQuantityDecimalCase => Hotel.Instance.ShowQuantityDecimalCase;

        public decimal Quantity
        {
            get => _prodLine.ProductQtd;
            set
            {
                var oldValue = Quantity;
                var newValue = value;
                var initialProductQuantity = oldValue;
                SetProperty(ref oldValue, newValue, () =>
                {
                    CostBeforeDiscount = (ProductPriceBeforeDiscount * newValue);

                    _prodLine.ProductQtd = newValue;
                    if (_prodLine.TableLines.Count > 0)
                    {
                        foreach (var tableLine in _prodLine.TableLines)
                        {
                            tableLine.Quantity = tableLine.Quantity / (initialProductQuantity != 0 ? initialProductQuantity : 1) * newValue;
                        }
                    }
                    
                    UpdateIncrementProductQuantity();
                    RecalculateCalculateTaxs();
                });
            }
        }

        /// <summary>
        /// Precio del producto antes de aplicar descuentos
        /// </summary>
        public decimal ProductPriceBeforeDiscount
        {
            get => !_productPriceBeforeDiscountOverride.HasValue && Quantity > 0
                ? (CostBeforeDiscount / Quantity)
                : _productPriceBeforeDiscountOverride ?? 0;
            set => SetProperty(ref _productPriceBeforeDiscountOverride, value);
        }

        /// <summary>
        /// Precio del producto con descuentos aplicados
        /// </summary>
        public decimal ProductPrice
        {
            get => _productPriceOverride ?? ProductPriceBeforeDiscount;
            set => SetProperty(ref _productPriceOverride, value);
        }

        /// <summary>
        /// Valor neto del producto antes de aplicar descuentos
        /// </summary>
        public decimal ProductNet
        {
            get
            {
                var productTax = Product?.GetTaxLocal(TaxSchemaId, TaxIncluded, ProductPriceBeforeDiscount);
                return productTax.NetValue;
            }
        }

        /// <summary>
        /// Valor con impuestos del producto antes de aplicar descuentos
        /// </summary>
        public decimal ProductGross
        {
            get
            {
                var productTax = Product?.GetTaxLocal(TaxSchemaId, TaxIncluded, ProductPriceBeforeDiscount);
                return productTax.GrossValue;
            }
        }

        /// <summary>
        /// Description order
        /// </summary>
        public string ProductDescription
        {
            get
            {
                var description = _prodLine.ProductDescription;
                if (_prodLine.MainProduct.HasValue)
                    description = "(+) " + description;
                if (_prodLine.ManualPriceDesc != null)
                    description += " " + _prodLine.ManualPriceDesc;

                return description;
            }
        }


        /// <summary>
        /// Costo de la orden antes de hacer descuentos.
        /// Costo original del producto por la cantidad
        /// </summary>
        public decimal CostBeforeDiscount
        {
            get => _prodLine.ValueBeforeDiscount;
            set
            {
                var refValue = _prodLine.ValueBeforeDiscount;
                SetProperty(ref refValue, value, () => { _prodLine.ValueBeforeDiscount = refValue; });
            }
        }

        public decimal GrossInBaseCurrency => _prodLine.GrossValueInBase; //(CostBeforeDiscount * Ticket.ExchangeFactor).Round2();
        public bool HasGrossInBaseCurrency => Gross != GrossInBaseCurrency; //CostBeforeDiscount != 0 && Ticket.ExchangeFactor != 1;

        /// <summary>
        /// Costo final de la orden despues de hacer descuentos y recargos
        /// CostBeforeDiscount - DiscountTotal + Reshargues
        /// </summary>
        public decimal Cost
        {
            get => _prodLine.CostValue;
            set
            {
                var refValue = _prodLine.CostValue;
                SetProperty(ref refValue, value, () => _prodLine.CostValue = refValue);
            }
        }

        /// <summary>
        /// Valor Gross(Impuestos incluidos) de la orden. Gross del producto * cantidad 
        /// </summary>
        public decimal Gross
        {
            get => _prodLine.GrossValue;
            set
            {
                var refValue = _prodLine.GrossValue;
                SetProperty(ref refValue, value, () => _prodLine.GrossValue = refValue);
            }
        }

        /// <summary>
        /// Valor Net(Sin impuestos incluidos) de la orden. Net del producto * cantidad 
        /// </summary>
        public decimal Net
        {
            get => _prodLine.NetValue;
            set
            {
                var refValue = _prodLine.NetValue;
                SetProperty(ref refValue, value, () => _prodLine.NetValue = refValue);
            }
        }

        /// <summary>
        /// % de descuento aplicado al pricio del producto
        /// </summary>
        public decimal DiscountPercent
        {
            get => _prodLine.DiscountPercent ?? decimal.Zero;
            set
            {
                var refValue = _prodLine.DiscountPercent;
                SetProperty(ref refValue, value, () => _prodLine.DiscountPercent = refValue);
            }
        }


        /// <summary>
        /// Descuento aplicado al producto
        /// </summary>
        public decimal DiscountValue
        {
            get => _prodLine.DiscountValue;
            set
            {
                var refValue = _prodLine.DiscountValue;
                SetProperty(ref refValue, value, () => _prodLine.DiscountValue = refValue);
            }
        }


        /// <summary>
        /// Descripcion de descuento aplicado al producto
        /// </summary>
        public string DiscountTypeDescription
        {
            get => _prodLine.DiscountTypeDescription;
            set
            {
                var refValue = _prodLine.DiscountTypeDescription;
                SetProperty(ref refValue, value, () => _prodLine.DiscountTypeDescription = refValue);
            }
        }

        private decimal Recharge
        {
            get => _prodLine.Recharge;
            set
            {
                var refValue = _prodLine.Recharge;
                SetProperty(ref refValue, value, () => _prodLine.Recharge = refValue);
            }
        }

        public bool AnulAfterClosed => _prodLine.AnulAfterClosed;

        public short CancellationStatus
        {
            get => _prodLine.AnnulmentCode;
            set
            {
                var refValue = _prodLine.AnnulmentCode;
                SetProperty(ref refValue, value, () => _prodLine.AnnulmentCode = refValue);
            }
        }

        public Guid? UtilCancellation
        {
            get => _prodLine.AnnulUserId;
            set
            {
                var refValue = _prodLine.AnnulUserId;
                SetProperty(ref refValue, value, () => _prodLine.AnnulUserId = refValue);
            }
        }

        private Guid Util
        {
            get => _prodLine.UserId;
            set
            {
                var refValue = _prodLine.UserId;
                SetProperty(ref refValue, value, () => _prodLine.UserId = refValue);
            }
        }

        private bool Aditional
        {
            get => _prodLine.IsAditional;
            set
            {
                var refValue = _prodLine.IsAditional;
                SetProperty(ref refValue, value, () => _prodLine.IsAditional = refValue);
            }
        }

        public short SendToAreaStatus
        {
            get => _prodLine.SendToAreaStatus;
            set
            {
                //var refValue = _prodLine.SendToAreaStatus;
                //if (SetProperty(ref refValue, value, () => _prodLine.SendToAreaStatus = refValue))
                if (SetProperty(_prodLine.SendToAreaStatus, value, x => _prodLine.SendToAreaStatus = x))
                {
                    OnPropertyChanged(nameof(NotMarch));
                    OnPropertyChanged(nameof(March));
                    OnPropertyChanged(nameof(MarchProcessing));
                    OnPropertyChanged(nameof(MarchCompleted));
                    OnPropertyChanged(nameof(MarchDispatched));
                    OnPropertyChanged(nameof(MarchAway));
                }
            }
        }

        public DateTime? SendToAreaStartTime
        {
            get => _prodLine.SendToAreaStartTime;
            set
            {
                var refValue = _prodLine.SendToAreaStartTime;
                SetProperty(ref refValue, value, () => _prodLine.SendToAreaStartTime = refValue);
            }
        }

        public DateTime? SendToAreaEndTime
        {
            get => _prodLine.SendToAreaEndTime;
            set
            {
                var refValue = _prodLine.SendToAreaEndTime;
                SetProperty(ref refValue, value, () => _prodLine.SendToAreaEndTime = refValue);
            }
        }

        public bool AvailableToMarch
        {
            get
            {
                bool isNotMarched;
                if (!IsMenu)
                {
                    isNotMarched = SendToAreaStatus == 0;
                }
                else
                {
                    isNotMarched = OrderTables.Any(o => o.NotMarch);
                }

                return isNotMarched;
            }
        }

        public bool AvailableToVoid
        {
            get
            {
                bool isMarched;
                if (!IsMenu)
                {
                    isMarched = SendToAreaStatus != 0;
                }
                else
                {
                    isMarched = OrderTables.Any(o => !o.NotMarch);
                }

                return isMarched;
            }
        }

        public bool PendingToChooseArea
        {
            get
            {
                bool pending;
                if (!IsMenu)
                {
                    pending = AreaId == null || AreaId == Guid.Empty;
                }
                else
                {
                    pending = OrderTablesPendingToDispatch.Any(o => o.AreaId == null || o.AreaId == Guid.Empty);
                }

                return pending;
            }
        }

        public bool NotMarch
        {
            get
            {
                bool isNotMarched;
                if (!IsMenu)
                {
                    isNotMarched = SendToAreaStatus == 0;
                }
                else
                {
                    isNotMarched = OrderTables.Any(o => o.NotMarch);
                }

                return isNotMarched;
            }
        }

        public bool March => !IsMenu ? SendToAreaStatus == 1 : OrderTables.Any(o => o.March);

        public bool MarchProcessing => SendToAreaStatus == 2;

        public bool MarchCompleted => SendToAreaStatus == 3;

        public bool MarchDispatched => SendToAreaStatus == 4;

        public bool MarchAway => !IsMenu ? SendToAreaStatus == 5 : OrderTables.Any(o => o.MarchAway);

        public short Printed
        {
            get => _prodLine.Printed;
            set
            {
                var refValue = _prodLine.Printed;
                SetProperty(ref refValue, value, () => _prodLine.Printed = refValue);
            }
        }

        public Guid? Separator
        {
            get => _prodLine.Separator;
            set
            {
                var refValue = _prodLine.Separator;
                SetProperty(ref refValue, value, () => _prodLine.Separator = refValue);
            }
        }


        Order _mainOrder;
        Order MainOrder
		{
			get
			{
                if (ImMain)
					return this;

				return _mainOrder ??= Ticket.Orders.FirstOrDefault(o => o.Id == MainProduct);
			}
		}

        bool ImMain => !_prodLine.MainProduct.HasValue;

		public Guid MainProduct => _prodLine.MainProduct ?? (Guid)_prodLine.Id;

        // public int MainProductOrder => (ItemNumber * 10) + (MainProduct == Id ? 1 : 2);
        public int MainProductOrder => (MainOrder.ItemNumber * 10) + (ImMain ? 1 : 2);

        public string Notes
        {
            get => _prodLine.Observations;
            set
            {
                var refValue = _prodLine.Observations;
                SetProperty(ref refValue, value, () => _prodLine.Observations = refValue);
            }
        }

        public string ObservationsMarch
        {
            get => _prodLine.ObservationsMarch;
            set
            {
                var refValue = _prodLine.ObservationsMarch;
                SetProperty(ref refValue, value, () => _prodLine.ObservationsMarch = refValue);
            }
        }

        public bool IsHappyHour
        {
            get => _prodLine.IsHappyHour;
            set
            {
                var refValue = _prodLine.IsHappyHour;
                SetProperty(ref refValue, value);
                _prodLine.IsHappyHour = value;
            }
        }

        public bool HasDiscount => DiscountModel != null;

        public string OrderAllPreparations => string.Join(", ", _prodLine.Preparations.Select(p => p.PreparationDescription));

        public bool OrderHasPreparations => _prodLine.Preparations.Count > 0;

        public bool OrderHasNotes => !string.IsNullOrEmpty(_prodLine.Observations);

        public Discount DiscountModel
        {
            get => _discountType;
            set
            {
                SetProperty(ref _discountType, value);

                if (IsNoRaiseEventsModeActivated) return;

                if (value == null)
                    DiscountPercent = 0;

                DiscountType = value?.Type.Id;
                RecalculateDiscountAndTaxs();
            }
        }

        public bool ShowDiscountInfo => DiscountValue > decimal.Zero;

        public Guid? DiscountType
        {
            get => _prodLine.DiscountTypeId;
            private set
            {
                var refValue = _prodLine.DiscountTypeId;
                SetProperty(ref refValue, value, () => _prodLine.DiscountTypeId = refValue);
            }
        }

        #region Taxs

        public decimal TotalTaxes => FirstIvas + (SecondIvas ?? 0) + (ThirdIvas ?? 0);

        #region First

        public Guid FirstIvaCode
        {
            get => _prodLine.FirstIvaId;
            set
            {
                var refValue = _prodLine.FirstIvaId;
                SetProperty(ref refValue, value, () => _prodLine.FirstIvaId = refValue);
            }
        }

        public string FirstIvaAuxCode
        {
            get => _prodLine.FirstIvaAuxCode;
            private set
            {
                var refValue = _prodLine.FirstIvaAuxCode;
                SetProperty(ref refValue, value, () => _prodLine.FirstIvaAuxCode = refValue);
            }
        }

        public string FirstIvaDescription
        {
            get => _prodLine.FirstIvaDescription;
            set
            {
                var refValue = _prodLine.FirstIvaDescription;
                SetProperty(ref refValue, value, () => _prodLine.FirstIvaDescription = refValue);
            }
        }

        public decimal FirstIvas
        {
            get => _prodLine.FirstIvaValue;
            set
            {
                var refValue = _prodLine.FirstIvaValue;
                SetProperty(ref refValue, value, () => _prodLine.FirstIvaValue = refValue);
            }
        }

        public decimal FirstIvaBase
        {
            get => _prodLine.FirstIvaBase;
            private set
            {
                var refValue = _prodLine.FirstIvaBase;
                SetProperty(ref refValue, value, () => _prodLine.FirstIvaBase = refValue);
            }
        }

        public decimal FirstIvaPercent
        {
            get => _prodLine.FirstIvaPercent;
            private set
            {
                var refValue = _prodLine.FirstIvaPercent;
                SetProperty(ref refValue, value, () => _prodLine.FirstIvaPercent = refValue);
            }
        }

        #endregion

        #region Second

        public Guid? SecondIvaCode
        {
            get => _prodLine.SecondIvaId;
            set
            {
                var refValue = _prodLine.SecondIvaId;
                SetProperty(ref refValue, value, () => _prodLine.SecondIvaId = refValue);
            }
        }

        public string SecondIvaAuxCode
        {
            get => _prodLine.SecondIvaAuxCode;
            private set
            {
                var refValue = _prodLine.SecondIvaAuxCode;
                SetProperty(ref refValue, value, () => _prodLine.SecondIvaAuxCode = refValue);
            }
        }

        public string SecondIvaDescription
        {
            get => _prodLine.SecondIvaDescription;
            set
            {
                var refValue = _prodLine.SecondIvaDescription;
                SetProperty(ref refValue, value, () => _prodLine.SecondIvaDescription = refValue);
            }
        }

        public decimal? SecondIvaBase
        {
            get => _prodLine.SecondIvaBase;
            set
            {
                var refValue = _prodLine.SecondIvaBase;
                SetProperty(ref refValue, value, () => _prodLine.SecondIvaBase = refValue);
            }
        }

        public decimal? SecondIvas
        {
            get => _prodLine.SecondIvaValue;
            set
            {
                var refValue = _prodLine.SecondIvaValue;
                SetProperty(ref refValue, value, () => _prodLine.SecondIvaValue = refValue);
            }
        }

        public decimal? SecondIvaPercent
        {
            get => _prodLine.SecondIvaPercent;
            private set
            {
                var refValue = _prodLine.SecondIvaPercent;
                SetProperty(ref refValue, value, () => _prodLine.SecondIvaPercent = refValue);
            }
        }

        #endregion

        #region Third

        public Guid? ThirdIvaCode
        {
            get => _prodLine.ThirdIvaId;
            set
            {
                var refValue = _prodLine.ThirdIvaId;
                SetProperty(ref refValue, value, () => _prodLine.ThirdIvaId = refValue);
            }
        }

        public string ThirdIvaAuxCode
        {
            get => _prodLine.ThirdIvaAuxCode;
            private set
            {
                var refValue = _prodLine.ThirdIvaAuxCode;
                SetProperty(ref refValue, value, () => _prodLine.ThirdIvaAuxCode = refValue);
            }
        }

        public string ThirdIvaDescription
        {
            get => _prodLine.ThirdIvaDescription;
            set
            {
                var refValue = _prodLine.ThirdIvaDescription;
                SetProperty(ref refValue, value, () => _prodLine.ThirdIvaDescription = refValue);
            }
        }

        public decimal? ThirdIvaBase
        {
            get => _prodLine.ThirdIvaBase;
            set
            {
                var refValue = _prodLine.ThirdIvaBase;
                SetProperty(ref refValue, value, () => _prodLine.ThirdIvaBase = refValue);
            }
        }

        public decimal? ThirdIvas
        {
            get => _prodLine.ThirdIvaValue;
            set
            {
                var refValue = _prodLine.ThirdIvaValue;
                SetProperty(ref refValue, value, () => _prodLine.ThirdIvaValue = refValue);
            }
        }

        public decimal? ThirdIvaPercent
        {
            get => _prodLine.ThirdIvaPercent;
            private set
            {
                var refValue = _prodLine.ThirdIvaPercent;
                SetProperty(ref refValue, value, () => _prodLine.ThirdIvaPercent = refValue);
            }
        }

        #endregion

        #endregion

        #endregion

        #region JustVisual

        public ObservableCollection<DiscountType> DiscountTypes => Hotel.Instance.DiscountTypes;

        private bool IsNoRaiseEventsModeActivated => _noRaiseEventsModeActivated > 0;

        public bool IsVisible => IsActive && ProductId != Hotel.Instance.CurrentStandCashierContext.Stand.TipService;
        
        public bool IsEditable => CancellationStatus == ProductLineCancellationStatus.Active && ProductId != Hotel.Instance.CurrentStandCashierContext.Stand.TipService;

        public bool IsTransfer => CancellationStatus == ProductLineCancellationStatus.ActiveByTransfer;

        public CancellationControl CancellationControl { get; set; }

        public string PaxName
        {
            get
            {
                var label = PaxNumber is null or 0 ? LocalizationMgr.Translation("Shared") : PaxNumber.ToString();
                // if (!string.IsNullOrEmpty(_clientInfo))
                //     label += " " + _clientInfo;
                return label;

                // var paxName = string.Empty;
                // if (_prodLine.PaxNumber.HasValue)
                // {
                //     if (_prodLine.PaxNumber.Value == 0)
                //         return LocalizationMgr.Translation("Shared");
                //     else
                //     {
                //         paxName += _prodLine.PaxNumber.Value.ToString();
                //         var client = Ticket.ClientsByPosition.FirstOrDefault(c => c.Pax == PaxNumber.Value);
                //         if (client != null)
                //         {
                //             paxName += $" {client.ClientName}";
                //             if (!string.IsNullOrEmpty(client.ClientRoom))
                //                 paxName += $": {LocalizationMgr.Translation("Room")} {client.ClientRoom}";
                //         }
                //     }
                // }
                //
                // return paxName;
            }
        }

        public Separator SeparatorModel
		{
			get => ImMain ? (_separatorModel ?? NewHotel.WPF.App.Pos.Model.Separator.EmptySeparator) : MainOrder?.SeparatorModel;
            set
            {
                if (ImMain)
					SetProperty(ref _separatorModel, value, nameof(SeparatorModel));
            }
        }

        public Product Product
        {
            get => _product;
            set => SetProperty(ref _product, value);
        }

        /// <summary>
        /// Muestra si la orden esta seleccionada
        /// </summary>
        public bool IsSelected
        {
            get { return OrderTables != null && IsMenuExpanded ? OrderTables.Any(t => t.IsSelected) : _isSelected; }
            set => SetProperty(ref _isSelected, value);
        }

        public bool HasManualSeparator
        {
            get => _prodLine.HasManualSeparator;
            set
            {
                SetProperty(_prodLine.HasManualSeparator, value, x => _prodLine.HasManualSeparator = x);
                //_prodLine.HasManualSeparator = value;
                //OnPropertyChanged(nameof(HasManualSeparator));
            }
        }

        public bool HasManualPrice
        {
            get => _prodLine.HasManualPrice;
            set
            {
                SetProperty(_prodLine.HasManualPrice, value, x => _prodLine.HasManualPrice = x);
                //_prodLine.HasManualPrice = value;
                //OnPropertyChanged(nameof(HasManualPrice));
            }
        }

        public bool AutomaticDiscount => _prodLine.AutomaticDiscount;

        public string DiscountLabel => _prodLine.AutomaticDiscount ? "a" : "m";

        public bool DiscountEnabled => !_prodLine.AutomaticDiscount;

        public bool HasPreparations => Preparations.Count > 0;

        public bool HasSeparators => Hotel.Instance.Separators.Count > 0;

        public bool IsActive => CancellationStatus == ProductLineCancellationStatus.Active ||
                                CancellationStatus == ProductLineCancellationStatus.ActiveByTransfer;

        public bool IsTip
        {
            get
            {
                try
                {
                    var tipService = Hotel.Instance.CurrentStandCashierContext.Stand.TipService;
                    return tipService.HasValue && tipService.Value == ProductId;
                }
                catch
                {
                    return false;
                }
            }
        }

        public short? PaxNumber
        {
            get => ImMain ? _prodLine.PaxNumber : MainOrder?.PaxNumber;
            set
            {
                if (ImMain)
                {
                    var refValue = _prodLine.PaxNumber;
                    SetProperty(ref refValue, value, () => _prodLine.PaxNumber = refValue);
                }
            }
        }

        /// <summary>
        /// Paso en el incremento/decremento de la cantidad del producto en la orden
        /// </summary>
        public decimal? IncrementProductQuantity
        {
            get => _incrementProductQuantity;
            set => SetProperty(ref _incrementProductQuantity, value, nameof(IncrementProductQuantity));
        }

        #region Preparations

        public ObservableCollection<Preparation> Preparations => Product?.ProductPreparations;

        public ObservableCollection<Preparation> OrderPreparations
        {
            get => _orderPreparations;
            private set => SetProperty(ref _orderPreparations, value);
        }

        public ObservableCollection<Area> OrderAreas
        {
            get => _orderAreas;
            private set => SetProperty(ref _orderAreas, value);
        }

        #endregion

        #endregion

        #region Table Properties

        public ObservableCollection<TableLineContract> OrderTableDetails => _prodLine.TableLines;

        public bool IsMenu => _prodLine?.TableLines != null && _prodLine.TableLines.Count > 0;

        public bool IsMenuExpanded
        {
            get => _isMenuExpanded;
            set
            {
                if (OrderTables != null)
                    SetProperty(ref _isMenuExpanded, value);
            }
        }

        public ObservableCollection<OrderTable>? OrderTables
        {
            get => _orderTables;
            set => SetProperty(ref _orderTables, value);
        }

        #endregion

        public Ticket Ticket { get; }

        public Guid TaxSchemaId => Ticket?.TaxSchemaId ?? Hotel.Instance.DefaultTaxSchema.Value;

        public bool TaxIncluded => Ticket?.TaxIncluded ?? Hotel.Instance.TaxIncluded;

        public string DiscountDescription => DiscountModel?.Type.Description;

        public bool? Alcoholic => _prodLine.Alcoholic;

        #endregion

        #region Events

        public event Action<Order, decimal> ImporteChanged;
        public event Action<Order, bool> IsSelectedChanged;

        #endregion

        #region Methods

        #region Public

        public void CleanDiscount()
        {
            if (HasDiscount)
                DiscountModel = null;
        }

        public Order Clone()
        {
            var serializedLine = Serializer.Serialize(_prodLine);
            var line = Serializer.Deserialize<POSProductLineContract>(serializedLine);

            // var line = new POSProductLineContract
            // {
            //     Id = Id,
            //     IsAditional = Aditional,
            //     Caja = Cashier,
            //     CostValue = Cost,
            //     DiscountValue = DiscountValue,
            //     FirstIvaId = FirstIvaCode,
            //     FirstIvaValue = FirstIvas,
            //     GrossValue = Gross,
            //     AnnulmentCode = CancellationStatus,
            //     Printed = Printed,
            //     ObservationsMarch = ObservationsMarch,
            //     Recharge = Recharge,
            //     SecondIvaId = SecondIvaCode,
            //     SecondIvaValue = SecondIvas,
            //     ThirdIvaId = ThirdIvaCode,
            //     ThirdIvaValue = ThirdIvas,
            //     Separator = Separator,
            //     Stand = Stand,
            //     Ticket = TicketId,
            //     UserId = Util,
            //     SendToAreaStatus = SendToAreaStatus,
            //     Product = ProductId,
            //     ProductQtd = Quantity,
            //     MainProduct = MainProduct,
            //     ItemNumber = ItemNumber,
            //     Observations = Notes,
            //     IsHappyHour = IsHappyHour,
            //     AreaId = AreaId,
            //     NetValue = Net,
            //     DiscountPercent = DiscountPercent,
            //     ValueBeforeDiscount = CostBeforeDiscount,
            //     DiscountTypeId = DiscountType,
            //     HasManualSeparator = HasManualSeparator,
            //     HasManualPrice = HasManualPrice,
            //     AutomaticDiscount = AutomaticDiscount,
            //     TableLines = new TypedList<TableLineContract>(OrderTableDetails),
            //     PaxNumber = PaxNumber,
            //     LastModified = LastModified
            // };

            // line.Preparations = new TypedList<POSProductLinePreparationContract>(OrderPreparations.Select(p =>
            //     new POSProductLinePreparationContract(p.RelationId, (Guid)line.Id, Product.Description, p.Id, p.Description)));
            // line.Areas = new TypedList<POSProductLineAreaContract>(Areas.Select(a => new POSProductLineAreaContract(Guid.NewGuid(), a.AreaId)));

            return new Order(Ticket, line)
            {
                IsSelected = IsSelected,
                IsMenuExpanded = IsMenuExpanded
            };
        }

        public void CopyFrom(Order order)
        {
            IsSelected = order.IsSelected;
            IsMenuExpanded = order.IsMenuExpanded;
            CopyFrom(order.AsContract);
        }

        private void CopyFrom(POSProductLineContract prodLine)
        {
            var copy = new POSProductLineContract
            {
                Id = prodLine.Id,
                IsAditional = prodLine.IsAditional,
                Caja = prodLine.Caja,
                CostValue = prodLine.CostValue,
                DiscountValue = prodLine.DiscountValue,
                FirstIvaId = prodLine.FirstIvaId,
                FirstIvaValue = prodLine.FirstIvaValue,
                GrossValue = prodLine.GrossValue,
                AnnulmentCode = prodLine.AnnulmentCode,
                Printed = prodLine.Printed,
                ObservationsMarch = prodLine.ObservationsMarch,
                Recharge = prodLine.Recharge,
                SecondIvaId = prodLine.SecondIvaId,
                SecondIvaValue = prodLine.SecondIvaValue,
                ThirdIvaId = prodLine.ThirdIvaId,
                ThirdIvaValue = prodLine.ThirdIvaValue,
                Separator = prodLine.Separator,
                Stand = prodLine.Stand,
                Ticket = prodLine.Ticket,
                UserId = prodLine.UserId,
                SendToAreaStatus = prodLine.SendToAreaStatus,
                Product = prodLine.Product,
                ProductQtd = prodLine.ProductQtd,
                MainProduct = prodLine.MainProduct,
                ItemNumber = prodLine.ItemNumber,
                Observations = prodLine.Observations,
                IsHappyHour = prodLine.IsHappyHour,
                AreaId = prodLine.AreaId,
                NetValue = prodLine.NetValue,
                DiscountPercent = prodLine.DiscountPercent,
                ValueBeforeDiscount = prodLine.ValueBeforeDiscount,
                DiscountTypeId = prodLine.DiscountTypeId,
                HasManualSeparator = prodLine.HasManualSeparator,
                HasManualPrice = prodLine.HasManualPrice,
                AutomaticDiscount = prodLine.AutomaticDiscount,
                TableLines = new TypedList<TableLineContract>(OrderTableDetails),
                PaxNumber = prodLine.PaxNumber,
                LastModified = prodLine.LastModified,
                Preparations = new TypedList<POSProductLinePreparationContract>(OrderPreparations.Select(p =>
                    new POSProductLinePreparationContract(p.RelationId, (Guid)prodLine.Id, Product.Description, p.Id, p.Description))),
                Areas = new TypedList<POSProductLineAreaContract>(OrderAreas.Select(a => new POSProductLineAreaContract(Guid.NewGuid(), a.Id))),
                DiscountTypeDescription = prodLine.DiscountTypeDescription
            };

            Initialize(copy);
        }

        public Exception Persist()
        {
            var task = Task.Run(async () => await PersistAsync());
            task.Wait();

            return task.IsFaulted ? task.Exception : null;
        }

        public void SetPriceRateTax(RateType rateType, decimal? manualPrice, decimal discountPercent = 0)
        {
            if (Product == null) return;
            if (manualPrice.HasValue)
            {
                // Se le pasó un precio manual directamnte
                _productPriceBeforeDiscountOverride = manualPrice.Value;
                HasManualPrice = true;
            }
            else if (!HasManualPrice)
            {
                // No tiene un precio manual incluido
                _productPriceBeforeDiscountOverride = rateType switch
                {
                    RateType.InternalUse => Product.HouseUsePrice,
                    RateType.PensionMode => Product.MealPlanPrice,
                    RateType.Standard => Product.StandardPrice,
                    _ => Product.StandardPrice,
                };
            }

            if (_productPriceBeforeDiscountOverride.HasValue)
                _productPriceBeforeDiscountOverride *= (1 - discountPercent / 100);
            else
                _productPriceBeforeDiscountOverride = ProductPriceBeforeDiscount * (1 - (discountPercent / 100));

            RecalculateDiscountAndTaxs();
            CostBeforeDiscount = ((_productPriceBeforeDiscountOverride ?? CostBeforeDiscount) * Quantity);
        }

        public static void ExecuteWithoutRaiseEvents(Action action, params Order[] orders)
        {
            DeactivateRaiseEvents(orders);
            action();
            ActivateRaiseEvents(orders);
        }

        #endregion

        #region Aux

        private void Initialize(POSProductLineContract prodLine)
        {
            CleanDiscount();
            DataContext = prodLine;
            if (IsMenu)
            {
                IsMenuExpanded = true;
            }
        }

        private void InitializeModels(POSProductLineContract prodLine)
        {
            var stand = Hotel.Instance.CurrentStandCashierContext.Stand;

            // var product = stand.Products.FirstOrDefault(x => x.Id == prodLine.Product) ?? new Product(TaxSchemaId, prodLine);
            var product = stand.GetProductById(prodLine.Product) ?? new Product(TaxSchemaId, prodLine);
            Product = product;

            if (prodLine.Preparations == null)
                OrderPreparations = new ObservableCollection<Preparation>();
            else
            {
                OrderPreparations = new ObservableCollection<Preparation>(prodLine.Preparations.Select(s => new Preparation()
                {
                    Id = s.PreparationId,
                    Description = s.PreparationDescription,
                    RelationId = (Guid)s.Id
                }));
            }

            if (prodLine.Areas == null)
                OrderAreas = new ObservableCollection<Area>();
            else
            {
                // OrderAreas = new ObservableCollection<Area>(prodLine.Areas.Select(a => new Area(a.AreaId)));
                OrderAreas = new ObservableCollection<Area>(prodLine.Areas.Select(a => stand.Areas.FirstOrDefault(sa => sa.Id == a.AreaId)).Where(x => x != null));
            }

            if (prodLine.Separator.HasValue)
            {
                SeparatorModel = stand.Separators
                    .Union(new[] { stand.TipSeparator })
                    .FirstOrDefault(x => x.Id == prodLine.Separator.Value);
            }

            // Load table lines
            if (prodLine.TableLines != null && prodLine.TableLines.Count > 0)
            {
                OrderTables = new ObservableCollection<OrderTable>(prodLine.TableLines.Select(tableLine =>
                {
                    var tableProduct = product.ProductTables.FirstOrDefault(x => x.ProductId == tableLine.ProductId);
                    var orderTable = new OrderTable(tableLine, tableProduct);
                    orderTable.IsSelectedChanged += OrderTableOnIsSelectedChanged;
                    return orderTable;
                }));
            }

            LoadDiscountModel();
        }

        private void OrderTableOnIsSelectedChanged(OrderTable orderTable, bool isSelected)
        {
            //IsSelected = OrderTables.All(ot => ot.IsSelected);
        }

        private async Task PersistAsync()
        {
            var result = await BusinessProxyRepository.Ticket.PersistentProductLineAsync(_prodLine, Hotel.Instance.Id, TaxSchemaId, TaxIncluded);

            if (result != null && !result.HasErrors)
            {
                if (result.Contract != null && result.Contract is POSProductLineContract)
                {
                    _prodLine = result.Contract as POSProductLineContract;
                    LastModified = _prodLine.LastModified;
                }

                _prodLine.Id = Id;

                RaiseAllPropertyChanged();
            }
        }

        private static void ActivateRaiseEvents(params Order[] orders)
        {
            foreach (var order in orders)
                order._noRaiseEventsModeActivated++;
        }

        private static void DeactivateRaiseEvents(params Order[] orders)
        {
            foreach (var order in orders)
                order._noRaiseEventsModeActivated--;
        }

        private void RecalculateDiscountAndTaxs()
        {
            CalculateDiscount();
            RecalculateCalculateTaxs();
        }

        private void UpdateIncrementProductQuantity()
        {
            if (Quantity.HasDecimals())
                IncrementProductQuantity = decimal.One / (decimal)Math.Pow(10, Quantity.GetAmountOfDecimals());
            else if (Product != null)
                IncrementProductQuantity = Product.Step;
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);

            if (!IsNoRaiseEventsModeActivated)
            {
                switch (propertyName)
                {
                    /*
                    case nameof(Id):
                        // Product = Hotel.Instance.CurrentStandCashierContext.Stand.Products.FirstOrDefault(x => x.Id == _prodLine.Product) ?? new Product(TaxSchemaId, _prodLine);
                        Product = Hotel.Instance.CurrentStandCashierContext.Stand.GetProductById(_prodLine.Product) ?? new Product(TaxSchemaId, _prodLine);
                        break;
                        */
                    case nameof(Gross):
                        if (ImporteChanged != null)
                            UIThread.Invoke(() => ImporteChanged(this, Gross));
                        break;
                    case nameof(IsSelected):
                        if (IsSelectedChanged != null)
                            UIThread.Invoke(() => IsSelectedChanged(this, IsSelected));
                        break;
                    case nameof(ProductPriceBeforeDiscount):
                        CostBeforeDiscount = (ProductPriceBeforeDiscount * Quantity);
                        break;
                    case nameof(Product):
                        ProductId = Product?.Id ?? Guid.Empty;
                        if (Quantity != decimal.Zero)
                            UpdateIncrementProductQuantity();
                        break;
                    case nameof(AreaId):
                        _prodLine.AreaId = AreaId;
                        break;
                }

                UpdateCostOrder();
            }
        }

        private void UpdateCostOrder()
        {
            Cost = CostBeforeDiscount - DiscountValue + Recharge;
        }

        private void CopyPreparationsToDataContext()
        {
            if (_prodLine.Preparations != null && OrderPreparations != null)
            {
                _prodLine.Preparations.Clear();
                foreach (var orderPreparation in OrderPreparations)
                    _prodLine.Preparations.Add(new POSProductLinePreparationContract(orderPreparation.RelationId, ProductId, string.Empty, orderPreparation.Id,
                        orderPreparation.Description));
            }
        }

        private void CopyAreasToDataContext()
        {
            if (_prodLine.Areas != null && OrderAreas != null)
            {
                _prodLine.Areas.Clear();
                foreach (var orderArea in OrderAreas)
                    _prodLine.Areas.Add(new POSProductLineAreaContract(Guid.NewGuid(), orderArea.Id));
            }
        }

        private void CalculateDiscount()
        {
            if (DiscountType.HasValue && DiscountModel == null)
                LoadDiscountModel();

            var percent = decimal.Zero;
            if (DiscountModel != null)
            {
                ProductPrice = DiscountModel.Calculate(ProductPriceBeforeDiscount, out percent, out var value);
                DiscountValue = (value * Quantity);
            }
            else
                ProductPrice = ProductPriceBeforeDiscount;

            // Consider minimum discount per item
            if (ProductPrice < Hotel.Instance.AddedValueOnDiscount)
            {
                ProductPrice = Hotel.Instance.AddedValueOnDiscount;
                DiscountValue = ((ProductPriceBeforeDiscount - Hotel.Instance.AddedValueOnDiscount) * Quantity);
            }

            DiscountPercent = percent;
        }

        private void LoadDiscountModel()
        {
            if (DiscountType.HasValue)
            {
                // Tratemos de identificar el DiscountType
                var dt = Hotel.Instance.DiscountTypes.FirstOrDefault(x => x.Id == DiscountType.Value);

                if (dt != null)
                {
                    // Tenemos cargado en el hotel ese tipo de descuento
                    if (DiscountModel == null)
                        DiscountModel = new Discount { Type = dt, Percent = DiscountPercent };
                }
            }

            if (DiscountModel == null)
            {
                // No encontramos el tipo de descuento apropiado
                if (DiscountPercent > decimal.Zero)
                {
                    // Pero sin embargo la orden posee un porciento de descuento
                    DiscountModel = new Discount
                    {
                        Percent = DiscountPercent,
                        Type = new DiscountType
                        {
                            Description = "[House discount]",
                            MinValue = decimal.Zero,
                            MaxValue = decimal.Zero,
                            Fixed = false
                        }
                    };

                    DiscountTypes.Add(DiscountModel.Type);
                }
            }
        }

        /**
         * Recalculate taxes from product
         */
        internal void RecalculateCalculateTaxs()
        {
            var productTax = Product?.GetTaxLocal(TaxSchemaId, TaxIncluded, ProductPrice);
            if (productTax != null)
            {
                Net = productTax.NetValue * Quantity;
                Gross = productTax.GrossValue * Quantity;
            
                // Tax Details
                foreach (MovementTaxDetail taxDetail in productTax.Taxes)
                {
                    switch (taxDetail.TaxLevel)
                    {
                        // Tax 1
                        case 0:
                            FirstIvaCode = taxDetail.TaxRateId;
                            FirstIvaAuxCode = taxDetail.TaxAuxCode;
                            FirstIvaDescription = taxDetail.TaxRateDescription;
                            FirstIvaBase = taxDetail.TaxBase * Quantity;
                            FirstIvas = taxDetail.TaxValue * Quantity;
                            break;
                        // Tax 2
                        case 1:
                            SecondIvaCode = taxDetail.TaxRateId;
                            SecondIvaAuxCode = taxDetail.TaxAuxCode;
                            SecondIvaDescription = taxDetail.TaxRateDescription;
                            SecondIvas = taxDetail.TaxValue * Quantity;
                            SecondIvaBase = taxDetail.TaxBase * Quantity;
                            break;
                        // Tax 3
                        case 2:
                            ThirdIvaCode = taxDetail.TaxRateId;
                            ThirdIvaAuxCode = taxDetail.TaxAuxCode;
                            ThirdIvaDescription = taxDetail.TaxRateDescription;
                            ThirdIvas = taxDetail.TaxValue * Quantity;
                            ThirdIvaBase = taxDetail.TaxBase * Quantity;
                            break;
                    }
                }
            }
            else
                Gross = Net = ProductPrice * Quantity;
        }

        #endregion

        #endregion

        #region IPrintableOrder implementation

        IPrinteableSeparator IPrintableOrder.Separator => SeparatorModel;

        ICollection<IPrinteablePreparation> IPrintableOrder.Preparations => OrderPreparations.Cast<IPrinteablePreparation>().ToList();

        ICollection<IPrinteableArea> IPrintableOrder.Areas => OrderAreas.Cast<IPrinteableArea>().ToList();

        public ICollection<IPrintableTableLine> TableLines
        {
            get
            {
                return _prodLine.TableLines.Select(tl =>
                    {
                        var line = new PrintableTableLine
                        {
                            Id = (Guid)tl.Id,
                            ProductQuantity = $"{tl.Quantity}",
                            ProductDescription = tl.ProductDescription,
                            Preparations = tl.TableLinePreparations.Select(p => new Preparation(p.PreparationId, p.PreparationDescription)).Cast<IPrinteablePreparation>().ToList()
                        };

                        if (tl.SeparatorId.HasValue || !string.IsNullOrEmpty(tl.SeparatorDescription))
                        {
                            line.Separator = new Separator
                            {
                                Id = tl.SeparatorId,
                                Description = tl.SeparatorDescription,
                                ImpresionOrden = tl.SeparatorOrden.GetValueOrDefault()
                            };
                        }

                        line.Notes = tl.Observations;

                        return line;
                    })
                    .Cast<IPrintableTableLine>()
                    .ToList();
            }
        }

        public string CodeIsentoIva => _prodLine.CodeIsentoIva;
        public string DescIsentoIva => _prodLine.DescIsentoIva;

        #endregion

        private ObservableCollection<OrderTable> _auxOrderTables;

        public void ApplyOrderFilter(Func<OrderTable, bool> filterAction)
        {
            if (OrderTables == null) return;
            if (filterAction != null)
            {
                _auxOrderTables = OrderTables;
                OrderTables = new ObservableCollection<OrderTable>(OrderTables.Where(filterAction));
            }
            else if (_auxOrderTables != null)
            {
                OrderTables = _auxOrderTables;
                _auxOrderTables = null;
            }
        }

        public ObservableCollection<OrderTable> OrderTablesPendingToDispatch = new ObservableCollection<OrderTable>();
    }
}