﻿using System;
using System.Collections.ObjectModel;
using NewHotel.Contracts;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.App.Pos.Model
{
	public class LocalDataInitializer : BindableBase
	{
        public LocalDataInitializer()
        {
        }

        #region Properties

        #region InternalUse

        public ObservableCollection<object> AllInternalUses { get; set; }

        public ObservableCollection<object> AllInternalUsesByStand { get; set; }

        #endregion

        #region Stands

        public ObservableCollection<object> AllStands { get; set; }

        public ObservableCollection<object> AllStandCashiers { get; set; }

        #endregion

        #region Categories

        public ObservableCollection<object> AllCategories { get; set; }

        #endregion

        #region Products

        public ObservableCollection<object> AllGroupProducts { get; set; }

        public ObservableCollection<object> AllFamilyProducts { get; set; }

        public ObservableCollection<object> AllSubfamilyProducts { get; set; }

        public ObservableCollection<object> AllProducts { get; set; }

        public ObservableCollection<object> AllStandProducts { get; set; }

        public ObservableCollection<object> AllStandCategoryProducts { get; set; }

        #endregion

        #region Preparations

        public ObservableCollection<object> AllPreparations { get; set; }

        public ObservableCollection<object> AllProductPreparations { get; set; }

        public ObservableCollection<object> AllFamilyPreparations { get; set; }

        public ObservableCollection<object> AllGroupPreparations { get; set; }

        #endregion

        #region Areas

        public ObservableCollection<object> AllAreas { get; set; }

        public ObservableCollection<object> AllStandProductAreas { get; set; }

        #endregion

        #region Taxes

        public ObservableCollection<object> AllTaxSequence { get; set; }

        public ObservableCollection<object> AllTaxSchema { get; set; }

        public ObservableCollection<object> AllTaxes { get; set; }

        public ObservableCollection<object> PosTaxServices { get; set; }

        #endregion

        #region Rates

        public ObservableCollection<object> AllProductRateTypes { get; set; }

        public ObservableCollection<object> AllPricePeriods { get; set; }

        public ObservableCollection<object> AllProductPriceRates { get; set; }

        public ObservableCollection<object> AllDiscountTypes { get; set; }


        #endregion

        #region Saloon and Tables

        public ObservableCollection<object> AllSaloons { get; set; }

        public ObservableCollection<object> AllStandSaloons { get; set; }

        public ObservableCollection<object> AllTables { get; set; }

        #region Tables Groups and Reservation

        public ObservableCollection<object> AllTablesGroups { get; set; }

        public ObservableCollection<object> AllLiteTablesReservations { get; set; }

        public QueryRequest LiteTablesReservationsQueryRequest
        {
            get
            {
                QueryRequest query = new QueryRequest();
                query.AddFilter("litr_esta", ReservationState.Reserved);
                return query;
            }
        }

        public DateTime? WorkDate { get { return null; } }

        #endregion

        #endregion

        #region Tickets and Orders

        public ObservableCollection<object> AllOpenTickets { get; set; }

        public QueryRequest TicketQueryRequest
        {
            get
            {
                var query = new QueryRequest();
                //query.AddFilter("ipos_pk", Id);
                //query.AddFilter("caja_pk", CashierId);
                query.AddFilter("vend_close", false);
                return query;
            }
        }

        #endregion

        #region Separators

        public ObservableCollection<object> AllSeparators { get; set; }

        #endregion

        #region Duties

        public ObservableCollection<object> AllDuties { get; set; }

        #endregion

        #region PaymentMethods

        public ObservableCollection<object> AllPaymentMethods { get; set; }

        #endregion

        #region CreditCardType

        public ObservableCollection<object> AllCreditCardTypes { get; set; }

        #endregion

        #region Cancellations

        public ObservableCollection<object> AllCancellationReasons { get; set; }

        #endregion

        #region Users

        public ObservableCollection<object> AllUsers { get; set; }

        public ObservableCollection<object> AllUserPermissions { get; set; }

        #endregion

        #region Cashiers

        public ObservableCollection<object> AllCashiers { get; set; }

        #endregion

        #region HappyHours

        public ObservableCollection<object> AllHappyHours { get; set; }

        public ObservableCollection<object> AllStandHappyHours { get; set; }

        #endregion

        #endregion

        #region Public Methods

        public void GetInternalUses()
        {
            //LocalDataMgr.ClearTable(inScope, LocalDataMgr.GetClientInstance(), "StandInternalUseTbl");
            //LocalDataMgr.ClearTable(inScope, LocalDataMgr.GetClientInstance(), "InternalUseTbl");
            //LoadElementProperty("AllInternalUses", false, inScope);
            //LoadElementProperty("AllInternalUsesByStand", false, inScope);
        }

        public void GetSeparators()
        {
            //LoadElementProperty("AllSeparators", false, inScope);
        }

        public void GetDuties()
        {
            //LoadElementProperty("AllDuties", false, inScope);
        }

        public void GetPaymentMethods()
        {
            //LoadElementProperty("AllPaymentMethods", false, inScope);
            //LocalDataMgr.ClearTable(inScope, LocalDataMgr.GetClientInstance(), "PaymentMethodTbl",
            //    from item in AllPaymentMethods
            //    where (item as ManagedObject).LocalData != null
            //    select (item as ManagedObject).LocalData);
        }

        public void GetCreditCardTypes()
        {
            //LoadElementProperty("AllCreditCardTypes", false, inScope);
        }

        public void GetStandsCategoriesAndProducts()
        {
            //SynchronizeElementProperty("AllCategories", false);
            //LoadElementProperty("AllGroupProducts", false);
            //LoadElementProperty("AllFamilyProducts", false);
            //LoadElementProperty("AllSubfamilyProducts", false);
            //SynchronizeElementProperty("AllProducts", false);
            //LoadElementProperty("AllStands", false);
            //SynchronizeElementProperty("AllStandProducts", false);
            //SynchronizeElementProperty("AllStandCategoryProducts", false);
            GetPreparations();
        }

        public void GetPreparations()
        {
            //JustOneSave((s) =>
            //{
            //    LoadElementProperty("AllPreparations", false);
            //    LoadElementProperty("AllProductPreparations", false);
            //    LoadElementProperty("AllFamilyPreparations", false);
            //    LoadElementProperty("AllGroupPreparations", false);
            //    LocalDataMgr.ClearTable(LocalDataMgr.GetClientInstance(), "PreparationTbl",
            //        from item in AllPreparations
            //        where (item as ManagedObject).LocalData != null
            //        select (item as ManagedObject).LocalData);
            //    LocalDataMgr.ClearTable(LocalDataMgr.GetClientInstance(), "ProductPreparationTbl",
            //        from item in AllProductPreparations
            //        where (item as ManagedObject).LocalData != null
            //        select (item as ManagedObject).LocalData);
            //    LocalDataMgr.ClearTable(LocalDataMgr.GetClientInstance(), "FamilyPreparationTbl",
            //        from item in AllFamilyPreparations
            //        where (item as ManagedObject).LocalData != null
            //        select (item as ManagedObject).LocalData);
            //    LocalDataMgr.ClearTable(LocalDataMgr.GetClientInstance(), "GroupPreparationTbl",
            //        from item in AllGroupPreparations
            //        where (item as ManagedObject).LocalData != null
            //        select (item as ManagedObject).LocalData);
            //});
        }


        public void GetDispatchAreas()
        {
           // JustOneSave( (s) =>
           //{
           //            LoadElementProperty("AllAreas", false);
           //            LoadElementProperty("AllStandProductAreas", false);
           //            LocalDataMgr.ClearTable(inScope, LocalDataMgr.GetClientInstance(), "StandProductAreaTbl",
           //                from item in AllStandProductAreas
           //                where (item as ManagedObject).LocalData != null
           //                select (item as ManagedObject).LocalData);
           //});
        }

        public void GetTaxes()
        {
            //JustOneSave((s) =>
            //{
            //    LoadElementProperty("AllTaxSequence", false);
            //    LoadElementProperty("AllTaxSchema", false);
            //    LoadElementProperty("AllTaxes", false);
            //    LoadElementProperty("PosTaxServices", false);
            //});
        }

        public void GetRates()
        {
            //LoadElementProperty("AllProductRateTypes", false);
            //LoadElementProperty("AllPricePeriods", false);
            //JustOneSave((s) =>
            //{
            //    LoadElementProperty("AllProductPriceRates", false);
            //});
            //LocalDataMgr.ClearTable(LocalDataMgr.GetClientInstance(), "PriceRateTbl",
            //    from item in AllProductPriceRates
            //    where (item as ManagedObject).LocalData != null
            //    select (item as ManagedObject).LocalData);
        }

        public void GetHappyHours()
        {
            //LoadElementProperty("AllHappyHours", false);
            //foreach (HappyHour item in AllHappyHours)
            //{
            //    item.InitialHour = new DateTime(item.InitialHour.Year, item.InitialHour.Month, item.InitialHour.Day, item.InitialHour.Hour, item.InitialHour.Minute, 0);
            //    item.FinalHour = new DateTime(item.FinalHour.Year, item.FinalHour.Month, item.FinalHour.Day, item.FinalHour.Hour, item.FinalHour.Minute, 0);
            //}
            //JustOneSave(inScope, (s) =>
            //{
            //    LoadElementProperty("AllStandHappyHours", false);
            //});
            //LocalDataMgr.ClearTable( LocalDataMgr.GetClientInstance(), "StandHappyHourTbl",
            //  from item in AllStandHappyHours
            //  where (item as ManagedObject).LocalData != null
            //  select (item as ManagedObject).LocalData);
        }

        public void GetDiscountTypes()
        {
            //LoadElementProperty("AllDiscountTypes", false);
        }

        public void GetSaloonsAndTables()
        {
            //LoadElementProperty("AllSaloons", false, inScope);
            //LoadElementProperty("AllStandSaloons", false, inScope);
            //LocalDataMgr.ClearTable(inScope, LocalDataMgr.GetClientInstance(), "StandSaloonTbl",
            //  from item in AllStandSaloons
            //  where (item as ManagedObject).LocalData != null
            //  select (item as ManagedObject).LocalData);
            //LoadElementProperty("AllTables", false, inScope);
            //LocalDataMgr.ClearTable(LocalDataMgr.GetClientInstance(), "TableTbl",
            //  from item in AllTables
            //  where (item as ManagedObject).LocalData != null
            //  select (item as ManagedObject).LocalData);
        }

        public void GetTablesGroupsAndReservations()
        {
            // LoadElementProperty("AllTablesGroups", false, inScope);
            //SynchronizeElementProperty("AllLiteTablesReservations", false, inScope);
        }

        public void GetTickets()
        {
            //LoadElementProperty("AllOpenTickets", true);
        }

        public void GetCancellationReasons()
        {
            //LoadElementProperty("AllCancellationReasons", true);
        }

        public void GetUserPermissions()
        {
            //JustOneSave(scope, (s) =>
            //{
            //    LoadElementProperty("AllUserPermissions", true, s);
            //});
        }

        #endregion

        #region Args

        public QueryRequest UserPermissionQueryRequest
        {
            get
            {
                var qr = new QueryRequest();
                qr.AddFilter("appl_pk", 2); // POS Application ID
                return qr;

            }
        }

        public QueryRequest ProductsQueryResquest
        {
            get
            {
                var qr = new QueryRequest();
                qr.AddFilter("only_active", null).Enabled = true;
                return qr;

            }
        }
        #endregion
    }
}