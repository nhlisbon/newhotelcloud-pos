﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Localization;
using NewHotel.Pos.Printer.Interfaces;
using NewHotel.WPF.Common.Models;
using NewHotel.WPF.MVVM;
using NewHotel.Contracts;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Model
{
    public class BuyerInfo : BindableBase, IPrinteableBuyerInfo
    {
        #region Members

        private Guid? _baseEntityId;
        private IEnumerable<CollectionViewItem<PersonalDocType>> _fiscalIdenTypes;
        private string _name;
        private PersonalDocType _selectedFiscalIdenType;
        private string _fiscalMail;
        private NationalityRecord _selectedNationality;
        private ObservableCollection<NationalityRecord> _nationalities;
        private string _address;
        private string _fiscalNumber;
        private bool _showFinalCustomerDefault;
        private bool _saveClient;
        private bool _isEnabled;
        private POSDocumentType _fiscalDocType;

        //btw fiscalization requirements
        private string _fiscalPhoneNumber;
        private string _fiscalCityName;
        private string _fiscalCityCode;
        private string _fiscalRegimeTypeCode;
        private string _fiscalResponsabilityCode;

        #endregion
        #region Constructor

        public BuyerInfo(POSDocumentType fiscalDocType, Ticket ticket, bool showFinalCustomerDefault)
        {
            Nationalities = Hotel.Instance.Nationalities;
            FiscalDocType = fiscalDocType;

            if (ticket != null && ticket.BaseEntityId.HasValue)
            {
                _baseEntityId = ticket.BaseEntityId.Value;
                ClientId = _baseEntityId;
                Name = ticket.FiscalDocTo;
                Address = ticket.FiscalDocInfoAddress;
                FiscalMail = ticket.FiscalDocEmail;
                FiscalNumber = ticket.FiscalDocFiscalNumber;             
                SelectedFiscalIdenType = ticket.FiscalIdenType ?? PersonalDocType.FiscalNumber;
                SelectedNationality = Nationalities.FirstOrDefault(n => n.CountryCode == ticket.FiscalDocNacionality);
               
                FiscalPhoneNumber = ticket.FiscalPhoneNumber;
                FiscalCityCode = ticket.FiscalCityCode;
                FiscalCityName = ticket.FiscalCityName;
                FiscalRegimeTypeCode = ticket.FiscalRegimeTypeCode;
                FiscalResponsabilityCode = ticket.FiscalResponsabilityCode;
            }
            else
            {
                SelectedFiscalIdenType = PersonalDocType.FiscalNumber;
                var country = Hotel.Instance.Country;
                SelectedNationality = Nationalities.FirstOrDefault(n => n.CountryCode == country);
            }

            ShowFinalCustomerDefault = showFinalCustomerDefault;
            IsEnabled = !ShowFinalCustomerDefault;
        }

        public static BuyerInfo CreateFinalCustomerBuyer(POSDocumentType fiscalDocType)
        {
            return new BuyerInfo(fiscalDocType, null, true);
        }

        public static BuyerInfo CreateBuyer(POSDocumentType fiscalDocType, Ticket ticket, bool showFinalCustomerDefault = false)
        {
            return new BuyerInfo(fiscalDocType, ticket, showFinalCustomerDefault);
        }

        #endregion
        #region Properties

        public IEnumerable<CollectionViewItem<PersonalDocType>> FiscalIdenTypes
        {
            get => _fiscalIdenTypes;
            set => SetProperty(ref _fiscalIdenTypes, value);
        }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public POSDocumentType FiscalDocType
        {
            get => _fiscalDocType;
            set => SetProperty(ref _fiscalDocType, value);
        }

        public string FiscalMail
        {
            get => _fiscalMail;
            set => SetProperty(ref _fiscalMail, value);
        }

        public PersonalDocType SelectedFiscalIdenType
        {
            get => _selectedFiscalIdenType;
            set => SetProperty(ref _selectedFiscalIdenType, value);
        }

        public NationalityRecord SelectedNationality
        {
            get => _selectedNationality;
            set
            {
                if (SetProperty(ref _selectedNationality, value))
                {
                    FiscalIdenTypes = GetFiscalIdenTypes(Hotel.Instance.Country, _selectedNationality?.CountryCode);
                    if (!FiscalIdenTypes.Any(itm => (PersonalDocType)itm.Item == SelectedFiscalIdenType))
                        SelectedFiscalIdenType = (PersonalDocType)FiscalIdenTypes.FirstOrDefault().Item;
                }
            }
        }

        public ObservableCollection<NationalityRecord> Nationalities
        {
            get => _nationalities;
            set => SetProperty(ref _nationalities, value);
        }

        public string Address
        {
            get => _address;
            set => SetProperty(ref _address, value);
        }

        public string FiscalNumber
        {
            get => _fiscalNumber;
            set => SetProperty(ref _fiscalNumber, value);
        }

        public string FiscalPhoneNumber
        {
            get => _fiscalPhoneNumber;
            set => SetProperty(ref _fiscalPhoneNumber, value);
        }

        public string FiscalCityName
        {
            get => _fiscalCityName;
            set => SetProperty(ref _fiscalCityName, value);
        }

        public string FiscalCityCode
        {
            get => _fiscalCityCode;
            set => SetProperty(ref _fiscalCityCode, value);
        }

        public string FiscalRegimeTypeCode
        {
            get => _fiscalRegimeTypeCode;
            set => SetProperty(ref _fiscalRegimeTypeCode, value);
        }

        public string FiscalResponsabilityCode
        {
            get =>_fiscalResponsabilityCode;
            set => SetProperty(ref _fiscalResponsabilityCode, value);
        }
        public bool IsDefaultCustomer => Name == FiscalValidations.GetFinalCustomerName(Hotel.Instance.Country) &&
                                         FiscalNumber == FiscalValidations.GetFinalCustomerNumber(Hotel.Instance.Country);

        public bool ShowFinalCustomerDefault
        {
            get => _showFinalCustomerDefault;
            set => SetProperty(ref _showFinalCustomerDefault, value);
        }

        public bool SaveClient
        {
            get => _saveClient;
            set => SetProperty(ref _saveClient, value);
        }

        public bool IsEnabled
        {
            get => _isEnabled;
            set
            {
                if (SetProperty(ref _isEnabled, value))
                    OnPropertyChanged("ClientSelectionEnabled");
            }
        }

        public Guid? ClientId { get; set; }

        public ClientRecord Client { get; set; }

        public bool AutomaticProductDiscount { get; set; }

        public Guid? BaseEntityId => _baseEntityId;

        public bool ClientSelectionEnabled => _isEnabled && !_baseEntityId.HasValue;

        public bool ShowFinalCustomer => FiscalValidations.HasFinalCustomer(Hotel.Instance.Country);

        #endregion
        #region Methods

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"{"Soldto".Translate()}: {Name}");
            if (!string.IsNullOrEmpty(Address))
                sb.AppendLine($"{"Address".Translate()}: {Address}");
            if (!string.IsNullOrEmpty(FiscalMail))
                sb.AppendLine($"{"Email".Translate()}: {FiscalMail}");
            if (SelectedFiscalIdenType != 0)
                sb.AppendLine($"{SelectedFiscalIdenType.ToString().Translate()}: {FiscalNumber}");
            else if (!string.IsNullOrEmpty(FiscalNumber))
                sb.AppendLine($"{"FiscalNumber".Translate()}: {FiscalNumber}");

            return sb.ToString();
        }

        public void Reset()
        {
            if (!_baseEntityId.HasValue)
            {
                ClientId = null;
                Client = null;
                Name = null;
                FiscalNumber = null;
                Address = null;
                SelectedNationality = null;
                FiscalMail = null;
                FiscalPhoneNumber = null;
                FiscalCityCode = null;
                FiscalCityName = null;
                FiscalRegimeTypeCode = null;
                FiscalResponsabilityCode = null;
            }
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);
            switch (propertyName)
            {
                case nameof(ShowFinalCustomerDefault):
                    OnShowFinalCustomerDefault(Hotel.Instance.Country);
                    break;
            }
        }

        private static IEnumerable<CollectionViewItem<PersonalDocType>> GetFiscalIdenTypes(string country, string fiscalIdenCountry)
        {
            var alloweds = new List<PersonalDocType>();
            switch (country)
            {
                case "PT":
                case "AO":
                    alloweds.Add(PersonalDocType.FiscalNumber);
                    break;
                case "BR":
                    if (fiscalIdenCountry == "BR")
                        alloweds.Add(PersonalDocType.FiscalNumber);
                    else
                    {
                        alloweds.Add(PersonalDocType.Passport);
                        alloweds.Add(PersonalDocType.IdentityDocument);
                        alloweds.Add(PersonalDocType.ResidenceCertificate);
                        alloweds.Add(PersonalDocType.Other);
                    }
                    break;
                default:
                    alloweds.Add(PersonalDocType.FiscalNumber);
                    alloweds.Add(PersonalDocType.Passport);
                    alloweds.Add(PersonalDocType.IdentityDocument);
                    alloweds.Add(PersonalDocType.ResidenceCertificate);
                    alloweds.Add(PersonalDocType.Other);
                    break;
            }

            var enumType = typeof(PersonalDocType);
            var items = new List<CollectionViewItem<PersonalDocType>>();
            foreach (var allowed in alloweds)
            {
                var traslation = LocalizationMgr.Translation(Enum.GetName(enumType, allowed));
                items.Add(new CollectionViewItem<PersonalDocType>(allowed, allowed, traslation));
            }

            return items;
        }

        private void OnShowFinalCustomerDefault(string country)
        {
            PersonalDocType selectedFiscalIdenType;
            if (ShowFinalCustomerDefault)
            {
                Name = FiscalValidations.GetFinalCustomerName(country);
                FiscalNumber = FiscalValidations.GetFinalCustomerNumber(country);
                SelectedNationality = Nationalities.FirstOrDefault(n => n.CountryCode == country);
                Address = string.Empty;
                selectedFiscalIdenType = PersonalDocType.FiscalNumber;
            }
            else
            {
                Name = string.Empty;
                FiscalNumber = string.Empty;
                selectedFiscalIdenType = SelectedFiscalIdenType;
            }

            if (!FiscalIdenTypes.Any(ft => ft.Id == selectedFiscalIdenType))
                SelectedFiscalIdenType = FiscalIdenTypes.First().Id;
            else
                SelectedFiscalIdenType = selectedFiscalIdenType;
            IsEnabled = !ShowFinalCustomerDefault;
        }

        #endregion 
    }
}