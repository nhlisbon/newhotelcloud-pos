﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Media;

namespace NewHotel.WPF.App.Pos.Model
{
	public sealed class LanguageCollection : ObservableCollection<LanguageView>
	{
		static readonly List<LanguageView> DefaultLanguages;
		private LanguageView selectedLanguage;

		public LanguageCollection() : base(DefaultLanguages)
		{
        }

        static LanguageCollection()
		{
			DefaultLanguages = new List<LanguageView>
			{
				new LanguageView() { Code = 1033, Culture = "en-US", Description = "English", FlagImage = (ImageSource)(new ImageSourceConverter()).ConvertFromString("pack://application:,,,/NewHotel.Pos;component/Resources/Flags/1033.png") },
				new LanguageView() { Code = 2070, Culture = "pt-PT", Description = "Portugues", FlagImage = (ImageSource)(new ImageSourceConverter()).ConvertFromString("pack://application:,,,/NewHotel.Pos;component/Resources/Flags/2070.png") },
				new LanguageView() { Code = 1046, Culture = "pt-BR", Description = "Portugues", FlagImage = (ImageSource)(new ImageSourceConverter()).ConvertFromString("pack://application:,,,/NewHotel.Pos;component/Resources/Flags/1046.png") },
				new LanguageView() { Code = 3082, Culture = "es-ES", Description = "Spanish", FlagImage = (ImageSource)(new ImageSourceConverter()).ConvertFromString("pack://application:,,,/NewHotel.Pos;component/Resources/Flags/3082.png") },
				new LanguageView() { Code = 1036, Culture = "fr-FR", Description = "French", FlagImage = (ImageSource)(new ImageSourceConverter()).ConvertFromString("pack://application:,,,/NewHotel.Pos;component/Resources/Flags/1036.png") },
				new LanguageView() { Code = 1063, Culture = "lt-LT", Description = "Italian", FlagImage = (ImageSource)(new ImageSourceConverter()).ConvertFromString("pack://application:,,,/NewHotel.Pos;component/Resources/Flags/1063.png") },
				new LanguageView() { Code = 1049, Culture = "ru-RU", Description = "Russian", FlagImage = (ImageSource)(new ImageSourceConverter()).ConvertFromString("pack://application:,,,/NewHotel.Pos;component/Resources/Flags/1049.png") },
				new LanguageView() { Code = 1027, Culture = "ca-ES", Description = "Catalan", FlagImage = (ImageSource)(new ImageSourceConverter()).ConvertFromString("pack://application:,,,/NewHotel.Pos;component/Resources/Flags/1027.png") },
				new LanguageView() { Code = 1031, Culture = "de-DE", Description = "German", FlagImage = (ImageSource)(new ImageSourceConverter()).ConvertFromString("pack://application:,,,/NewHotel.Pos;component/Resources/Flags/1031.png") },
				new LanguageView() { Code = 1062, Culture = "lv-LV", Description = "Latvian", FlagImage = (ImageSource)(new ImageSourceConverter()).ConvertFromString("pack://application:,,,/NewHotel.Pos;component/Resources/Flags/1062.png") },
				new LanguageView() { Code = 2052, Culture = "zh-CN", Description = "Chinese", FlagImage = (ImageSource)(new ImageSourceConverter()).ConvertFromString("pack://application:,,,/NewHotel.Pos;component/Resources/Flags/2052.png") },
				new LanguageView() { Code = 1040, Culture = "it-IT", Description = "Italian", FlagImage = (ImageSource)(new ImageSourceConverter()).ConvertFromString("pack://application:,,,/NewHotel.Pos;component/Resources/Flags/1040.png") }
			};
		}

		public LanguageView SelectedLanguage
		{
			get { return this.selectedLanguage; }
			set { ChangeSelection(value); }
		}

		private void ChangeSelection(LanguageView laguage)
		{
			if (this.selectedLanguage != laguage && (laguage == null || this.Contains(laguage)))
			{
				this.selectedLanguage = laguage;
				OnPropertyChanged(new PropertyChangedEventArgs("SelectedLanguage"));
            }
		}
    }
}
