﻿using NewHotel.WPF.MVVM;
using System;

namespace NewHotel.WPF.App.Pos.Model
{
	public class CreditCardType : BindableBase
	{
		private string descriptionProperty;
		private string validationTypeIdProperty;
        private bool CreditCardProperty;
        private bool DebitCardProperty;
        // Initializer

        public Guid Id { get; set; }

        public string Description
		{
			get { return descriptionProperty; }
			set { SetProperty(ref descriptionProperty, value); }
		}

        public string ValidationTypeId
		{
			get { return validationTypeIdProperty; }
			set { SetProperty(ref validationTypeIdProperty, value); }
		}

		public override string ToString()
        {
            return Description;
        }

        public bool CreditCard
        {
            get { return CreditCardProperty; }
            set { SetProperty(ref CreditCardProperty, value); }
        }

        public bool DebitCard
        {
            get { return DebitCardProperty; }
            set { SetProperty(ref DebitCardProperty, value); }
        }
    }
}
