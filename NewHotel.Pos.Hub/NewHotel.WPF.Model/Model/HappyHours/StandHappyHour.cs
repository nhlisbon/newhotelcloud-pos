﻿using NewHotel.WPF.MVVM;
using System;

namespace NewHotel.WPF.App.Pos.Model
{
	public class StandHappyHour : BindableBase
	{

        #region Dependency Properties

        private Guid HappyHourProperty;

        private Guid StandProperty;

        #endregion

        #region Properties

        public Guid HappyHour
        {
            get { return HappyHourProperty; }
            set { SetProperty(ref HappyHourProperty, value); }
        }

        public Guid Stand
        {
            get { return StandProperty; }
            set { SetProperty(ref StandProperty, value); }
        }

        #endregion

    }
}
