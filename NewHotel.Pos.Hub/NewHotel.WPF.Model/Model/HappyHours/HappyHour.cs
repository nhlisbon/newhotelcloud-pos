﻿using NewHotel.WPF.MVVM;
using System;
using NewHotel.Pos.Hub.Model;

namespace NewHotel.WPF.App.Pos.Model
{
	public class HappyHour : BindableBase
	{
        #region Private Members
        private HappyHourByStandRecord _happyHour;
        #endregion
        #region Constructor
        public HappyHour(HappyHourByStandRecord record)
        {
            _happyHour = record;
        }
        #endregion
        #region DataContext
        public override object DataContext
        {
            get
            {
                return _happyHour;
            }

            set
            {
                _happyHour = value as HappyHourByStandRecord;
                RaiseAllPropertyChanged();
            }
        }
        #endregion
        #region Properties

        public Guid Id
        {
            get { return _happyHour.Id; }
        }

        public string Description
        {
            get { return _happyHour.Description; }
            set { _happyHour.Description = value; }
        }

        public DateTime InitialHour
        {
            get { return _happyHour.InitialTime; }
            set { _happyHour.InitialTime = value; }
        }

        public DateTime FinalHour
        {
            get { return _happyHour.FinalTime; }
            set { _happyHour.FinalTime = value; }
        }

        public Guid TaxRate
        {
            get { return _happyHour.PriceRateId; }
            set { _happyHour.PriceRateId = value; }
        }

        public string DaysOfTheWeek
        {
            get { return _happyHour.DaysOfWeek; }
            set { _happyHour.DaysOfWeek = value; }
        }

        public short Quantity
        {
            get { return _happyHour.Quantity; }
            set { _happyHour.Quantity = value; }
        }

        public bool Inactive
        {
            get { return _happyHour.Inactive; }
            set { _happyHour.Inactive = value; }
        }

        #endregion
        #region Dummy

        public bool Sunday { get { return (string.IsNullOrEmpty(DaysOfTheWeek)) ? false : DaysOfTheWeek[0] == '1'; } }
        public bool Monday { get { return (string.IsNullOrEmpty(DaysOfTheWeek)) ? false : DaysOfTheWeek[1] == '1'; } }
        public bool Tuesday { get { return (string.IsNullOrEmpty(DaysOfTheWeek)) ? false : DaysOfTheWeek[2] == '1'; } }
        public bool Wednesday { get { return (string.IsNullOrEmpty(DaysOfTheWeek)) ? false : DaysOfTheWeek[3] == '1'; } }
        public bool Thursday { get { return (string.IsNullOrEmpty(DaysOfTheWeek)) ? false : DaysOfTheWeek[4] == '1'; } }
        public bool Friday { get { return (string.IsNullOrEmpty(DaysOfTheWeek)) ? false : DaysOfTheWeek[5] == '1'; } }
        public bool Saturday { get { return (string.IsNullOrEmpty(DaysOfTheWeek)) ? false : DaysOfTheWeek[6] == '1'; } }

        #endregion
    }
}
