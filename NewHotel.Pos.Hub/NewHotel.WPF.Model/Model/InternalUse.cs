﻿using NewHotel.WPF.MVVM;
using System;

namespace NewHotel.WPF.App.Pos.Model
{
	public class InternalUse : BindableBase
	{
        private string DescriptionProperty;

        private bool InvitationsProperty;

        public InternalUse()
        {
        }

        public Guid Id { get; set; }
        public string Description
        {
            get { return DescriptionProperty; }
            set { SetProperty(ref DescriptionProperty, value); }
        }

        public bool Invitations
        {
            get { return InvitationsProperty; }
            set { SetProperty(ref InvitationsProperty, value); }
        }

        public decimal? LimitPercent
        {
            get;
            set;
        }
        public decimal? MonthlyLimit
        {
            get;
            set;
        }
        public bool Unlimited
        {
            get;
            set;
        }
        public override string ToString()
        {
            return Description;
        }

    }
}
