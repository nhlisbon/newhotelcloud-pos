﻿using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.App.Pos.Model
{
    public class NewCharge : BindableBase
	{
		private NewChargeValueType valueTypeProperty;
		private NewChargeType typeProperty;
		private double valueProperty;

		public enum NewChargeValueType { Percent, Value };
        public enum NewChargeType { Local, Global };

        public NewChargeValueType ValueType
		{
			get { return valueTypeProperty; }
			set { SetProperty(ref valueTypeProperty, value); }
		}

		public NewChargeType Type
		{
			get { return typeProperty; }
			set { SetProperty(ref typeProperty, value); }
		}

		public double Value
		{
			get { return valueProperty; }
			set { SetProperty(ref valueProperty, value); }
		}

		public string Description
        {
            get { return string.Format("{0:F2}{1}", Value, (ValueType == NewChargeValueType.Value ? "" : " %")); }
        }
    }
}
