﻿using System;
using System.Linq;
using System.Collections.Generic;
using NewHotel.Pos.Printer.Interfaces;

namespace NewHotel.WPF.App.Pos.Model
{
    public class TaxModel
    {
        #region Constructor

        public TaxModel()
            : base()
        {
            Taxes = new List<MovementTaxDetail>();
        }

        public TaxModel(Guid? taxSchemaId, bool taxIncluded, decimal informedPrice)
            : this()
        {
            TaxSchemaId = taxSchemaId;
            TaxIncluded = taxIncluded;
            InformedPrice = informedPrice;
        }

        #endregion

        public Guid? TaxSchemaId { get; private set; }

        public bool TaxIncluded { get; private set; }

        public decimal InformedPrice { get; private set; }

        public List<MovementTaxDetail> Taxes { get; private set; }

        public decimal GrossValue { get; set; }

        public decimal NetValue { get; set; }

        public decimal TotalTaxes
        {
            get { return Taxes.Sum(x => x.TaxValue); }
        }

        public MovementTaxDetail TaxDetailIva1 => Taxes.Count > 0 ? Taxes[0] : null;

        public MovementTaxDetail TaxDetailIva2 => Taxes.Count > 1 ? Taxes[1] : null;

        public MovementTaxDetail TaxDetailIva3 => Taxes.Count == 2 ? Taxes[2] : null;
    }

    public class MovementTaxDetail: IPrinteableMovementTaxDetail
    {
        #region Properties

        public Guid TaxRateId { get; set; }

        public decimal TaxPercent { get; set; }

        public string TaxRateDescription { get; set; }

        public short TaxLevel { get; set; }

        public decimal TaxValue { get; set; }

        public decimal TaxBase { get; set; }

        public decimal TaxIncidence { get; set; }

        public long? Mode { get; set; }

        public string TaxAuxCode { get; set; }

        #endregion
        #region Contructor

        public MovementTaxDetail()
            : base() { }

        public MovementTaxDetail(Guid taxRateId, decimal taxPercent, short taxLevel)
            : this()
        {
            TaxRateId = taxRateId;
            TaxPercent = taxPercent;
            TaxLevel = taxLevel;
        }

        #endregion
    }
}
