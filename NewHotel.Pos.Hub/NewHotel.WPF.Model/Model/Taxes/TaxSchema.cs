﻿using System;
using System.Collections.Generic;
using System.Linq;
using NewHotel.Pos.Hub.Contracts.Cashier.Records;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.Model.Model.Taxes
{
    public class TaxSchema
    {
        private readonly TaxSchemaRecord _record;

        public TaxSchema(TaxSchemaRecord record)
        {
            _record = record;
        }

        public Guid Id => (Guid) _record.Id;

        public Guid HotelId => _record.HotelId;

        public bool Default => _record.Default;

        public string Description => _record.Description;
    }
}
