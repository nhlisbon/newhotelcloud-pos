﻿using NewHotel.WPF.MVVM;
using System;

namespace NewHotel.WPF.App.Pos.Model
{
	public class DiscountType : BindableBase
	{
        #region Variables

        private string _description;
        private bool _fixed;
        private decimal _minValue;
        private decimal _maxValue;

        #endregion 
        #region Properties

        public Guid Id { get; set; }

        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }

        public bool Fixed
        {
            get => _fixed;
            set => SetProperty(ref _fixed, value);
        }

        public decimal MinValue
        {
            get => _minValue;
            set => SetProperty(ref _minValue, value);
        }

        public decimal MaxValue
        {
            get => _maxValue;
            set => SetProperty(ref _maxValue, value);
        }

        #endregion
    }
}
