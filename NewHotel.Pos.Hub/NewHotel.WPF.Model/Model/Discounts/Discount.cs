﻿using NewHotel.WPF.MVVM;
using System;
using System.Linq;
using NewHotel.Pos.Core.Ext;

namespace NewHotel.WPF.App.Pos.Model
{
    public class Discount : BindableBase
    {
        #region Variables

        private DiscountType _discountType;
        private decimal _percent;

        #endregion

        #region Properties

        public DiscountType Type
        {
            get => _discountType;
            set => SetProperty(ref _discountType, value);
        }

        public decimal Percent
        {
            get => _percent;
            set => SetProperty(ref _percent, value);
        }

        #endregion

        public static Discount Create(DiscountType type, decimal percent = 0)
        {
            return new Discount { Type = type, Percent = percent };
        }

        public static Discount CreateNotDiscount()
        {
            return new Discount
            {
                Percent = 0,
                Type = Hotel.Instance.DiscountTypes.First()
            };
        }

        public decimal Calculate(decimal priceBeforeDiscount, out decimal discountPercent, out decimal discountValue)
        {
            if (Type != null)
            {
                // Tenemos un Discount valido
                if (Type.Fixed)
                {
                    // Es un descuento fijo
                    if (Type.MinValue > 0)
                    {
                        // Aplicamos el porciento del MinValue 
                        Percent = Type.MinValue;
                    }
                    else if (Type.MaxValue > 0)
                    {
                        // Aplicamos el porciento del MaxValue 
                        Percent = Type.MaxValue;
                    }
                }
                
                discountPercent = Percent;
                discountValue = priceBeforeDiscount.GetDiscountValue(Percent);
                
                return priceBeforeDiscount - discountValue;
            }

            // No tenemos un Discount
            discountPercent = decimal.Zero;
            discountValue = decimal.Zero;

            return priceBeforeDiscount;
        }
    }
}
