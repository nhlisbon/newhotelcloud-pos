﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using NewHotel.WPF.Model.Model;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.App.Pos.Model
{
	public class Category : BindableBase
	{
		private string DescriptionProperty;

		private ObservableCollection<Product> productsProperty;

		private ObservableCollection<Category> subCategoriesProperty;

		private ObservableCollection<Guid> productIdListProperty;

		private string imagePathProperty;

		private Guid? parentCategoryProperty;

		private int indexProperty;

		private DateTime lastModifiedProperty;

		public Category()
		{ }

		public Guid Id { get; set; }

		public string Description
		{
			get { return DescriptionProperty; }
			set { SetProperty(ref DescriptionProperty, value); }
		}

		public ObservableCollection<Product> Products
		{
			get { return productsProperty; }
			set { SetProperty(ref productsProperty, value); }
		}
		
        public ObservableCollection<Guid> ProductIdList
		{
			get { return productIdListProperty; }
			set { SetProperty(ref productIdListProperty, value); }
		}

		public ObservableCollection<Category> SubCategories
		{
			get { return subCategoriesProperty; }
			set { SetProperty(ref subCategoriesProperty, value); }
		}

		public string ImagePath
		{
			get { return imagePathProperty; }
			set { SetProperty(ref imagePathProperty, value); }
		}

		public Guid? ParentCategory
		{
			get { return parentCategoryProperty; }
			set { SetProperty(ref parentCategoryProperty, value); }
		}

		public int Index
		{
			get { return indexProperty; }
			set { SetProperty(ref indexProperty, value); }
		}

		public DateTime LastModified
		{
			get { return lastModifiedProperty; }
			set { SetProperty(ref lastModifiedProperty, value); }
		}

		public Guid[] ListContractId
		{
			get { return new Guid[] { Id }; }
		}

		public void GetProducts(Action action, Stand stand = null)
		{
			if (action != null)
				action();
		}

		public Guid Stand { get { return Hotel.Instance.CurrentStandCashierContext.Stand.Id; } }

		private bool? isVisible = null;

		public bool IsVisible
		{
			get
			{
				if (isVisible.HasValue)
					return isVisible.Value;

				isVisible = (SubCategories != null && SubCategories.Any(x => (x as Category).IsVisible)) ||
					(Products != null && Products.Count > 0);
				return isVisible.Value;
			}
		}

		public void SortSubCategories(bool sortChildren = false)
		{
			if (SubCategories != null)
			{
				if (sortChildren)
					foreach (Category item in SubCategories)
						item.SortSubCategories(true);
			}
		}
	}
}
