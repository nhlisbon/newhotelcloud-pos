﻿using System;
using System.Linq;
using System.Collections.Generic;
using NewHotel.Contracts;
using NewHotel.WPF.MVVM;
using NewHotel.Pos.Localization;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Model
{
    public class LiteTablesReservation : BindableBase
    {
        #region Variables

        private string _serieName;
        private ReservationState _status;
        private DateTime _date;
        private DateTime _initialTime;
        private DateTime _finalTime;
        private DateTime? _realInitialTime;
        private DateTime? _realFinalTime;
        private short _paxsAdults;
        private short _paxsChildren;
        private short _paxsBabies;
        private Stand _standModel;
        private Saloon _saloonModel;
        private Table _tableModel;
        private TablesGroup _tablesGroupModel;
        private bool _lock;
        private bool? _smoke;
        private string _fullName;
        private string _cellPhone;
        private int? _age;
        private string _email;
        private DateTime? _birthdate;
        private string _imagePath;
        private long _gender;
        private string _countryId;
        private string _comments;
        private string _preferences;
        private string _diets;
        private string _attentions;
        private string _allergies;

        #endregion
        #region Constructor

        public LiteTablesReservation(LiteTablesReservationRecord reservation, Saloon saloon = null)
        {
            Id = reservation.Id;
            FullName = reservation.FullName;
            CellPhone = reservation.CellPhone;
            Email = reservation.Email;
            PaxsChildren = reservation.PaxsChildren;
            PaxsBabies = reservation.PaxsBabies;
            PaxsAdults = reservation.PaxsAdults;
            InitialTime = reservation.InitialTime;
            FinalTime = reservation.FinalTime;
            CountryId = reservation.CountryId;
            Comments = reservation.Comments;
            SerieName = reservation.SerieName;
            QuickInformation = reservation.QuickInformation;
            Preferences = reservation.Preferences;
            Diets = reservation.Diets;
            Attentions = reservation.Attentions;
            Allergies = reservation.Allergies;

            var stand = Hotel.Instance.CurrentStandCashierContext.Stand;
            if (reservation.Saloon.HasValue)
                Saloon = saloon ?? stand.FindSaloon(reservation.Saloon.Value);

            if (reservation.TablesDescription.Length == 1)
            {
                // Reservation to one table
                var tableName = reservation.TablesDescription[0];
                Table = Saloon?.FindTable(tableName) ?? stand.FindTable(tableName);
                TableDesc = tableName;
            }
            else if (reservation.TablesDescription.Length > 1)
            {
                // Reservation to group table
                TableGroupDesc = reservation.TableDescription;

                // The table group id is the reservation Id
                var group = new TablesGroup((Guid)reservation.Id);
                group.SaloonId = reservation.Saloon;

                foreach (var tableName in reservation.TablesDescription)
                {
                    var table = Saloon?.FindTable(tableName) ?? stand.FindTable(tableName);
                    if (table != null)
                        group.AddTable(table);
                }

                if (group.Count > 0)
                    TableGroup = group;
            }
        }

        #endregion
        #region Properties

        public Guid Id { get; set; }

        public string Comments
        {
            get => _comments;
            set => SetProperty(ref _comments, value);
        }

        public bool HasComments => !string.IsNullOrEmpty(Comments);

        public string CountryId
        {
            get => _countryId;
            set => SetProperty(ref _countryId, value);
        }

        public string SerieName
        {
            get => _serieName;
            set => SetProperty(ref _serieName, value);
        }

        public ReservationState Status
        {
            get => _status;
            set => SetProperty(ref _status, value);
        }

        public DateTime Date
        {
            get => _date;
            set => SetProperty(ref _date, value);
        }

        public DateTime InitialTime
        {
            get => _initialTime;
            set => SetProperty(ref _initialTime, value);
        }

        public DateTime FinalTime
        {
            get => _finalTime;
            set => SetProperty(ref _finalTime, value);
        }

        public DateTime? RealInitialTime
        {
            get => _realInitialTime;
            set => SetProperty(ref _realInitialTime, value);
        }

        public DateTime? RealFinalTime
        {
            get => _realFinalTime;
            set => SetProperty(ref _realFinalTime, value);
        }

        public short PaxsAdults
        {
            get => _paxsAdults;
            set => SetProperty(ref _paxsAdults, value);
        }

        public short PaxsChildren
        {
            get => _paxsChildren;
            set => SetProperty(ref _paxsChildren, value);
        }

        public short PaxsBabies
        {
            get => _paxsBabies;
            set => SetProperty(ref _paxsBabies, value);
        }

        public Guid? StandId => Stand?.Id;

        public Stand Stand
        {
            get => _standModel;
            set => SetProperty(ref _standModel, value);
        }

        public Guid? SaloonId => Saloon?.Id;

        public bool HasSaloonDesc => !string.IsNullOrEmpty(SaloonDesc);

        public string SaloonDesc => Saloon?.Description;

        public Saloon Saloon
        {
            get => _saloonModel;
            set => SetProperty(ref _saloonModel, value);
        }

        public Guid? TableId => Table?.Id;

        public string TableDesc { get; set; }

        public Table Table
        {
            get => _tableModel;
            set => SetProperty(ref _tableModel, value);
        }

        public Guid? TableGroupId => TableGroup?.Id;

        public string TableGroupDesc { get; set; }

        public TablesGroup TableGroup
        {
            get => _tablesGroupModel;
            set => SetProperty(ref _tablesGroupModel, value);
        }

        public bool Lock
        {
            get => _lock;
            set => SetProperty(ref _lock, value);
        }

        public bool? Smoke
        {
            get => _smoke;
            set => SetProperty(ref _smoke, value);
        }

        public string FullName
        {
            get => _fullName;
            set => SetProperty(ref _fullName, value);
        }

        public long Gender
        {
            get => _gender;
            set => SetProperty(ref _gender, value);
        }

        public DateTime? Birthdate
        {
            get => _birthdate;
            set => SetProperty(ref _birthdate, value);
        }

        public string ImagePath
        {
            get => _imagePath;
            set => SetProperty(ref _imagePath, value);
        }

        public int? Age
        {
            get => _age;
            set => SetProperty(ref _age, value);
        }

        public string CellPhone
        {
            get => _cellPhone;
            set => SetProperty(ref _cellPhone, value);
        }

        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        public string Preferences
        {
            get => _preferences;
            set => SetProperty(ref _preferences, value);
        }

        public string Diets
        {
            get => _diets;
            set => SetProperty(ref _diets, value);
        }

        public string Attentions
        {
            get => _attentions;
            set => SetProperty(ref _attentions, value);
        }

        public string Allergies
        {
            get => _allergies;
            set => SetProperty(ref _allergies, value);
        }

        public DateTime LastModified { get; set; }

        public bool HasTablesDescription => !string.IsNullOrEmpty(TablesDescription);

        public string TablesDescription => string.IsNullOrEmpty(TableDesc) ? TableGroupDesc : TableDesc;

        public short TotalPaxs => (short)(PaxsAdults + PaxsChildren + PaxsBabies);

        public string PaxsDescription => $"{PaxsAdults} {PaxsChildren} {PaxsBabies}";

        #region QuickInfo

        public string QuickInformation { get; set; }

        private bool ContainsQuickInformation(int index) => QuickInformation != null && QuickInformation[index] == '1';

        public bool Vegetarian => ContainsQuickInformation((int)TablesReservationQuickInfo.Vegetarian);

        public bool GlutenFree => ContainsQuickInformation((int)TablesReservationQuickInfo.GlutenFree);

        public bool Vip => ContainsQuickInformation((int)TablesReservationQuickInfo.VIP);

        public bool Vegan => ContainsQuickInformation((int)TablesReservationQuickInfo.Vegan);

        public bool Disabled => ContainsQuickInformation((int)TablesReservationQuickInfo.Disabled);

        public bool Bigspender => ContainsQuickInformation((int)TablesReservationQuickInfo.Bigspender);

        public bool NutAllergy => ContainsQuickInformation((int)TablesReservationQuickInfo.NutAllergy);

        public bool WheelChair => ContainsQuickInformation((int)TablesReservationQuickInfo.WheelChair);

        public bool FriendOwner => ContainsQuickInformation((int)TablesReservationQuickInfo.FriendOwner);

        public bool HasQuickInformation => Vegetarian || GlutenFree || Vip || Vegan || Disabled || Bigspender ||
                                           NutAllergy || WheelChair || FriendOwner;

        public string QuickInformationDescription
        {
            get
            {
                var informations = new List<string>();

                if (Vegetarian)
                    informations.Add(LocalizationMgr.Translation("Vegetarian"));
                if (GlutenFree)
                    informations.Add(LocalizationMgr.Translation("GlutenFree"));
                if (Vip)
                    informations.Add(LocalizationMgr.Translation("VIP"));
                if (Vegan)
                    informations.Add(LocalizationMgr.Translation("Vegan"));
                if (Disabled)
                    informations.Add(LocalizationMgr.Translation("Disabled"));
                if (Bigspender)
                    informations.Add(LocalizationMgr.Translation("Bigspender"));
                if (NutAllergy)
                    informations.Add(LocalizationMgr.Translation("NutAllergy"));
                if (WheelChair)
                    informations.Add(LocalizationMgr.Translation("WheelChair"));
                if (FriendOwner)
                    informations.Add(LocalizationMgr.Translation("FriendOwner"));

                var description = string.Empty;
                if (informations.Count > 0)
                {
                    for (int i = 0; i < informations.Count - 1; i++)
                        description += informations[i] + ", ";
                    if (informations.Count > 0)
                        description += informations[informations.Count - 1];
                }

                return description;
            }
        }

        #endregion

        #endregion
    }
}