﻿using System;
using NewHotel.Contracts;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.App.Pos.Model
{
    public class TablesReservationFilter : BindableBase
    {
        private DateTime? _inDate;
        private Guid? _standId;
        private string _name;
        private string _phone;
        private string _email;
        private long? _kindReservation;
        private ReservationState? _stateReservation;
        private Guid? _saloonId;
        private Guid? _bookingSlotId;
        private Guid? _slotId;

        public DateTime? InDate
        {
            get { return _inDate; }
            set { SetProperty(ref _inDate, value, nameof(InDate)); }
        }

        public Guid? StandId
        {
            get { return _standId; }
            set { SetProperty(ref _standId, value, nameof(StandId)); }
        }

        public Guid? SlotId 
        { 
            get { return _slotId;} 
            set { SetProperty(ref _slotId, value, nameof(SlotId)); }
        }

        public Guid? SaloonId
        {
            get { return _saloonId; }
            set { SetProperty(ref _saloonId, value, nameof(SaloonId)); }
        }

        public Guid? BookingSlotId
        {
            get { return _bookingSlotId; }
            set { SetProperty(ref _bookingSlotId, value, nameof(BookingSlotId)); }
        }
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value, nameof(Name)); }
        }

        public string Phone
        {
            get { return _phone; }
            set { SetProperty(ref _phone, value, nameof(Phone)); }
        }

        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value, nameof(Email)); }
        }

        public long? KindReservation
        {
            get { return _kindReservation; }
            set { SetProperty(ref _kindReservation, value, nameof(KindReservation)); }
        }

        public ReservationState? StateReservation
        {
            get { return _stateReservation; }
            set { SetProperty(ref _stateReservation, value, nameof(StateReservation)); }
        }
    }
}