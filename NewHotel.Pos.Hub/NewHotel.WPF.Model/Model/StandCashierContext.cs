﻿using NewHotel.Contracts;
using NewHotel.WPF.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Specialized;
using NewHotel.Pos.Communication;
using NewHotel.WPF.Model.Model;
using DocumentSerieRecord = NewHotel.Pos.Hub.Model.DocumentSerieRecord;

namespace NewHotel.WPF.App.Pos.Model
{
	public class StandCashierContext : BindableBase
	{
        #region Variables

	    private TicketSerie _ticketSerie;
	    private TicketSerie _invoiceSerie;
	    private TicketSerie _receiptSerie;
	    private ObservableCollection<ITicket> _quickTickets;
	    private ObservableCollection<ITicket> _noAllocatedAcceptedTickets;
	    private ObservableCollection<ITicket> _noAllocatedReturnedTickets;
	    private ObservableCollection<Product> _top10Products;
	    private Stand _stand;
	    private Cashier _cashier;

        #endregion
        #region Constructor

	    public StandCashierContext(Stand stand, Cashier cashier)
	    {
	        Stand = stand;
	        Cashier = cashier;

	        QuickTickets = new ObservableCollection<ITicket>();
	        AcceptedTickets = new ObservableCollection<ITicket>();
	        ReturnedTickets = new ObservableCollection<ITicket>();

	        QuickTickets.CollectionChanged += collectionChanged;
	        AcceptedTickets.CollectionChanged += collectionChanged;
	        ReturnedTickets.CollectionChanged += collectionChanged;

	        SetSeries();
	    }


        #endregion
        #region Properties

	    public TicketSerie TicketSerie
	    {
	        get { return _ticketSerie; }
	        set { SetProperty(ref _ticketSerie, value); }
	    }

	    public TicketSerie InvoiceSerie
	    {
	        get { return _invoiceSerie; }
	        set { SetProperty(ref _invoiceSerie, value); }
	    }

	    public TicketSerie ReceiptSerie
	    {
	        get { return _receiptSerie; }
	        set { SetProperty(ref _receiptSerie, value); }
	    }

	    public ObservableCollection<ITicket> QuickTickets
	    {
	        get { return _quickTickets; }
	        set { SetProperty(ref _quickTickets, value); }
	    }

	    public ObservableCollection<ITicket> AcceptedTickets
	    {
	        get { return _noAllocatedAcceptedTickets; }
	        set { SetProperty(ref _noAllocatedAcceptedTickets, value); }
	    }

	    public ObservableCollection<ITicket> ReturnedTickets
	    {
	        get { return _noAllocatedReturnedTickets; }
	        set { SetProperty(ref _noAllocatedReturnedTickets, value); }
	    }

	    public ObservableCollection<Product> Top10Products
	    {
	        get { return _top10Products; }
	        set { SetProperty(ref _top10Products, value); }
	    }

	    public Stand Stand
	    {
	        get { return _stand; }
	        set { SetProperty(ref _stand, value); }
	    }

	    public Cashier Cashier
	    {
	        get { return _cashier; }
	        set { SetProperty(ref _cashier, value); }
	    }

        #endregion
        #region Methods

	    public void SetSeries()
	    {
	        BusinessProxyRepository.Ticket.GetTicketSerieByStandCashierAsync(Stand.Id, Cashier.Id).ContinueWith(t =>
	        {
	            if (t.Status == TaskStatus.RanToCompletion)
	            {
	                DocumentSerieRecord[] list = t.Result;
	                if (list == null) throw new Exception("Local ticket serie not found");

	                var record = list.FirstOrDefault(x => x.DocType == POSDocumentType.Ticket);
	                if (record != null)
	                    TicketSerie = new TicketSerie(record, Stand.Id, Cashier.Id);

	                record = list.FirstOrDefault(x => x.DocType == POSDocumentType.CashInvoice);
	                if (record != null)
	                    InvoiceSerie = new TicketSerie(record, Stand.Id, Cashier.Id);

	                record = list.FirstOrDefault(x => x.DocType == POSDocumentType.TableBalance);
	                if (record != null)
	                    ReceiptSerie = new TicketSerie(record, Stand.Id, Cashier.Id);
	            }
	        });
	    }

	    private void collectionChanged(object sender, NotifyCollectionChangedEventArgs e)
	    {
	        if (e.NewItems != null)
	        {
	            foreach (ITicket ticket in e.NewItems)
	                ticket.IsEditable = true;
	        }
	    }

        #endregion
    }
}