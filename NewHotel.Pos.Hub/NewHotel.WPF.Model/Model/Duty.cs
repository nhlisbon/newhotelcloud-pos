﻿using NewHotel.WPF.MVVM;
using System;

namespace NewHotel.WPF.App.Pos.Model
{
	public class Duty : BindableBase
	{
        #region Variables

	    private string _description;
	    private int _delay;
	    private DateTime _initialDate;
	    private DateTime _finalDate;

        #endregion
        #region Constructor

	    // Initializer
	    public Duty()
	    {
	    }

        #endregion
        #region Properties

	    public Guid Id { get; set; }

	    public string Description
	    {
	        get { return _description; }
	        set { SetProperty(ref _description, value); }
	    }

	    public DateTime InitialDate
	    {
	        get { return _initialDate; }
	        set { SetProperty(ref _initialDate, value); }
	    }

	    public DateTime FinalDate
	    {
	        get { return _finalDate; }
	        set { SetProperty(ref _finalDate, value); }
	    }

	    public int Delay
	    {
	        get { return _delay; }
	        set { SetProperty(ref _delay, value); }
	    }

        #endregion
        #region Methods

	    public override string ToString()
	    {
	        return Description;
	    }

        #endregion
    }
}
