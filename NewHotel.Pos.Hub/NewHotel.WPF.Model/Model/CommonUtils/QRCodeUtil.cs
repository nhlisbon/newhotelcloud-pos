﻿using NewHotel.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.WPF.Model.Model.CommonUtils
{
    public static class QRCodeUtil
    {
        public static byte[] GetQRCodeFromData(string content) => new QRCode(content);
    }
}
