﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NewHotel.Contracts;
using NewHotel.WPF.Common;
using NewHotel.WPF.MVVM;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Hub.Contracts.Common.Records.Stands;
using NewHotel.Pos.Hub.Model;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Model
{
    public class Cashier : BindableBase
    {
        #region Variables

        private string _description;

        #endregion
        #region Constructor

        public Cashier(CashierRecord record)
        {
            Id = record.Id;
            Description = record.Description;
        }

        #endregion
        #region Properties

        public Guid Id { get; set; }

        public string Description
        {
            get { return _description; }
            set { SetProperty(ref _description, value); }
        }

        public Stand[] Stands { get; set; }

        #endregion
        #region Methods

        public override string ToString()
        {
            return Description;
        }

        public void GetSalesReport(Guid standId, Guid cashierId, long shift, DateTime standWorkDate, Action<IEnumerable<SalesCashierRecord>, Exception> afterAction, bool isTurnReport)
        {
            var queryRequest = new QueryRequest();
            queryRequest.AddParameter("ipos_pk", standId);
            queryRequest.AddParameter("caja_pk", cashierId);
            queryRequest.AddParameter("turn_codi", shift);
            queryRequest.AddParameter("workDate", standWorkDate.Ticks);

            BusinessProxyRepository.Stand.GetSalesReportAsync(queryRequest, isTurnReport).ContinueWith(t =>
            {
                if (afterAction != null)
                {
                    if (t.Status == TaskStatus.RanToCompletion) UIThread.Invoke(() => afterAction(t.Result, null));
                    else if (t.IsFaulted) UIThread.Invoke(() => afterAction(null, t.Exception.GetBaseException()));
                }
            });
        } 

        #endregion
    }
}