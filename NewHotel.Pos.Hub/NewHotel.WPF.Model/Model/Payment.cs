﻿using NewHotel.Contracts;
using NewHotel.WPF.MVVM;
using System;
using System.Linq;
using NewHotel.Pos.Hub.Contracts.Common.Records;
using NewHotel.Pos.Localization;
using NewHotel.Pos.Printer.Interfaces;
using NewHotel.Pos.PrinterDocument.Interfaces;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Model
{
    public class Payment : BindableBase, IPrintablePayment
    {
        #region Consts

        private const string HouseUseImagePath =
            "pack://application:,,,/NewHotel.Pos;component/Resources/Style_3/blue-hands.png";
        private const string CreditImagePath =
            "pack://application:,,,/NewHotel.Pos;component/Resources/Style_3/blue-credit.png";
        private const string RoomPlanImagePath =
            "pack://application:,,,/NewHotel.Pos;component/Resources/Style_3/blue-dish.png";

        #endregion
        #region Variables

        private bool _wideDetails;
        private CreditCard _creditCardModel;
        private readonly POSPaymentLineContract _payLine;
        private string _imagePath;

        #endregion
        #region Constructors

        public static Payment CreateHouseUse(Ticket ticket, Guid internalUseId, string description, string detailDescription, decimal amount, string currencyId)
        {
            var imageUri = new Uri(HouseUseImagePath);
            return new Payment(ticket, imageUri, description, amount, amount, detailDescription)
            {
                InternalUse = internalUseId,
                Tire = ReceivableType.HouseUse,
                CurrencyId = currencyId,
                MultCurrency = true,
                ExchangeCurrency = 1
            };
        }

        public static Payment CreateCredit(Ticket ticket, string description, string detailDescription, decimal amount, string currencyId,
                                           Guid accountId, CurrentAccountType? creditType, bool isWideDetails)
        {
            var image = new Uri(CreditImagePath);
            var pay = new Payment(ticket, image, description, amount, amount, detailDescription)
            {
                Account = accountId,
                AccountInstallationId = ticket.AccountInstallationId,
                Tire = ReceivableType.Credit,
                WideDetails = isWideDetails,
                CreditType = creditType,
                CurrencyId = currencyId,
                MultCurrency = true,
                ExchangeCurrency = 1
            };

            string[] stringSeparators = new string[] { "\r\n" };
            var hotel = pay.Description.Split(stringSeparators, StringSplitOptions.None)[0];
            string[] payDetails = pay.Details.Split(stringSeparators, StringSplitOptions.None);
            if (payDetails.Count() > 1)
            {
                var reserva = payDetails[0].Substring(payDetails[0].IndexOf(':') + 2);
                var quarto = payDetails[1].Substring(payDetails[1].IndexOf(':') + 2);
                var titular = payDetails[2].Substring(payDetails[2].IndexOf(':') + 2);
                pay.Observations = hotel + '-' + reserva + '-' + quarto + '-' + titular;
                return pay;
            }
            //var reserva = payDetails[0].Substring(payDetails[0].IndexOf(':') + 2);
            //var quarto = payDetails[1].Substring(payDetails[1].IndexOf(':') + 2);
            //var titular = payDetails[2].Substring(payDetails[2].IndexOf(':') + 2);
            //pay.Observations = hotel + '-' + reserva + '-' + quarto + '-' + titular;
            //pay.Observations = pay.Description;

            pay.Observations = payDetails[0];
            return pay;
        }

        public static Payment CreateControlAccountDeposit(Ticket ticket, ControlAccountDeposit deposit, string currencyId, string details)
        {
            var image = new Uri(CreditImagePath);
            var pay = new Payment(ticket, image, ticket.AccountDescription ?? "", deposit.Balance, deposit.Balance, details)
            {
                DepositId = deposit.DepositId,
                Account = ticket.Account,
                AccountInstallationId = ticket.AccountInstallationId,
                Tire = ReceivableType.UseAccountDeposit,
                WideDetails = true,
                CreditType = CurrentAccountType.Control,
                CurrencyId = currencyId,
                MultCurrency = true,
                ExchangeCurrency = 1
            };

            return pay;
        }

        public static Payment CreateRoomPlan(Ticket ticket, string description, string detailDescription, decimal amount, string currencyId,
            Guid? accountId)
        {
            var imag = new Uri(RoomPlanImagePath);
            var pay = new Payment(ticket, imag, description, amount, amount, detailDescription)
            {
                Account = accountId,
                Tire = ReceivableType.RoomPlan,
                CurrencyId = currencyId,
                MultCurrency = true,
                ExchangeCurrency = 1
            };
            return pay;
        }

        public static Payment Create(Ticket ticket, PaymentMethod paymentType, decimal value, decimal received)
        {
            return new Payment(ticket, paymentType, value, received);
        }

        public static Payment Create(POSPaymentLineContract payLine)
        {
            return new Payment(payLine);
        }

        private Payment(POSPaymentLineContract payLine)
        {
            _payLine = payLine;
            if (PaymentType != null)
            {
                var paymentType = Hotel.Instance.CurrentStandCashierContext.Stand.PaymentMethods
                    .FirstOrDefault(x => x.Id == this.PaymentType);
                Type = paymentType;
                Details = paymentType.Unmo;
                ImagePath = paymentType.ImagePath;
            }
            else
            {
                switch (Tire)
                {
                    case ReceivableType.HouseUse:
                        ImagePath = HouseUseImagePath;
                        InternalUse = payLine.InternalConsumptionId;
                        var houseUse = Hotel.Instance.CurrentStandCashierContext.Stand.InternalUses.FirstOrDefault(x => x.Id == InternalUse);
                        if (houseUse != null && houseUse.LimitPercent.HasValue)
                            InternalUsePercent = houseUse.LimitPercent.Value;
                        Details = string.Empty;
                        break;
                    case ReceivableType.RoomPlan:
                        ImagePath = RoomPlanImagePath;
                        break;
                    case ReceivableType.Credit:
                        ImagePath = CreditImagePath; 
                        break;
                }
            }

            if (CreditCard.HasValue)
            {
                CreditCardModel = new CreditCard(payLine.CreditCardContract.CreditCardTypeDescription,
                    payLine.CreditCardContract.CreditCardTypeId, payLine.CreditType.HasValue ? payLine.CreditType.Value.ToString() : null,
                    payLine.CreditCardContract.ValidationMonth, payLine.CreditCardContract.ValidationYear,
                    payLine.CreditCardContract.ValidationCode, payLine.CreditCardContract.TransactionNumber, payLine.CreditCardContract.AuthorizationCode);
                CreditCardModel.IdentNumber = payLine.CreditCardNumber;
            }
        }

        private Payment(Ticket ticket, Uri imageUri, string description, decimal value, decimal received, string details)
        {
            _payLine = new POSPaymentLineContract
            {
                Id = Guid.NewGuid(),
                Ticket = ticket.Id,
                ReceivableType = ReceivableType.Payment,
                PaymentDescription = description,
                PaymentAmount = value,
                PaymentReceived = received,
            };
            Details = details;
            ImagePath = imageUri.AbsoluteUri;
            InitialializeCurrencyInfo(ticket.Unmo);
        }

        private Payment(Ticket ticket, PaymentMethod paymentType, decimal value, decimal received)
        {
            _payLine = new POSPaymentLineContract
            {
                Id = Guid.NewGuid(),
                Ticket = ticket.Id,
                ReceivableType = ReceivableType.Payment,
                PaymentId = paymentType.Id,
                PaymentDescription = paymentType.Description,
                PaymentAmount = value,
                PaymentReceived = received
            };
            Type = paymentType;
            ImagePath = paymentType.ImagePath;
            
            InitialializeCurrencyInfo(ticket.Unmo);
        }

        #endregion
        #region Properties

        public string Details { get; set; }

        public PaymentMethod Type { get; set; }

        public string Description
        {
            get => _payLine.PaymentDescription;
            set
            {
                var refValue = _payLine.PaymentDescription;
                SetProperty(ref refValue, value, () => _payLine.PaymentDescription = refValue);
            }
        }

        public string ImagePath
        {
            get => _imagePath;
            set => SetProperty(ref _imagePath, value);
        }

        public bool IsSpecialPayment => (IsHouseUsePayment || IsMealPlanPayment);

        public bool IsHouseUsePayment => (Tire == ReceivableType.HouseUse);

        public bool IsMealPlanPayment => (Tire == ReceivableType.RoomPlan);

        public bool IsCreditRoom => Tire == ReceivableType.Credit;

        public bool IsCash => Tire == ReceivableType.Payment;

        public bool IsAccountDeposit => Tire == ReceivableType.UseAccountDeposit;

        public decimal InternalUsePercent { get; set; }

        public CurrentAccountType? CreditType
        {
            get => _payLine.CreditType;
            set
            {
                var refValue = _payLine.CreditType;
                SetProperty(ref refValue, value, () => _payLine.CreditType = refValue);
            }
        }

        public bool WideDetails
        {
            get => _wideDetails;
            set => SetProperty(ref _wideDetails, value);
        }

        public CreditCard CreditCardModel
        {
            get => _creditCardModel;
            set => SetProperty(ref _creditCardModel, value);
        }

        public Guid Ticket
        {
            get => _payLine.Ticket;
            set
            {
                var refValue = _payLine.Ticket;
                SetProperty(ref refValue, value, () => _payLine.Ticket = refValue);
            }
        }

        public short IsAnul
        {
            get => _payLine.AnnulmentCode;
            set
            {
                var refValue = _payLine.AnnulmentCode;
                SetProperty(ref refValue, value, () => _payLine.AnnulmentCode = refValue);
            }
        }

        public ReceivableType Tire
        {
            get => _payLine.ReceivableType;
            set
            {
                var refValue = _payLine.ReceivableType;
                SetProperty(ref refValue, value, () => _payLine.ReceivableType = refValue);
            }
        }

        public string TireDescription
        {
            get
            {
                var receivable = Tire;
                switch (receivable)
                {
                    case ReceivableType.Payment:
                        break;
                    case ReceivableType.RoomPlan:
                        return LocalizationMgr.Translation("RoomPlan");
                    case ReceivableType.HouseUse:
                        return LocalizationMgr.Translation("HouseUse");
                    case ReceivableType.Credit:
                        return LocalizationMgr.Translation("RoomCredit");
                    case ReceivableType.UseAccountDeposit:
                        return LocalizationMgr.Translation("Deposit");
                    default:
                        return String.Empty;
                }
                return String.Empty;
            }
        }

        public Guid? PaymentType
        {
            get => _payLine.PaymentId;
            set
            {
                var refValue = _payLine.PaymentId;
                SetProperty(ref refValue, value, () => _payLine.PaymentId = refValue);
            }
        }

        public decimal BaseValue
        {
            get => _payLine.PaymentAmount;
            set
            {
                var refValue = _payLine.PaymentAmount;
                SetProperty(ref refValue, value, () => _payLine.PaymentAmount = refValue);
            }
        }

        public decimal ReceiveValue
        {
            get => _payLine.PaymentReceived;
            set
            {
                var refValue = _payLine.PaymentReceived;
                SetProperty(ref refValue, value, () => _payLine.PaymentReceived = refValue);
            }
        }

        public decimal ChangeValue => ReceiveValue - BaseValue;

        public Guid? CreditCard
        {
            get => _payLine.CreditCard;
            set
            {
                var refValue = _payLine.CreditCard;
                SetProperty(ref refValue, value, () => _payLine.CreditCard = refValue);
            }
        }

        public Guid? Account
        {
            get => _payLine.AccountId;
            set
            {
                var refValue = _payLine.AccountId;
                SetProperty(ref refValue, value, () => _payLine.AccountId = refValue);
            }
        }

        public Guid? AccountInstallationId
        {
            get => _payLine.AccountInstallationId;
            set
            {
                var refValue = _payLine.AccountInstallationId;
                SetProperty(ref refValue, value, () => _payLine.AccountInstallationId = refValue);
            }
        }

        public Guid? InternalUse
        {
            get => _payLine.InternalConsumptionId;
            set
            {
                var refValue = _payLine.InternalConsumptionId;
                SetProperty(ref refValue, value, () => _payLine.InternalConsumptionId = refValue);
            }
        }

        public string Observations
        {
            get => _payLine.Observations;
            set
            {
                var refValue = _payLine.Observations;
                SetProperty(ref refValue, value, () => _payLine.Observations = refValue);
            }
        }

        public Guid Id
        {
            get => (Guid)_payLine.Id;
            set
            {
                var refValue = _payLine.Id;
                SetProperty(ref refValue, value, () => _payLine.Id = refValue);
            }
        }

        IPrinteablePaymentType IPrintablePayment.Type => this.Type;

        public string CurrencyId
        {
            get => _payLine.CurrencyId;
            set
            {
                string refValue = _payLine.CurrencyId;
                if (SetProperty(ref refValue, value))
                    _payLine.CurrencyId = refValue;
            }
        }

        public decimal? ExchangeCurrency
        {
            get => _payLine.ExchangeCurrency;
            set
            {
                decimal? refValue = _payLine.ExchangeCurrency;
                if (SetProperty(ref refValue, value))
                    _payLine.ExchangeCurrency = refValue;
            }
        }

        public bool? MultCurrency
        {
            get => _payLine.MultCurrency;
            set
            {
                bool? refValue = _payLine.MultCurrency;
                if (SetProperty(ref refValue, value))
                    _payLine.MultCurrency = refValue;
            }
        }
        
        public Guid? DepositId
        {
            get => _payLine.DepositId;
            set
            {
                var refValue = _payLine.DepositId;
                SetProperty(ref refValue, value, () => _payLine.DepositId = refValue);
            }
        }

        public bool AllowInvoiceClick => Type.AllowInvoiceClick;
        
        public bool AllowTicketClick => Type.AllowTicketClick;

        #endregion
        #region Methods

        internal void AddTo(POSTicketContract ticket, decimal excessTipAmount = 0)
        {
            ticket.PaymentLineByTicket ??= [];

            var payed = ticket.PaymentLineByTicket.Sum(x => x.PaymentAmount);
            // var payed = ticket.PaymentLineByTicket.Sum(x => x.PaymentReceived);
            var total = ticket.Total;
            
            var currentTotalPayment = total - payed - _payLine.PaymentAmount;
            if (excessTipAmount is not 0)
                _payLine.PaymentAmount = total - payed + excessTipAmount;
            
            ticket.PaymentLineByTicket.Add(_payLine);
        }

        internal void RemoveFrom(POSTicketContract ticket)
        {
            var payLine = ticket.PaymentLineByTicket?.FirstOrDefault(x => (Guid)x.Id == Id);
            if (payLine != null)
                ticket.PaymentLineByTicket?.Remove(payLine);
        }

        public void SetCreditCard(CreditCard card, decimal amount, bool hideDetails)
        {
            _payLine.CreditCardContract = new POSCreditCardContract()
            {
                Id = card.Id,
                CreditCardTypeDescription = card.CreditCardTypeDesc,
                CreditCardTypeId = card.CreditCardTypeId ?? Guid.Empty,
                CreditCardNumber = card.IdentNumber,
                ValidationMonth = card.ValidationMonth,
                ValidationYear = card.ValidationYear,
                TransactionNumber = card.TransactionNumber,
                AuthorizationCode = card.AuthorizationCode
            };
            _payLine.CreditCard = (Guid)_payLine.CreditCardContract.Id;
            _payLine.CreditCardNumber = card.IdentNumber;
            _payLine.IsCreditCard = true;
        }

        private void InitialializeCurrencyInfo(string currencyId)
        {
            CurrencyId = currencyId;
            ExchangeCurrency = 1;
            MultCurrency = true;
        }
        #endregion
    }
}
