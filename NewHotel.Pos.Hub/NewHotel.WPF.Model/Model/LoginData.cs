﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using NewHotel.Contracts;
using NewHotel.Pos.Communication;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.Model.Model
{
    public class LoginData : BindableBase
    {
        #region Variables + Constructor

        private Guid? _lastStand, _lastCashier;
        private UserPromptState _loginState;
        private string _userName;
        private string _password;
        private LicenseProductType _licenceType;
        private string _activationCode;
        private ObservableCollection<Cashier>? _cashiers;
        private ObservableCollection<Stand>? _stands;
        private ObservableCollection<HotelRecordModel>? _hotels;
        private Cashier? _selectedCashier;
        private Stand? _selectedStand;
        private KeyDescHotelRecord? _selectedHotel;
        private bool _isUserValid;
        private string _code;
        private User? _userModel;
        private string _hotelDescription;
        private bool _isByUserPwd;

        public LoginData()
        {
            var value = MgrServices.GetConfigurationSetting("UseNumericLogin");
            IsByUserPwd = !string.IsNullOrEmpty(value) && (value.ToLower() != "true");
            value = MgrServices.GetConfigurationSetting("LastStand");
            if (string.IsNullOrEmpty(value) || !Guid.TryParse(value, out Guid auxId))
                _lastStand = null;
            else
                _lastStand = auxId;

            value = MgrServices.GetConfigurationSetting("LastCashier");
            if (string.IsNullOrEmpty(value) || !Guid.TryParse(value, out auxId))
                _lastCashier = null;
            else
                _lastCashier = auxId;
        }

        #endregion
        #region Properties

        public UserPromptState State
        {
            get => _loginState;
            set => SetProperty(ref _loginState, value);
        }

        public string UserName
        {
            get => _userName;
            set => SetProperty(ref _userName, value);
        }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public string Code
        {
            get => _code;
            set => SetProperty(ref _code, value);
        }

        public Cashier? SelectedCashier
        {
            get => _selectedCashier;
            set => SetProperty(ref _selectedCashier, value);
        }

        public Stand? SelectedStand
        {
            get => _selectedStand;
            set => SetProperty(ref _selectedStand, value);
        }

        public KeyDescHotelRecord? SelectedHotel
        {
            get => _selectedHotel;
            set => SetProperty(ref _selectedHotel, value);
        }

        public Guid? UtilId => UserModel?.Id;

        public Guid? HotelId { get; set; }

        public string HotelDescription
        {
            get => _hotelDescription;
            set => SetProperty(ref _hotelDescription, value);
        }

        public ObservableCollection<Cashier>? Cashiers
        {
            get => _cashiers;
            private set => SetProperty(ref _cashiers, value);
        }

        public ObservableCollection<Stand>? Stands
        {
            get => _stands;
            private set => SetProperty(ref _stands, value);
        }

        public ObservableCollection<HotelRecordModel>? Hotels
        {
            get => _hotels;
            private set => SetProperty(ref _hotels, value);
        }

        public User? UserModel
        {
            get => _userModel;
            private set => SetProperty(ref _userModel, value);
        }

        private Guid UserModelId { get; set; }

        public bool IsTicketSeriesValid { get; private set; }

        public bool IsInvoiceSeriesValid { get; private set; }

        public bool IsReceiptSeriesValid { get; private set; }

        public bool IsControlAccountValid { get; private set; }

        public bool IsCashierStandValid { get; private set; }

        public string ActivationCode
        {
            get => _activationCode;
            set => SetProperty(ref _activationCode, value);
        }

        public bool IsUserValid
        {
            get => _isUserValid;
            private set => SetProperty(ref _isUserValid, value);
        }

        public bool IsByUserPwd
        {
            get => _isByUserPwd;
            set => SetProperty(ref _isByUserPwd, value);
        }

        #endregion
        #region Methods

        public async Task<Cashier?> GetCashiersAsync()
        {
            var result = Hotel.CheckCommunication();
            if (!string.IsNullOrEmpty(result)) return null;
            Cashiers = null;
            State = UserPromptState.Loading;

            if (HotelId != null)
            {
                var hotelId = HotelId.Value;
                var hotel = UserModel?.Hotels.First(h => (Guid)h.Id == hotelId);

                var cashiers = hotel?.Cashiers;
                if (cashiers == null)
                {
                    var cashierRecords = await BusinessProxyRepository.Stand.GetCashiersAsync(hotelId,
                        UserModel is { Internal: true } ? Guid.Empty : UserModelId, Hotel.SelectedLanguage.Code);
                    cashiers = cashierRecords.Select(c => new Cashier(c)).ToArray();
                    if (hotel != null) hotel.Cashiers = cashiers;
                }

                Cashiers = new ObservableCollection<Cashier>(cashiers);
            }

            Hotel.Instance.Cashiers = Cashiers;

            if (Cashiers?.Count == 0) return null;
            {
                Cashier? cashier = null;
                if (_lastCashier.HasValue)
                    if (Cashiers != null)
                        cashier = Cashiers.FirstOrDefault(c => c.Id == _lastCashier.Value);

                if (Cashiers != null && (cashier != null || Cashiers.Count <= 0)) return cashier;
                if (Cashiers != null) cashier = Cashiers.First();
                _lastCashier = cashier?.Id;

                return cashier;
            }

        }

        public async Task<Stand?> GetStandAsync()
        {
            State = UserPromptState.Loading;

            if (SelectedCashier != null)
            {
                var cashierId = SelectedCashier.Id;
                var language = Hotel.SelectedLanguage.Code;
                var hotelId = Hotel.Instance.Id;

                var stands = SelectedCashier.Stands;
                if (stands == null)
                {
                    var standRecords = await BusinessProxyRepository.Stand.GetStandsAsync(cashierId, language);
                    stands = standRecords.Select(s => new Stand(s)).ToArray();
                    SelectedCashier.Stands = stands;
                }

                Stands = new ObservableCollection<Stand>(stands);
            }

            Hotel.Instance.Stands = Stands;

            if (Stands != null && Stands.Count > 0)
            {
                Stand? stand = null;
                if (_lastStand.HasValue)
                    stand = Stands.FirstOrDefault(s => s.Id == _lastStand.Value);

                if (stand == null && Stands.Count > 0)
                {
                    stand = Stands.First();
                    _lastStand = stand.Id;
                }

                return stand;
            }

            return null;
        }

        public void CheckTicketSerieAndControlAccount()
        {
            IsCashierStandValid = true;

            if (SelectedStand != null && (SelectedStand.ControlAccount == null || Guid.Empty == SelectedStand.ControlAccount))
            {
                IsControlAccountValid = false;
                return;
            }
            else
            {
                // FALTA : Aqui deberiamos chequear si realmente es una cuenta de control
                IsControlAccountValid = true;
            }

            // Es una base de datos real
            try
            {
                IsTicketSeriesValid = Hotel.Instance.CurrentStandCashierContext.TicketSerie != null;
                IsInvoiceSeriesValid = Hotel.Instance.CurrentStandCashierContext.InvoiceSerie != null;
                IsReceiptSeriesValid = Hotel.Instance.CurrentStandCashierContext.ReceiptSerie != null;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                IsTicketSeriesValid = false;
                IsInvoiceSeriesValid = false;
                IsReceiptSeriesValid = false;
            }
        }

        public async Task CheckUserAndGetHotelsAsync()
        {
            IsUserValid = false;

            if ((IsByUserPwd && string.IsNullOrEmpty(UserName)) || (!IsByUserPwd && string.IsNullOrEmpty(Code)))
                Hotels = new ObservableCollection<HotelRecordModel>();
            else
            {
                var validationResult = IsByUserPwd
                    ? await Hotel.GetUserByNameAsync(UserName)
                    : await Hotel.GetUserByCodeAsync(Code);

                var user = validationResult.Item;
                if (user != null && (IsByUserPwd && user.Username == UserName || !IsByUserPwd && user.Code == Code))
                {
                    if (!IsByUserPwd || PasswordUtils.VerifyPassword(user.Password, Password))
                    {
                        UserModelId = user.Id;
                        UserModel = user;
                        IsUserValid = true;
                    }
                }

                if (IsUserValid)
                    Hotels = new ObservableCollection<HotelRecordModel>(await GetHotelsAsync(UserModel));
                else
                    Hotels = new ObservableCollection<HotelRecordModel>();
            }
        }

        private async Task<HotelRecordModel[]> GetHotelsAsync(User? user)
        {
            if (user.Hotels == null)
            {
                var records = await BusinessProxyRepository.Settings.GetHotelsAsync(user.Id);
                user.Hotels = records.Select(r => new HotelRecordModel(r))
                                     .OrderBy(x => x.Description)
                                     .ToArray();
            }

            return user.Hotels;
        }

        public void Reset()
        {
            Code = string.Empty;
            Password = string.Empty;
        }

        public LoginDataStore GetDataStore()
        {
            var data = new LoginDataStore();
            data.UtilId = UtilId ?? default(Guid);
            data.CashierId = SelectedCashier.Id;
            data.StandId = SelectedStand.Id;
            data.HotelId = (Guid)SelectedHotel.Id;

            return data;
        }

        protected override async void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);
            switch (propertyName)
            {
                case nameof(SelectedCashier):
                    if (SelectedCashier != null)
                    {
                        SelectedStand = null;
                        try
                        {
                            var stand = await GetStandAsync();
                            Hotel.Instance.Stands = Stands ?? [];
                            SelectedStand = stand;
                            Hotel.Instance.CurrentStandCashierContext = new StandCashierContext(SelectedStand, SelectedCashier);
                            State = UserPromptState.Enabled;
                        }
                        catch (Exception ex)
                        {
                            Trace.TraceError(ex.Message);
                            State = UserPromptState.Disabled;
                        }
                    }
                    break;
                case nameof(SelectedStand):
                    if (SelectedStand != null && SelectedCashier != null)
                        Hotel.Instance.CurrentStandCashierContext = new StandCashierContext(SelectedStand, SelectedCashier);
                    break;
            }
        }

        #endregion
    }

    public class LoginDataStore
    {
        #region Members

        public Guid UtilId { get; set; }

        public Guid CashierId { get; set; }

        public Guid StandId { get; set; }

        public Guid HotelId { get; set; }

        public StandCashierContext Context { get; set; }

        public override bool Equals(object other)
        {
            return (other is LoginDataStore) &&
                   UtilId == (other as LoginDataStore).UtilId &&
                   CashierId == (other as LoginDataStore).CashierId &&
                   StandId == (other as LoginDataStore).StandId &&
                   HotelId == (other as LoginDataStore).HotelId;
        }

        public override int GetHashCode()
        {
            int hashcode = 17;
            hashcode = 37 * hashcode + UtilId.GetHashCode();
            hashcode = 37 * hashcode + CashierId.GetHashCode();
            hashcode = 37 * hashcode + StandId.GetHashCode();
            hashcode = 37 * hashcode + HotelId.GetHashCode();
            return hashcode;
        }

        #endregion
    }

    public enum UserPromptState
    {
        Disabled, Enabled, Loading
    }
}