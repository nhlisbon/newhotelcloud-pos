﻿using System.Collections.Generic;
using NewHotel.WPF.MVVM;
using NewHotel.Pos.Hub.Model;

namespace NewHotel.WPF.App.Pos.Model
{
    public class ClientEntityRequestModel : BindableBase
    {
        private List<ClientRecord> RecordsProperty;
        public List<ClientRecord> Records
        {
            get { return RecordsProperty; }
            set { SetProperty(ref RecordsProperty, value); }
        }

        private string NameProperty;
        public string Name
        {
            get { return NameProperty; }
            set { SetProperty(ref NameProperty, value); }
        }
        private string FiscalNumberProperty;
        public string FiscalNumber
        {
            get { return FiscalNumberProperty; }
            set { SetProperty(ref FiscalNumberProperty, value); }
        }

        private string CountryProperty;
        public string Country
        {
            get { return CountryProperty; }
            set { SetProperty(ref CountryProperty, value); }
        }
    }
}