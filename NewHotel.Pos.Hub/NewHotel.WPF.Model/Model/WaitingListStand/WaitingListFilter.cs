﻿using System;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.App.Pos.Model.WaitingListStand
{
    public class WaitingListFilter : BindableBase
    {
        private Guid? _standId;
        private Guid? _salonId;
        private string _name;
        private string _phoneNumber;
        private DateTime? _registerDate;
        private short? _paxs;

        public Guid? StandId
        {
            get { return _standId; }
            set { SetProperty(ref _standId, value, nameof(StandId)); }
        }

        public Guid? SaloonId
        {
            get { return _salonId; }
            set { SetProperty(ref _salonId, value, nameof(SaloonId)); }
        }

        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value, nameof(Name)); }
        }

        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { SetProperty(ref _phoneNumber, value, nameof(PhoneNumber)); }
        }

        public DateTime? RegisterDate
        {
            get { return _registerDate; }
            set { SetProperty(ref _registerDate, value, nameof(RegisterDate)); }
        }

        public short? Paxs
        {
            get { return _paxs; }
            set { SetProperty(ref _paxs, value, nameof(Paxs)); }
        }
    }
}