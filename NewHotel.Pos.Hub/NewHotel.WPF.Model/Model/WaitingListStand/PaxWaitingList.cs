﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewHotel.Pos.Hub.Model.WaitingListStand.Contract;
using NewHotel.Pos.Hub.Model.WaitingListStand.Record;
using NewHotel.WPF.Model.Model;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.App.Pos.Model.WaitingListStand
{
    public class PaxWaitingList : BindableBase
    {
        private PaxWaitingListContract _contract;

        public PaxWaitingList(PaxWaitingListRecord record) : this()
        {
            Id = (Guid)record.Id;
            StandId = record.StandId;
            SaloonId = record.SalonId;
            Name = record.Name;
            PhoneNumber = record.PhoneNumber;
            RegisterDate = record.RegisterDate;
            Paxs = record.Paxs;
        }

        public PaxWaitingList()
        {
            _contract = new PaxWaitingListContract();
            _contract.Paxs = 1;
        }

        public override object DataContext
        {
            get { return _contract; }
            set { _contract = (PaxWaitingListContract)value; }
        }

        public Guid Id
        {
            get { return (Guid)_contract.Id; }
            set
            {
                _contract.Id = value;
                OnPropertyChanged(nameof(Id));
            }
        }

        public Guid? StandId
        {
            get { return _contract.StandId; }
            set
            {
                _contract.StandId = value;
                OnPropertyChanged(nameof(StandId));
            }
        }

        public Stand Stand
        {
            get { return Hotel.Instance.Stands.FirstOrDefault(s => s.Id == StandId); }
        }

        public Guid? SaloonId
        {
            get { return _contract.SaloonId; }
            set
            {
                _contract.SaloonId = value;
                OnPropertyChanged(nameof(SaloonId));
            }
        }

        public Saloon Saloon
        {
            get
            {
                if (Stand != null)
                    return Stand.Saloons.FirstOrDefault(s => s.Id == SaloonId);
                return null;
            }
        }

        public string Name
        {
            get { return _contract.Name; }
            set
            {
                _contract.Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public string PhoneNumber
        {
            get { return _contract.PhoneNumber; }
            set
            {
                _contract.PhoneNumber = value;
                OnPropertyChanged(nameof(PhoneNumber));
            }
        }

        public DateTime RegisterDate
        {
            get { return _contract.RegisterDate; }
            set
            {
                _contract.RegisterDate = value;
                OnPropertyChanged(nameof(RegisterDate));
            }
        }

        public short Paxs
        {
            get { return _contract.Paxs; }
            set
            {
                _contract.Paxs = value;
                OnPropertyChanged(nameof(Paxs));
            }
        }

        public PaxWaitingListContract AsContract()
        {
            return _contract;
        }

        public bool IsValid
        {
            get { return !string.IsNullOrEmpty(Name); }
        }
    }
}