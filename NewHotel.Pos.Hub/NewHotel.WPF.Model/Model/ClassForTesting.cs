﻿using System.Collections.ObjectModel;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Model
{
    /// <summary>
    /// Esto es una clase provisional para pruebas
    /// </summary>
    public class TablesList : ObservableCollection<Table>
    {
        public TablesList()
        {
            //Add(new Table() { Name = "Saloon 1", Priority = 1, State = Table.States.Ready, Type = "Saloon" });
            //Add(new Table() { Name = "Saloon 2", Priority = 1, State = Table.States.Ready, Type = "Saloon" });
            //Add(new Table() { Name = "Saloon 3", Priority = 1, State = Table.States.Ready, Type = "Saloon" });
            //Add(new Table() { Name = "Saloon 4", Priority = 1, State = Table.States.Ready, Type = "Saloon" });
            //Add(new Table() { Name = "Saloon 5", Priority = 1, State = Table.States.Ready, Type = "Saloon" });
            //Add(new Table() { Name = "Saloon 6", Priority = 1, State = Table.States.Ready, Type = "Saloon" });
            //Add(new Table() { Name = "Saloon 7", Priority = 1, State = Table.States.Ready, Type = "Saloon" });
            //Add(new Table() { Name = "Saloon 8", Priority = 1, State = Table.States.Ready, Type = "Saloon" });
            //Add(new Table() { Name = "Saloon 9", Priority = 1, State = Table.States.Ready, Type = "Saloon" });
            //Add(new Table() { Name = "Saloon 10", Priority = 1, State = Table.States.Ready, Type = "Saloon" });
            //Add(new Table() { Name = "Saloon 11", Priority = 1, State = Table.States.Ready, Type = "Saloon" });
            //Add(new Table() { Name = "Saloon 12", Priority = 1, State = Table.States.Ready, Type = "Saloon" });
            //Add(new Table() { Name = "Saloon 13", Priority = 1, State = Table.States.Ready, Type = "Saloon" });
            //Add(new Table() { Name = "Saloon 14", Priority = 1, State = Table.States.Ready, Type = "Saloon" });
            //Add(new Table() { Name = "Saloon 15", Priority = 1, State = Table.States.Ready, Type = "Saloon" });
            //Add(new Table() { Name = "Saloon 16", Priority = 1, State = Table.States.Ready, Type = "Saloon" });
            //Add(new Table() { Name = "Saloon 17", Priority = 1, State = Table.States.Ready, Type = "Saloon" });
            //Add(new Table() { Name = "Saloon 18", Priority = 1, State = Table.States.Ready, Type = "Saloon" });
            //Add(new Table() { Name = "Saloon 19", Priority = 1, State = Table.States.Ready, Type = "Saloon" });
            //Add(new Table() { Name = "Saloon 20", Priority = 1, State = Table.States.Ready, Type = "Saloon" });

            //Add(new Table() { Name = "Bar 1", Priority = 4, State = Table.States.Ready, Type = "Bar" });
            //Add(new Table() { Name = "Bar 2", Priority = 4, State = Table.States.Ready, Type = "Bar" });
            //Add(new Table() { Name = "Bar 3", Priority = 4, State = Table.States.Ready, Type = "Bar" });
            //Add(new Table() { Name = "Bar 4", Priority = 4, State = Table.States.Ready, Type = "Bar" });
            //Add(new Table() { Name = "Bar 5", Priority = 4, State = Table.States.Ready, Type = "Bar" });
            //Add(new Table() { Name = "Bar 6", Priority = 4, State = Table.States.Ready, Type = "Bar" });
            //Add(new Table() { Name = "Bar 7", Priority = 4, State = Table.States.Ready, Type = "Bar" });
        }
    }

    public class ProductGroupsList : ObservableCollection<Category>
    {
        public ProductGroupsList()
        {
            //        Stream stream = null;
            //#region Drinks


            //Add(new Category()
            //{
            //    //Description = "Drinks",
            //    Image = new Blob(new BinaryReader(
            //        stream = (new FileInfo("../../Resources/Categories/brown-drink-cocktail.png")).
            //        OpenRead()).ReadBytes((int)stream.Length)),
            //    SubCategories = new ObservableCollection<Category>()
            //    {
            //        #region Sodas

            //        new Category()
            //        {
            //            //Description = "Sodas",
            //            //Image = new Blob(new BinaryReader(
            //            //    (stream = ((new BitmapImage(new Uri("pack://application:,,,/NewHotel.Pos;component/Resources/Categories/brown-soda-128.png"))).
            //            //    StreamSource))).ReadBytes((int)stream.Length)),
            //            SubCategories = new ObservableCollection<Category>()
            //            {
            //                new Category()
            //                {
            //                    //Description = "Normal",
            //                    Products = new ObservableCollection<Product>()
            //                    {
            //                        new Product(){
            //                            Description = "Coca Cola",
            //                            Price = 1.75
            //                        },
            //                        new Product(){
            //                            Description = "Pepsi",
            //                            Price = 1.5
            //                        },
            //                        new Product(){
            //                            Description = "Fanta",
            //                            Price = 1.5
            //                        },
            //                        new Product(){
            //                            Description = "Fanta Lemmon",
            //                            Price = 1.5
            //                        }
            //                    }
                                
            //                },
            //                new Category(){
            //                    //Description = "Light",
            //                    Products = new ObservableCollection<Product>()
            //                    {
            //                        new Product(){
            //                            Description = "Coca Cola",
            //                            Price = 1.75
            //                        },
            //                        new Product(){
            //                            Description = "Pepsi",
            //                            Price = 1.5
            //                        }
            //                    }
            //                }
            //            }
            //        },

            //        #endregion
            //        #region Wine

            

            //        new Category()
            //        {
            //            //Description = "Wine",
            //            //Image = new Blob(new BinaryReader(
            //            //        (stream = ((new BitmapImage(new Uri("pack://application:,,,/NewHotel.Pos;component/Resources/Categories/brown-wine-128.png"))).
            //            //        StreamSource))).
            //            //        ReadBytes((int)stream.Length)),
            //            SubCategories = new ObservableCollection<Category>()
            //            {
            //                new Category(){
            //                    //Description = "White",
            //                    Products = new ObservableCollection<Product>(){
            //                        new Product(){
            //                            Description = "CocaCola",
            //                            Price = 0.85
            //                        },
            //                        new Product(){
            //                            Description = "Fanta",
            //                            Price = 0.85
            //                        },
            //                    }
            //                },
            //                new Category(){
            //                    //Description = "Red",
            //                    Products = new ObservableCollection<Product>(){
            //                        new Product(){
            //                            Description = "CocaCola",
            //                            Price = 0.85
            //                        },
            //                        new Product(){
            //                            Description = "Fanta",
            //                            Price = 0.85
            //                        },
            //                    }
            //                },
            //                new Category(){
            //                    //Description = "Rose",
            //                    Products = new ObservableCollection<Product>(){
            //                        new Product(){
            //                            Description = "CocaCola",
            //                            Price = 0.85
            //                        },
            //                        new Product(){
            //                            Description = "Fanta",
            //                            Price = 0.85
            //                        },
            //                    }
            //                },
            //                new Category(){
            //                    //Description = "Sparkling",
            //                    Products = new ObservableCollection<Product>(){
            //                        new Product(){
            //                            Description = "CocaCola",
            //                            Price = 0.85
            //                        },
            //                        new Product(){
            //                            Description = "Fanta",
            //                            Price = 0.85
            //                        },
            //                    }
            //                }
            //            }
            //        },

            //        #endregion
            //        #region Water
                    
            //        new Category()
            //        {
            //            //Description = "Water",
            //            //Image = new Blob(new BinaryReader(
            //            //    (stream = ((new BitmapImage(new Uri("pack://application:,,,/NewHotel.Pos;component/Resources/Categories/brown-water-128.png"))).
            //            //    StreamSource))).ReadBytes((int)stream.Length)),
            //            SubCategories = new ObservableCollection<Category>(){
            //                new Category(){
            //                    //Description = "Natural",
            //                    Products = new ObservableCollection<Product>(){
            //                        new Product(){
            //                            Description = "Mango",
            //                            Price = 0.85
            //                        },
            //                        new Product(){
            //                            Description = "Guava",
            //                            Price = 0.85
            //                        },
            //                        new Product(){
            //                            Description = "Peach",
            //                            Price = 0.85
            //                        },
            //                        new Product(){
            //                            Description = "Pineapple",
            //                            Price = 0.85
            //                        }
            //                    }
            //                },
            //                new Category(){
            //                    //Description = "Carbonated",
            //                    Products = new ObservableCollection<Product>(){
            //                        new Product(){
            //                            Description = "Grape",
            //                            Price = 0.5
            //                        },
            //                        new Product(){
            //                            Description = "Guava",
            //                            Price = 0.5
            //                        },
            //                        new Product(){
            //                            Description = "Peach",
            //                            Price = 0.5
            //                        },
            //                        new Product(){
            //                            Description = "Pineapple",
            //                            Price = 0.5
            //                        },
            //                        new Product(){
            //                            Description = "Mango",
            //                            Price = 0.5
            //                        },
            //                        new Product(){
            //                            Description = "Cocktail",
            //                            Price = 0.5
            //                        },
            //                        new Product(){
            //                            Description = "Mediterraneo",
            //                            Price = 0.5
            //                        }
            //                    }
            //                }
            //            }
            //        },

            //        #endregion
            //        #region Coffee
                    
            //        new Category()
            //        {
            //            //Description = "Coffee",
            //            //Image = new Blob(new BinaryReader(
            //            //    (stream = ((new BitmapImage(new Uri("pack://application:,,,/NewHotel.Pos;component/Resources/Categories/brown-coffee-128.png"))).
            //            //    StreamSource))).ReadBytes((int)stream.Length)),
            //            SubCategories = new ObservableCollection<Category>(){
            //                new Category(){
            //                    //Description = "Coffee",
            //                    Products = new ObservableCollection<Product>(){
            //                        new Product(){
            //                            Description = "Americano",
            //                            Price = 0.5
            //                        },
            //                        new Product(){
            //                            Description = "Black coffee",
            //                            Price = 0.5
            //                        },
            //                        new Product(){
            //                            Description = "Cafe au Lait",
            //                            Price = 0.5
            //                        },
            //                        new Product(){
            //                            Description = "Cappuccino",
            //                            Price = 0.5
            //                        },
            //                        new Product(){
            //                            Description = "Latte",
            //                            Price = 0.5
            //                        },
            //                        new Product(){
            //                            Description = "Macchiato",
            //                            Price = 0.5
            //                        }
            //                    }
            //                }
            //            }
            //        }

            //    #endregion
            //    }
            //});

            //#endregion
            //#region Foods

            //Add(new Category()
            //{
            //    //Description = "Food",
            //    //Image = new Blob(new BinaryReader(
            //    //    (stream = ((new BitmapImage(new Uri("pack://application:,,,/NewHotel.Pos;component/Resources/Categories/brown-meat.png"))).
            //    //    StreamSource))).ReadBytes((int)stream.Length)),
            //    SubCategories = new ObservableCollection<Category>()
            //    {
            //        #region Pizzas
                    
            //        new Category()
            //        {
            //            //Description = "Pizzas",
            //            //Image = new Blob(new BinaryReader(
            //            //    (stream = ((new BitmapImage(new Uri("pack://application:,,,/NewHotel.Pos;component/Resources/Categories/brown-dinner-128.png"))).
            //            //    StreamSource))).ReadBytes((int)stream.Length)),
            //            SubCategories = new ObservableCollection<Category>()
            //            {
            //                new Category()
            //                {
            //                    //Description = "Small",
            //                    Products = new ObservableCollection<Product>()
            //                    {
            //                        new Product(){
            //                            Description = "Cheese",
            //                            Price = 1.2
            //                        },
            //                        new Product(){
            //                            Description = "Ham",
            //                            Price = 1.8
            //                        },
            //                        new Product(){
            //                            Description = "Onions",
            //                            Price = 2.2
            //                        }
            //                    }
            //                },
            //                new Category()
            //                {
            //                    //Description = "Normal",
            //                    Products = new ObservableCollection<Product>(){
            //                        new Product(){
            //                            Description = "Cheese",
            //                            Price = 1.2
            //                        },
            //                        new Product(){
            //                            Description = "Vegetables",
            //                            Price = 1.7
            //                        },
            //                        new Product(){
            //                            Description = "Ham",
            //                            Price = 8.6
            //                        },
            //                        new Product(){
            //                            Description = "Sausage",
            //                            Price = 23.5
            //                        },
            //                        new Product(){
            //                            Description = "Tuna",
            //                            Price = 2.9
            //                        },
            //                        new Product(){
            //                            Description = "Onions",
            //                            Price = 2.2
            //                        },
            //                        new Product(){
            //                            Description = "Shrimps",
            //                            Price = 4.15
            //                        }

            //                    }
            //                },
            //                new Category(){
            //                    //Description = "Familiar",
            //                    Products = new ObservableCollection<Product>(){
            //                        new Product(){
            //                            Description = "Cheese",
            //                            Price = 1.2
            //                        },
            //                        new Product(){
            //                            Description = "Vegetables",
            //                            Price = 1.7
            //                        },
            //                        new Product(){
            //                            Description = "Ham",
            //                            Price = 1.8
            //                        },
            //                        new Product(){
            //                            Description = "Mix",
            //                            Price = 2.6
            //                        },
            //                    }
            //                }
            //            }
            //        },

            //        #endregion
            //        #region Pasta

            //        new Category()
            //        {
            //            //Description = "Pasta",
            //            //Image = new Blob(new BinaryReader(
            //            //    (stream = ((new BitmapImage(new Uri("pack://application:,,,/NewHotel.Pos;component/Resources/Categories/brown-dining-128.png"))).
            //            //    StreamSource))).ReadBytes((int)stream.Length)),
            //            SubCategories = new ObservableCollection<Category>()
            //            {
            //                new Category()
            //                {
            //                    //Description = "Spaguettis",
            //                    Products = new ObservableCollection<Product>(){
            //                        new Product(){
            //                            Description = "Carbonara",
            //                            Price = 3.5,
            //                        },
            //                        new Product(){
            //                            Description = "Ham",
            //                            Price = 3.8,
            //                        },
            //                        new Product(){
            //                            Description = "Napolitana",
            //                            Price = 3,
            //                        }
            //                    }

            //                },
            //                new Category()
            //                {
            //                    //Description = "Cannelloni",
            //                    Products = new ObservableCollection<Product>(){
            //                        new Product(){
            //                            Description = "Ham",
            //                            Price = 2.5,
            //                        },
            //                        new Product(){
            //                            Description = "Pesto",
            //                            Price = 2.5,
            //                        }
            //                    }

            //                }
            //            }
            //        },

            //        #endregion
            //        #region Salads

            //        new Category()
            //        {
            //            //Description = "Salads",
            //            //Image = new Blob(new BinaryReader(
            //            //    (stream = ((new BitmapImage(new Uri("pack://application:,,,/NewHotel.Pos;component/Resources/Categories/brown-salad-128.png"))).
            //            //    StreamSource))).ReadBytes((int)stream.Length)),
            //            SubCategories = new ObservableCollection<Category>()
            //            {
            //                new Category()
            //                {
            //                    //Description = "Vegetable",
            //                    Products = new ObservableCollection<Product>(){
            //                        new Product(){
            //                            Description = "Greek",
            //                            Price = 3.5,
            //                        },
            //                        new Product(){
            //                            Description = "Caesar",
            //                            Price = 3.5,
            //                        }
            //                    }
            //                },
            //                new Category()
            //                {
            //                    //Description = "Fruit",
            //                    Products = new ObservableCollection<Product>(){
            //                        new Product(){
            //                            Description = "Fruit",
            //                            Price = 3.5,
            //                        },
            //                        new Product(){
            //                            Description = "Ambrosia",
            //                            Price = 3.5,
            //                        },
            //                        new Product(){
            //                            Description = "Waldorf",
            //                            Price = 3.5,
            //                        }
            //                    }
            //                },
            //                new Category()
            //                {
            //                    //Description = "Bean",
            //                    Products = new ObservableCollection<Product>(){
            //                        new Product(){
            //                            Description = "Bean",
            //                            Price = 3.5,
            //                        },
            //                        new Product(){
            //                            Description = "Fattoush",
            //                            Price = 3.5,
            //                        },
            //                        new Product(){
            //                            Description = "Piyaz",
            //                            Price = 3.5,
            //                        }
            //                    }
            //                },
            //                new Category()
            //                {
            //                    //Description = "Meat",
            //                    Products = new ObservableCollection<Product>(){
            //                        new Product(){
            //                            Description = "Chef",
            //                            Price = 3.5,
            //                        },
            //                        new Product(){
            //                            Description = "Chicken",
            //                            Price = 3.5,
            //                        }
            //                    }
            //                }
            //            }
            //        },

            //        #endregion
            //        #region Soups
                    
            //        new Category()
            //        {
            //            //Description = "Soups",
            //            //Image = new Blob(new BinaryReader(
            //            //    (stream = ((new BitmapImage(new Uri("pack://application:,,,/NewHotel.Pos;component/Resources/Categories/brown-soup-128.png"))).
            //            //    StreamSource))).ReadBytes((int)stream.Length)),
            //            SubCategories = new ObservableCollection<Category>()
            //            {
            //                new Category()
            //                {
            //                    //Description = "Meat and vegetable",
            //                    Products = new ObservableCollection<Product>(){
            //                        new Product(){
            //                            Description = "Ajiaco",
            //                            Price = 3.5,
            //                        },
            //                        new Product(){
            //                            Description = "Stone",
            //                            Price = 3.8,
            //                        },
            //                        new Product(){
            //                            Description = "Green",
            //                            Price = 3.5,
            //                        },
            //                        new Product(){
            //                            Description = "Cazuela",
            //                            Price = 5,
            //                        },
            //                        new Product(){
            //                            Description = "Tomato",
            //                            Price = 3.5,
            //                        },
            //                        new Product(){
            //                            Description = "Fish",
            //                            Price = 5.5,
            //                        }
            //                    }
            //                },
            //                new Category()
            //                {
            //                    //Description = "Creams",
            //                    Products = new ObservableCollection<Product>(){
            //                        new Product(){
            //                            Description = "Tomato",
            //                            Price = 3.5,
            //                        },
            //                        new Product(){
            //                            Description = "Broccoly",
            //                            Price = 3.8,
            //                        },
            //                        new Product(){
            //                            Description = "Carrot",
            //                            Price = 3.6,
            //                        },
            //                        new Product(){
            //                            Description = "Chicken",
            //                            Price = 3.6,
            //                        },
            //                        new Product(){
            //                            Description = "Potato",
            //                            Price = 3.6,
            //                        }
            //                    }
            //                }
            //            }
            //        },
                    
            //        #endregion
            //        #region Ice Creams
                    
            //        new Category()
            //        {
            //            //Description = "Ice Creams",
            //            //Image = new Blob(new BinaryReader(
            //            //    (stream = ((new BitmapImage(new Uri("pack://application:,,,/NewHotel.Pos;component/Resources/Categories/brown-icecream-128.png"))).
            //            //    StreamSource))).ReadBytes((int)stream.Length)),
            //            SubCategories = new ObservableCollection<Category>()
            //            {
            //                new Category()
            //                {
            //                    //Description = " International",
            //                    Products = new ObservableCollection<Product>()
            //                    {
            //                        new Product()
            //                        {
            //                            Description = "Vanilla",
            //                            Price = 3.5,
            //                        },
            //                        new Product()
            //                        {
            //                            Description = "Chocolate",
            //                            Price = 3.5,
            //                        },
            //                        new Product()
            //                        {
            //                            Description = "Strawberry",
            //                            Price = 3.5,
            //                        },
            //                        new Product()
            //                        {
            //                            Description = "Neapolitan",
            //                            Price = 3.5,
            //                        },
            //                        new Product()
            //                        {
            //                            Description = "Chocolate chip",
            //                            Price = 3.5,
            //                        },
            //                        new Product()
            //                        {
            //                            Description = "French vanilla",
            //                            Price = 3.5,
            //                        },
            //                        new Product()
            //                        {
            //                            Description = " Cookies and cream",
            //                            Price = 3.5,
            //                        },
            //                        new Product()
            //                        {
            //                            Description = "Cherry",
            //                            Price = 3.5,
            //                        },
            //                        new Product()
            //                        {
            //                            Description = "Coffee",
            //                            Price = 3.5,
            //                        },
            //                        new Product()
            //                        {
            //                            Description = "Rocky road",
            //                            Price = 3.5,
            //                        }
            //                    }
            //                }
            //            }
            //        }
                    
            //        #endregion
            //    }
            //});

            //#endregion
        }
    }

    //F111029:ped033:02:Lista provisional de los house-use
    public class HouseUseList : ObservableCollection<string>
    {
        public HouseUseList()
        {
            Add("Mr. Bolton, Nolan");
            Add("Sales Department");
            Add("Mrs. Tyrell, Melisandre");
            Add("Mr. Ryan, Nolan");
        }
    }
    //public class PaymentTypeList : ObservableCollection<PaymentMethod>
    //{
    //    public PaymentTypeList()
    //    {
    //        Add(new PaymentMethod()
    //        {
    //            Type = PaymentMethod.PaymentTypeEnum.Cash,
    //            Name = "Cash",
    //            ImageName = "cashPay.png",
    //            //Discount = new Discount()
    //            //{
    //            //    Type = Discount.DiscountType.Local,
    //            //    ValueType = Model.Discount.DiscountValueType.Percent,
    //            //    Value = 10
    //            //}
    //        });

    //        Add(new PaymentMethod()
    //        {
    //            Type = PaymentMethod.PaymentTypeEnum.CreditCard,
    //            Name = "AmericanExpress",
    //            ImageName = "americanExpPay.png"
    //        });
    //        Add(new PaymentMethod()
    //        {
    //            Type = PaymentMethod.PaymentTypeEnum.CreditCard,
    //            Name = "Citi",
    //            ImageName = "citiPay.png"
    //        });
    //        Add(new PaymentMethod()
    //        {
    //            Type = PaymentMethod.PaymentTypeEnum.CreditCard,
    //            Name = "GiftCard",
    //            ImageName = "giftCardPay.png",
    //            //Discount = new Discount()
    //            //{
    //            //    Type = Discount.DiscountType.Global,
    //            //    ValueType = Model.Discount.DiscountValueType.Percent,
    //            //    Value = 10
    //            //}
    //        });
    //        Add(new PaymentMethod()
    //        {
    //            Type = PaymentMethod.PaymentTypeEnum.CreditCard,
    //            Name = "VISA Electron",
    //            ImageName = "visaElectronPay.png"
    //        });
    //        Add(new PaymentMethod()
    //        {
    //            Type = PaymentMethod.PaymentTypeEnum.CreditCard,
    //            Name = "VISA",
    //            ImageName = "visaPay.png"
    //        });
    //        Add(new PaymentMethod()
    //        {
    //            Type = PaymentMethod.PaymentTypeEnum.CreditCard,
    //            Name = "VISA",
    //            ImageName = "visaPay.png"
    //        });
    //        Add(new PaymentMethod()
    //        {
    //            Type = PaymentMethod.PaymentTypeEnum.CreditCard,
    //            Name = "VISA",
    //            ImageName = "visaPay.png"
    //        });
    //        Add(new PaymentMethod()
    //        {
    //            Type = PaymentMethod.PaymentTypeEnum.CreditCard,
    //            Name = "VISA",
    //            ImageName = "visaPay.png"
    //        });
    //        Add(new PaymentMethod()
    //        {
    //            Type = PaymentMethod.PaymentTypeEnum.CreditCard,
    //            Name = "VISA",
    //            ImageName = "visaPay.png"
    //        });
    //        Add(new PaymentMethod()
    //        {
    //            Type = PaymentMethod.PaymentTypeEnum.CreditCard,
    //            Name = "VISA",
    //            ImageName = "visaPay.png"
    //        });
    //        Add(new PaymentMethod()
    //        {
    //            Type = PaymentMethod.PaymentTypeEnum.CreditCard,
    //            Name = "VISA",
    //            ImageName = "visaPay.png"
    //        });
    //        Add(new PaymentMethod()
    //        {
    //            Type = PaymentMethod.PaymentTypeEnum.CreditCard,
    //            Name = "VISA",
    //            ImageName = "visaPay.png"
    //        });
    //        Add(new PaymentMethod()
    //        {
    //            Type = PaymentMethod.PaymentTypeEnum.CreditCard,
    //            Name = "VISA",
    //            ImageName = "visaPay.png"
    //        });

    //    }
    //}

    public class BillList : ObservableCollection<Money>
    {
        public BillList()
        {
            //Add(new Money()
            //{
            //    Description = "500 Euros",
            //    ImageName = "500_Euro.png",
            //    Value = 500
            //});
            Add(new Money()
            {
                Description = "200 Euros",
                ImageName = "200_Euro.png",
                Value = 200
            });
            Add(new Money()
            {
                Description = "100 Euros",
                ImageName = "100_Euro.png",
                Value = 100
            });
            Add(new Money()
            {
                Description = "50 Euros",
                ImageName = "50_Euro.png",
                Value = 50
            });
            Add(new Money()
            {
                Description = "20 Euros",
                ImageName = "20_Euro.png",
                Value = 20
            });
            Add(new Money()
            {
                Description = "10 Euros",
                ImageName = "10_Euro.png",
                Value = 10
            });
            Add(new Money()
            {
                Description = "5 Euros",
                ImageName = "5_Euro.png",
                Value = 5
            });


        }
    }

    public class CoinList : ObservableCollection<Money>
    {
        public CoinList()
        {
            Add(new Money()
            {
                Description = "2 Euros",
                ImageName = "2_Euro_c.png",
                Value = 2
            });
            Add(new Money()
            {
                Description = "1 Euro",
                ImageName = "1_Euro_c.png",
                Value = 1
            });
            Add(new Money()
            {
                Description = "50 Cent Euro",
                ImageName = "50c_Euro_c.png",
                Value = 0.5M
            });
            Add(new Money()
            {
                Description = "20 Cent Euro",
                ImageName = "20c_Euro_c.png",
                Value = 0.2M
            });
            Add(new Money()
            {
                Description = "10 Cent Euro",
                ImageName = "10c_Euro_c.png",
                Value = 0.1M
            });
            Add(new Money()
            {
                Description = "5 Cent Euro",
                ImageName = "5c_Euro_c.png",
                Value = 0.05M
            });
        }
    }
}
