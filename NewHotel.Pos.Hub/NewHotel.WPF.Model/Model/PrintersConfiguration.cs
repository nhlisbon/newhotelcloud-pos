﻿using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Printing;
using System.ServiceModel.Configuration;
using NewHotel.Contracts;
using NewHotel.WPF.Model;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.App.Pos.Model
{
    public class PrintersConfiguration : BindableBase
    {
        #region Constants

        public const string BallotPrinterServerSetting = "BallotPrinterServer";
        public const string TicketPrinterServerSetting = "TicketPrinterServer";
        public const string InvoicePrinterServerSetting = "InvoicePrinterServer";

        #endregion
        #region Variables

        private string _ticketDescription;
        private string _ticketServer;
        private string _invoiceDescription;
        private string _invoiceServer;
        private string _drawerDescription;
        private string _drawerServer;
        private string _escCode;
        private bool _hasDrawer;
        private ObservableCollection<string> _ticketPrinters;
        private ObservableCollection<string> _ballotPrinters;
        private ObservableCollection<string> _invoicePrinters;
        private ObservableCollection<string> _drawerPrinters;
        private bool _hasSerialPort;
        private string _serialPortDescription;
        private int _serialPortBaudRate;
        private int _serialPortParity;
        private int _serialPortDataBit;
        private int _serialPortStopBits;
        private string _fiscalPrinterServiceAddress;
        private string _ballotDescription;
        private string _ballotServer;

        #endregion
        #region Contructor

        public PrintersConfiguration(Guid standId)
        {
            Id = standId;

            var server = new LocalPrintServer();

            var value = MgrServices.GetPrinterSettings(TicketPrinterServerSetting);
            TicketServer = string.IsNullOrEmpty(value) ? server.Name : value;

            value = MgrServices.GetPrinterSettings(BallotPrinterServerSetting);
            BallotServer = string.IsNullOrEmpty(value) ? server.Name : value;

            value = MgrServices.GetPrinterSettings(InvoicePrinterServerSetting);
            InvoiceServer = string.IsNullOrEmpty(value) ? server.Name : value;

            value = MgrServices.GetPrinterSettings("DrawerPrinterServer");
            DrawerServer = string.IsNullOrEmpty(value) ? server.Name : value;

            value = MgrServices.GetPrinterSettings("TicketPrinterDescription");
            TicketDescription = string.IsNullOrEmpty(value) ? string.Empty : value;

            value = MgrServices.GetPrinterSettings("BallotPrinterDescription");
            BallotDescription = string.IsNullOrEmpty(value) ? string.Empty : value;

            value = MgrServices.GetPrinterSettings("InvoicePrinterDescription");
            InvoiceDescription = string.IsNullOrEmpty(value) ? string.Empty : value;

            value = MgrServices.GetPrinterSettings("DrawerPrinterDescription");
            DrawerDescription = string.IsNullOrEmpty(value) ? string.Empty : value;

            value = MgrServices.GetPrinterSettings("HasDrawer");
            if (!string.IsNullOrEmpty(value) && bool.TryParse(value, out var hasDrawer))
                HasDrawer = hasDrawer;

            EscCode = MgrServices.GetPrinterSettings("EscCode");

            value = MgrServices.GetPrinterSettings("HasSerialPort");
            if (!string.IsNullOrEmpty(value) && bool.TryParse(value, out var hasSerialPort))
                HasSerialPort = hasSerialPort;

            value = MgrServices.GetPrinterSettings("SerialPortDescription");
            SerialPortDescription = string.IsNullOrEmpty(value) ? string.Empty : value;

            value = MgrServices.GetPrinterSettings("SerialPortNumber");
            if (!string.IsNullOrEmpty(value))
            {
                MgrServices.RemoveConfigurationSetting("SerialPortNumber");
                if (int.TryParse(value, out var baudRate))
                {
                    SerialPortBaudRate = baudRate;
                    MgrServices.SetPrinterSettings("SerialPortBaudRate", value);
                }
            }
            else
            {
                value = MgrServices.GetPrinterSettings("SerialPortBaudRate");
                if (int.TryParse(value, out var baudRate))
                    SerialPortBaudRate = baudRate;
            }

            if (SerialPortBaudRate == 0)
                SerialPortBaudRate = 9600;

            value = MgrServices.GetPrinterSettings("SerialPortParity");
            SerialPortParity = int.TryParse(value, out var parity) ? parity : 0;

            value = MgrServices.GetPrinterSettings("SerialPortDataBit");
            SerialPortDataBit = int.TryParse(value, out var dataBit) ? dataBit : 8;

            value = MgrServices.GetPrinterSettings("SerialPortStopBits");
            SerialPortStopBits = int.TryParse(value, out var stopBits) ? stopBits : 1;

            FiscalPrinterServiceAddress = GetAddressEndPoint(MgrServices.Settings.FiscalPrinterEndpointName);
        }

        #endregion
        #region Properties

        public Guid Id { get; set; }

        public string TicketDescription
        {
            get => _ticketDescription;
            set => SetProperty(ref _ticketDescription, value);
        }

        public string TicketServer
        {
            get => _ticketServer;
            set => SetProperty(ref _ticketServer, value);
        }

        public string BallotDescription
        {
            get => _ballotDescription;
            set => SetProperty(ref _ballotDescription, value);
        }

        public string BallotServer
        {
            get => _ballotServer;
            set => SetProperty(ref _ballotServer, value);
        }

        public string InvoiceDescription
        {
            get => _invoiceDescription;
            set => SetProperty(ref _invoiceDescription, value);
        }

        public string InvoiceServer
        {
            get => _invoiceServer;
            set => SetProperty(ref _invoiceServer, value);
        }

        public string DrawerDescription
        {
            get => _drawerDescription;
            set => SetProperty(ref _drawerDescription, value);
        }

        public string DrawerServer
        {
            get => _drawerServer;
            set => SetProperty(ref _drawerServer, value);
        }

        public bool HasDrawer
        {
            get => _hasDrawer;
            set => SetProperty(ref _hasDrawer, value);
        }

        public string EscCode
        {
            get => _escCode;
            set => SetProperty(ref _escCode, value);
        }

        public bool HasSerialPort
        {
            get => _hasSerialPort;
            set => SetProperty(ref _hasSerialPort, value);
        }

        public string SerialPortDescription
        {
            get => _serialPortDescription;
            set => SetProperty(ref _serialPortDescription, value);
        }

        public int SerialPortBaudRate
        {
            get => _serialPortBaudRate;
            set => SetProperty(ref _serialPortBaudRate, value);
        }

        public int SerialPortParity
        {
            get => _serialPortParity;
            set => SetProperty(ref _serialPortParity, value);
        }

        public int SerialPortDataBit
        {
            get => _serialPortDataBit;
            set => SetProperty(ref _serialPortDataBit, value);
        }

        public int SerialPortStopBits
        {
            get => _serialPortStopBits;
            set => SetProperty(ref _serialPortStopBits, value);
        }

        #region Fiscal Printers

        public FiscalPOSType FiscalPrinter => Hotel.Instance.FiscalPrinter;

        public bool HasFiscalPrinter => Hotel.Instance.HasFiscalPrinter;

        public string FiscalPrinterServiceAddress
        {
            get => _fiscalPrinterServiceAddress;
            set => SetProperty(ref _fiscalPrinterServiceAddress, value);
        }

        #endregion

        public ObservableCollection<string> TicketPrinters
        {
            get => _ticketPrinters;
            set => SetProperty(ref _ticketPrinters, value);
        }

        public ObservableCollection<string> BallotPrinters
        {
            get => _ballotPrinters;
            set => SetProperty(ref _ballotPrinters, value);
        }

        public ObservableCollection<string> InvoicePrinters
        {
            get => _invoicePrinters;
            set => SetProperty(ref _invoicePrinters, value);
        }

        public ObservableCollection<string> DrawerPrinters
        {
            get => _drawerPrinters;
            set => SetProperty(ref _drawerPrinters, value);
        }

        #endregion
        #region Methods

        public byte[] GetEscCode()
        {
            try
            {
                var esc = EscCode.Trim('[', ']');
                var listEsc = esc.Split(';');
                var ret = new byte[listEsc.Count()];
                for (int i = 0; i < ret.Length; i++)
                    ret[i] = byte.Parse(listEsc.ElementAt(i));

                return ret;
            }
            catch
            { 
                return new byte[0];
            }
        }

        private static ObservableCollection<string> GetPrinters(PrintServer server)
        {
            return new ObservableCollection<string>(
                server.GetPrintQueues(new EnumeratedPrintQueueTypes[] { EnumeratedPrintQueueTypes.Local })
                .Select(x => x.Name)
                .Concat(server.GetPrintQueues(new EnumeratedPrintQueueTypes[] { EnumeratedPrintQueueTypes.Connections })
                .Select(x => x.Description)));
        }

        public void SetTicketPrinters()
        {
            PrintServer server = null;
            try
            {
                server = new PrintServer(TicketServer);
            }
            catch { }

            TicketPrinters = server != null ? GetPrinters(server) : null;
        }

        public void SetBallotPrinters()
        {
            PrintServer server = null;
            try
            {
                server = new PrintServer(BallotServer);
            }
            catch { }

            BallotPrinters = server != null ? GetPrinters(server) : null;
        }

        public void SetInvoicePrinters()
        {
            PrintServer server = null;
            try
            {
                server = new PrintServer(InvoiceServer);
            }
            catch { }

            InvoicePrinters = server != null ? GetPrinters(server) : null;
        }

        public void SetDrawerPrinters()
        {
            PrintServer server = null;
            try
            {
                server = new PrintServer(DrawerServer);
            }
            catch { }

            DrawerPrinters = server != null ? GetPrinters(server) : null;
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);

            switch (propertyName)
            {
                case nameof(TicketServer):
                    SetTicketPrinters();
                    MgrServices.SetPrinterSettings(TicketPrinterServerSetting, TicketServer);
                    break;
                case nameof(BallotServer):
                    SetBallotPrinters();
                    MgrServices.SetPrinterSettings(BallotPrinterServerSetting, BallotServer);
                    break;
                case nameof(InvoiceServer):
                    SetInvoicePrinters();
                    MgrServices.SetPrinterSettings(InvoicePrinterServerSetting, InvoiceServer);
                    break;
                case nameof(DrawerServer):
                    SetDrawerPrinters();
                    MgrServices.SetPrinterSettings("DrawerPrinterServer", DrawerServer);
                    break;
                case nameof(TicketDescription):
                    MgrServices.SetPrinterSettings("TicketPrinterDescription", TicketDescription);
                    SetDefaultPrinterMargins(TicketDescription);
                    break;
                case nameof(BallotDescription):
                    MgrServices.SetPrinterSettings("BallotPrinterDescription", BallotDescription);
                    SetDefaultPrinterMargins(BallotDescription);
                    break;
                case nameof(InvoiceDescription):
                    MgrServices.SetPrinterSettings("InvoicePrinterDescription", InvoiceDescription);
                    SetDefaultPrinterMargins(InvoiceDescription);
                    break;
                case nameof(DrawerDescription):
                    MgrServices.SetPrinterSettings("DrawerPrinterDescription", DrawerDescription);
                    break;
                case nameof(HasDrawer):
                    MgrServices.SetPrinterSettings("HasDrawer", HasDrawer.ToString().ToLower());
                    break;
                case nameof(EscCode):
                    MgrServices.SetPrinterSettings("EscCode", EscCode);
                    break;
                case nameof(HasSerialPort):
                    MgrServices.SetPrinterSettings("HasSerialPort", HasSerialPort.ToString().ToLower());
                    break;
                case nameof(SerialPortDescription):
                    MgrServices.SetPrinterSettings("SerialPortDescription", SerialPortDescription);
                    break;
                case nameof(SerialPortBaudRate):
                    MgrServices.SetPrinterSettings("SerialPortBaudRate", SerialPortBaudRate.ToString());
                    break;
                case nameof(SerialPortParity):
                    MgrServices.SetPrinterSettings("SerialPortParity", SerialPortParity.ToString());
                    break;
                case nameof(SerialPortDataBit):
                    MgrServices.SetPrinterSettings("SerialPortDataBit", SerialPortDataBit.ToString());
                    break;
                case nameof(SerialPortStopBits):
                    MgrServices.SetPrinterSettings("SerialPortStopBits", SerialPortStopBits.ToString());
                    break;
                case nameof(FiscalPrinterServiceAddress):
                    SetAddressEndPoint(MgrServices.Settings.FiscalPrinterEndpointName, FiscalPrinterServiceAddress);
                    break;
            }
        }

        private void SetDefaultPrinterMargins(string printer)
        {
            if (!string.IsNullOrEmpty(printer))
            {
                MgrServices.SetPrinterSettings($"{printer}_LeftMargin", "0", true);
                MgrServices.SetPrinterSettings($"{printer}_RightMargin", "0", true);
                MgrServices.SetPrinterSettings($"{printer}_TopMargin", "0", true);
                MgrServices.SetPrinterSettings($"{printer}_BottomMargin", "0", true);
            }
        }

        private string GetAddressEndPoint(string endPointName)
        {
            var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var wServiceSection = ServiceModelSectionGroup.GetSectionGroup(configuration);
            if (wServiceSection != null)
            {
                var wClientSection = wServiceSection.Client;
                var address = wClientSection.Endpoints.Cast<ChannelEndpointElement>()
                    .FirstOrDefault(x => x.Name == endPointName)?.Address;
                
                return address?.AbsoluteUri;
            }

            return null;
        }

        private void SetAddressEndPoint(string endPointName, string address)
        {
            var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var wServiceSection = ServiceModelSectionGroup.GetSectionGroup(configuration);
            
            var wClientSection = wServiceSection.Client;
            var endPoint = wClientSection.Endpoints.Cast<ChannelEndpointElement>()
                .FirstOrDefault(x => x.Name == endPointName);
            if (endPoint != null)
            {
                endPoint.Address = new Uri(address);
                configuration.Save();

                ConfigurationManager.RefreshSection("system.serviceModel");
            }
        }

        #endregion
    }
}