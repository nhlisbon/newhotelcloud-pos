﻿using NewHotel.WPF.MVVM;
using System;

namespace NewHotel.WPF.App.Pos.Model
{
	public class CreditCard : BindableBase
	{
		private Guid? creditCardTypeIdProperty;
		private string creditCardTypeDescProperty;
		private short? validationMonthProperty;
		private short? validationYearProperty;
		private string validationCodeProperty;
		private string identNumberProperty;
        private string transactionNumberProperty;
        private string authorizationCodeProperty;

		public CreditCard(string description, Guid? creditCardTypeId, string creditCardTypeDesc, short? validationMonth, short? validationYear, string validationCode, string transactionNumber, string authorizationCode)
        {
            // Esto es super prvisional
            if (string.IsNullOrEmpty(IdentNumber))
                IdentNumber = "0000000000000000";
            CreditCardTypeId = creditCardTypeId;
            CreditCardTypeDesc = creditCardTypeDesc;
            ValidationMonth = validationMonth;
            ValidationYear = validationYear;
            ValidationCode = validationCode;
            TransactionNumber = transactionNumber;
            AuthorizationCode = authorizationCode;
        }

        public CreditCard()
        {
        }

        public Guid Id { get; set; }

        public Guid? CreditCardTypeId
		{
			get { return creditCardTypeIdProperty; }
			set { SetProperty(ref creditCardTypeIdProperty, value); }
		}

		public string CreditCardTypeDesc
		{
			get { return creditCardTypeDescProperty; }
			set { SetProperty(ref creditCardTypeDescProperty, value); }
		}

		public short? ValidationMonth
		{
			get { return validationMonthProperty; }
			set { SetProperty(ref validationMonthProperty, value); }
		}

		public short? ValidationYear
		{
			get { return validationYearProperty; }
			set { SetProperty(ref validationYearProperty, value); }
		}

		public string ValidationCode
		{
			get { return validationCodeProperty; }
			set { SetProperty(ref validationCodeProperty, value); }
		}

		public string IdentNumber
		{
			get { return identNumberProperty; }
			set { SetProperty(ref identNumberProperty, value); }
		}

        public string TransactionNumber
        {
            get { return transactionNumberProperty; }
            set { SetProperty(ref transactionNumberProperty, value); }
        }

        public string AuthorizationCode
        {
            get { return authorizationCodeProperty; }
            set { SetProperty(ref authorizationCodeProperty, value); }
        }

		public override string ToString()
        {
            return IdentNumber;
        }
    }
}
