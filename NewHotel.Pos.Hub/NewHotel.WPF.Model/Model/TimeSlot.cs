﻿using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.WPF.Model.Model
{
    public class TimeSlot
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public Guid StandId { get; set; }
        public string StartHours { get; set; }
        public string StartMinutes { get; set; }
        public int? PaxsMaxPercent { get; set; }
        public int ReservationDurationEstimated { get; set; }
        public int? PaxsMaxNumber { get; set; }

        public TimeSlot(StandTimeSlotRecord standTimeSlotRecord)
        {
            Id = (Guid)standTimeSlotRecord.Id;
            Description = standTimeSlotRecord.Description;
            StandId = standTimeSlotRecord.StandId;
            StartHours = standTimeSlotRecord.StartHours;
            StartMinutes = standTimeSlotRecord.StartMinutes;
            if (standTimeSlotRecord.PaxsMaxPercent.HasValue) 
                PaxsMaxPercent = standTimeSlotRecord.PaxsMaxPercent;
            ReservationDurationEstimated = standTimeSlotRecord.ReservationDurationEstimated;
            if (standTimeSlotRecord.PaxsMaxNumber.HasValue)
                PaxsMaxNumber = standTimeSlotRecord.PaxsMaxNumber;
        }
    }
}
