﻿using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.App.Pos.Model
{
	public class AutoSplitModel : BindableBase
	{
        public int Quantity
        {
            get { return QuantityProperty; }
            set { SetProperty(ref QuantityProperty, value); }
        }

        private int QuantityProperty;
    }
}
