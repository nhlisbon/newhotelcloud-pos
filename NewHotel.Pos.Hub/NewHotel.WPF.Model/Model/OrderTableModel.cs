﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using NewHotel.Contracts;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.Model.Model
{
    public class OrderTable: BindableBase
    {
        private TableLineContract _tableLine;
        private ObservableCollection<Preparation> _preparations;
        private string _observations;
        private bool _isSelected;

        public OrderTable()
        {
        }
        
        public OrderTable(TableLineContract tableLine, TableProductModel product)
        {
            DataContext = tableLine ?? new TableLineContract();
            Initialize(tableLine, product);
        }

        #region Events
        public event Action<OrderTable, bool> IsSelectedChanged;

        #endregion
        /// <summary>
        /// Show if order was selected
        /// </summary>
        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(ref _isSelected, value);
        }

        /// <summary>
        /// Product related with the order
        /// </summary>
        public TableProductModel Product { get; private set; }

        #region Read only properites
        public TableLineContract TableLine => _tableLine;

        public decimal Quantity => _tableLine.Quantity;

        public string Description => Product?.ProductDescription ?? _tableLine.ProductDescription;

        public Guid Id => (Guid)_tableLine.Id;

        public bool NotMarch => SendToAreaStatus == 0;

        public bool March => SendToAreaStatus == 1;

        public bool MarchProcessing => SendToAreaStatus == 2;

        public bool MarchCompleted => SendToAreaStatus == 3;

        public bool MarchDispatched => SendToAreaStatus == 4;

        public bool MarchAway => SendToAreaStatus == 5;

        public string AllPreparations => string.Join(", ", _tableLine.TableLinePreparations.Select(p => p.PreparationDescription));

        public bool HasObservations => !string.IsNullOrEmpty(_tableLine.Observations);

        public bool HasPreparations => !_tableLine.TableLinePreparations.IsEmpty;

        public short? SeparatorOrder => _tableLine.SeparatorOrden;

        public string SeparatorDescription => _tableLine.SeparatorDescription ?? Product.SeparatorDescription;

        #endregion

        #region Modificables properies

        public string Observations
        {
            get => _observations;
            set => SetProperty(ref _observations, value);
        }
        
        public ObservableCollection<Preparation> Preparations
        {
            get => _preparations;
            private set => SetProperty(ref _preparations, value);
        }

        public TypedList<POSProductLineAreaContract> Areas
        {
            get => _tableLine.Areas;
            set
            {
                _tableLine.Areas = value;
                OnPropertyChanged();
            }
        }

        public Guid? AreaId
        {
            get => _tableLine.AreaId;
            set
            { _tableLine.AreaId = value; }
        }

        public short SendToAreaStatus
        {
            get => _tableLine.SendToAreaStatus;
            set
            {
                var refValue = _tableLine.SendToAreaStatus;
                if (SetProperty(ref refValue, value, () => _tableLine.SendToAreaStatus = refValue))
                {
                    OnPropertyChanged("NotMarch");
                    OnPropertyChanged("March");
                    OnPropertyChanged("MarchProcessing");
                    OnPropertyChanged("MarchCompleted");
                    OnPropertyChanged("MarchDispatched");
                    OnPropertyChanged("MarchAway");
                }
            }
        }

        #endregion

        public void DiscardChanges()
        {
            Initialize(_tableLine, Product);
        }

        public void ApplyChanges()
        {
            // Observations
            _tableLine.Observations = Observations;
            // Preparations
            if (Preparations == null) return;
            if (_tableLine.TableLinePreparations == null)
                _tableLine.TableLinePreparations = new TypedList<TableLinePreparationContract>();
            else
                _tableLine.TableLinePreparations.Clear();
                
            foreach (var prep in Preparations)
                _tableLine.TableLinePreparations.Add(new TableLinePreparationContract(prep.RelationId, prep.Id, (Guid)_tableLine.Id, prep.Description, _tableLine.ProductDescription));
        }

        private void Initialize(TableLineContract tableLine, TableProductModel product)
        {
            _tableLine = tableLine;
            Product = product;

            _observations = _tableLine.Observations;

            if (tableLine.TableLinePreparations == null)
                Preparations = new ObservableCollection<Preparation>();
            else
            {
                Preparations = new ObservableCollection<Preparation>(tableLine.TableLinePreparations.Select(s => new Preparation()
                {
                    Id = s.PreparationId,
                    Description = s.PreparationDescription,
                    RelationId = (Guid)s.Id
                }));
            }
        }

        protected override void OnPropertyChanged(string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            switch (propertyName)
            {
                case nameof(IsSelected):
                    IsSelectedChanged?.Invoke(this, IsSelected);
                    break;
            }
        }
    }
}
