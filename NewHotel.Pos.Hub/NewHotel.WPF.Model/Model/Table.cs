﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using NewHotel.Contracts;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Localization;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.Model.Model;

public class Table : BindableBase
{
	#region Enums

	public enum States { Ready, Busy }

	#endregion
	#region Variable

	private readonly MesaContract _tableContract = new();
	private readonly ObservableCollection<LiteTablesReservation> _reservations = new();
	private readonly ObservableCollection<Ticket> _bills = new();
	private States _state;
	private string _type;
	private string _saloon;
	private int _priority;
	private decimal _totalAmount;
	private decimal _visualTotal;
	private bool _externalUse;
	private bool _isReserved;
	private DateTime? _reservationTime;
	private TablesGroup _tablesGroup;
	private bool _showTablesGroupBorder;
	private bool _mixUse;
	private bool _isSelected;
	private bool _isExpanded;
	private LiteTablesReservation _lastDeletedReservation;
	private bool _showReservations;
	private object _freeImage;
	private object _busyImage;
	private string _defaultFreeImageId;
	private string _defaultBusyImageId;
	private short _paxSeated;
	private string _room;
	private string _prePaidCode;

	#endregion
	#region Constructors

	public Table(Guid id)
	{
		Id = id;
		if (!Row.HasValue)
			Row = 0;
		if (!Column.HasValue)
			Column = 0;
		if (!Width.HasValue)
			Width = 2;
		if (!Height.HasValue)
			Height = 2;

		Hotel.Instance.AddTableToFastLocation(this);
		_bills.CollectionChanged += OnBillsCollectionChanged;
		tickets = new ObservableCollection<TicketSlim>();
	}

	#endregion
	#region Properties

	public object TableImage
	{
		get
		{
			if (IsBusy)
				return BusyImage ?? DefaultBusyImageId;

			return FreeImage ?? DefaultFreeImageId;
		}
	}

	public object FreeImage
	{
		get { return _freeImage; }
		set { SetProperty(ref _freeImage, value, nameof(FreeImage)); }
	}

	public object BusyImage
	{
		get { return _busyImage; }
		set { SetProperty(ref _busyImage, value, nameof(BusyImage)); }
	}

	public string Room
	{
		get => _room;
		set => SetProperty(ref _room, value, nameof(Room));
	}
	
	public string DefaultFreeImageId
	{
		get { return _defaultFreeImageId; }
		set { SetProperty(ref _defaultFreeImageId, value, nameof(DefaultFreeImageId)); }
	}

	public string DefaultBusyImageId
	{
		get { return _defaultBusyImageId; }
		set { SetProperty(ref _defaultBusyImageId, value, nameof(DefaultBusyImageId)); }
	}

	public Guid Id
	{
		get { return (Guid)_tableContract.Id; }
		set { _tableContract.Id = value; }
	}

	public short Paxs
	{
		get { return _tableContract.MaxPaxs; }
		set
		{
			var refValue = _tableContract.MaxPaxs;
			SetProperty(ref refValue, value, () => _tableContract.MaxPaxs = refValue);
		}
	}

	public short PaxSeated
	{
		get => _paxSeated;
		set
		{
			var refValue = _paxSeated;
			SetProperty(ref refValue, value, () => _paxSeated = refValue);
		}
	}

	public short? Row
	{
		get { return _tableContract.Row; }
		set
		{
			var refValue = _tableContract.Row;
			SetProperty(ref refValue, value, () => _tableContract.Row = refValue);
		}
	}

	public short? Column
	{
		get { return _tableContract.Column; }
		set
		{
			var refValue = _tableContract.Column;
			SetProperty(ref refValue, value, () => _tableContract.Column = refValue);
		}
	}

	public short? Width
	{
		get { return _tableContract.Width; }
		set
		{
			var refValue = _tableContract.Width;
			SetProperty(ref refValue, value, () => _tableContract.Width = refValue);
		}
	}

	public short? Height
	{
		get { return _tableContract.Height; }
		set
		{
			var refValue = _tableContract.Height;
			SetProperty(ref refValue, value, () => _tableContract.Height = refValue);
		}
	}

	public States State
	{
		get { return _state; }
		set { SetProperty(ref _state, value, nameof(State)); }
	}

	public string TypeDesc
	{
		get { return _type; }
		set { SetProperty(ref _type, value); }
	}

	public EPosMesaType Type
	{
		get { return _tableContract.Type; }
		set
		{
			var refValue = _tableContract.Type;
			SetProperty(ref refValue, value, () => _tableContract.Type = refValue);
		}
	}

	public string Saloon
	{
		get { return _saloon; }
		set { SetProperty(ref _saloon, value); }
	}

	public Guid SaloonId
	{
		get { return _tableContract.Saloon; }
		set
		{
			var refValue = _tableContract.Saloon;
			SetProperty(ref refValue, value, () => _tableContract.Saloon = refValue);
		}
	}

	public TablesGroup? TablesGroup
	{
		get => _tablesGroup;
		set => SetProperty(ref _tablesGroup, value);
	}

	public bool ShowTablesGroupBorder
	{
		get => _showTablesGroupBorder;
		set => SetProperty(ref _showTablesGroupBorder, value);
	}

	public bool IsAvailable
	{
		get { return _tableContract.IsAvailable; }
		set
		{
			var refValue = _tableContract.IsAvailable;
			SetProperty(ref refValue, value, () => _tableContract.IsAvailable = refValue);
		}
	}

	public DateTime? ReservationTime
	{
		get { return _reservationTime; }
		set { SetProperty(ref _reservationTime, value); }
	}

	public bool IsReserved
	{
		get { return _isReserved; }
		protected set { SetProperty(ref _isReserved, value); }
	}

	public int Priority
	{
		get { return _priority; }
		set { SetProperty(ref _priority, value); }
	}

	public string Name
	{
		get { return _tableContract.Description; }
		set
		{
			var refValue = _tableContract.Description;
			SetProperty(ref refValue, value, () => _tableContract.Description = refValue);
		}
	}

	public decimal TotalAmount
	{
		get { return _totalAmount; }
		set { SetProperty(ref _totalAmount, value); }
	}

	public decimal VisualTotal
	{
		get { return _visualTotal; }
		set { SetProperty(ref _visualTotal, value); }
	}

	public bool IsBusy
	{
		// get { return HasBills || State == States.Busy; }
		get { return HasTickets || State == States.Busy; }
	}

	public int ReservationsCount
	{
		get { return _reservations.Count; }
	}

	public IEnumerable<LiteTablesReservation> Reservations
	{
		get { return _reservations; }
	}

	public bool ExternalUse
	{
		get { return _externalUse; }
		set { SetProperty(ref _externalUse, value); }
	}

	public bool MixUse
	{
		get { return _mixUse; }
		set { SetProperty(ref _mixUse, value); }
	}

	public bool HasTablesGroup => TablesGroup != null;

	public bool MainTable => !HasTablesGroup || TablesGroup.Count == 0 || TablesGroup.MainTable == this;

	[Obsolete("Do not use this collection, use Tickets instead")]
	public ObservableCollection<Ticket> Bills => _bills;

	public bool HasBills => _bills.Count > 0;

	public bool ContainsTicket(Guid ticketId) => _bills.Any(b => b.Id == ticketId);


	ObservableCollection<TicketSlim> tickets;
	public ObservableCollection<TicketSlim> Tickets
	{
		get => tickets;
		set => SetProperty(ref tickets, value);
	}

	public bool HasTickets => tickets?.Any() ?? false;

	public string PrePaidCode
	{
		get { return _prePaidCode; }
		set { SetProperty(ref _prePaidCode, value); }
	}

	#region Visual

	public bool ShowMarchCompleted
	{
		get
		{
			foreach (var bill in _bills)
			{
				// Any completed order
				if (bill.Orders.Any(o => o.SendToAreaStatus == 3))
					return true;
			}

			return false;
		}
	}

	public bool IsSelected
	{
		get { return _isSelected; }
		set { SetProperty(ref _isSelected, value); }
	}

	public bool IsExpanded
	{
		get { return _isExpanded; }
		set { SetProperty(ref _isExpanded, value); }
	}

	public bool ShowReservations
	{
		get { return _showReservations; }
		set { SetProperty(ref _showReservations, value); }
	}

	#endregion

	#endregion
	#region Methods

	public void RefreshSendToAreaStatus()
	{
		OnPropertyChanged(nameof(ShowMarchCompleted));
	}

	public void CleanGroupData()
	{
		TablesGroup?.Remove(this);
		TablesGroup = null;
		IsExpanded = false;
		ShowTablesGroupBorder = GetShowTablesGroupBorder();
		UpdateState();
	}

	public void AddReservation(LiteTablesReservation reservation)
	{
		if (_reservations.Any(r => r.Id == reservation.Id)) return;
		_reservations.Add(reservation);
		UpdateReservationTime();
		IsReserved = true;
	}

	public void AddReservationRange(IEnumerable<LiteTablesReservation> reservations)
	{
		foreach (var reservation in reservations)
			AddReservation(reservation);
	}

	public void RemoveReservation(Guid reservationId)
	{
		_lastDeletedReservation = _reservations.FirstOrDefault(r => r.Id == reservationId);
		if (_lastDeletedReservation != null && _reservations.Remove(_lastDeletedReservation))
		{
			UpdateReservationTime();
			if (_reservations.Count == 0)
				IsReserved = false;
		}
	}

	public void RollBackLastRemoveReservation()
	{
		AddReservation(_lastDeletedReservation);
	}

	public bool IsReservedBy(LiteTablesReservation reservation)
	{
		return _reservations.Contains(reservation);
	}

	public void RecalculateTotal()
	{
		VisualTotal = HasTickets ? tickets.Sum(t => t.TotalAmount) : 0;
		TotalAmount = HasBills ? _bills.Sum(b => b.TotalAmount) : VisualTotal;
	}

	public void AddTicket(Ticket ticket)
	{
		_bills.Add(ticket);
		UpsertSlimTicket(ticket);
	}

	public void ReplaceOrAddTicket(Ticket ticket)
	{
		var added = false;
		for (int i = 0; i < _bills.Count; i++)
		{
			if (_bills[i].Id == ticket.Id)
			{
				_bills[i] = ticket;
				UpsertSlimTicket(ticket);
				added = true;
				break;
			}
		}

		if (!added)
			AddTicket(ticket);
	}

	public void RemoveTicket(ITicket ticket, bool error = false)
	{
		RemoveSlimTicket(ticket.Id);
		if (error)
			throw new Exception("NotVoidEmptyTicket".Translate());
	}

	public void UpdateExternalUse()
	{
		ExternalUse = _bills.Any(b => !b.IsEditable);
		MixUse = ExternalUse && _bills.Any(b => b.IsEditable);
	}

	public bool Available(LiteTablesReservation reservation)
	{
		if (State != States.Ready || !reservation.TableGroupId.HasValue && reservation.TotalPaxs > Paxs)
			return false;
		if (IsReserved && !IsReservedBy(reservation))
			return ReservationTime > Hotel.Instance.CurrentStandCashierContext.Stand.StandWorkDate;

		return true;
	}

	public bool Available(short paxs)
	{
		return State == States.Ready && paxs <= Paxs;
	}

	public void PersistObject()
	{
		BusinessProxyRepository.Stand.UpdateMesaAsync(_tableContract).ContinueWith(t =>
		{
			if (t.IsFaulted)
				Trace.WriteLine(t.Exception.GetBaseException().Message);
		});
	}


	private void RemoveSlimTicket(Guid id)
	{
		tickets.Remove(tickets.FirstOrDefault(x => x.Id == id));
		_bills.Remove(_bills.FirstOrDefault(x => x.Id == id));
		UpdateTableStatus();
	}

	private void UpsertSlimTicket(Ticket ticket)
	{
		RemoveSlimTicket(ticket.Id);
		tickets.Add(new TicketSlim(ticket.AsContract()));
	}

	private void UpdateReservationTime()
	{
		if (ReservationsCount > 0)
			ReservationTime = _reservations.Min(r => r.InitialTime);
		else
			ReservationTime = new DateTime(0);
	}

	private void UpdateState()
	{
		// State = HasBills ? States.Busy : States.Ready;
		// State = (HasTickets || HasBills) ? States.Busy : States.Ready;
		State = HasTickets ? States.Busy : States.Ready;
	}

	private void UpdateTableStatus()
	{
		UpdateState();
		UpdateExternalUse();
		RecalculateTotal();
	}

	private bool GetShowTablesGroupBorder()
	{
		return HasTablesGroup && IsBusy;
	}

	#endregion
	#region Event Handlers

	private void Bill_TotalAmountChanged(ITicketInformation arg1, decimal arg2)
	{
		RecalculateTotal();
	}

	private void OnBillsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
	{
		switch (e.Action)
		{
			case NotifyCollectionChangedAction.Replace:
			case NotifyCollectionChangedAction.Add:
				if (e.NewItems != null)
				{
					foreach (var ticket in e.NewItems.Cast<Ticket>())
						ticket.TotalAmountChanged += Bill_TotalAmountChanged;
				}
				break;
			case NotifyCollectionChangedAction.Remove:
			case NotifyCollectionChangedAction.Reset:
				if (e.OldItems != null)
				{
					foreach (var ticket in e.OldItems.Cast<Ticket>())
						ticket.TotalAmountChanged -= Bill_TotalAmountChanged;
				}
				break;
		}

		UpdateTableStatus();

		OnPropertyChanged(nameof(HasBills));
		RefreshSendToAreaStatus();
	}

	protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
	{
		base.OnPropertyChanged(propertyName);

		switch (propertyName)
		{
			case nameof(TablesGroup):
				OnTablesGroupPropertyChanged();
				break;
			case nameof(State):
				base.OnPropertyChanged(nameof(IsBusy));
				base.OnPropertyChanged(nameof(TableImage));
				OnStatePropertyChanged();
				break;
			case nameof(IsBusy):
			case nameof(FreeImage):
			case nameof(BusyImage):
			case nameof(DefaultBusyImageId):
			case nameof(DefaultFreeImageId):
				base.OnPropertyChanged(nameof(TableImage));
				break;
			case nameof(Tickets):
			case nameof(Bills):
				UpdateTableStatus();
				break;
		}
	}

	private void OnStatePropertyChanged()
	{
		if (HasTablesGroup && MainTable)
		{
			foreach (var table in TablesGroup)
				table.State = State;
		}

		ShowTablesGroupBorder = GetShowTablesGroupBorder();
	}

	private void OnTablesGroupPropertyChanged()
	{
		if (HasTablesGroup)
		{
			if (!MainTable && TablesGroup.MainTable != null)
				State = TablesGroup.MainTable.State;
		}
		else
			UpdateState();

		ShowTablesGroupBorder = GetShowTablesGroupBorder();
	}

	#endregion
}