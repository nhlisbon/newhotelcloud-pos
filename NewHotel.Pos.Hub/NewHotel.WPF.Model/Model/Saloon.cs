﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using NewHotel.WPF.Model.Model;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.App.Pos.Model
{
    public class Saloon : BindableBase
    {
        #region Variable

        private string _description;
        private short? _width;
        private short? _height;
        private object _imageFree;
        private object _imageBusy;
        private short _ordenProperty;
        private object _backImage;
        private ObservableCollection<Table> _tables = new ObservableCollection<Table>();

        #endregion
        #region Constructors

        public Saloon(Guid saloonId, string description)
        {
            Id = saloonId;
            Description = description;
        }

        #endregion
        #region Properties

        public Guid Id { get; set; }

        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }

        public short? Width
        {
            get => _width ?? 30;
            set => SetProperty(ref _width, value);
        }

        public short? Height
        {
            get => _height ?? 30;
            set => SetProperty(ref _height, value);
        }

        public object ImageFree
        {
            get => _imageFree;
            set => SetProperty(ref _imageFree, value);
        }

        public object ImageBusy
        {
            get => _imageBusy;
            set => SetProperty(ref _imageBusy, value);
        }

        public short Orden
        {
            get => _ordenProperty;
            set => SetProperty(ref _ordenProperty, value);
        }

        public object BackImage
        {
            get => _backImage;
            set => SetProperty(ref _backImage, value);
        }

        public ObservableCollection<Table> Tables
        {
            get => _tables;
            set => SetProperty(ref _tables, value);
        }

        #endregion
        #region Methods

        public void AddTable(Table table)
        {
            _tables.Add(table);
        }

        public void AddTableRange(IEnumerable<Table> tables)
        {
            foreach (var table in tables)
                AddTable(table);
        }

        public Table FindTable(Guid tableId)
        {
            return _tables.FirstOrDefault(x => x.Id == tableId);
        }

        public Table FindTable(string description)
        {
            return _tables.FirstOrDefault(x => x.Name == description);
        }

        public Table? FindTableByTicketId(Guid ticketId)
        {
            return _tables.FirstOrDefault(x => x.ContainsTicket(ticketId));
        }

        #endregion
    }
}
