﻿using System.Windows.Media;

namespace NewHotel.WPF.App.Pos.Model
{
    public class LanguageView
    {
        public ImageSource FlagImage { get; set; }
        public string Description { get; set; }
        public string Culture { get; set; }
        public int Code { get; set; }

        public override string ToString()
        {
            return Description;
        }
    }
}
