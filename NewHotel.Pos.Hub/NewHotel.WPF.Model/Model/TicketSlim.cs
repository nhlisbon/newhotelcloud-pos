﻿using System;
using System.Linq;
using NewHotel.Contracts;
using NewHotel.Pos.Hub.Model;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.App.Pos.Model
{
	public class TicketSlim : BindableBase, ITicket
	{
        bool isEditable;

        TicketSlim()
        {
			isEditable = true;
		}

        public TicketSlim(POSTicketContract ticket)
            : this()
        {
            Id = (Guid)ticket.Id;
            DigitalMenu = ticket.DigitalMenu;
			ShowDescription = !string.IsNullOrEmpty(ticket.Name);
            Description = ticket.Name;
            Serie = ticket.Number;
            SeriePrex = ticket.Serie;
            Blocked = ticket.Opened;
            TableId = ticket.Mesa;
            IsAcceptedAsTransfer = ticket.IsAcceptedAsTransfer;
			TotalAmount = ticket.Total;
            Stand = ticket.Stand;
            IsClosed = ticket.ClosedDate.HasValue;
            Cashier = ticket.Caja;
            CloseDate = ticket.CloseDate;
            Shift = (int)ticket.Shift;
            IsPersisted = ticket.IsPersisted;
            HasOrders = ticket.ProductLineByTicket?.Any() ?? false;
		}

        public TicketSlim(TicketInfo ticket)
			: this()
		{
            Id = ticket.Id;
            DigitalMenu = ticket.DigitalMenu;
			ShowDescription = !string.IsNullOrEmpty(ticket.Name);
            Description = ticket.Name;
            Serie = ticket.Number;
			SeriePrex = ticket.Serie;
			Blocked = ticket.Opened;
            TableId = ticket.MesaId;
            IsAcceptedAsTransfer = ticket.IsAcceptedTransfer;
            TotalAmount = ticket.Total;
			Stand = ticket.Stand;
			IsClosed = ticket.ClosedDate.HasValue;
			Cashier = ticket.Caja;
            CloseDate = ticket.ClosedDate;
            Shift = (int)ticket.Shift;
			IsPersisted = ticket.IsPersisted;
            HasOrders = true;
		}

        public Guid Id { get; }
		public bool DigitalMenu { get; }
		public bool ShowDescription { get; }
        public string Description { get; }
        public long? Serie { get; }
		public string SeriePrex { get; }
		public bool Blocked { get; }
        public Guid? TableId { get; }
        public bool IsAcceptedAsTransfer { get; }
        public decimal TotalAmount { get; }
		public Guid Stand { get; }
		public bool IsClosed { get; }
        public Guid Cashier { get; }

		public bool IsEditable
        {
            get => isEditable;
            set => SetProperty(ref isEditable, value);
        }

        public bool IsFull => false;

		public DateTime? CloseDate { get; }

		public int Shift { get; }
		public bool IsPersisted { get; }
        public bool HasOrders { get; }
	}
}