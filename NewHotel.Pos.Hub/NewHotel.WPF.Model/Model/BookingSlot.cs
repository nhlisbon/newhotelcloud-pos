﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using NewHotel.WPF.Model.Model;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.App.Pos.Model
{
    public class BookingSlot : BindableBase
    {
        #region Variable
//comentario en booking slot
        private string _description;
        private ObservableCollection<Table> _tables = new ObservableCollection<Table>();

        #endregion
        #region Constructors

        public BookingSlot(Guid slotId, string description)
        {
            Id = slotId;
            Description = description;
        }

        #endregion
        #region Properties

        public Guid Id { get; set; }

        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }

        public ObservableCollection<Table> Tables
        {
            get => _tables;
            set => SetProperty(ref _tables, value);
        }
        #endregion
    }
}
