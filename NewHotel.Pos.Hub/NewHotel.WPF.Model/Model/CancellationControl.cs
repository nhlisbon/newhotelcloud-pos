﻿using NewHotel.Contracts;
using NewHotel.WPF.MVVM;
using System;

namespace NewHotel.WPF.App.Pos.Model
{
	public class CancellationControl : BindableBase
	{
        public CancellationControlContract cancellationContract = null;

        public CancellationControl(Guid? reason, string observation, DateTime datetime, DateTime regDate, Guid UtilId, string userLogin, string cancellationReasonDescription)
        {
            cancellationContract = new CancellationControlContract(datetime, regDate, UtilId, userLogin, cancellationReasonDescription) { CancellationReasonId = reason};
        }

        public Guid Id 
        {
            get { return (Guid)cancellationContract.Id; }
            set { cancellationContract.Id = value; }
        }

        public DateTime CancellationDate
        {
            get { return cancellationContract.CancellationDate; }
        }

        public Guid? CancellationReasonId
        {
            get { return cancellationContract.CancellationReasonId; }
			set
			{
				var refValue = cancellationContract.CancellationReasonId;
				SetProperty(ref refValue, value, () => cancellationContract.CancellationReasonId = refValue);
			}
		}

        public string Comment
        {
            get { return cancellationContract.Comment; }
			set
			{
				var refValue = cancellationContract.Comment;
				SetProperty(ref refValue, value, () => cancellationContract.Comment = refValue);
			}
		}

        public DateTime RegistrationDate
        {
            get { return cancellationContract.RegistrationDate; }
        }

        public DateTime RegistrationTime
        {
            get { return cancellationContract.RegistrationTime; }
        }

        public Guid UserId
        {
            get { return cancellationContract.UserId; }
        }

        public string UserLogin
        {
            get { return cancellationContract.UserLogin; }
        }

        public string CancellationReasonDescription
        {
            get { return cancellationContract.CancellationReasonDescription; }
        }
    }
}
