﻿using System;
using NewHotel.WPF.MVVM;
using NewHotel.Pos.Printer.Interfaces;

namespace NewHotel.WPF.App.Pos.Model
{
    public class Separator : BindableBase, IComparable, IPrinteableSeparator
    {
        #region Variables

        private bool _isChecked;

        public static readonly Separator EmptySeparator = new Separator(string.Empty, short.MinValue);

        #endregion
        #region Constructors

        public Separator()
        {
        }

        public Separator(string description, short impresionOrder, short paxMax, bool acumRound)
        {
            Description = description;
            ImpresionOrden = impresionOrder;
            PaxMax = paxMax;
            AcumRound = acumRound;
        }

        public Separator(string description, short impresionOrder)
            : this(description, impresionOrder, 0, false)
        {
        }

        #endregion
        #region Properties

        public Guid? Id { get; set; }
        public string Description { get; set; }

        public short ImpresionOrden { get; set; }

        public short PaxMax { get; set; }

        public bool AcumRound { get; set; }

        public bool IsChecked
        {
            get => _isChecked;
            set => SetProperty(ref _isChecked, value);
        }

        public override string ToString()
        {
            return Description;
        }

        #endregion
        #region Methods

        public int CompareTo(object other)
        {
            return ImpresionOrden.CompareTo((other as Separator).ImpresionOrden);
        }

        #endregion
    }
}