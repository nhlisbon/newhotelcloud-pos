﻿using NewHotel.WPF.MVVM;
using System;

namespace NewHotel.WPF.App.Pos.Model
{
	public class FamilyPreparation : BindableBase
	{

        #region Dependency Properties

        private Guid FamilyIdProperty;
        private Guid PreparationIdProperty;

        #endregion

        #region Constructors

        public FamilyPreparation()
        {
        }

        #endregion

        #region Properties

        public Guid FamilyId
        {
            get { return FamilyIdProperty; }
            set { SetProperty(ref FamilyIdProperty, value); }
        }

        public Guid PreparationId
        {
            get { return PreparationIdProperty; }
            set { SetProperty(ref PreparationIdProperty, value); }
        }

        #endregion

    }
}
