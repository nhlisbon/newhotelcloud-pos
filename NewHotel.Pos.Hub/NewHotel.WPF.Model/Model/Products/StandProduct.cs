﻿using NewHotel.WPF.MVVM;
using System;

namespace NewHotel.WPF.App.Pos.Model
{
	public class StandProduct : BindableBase
	{

        #region Dependency Properties

        private Guid ProductProperty;

        private Guid StandProperty;

        private long? FavoritesOrderProperty;

        #endregion

        #region Constructors

        public StandProduct()
        {
        }

        #endregion

        #region Properties

        public Guid Product
        {
            get { return ProductProperty; }
            set { SetProperty(ref ProductProperty, value); }
        }

        public Guid Stand
        {
            get { return StandProperty; }
            set { SetProperty(ref StandProperty, value); }
        }

        public long? FavoritesOrder
        {
            get { return FavoritesOrderProperty; }
            set { SetProperty(ref FavoritesOrderProperty, value); }
        }

        public DateTime LastModified { get; set; }
        #endregion

    }
}
