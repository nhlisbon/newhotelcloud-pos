﻿using NewHotel.Core;
using NewHotel.WPF.MVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media;

namespace NewHotel.WPF.App.Pos.Model
{
	public class Product : BindableBase
	{
        public event Action<Product> PriceChanged;
        private string DescriptionProperty;
        private string AbreProperty;
        private string ImagePathProperty;
		private string CodeProperty;
		private decimal? PriceProperty;
		private Tax TaxProperty;
		private long IndexProperty;
		private decimal StepProperty;
		private Color ColorProperty;

		private ObservableCollection<Area> AreasProperty;
		private ObservableCollection<Preparation> ProductPreparationsProperty;

		public Product()
        {
            Areas = new ObservableCollection<Area>();
			ProductPreparations = new ObservableCollection<Preparation>();
        }

		public Product(IEnumerable<Area> areas)
		{
			Areas = new ObservableCollection<Area>(areas);
			ProductPreparations = new ObservableCollection<Preparation>();
		}

		public Guid Id { get; set; }

        public string Description
        {
            get { return DescriptionProperty; }
            set { SetProperty(ref DescriptionProperty, value); }
        }

        public string Abre
        {
            get { return AbreProperty; }
            set { SetProperty(ref AbreProperty, value); }
        }

        public string ImagePath
        {
            get { return ImagePathProperty; }
            set { SetProperty(ref ImagePathProperty, value); }
        }

        public string Code
        {
            get { return CodeProperty; }
            set { SetProperty(ref CodeProperty, value); }
        }

        public decimal? Price
        {
            get { return PriceProperty; }
            set { SetProperty(ref PriceProperty, value); }
        }

        public Tax Tax
        {
            get { return TaxProperty; }
            set { SetProperty(ref TaxProperty, value); }
        }

        public Guid TaxRateId1 { get; set; }
        public Guid TaxRateId2 { get; set; }
        public Guid TaxRateId3 { get; set; }

        public string TaxRateDesc1 { get; set; }
        public string TaxRateDesc2 { get; set; }
        public string TaxRateDesc3 { get; set; }

        public decimal? Percent1 { get; set; }
        public decimal? Percent2 { get; set; }
        public decimal? Percent3 { get; set; }

        public long? Mode2 { get; set; }
        public long? Mode3 { get; set; }



        public long Index
        {
            get { return IndexProperty; }
            set { SetProperty(ref IndexProperty, value); }
        }

        public decimal Step
        {
            get { return StepProperty; }
            set { SetProperty(ref StepProperty, value); }
        }

        public long? ColorId { get; set; }

        public Color Color
        {
            get { return ColorProperty; }
            set { SetProperty(ref ColorProperty, value); }
        }

        public long PriceType
        {
            get { return PriceTypeProperty; }
            set { SetProperty(ref PriceTypeProperty, value); }
        }

		private long PriceTypeProperty;


        public Guid? Separator { get; set; }

        public Guid? Group { get; set; }

        public Guid? Family { get; set; }

        public Guid? Subfamily { get; set; }

        public string GroupDescription { get; set; }

        public string FamilyDescription { get; set; }

        public string SubfamilyDescription { get; set; }

        public long? FavoritesOrder { get; set; }

        public DateTime LastModified { get; set; }

        public ObservableCollection<Area> Areas
        {
            get { return AreasProperty; }
            private set { SetProperty(ref AreasProperty, value); }
        }

        public ObservableCollection<Preparation> ProductPreparations
        {
            get { return ProductPreparationsProperty; }
            private set { SetProperty(ref ProductPreparationsProperty, value); }
        }

        public async Task LoadPreparations()
        {
            var preparations = await POSComunication.Comunication.GetPreparationsByProductsAsync(
				Id.ToHex(), Hotel.Instance.SelectedLanguage.Code.ToString());

			ProductPreparations = (preparations == null) ? new ObservableCollection<Preparation>() :
				new ObservableCollection<Preparation>(preparations.Select(s => new Preparation
				{
					Id = s.Id.ToGuid(),
					Description = s.Description
				}));
        }

        public void LoadCurrentPrice(Action action, Guid? currentRate = null)
        {
            //_currentRate = currentRate;
            //LoadElementProperty("Price", true);
            //if (action != null)
            //    action;
        }

        public decimal PreviousPrice;

        public void ClearHappyHour()
        {
            if (IsHappyHour)
            {
                IsHappyHour = false;
                Price = PreviousPrice;
            }
        }

        public void SetHappyHour(decimal? price)
        {
            if (price.HasValue && Price != price)
            {
                IsHappyHour = true;
                PreviousPrice = Price ?? 0;
                Price = price.Value;
            }
        }

        public bool IsHappyHour
        {
            get { return IsHappyHourProperty; }
            set { SetProperty(ref IsHappyHourProperty, value); }
        }

        private bool IsHappyHourProperty;

        public decimal ManualPrice
        {
            get { return ManualPriceProperty; }
            set { SetProperty(ref ManualPriceProperty, value); }
        }

        private decimal ManualPriceProperty;

        public Product Clone()
        {
            return new Product()
			{
				CurrentRate = CurrentRate,
				Id = Id,
				Code = Code,
				Description = Description,
				ImagePath = ImagePath,
				Price = Price
			};
        }

        public DateTime DateTime
        {
            get
            {
                // lo obtenemos del stand
                return DateTime.Now;
            }
        }

        private Guid? _currentRate;
        public Guid CurrentRate
        {
            get
            {
                if (!_currentRate.HasValue)
                {
                    if (Hotel.Instance.CurrentStandCashierContext.Stand.StandardRate.HasValue)
                        return Hotel.Instance.CurrentStandCashierContext.Stand.StandardRate.Value;
                    else return Guid.Empty;
                }
                else return _currentRate.Value;
            }
            set
            {
                _currentRate = value;
            }
        }

        public Guid[] ProductsList
        {
            get
            {
                return new Guid[] { Id };
            }
        }

        #region Define taxes (Local)
        /// <summary>
        /// IMSE.TIVA_APL2 = Aplicación Impuesto 2 (Sobre Base, Sobre Base + Taxa1)
        /// </summary>
        public enum ApplyTax2 : long { OverBase = 91, OverBasePlusRate1 = 92 };

        /// <summary>
        /// IMSE.TIVA_APL3 = Aplicación Impuesto 3 (Sobre Base, Sobre Base + Taxa1, Sobre Base + Taxa2 , Sobre Base + Taxa1 + Taxa2)
        /// </summary>
        public enum ApplyTax3 : long { OverBase = 91, OverBasePlusRate1 = 92, OverBasePlusRate2 = 93, OverBasePlusRate1PlusRate2 = 94 };

        public Tax GetTaxLocal(decimal? amount = null, bool? included = null)
        {
            Guid schema = Hotel.Instance.DefaultTaxSchema.Value;

            if (schema == null) return null;
 
            included = included ?? Hotel.Instance.TaxIncluded;

            if (!amount.HasValue)
            {
                if (!Price.HasValue)
                    return null;
                amount = Price;
            }
            if (included.Value)
                return DeduceTax(schema, amount.Value);
            else return AddTax(schema, amount.Value);
        }

        public void DefineTaxLocal(decimal? amount = null)
        {
            Tax = GetTaxLocal(amount);
        }

        public Tax DeduceTax(Guid schema, decimal amount)
        {
            decimal value = amount;
            Tax tax = new Tax(schema, true, value);

            MovementTaxDetail tax1 = null;
            MovementTaxDetail tax2 = null;
            MovementTaxDetail tax3 = null;

            decimal factor1 = 0;
            decimal factor2 = 0;
            decimal factor3 = 0;

                // al menos 1 impuesto tiene que haber
            if (TaxRateId1 != null)
            {
                tax1 = new MovementTaxDetail(TaxRateId1, Percent1.Value, 0) { TaxRateDescription = TaxRateDesc1 };
                factor1 = tax1.Percent / 100;

                // segundo impuesto definido
                if (TaxRateId2 != null && TaxRateId2 != Guid.Empty)
                {
                    tax2 = new MovementTaxDetail(TaxRateId2, Percent2.Value, 0) { TaxRateDescription = TaxRateDesc2 };

                    if (Mode2.HasValue)
                    {
                        // modos de aplicación para el 2do impuesto
                        switch (Mode2.Value)
                        {
                            case (long)ApplyTax2.OverBase:
                                factor2 = tax2.Percent / 100;
                                break;
                            case (long)ApplyTax2.OverBasePlusRate1:
                                factor2 = (1 + tax1.Percent / 100) * (tax2.Percent / 100);
                                break;
                        }
                    }
                    // tercer impuesto definido
                    if (TaxRateId3 != null && TaxRateId2 != Guid.Empty)
                    {
                        tax3 = new MovementTaxDetail(TaxRateId3, Percent3.Value, 0) { TaxRateDescription = TaxRateDesc3 };

                        if (Mode3.HasValue)
                        {
                            switch (Mode3.Value)
                            {
                                case (int)ApplyTax3.OverBase:
                                    factor3 = (decimal)(tax3.Percent / 100);
                                    break;
                                case (int)ApplyTax3.OverBasePlusRate1:
                                    factor3 = (1 + tax1.Percent / 100) * (tax3.Percent / 100);
                                    break;
                                case (int)ApplyTax3.OverBasePlusRate2:
                                    factor3 = (1 + tax2.Percent / 100) * (tax3.Percent / 100);
                                    break;
                                case (int)ApplyTax3.OverBasePlusRate1PlusRate2:
                                    factor3 = (1 + tax1.Percent / 100 + tax2.Percent / 100) * (tax3.Percent / 100);
                                    break;
                            }
                        }
                    }
                }
            }

            tax.NetValue = (value / (1 + factor1 + factor2 + factor3));
            tax.GrossValue = value;
            if (tax1 != null)
            {
                tax1.TaxValue = tax.NetValue * factor1;
                tax.Taxes.Add(tax1);
            }
            if (tax2 != null)
            {
                tax2.TaxValue = tax.NetValue * factor2;
                tax2.TaxBase = tax.GrossValue - tax2.TaxValue;
                tax.Taxes.Add(tax2);
            }
            if (tax3 != null)
            {
                tax3.TaxValue = tax.NetValue * factor3;
                tax3.TaxBase = tax.GrossValue - tax3.TaxValue;
                tax.Taxes.Add(tax3);
            }
            if (tax1 != null)
                tax1.TaxBase = tax.GrossValue - tax1.TaxValue - (tax2 != null ? tax2.TaxValue : 0) - (tax3 != null ? tax3.TaxValue : 0);
            if (tax2 != null)
                tax2.TaxBase = tax.GrossValue - tax2.TaxValue - (tax3 != null ? tax3.TaxValue : 0);
            if (tax3 != null)
                tax3.TaxBase = tax.GrossValue - tax3.TaxValue;
            return tax;
        }

        public Tax AddTax(Guid schema, decimal amount)
        {
            decimal value = amount;

            Tax tax = new Tax(schema, false, value);
            MovementTaxDetail tax1 = null;
            MovementTaxDetail tax2 = null;
            MovementTaxDetail tax3 = null;

            // al menos 1 impuesto tiene que haber
            if (TaxRateId1 != null)
            {
                tax1 = new MovementTaxDetail(TaxRateId1, Percent1.Value, 0) { TaxRateDescription = TaxRateDesc1 };

                tax1.TaxBase = value;
                tax1.TaxValue = (value * (tax1.Percent / 100));
                tax.Taxes.Add(tax1);
                // segundo impuesto definido
                if (TaxRateId2 != null && TaxRateId2 != Guid.Empty)
                {
                    tax2 = new MovementTaxDetail(TaxRateId2, Percent2.Value, 0) { TaxRateDescription = TaxRateDesc2 };

                    if (Mode2.HasValue)
                    {
                        // modos de aplicación para el 2do impuesto
                        switch (Mode2.Value)
                        {
                            case (int)ApplyTax2.OverBase:
                                tax2.TaxBase = value;
                                break;
                            case (int)ApplyTax2.OverBasePlusRate1:
                                tax2.TaxBase = value + tax1.TaxValue;
                                break;
                        }
                        tax2.TaxValue = (tax2.TaxBase * (tax2.Percent / 100));
                        tax.Taxes.Add(tax2);
                    }

                    // tercer impuesto definido
                    if (TaxRateId3 != null && TaxRateId3 != Guid.Empty)
                    {
                        tax3 = new MovementTaxDetail(TaxRateId3, Percent3.Value, 0) { TaxRateDescription = TaxRateDesc3 };

                        if (Mode3.HasValue)
                        {
                            switch (Mode3.Value)
                            {
                                case (int)ApplyTax3.OverBase:
                                    tax3.TaxBase = value;
                                    break;
                                case (int)ApplyTax3.OverBasePlusRate1:
                                    tax3.TaxBase = value + tax1.TaxValue;
                                    break;
                                case (int)ApplyTax3.OverBasePlusRate2:
                                    tax3.TaxBase = value + tax2.TaxValue;
                                    break;
                                case (int)ApplyTax3.OverBasePlusRate1PlusRate2:
                                    tax3.TaxBase = value + tax1.TaxValue + tax2.TaxValue;
                                    break;
                            }
                            tax3.TaxValue = (tax3.TaxBase * (tax3.Percent / 100));
                            tax.Taxes.Add(tax3);
                        }
                    }
                }
            }
            tax.NetValue = value;
            tax.GrossValue = value + tax.TotalTaxes;

            return tax;
        }

		#endregion

		protected override void OnPropertyChanged(string propertyName)
		{
			base.OnPropertyChanged(propertyName);

			switch (propertyName)
			{
				case "Price":
					if (PriceChanged != null)
						PriceChanged(this);
					break;
			}
		}
    }
}
