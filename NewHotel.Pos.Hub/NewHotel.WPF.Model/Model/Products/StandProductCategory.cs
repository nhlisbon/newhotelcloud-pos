﻿using NewHotel.WPF.MVVM;
using System;

namespace NewHotel.WPF.App.Pos.Model
{
	public class StandProductCategory : BindableBase
	{

        #region Dependency Properties

        private Guid ProductProperty;
        private Guid StandProperty;
        private Guid CategoryProperty;
        private int OrderProperty;
        private Guid? NextProperty;

        #endregion

        #region Properties

        public Guid Product
        {
            get { return ProductProperty; }
            set { SetProperty(ref ProductProperty, value); }
        }

        public Guid Stand
        {
            get { return StandProperty; }
            set { SetProperty(ref StandProperty, value); }
        }

        public Guid Category
        {
            get { return CategoryProperty; }
            set { SetProperty(ref CategoryProperty, value); }
        }

        public int Order
        {
            get { return OrderProperty; }
            set { SetProperty(ref OrderProperty, value); }
        }

        public Guid? Next
        {
            get { return NextProperty; }
            set { SetProperty(ref NextProperty, value); }
        }

        
        public DateTime LastModified { get; set; }
        #endregion

    }
}
