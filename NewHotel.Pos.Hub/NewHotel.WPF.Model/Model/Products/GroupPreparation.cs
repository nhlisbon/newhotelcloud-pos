﻿using NewHotel.WPF.MVVM;
using System;

namespace NewHotel.WPF.App.Pos.Model
{
	public class GroupPreparation : BindableBase
	{

        #region Dependency Properties

        private Guid GroupIdProperty;

        private Guid PreparationIdProperty;

        #endregion

        #region Constructors

        public GroupPreparation()
        {
        }

        #endregion

        #region Properties

        public Guid GroupId
        {
            get { return GroupIdProperty; }
            set { SetProperty(ref GroupIdProperty, value); }
        }

        public Guid PreparationId
        {
            get { return PreparationIdProperty; }
            set { SetProperty(ref PreparationIdProperty, value); }
        }

        #endregion

    }
}
