﻿using NewHotel.WPF.MVVM;
using System;

namespace NewHotel.WPF.App.Pos.Model
{
	public class ProductPreparation : BindableBase
	{

        #region Dependency Properties

        private Guid ProductIdProperty;

        private Guid PreparationIdProperty;

        #endregion

        #region Constructors

        public ProductPreparation()
        {
        }

        #endregion

        #region Properties

        public Guid ProductId
        {
            get { return ProductIdProperty; }
            set { SetProperty(ref ProductIdProperty, value); }
        }

        public Guid PreparationId
        {
            get { return PreparationIdProperty; }
            set { SetProperty(ref PreparationIdProperty, value); }
        }

        #endregion

    }
}
