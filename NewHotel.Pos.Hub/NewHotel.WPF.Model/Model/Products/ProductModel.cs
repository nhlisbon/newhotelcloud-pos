﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Hub.Contracts.Common.Records.Products;
using NewHotel.WPF.MVVM;
using ProductRecord = NewHotel.Pos.Hub.Contracts.Common.Records.Products.ProductRecord;

namespace NewHotel.WPF.App.Pos.Model
{
    public class Product : BindableBase
    {
        #region Variables

        private Guid? _currentRate;
        private long _index;
        private bool _isHappyHour;
        private decimal? _manualPrice;
        private string _manualPriceDesc;
        private ObservableCollection<Area> _productAreas;
        private ObservableCollection<Preparation> _productPreparations;
        private ObservableCollection<TableProductModel> _productTables;
        private ProductRecord _productRecord;
        private decimal _previousPrice;
        public bool hasImage => ImagePath != null;
        public bool doesntHaveImage => !hasImage;
        #endregion
        
        #region Constructors

        public Product()
        {
            Areas = new ObservableCollection<Area>();
            ProductTables = new ObservableCollection<TableProductModel>();
            ProductPreparations = new ObservableCollection<Preparation>();
            _productRecord = new ProductRecord();
        }

        public Product(ProductRecord productRecord, IEnumerable<Area> areas, IEnumerable<Preparation> preparations)
        {
            Areas = new ObservableCollection<Area>(areas);
            ProductPreparations = new ObservableCollection<Preparation>(preparations);
            ProductTables = new ObservableCollection<TableProductModel>();
            DataContext = productRecord;
        }

        public Product(Guid taxSchemaId, POSProductLineContract line) : this()
        {
            _productRecord.Id = line.Product;
            _productRecord.Abbreviation = line.ProductAbbreviation;
            _productRecord.StandardPrice = line.CostValue;
            _productRecord.ProductCode = line.ProductCode;
            _productRecord.Description = line.ProductDescription;
            _productRecord.Image = line.ProductImage;
            _productRecord.Color = line.ProductColor;

            #region Initialize taxs definition

            var taxsDefinition = new TaxSchemaProductRecord { TaxSchemaId = taxSchemaId };
            taxsDefinition.TaxRates.Add(new TaxRateProductRecord
            {
                TaxRateId = line.FirstIvaId,
                TaxRateDesc = line.FirstIvaDescription,
                Percent = line.FirstIvaPercent,
                TaxRateAuxCode = line.FirstIvaAuxCode
            });

            if (line.SecondIvaId.HasValue)
            {
                taxsDefinition.TaxRates.Add(new TaxRateProductRecord
                {
                    TaxRateId = line.SecondIvaId.Value,
                    TaxRateDesc = line.SecondIvaDescription,
                    Percent = line.SecondIvaPercent ?? 0,
                    TaxRateAuxCode = line.SecondIvaAuxCode,
                    Mode2 = null
                });
            }

            if (line.ThirdIvaId.HasValue)
            {
                taxsDefinition.TaxRates.Add(new TaxRateProductRecord
                {
                    TaxRateId = line.ThirdIvaId.Value,
                    TaxRateDesc = line.ThirdIvaDescription,
                    Percent = line.ThirdIvaPercent ?? 0,
                    TaxRateAuxCode = line.ThirdIvaAuxCode,
                    Mode3 = null
                });
            }

            _productRecord.TaxSchemas.Add(taxsDefinition);

            #endregion
        }

        #endregion

        #region Properties

        public override object DataContext
        {
            get => _productRecord;
            set
            {
                _productRecord = (ProductRecord)value;
                ProductTables = new ObservableCollection<TableProductModel>(_productRecord.TableProducts.Select(tp => new TableProductModel(tp)));

                RaiseAllPropertyChanged();
            }
        }

        public Guid Id => _productRecord.Id;

        public string Description
        {
            get => _productRecord.Description;
            set
            {
                var refValue = _productRecord.Description;
                SetProperty(ref refValue, value, () => _productRecord.Description = refValue);
            }
        }

        public string Abre
        {
            get => _productRecord.Abbreviation;
            set
            {
                var refValue = _productRecord.Abbreviation;
                SetProperty(ref refValue, value, () => _productRecord.Abbreviation = refValue);
            }
        }

        public TableType? ProductTableType
        {
            get => _productRecord.ProductTableType;
            set
            {
                var refValue = _productRecord.ProductTableType;
                SetProperty(ref refValue, value, () => _productRecord.ProductTableType = refValue);
            }
        }

        public string ImagePath
        {
            get => _productRecord.Image;
            set
            {
                var refValue = _productRecord.Image;
                SetProperty(ref refValue, value, () => _productRecord.Image = refValue);
            }
        }

        public string Code
        {
            get => _productRecord.ProductCode;
            set
            {
                var refValue = _productRecord.ProductCode;
                SetProperty(ref refValue, value, () => _productRecord.ProductCode = refValue);
            }
        }

        public Color Color
        {
            get
            {
                if (_productRecord.Color.HasValue)
                    return Color.FromRgb(_productRecord.Color.Value.R, _productRecord.Color.Value.G, _productRecord.Color.Value.B);

                return Colors.Black;
            }
            set
            {
                var refValue = _productRecord.Color.HasValue ? Color.FromRgb(_productRecord.Color.Value.R, _productRecord.Color.Value.G, _productRecord.Color.Value.B) : Colors.Black;
                SetProperty(ref refValue, value, () => _productRecord.Color = new ARGBColor(refValue.A, refValue.R, refValue.G, refValue.B, ""));
            }
        }

        public long PriceType
        {
            get => (long)_productRecord.PriceType;
            set
            {
                var refValue = (long)_productRecord.PriceType;
                SetProperty(ref refValue, value, () => _productRecord.PriceType = (ProductPriceType)refValue);
            }
        }

        public Guid? Separator
        {
            get
            {
                return _productRecord.SeparatorId;
            }
            set
            {
                var refValue = _productRecord.SeparatorId;
                SetProperty(ref refValue, value, () => _productRecord.SeparatorId = refValue);
            }
        }

        public Guid? Group
        {
            get
            {
                return _productRecord.GroupId;
            }
            set
            {
                var refValue = _productRecord.GroupId;
                SetProperty(ref refValue, value, () => _productRecord.GroupId = refValue);
            }
        }

        public Guid? Family
        {
            get
            {
                return _productRecord.FamilyId;
            }
            set
            {
                var refValue = _productRecord.FamilyId;
                SetProperty(ref refValue, value, () => _productRecord.FamilyId = refValue);
            }
        }

        public Guid? Subfamily
        {
            get
            {
                return _productRecord.SubFamilyId;
            }
            set
            {
                var refValue = _productRecord.SubFamilyId;
                SetProperty(ref refValue, value, () => _productRecord.SubFamilyId = refValue);
            }
        }

        public string GroupDescription
        {
            get
            {
                return _productRecord.GroupDescription;
            }
            set
            {
                var refValue = _productRecord.GroupDescription;
                SetProperty(ref refValue, value, () => _productRecord.GroupDescription = refValue);
            }
        }

        public string FamilyDescription
        {
            get
            {
                return _productRecord.FamilyDescription;
            }
            set
            {
                var refValue = _productRecord.FamilyDescription;
                SetProperty(ref refValue, value, () => _productRecord.FamilyDescription = refValue);
            }
        }

        public string SubfamilyDescription
        {
            get
            {
                return _productRecord.SubFamilyDescription;
            }
            set
            {
                var refValue = _productRecord.SubFamilyDescription;
                SetProperty(ref refValue, value, () => _productRecord.SubFamilyDescription = refValue);
            }
        }

        public long? FavoritesOrder
        {
            get
            {
                return _productRecord.OrderInFavorites;
            }
            set
            {
                var refValue = _productRecord.OrderInFavorites;
                SetProperty(ref refValue, value, () => _productRecord.OrderInFavorites = refValue);
            }
        }

        public string BarCode
        {
            get
            {
                return _productRecord.BarCode;
            }
            set
            {
                var refValue = _productRecord.BarCode;
                SetProperty(ref refValue, value, () => _productRecord.BarCode = refValue);
            }
        }

        public bool Inactive
        {
            get
            {
                return _productRecord.Inactive;
            }
            set
            {
                var refValue = _productRecord.Inactive;
                SetProperty(ref refValue, value, () => _productRecord.Inactive = refValue);
            }
        }

        public long Index
        {
            get { return _index; }
            set { SetProperty(ref _index, value); }
        }

        public DateTime LastModified { get; set; }

        public ObservableCollection<Area> Areas
        {
            get { return _productAreas; }
            private set { SetProperty(ref _productAreas, value); }
        }

        public ObservableCollection<Preparation> ProductPreparations
        {
            get => _productPreparations;
            private set => SetProperty(ref _productPreparations, value);
        }

        public ObservableCollection<TableProductModel> ProductTables
        {
            get => _productTables;
            private set => SetProperty(ref _productTables, value);
        }

        public bool HasAccompanyingProducts => _productRecord.AccompanyingProducts.Count > 0;
        public List<Guid> AccompanyingProducts => _productRecord.AccompanyingProducts;

        public bool IsLaunchFromSpaService
        {
            get
            {
                return _productRecord.IsLaunchFromSpaService;
            }
            set
            {
                var refValue = _productRecord.IsLaunchFromSpaService;
                SetProperty(ref refValue, value, () => _productRecord.IsLaunchFromSpaService = refValue);
            }
        }

        public bool IsHappyHour
        {
            get => _isHappyHour;
            set => SetProperty(ref _isHappyHour, value);
        }

        public decimal? ManualPrice
        {
            get => _manualPrice;
            set => SetProperty(ref _manualPrice, value);
        }
        
        public string ManualPriceDesc
        {
            get => _manualPriceDesc;
            set => SetProperty(ref _manualPriceDesc, value);
        }

        public DateTime DateTime => DateTime.Now;

        public Guid CurrentRate
        {
            get
            {
                if (!_currentRate.HasValue)
                {
                    var standardRate = Hotel.Instance.CurrentStandCashierContext.Stand.StandardRate;
                    if (standardRate.HasValue)
                        return standardRate.Value;
                    
                    return Guid.Empty;
                }
                else
                    return _currentRate.Value;
            }
            set { _currentRate = value; }
        }

        public Guid[] ProductsList => new Guid[] { Id };

        public ObservableCollection<Area> NotMandatoryAreas => new ObservableCollection<Area>(_productAreas.Where(a => !a.Mandatory));

        #region Price

        public decimal? StandardPrice
        {
            get { return _productRecord.StandardPrice; }
            set
            {
                var refValue = _productRecord.StandardPrice;
                SetProperty(ref refValue, value, () => _productRecord.StandardPrice = refValue);
            }
        }

        public decimal? HouseUsePrice
        {
            get { return _productRecord.HouseUsePrice; }
            set
            {
                var refValue = _productRecord.HouseUsePrice;
                SetProperty(ref refValue, value, () => _productRecord.HouseUsePrice = refValue);
            }
        }

        public decimal? MealPlanPrice
        {
            get { return _productRecord.MealPlanPrice; }
            set 
            {
                var refValue = _productRecord.MealPlanPrice;
                SetProperty(ref refValue, value, () => _productRecord.MealPlanPrice = refValue);
            }
        }

        public bool HasAccompanyingPrice => AccompanyingPrice.HasValue;
        public decimal? AccompanyingPrice => _productRecord.AccompanyingPrice;
        public string AccompanyingPriceDescription =>  AccompanyingPrice.HasValue ? string.Format(Hotel.Culture, "{0:C2}", AccompanyingPrice.Value) : "No defined";

        public decimal Step
        {
            get { return _productRecord.Step; }
            set
            {
                var refValue = _productRecord.Step;
                SetProperty(ref refValue, value, () => _productRecord.Step = refValue);
            }
        }

        public bool IsProductForSale => _productRecord.IsProduct;

        #endregion
        #region Tax rate to default tax schema

        public Guid TaxRateId1 => GetTaxDefinition(Hotel.Instance.DefaultTaxSchema.Value).TaxRate1.TaxRateId;
        public string TaxRateDesc1 => GetTaxDefinition(Hotel.Instance.DefaultTaxSchema.Value).TaxRate1.TaxRateDesc;
        public decimal Percent1 => GetTaxDefinition(Hotel.Instance.DefaultTaxSchema.Value).TaxRate1.Percent;
        public string TaxRateAuxCode1 => GetTaxDefinition(Hotel.Instance.DefaultTaxSchema.Value).TaxRate1.TaxRateAuxCode;
        public Guid? TaxRateId2 => GetTaxDefinition(Hotel.Instance.DefaultTaxSchema.Value).TaxRate2?.TaxRateId;
        public string TaxRateDesc2 => GetTaxDefinition(Hotel.Instance.DefaultTaxSchema.Value).TaxRate2?.TaxRateDesc;
        public decimal? Percent2 => GetTaxDefinition(Hotel.Instance.DefaultTaxSchema.Value).TaxRate2?.Percent;
        public long? Mode2 => (long?)GetTaxDefinition(Hotel.Instance.DefaultTaxSchema.Value).TaxRate2?.Mode2;
        public string TaxRateAuxCode2 => GetTaxDefinition(Hotel.Instance.DefaultTaxSchema.Value).TaxRate2?.TaxRateAuxCode;
        public Guid? TaxRateId3 => GetTaxDefinition(Hotel.Instance.DefaultTaxSchema.Value).TaxRate3?.TaxRateId;
        public string TaxRateDesc3 => GetTaxDefinition(Hotel.Instance.DefaultTaxSchema.Value).TaxRate3?.TaxRateDesc;
        public decimal? Percent3 => GetTaxDefinition(Hotel.Instance.DefaultTaxSchema.Value).TaxRate3?.Percent;
        public long? Mode3 => (long?)GetTaxDefinition(Hotel.Instance.DefaultTaxSchema.Value).TaxRate3?.Mode2;
        public string TaxRateAuxCode3 => GetTaxDefinition(Hotel.Instance.DefaultTaxSchema.Value).TaxRate3?.TaxRateAuxCode;

        #endregion
        
        #region Visual

        string GetPriceDescription()
            {
                if (PriceType == (long)ProductPriceType.AlwaysManual)
                    return "Manual";

                return !StandardPrice.HasValue ? "No defined" : string.Format(Hotel.Culture, "{0:C2}", StandardPrice.Value);
            }

        string _priceDescription;
		public string PriceDescription => _priceDescription ??= GetPriceDescription();

        public string AbbreviationAndPrice => Abre + '\n' + PriceDescription;
        
        private bool _selectedProperty;
        public bool Selected
        {
            get => _selectedProperty;
            set => SetProperty(ref _selectedProperty, value);
        }
        bool? _canBeSold;
        public bool CanBeSold => _canBeSold ??= (
            _productRecord.PriceType == ProductPriceType.AlwaysManual ||
            StandardPrice.HasValue ||
            HouseUsePrice.HasValue ||
            MealPlanPrice.HasValue ||
            AccompanyingPrice.HasValue)
            ;
        
        #endregion

        #endregion
        #region Methods

        public TaxSchemaProductRecord GetTaxDefinition(Guid taxSchemaId) => _productRecord.TaxSchemas.FirstOrDefault(x => x.TaxSchemaId == taxSchemaId);

        public async Task LoadPreparations()
        {
            var preparations = await BusinessProxyRepository.Product.GetPreparationsByProductsAsync(Id, Hotel.SelectedLanguage.Code);

            ProductPreparations = preparations == null ? new ObservableCollection<Preparation>() :
                new ObservableCollection<Preparation>(preparations.Select(s => new Preparation
                {
                    Id = s.Id,
                    Description = s.Description
                }));
        }

        public void ClearHappyHour()
        {
            if (!IsHappyHour) return;
            IsHappyHour = false;
            StandardPrice = _previousPrice;
            ManualPrice = null;
        }

        public void SetHappyHour(decimal? price)
        {
            if (!price.HasValue) return;
            IsHappyHour = true;
            _previousPrice = StandardPrice ?? decimal.Zero;
            StandardPrice = price.Value;
        }

        #region Define taxes (Local)

        internal TaxModel GetTaxLocal(Guid taxSchemaId, bool included, decimal? amount = null)
        {
            if (!amount.HasValue)
            {
                if (!StandardPrice.HasValue)
                    return null;

                amount = StandardPrice;
            }

            var taxSchema = GetTaxDefinition(taxSchemaId);
            if (taxSchema == null)
                return null;

            var tc = new TaxCalculation(Hotel.Instance.Country,
                                        taxSchema.TaxRate1?.TaxRateId, taxSchema.TaxRate1?.Percent,
                                        taxSchema.TaxRate2?.TaxRateId, taxSchema.TaxRate2?.Percent, taxSchema.TaxRate2?.Mode2,
                                        taxSchema.TaxRate3?.TaxRateId, taxSchema.TaxRate3?.Percent, taxSchema.TaxRate3?.Mode3)
            { 
                RoundMode = RoundMode.Close,
                Decimals = 8
            };

            if (included)
                tc.DeduceTax(amount.Value);
            else
                tc.AddTax(amount.Value);

            var result = new TaxModel(taxSchemaId, included, amount.Value)
            {
                GrossValue = tc.GrossValue,
                NetValue = tc.NetValue
            };

            if (tc.TaxRateId1.HasValue)
            {
                var tax1 = new MovementTaxDetail(tc.TaxRateId1.Value, tc.Percent1 ?? 0, 0)
                {
                    TaxValue = tc.TaxValue1 ?? 0,
                    TaxBase = tc.TaxBase1 ?? 0,
                    TaxIncidence = tc.TaxIncidence1 ?? 0,
                    TaxRateDescription = taxSchema.TaxRate1.TaxRateDesc,
                    TaxAuxCode = taxSchema.TaxRate1.TaxRateAuxCode
                };

                result.Taxes.Add(tax1);
            }

            if (tc.TaxRateId2.HasValue)
            {
                var tax2 = new MovementTaxDetail(tc.TaxRateId2.Value, tc.Percent2 ?? 0, 1)
                {
                    TaxValue = tc.TaxValue2 ?? 0,
                    TaxBase = tc.TaxBase2 ?? 0,
                    TaxIncidence = tc.TaxIncidence2 ?? 0,
                    TaxRateDescription = taxSchema.TaxRate2.TaxRateDesc,
                    TaxAuxCode = taxSchema.TaxRate2.TaxRateAuxCode,
                    Mode = (long?)taxSchema.TaxRate2.Mode2
                };

                result.Taxes.Add(tax2);
            }

            if (tc.TaxRateId3.HasValue)
            {
                var tax3 = new MovementTaxDetail(tc.TaxRateId3.Value, tc.Percent3 ?? 0, 2)
                {
                    TaxValue = tc.TaxValue3 ?? 0,
                    TaxBase = tc.TaxBase3 ?? 0,
                    TaxIncidence = tc.TaxIncidence3 ?? 0,
                    TaxRateDescription = taxSchema.TaxRate3.TaxRateDesc,
                    TaxAuxCode = taxSchema.TaxRate3.TaxRateAuxCode,
                    Mode = (long?)taxSchema.TaxRate3.Mode3
                };

                result.Taxes.Add(tax3);
            }

            return result;
        }

        #endregion

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);

            switch (propertyName)
            {
                case nameof(IsHappyHour):
                case nameof(PriceType):
                case nameof(StandardPrice):
                    OnPropertyChanged(nameof(PriceDescription));
                    break;
            }
        }

        public void SetTableProductSelectionOnRecord()
        {
            foreach (var model in ProductTables)
            {
                var record = _productRecord.TableProducts.First(x => x.Id == model.Id);
                record.Selected = model.Selected;
                model.Selected = false;
            }
        }

        public void SetAccompanyingProductsSelection(List<Product> products)
        {
            _productRecord.AccompanyingProductsSelected = products.Select(x => x.Id).ToList();
        }
        #endregion
        #region Operators

        public static implicit operator ProductRecord?(Product? source)
        {
            return source?.DataContext as ProductRecord;
        }

        #endregion
    }
}
