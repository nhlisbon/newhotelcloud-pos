﻿using NewHotel.WPF.MVVM;
using System;

namespace NewHotel.WPF.App.Pos.Model
{
	public class SubfamilyProduct : BindableBase
	{

        #region Dependency Properties

        private string DescriptionProperty;

        #endregion

        #region Constructors

        public SubfamilyProduct()
        {
        }

        #endregion

        #region Properties

        public Guid Id { get; set; }

        public string Description
        {
            get { return DescriptionProperty; }
            set { SetProperty(ref DescriptionProperty, value); }
        }

        #endregion

    }
}
