﻿using System;
using NewHotel.WPF.MVVM;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Printer.Interfaces;

namespace NewHotel.WPF.App.Pos.Model
{
    public class Preparation : BindableBase, IPrinteablePreparation
    {
        #region Variables

        private string _description;
        private bool _isChecked;

        #endregion
        #region Constructors

        public Preparation()
        {
            RelationId = Guid.NewGuid();
        }

        public Preparation(PreparationRecord record) : this()
        {
            DataContext = record;

            Id = record.Id;
            Description = record.Description;
        }

        public Preparation(Guid id, string description) : this(new PreparationRecord { Id = id, Description = description })
        {

        }

        #endregion
        #region Properties

        public Guid RelationId { get; set; }

        public Guid Id { get; set; }

        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }

        public bool IsChecked
        {
            get => _isChecked;
            set => SetProperty(ref _isChecked, value);
        }

        #endregion
        #region Methods

        public override string ToString()
        {
            return Description;
        }

        #endregion
    }
}
