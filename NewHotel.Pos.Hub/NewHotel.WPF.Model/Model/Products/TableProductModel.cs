﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using NewHotel.Pos.Hub.Contracts.Common.Records.Products;
using NewHotel.Pos.Hub.Model;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.App.Pos.Model
{
    public class TableProductModel : BindableBase
    {
        #region Private Members

        private ProductTableRecord _record;

        #endregion

        #region Constructor

        public TableProductModel(ProductTableRecord record)
        {
            _record = record;
        }

        #endregion

        #region DataContext

        public override object DataContext
        {
            get => _record;
            set
            {
                _record = value as ProductTableRecord;
                RaiseAllPropertyChanged();
            }
        }

        #endregion

        #region Properties

        public Guid Id => _record.Id;

        public Guid TableId => _record.TableId;

        public Guid ProductId => _record.ProductId;

        public string ProductAbbreviation => _record.ProductAbbreviation;

        public string ProductDescription => _record.ProductDescription;

        public decimal Quantity => _record.Quantity;

        public Guid? SeparatorId => _record.MenuSeparatorId;

        public short? SeparatorOrder => _record.MenuSeparatorOrder;

        public string SeparatorDescription => _record.MenuSeparatorDescription;

        public int? SeparatorMaxQuantitySelection => _record.MenuSeparatorSelection;

        public Preparation[] Preparations => _record.Preparations.Select(x => new Preparation(x)).ToArray();

        public Area[] Areas => _record.Areas.Select(x => new Area(x.Id, x.Description) { Mandatory = x.Mandatory }).ToArray();

        #endregion

        #region Visual Properties

        public string ImagePath
        {
            get
            {
                var product = Hotel.Instance.CurrentStandCashierContext.Stand.Products.FirstOrDefault(x => x.Id == ProductId);
                return product != null ? product.ImagePath : string.Empty;
            }
        }

        private bool _selectedProperty;

        public bool Selected
        {
            get => _selectedProperty;
            set => SetProperty(ref _selectedProperty, value);
        }
        
        public ObservableCollection<Area> NotMandatoryAreas => new ObservableCollection<Area>(Areas.Where(a => !a.Mandatory));

        #endregion
    }
}