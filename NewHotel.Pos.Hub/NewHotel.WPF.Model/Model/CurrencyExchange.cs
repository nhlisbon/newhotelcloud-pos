﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewHotel.Pos.Hub.Contracts.Cashier.Records;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common.Controls;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.Model.Model
{
    public class CurrencyExchange : BindableBase
    {
        #region Variables

        private DateTime _date;
        private bool _isSelected;
        private decimal _changeValue;
        private Guid _currencyCashierId;
        private string _currencyId;

		#endregion
		#region Contructors

		public CurrencyExchange(CurrencyCashier currencyCashier, decimal changeValue = 1M)
        {
			_currencyCashierId = currencyCashier.Id;
            ChangeValue = changeValue;
            _currencyId = currencyCashier.CurrencyId;
        }

        public CurrencyExchange(CurrencyExchangeRecord record, string currencyId)
        {
            Id = (Guid)record.Id;
            CurrencyCashierId = record.CurrencyCashierId;
            Date = record.Date;
            ChangeValue = record.ChangeValue;
            _currencyId = currencyId;
        }

        private CurrencyExchange(CurrencyExchange other)
		{
            Id = other.Id;
            CurrencyCashierId = other.CurrencyCashierId;
            Date = other.Date;
            ChangeValue = other.ChangeValue;
            _currencyId = other.CurrencyId;
		}

        public CurrencyExchange Clone() => new CurrencyExchange(this);

        #endregion
        #region Events

        public event EventHandler<NHPropertyChangedEventArgs> IsSelectedChanged;

        #endregion
        #region Properties

        public Guid Id { get; set; }
        public string CurrencyId
		{
			get => _currencyId;
			set => SetProperty(ref _currencyId, value);
		}

        public Guid CurrencyCashierId
        {
            get => _currencyCashierId;
            set => SetProperty(ref _currencyCashierId, value);
        }

        public CurrencyCashier CurrencyCashier =>
            Hotel.Instance.Currencys.FirstOrDefault(x => x.Id == CurrencyCashierId);

        public DateTime Date
        {
            get => _date;
            set => SetProperty(ref _date, value);
        }

        public decimal ChangeValue
        {
            get => _changeValue;
            set => SetProperty(ref _changeValue, value);
        }

        #region Visuals

        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                bool previous = IsSelected;
                if (SetProperty(ref _isSelected, value))
                {
                    var args = new NHPropertyChangedEventArgs(nameof(IsSelected))
                    {
                        NewValue = IsSelected,
                        OldValue = previous
                    };
                    IsSelectedChanged?.Invoke(this, args);
                }
            }
        }


        #endregion

        #endregion
    }
}
