﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewHotel.Pos.Hub.Contracts.Cashier.Records;
using NewHotel.WPF.Common.Controls;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.Model.Model
{
    public class CurrencyCashier : BindableBase
    {
        #region Variables

        private string _currencyId;
        private bool _mult;
        private string _symbol;
        private bool _isBase;

        #endregion
        #region Constructors

        public CurrencyCashier(CurrencyCashierRecord record)
        {
            Id = (Guid)record.Id;
            CurrencyId = record.CurrencyId;
            Mult = record.Mult;
            Symbol = record.Symbol;
            IsBase = record.IsBase;
            IsHotelBase = record.IsHotelBase;
        }

        #endregion
        #region Properties

        public Guid Id { get; set; }

        public string CurrencyId
        {
            get => _currencyId;
            set => SetProperty(ref _currencyId, value);
        }

        public bool Mult
        {
            get => _mult;
            set => SetProperty(ref _mult, value);
        }

        public string Symbol
        {
            get => _symbol;
            set => SetProperty(ref _symbol, value);
        }

        public bool IsBase
        {
            get => _isBase;
            set => SetProperty(ref _isBase, value);
        }

        public bool IsHotelBase { get; set; }

        #endregion
    }
}
