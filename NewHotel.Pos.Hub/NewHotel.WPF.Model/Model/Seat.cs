﻿using System;
using NewHotel.WPF.MVVM;
using NewHotel.Pos.Printer.Interfaces;

namespace NewHotel.WPF.App.Pos.Model
{
    public class Seat : BindableBase
    {
        #region Variables

        private bool _isChecked;

        #endregion
        
        #region Constructors

        public Seat()
        {
        }

        public Seat(int number, string description)
        {
            Number = number;
            Description = description;
        }

        #endregion
        
        #region Properties

        public int Number { get; set; }
        public string Description { get; set; }
        

        public bool IsChecked
        {
            get => _isChecked;
            set => SetProperty(ref _isChecked, value);
        }

        public override string ToString()
        {
            return Description;
        }

        #endregion
    }
}