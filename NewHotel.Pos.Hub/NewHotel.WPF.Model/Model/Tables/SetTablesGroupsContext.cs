﻿using NewHotel.WPF.MVVM;
using System.Collections.ObjectModel;
using System.Linq;
using NewHotel.WPF.Model.Model;


namespace NewHotel.WPF.App.Pos.Model
{
    public class SetTablesGroupsContext : BindableBase
    {
        private ObservableCollection<TablesGroup> _tablesGroupsProperty;
        private Saloon _saloonProperty;
        private ObservableCollection<Table> _tablesProperty;
        private TablesGroup _selectedTablesGroupProperty;
        private bool _isSelectedTablesGroupEditableProperty;
        private Actions _currentActionProperty;

        public enum Actions { Selecting, Creating, Removing };

        public SetTablesGroupsContext()
        {
            TablesGroups = new ObservableCollection<TablesGroup>();
            CurrentAction = Actions.Selecting;
        }

        public Actions CurrentAction
        {
            get { return _currentActionProperty; }
            set { SetProperty(ref _currentActionProperty, value); }
        }

        public Saloon Saloon
        {
            get { return _saloonProperty; }
            set { SetProperty(ref _saloonProperty, value); }
        }

        /// <summary>
        /// Available Tables
        /// </summary>
	    public ObservableCollection<Table> Tables
        {
            get { return _tablesProperty; }
            set { SetProperty(ref _tablesProperty, value); }
        }

        /// <summary>
        /// Available Tables Groups
        /// </summary>
        public ObservableCollection<TablesGroup> TablesGroups
        {
            get { return _tablesGroupsProperty; }
            set { SetProperty(ref _tablesGroupsProperty, value); }
        }

        public TablesGroup SelectedTablesGroup
        {
            get { return _selectedTablesGroupProperty; }
            set
            {
                IsSelectedTablesGroupEditable = value != null && value.Count > 1;
                SetProperty(ref _selectedTablesGroupProperty, value);
            }
        }

        public bool IsSelectedTablesGroupEditable
        {
            get { return _isSelectedTablesGroupEditableProperty; }
            set { SetProperty(ref _isSelectedTablesGroupEditableProperty, value); }
        }
    }
}
