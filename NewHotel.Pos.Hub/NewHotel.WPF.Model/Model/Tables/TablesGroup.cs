﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Media;
using NewHotel.WPF.Model.Model;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.App.Pos.Model
{
    public class TablesGroup : BindableBase, IEnumerable<Table>
    {
        #region Variables

        private Color _colorProperty;
        private short _paxsProperty;
        private bool _busyProperty;
        private bool _isSelectedProperty;
        private bool _isCheckedProperty;
        private bool? _areNear = null;
        private bool _inactiveProperty;
        private readonly ObservableCollection<Table> _tables = new ObservableCollection<Table>();
        private static readonly Dictionary<Guid, Color> GroupToColor = new Dictionary<Guid, Color>();
        private static readonly Random Random = new Random(Environment.TickCount);
        private Guid _id;
        #endregion
        #region Constructors

        public TablesGroup(Guid tablesGroupId)  
        {
            Color = new Color
            {
                A = 255,
                R = (byte)Random.Next(100, 255),
                G = (byte)Random.Next(100, 255),
                B = (byte)Random.Next(100, 255)
            };
            _tables.CollectionChanged += TablesOnCollectionChanged;

            Id = tablesGroupId;
            InitializeColor();
        }

        #endregion
        #region Properties

        public Guid Id
        {
            get => _id;
            set
            {
                Guid previous = Id;
                _id = value;
                if (previous != Id)
                    InitializeColor();
            }
        }

        public short MaxPaxs
        {
            get => _paxsProperty;
            set => SetProperty(ref _paxsProperty, value);
        }

        public Guid? SaloonId { get; set; }

        public Color Color
        {
            get => _colorProperty;
            set => SetProperty(ref _colorProperty, value);
        }

        public bool Busy
        {
            get => _busyProperty;
            set => SetProperty(ref _busyProperty, value);
        }

        public bool Inactive
        {
            get => _inactiveProperty;
            set => SetProperty(ref _inactiveProperty, value);
        }

        public int Count => _tables.Count;

        public bool IsSelected
        {
            get => _isSelectedProperty;
            set => SetProperty(ref _isSelectedProperty, value);
        }

        public bool AreTablesNear
        {
            get
            {
                if (!_areNear.HasValue)
                {
                    for (int i = 1; i < _tables.Count; i++)
                    {
                        if (!IsNear(i))
                        {
                            _areNear = false;
                            return AreTablesNear;
                        }
                    }
                    _areNear = true;
                    return AreTablesNear;
                }

                return _areNear.Value;
            }
        }

        public bool IsChecked
        {
            get { return _isCheckedProperty; }
            set { SetProperty(ref _isCheckedProperty, value); }
        }

        public string Description
        {
            get
            {
                return _tables.Select(x => (x as Table).Name).Aggregate((x, y) => x + " - " + y);
            }
        }

        public Table MainTable
        {
            get { return _tables.FirstOrDefault(t => t.HasBills); }
        }

        #endregion
        #region Methods
        /*
        public void NormalizeTables()
        {
            if (_tables != null && _tables.Count > 0)
            {
                // Obtener la MainTable
                Table mainTable = _tables[0] as Table;
                // Poner todos los tickets en ella (si existen)
                foreach (Table table in _tables.Where(s => s != mainTable))
                {
                    foreach (Ticket ticket in table.Bills)
                    {
                        Table prevTable = ticket.Table;
                        Hotel.Instance.MoveTicket(ticket, mainTable, null, false);
                    }
                }
            }
        }

        public void Clear()
        {
            _tables.Clear();
        }
        */
        public void Remove(Table table)
        {
            _tables.Remove(table);
        }

        public void AddTable(Table table)
        {
            _tables.Add(table);
            if (Guid.Empty == SaloonId)
                SaloonId = table.SaloonId;
        }

        /*
        public void AddTableRange(IEnumerable<Table> tables)
        {
            foreach (var table in tables)
                AddTable(table);
        }
        */

        public void AddAndLinkTable(Table table)
        {
            table.CleanGroupData();
            _tables.Add(table);
            table.TablesGroup = this;
            if (Guid.Empty == SaloonId)
                SaloonId = table.SaloonId;
        }

        public void AddAndLinkTableRange(IEnumerable<Table> tables)
        {
            foreach (var table in tables)
                AddAndLinkTable(table);
        }

        /*
        public void LinkTables()
        {
            foreach (var table in this)
                table.TablesGroup = this;
        }
        */

        public void UnlinkTables()
        {
            foreach (var table in this.ToArray())
                table.CleanGroupData();
        }

        private void TablesOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            UpdateMaxPaxs();
        }

        private void InitializeColor()
        {
            if (GroupToColor.ContainsKey(Id))
                Color = GroupToColor[Id];
            else GroupToColor[Id] = Color;
        }

        private bool IsNear(int index)
        {
            Table table1, minTable, maxTable;
            Table table2 = _tables[index] as Table;
            bool isNear;
            int offset = 0;
            for (int x = 0; x < index; x++)
            {
                isNear = true;
                table1 = _tables[x] as Table;
                offset = table1.Column.Value - table2.Column.Value;
                if (offset != 0)
                {
                    minTable = offset > 0 ? table2 : table1;
                    maxTable = offset > 0 ? table1 : table2;
                    isNear &= maxTable.Column - (minTable.Column + minTable.Width) <= 1;
                }

                offset = table1.Row.Value - table2.Row.Value;
                if (offset != 0)
                {
                    minTable = offset > 0 ? table2 : table1;
                    maxTable = offset > 0 ? table1 : table2;
                    isNear &= maxTable.Row - (minTable.Row + minTable.Height) <= 1;
                }
                if (isNear) return true;
            }
            return false;
        }

        private void UpdateMaxPaxs()
        {
            MaxPaxs = (short)this.Sum(x => x.Paxs);
        }

        #endregion
        #region IEnumerable

        public IEnumerator<Table> GetEnumerator()
        {
            return _tables.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}