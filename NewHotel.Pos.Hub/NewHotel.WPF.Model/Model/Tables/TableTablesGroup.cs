﻿using NewHotel.WPF.MVVM;
using System;

namespace NewHotel.WPF.App.Pos.Model
{
	public class TableTablesGroup : BindableBase
	{

        #region Dependency Properties

        private Guid TableProperty;
        private Guid TablesGroupProperty;

        #endregion

        #region Constructors

        public TableTablesGroup(Guid table, Guid tablesGroup)
        {
            Table = table;
            TablesGroup = tablesGroup;
        }

        #endregion

        #region Properties

        public Guid Table
        {
            get { return TableProperty; }
            set { SetProperty(ref TableProperty, value); }
        }

        public Guid TablesGroup
        {
            get { return TablesGroupProperty; }
            set { SetProperty(ref TablesGroupProperty, value); }
        }

        #endregion

    }
}
