﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using NewHotel.Business;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;
using NewHotel.Fiscal.Saft;
using NewHotel.Pos.Core.Ext;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Localization;
using NewHotel.Pos.Printer.Interfaces;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Model.Model.Taxes;
using NewHotel.WPF.MVVM;
using NewHotel.WPF.Model.Model.CommonUtils;
using System.Diagnostics.Contracts;

namespace NewHotel.WPF.Model.Model
{

	public interface ITicketInformation : ITicket
    {
        #region Members

        event Action<ITicketInformation, decimal> TotalAmountChanged;
        // decimal TotalAmount { get; set; }
        Table? Table { get; set; }
        int OrderCount { get; }
        bool Uploaded { get; }
        string OpenUserDescription { get; }

        #endregion
    }

    public class Ticket : BindableBase, ITicketInformation, IPrintableDoc
    {
        #region Variables

        private ObservableCollection<Order>? _orders;
        private ObservableCollection<Order> _auxOrders;
        private ObservableCollection<Payment>? _payments;
        private ObservableCollection<LookupTable> _lookupTables;
        private ObservableCollection<Client> _clients;
        private decimal _subTotalBeforeDiscount;
        private TablesGroup _tablesGroupModel;
        private bool _tipOverNetProperty;
        private bool _isEditableProperty;
        private static long _consecutive = 1;
        private static long _trialConsecutive = 1;
        int _previousReceiveCons;
        private Table? _tableModel;
        private decimal _subTotalAmount;
        private decimal _netSubTotalAmount;
        private decimal _grossSubTotalAmount;
        private decimal _grossInBaseCurrencySubTotalAmount;
        private decimal _grossSubTotalAmountInBaseCurrency;
        private decimal _newChargeAmount;
        private decimal _paymentTotalAmount;
        private decimal _balanceAmount;
        private decimal _taxAmount;
        private decimal _debtAmount;
        private bool _showDescription;
        private string _blockCode;
        private Guid? _anulation;
        private decimal _changeAmount;
        private POSTicketContract _contract;
        private bool _canAddSpaProduct;

        #endregion
        #region Constructors

        public Ticket(Guid standId, Guid cashierId, Guid userId, string seriePrex, long shift, Guid? table, bool taxIncluded, Guid? taxSchemaId)
        {
            _contract = new POSTicketContract
            {
                Stand = standId,
                Caja = cashierId,
                OpeningDate = Hotel.Instance.CurrentStandCashierContext.Stand.StandWorkDate,
                OpeningTime = DateTime.Today,
                Currency = string.Empty,
                UserId = userId,
                Serie = seriePrex,
                Shift = shift,
                TaxIncluded = taxIncluded,
                TaxSchemaId = taxSchemaId,
                Mesa = table,
            };

            IsEditable = true;
            InitModel();
        }

        public Ticket(POSTicketContract? ticket = null)
        {
            if (ticket != null)
            {
                _contract = ticket;
                IsEditable = true;
                ShowDescription = !string.IsNullOrEmpty(ticket.Name);
            }
            else
            {
                _contract = new POSTicketContract();
                TaxIncluded = Hotel.Instance.TaxIncluded;
                TaxSchemaId = Hotel.Instance.DefaultTaxSchema.Value;
                TipValue = 0;
                TipPercent = 0;
            }

            if (!string.IsNullOrEmpty(TicketSignature))
                SignedLabel = SAFTPT.GetLabel(TicketSignature);

            InitModel();
        }

        #endregion
        #region Events 

        public event Action<Ticket, Action<bool, Exception>> InitRequested;
        public event Action<Ticket, Action<bool>, bool> InitTip;
        public event Action<Ticket, LookupTable, Action<bool>> InitLookupTableRequested;
        public event Action<ITicketInformation, decimal> TotalAmountChanged;
        public event Action<Ticket, string> CriticalError;
        public event Action<Ticket> DataContextChanged;

        #endregion
        #region Properties

        public Guid TaxSchemaId
        {
            get => _contract.TaxSchemaId ?? (_contract.TaxSchemaId = Hotel.Instance.DefaultTaxSchema.Value).Value;
            set
            {
                var refValue = _contract.TaxSchemaId;
                SetProperty(ref refValue, value, () => _contract.TaxSchemaId = refValue);
            }
        }

        public TaxSchema TaxSchema => Hotel.Instance.TaxSchemas.FirstOrDefault(x => x.Id == TaxSchemaId);

        public POSDocumentType DocumentType
        {
            get => _contract.DocumentType;
            set
            {
                var refValue = _contract.DocumentType;
                SetProperty(ref refValue, value, () => _contract.DocumentType = refValue);
            }
        }

        public Guid Stand
        {
            get => _contract.Stand;
            set
            {
                var refValue = _contract.Stand;
                SetProperty(ref refValue, value, () => _contract.Stand = refValue);
            }
        }

        public Guid Cashier
        {
            get => _contract.Caja;
            set
            {
                var refValue = _contract.Caja;
                SetProperty(ref refValue, value, () => _contract.Caja = refValue);
            }
        }

        public DateTime CheckOpeningDate
        {
            get => _contract.OpeningDate;
            set
            {
                var refValue = _contract.OpeningDate;
                SetProperty(ref refValue, value, () => _contract.OpeningDate = refValue);
            }
        }

        public DateTime CheckOpeningTime
        {
            get => _contract.OpeningTime;
            set
            {
                var refValue = _contract.OpeningTime;
                SetProperty(ref refValue, value, () => _contract.OpeningTime = refValue);
            }
        }

        public DateTime? CloseDate
        {
            get => _contract.ClosedDate;
            set
            {
                var refValue = _contract.ClosedDate;
                SetProperty(ref refValue, value, () => _contract.ClosedDate = refValue);
            }
        }

        public short Paxs
        {
            get => _contract.Paxs;
            set
            {
                var refValue = _contract.Paxs;
                SetProperty(ref refValue, value, () => _contract.Paxs = refValue);
            }
        }
        
        public short CurrentPaxNumber
        {
            get => _contract.CurrentPaxNumber;
            set
            {
                var refValue = _contract.CurrentPaxNumber;
                if (SetProperty(ref refValue, value, () => _contract.CurrentPaxNumber = refValue))
                    OnPropertyChanged(nameof(CurrentPaxName));
            }
        }

        public string CurrentPaxName
        {
            get
            {
                if (_contract.CurrentPaxNumber <= 0) return string.Empty;
                var clientByPosition = _contract.ClientsByPosition.FirstOrDefault(cp => cp.Pax == _contract.CurrentPaxNumber);
                return clientByPosition != null ? $"{clientByPosition.ClientName} ({_contract.CurrentPaxNumber})" : string.Empty;
            }
        }

        public int ReceiveCons
        {
            get => _contract.ReceiveCons;
            set
            {
                _previousReceiveCons = this.ReceiveCons;
                var refValue = _contract.ReceiveCons;
                SetProperty(ref refValue, value, () => _contract.ReceiveCons = refValue);
            }
        }

        public Guid UserId
        {
            get => _contract.UserId;
            set
            {
                var refValue = _contract.UserId;
                SetProperty(ref refValue, value, () => _contract.UserId = refValue);
            }
        }

        public decimal TipAmount
        {
            get => TipValue;
            set => TipValue = value;
        }

        public string UserDescription
        {
            get => _contract.UserDescription;
            set
            {
                var refValue = _contract.UserDescription;
                SetProperty(ref refValue, value, () => _contract.UserDescription = refValue);
            }
        }

        public Guid? OpenUserId
        {
            get => _contract.OpenUserId;
            set
            {
                var refValue = _contract.OpenUserId;
                SetProperty(ref refValue, value, () => _contract.OpenUserId = refValue);
            }
        }

        public string OpenUserDescription
        {
            get => _contract.OpenUserDescription;
            set
            {
                var refValue = _contract.OpenUserDescription;
                SetProperty(ref refValue, value, () => _contract.OpenUserDescription = refValue);
            }
        }

        public decimal GeneralDiscount
        {
            get => _contract.GeneralDiscount;
            set
            {
                var refValue = _contract.GeneralDiscount;
                SetProperty(ref refValue, value, () => _contract.GeneralDiscount = refValue);
            }
        }

        public decimal Recharge
        {
            get => _contract.Recharge;
            set
            {
                var refValue = _contract.Recharge;
                SetProperty(ref refValue, value, () => _contract.Recharge = refValue);
            }
        }

        public decimal ChangeAmount
        {
            get => _changeAmount;
            set => SetProperty(ref _changeAmount, value);
        }

        public string FpSerialInvoice
        {
            get => _contract.FpSerialInvoice;
            set
            {
                var refValue = _contract.FpSerialInvoice;
                SetProperty(ref refValue, value, () => _contract.FpSerialInvoice = refValue);
            }
        }

        public long? FpInvoiceNumber
        {
            get => _contract.FpInvoiceNumber;
            set
            {
                var refValue = _contract.FpInvoiceNumber;
                SetProperty(ref refValue, value, () => _contract.FpInvoiceNumber = refValue);
            }
        }

        public bool IsTicket => _contract.IsOnlyTicket;
        public bool IsBallot => _contract.IsBallot;
        public bool IsInvoice => _contract.IsInvoice;
        public bool IsCreditNote => _contract.IsCreditNote;
        public bool DigitalMenu => _contract.DigitalMenu;

        public string TableDescription => Table?.Name;

        public string FpSerialCreditNote
        {
            get => _contract.FpSerialCreditNote;
            set
            {
                var refValue = _contract.FpSerialCreditNote;
                SetProperty(ref refValue, value, () => _contract.FpSerialCreditNote = refValue);
            }
        }

        public long? FpCreditNoteNumber
        {
            get => _contract.FpCreditNoteNumber;
            set
            {
                var refValue = _contract.FpCreditNoteNumber;
                SetProperty(ref refValue, value, () => _contract.FpCreditNoteNumber = refValue);
            }
        }

        public Guid? CashierPave
        {
            get => _contract.CajaPave;
            set
            {
                var refValue = _contract.CajaPave;
                SetProperty(ref refValue, value, () => _contract.CajaPave = refValue);
            }
        }

        public string SeriePrex
        {
            get => _contract.Serie;
            set
            {
                var refValue = _contract.Serie;
                SetProperty(ref refValue, value, () => _contract.Serie = refValue);
            }
        }

        public long? Serie
        {
            get => _contract.Number;
            set
            {
                var refValue = _contract.Number;
                SetProperty(ref refValue, value, () => _contract.Number = refValue);
            }
        }

        public string ValidationCode
        {
            get => _contract.ValidationCode;
            set
            {
                var refValue = _contract.ValidationCode;
                SetProperty(ref refValue, value, () => _contract.ValidationCode = refValue);
            }
        }

        public Blob? QrCode
        {
            get
            {
                if (_contract.QrCode == null && !string.IsNullOrEmpty(_contract.QrCodeData))
                {
                    _contract.QrCode = QRCodeUtil.GetQRCodeFromData(_contract.QrCodeData);
                }
                return _contract.QrCode;
            }
            set
            {
                var refValue = _contract.QrCode;
                SetProperty(ref refValue, value, () => _contract.QrCode = refValue);
            }
        }

        public long OpeningNumber
        {
            get => _contract.OpeningNumber;
            set
            {
                var refValue = _contract.OpeningNumber;
                SetProperty(ref refValue, value, () => _contract.OpeningNumber = refValue);
            }
        }

        public DateTime? CloseTime
        {
            get => _contract.ClosedTime;
            set
            {
                var refValue = _contract.ClosedTime;
                SetProperty(ref refValue, value, () => _contract.ClosedTime = refValue);
            }
        }

        public Guid? Anulation
        {
            get => _anulation;
            set => SetProperty(ref _anulation, value);
        }

        public Guid? InternalUse
        {
            get => _contract.InternalUse;
            set
            {
                var refValue = _contract.InternalUse;
                SetProperty(ref refValue, value, () => _contract.InternalUse = refValue);
            }
        }

        public Guid? Account
        {
            get => _contract.Account;
            set
            {
                var refValue = _contract.Account;
                SetProperty(ref refValue, value, () => _contract.Account = refValue);
            }
        }

        public CurrentAccountType? AccountType
        {
            get => _contract.AccountType;
            set
            {
                var refValue = _contract.AccountType;
                SetProperty(ref refValue, value, () => _contract.AccountType = refValue);
            }
        }

        public string AccountDescription
        {
            get => _contract.AccountDescription;
            set
            {
                var refValue = _contract.AccountDescription;
                SetProperty(ref refValue, value, () => _contract.AccountDescription = refValue);
            }
        }

        public Guid? AccountInstallationId
        {
            get => _contract.AccountInstallationId;
            set
            {
                var refValue = _contract.AccountInstallationId;
                SetProperty(ref refValue, value, () => _contract.AccountInstallationId = refValue);
            }
        }

        public decimal? AccountBalance
        {
            get => _contract.AccountBalance;
            set
            {
                var refValue = _contract.AccountBalance;
                SetProperty(ref refValue, value, () => _contract.AccountBalance = refValue);
            }
        }

        public decimal? PendingBalance => AccountBalance + TotalAmount;

        public bool AllowRoomCredit
        {
            get => _contract.AllowRoomCredit;
            set
            {
                var refValue = _contract.AllowRoomCredit;
                SetProperty(ref refValue, value, () => _contract.AllowRoomCredit = refValue);
            }
        }

        public string CurrencySymbol => _contract.CurrencySymbol;

        public string LastUserDispatched { get; set; }

        public Guid? Duty
        {
            get => _contract.Duty;
            set
            {
                var refValue = _contract.Duty;
                SetProperty(ref refValue, value, () => _contract.Duty = refValue);
            }
        }

        public string Observations
        {
            get => _contract.Observations;
            set
            {
                var refValue = _contract.Observations;
                SetProperty(ref refValue, value, () => _contract.Observations = refValue);
            }
        }

        public string Unmo
        {
            get => _contract.Currency;
            set
            {
                var refValue = _contract.Currency;
                SetProperty(ref refValue, value, () => _contract.Currency = refValue);
            }
        }

        public DateTime? AnulDate
        {
            get => _contract.CancellationDate;
            set
            {
                var refValue = _contract.CancellationDate;
                SetProperty(ref refValue, value, () => _contract.CancellationDate = refValue);
            }
        }

        public DateTime? AnulTime
        {
            get => _contract.CancellationSystemDate;
            set
            {
                var refValue = _contract.CancellationSystemDate;
                SetProperty(ref refValue, value, () => _contract.CancellationSystemDate = refValue);
            }
        }

        public Guid? TableId
        {
            get => _contract.Mesa;
            set
            {
                var refValue = _contract.Mesa;
                SetProperty(ref refValue, value, () => _contract.Mesa = refValue);
            }
        }

        public int Shift
        {
            get => (int)_contract.Shift;
            set
            {
                int refValue = (int)_contract.Shift;
                SetProperty(ref refValue, value, () => _contract.Shift = refValue);
            }
        }

        public bool TaxIncluded
        {
            get => _contract.TaxIncluded;
            set
            {
                var refValue = _contract.TaxIncluded;
                SetProperty(ref refValue, value, () => _contract.TaxIncluded = refValue);
            }
        }

        public decimal SubTotalAmountBeforeDiscount
        {
            get => _subTotalBeforeDiscount;
            set => SetProperty(ref _subTotalBeforeDiscount, value, nameof(SubTotalAmountBeforeDiscount));
        }

        public decimal ExchangeFactor => _contract.ExchangeFactor;

        void SetDataContext(POSTicketContract contract, bool isNew)
        {
            if (isNew)
            {
                savedStatus.Clear();
            }
            _contract = contract;
			InitModel();
			DataContextChanged?.Invoke(this);
			RaiseAllPropertyChanged();
		}

		public override object DataContext
        {
            get => _contract;
            set => SetDataContext((POSTicketContract)value, true);
        }

        public static long TrialConsecutive => _trialConsecutive++;

        public static long Consecutive => _consecutive++;

        public DateTime LastModified
        {
            get => _contract.LastModified;
            set
            {
                var refValue = _contract.LastModified;
                SetProperty(ref refValue, value, () => _contract.LastModified = refValue);
            }
        }

        public string SignedLabel { get; set; }

        public CancellationControl AnulationModel { get; set; }

        public bool HasVisibleOrders => Orders.Any(o => o.IsVisible);

        public bool HasAlcoholicOrders => Orders.Any(o => o.IsActive && (o.Alcoholic ?? false));

        public bool HasNonAlcoholicOrders => Orders.Any(o => o.IsActive && !(o.Alcoholic ?? false));

        public bool HasTransferredOrders => Orders.Any(o => o.CancellationStatus == ProductLineCancellationStatus.ActiveByTransfer);

        public bool IsEditable
        {
            get => _isEditableProperty;
            set => SetProperty(ref _isEditableProperty, value);
        }

        public bool IsFull => true;

        public int VisibleOrdersCount => Orders?.Count(x => x.IsVisible) ?? 0;

        public int OrderCount => Orders == null ? 0 : Orders.Count;

        public bool IsPersisted => _contract.IsPersisted;

        public bool CanAddSpaProduct
        {
            get => _canAddSpaProduct;
            set => SetProperty(ref _canAddSpaProduct, value);
        }

IEnumerable<IPrintableOrder> IPrintableDoc.ProductLines => Orders;

        IEnumerable<IPrinteableLookupTable> IPrintableDoc.LookupTables => LookupTables;

        IEnumerable<IPrintablePayment> IPrintableDoc.Payments => Payments;

        IEnumerable<IPrintableClient> IPrintableDoc.Clients => Clients;

        #region DummyProperties

        public bool IsAccepted { get; set; }

        public bool IsReturned { get; set; }

        public bool NoTable => !_contract.Mesa.HasValue;

        public bool AllowPrint
        {
            get
            {
                if (TotalAmount > PaymentTotalAmount)
                    return false;

                if (HasAccountDepositPayment)
                    return false;

                if (Hotel.Instance.HasFiscalPrinter)
                    return !HasCashPayment;

                switch (Hotel.Instance.SignType)
                {
                    case DocumentSign.None:
                        return TotalAmount <= PaymentTotalAmount;
                    case DocumentSign.FiscalizationEcuador:
                        return HasCreditRoomPayment ||
                               HasMealPlanPayment ||
                               IsHouseUsePayment ||
                               IsHouseUseAndCashCombinedPayments;
                    case DocumentSign.FiscalizationPortugal:
                    case DocumentSign.FiscalizationAngola:
                    case DocumentSign.FiscalizationMalta:
                        return !HasCashPayment;
                    case DocumentSign.FiscalPrinter:
                    case DocumentSign.FiscalizationBrazilNFE:
                    case DocumentSign.FiscalizationCroatia:
                    case DocumentSign.FiscalizationChile:
                    case DocumentSign.FiscalizationBrazilCMFLEX:
                    case DocumentSign.FiscalizationPeruDFacture:
                    case DocumentSign.FiscalizationColombiaBtw:
                    case DocumentSign.FiscalizationBrazilProsyst:
                    case DocumentSign.FiscalizationCuracao:
                    case DocumentSign.FiscalizationMexicoCfdi:
                    case DocumentSign.FiscalizationUruguayExFactura:
                    case DocumentSign.FiscalizationPanamaHKA:
                        return IsCashPayment || HasCreditRoomPayment || HasMealPlanPayment || HasHouseUsePayment;
                    default:
                        return HasPayments;
                }
            }
        }

        public bool AllowBallot => Hotel.Instance.AllowBallotDocument && AllowPrint;

        public bool AllowPrintInvoice
        {
            get
            {
                if (TotalAmount > PaymentTotalAmount)
                    return false;

                if (HasAccountDepositPayment)
                    return true;
                
                if (Hotel.Instance.HasFiscalPrinter)
                    return HasCashPayment && !HasMealPlanPayment && !HasCreditRoomPayment && !HasHouseUsePayment;

                // Don't let invoice if the ticket only has a meal plan payment and pay the total amount
                if (IsMealPlanPayment && TotalAmount <= PaymentTotalAmount)
                    return false;

                switch (Hotel.Instance.SignType)
                {
                    case DocumentSign.None:
                        return Hotel.Instance.EmitOnlyTickets == false && TotalAmount <= PaymentTotalAmount;
                    case DocumentSign.FiscalizationColombiaBtw:
                    case DocumentSign.FiscalizationPanamaHKA:
                        return !HasCreditRoomPayment;
                    case DocumentSign.FiscalizationEcuador:
                        return IsCashPayment;
                    case DocumentSign.FiscalizationPortugal:
                    case DocumentSign.FiscalizationAngola:
                    case DocumentSign.FiscalizationMalta:
                        return !HasCreditRoomPayment && !IsHouseUsePayment;
                    case DocumentSign.FiscalizationBrazilCMFLEX:
                        return false;
                    case DocumentSign.FiscalPrinter:
                    case DocumentSign.FiscalizationBrazilNFE:
                    case DocumentSign.FiscalizationCroatia:
                    case DocumentSign.FiscalizationChile:
                    case DocumentSign.FiscalizationPeruDFacture:
                    case DocumentSign.FiscalizationBrazilProsyst:
                    case DocumentSign.FiscalizationCuracao:
                    case DocumentSign.FiscalizationMexicoCfdi:
                    case DocumentSign.FiscalizationUruguayExFactura:
                        return IsCashPayment || IsHouseUseAndCashCombinedPayments;
                    default:
                        return HasPayments;
                }
            }
        }

        public bool AllowDepositPayment
        {
            get
            {
                return !HasPayments || !HasCreditRoomPayment;
            }
        }

        public bool AllowCreditPayment
        {
            get
            {
                return !HasPayments || !HasAccountDepositPayment;
            }
        }

        public bool IsHouseUseAndCashCombinedPayments => HasHouseUsePayment && Payments.Count - Payments.Count(x => x.IsCash) == 1;

        public bool HasMealPlanPayment => HasPayments && Payments.Any(x => x.IsMealPlanPayment);

		/// <summary>
		/// Check if the ticket only has meal plan payment
		/// </summary>
		private bool IsMealPlanPayment => HasPayments && Payments.All(x => x.IsMealPlanPayment);

		public bool IsCreditRoomPayment => HasPayments && Payments.All(x => x.IsCreditRoom);

        public bool HasCreditRoomPayment => HasPayments && Payments.Any(x => x.IsCreditRoom);

        public bool HasHouseUsePayment => HasPayments && Payments.Any(x => x.IsHouseUsePayment);

        public bool IsHouseUsePayment => Payments.Count == 1 && Payments.All(x => x.IsHouseUsePayment);

        public bool  IsCashPayment => HasPayments && Payments.All(x => x.IsCash);

        public bool HasCashPayment => HasPayments && Payments.Any(x => x.IsCash);

        public bool HasAccountDepositPayment => HasPayments && Payments.Any(x => x.IsAccountDeposit);

		public bool HasPayments => Payments is { Count: > 0 };

		public Func<decimal, decimal> Tax { get; set; }

        #region Series

        #region Online

        public string FiscalDocSerie => string.IsNullOrEmpty(FiscalDocSeriePrex) ? string.Empty : $"{FiscalDocSeriePrex}/{FiscalDocNumber ?? 0}";

        #endregion

        #region Offline SAFT

        public Guid? BaseEntityId
        {
            get => _contract.BaseEntityId;
            set => _contract.BaseEntityId = value;
        }

        public bool AutomaticProductDiscount
        {
            get => _contract.ApplyAutomaticProductDiscount;
            set => _contract.ApplyAutomaticProductDiscount = value;
        }

        public string TicketSignature
        {
            get => _contract.Signature;
            set => _contract.Signature = value;
        }

        public Blob? DigitalSignature
        {
            get => _contract.DigitalSignature;
            set
            {
                var refValue = _contract.DigitalSignature;
                SetProperty(ref refValue, value, () => _contract.DigitalSignature = refValue);
            }
        }

        public string FiscalDocSeriePrex
        {
            get => _contract.FiscalDocSerie;
            set
            {
                var refValue = _contract.FiscalDocSerie;
                SetProperty(ref refValue, value, () => _contract.FiscalDocSerie = refValue);
            }
        }

        public long? FiscalDocNumber
        {
            get => _contract.FiscalDocNumber;
            set
            {
                var refValue = _contract.FiscalDocNumber;
                SetProperty(ref refValue, value, () => _contract.FiscalDocNumber = refValue);
            }
        }

        public string FiscalDocValidationCode
        {
            get => _contract.FiscalDocValidationCode;
            set => _contract.FiscalDocValidationCode = value;
        }

        public Blob? FiscalDocQrCode
        {          
            get 
            {
                if (_contract.FiscalDocQrCode == null && !string.IsNullOrEmpty(_contract.FiscalDocQrCodeText))
                {
                    _contract.FiscalDocQrCode = QRCodeUtil.GetQRCodeFromData(_contract.FiscalDocQrCodeText);
                }
                return _contract.FiscalDocQrCode;
            }
            set => _contract.FiscalDocQrCode = value;
        }

        public string FiscalDocSignature
        {
            get => _contract.FiscalDocSignature;
            set => _contract.FiscalDocSignature = value;
        }

        public string FiscalDocTo
        {
            get => _contract.FiscalDocTo;
            set => _contract.FiscalDocTo = value;
        }

        public string FiscalPhoneNumber
        {
            get => _contract.FiscalPhoneNumber;
            set => _contract.FiscalPhoneNumber = value;
        }

        public string FiscalCityName
        {
            get => _contract.FiscalCityName;
            set => _contract.FiscalCityName = value;
        }

        public string FiscalCityCode
        {
            get => _contract.FiscalCityCode;
            set => _contract.FiscalCityCode = value;
        }

        public string FiscalRegimeTypeCode
        {
            get => _contract.FiscalRegimeTypeCode;
            set => _contract.FiscalRegimeTypeCode = value;
        }

        public string FiscalResponsabilityCode
        {
            get => _contract.FiscalResponsabilityCode;
            set => _contract.FiscalResponsabilityCode = value;
        }

        public string FiscalIdenTypeName
        {
            get
            {
                PersonalDocType? identity = FiscalIdenType;
                return (identity switch
                {
                    0 => "FiscalNumber".Translate(),
                    null => "FiscalNumber".Translate(),
                    _ => LocalizationMgr.Translation(identity.ToString()),
                }) + ":";
            }
        }

        public string FiscalDocAddress => _contract.FiscalDocAddress;

        public string FiscalDocInfoName
        {
            get => _contract.FiscalDocTo;
            set
            {
                _contract.FiscalDocTo = value;
                OnPropertyChanged("FiscalDocInfoName");
            }
        }

        public string FiscalDocInfoAddress
        {
            get => _contract.FiscalDocAddress;
            set => _contract.FiscalDocAddress = value;
        }

        public string FiscalDocFiscalNumber
        {
            get => _contract.FiscalDocFiscalNumber;
            set => _contract.FiscalDocFiscalNumber = value;
        }
        public string FiscalDocNacionality
        {
            get => _contract.FiscalDocNacionality;
            set => _contract.FiscalDocNacionality = value;
        }
        public string FiscalDocEmail
        {
            get => _contract.FiscalDocEmail;
            set => _contract.FiscalDocEmail = value;
        }
        public PersonalDocType? FiscalIdenType
        {
            get => (PersonalDocType?)_contract.FiscalIdenType;
            set => _contract.FiscalIdenType = (long?)value;
        }

        public string Cufe 
        {
            get => _contract.Cufe;
            set => _contract.Cufe = value;
        }
        public string CreditNoteSerie => string.IsNullOrEmpty(CreditNoteSeriePrex) ? string.Empty : $"{CreditNoteSeriePrex}/{CreditNoteNumber ?? 0}";

        public string CreditNoteSeriePrex
        {
            get => _contract.CreditNoteSerie;
            set
            {
                var refValue = _contract.CreditNoteSerie;
                SetProperty(ref refValue, value, () => _contract.CreditNoteSerie = refValue);
            }
        }

        public long? CreditNoteNumber
        {
            get => _contract.CreditNoteNumber;
            set
            {
                var refValue = _contract.CreditNoteNumber;
                SetProperty(ref refValue, value, () => _contract.CreditNoteNumber = refValue);
            }
        }

        public DateTime? CreditNoteSystemDate
        {
            get => _contract.CreditNoteSystemDate;
            set => _contract.CreditNoteSystemDate = value;
        }

        public DateTime? CreditNoteWorkDate
        {
            get => _contract.CreditNoteWorkDate;
            set => _contract.CreditNoteWorkDate = value;
        }

        public string CreditNoteValidationCode
        {
            get => _contract.CreditNoteValidationCode;
            set => _contract.CreditNoteValidationCode = value;
        }

        public Blob? CreditNoteQrCode
        {
            get 
            {
                if (_contract.CreditNoteQrCode == null && !string.IsNullOrEmpty(_contract.CreditNoteQrCodeData))
                {
                    _contract.CreditNoteQrCode = QRCodeUtil.GetQRCodeFromData(_contract.CreditNoteQrCodeData);
                }
                return _contract.CreditNoteQrCode;
            }
            set => _contract.CreditNoteQrCode = value;
        }

        public string CreditNoteSignature
        {
            get => _contract.CreditNoteSignature;
            set => _contract.CreditNoteSignature = value;
        }

        public string CreditNoteUserDescription
        {
            get => _contract.CreditNoteUserDescription;
            set => _contract.CreditNoteUserDescription = value;
        }

        #endregion

        #endregion

        #region Credit

        public ObservableCollection<object> AccountRecords
        {
            get => AccountRecordsProperty;
            set => SetProperty(ref AccountRecordsProperty, value);
        }

        private ObservableCollection<object> AccountRecordsProperty = [];


        #endregion

        #region Pension

        public string PensionDescription
        {
            get => PensionDescriptionProperty;
            set => SetProperty(ref PensionDescriptionProperty, value);
        }

        private string PensionDescriptionProperty;

        public short? PensionId
        {
            get => PensionIdProperty;
            set => SetProperty(ref PensionIdProperty, value);
        }

        private short? PensionIdProperty;

        public string PensionIni
        {
            get => PensionIniProperty;
            set => SetProperty(ref PensionIniProperty, value);
        }

        private string PensionIniProperty;

        public string PensionFin
        {
            get => PensionFinProperty;
            set => SetProperty(ref PensionFinProperty, value);
        }

        private string PensionFinProperty;



        public ObservableCollection<string> PensionGuests
        {
            get => PensionGuestsProperty;
            set => SetProperty(ref PensionGuestsProperty, value);
        }

        private ObservableCollection<string> PensionGuestsProperty;

        #endregion

        public bool IsClosed => _contract.ClosedDate.HasValue;

        public bool Uploaded { get; private set; }

        #endregion

        public Guid? ReservationTableId
        {
            get => _contract.ReservationTableId;
            set => _contract.ReservationTableId = value;
        }

        public bool IsFromTableGroupReservation => _contract.ReservationTable != null && _contract.ReservationTable.IsGroupTables;

        public Guid Id
        {
            get => (Guid)_contract.Id;
            set => _contract.Id = value;
        }

        public bool IsAcceptedAsTransfer
        {
            get => _contract.IsAcceptedAsTransfer;
            set => _contract.IsAcceptedAsTransfer = value;
        }

        public Table? Table
        {
            get => _tableModel;
            set => SetProperty(ref _tableModel, value);
        }

        public Guid? TablesGroupId => TablesGroup?.Id;

        public TablesGroup TablesGroup
        {
            get => _tablesGroupModel;
            set => SetProperty(ref _tablesGroupModel, value);
        }

        public decimal SubTotalAmount
        {
            get => _subTotalAmount;
            set => SetProperty(ref _subTotalAmount, value);
        }

        public decimal NetSubTotalAmount
        {
            get => _netSubTotalAmount;
            set => SetProperty(ref _netSubTotalAmount, value);
        }

        public decimal GrossSubTotalAmount
        {
            get => _grossSubTotalAmount;
            set => SetProperty(ref _grossSubTotalAmount, value, nameof(GrossSubTotalAmount));
        }

        public decimal GrossInBaseCurrencySubTotalAmount
        {
            get => _grossInBaseCurrencySubTotalAmount;
            set => SetProperty(ref _grossInBaseCurrencySubTotalAmount, value, nameof(GrossInBaseCurrencySubTotalAmount));
        }

        public bool HasConversionRate => _contract.ExchangeFactor != 1M;

        public decimal TotalAmount
        {
            get => _contract.Total;
            set
            {
                var refValue = _contract.Total;
                SetProperty(ref refValue, value, () => _contract.Total = refValue);
            }
        }

        public decimal NewChargeAmount
        {
            get => _newChargeAmount;
            set => SetProperty(ref _newChargeAmount, value);
        }

        public decimal PaymentTotalAmount
        {
            get => _paymentTotalAmount;
            set => SetProperty(ref _paymentTotalAmount, value);
        }

        public decimal BalanceAmount
        {
            get => _balanceAmount;
            set => SetProperty(ref _balanceAmount, value);
        }

        public decimal TaxAmount
        {
            get => _taxAmount;
            set => SetProperty(ref _taxAmount, value);
        }

        public decimal DebtAmount
        {
            get => _debtAmount;
            set => SetProperty(ref _debtAmount, value);
        }

        public string Description
        {
            get => _contract.Name;
            set
            {
                var refValue = _contract.Name;
                SetProperty(ref refValue, value, () => _contract.Name = refValue, nameof(Description));
            }
        }

        public bool ShowDescription
        {
            get => _showDescription;
            set => SetProperty(ref _showDescription, value);
        }

        // public bool Process
        // {
        //     get => _contract.Process;
        //     set
        //     {
        //         var refValue = _contract.Process;
        //         SetProperty(ref refValue, value, () => _contract.Process = refValue);
        //     }
        // }

        public Guid[] ProductLinesIds => _contract.ProductLinesIds;

        public bool HasLines => _contract.ProductLines.Any();

        public bool TipsEnabled => !Payments.Any();

        public decimal TipPercent
        {
            get => _contract.TipPercent ?? decimal.Zero;
            set
            {
                var refValue = _contract.TipPercent;
                SetProperty(ref refValue, value, () => _contract.TipPercent = refValue);
            }
        }

        public bool IsTipPercent => _contract.TipPercent.HasValue && _contract.TipPercent.Value > decimal.Zero;

        public decimal TipValue
        {
            get => _contract.TipValue ?? decimal.Zero;
            set
            {
                var refValue = _contract.TipValue;
                SetProperty(ref refValue, value, () => _contract.TipValue = refValue);
            }
        }

        public bool TipOverNet
        {
            get => _tipOverNetProperty;
            set => SetProperty(ref _tipOverNetProperty, value);
        }

        private Order? GetTipOrder()
        {
            return Orders.FirstOrDefault(o => o.IsTip);
        }

        public string TipDescription
        {
            get
            {
                var tip = GetTipOrder();
                if (tip != null)
                    return tip.ProductDescription;

                return "Tip";
            }
        }

        public bool Blocked
        {
            get => _contract.Opened;
            set
            {
                var refValue = _contract.Opened;
                SetProperty(ref refValue, value, () => _contract.Opened = refValue);
            }
        }

        public string BlockCode
        {
            get => _blockCode;
            set => SetProperty(ref _blockCode, value);
        }

        public short FiscalDocPrints { get { return _contract.FiscalDocPrints; } set { _contract.FiscalDocPrints = value; } }

        public short CreditNotePrints { get { return _contract.CreditNotePrints; } set { _contract.CreditNotePrints = value; } }

        public short Prints { get { return _contract.Prints; } set { _contract.Prints = value; } }

        public bool IsAnul => _contract.IsAnul;

        public string CashierDescription => _contract.CashierDescription;

        public string StandDescription => _contract.StandDescription;

        public string Pension
        {
            get => _contract.Pension;
            set => _contract.Pension = value;
        }

        public string Room
        {
            get => _contract.Room;
            set
            {
                var refValue = _contract.Room;
                SetProperty(ref refValue, value, () => _contract.Room = refValue);
            }
        }

        public string CardNumber
        {
            get => _contract.CardNumber;
            set => _contract.CardNumber = value;
        }
        
        public string Name
        {
            get => _contract.Name;
            set => _contract.Name = value;
        }

        public Guid? SpaServiceId
        {
            get => _contract.SpaServiceId;
            set => _contract.SpaServiceId = value;
        }

        public bool IsSpaServiceSigned
        {
            get => _contract.IsSpaServiceDigitallySigned;
            set => _contract.IsSpaServiceDigitallySigned = value;
        }

        public string SaloonDescription => _contract.SaloonDescription;

        public bool DiscountEnabled => Orders.Any(o => o.IsVisible && !o.AutomaticDiscount);

        public bool ClientEnabled => Orders.All(o => !o.IsVisible || !o.AutomaticDiscount);

        public bool MustOpenDrawer => Payments.Any(p => p.Type != null && p.Type.IsCashPayment);

        public bool HasRoomServiceNumber => !string.IsNullOrEmpty(_contract.RoomServiceNumber);

        public bool HasOrders => Orders?.Any() ?? false;

        public TypedList<POSClientByPositionContract> ClientsByPosition => _contract.ClientsByPosition;

        #region ContractList

        public ObservableCollection<Order>? Orders
        {
            get => _orders;
            set => SetProperty(ref _orders, value);
        }

        public ObservableCollection<Payment>? Payments
        {
            get => _payments;
            set => SetProperty(ref _payments, value);
        }

        public ObservableCollection<LookupTable> LookupTables
        {
            get => _lookupTables;
            set => SetProperty(ref _lookupTables, value);
        }

        public ObservableCollection<Client> Clients
        {
            get => _clients;
            set => SetProperty(ref _clients, value);
        }

        #endregion

        #endregion
        #region Methods

        #region Public

        Stack<POSTicketContract> savedStatus = new Stack<POSTicketContract>();

        public void SaveStatus()
        {
            var serialized = _contract.SerializeContract();
            var newOne = serialized.DeserializeContract<POSTicketContract>();
            savedStatus.Push(newOne);
        }

        public POSTicketContract GetStatusCopy(bool andKeepIt)
        {
            POSTicketContract? status = null;

            if (savedStatus.Any())
            {
                status = andKeepIt ? savedStatus.Peek() : savedStatus.Pop();
                var serialized = status.SerializeContract();
                status = serialized.DeserializeContract<POSTicketContract>();
            }

            return status;
		}

        public void RestoreStatus(bool andKeepIt, bool keepDiscounts)
        {
            if (savedStatus.Count > 0)
            {
                var status = GetStatusCopy(andKeepIt);
                
                if (keepDiscounts && !_contract.HasMealPlanPayment)// _contract.GeneralDiscount > 0 && _contract.TicketValueBeforeDiscount != _contract.Total)
                {
                    status.GeneralDiscount = _contract.GeneralDiscount;
                    status.Total = _contract.Total;
                    status.Observations = _contract.Observations;
                    foreach (var line in status.ProductLineByTicket)
                    {
                        if (_contract.ProductLineByTicket.FirstOrDefault(x => x.Id.Equals(line.Id)) is POSProductLineContract currentLine)
                        {
                            line.DiscountPercent = currentLine.DiscountPercent;
                            line.DiscountTypeId = currentLine.DiscountTypeId;
                            line.DiscountValue = currentLine.DiscountValue;
                            line.FirstIvaBase = currentLine.FirstIvaBase;
                            line.FirstIvaValue = currentLine.FirstIvaValue;
                            line.GrossValue = currentLine.GrossValue;
                            line.CostValue = currentLine.CostValue;
                            line.NetValue = currentLine.NetValue;
                        }
                    }
                }
                SetDataContext(status, false);
            }
        }

        public void DiscardSavedStatus()
        {
            if (savedStatus.Count > 0)
                savedStatus.Pop();
        }

        public void SetTaxSchema(Guid taxSchemaId)
        {
            TaxSchemaId = taxSchemaId;
            foreach (var order in Orders)
                order.RecalculateCalculateTaxs();
            RecalculateAll();
        }

        public void ClearCollectData(bool resetTaxSchema = true)
        {
            ClearPayments();

            if (resetTaxSchema && TaxSchemaId != Hotel.Instance.DefaultTaxSchema)
                SetTaxSchema(Hotel.Instance.DefaultTaxSchema.Value);

            TipPercent = decimal.Zero;
            TipValue = decimal.Zero;
            RemoveTipOrder();
            RecalculateAll();
        }

        public void BindTablesReservationIntoGroup(Stand stand)
        {
            TablesGroup?.UnlinkTables();
            if (IsFromTableGroupReservation && TableId.HasValue)
            {
                var ticketContract = AsContract();
                var group = new TablesGroup(ReservationTableId.Value);

                group.AddAndLinkTable(Table);
                foreach (var tableId in ticketContract.ReservationTable.TablesIds.Where(t => t != TableId))
                {
                    var tableInGroup = stand.FindTable(tableId);
                    if (tableInGroup != null)
                        group.AddAndLinkTable(tableInGroup);
                }

                TablesGroup = group;
            }
        }

        public void RemoveTip()
        {
            TipPercent = decimal.Zero;
            TipValue = decimal.Zero;
            RemoveTipOrder();
        }

        public void AddPayment(Payment pay, decimal excessTipAmount = 0)
        {
            pay.AddTo(_contract, excessTipAmount);
            Payments?.Add(pay);
            RecalculateAll();
        }

        public void RemovePayment(Payment pay)
        {
            pay.RemoveFrom(_contract);
            Payments?.Remove(pay);
            RecalculateAll();
        }

        public void ApplyOrderFilter(Func<Order, bool>? filterAction, Func<OrderTable, bool> subFilterAction)
        {
            if (filterAction != null)
            {
                _auxOrders = Orders;
                
                Orders = new ObservableCollection<Order>(Orders.Where(filterAction));
                foreach (var order in Orders)
                {
                    if (!order.IsMenu) continue;
                    order.ApplyOrderFilter(subFilterAction);
                }
            }
            else
            {
                Orders = _auxOrders;

                foreach (var order in Orders)
                {
                    if (!order.IsMenu) continue;
                    order.ApplyOrderFilter(subFilterAction);
                }
                
                _auxOrders = null!;
            }
        }

        public void VoidOrder(Order order, bool removeVisual, bool forceCalc = true)
        {
            order.ImporteChanged -= order_QuantityChanged;
            order.CancellationStatus = order.Printed > 0 ? ProductLineCancellationStatus.CancelledAfterPrint : ProductLineCancellationStatus.Cancelled;
            order.Persist();

            if (removeVisual && Orders != null)
                Orders.Remove(order);

            if (!forceCalc) return;
            RecalculateSubTotal();
            RecalculateBalance();
        }
        
        /// <summary>
        /// Recalculate all the ticket values, including the sub total, total, taxes, discounts, tips, etc.
        /// </summary>
        public void RecalculateAll()
        {
            RecalculateSubTotal();
            RecalculatePaymentTotal();
            RecalculateBalance();
        }
        
        /// <summary>
        /// Recalculate the sub total of the ticket, including the net, gross, taxes, discounts, etc.
        /// </summary>
        /// <param name="recalculateAll">It this is true it will calculate the value with a discount</param>
        private void RecalculateSubTotal(bool recalculateAll = true)
        {
            if (Orders != null)
            {
                (SubTotalAmountBeforeDiscount, NetSubTotalAmount, GrossSubTotalAmount, GrossInBaseCurrencySubTotalAmount) =
				Orders.Aggregate(
                        (CostBeforeDiscount: 0M, Net: 0M, Gross: 0M, GrossInBaseCurrency: 0M),
                        (c, x) => (
                            c.CostBeforeDiscount + x.CostBeforeDiscount * Annul(x),
                            c.Net + x.Net * Annul(x),
                            c.Gross + x.Gross * Annul(x),
                            c.GrossInBaseCurrency + x.GrossInBaseCurrency * Annul(x)));
                
                //SubTotalAmountBeforeDiscount = Orders.Sum(x => x.CostBeforeDiscount * Annul(x));
                //NetSubTotalAmount = Orders.Sum(x => x.Net * Annul(x));
                //GrossSubTotalAmount = Orders.Sum(x => x.Gross * Annul(x));
            }
            SubTotalAmount = TaxIncluded ? GrossSubTotalAmount : NetSubTotalAmount;
            if (recalculateAll) Recalculate_NewCharge_Discount_Total(); return;
            // Show or hide the annulled value
            int Annul(IPrintableOrder o) => o.IsVisible ? 1 : 0;
        }
        
        /// <summary>
        /// Calculate the new subtotal value with a discount applied
        /// </summary>
        private void Recalculate_NewCharge_Discount_Total()
        {
            var total = SubTotalAmountBeforeDiscount;
            // Taxes
            if (Orders != null)
            {
                TaxAmount = Orders.Sum(x => x.IsVisible ? x.TotalTaxes : decimal.Zero);
                if(TaxAmount.DecimalPlaces() > 2)
                    TaxAmount = Math.Round(TaxAmount, 2, MidpointRounding.AwayFromZero);
                var addTax = TaxIncluded ? decimal.Zero : TaxAmount;
                // Discounts
                GeneralDiscount = Orders.Sum(x => x.IsVisible ? x.DiscountValue : decimal.Zero);
                // Total sin tips
                TotalAmount = total - GeneralDiscount + addTax;
            }
            // Tips
            if (IsTipPercent) 
                TipValue = (TipOverNet ? NetSubTotalAmount : TotalAmount).GetDiscountValue(TipPercent);
            // Total
            TotalAmount += TipValue + Recharge;
        }
        
        /// <summary>
        /// Recalculate the balance of the ticket, including the debt and change
        /// </summary>
        private void RecalculateBalance()
        {
            BalanceAmount = TotalAmount - PaymentTotalAmount;
            if(Payments != null)
                ChangeAmount = Payments.Sum(x => x.ChangeValue);
            
            DebtAmount = Math.Max(0, BalanceAmount);
        }

        public decimal TryPayment()
        {
            return BalanceAmount;
        }

        /// <summary>
        /// This methods returns the balance amount of the ticket adjusted to 2 decimal places, so that the user pays the exact amount
        /// </summary>
        /// <returns>A adjusted copy of the balance amount property</returns>
        public decimal GetCorrectBalanceAmount()
        {
            var amount = BalanceAmount;
            if(amount.DecimalPlaces() > 2)
                amount = amount.RoundTo(2);

            return amount;
        }

        public Guid? PensionAccountId { get; set; }

        public bool IsPensionValid { get; private set; }

        public Guid CurrentRate { get; set; }

        public void SetRateType(RateType rateType)
        {
            var orders = Orders.Where(x => !x.IsTransfer).ToArray();
            foreach (var order in orders)
                order.SetPriceRateTax(rateType, null);
        }

        /// <summary>
        /// Block the ticket
        /// </summary>
        /// <returns>Returns the blockCode, if it's already blocked return null</returns>
        public string BlockTicket()
        {
            if (Blocked)
                return null;

            Blocked = true;
            return BlockCode = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// Unblock the ticket using the blockCode
        /// </summary>
        /// <param name="blockCode">BlockCode</param>
        /// <returns>Returns true if the unblock opertation is successful</returns>
        public bool UnblockTicket(string blockCode)
        {
            if (BlockCode != blockCode)
                return false;

            BlockCode = null;
            Blocked = false;
            return true;
        }

        public bool ForceUnblockTicket()
        {
            if (!Blocked)
                return false;

            BlockCode = null;
            Blocked = false;
            return true;
        }

        public void ClearPayments()
        {
            // No limpiar la cuenta
            // El valor de la cuenta seleccionada es usado como cuenta predefinida para colocar en el pago
            // cuando el pago lo requiere mas en el ticket queda la cuenta informativa
            // en los campos CCCO_PK, CCCO_TIPO, CCCO_DESC
            InternalUse = null;
            if (Payments.Count > 0)
                Payments.Clear();
            _contract.PaymentLineByTicket.Clear();
        }

        #endregion
        #region Private

        public void InitializeTip(Stand stand, bool recalculateAll = true)
        {
            TipOverNet = stand.TipOverNet;
            if (IsClosed || !stand.TipAuto) return;
            var tipOrder = GetTipOrder();
            if (tipOrder != null || !stand.AddTipOrder(this)) return;
            if (TipPercent > decimal.Zero)
                TipOverNet = stand.TipOverNet;
            else if (TipValue <= decimal.Zero)
            {
                if (stand.TipPercent is > decimal.Zero)
                {
                    TipPercent = stand.TipPercent.Value;
                    TipOverNet = stand.TipOverNet;
                }
                else if (stand.TipValue is > decimal.Zero)
                    TipValue = stand.TipValue.Value;
            }

            if (recalculateAll)
                RecalculateAll();
        }

        public void RemoveTipOrder()
        {
            var stand = Hotel.Instance.CurrentStandCashierContext.Stand;
            if (!stand.TipAuto)
            {
                var tip = GetTipOrder();
                if (tip != null)
                    VoidOrder(tip, true);
            }
        }

        private void Orders_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged("DiscountEnabled");
            OnPropertyChanged("ClientEnabled");
        }

        private void InitModel()
        {
            Orders = _contract.ProductLineByTicket != null
                ? new ObservableCollection<Order>(_contract.ProductLineByTicket.Select(pl => new Order(this, pl)))
                : [];
            Orders.CollectionChanged += Orders_CollectionChanged;

            LookupTables = _contract.LookupTableByTicket != null ? new ObservableCollection<LookupTable>(_contract.LookupTableByTicket.Select(lt => new LookupTable(lt))) : [];

            if (Payments != null) Payments.CollectionChanged -= Payments_CollectionChanged;

            Payments = _contract.PaymentLineByTicket != null ? new ObservableCollection<Payment>(_contract.PaymentLineByTicket.Select(Payment.Create)) : [];

            Clients = _contract.ClientsByPosition != null ? new ObservableCollection<Client>(_contract.ClientsByPosition.Select(cp => new Client(cp))) : [];

            Table = _contract.Mesa.HasValue ? Hotel.Instance.GetTable(_contract.Mesa.Value) : null;
            Payments.CollectionChanged += Payments_CollectionChanged;

            var stand = Hotel.Instance.CurrentStandCashierContext.Stand;
            InitializeTip(stand, false);
            BindTablesReservationIntoGroup(stand);
            RecalculateAll();
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            switch (propertyName)
            {
                case nameof(HasMealPlanPayment):
                    OnAllowPrintDocChanged();
                    break;
                case nameof(Description):
                    ShowDescription = !string.IsNullOrEmpty(Description);
                    break;
                case nameof(ReceiveCons):
                    if (_previousReceiveCons < ReceiveCons && Orders != null)
                    {
                        foreach (var item in Orders)
                            item.Printed++;
                    }
                    break;
                case nameof(TotalAmount):
                    TotalAmountChanged?.Invoke(this, TotalAmount);
                    break;
                case nameof(TableId):
                    if (TableId.HasValue && (Table == null || Table.Id != TableId.Value))
                        Table = Hotel.Instance.GetTable(TableId);
                    break;
                case nameof(Table):
                    if (Table != null && (!TableId.HasValue || Table.Id != TableId.Value))
                        TableId = Table.Id;
                    break;
                case nameof(TaxSchemaId):
                    OnPropertyChanged(nameof(TaxSchema));
                    break;
                case nameof(Room):
                    CanAddSpaProduct = !IsPersisted && !string.IsNullOrEmpty(Room);
                    break;
            }
        }

        public POSTicketContract AsContract()
        {
            return _contract;
        }

        private void RecalculatePayments()
        {
			PaymentTotalAmount = Payments.Sum(x => x.ReceiveValue);
		}

        private void RecalculatePaymentTotal(bool recalculateAll = true)
        {
            RecalculatePayments();
            if (recalculateAll)
                Recalculate_NewCharge_Discount_Total();
        }

        private void order_QuantityChanged(Order arg1, decimal arg2)
        {
            RecalculateSubTotal();
            RecalculateBalance();
        }

        private void Payments_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            RecalculatePaymentTotal();
            RecalculateBalance();
            OnAllowPrintDocChanged();
            OnPropertyChanged(nameof(TipsEnabled));
        }

        public void OnAllowPrintDocChanged()
        {
            OnPropertyChanged(nameof(AllowPrint));
            OnPropertyChanged(nameof(AllowPrintInvoice));
            OnPropertyChanged(nameof(AllowBallot));
            OnPropertyChanged(nameof(AllowDepositPayment));
            OnPropertyChanged(nameof(AllowCreditPayment));
        }

        #endregion

        #endregion
    }
}