﻿using System.Linq;
using System.Collections.Generic;
using NewHotel.WPF.App.Kitchen;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.Contracts;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Kitchen
{
    public sealed class Kitchen
    {
        #region Members

        private readonly Area _area;
        private readonly List<Order> _orders = new List<Order>();
        private readonly List<TicketKitchen> _tickets = new List<TicketKitchen>();

        #endregion
        #region Constructor

        public Kitchen(Area area)
        {
            _area = area;
        }

        #endregion
        #region Properties

        public bool PrinterValid
        {
            get { return _area.PrinterValid; }
        }

        public bool DisplayValid
        {
            get { return _area.DisplayValid; }
        }

        public bool MarchPending
        {
            get { return _tickets.Any(x => x.MarchPending); }
        }

        public bool VoidPending
        {
            get { return _tickets.Any(x => x.VoidPending); }
        }

        public bool AwayPending
        {
            get { return _tickets.Any(x => x.AwayPending); }
        }

        public IEnumerable<TicketKitchen> Tickets
        {
            get { return _tickets.Where(x => x.MarchPending || x.VoidPending || x.AwayPending); }
        }

        #endregion
        #region Methods

        private TicketKitchen GetTicketKitchen(Ticket ticket, Order order)
        {
            var ticketKitchen = _tickets.FirstOrDefault(x => x.Id == ticket.Id);
            if (ticketKitchen == null)
            {
                ticketKitchen = new TicketKitchen(_area, ticket);
                _tickets.Add(ticketKitchen);
            }
            else
                ticketKitchen.Ticket = ticket;

            return ticketKitchen;
        }

        public void AddOrder(Ticket ticket, Order order)
        {
            var ticketKitchen = GetTicketKitchen(ticket, order);

            ticketKitchen.AddOrder(new OrderKitchen(order));
            _orders.Add(order);
        }

        public void VoidOrder(Ticket ticket, Order order)
        {
            var ticketKitchen = GetTicketKitchen(ticket, order);

            _orders.Remove(_orders.FirstOrDefault(x => x.Id == order.Id));
            ticketKitchen.VoidOrder(new OrderKitchen(order));
        }

        public void AwayOrder(Ticket ticket, Order order)
        {
            var ticketKitchen = GetTicketKitchen(ticket, order);

            ticketKitchen.AwayOrder(new OrderKitchen(order));
            _orders.Add(order);
        }

        public ValidationResult SendToPrinter()
        {
            var result = new ValidationResult();
            foreach (var ticket in Tickets)
            {
                result.Add(ticket.SendToPrinter());
                if (!result.IsEmpty)
                    break;
            }

            return result;
        }

        public override string ToString()
        {
            return $"{_area.Description} ({_area.PrinterDescription})"; 
        }

        #endregion
    }
}