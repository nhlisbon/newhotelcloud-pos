﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Diagnostics;
using NewHotel.Pos.IoC;
using NewHotel.Pos.PrinterDocument.Interfaces;
using NewHotel.Pos.PrinterDocument.Simple.Visual;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.MVVM;
using NewHotel.WPF.Model.Extentions;
using NewHotel.Pos.Localization;
using NewHotel.Contracts;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Kitchen
{
    public class TicketKitchen : BindableBase
    {
        #region Variables

        private readonly List<OrderKitchen> _newOrders = new List<OrderKitchen>();
        private readonly List<OrderKitchen> _voidOrders = new List<OrderKitchen>();
        private readonly List<OrderKitchen> _awayOrders = new List<OrderKitchen>();

        #endregion
        #region Constructors

        public TicketKitchen(Area area, Ticket ticket)
        {
            Ticket = ticket;
            Area = area;
            Orders = new ObservableCollection<OrderKitchen>();
            FailedOrders = new List<OrderKitchen>();
        }

        #endregion
        #region Properties

        public Guid Id => Ticket.Id;
        public ObservableCollection<OrderKitchen> Orders { get; private set; }
        public Ticket Ticket { get; set; }
        public Area Area { get; private set; }
        public ITicketPrinter DocPrinter => CwFactory.Resolve<ITicketPrinter>();
        public List<OrderKitchen> FailedOrders { get; private set; }

        public IEnumerable<OrderKitchen> NewOrders
        {
            get { return _newOrders; }
        }

        public IEnumerable<OrderKitchen> VoidOrders
        {
            get { return _voidOrders; }
        }

        public IEnumerable<OrderKitchen> AwayOrders
        {
            get { return _awayOrders; }
        }

        #endregion
        #region Methods

        public void AddOrder(OrderKitchen order)
        {
            Trace.TraceInformation($"Order append Id:{order.Id} Qty:{order.Order.Quantity} Product:{order.Order.ProductDescription}");
            if (!Orders.Any(o => o.Id == order.Id))
            {
                Orders.Add(order);
                if (Area != null)
                {
                    _newOrders.Add(order);
                    Trace.TraceInformation($"Order appended Id:{order.Id} Qty:{order.Order.Quantity} Product:{order.Order.ProductDescription}");
                }
                else
                    Trace.TraceInformation($"Undefined area on add order");
            }
        }

        public void VoidOrder(OrderKitchen order)
        {
            Trace.TraceInformation($"Order void Id:{order.Id} Qty:{order.Order.Quantity} Product:{order.Order.ProductDescription}");
            if (!Orders.Any(o => o.Id == order.Id))
            {
                order.Canceled = true;
                Orders.Add(order);
                if (Area != null)
                {
                    _voidOrders.Add(order);
                    Trace.TraceInformation($"Order voided Id:{order.Id} Qty:{order.Order.Quantity} Product:{order.Order.ProductDescription}");
                }
                else
                    Trace.TraceInformation($"Undefined area on void order");
            }
        }

        public void AwayOrder(OrderKitchen order)
        {
            Trace.TraceInformation($"Order append Id:{order.Id} Qty:{order.Order.Quantity} Product:{order.Order.ProductDescription}");
            if (!Orders.Any(o => o.Id == order.Id))
            {
                Orders.Add(order);
                if (Area != null)
                {
                    _awayOrders.Add(order);
                    Trace.TraceInformation($"Order appended Id:{order.Id} Qty:{order.Order.Quantity} Product:{order.Order.ProductDescription}");
                }
                else
                    Trace.TraceInformation($"Undefined area on add order");
            }
        }

        public bool MarchPending
        {
            get { return _newOrders.Count > 0; }
        }

        public bool VoidPending
        {
            get { return _voidOrders.Count > 0; }
        }

        public bool AwayPending
        {
            get { return _awayOrders.Count > 0; }
        }

        public ValidationResult SendToPrinter()
        {
            var result = new ValidationResult();

            if (Area != null && Area.UsePrinter)
            {
                result.Add(SendAndClear("ORDER", _newOrders));
                result.Add(SendAndClear("VOID", _voidOrders));
                result.Add(SendAndClear("AWAY", _awayOrders));
            }

            return result;
        }

        private KitchenDoc CreateKitchenDoc(string action, Dictionary<Guid, HashSet<Guid>> orderIds, TicketProductGrouping productGrouping)
        {
            var kitchenDoc = new KitchenDoc
            {
                Ticket = Ticket,
                ActionDescription = action.Translate().ToUpper(),
                AreaDescription = Area.Description,
                AreaId = Area.Id,
                OrderIds = orderIds,
                ProductGrouping = productGrouping
            };

            if (string.IsNullOrEmpty(kitchenDoc.Ticket.SeriePrex))
                Trace.TraceInformation($"Kitchen doc Num:{kitchenDoc.Ticket.OpeningNumber} Ticket:{kitchenDoc.Ticket.Serie ?? 0}");
            else
                Trace.TraceInformation($"Kitchen doc Num:{kitchenDoc.Ticket.OpeningNumber} Ticket:{kitchenDoc.Ticket.Serie ?? 0}/{kitchenDoc.Ticket.SeriePrex}");
            foreach (var productLine in kitchenDoc.Ticket.ProductLines)
                Trace.TraceInformation($"ProductLine Id:{productLine.Id}");
            foreach (var orderId in kitchenDoc.OrderIds)
                Trace.TraceInformation($"Order Id:{orderId}");

            return kitchenDoc;
        }

        private ValidationResult SendAndClear(string action, IList<OrderKitchen> orders)
        {
            var result = new ValidationResult();

            if (orders.Count > 0)
            {
                var ordersIds = new Dictionary<Guid, HashSet<Guid>>();
                foreach ( var order in orders)
                {
                    ordersIds.Add(order.Id, new HashSet<Guid>(order.Order.TableLines.Select(x => x.Id)));
                }

                var doc = CreateKitchenDoc(action, ordersIds, Hotel.Instance.CurrentStandCashierContext.Stand.ProductGrouping);
                orders.Clear();

                if (Area != null)
                {
                    if (string.IsNullOrEmpty(doc.Ticket.SeriePrex))
                        Trace.TraceInformation($"Sending orders to area printer Num:{doc.Ticket.OpeningNumber} Ticket:{doc.Ticket.Serie ?? 0}");
                    else
                        Trace.TraceInformation($"Sending orders to area printer Num:{doc.Ticket.OpeningNumber} Ticket:{doc.Ticket.Serie ?? 0}/{doc.Ticket.SeriePrex}");
                    var printer = Area.GetPrinter();
                    for (int i = 0; i < Area.KitchenCopies; i++)
                        result.AddValidations(DocPrinter.PrintKitchenDoc(printer, doc));
                }
                else
                {
                    if (string.IsNullOrEmpty(doc.Ticket.SeriePrex))
                        Trace.TraceInformation($"Area undefined Num:{doc.Ticket.OpeningNumber} Ticket:{doc.Ticket.Serie ?? 0}");
                    else
                        Trace.TraceInformation($"Area undefined Num:{doc.Ticket.OpeningNumber} Ticket:{doc.Ticket.Serie ?? 0}/{doc.Ticket.SeriePrex}");
                }

                if (Hotel.Instance.CurrentStandCashierContext.Stand.CopyKitchenInTicketPrinter)
                {
                    if (string.IsNullOrEmpty(doc.Ticket.SeriePrex))
                        Trace.TraceInformation($"Sending orders copy to ticket printer Num:{doc.Ticket.OpeningNumber} Ticket:{doc.Ticket.Serie ?? 0}");
                    else
                        Trace.TraceInformation($"Sending orders copy to ticket printer Num:{doc.Ticket.OpeningNumber} Ticket:{doc.Ticket.Serie ?? 0}/{doc.Ticket.SeriePrex}");
                    var printerSettings = Hotel.Instance.CurrentStandCashierContext.Stand.PrintersConfiguration;
                    var printer = printerSettings.GetPrinter();
                    result.AddValidations(DocPrinter.PrintKitchenDoc(printer, doc));
                }
                else
                {
                    if (string.IsNullOrEmpty(doc.Ticket.SeriePrex))
                        Trace.TraceInformation($"Send copy to ticket printer disabled Num:{doc.Ticket.OpeningNumber} Ticket:{doc.Ticket.Serie ?? 0}");
                    else
                        Trace.TraceInformation($"Send copy to ticket printer disabled Num:{doc.Ticket.OpeningNumber} Ticket:{doc.Ticket.Serie ?? 0}/{doc.Ticket.SeriePrex}");
                }
            }

            return result;
        }

        #endregion
    }
}