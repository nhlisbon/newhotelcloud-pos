﻿using System;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Model.Model;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.App.Kitchen
{
    public class OrderKitchen : BindableBase
    {
        #region Variables
       
        private bool _canceled;

        #endregion
        #region Constructor

        public OrderKitchen(Order order)
        {
            Order = order;
        }

        #endregion
        #region Events

        public event Action<OrderKitchen> OrderCanceled;

        #endregion
        #region Properties

        public Guid Id => Order.Id;

        public bool Canceled
        {
            get => _canceled;
            set
            {
                SetProperty(ref _canceled, value);
                OrderCanceled?.Invoke(this);
            }
        }

        public Order Order { get; set; }

        #endregion
    }
}
