﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Markup;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Hub.Contracts.Cashier.Records;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.IoC;
using NewHotel.Pos.PrinterDocument.Fiscal;
using NewHotel.Pos.PrinterDocument.Fiscal.CuracaoPanama.Curacao;
using NewHotel.Pos.PrinterDocument.Fiscal.CuracaoPanama.Panama;
using NewHotel.WPF.Model;
using NewHotel.WPF.Model.Interface;
using NewHotel.WPF.Model.Model;
using NewHotel.WPF.Model.Model.Taxes;
using NewHotel.WPF.Common;
using NewHotel.WPF.MVVM;
using NewHotel.Contracts;

namespace NewHotel.WPF.App.Pos.Model
{
    public class Hotel : BindableBase, IPosSettings
    {
        #region Members

        private ObservableCollection<TaxSchema> _taxSchemas;
        private ObservableCollection<CurrencyCashier> _currencysCashier;
        private ObservableCollection<NationalityRecord> _nationalities;
        private StandCashierContext _currentStandCashierContext;
        private User _currentUserModel;
        private ObservableCollection<CreditCardType> _creditCardTypes;
        private Guid _userId;
        private Guid _id;
        private readonly List<User> _previousLoguedUserSet;
        private AlarmsManager _alarmsManager = new AlarmsManager();
        private string _description;
        private string _imagePath;
        private bool _taxIncluded;
        private bool _leaveMealPlanOpened;
        private decimal _addedValueOnHouseUse;
        private bool _fiscalNumberOnTickets;
        private bool _allowFastPayment;
        private Guid? _fastPaymentType;
        private Guid? _defaultTaxSchema;
        private Guid? _defaultMergeTicketCancellationReason;
        private decimal? _tipPercent;
        private bool _useNumericLogin;
        private bool _workDateOverSystemDate;
        private FiscalPOSType _fiscalPrinter;
        private ObservableCollection<ITicket> _tickets;
        private bool _isEmptyCategoriesVisible;
        private long? _accountFolder;
        private decimal _addedValueOnDiscount;
        private readonly IDictionary<Guid, Table> _fastTableLocation = new ConcurrentDictionary<Guid, Table>();

        private static Hotel _instance = new Hotel();

        #endregion
        #region Properties

        public static LanguageCollection Laguages { get; set; }

        public static CultureInfo DefaultCultureInfo { get; set; } = CultureInfo.GetCultureInfo(1033);

        public static Hotel Instance => _instance;

        public bool IsHtml5 { get; set; }

        public IEnumerable<User> PreviousLoguedUsers => _previousLoguedUserSet;

        public ObservableCollection<NationalityRecord> Nationalities
        {
            get { return _nationalities; }
            set
            {
                SetProperty(ref _nationalities, value);
            }
        }

        // public CurrencyCashier BaseCurrency => Currencys.FirstOrDefault(x => x.IsBase);
        public CurrencyCashier BaseCurrency => Currencys.FirstOrDefault(x => x.IsHotelBase);
		public CurrencyCashier StandBaseCurrency => Currencys.FirstOrDefault(x => x.IsBase);

		public ObservableCollection<CurrencyCashier> Currencys
        {
            get => _currencysCashier;
            set => SetProperty(ref _currencysCashier, value);
        }

        public ObservableCollection<TaxSchema> TaxSchemas
        {
            get => _taxSchemas;
            set => SetProperty(ref _taxSchemas, value);
        }

        public TaxSchema DefaultTaxSchemaModel => TaxSchemas.First(x => x.Default);
        public bool MultTaxSchema => TaxSchemas.Count > 1;

        public Guid Id
        {
            get
            {
                if (NewHotelContext.Current != null)
                    return NewHotelContext.Current.Data.InstallationId;
                else return _id;
            }
            set
            {
                _id = value;
                if (NewHotelContext.Current != null)
                {
                    if (NewHotelContext.Current.Data.InstallationId != _id)
                    {
                        var contextData = NewHotelContext.Current.Data;
                        contextData.InstallationId = _id;
                        NewHotelContext.Current.SetContextData(contextData);
                    }
                }
            }
        }

        public Guid UserId
        {
            get
            {
                if (NewHotelContext.Current != null)
                    return NewHotelContext.Current.Data.UserId;
                return _userId;
            }
            set
            {
                _userId = value;
                if (NewHotelContext.Current != null)
                {
                    if (NewHotelContext.Current.Data.UserId != _userId)
                    {
                        var contextData = NewHotelContext.Current.Data;
                        contextData.UserId = _userId;
                        NewHotelContext.Current.SetContextData(contextData);
                    }
                }
            }
        }

        public StandCashierContext CurrentStandCashierContext
        {
            get => _currentStandCashierContext;
            set
            {
                SetProperty(ref _currentStandCashierContext, value);
                MgrServices.SetConfigurationSetting("LastStand", value.Stand.Id.ToString());
                MgrServices.SetConfigurationSetting("LastCashier", value.Cashier.Id.ToString());
            }
        }

        public User CurrentUserModel
        {
            get => _currentUserModel;
            set => SetProperty(ref _currentUserModel, value);
        }

        public string Country { get; set; }

        public string FiscalNumber { get; set; }

        public bool AllowBallotDocument { get; set; }

        public bool AllowFastTicket
        {
            get
            {
                switch (SignType)
                {
                    case DocumentSign.FiscalizationPortugal:
                    case DocumentSign.FiscalizationAngola:
                    case DocumentSign.FiscalizationMalta:
                        return false;
                    case DocumentSign.None:
                    case DocumentSign.FiscalPrinter:
                    case DocumentSign.FiscalizationBrazilNFE:
                    case DocumentSign.FiscalizationCroatia:
                    case DocumentSign.FiscalizationChile:
                    case DocumentSign.FiscalizationBrazilCMFLEX:
                    case DocumentSign.FiscalizationPeruDFacture:
                    case DocumentSign.FiscalizationEcuador:
                    case DocumentSign.FiscalizationColombiaBtw:
                    case DocumentSign.FiscalizationBrazilProsyst:
                    case DocumentSign.FiscalizationCuracao:
                    case DocumentSign.FiscalizationMexicoCfdi:
                    case DocumentSign.FiscalizationUruguayExFactura:
                    case DocumentSign.FiscalizationPanamaHKA:
                    default:
                        return true;
                }
            }
        }

        public bool AllowFastInvoice
        {
            get
            {
                return SignType switch
                {
                    DocumentSign.None => !EmitOnlyTickets,
                    DocumentSign.FiscalizationBrazilCMFLEX => false,
                    _ => true
                };
            }
        }

        public bool AllowFastBallot => AllowBallotDocument;

        public bool AllowFastPlan { get; private set; }

        public bool IsMandatoryRoomInfo { get; private set; }

        public IEnumerable<Table> Tables => _fastTableLocation.Values;

        public ObservableCollection<CancellationReason> CancellationReasons { get; set; }

        public ObservableCollection<DiscountType> DiscountTypes { get; set; }

        public ObservableCollection<Separator> Separators { get; set; }

        public DiscountType NotDiscountType { get; set; }

        public ObservableCollection<Duty> Duties { get; set; }

        public ObservableCollection<CreditCardType> CreditCardTypes
        {
            get { return _creditCardTypes; }
            set { SetProperty(ref _creditCardTypes, value); }
        }

        public ObservableCollection<Stand> Stands { get; set; }

        public ObservableCollection<Cashier>? Cashiers { get; set; }

        public ObservableCollection<ITicket> Tickets
        {
            get { return _tickets; }
            private set { SetProperty(ref _tickets, value); }
        }

        public string Description
        {
            get { return _description; }
            set { SetProperty(ref _description, value); }
        }

        public string ImagePath
        {
            get { return _imagePath; }
            set { SetProperty(ref _imagePath, value); }
        }

        public bool TaxIncluded
        {
            get { return _taxIncluded; }
            set { SetProperty(ref _taxIncluded, value); }
        }

        public bool LeaveMealPlanOpened
        {
            get => _leaveMealPlanOpened;
            private set => SetProperty(ref _leaveMealPlanOpened, value);
        }

        public decimal AddedValueOnHouseUse
        {
            get { return _addedValueOnHouseUse; }
            set { SetProperty(ref _addedValueOnHouseUse, value); }
        }

        public decimal AddedValueOnDiscount
        {
            get { return _addedValueOnDiscount; }
            set { SetProperty(ref _addedValueOnDiscount, value); }
        }

        public bool FiscalNumberOnTickets
        {
            get { return _fiscalNumberOnTickets; }
            set { SetProperty(ref _fiscalNumberOnTickets, value); }
        }

        public bool AllowFastPayment
        {
            get => _allowFastPayment;
            private set => SetProperty(ref _allowFastPayment, value);
        }

        public Guid? FastPaymentType
        {
            get { return _fastPaymentType; }
            set { SetProperty(ref _fastPaymentType, value); }
        }

        public Guid? DefaultTaxSchema
        {
            get => _defaultTaxSchema;
            set => SetProperty(ref _defaultTaxSchema, value);
        }

        public Guid? DefaultMergeTicketCancellationReason
        {
            get { return _defaultMergeTicketCancellationReason; }
            set { SetProperty(ref _defaultMergeTicketCancellationReason, value); }
        }

        public decimal? TipPercent
        {
            get { return _tipPercent; }
            set { SetProperty(ref _tipPercent, value); }
        }

        public long? AccountFolder
        {
            get { return _accountFolder; }
            set { SetProperty(ref _accountFolder, value); }
        }

        public bool UseNumericLogin
        {
            get { return _useNumericLogin; }
            set { SetProperty(ref _useNumericLogin, value); }
        }

        public bool WorkDateOverSystemDate
        {
            get { return _workDateOverSystemDate; }
            set { SetProperty(ref _workDateOverSystemDate, value); }
        }

        public FiscalPOSType FiscalPrinter
        {
            get { return _fiscalPrinter; }
            set { SetProperty(ref _fiscalPrinter, value); }
        }

        public bool HasFiscalPrinter => FiscalPrinter != FiscalPOSType.None;

        public bool MultCurrency => Currencys.Count > 1;

        public int? CribNumberHotel { get; set; }

        public static CultureInfo Culture { get; set; }

        public static XmlLanguage XmlLanguage { get; set; }

        public CultureInfo CultureProperty;

        public DocumentSign SignType { get; set; }

        public float AutoLogOutTimerValue { get; set; }

        public bool UsingAutoLogOutTimer { get; set; }

        public bool IsEmptyCategoriesVisible
        {
            get { return _isEmptyCategoriesVisible; }
            set { SetProperty(ref _isEmptyCategoriesVisible, value); }
        }

        public bool ApplyAutomaticProductDiscount { get; set; }
        public decimal? MandatoryValueForFiscalNumber { get; set; }

        public short? DaysToCancelInvoice { get; set; }
        public bool CancelInvoicesOnlyEmissionMonth { get; set; }
        public TimeSpan? TicketCancelWindow { get; set; }

        public long LanguageId => NewHotelContext.Current.Data.LanguageId;

        public bool HasTicketPrintSettings { get; set; }
        public bool HasBallotPrintSettings { get; set; }
        public bool HasInvoicePrintSettings { get; set; }
        public bool HasReceiptPrintSettings { get; set; }
        public bool EmitOnlyTickets { get; set; }

        public bool ShowQuantityDecimalCase { get; set; }
        public bool ShowInvoiceButton { get; set; }
        public bool AllowDigitalSign { get; set; }
        public bool AllowSelectSeparators { get; set; }

        public bool SkipFillFiscalDataAndPutFinalConsumer { get; set; }
        public bool AutoTableAfterAllToKitchen { get; set; }
        public bool AllowCreditNoteTicketReversal { get; set; }

        
        #endregion
        
        #region Events

        public static event Action<bool> HasCommunicationChanged;

        #endregion
        #region Constructor

        private Hotel()
        {
            Stands = new ObservableCollection<Stand>();
            Tickets = new ObservableCollection<ITicket>();

            var configSetting = MgrServices.GetConfigurationSetting("UseNumericLogin");
            bool useNumericLogin;
            if (!string.IsNullOrEmpty(configSetting) && bool.TryParse(configSetting, out useNumericLogin))
                UseNumericLogin = useNumericLogin;

            _previousLoguedUserSet = new List<User>();
        }

        #endregion
        #region Methods

        public void RefreshEnvironment(StandEnvironment environment)
        {
            #region Hotel

            Country = environment.Hotel.Nationality;
            Description = environment.Hotel.Description;
            FiscalNumber = environment.Hotel.FiscalNumber;

            #endregion
            #region Duties

            Duties = (environment.TicketDuties == null) ? new ObservableCollection<Duty>() :
                new ObservableCollection<Duty>(environment.TicketDuties.Select(s => new Duty
                {
                    Id = s.Id,
                    Delay = s.TimeDelay,
                    Description = s.Description,
                    FinalDate = s.FinalTime,
                    InitialDate = s.InitialTime
                }));

            #endregion
            #region CreditCardTypes

            CreditCardTypes = (environment.CreditCardTypes == null) ? new ObservableCollection<CreditCardType>() :
                new ObservableCollection<CreditCardType>(environment.CreditCardTypes.Select(s => new CreditCardType
                {
                    Description = s.Description,
                    Id = s.Id,
                    CreditCard = s.CreditCard,
                    DebitCard = s.DebitCard

                }));

            #endregion
            #region CancellationReasons

            CancellationReasons = (environment.CancellationReasons == null) ? new ObservableCollection<CancellationReason>() :
                new ObservableCollection<CancellationReason>(environment.CancellationReasons.Select(s => new CancellationReason
                {
                    Description = s.Description,
                    Id = s.Id
                }));

            #endregion
            #region DiscountTypes

            DiscountTypes = (environment.DiscountTypes == null) ? new ObservableCollection<DiscountType>() :
                new ObservableCollection<DiscountType>(environment.DiscountTypes.Select(dt => new DiscountType
                {
                    Id = dt.Id,
                    Description = dt.Description,
                    Fixed = dt.FixedDiscount,
                    MaxValue = dt.MaxPercent,
                    MinValue = dt.MinPercent
                }));

            DiscountTypes.Insert(0, new DiscountType
            {
                Description = "[No discounts]",
                MinValue = 0,
                MaxValue = 0,
                Fixed = true
            });

            NotDiscountType = DiscountTypes.First();

            #endregion
            #region Separators

            Separators = (environment.Separators == null) ? new ObservableCollection<Separator>() :
                new ObservableCollection<Separator>(environment.Separators.Select(s => new Separator
                {
                    Id = s.Id,
                    Description = s.Description,
                    ImpresionOrden = s.Order,
                    PaxMax = s.MaxCoversAllowed,
                    AcumRound = s.CumulativeRound
                }));

            #endregion
            #region Nationalitys

            Nationalities = new ObservableCollection<NationalityRecord>(environment.Nationalities);

            #endregion
            #region Currencies

            var currencies = environment.Currencys ?? new CurrencyCashierRecord[0];
            Currencys = new ObservableCollection<CurrencyCashier>(currencies.Select(c => new CurrencyCashier(c)));

            #endregion
            #region TaxSchemas

            var taxSchemas = environment.TaxSchemas ?? new NewHotel.Pos.Hub.Contracts.Cashier.Records.TaxSchemaRecord[0];
            TaxSchemas = new ObservableCollection<TaxSchema>(taxSchemas.Select(t => new TaxSchema(t)));

            #endregion
            #region PrintSettings

            HasTicketPrintSettings = environment.TicketPrintSettings != null;
            HasBallotPrintSettings = environment.BallotPrintSettings != null;
            HasInvoicePrintSettings = environment.InvoicePrintSettings != null;
            HasReceiptPrintSettings = environment.ReceiptPrintSettings != null;
            
            #endregion
            #region HTML 5

            IsHtml5 = environment.IsHtml5;

            #endregion
        }

        public bool AllowCancelTicket(DateTime emissionDate, bool isInvoiced)
        {
            if (isInvoiced)
            {
                if (Instance.AllowCreditNoteTicketReversal)
                    return false;

                switch (SignType)
                {
                    case DocumentSign.FiscalizationEcuador:
                    case DocumentSign.FiscalizationColombiaBtw:
                    case DocumentSign.FiscalizationPortugal:
                        return false;
                    default:
                        if(TicketCancelWindow.HasValue)
                            return emissionDate.Date.Add(TicketCancelWindow.Value) >= CurrentStandCashierContext.Stand.StandWorkDate;
                        else 
                            break;
                }
            }

            if (TicketCancelWindow.HasValue)
                return emissionDate.Date.Add(TicketCancelWindow.Value) >= CurrentStandCashierContext.Stand.StandWorkDate;

            return true;
        }

        /// <summary>
        /// Gets the general settings from the cloud or from the local database.
        /// It gets it's values already converted to the correct types, from the database.
        /// </summary>
        /// <param name="fromCloud">If it's from the cloud or not</param>
        public async Task GetGeneralSettingsAsync(bool fromCloud = false)
        {
            var result = CheckCommunication();

            if (string.IsNullOrEmpty(result))
            {
                var generalSettings = await BusinessProxyRepository.Settings.GetGeneralSettingsAsync(fromCloud);

                SignType = generalSettings.SignType;
                DefaultTaxSchema = generalSettings.DefaultTaxSchemaId;
                AccountFolder = (long)generalSettings.AccountFolder;
                AllowFastPayment = generalSettings.FastPayment;
                DefaultMergeTicketCancellationReason = generalSettings.DefaultCancellationReasonId;
                FastPaymentType = generalSettings.FastPaymentFormId;
                ImagePath = generalSettings.ImagePath;
                TaxIncluded = generalSettings.TaxesIncluded;
                LeaveMealPlanOpened = generalSettings.LeaveMealPlanOpened;
                AddedValueOnHouseUse = generalSettings.AddedValueOnHouseUse;
                AddedValueOnDiscount = generalSettings.AddedValueOnDiscount;
                FiscalNumberOnTickets = generalSettings.FiscalNumberOnTickets;
                WorkDateOverSystemDate = generalSettings.WorkDateOverSystemDate;
                FiscalPrinter = generalSettings.FiscalType;
                CribNumberHotel = generalSettings.CribNumberHotel;
                AllowBallotDocument = generalSettings.AllowBallotDocument;
                ApplyAutomaticProductDiscount = generalSettings.ApplyAutomaticProductDiscount;
                MandatoryValueForFiscalNumber = generalSettings.MandatoryValueForFiscalNumber;
                DaysToCancelInvoice = generalSettings.CancelInvoicesOnlyEmissionDate ? 0 : generalSettings.DaysToCancelInvoice;
                TicketCancelWindow = generalSettings.TicketCancelWindow;
                CancelInvoicesOnlyEmissionMonth = generalSettings.CancelInvoicesOnlyEmissionMonth;
                AllowFastPlan = generalSettings.FastPlan;
                IsMandatoryRoomInfo = generalSettings.IsMandatoryFastPlanRoomInfo;
                EmitOnlyTickets = generalSettings.EmitOnlyTickets;
                UsingAutoLogOutTimer = generalSettings.UsingAutoLogOutTimer;
                AutoLogOutTimerValue = generalSettings.AutoLogOutTimerValue;
                ShowQuantityDecimalCase = generalSettings.ShowQuantityDecimalCase;
                ShowInvoiceButton = generalSettings.ShowInvoiceButton;
                AllowDigitalSign = generalSettings.AllowDigitalSign;
                AllowSelectSeparators = generalSettings.AllowSelectSeparators;
                SkipFillFiscalDataAndPutFinalConsumer = generalSettings.SkipFillFiscalDataAndPutFinalConsumer;
                AutoTableAfterAllToKitchen = generalSettings.AutoTableAfterAllToKitchen;
                AllowCreditNoteTicketReversal = generalSettings.AllowCreditNoteTicketReversal;
                InitializeFiscalPrinterDependencies(FiscalPrinter);
            }
        }

        public ObservableCollection<Duty> GetDuties(DateTime time)
        {
            return GetDuties(time, time);
        }

        public ObservableCollection<Duty> GetDuties(DateTime ini, DateTime fin)
        {
            if (Duties == null)
                return null;

            return new ObservableCollection<Duty>(from duty in Duties
                                                  where
                                                  //           | <  > | 
                                                  ((duty as Duty).InitialDate.TimeOfDay >= ini.TimeOfDay && (duty as Duty).FinalDate.TimeOfDay <= ini.TimeOfDay) ||
                                                  //       <          |   >
                                                  ((duty as Duty).InitialDate.TimeOfDay <= fin.TimeOfDay && (duty as Duty).FinalDate.TimeOfDay >= fin.TimeOfDay) ||
                                                  //       <   |          >
                                                  ((duty as Duty).InitialDate.TimeOfDay <= ini.TimeOfDay && (duty as Duty).FinalDate.TimeOfDay >= ini.TimeOfDay)
                                                  select duty as Duty);
        }

        #region Preparations

        public Dictionary<Guid, List<Preparation>> GroupsPreparations { get; set; }
        public Dictionary<Guid, List<Preparation>> FamiliesPreparations { get; set; }

        #endregion

        public static LanguageView SelectedLanguage
        {
            get { return Laguages.SelectedLanguage; }
            set { Laguages.SelectedLanguage = value; }
        }

        public void RefreshEmptyCategoriesVisibility()
        {
            if (IsEmptyCategoriesVisible)
                // Mostrar todas las categorias
                CurrentStandCashierContext.Stand.ShowEmptyCategories();
            else
                // Mostrar solo categorias con productos
                CurrentStandCashierContext.Stand.HideEmptyCategories();
        }

        public enum Margin { Left, Right, Top, Bottom }

        public int GetMarginPrinter(string description, Margin margin)
        {
            try
            {
                return int.Parse(MgrServices.GetConfigurationSetting(description + $"_{margin.ToString()}Margin"));
            }
            catch
            {
                return 0;
            }
        }

        public int GetLineFontSize(string description)
        {
            try
            {
                return int.Parse(MgrServices.GetConfigurationSetting(description + $"_LineFontSize"));
            }
            catch
            {
                return 14;
            }
        }

        public static int GetNoteFontSize(string description)
        {
            try
            {
                return int.Parse(MgrServices.GetConfigurationSetting(description + "_NoteFontSize"));
            }
            catch
            {
                return 9;
            }
        }
        
        public static int GetActionFontSize(string description)
        {
            try
            {
                return int.Parse(MgrServices.GetConfigurationSetting(description + "_ActionFontSize"));
            }
            catch
            {
                return 20;
            }
        }
        
        public static int GetSaloonFontSize(string description)
        {
            try
            {
                return int.Parse(MgrServices.GetConfigurationSetting(description + "_SaloonFontSize"));
            }
            catch
            {
                return 14;
            }
        }

        public Table GetTable(Guid? id)
        {
            if (id.HasValue && _fastTableLocation.TryGetValue(id.Value, out var table))
                return table;

            return null;
        }

        public void AddTableToFastLocation(Table table)
        {
            if (_fastTableLocation.ContainsKey(table.Id))
                _fastTableLocation[table.Id] = table;
            else
                _fastTableLocation.Add(table.Id, table);
        }

        /// <summary>
        /// Agrega el ticket a su mesa, (si existe) y a la lista de tickets del hotel.
        /// </summary>
        /// <param name="ticket">Ticket que se desea agregar</param>
        public void AddTicket(Ticket ticket)
        {
            // Si tenemos mesa donde ponerlo lo agregamos
            if (ticket.Table != null)
            {
                // Tenemos mesa donde ponerlo
                // Definamos si es para rellenar o para agregar
                var oldTicket = ticket.Table.Bills.FirstOrDefault(x => x.Id == ticket.Id);
                if (oldTicket != null)
                {
                    // El ticket ya existe en esta mesa
                    if (!oldTicket.IsFull && ticket.IsFull)
                    {
                        // El anterior es una imagen y el nuevo esta completo, debemos hacer el cambio
                        FillTicket(ticket);
                    }
                }
                else
                {
                    // Simplemente agregamos el ticket
                    if (!ticket.IsClosed)
                        ticket.Table.AddTicket(ticket);
                }

                Tickets.Add(ticket);
            }
        }

        public void UpdateTicketsEditableProperty(Guid stand, Guid cashier)
        {
            foreach (var ticket in Tickets)
                ticket.IsEditable = ticket.IsFull && ticket.Stand == stand && ticket.Cashier == cashier;

            foreach (var table in _fastTableLocation.Values)
                table.UpdateExternalUse();
        }

        public void FillTicket(Ticket ticket)
        {
            var oldTicket = (Ticket)Tickets.FirstOrDefault(x => x.Id == ticket.Id);

            if (oldTicket != null)
            {
                if (oldTicket.Table != null)
                    oldTicket.Table.RemoveTicket(oldTicket);

                if (ticket.Table != null)
                    ticket.Table.AddTicket(ticket);

                Tickets.Remove(oldTicket);
                Tickets.Add(ticket);
            }
            else
                AddTicket(ticket);
        }

        public void RemoveTicket(ITicket ticket)
        {
            GetTable(ticket.TableId)?.RemoveTicket(ticket);
        }

        public void RefreshStandTransfersNotifications(Stand standModel, Guid cashierId)
        {
            standModel.VerifyTransferToAccept();
            standModel.VerifyTransferReturns(cashierId);
        }

        public static async Task<ValidationItemResult<User>> GetUserByCodeAsync(string userCode)
        {
            var result = new ValidationItemResult<User>
            {
                Item = Instance?.PreviousLoguedUsers.FirstOrDefault(x => x.Code == userCode)
            };

            if (result.Item == null)
            {
                var validationResult = await BusinessProxyRepository.Stand.GetUserByCodeAsync(userCode);
                result.AddValidations(validationResult);
                if (validationResult.Item != null)
                    result.Item = new User(validationResult.Item);
            }
            return result;
        }

        public static async Task<ValidationItemResult<User>> GetUserByNameAsync(string loginUser)
        {
            var result = new ValidationItemResult<User>
            {
                Item = Instance?.PreviousLoguedUsers.FirstOrDefault(x => x.Username == loginUser)
            };

            if (result.Item == null)
            {
                var validationResult = await BusinessProxyRepository.Stand.GetUserByNameAsync(loginUser);
                result.AddValidations(validationResult);
                if (validationResult.Item != null)
                    result.Item = new User(validationResult.Item);
            }

            return result;
        }

        public void AddLoguedUser(User user)
        {
            if (PreviousLoguedUsers.All(x => x.Id != user.Id))
                _previousLoguedUserSet.Add(user);
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);

            switch (propertyName)
            {
                case nameof(IsEmptyCategoriesVisible):
                    {
                        if (IsEmptyCategoriesVisible) // Mostrar todas las categorias
                            CurrentStandCashierContext.Stand.ShowEmptyCategories();
                        else // Mostrar solo categorias con productos
                            CurrentStandCashierContext.Stand.HideEmptyCategories();
                        break;
                    }
                case nameof(UseNumericLogin):
                    {
                        MgrServices.SetConfigurationSetting(nameof(UseNumericLogin), UseNumericLogin.ToString().ToLower());
                        break;
                    }
            }
        }

        public static string CheckCommunication()
        {
            try
            {
                if (HasCommunicationChanged != null)
                {
                    var hasCommunication = BusinessProxyRepository.Cashier.CheckCommunication();
                    UIThread.Invoke(() => HasCommunicationChanged(hasCommunication));
                }

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        private void InitializeFiscalPrinterDependencies(FiscalPOSType fiscalType)
        {
            switch (fiscalType)
            {
                case FiscalPOSType.None:
                    break;
                case FiscalPOSType.BrazilBematech:
                    break;
                case FiscalPOSType.CuracaoBixolon:
                    CwFactory.Register<IFiscalPrinter, FiscalPrinterCuracao>();
                    CwFactory.Register<IFiscalPrinterCuracao, FiscalPrinterCuracao>();
                    break;
                case FiscalPOSType.PanamaTally:
                    CwFactory.Register<IFiscalPrinter, FiscalPrinterPanama>();
                    CwFactory.Register<IFiscalPrinterPanama, FiscalPrinterPanama>();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        #endregion
    }
}