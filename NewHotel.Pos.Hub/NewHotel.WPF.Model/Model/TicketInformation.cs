﻿using NewHotel.WPF.MVVM;
using System;
using NewHotel.Pos.PrinterDocument.Interfaces;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Model
{
	public class TicketInformation : BindableBase, ITicketInformation
	{
		private bool IsEditableProperty;
		private decimal TotalAmountProperty;

		public event Action<ITicketInformation, decimal> TotalAmountChanged;

		public Guid Id { get; set; }

		public decimal TotalAmount
		{
			get { return TotalAmountProperty; }
			set
			{
				SetProperty(ref TotalAmountProperty, value);
				if (TotalAmountChanged != null)
					TotalAmountChanged(this, TotalAmount);
			}
		}

		public Guid? TableId => Table?.Id;
		public Table Table
		{
			get { return TableModelProperty; }
			set { SetProperty(ref TableModelProperty, value); }
		}
		private Table TableModelProperty;

		public string Description
		{
			get { return DescriptionProperty; }
			set { SetProperty(ref DescriptionProperty, value); }
		}

		private string DescriptionProperty;

		public Guid Stand { get; set; }

		public Guid Cashier { get; set; }

		public int OrderCount
		{
			get { return OrderCountProperty; }
			set { SetProperty(ref OrderCountProperty, value); }
		}

		private int OrderCountProperty;

		public long? Serie { get; set; }

		public string SeriePrex { get; set; }

		public bool IsEditable
		{
			get { return IsEditableProperty; }
			set { SetProperty(ref IsEditableProperty, value); }
		}

		public bool IsFull
		{
			get
			{
				return false;
			}
		}

		public bool ShowDescription
		{
			get
			{
				return false;
			}
		}

		public bool IsClosed { get; set; }

		public bool Uploaded { get; set; }

		public bool Blocked { get; set; }

        public string OpenUserDescription { get; set; }
		public DateTime? CloseDate { get; set; }

		public int Shift { get; set; }

		public bool IsPersisted { get; set; }

		public bool HasOrders { get; set; }
	}

}
