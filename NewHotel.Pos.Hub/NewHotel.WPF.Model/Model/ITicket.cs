﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NewHotel.WPF.App.Pos.Model
{
	public interface ITicket
    {
		Guid Id { get; }
		bool IsEditable { get; set; }
		bool IsFull { get; }
		Guid Stand { get; }
		Guid Cashier { get; }
		long? Serie { get; }
		string Description { get; }
		decimal TotalAmount { get; }
		string SeriePrex { get; }
		bool ShowDescription { get; }
		bool Blocked { get; }
		bool IsClosed { get; }
		Guid? TableId { get; }
		DateTime? CloseDate { get; }
		int Shift { get; }
		bool IsPersisted { get; }
		bool HasOrders { get; }
	}


	public static class ITicketActions
	{
		public static bool HasTable(this ITicket ticket) => ticket.TableId.HasValue;

		public static bool RemoveTicket(this ICollection<ITicket> tickets, ITicket ticket)
		{
			if (tickets.FirstOrDefault(x => x.Id == ticket.Id) is ITicket toRemove)
			{
				return tickets.Remove(toRemove);
			}

			return false;
		}
	}
}