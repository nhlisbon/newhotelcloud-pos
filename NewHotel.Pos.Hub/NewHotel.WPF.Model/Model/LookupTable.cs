﻿using System;
using System.Linq;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;
using NewHotel.Fiscal.Saft;
using NewHotel.Pos.Localization;
using NewHotel.Pos.Printer.Interfaces;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Model.Model.CommonUtils;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.Model.Model
{
	public class LookupTable : BindableBase, IPrinteableLookupTable
    {
        private LookupTableContract _lookupContract;
     
        public override object DataContext
        {
            get => _lookupContract;
            set => _lookupContract = (LookupTableContract)value;
        }

        #region Constructors

        public LookupTable(Ticket ticket)
        {
            this.CashierId = ticket.Cashier;
            this.StandId = ticket.Stand;
            this.Ticket = ticket.Id;
            this.Value = ticket.TotalAmount;
        }

        public LookupTable(LookupTableContract dataContext)
        {
            DataContext = dataContext;
            RaiseAllPropertyChanged();
            if (!string.IsNullOrEmpty(Signature))
                SignedLabel = SAFTPT.GetLabel(Signature);
        }


        #endregion

        #region Properties

        public Guid? StandId 
        {
            get => _lookupContract.Stand;
            set
            { 
                if (value.HasValue)
                    _lookupContract.Stand = value.Value;
            }
        }

        public Guid? CashierId
        { 
            get => _lookupContract.Caja;
            set
            { 
                if (value.HasValue)
                    _lookupContract.Caja = value.Value;
            }
        }

        public DateTime? DocDate
        { 
            get => _lookupContract.WorkDate;
            set
            { 
                if (value.HasValue)
                    _lookupContract.WorkDate = value.Value;
            }
        }

        public DateTime? DocSysDateTime
        { 
            get => _lookupContract.SystemDateTime;
            set
            { 
                if (value.HasValue)
                    _lookupContract.SystemDateTime = value.Value;
            }
        }

        public string Signature
        { 
            get => _lookupContract.Signature;
            set => _lookupContract.Signature = value;
        }

        public Guid? User
        {
            get => _lookupContract.User;
            set
            {
                if (value.HasValue)
                    _lookupContract.User = value.Value;
            }
        }

        public decimal Value
        { 
            get => _lookupContract.Total;
            set => _lookupContract.Total = value;
        }

        public string Serie
        {
            get => _lookupContract.Serie;
            set => _lookupContract.Serie = value;
        }

        public long? Number
        {
            get => _lookupContract.Number;
            set
            { 
                if (value.HasValue)
                    _lookupContract.Number = value.Value;
            }
        }

        public string ValidationCode
        {
            get => _lookupContract.ValidationCode;
            set => _lookupContract.ValidationCode = value;
        }

        public Blob? QrCode
        {
            get
            {
                if (_lookupContract.QrCode == null && !string.IsNullOrEmpty(_lookupContract.QrCodeData))
                {
                    QRCodeUtil.GetQRCodeFromData(_lookupContract.QrCodeData);
                }
                return _lookupContract.QrCode;
            }
            set => _lookupContract.QrCode = value;
        }

        public Guid? Ticket
        {
            get => _lookupContract.Caja;
            set
            { 
                if (value.HasValue)
                    _lookupContract.Caja = value.Value;
            }
        }

        #endregion

        #region Dummy

        public string SignedLabel { get; set; }

        #endregion
    }

    public class Client : BindableBase, IPrintableClient
    {
        private POSClientByPositionContract _contract;

        public override object DataContext
        {
            get => _contract;
            set => _contract = (POSClientByPositionContract)value;
        }

        #region Constructors

        public Client(POSClientByPositionContract dataContext)
        {
            DataContext = dataContext;
            RaiseAllPropertyChanged();
        }

        #endregion
        #region Properties

        public string ClientInfo
        {
            get
            {
                var clientInfo = _contract.ClientName;
                if (!string.IsNullOrEmpty(_contract.ClientRoom))
                    clientInfo += $" # {_contract.ClientRoom}";

                return clientInfo;
            }
        }

        public string Preferences => string.Join(", ", _contract.Preferences.Select(x => x.Description));

        public string Diets => string.Join(", ", _contract.Diets.Select(x => x.Description));

        public string Attentions => string.Join(", ", _contract.Attentions.Select(x => x.Description));

        public string Allergies => string.Join(", ", _contract.Allergies.Select(x => x.Description));
        
        public string UncommonAllergies => _contract.UncommonAllergies;
        public string SegmentOperations => _contract.SegmentOperations;

        public short Pax => _contract.Pax;

        #endregion
    }
}