﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using System.Threading.Tasks;
using System.Windows.Media;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using NewHotel.Pos.Communication;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Hub.Model.Cashier.Contracts.Settings;
using NewHotel.Pos.Printer;
using NewHotel.WPF.App.Pos.Model;
using NewHotel.WPF.Common;
using NewHotel.WPF.MVVM;
using CurrencyExchangeRecord = NewHotel.Pos.Hub.Contracts.Cashier.Records.CurrencyExchangeRecord;

namespace NewHotel.WPF.Model.Model;

public class Stand : BindableBase
{
    #region Variables

    private readonly Separator _defaultSeparator = new Separator("Others", short.MaxValue - 10, short.MaxValue - 10, false);

    private ObservableCollection<CurrencyExchange> _currencyExchanges;
    private PrintersConfiguration _printersConfiguration;
    private string _description;
    private PrintDocSetting _ticketPrintSettings;
    private PrintDocSetting _receiptPrintSettings;
    private PrintDocSetting _invoicePrintSettings;
    private PrintDocSetting _ballotPrintSettings;
    private ObservableCollection<Product> _products;
    private Dictionary<Guid, Product> _productById;
    private ObservableCollection<PaymentMethod> _paymentMethods;
    private Guid? _controlAccount;
    private bool _copyKitchenInTicketPrinter;
    private HappyHour _currentHappyHour;
    private DateTime _happyHourCountdown;
    private ObservableCollection<GroupProduct> _groupProducts;
    private ObservableCollection<FamilyProduct> _familyProducts;
    private ObservableCollection<SubfamilyProduct> _subfamilyProducts;
    private Separator _tipSeparator;
    private int _outTransferNotificationsCount;
    private int _inTransferNotificationsCount;
    private ObservableCollection<Area> _areas;
    private ObservableCollection<Product> _recentProducts;
    private ObservableCollection<InternalUse> _internalUses;

    #endregion
    #region Properties

    public DateTime StandWorkDateTime => new DateTime(StandWorkDate.Year, StandWorkDate.Month, StandWorkDate.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0);

    public int InTransferNotificationsCount
    {
        get => _inTransferNotificationsCount;
        set => SetProperty(ref _inTransferNotificationsCount, value);
    }

    public int OutTransferNotificationsCount
    {
        get => _outTransferNotificationsCount;
        set => SetProperty(ref _outTransferNotificationsCount, value);
    }

    public int TotalTransferNotificationsCount => InTransferNotificationsCount + OutTransferNotificationsCount;

    public POSStandRecord StandRecord { get; set; }

    public bool IsHappyHourTime => CurrentHappyHour != null;

    public HappyHour CurrentHappyHour
    {
        get => _currentHappyHour;
        set => SetProperty(ref _currentHappyHour, value);
    }

    public DateTime HappyHourCountdown
    {
        get => _happyHourCountdown;
        set => SetProperty(ref _happyHourCountdown, value);
    }

    public Guid? TipService
    {
        get => StandRecord.TipServiceId;
        set => StandRecord.TipServiceId = value;
    }

    public decimal? TipPercent
    {
        get => StandRecord.TipPercent;
        set => StandRecord.TipPercent = value;
    }

    public decimal? TipValue
    {
        get => StandRecord.TipValue;
        set => StandRecord.TipValue = value;
    }

    public bool TipAuto
    {
        get => StandRecord.ApplyTipAutomatically;
        set => StandRecord.ApplyTipAutomatically = value;
    }

    public bool TipOverNet
    {
        get => StandRecord.ApplyTipOverNetValue;
        set => StandRecord.ApplyTipOverNetValue = value;
    }

    public bool IsRoomService => StandRecord.IsRoomService;

    public string CurrencyId { get; set; }

    public long Shift
    {
        get => StandRecord.StandWorkShift;
        set
        {
            var refValue = StandRecord.StandWorkShift;
            SetProperty(ref refValue, value, () => StandRecord.StandWorkShift = refValue);
        }
    }

    public string Description
    {
        get => _description;
        set => SetProperty(ref _description, value);
    }

    public DateTime StandWorkDate
    {
        get => StandRecord.StandWorkDate;
        set
        {
            var refValue = StandRecord.StandWorkDate;
            SetProperty(ref refValue, value, () => StandRecord.StandWorkDate = refValue);
        }
    }

    public Guid? PensionRate
    {
        get => StandRecord.PensionRateType;
        set
        {
            var refValue = StandRecord.PensionRateType;
            SetProperty(ref refValue, value, () => StandRecord.PensionRateType = refValue);
        }
    }

    public Guid Id { get; set; }

    public Guid? InternalUseRate
    {
        get => StandRecord.ConsIntRateType;
        set
        {
            var refValue = StandRecord.ConsIntRateType;
            SetProperty(ref refValue, value, () => StandRecord.ConsIntRateType = refValue);
        }
    }

    public decimal? InternalUsePercent
    {
        get => StandRecord.ConsIntPercentValue;
        set
        {
            var refValue = StandRecord.ConsIntPercentValue;
            SetProperty(ref refValue, value, () => StandRecord.ConsIntPercentValue = refValue);
        }
    }

    public Guid? StandardRate
    {
        get => StandRecord.StandardRateType;
        set
        {
            var refValue = StandRecord.StandardRateType;
            SetProperty(ref refValue, value, () => StandRecord.StandardRateType = refValue);
        }
    }

    public PrintDocSetting TicketPrintSettings
    {
        get => _ticketPrintSettings;
        set => SetProperty(ref _ticketPrintSettings, value);
    }

    public PrintDocSetting ReceiptPrintSettings
    {
        get => _receiptPrintSettings;
        set => SetProperty(ref _receiptPrintSettings, value);
    }

    public PrintDocSetting BallotPrintSettings
    {
        get => _ballotPrintSettings;
        set => SetProperty(ref _ballotPrintSettings, value);
    }

    public PrintDocSetting InvoicePrintSettings
    {
        get => _invoicePrintSettings;
        set => SetProperty(ref _invoicePrintSettings, value);
    }

    public Guid? ControlAccount
    {
        get => _controlAccount;
        set => SetProperty(ref _controlAccount, value);
    }

    public bool CopyKitchenInTicketPrinter
    {
        get => _copyKitchenInTicketPrinter;
        set => SetProperty(ref _copyKitchenInTicketPrinter, value);
    }

    public ObservableCollection<Category> TopCategories { get; private set; } = [];

    public void SetTopCategories(IEnumerable<Category> categories)
	{
        TopCategories.Clear();
        if (categories == null)
            return;
        foreach (var category in categories)
		{
			TopCategories.Add(category);
		}
	}

    public ObservableCollection<InternalUse> InternalUses
    {
        get => _internalUses;
        set => SetProperty(ref _internalUses, value);
    }

    public ObservableCollection<Product> Products
    {
        get => _products;
        set => SetProducts(value);
        //set
        //{
        //    _productById = value.ToDictionary(x => x.Id);
        //    SetProperty(ref _products, value);
        //}
    }

    private void InternalClearProducts()
    {
        try
        {
            (_products ??= []).Clear();
            (_productById ??= []).Clear();
        }
		catch (Exception ex)
		{
			Trace.TraceError(ex.Message);
		}
	}

	public void ClearProducts()
    {
        InternalClearProducts();
		OnPropertyChanged(nameof(Products));
	}

    public void SetProducts(IEnumerable<Product> products)
    {
        InternalClearProducts();
        if (products != null)
        {
            foreach (var product in products)
			{
                AddProduct(product);
			}
        }
		OnPropertyChanged(nameof(Products));
	}

    public Product? GetProductById(Guid id)
        => (_productById?.TryGetValue(id,out var product) ?? false) ? product : null;

    public void AddProduct(Product product)
    {
        try
        {
            _products.Add(product);
            _productById[product.Id] = product;
        }
		catch (Exception ex)
		{
			Trace.TraceError(ex.Message);
		}
    }

    public ObservableCollection<GroupProduct> GroupProducts
    {
        get => _groupProducts;
        set => SetProperty(ref _groupProducts, value);
    }

    public ObservableCollection<FamilyProduct> FamilyProducts
    {
        get => _familyProducts;
        set => SetProperty(ref _familyProducts, value);
    }

    public ObservableCollection<SubfamilyProduct> SubfamilyProducts
    {
        get => _subfamilyProducts;
        set => SetProperty(ref _subfamilyProducts, value);
    }

    public ObservableCollection<PaymentMethod> PaymentMethods
    {
        get => _paymentMethods;
        set => SetProperty(ref _paymentMethods, value);
    }

    public ObservableCollection<Separator> Separators { get; set; }

    public ObservableCollection<ITicket> Tickets { get; set; }

    public ObservableCollection<Saloon> Saloons { get; set; }

    public ObservableCollection<HappyHour> HappyHours { get; set; }

    public PrintersConfiguration PrintersConfiguration
    {
        get { return _printersConfiguration ??= new PrintersConfiguration(Id); }
    }

    public ObservableCollection<Area> Areas
    {
        get => _areas;
        set => SetProperty(ref _areas, value);
    }

    public ObservableCollection<Product> RecentProducts
    {
        get => _recentProducts;
        set => SetProperty(ref _recentProducts, value);
    }

    public ObservableCollection<CurrencyExchange> CurrencyExchanges
    {
        get => _currencyExchanges;
        set => SetProperty(ref _currencyExchanges, value);
    }

    public TicketProductGrouping ProductGrouping
    {
        get => StandRecord.ProductGrouping;
        set
        {
            var refValue = StandRecord.ProductGrouping;
            SetProperty(ref refValue, value, () => StandRecord.ProductGrouping = refValue);
        }
    }

    #endregion

    #region Constructor

    public Stand(POSStandRecord record)
    {
        Id = (Guid)record.Id;
        Description = record.Description;
        ControlAccount = record.ControlAccount;
        StandRecord = record;
        CopyKitchenInTicketPrinter = record.CopyKitchenInTicketPrinter;
        InitDummy();
    }

    #endregion
    #region Methods

    ImageManager imageManager;

	public ValidationItemResult<StandEnvironment> UpdateEnvironment(ValidationItemResult<StandEnvironment> environmentResult)
    {
        if (environmentResult.HasErrors)
			return environmentResult;

		Trace.TraceInformation("ENVIRONMENT.....start at " + DateTime.Now);
		var environment = environmentResult.Item;

		imageManager = new ImageManager();
		CurrencyId = environment.Currency;

		#region Internal Uses   

		InternalUses = (environment.HouseUses == null) ?
			new ObservableCollection<InternalUse>() :
			new ObservableCollection<InternalUse>(environment.HouseUses.Select(s => new InternalUse
			{
				Id = s.Id,
				Description = s.Description,
				Invitations = s.IsInvitation,
				LimitPercent = s.LimitPercent,
				MonthlyLimit = s.MonthlyLimit,
				Unlimited = s.Unlimited
			}));

		Trace.TraceInformation("ENVIRONMENT.....Internal Uses " + DateTime.Now);

		#endregion

		#region Saloons

		Saloons = (environment.Saloons == null) ? new ObservableCollection<Saloon>() :
			new ObservableCollection<Saloon>(environment.Saloons.Select(se =>
			{
				var saloonModel = new Saloon(se.Saloon.Id, se.Saloon.Description)
				{
					Id = se.Saloon.Id,
					Description = se.Saloon.Description,
					Height = se.Saloon.SaloonHeight,
					Width = se.Saloon.SaloonWidth,
					ImageFree = imageManager.ProcessHubImagePath(se.Saloon.ImageTableVacantState),
					ImageBusy = imageManager.ProcessHubImagePath(se.Saloon.ImageTableBusyState),
					BackImage = imageManager.ProcessHubImagePath(se.Saloon.ImageSaloonFloorPlan)
				};

				saloonModel.AddTableRange(se.Tables.Select(te =>
				{
					var tableModel = new Table(te.Table.Id)
					{
						Column = te.Table.Column,
						Height = te.Table.Height ?? 2,
						Name = te.Table.Description,
						Paxs = te.Table.MaxPaxs,
						Row = te.Table.Row,
						Saloon = se.Saloon.Description,
						SaloonId = se.Saloon.Id,
						Type = te.Table.Type,
						TypeDesc = te.Table.Type.ToString(),
						Width = te.Table.Width ?? 2,
					};

					return tableModel;
				}));

				foreach (var table in saloonModel.Tables)
				{
					var reservationes = se.Tables
						.First(x => x.Table.Id == table.Id)
						.Reservations
						.Select(x => new LiteTablesReservation(x, saloonModel))
						.OrderBy(x => x.InitialTime)
						.ToList();
					table.AddReservationRange(reservationes);
				}

				return saloonModel;
			}));

		Trace.TraceInformation("ENVIRONMENT.....Saloons " + DateTime.Now);

		#endregion

		#region Separator

		Separators = (environment.Separators == null) ? new ObservableCollection<Separator>() :
			new ObservableCollection<Separator>(environment.Separators.Select(s => new Separator
			{
				Id = s.Id,
				Description = s.Description,
				AcumRound = s.CumulativeRound,
				ImpresionOrden = s.Order,
				PaxMax = s.MaxCoversAllowed
			}));

		Trace.TraceInformation("ENVIRONMENT.....Separator " + DateTime.Now);

		#endregion

		#region Group Product

		GroupProducts = (environment.ProductGroups == null) ? new ObservableCollection<GroupProduct>() :
			new ObservableCollection<GroupProduct>(environment.ProductGroups.Select(s => new GroupProduct
			{
				Id = s.Id,
				Description = s.Description
			}));

		Trace.TraceInformation("ENVIRONMENT.....Group Product " + DateTime.Now);

		#endregion

		#region Families

		FamilyProducts = (environment.ProductFamilies == null) ? new ObservableCollection<FamilyProduct>() :
			new ObservableCollection<FamilyProduct>(environment.ProductFamilies.Select(s => new FamilyProduct
			{
				Id = s.Id,
				Description = s.Description
			}));

		Trace.TraceInformation("ENVIRONMENT.....Families " + DateTime.Now);

		#endregion

		#region Subfamilies

		SubfamilyProducts = (environment.ProductSubFamilies == null) ? new ObservableCollection<SubfamilyProduct>() :
			new ObservableCollection<SubfamilyProduct>(environment.ProductSubFamilies.Select(s => new SubfamilyProduct
			{
				Id = s.Id,
				Description = s.Description
			}));

		Trace.TraceInformation("ENVIRONMENT.....Subfamilies " + DateTime.Now);

		#endregion

		#region Payment

		PaymentMethods = (environment.PaymentMethods == null)
			? new ObservableCollection<PaymentMethod>()
			: new ObservableCollection<PaymentMethod>(environment.PaymentMethods
				.Select(s => new PaymentMethod(s)));

		Trace.TraceInformation("ENVIROMENT.....Payment " + DateTime.Now);

		#endregion

		#region Areas

		Areas = (environment.Areas == null) ? new ObservableCollection<Area>() :
			new ObservableCollection<Area>(environment.Areas.Select(s => new Area(s.Id)
			{
				Color = Color.FromRgb(s.AreaColor.R, s.AreaColor.G, s.AreaColor.B),
				ColorRGB = Color.FromRgb(s.AreaColor.R, s.AreaColor.G, s.AreaColor.B),
				Description = s.Description,
				KitchenCopies = s.NumberCopies,
				Mandatory = s.Mandatory
			}));

		Trace.TraceInformation("ENVIRONMENT.....Areas " + DateTime.Now);

		#endregion

		#region Happy Hours

		HappyHours = environment.HappyHours == null ? new ObservableCollection<HappyHour>() :
			new ObservableCollection<HappyHour>(environment.HappyHours.Select(s => new HappyHour(s)));

		Trace.TraceInformation("ENVIRONMENT.....Happy Hours " + DateTime.Now);

		#endregion

		#region Products

		ClearProducts();
		if (environment.Products != null)
		{
			foreach (var productRecord in environment.Products)
			{
				var areas = new List<Area>();
				foreach (var areaRecord in productRecord.Areas)
				{
					var area = Areas.FirstOrDefault(a => a.Id == areaRecord.Id);
					if (area == null) continue;
					area.Mandatory = areaRecord.Mandatory;
					areas.Add(area);
				}

				var preparations = productRecord.Preparations.Select(x => new Preparation(x));
				var product = new Product(productRecord, areas, preparations)
				{
					ImagePath = imageManager.ProcessHubImagePath(productRecord.Image),

				};
				AddProduct(product);
			}
		}

		Trace.TraceInformation("ENVIRONMENT.....Products " + DateTime.Now);

		#endregion

		#region Categories

		var topCategories = (environment.Categories == null) ? new ObservableCollection<Category>() :
			new ObservableCollection<Category>(environment.Categories.Select(s => new Category
			{
				Id = s.Category.Id,
				Description = s.Category.Description,
				ImagePath = s.Category.ImagePath,
				Index = s.Category.Order,
				SubCategories = new ObservableCollection<Category>(s.SubCategories.Select(s2 => new Category
				{
					Id = s2.SubCategory.Id,
					Description = s2.SubCategory.Description,
					ImagePath = s2.SubCategory.ImagePath,
					Index = s2.SubCategory.Order,
					SubCategories = new ObservableCollection<Category>(s2.ProductColumns
						.GroupBy(s3 => new { s3.ColumnId, s3.ColumnDescription })
						.Select((s4, n) => new Category
						{
							Index = n,
							Id = s4.Key.ColumnId,
							Description = s4.Key.ColumnDescription,
							Products = new ObservableCollection<Product>(s4.Where(p => Products.Any(p1 => !p1.Inactive && p1.Id == p.ProductId)).Select(p => Products.First(p1 => p1.Id == p.ProductId))),
							ProductIdList = new ObservableCollection<Guid>(s4.Select(p => p.ProductId))
						}))
				}))
			}));

		SetTopCategories(topCategories);

		Trace.TraceInformation("ENVIRONMENT.....Categories " + DateTime.Now);

		#endregion

		#region PrintSettings

		var documentSign = Hotel.Instance.SignType;
		var country = environment.Hotel.Nationality;

		TicketPrintSettings = environment.TicketPrintSettings == null ? null :
			PrintDocSetting.Create(environment.TicketPrintSettings, documentSign, country, StandRecord);

		BallotPrintSettings = environment.BallotPrintSettings == null ? null :
			PrintDocSetting.Create(environment.BallotPrintSettings, documentSign, country, StandRecord);

		InvoicePrintSettings = (environment.InvoicePrintSettings == null) ? null :
			PrintDocSetting.Create(environment.InvoicePrintSettings, documentSign, country, StandRecord);

		ReceiptPrintSettings = environment.ReceiptPrintSettings == null ? null :
			PrintDocSetting.Create(environment.ReceiptPrintSettings, documentSign, country, StandRecord);

		// esto esta asi porque cuando IsLarge es true tiene que ir directo al cloud
		// hay que ver como hacer para imprimir en el HUB
		if (TicketPrintSettings != null)
			TicketPrintSettings.IsLarge = false;
		if (BallotPrintSettings != null)
			BallotPrintSettings.IsLarge = false;
		if (InvoicePrintSettings != null)
			InvoicePrintSettings.IsLarge = false;
		if (ReceiptPrintSettings != null)
			ReceiptPrintSettings.IsLarge = false;

		#endregion

		#region CurrencyExchanges

		IEnumerable<CurrencyExchangeRecord> currencyExchanges = environment.CurrencyExchanges ?? new CurrencyExchangeRecord[0];
		CurrencyExchanges = new ObservableCollection<CurrencyExchange>(currencyExchanges.Select(t => new CurrencyExchange(t, environment.Currencys.FirstOrDefault(x => (Guid)x.Id == t.CurrencyCashierId)?.CurrencyId)));

		#endregion

		Trace.TraceInformation("ENVIRONMENT.....finish at " + DateTime.Now);
		return environmentResult;
	}

    public async Task<ValidationResult> UpdateImages()
    {
        ValidationResult result = [];

        ImageManager imageManager;

		if ((imageManager = this.imageManager) == null)
            return result;

        #region DownloadNewImages

        try
        {
            if (imageManager.MissImages)
            {
                result.AddValidations(await imageManager.DownloadNewImages());
            }

        }
        catch (Exception ex)
        {
            Trace.TraceError("ENVIRONMENT.....DownloadNewImages " + DateTime.Now, ex);
            result.AddError(ex.ToString());
        }
        #endregion

        imageManager = null;

        return result;
    }

    public async Task<ValidationItemResult<StandEnvironment>> LoadEnvironmentAsync()
    {
        int languageId = Hotel.SelectedLanguage.Code;

        Trace.TraceInformation("ENVIRONMENT.....load requested at " + DateTime.Now);

		ValidationItemResult<StandEnvironment> environmentResult = await BusinessProxyRepository.Stand.GetStandEnvironmentAsync(Id, languageId);

        return environmentResult;
    }

    public Saloon FindSaloon(Guid saloonId)
    {
        return Saloons.FirstOrDefault(x => x.Id == saloonId);
    }

    public Table FindTable(Guid tableId)
    {
        foreach (var saloon in Saloons)
        {
            Table table = saloon.FindTable(tableId);
            if (table != null)
                return table;
        }
        return null;
    }

    public Table FindTable(string name)
    {
        if (Saloons != null)
        {
            foreach (var saloon in Saloons)
            {
                Table table = saloon.FindTable(name);
                if (table != null)
                    return table;
            }
        }
        return null;
    }

    public void SetHappyHour()
    {
        SetHappyHour(false);
    }

    public void SetHappyHour(Guid id)
    {
        HappyHour item = HappyHours.FirstOrDefault(x => x.Id == id);

        if (CurrentHappyHour == null || CurrentHappyHour.Id != item.Id) SetHappyHour(item);
    }

    public void ClearHappyHour()
    {
        SetHappyHour(null);
    }

    public override string ToString()
    {
        return Description ?? String.Empty;
    }

    public void UpdateTurnAndDate(StandTurnDateContract contract)
    {
        StandWorkDate = contract.WorkDate;
        Shift = (int)contract.Turn;
    }

    public void HideEmptyCategories()
    {
        foreach (Category cat in TopCategories)
        {
            foreach (Category subCat in cat.SubCategories)
                if (subCat.SubCategories.Count > 0)
                    subCat.SubCategories.GetCollectionView().Filter = obj => { return (obj as Category).IsVisible; };
            cat.SubCategories.GetCollectionView().Filter = obj => { return (obj as Category).IsVisible; };
        }
        TopCategories.GetCollectionView().Filter = obj => { return (obj as Category).IsVisible; };
    }

    public void ShowEmptyCategories()
    {
        TopCategories.GetCollectionView().Filter = null;
        foreach (Category cat in TopCategories)
        {
            cat.SubCategories.GetCollectionView().Filter = null;
            foreach (Category subCat in cat.SubCategories)
                subCat.SubCategories.GetCollectionView().Filter = null;
        }
    }

    public void AddTicket(Ticket ticket)
    {
        if (!Tickets.Any(x => x.Id == ticket.Id))
            Tickets.Add(ticket);

        if (ticket.Table != null)
            Hotel.Instance.AddTicket(ticket);
    }

    public void RemoveTicket(ITicket ticket)
    {
        Tickets.RemoveTicket(ticket);
        Hotel.Instance.RemoveTicket(ticket);
    }

    public bool AddTipOrder(Ticket ticket)
    {
        if (TipService.HasValue)
        {
            var tip = GetProductById(TipService.Value);
            if (tip != null)
            {
                var order = Order.CreateTipOrder(this, ticket, tip);
                if (order != null)
                {
                    ticket.Orders.Add(order);
                    return true;
                }
            }
        }

        return false;
    }

    public void InitializeTip(Ticket ticket, Action<bool> afterAction = null)
    {
        if (AddTipOrder(ticket))
        {
            if (TipPercent.HasValue)
            {
                ticket.TipPercent = TipPercent.Value;
                ticket.TipOverNet = TipOverNet;
            }
            else if (TipValue.HasValue)
                ticket.TipValue = TipValue.Value;
            afterAction?.Invoke(true);
        }
        else
            afterAction?.Invoke(false);
    }

    public bool ContainsTicket(Ticket ticket)
    {
        return Tickets.Any(x => x.Id == ticket.Id);
    }

    public void ChangeContext()
    {
        RaiseAllPropertyChanged();
    }

    //public Product GetProductFromId(Guid productId)
    //{
    //    return Products.FirstOrDefault(x => x.Id == productId);
    //}

    public static Separator transferSeparator = new Separator("Transfers", short.MinValue, short.MaxValue, false);

    public Separator TipSeparator => _tipSeparator ??= new Separator("Tip", short.MaxValue, short.MaxValue, false) { Id = Guid.Empty };

    public void VerifyTransferToAccept(Action<ContainerTransferContract, Exception> afterAction = null)
    {
        Task.Run(() => BusinessProxyRepository.Stand.VerifyTransferToAcceptAsync(Id, Hotel.SelectedLanguage.Code))
            .ContinueWith(t =>
                {
                    if (t.IsFaulted)
                    {
                        afterAction?.Invoke(null, t.Exception.GetBaseException());
                    }
                    else if (t.Status == TaskStatus.RanToCompletion && !t.Result.HasErrors)
                    {
                        InTransferNotificationsCount = 0;
                        InTransferNotificationsCount = ((ContainerTransferContract)t.Result.Contract).Transfers.Count;
                        afterAction?.Invoke(t.Result.Contract as ContainerTransferContract, null);
                    }
                },
                UIThread.Scheduler);
    }

    public void VerifyTransferReturns(Guid cashierOrigId, Action<ContainerTransferContract, Exception> afterAction = null)
    {
        Guid standId = Id;
        int language = Hotel.SelectedLanguage.Code;

        Task.Run(async () =>
            {
                return await BusinessProxyRepository.Stand.VerifyTransferToReturnAsync(standId, language);
            })
            .ContinueWith(t =>
                {
                    if (t.IsFaulted)
                    {
                        if (afterAction != null)
                            afterAction(null, t.Exception.GetBaseException());
                    }
                    else if (t.Status == TaskStatus.RanToCompletion && !t.Result.HasErrors)
                    {
                        OutTransferNotificationsCount = 0;
                        OutTransferNotificationsCount = (t.Result.Contract as ContainerTransferContract).Transfers.Count;
                        if (afterAction != null) afterAction(t.Result.Contract as ContainerTransferContract, null);
                    }
                },
                UIThread.Scheduler);
    }

    public HappyHour GetHappyHour(DateTime time)
    {
        //No Happy Hours
        if (HappyHours == null) return null;

        //Starts now
        var hhs = (from happy in HappyHours
            where happy.DaysOfTheWeek[(int)time.DayOfWeek] == '1' &&
                  !happy.Inactive &&
                  (Math.Abs((happy.InitialHour.TimeOfDay - time.TimeOfDay).TotalSeconds) < 30 || Math.Abs((happy.FinalHour.TimeOfDay - time.TimeOfDay).TotalSeconds) < 30)
            select happy).FirstOrDefault();

        if (hhs != null) return hhs;

        //Already started
        foreach (HappyHour hh in HappyHours)
        {
            if (!hh.Inactive && hh.DaysOfTheWeek[(int)time.DayOfWeek] == '1')
            {
                if (hh.InitialHour.TimeOfDay <= time.TimeOfDay && hh.FinalHour.TimeOfDay > time.TimeOfDay) return hh;
            }
        }

        return null;
    }

    protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
        base.OnPropertyChanged(propertyName);

        switch (propertyName)
        {
            case nameof(CurrentHappyHour):
                if (IsHappyHourTime)
                    HappyHourCountdown = CurrentHappyHour.FinalHour.Subtract(DateTime.Now.TimeOfDay);
                OnPropertyChanged(nameof(IsHappyHourTime));
                break;
            case nameof(InTransferNotificationsCount):
            case nameof(OutTransferNotificationsCount):
                base.OnPropertyChanged(nameof(TotalTransferNotificationsCount));
                break;
        }
    }

    #region Private

    private void SetCategoryProducts(IEnumerable<object> categories)
    {
        if (categories != null)
        {
            foreach (Category item in categories)
            {
                if (item.ProductIdList != null && item.ProductIdList.Count > 0)
                {
                    var replace = new ObservableCollection<Product>();
                    foreach (var productId in item.ProductIdList)
                    {
                        try
                        {
                            var pp = Products.FirstOrDefault(x => x.Id == productId);
                            if (pp != null)
                                replace.Add(pp);
                        }
                        catch
                        { }
                    }
                    item.Products = replace;
                }
                SetCategoryProducts(item.SubCategories);
            }
        }
    }

    private void SetHappyHour(bool forceSet)
    {
        var happy = GetHappyHour(DateTime.Now);
        if (CurrentHappyHour != null)
        {
            // Tenemos happy hour ahora
            if (happy != null)
            {
                // No se ha acabado el happy hour
                if (CurrentHappyHour != happy || forceSet)
                {
                    // Es un happy hour diferente debemos cambiarlo
                    SetHappyHour(happy);
                }
                else
                {
                    //Alarma disparada para el mismo happy hour.
                    SetHappyHour(null);
                }
            }
            else
            {
                // Se acabo el happy hour
                SetHappyHour(null);
            }
        }
        else // No tenemos happy hour ahora
        {
            if (happy != null)
            {
                // Hay un happy hour nuevo 
                SetHappyHour(happy);
            }
        }
    }

    private void SetHappyHour(HappyHour happyHour)
    {
        if (CurrentHappyHour != null)
        {// Tenemos un happy hour activado 
            // Desactivar los happy de los productos
            foreach (Product item in Products) item.ClearHappyHour();
            CurrentHappyHour = null;
        }// Tenemos un happy hour activado

        if (happyHour != null)
        {// Tenemos que activar el happy hour

            Guid standId = Id;
            Guid rateId = happyHour.TaxRate;
            // Activar los happy de los productos
            Task.Run(async () => await BusinessProxyRepository.Product.GetProductPricesAsync(standId, rateId))
                .ContinueWith(t =>
                    {
                        if (t.IsFaulted)
                        {

                        }
                        if (t.Status == TaskStatus.RanToCompletion)
                        {
                            ValidationItemSource<ProductPriceRecord> result = t.Result;
                            foreach (var productPrice in result.ItemSource)
                            {
                                GetProductById(productPrice.ProductId)?.SetHappyHour(productPrice.Price);
                            }
                            CurrentHappyHour = happyHour;
                        }
                    },
                    UIThread.Scheduler);

        }// Tenemos que activar el happy hour
    }

    private void InitDummy()
    {
        if (Tickets == null)
            Tickets = new ObservableCollection<ITicket>();
        if (Products == null)
            Products = new ObservableCollection<Product>();
        if (TopCategories == null)
            TopCategories = new ObservableCollection<Category>();
        if (RecentProducts == null)
            RecentProducts = new ObservableCollection<Product>();
    }

    #endregion

    #endregion
    #region Class

    public class ImageManager
    {
        private static readonly IDictionary<string, DateTime> NewImages = new Dictionary<string, DateTime>();
        private const string ImagesFolderName = "Images";

        public bool MissImages => NewImages.Count > 0;

        public string ProcessHubImagePath(string hubPath)
        {
            if (string.IsNullOrEmpty(hubPath))
                return null;

            var localPath = ToLocalPath(hubPath);
            if (!NewImages.ContainsKey(hubPath))
            {
                var fileInfo = new FileInfo(localPath);
                if (fileInfo.Exists)
                    NewImages.Add(hubPath, fileInfo.LastWriteTimeUtc);
                else
                    NewImages.Add(hubPath, DateTime.UtcNow);
            }

            return localPath;
        }

        public async Task<ValidationResult> DownloadNewImages()
        {
            var result = new ValidationResult();

            var downloadImagesResult = await BusinessProxyRepository.Settings.DownloadImages(NewImages.ToArray());

            result.AddValidations(downloadImagesResult);
            if (!result.HasErrors)
            {
                // Guardar imagenes en disco local
                Parallel.ForEach(downloadImagesResult.ItemSource, image =>
                {
                    try
                    {
                        // Salvar localmente la imagen
                        SaveImage(image);
                        Trace.TraceInformation($"Image {image.PathHub} saved");
                    }
                    catch (Exception ex)
                    {
                        Trace.TraceInformation($"Image {image.PathHub} not saved: {ex.Message}");
                    }
                });
            }

            return result;
        }

        private string ToLocalPath(string hubPath)
        {
            if (string.IsNullOrEmpty(hubPath))
                return null;

            int index = hubPath.IndexOf(ImagesFolderName, StringComparison.InvariantCulture);
            string imgPath;
            if (index < 0)
                imgPath = Path.Combine(ImagesFolderName, hubPath);
            else
                imgPath = hubPath.Substring(index);

            return Path.Combine(Directory.GetCurrentDirectory(), imgPath);
        }

        private void SaveImage(DownloadedImageContact image)
        {
            var localPath = ToLocalPath(image.PathHub);
            var fileInfo = new FileInfo(localPath);
            if (!fileInfo.Directory.Exists)
                fileInfo.Directory.Create();
            File.WriteAllBytes(localPath, image.Image);
            File.SetLastWriteTimeUtc(localPath, image.LastWriteTimeUtc);
        }
    }

    #endregion
}