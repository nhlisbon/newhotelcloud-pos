﻿using NewHotel.WPF.MVVM;
using System;

namespace NewHotel.WPF.App.Pos.Model
{
	[Serializable]
    public class Money : BindableBase
	{
        private string descriptionProperty;

        private decimal valueProperty;

        public string Description
        {
            get { return descriptionProperty; }
            set { SetProperty(ref descriptionProperty, value); }
        }

        public decimal Value
        {
            get { return valueProperty; }
            set { SetProperty(ref valueProperty, value); }
        }


        // ESTO ES PROVISIONAL
        public string ImageName { get; set; }

        //public ImageSource ImageBinding
        //{
        //    get
        //    {
        //        return new BitmapImage(new Uri("pack://application:,,,/NewHotel.Pos;component/Resources/Bills&Coins/" + ImageName));
        //    }
        //}


        public string Code
        {
            get { return CodeProperty; }
            set { SetProperty(ref CodeProperty, value); }
        }

        private string CodeProperty;

    }
}
