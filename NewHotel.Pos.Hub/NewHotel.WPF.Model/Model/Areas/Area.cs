﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Printing;
using System.Windows.Media;
using System.Diagnostics;
using NewHotel.WPF.Model;
using NewHotel.WPF.MVVM;
using NewHotel.Pos.Printer.Interfaces;

namespace NewHotel.WPF.App.Pos.Model
{
    public class Area : BindableBase, IPrinteableArea
    {
        #region Dependency Properties

        private string DescriptionProperty;
        private Color ColorProperty;
        private bool InactiveProperty;
        private short KitchenCopiesProperty;
        private bool MandatoryProperty;

        #endregion

        #region Constructor

        private static bool GetBoolValue(string s)
        {
            if (!string.IsNullOrEmpty(s) && bool.TryParse(s, out bool result))
                return result;

            return false;
        }

        private static string GetStringValue(string s, string nullValue = "")
        {
            return string.IsNullOrEmpty(s) ? nullValue : s;
        }

        public Area(Guid id)
        {
            Id = id;
            Color = Colors.AntiqueWhite;
            ColorRGB = Color;

            Trace.TraceInformation("Start area " + id.ToString("N").ToUpper() + DateTime.Now);

            var server = new LocalPrintServer();

            PrinterServer = GetStringValue(MgrServices.GetPrinterSettings($"Area_{id}_PrinterServer"), server.Name);
            PrinterDescription = GetStringValue(MgrServices.GetPrinterSettings($"Area_{id}_PrinterDescription"));
            UsePrinter = GetBoolValue(MgrServices.GetPrinterSettings($"Area_{id}_UsePrinter"));
            UseDisplay = GetBoolValue(MgrServices.GetPrinterSettings($"Area_{id}_UseDisplay"));
            PrintUsingAreaPrinter = GetBoolValue(MgrServices.GetPrinterSettings($"Area_{id}_PrintUsingAreaPrinter"));

            Trace.TraceInformation("End area " + id.ToString("N").ToUpper() + DateTime.Now);
        }

        public Area(Guid id, string description)
            : this(id)
        {
            Description = description;
        }

        #endregion

            #region Properties

        public Guid Id { get; set; }

        public string Description
        {
            get { return DescriptionProperty; }
            set { SetProperty(ref DescriptionProperty, value); }
        }

        public Color Color
        {
            get { return ColorProperty; }
            set { SetProperty(ref ColorProperty, value); }
        }

        public bool Inactive
        {
            get { return InactiveProperty; }
            set { SetProperty(ref InactiveProperty, value); }
        }

        public short KitchenCopies
        {
            get { return KitchenCopiesProperty; }
            set { SetProperty(ref KitchenCopiesProperty, value); }
        }

        public bool Mandatory
        {
            get { return MandatoryProperty; }
            set { SetProperty(ref MandatoryProperty, value); }
        }

        #endregion

        public bool DisplayActivated
        {
            get { return DisplayActivatedProperty; }
            set { SetProperty(ref DisplayActivatedProperty, value); }
        }

        private bool DisplayActivatedProperty;

        public bool Activated
        {
            get { return ActivatedProperty; }
            set { SetProperty(ref ActivatedProperty, value); }
        }

        private bool ActivatedProperty;

        public Color ColorRGB
        {
            get { return ColorRGBProperty; }
            set { SetProperty(ref ColorRGBProperty, value); }
        }

        private Color ColorRGBProperty;

        #region Settings

        public bool UsePrinter
        {
            get { return UsePrinterProperty; }
            set { SetProperty(ref UsePrinterProperty, value); }
        }

        private bool UsePrinterProperty;

        public bool UseDisplay
        {
            get { return UseDisplayProperty; }
            set { SetProperty(ref UseDisplayProperty, value); }
        }

        private bool UseDisplayProperty;

        public string PrinterServer
        {
            get { return PrinterServerProperty; }
            set { SetProperty(ref PrinterServerProperty, value); }
        }

        private string PrinterServerProperty;

        public string PrinterDescription
        {
            get { return PrinterDescriptionProperty; }
            set { SetProperty(ref PrinterDescriptionProperty, value); }
        }

        private string PrinterDescriptionProperty;

        public bool PrintUsingAreaPrinter
        {
            get { return PrintUsingAreaPrinterProperty; }
            set { SetProperty(ref PrintUsingAreaPrinterProperty, value); }
        }

        private bool PrintUsingAreaPrinterProperty;

        public ObservableCollection<string> Printers
        {
            get
            {
                EnsurePrinterCollection();
                return PrintersProperty;
            }
        }

        private ObservableCollection<string> PrintersProperty;

        #endregion

        private static ObservableCollection<string> GetPrinters(PrintServer server)
        {
            return new ObservableCollection<string>(
                server.GetPrintQueues(new EnumeratedPrintQueueTypes[] { EnumeratedPrintQueueTypes.Local }).Select(x => x.Name)
                .Concat(server.GetPrintQueues(new EnumeratedPrintQueueTypes[] { EnumeratedPrintQueueTypes.Connections }).Select(x => x.Description)));
        }

        public void EnsurePrinterCollection()
        {
            try
            {
                PrintersProperty = null;
                var server = new PrintServer(PrinterServer);
                PrintersProperty = GetPrinters(server);
            }
            catch { }
        }

        private void SetActivated()
        {
            Activated = PrinterValid || DisplayValid;
        }

        public bool PrinterValid
        {
            get { return UsePrinter && !string.IsNullOrEmpty(PrinterServer) && !string.IsNullOrEmpty(PrinterDescription); }
        }

        public bool DisplayValid
        {
            //get { return UseDisplay && DisplayActivated; }
            get { return UseDisplay; }
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);
            switch (propertyName)
            {
                case "PrinterServer":
                    SetActivated();
                    EnsurePrinterCollection();
                    OnPropertyChanged("Printers");
                    MgrServices.SetPrinterSettings($"Area_{Id}_PrinterServer", PrinterServer);
                    break;
                case "PrinterDescription":
                    SetActivated();
                    MgrServices.SetPrinterSettings($"Area_{Id}_PrinterDescription", PrinterDescription);
                    MgrServices.SetPrinterSettings($"{PrinterDescription}_LeftMargin", "0", true);
                    MgrServices.SetPrinterSettings($"{PrinterDescription}_RightMargin", "0", true);
                    MgrServices.SetPrinterSettings($"{PrinterDescription}_LineFontSize", "14", true);
                    MgrServices.SetPrinterSettings($"{PrinterDescription}_NoteFontSize", "9", true);
                    MgrServices.SetPrinterSettings($"{PrinterDescription}_ActionFontSize", "20", true);
                    MgrServices.SetPrinterSettings($"{PrinterDescription}_SaloonFontSize", "14", true);
                    break;
                case "UsePrinter":
                    SetActivated();
                    MgrServices.SetPrinterSettings($"Area_{Id}_UsePrinter", UsePrinter.ToString().ToLower());
                    break;
                case "UseDisplay":
                    SetActivated();
                    MgrServices.SetPrinterSettings($"Area_{Id}_UseDisplay", UseDisplay.ToString().ToLower());
                    break;
                case "PrintUsingAreaPrinter":
                    MgrServices.SetPrinterSettings($"Area_{Id}_PrintUsingAreaPrinter", PrintUsingAreaPrinter.ToString().ToLower());
                    break;
                case "DisplayActivated":
                    SetActivated();
                    break;
            }
        }
    }
}