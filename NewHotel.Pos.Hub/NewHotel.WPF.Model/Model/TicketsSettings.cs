﻿using NewHotel.Pos.Printer.Interfaces;

namespace NewHotel.WPF.App.Pos.Model
{
    public class TipSuggestionSetting : ITipSuggestionSetting
    {
        public string Description { get; set; }
        public decimal Percent { get; set; }
        public bool Autogenerate { get; set; }
        public bool OverNet { get; set; }
    } 
}