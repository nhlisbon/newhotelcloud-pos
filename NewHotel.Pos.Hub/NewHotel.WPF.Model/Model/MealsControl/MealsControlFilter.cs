﻿using System;
using NewHotel.WPF.MVVM;

namespace NewHotel.WPF.Model.Model.MealsControl
{
    public class MealsControlFilter : BindableBase
    {
        private DateTime _workDate;
        private string _groupName = "";
        private string _room = "";
        private string _guestFullName = "";
        private bool _isCheckIn;
        private string _consumptionCardNumber = "";

        public DateTime WorkDate
        {
            get => _workDate;
            set => SetProperty(ref _workDate, value);
        }

        public string GroupName
        {
            get => _groupName;
            set => SetProperty(ref _groupName, value);
        }

        public string Room
        {
            get => _room;
            set => SetProperty(ref _room, value);
        }

        public string GuestFullName
        {
            get => _guestFullName;
            set => SetProperty(ref _guestFullName, value);
        }

        public bool IsCheckIn
        {
            get => _isCheckIn;
            set => SetProperty(ref _isCheckIn, value);
        }

        public string ConsumptionCardNumber
        {
            get => _consumptionCardNumber;
            set => SetProperty(ref _consumptionCardNumber, value);
        }
    }
}