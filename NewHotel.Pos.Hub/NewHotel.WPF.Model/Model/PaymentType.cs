﻿using System;
using NewHotel.Pos.Hub.Model;
using NewHotel.WPF.MVVM;
using NewHotel.Pos.Printer.Interfaces;
using NewHotel.WPF.Model.Model;

namespace NewHotel.WPF.App.Pos.Model
{
	public class PaymentMethod : BindableBase, IPrinteablePaymentType
    {
        #region Variables

	    private string _abbreviationProperty;
	    private bool _isCashPaymentProperty;
	    private string _descriptionProperty;
	    private bool _inactiveProperty;
	    private bool _isCreditCardPaymentProperty;
	    private string _unmoProperty;
	    private bool _allowCreditCardProperty;
	    private bool _allowDebitCardProperty;
	    private string _image;
        private string _auxiliarCode; 
        private bool _allowInvoiceClickProperty;
        private bool _allowTicketClickProperty;
        private Guid? _paySystemOriginId;

        #endregion
        
        #region Constants

        public const string CashPaymentMethodDefaultImage = "pack://application:,,,/NewHotel.Pos;component/Resources/PaymentMethods/cash.png";
	    public const string CreditPaymentMethodDefaultImage = "pack://application:,,,/NewHotel.Pos;component/Resources/PaymentMethods/credit.png";

        public PaymentMethod(PaymentMethodsRecord s)
        {
            var imageManager = new Stand.ImageManager();
            Id = s.Id;
            Abbreviation = s.Abbreviation;
            Description = s.Description;
            if (string.IsNullOrEmpty(s.ImagePath))
                ImagePath = s.ConsideredCreditCard ? PaymentMethod.CreditPaymentMethodDefaultImage : PaymentMethod.CashPaymentMethodDefaultImage;
            else
                ImagePath = imageManager.ProcessHubImagePath(s.ImagePath);
            IsCashPayment = !s.ConsideredCreditCard;
            IsCreditCardPayment = s.ConsideredCreditCard;
            AllowCreditCard = s.AllowCreditCard;
            AllowDebitCard = s.AllowDebitCard;
            AllowInvoiceClick = s.AllowInvoiceClick;
            AllowTicketClick = s.AllowTicketClick;
            PaySystemOriginId = s.PaySystemOriginId;
            
            Unmo = s.CurrencyId;
            AuxiliarCode = s.AuxiliarCode;
        }

        #endregion
        
        #region Properties

        public Guid Id { get; set; }

        public string Abbreviation
        {
            get => _abbreviationProperty;
            set => SetProperty(ref _abbreviationProperty, value);
        }

        public bool IsCashPayment
        {
            get => _isCashPaymentProperty;
            set => SetProperty(ref _isCashPaymentProperty, value);
        }

        public string Description
        {
            get => _descriptionProperty;
            set => SetProperty(ref _descriptionProperty, value);
        }

        public bool Inactive
        {
            get => _inactiveProperty;
            set => SetProperty(ref _inactiveProperty, value);
        }

        public bool IsCreditCardPayment
        {
            get => _isCreditCardPaymentProperty;
            set => SetProperty(ref _isCreditCardPaymentProperty, value);
        }

        public string Unmo
        {
            get => _unmoProperty;
            set => SetProperty(ref _unmoProperty, value);
        }


        public string ImagePath
        {
            get => _image;
            set => _image = value;
        }

        public bool AllowCreditCard
        {
            get => _allowCreditCardProperty;
            set => SetProperty(ref _allowCreditCardProperty, value);
        }

        public bool AllowDebitCard
        {
            get => _allowDebitCardProperty;
            set => SetProperty(ref _allowDebitCardProperty, value);
        }

        public string AuxiliarCode
        {
            get => _auxiliarCode;
            set => SetProperty(ref _auxiliarCode, value);
        }


        public string Type
        {
            get
            {
                if (IsCashPayment) return "Cash";
                else if (IsCreditCardPayment) return "CreditCard";
                else return "Unknown";
            }
        }
        
        public bool AllowInvoiceClick
        {
            get => _allowInvoiceClickProperty;
            set => SetProperty(ref _allowInvoiceClickProperty, value);
        }

        public bool AllowTicketClick
        {
            get => _allowTicketClickProperty;
            set => SetProperty(ref _allowTicketClickProperty, value);
        }

        public bool HasPaySystemOriginId => PaySystemOriginId.HasValue;

        public Guid? PaySystemOriginId
		{
			get => _paySystemOriginId;
			set => SetProperty(ref _paySystemOriginId, value);
		}

        public bool HasPaySystem => PaySystemOriginId.HasValue;

        #endregion
    }
}