﻿using NewHotel.Core;
using NewHotel.WPF.MVVM;
using System;
using NewHotel.Contracts;
using NewHotel.Fiscal.Saft;
using DocumentSerieRecord = NewHotel.Pos.Hub.Model;

namespace NewHotel.WPF.App.Pos.Model
{
	public class TicketSerie : BindableBase
	{
		private Guid cashierIdProperty;
		private Guid standIdProperty;
		private string serieProperty;
		private long nextNumberProperty;
		private DateTime? finalDateProperty;
		private long? finalNumberProperty;


		// SerialNumberResult
		public TicketSerie(SerialNumberResult data, Guid standId, Guid cashierId)
        {
			this.Id = Guid.NewGuid();
			this.standIdProperty = standId;
			this.cashierIdProperty = cashierId;
			this.serieProperty = data.DocumentSerie;
			this.nextNumberProperty = data.DocumentNumber.Value;
			this.finalNumberProperty = long.MaxValue;
        }

        public TicketSerie(DocumentSerieRecord.DocumentSerieRecord data, Guid standId, Guid cashierId)
        {
			this.Id = data.Id;
			this.cashierIdProperty = cashierId;
            this.standIdProperty = standId;
            this.finalDateProperty = data.EndDate;
            this.finalNumberProperty = data.EndNumber;
            this.nextNumberProperty = data.NextNumber;
            
            this.serieProperty = (data.SerieOrder == 691) ?
                String.Concat(data.SerieTex, data.SerieNum) :
				String.Concat(data.SerieNum, data.SerieTex);

            this.Signature = data.Signature;
            this.Type = (long)data.DocType;
        }

		public Guid Id { get; private set; }

		public Guid CashierId
		{
			get { return cashierIdProperty; }
			set { SetProperty(ref cashierIdProperty, value); }
		}

		public Guid StandId
		{
			get { return standIdProperty; }
			set { SetProperty(ref standIdProperty, value); }
		}

		public string Serie
        {
            get { return serieProperty; }
            set { SetProperty(ref serieProperty, value); }
        }

        public long Number
        {
            get { return nextNumberProperty; }
            set { SetProperty(ref nextNumberProperty, value); }
        }

        public DateTime? FinalDate
        {
            get { return finalDateProperty; }
            set { SetProperty(ref finalDateProperty, value); }
        }

        public long? FinalNumber
        {
            get { return finalNumberProperty; }
            set { SetProperty(ref finalNumberProperty, value); }
        }

        public long Type { get; set; }

        public string Signature { get; set; }

        public string GetSignatureLabel(string signature)
        {
            return SAFTPT.GetLabel(signature);
        }

        public bool IsValid
        {
            get
            {
                return !((FinalNumber.HasValue && Number > FinalNumber.Value) ||
                    (FinalDate.HasValue && Hotel.Instance.CurrentStandCashierContext.Stand.StandWorkDate > FinalDate));
            }
        }

        public override string ToString()
        {
            return Serie;
        }
    }
}
