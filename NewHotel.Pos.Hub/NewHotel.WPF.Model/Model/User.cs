﻿using System;
using System.Collections.Generic;
using NewHotel.Contracts;
using NewHotel.WPF.MVVM;
using HotelRecord = NewHotel.Pos.Hub.Model.HotelRecord;
using UserRecord = NewHotel.Pos.Hub.Model.UserRecord;

namespace NewHotel.WPF.App.Pos.Model
{
    public class User : BindableBase
    {
        #region Variables

        private string _username;
        private string _password;
        private bool _allowDraw;
        private bool _internal;
        private string _code;
        private string _description;

        #endregion
        #region Constructors

        public User(UserRecord userRecord)
        {
            Id = userRecord.Id;
            Username = userRecord.UserLogin;
            Description = userRecord.Description;
            Code = userRecord.UserCode;
            Password = userRecord.UserPassword;
            Internal = userRecord.UserInternal;
            Permissions = userRecord.Permissions;
        }

        #endregion
        #region Properties

        public Guid Id { get; set; }

        public string Description
        {
            get { return _description; }
            set { SetProperty(ref _description, value); }
        }

        public string Code
        {
            get { return _code; }
            set { SetProperty(ref _code, value); }
        }

        public bool AllowDraw
        {
            get { return _allowDraw; }
            set { SetProperty(ref _allowDraw, value); }
        }

        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }

        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        public bool Internal
        {
            get { return _internal; }
            set { SetProperty(ref _internal, value); }
        }

        public Dictionary<Permissions, Tuple<SecurityCode, string>> Permissions { get; set; } = new Dictionary<Permissions, Tuple<SecurityCode, string>>();

        public HotelRecordModel[] Hotels { get; set; }

        #endregion
        #region Public Methods

        public bool HasWritePermission(Permissions permission, out string permissionTranslation)
        {
            var securityCode = SecurityCode.FullAccess;
            permissionTranslation = permission.ToString();

            Tuple<SecurityCode, string> tuple;
            if (Permissions != null && Permissions.TryGetValue(permission, out tuple))
            {
                securityCode = tuple.Item1;
                permissionTranslation = tuple.Item2;
            }

            return securityCode == SecurityCode.FullAccess || securityCode == SecurityCode.ReadWrite;
        }
        
        public bool HasPermission(Permissions permission)
        {
            if(Permissions != null && Permissions.TryGetValue(permission, out var val))
                return val != null && (val.Item1 == SecurityCode.FullAccess || val.Item1 == SecurityCode.ReadWrite);  
            
            return false;
        }

        #endregion
    }

    

    public class HotelRecordModel : KeyDescHotelRecord
    {
        public HotelRecordModel(HotelRecord record)
        {
            Id = record.Id;
            Country = record.CountryID;
            Description = record.Description;
            Inactive = !record.Synced.HasValue;
        }

        public Cashier[] Cashiers { get; set; }
    }
}