﻿namespace NewHotel.WPF.Common
{
    public class CloudServiceNotFoundError : Error
    {
        public CloudServiceNotFoundError(string errorMessage, string scopeMessage)
            : base(2, errorMessage, scopeMessage)
        { 
        }
    }
}
