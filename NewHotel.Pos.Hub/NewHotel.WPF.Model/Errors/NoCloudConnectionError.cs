﻿namespace NewHotel.WPF.Common
{
    public class NoCloudConnectionError : Error
    {
        public NoCloudConnectionError(string errorMessage, string scopeMessage)
            : base(10, errorMessage, scopeMessage, true)
        { 
        }

        public void SetAllowIgnore(bool value)
        {
            AllowIgnore = value;
        }
    }

}
