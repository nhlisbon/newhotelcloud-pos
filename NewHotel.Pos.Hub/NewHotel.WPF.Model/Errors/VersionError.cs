﻿namespace NewHotel.WPF.Common
{
    public class VersionError : Error
    {
        public string CloudVersion { get; private set; }
        public string PosVersion { get; private set; }

        public VersionError(string errorMessage, string scopeMessage, string cloudVersion, string posVersion)
            : base(141, errorMessage, scopeMessage, false)
        {
            CloudVersion = cloudVersion;
            PosVersion = posVersion;
        }
    }

    public class AfterUpdateError : Error
    {
        public AfterUpdateError(string errorMessage, string scopeMessage)
            : base(151, errorMessage, scopeMessage, true, false)
        {
        }
    }
}
