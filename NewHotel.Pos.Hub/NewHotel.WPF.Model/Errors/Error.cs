﻿using System;

namespace NewHotel.WPF.Common
{
    public class Error
    {
        long code;
        string errorMessage;
        string scopeMessage;
        bool allowIgnore;
        bool allowRetry = true;

        public Error(long code, string errorMessage, string scopeMessage = "", bool allowIgnore = true, bool allowRetry = true)
        {
            this.code = code;
            this.errorMessage = errorMessage;
            this.scopeMessage = scopeMessage;
            this.allowIgnore = allowIgnore;
            this.allowRetry = allowRetry;
        }

        public long Code
        {
            get
            {
                return code;
            }
        }

        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }
        }

        public string ScopeMessage
        {
            get
            {
                return scopeMessage;
            }
        }

        public bool AllowIgnore
        {
            get
            {
                return allowIgnore;
            }
            protected set
            {
                allowIgnore = value;
            }
        }

        public bool AllowRetry
        {
            get
            {
                return allowRetry;
            }
        }


        public static Exception GetInnerException(Exception ex)
        {
            if (ex == null) return null;
            while (ex.InnerException != null)
                ex = ex.InnerException;
            return ex;
        }

        public static void ProcessProxyException(Exception ex, Action updateAction = null, string serviceName = "")
        {
            updateAction();
        }
    }

    public class ProvisionalError : Error 
    {
        public ProvisionalError(string errorMessage, string scopeMessage)
            : base(9999, errorMessage, scopeMessage, false)
        { 
        }
    }

    public class ProvisionalErrorAllowIgnore : Error
    {
        public ProvisionalErrorAllowIgnore(string errorMessage, string scopeMessage)
            : base(9999, errorMessage, scopeMessage, true)
        {
        }
    }
}
