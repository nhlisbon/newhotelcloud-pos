﻿namespace NewHotel.WPF.Common
{
    public class LocalDataSavingError : Error
    {
        public LocalDataSavingError(string errorMessage, string scopeMessage)
            : base(20, errorMessage, scopeMessage, false)
        {
        }

    }

    public class LocalDataAccessingError : Error
    {
        bool updateSQLiteDlls = false;
        public LocalDataAccessingError(string errorMessage, string scopeMessage, bool updateSQLiteDlls = false)
            : base(10, errorMessage, scopeMessage, false)
        {
            this.updateSQLiteDlls = updateSQLiteDlls;
        }

        public bool UpdateSQLiteDlls
        {
            get
            {
                return updateSQLiteDlls;
            }
        }
    }
}
