﻿namespace NewHotel.WPF.Common
{
    public class PersistingCloudError : Error
    {
        public PersistingCloudError(string errorMessage, string scopeMessage)
            : base(21, errorMessage, scopeMessage, true)
        {
        }
    }

    public class PersistingCloudErrorNoAllowIgnore : PersistingCloudError
    {
        public PersistingCloudErrorNoAllowIgnore(string errorMessage, string scopeMessage)
            : base(errorMessage, scopeMessage)
        {
            AllowIgnore = false;
        }
    }

}
