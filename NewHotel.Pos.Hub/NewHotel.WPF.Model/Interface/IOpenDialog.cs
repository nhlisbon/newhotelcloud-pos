﻿namespace NewHotel.WPF.Model.Interface;

public interface IOpenDialog<out T>
{
    public T GetValue();
}