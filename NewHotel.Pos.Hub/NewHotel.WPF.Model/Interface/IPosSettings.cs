﻿using NewHotel.Contracts;

namespace NewHotel.WPF.Model.Interface
{
    public interface IPosSettings
    {
        int? CribNumberHotel { get; }
        string Country { get; }
        bool AllowBallotDocument { get; }
        bool HasFiscalPrinter { get; }
        bool MultCurrency { get; }
        decimal AddedValueOnHouseUse { get; }
        bool LeaveMealPlanOpened { get; }
        DocumentSign SignType { get; }
    }
}