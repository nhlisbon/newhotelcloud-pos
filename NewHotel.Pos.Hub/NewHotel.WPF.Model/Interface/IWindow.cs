﻿using System;
using System.Windows;
using System.Windows.Controls.Primitives;
using NewHotel.Contracts;
using NewHotel.WPF.Core;

namespace NewHotel.WPF.Model.Interface
{
    public interface IWindow
    {
        void ShowBusyDialog(string saving);
        void ShowBusyDialog();
        void CloseBusyDialog();

        void ShowError(ValidationResult validation);

        void ShowError(Exception exception);

        void ShowError(string message);

        void ShowError(string message, string title);

        void ShowInfo(string message, string header);

        void ShowOkCancelErrorWindow(string text, Action<bool?> callback);

        Action OpenDialog(object content, bool autoCancel, object? dataContext, Func<bool> okAction, Point location,
            DialogButtons dialogButtons = DialogButtons.OkCancel, Action cancelAction = null, Action closeAction = null,
            bool hasMaxHeight = true);
        
        Action OpenDialog<T>(IOpenDialog<T> content, bool autoCancel, object? dataContext, Func<T,bool> okAction, Point location,
            DialogButtons dialogButtons = DialogButtons.OkCancel, Action cancelAction = null, Action closeAction = null,
            bool hasMaxHeight = true);

        Action OpenDialog(object content, Func<bool> okAction, DialogButtons dialogButtons = DialogButtons.OkCancel,
            Action cancelAction = null);

        void OpenAcceptCancelDialog(object content, Action<ButtonBase> accept,
            Action<ButtonBase> cancelAction = null);

        void OpenAcceptCancelDialog(object content, Func<ButtonBase, bool> accept,
            Action<ButtonBase> cancelAction = null);

        void CloseDialog();
        void OpenDrawer();
        Action PopCloseAction();
        void ClosePopUp();
    }
}