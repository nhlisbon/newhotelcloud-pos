﻿using System;
using NewHotel.Contracts;

namespace NewHotel.WPF.Model.Interface
{
    public interface IAppSecurity
    {
        void CheckUserWritePermission(Guid userId, Permissions perm, Action<bool, Guid?> callback);
    }
}