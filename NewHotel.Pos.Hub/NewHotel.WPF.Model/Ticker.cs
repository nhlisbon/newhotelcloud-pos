﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Timers;
using NewHotel.WPF.Common;

namespace NewHotel.WPF.Model
{
    public class Ticker : INotifyPropertyChanged
    {
        SortedList<TimeSpan, AlarmInfo> alarms = new SortedList<TimeSpan, AlarmInfo>();

        public event Action<AlarmInfo, TimeSpan> AlarmRaised;
        public event PropertyChangedEventHandler PropertyChanged;

        public Ticker()
        {
            Timer timer = new Timer();
            timer.Interval = 30000; // 30 second updates 
            timer.Elapsed += timer_Elapsed;
            timer.Start();
        }

        public string Now
        {
            get { return DateTime.Now.ToShortTimeString(); }
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            TimeSpan now;
            DateTime nowDate = DateTime.Now;
            if (AlarmRaised != null)
            {
                if (alarms.ContainsKey(now = new TimeSpan(nowDate.Hour, nowDate.Minute, 0)))
                {
                    AlarmRaised(alarms[now], now);
                    alarms.Remove(now);
                }
                else
                {
                    //Find one that happens before, look for same that happens after
                    foreach (var startTime in alarms.Keys)
                    {
                        //Starts before now
                        if (startTime < now)
                        {
                            //Get alarm
                            AlarmInfo alarm = alarms[startTime];
                            if (alarm.Action == AlarmAction.HappyHourStart || alarm.Action == AlarmAction.DutyStart)
                            {
                                //Find if ends after now
                                foreach (var endTime in alarms.Keys)
                                {
                                    if (endTime > now && alarms[endTime].Identifier == alarms[startTime].Identifier && (alarms[endTime].Action == AlarmAction.HappyHourEnd || alarms[endTime].Action == AlarmAction.DutyEnd))
                                    {
                                        //Already happen, act as if it is
                                        AlarmRaised(alarm, startTime);
                                        alarms.Remove(startTime);
                                        if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Now"));
                                        return;
                                    }
                                }
                            }


                        }
                    }
                }
            }
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("Now"));
        }

        public bool AddAlarm(AlarmInfo info, DateTime time)
        {
            TimeSpan timeString = new TimeSpan(time.Hour, time.Minute, 0);
            if (!alarms.ContainsKey(timeString))
            {
                alarms.Add(timeString, info);
                return true;
            }
            else return false;
        }

        public void ResetAllAlarms()
        {
            alarms.Clear();
        }

        public void RemoveAlarms(string description)
        {
        }

        public void RemoveAlarm(DateTime dateTime, string description)
        {
 
        }

        public void TriggerEvent()
        {
            timer_Elapsed(this, null);
        }
      
    }

}
