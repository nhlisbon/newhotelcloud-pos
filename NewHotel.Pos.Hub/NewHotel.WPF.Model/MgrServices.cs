﻿using System.Linq;
using System.Collections.Specialized;
using System.Configuration;

namespace NewHotel.WPF.Model
{
    public class MgrServices
    {
        private const string PrinterSettingsSection = "printerSettings";
        private const string AppSettingsSection = "appSettings";

        private static NameValueCollection AppSettings = ConfigurationManager.AppSettings;
        private static NameValueCollection PrinterSettings = ConfigurationManager.GetSection(PrinterSettingsSection) as NameValueCollection;

        public static void SetConfigurationSetting(string settingName, string settingValue, bool noOverride = false)
        {
            if (noOverride)
            {
                try
                {
                    if (ConfigurationManager.AppSettings.Get(settingName) != null) return;
                }
                catch
                {
                }
            }

            var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configuration.AppSettings.Settings;

            var setting = settings[settingName];
            if (setting != null)
                setting.Value = settingValue;
            else
                settings.Add(settingName, settingValue);

            configuration.Save(ConfigurationSaveMode.Modified);

            AppSettings.Set(settingName, settingValue);

            ConfigurationManager.RefreshSection(AppSettingsSection);
        }

        public static void SetPrinterSettings(string settingName, string settingValue, bool noOverride = false)
        {
            if (noOverride)
            {
                try
                {
                    if (PrinterSettings.Get(settingName) != null) return;
                }
                catch
                {
                }
            }

            var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = ((AppSettingsSection)configuration.GetSection(PrinterSettingsSection)).Settings;

            var setting = settings[settingName];
            if (setting != null)
                setting.Value = settingValue;
            else
                settings.Add(settingName, settingValue);

            configuration.Save(ConfigurationSaveMode.Modified);

            PrinterSettings.Set(settingName, settingValue);

            ConfigurationManager.RefreshSection(PrinterSettingsSection);
        }

        public static string GetConfigurationSetting(string settingName)
        {
            try
            {
                if (AppSettings.AllKeys.Contains(settingName))
                    return AppSettings.Get(settingName);

                if (PrinterSettings.AllKeys.Contains(settingName))
                    return PrinterSettings.Get(settingName);
            }
            catch
            {
            }

            return string.Empty;
        }

        public static void RemoveConfigurationSetting(string settingName)
        {
            try
            {
                if (AppSettings.AllKeys.Contains(settingName))
                    AppSettings.Remove(settingName);

                if (PrinterSettings.AllKeys.Contains(settingName))
                    PrinterSettings.Remove(settingName);
            }
            catch
            {
            }
        }

        public static string GetPrinterSettings(string settingName)
        {
            try
            {
                if (PrinterSettings.AllKeys.Contains(settingName))
                    return PrinterSettings.Get(settingName);
            }
            catch
            {
            }

            return string.Empty;
        }

        public class Settings
        {
            public const string SaloonZoomValue = "SaloonZoomValue";
            public const string FiscalPrinterEndpointName = "NetTcpBinding_INHFiscalPrinterService";
        }
    }
}