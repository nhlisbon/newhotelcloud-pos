﻿using NewHotel.Payments.Device.Processors;
using PayClient;
using System;
using System.Collections.Generic;

namespace NewHotel.WPF.App.Pos
{
	public static class ExternalPaymentsSupport
	{
		static ITerminalSystems terminalSystems;

		public static void AddTerminalSystems(params IEnumerable<IPayMessageProcessor> processors)
		{
			terminalSystems = new TerminalSystems(processors);
		}

		public static ITerminalSystems GetTerminalSystems()
		{
			return terminalSystems;
		}
	}
}
