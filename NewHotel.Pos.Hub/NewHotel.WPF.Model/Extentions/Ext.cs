﻿using System;
using NewHotel.Contracts;
using NewHotel.Pos.PrinterDocument.Simple;
using NewHotel.WPF.App.Pos.Model;

namespace NewHotel.WPF.Model.Extentions
{
    public static class Ext
    {
        public static Printer GetPrinter(this PrintersConfiguration settings, POSDocumentType documentType = POSDocumentType.Ticket)
        {
            Printer result = null;

            try
            {
                if (settings != null)
                {
                    string serverPrinter, namePrinter;

                    switch (documentType)
                    {
                        case POSDocumentType.TableBalance:
                        case POSDocumentType.Ticket:
                            serverPrinter = settings.TicketServer;
                            namePrinter = settings.TicketDescription;
                            break;
                        case POSDocumentType.Ballot:
                            serverPrinter = settings.BallotServer;
                            namePrinter = settings.BallotDescription;
                            break;
                        case POSDocumentType.Invoice:
                        case POSDocumentType.CashInvoice:
                        case POSDocumentType.CashCreditNote:
                            serverPrinter = settings.InvoiceServer;
                            namePrinter = settings.InvoiceDescription;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(documentType), documentType, null);
                    }

                    if (!string.IsNullOrEmpty(serverPrinter) && !string.IsNullOrEmpty(namePrinter))
                    {
                        result = new Printer
                        {
                            Server = serverPrinter,
                            Name = namePrinter,
                            Setting = new PrinterSetting
                            {
                                LeftMargin = Hotel.Instance.GetMarginPrinter(namePrinter, Hotel.Margin.Left),
                                RigthMargin = Hotel.Instance.GetMarginPrinter(namePrinter, Hotel.Margin.Right),
                                TopMargin = Hotel.Instance.GetMarginPrinter(namePrinter, Hotel.Margin.Top),
                                BottomMargin = Hotel.Instance.GetMarginPrinter(namePrinter, Hotel.Margin.Bottom),
                                LineFontSize = Hotel.Instance.GetLineFontSize(namePrinter)
                            }
                        };
                    }
                }
            }
            catch
            {
                // ignored
            }

            return result;
        }

        public static Printer GetPrinter(this Area area)
        {
            var serverPrinter = area.PrinterServer;
            var namePrinter = area.PrinterDescription;
            return new Printer
            {
                Server = serverPrinter,
                Name = namePrinter,
                Setting = new PrinterSetting
                {
                    LeftMargin = Hotel.Instance.GetMarginPrinter(namePrinter, Hotel.Margin.Left),
                    RigthMargin = Hotel.Instance.GetMarginPrinter(namePrinter, Hotel.Margin.Right),
                    TopMargin = Hotel.Instance.GetMarginPrinter(namePrinter, Hotel.Margin.Top),
                    BottomMargin = Hotel.Instance.GetMarginPrinter(namePrinter, Hotel.Margin.Bottom),
                    LineFontSize = Hotel.Instance.GetLineFontSize(namePrinter),
                    NoteFontSize = Hotel.GetNoteFontSize(namePrinter),
                    ActionFontSize = Hotel.GetActionFontSize(namePrinter),
                    SaloonFontSize = Hotel.GetSaloonFontSize(namePrinter)
                }
            };
        }
    }
}