﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Spooler.Service
{
	internal static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main(string[] args)
		{
			if (Environment.UserInteractive)
			{
				var interactiveService = new SpoolerSrv();
				interactiveService.RunInteractive(args);
			}
			else
			{
				ServiceBase[] ServicesToRun;
				ServicesToRun = new ServiceBase[] { new SpoolerSrv() };
				ServiceBase.Run(ServicesToRun);
			}
		}
	}
}
