﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace NewHotel.Pos.Spooler.Service
{
	public partial class SpoolerSrv : ServiceBase
	{
		const string baseAddress = "http://localhost:13010/spooler";

		ServiceHost host;
		public SpoolerSrv()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			host = new ServiceHost(typeof(SpoolerService), new Uri(baseAddress));

			// Create binding
			var binding = new BasicHttpBinding(BasicHttpSecurityMode.None)
			{
				MaxReceivedMessageSize = int.MaxValue,
				MaxBufferSize = int.MaxValue,
				MaxBufferPoolSize = int.MaxValue
			};

			// Create quotas
			var readerQuotas = new XmlDictionaryReaderQuotas
			{
				MaxStringContentLength = int.MaxValue,
				MaxArrayLength = int.MaxValue,
				MaxBytesPerRead = int.MaxValue,
				MaxDepth = int.MaxValue,
				MaxNameTableCharCount = int.MaxValue
			};

			// Setting quotas on a BindingElement after the binding is created has no effect on that binding.
			// See: http://stackoverflow.com/questions/969479/modify-endpoint-readerquotas-programatically
			binding.GetType().GetProperty("ReaderQuotas").SetValue(binding, readerQuotas, null);
			binding.ReceiveTimeout = new TimeSpan(0, 5, 0);
			binding.SendTimeout = new TimeSpan(0, 5, 0);

			// Add the service endpoint
			var ep = host.AddServiceEndpoint(typeof(ISpoolerService), binding, baseAddress);

			// Increase the MaxItemsInObjectGraph quota for all operations in this service
			foreach (var operation in ep.Contract.Operations)
				operation.Behaviors.Find<DataContractSerializerOperationBehavior>().MaxItemsInObjectGraph = int.MaxValue;

			// Enable metadata publishing.
			var smb = new ServiceMetadataBehavior();
			smb.HttpGetEnabled = true;
			smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;

			host.Open();
		}

		protected override void OnStop()
		{
			((IDisposable)host).Dispose();
		}

		public void RunInteractive(string[] args)
		{
			OnStart(args);
			Console.WriteLine("Running ...");
			Console.WriteLine("Press enter to stop ...");
			Console.ReadLine();
			OnStop();
		}
	}
}
