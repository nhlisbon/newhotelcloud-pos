﻿using System;
using System.Data;
using System.Collections.Generic;
using NewHotel.DataAnnotations;

namespace NewHotel.Core
{
    public abstract class QueryReader : IDataReader, IQueryRecord
    {
        #region Members

        protected readonly IDataReader Reader;
        private object[] _values;

        #endregion
        #region Constructor

        public QueryReader(IDataReader reader)
        {
            Reader = reader;
        }

        #endregion
        #region Private Properties & Methods

        private object[] Values
        {
            get
            {
                if (_values == null)
                    _values = new object[FieldCount];

                return _values;
            }
        }

        private void ClearValues()
        {
            if (_values != null)
                _values = null;
        }

        private object ValueAs(Type type, object value)
        {
            if (type.Equals(typeof(Guid)))
            {
                if (value is byte[])
                    value = new Guid((byte[])value);
                else
                    value = new Guid(value.ToString());
            }
            if (type.Equals(typeof(bool)))
            {
                var str = value.ToString();
                value = str.Length > 0 && str[0].Equals('1');
            }
            else if (type.Equals(typeof(ARGBColor)))
            {
                var color = (int)Convert.ChangeType(value, typeof(int));
                value = new ARGBColor(color);
            }
            else if (!type.IsEnum && value is IConvertible)
            {
                try
                {
                    value = Convert.ChangeType(value, type);
                }
                catch
                {
                }
            }
            else if (type.IsEnum)
                value = Enum.ToObject(type, Convert.ChangeType(value, Enum.GetUnderlyingType(type)));

            return value;
        }

        #endregion
        #region Protected Methods

        protected abstract Type GetTypeFromMetadata(Type type, string name, object[] metadata, out bool isNullable);

        #endregion
        #region IDataReader Members

        public void Close()
        {
            ClearValues();
            Reader.Close();
        }

        public int Depth
        {
            get { return Reader.Depth; }
        }

        public DataTable GetSchemaTable()
        {
            return Reader.GetSchemaTable();
        }

        public bool IsClosed
        {
            get { return Reader.IsClosed; }
        }

        public bool NextResult()
        {
            ClearValues();
            return Reader.NextResult();
        }

        public bool Read()
        {
            ClearValues();
            return Reader.Read();
        }

        public int RecordsAffected
        {
            get { return Reader.RecordsAffected; }
        }

        #endregion
        #region IDataRecord Members

        public int FieldCount
        {
            get { return Reader.FieldCount; }
        }

        public bool GetBoolean(int i)
        {
            return Reader.GetBoolean(i);
        }

        public byte GetByte(int i)
        {
            return Reader.GetByte(i);
        }

        public long GetBytes(int i, long fieldoffset, byte[] buffer, int bufferoffset, int length)
        {
            return Reader.GetBytes(i, fieldoffset, buffer, bufferoffset, length);
        }

        public char GetChar(int i)
        {
            return Reader.GetChar(i);
        }

        public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            return Reader.GetChars(i, fieldoffset, buffer, bufferoffset, length);
        }

        public IDataReader GetData(int i)
        {
            return Reader.GetData(i);
        }

        public string GetDataTypeName(int i)
        {
            return Reader.GetDataTypeName(i);
        }

        public DateTime GetDateTime(int i)
        {
            return Reader.GetDateTime(i);
        }

        public decimal GetDecimal(int i)
        {
            return Reader.GetDecimal(i);
        }

        public double GetDouble(int i)
        {
            return Reader.GetDouble(i);
        }

        public Type GetFieldType(int i)
        {
            return Reader.GetFieldType(i);
        }

        public float GetFloat(int i)
        {
            return Reader.GetFloat(i);
        }

        public Guid GetGuid(int i)
        {
            return Reader.GetGuid(i);
        }

        public short GetInt16(int i)
        {
            return Reader.GetInt16(i);
        }

        public int GetInt32(int i)
        {
            return Reader.GetInt32(i);
        }

        public long GetInt64(int i)
        {
            return Reader.GetInt64(i);
        }

        public string GetName(int i)
        {
            return Reader.GetName(i);
        }

        public int GetOrdinal(string name)
        {
            return Reader.GetOrdinal(name);
        }

        public string GetString(int i)
        {
            return Reader.GetString(i);
        }

        public object GetValue(int i)
        {
            object value = null;
            if (_values != null)
                value = _values[i];

            return value ?? Reader.GetValue(i);
        }

        public int GetValues(object[] values)
        {
            var count = Reader.GetValues(values);
            if (_values != null)
            {
                for (int i = 0; i < count; i++)
                {
                    if (_values[i] != null)
                        values[i] = Values[i];
                }
            }

            return count;
        }

        public bool IsDBNull(int i)
        {
            if (_values != null)
                return _values[i] == DBNull.Value;
            else
                return Reader.IsDBNull(i);
        }

        object IDataRecord.this[string name]
        {
            get { return this[name]; }
        }

        object IDataRecord.this[int i]
        {
            get { return this[i]; }
        }

        #endregion
        #region IQueryRecord Members

        public object this[int index]
        {
            get { return GetValue(index); }
            set { Values[index] = value; }
        }

        public object this[string name]
        {
            get { return GetValue(GetOrdinal(name)); }
            set { SetValue(name, value); }
        }

        public bool IsDBNull(string name)
        {
            return IsDBNull(GetOrdinal(name));
        }

        public void SetValue(int index, object value)
        {
            Values[index] = value;
        }

        public void SetValue(string name, object value)
        {
            SetValue(GetOrdinal(name), value);
        }

        public object GetValue(string name)
        {
            var index = GetOrdinal(name);
            return Values[index];
        }

        public virtual object ValueAs(Type type, string name)
        {
            var value = this[name];
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                if (value == null || value == DBNull.Value)
                    value = null;
                else
                {
                    var genericType = type.GetGenericArguments()[0];
                    if (value.GetType() != genericType)
                        value = ValueAs(genericType, value);
                }
            }
            else
            {
                if (value == null || value == DBNull.Value)
                {
                    if (type.IsValueType)
                        value = Activator.CreateInstance(type);
                    else
                        value = null;
                }
                else if (value.GetType() != type)
                    value = ValueAs(type, value);
            }

            return value;
        }

        #endregion
        #region IRecord Members

        public int Index
        {
            get { return RecordsAffected; }
        }

        public string[] Fields
        {
            get 
            {
                var fields = new List<string>();
                for (int i = 0; i < Reader.FieldCount; i++)
                    fields.Add(Reader.GetName(i));

                return fields.ToArray();
            }
        }

        object IRecord.this[int index]
        {
            get
            {
                return this[index];
            }
            set
            {
                this[index] = value;
            }
        }

        object IRecord.this[string name]
        {
            get
            {
                return this[name];
            }
            set
            {
                this[name] = value;
            }
        }

        public bool Contains(string name)
        {
            int index;
            try
            {
                index = Reader.GetOrdinal(name);
            }
            catch (IndexOutOfRangeException)
            {
                index = -1;
            }

            return index >= 0;
        }

        public int Count 
        {
            get { return FieldCount; }
        }

        public object Data
        {
            get { return null; }
            set { }
        }

        bool IRecord.IsDBNull(string name)
        {
            int index = GetOrdinal(name);
            return IsDBNull(index);
        }

        public Type GetDataType(string name)
        {
            var index = GetOrdinal(name);
            var metadata = Reader.GetSchemaTable().Rows[index].ItemArray;

            // Type
            var type = Reader.GetFieldType(index);
            // DataTypeName
            var typeName = Reader.GetDataTypeName(index).ToUpper();

            bool isNullable;
            return GetTypeFromMetadata(type, typeName, metadata, out isNullable);
        }

        public object[] ToArray()
        {
            var values = new List<object>();
            for (int i = 0; i < Count; i++)
                values.Add(this[i]);

            return values.ToArray();
        }

        public abstract IRecord Clone();

        #endregion
        #region IEnumerable Members

        public System.Collections.IEnumerator GetEnumerator()
        {
            return ToArray().GetEnumerator();
        }

        #endregion
        #region IDisposable Members

        public void Dispose()
        {
            if (Reader != null)
                Reader.Dispose();
        }

        #endregion
    }
}
