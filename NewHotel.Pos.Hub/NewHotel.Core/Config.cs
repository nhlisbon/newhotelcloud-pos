﻿using System;

namespace NewHotel.Core
{
    [Serializable]
	public abstract class Config
	{
		protected object this[string name]
		{
			set { NewHotelAppContext.OperationData[name] = value; }
		}

        protected void Remove(string name)
        {
            NewHotelAppContext.OperationData.Remove(name);
        }

		protected bool TryGetContract<T>(string name, out T contract)
			where T : class
		{
			contract = null;
			object obj;
			if (NewHotelAppContext.OperationData.TryGetValue(name, out obj))
			{
				contract = obj as T;
				return contract != null;
			}

			return false;
		}
	}
}