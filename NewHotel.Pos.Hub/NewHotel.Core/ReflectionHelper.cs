﻿using System;
using System.Reflection;
using System.IO;

namespace NewHotel.Core
{
    public static class ReflectionHelper
    {
        public static Type TypeFromAssembly(AssemblyQualifiedTypeName name, string path, bool throwOnError)
        {
            try
            {
                // Try to get the type from an already loaded assembly
                var type = Type.GetType(name.ToString());

                if (type != null)
                    return type;

                if (name.Assembly == null)
                {
                    // No assembly was specified for the type, so just fail
                    if (throwOnError) throw new TypeLoadException("Could not load type " + name + ". No assembly name specified.");
                    return null;
                }

                Assembly assembly = null;
                if (path != string.Empty)
                {
                    try
                    {
                        assembly = Assembly.LoadFrom(Path.GetFullPath(path) + Path.DirectorySeparatorChar + name.Assembly + ".dll");
                    }
                    catch (FileNotFoundException)
                    {
                        assembly = null;
                    }
                }
                else
                    assembly = Assembly.Load(name.Assembly);

                if (assembly == null)
                {
                    if (throwOnError) throw new TypeLoadException("Could not load type " + name + ". Incorrect assembly name specified.");
                    return null;
                }

                type = assembly.GetType(name.ToString());

                if (type == null)
                {
                    if (throwOnError) throw new TypeLoadException("Could not load type " + name + ".");
                    return null;
                }

                return type;
            }
            catch (Exception e)
            {
                if (throwOnError)
                    throw e;
                return null;
            }
        }

        public static Type TypeFromAssembly(AssemblyQualifiedTypeName name, bool throwOnError)
        {
            return TypeFromAssembly(name, string.Empty, throwOnError);
        }
    }
}