﻿using System;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace NewHotel.Core
{
    public static class JsonSerialization
    {
        private sealed class ObjectDictionaryJsonConverter : CustomCreationConverter<IDictionary<string, object>>
        {
            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                var obj = (IDictionary<string, object>)base.ReadJson(reader, objectType, existingValue, serializer);
                foreach (var key in obj.Keys.ToArray())
                {
                    var value = obj[key];
                    var str = value as string;
                    if (value != null)
                    {
                        Guid guid;
                        if (Guid.TryParseExact(str, "D", out guid))
                            obj[key] = guid;
                    }
                }

                return obj;
            }

            public override IDictionary<string, object> Create(Type objectType)
            {
                return new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);
            }
        }

        private sealed class ObjectListJsonConverter : CustomCreationConverter<IList<object>>
        {          
            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                var obj = (IList<object>)base.ReadJson(reader, objectType, existingValue, serializer);
                for (int index = 0; index < obj.Count; index++)
                {
                    var value = obj[index];
                    var str = value as string;
                    if (value != null)
                    {
                        Guid guid;
                        if (Guid.TryParseExact(str, "D", out guid))
                            obj[index] = guid;
                    }
                }

                return obj;
            }

            public override IList<object> Create(Type objectType)
            {
                return new List<object>();
            }
        }

        public static string ToJson<T>(this T obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.Indented);
        }

        public static T FromJson<T>(this string json)
        {
            return JsonConvert.DeserializeObject<T>(json, new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.None,
                Converters = new List<JsonConverter>() { new ObjectDictionaryJsonConverter(), new ObjectListJsonConverter() }
            });
        }
    }
}