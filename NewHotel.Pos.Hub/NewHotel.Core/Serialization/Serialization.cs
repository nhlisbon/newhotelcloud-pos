﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace NewHotel.Core
{
    public static class Serialization
    {
        private const int maxItemsObjectGraph = int.MaxValue;
        private static IList<Type> Types = new List<Type>();
        private static readonly JsonSerializerSettings jsonSettings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };

		public static void Register(Type type)
        {
            if (!Types.Contains(type))
                Types.Add(type);
        }

        public static string Serialize<T>(this T obj, IDataContractSurrogate dataContractSurrogate)
        {
            try
            {
                var type = obj.GetType();
                var serializer = new DataContractSerializer(type,
                    GetSubclasesOf(type), maxItemsObjectGraph, false, false, dataContractSurrogate);

                using (var stream = new MemoryStream())
                {
                    try
                    {
                        serializer.WriteObject(stream, obj);
                        return Encoding.Default.GetString(stream.ToArray());
                    }
                    finally
                    {
                        stream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error serializing object.", ex);
            }
        }

        public static string ToJson<T>(this T obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public static string ToJson<T>(this T obj, bool ignoreNull)
        {
            if (ignoreNull) return JsonConvert.SerializeObject(obj, jsonSettings);
            else return JsonConvert.SerializeObject(obj);
        }

        public static string ToJson(this object obj, bool ignoreNull)
        {
            if (ignoreNull) return JsonConvert.SerializeObject(obj, jsonSettings);
            else return JsonConvert.SerializeObject(obj);
        }

        public static object Deserialize(this Type type, string xml, IDataContractSurrogate dataContractSurrogate)
        {
            try
            {
                var serializer = new DataContractSerializer(type,
                    GetSubclasesOf(type), maxItemsObjectGraph, false, false, dataContractSurrogate);

                using (var reader = new StringReader(xml))
                {
                    using (var xmlReader = XmlReader.Create(reader))
                    {
                        return serializer.ReadObject(xmlReader);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error deserializing object to xml.", ex);
            }
        }

        public static T Deserialize<T>(this string xml, IDataContractSurrogate dataContractSurrogate)
        {
            return (T)Deserialize(typeof(T), xml, dataContractSurrogate);
        }

        public static object FromJSON(this Type type, string json)
        {
            return JsonConvert.DeserializeObject(json, type);
        }

        public static T FromJSON<T>(this string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static IEnumerable<Type> GetSubclasesOf(Type type)
        {
            var assembly = Assembly.GetExecutingAssembly();

            var types = new List<Type>();
            types.AddRange(assembly.GetTypes()
                .Where(t => !t.IsGenericType && !t.IsAbstract && t.IsSubclassOf(type)));

            return types;
        }

        public static T Download_Serialized_JsonData<T>(string url) where T : new()
        {
            using (var w = new WebClient())
            {
                var json_data = string.Empty;
                try
                {
                    json_data = w.DownloadString(url);
                }
                catch (Exception) { }
                return !string.IsNullOrEmpty(json_data) ? JsonConvert.DeserializeObject<T>(json_data) : new T();
            }
        }
    }
}