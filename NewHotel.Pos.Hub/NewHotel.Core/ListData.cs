﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Diagnostics;
using NewHotel.DataAnnotations;

namespace NewHotel.Core
{
    /// <summary>
    /// Definición de una columna
    /// </summary>
    public sealed class ColumnDefinition
    {
        public readonly string Name;
        public readonly Type Type;
        public readonly int Index;
        public readonly string DisplayName;
        public readonly Func<object, object> Converter;

        public ColumnDefinition(string name, Type type, int index, string displayName, Func<object, object> converter)
        {
            Name = name;
            Type = type;
            Index = index;
            DisplayName = displayName;
            Converter = converter;
        }

        public ColumnDefinition(string name, Type type, int index, string displayName)
            : this(name, type, index, displayName, (value) => { return value; })
        {
        }

        public ColumnDefinition(string name, Type type, int index, Func<object, object> converter)
            : this(name, type, index, name, converter)
        {
        }

        public ColumnDefinition(string name, Type type, int index)
            : this(name, type, index, name, (value) => { return value; })
        {
        }
    }

    public sealed class QueryPropertyDescriptor : PropertyDescriptor
    {
        private int _index;
        private Type _type;
        private string _dispName;

        public QueryPropertyDescriptor(string name, Type type, int index, string dispName)
            : base(name, null)
        {
            _index = index;
            _type = type;
            _dispName = dispName;
        }

        public QueryPropertyDescriptor(string name, Type type, int index)
            : this(name, type, index, name)
        {
        }

        public int Index
        {
            get { return _index; }
        }

        public override string DisplayName
        {
            get { return _dispName; }
        }

        public override bool CanResetValue(object component)
        {
            return false;
        }

        public override Type ComponentType
        {
            get { return typeof(Record); }
        }

        public override Type PropertyType
        {
            get { return _type; }
        }

        public override bool IsReadOnly
        {
            get { return true; }
        }

        public override object GetValue(object component)
        {
            return ((Record)component)[_index];
        }

        public override void SetValue(object component, object value)
        {
            ((Record)component)[_index] = value;
        }

        public override void ResetValue(object component)
        {
        }

        public override bool ShouldSerializeValue(object component)
        {
            return true;
        }
    }

    [CollectionDataContract]
    public class Record : IRecord
    {
        private readonly int _index;
        private readonly IDictionary<string, QueryPropertyDescriptor> _desc;
        private readonly object[] _values;

        public Record() { }

        public Record(IDictionary<string, QueryPropertyDescriptor> desc, object[] values, int index)
        {
            _desc = desc;
            _values = values;
            _index = index;
        }

        public Record(IRecord record, IDictionary<string, QueryPropertyDescriptor> desc)
        {
            _desc = desc;
            _values = record.ToArray();
            _index = record.Index;
        }

        public static implicit operator object[](Record record)
        {
            return record.ToArray();
        }

        [IgnoreDataMember]
        public int Index
        {
            get { return _index; }
        }

        [IgnoreDataMember]
        public string[] Fields
        {
            get { return _desc.Keys.ToArray(); }
        }

        [IgnoreDataMember]
        public object this[int index]
        {
            get { return _values[index]; }
            set { _values[index] = value; }
        }

        private PropertyDescriptor Find(string name)
        {
            return _desc.Where(x => x.Key.Equals(name.ToUpper()))
                .Select(x => x.Value).FirstOrDefault();
        }

        [IgnoreDataMember]
        public object this[string name]
        {
            get
            {
                PropertyDescriptor desc = Find(name);

                if (desc != null)
                    return desc.GetValue(this);
                else
                    throw new ArgumentOutOfRangeException(name, "Invalid column name");
            }
            set
            {
                PropertyDescriptor desc = Find(name);

                if (desc != null)
                    desc.SetValue(this, value);
                else
                    throw new ArgumentOutOfRangeException(name, "Invalid column name");
            }
        }

        public bool Contains(string name)
        {
            return Find(name) != null;
        }

        public int Count
        {
            get { return _desc.Count; }
        }

        private object ValueAs(Type type, object value)
        {
            if (type.Equals(typeof(bool)))
            {
                if (value is bool)
                    value = (bool)value;
                else
                {
                    var str = value.ToString();
                    value = str.Length > 0 && str[0].Equals('1');
                }
            }
            else if (type.Equals(typeof(ARGBColor)))
            {
                int color = (int)Convert.ChangeType(value, typeof(int));
                value = new ARGBColor(color);
            }
            else if (!type.IsEnum && value is IConvertible)
            {
                try
                {
                    value = Convert.ChangeType(value, type);
                }
                catch
                { 
                }
            }
            else if (type.IsEnum)
                value = Enum.ToObject(type, Convert.ChangeType(value, Enum.GetUnderlyingType(type)));

            return value;
        }

        public virtual object ValueAs(Type type, string name)
        {
            object value = this[name];
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                if (value == null || value == DBNull.Value)
                    return null;
                else
                {
                    Type genericType = type.GetGenericArguments()[0];
                    if (value.GetType() != genericType)
                        value = ValueAs(genericType, value);

                    return value;
                }
            }
            else
            {
                if (value == null || value == DBNull.Value)
                {
                    if (type.IsValueType)
                        return Activator.CreateInstance(type);
                    else
                        return null;
                }
                else
                    return ValueAs(type, value);
            }
        }

        public bool IsDBNull(string name)
        {
            object value = this[name];
            return value == DBNull.Value;
        }

        public Type GetDataType(string name)
        {
            PropertyDescriptor pd = Find(name);
            if (pd != null)
                return pd.PropertyType;

            return null;
        }

        public object[] ToArray()
        {
            var values = new object[_values.Length];
            Array.Copy(_values, values, _values.Length);
            return values;
        }
        
        public IRecord Clone()
        {
            List<object> values = new List<object>(_values);
            return new Record(_desc, values.ToArray(), _index);
        }

        public override string ToString()
        {
            return "Record(" + _index.ToString() + ") " +
                string.Join(", ", _desc.Select(x => x.Key + "=" + (x.Value.GetValue(this) ?? "NULL").ToString()).ToArray());
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return _desc.Select(x => x.Value.GetValue(this)).GetEnumerator();
        }

        private void Add(object obj) { }

        #endregion
    }

    /// <summary>
    /// Clase utilitaria para ser usada en el acceso a datos de los controles,
    /// implementa IList e ITypedList.
    /// </summary>
    /// <remarks>Esta clase debe ser abstracta</remarks>
    [DebuggerDisplay("{Count} records")]
    public abstract class ListData<T> : ITypedList, IList, IList<T>
        where T : IRecord
    {
        protected string _listName;
        protected IList<T> _list;
        protected IDictionary<string, QueryPropertyDescriptor> _props;
        protected int _recordNumber;

        public ListData() { }

        private ListData(string listName, IDictionary<string, QueryPropertyDescriptor> props)
        {
            _listName = listName;
            _props = props;
        }

        protected ListData(string listName, IDictionary<string, QueryPropertyDescriptor> props, T[] list)
            : this(listName, props)
        {
            _list = list;
            _recordNumber = _list.Count > 0 ? 0 : -1;
        }

        protected ListData(string listName, IDictionary<string, QueryPropertyDescriptor> props, IList<T> list)
            : this(listName, props)
        {
            _list = list;
            _recordNumber = _list.Count > 0 ? 0 : -1;
        }

        protected ListData(string listName, ListData listData)
            : this(listName, listData._props)
        {
            _list = new List<T>();
            _recordNumber = -1;
        }

        public ListData(string listName, ColumnDefinition[] colDefs, IList list)
        {
            _listName = listName;

            _props = new Dictionary<string, QueryPropertyDescriptor>(StringComparer.InvariantCultureIgnoreCase);
            foreach (ColumnDefinition colDef in colDefs)
                _props.Add(colDef.Name.ToUpper(), GetPropertyDescriptor(colDef.Index, colDef));

            _list = list.Cast<object[]>().Select((x,i) => BuildRecord(_props, x, i))
                .ToList();
            _recordNumber = _list.Count > 0 ? 0 : -1;
        }

        private T BuildRecord(IDictionary<string, QueryPropertyDescriptor> desc, object[] values, int index)
        {
            return CreateRecord(desc, values, index);
        }

        protected abstract T CreateRecord(IDictionary<string, QueryPropertyDescriptor> desc, object[] values, int index);
        protected abstract T CreateRecord(IRecord record, IDictionary<string, QueryPropertyDescriptor> desc);

        private IList<T> List
        {
            get { return _list; }
        }

        public string Name
        {
            get { return _listName; }
        }

        public bool Locate(Func<T, bool> predicate)
        {
            T record = this.FirstOrDefault(predicate);
            _recordNumber = record == null ? -1 : record.Index;

            return _recordNumber != -1;
        }

        public object this[string name, int index]
        {
            get
            {
                if (index >= 0 && index < _list.Count)
                {
                    QueryPropertyDescriptor desc;
                    if (!_props.TryGetValue(name, out desc))
                        throw new ArgumentOutOfRangeException("Invalid column name");

                    return desc.GetValue(_list[index]);
                }
                else
                    throw new IndexOutOfRangeException("Record number out of bounds");
            }
        }

        public int RecordNumber
        {
            get { return _recordNumber; }
        }

        public bool First()
        {
            if (_list.Count != 0)
            {
                _recordNumber = 0;
                return true;
            }
                
            _recordNumber = -1;
            return false;
        }

        public bool Last()
        {
            if (_list.Count != 0)
            {
                _recordNumber = _list.Count - 1;
                return true;
            }

            _recordNumber = -1;
            return false;
        }

        public bool Previous()
        {
            if (_recordNumber > 0)
            {
                _recordNumber--;
                return true;
            }

            _recordNumber = -1;
            return false;
        }

        public bool Next()
        {
            if (_recordNumber < (_list.Count - 1))
            {
                _recordNumber++;
                return true;
            }

            _recordNumber = -1;
            return false;
        }

        public bool Goto(int index)
        {
            if (index >= 0 && index < _list.Count)
            {
                _recordNumber = index;
                return true;
            }

            _recordNumber = -1;
            return false;
        }

        public bool IsNull(string name)
        {
            object value = Value(name);
            return value == null || value.Equals(DBNull.Value);
        }

        public object Value(string name)
        {
            return this[name, _recordNumber];
        }

        public R ValueAs<R>(string name)
        {
            var value = Value(name);
            var type = typeof(R);
            if (value != null &&
                type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                var argType = type.GetGenericArguments()[0];
                if (argType.IsEnum && value.GetType().Equals(typeof(long)))
                    value = Enum.ToObject(argType, value);
                else if (argType.Equals(typeof(bool)) && value.GetType().Equals(typeof(string)))
                    value = value.ToString().Equals("1");
                else if (value.GetType() != argType)
                    value = Convert.ChangeType(value, argType);

                return (R)Activator.CreateInstance(type, value);
            }
            else
            {
                if (value == null)
                    return default(R);
                else
                {
                    if (type.Equals(typeof(bool)) && value.GetType().Equals(typeof(string)))
                        value = value.ToString().Equals("1");
                    else
                    {
                        if (type.IsEnum)
                            type = typeof(long);
                        try
                        {
                            value = Convert.ChangeType(value, type);
                        }
                        catch (InvalidCastException)
                        {
                        }
                    }

                    return (R)value;
                }
            }
        }

        protected QueryPropertyDescriptor GetPropertyDescriptor(int index, ColumnDefinition colDef)
        {
            return new QueryPropertyDescriptor(colDef.Name, colDef.Type, index, colDef.DisplayName);
        }

        public IEnumerable<string> Cols
        {
            get { return _props.Select(x => x.Key); }
        }

        public int IndexOf(string column)
        {
            int index = 0;
            foreach (var col in Cols)
            {
                if (string.Compare(col, column, true) == 0)
                    return index;
                index++;
            }

            return -1;
        }

        public bool IsEmpty
        {
            get { return _list.Count == 0; }
        }

        #region ITypedList Members

        public PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors)
        {
            if (listAccessors == null)
            {
                return new PropertyDescriptorCollection(_props.Values.ToArray());
            }

            return null;
        }

        public string GetListName(PropertyDescriptor[] listAccessors)
        {
            return Name;
        }

        #endregion

        public T Append()
        {
            int index = List.Count;
            T record = New();
            List.Add(record);
            return record;
        }

        public T New()
        {
            return CreateRecord(_props, new object[_props.Count], 0);
        }

        #region IList Members

        public virtual int Add(object value)
        {
            int index = List.Count;
            List.Add(CreateRecord(_props, (object[])value, index));
            return index;
        }

        public virtual void Clear()
        {
            List.Clear();
        }

        public bool Contains(object value)
        {
            return List.Contains((T)value);
        }

        public int IndexOf(object value)
        {
            return List.IndexOf((T)value);
        }

        public virtual void Insert(int index, object value)
        {
            List.Insert(index, (T)value);
        }

        public bool IsFixedSize
        {
            get { return true; }
        }

        public virtual bool IsReadOnly
        {
            get { return List.IsReadOnly; }
        }

        public virtual void Remove(object value)
        {
            List.Remove((T)value);
        }

        public virtual void RemoveAt(int index)
        {
            List.RemoveAt(index);
        }

        public virtual object this[int index]
        {
            get
            {
                return List[index];
            }
            set
            {
                List[index] = (T)value;
            }
        }

        #endregion
        #region ICollection Members

        public void CopyTo(Array array, int index)
        {
            _list.ToArray().CopyTo(array, index);
        }

        public int Count
        {
            get { return _list.Count; }
        }

        public bool IsSynchronized
        {
            get { return false; }
        }

        public object SyncRoot
        {
            get { return _list; }
        }

        #endregion
        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return List.GetEnumerator();
        }

        #endregion
        #region IList<IRecord> Members

        public int IndexOf(T item)
        {
            return List.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            List.Insert(index, item);
        }

        T IList<T>.this[int index]
        {
            get
            {
                return List[index];
            }
            set
            {
                List[index] = value;
            }
        }

        #endregion
        #region ICollection<Record> Members

        public void Add(T item)
        {
            List.Add(item);
        }

        public bool Contains(T item)
        {
            return List.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            List.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            return List.Remove(item);
        }

        #endregion
        #region IEnumerable<Record> Members

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return List.GetEnumerator();
        }

        #endregion
    }

    [KnownType(typeof(Record))]
    public class ListData : ListData<IRecord>
    {
        public ListData() : base() { }

        public ListData(string listName, ColumnDefinition[] colDefs, IList list)
            : base(listName, colDefs, list) { }

        protected ListData(string listName, IDictionary<string, QueryPropertyDescriptor> props, IRecord[] list)
            : base(listName, props, list) { }

        protected ListData(string listName, IDictionary<string, QueryPropertyDescriptor> props, IList<IRecord> list)
            : base(listName, props, list) { }

        protected ListData(string listName, ListData listData)
            : base(listName, listData) { }

        protected override IRecord CreateRecord(IDictionary<string, QueryPropertyDescriptor> desc, object[] values, int index)
        {
            return new Record(desc, values, index);
        }

        protected override IRecord CreateRecord(IRecord record, IDictionary<string, QueryPropertyDescriptor> desc)
        {
            return new Record(record, desc);
        }

        public ListData Create(string name)
        {
            var props = _props.ToDictionary(y => y.Key, y => y.Value);
            ColumnDefinition[] colDefs = props
                .Select(x => new ColumnDefinition(x.Key, x.Value.PropertyType, x.Value.Index))
                .ToArray();
            return new ListData(name, props, new List<IRecord>());
        }

        public ListData Copy(Func<IRecord, bool> predicate)
        {
            IRecord[] list = _list.Where(predicate).ToArray();
            return new ListData(_listName, _props, list);
        }

        public ListData GetListView(params string[] cols)
        {
            if (cols == null || cols.Length == 0)
                return this;
            else
            {
                var props = _props.Where(x => cols.Contains(x.Key, StringComparer.InvariantCultureIgnoreCase))
                    .ToDictionary(y => y.Key, y => y.Value);
                ColumnDefinition[] colDefs = props
                    .Select(x => new ColumnDefinition(x.Key, x.Value.PropertyType, x.Value.Index))
                    .ToArray();
                IEnumerable<IRecord> list = _list.Select(x => new Record(x, props));
                return new ListData(string.Empty, props, list.ToArray());
            }
        }
    }
}
