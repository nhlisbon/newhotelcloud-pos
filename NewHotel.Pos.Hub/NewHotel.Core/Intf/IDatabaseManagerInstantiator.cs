﻿using System;

namespace NewHotel.Core
{
    public interface IDatabaseManagerInstantiator
    {
        DataProvider Provider { get; }
        string User { get; }
        string Password { get; }
        string Server { get; }
        string Database { get; }

        IDatabaseManager Create(byte poolSize);
    }
}
