﻿using System.Data;
using NewHotel.DataAnnotations;

namespace NewHotel.Core
{
    public interface IQueryRecord : IDataRecord, IRecord
    {
        new object this[int index] { get; set; }
        new object this[string name] { get; set; }
        new bool IsDBNull(string name);
        void SetValue(int index, object value);
        void SetValue(string name, object value);
        object GetValue(string name);
    }
}

