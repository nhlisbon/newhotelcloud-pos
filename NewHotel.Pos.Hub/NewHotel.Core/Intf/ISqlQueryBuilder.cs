﻿using System;

namespace NewHotel.Core
{
    public interface ISqlQueryBuilder
    {
        string[] Columns { get; }
        string[] Parameters { get; }
        bool ContainsWhere { get; }
        bool Paged { get; set; }
        ISqlQueryBuilder Append(string str);
        ISqlQueryBuilder AppendLine(string str);
    }
}
