﻿using System;

namespace NewHotel.Core
{
    public interface IDeleteStatement : IStatement
    {
        IDeleteStatement Clone();
    }
}
