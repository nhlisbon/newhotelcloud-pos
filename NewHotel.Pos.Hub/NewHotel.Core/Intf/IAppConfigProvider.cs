﻿using System;

namespace NewHotel.Core
{
    public interface IAppConfigProvider
    {
        string Provider { get; }
        string Cache { get; }
        string SessionCache { get; }
        string Context { get; }
        bool Encrypted { get; }
        byte PoolSize { get; }
        long Language { get; }
        string User { get; }
        string Password { get; }
        string Service { get; }
        string Database { get; }
        int MaxTabCount { get; }
        string ModuleName { get; }
        string PoolName { get; }
    }
}
