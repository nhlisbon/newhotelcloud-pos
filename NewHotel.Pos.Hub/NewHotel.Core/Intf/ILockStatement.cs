﻿using System;

namespace NewHotel.Core
{
    public interface ILockStatement : IStatement
    {
        ILockStatement Clone();
    }
}
