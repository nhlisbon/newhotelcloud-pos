﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace NewHotel.Core
{
    public interface ICommandLogger : IDisposable
    {
        void Executed();
        void Fetched();
        TimeSpan ElapsedTime { get; }
    }

    public interface IDbCommandCollection : IDbCommand
    {
        string Name { get; set; }
        int Index { get; }
        int Count { get; }
    }

    public interface IDbParameter : IDbDataParameter
    {
        bool IsTimestamp { get; set; }
    }

    /// <summary>
    /// Define the interface standard for database access communication
    /// </summary>
    /// <remarks>
    /// Currently, OracleDatabaseManager is the only implemented manager. Its accessible through the Database Manager abstract class. 
    /// Watch example section for use reference
    /// </remarks>
    /// <example>
    ///  IDatabaseManager manager = NewHotelAppContext.GetManager(Guid.Empty);
    /// </example>
    public interface IDatabaseManager : ICloneable, IDisposable
    {
        /// <summary>
        /// To be defined
        /// </summary>
        string TableAliasParamName { get; }
        /// <summary>
        /// To be defined
        /// </summary>
        string RowOffsetParamName { get; }
        /// <summary>
        /// To be defined
        /// </summary>
        string RowCountParamName { get; }
        /// <summary>
        /// To be defined
        /// </summary>
        string RowNumParamName { get; }
        /// <summary>
        /// To be defined
        /// </summary>
        string StartRowParamName { get; }
        /// <summary>
        /// To be defined
        /// </summary>
        string EndRowParamName { get; }
        /// <summary>
        /// Connection string to be used
        /// </summary>
        string ConnectionString { get; }
        /// <summary>
        /// To be defined
        /// </summary>
        string User { get; }
        /// <summary>
        /// To be defined
        /// </summary>
        string Password { get; }
        /// <summary>
        /// To be defined
        /// </summary>
        string DataSource { get; }
        /// <summary>
        /// To be defined
        /// </summary>
        Type GetTypeFromMetadata(Type type, string dataTypeName, object[] metadata, out bool isNullable);
        /// <summary>
        /// To be defined
        /// </summary>
        object ConvertValueType(object value);
        /// <summary>
        /// Indicates if Manager is connected or not
        /// </summary>
        bool IsConnected { get; }
        /// <summary>
        /// Current Transaction
        /// </summary>
        IDbTransaction Transaction { get; }
        /// <summary>
        /// Transaction Level
        /// </summary>
        /// <remarks>
        /// Nested transactions are allowed. This indicator will show how deep in the transaction tree is the current manager working on
        /// </remarks>
        int TransactionLevel { get; }
        /// <summary>
        /// Open Manager connection
        /// </summary>
        void Open();
        /// <summary>
        /// Begin a new transaction
        /// </summary>
        void BeginTransaction();
        /// <summary>
        /// Commits current transaction and decrease the transaction level
        /// </summary>
        void CommitTransaction();
        /// <summary>
        /// Rollback current transaction and decrease the transaction level
        /// </summary>
        void RollbackTransaction();
        /// <summary>
        /// Close Manager connection
        /// </summary>
        void Close();
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        ICommandLogger StartCommand(IDbCommand command);
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="commandName"></param>
        /// <returns></returns>
        ICommandLogger StartCommand(string commandName);
        /// <summary>
        /// To be defined
        /// </summary>
        void EndCommand(ICommandLogger commandLogger);
        /// <summary>
        /// Creates a new Command using the Manager connection specifications
        /// </summary>
        /// <param name="commandName">Name of the command</param>
        /// <param name="pageNumber">Page to be returned</param>/// 
        /// <param name="pageSize">The amount of item per page that will be used</param>
        /// <param name="commandLabel">Label of the command</param>
        /// <returns>Created Command</returns>
        IDbCommand CreateCommand(string commandName, int pageNumber, short pageSize, string commandLabel = null);
        /// <summary>
        /// Creates a new Command using the Manager connection specifications
        /// </summary>
        /// <param name="commandName">Name of the command</param>
        /// <param name="arrayBindCount">Array length to bind</param>/// 
        /// <param name="commandLabel">Label of the command</param>
        /// <returns>Created Command</returns>
        IDbCommand CreateCommand(string commandName, int arrayBindCount, string commandLabel = null);
        /// <summary>
        /// Creates a new Command using the Manager connection specifications
        /// </summary>
        /// <param name="commandName">Name of the command</param>
        /// <param name="commandLabel">Label of the command</param>
        /// <returns>Created Command</returns>
        IDbCommand CreateCommand(string commandName, string commandLabel = null);
        /// <summary>
        /// Creates a new parameter to be used with the provided command
        /// </summary>
        /// <param name="command">Command that will use the parameter</param>
        /// <returns>Created parameter</returns>
        IDbParameter CreateParameter(IDbCommand command);
        /// <summary>
        /// Creates a new parameter to be used with the provided command. Aditional values are provided
        /// </summary>
        /// <param name="command">Command that will use the parameter</param>
        /// <param name="name">Name of the parameter</param>
        /// <param name="value">Value to be assigned</param>
        /// <returns>Parameter with information already set (name and value)</returns>
        IDbParameter CreateParameter(IDbCommand command, string name, object value);
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="command"></param>
        /// <param name="name"></param>
        /// <param name="count"></param>
        /// <param name="value"></param>
		void CreateParameter(IDbCommand command, string name, int count, object[] value);
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="commandCols"></param>
        /// <returns></returns>
        string GetCommandText(string commandText, string commandCols);
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="commandText"></param>
        /// <returns></returns>
        string GetCountCommandText(string commandText);
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="commandCols"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        string GetPagedCommandText(string commandText, string commandCols, int pageNumber, short pageSize);
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="commandText1"></param>
        /// <param name="commandText2"></param>
        /// <returns></returns>
        string GetUnionCommandText(string commandText1, string commandText2);
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="type"></param>
        /// <param name="exprs"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        IQueryStatement GetQueryCommandText(Type type, LambdaExpression[] exprs, int count = 0);
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        IQueryStatement GetLoadCommandText(Type type);
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="type"></param>
        /// <param name="query"></param>
        void RegisterCustomLoadCommandText(Type type, IQueryStatement query);
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        IDictionary<string, IMergeStatement> GetMergeCommandText(Type type);
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        IDictionary<string, IDeleteStatement> GetDeleteCommandText(Type type);
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        IDictionary<string, ILockStatement> GetLockCommandText(Type type);
        /// <summary>
        /// To be defined
        /// </summary>
        string StatementLock { get; }
        /// <summary>
        /// To be defined
        /// </summary>
        string TableLock { get; }
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        string GetParameters(IEnumerable<KeyValuePair<string, object>> parameters);
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        QueryReader GetQueryReader(IDataReader reader);
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="command"></param>
        /// <param name="behavior"></param>
        /// <returns></returns>
        IDataReader ExecuteReader(IDbCommand command, CommandBehavior behavior);
        /// <summary>
        /// Executes the command, returning a DataReader for data manipulation
        /// </summary>
        /// <param name="command">Command to be executed</param>
        /// <returns>DataReader with command execution result</returns>
        IDataReader ExecuteReader(IDbCommand command);
        /// <summary>
        /// Executes the command, returning the scalar object returned by the command
        /// </summary>
        /// <param name="command">Command to be executed</param>
        /// <returns>Object with command result</returns>
        object ExecuteScalar(IDbCommand command);
        /// <summary>
        /// Executes a command and return the number of affected rows
        /// </summary>
        /// <param name="command">Command to be executed</param>
        /// <returns>Number of affected rows</returns>
        int ExecuteNonQuery(IDbCommand command);
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="commandName"></param>
        /// <returns></returns>
        int ExecuteNonQuery(string commandText, string commandName);
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        long GetSequenceNextValue(string name);
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        long GetSequenceCurrentValue(string name);
        /// <summary>
        /// To be defined
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        string GetSqlExpression(object value, Type type);
        /// <summary>
        /// To be defined
        /// </summary>
        /// <returns></returns>
        string GetSqlConcatOperator();
    }
}