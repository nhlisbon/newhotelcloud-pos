﻿using System;

namespace NewHotel.Core
{
    public interface IMergeStatement : IStatement
    {
        string TimestampName { get; }
        string FormatTimestamp(DateTime date);
        string GetTimestampFormatExpression(string paramName, string colName);
        IMergeStatement Clone();
    }
}
