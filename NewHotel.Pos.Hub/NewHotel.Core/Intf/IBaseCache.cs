﻿using System.Collections.Generic;

namespace NewHotel.Core
{
    public interface IBaseCache
    {
        IDictionary<string, object> Data { get; }
        IDictionary<string, object> CloneData();
        bool ContainsData { get; }
        void Clear();
    }
}
