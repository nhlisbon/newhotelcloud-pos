﻿using System;

namespace NewHotel.Core
{
    public interface IQueryStatement : IStatement
    {
        void ClearWhere();
        void AppendWhere(string where);
        IQueryStatement Clone();
    }
}
