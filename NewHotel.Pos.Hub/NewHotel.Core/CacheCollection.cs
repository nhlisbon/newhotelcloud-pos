﻿using System;
using System.Collections.Generic;

namespace NewHotel.Core
{
    [Serializable]
    public class CacheCollection
    {
        #region Members

        private readonly IDictionary<int, IContractTree> dict =
            new Dictionary<int, IContractTree>();

        #endregion
        #region Public Methods

        public void Add(IContractTree tree)
        {
            if (tree != null)
                dict[tree.Context] = tree;
        }

        public bool TryGetValue(int key, out IContractTree tree)
        {
            return dict.TryGetValue(key, out tree);
        }

        public bool Remove(int key)
        {
            return dict.Remove(key);
        }

        public bool Remove(IContractTree tree)
        {
            return dict.Remove(tree.Context);
        }

        public override string ToString()
        {
            return string.Format("Count={0}", dict.Count);
        }

        #endregion
    }
}
