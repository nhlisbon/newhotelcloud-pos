﻿using System;
using System.Linq;
using System.Text;
using System.Net;
using System.Data;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using System.Security.Cryptography;
using System.Diagnostics;
using NewHotel.DataAnnotations;
using NewHotel.Contracts;

namespace NewHotel.Core
{
    public class NewHotelAppContext
    {
        #region Constants

        public const string LanguageKey = "Language";
		public const string ApplicationVersionKey = "ApplicationVersion";
        public const string ApplicationIdKey = "ApplicationId";
        public const string InstallationIdKey = "InstallationId";
        public const string UserIdKey = "UserId";
        public const string UserLoginKey = "UserLogin";

        public const string AuthenticatedUserKey = "NewHotelSessionUser";
        public const string AuthenticatedPasswordKey = "NewHotelSessionPassword";

        private const string ContractsKey = "Contracts";
        private const string CurrentContextKey = "CurrentContext";
        private const string ClientDateTimeKey = "ClientDateTime";
        private const string ChannelIdKey = "ChannelId";

        private const byte MinPoolSize = 1;

        #endregion
        #region Members

        private static Cache cache;

        private static readonly IBaseCache _cache;
        private static readonly IBaseCache _sessionCache;
        private static readonly object _managerSync = new object();

        private static IDictionary<Guid, ManagerInstantiator> _managers;
        private static DataProvider _provider = DataProvider.Invalid;

        #endregion
        #region Constructors

        static Type baseCacheType = typeof(IBaseCache);

        static NewHotelAppContext()
        {
            try
            {
                var cacheTypeName = AppConfig.Instance.Cache;

                var type = typeof(DefaultContextCache);
                if (!string.IsNullOrEmpty(cacheTypeName))
                {
                    var cacheType = Type.GetType(cacheTypeName, true);
                    if (baseCacheType.IsAssignableFrom(cacheType))
                    {
                        type = cacheType;
                        Debug.Print("Using context cache: {0}", cacheTypeName);
                    }
                }

                _cache = Activator.CreateInstance(type) as IBaseCache;
            }
            catch (Exception ex)
            {
                Debug.Print("Creating context cache: {0}", ex.Message);
                throw ex;
            }

            try
            {
                var cacheTypeName = AppConfig.Instance.SessionCache;

                var type = typeof(DefaultSessionCache);
                if (!string.IsNullOrEmpty(cacheTypeName))
                {
                    var cacheType = Type.GetType(cacheTypeName, true);
                    if (baseCacheType.IsAssignableFrom(cacheType))
                    {
                        type = cacheType;
                        Debug.Print("Using session cache: {0}", cacheTypeName);
                    }
                }

                _sessionCache = Activator.CreateInstance(type) as IBaseCache;
            }
            catch (Exception ex)
            {
                Debug.Print("Creating session cache: {0}", ex.Message);
                throw ex;
            }
        }

        #endregion
        #region Methods & Properties

        public virtual string ApplicationName 
        {
            get { return string.Empty; }
        }

        public virtual bool ExistInstallation(Guid installationId)
        {
            return false;
        }

        public virtual void CleanInstallation(Guid installationId)
        {
        }

        public virtual string InstallationName 
        {
            get { return string.Empty; }
        }

        public virtual string InstallationUrl
        {
            get { return string.Empty; }
        }

        public virtual string InstallationCountry
        {
            get { return string.Empty; }
        }

        public virtual string InstallationReportFolder
        {
            get { return string.Empty; }
        }

        public virtual string UserLogged 
        {
            get { return string.Empty; }
        }

        public virtual string Copyright
        {
            get { return "NewHotel Software S.A."; }
        }

        protected virtual void LoadInstallations(IDatabaseManager manager)
        {
            manager.Open();
            try
            {
                using (var command = manager.CreateCommand("Context.LoadInstallations"))
                {
                    command.CommandText = "select hote_pk, hote_prov, hote_user, hote_pwrd, hote_serv from tnht_hote";
                    using (var reader = command.ExecuteReader(CommandBehavior.SingleResult))
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                var id = reader.GetGuid(0);
                                var provider = (DataProvider)reader.GetInt16(1);
                                var user = reader.GetString(2);
                                var password = reader.GetString(3);
                                var schema = reader.GetString(4);
                                AddManager(id, provider, user, password, schema, string.Empty);
                                Debug.Print("Hotel: {0}", id.ToHex());
                                Debug.Print("Provider: {0}", provider);
                                Debug.Print("User: {0}", user);
                                Debug.Print("Password: {0}", password);
                                Debug.Print("Schema: {0}", schema);
                            }
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }
                }
            }
            finally
            {
                manager.Close();
            }
        }

        public virtual DateTime GetSysDate()
        {
            return DateTime.Now.Date.ToUtcDateTime();
        }

        public virtual string GetApplicationName(long applicationId)
        {
            return string.Empty;
        }

        public virtual void GetWorkDateAndShift(out DateTime workDate, out short workShift, long applicationId)
        {
            workDate = GetSysDate();
            workShift = 0;
        }

        public void GetWorkDateAndShift(out DateTime workDate, out short workShift)
        {
            GetWorkDateAndShift(out workDate, out workShift, ApplicationId);
        }

        public DateTime GetWorkDate(long applicationId)
        {
            DateTime workDate;
            short workShift;
            GetWorkDateAndShift(out workDate, out workShift, applicationId);
            return workDate;
        }

        public DateTime GetWorkDate()
        {
            return GetWorkDate(ApplicationId);
        }

        public short GetWorkShift(long applicationId)
        {
            DateTime workDate;
            short workShift;
            GetWorkDateAndShift(out workDate, out workShift, applicationId);
            return workShift;
        }

        public short GetWorkShift()
        {
            return GetWorkShift(ApplicationId);
        }

        public int? GetFirstOpenedYear()
        {
            var firstOpenedDate = GetFirstOpenedDate();
            if (firstOpenedDate.HasValue)
                return firstOpenedDate.Value.Year;

            return null;
        }

        public virtual DateTime? GetFirstOpenedDate()
        {
            return null;
        }

        public virtual int? GetLastOpenedYear()
        {
            return null;
        }

        public DateTime? GetLastOpenedDate()
        {
            var lastOpenedYear = GetLastOpenedYear();
            if (lastOpenedYear.HasValue)
                return new DateTime(lastOpenedYear.Value, 12, 31);

            return null;
        }

        public virtual string CurrencyId
        {
            get { return string.Empty; }
        }

        public virtual string CurrencySymbol
        {
            get { return "$"; }
        }

        public virtual short DecimalsInPercents
        {
            get { return 2; }
        }

        public virtual short DecimalsInPrices
        {
            get { return 2; }
        }

        public virtual short DecimalsInExchange
        {
            get { return 2; }
        }

        public virtual long DateTimeCulture
        {
            get { return -1; }
        }

        public virtual IDictionary<long, string> GetLanguages()
        {
            return new Dictionary<long, string>();
        }

        public virtual SecurityCode? GetPermission(long permission, long applicationId)
        {
            return null;
        }

        public virtual IDictionary<long, long> GetPermissions(Guid userId, long applicationId)
        {
            return new Dictionary<long, long>();
        }

        public virtual IDictionary<long, long> GetPermissionsAdm(Guid userId)
        {
            return new Dictionary<long, long>();
        }

        public virtual IDictionary<Guid, long> GetPermissionsbyGroup(Guid userId, long applicationId)
        {
            return new Dictionary<Guid, long>();
        }

        public virtual IDictionary<Guid, string> GetInstallations(Guid userId)
        {
            return new Dictionary<Guid, string>();
        }

        public virtual IDictionary<long, IList<PermissionGroupDetails>> GetPermissionDetailsbyGroup(Guid userId, long applicationId)
        {
            return new Dictionary<long, IList<PermissionGroupDetails>>();
        }

        public virtual Guid? GetUser(string loginName)
        {
            return null;
        }

        public virtual Guid? GetUser(string loginName, Guid installationId)
        {
            return null;
        }

        public virtual bool IsAdmin(string loginName)
        {
            return false;
        }

        public virtual bool IsInternal(string loginName)
        {
            return false;
        }

        public virtual bool IsMultiProperty
        {
            get { return false; }
        }

        public virtual bool IsCondoFriendly
        {
            get { return false; }
        }

        public virtual bool IsUserMultiProperty
        {
            get { return false; }
        }

        public virtual Guid? AuthorizeUser(string loginName, string loginPassword)
        {
            return null;
        }

        public virtual Guid? AuthorizeUser(string loginName, string loginPassword, Guid installationId)
        {
            return null;
        }

        public virtual void RegisterLog(long operId, Guid? hotelId, Guid userId)
        {
        }

        public virtual bool Initialize(Guid publicKeyId)
        {
            return false;
        }

        public virtual void ClearPublicKeys()
        {
        }

        #endregion
        #region Singleton

        protected static readonly IDictionary<Type, NewHotelAppContext> instances = new Dictionary<Type, NewHotelAppContext>();

        protected static Type GetInstanceType()
        {
            try
            {
                var contextType = AppConfig.Instance.Context;
                if (string.IsNullOrEmpty(contextType))
                    throw new ApplicationException("Context undefined.");

                return Type.GetType(contextType, true);
            }
            catch (Exception ex)
            {
                Debug.Print("Creating context\n{0}", ex.Message);
                throw ex;
            }
        }

        public static NewHotelAppContext GetInstance()
        {
            NewHotelAppContext instance = null;
            lock (instances)
            {
                var instanceType = GetInstanceType();
                if (!instances.TryGetValue(instanceType, out instance))
                {
                    try
                    {
                        instance = Activator.CreateInstance(instanceType) as NewHotelAppContext;
                        instances.Add(instanceType, instance);
                    }
                    catch (Exception ex)
                    {
                        Debug.Print("Creating context\n{0}", ex.Message);
                        throw ex;
                    }
                }
            }

            return instance;
        }

        #endregion
        #region Public Methods & Properties

        public static int MaxTabCount
        {
            get { return AppConfig.Instance.MaxTabCount; }
        }

        public CacheData CacheData
        {
            get 
            {
                if (cache != null)
                    return cache.CacheData;
                else
                    return null;
            }
        }

        public void Refresh()
        {
            if (cache != null)
                cache.Refresh();
        }

        public static void Clear()
        {
            _cache.Clear();

            if (_sessionCache != null)
                _sessionCache.Clear();
        }

        public static string GetUniqueAlphaNumeric(int maxSize)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            var crypto = new RNGCryptoServiceProvider();
            var data = new byte[maxSize];
            crypto.GetNonZeroBytes(data);
            var sb = new StringBuilder(maxSize);
            foreach (byte b in data)
                sb.Append(chars[b % (chars.Length - 1)]);

            return sb.ToString();
        }

        private static IPHostEntry GetIpHostEntry(string hostName)
        {
            return Dns.GetHostEntry(hostName);
        }

        public static string GetHostIpAddress(string hostName)
        {
            var addrs = GetIpHostEntry(hostName).AddressList;
            if (addrs.Length > 0)
                return addrs[addrs.Length - 1].ToString();

            return string.Empty;
        }

        public static string HostIpAddress
        {
            get { return GetHostIpAddress(Dns.GetHostName()); }
        }

        public static string HostName
        {
            get { return GetIpHostEntry(Dns.GetHostName()).HostName; }
        }

        public T GetParameters<T>()
            where T : Config
        {
            if (CallContext.LogicalGetData(BaseContextCache.SessionCacheKey) == null)
                CallContext.LogicalSetData(BaseContextCache.SessionCacheKey, OperationData);

            return Activator.CreateInstance<T>();
        }

        #endregion
        #region Protected Methods & Properties

        protected static void InitCache<T>() where T : Cache, new()
        {
            if (cache == null)
                cache = new T();
        }

		protected object this[string name]
		{
			get
			{
				object value;
				if (!OperationData.TryGetValue(name, out value))
					value = null;

				return value;
			}
			set
			{
				OperationData[name] = value;
			}
		}

        #endregion
        #region Enum Methods

        public IEnumerable<Type> GetEnumTypes()
        {
            return GetType().GetCustomAttributes(typeof(DomainType), true)
                .Cast<DomainType>().Select(x => x.Type).Distinct();
        }

        public static string GetEnumName(string enumTypeName, long language)
        {
            return string.Format("{0}_{1}", enumTypeName, language);
        }

        public static string GetEnumName(Type enumType, long language)
        {
            return GetEnumName(enumType.Name, language);
        }

        public string GetEnumName(string enumTypeName)
        {
            return GetEnumName(enumTypeName, Language);
        }

        public string GetEnumName(Type enumType)
        {
            return GetEnumName(enumType.Name);
        }

        public string GetEnumName<T>()
        {
            return GetEnumName(typeof(T));
        }

        public string GetEnumTranslation<T>(T id)
        {
            var type = typeof(T);
            var list = CacheData.EnumDataStorage[GetEnumName<T>()];
            if (list != null && list.Locate(x => x.ValueAs(type, "ENUM_PK").Equals(id)))
                return list.ValueAs<string>("ENUM_DESC");

            return string.Empty;
        }

        #endregion
        #region Database Manager Methods & Clases

        public sealed class ManagerInstantiator : IDatabaseManagerInstantiator
        {
            public DataProvider Provider { get; private set; }
            public string User { get; private set; }
            public string Password { get; private set; }
            public string Server { get; private set; }
            public string Database { get; private set; }

            public ManagerInstantiator(DataProvider provider, string user, string password, string server, string database)
            {
                Provider = provider;
                User = user;
                Password = password;
                Server = server;
                Database = database;
            }

            public IDatabaseManager Create(byte poolSize)
            {
                return CreateManager(Provider, User, Password, Server, Database, poolSize);
            }

            public override string ToString()
            {
                return string.Format("Provider={0}, User={1}, Password={2}, Server={3}, Database={4}",
                    Provider.ToString(), User, Password, Server, Database);
            }
        }

        private static ManagerInstantiator GetDefaultManager(DataProvider provider)
        {
            return new ManagerInstantiator(provider,
                AppConfig.Instance.User, CryptUtils.Decrypt(AppConfig.Instance.Password),
                AppConfig.Instance.Service, AppConfig.Instance.Database);
        }

        public static DataProvider Provider
        {
            get { return _provider == DataProvider.Invalid ? AppConfig.Instance.DataProvider : _provider; }
            set
            {
                if (value != _provider && value != DataProvider.Invalid)
                {
                    _provider = value;
                    lock (_managerSync)
                    {
                        if (_managers != null && _managers.ContainsKey(Guid.Empty))
                        {
                            var instantiator = GetDefaultManager(Provider);
                            _managers[Guid.Empty] = instantiator;
                        }
                    }
                }
            }
        }

        private static IDictionary<Guid, ManagerInstantiator> Managers
        {
            get
            {
                if (_managers == null)
                {
                    _managers = new Dictionary<Guid, ManagerInstantiator>();
                    var instantiator = GetDefaultManager(Provider);
                    _managers[Guid.Empty] = instantiator;
                    GetInstance().LoadInstallations(instantiator.Create(0));
                }

                return _managers;
            }
        }

        private static IDatabaseManager CreateManager(DataProvider provider, string user, string password, string server, string database, byte poolSize)
        {
            AssemblyQualifiedTypeName typeName;
            object[] managerParams;

            switch (provider)
            {
                case DataProvider.Oracle:
                    typeName = new AssemblyQualifiedTypeName("OracleDatabaseManager", "NewHotel.Manager.Oracle");
                    managerParams = new object[] { AppConfig.Instance.ModuleName, AppConfig.Instance.PoolName, user, password, server, poolSize };
                    break;
                case DataProvider.SqlServer:
                    typeName = new AssemblyQualifiedTypeName("SqlServerDatabaseManager", "NewHotel.Manager.SqlServer");
                    managerParams = new object[] { AppConfig.Instance.ModuleName, user, password, server, database, poolSize };
                    break;
                default:
                    throw new ApplicationException("Invalid provider");
            }

            var managerType = ReflectionHelper.TypeFromAssembly(typeName, string.Empty, true);
            return (IDatabaseManager)Activator.CreateInstance(managerType, managerParams);
        }

        public static IDatabaseManagerInstantiator FindManagerIntantiator(Guid installationId)
        {
            ManagerInstantiator instantiator = null;
            lock (_managerSync)
            {
                if (!Managers.TryGetValue(installationId, out instantiator))
                {
                    _managers = null;
                    if (!Managers.TryGetValue(installationId, out instantiator))
                        return null;
                }
            }

            return instantiator;
        }

        protected static void AddManager(Guid installationId, DataProvider provider, string user, string password, string server, string database)
        {
            lock (_managerSync)
            {
                Managers[installationId] = new ManagerInstantiator(Provider, user, Core.CryptUtils.Decrypt(password), server, database);
            }
        }

        public static IDatabaseManager GetManager(DataProvider provider, string user, string password, string server, string database, byte poolSize)
        {
            return CreateManager(provider, user, password, server, database, poolSize);
        }

        public static IDatabaseManager GetManager(DataProvider provider, string user, string password, string server, string database = null)
        {
            return GetManager(provider, user, password, server, database, 0);
        }

        public static IDatabaseManager GetManager(string user, string password, string server, string database, byte poolSize)
        {
            return CreateManager(Provider, user, password, server, database, poolSize);
        }

        public static IDatabaseManager GetManager(string user, string password, string server, string database = null)
        {
            return GetManager(user, password, server, database, 0);
        }

        public static IDatabaseManager GetManager()
        {
            return GetManager(GetInstance().InstallationId ?? Guid.Empty);
        }

        private static IDatabaseManager GetManager(Guid installationId, byte poolSize)
        {
            var instantiator = FindManagerIntantiator(installationId);
            if (instantiator != null)
                return instantiator.Create(poolSize);

            return null;
        }

        public static IDatabaseManager GetManager(Guid installationId)
        {
            return GetManager(installationId, AppConfig.Instance.PoolSize);
        }

        public static void ClearManagers()
        {
            lock (_managerSync)
            {
                _managers = null;
            }
        }

        public static IDictionary<IDatabaseManager, List<string>> GetSchemasManagers(IDatabaseManager manager, IDbCommand command)
        {
            var schemasByConnection = new Dictionary<string, IDictionary<string, string>>();
            using (var reader = manager.ExecuteReader(command))
            {
                try
                {
                    while (reader.Read())
                    {
                        var serviceName = reader.GetString(0);
                        IDictionary<string, string> installationSchemas;
                        if (!schemasByConnection.TryGetValue(serviceName, out installationSchemas))
                        {
                            installationSchemas = new Dictionary<string, string>();
                            schemasByConnection.Add(serviceName, installationSchemas);
                        }

                        var schemaName = reader.GetString(1);
                        var schemaPassword = reader.GetString(2);
                        installationSchemas[schemaName] = schemaPassword;
                    }
                }
                finally
                {
                    reader.Close();
                }
            }

            var schemasByManager = new Dictionary<IDatabaseManager, List<string>>();
            foreach (var schemaByConnection in schemasByConnection)
            {
                var schemaName = schemaByConnection.Value.First().Key;
                var schemaPassword = CryptUtils.Decrypt(schemaByConnection.Value.First().Value);
                var serviceName = schemaByConnection.Key;

                schemasByManager.Add(NewHotelAppContext.GetManager(schemaName, schemaPassword, serviceName, string.Empty),
                    schemaByConnection.Value.Keys.ToList());
            }

            return schemasByManager;
        }

        public static IDictionary<IDatabaseManager, List<string>> GetSchemasManagers(IDatabaseManager manager)
        {
            var sql = new StringBuilder();
            sql.AppendLine("select hote.hote_serv, hote.hote_user, hote.hote_pwrd from tnht_hote hote");
            //sql.AppendLine("inner join all_users allu on allu.username = hote.hote_user");

            manager.Open();
            try
            {
                using (var command = manager.CreateCommand("Context.GetSchemas"))
                {
                    command.CommandText = sql.ToString();
                    return GetSchemasManagers(manager, command);
                }
            }
            finally
            {
                manager.Close();
            }
        }

        public static IDictionary<IDatabaseManager, List<string>> GetSchemasManagers(IDatabaseManager manager, Guid[] installationIds)
        {
            if (installationIds.Length > 0)
            {
                var sql = new StringBuilder();
                sql.AppendLine("select hote.hote_serv, hote.hote_user, hote.hote_pwrd from tnht_hote hote");
                //sql.AppendLine("inner join all_users allu on allu.username = hote.hote_user");
                sql.AppendLine("where hote_pk in (" +
                    string.Join(",", Enumerable.Range(0, installationIds.Length).Select(i => ":hote_pk" + i.ToString()))
                    + ")");

                manager.Open();
                try
                {
                    using (var command = manager.CreateCommand("Context.GetSchemas"))
                    {
                        command.CommandText = sql.ToString();
                        manager.CreateParameter(command, "hote_pk", installationIds.Length, installationIds.Cast<object>().ToArray());
                        return GetSchemasManagers(manager, command);
                    }
                }
                finally
                {
                    manager.Close();
                }
            }

            return new Dictionary<IDatabaseManager, List<string>>();
        }

        #endregion
        #region Session Data

        public static IDictionary<string, object> SessionData
        {
            get 
            { 
                if (_sessionCache == null)
                    return OperationData;

                return _sessionCache.Data; 
            }
        }

        private static CacheCollection ContractsData
        {
            get
            {
                object data;
                if (!SessionData.TryGetValue(ContractsKey, out data))
                {
                    data = new CacheCollection();
                    SessionData.Add(ContractsKey, data);
                }

                return data as CacheCollection;
            }
        }

        public static IContractTree Contracts
        {
            get
            {
                IContractTree tree;
                if (ContractsData.TryGetValue(CurrentContext, out tree))
                    return tree;

                return null;
            }
            set
            {
                if (value == null)
                    ContractsData.Remove(CurrentContext);
                else
                    ContractsData.Add(value);
            }
        }

        public Guid ChannelId
        {
            get
            {
                object value;
                if (!OperationData.TryGetValue(ChannelIdKey, out value))
                    return Guid.Empty;

                return (Guid)value;
            }
            set
            {
                OperationData[ChannelIdKey] = value;
            }
        }

        public Guid UserId
        {
            get
            {
                object value;
                if (!OperationData.TryGetValue(UserIdKey, out value))
                    value = Guid.Empty;

                return (Guid)value;
            }
            set
            {
                OperationData[UserIdKey] = value;
            }
        }

		public string UserLogin
		{
			get
			{
				object value;
				if (!OperationData.TryGetValue(UserLoginKey, out value))
					value = string.Empty;

				return (string)value;
			}
			set
			{
				OperationData[UserLoginKey] = value;
			}
		}

        public static long GetLanguage()
        {
            object value;
            if (!OperationData.TryGetValue(LanguageKey, out value))
            {
                value = AppConfig.Instance.Language;
            }

            return Convert.ToInt64(value);
        }

        public long Language
        {
            get
            {
                object value;
                if (!OperationData.TryGetValue(LanguageKey, out value))
                {
                    value = AppConfig.Instance.Language;
                    Language = (long)value;
                }

                return Convert.ToInt64(value);
            }
            set
            {
                OperationData[LanguageKey] = value;
            }
        }

        public long ApplicationId
        {
            get
            {
                object value;
				if (!OperationData.TryGetValue(ApplicationIdKey, out value))
					return 1L;

				return Convert.ToInt64(value);
            }
            set
            {
                OperationData[ApplicationIdKey] = value;
            }
        }

        public static Guid GetInstallation()
        {
            object value;
            if (!OperationData.TryGetValue(InstallationIdKey, out value))
                value = Guid.Empty;

            return (Guid)value;
        }

        public Guid? InstallationId
        {
            get
            {
                object value;
                if (!OperationData.TryGetValue(InstallationIdKey, out value))
                    value = null;

                return (Guid?)value;
            }
            set
            {
                OperationData[InstallationIdKey] = value;
            }
        }

        #endregion
        #region Operation Data

        public static IDictionary<string, object> OperationData
        {
            get
            {
                lock (_cache.Data)
                {
                    return _cache.Data;
                }
            }
        }

        public static IDictionary<string, object> CloneOperationData()
        {
            lock (_cache.Data)
            {
                return _cache.CloneData();
            }
        }

        public virtual DateTime ClientDateTime
        {
            get
            {
                object value;
                if (!OperationData.TryGetValue(ClientDateTimeKey, out value))
                    value = DateTime.MinValue.ToUtcDateTime();

                return (DateTime)value;
            }
            set
            {
                OperationData[ClientDateTimeKey] = value;
            }
        }

        public string Version
        {
            get
            {
                object value;
                if (!OperationData.TryGetValue(ApplicationVersionKey, out value))
                    value = null;

                return (string)value;
            }
            set
            {
                OperationData[ApplicationVersionKey] = value;
            }
        }

        #endregion
        #region Context Methods

        public static int CurrentContext
        {
            get
            {
                object value;
                if (!OperationData.TryGetValue(CurrentContextKey, out value))
                    value = 0;

                return (int)value;
            }
            set
            {
                if (value < 0)
                    throw new ApplicationException(string.Format("Invalid context id: {0}", value));

                OperationData[CurrentContextKey] = value;
            }
        }

        private static object ContextCountSync = new object();
        private static int contextCount;
        private static int ContextCount
        {
            get
            {
                lock (ContextCountSync)
                {
                    return contextCount;
                }
            }
            set
            {
                lock (ContextCountSync)
                {
                    contextCount = value;
                }
            }
        }

        public static int BeginContext()
        {
            lock (ContextCountSync)
            {
                CurrentContext = ++ContextCount;
                return ContextCount;
            }
        }

        public static void EndContext(int id)
        {
            if (id < 0)
                throw new ApplicationException(string.Format("Invalid context id: {0}", id));

            lock (ContextCountSync)
            {
                Contracts = null;
                if (CurrentContext == id)
                    CurrentContext = 0;
            }
        }

        #endregion
        #region Default Session Cache

        public class DefaultSessionCache : IBaseCache
        {
            #region Constants

            private const string SessionDataKey = "SESSION_DATA";

            #endregion
            #region Private Methods

            private static object GetDataByKey(string key)
            {
                return CallContext.GetData(key);
            }

            private static void SetDataByKey(string key, object data)
            {
                CallContext.SetData(key, data);
            }

            private static void RemoveDataByKey(string key)
            {
                CallContext.FreeNamedDataSlot(key);
            }

            #endregion
            #region IBaseCache Members

            public IDictionary<string, object> Data
            {
                get
                {
                    lock (this)
                    {
                        var data = GetDataByKey(SessionDataKey);
                        if (data == null)
                        {
                            data = new Dictionary<string, object>();
                            SetDataByKey(SessionDataKey, data);
                        }

                        return data as IDictionary<string, object>;
                    }
                }
            }

            public IDictionary<string, object> CloneData()
            {
                lock (this)
                {
                    return new Dictionary<string, object>(Data);
                }
            }

            public bool ContainsData
            {
                get
                {
                    lock (this)
                    {
                        var data = GetDataByKey(SessionDataKey);
                        var dict = data as IDictionary<string, object>;
                        return dict != null || dict.Count > 0;
                    }
                }
            }

            public void Clear()
            {
                lock (this)
                {
                    RemoveDataByKey(SessionDataKey);
                }
            }

            #endregion
        }

        #endregion
        #region Default Context Cache

        private class DefaultContextCache : BaseContextCache
		{
			protected override IDictionary<string, object> GetData(string cacheKey)
			{
				return GetCallContextData(cacheKey);
			}

			protected override void RemoveData(string cacheKey)
			{
				RemoveCallContextData(cacheKey);
			}
		}

        #endregion
    }
}