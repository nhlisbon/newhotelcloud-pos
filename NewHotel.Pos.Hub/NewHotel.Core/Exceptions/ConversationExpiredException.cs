﻿using System;

namespace NewHotel.Core
{
    public class ConversationExpiredException : ApplicationException
    {
        public ConversationExpiredException(int conversationId)
            : base(string.Format("Conversation {0} expired", conversationId))
        {
        }
    }
}
