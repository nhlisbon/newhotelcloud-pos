﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;

namespace NewHotel.Core
{
    public class UniqueViolationException : DatabaseException
    {
        public UniqueViolationException(IDatabaseManager manager, Exception ex, string sql, IEnumerable<KeyValuePair<string, object>> parameters)
            : base(manager, ex, "Duplicated record.", sql, parameters)
        {
            var match = Regex.Match(ex.Message, @"\.\w+\)");
            if(match.Success)
                ConstraintName = match.Value.Trim('.', ')');
        }

        public string ConstraintName { get; set; }
    }
}
