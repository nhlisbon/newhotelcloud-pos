﻿using System;

namespace NewHotel.Core
{
    public class ConversationNotFoundException : ApplicationException
    {
        public ConversationNotFoundException(int conversationId)
            : base(string.Format("Conversation {0} not found", conversationId))
        {
        }
    }
}
