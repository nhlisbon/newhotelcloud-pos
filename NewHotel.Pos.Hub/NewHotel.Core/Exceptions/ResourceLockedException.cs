﻿using System;
using System.Collections.Generic;
using System.Data;

namespace NewHotel.Core
{
    public class ResourceLockedException : DatabaseException
    {
        public ResourceLockedException(IDatabaseManager manager, Exception ex, string sql, IEnumerable<KeyValuePair<string, object>> parameters)
            : base(manager, ex, "Resource is locked.", sql, parameters)
        {
        }
    }
}
