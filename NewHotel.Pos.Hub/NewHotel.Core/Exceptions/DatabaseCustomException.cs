﻿using System;
using System.Data.Common;

namespace NewHotel.Core
{
    public class DatabaseCustomException : DbException
    {
        public DatabaseCustomException(Exception ex, int errorCode)
            : base(ex.Message, errorCode)
        {
        }
    }
}