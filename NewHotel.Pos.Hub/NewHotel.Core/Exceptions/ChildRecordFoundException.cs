﻿using System;
using System.Collections.Generic;
using System.Data;

namespace NewHotel.Core
{
    public class ChildRecordFoundException : DatabaseException
    {
        public ChildRecordFoundException(IDatabaseManager manager, Exception ex, string sql, IEnumerable<KeyValuePair<string, object>> parameters)
            : base(manager, ex, "Child record found.", sql, parameters)
        {
        }
    }
}
