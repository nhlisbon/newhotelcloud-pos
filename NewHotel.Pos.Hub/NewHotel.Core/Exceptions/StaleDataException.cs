﻿using System;
using System.Collections.Generic;
using System.Data;

namespace NewHotel.Core
{
    public class StaleDataException : DatabaseException
    {
        public StaleDataException(IDatabaseManager manager, string sql, IEnumerable<KeyValuePair<string, object>> parameters)
            : base(manager, null, "Stale data.", sql, parameters)
        {
        }
    }
}
