﻿using System;
using System.Collections.Generic;
using System.Data.Common;

namespace NewHotel.Core
{
    public class DatabaseException : DbException
    {
        public DatabaseException(IDatabaseManager manager, Exception ex, string message, string sql, IEnumerable<KeyValuePair<string, object>> parameters)
            : base(message +
            (string.IsNullOrEmpty(sql) ? string.Empty : "\r\n" + "Sql: " + sql) +
            (manager != null && parameters != null ? "\r\n" + "Parameters: " + manager.GetParameters(parameters) : string.Empty), ex)
        {
        }

        public DatabaseException(IDatabaseManager manager, Exception ex, string sql, IEnumerable<KeyValuePair<string, object>> parameters)
            : this(manager, ex, "Database exception.", sql, parameters)
        {
        }
    }
}