﻿using System;
using System.Collections.Generic;
using System.Data;

namespace NewHotel.Core
{
    public class NoDataFoundException : DatabaseException
    {
        public NoDataFoundException(IDatabaseManager manager, string sql, IEnumerable<KeyValuePair<string, object>> parameters)
            : base(manager, null, "No data found.", sql, parameters)
        {
        }

        public NoDataFoundException(string message)
            : base(null, null, message, null, null)
        {
        }
    }
}
