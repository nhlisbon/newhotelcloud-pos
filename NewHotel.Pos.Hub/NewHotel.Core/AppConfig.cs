﻿namespace NewHotel.Core
{
    public enum DataProvider { Invalid = 0, Oracle = 1, SqlServer = 2 }

    public sealed class AppConfig
    {
        public readonly static AppConfig Instance = new AppConfig();

        private IAppConfigProvider _configProvider = new AppConfigProvider();

        public string Provider { get { return _configProvider != null ? _configProvider.Provider : null; } }
        public string Service { get { return _configProvider != null ? _configProvider.Service : null; } }
        public string Database { get { return _configProvider != null ? _configProvider.Database : null; } }
        public string User { get { return _configProvider != null ? _configProvider.User : null; } }
        public string Password { get { return _configProvider != null ? _configProvider.Password : null; } }
        public bool Encrypted { get { return _configProvider != null ? _configProvider.Encrypted : false; } }
        public byte PoolSize { get { return _configProvider != null ? _configProvider.PoolSize : (byte)0; } }
        public long Language { get { return _configProvider != null ? _configProvider.Language : 1033; } }
        public string Cache { get { return _configProvider != null ? _configProvider.Cache : null; } }
        public string SessionCache { get { return _configProvider != null ? _configProvider.SessionCache : null; } }
        public string Context { get { return _configProvider != null ? _configProvider.Context : null; } }
        public int MaxTabCount { get { return _configProvider != null ? _configProvider.MaxTabCount : 0; } }
        public string ModuleName { get { return _configProvider != null ? _configProvider.ModuleName : null; } }
        public string PoolName { get { return _configProvider != null ? _configProvider.PoolName : null; } }

        public DataProvider DataProvider
        {
            get
            {
                if (!string.IsNullOrEmpty(Provider))
                {
                    switch (Provider.ToLowerInvariant())
                    {
                        case "oracle": return DataProvider.Oracle;
                        case "sqlserver": return DataProvider.SqlServer;
                        default: return DataProvider.Invalid;
                    }
                }

                return DataProvider.Oracle;
            }
        }
    }
}