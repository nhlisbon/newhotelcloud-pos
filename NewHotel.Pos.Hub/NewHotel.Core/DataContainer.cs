﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;

namespace NewHotel.Core
{
    public class DataContainer : ITypedList
    {
        private sealed class ContainerPropertyDescriptor : PropertyDescriptor
        {
            public ContainerPropertyDescriptor(string name)
                : base(name, null) { }

            public override string DisplayName
            {
                get { return Name; }
            }

            public override bool CanResetValue(object component)
            {
                return false;
            }

            public override Type ComponentType
            {
                get { return typeof(DataContainer); }
            }

            public override Type PropertyType
            {
                get { return typeof(ListData); }
            }

            public override bool IsReadOnly
            {
                get { return true; }
            }

            public override object GetValue(object component)
            {
                return ((DataContainer)component)[Name];
            }

            public override void SetValue(object component, object value)
            {
            }

            public override void ResetValue(object component)
            {
            }

            public override bool ShouldSerializeValue(object component)
            {
                return true;
            }
        }

        private readonly IDictionary<string, ListData> _data = new Dictionary<string, ListData>();

        public ListData this[string name]
        {
            get
            {
                ListData data;
                lock (_data)
                {
                    if (!_data.TryGetValue(name.ToUpperInvariant(), out data))
                        data = null;
                }

                return data;
            }
            set
            {
                lock (_data)
                {
                    _data[name.ToUpperInvariant()] = value;
                }
            }
        }

        public bool Contains(string name)
        {
            return _data.ContainsKey(name.ToUpperInvariant());
        }

        public IDictionary<string, ListData> GetData(Func<string,bool> predicate)
        {
            lock (_data)
            {
                return _data.Where(x => predicate.Invoke(x.Key)).ToDictionary(x => x.Key, x => x.Value);
            }
        }

        private object ConvertValueToType(Type type, object value)
        {
            const char TrueValue = '1';
            const char FalseValue = '0';

            bool isNullable = type == typeof(string);
            if (!isNullable)
            {
                isNullable = type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
                if (isNullable)
                    type = type.GetGenericArguments()[0];
            }

            if (value == DBNull.Value)
            {
                if (type == typeof(string))
                    value = string.Empty;
                else
                {
                    if (!isNullable)
                        throw new ApplicationException(string.Format("Conversion error from {0} to {1}", value.GetType().Name, type.Name));
                }
            }
            else if (type.IsEnum)
            {
                if (type.GetCustomAttributes(typeof(FlagsAttribute), false).Length > 0)
                {
                    if (value.GetType() != typeof(string))
                        throw new ApplicationException(string.Format("Conversion error from {0} to {1}", value.GetType().Name, type.Name));
                    var size = Enum.GetValues(type).Length;

                    var values = value.ToString().ToCharArray();
                    if (values.Length != size)
                        throw new ApplicationException(string.Format("Conversion error from {0} to {1}", value.GetType().Name, type.Name));

                    long bits = 0;
                    for (int i = 0; i < values.Length; i++)
                    {
                        switch (values[i])
                        {
                            case TrueValue:
                                bits += 2 << i;
                                break;
                            case FalseValue:
                                break;
                            default:
                                throw new ApplicationException(string.Format("Conversion error from {0} to {1}", value.GetType().Name, type.Name));
                        }
                    }

                    value = Enum.ToObject(type, bits);
                }
                else
                {
                    var underlyingType = Enum.GetUnderlyingType(type);
                    value = Convert.ChangeType(value, underlyingType);
                    var values = Enum.GetValues(type).Cast<object>().Select(x => Convert.ChangeType(x, underlyingType));
                    if (!values.Contains(value))
                        throw new ApplicationException(string.Format("Conversion error from {0} to {1}", value.GetType().Name, type.Name));
                    value = Enum.ToObject(type, value);
                }
            }
            else if (type == typeof(bool))
            {
                var boolStr = value.ToString();
                switch (boolStr.Length > 0 ? boolStr[0] : char.MinValue)
                {
                    case FalseValue:
                        value = false;
                        break;
                    case TrueValue:
                        value = true;
                        break;
                    default:
                        throw new ApplicationException(string.Format("Conversion error from {0} to {1}", value.GetType().Name, type.Name));
                }
            }
            else if (type == typeof(Guid))
            {
                if (value.GetType() == typeof(byte[]))
                    value = new Guid((byte[])value);
                else if (value.GetType() != typeof(Guid))
                    throw new ApplicationException(string.Format("Conversion error from {0} to {1}", value.GetType().Name, type.Name));
            }
            else if (type == typeof(object))
            {
                if (value.GetType() == typeof(byte[]))
                    value = new Guid((byte[])value);
            }

            return value;
        }

        public void FillData(IDataReader reader, string name)
        {
            var defs = new List<ColumnDefinition>(reader.FieldCount);
            for (int i = 0; i < reader.FieldCount; i++)
            {
                var type = reader.GetFieldType(i);
                if (type == typeof(byte[]))
                    type = typeof(Guid);
                defs.Add(new ColumnDefinition(reader.GetName(i), type, i));
            }

            var list = new List<object[]>();
            while (reader.Read())
            {
                var objs = new object[reader.FieldCount];
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    var value = reader.GetValue(i);
                    objs[i] = value == DBNull.Value ? null : ConvertValueToType(defs[i].Type, value);
                }
                list.Add(objs);
            }

            this[name] = new ListData(name.ToUpperInvariant(), defs.ToArray(), list);
            _propDesc = null;
        }

        public void FillData(IDatabaseManager manager, string commandText, string name)
        {
            var command = manager.CreateCommand("DataContainer.FillData");
            command.CommandText = commandText;
            var reader = manager.ExecuteReader(command);
            try
            {
                FillData(reader, name);
            }
            finally
            {
                reader.Close();
            }
        }

        public bool RemoveData(string name)
        {
            _propDesc = null;
            return _data.Remove(name.ToUpper());
        }

        public void Clear()
        {
            _propDesc = null;
            _data.Clear();
        }

        private PropertyDescriptorCollection _propDesc;
        private PropertyDescriptorCollection propDesc
        {
            get
            {
                if (_propDesc == null)
                    _propDesc = new PropertyDescriptorCollection(
                        _data.Keys.Select(x => new ContainerPropertyDescriptor(x))
                        .ToArray());

                return _propDesc;
            }
        }
         
        #region ITypedList Members

        public PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors)
        {
            if (listAccessors != null && listAccessors.Length > 0)
            {
                var name = listAccessors[0].Name;
                ITypedList typedList = this[name];
                if (typedList != null)
                    return typedList.GetItemProperties(null);
            }
            else
                return propDesc;

            return null;
        }

        public string GetListName(PropertyDescriptor[] listAccessors)
        {
            if (listAccessors != null && listAccessors.Length > 0)
                return listAccessors[0].Name;
            else
                return GetType().Name;
        }

        #endregion
    }
}
