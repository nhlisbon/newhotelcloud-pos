﻿using System.Configuration;

namespace NewHotel.Core
{
    public sealed class AppConfigSection : ConfigurationSection
    {
        #region Constants

        private const string ProviderKey = "provider";
        private const string ServiceKey = "service";
        private const string DatabaseKey = "database";
        private const string UserKey = "user";
        private const string PasswordKey = "password";
        private const string EncryptedKey = "encrypted";
        private const string PoolSizeKey = "poolSize";
        private const string LanguageKey = "language";
        private const string CacheKey = "cache";
        private const string SessionCacheKey = "sessionCache";
        private const string ContextKey = "context";
        private const string MaxTabCountKey = "maxTabCount";
        private const string ModuleNameKey = "moduleName";
        private const string PoolNameKey = "poolName";

        #endregion
        #region Properties

        [ConfigurationProperty(ProviderKey, IsRequired = true, IsKey = false, DefaultValue = "")]
        public string Provider
        {
            get { return (string)this[ProviderKey]; }
            set { this[ProviderKey] = value; }
        }

        [ConfigurationProperty(ServiceKey, IsRequired = true, IsKey = false, DefaultValue = "ORCL")]
        public string Service
        {
            get { return (string)this[ServiceKey]; }
            set { this[ServiceKey] = value; }
        }

        [ConfigurationProperty(DatabaseKey, IsRequired = false, IsKey = false, DefaultValue = "")]
        public string Database
        {
            get { return (string)this[DatabaseKey]; }
            set { this[DatabaseKey] = value; }
        }

        [ConfigurationProperty(UserKey, IsRequired = true, IsKey = false, DefaultValue = "NHT")]
        public string User
        {
            get { return (string)this[UserKey]; }
            set { this[UserKey] = value; }
        }

        [ConfigurationProperty(PasswordKey, IsRequired = true, IsKey = false, DefaultValue = "NHT")]
        public string Password
        {
            get { return (string)this[PasswordKey]; }
            set { this[PasswordKey] = value; }
        }

        [ConfigurationProperty(EncryptedKey, IsRequired = true, IsKey = false, DefaultValue = "false")]
        public bool Encrypted
        {
            get { return (bool)this[EncryptedKey]; }
            set { this[EncryptedKey] = value; }
        }

        [ConfigurationProperty(PoolSizeKey, IsRequired = false, IsKey = false, DefaultValue = "0")]
        public byte PoolSize
        {
            get { return (byte)this[PoolSizeKey]; }
            set { this[PoolSizeKey] = value; }
        }

        [ConfigurationProperty(LanguageKey, IsRequired = false, IsKey = false, DefaultValue = "1033")]
        public long Language
        {
            get { return (long)this[LanguageKey]; }
            set { this[LanguageKey] = value; }
        }

        [ConfigurationProperty(CacheKey, IsRequired = false, IsKey = false, DefaultValue = "")]
        public string Cache
        {
            get { return (string)this[CacheKey]; }
            set { this[CacheKey] = value; }
        }

        [ConfigurationProperty(SessionCacheKey, IsRequired = false, IsKey = false, DefaultValue = "")]
        public string SessionCache
        {
            get { return (string)this[SessionCacheKey]; }
            set { this[SessionCacheKey] = value; }
        }

        [ConfigurationProperty(ContextKey, IsRequired = false, IsKey = false, DefaultValue = "")]
        public string Context
        {
            get { return (string)this[ContextKey]; }
            set { this[ContextKey] = value; }
        }

        [ConfigurationProperty(MaxTabCountKey, IsRequired = false, IsKey = false, DefaultValue = "0")]
        public int MaxTabCount
        {
            get { return (int)this[MaxTabCountKey]; }
            set { this[MaxTabCountKey] = value; }
        }

        [ConfigurationProperty(ModuleNameKey, IsRequired = false, IsKey = false, DefaultValue = "")]
        public string ModuleName
        {
            get { return (string)this[ModuleNameKey]; }
            set { this[ModuleNameKey] = value; }
        }

        [ConfigurationProperty(PoolNameKey, IsRequired = false, IsKey = false, DefaultValue = "")]
        public string PoolName
        {
            get { return (string)this[PoolNameKey]; }
            set { this[PoolNameKey] = value; }
        }

        #endregion
    }
}