﻿using System;
using System.Runtime.Remoting.Messaging;
using System.Collections.Generic;
using System.Linq;

namespace NewHotel.Core
{
    public abstract class BaseContextCache : IBaseCache
    {
        public static string SessionCacheKey = Guid.NewGuid().ToString("N");

        public override string ToString()
        {
            return SessionCacheKey;
        }

        #region Abstract methods

        protected abstract IDictionary<string, object> GetData(string cacheKey);
        protected abstract void RemoveData(string cacheKey);

        #endregion
		#region Protected Methods

		protected static IDictionary<string, object> GetCallContextData(string cacheKey)
		{
			var data = CallContext.GetData(cacheKey) as IDictionary<string, object>;
			if (data == null)
			{
				data = new Dictionary<string, object>();
				CallContext.SetData(cacheKey, data);
			}

			return data;
		}

		protected static void RemoveCallContextData(string cacheKey)
		{
			CallContext.FreeNamedDataSlot(cacheKey);
		}

		#endregion
		#region IBaseCache members

		public IDictionary<string, object> Data
        {
            get
            {
                var data = GetData(SessionCacheKey);
                if (data == null)
                    data = GetCallContextData(SessionCacheKey);
                
                return data;
            }
        }

        public IDictionary<string, object> CloneData()
        {
            var data = GetData(SessionCacheKey) as ICloneable;
			if (data != null)
				return (IDictionary<string, object>)data.Clone();

			return null;
        }

        public bool ContainsData
        {
            get { return GetData(SessionCacheKey) != null; }
        }

        public void Clear()
        {
            RemoveData(SessionCacheKey);
        }

        #endregion
        #region Static methods

        public static void CopyDataToCallContext(IDictionary<string, object> data)
        {
            foreach (var d in data.ToArray())
                NewHotelAppContext.OperationData[d.Key] = d.Value;
        }

        public static void ClearDataFromCallContext()
        {
        }

        #endregion
    }
}
