﻿
namespace NewHotel.Core
{
    public class AppPrinters
    {
        private readonly string Workstation;

        public AppPrinters(string workstation)
        {
            Workstation = workstation.ToUpperInvariant();
        }

        private string GetKey(string printerName)
        {
            return Workstation + "\\" + printerName;
        }

        protected virtual string GetPrinter(string workstation, string printerName)
        {
            //System.Configuration.ConfigurationManager.
            //Configuration config = ConfigurationManager.OpenExeConfiguration(Application.StartupPath & "\WindowsApplication1.exe")

            //AppSettingsSection appSetting = config.AppSettings;
            //string key = GetKey(printerName);
            //if (appSetting.Settings.AllKeys.Contains(key))
            //    return appSetting.Settings[key].Value;
            //else
            //    return string.Empty;

            return string.Empty;
        }

        protected virtual void SetPrinter(string workstation, string printerName, string printer)
        {
            //Configuration config = ConfigurationManager.OpenExeConfiguration(Application.StartupPath & "\WindowsApplication1.exe")

            //AppSettingsSection appSetting = config.AppSettings;
            //appSetting.Settings[GetKey(printerName)].Value = printer;
            //config.Save(ConfigurationSaveMode.Modified);
        }

        public string this[string printerName]
        {
            get { return GetPrinter(Workstation, printerName); }
            set { SetPrinter(Workstation, printerName, value); }
        }
    }
}
