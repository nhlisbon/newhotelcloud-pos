﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Core
{
    [Serializable]
    public class ThreadSafeDictionary<TKey, TValue> : IDictionary<TKey, TValue>, ISerializable, IDeserializationCallback, ICloneable
    {
        #region Private Class

        private sealed class InternalDictionary<TKey1, TValue1> : Dictionary<TKey1, TValue1>
        {
            public InternalDictionary(SerializationInfo info, StreamingContext context)
                : base(info, context) { }

            public InternalDictionary()
                : base() { }

            public InternalDictionary(int capacity)
                : base(capacity) { }

            public InternalDictionary(IDictionary<TKey1, TValue1> dictionary)
                : base(dictionary) { }
        }

        #endregion
        #region Private Members

        private readonly Dictionary<TKey, TValue> _dict;
        private readonly object _syncObject = new object();

        #endregion
        #region Properties

        private IDictionary<TKey, TValue> Dict
        {
            get { return _dict; }
        }

        #endregion
        #region IDictionary<TKey,TValue> Members

        public void Add(TKey key, TValue value)
        {
            lock (_syncObject)
            {
                Dict.Add(key, value);
            }
        }

        public bool ContainsKey(TKey key)
        {
            lock (_syncObject)
            {
                return Dict.ContainsKey(key);
            }
        }

        public ICollection<TKey> Keys
        {
            get
            {
                lock (_syncObject)
                {
                    return Dict.Keys;
                }
            }
        }

        public bool Remove(TKey key)
        {
            lock (_syncObject)
            {
                return Dict.Remove(key);
            }
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            lock (_syncObject)
            {
                return Dict.TryGetValue(key, out value);
            }
        }

        public ICollection<TValue> Values
        {
            get
            {
                lock (_syncObject)
                {
                    return Dict.Values;
                }
            }
        }

        public TValue this[TKey key]
        {
            get
            {
                lock (_syncObject)
                {
                    return Dict[key];
                }
            }
            set
            {
                lock (_syncObject)
                {
                    Dict[key] = value;
                }
            }
        }

        #endregion
        #region ICollection<KeyValuePair<TKey,TValue>> Members

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            lock (_syncObject)
            {
                Dict.Add(item);
            }
        }

        public void Clear()
        {
            lock (_syncObject)
            {
                Dict.Clear();
            }
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            lock (_syncObject)
            {
                return Dict.Contains(item);
            }
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            lock (_syncObject)
            {
                Dict.CopyTo(array, arrayIndex);
            }
        }

        public int Count
        {
            get
            {
                lock (_syncObject)
                {
                    return Dict.Count;
                }
            }
        }

        public bool IsReadOnly
        {
            get
            {
                lock (_syncObject)
                {
                    return Dict.IsReadOnly;
                }
            }
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            lock (_syncObject)
            {
                return Dict.Remove(item);
            }
        }

        #endregion
        #region IEnumerable<KeyValuePair<TKey,TValue>> Members

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            lock (_syncObject)
            {
                return Dict.GetEnumerator();
            }
        }

        #endregion
        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            lock (_syncObject)
            {
                return Dict.GetEnumerator();
            }
        }

        #endregion
        #region Constructors

        protected ThreadSafeDictionary(SerializationInfo info, StreamingContext context)
        {
            _dict = new InternalDictionary<TKey, TValue>(info, context);
        }

        public ThreadSafeDictionary(int capacity)
        {
            _dict = new InternalDictionary<TKey, TValue>(capacity);
        }

        public ThreadSafeDictionary(IDictionary<TKey, TValue> dict)
        {
            _dict = new InternalDictionary<TKey, TValue>(dict);
        }

        public ThreadSafeDictionary()
        {
            _dict = new InternalDictionary<TKey, TValue>();
        }

        #endregion
        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            _dict.GetObjectData(info, context);
        }

        #endregion
        #region IDeserializationCallback Members

        public void OnDeserialization(object sender)
        {
            _dict.OnDeserialization(sender);
        }

        #endregion
        #region ICloneable Members

        public object Clone()
        {
            var clone = new Dictionary<TKey, TValue>();
            foreach (var item in Dict)
            {
                var clonable = item.Value as ICloneable;
                if (clonable != null)
                    clone.Add(item.Key, (TValue)clonable.Clone());
            }

            return clone;
        }

        #endregion
    }
}
