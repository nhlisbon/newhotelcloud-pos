﻿using System;

namespace NewHotel.Core
{
    public abstract class Cache
    {
        public abstract CacheData CacheData { get; }

        public virtual void Refresh() { }
    }
}
