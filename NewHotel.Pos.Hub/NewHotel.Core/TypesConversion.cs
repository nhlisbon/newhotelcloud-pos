﻿using System;
using System.Data;

namespace NewHotel.Core
{
    public static class TypesExt
    {
        public static DbType ToDbType(this Type type)
        {
            if (type == typeof(Guid))
                return DbType.Guid;
            else if (type == typeof(string))
                return DbType.String;
            else if (type == typeof(decimal))
                return DbType.Decimal;
            else if (type == typeof(bool))
                return DbType.Boolean;
            else if (type == typeof(byte))
                return DbType.Byte;
            else if (type == typeof(int))
                return DbType.Int32;
            else if (type == typeof(long))
                return DbType.Int64;
            else if (type == typeof(DateTime))
                return DbType.DateTime;
            else if (type == typeof(Enum))
                return DbType.Int64;

            throw new ArgumentException("Invalid type");
        }

        public static Type FromDbType(this DbType dbType)
        {
            switch (dbType)
            {
                case DbType.Guid: return typeof(Guid);
                case DbType.String: return typeof(String);
                case DbType.Decimal: return typeof(Decimal);
                case DbType.Boolean: return typeof(Boolean);
                case DbType.Byte: return typeof(Byte);
                case DbType.Int32: return typeof(Int32);
                case DbType.Int64: return typeof(Int64);
                case DbType.DateTime: return typeof(DateTime);
                default: throw new ArgumentException("Invalid DbType");
            }
        }

        public static bool IsNullable(this Type type)
        {
            return (type == typeof(string)) ||
                type.IsClass || type.IsInterface ||
                (type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>)));
        }

        public static object ChangeType(this Type type, object obj)
        {
            if (type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                return Activator.CreateInstance(type, obj);
            else if (obj is IConvertible)
                return System.Convert.ChangeType(obj, type);

            return obj;
        }
    }
}
