﻿using System;
using System.Collections.Generic;

namespace NewHotel.Core
{
    public class PredicateComparer<T> : EqualityComparer<T>
    {
        private readonly Func<T, T, bool> _predicate;

        public PredicateComparer(Func<T, T, bool> predicate)
        {
            _predicate = predicate;
        }

        public override bool Equals(T x, T y)
        {
            return _predicate.Invoke(x, y);
        }

        public override int GetHashCode(T obj)
        {
            return obj.GetHashCode();
        }
    }
}


