﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewHotel.Pos.Core.Ext;

namespace NewHotel.Pos.Core
{
    public static class TipHelper
    {
        public static decimal CalculateTipPercent(decimal total, decimal tipPercent)
        {
            return (total * tipPercent / 100).RoundTo(2);
        }

        public static decimal CalculateTotalByTipPercent(decimal total, decimal tipPercent)
        {
            return total + CalculateTipPercent(total, tipPercent);
        }
    }
}