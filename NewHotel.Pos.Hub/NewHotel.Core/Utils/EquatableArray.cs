﻿using System;
using System.Linq;

namespace NewHotel.Core
{
    public struct EquatableArray : IEquatable<EquatableArray>
    {
        #region Members

        private readonly object[] _values;
        private int _hashCode;

        #endregion
        #region Constructor

        public EquatableArray(params object[] values)
        {
            _values = values ?? new object[0];
            _hashCode = 0;
        }

        #endregion
        #region IEquatable<EquatableArray> Members

        public bool Equals(EquatableArray other)
        {
            if (other._values.Length != _values.Length)
                return false;

            for (int i = 0; i < _values.Length; i++)
            {
                var x = _values[i];
                var y = other._values[i];

                if (x != null)
                {
                    if (!x.Equals(y))
                        return false;
                }
                else if (y != null)
                {
                    if (!y.Equals(x))
                        return false;
                }
            }

            return true;
        }

        #endregion
        #region Public Methods

        public override int GetHashCode()
        {
            if (_hashCode == 0 && _values.Length > 0)
            {
                unchecked
                {
                    _hashCode = 17;
                    for (int index = 0; index < _values.Length; index++)
                        _hashCode = 31 * _hashCode + (_values[index] == null ? 0 : _values[index].GetHashCode());
                }
            }

            return _hashCode;
        }

        public override string ToString()
        {
            if (_values.Length > 0)
                return string.Join(", ", _values.Select(x => (x ?? "NULL").ToString()));

            return string.Empty;
        }

        #endregion
        #region Public Properties
        public object[] Values
        {
            get
            {
                return _values;
            }
        } 
        #endregion
    }
}
