﻿using System;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace NewHotel.Core
{
    public static class CryptUtils
    {
        private const string password = "1C756173-402D-4958-AA76-F2B3511955D7";

        public static string Encrypt(string input, string password)
        {
            // Test data
            var data = input;
            var utfdata = UTF8Encoding.UTF8.GetBytes(data);
            var saltBytes = UTF8Encoding.UTF8.GetBytes("saltIsGoodForYou");

            // Our symmetric encryption algorithm
            var aes = new AesManaged();

            // We're using the PBKDF2 standard for password-based key generation
            var rfc = new Rfc2898DeriveBytes(password, saltBytes);

            // Setting our parameters
            aes.BlockSize = aes.LegalBlockSizes[0].MaxSize;
            aes.KeySize = aes.LegalKeySizes[0].MaxSize;

            aes.Key = rfc.GetBytes(aes.KeySize / 8);
            aes.IV = rfc.GetBytes(aes.BlockSize / 8);

            // Encryption
            var encryptTransf = aes.CreateEncryptor();

            // Output stream, can be also a FileStream
            byte[] encryptBytes;
            using (var encryptStream = new MemoryStream())
            {
                using (var encryptor = new CryptoStream(encryptStream, encryptTransf, CryptoStreamMode.Write))
                {
                    encryptor.Write(utfdata, 0, utfdata.Length);
                    encryptor.Flush();
                    encryptor.Close();
                }

                // Showing our encrypted content
                encryptBytes = encryptStream.ToArray();
            }

            return Convert.ToBase64String(encryptBytes);
        }

        public static string Encrypt(string input)
        {
            return Encrypt(input, password);
        }

        public static string Decrypt(string base64Input, string password)
        {
            var encryptBytes = Convert.FromBase64String(base64Input);
            var saltBytes = Encoding.UTF8.GetBytes("saltIsGoodForYou");

            // Our symmetric encryption algorithm
            var aes = new AesManaged();

            // We're using the PBKDF2 standard for password-based key generation
            var rfc = new Rfc2898DeriveBytes(password, saltBytes);

            // Setting our parameters
            aes.BlockSize = aes.LegalBlockSizes[0].MaxSize;
            aes.KeySize = aes.LegalKeySizes[0].MaxSize;

            aes.Key = rfc.GetBytes(aes.KeySize / 8);
            aes.IV = rfc.GetBytes(aes.BlockSize / 8);

            // Now, decryption
            var decryptTrans = aes.CreateDecryptor();

            // Output stream, can be also a FileStream
            byte[] decryptBytes;
            using (var decryptStream = new MemoryStream())
            {
                using (var decryptor = new CryptoStream(decryptStream, decryptTrans, CryptoStreamMode.Write))
                {
                    decryptor.Write(encryptBytes, 0, encryptBytes.Length);
                    decryptor.Flush();
                    decryptor.Close();
                }

                // Showing our decrypted content
                decryptBytes = decryptStream.ToArray();
            }

            return UTF8Encoding.UTF8.GetString(decryptBytes, 0, decryptBytes.Length);
        }

        public static string Decrypt(string base64Input)
        {
            return Decrypt(base64Input, password);
        }

        public static bool VerifyHashedPassword(this string hashedPassword, string password)
        {
            if (hashedPassword == null)
                return false;
            if (password == null)
                return false;

            byte[] buffer4;
            byte[] src = Convert.FromBase64String(hashedPassword);
            if ((src.Length != 0x31) || (src[0] != 0))
                return false;

            byte[] dst = new byte[0x10];
            Buffer.BlockCopy(src, 1, dst, 0, 0x10);
            byte[] buffer3 = new byte[0x20];
            Buffer.BlockCopy(src, 0x11, buffer3, 0, 0x20);

            using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(password, dst, 0x3e8))
            {
                buffer4 = bytes.GetBytes(0x20);
            }

            return ByteArraysEqual(buffer3, buffer4);
        }

        private static bool ByteArraysEqual(byte[] firstHash, byte[] secondHash)
        {
            int _minHashLength = firstHash.Length <= secondHash.Length ? firstHash.Length : secondHash.Length;
            var xor = firstHash.Length ^ secondHash.Length;
            for (int i = 0; i < _minHashLength; i++)
                xor |= firstHash[i] ^ secondHash[i];
            return 0 == xor;
        }
    }
}