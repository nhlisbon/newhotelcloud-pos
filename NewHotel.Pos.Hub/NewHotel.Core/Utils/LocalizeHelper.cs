﻿using System.Linq;
using System.Globalization;
using System.Resources;
using System.Reflection;

namespace NewHotel.Core
{
    public static class LocalizeHelper
    {
        private static ResourceManager _manager;
        public static Assembly Assembly;

        private static ResourceManager Manager
        {
            get
            {
                if (_manager == null)
                {
                    if (Assembly == null)
                        Assembly = Assembly.GetExecutingAssembly();

                    var resource = Assembly.GetManifestResourceNames().FirstOrDefault(x => x.IndexOf("LocalizedText") != -1);
                    if (resource != null)
                    {
                        _manager = new ResourceManager(resource.Replace(".resources", string.Empty), Assembly);
                        _manager.IgnoreCase = true;
                    }
                }

                return _manager;
            }
        }

        public static string Localize(CultureInfo culture, string name, string suffix)
        {
            return Manager.GetString(name + suffix, culture) ?? "{" + name + "}";
        }

        public static string Localize(CultureInfo culture, string name)
        {
            return Localize(culture, name, "Lbl");
        }
    }
}
