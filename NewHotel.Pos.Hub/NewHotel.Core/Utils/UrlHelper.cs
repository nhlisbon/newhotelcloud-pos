﻿using System;
using System.Web;

namespace NewHotel.Core
{
	public static class UrlHelper
	{
		public static string GetUrl(HttpRequest req, string s, string page)
		{
			var url = req.Url.ToString();
			return url.Substring(0, url.LastIndexOf(s) + s.Length) + page;
		}

		public static string GetUrl(HttpRequest req, string page)
		{
			return GetUrl(req, "/", page);
		}
	}
}
