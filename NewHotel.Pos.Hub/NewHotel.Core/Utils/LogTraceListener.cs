﻿using System;
using System.IO;
using System.Diagnostics;

namespace NewHotel.Core.Utils
{
	public class LogTraceListener : XmlWriterTraceListener
	{
		private class BackupFileStream : FileStream
		{
			private readonly object _sync = new object();
			private readonly Func<string, string> _backupName;
			private string _name;

			public BackupFileStream(string path, Func<string, string> backupName)
				: base(path, FileMode.OpenOrCreate, FileAccess.Write)
			{
				_backupName = backupName;
				_name = _backupName.Invoke(Name);
			}

			public override void Write(byte[] array, int offset, int count)
			{
				lock (_sync)
				{
					var name = _backupName.Invoke(Name);
					if (name.CompareTo(name) > 0)
					{
						Flush();
						File.Copy(Name, _name, true);
						SetLength(0);
						_name = name;
					}
				}

				Write(array, offset, count);
			}
		}

		private LogTraceListener(string filename, string name, Func<string, string> backupName)
			: base(new BackupFileStream(filename, backupName), name)
		{
		}

		public LogTraceListener(string filename, string name)
			: base(new BackupFileStream(filename, (n) => string.Format("{0}-{1}", n, DateTime.Now.Date.ToString("yyyyMMdd:HHmm"))), name)
		{
		}
	}
}
