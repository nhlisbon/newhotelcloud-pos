﻿using System;
using System.Web.Security;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Core
{
    public static class StringUtils
    {
        public static string GenerateRandomString(int length, string inistring)
        {
            int randNumber;
            var random = new Random((int)DateTime.Now.ToUtcDateTime().Ticks + 15);
            for (int i = 0; i < length; i++)
            {
                randNumber = random.Next(97, 123); //char {a-z}
                inistring = inistring + (char)randNumber;
            }

            return inistring;
        }

        public static string CreateMembershipPassword(int passwordLength, int carct)
        {
            return Membership.GeneratePassword(passwordLength, carct);
        }
       
        public static string CreateRandomString(int passwordLength, string initpass,  GenerateType typegen, int iter)
        {
            var allowedChars = typegen == GenerateType.both ? "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-"
               : (typegen == GenerateType.numbers ? "0123456789" : "ABCDEFGHJKLMNOPQRSTUVWXYZ");

            passwordLength = !string.IsNullOrEmpty(initpass) ? passwordLength - initpass.Length : passwordLength;
            var chars = new char[passwordLength];
            var rd = new Random((int)DateTime.Now.ToUtcDateTime().Ticks + iter);

            for (int i = 0; i < passwordLength; i++)
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];

            return string.IsNullOrEmpty(initpass) ? new string(chars) : initpass + new string(chars);
        }

        public static string CreateRandomCode( int iter)
        {
            var allowedChars = "0123456789";
            var chars = new char[3];
            var rd = new Random((int)DateTime.Now.ToUtcDateTime().Ticks + iter);

            for (int i = 0; i < 3; i++)
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];

            return new string(chars);
        }
    }
}
