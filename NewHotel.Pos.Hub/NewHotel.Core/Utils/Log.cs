﻿using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;

namespace NewHotel.Core
{
    public sealed class Log
    {
        #region Members

        private IDictionary<string, TraceSource> _sources;

        #endregion
        #region Public Properties

        public TraceSource this[string name]
        {
            get
            {
                TraceSource source = null;
                lock (this)
                {
                    if (_sources == null)
                        _sources = new Dictionary<string, TraceSource>();

                    if (!_sources.TryGetValue(name.ToUpperInvariant(), out source))
                    {
                        source = new TraceSource(name);
                        _sources.Add(name.ToUpperInvariant(), source);
                    }
                }

                return source;
            }
        }

        public override string ToString()
        {
            lock (this)
            {
                if (_sources == null)
                    return string.Empty;

                return string.Join(", ", _sources.Select(x => string.Format("{0}={1}", x.Key, x.Value.Listeners.Count > 0)).ToArray());
            }
        }

        public static readonly Log Source = new Log();

        #endregion

        public static void CheckDisposed(object obj, bool disposed)
        {
            if (disposed && obj != null)
            {
                var stackTrace = new StringBuilder();
                for (int i = 2; i <= 6; i++)
                {
                    var stackFrame = new StackFrame(i);
                    stackTrace.AppendFormat("\nFile: {0}, Line: {1}, Caller: {2}", stackFrame.GetFileName(), stackFrame.GetFileLineNumber(), stackFrame.GetMethod());
                }

                Log.Source["NewHotel.Core.GC"].TraceEvent(TraceEventType.Warning, 0, "Object disposed warning.\nType: {0}{1}",
                    obj.GetType().FullName, stackTrace.ToString());
            }
        }
    }
}
