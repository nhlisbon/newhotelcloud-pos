﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace NewHotel.Core
{
	public sealed class ArrayKey : IEquatable<ArrayKey>, IEqualityComparer<ArrayKey>
    {
		#region Members

		private readonly IComparable[] _keys;
        private readonly int _hashCode;

		#endregion
		#region Public Methods

		public ArrayKey(params IComparable[] keys)
        {
			_keys = keys ?? new IComparable[0];
            _hashCode = ToString().GetHashCode();
        }

        public IComparable this[int index]
        {
            get { return _keys[index]; }
            set { _keys[index] = value; }
        }

        public int Length
        {
            get { return _keys.Length; }
        }

        public override bool Equals(object obj)
        {
            var other = obj as ArrayKey;

            if (other != null)
                return Equals(other);

            return false;
        }

        public override int GetHashCode()
        {
            return _hashCode;
        }

        public override string ToString()
        {
			if (_keys.Length > 0)
                return string.Join(", ", _keys.Select(x => x == null ? "NULL" : x.ToString()).ToArray());

			return string.Empty;
        }

        #endregion
        #region IEquatable<ArrayKey> Members

        public bool Equals(ArrayKey other)
		{
			if (other != null)
			{
				if (other.Length == _keys.Length)
				{
					var x = _keys;
					var y = other._keys;

					for (int index = 0; index < x.Length; index++)
					{
						var xKey = x[index];
						var yKey = y[index];
						if (xKey != null && yKey != null)
						{
							if (xKey.CompareTo(yKey) != 0)
								return false;
						}
						else if ((xKey == null && yKey != null) || (xKey != null && yKey == null))
							return false;
					}

					return true;
				}
			}

			return false;
		}

        #endregion
        #region IEqualityComparer<ArrayKey> Members

        public bool Equals(ArrayKey x, ArrayKey y)
        {
            return x.Equals(y);
        }

        public int GetHashCode(ArrayKey obj)
        {
            return obj.GetHashCode();
        }

        #endregion
    }
}