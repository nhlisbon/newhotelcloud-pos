﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Data;

namespace NewHotel.Core
{
    public static class IDatabaseManagerExt
    {
        public static void SetCommandParameters(this IDatabaseManager manager,
            IDbCommand command, params KeyValuePair<string, object>[] parameters)
        {
            if (parameters != null && parameters.Length > 0)
            {
                foreach (var parameter in parameters)
                    manager.CreateParameter(command, parameter.Key, parameter.Value);
            }
        }

        public static void SetCommandParameters(this IDatabaseManager manager,
            IDbCommand command, IDictionary<string, object> parameters)
        {
            manager.SetCommandParameters(command, parameters.ToArray());
        }

        public static IDbCommand GetReplicateCommands(this IDatabaseManager manager,
            string schemaName, List<IDbCommand> commands,
            StringBuilder sql, IDictionary<string, object> fields)
        {
            var command = manager.CreateCommand("BasePersistent.Replicate.Command");
            command.CommandText = string.Format(sql.ToString(), schemaName);
            manager.SetCommandParameters(command, fields);

            commands.Add(command);
            return command;
        }
       
        public static void GetReplicateCommands(this IDatabaseManager manager,
            IDictionary<string, List<IDbCommand>> commands,
            StringBuilder sql, IDictionary<string, object> fields)
        {
            foreach (var schemaName in commands.Keys)
                manager.GetReplicateCommands(schemaName, commands[schemaName], sql, fields);
        }

        public static void GetReplicateCommands(this IDatabaseManager manager,
            IDataReader reader,
            IDictionary<string, List<IDbCommand>> commands,
            StringBuilder sql)
        {
            var fieldCount = reader.FieldCount;
            var fields = new Dictionary<string, object>(fieldCount);
            if (fieldCount > 0)
            {
                try
                {
                    while (reader.Read())
                    {
                        for (int index = 0; index < fieldCount; index++)
                            fields[reader.GetName(index)] = reader.GetValue(index);

                        manager.GetReplicateCommands(commands, sql, fields);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
        }

        public static void GetReplicateCommands(this IDatabaseManager manager,
            IDataReader reader, string schemaName,
            List<IDbCommand> commands, StringBuilder sql)
        {
            var fieldCount = reader.FieldCount;
            var fields = new Dictionary<string, object>(fieldCount);
            if (fieldCount > 0)
            {
                try
                {
                    while (reader.Read())
                    {
                        for (int index = 0; index < fieldCount; index++)
                            fields[reader.GetName(index)] = reader.GetValue(index);

                        manager.GetReplicateCommands(schemaName, commands, sql, fields);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
        }

        public static IDbCommand GetReplicateCommands(this IDatabaseManager manager,
            IDataRecord record,
            string schemaName, List<IDbCommand> commands,
            StringBuilder sql, params string[] fieldNames)
        {
            if (fieldNames.Length > 0)
            {
                var fields = new Dictionary<string, object>(fieldNames.Length);
                foreach (var fieldName in fieldNames)
                    fields[fieldName] = record.GetValue(record.GetOrdinal(fieldName));

                return manager.GetReplicateCommands(schemaName, commands, sql, fields);
            }

            return null;
        }

        public static IDbCommand GetReplicateCommands(this IDatabaseManager manager,
            IDataRecord record,
            string schemaName, List<IDbCommand> commands,
            StringBuilder sql)
        {
            var fieldCount = record.FieldCount;
            var fieldNames = new List<string>(fieldCount);
            for (int index = 0; index < fieldCount; index++)
                fieldNames.Add(record.GetName(index));

            return manager.GetReplicateCommands(record, schemaName, commands, sql, fieldNames.ToArray());
        }

        public static void GetReplicateCommands(this IDatabaseManager manager,
            IDataReader reader,
            IDictionary<string, List<IDbCommand>> commands,
            bool insert, string tableName, params string[] primaryKeyNames)
        {
            try
            {
                var fieldCount = reader.FieldCount;
                var fields = new Dictionary<string, object>(fieldCount);
                for (int index = 0; index < fieldCount; index++)
                    fields.Add(reader.GetName(index), null);

                var sql = new StringBuilder();
                sql.Append("merge into {0}.");
                sql.Append(tableName);
                sql.AppendLine(" using dual on");
                sql.AppendLine("(" + string.Join(" and ", primaryKeyNames.Select(x => x + " = :" + x).ToArray()) + ")");
                var updateFields = fields.Keys.Where(x => !primaryKeyNames.Contains(x, StringComparer.InvariantCultureIgnoreCase)).Select(x => x + " = :" + x).ToArray();
                if (updateFields.Length > 0)
                {
                    sql.AppendLine("when matched then update set");
                    sql.AppendLine(string.Join(",", updateFields));
                }
                if (insert)
                {
                    var insertFields = fields.Keys.ToArray();
                    if (insertFields.Length > 0)
                    {
                        sql.AppendLine("when not matched then insert");
                        sql.AppendLine("(" + string.Join(",", insertFields) + ")");
                        sql.AppendLine("values");
                        sql.AppendLine("(" + string.Join(",", insertFields.Select(x => ":" + x).ToArray()) + ")");
                    }
                }

                while (reader.Read())
                {
                    foreach (var schemaName in commands.Keys)
                    {
                        for (int index = 0; index < fieldCount; index++)
                            fields[reader.GetName(index)] = reader.GetValue(index);

                        manager.GetReplicateCommands(schemaName, commands[schemaName], sql, fields);
                    }
                }
            }
            finally
            {
                reader.Close();
            }
        }

        public static void ExecuteCommands(this IDatabaseManager manager,
            IDictionary<string, List<IDbCommand>> commands, StringBuilder exceptions)
        {
            try
            {
                foreach (var schema in commands.Keys)
                {
                    manager.BeginTransaction();
                    try
                    {
                        List<IDbCommand> cmds;
                        if (commands.TryGetValue(schema, out cmds))
                        {
                            foreach (var cmd in cmds)
                            {
                                cmd.Transaction = manager.Transaction;
                                manager.ExecuteNonQuery(cmd);
                            }
                        }

                        manager.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        manager.RollbackTransaction();
                        exceptions.AppendLine(schema);
                        exceptions.AppendLine(ex.Message);
                    }
                }
            }
            finally
            {
                foreach (var command in commands.SelectMany(x => x.Value))
                    command.Dispose();
            }
        }

        private sealed class OpenSessionContext : IDisposable
		{
			private readonly IDatabaseManager _manager;

			public OpenSessionContext(IDatabaseManager manager)
			{
				_manager = manager;
				_manager.Open();
			}

			public void Dispose()
			{
				_manager.Close();
			}
		}

        public static IDisposable OpenSession(this IDatabaseManager manager)
        {
            return new OpenSessionContext(manager);
        }
    }
}
