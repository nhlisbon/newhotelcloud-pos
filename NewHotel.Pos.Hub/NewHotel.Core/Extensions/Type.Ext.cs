﻿//using System;
//using System.Collections.Generic;
//using NewHotel.DataAnnotations;

//namespace NewHotel.Core
//{
//    public static class TypeExtension
//    {
//        #region Accessors

//        private static IDictionary<Type, IDictionary<string, IAccessor>> _propAccessors =
//            new Dictionary<Type, IDictionary<string, IAccessor>>();
//        /// <summary>
//        /// Propiedades de la clase
//        /// </summary>
//        public static IDictionary<string, IAccessor> PropAccessors(this Type type)
//        {
//            IDictionary<string, IAccessor> accessors;

//            lock (type)
//            {
//                if (!_propAccessors.TryGetValue(type, out accessors))
//                {
//                    accessors = type.GetAccessors();
//                    _propAccessors.Add(type, accessors);
//                }
//            }

//            return accessors;
//        }

//        #endregion
//    }
//}
