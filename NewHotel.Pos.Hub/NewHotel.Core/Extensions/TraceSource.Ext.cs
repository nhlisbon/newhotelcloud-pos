﻿using System;
using System.Diagnostics;
using NewHotel.DataAnnotations;

namespace NewHotel.Core
{
    public static class TraceSourceExt
    {
        public static void TraceStart(this TraceSource source, string name, int id)
        {
            source.TraceEvent(TraceEventType.Start, id, name);
        }

        public static void TraceStart(this TraceSource source, string name)
        {
            TraceStart(source, name, 0);
        }

        public static void TraceStop(this TraceSource source, string name, int id)
        {
            source.TraceEvent(TraceEventType.Stop, id, name);
        }

        public static void TraceStop(this TraceSource source, string name)
        {
            TraceStop(source, name, 0);
        }

        public static void TraceStop(this TraceSource source, string name, TimeSpan duration)
        {
            source.TraceEvent(TraceEventType.Stop, 0, "{0}: Time: {1}", name, duration);
        }

        public static void TraceInfo(this TraceSource source, string message, params object[] args)
        {
            source.TraceEvent(TraceEventType.Information, 0, message, args);
        }

        public static void TraceData(this TraceSource source, object data)
        {
            source.TraceData(TraceEventType.Information, 0, data.ToXml(true));
        }

        public static void TraceWarning(this TraceSource source, string warning, params object[] args)
        {
            source.TraceEvent(TraceEventType.Warning, 0, warning, args);
        }

        public static void TraceError(this TraceSource source, string error, params object[] args)
        {
            source.TraceEvent(TraceEventType.Error, 0, error, args);
        }

        public static void TraceException(this TraceSource source, Exception ex)
        {
            source.TraceEvent(TraceEventType.Error, 0, ex.Message + "\n" + ex.StackTrace);
            if (ex.InnerException != null)
                source.TraceEvent(TraceEventType.Error, 0, ex.InnerException.Message + "\n" + ex.InnerException.StackTrace);
        }
    }
}
