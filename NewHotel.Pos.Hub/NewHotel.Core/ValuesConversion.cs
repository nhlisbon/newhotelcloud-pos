﻿using System;

namespace NewHotel.Core
{
    public static class ValuesConversion
    {
        public static object ToType(this object value, Type type)
        {
            if (value != null)
            {
                if (type == typeof(Guid))
                    return new Guid(value.ToString());
                else
                    return System.Convert.ChangeType(value, type);
            }

            return null;
        }
    }
}
