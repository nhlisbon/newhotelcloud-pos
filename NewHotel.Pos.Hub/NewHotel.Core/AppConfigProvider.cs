﻿using System.Configuration;

namespace NewHotel.Core
{
    public sealed class AppConfigProvider : IAppConfigProvider
    {
        #region Constants

        private const string sectionName = "app.config";

        #endregion
        #region Properties

        public string Provider { get; private set; }
        public string Service { get; private set; }
        public string Database { get; private set; }
        public string User { get; private set; }
        public string Password { get; private set; }
        public bool Encrypted { get; private set; }
        public byte PoolSize { get; private set; }
        public long Language { get; private set; }
        public string Cache { get; private set; }
        public string SessionCache { get; private set; }
        public string Context { get; private set; }
        public int MaxTabCount { get; private set; }
        public string ModuleName { get; private set; }
        public string PoolName { get; private set; }

        #endregion
        #region Private Properties

        private AppConfigSection AppConfigSection
        {
            get { return ConfigurationManager.GetSection(sectionName) as AppConfigSection; }
        }

        #endregion
        #region Constructor

        public AppConfigProvider()
        {
            ReadConfig();
        }

        #endregion
        #region Private Methods

        private void ReadConfig()
        {
            var appConfigSection = AppConfigSection;
            if (appConfigSection != null)
            {
                Provider = appConfigSection.Provider;
                Service = appConfigSection.Service;
                Database = appConfigSection.Database;
                User = appConfigSection.User;
                Password = appConfigSection.Password;
                Encrypted = appConfigSection.Encrypted;
                PoolSize = appConfigSection.PoolSize;
                Language = appConfigSection.Language;
                Cache = appConfigSection.Cache;
                SessionCache = appConfigSection.SessionCache;
                Context = appConfigSection.Context;
                MaxTabCount = appConfigSection.MaxTabCount;
                ModuleName = appConfigSection.ModuleName;
                PoolName = appConfigSection.PoolName;
            }
        }

        #endregion
        #region Public Methods

        public void Refresh()
        {
            ConfigurationManager.RefreshSection(sectionName);
            ReadConfig();
        }

        #endregion
    }
}