﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace NewHotel.Core
{
    public abstract class CacheData
    {
        public readonly DataContainer LanguageDataStorage = new DataContainer();
        public readonly DataContainer EnumDataStorage = new DataContainer();

        protected readonly IDictionary<Type, long[]> EnumTypes;

        public CacheData()
        {
            EnumTypes = NewHotelAppContext.GetInstance().GetEnumTypes()
                .ToDictionary(e => e, e => Enum.GetValues(e).Cast<long>().ToArray());
        }

        public abstract void DataBinding(IDatabaseManager manager, long language);

        public abstract IDictionary<string, ListData> GetEnums(long languageId);
        public abstract ListData GetLanguages(long languageId);
    }
}