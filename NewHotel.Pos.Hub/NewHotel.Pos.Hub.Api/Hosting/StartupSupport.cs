﻿using Microsoft.Owin.Security.OAuth;
using NewHotel.Pos.Hub.Api.Controllers;
using NewHotel.Pos.Hub.Api.Models;
using NewHotel.Pos.Hub.Core;
using NewHotel.Pos.IoC;
using Owin;
using System;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace NewHotel.Pos.Hub.Hosting
{
	public static class StartupSupport
	{
		public static IAppBuilder ConfigureWebApi(
			this IAppBuilder appBuilder,
			PosDependencyResolver resolver)
		{
			HttpConfiguration config = new HttpConfiguration();

			resolver.RegisterBasedOn<BaseController>();
			config.DependencyResolver = new CustomDependencyResolver(resolver);

			config.Formatters.Remove(config.Formatters.JsonFormatter);
			//config.Formatters.Remove(config.Formatters.XmlFormatter);
			//config.Formatters.Add(new NewJsonMediaTypeFormatter());
			config.Formatters.Insert(0, new NewJsonMediaTypeFormatter());
			config.Formatters.Add(new NewJsonMediaTypeFormatter());
			config.ConfigureRoutes();
			appBuilder.Use(async (context, next) =>
			{
				var apiContext = (context.Request.User as ClaimsPrincipal)?.Claims.GetApiContext();
				IDisposable scope = null;

				try
				{
					if (apiContext != null)
					{
						// scope = CwFactory.Instance.BeginScope();
						scope = CwFactory.Instance.RequireScope();
						var hubSessionData = BaseService.GetHubSessionData(apiContext.SessionId);
						var factory = CwFactory.Resolve<IPosSessionFactory>();
						if (hubSessionData == null)
							factory.Create(apiContext);
						else
						{
							factory.Create(apiContext.SessionId, hubSessionData, apiContext.IsInternal);
						}
					}

					await next();
				}
				finally
				{
					scope?.Dispose();
				}
			});
			return appBuilder
				.UseWebApi(config)
				;
		}

		public static HttpConfiguration ConfigureRoutes(this HttpConfiguration config)
		{
			config.MapHttpAttributeRoutes();
			config.Routes.MapHttpRoute(
				name: "DefaultApi",
				routeTemplate: "api/{controller}/{id}",
				defaults: new { id = RouteParameter.Optional }
			);

			return config;
		}
	}
}
