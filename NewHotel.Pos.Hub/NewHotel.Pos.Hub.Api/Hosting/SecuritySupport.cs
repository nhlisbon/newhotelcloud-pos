﻿using Microsoft.Owin.Security;
using Owin;
using System;

using Microsoft.Owin.Security.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace NewHotel.Pos.Hub.Hosting
{
	public static class SecuritySupport
	{
		public static SymmetricSecurityKey GetSymmetricSecurityKey() => new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes("856FECBA3B06519C8DDDBC80BB080553"));

		private static JwtBearerAuthenticationOptions jwtBearerAuthenticationOptions;

		public const string NameId = JwtRegisteredClaimNames.NameId;
		public const string UniqueName = JwtRegisteredClaimNames.UniqueName;
		public const string SessionId = JwtRegisteredClaimNames.Jti;

		public static string AuthenticationType => jwtBearerAuthenticationOptions.AuthenticationType;//bearerAuthenticationOptions.AuthenticationType;

		public static string GenerateToken(this ClaimsIdentity identity, DateTimeOffset issuedAt, DateTimeOffset expires)
		{
			JwtSecurityTokenHandler tokenHandler = new();
			var token = tokenHandler.CreateToken(
				new SecurityTokenDescriptor
				{
					Subject = identity,
					Expires = expires.UtcDateTime,
					IssuedAt = issuedAt.UtcDateTime,
					SigningCredentials = new SigningCredentials(GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256Signature),
				});

			return tokenHandler.WriteToken(token);
		}

		public static ClaimsIdentity CreateIdentity(Guid sessionId, Guid userId, string description)
		{
			var claims = new ClaimsIdentity(AuthenticationType)
				.AddClaim(NameId, userId.ToString("n"))
				.AddClaim(UniqueName, description)
				.AddClaim(SessionId, sessionId.ToString("n"))
				;

			return claims;
		}

		public static ClaimsIdentity AddClaim(this ClaimsIdentity claims, string type, string value)
		{
			claims.AddClaim(new Claim(type, value));
			return claims;
		}

		public static void BuildSecurity()
		{
			jwtBearerAuthenticationOptions = new JwtBearerAuthenticationOptions
			{
				AuthenticationMode = AuthenticationMode.Active,
				//AllowedAudiences = new string[] { "http://localhost:8080" },
				TokenValidationParameters = new TokenValidationParameters
				{
					// The signing key must match!
					ValidateIssuerSigningKey = true,
					IssuerSigningKey = GetSymmetricSecurityKey(),

					// Validate the JWT Issuer (iss) claim
					ValidateIssuer = false,
					//ValidIssuer = "http://localhost:8080",

					// Validate the JWT Audience (aud) claim
					ValidateAudience = false,
					//ValidAudience = "http://localhost:8080",

					// Validate the token expiry
					ValidateLifetime = true,

					// If you want to allow a certain amount of clock drift, set that here:
					ClockSkew = TimeSpan.Zero
				},
			};
		}

		public static IAppBuilder ConfigureOAuth(this IAppBuilder appBuilder)
		{
			BuildSecurity();
			return appBuilder
				.UseJwtBearerAuthentication(jwtBearerAuthenticationOptions)
				;
		}
	}
}
