﻿using System;
using System.IO;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Net;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Hosting
{
	public static class MediaTypeConstants
	{
		public static MediaTypeHeaderValue Clone(this MediaTypeHeaderValue obj)
		{
			return (MediaTypeHeaderValue)((ICloneable)obj).Clone();
		}

		private static readonly MediaTypeHeaderValue _defaultApplicationXmlMediaType = new MediaTypeHeaderValue("application/xml");

		private static readonly MediaTypeHeaderValue _defaultTextXmlMediaType = new MediaTypeHeaderValue("text/xml");

		private static readonly MediaTypeHeaderValue _defaultApplicationJsonMediaType = new MediaTypeHeaderValue("application/json");

		private static readonly MediaTypeHeaderValue _defaultTextJsonMediaType = new MediaTypeHeaderValue("text/json");

		private static readonly MediaTypeHeaderValue _defaultApplicationOctetStreamMediaType = new MediaTypeHeaderValue("application/octet-stream");

		private static readonly MediaTypeHeaderValue _defaultApplicationFormUrlEncodedMediaType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

		private static readonly MediaTypeHeaderValue _defaultApplicationBsonMediaType = new MediaTypeHeaderValue("application/bson");

		public static MediaTypeHeaderValue ApplicationOctetStreamMediaType => _defaultApplicationOctetStreamMediaType.Clone();

		public static MediaTypeHeaderValue ApplicationXmlMediaType => _defaultApplicationXmlMediaType.Clone();

		public static MediaTypeHeaderValue ApplicationJsonMediaType => _defaultApplicationJsonMediaType.Clone();

		public static MediaTypeHeaderValue TextXmlMediaType => _defaultTextXmlMediaType.Clone();

		public static MediaTypeHeaderValue TextJsonMediaType => _defaultTextJsonMediaType.Clone();

		public static MediaTypeHeaderValue ApplicationFormUrlEncodedMediaType => _defaultApplicationFormUrlEncodedMediaType.Clone();

		public static MediaTypeHeaderValue ApplicationBsonMediaType => _defaultApplicationBsonMediaType.Clone();
	}

	public sealed class NewJsonMediaTypeFormatter : MediaTypeFormatter
	{
		private readonly JsonSerializerOptions _jsonSerializerOptions;
		private readonly JsonWriterOptions jsonWriterOptions;

		public NewJsonMediaTypeFormatter()
		{
			SupportedMediaTypes.Add(MediaTypeConstants.ApplicationJsonMediaType);
			SupportedMediaTypes.Add(MediaTypeConstants.TextJsonMediaType);
			_jsonSerializerOptions = new JsonSerializerOptions
			{
				// PropertyNameCaseInsensitive = true,
				PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
				DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
				Converters = { new Communication.Api.GuidConverter(),new Communication.Api.TypeConverter(), },
				// WriteIndented = true
				// TypeInfoResolver = new GuidConverter()
			};
			jsonWriterOptions = new JsonWriterOptions { Indented = false };
		}

		public override bool CanReadType(Type type)
		{
			return type != null;
		}

		public override bool CanWriteType(Type type)
		{
			return type != null;
		}

		public override async Task<object> ReadFromStreamAsync(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
		{
			if (readStream == null)
			{
				throw new ArgumentNullException(nameof(readStream));
			}

			using (var streamReader = new StreamReader(readStream, Encoding.UTF8))
			{
				var jsonString = await streamReader.ReadToEndAsync();
				return JsonSerializer.Deserialize(jsonString, type, _jsonSerializerOptions);
			}
		}

		public override async Task WriteToStreamAsync(Type type, object value, Stream writeStream, HttpContent content, TransportContext transportContext)
		{
			if (writeStream == null)
			{
				throw new ArgumentNullException(nameof(writeStream));
			}

			//var aa = JsonSerializer.SerializeToElement(value, type, _jsonSerializerOptions);
			//var xx = JsonSerializer.Serialize(value, type, _jsonSerializerOptions);
			//var yy = Newtonsoft.Json.JsonConvert.SerializeObject(value);

			using (var utf8JsonWriter = new Utf8JsonWriter(writeStream, jsonWriterOptions))
			{
				try
				{
					JsonSerializer.Serialize(utf8JsonWriter, value, type, _jsonSerializerOptions);
					await utf8JsonWriter.FlushAsync();
				}
				catch (Exception)
				{

					throw;
				}
			}
		}
	}
}
