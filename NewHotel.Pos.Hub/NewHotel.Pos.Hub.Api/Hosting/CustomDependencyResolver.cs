﻿using NewHotel.Pos.IoC;
using System;
using System.Collections.Generic;
using System.Web.Http.Dependencies;

namespace NewHotel.Pos.Hub.Hosting
{
	public class CustomDependencyResolver : IDependencyResolver
	{
		PosDependencyResolver resolver;

		public CustomDependencyResolver(PosDependencyResolver resolver)
		{
			this.resolver = resolver;
		}

		public IDependencyScope BeginScope()
		{
			return new CustomDependencyScope(resolver.BeginScope());
		}

		public void Dispose()
		{
		}

		public object GetService(Type serviceType)
		{
			return resolver.GetService(serviceType);
		}

		public IEnumerable<object> GetServices(Type serviceType)
		{
			return resolver.GetServices(serviceType);
		}
	}

	public class CustomDependencyScope : IDependencyScope
	{
		private readonly PosDependencyScope scope;

		public CustomDependencyScope(PosDependencyScope scope)
		{
			this.scope = scope;
		}

		public void Dispose()
		{
			scope.Dispose();
		}

		public object GetService(Type serviceType)
		{
			return scope.GetService(serviceType);
		}

		public IEnumerable<object> GetServices(Type serviceType)
		{
			return scope.GetServices(serviceType);
		}
	}
}
