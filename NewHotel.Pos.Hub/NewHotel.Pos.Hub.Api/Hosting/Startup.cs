﻿using Microsoft.Owin.Hosting;
using NewHotel.Pos.IoC;
using Owin;
using System;

namespace NewHotel.Pos.Hub.Hosting
{
	public sealed class Startup
	{
		public void Configuration(IAppBuilder appBuilder, PosDependencyResolver resolver)
		{
			appBuilder
				.ConfigureOAuth()
				.ConfigureWebApi(resolver)
				;
		}
	}

	public class WebSelfHost
	{
		const string DEFAULT_BASEADDRESS = "http://*:13002/";
		readonly string baseAddress;

		public WebSelfHost(string baseAddress)
		{
			this.baseAddress = string.IsNullOrEmpty(baseAddress) ? DEFAULT_BASEADDRESS : baseAddress;
		}

		public IDisposable Start(PosDependencyResolver resolver)
		{
			return WebApp.Start(
				baseAddress,
				app =>
				{
					new Startup().Configuration(app, resolver);
					//app.
				})
				;
			// return WebApp.Start<Startup>(url: baseAddress);
		}
	}
}
