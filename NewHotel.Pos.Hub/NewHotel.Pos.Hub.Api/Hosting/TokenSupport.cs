﻿using Microsoft.Owin.Infrastructure;
using NewHotel.Pos.Hub.Api.Models;
using NewHotel.Pos.Hub.Business.Interfaces;
using System;
using System.Security.Claims;
using System.Collections.Generic;
using NewHotel.Pos.Hub.Core;
using NewHotel.Pos.Hub.Services;
using NewHotel.Contracts;
using NewHotel.Pos.Hub.Model.Session;

namespace NewHotel.Pos.Hub.Hosting
{
	public static class TokenSupport
	{
		public static ApiValidationResult<LoginResultModel> CreateToken(this ISessionService sessionService, LoginModel loginModel)
		{
			return sessionService
				.Login(loginModel)
				.Map(session =>
				{
					var token = SecuritySupport
						.CreateIdentity(session.Id, session.User.Id, session.User.Description)
						.SetClaims(session.User, session.InstallationId)
						.CreateToken()
						;
					return new LoginResultModel { SessionId = session.Id, Token = token, UserName = session.User.Description };
				});
		}

		public static ApiValidationResult<LoginResultModel> UpdateToken(this ISessionService sessionService, UpdateSessionModel sessionModel)
		{
			return sessionService
				.UpdateSession(sessionModel)
				.Map(session =>
				{
					var token = SecuritySupport
						.CreateIdentity(session.SessionId, session.UserId, session.UserName)
						.UpdateToken(session)
						.CreateToken()
						;
					return new LoginResultModel { SessionId = session.SessionId, Token = token, UserName = session.UserName };
				});
		}

		public static ClaimsIdentity SetClaims(
			this ClaimsIdentity identity,
			Model.UserRecord user,
			Guid installationId,
			long applicationId = 107L,
			long languageId = 1033L)
		{
			return identity
				.AddClaim(nameof(ApiContextModel.UserLogin), user.UserLogin)
				.AddClaim(nameof(ApiContextModel.IsInternal), user.UserInternal.ToString())
				.AddClaim(nameof(ApiContextModel.InstallationId), installationId.ToString("n"))
				.AddClaim(nameof(ApiContextModel.ApplicationId), applicationId.ToString())
				.AddClaim(nameof(ApiContextModel.LanguageId), languageId.ToString())
				;
		}

		public static string CreateToken(this ClaimsIdentity identity)
		{
			var currentUtc = new SystemClock().UtcNow;

			return identity.GenerateToken(
				currentUtc,
				currentUtc.Add(TimeSpan.FromDays(3)));
		}

		public static ClaimsIdentity UpdateToken(this ClaimsIdentity identity, IPosSessionContext apiContext)
		{
			return identity
				.AddClaim(nameof(ApiContextModel.UserLogin), apiContext.UserLogin)
				.AddClaim(nameof(ApiContextModel.IsInternal), apiContext.IsInternal.ToString())
				.AddClaim(nameof(ApiContextModel.InstallationId), apiContext.InstallationId.ToString("n"))
				.AddClaim(nameof(ApiContextModel.ApplicationId), apiContext.ApplicationId.ToString())
				.AddClaim(nameof(ApiContextModel.LanguageId), apiContext.LanguageId.ToString())
				.AddClaim(nameof(ApiContextModel.StandId), apiContext.StandId.ToString("n"))
				.AddClaim(nameof(ApiContextModel.CashierId), apiContext.CashierId.ToString("n"))
				.AddClaim(nameof(ApiContextModel.TaxSchemaId), apiContext.TaxSchemaId.ToString("n"))
				.AddClaim(nameof(ApiContextModel.DocumentSign), ((long)apiContext.SignatureMode).ToString())
				.AddClaim(nameof(ApiContextModel.WorkDate), apiContext.WorkDate.ToString("O"))
				.AddClaim(nameof(ApiContextModel.Country), apiContext.Country)
				;
		}

		public static ApiContextModel GetApiContext(this IEnumerable<Claim> claims)
		{
			var result = new ApiContextModel();

			foreach (var claim in claims)
			{
				switch (claim.Type)
				{
					case SecuritySupport.UniqueName:
						result.UserName = claim.Value;
						break;
					case SecuritySupport.NameId:
						Guid.TryParse(claim.Value, out Guid userId);
						result.UserId = userId;
						break;
					case SecuritySupport.SessionId:
						Guid.TryParse(claim.Value, out Guid sessionId);
						result.SessionId = sessionId;
						break;
					case nameof(ApiContextModel.UserLogin):
						result.UserLogin = claim.Value;
						break;
					case nameof(ApiContextModel.IsInternal):
						bool.TryParse(claim.Value, out bool isInternal);
						result.IsInternal = isInternal;
						break;
					case nameof(ApiContextModel.InstallationId):
						Guid.TryParse(claim.Value, out Guid installationId);
						result.InstallationId = installationId;
						break;
					case nameof(ApiContextModel.StandId):
						Guid.TryParse(claim.Value, out Guid standId);
						result.StandId = standId;
						break;
					case nameof(ApiContextModel.CashierId):
						Guid.TryParse(claim.Value, out Guid cashierId);
						result.CashierId = cashierId;
						break;

					case nameof(ApiContextModel.TaxSchemaId):
						Guid.TryParse(claim.Value, out Guid taxSchemaId);
						result.TaxSchemaId = taxSchemaId;
						break;

					case nameof(ApiContextModel.ApplicationId):
						long.TryParse(claim.Value, out long applicationId);
						result.ApplicationId = applicationId;
						break;

					case nameof(ApiContextModel.DocumentSign):
						long.TryParse(claim.Value, out long documentSign);
						result.DocumentSign = documentSign;
						break;
					case nameof(ApiContextModel.LanguageId):
						long.TryParse(claim.Value, out long languageId);
						result.LanguageId = languageId;
						break;
					case nameof(ApiContextModel.WorkDate):
						DateTime.TryParseExact(claim.Value, "O", null, System.Globalization.DateTimeStyles.RoundtripKind, out DateTime workDate);
						result.WorkDate = workDate;
						break;
					case nameof(ApiContextModel.Country):
						result.Country = claim.Value;
						break;
				}
			}

			return result;
		}
	}
}
