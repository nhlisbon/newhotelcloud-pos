﻿using NewHotel.Pos.Hub.Api.Models;
using NewHotel.Pos.Hub.Hosting;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Hub.Services;
using System.Web.Http;

namespace NewHotel.Pos.Hub.Api.Controllers
{

	[RoutePrefix("api/auth")]
	public class AuthenticationController : BaseController
	{
		private readonly ISessionService sessionService;

		public AuthenticationController(ISessionService sessionService)
		{
			this.sessionService = sessionService;
		}

		[HttpGet]
		[Route("user")]
		public ApiValidationResult<UserRecord> GetUserByCode(string userCode)
		{
			return sessionService.GetUserByCode(userCode);
		}

		[HttpPost]
		[Route("login")]
		public ApiValidationResult<LoginResultModel> Login(LoginModel loginModel)
		{
			return sessionService.CreateToken(loginModel);
		}

		[HttpPut]
		[Route("login")]
		[Authorize]
		public ApiValidationResult<LoginResultModel> UpdateSession(UpdateSessionModel sessionModel)
		{
			return sessionService.UpdateToken(sessionModel);
		}
	}
}
