﻿using NewHotel.Pos.Hub.Api.Models;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Services;
using System;
using System.Web.Http;

namespace NewHotel.Pos.Hub.Api.Controllers
{
	[RoutePrefix("api/ticket")]
	[Authorize]
	public class TicketController : BaseController
	{
		private readonly ITicketService ticketService;

		public TicketController(ITicketService ticketService)
		{
			this.ticketService = ticketService;
		}

		[HttpGet]
		public ApiValidationResult<Model.TicketInfo[]> ListTickets()
		{
			return ticketService.ListTickets();
		}

        [HttpGet]
        [Route("withproducts")]
        public ApiValidationResult<Model.TicketWithProducts> ListTicketsWithProducts(Guid ticketId)
        {
            return ticketService.ListTicketsWithProducts(ticketId);
        }

        [HttpGet]
		[Route("series")]
		public ApiValidationResult<Model.DocumentSerieRecord[]> GetTicketSerieByStandCashier(Guid standId, Guid cashierId)
		{
			return ticketService.GetTicketSerieByStandCashier(standId, cashierId);
		}

		[HttpPost]
		[Route("items")]
		public ApiValidationResult<NewHotel.Contracts.POSTicketContract> AddTicketItem(NewHotel.Contracts.NewTicketItemModel ticketItem)
		{
			return ticketService
				.AddTicketItem(ticketItem)
				;
		}
	}
}
