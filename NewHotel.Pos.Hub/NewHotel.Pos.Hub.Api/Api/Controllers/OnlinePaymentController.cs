﻿using NewHotel.Pos.Hub.Api.Models;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Contracts.PaySystem;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace NewHotel.Pos.Hub.Api.Controllers
{
	[RoutePrefix("api/onlinepayment")]
	[Authorize]
	public class OnlinePaymentController : BaseController
	{
		private readonly IPaySystemBusiness paySystemBusiness;

		public OnlinePaymentController(IPaySystemBusiness paySystemBusiness)
		{
			this.paySystemBusiness = paySystemBusiness;
		}

		[HttpPost]
		[Route("pay")]
		public async Task<ApiValidationResult<ExternalPaymentResponseContract>> RequestPayment(ExternalPaymentRequestContract request)
		{
			return await paySystemBusiness.RequestPayment(request);
		}

		[HttpPost]
		[Route("confirm")]
		public async Task<ApiValidationResult<ExternalPaymentEntryContract>> ConfirmPayment(Guid entryId)
		{
			return await paySystemBusiness.UpdatePendingPayment(entryId);
		}

		[HttpPost]
		[Route("updatedevice")]
		public async Task<ApiValidationResult<ExternalPaymentEntryContract>> UpdateDevice(ExternalPaymentDeviceCompleteRequest request)
		{
			return await paySystemBusiness.UpdateDevicePayment(request);
		}

		[HttpGet]
		[Route("terminals")]
		public async Task<ApiValidationResult<RequestTerminalsContract>> GetPaymentTerminals(Guid originId)
		{
			return await paySystemBusiness.GetPayTerminals(originId);
		}

		[HttpGet]
		[Route("ticket/{ticketId}")]
		public ApiValidationResult<TicketExternalPayments> GetTicketExternalPayments([FromUri] Guid ticketId)
		{
			return paySystemBusiness.GetTicketExternalPayments(ticketId);
		}
	}
}
