﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http;
using System.Security.Claims;
using NewHotel.Pos.Hub.Hosting;
using NewHotel.Pos.Hub.Api.Models;
using NewHotel.Pos.IoC;
using NewHotel.Pos.Hub.Core;

namespace NewHotel.Pos.Hub.Api.Controllers
{
	public class BaseController : ApiController
	{
		//ApiContextModel apiContext;

		public BaseController()
		{
		}

		//public ApiContextModel ApiContext  => apiContext;

		public override Task<HttpResponseMessage> ExecuteAsync(HttpControllerContext controllerContext, CancellationToken cancellationToken)
		{
			// controllerContext.RequestContext.Configuration.DependencyResolver.GetService(typeof(ApiContextModel));

			return base.ExecuteAsync(controllerContext, cancellationToken);
		}
	}

	public class HealthController : BaseController
	{
		[HttpGet]
		public string Get()
		{
			return "Healthy";
		}
	}
}
