﻿using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;
using NewHotel.Pos.Hub.Api.Models;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Contracts.Common.Records.Stands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;

namespace NewHotel.Pos.Hub.Api.Controllers
{

	[RoutePrefix("api/stand")]
	[Authorize]
	public class StandController : BaseController
	{
		private readonly IStandBusiness standBusiness;

		public StandController(IStandBusiness standBusiness)
		{
			this.standBusiness = standBusiness;
		}

		[HttpGet]
		[Route("cashiers")]
		public ApiValidationResult<CashierRecord[]> GetCashiers()
		{
			return standBusiness.GetCashiers().ToArray();
		}

		[HttpGet]
		[Route("list")]
		public ApiValidationResult<POSStandRecord[]> GetStands(Guid cashierId)
		{
			return standBusiness.GetStands(cashierId,0).ToArray();
		}

		[HttpGet]
		[Route("saloons")]
		public ApiValidationResult<Model.SaloonRecord[]> ListSaloons(Guid standId)
		{
			return standBusiness.GetSaloonsByStand(standId, 1033).ToArray();
		}

        [HttpGet]
        [Route("products")]
        public ApiValidationResult<Model.ProductInStandRecord[]> ListProducts(Guid standId)
        {
            return standBusiness.GetProductsByStand(standId, 1033).ToArray();
        }
    }
}
