﻿using NewHotel.Pos.Hub.Api.Models;
using NewHotel.Pos.Hub.Business.Interfaces;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Hub.Model.Cashier.Contracts.Settings;
using NewHotel.Pos.Hub.Model.Session;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace NewHotel.Pos.Hub.Api.Controllers
{
	[RoutePrefix("api/settings")]
	public class SettingsController : BaseController
	{
		private readonly IPosSessionContext sessionContext;
		private readonly ISettingBusiness settingBusiness;

		public SettingsController(
			IPosSessionContext sessionContext,
			ISettingBusiness settingBusiness)
		{
			this.sessionContext = sessionContext;
			this.settingBusiness = settingBusiness;
		}

		[HttpGet]
		[Route("hotels")]
		public ApiValidationResult<HotelRecord[]> GetHotels(Guid userId)
		{
			return settingBusiness.GetHotels(userId).ToArray();
		}

		[HttpGet]
		//[Route("hotels")]
		public ApiValidationResult<NewHotel.Contracts.Pos.Records.POSGeneralSettingsRecord> GetGeneralSettings(bool fromCloud)
		{
			return settingBusiness.GetGeneralSettings(sessionContext.InstallationId, fromCloud);
		}


		[HttpPost]
		[Route("images")]
		public ApiValidationResult<DownloadedImageContact[]> GetImages(KeyValuePair<string, DateTime>[] paths)
		{
			return settingBusiness.DownloadImages(paths).ItemSource.ToArray();
		}

		[HttpPost]
		[Route("sync-local-stock")]
		public async Task<ApiValidationResult<string>> SyncLocalStock()
		{
			return await settingBusiness.SyncLocalStock();
		}
	}
}
