﻿using NewHotel.Contracts;
using NewHotel.Pos.Hub.Model.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Api.Models
{
	public class ApiContextModel : IPosSessionContext, IDisposable
	{
		public Guid SessionId { get; set; }
		public long ApplicationId { get; set; }
		public string UserName { get; set; }
		public string UserLogin { get; set; }
		public Guid UserId { get; set; }
		public bool IsInternal { get; set; }
		public Guid InstallationId { get; set; }
		public Guid StandId { get; set; }
		public Guid CashierId { get; set; }
		public Guid TaxSchemaId { get; set; }
		public long DocumentSign { get; set; }
		public DateTime WorkDate { get; set; }
		public long LanguageId { get; set; }
		public string Country { get; set; }
		public string[] Roles { get; set; }
		public string[] Permissions { get; set; }

		public DocumentSign SignatureMode { get => (DocumentSign)DocumentSign; set => DocumentSign = (long)value; }

		public void Dispose()
		{
		}
	}
}
