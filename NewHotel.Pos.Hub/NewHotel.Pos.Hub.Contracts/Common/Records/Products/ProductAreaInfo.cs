﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Model
{
    public class OrderAreaInfo
    {
        public Guid Id { get; set; }
        public Guid? AreaId { get; set; }
    }
}
