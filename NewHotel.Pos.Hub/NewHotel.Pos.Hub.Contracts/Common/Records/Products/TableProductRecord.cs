﻿using System;
using System.Collections.Generic;
using NewHotel.Pos.Hub.Model;

namespace NewHotel.Pos.Hub.Contracts.Common.Records.Products
{
    /// <summary>
    /// table="TNHT_ARTB"
    /// Productos
    /// </summary>
    public class ProductTableRecord
    {
        /// <summary>
        /// column=ARTB_PK
        /// Llave Primaria
        /// </summary>
        public Guid Id { get; set; }

        public decimal? ProductStandardPrice { get; set; }
        public decimal? ProductHouseUsePrice { get; set; }
        public decimal? ProductMealPlanPrice { get; set; }
        public bool Selected { get; set; }

        /// <summary>
        /// column=ARTG_PK
        /// Table, Llave Primaria
        /// </summary>
        public Guid TableId { get; set; }

        /// <summary>
        /// column=ARTB_CODI
        /// Producto Asociado
        /// </summary>
        public Guid ProductId { get; set; }

        /// <summary>
        /// column=ARTB_ABRE
        /// Producto Asociado, Abreviatura
        /// </summary>
        public string ProductAbbreviation { get; set; }

        /// <summary>
        /// column=ARTB_DESC
        /// Producto Asociado, Descripcion
        /// </summary>
        public string ProductDescription { get; set; }

        /// <summary>
        /// column=ARTB_CANT
        /// Cantidad
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// column=TBSP_PK
        /// MenuSeparatorOrderId
        /// </summary>
        public Guid? MenuSeparatorId { get; set; }

        /// <summary>
        /// column=TBSP_ORDER
        /// MenuSeparatorOrder
        /// </summary>
        public short? MenuSeparatorOrder { get; set; }

        /// <summary>
        /// column=TBSP_MXSE
        /// MenuSeparatorSelection
        /// </summary>
        public int? MenuSeparatorSelection { get; set; }

        /// <summary>
        /// column=TBSP_DESC
        /// MenuSeparatorDescription
        /// </summary>
        public string MenuSeparatorDescription { get; set; }
        
        /// <summary>
        /// column=TBSP_DESC
        /// MenuSeparatorDescription
        /// </summary>
        public Guid? MenuSeparatorDescriptionId { get; set; }

        /// <summary>
        /// Preparaciones de la linea del table
        /// </summary>
        public List<PreparationRecord>? Preparations { get; set; }
        
        public List<AreasRecord> Areas { get; set; } = new List<AreasRecord>();
    }
}