﻿using System;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_PREP"
    /// Preparaciones y Condimentos
    /// </summary>
    public class PreparationRecord
    {
        /// <summary>
        /// column="PREP_PK"
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// PREP_DESC
        /// Descripcion
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Preparation Selected on Dialog
        /// </summary>
        public bool Selected { get; set; }
    }
}
