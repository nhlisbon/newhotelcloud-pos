﻿using System;
using System.ComponentModel;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_FAMI"
    /// Familia de Productos
    /// </summary>
    public class FamilyRecord : INotifyPropertyChanged
    {
        /// <summary>
        /// column="FAMI_PK"
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// FAMI_DESC
        /// Descripcion
        /// </summary>
        public string Description { get; set; }
        
        public event PropertyChangedEventHandler? PropertyChanged;

        private bool _isSelected;
        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                if (_isSelected == value) return;
                _isSelected = value;
                OnPropertyChanged(nameof(IsSelected));
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
