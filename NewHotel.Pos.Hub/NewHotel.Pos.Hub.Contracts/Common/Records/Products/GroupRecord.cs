﻿using System;
using System.ComponentModel;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_GRUP"
    /// Grupo de Productos
    /// </summary>
    public class GroupRecord : INotifyPropertyChanged
    {
        /// <summary>
        /// column="GRUP_PK"
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// GRUP_DESC
        /// Descripcion
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// GRUP_IMAG
        /// Imagen standard asociada al grupo
        /// </summary>
        public string Image { get; set; }
        
        public event PropertyChangedEventHandler? PropertyChanged;

        private bool _isSelected;
        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                if (_isSelected == value) return;
                _isSelected = value;
                OnPropertyChanged(nameof(IsSelected));
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}