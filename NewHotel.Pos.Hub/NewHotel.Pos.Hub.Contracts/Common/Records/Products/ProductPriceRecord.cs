﻿using System;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_ARTG"
    /// Productos
    /// </summary>
    public class ProductPriceRecord 
    {
        public Guid RateId { get; set; }
        public Guid ProductId { get; set; }
        public decimal? Price { get; set; }
    }
}