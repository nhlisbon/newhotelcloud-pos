﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_AREA"
    /// Area de Impresion
    /// </summary>
    public class AreasRecord
    {
        /// <summary>
        /// ARAR_PK
        /// Primary key
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// column="AREA_DESC"
        /// Descripción Area
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// AREA_NUCO
        /// No. Copias
        /// </summary>
        public short NumberCopies { get; set; }
        /// <summary>
        /// column="AREA_COLO"
        /// Color para representar el Area
        /// </summary>
        public ARGBColor AreaColor { get; set; }
        /// <summary>
        /// ARAR_ORDE
        /// Orden
        /// </summary>
        public short Order { get; set; }
        /// <summary>
        /// ARAR_ARMA
        /// Obligatorio enviar para el area
        /// </summary>
        public bool Mandatory { get; set; }
    }
}