﻿using System;
using System.Collections.Generic;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;
using NewHotel.Pos.Hub.Model;

namespace NewHotel.Pos.Hub.Contracts.Common.Records.Products
{
    /// <summary>
    /// table="TNHT_ARTG"
    /// Productos
    /// </summary>
    public class ProductRecord
    {
        #region Members

        public decimal? StandardPrice { get; set; }
        public decimal? HouseUsePrice { get; set; }
        public decimal? MealPlanPrice { get; set; }
        public decimal? AccompanyingPrice { get; set; }

        /// <summary>
        /// ARTG_PK
        /// Primary key
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// HOTE_PK
        /// Código Instalación
        /// </summary>
        public Guid HotelId { get; set; }
        /// <summary>
        /// ARTG_CODI
        /// Codigo visual del producto de venta
        /// </summary>
        public string ProductCode { get; set; }
        /// <summary>
        /// column="ARTG_ABRE"
        /// Traducción descripción producto
        /// </summary>
        public string Abbreviation { get; set; }
        /// <summary>
        /// column="ARTG_DESC"
        /// Traducción descripción producto
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// ARTG_STEP
        /// Step del producto
        /// </summary>
        public decimal Step { get; set; }
        /// <summary>
        /// GRUP_PK, GRUP_DESC
        /// Grupo al que pertenece el producto
        /// </summary>
        public Guid? GroupId { get; set; }
        public string GroupDescription { get; set; }
        /// <summary>
        /// FAMI_PK, FAMI_DESC
        /// Familia al que pertenece el producto
        /// </summary>
        public Guid? FamilyId { get; set; }
        public string FamilyDescription { get; set; }
        /// <summary>
        /// SFAM_PK, SFAM_DESC
        /// SubFamilia a la que pertenece el producto
        /// </summary>
        public Guid? SubFamilyId { get; set; }
        public string SubFamilyDescription { get; set; }
        /// <summary>
        /// SEPA_PK, SEPA_DESC
        /// Separador por defecto
        /// </summary>
        public Guid? SeparatorId { get; set; }
        public string SeparatorDescription { get; set; }
        /// <summary>
        /// ARTG_IMAG
        /// Imagen standard asociada al producto
        /// </summary>
        public string Image { get; set; }
        /// <summary>
        /// column="ARTG_COLO"
        /// Color de fondo del producto en caso de no usarse imagen
        /// </summary>
        public ARGBColor? Color { get; set; }
        /// <summary>
        /// column="ARTG_TIPR"
        /// Tipo precio a aplicar (Always Rate, Always Manual, Combined)
        /// </summary>
        public ProductPriceType PriceType { get; set; }
        /// <summary>
        /// column="FAVO_ORDE"
        /// Orden ocupado en la lista de favoritos
        /// </summary>
        public long? OrderInFavorites { get; set; }
        /// <summary>
        /// column="ARTG_PDES"
        /// Descuento automático en porciento sobre el precio del producto
        /// </summary>
        public decimal? DiscountPercent { get; set; }
        /// <summary>
        /// column="SERV_PK"
        /// Codigo Servicio PMS asociado al producto
        /// </summary>
        public Guid PmsServiceId { get; set; }
        /// <summary>
        /// column=ARTG_TABL
        /// Table Type (if applicable)
        /// </summary>
        public TableType? ProductTableType { get; set; }
        /// <summary>
        /// column=ARTG_BARC
        /// Código de barra
        /// </summary>
        public string BarCode { get; set; }
        /// <summary>
        /// column=ARTG_INAC
        /// Producto Inactivo
        /// </summary>
        public bool Inactive { get; set; }
        /// <summary>
        /// column=SERV_PROD
        /// Is Product
        /// </summary>
        public bool IsProduct { get; set; }
        /// <summary>
        /// column=DESC_ISENTO
        /// Is Product
        /// </summary>
        public string DescIsentoIva { get; set; }
        /// <summary>
        /// column=CODE_ISENTO
        /// Is Product
        /// </summary>
        public string CodeIsentoIva { get; set; }

        public List<AreasRecord> Areas { get; set; } = new();
        public List<PreparationRecord> Preparations { get; set; } = new();
        public List<ProductTableRecord> TableProducts { get; set; } = new();
        public List<TaxSchemaProductRecord> TaxSchemas { get; set; } = new();
        public List<string> Sliders { get; set; } = new();
        
        public List<Guid> AccompanyingProducts { get; set; } = [];

        public List<Guid> AccompanyingProductsSelected { get; set; } = [];

        public bool IsLaunchFromSpaService { get; set; }

        #endregion
    }

    public class TaxSchemaProductRecord
    {
        public bool Default { get; set; }
        public Guid TaxSchemaId { get; set; }
        public TaxRateProductRecord TaxRate1 => TaxRates.Count > 0 ? TaxRates[0] : null;
        public TaxRateProductRecord TaxRate2 => TaxRates.Count > 1 ? TaxRates[1] : null;
        public TaxRateProductRecord TaxRate3 => TaxRates.Count > 2 ? TaxRates[2] : null;
        public List<TaxRateProductRecord> TaxRates { get; set; } = new List<TaxRateProductRecord>();
    }

    public class TaxRateProductRecord
    {
        public Guid TaxRateId { get; set; }
        public decimal Percent { get; set; }
        public string TaxRateDesc { get; set; }
        public bool ApplyRetention { get; set; }
        public string TaxRateAuxCode { get; set; }
        public ApplyTax2? Mode2 { get; set; }
        public ApplyTax3? Mode3 { get; set; }
    }

    public static class ProductRecordExt
    {
        public static bool IsProductMenu (this ProductRecord record) => record is { ProductTableType: not null, TableProducts.Count: > 0 };
        public static bool HasAccompanyingProducts(this ProductRecord record) => record.AccompanyingProducts.Count > 0;
    }
}