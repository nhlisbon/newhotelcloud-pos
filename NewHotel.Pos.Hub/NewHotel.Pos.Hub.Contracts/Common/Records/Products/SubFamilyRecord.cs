﻿using System;
using System.ComponentModel;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_SFAM"
    /// SubFamilia de Productos
    /// </summary>
    public class SubFamilyRecord : INotifyPropertyChanged
    {
        /// <summary>
        /// column="SFAM_PK"
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// SFAM_DESC
        /// Descripcion
        /// </summary>
        public string Description { get; set; }
        
        public event PropertyChangedEventHandler? PropertyChanged;

        private bool _isSelected;
        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                if (_isSelected == value) return;
                _isSelected = value;
                OnPropertyChanged(nameof(IsSelected));
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
