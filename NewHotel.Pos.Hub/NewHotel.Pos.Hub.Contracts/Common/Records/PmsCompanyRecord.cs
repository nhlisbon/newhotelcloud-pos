﻿using System;
using NewHotel.Pos.Localization;

namespace NewHotel.Pos.Hub.Contracts.Common.Records;

public class PmsCompanyRecord : IAccountRecord
{
    public Guid Id { get; set; }
    public string Abbreviation { get; set; }
    public string CompanyName { get; set; }
    public string FiscalNumber { get; set; }
    public string CompanyType { get; set; }
    public string Country { get; set; }
    public string Category { get; set; }
    public string MarketSource { get; set; }
    public string MarketSegment { get; set; }
    public Guid? AccountId { get; set; }
    public bool? IsLock { get; set; }
    public decimal? Balance { get; set; }
    public string Currency { get; set; }
    public string Details => $"{"Balance".Translate()}: {Balance} {Currency}";
}