﻿using System;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_BCAT"
    /// Botones (SubCategorias) 
    /// </summary>
    public class SubCategoriesRecord
    {
        /// <summary>
        /// column="BCAT_PK"
        /// Primary Key
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// column="BCAT_DESC"
        /// Descripcion SubCategoria
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// column="BCAT_IMAG"
        /// Imagen SubCategoria
        /// </summary>
        public string ImagePath { get; set; }
        /// <summary>
        /// column="BCAT_ORDE"
        /// Orden SubCategorias
        /// </summary>
        public short Order { get; set; }
    }
}
