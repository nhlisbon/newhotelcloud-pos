﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Pos.Hub.Model
{
	[DataContract]
	public class CategoryOption
	{
		[DataMember]
		public CategoriesRecord Category;

		[DataMember]
		public IList<SubCategoryOption> SubCategories;
	}
}
