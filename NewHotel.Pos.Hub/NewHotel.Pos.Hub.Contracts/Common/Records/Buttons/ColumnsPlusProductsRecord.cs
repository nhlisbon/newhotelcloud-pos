﻿using System;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_BCAT, TNHT_BOTV"
    /// Columnas y Productos 
    /// </summary>
    public class ColumnsPlusProductsRecord
    {
        /// <summary>
        /// column="BCAT_PK"
        /// Codigo Columna
        /// </summary>
        public Guid ColumnId { get; set; }
        /// <summary>
        /// column="BCAT_DESC"
        /// Descripcion Columna
        /// </summary>
        public string ColumnDescription { get; set; }
        /// <summary>
        /// column="ARTG_PK"
        /// Codigo Producto
        /// </summary>
        public Guid ProductId { get; set; }
        /// <summary>
        /// column="ARTG_DESC"
        /// Descripcion Producto
        /// </summary>
        public string ProductDescription { get; set; }
        /// <summary>
        /// Category Orden
        /// </summary>
        public short CategoryOrden { get; set; }
        /// <summary>
        /// Product Order
        /// </summary>
        public short ProductOrder { get; set; }
    }
}
