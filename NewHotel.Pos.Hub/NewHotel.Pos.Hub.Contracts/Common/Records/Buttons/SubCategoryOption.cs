﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Pos.Hub.Model
{
	[DataContract]
	public class SubCategoryOption
	{
		[DataMember]
		public SubCategoriesRecord SubCategory;

		[DataMember]
		public IList<ColumnsPlusProductsRecord> ProductColumns;
	}
}
