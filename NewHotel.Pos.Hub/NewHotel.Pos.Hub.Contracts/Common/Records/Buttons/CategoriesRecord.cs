﻿using System;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_BCAT"
    /// Botones (Categorias) 
    /// </summary>
    public class CategoriesRecord
    {
        /// <summary>
        /// column="BCAT_PK"
        /// Primary Key
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// column="BCAT_DESC"
        /// Descripcion Categoria
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// column="BCAT_IMAG"
        /// Imagen Categoria
        /// </summary>
        public string ImagePath { get; set; }
        /// <summary>
        /// column="BCAT_ORDE"
        /// Orden Categorias
        /// </summary>
        public short Order { get; set; }
    }
}
