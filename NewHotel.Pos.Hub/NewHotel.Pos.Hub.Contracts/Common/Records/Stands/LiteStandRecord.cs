﻿using System;

namespace NewHotel.Pos.Hub.Model
{
    public class LiteStandRecord
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
    }
}
