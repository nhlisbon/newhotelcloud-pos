﻿using NewHotel.Contracts.Pos.Records;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Pos.Hub.Model
{
	[DataContract]
	public class SaloonEnvironment
	{
	    public SaloonEnvironment()
	    {
	        Tables = new List<TableEnvironment>();
	    }

		[DataMember]
		public SaloonRecord Saloon { get; set; }

	    [DataMember]
	    public IList<TableEnvironment> Tables { get; set; }
	}
}
