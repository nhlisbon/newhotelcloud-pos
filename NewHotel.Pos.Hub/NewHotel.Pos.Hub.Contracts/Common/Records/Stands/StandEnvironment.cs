﻿using NewHotel.Contracts.Pos.Records;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.Pos.Hub.Contracts;
using NewHotel.Pos.Hub.Contracts.Cashier.Records;
using NewHotel.Pos.Hub.Contracts.Common.Records.Products;

namespace NewHotel.Pos.Hub.Model
{
	[DataContract]
	public class StandEnvironment
	{
        public StandEnvironment()
        {
            Saloons = new List<SaloonEnvironment>();
            Categories = new List<CategoryOption>();
        }

        [DataMember]
        public bool IsHtml5 { get; set; }

        [DataMember]
		public IList<SaloonEnvironment> Saloons { get; set; }

        [DataMember]
		public IList<PaymentMethodsRecord> PaymentMethods { get; set; }

        [DataMember]
		public IList<DiscountTypesRecord> DiscountTypes { get; set; }

        [DataMember]
		public POSTicketPrintConfigurationRecord TicketPrintSettings { get; set; }

        [DataMember]
        public POSTicketPrintConfigurationRecord BallotPrintSettings { get; set; }

        [DataMember]
		public POSTicketPrintConfigurationRecord InvoicePrintSettings { get; set; }

        [DataMember]
        public POSTicketPrintConfigurationRecord ReceiptPrintSettings { get; set; }

        [DataMember]
		public IList<HouseUseRecord> HouseUses { get; set; }

        [DataMember]
		public IList<SeparatorsRecord> Separators { get; set; }

        [DataMember]
		public IList<GroupRecord> ProductGroups { get; set; }

        [DataMember]
		public IList<FamilyRecord> ProductFamilies { get; set; }

        [DataMember]
		public IList<SubFamilyRecord> ProductSubFamilies { get; set; }

        [DataMember]
		public IList<ProductRecord> Products { get; set; }

        [DataMember]
		public IList<AreasRecord> Areas { get; set; }

        [DataMember]
		public IList<CategoryOption> Categories { get; set; }

        [DataMember]
		public IList<TicketDutyRecord> TicketDuties { get; set; }

        [DataMember]
		public IList<CreditCardTypesRecord> CreditCardTypes { get; set; }

        [DataMember]
        public IList<HappyHourByStandRecord> HappyHours { get; set; }

        [DataMember]
        public IList<CancellationTypesRecord> CancellationReasons { get; set; }

        [DataMember]
        public IList<PreparationRecord> Preparations { get; set; }

	    [DataMember]
	    public IList<NationalityRecord> Nationalities{ get; set; }

        [DataMember]
        public string Currency { get; set; }

        [DataMember]
        public IList<CurrencyExchangeRecord> CurrencyExchanges { get; set; }

        [DataMember]
        public IList<CurrencyCashierRecord> Currencys { get; set; }

        [DataMember]
        public IList<TaxSchemaRecord> TaxSchemas { get; set; }

        [DataMember]
        public HotelContract Hotel { get; set; }
        
        [DataMember]
        public string AppVersion { get; set; }
    }
}
