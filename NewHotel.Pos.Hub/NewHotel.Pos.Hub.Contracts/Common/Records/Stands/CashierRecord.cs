﻿using System;
using System.Collections.Generic;
using NewHotel.Contracts;
using NewHotel.Contracts.Pos.Records;

namespace NewHotel.Pos.Hub.Contracts.Common.Records.Stands
{
    public class CashierRecord
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public bool TicketsByCashiers { get; set; }
        public bool ReportsByCashiers { get; set; }
    }
}