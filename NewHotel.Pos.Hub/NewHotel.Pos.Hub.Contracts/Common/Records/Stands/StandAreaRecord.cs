﻿using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Pos.Hub.Contracts.Common.Records.Stands
{
    public class StandAreaRecord
    {
        public Guid StandId { get; set; }
        public Guid AreaId { get; set; }
        public string StandDescription { get; set; }
        public string AreaDescription { get; set; }
        public ARGBColor? AreaColor { get; set; }
        public int NumberOfCopies { get; set; }
    }
}