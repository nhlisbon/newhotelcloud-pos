﻿using System;
using System.Collections.Generic;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_MESA"
    /// </summary>
    public class TableRecord
    {
        /// <summary>
        /// MESA_PK
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        ///Descripcion
        ///MESA_DESC
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        ///Cantidad maxima de pax permitidos en esa mesa
        ///MESA_PAXS
        /// </summary>
        public short MaxPaxs { get; set; }
        /// <summary>
        ///Tipo de Mesa (Standard,Bar,Reserved)
        ///MESA_TYPE
        /// </summary>
        public NewHotel.Contracts.EPosMesaType Type { get; set; }
        /// <summary>
        ///Row
        ///MESA_ROW
        /// </summary>
        public short? Row { get; set; }
        /// <summary>
        ///Column
        ///MESA_COLU
        /// </summary>
        public short? Column { get; set; }
        /// <summary>
        ///Width
        ///MESA_WIDT
        /// </summary>
        public short? Width { get; set; }
        /// <summary>
        ///Height
        ///MESA_HEIG
        /// </summary>
        public short? Height { get; set; }

        /// <summary>
        /// Reservaciones en el dia
        /// </summary>
        public IList<LiteTablesReservationRecord> Reservations { get; set; }
    }
}
