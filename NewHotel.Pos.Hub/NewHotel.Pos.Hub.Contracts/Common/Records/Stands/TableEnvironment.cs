﻿using NewHotel.Contracts.Pos.Records;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Model
{
	[DataContract]
	public class TableEnvironment
    {
        public TableEnvironment()
        {
            Reservations = new List<LiteTablesReservationRecord>();
        }

		[DataMember]
		public TableRecord Table { get; set; }

        [DataMember]
		public IList<LiteTablesReservationRecord> Reservations { get; set; }
	}
}
