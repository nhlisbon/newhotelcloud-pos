﻿using System;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// TNHT_SALO
    /// </summary>
    public class SaloonRecord
    {
        /// <summary>
        /// SALO_PK
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// column="LITE_PK"
        /// Traducción de la descripción
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// SALO_WIDT
        /// Width
        /// </summary>
        public short? SaloonWidth { get; set; }
        /// <summary>
        /// SALO_HEIG
        /// Height
        /// </summary>
        public short? SaloonHeight { get; set; }
        /// <summary>
        /// Imagen de las mesas libres
        /// </summary>
        public string ImageTableVacantState { get; set; }
        /// <summary>
        /// Imagen de las mesas ocupadas
        /// </summary>
        public string ImageTableBusyState { get; set; }

        /// <summary>
        /// Imagen de fondo del salon
        /// </summary>
        public string ImageSaloonFloorPlan { get; set; }
    }
}
