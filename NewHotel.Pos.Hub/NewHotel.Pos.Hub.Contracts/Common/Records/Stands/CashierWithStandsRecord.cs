﻿using System;
using System.Collections;
using System.Collections.Generic;
using NewHotel.Contracts.Pos.Records;

namespace NewHotel.Pos.Hub.Contracts.Common.Records.Stands;

public class CashierWithStandsRecord
{
    public CashierRecord Cashier { get; set; }
    public List<POSStandRecord> Stands { get; set; }
}