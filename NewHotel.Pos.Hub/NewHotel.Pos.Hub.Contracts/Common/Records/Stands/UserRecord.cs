﻿using System;
using NewHotel.Contracts;
using System.Collections.Generic;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_UTIL"
    /// </summary>
    public class UserRecord
    {
        public Guid Id { get; set; }
        /// <summary>
        /// column="UTIL_DESC"
        /// User Description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// column="UTIL_LOGIN"
        /// User login
        /// </summary>
        public string UserLogin { get; set; }
        /// <summary>
        /// column="UTIL_PASS"
        /// User Password
        /// </summary>
        public string UserPassword { get; set; }
        /// <summary>
        /// column="UTIL_CODE"
        /// User Code
        /// </summary>
        public string UserCode { get; set; }
        /// <summary>
        /// column="UTIL_INTE"
        /// User Internal
        /// </summary>
        public bool UserInternal { get; set; }
        /// <summary>
        /// Permissions Per User
        /// </summary>
        public Dictionary<Permissions, Tuple<SecurityCode, string>> Permissions { get; set; }
        /// <summary>
        /// Translations of Permissions
        /// </summary>
        //public Dictionary<NewHotel.Contracts.Permissions, string> PermissionsTranslation { get; set; }
    }
}
