﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Model
{
    public class ProductInStandRecord
    {
        public Guid Id { get; set; }
        public string Product { get; set; }
        public string ProductCode { get; set; }
    }
}
