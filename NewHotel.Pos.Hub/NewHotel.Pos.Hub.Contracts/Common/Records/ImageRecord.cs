﻿using System.Runtime.Serialization;

namespace NewHotel.Pos.Hub.Model
{
    [DataContract]
    public class ImageRecord
    {
        [DataMember(Order = 1)]
        public string Id { get; set; }
        [DataMember(Order = 2)]
        public long Timestamp { get; set; }
        [DataMember(Order = 3)]
        public string Image { get; set; }
    }
}
