﻿
using System;
using NewHotel.Contracts;
using NewHotel.Pos.Localization;

namespace NewHotel.Pos.Hub.Contracts.Common.Records
{
    public class SpaReservationRecord : IAccountRecord
    {
        public Guid Id { get; set; }
        public Guid? AccountId { get; set; }
        public string Guest { get; set; }
        public string Room { get; set; }
        public string ReservationNumber { get; set; }
        public string Details => $"{Guest}";
    }
}