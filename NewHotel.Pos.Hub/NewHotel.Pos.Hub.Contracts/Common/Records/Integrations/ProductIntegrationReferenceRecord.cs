﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Contracts.Common.Records.Integrations
{
    public class ProductIntegrationReferenceRecord
    {
        public Guid Id { get; set; }

        /// <summary>
        /// The id of the product from the table "TNHT_ARTG", column "ARTG_PK"
        /// </summary>
        public Guid ProductId { get; set; }

        /// <summary>
        /// The id of the integration from the table "TNHT_INTE", column "INTE_PK"
        /// </summary>
        public Guid IntegrationId { get; set; }
        
        /// <summary>
        /// This is the external code that represents the id of the product in the other system.
        /// It's always required
        /// </summary>
        public string ProductCode { get; set; }

        /// <summary>
        /// This code is optional, with the exceptions of a few systems.
        /// Mxm where it's required and it's used to hold the Destination code (property name is: Destinacao)
        /// </summary>
        public string OptionalCodeOne { get; set; }

        /// <summary>
        /// This code is optional, with the exceptions of a few systems.
        /// Mxm where it's required and it's used to hold the Cost Center code (property name is: CentrodeCusto)
        /// </summary>
        public string OptionalCodeTwo { get; set; }
    }
}
