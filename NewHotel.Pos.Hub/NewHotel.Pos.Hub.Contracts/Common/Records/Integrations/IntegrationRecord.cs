﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Contracts.Common.Records.Integrations
{
    public class IntegrationRecord
    {
        public Guid Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Config { get; set; }
        public bool Active { get; set; }
        public DateTime LastModification { get; set; }
    }
}
