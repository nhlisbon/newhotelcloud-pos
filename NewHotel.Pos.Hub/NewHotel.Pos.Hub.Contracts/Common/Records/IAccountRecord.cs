﻿namespace NewHotel.Pos.Hub.Contracts.Common.Records;

public interface IAccountRecord
{
    public string Details { get; }
}