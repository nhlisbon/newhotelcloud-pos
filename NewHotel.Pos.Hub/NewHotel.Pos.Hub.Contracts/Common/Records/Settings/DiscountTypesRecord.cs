﻿using System;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_TIDE"
    /// Tipos Descuentos
    /// </summary>
    public class DiscountTypesRecord
    {
        /// <summary>
        /// TIDE_PK
        /// Primary key
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// column="TIDE_DESC"
        /// Descripción
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// column="TIDE_FIXE"
        /// flag: descuento fijo = 1, descuento en rango = 0 
        /// </summary>
        public bool FixedDiscount { get; set; }
        public decimal? FixedPercent
        {
            get
            {
                if (FixedDiscount)
                    return MaxPercent;
                else return null;
            }

        }
        /// <summary>
        /// column="TIDE_VMIN"
        /// valor minimo del rango de impuesto, en caso de impuesto fijo contiene el valor de impuesto
        /// </summary>
        public decimal MinPercent { get; set; }
        /// <summary>
        /// column="TIDE_VMAX"
        /// valor maximo del rango de impuesto, en caso de impuesto fijo contiene el valor de impuesto
        /// </summary>
        public decimal MaxPercent { get; set; }
    }
}
