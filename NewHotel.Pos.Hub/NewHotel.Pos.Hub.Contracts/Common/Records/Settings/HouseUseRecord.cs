﻿using System;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_COIN, TNHT_PVCI"
    /// Consumos Internos
    /// </summary>
    public class HouseUseRecord
    {
        /// <summary>
        /// column="COIN_PK"
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// COIN_DESC
        /// Descripcion
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// PVCI_LIMI
        /// Limite mensual de consumo interno
        /// </summary>
        public decimal? MonthlyLimit { get; set; }
        /// <summary>
        /// PVCI_ILIM
        /// Pago ilimitado (1.Si, 2.No)
        /// </summary>
        public bool Unlimited { get; set; }
        /// <summary>
        /// PVCI_PORL
        /// Porciento en caso de no ser ilimitado
        /// </summary>
        public decimal? LimitPercent { get; set; }
        /// <summary>
        /// COIN_INVI
        /// Es Invitacion
        /// </summary>
        public bool IsInvitation { get; set; }
    }
}
