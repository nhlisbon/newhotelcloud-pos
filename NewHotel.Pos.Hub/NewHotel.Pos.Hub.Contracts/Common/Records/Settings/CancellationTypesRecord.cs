﻿using System;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_MTCO"
    /// Motivos Anulación
    /// </summary>
    public class CancellationTypesRecord
    {
        /// <summary>
        /// MTCO_PK
        /// Primary key
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// column="MTCO_DESC"
        /// Descripción
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// column="MTCO_TICA"
        /// Tipo Cancelación (Facturas, Movimientos, Reservas)
        /// </summary>
        public CancellationType Type { get; set; }
    }
}
