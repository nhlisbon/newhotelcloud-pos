﻿using NewHotel.Contracts.Pos.Records;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.Pos.Hub.Contracts.Common.Records.Stands;

namespace NewHotel.Pos.Hub.Model
{
	[DataContract]
	public class HotelEnvironment
	{
		[DataMember]
		public HotelRecord Hotel { get; set; }

		[DataMember]
		public IList<CashierRecord> Cashiers { get; set; }

        [DataMember]
		public IList<POSStandRecord> Stands { get; set; }
    }
}
