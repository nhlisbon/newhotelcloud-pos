﻿using NewHotel.Contracts;
using System;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_HAIP"
    /// Happy Hour by Stand
    /// </summary>
    public class HappyHourByStandRecord
    {
        /// <summary>
        /// HAIP_PK
        /// Primary key
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// column="HOUR_PK"
        /// ID del horario
        /// </summary>
        public Guid PeriodId { get; set; }
        /// <summary>
        /// column="HOUR_DESC"
        /// Description del Happy Hour
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// column="HOUR_HORI"
        /// Hora inicial
        /// </summary>
        public DateTime InitialTime { get; set; }
        /// <summary>
        /// column="HOUR_HORF"
        /// Hora final
        /// </summary>
        public DateTime FinalTime { get; set; }
        /// <summary>
        /// column="HOUR_DIAS"
        /// Days of the Week
        /// </summary>
        public string DaysOfWeek { get; set; }
        /// <summary>
        /// column="TPRG_PK"
        /// Tarifa de precios
        /// </summary>
        public Guid PriceRateId { get; set; }
        /// <summary>
        /// column="HOUR_CANT"
        /// Cantidad
        /// </summary>
        public short Quantity { get; set; }
        /// <summary>
        /// column="HOUR_INAC"
        /// Inactivo o no
        /// </summary>
        public bool Inactive { get; set; }
    }
}
