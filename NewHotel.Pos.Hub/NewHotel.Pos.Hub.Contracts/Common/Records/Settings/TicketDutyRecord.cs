﻿using System;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_DUTY"
    /// Servicios en los Tickets
    /// </summary>
    public class TicketDutyRecord
    {
        /// <summary>
        /// DUTY_PK
        /// Primary key
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// column="DUTY_DESC"
        /// Descripción
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// column="DUTY_HORI"
        /// Hora Inicio
        /// </summary>
        public DateTime InitialTime { get; set; }
        /// <summary>
        /// column="DUTY_HORF"
        /// Hora Fin
        /// </summary>
        public DateTime FinalTime { get; set; }
        /// <summary>
        /// column="DUTY_ETME"
        /// Tiempo estimado de demora de los comensales durante este turno, dado en minutos
        /// </summary>
        public short TimeDelay { get; set; }
    }
}
