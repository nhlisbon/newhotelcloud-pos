﻿using System;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_HOTE"
    /// Hoteles
    /// </summary>
    public class HotelRecord
    {
        /// <summary>
        /// HOTE_PK
        /// Primary key
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// column="HOTE_ABRE"
        /// Abbreviation
        /// </summary>
        public string Abbreviation { get; set; }
        /// <summary>
        /// column="HOTE_DESC"
        /// Description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// column="HOTE_CAUX"
        /// Free Code
        /// </summary>
        public string FreeCode { get; set; }
        /// <summary>
        /// column="NACI_PK"
        /// Country ID
        /// </summary>
        public string CountryID { get; set; }
        /// <summary>
        /// column="HOTE_KALI"
        /// Intervalo de tiempo en segundos para el chequeo de conexión (keepalive). Por defecto a 30s.
        /// </summary>
        public short KeppAliveTime { get; set; }
        /// <summary>
        /// column="HOTE_EXPC"
        /// Contador límite de fallos para considerar conexión expirada. Por defecto a 10.
        /// </summary>
        public short KeppAliveCount { get; set; }
        /// <summary>
        /// column="HOTE_ANDR"
        /// Permite Utilizacion de Android 
        /// </summary>
        public bool AndroidEnabled { get; set; }
        /// <summary>
        /// column="HOTE_SYNC"
        /// Timestamp de la ultima sincronizacion
        /// </summary>
        public DateTime? Synced { get; set; }
        /// <summary>
        /// column="EMAIL_ADDR"
        /// Email
        /// </summary>
        public string EmailAddress { get; set; }
    }
}
