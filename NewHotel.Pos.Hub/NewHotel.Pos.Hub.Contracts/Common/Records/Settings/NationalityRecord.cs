﻿namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_NACI"
    /// Nacionalidades
    /// </summary>
    public class NationalityRecord
    {
        /// <summary>
        /// NACI_PK
        /// Código País - ISO 3166 (alpha 2)
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// LITE_DESC
        /// Traducción Nombre Pais
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// REMU_PK
        /// Código región del mundo a la que pertenece el país, ref. tnht_remu(remu_pk)
        /// </summary>
        public int RegionCode { get; set; }

        /// <summary>
        /// LICL_PK
        /// Código idioma oficial hablado en el país, ref. tnht_licl(licl_pk)
        /// </summary>
        public int LanguageCode { get; set; }

        /// <summary>
        /// NACI_CACO
        /// Código internacional telefónico (Calling Code)
        /// </summary>
        public string CallingCode { get; set; }

        /// <summary>
        /// ISO_3166
        /// ISO 3166 (alpha 3)
        /// </summary>
        public string ISO3166 { get; set; }

        /// <summary>
        /// PT_SEF
        /// Código oficial para SEF-Portugal
        /// </summary>
        public string SEFCode { get; set; }

        /// <summary>
        /// NACI_CAUX
        /// Código Auxiliar de País
        /// </summary>
        public string CountryAuxiliaryCode { get; set; }
    }
}
