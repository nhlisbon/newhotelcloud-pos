﻿using System;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_SEPA"
    /// Separadores
    /// </summary>
    public class SeparatorsRecord
    {
        /// <summary>
        /// SEPA_PK
        /// Primary key
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// column="SEPA_DESC"
        /// Descripción
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// column="SEPA_ORDE"
        /// Orden de impresion
        /// </summary>
        public short Order { get; set; }
        /// <summary>
        /// column="SEPA_MAXP"
        /// Cantidad máxima de platos/paxs de la ronda
        /// </summary>
        public short MaxCoversAllowed { get; set; }
        /// <summary>
        /// column="SEPA_ACUM"
        /// Flag: Ronda acumulativa (1. si, 0. no)
        /// </summary>
        public bool CumulativeRound { get; set; }
        /// <summary>
        /// column="SEPA_TICK"
        /// Flag: Se muestra en el ticket (1. se, 0. no)
        /// </summary>
        public bool ShowedInTickets { get; set; }
    }
}
