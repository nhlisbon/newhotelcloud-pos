﻿using System;
using NewHotel.Pos.Hub.Model.Validator;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_CLIE"
    /// Clients
    /// </summary>
    public class ClientRecord : IClient
    {
        /// <summary>
        /// CLIE_PK
        /// Primary key
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// column="CLIE_NOME"
        /// Name / Description
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// column="CLIE_CAUX"
        /// Auxiliar Code
        /// </summary>
        public string AuxiliarCode { get; set; }
        /// <summary>
        /// column="FISC_NUMB"
        /// Fiscal Number
        /// </summary>
        public string FiscalNumber { get; set; }
        /// <summary>
        /// column="FISC_ADDR1"
        /// Address
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// column="FISC_ADDR2"
        /// Address
        /// </summary>
        public string AddressComplement { get; set; }
        /// <summary>
        /// column="NACI_PK"
        /// Country
        /// </summary>
        public string Country { get; set; }
        /// <summary>
        /// column="NACI_DESC"
        /// Country Name
        /// </summary>
        public string CountryName { get; set; }
        /// <summary>
        /// column="ENTI_FINO"
        /// Fiscal Note Preferred
        /// </summary>
        public bool FiscalNotePreferred { get; set; }
        /// <summary>
        /// column="HOME_PHONE"
        /// Phone
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// column="FISC_DOOR"
        /// Door Number
        /// </summary>
        public string DoorNumber { get; set; }
        /// <summary>
        /// column="FISC_LOCA"
        /// Door Number
        /// </summary>
        public string Location { get; set; }
        /// <summary>
        /// column="FISC_COPO"
        /// Postal Code
        /// </summary>
        public string PostalCode { get; set; }
        /// <summary>
        /// column="REGI_FISC"
        /// Fiscal Register (IE)
        /// </summary>
        public string FiscalRegister { get; set; }
        /// <summary>
        /// column="CLIE_TYPE" 11 Client, 211 Entity
        /// </summary>
        public bool IsClient { get; set; }
        /// <summary>
        /// column="DIST_CAUX"
        /// </summary>
        public string DistrictCode { get; set; }
        /// <summary>
        /// column="DIST_DESC"
        /// </summary>
        public string District { get; set; }
        /// <summary>
        /// column="COMU_CAUX"
        /// </summary>
        public string StateCode { get; set; }
        /// <summary>
        /// column="EMAIL_ADDR"
        /// </summary>
        public string EmailAddress { get; set; }
        /// <summary>
        /// column="CLIE_PASS"
        /// </summary>
        public string Passport { get; set; }
        /// <summary>
        /// column="CLIE_IDEN"
        /// </summary>
        public string Identity { get; set; }
        /// <summary>
        /// column="CLIE_RESD"
        /// </summary>
        public string Residence { get; set; }
        /// <summary>
        /// column="CLIE_DRIV"
        /// </summary>
        public string DriverLicense { get; set; }
        /// <summary>
        /// column="CLIE_DEAU"
        /// </summary>
        public bool ApplyAutomaticProductDiscount { get; set; }

        /// <summary>
        /// column="FISC_COTR"
        /// </summary>
        public string FiscalRegimeCode { get; set; }
        
        /// <summary>
        /// column="FISC_CORF"
        /// </summary>
        public string FiscalResponsibilityCode { get; set; }
    }
}