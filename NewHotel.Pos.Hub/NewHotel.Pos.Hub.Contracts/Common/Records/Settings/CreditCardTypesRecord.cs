﻿using System;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_CACR"
    /// Tipos tarjetas crédito
    /// </summary>
    public class CreditCardTypesRecord
    {
        /// <summary>
        /// CACR_PK
        /// Primary key
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// column="CACR_DESC"
        /// Descripción
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// column="CACR_CACR"
        /// Is Credit Card
        /// </summary>
        public bool CreditCard { get; set; }
        /// <summary>
        /// column="CACR_CADE"
        /// Is Debit Card
        /// </summary>
        public bool DebitCard { get; set; }
    }
}
