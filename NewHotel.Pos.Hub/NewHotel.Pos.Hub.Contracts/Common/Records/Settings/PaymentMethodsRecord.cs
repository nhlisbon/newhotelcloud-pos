﻿using System;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// table="TNHT_FORE"
    /// Tipos Pagos
    /// </summary>
    public class PaymentMethodsRecord
    {
        /// <summary>
        /// FORE_PK
        /// Primary key
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// column="FORE_ABRE"
        /// Abreviatura
        /// </summary>
        public string Abbreviation { get; set; }
        /// <summary>
        /// column="FORE_DESC"
        /// Descripción
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// column="UNMO_PK"
        /// Moneda
        /// </summary>
        public string CurrencyId { get; set; }
        /// <summary>
        /// column="FORE_CASH"
        /// flag: indica si se considera venta en efectivo
        /// </summary>
        public bool ConsideredCash { get; set; }
        /// <summary>
        /// column="FORE_CACR"
        /// Flag: Tipo Cobro Tarjeta Crédito (1.si, 0.no) 
        /// </summary>
        public bool ConsideredCreditCard { get; set; }
        /// <summary>
        /// column="REPO_CASH"
        /// flag: Incluir en reporte de caja
        /// </summary>
        public bool ShowedInCashierReport { get; set; }
        /// <summary>
        /// column="FORE_PATH"
        /// Camino relativo a la imagen
        /// </summary>
        public string ImagePath { get; set; }
        /// <summary>
        /// column="FORE_ORDE"
        /// Orden en que se muestran
        /// </summary>
        public short Order { get; set; }
        /// <summary>
        /// column="FORE_CRED"
        /// Orden en que se muestran
        /// </summary>
        public bool AllowCreditCard { get; set; }
        /// <summary>
        /// column="FORE_DEBI"
        /// Orden en que se muestran
        /// </summary>
        public bool AllowDebitCard { get; set; }
        /// <summary>
        /// column="FORE_PCDS"
        /// Pedir firma digital para esta forma de pago
        /// </summary>
        public bool PaymentChargeDigitalSignature { get; set; }
        /// <summary>
        /// column="FORE_CAUX"
        /// Codigo auxiliar
        /// </summary>
        public string AuxiliarCode { get; set; }
        /// <summary>
        /// column="FORE_INVC"
        /// Allows the payment to be payed has a invoice (0=no, 1=yes)
        /// </summary>
        public bool AllowInvoiceClick { get; set; }
        /// <summary>
        /// column="FORE_TICK"
        /// Allows the payment to be payed has a ticket (0=no, 1=yes)
        /// </summary>
        public bool AllowTicketClick { get; set; }

		/// <summary>
		/// column=PAYS_ORIGIN"
		/// PaySystem origin id
		/// </summary>
		public Guid? PaySystemOriginId { get; set; }

	}
}