﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.Pos.Localization;

namespace NewHotel.Pos.Hub.Contracts.Common.Records;

public class PmsControlAccountRecord : IAccountRecord
{
    public Guid? Id { get; set; }
    public string AccountName { get; set; }
    public string AccountEmail { get; set; }
    public string FiscalNumber { get; set; }
    public string Remarks { get; set; }
    public Guid? AccountId { get; set; }
    public bool? IsLock { get; set; }
    public string FreeCode { get; set; }
    public decimal? Balance { get; set; }
    public string Currency { get; set; }
    public string Address { get; set; }
    public bool IsBalanceChecked { get; set; }

    public List<ControlAccountDeposit> AccountDepositList { get; set; } 

    public string Details => $"{"Balance".Translate()}: {Balance} {Currency}";
}

public class ControlAccountDeposit
{
   
    public decimal Balance { get; set; }
 
    public Guid DepositId { get; set; }


    public string Description { get; set;}
 
    public DateTime WorkDate { get; set; }
 
    public DateTime ValueDate { get; set;}
    public DateTime RegisterDate { get; set; }

    public bool IsSelected { get; set; } = false;

}