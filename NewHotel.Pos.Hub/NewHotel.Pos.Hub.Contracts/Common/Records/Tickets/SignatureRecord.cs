using System;
using NewHotel.DataAnnotations;

namespace NewHotel.Pos.Hub.Contracts.Common.Records.Tickets;

/// <summary>
/// TNHT_SIRE
/// The signature records for the tickets
/// </summary>
public class SignatureRecord
{
    /// <summary>
    /// VEND_PK
    /// The foreign key to the ticket in the table TNHT_VEND
    /// </summary>
    public Guid TicketId { get; set; }
    
    /// <summary>
    /// SIRE_DISI
    /// The signature in a blob/image format
    /// </summary>
    public Blob? Signature { get; set; }

    /// <summary>
    /// SIRE_PROC
    /// If the signature is signed or not
    /// </summary>
    public bool IsSigned { get; set; } = false;
}