﻿using NewHotel.Contracts;
using System;

namespace NewHotel.Pos.Hub.Model
{
    /// <summary>
    /// TNHT_SEDO
    /// </summary>
    public class DocumentSerieRecord
    {
        /// <summary>
        /// SEDO_PK
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// TIDO_PK
        /// </summary>
        public POSDocumentType DocType { get; set; }
        /// <summary>
        /// SEDO_PTEX
        /// SerieTex
        /// </summary>
        public string SerieTex { get; set; }
        /// <summary>
        /// SEDO_PNUM
        /// SerieNum
        /// </summary>
        public long SerieNum { get; set; }
        /// <summary>
        /// SEDO_ORDE
        /// SerieOrder
        /// </summary>
        public long SerieOrder { get; set; }
        /// <summary>
        /// IPOS_PK
        /// Stand
        /// </summary>
        public Guid StandId { get; set; }
        /// <summary>
        /// CAJA_PK
        /// Cashier
        /// </summary>
        public Guid CajaId { get; set; }
        /// <summary>
        /// SEDO_NUFA
        /// proximo numero documento
        /// </summary>
        public long NextNumber { get; set; }
        /// <summary>
        /// SEDO_NUFI
        /// numero final de la serie
        /// </summary>
        public long? EndNumber { get; set; }
        /// <summary>
        /// SEDO_DAFI
        /// fecha final de la serie
        /// </summary>
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// SEDO_SEAC
        /// status (activa, inactiva, futura) de la serie
        /// </summary>
        public SerieStatus SerieStatus { get; set; }
        /// <summary>
        /// SEDO_SAFT
        /// Firma Digital SAFT-PT
        /// </summary>
        public string Signature { get; set; }
    }
}
