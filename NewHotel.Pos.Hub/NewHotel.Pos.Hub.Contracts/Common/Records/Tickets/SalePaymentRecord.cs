﻿using System;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;
using NewHotel.Pos.Core.Ext;

namespace NewHotel.Pos.Hub.Contracts.Common.Records.Tickets
{
    [MappingQuery("PAVE_PK")]
    public class SalePaymentRecord : BaseRecord
    {
        #region Variables

        private bool? _multCurrencyExchange;
        private decimal? _currencyExchange;
        private decimal _value;
        private string _currencyIdPayment;

        #endregion
        #region Properties

        #region Mapping

        [MappingColumn("VEND_PK")]
        public Guid TicketId { get; set; }

        [MappingColumn("VEND_DATF")]
        public DateTime CloseDate { get; set; }

        [MappingColumn("TURN_CODI")]
        public short Shift { get; set; }

        [MappingColumn("UTIL_DESC")]
        public string Username { get; set; }

        [MappingColumn("TIRE_PK")]
        public ReceivableType ReceivableType { get; set; }

        [MappingColumn("TACR_DESC")]
        public string CreditCardDescription { get; set; }

        [MappingColumn("PAYMENT_TYPE_NAME")]
        public string PaymentTypeName { get; set; }

        [MappingColumn("PAYMENT_DESC")]
        public string PaymentDescription { get; set; }

        [MappingColumn("PAYMENT_DESC_DETAILED")]
        public string PaymentDescriptionDetailed { get; set; }

        [MappingColumn("VEND_ANUL")]
        public bool CancelledTicket { get; set; }

        [MappingColumn("VEND_CODI", Nullable = true)]
        public long? Number { get; set; }

        [MappingColumn("VEND_SERI", Nullable = true)]
        public string Serie { get; set; }

        [MappingColumn("FACT_CODI")]
        public long? InvoiceNumber { get; set; }

        [MappingColumn("FACT_SERI")]
        public string InvoiceSerie { get; set; }

        [MappingColumn("NCRE_CODI")]
        public long? CreditNoteNumber { get; set; }

        [MappingColumn("NCRE_SERI")]
        public string CreditNoteSerie { get; set; }

        [MappingColumn("PAVE_VALO")]
        public decimal Value
        {
            get => _value;
            set
            {
                if (Set(ref _value, value, nameof(Value)))
                    UpdateValueExchange();
            }
        }

        [MappingColumn("VEND_UNMO")]
        public string CurrencyIdTicket { get; set; }

        [MappingColumn("PAVE_UNMO")]
        public string CurrencyIdPayment
        {
            get => !string.IsNullOrEmpty(_currencyIdPayment) ? _currencyIdPayment : CurrencyIdTicket;
            set => _currencyIdPayment = value;
        }

        [MappingColumn("CAMB_ONLINE")]
        public decimal? CurrencyExchange
        {
            get => _currencyExchange;
            set
            {
                if (Set(ref _currencyExchange, value, nameof(CurrencyExchange)))
                    UpdateValueExchange();
            }
        }

        [MappingColumn("CAMB_MULT")]
        public bool? MultCurrencyExchange
        {
            get => _multCurrencyExchange;
            set
            {
                if (Set(ref _multCurrencyExchange, value, nameof(MultCurrencyExchange)))
                    UpdateValueExchange();
            }
        }

        public long Count { get; set; } = 1;
        public decimal ValueExchange { get; set; }

        public string TicketDescription => !string.IsNullOrEmpty(Serie)
            ? $"{Number}/{Serie}" : string.Empty;

        public string InvoiceDescription => !string.IsNullOrEmpty(InvoiceSerie) && InvoiceNumber.HasValue
            ? $"{InvoiceNumber.Value}/{InvoiceSerie}" : string.Empty;

        public string CreditNoteDescription => !string.IsNullOrEmpty(CreditNoteSerie) && CreditNoteNumber.HasValue
            ? $"{CreditNoteNumber.Value}/{CreditNoteSerie}" : string.Empty;

        #endregion
        #endregion
        #region Methods

        private void UpdateValueExchange()
        {
            decimal currencyExchange = CurrencyExchange ?? 1;
            if (MultCurrencyExchange ?? true) ValueExchange = Value * currencyExchange;
            else ValueExchange = currencyExchange > 0 ? Value / currencyExchange : 0;
            ValueExchange = ValueExchange.Round2();
        }

        #endregion
    }
}