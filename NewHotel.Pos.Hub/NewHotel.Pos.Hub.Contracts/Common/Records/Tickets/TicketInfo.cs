﻿using NewHotel.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Model
{
	public class TicketInfo
	{
		public Guid Id { get; set; }
		public Guid? MesaId { get; set; }
		public string Description { get; set; }
		public string Name { get; set; }
		public bool IsAcceptedTransfer { get; set; }
		public bool DigitalMenu { get; set; }
		public long? Number { get; set; }
		public string Serie { get; set; }
		public bool Opened { get; set; }
		public decimal Total { get; set; }
		public Guid Stand { get; set; }
		public DateTime? ClosedDate { get; set; }
		public Guid Caja { get; set; }
		public long Shift { get; set; }
		public bool IsPersisted { get; set; }
		public string MesaDescription { get; set; }
		public Guid? SaloonId { get; set; }
	}

	public class TicketIdentification
	{
		public Guid Id { get; set; }
		public DateTime LastModified { get; set; }
	}

	public static class TicketInfoSupport
	{
		public static TicketIdentification AsTicketIdentification(this POSTicketContract ticket)
		{
			return new TicketIdentification
			{
				Id = (Guid)ticket.Id,
				LastModified = ticket.LastModified,
			};
		}
	}


	public class TicketProduct
    {
		public Guid Id { get; set; }
		public string Code { get; set; }
		public string Name { get; set; }
		public Decimal? Quantity { get; set; }
		public Decimal? Value { get; set; }
	}

	public class TicketWithProducts : TicketInfo
	{
        public TicketProduct[] Products { get; set; }
		
    }
}
