﻿using System;

namespace NewHotel.Pos.Hub.Model
{
    public class ClosedTicketRecord : NewHotel.Contracts.Pos.Records.ClosedTicketRecord
    {
        public ClosedTicketRecord()
        {
        }

        public ClosedTicketRecord(NewHotel.Contracts.Pos.Records.ClosedTicketRecord record)
        {
            Id = record.Id;
            Cancelled = record.Cancelled;
            CashierDescription = record.CashierDescription;

            OpeningNumber = record.OpeningNumber;
            Number = record.Number;
            BallotNumber = record.BallotNumber;
            InvoiceNumber = record.InvoiceNumber;
            CreditNoteNumber = record.CreditNoteNumber;
            RoomNumber = record.RoomNumber;
            AccountId = record.AccountId;
            AccountDescription = record.AccountDescription;
            StandDescription = record.StandDescription;
            CloseDate = record.CloseDate;
            CloseTime = record.CloseTime;
            Total = record.Total;
            CurrencyId = record.CurrencyId;
            DocumentType = record.DocumentType;

            FpCreditNoteNumber = record.FpCreditNoteNumber;
            FpInvoiceNumber = record.FpInvoiceNumber;
            FpSerialCreditNote = record.FpSerialCreditNote;
            FpSerialInvoice = record.FpSerialInvoice;
        }

        public Guid IdAsGuid()
        {
            return Guid.Parse(Id.ToString());
        }

        public string CloseDateTime { get { return $"{CloseDate.ToString("dd/MM/yyyy")} {CloseTime.ToString("HH:mm")}"; } }

        public string Details => AccountDescription != null ? string.Join(" ", AccountDescription.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)) : null;

        public bool LocalCreditNote { get; set; }

        public bool LocalTicket { get; set; }
    }
}