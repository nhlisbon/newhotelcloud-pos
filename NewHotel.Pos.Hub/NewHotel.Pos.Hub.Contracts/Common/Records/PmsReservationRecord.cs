﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Contracts.Common.Records
{
    public class PmsReservationRecord
    {
        public DateTime? Arrival { get; set; }
        public DateTime? Departure { get; set; }
        public int? Nights { get; set; }
        public string ReservationNumber { get; set; }
        public string Guests { get; set; }
        public int? State { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string Room { get; set; }
        public Guid? AccountId { get; set; }
        public bool? IsLock { get; set; }
        public decimal? Balance { get; set; }
        public string Currency { get; set; }
        public string Pension { get; set; }
        public string Paxs { get; set; }
        public bool AllowCreditPos { get; set; }
        public string CardNumbers { get; set; }
        public string Company { get; set; }
    }
}
