﻿using System;

namespace NewHotel.Pos.Hub.Contracts.Common.Contracts
{
    public class SpaSearchReservationFilterModel
    {
        public new Guid? HotelId { get; set; }
        public string  ReservationNumber { get; set; }
        public string Room { get; set; }
        public string Guest { get; set; }
        public string Code { get; set; }
    }
}
