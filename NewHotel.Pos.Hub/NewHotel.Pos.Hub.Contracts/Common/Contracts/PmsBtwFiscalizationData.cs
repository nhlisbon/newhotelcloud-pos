﻿using System;

namespace NewHotel.Pos.Hub.Contracts.Common.Contracts
{
    public class PmsBtwFiscalizationData
    {
        public string DocumentType { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string Country { get; set; }

        public string StateCode { get; set; }

        public string StateName { get; set; }
        
        public string CityCode { get; set; }

        public string CityName { get; set; }

        public string RegimeTypeCode { get; set; }

        public string ResponsabilityCode { get; set;}
    }
}
