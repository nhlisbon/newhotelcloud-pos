﻿using System;

namespace NewHotel.Pos.Hub.Contracts.Common.Contracts;

public class PmsCompanyFilterModel
{
    public new Guid? HotelId { get; set; }
    public string Abbreviation { get; set; }
    public string Name { get; set; }
    public string FiscalNumber { get; set; }
}