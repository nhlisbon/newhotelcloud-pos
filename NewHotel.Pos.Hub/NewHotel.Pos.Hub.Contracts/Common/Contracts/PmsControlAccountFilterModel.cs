﻿using System;

namespace NewHotel.Pos.Hub.Contracts.Common.Contracts;

public class PmsControlAccountFilterModel
{
    public new Guid? HotelId { get; set; }
    public string AccountName { get; set; }
    public string AccountEmail { get; set; }
    public string AccountFreeCode { get; set; }
    public string AccountFiscalNumber { get; set; }

    public bool GetDeposits { get; set; } = false;
}