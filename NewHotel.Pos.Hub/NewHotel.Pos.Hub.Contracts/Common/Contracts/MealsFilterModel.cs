﻿using System;

namespace NewHotel.Pos.Hub.Contracts.Common.Contracts;

public class MealsFilterModel
{
    public DateTime Date { get; set; }
    public Guid? InstallationId { get; set; }
    public bool JustCheckInReservation { get; set; }
}