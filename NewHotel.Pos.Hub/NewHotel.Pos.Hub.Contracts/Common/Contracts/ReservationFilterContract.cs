﻿using System;

namespace NewHotel.Pos.Hub.Contracts.Common.Contracts;

/// <summary>
/// 
/// </summary>
public class ReservationFilterContract
{
    public Guid? HotelId { get; set; }
    public string ReservationSerie { get; set; }
    public string GuestName { get; set; }
    public string RoomNumber { get; set; }
    public string CardNumber { get; set; }
    public bool JustCheckedInReservations { get; set; } = true;
    public bool ShowAllGuests { get; set; } = false;
    public bool ShowAccountBalance { get; set; } = false;
    public string RfId { get; set; }
}