﻿using System;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Pos.Hub.Contracts.Common.Contracts;

public class ControlAccountMovementsFilterModel
{

    public Guid? HotelId { get; set; }

    public Guid ControlAccountId { get; set; }

    public EntrieType? MovementType { get; set; }

    public bool? HasReceipt { get; set; }

    public bool? IsNotInvoiced { get; set; }
}