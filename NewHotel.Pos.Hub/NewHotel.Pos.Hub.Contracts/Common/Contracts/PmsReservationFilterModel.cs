﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Contracts.Common.Contracts
{
    public class PmsReservationFilterModel
    {
        public new Guid? HotelId { get; set; }

        public string ReservationSerie { get; set; }

        public string GuestName { get; set; }

        public string RoomNumber { get; set; }

        public string CardNumber { get; set; }

        public bool JustCheckedInReservations { get; set; } = true;

        public bool ShowAllGuests { get; set; } = false;

        public bool ShowAccountBalance { get; set; }
    }
}
