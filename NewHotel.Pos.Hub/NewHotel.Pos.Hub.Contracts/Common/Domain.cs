﻿namespace NewHotel.Pos.Hub.Model
{
    public enum RateType : short { Standard = 0, InternalUse = 1, PensionMode = 2 }

    public static class ProductLineCancellationStatus
    {
        public const short Active = 0;
        public const short Cancelled = 1;
        public const short ActiveByTransfer = 2;
        public const short CancelledAfterPrint = 3;
        public const short CancelledByTransfer = 4;
        public const short CancelledBySplit = 5;
    }
}