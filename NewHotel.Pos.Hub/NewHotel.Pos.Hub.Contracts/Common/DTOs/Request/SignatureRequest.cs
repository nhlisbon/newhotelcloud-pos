﻿using System;

namespace NewHotel.Pos.Hub.Contracts.Common.DTOs.Request;

public class SignatureRequest
{
    public Guid HotelId { get; set; }
    public string LocalCode { get; set; }
    public string Description { get; set; }
    public string Value { get; set; }
    public SignatureDetails[] Details { get; set; }
}

public class SignatureDetails
{
    public string Description { get; set; }
    public string Value { get; set; }
}