﻿namespace NewHotel.Pos.Hub.Contracts.Common.Records.CurrentAccount;

public class ControlAccountMovementDto
{
    public decimal Balance { get; set; }
    public string Currency { get; set; }
}