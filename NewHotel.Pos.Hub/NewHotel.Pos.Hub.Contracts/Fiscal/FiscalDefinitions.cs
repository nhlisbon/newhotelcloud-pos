﻿using NewHotel.Contracts;
using NewHotel.Pos.Hub.Model;
using NewHotel.Pos.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Contracts.Fiscal
{
    public interface IFiscalDefinitions
    {
        bool FiscalNumberRequired { get; }
        bool AddressRequired { get; }
        bool NameRequired { get; }
        bool NationalityRequired { get; }
        bool EmailRequired { get; }
        bool PhoneNumberRequired { get; }
        bool CityRequired {  get; }
        bool CityCodeRequired {  get; }
        bool ClientRegimeTypeCodeRequired {  get; }
        bool ClientResponsibilityCodeRequired { get; }
    }

    public class NullFiscalDefinitions : IFiscalDefinitions
    {
        public bool FiscalNumberRequired => false;
        public bool AddressRequired => false;
        public bool NameRequired => false;
        public bool NationalityRequired => false;
        public bool EmailRequired => false;
        public bool PhoneNumberRequired { get; }
        public bool CityRequired { get; }
        public bool CityCodeRequired { get; }
        public bool ClientRegimeTypeCodeRequired { get; }
        public bool ClientResponsibilityCodeRequired { get; }
    }

    public class FiscalDefinitions : IFiscalDefinitions
    {
        public virtual bool FiscalNumberRequired => true;
        public virtual bool AddressRequired => false;
        public virtual bool NameRequired => false;
        public virtual bool NationalityRequired => false;
        public virtual bool EmailRequired => false;
        public virtual bool PhoneNumberRequired => false;
        public virtual bool CityRequired => false;
        public virtual bool CityCodeRequired => false;
        public virtual bool ClientRegimeTypeCodeRequired => false;
        public virtual bool ClientResponsibilityCodeRequired => false;

    }

    public class PortugalSAFPTFiscalDefinitions : FiscalDefinitions
    {
    }

    public class ColombiaBtwFiscalDefinitions: IFiscalDefinitions 
    {
        public virtual bool FiscalNumberRequired => true;
        public virtual bool AddressRequired => true;
        public virtual bool NameRequired => true;
        public virtual bool NationalityRequired => true;
        public virtual bool EmailRequired => true;
        public virtual bool PhoneNumberRequired => true;
        public virtual bool CityRequired => true;
        public virtual bool CityCodeRequired => true;
        public virtual bool ClientRegimeTypeCodeRequired => true;
        public virtual bool ClientResponsibilityCodeRequired => true;
    }

    public static class FiscalDefinitionsActions
    {
        public static IFiscalDefinitions GetFiscalDefinitions(this DocumentSign documentSign)
        {
            return documentSign switch
            {
                DocumentSign.None => new NullFiscalDefinitions(),
                DocumentSign.FiscalizationPortugal => new PortugalSAFPTFiscalDefinitions(),
                DocumentSign.FiscalizationColombiaBtw => new ColombiaBtwFiscalDefinitions(),
                _ => new FiscalDefinitions()
            };
        }
    }

    public static class FiscalDefinitionsExtensions
    {
        static ValidationResult NewValidation() => [];

        public static ValidationResult ValidateFiscalNumber(this IFiscalDefinitions definitions, string fiscalNumber)
            => NewValidation().Validate(
                () => !definitions.FiscalNumberRequired || !string.IsNullOrEmpty(fiscalNumber),
                999, "FiscalNumberRequired".Translate());

        public static T ValidateFiscalNumber<T>(this T validations, IFiscalDefinitions definitions, string fiscalNumber) where T: ValidationResult
            => validations.Validate(definitions.ValidateFiscalNumber(fiscalNumber));

        public static ValidationResult ValidateAddress(this IFiscalDefinitions definitions, string address)
            => NewValidation().Validate(
                () => !definitions.AddressRequired || !string.IsNullOrEmpty(address),
                999, "AddressRequired".Translate());

        public static T ValidateAddress<T>(this T validations, IFiscalDefinitions definitions, string address) where T : ValidationResult
            => validations.Validate(definitions.ValidateAddress(address));   
        
        public static ValidationResult ValidateName(this IFiscalDefinitions definitions, string name)
            => NewValidation().Validate(
                () => !definitions.NameRequired || !string.IsNullOrEmpty(name),
                999, "NameRequired".Translate());

        public static T ValidateName<T>(this T validations, IFiscalDefinitions definitions, string name) where T : ValidationResult
            => validations.Validate(definitions.ValidateName(name));
        
        public static ValidationResult ValidateNationality(this IFiscalDefinitions definitions, NationalityRecord nationality)
            => NewValidation().Validate(
                () => !definitions.NationalityRequired || nationality is not null,
                999, "NationalityRequired".Translate());

        public static T ValidateNationality<T>(this T validations, IFiscalDefinitions definitions, NationalityRecord nationality) where T : ValidationResult
            => validations.Validate(definitions.ValidateNationality(nationality));

        public static ValidationResult ValidateEmail(this IFiscalDefinitions definitions, string email)
            => NewValidation().Validate(
                () => !definitions.EmailRequired || !string.IsNullOrEmpty(email),
                999, "EmailRequired".Translate());

        public static T ValidateEmail<T>(this T validations, IFiscalDefinitions definitions, string email) where T : ValidationResult
            => validations.Validate(definitions.ValidateEmail(email));

        public static ValidationResult ValidatePhoneNumber(this IFiscalDefinitions definitions, string phoneNumber)
            => NewValidation().Validate(
                () => !definitions.PhoneNumberRequired || !string.IsNullOrEmpty(phoneNumber),
                998, "PhoneNumberRequired".Translate());

        public static T ValidatePhoneNumber<T>(this T validations, IFiscalDefinitions definitions, string phoneNumber) where T : ValidationResult
            => validations.Validate(definitions.ValidatePhoneNumber(phoneNumber));

        public static ValidationResult ValidateCity(this IFiscalDefinitions definitions, string city)
            => NewValidation().Validate(
                () => !definitions.CityRequired || !string.IsNullOrEmpty(city),
                995, "CityRequired".Translate());

        public static T ValidateCity<T>(this T validations, IFiscalDefinitions definitions, string city) where T : ValidationResult
            => validations.Validate(definitions.ValidateCity(city));

        public static ValidationResult ValidateCityCode(this IFiscalDefinitions definitions, string cityCode)
            => NewValidation().Validate(
                () => !definitions.CityCodeRequired || !string.IsNullOrEmpty(cityCode),
                994, "CityCodeRequired".Translate());

        public static T ValidateCityCode<T>(this T validations, IFiscalDefinitions definitions, string cityCode) where T : ValidationResult
            => validations.Validate(definitions.ValidateCityCode(cityCode));

        public static ValidationResult ValidateClientRegimeTypeCode(this IFiscalDefinitions definitions, string clientRegimeTypeCode)
            => NewValidation().Validate(
                () => !definitions.ClientRegimeTypeCodeRequired || !string.IsNullOrEmpty(clientRegimeTypeCode),
                993, "ClientRegimeTypeCodeRequired".Translate());

        public static T ValidateClientRegimeTypeCode<T>(this T validations, IFiscalDefinitions definitions, string cityCode) where T : ValidationResult
            => validations.Validate(definitions.ValidateClientRegimeTypeCode(cityCode));

        public static ValidationResult ValidateClientResponsibilityCode(this IFiscalDefinitions definitions, string clientResponsibilityCode)
            => NewValidation().Validate(
                () => !definitions.ClientResponsibilityCodeRequired || !string.IsNullOrEmpty(clientResponsibilityCode),
                992, "ClientResponsibilityCodeRequired".Translate());

        public static T ValidateClientResponsibilityCode<T>(this T validations, IFiscalDefinitions definitions, string clientResponsibilityCode) where T : ValidationResult
            => validations.Validate(definitions.ValidateClientResponsibilityCode(clientResponsibilityCode));
    }
}
