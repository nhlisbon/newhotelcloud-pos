﻿using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Contracts
{
    public class ErrorCodePos
    {
        public static int Communication => Res.CommunicationError;
        public static int General => 2;

        /// <summary>
        /// Communication Error on online doc exportation
        /// </summary>
        public static int DocExportationCommunication => 3;
    }
}
