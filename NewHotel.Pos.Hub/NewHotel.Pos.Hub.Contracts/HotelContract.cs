﻿using System.Runtime.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Contracts
{
    [DataContract]
    public class HotelContract : BaseContract
    {

        [DataMember]
        public string Abbreviature { get; set; }
        
        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string AuxiliarCode { get; set; }

        [DataMember]
        public string Nationality { get; set; }

        [DataMember]
        public string Logotype { get; set; }

        [DataMember]
        public string FiscalNumber { get; set; }
    }
}
