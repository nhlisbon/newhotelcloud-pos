﻿using NewHotel.Contracts;
using NewHotel.Payments.Device.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Pos.Hub.Contracts.PaySystem
{
	public sealed class ExternalPaymentValues
    {
		public string UserReference { get; set; }
		public string Workstation { get; set; }

		#region Values On Input
		public decimal Value { get; set; }
		public decimal Tax { get; set; }
		public string CurrencyCode { get; set; }
		/// <summary>Payments quotes.</summary>
		public decimal Quota { get; set; }
		public decimal? Tip { get; set; }
		#endregion
	}

	public class ExternalPaymentRequestContract : BaseContract
	{
		public ExternalPaymentValues Values { get; set; }

		public Guid PaymentMethodId { get; set; }
		public Guid TicketId { get; set; }
		public Guid? PaySystemTerminalId { get; set; }
		public string AuthorizationCode { get; set; }
		public string ExternalToken { get; set; }
		public string ReferenceTransactionId { get; set; }
	}

	public class ExternalPaymentDeviceCompleteRequest
	{
		public Guid EntryId { get; set; }
		public DeviceActionResult DeviceActionResult { get; set; }
	}
}
