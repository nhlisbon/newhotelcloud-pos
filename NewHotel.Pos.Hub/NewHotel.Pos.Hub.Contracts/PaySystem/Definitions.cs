﻿namespace NewHotel.Pos.Hub.Contracts.PaySystem
{
	public enum ActionRequestType
	{
		None = 0x0f,
		/// <summary>Verificar la validez y saldo del medio de pago o reservar valor para uso posterior</summary>
		Authorize = 0,
		/// <summary>Verificar y cobrar</summary>
		AuthorizeAndCapture = 1,
		/// <summary>Cancelar un pago hecho</summary>
		Void = 2,
		/// <summary>Devolver un montante cobrado.</summary>
		Credit = 3,
		/// <summary>Finalizar un pago ya autorizado.</summary>
		Finalize = 4,
		/// <summary>Aumentar valor reservado</summary>
		TopUp = 5,
		/// <summary>Completar una operación que quedó pendiente (no se persiste en base de datos).</summary>
		ContinuePending = 6,
		/// <summary>Abortar una operación que está pendiente.</summary>
		Abort = 7,
	}

	public enum PayReason
	{
		NoReason = 0,
		NoShow = 1,
		AdvancedDeposit = 2,
		LateCharge = 3,
	}

	/*
	/// <summary>El resultado de una operación.</summary>
	public enum PayOperationResult
	{
		/// <summary>No se conoce el resultado de la operación o esta no ha concluído.</summary>
		Unknown = 0,
		/// <summary>La operación fue aprovada.</summary>
		Approved = 1,
		/// <summary>La operación de rechazó porque requiere de autorización.</summary>
		AuthorizationNeeded = 3,
		/// <summary>La operación se rechazó.</summary>
		Declined = 4,
		/// <summary>La operación falló.</summary>
		Failed = 5,
		/// <summary>La operación se ha iniciado, pero no se ha concluído y está en un paso intermedio.</summary>
		Processing = 6,
	}
	*/
}
