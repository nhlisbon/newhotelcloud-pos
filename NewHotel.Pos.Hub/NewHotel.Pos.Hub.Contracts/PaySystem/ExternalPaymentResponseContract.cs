﻿using NewHotel.Contracts;
using System;

namespace NewHotel.Pos.Hub.Contracts.PaySystem
{
	public class ExternalPaymentResponseContract : BaseContract
	{
		public Guid OperationId { get; set; }
		public Guid RequestId { get; set; }
		public Guid TicketId { get; set; }
		public decimal Value { get; set; }
		public string CurrencyCode { get; set; }
		public string DeviceRequest { get; set; }
		public string SystemType { get; set; }
	}

	public class ExternalPaymentEntryContract : BaseContract
	{
		public Guid RequestId { get; set; }
		public Guid OriginId { get; set; }
		public Guid PaymentMethodId { get; set; }
		public Guid? ReferencedEntryId { get; set; }
		public Guid Ticket { get; set; }
		public Guid? TransactionId { get; set; }
		public ActionRequestType Type { get; set; }
		public PaymentOperationStatus Status { get; set; }
		public DateTime Date { get; set; }
		public DateTime WorkDate { get; set; }
		public decimal Amount { get; set; }
		public Guid? TerminalId { get; set; }
		public Guid? OperationId { get; set; }
		public string LinkUrl { get; set; }
		public string CardExternalType { get; set; }
		public string CardNumber { get; set; }
		public string CardHolder { get; set; }
		public string Authorization { get; set; }
		public string Nsu { get; set; }
		public string Token { get; set; }
		public string SysResultMessage { get; set; }
		public string SysResultCode { get; set; }
	}

	public class ExternalPaymentEntryModel
	{
		public Guid Id { get; set; }
		public Guid RequestId { get; set; }
		public Guid OriginId { get; set; }
		public Guid PaymentMethodId { get; set; }
		public Guid? ReferencedEntryId { get; set; }
		public Guid Ticket { get; set; }
		public Guid? TransactionId { get; set; }
		public ActionRequestType Type { get; set; }
		public PaymentOperationStatus Status { get; set; }
		public DateTime Date { get; set; }
		public DateTime WorkDate { get; set; }
		public decimal Amount { get; set; }
		public Guid? TerminalId { get; set; }
		public Guid? OperationId { get; set; }
		public string LinkUrl { get; set; }
		public string CardExternalType { get; set; }
		public string CardNumber { get; set; }
		public string CardHolder { get; set; }
		public string Authorization { get; set; }
		public string Nsu { get; set; }
		public string Token { get; set; }
		public string SysResultMessage { get; set; }
		public string SysResultCode { get; set; }
	}

	public class TicketExternalPayments
	{
		public Guid TicketId { get; set; }
		public ExternalPaymentEntryModel[] Entries { get; set; }
	}
}
