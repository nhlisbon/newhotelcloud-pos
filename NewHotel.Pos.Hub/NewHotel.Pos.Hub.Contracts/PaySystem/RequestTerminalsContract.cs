﻿using NewHotel.Contracts;
using System;

namespace NewHotel.Pos.Hub.Contracts.PaySystem
{
	public class RequestTerminalsContract : BaseContract
	{
		public PayTerminalModel[] Terminals { get; set; }
	}

	public class PayTerminalModel
	{
		public Guid Id { get; set; }
		public Guid OriginId { get; set; }
		public string Name { get; set; } = string.Empty;
		public string Workstation { get; set; }
	}
}
