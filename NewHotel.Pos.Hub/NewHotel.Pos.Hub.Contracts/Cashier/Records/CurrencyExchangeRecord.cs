﻿using System;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Pos.Hub.Contracts.Cashier.Records
{
    [MappingQuery("CAMB_PK")]
    public class CurrencyExchangeRecord : BaseRecord
    {
        [MappingColumn("HUNMO_PK")]
        public Guid CurrencyCashierId { get; set; }

        [MappingColumn("CAMB_DATE")]
        public DateTime Date { get; set; }

        [MappingColumn("CAMB_VALO")]
        public decimal ChangeValue { get; set; }
    }
}
