﻿using System;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Pos.Hub.Model.WaitingListStand.Record
{
    [MappingQuery("WLST_PK")]
    public class PaxWaitingListRecord : BaseRecord
    {
        [MappingColumn("IPOS_PK", Nullable = true)]
        public Guid? StandId { get; set; }

        [MappingColumn("SALO_PK", Nullable = true)]
        public Guid? SalonId { get; set; }

        [MappingColumn("WLST_NAME", Nullable = false)]
        public string Name { get; set; }

        [MappingColumn("WLST_PHONE", Nullable = true)]
        public string PhoneNumber { get; set; }

        [MappingColumn("WLST_DARE", Nullable = false)]
        public DateTime RegisterDate { get; set; }

        [MappingColumn("WLST_PAXS", Nullable = false)]
        public short Paxs { get; set; }
    }
}