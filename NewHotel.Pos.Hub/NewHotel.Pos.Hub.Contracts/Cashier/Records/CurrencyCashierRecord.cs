﻿using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Pos.Hub.Contracts.Cashier.Records
{
    [MappingQuery("HUNMO_PK")]
    public class CurrencyCashierRecord : BaseRecord
    {
        [MappingColumn("UNMO_PK")]
        public string CurrencyId { get; set; }

        [MappingColumn("UNMO_SYMB")]
        public string Symbol { get; set; }

        [MappingColumn("CAMB_MULT")]
        public bool Mult { get; set; }

        public bool IsBase { get; set; }

		[MappingColumn("UNMO_BASE")]
		public bool IsHotelBase { get; set; }
    }
}
