﻿using System;
using NewHotel.Contracts;
using NewHotel.DataAnnotations;

namespace NewHotel.Pos.Hub.Contracts.Cashier.Records
{
    [MappingQuery("ESIM_PK")]
    public class TaxSchemaRecord : BaseRecord
    {
        [MappingColumn("HOTE_PK")]
        public Guid HotelId { get; set; }

        [MappingColumn("ESIM_DEFA")]
        public bool Default { get; set; }

        [MappingColumn("ESIM_DESC")]
        public string Description { get; set; }
    }
}
