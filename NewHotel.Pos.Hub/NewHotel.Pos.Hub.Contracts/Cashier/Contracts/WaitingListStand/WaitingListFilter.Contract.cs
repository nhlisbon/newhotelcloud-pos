﻿using System;
using System.Runtime.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Model.WaitingListStand.Contract
{
    [DataContract]
    public class WaitingListFilterContract : BaseContract
    {
        [DataMember]
        public Guid? StandId { get; set; }

        [DataMember]
        public Guid? SaloonId
        { get; set; }

        [DataMember]
        public string Name
        { get; set; }

        [DataMember]
        public string PhoneNumber
        { get; set; }

        [DataMember]
        public DateTime? RegisterDate
        { get; set; }

        [DataMember]
        public short? Paxs
        { get; set; }
    }
}