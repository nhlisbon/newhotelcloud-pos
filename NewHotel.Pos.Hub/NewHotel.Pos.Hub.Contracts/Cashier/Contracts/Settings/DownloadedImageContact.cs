﻿using System;
using System.Runtime.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Model.Cashier.Contracts.Settings
{
    [DataContract]
    public class DownloadedImageContact : BaseContract
    {
        [DataMember]
        public string PathHub { get; set; }
        [DataMember]
        public byte[] Image { get; set; }
        [DataMember]
        public DateTime LastWriteTimeUtc { get; set; }
    }
}