﻿using System;
using System.Runtime.Serialization;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Model.WaitingListStand.Contract
{
    [DataContract]
    public class TablesReservationFilterContract : BaseContract
    {
        [DataMember]
        public Guid? TableReservationId { get; set; }

        [DataMember]
        public DateTime? InDate { get; set; }

        [DataMember]
        public Guid? StandId { get; set; }

        [DataMember]
        public Guid[] SaloonIds { get; set; }

        [DataMember]
        public Guid? SlotId { get; set; }

        [DataMember]
        public Guid[] BookingSlotIds { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public long? KindReservation { get; set; }

        [DataMember]
        public ReservationState? StateReservation { get; set; }
    }
}