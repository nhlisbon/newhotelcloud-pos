﻿using System;

namespace NewHotel.Pos.Hub.Contracts.Handheld.Contracts
{
    public class DispatchContract
    {
        public Guid OrderId { get; set; }

        public Guid[] AreaIds { get; set; } = [];

        public DispatchContract[] OrderIds { get; set; } = [];
    }
}
