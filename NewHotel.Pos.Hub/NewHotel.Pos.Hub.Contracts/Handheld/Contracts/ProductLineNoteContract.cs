﻿using System;

namespace NewHotel.Pos.Hub.Contracts.Handheld.Contracts
{
    public class ProductLineNoteContract
    {
        public Guid ProductLineId { get; set; }

        public string Note { get; set; }
        
        public TableLineNoteContract[] TableLineIds { get; set; }
    }

    public class TableLineNoteContract
    {
        public Guid TableLineId { get; set; }

        public string Note { get; set; }
    }
}
