﻿using System;

namespace NewHotel.Pos.Hub.Contracts.Handheld.Contracts
{
    public class ProductLinePreparationContract
    {
        public Guid ProductLineId { get; set; }

        public Guid[] PreparationIds { get; set; }
        
        public TableLinePreparationContract[] TableLineIds { get; set; }
    }

    public class TableLinePreparationContract
    {
        public Guid TableLineId { get; set; }

        public Guid[] PreparationIds { get; set; }
    }
}
