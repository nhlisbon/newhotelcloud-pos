using System;

namespace NewHotel.Pos.Hub.Contracts.Handheld.Records
{
    public class OrderRecord
    {
        public Guid Id { get; set; }
        public Guid TableId { get; set; }
        public Guid ProductId { get; set; }
        public string ProductDescription { get; set; }
        public decimal ProductPrice { get; set;  }
        public decimal Quantity { get; set; }
        public bool Placed { get; set; }
        public string Notes { get; set; }
        public Guid[] Preparations { get; set; }
    }
}