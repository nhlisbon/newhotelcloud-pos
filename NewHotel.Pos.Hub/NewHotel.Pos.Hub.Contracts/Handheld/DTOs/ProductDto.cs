using System.Collections.Generic;

namespace NewHotel.Pos.Hub.Contracts.Handheld.DTOs
{
    public class ProductDto
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public decimal Price { get; set; }
        public IEnumerable<PreparationDto> Preparations { get; set; }
    }
}