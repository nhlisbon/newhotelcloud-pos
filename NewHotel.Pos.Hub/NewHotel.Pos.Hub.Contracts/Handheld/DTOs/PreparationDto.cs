namespace NewHotel.Pos.Hub.Contracts.Handheld.DTOs
{
    public class PreparationDto
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }
}