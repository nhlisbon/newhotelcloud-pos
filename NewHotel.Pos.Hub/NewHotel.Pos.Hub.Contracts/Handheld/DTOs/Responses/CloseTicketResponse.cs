﻿using NewHotel.Contracts;
using NewHotel.Pos.Hub.Contracts.Handheld.DTOs.Requests;

namespace NewHotel.Pos.Hub.Contracts.Handheld.DTOs.Responses;

public class CloseTicketResponse
{
    public POSTicketContract Ticket { get; set; }
    public CloseTicketRequest Request { get; set; }
}