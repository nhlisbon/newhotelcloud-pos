﻿using NewHotel.Contracts;
using NewHotel.Pos.Hub.Contracts.Handheld.DTOs.Requests;

namespace NewHotel.Pos.Hub.Contracts.Handheld.DTOs.Responses;

public class AddPaymentResponse
{
    public POSTicketContract Ticket { get; set; }
    public AddPaymentsRequest Request { get; set; }
}