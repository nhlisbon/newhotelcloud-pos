﻿using System;
using System.Collections.Generic;
using NewHotel.Contracts;

namespace NewHotel.Pos.Hub.Contracts.Handheld.DTOs;

public class StandTableInfoDto
{
    public List<Guid> TablesWithOpenOrders { get; set; }
    public List<Guid> TablesWithPendingMenuOrders { get; set; }
    public List<LiteTablesReservationRecord> TablesWithCheckInReservations { get; set; }
}