﻿using System;
using NewHotel.Contracts;
using NewHotel.Pos.Hub.Model;

namespace NewHotel.Pos.Hub.Contracts.Handheld.DTOs.Requests;

public class CloseTicketRequest
{
    public Guid TicketId { get; set; }
    public bool CloseAsInvoice { get; set; }
    public ClientDto? Client { get; set; }
    public PersonalDocType DocTypeSelected { get; set; }
    public bool SaveClient { get; set; }
    public string Signature { get; set; }
    public PaymentsDto[] Payments { get; set; }
}

public class ClientDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Address { get; set; }
    public string EmailAddress { get; set; }
    public string FiscalNumber { get; set; }
    public string Country { get; set; }
    public string Identity { get; set; }
    public string Passport { get; set; }
    public string Residence { get; set; }
}