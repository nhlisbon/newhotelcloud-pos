﻿using System;

namespace NewHotel.Pos.Hub.Contracts.Handheld.DTOs.Requests;

public class PaymentsDto
{
    public CashPaymentDto? CashPayment { get; set; }
    public CreditRoomPaymentDto? CreditRoomPayment { get; set; }
    public RoomPlanPaymentDto? RoomPlanPayment { get; set; }
    public HouseUsePaymentDto? HouseUsePayment { get; set; }
}

public sealed class CashPaymentDto {
    public Guid PaymentId { get; set; }
    public string PaymentDescription { get; set; }
    public Guid? CreditCardId { get; set; }
    public decimal Received { get; set; }
    public decimal Value { get; set; }
    public bool ExcessPaymentAsTip { get; set; }
}

public sealed class CreditRoomPaymentDto
{
    public decimal? Amount { get; set; }
    public ReservationDto Reservation { get; set; }
}

public sealed class RoomPlanPaymentDto
{
    public ReservationDto Reservation { get; set; }
}

public sealed class HouseUsePaymentDto
{
    public Guid Id { get; set; }
    public string Description { get; set; }
    public bool Unlimited { get; set; }
    public decimal? LimitPercent { get; set; }
}