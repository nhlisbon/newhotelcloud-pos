﻿using System;
using System.Collections.Generic;

namespace NewHotel.Pos.Hub.Contracts.Handheld.DTOs.Requests;

public class NewTicketDto
{
    public Guid? TableId { get; set; }
    public string Description { get; set; }
    public short? Paxs { get; set; }
    public AddClientDto? Client { get; set; }
    public AddProductDto Product { get; set; }
    public ReservationDto? Reservation { get; set; }
    public SpaServiceDto? SpaService { get; set; }
}

public class AddProductDto
{
    public Guid ProductId { get; set; }
    public int Quantity { get; set; }
    public decimal? ManualPrice { get; set; }
    public string ManualPriceDescription { get; set; }
    public Guid[]? ProductIds { get; set; }
    public Guid[]? PreparationsIds { get; set; }
    public short? Seat { get; set; }
    public Guid? SeparatorId { get; set; }
    public string Observations { get; set; }
}

public static class AddProductExt
{
    public static bool HasAdditionalProducts(this AddProductDto dto) => dto.ProductIds is { Length: > 0 }; 
}

public class AddClientDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string FiscalNumber { get; set; }
    public string Address { get; set; }
    public string Country { get; set; }
    public string EmailAddress { get; set; }
    public bool ApplyAutomaticProductDiscount { get; set; }
}

public class SpaServiceDto
{
    public Guid ServiceId {get; set; }
    public string GuestName { get; set; }
    public string[] Allergies { get; set; }
    public string[] Diets { get; set; }
    public string UncommonAllergies { get; set; }
}