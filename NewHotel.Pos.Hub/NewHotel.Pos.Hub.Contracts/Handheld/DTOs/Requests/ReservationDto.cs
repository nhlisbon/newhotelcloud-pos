﻿using System;

namespace NewHotel.Pos.Hub.Contracts.Handheld.DTOs.Requests;

public class ReservationDto
{
    public short? Type { get; set; }
    public Guid AccountId { get; set; }
    public string ReservationNumber { get; set; }
    public string Room { get; set; }
    public string Guest { get; set; }
    public string Company { get; set; }
    public bool AllowCreditPos { get; set; }
    public bool IsLock { get; set; } = false;
    public decimal? Balance { get; set; }
    public string CardNumber { get; set; }
}