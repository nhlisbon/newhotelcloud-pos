﻿using System;

namespace NewHotel.Pos.Hub.Contracts.Handheld.DTOs.Requests;

public sealed class AddPaymentsRequest
{
    public Guid TicketId { get; set; }
    public PaymentsDto[] Payments { get; set; }
}