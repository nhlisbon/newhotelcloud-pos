using System.Collections.Generic;

namespace NewHotel.Pos.Hub.Contracts.Handheld.DTOs
{
    public class MenuEnvDto
    {
        public List<MenuDto> Menus { get; set; }
        public List<TicketDto> Tickets { get; set; }
        
        public string SocketIoUrl { get; set; }
    }
}