using System.Collections.Generic;

namespace NewHotel.Pos.Hub.Contracts.Handheld.DTOs
{
    public class MenuDto
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public IEnumerable<ProductDto> Products { get; set; }
    }
}