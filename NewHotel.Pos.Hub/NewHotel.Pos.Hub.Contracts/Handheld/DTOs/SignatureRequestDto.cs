﻿using System;

namespace NewHotel.Pos.Hub.Contracts.Handheld.DTOs;

public class SignatureRequestDto
{
    public Guid TicketId { get; set; }
    public string TicketSaleNumber  { get; set; }
    public string PointOfSale { get; set; }
}