using System;

namespace NewHotel.Pos.Hub.Contracts.Handheld.DTOs
{
    public class MenuDataDto
    {
        public Guid Id { get; set; }
        public Guid HotelId { get; set; }
        public Guid PosId { get; set; }
        public Guid SaloonId { get; set; }
        public Guid TableId { get; set; }
        public Guid ProductId { get; set; }
        public decimal? Quantity { get; set; }
        public Guid[] Preparations { get; set; }
    }
}