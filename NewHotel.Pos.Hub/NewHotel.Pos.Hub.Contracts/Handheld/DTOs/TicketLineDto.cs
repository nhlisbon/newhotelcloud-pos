using System;
using System.Collections.Generic;
using System.Configuration;

namespace NewHotel.Pos.Hub.Contracts.Handheld.DTOs
{
    public class TicketLineDto
    {
        public string Id { get; set; }
        public string ProductId { get; set; }
        public string ProductDescription { get; set; }
        public decimal ProductPrice { get; set; }
        public decimal Quantity { get; set; }
        public bool Placed { get; set; }
        public bool Attended { get; set; }
        public IEnumerable<Guid> Preparations { get; set; }
    }
}