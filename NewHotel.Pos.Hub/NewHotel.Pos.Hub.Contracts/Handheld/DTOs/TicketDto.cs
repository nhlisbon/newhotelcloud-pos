using System.Collections.Generic;

namespace NewHotel.Pos.Hub.Contracts.Handheld.DTOs
{
    public class TicketDto
    {
        public string Id { get; set; }
        public IEnumerable<TicketLineDto> TicketLines { get; set; }
    }
}