using System;

namespace NewHotel.Pos.Hub.Contracts.Handheld.DTOs
{
    public class ProductLineDto
    {
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
        public Guid[]? Preparations { get; set; }
        public string Observations { get; set; } 
    }
}