﻿using System.ComponentModel;

namespace NewHotel.WPF.Core
{
    public static class Ext
    {
        public static void Clean(this ICollectionView view)
        {
            view.Filter = null;
            view.SortDescriptions.Clear();
            view.GroupDescriptions.Clear();
        }
    }
}