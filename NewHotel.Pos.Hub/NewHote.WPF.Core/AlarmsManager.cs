﻿using System;
using System.Collections.Generic;
using System.Timers;

namespace NewHotel.WPF.Common
{
    public class AlarmsManager
    {
        Timer timer;
        Dictionary<string, AlarmsSet> data = new Dictionary<string, AlarmsSet>();

        public AlarmsManager()
        {
            timer = new Timer();
            timer.Interval = 30000; // 30 second updates
            timer.Elapsed += timer_Elapsed;
            timer.Start();
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            foreach (AlarmsSet set in data.Values)
                set.RaiseAlarm(DateTimeReference.TimeOfDay, DateTimeReference);
        }

        public Guid? AddAlarm(string alarmsSetDescription, TimeSpan time, object tag, Action<object> action, Func<object, DateTime, bool> activationRequest)
        {
            Alarm newAlarm = null;
            if (!data.ContainsKey(alarmsSetDescription))
                data.Add(alarmsSetDescription, new AlarmsSet());
            data[alarmsSetDescription].AddAlarm(newAlarm = new Alarm(time, tag, action, activationRequest, DateTimeReference), DateTimeReference);

            return newAlarm != null ? (Guid?)newAlarm.id : null;
        }

        public bool RemoveAlarms(string alarmsSet, TimeSpan time)
        {
            throw new NotImplementedException();
        }

        public bool RemoveAlarms(string alarmsSetDescription)
        {
            try
            {
                data.Remove(alarmsSetDescription);
            }
            catch { return false; }
            return true;
        }


        private DateTime DateTimeReference
        {
            get
            {
                return DateTime.Now;
            }
        }

    }

    internal class Alarm : IComparable<Alarm>
    {
        internal TimeSpan time;
        internal Action<object> action;
        internal Guid id;
        internal bool activated;
        Func<object, DateTime, bool> reactivationRequest;
        internal DateTime lastRaised;
        private object tag;

        internal Alarm(TimeSpan time, object tag, Action<object> action, Func<object, DateTime, bool> activationRequest, DateTime dateTimeReference)
        {
            id = Guid.NewGuid();
            this.time = time;
            this.action = action;
            this.tag = tag;
            this.reactivationRequest = activationRequest;
            if (reactivationRequest == null) activated = true;
            else activated = activationRequest(tag, dateTimeReference);
        }

        internal bool Reactivate(DateTime dateTimeReference)
        {
            if (reactivationRequest == null) return false;
            return activated = reactivationRequest(tag, dateTimeReference);
        }

        internal Guid Id
        {
            get
            {
                return id;
            }
        }

        internal bool Raise(DateTime dateTime)
        {
            lastRaised = dateTime;
            try { action(tag); }
            catch  { return false; }
            finally { activated = false; }
            return true;
        }

        public int CompareTo(Alarm other)
        {
            return time.CompareTo(other.time);
        }
    }

    internal class AlarmsSet
    {
        Queue<Alarm> queue = new Queue<Alarm>();
        //SortedList<TimeSpan, Alarm> list = new SortedList<TimeSpan, Alarm>();
        DateTime lastAlarmDateTime = DateTime.MinValue;

        internal int RaiseAlarm(TimeSpan time, DateTime dateTimeReference)
        {
            int count = 0;
            if (queue.Count > 0)
            {
                while (queue.Peek().time <= time && queue.Peek().lastRaised.Date < dateTimeReference.Date)
                {
                    var alarm = queue.Dequeue();
                    if (!alarm.activated) alarm.Reactivate(dateTimeReference);
                    if (alarm.activated)
                    {// La alarma esta activada
                        alarm.Raise(dateTimeReference);
                        count++;
                    }// La alarma esta activada
                    queue.Enqueue(alarm);
                }
            }
            return count;
        }

        internal bool AddAlarm(Alarm alarm, DateTime dateTimeReference)
        {
            if (alarm.time <= dateTimeReference.TimeOfDay)
                alarm.lastRaised = dateTimeReference;

            if (queue.Count == 0)
                queue.Enqueue(alarm);
            else
            {
                var auxList = new SortedSet<Alarm>(queue);
                auxList.Add(alarm);
                queue = new Queue<Alarm>(auxList);
                int count = queue.Count;
                while (queue.Peek().time <= dateTimeReference.TimeOfDay && count > 0)
                {
                    var auxAlarm = queue.Dequeue();
                    auxAlarm.activated = false;
                    auxAlarm.lastRaised = dateTimeReference;
                    queue.Enqueue(auxAlarm);
                    count--;
                }
            }
            return true;
        }

        internal bool RemoveAllAlarms()
        {
            queue.Clear();
            return true;
        }
    }
}
