﻿using System;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace NewHotel.WPF.Common
{
    public enum AlarmAction { HappyHourStart, HappyHourEnd, DutyStart, DutyEnd };

    public class GridColumnDefinition
    {
        public GridColumnDefinition(string path, string header, double minWidth, ControlTemplate template)
        {
            Path = path;
            Header = header;
            MinWidth = minWidth;
            Template = template;
        }

        public string Path { get; set; }
        public string Header { get; set; }
        public double MinWidth { get; set; }
        public ControlTemplate Template { get; set; }
    }

    public static class Utils
    {
        public enum TransferTicketOperations { Request, Accept, Return };

        public static void GenerateColumns(this DataGrid dataGrid, string representation = "", params string[] columns)
        {
            var style = new Style(typeof(TextBlock));
            style.Setters.Add(new Setter() { Property = TextBlock.VerticalAlignmentProperty, Value = VerticalAlignment.Center });

            dataGrid.Columns.Clear();
            if (dataGrid.ItemsSource != null)
            {
                var etor = dataGrid.ItemsSource.GetEnumerator();
                if (etor.MoveNext())
                {
                    var type = etor.Current.GetType();
                    if (columns == null || columns.Length == 0)
                    {
                        foreach (var prop in type.GetProperties())
                        {
                            var column = new DataGridTextColumn
                            {
                                ElementStyle = style,
                                Header = prop.Name,
                                Binding = new Binding(prop.Name),
                                Width = new DataGridLength(1, DataGridLengthUnitType.Star)
                            };

                            if (columns == null || columns.Length == 0 || columns.Contains(column.Header))
                                dataGrid.Columns.Add(column);
                        }
                    }
                    else
                    {
                        foreach (var propName in columns)
                        {
                            {
                                var prop = type.GetProperty(propName);
                                var column = new DataGridTextColumn() { ElementStyle = style };
                                column.Header = propName;
                                column.Binding = new Binding(propName);
                                column.Width = new DataGridLength(1, DataGridLengthUnitType.Star);
                                if ((columns == null || columns.Length == 0) || columns.Contains(column.Header))
                                    dataGrid.Columns.Add(column);
                            }
                        }
                    }

                    if (dataGrid.Columns.Count == 0)
                        GenerateColumns(dataGrid, string.Empty, columns);
                }
            }
        }

        public static void GenerateColumns(this DataGrid dataGrid, string representation = "", params GridColumnDefinition[] columns)
        {
            var style = new Style(typeof(TextBlock));
            style.Setters.Add(new Setter { Property = TextBlock.VerticalAlignmentProperty, Value = VerticalAlignment.Center });

            dataGrid.Columns.Clear();
            if (dataGrid.ItemsSource != null)
            {
                var etor = dataGrid.ItemsSource.GetEnumerator();
                if (etor.MoveNext())
                {
                    var type = etor.Current.GetType();
                    if (columns != null && columns.Length > 0)
                    {
                        foreach (var col in columns)
                        {
                            var path = new string[] { col.Path };
                            if (col.Path.Contains('*'))
                                path = col.Path.Split('*');

                            var prop = type.GetProperty(path[0]);
                            DataGridBoundColumn column = null;
                            if (prop != null && prop.PropertyType == typeof(bool))
                            {
                                var styleCheckBox = new Style(typeof(CheckBox));
                                styleCheckBox.Setters.Add(new Setter() { Property = TextBlock.VerticalAlignmentProperty, Value = VerticalAlignment.Center });
                                styleCheckBox.Setters.Add(new Setter() { Property = TextBlock.HorizontalAlignmentProperty, Value = HorizontalAlignment.Center });
                                styleCheckBox.Setters.Add(new Setter() { Property = TextBlock.IsEnabledProperty, Value = false });
                                if (col.Template != null)
                                    styleCheckBox.Setters.Add(new Setter(CheckBox.TemplateProperty, col.Template));
                                column = new DataGridCheckBoxColumn() { ElementStyle = styleCheckBox };
                            }
                            else
                                column = new DataGridTextColumn() { ElementStyle = style };
                            column.Header = col.Header;
                            column.Binding = new Binding(path[0]);
                            if (path.Length > 1) column.Binding.StringFormat = path[1];
                            column.Width = new DataGridLength(1, DataGridLengthUnitType.Star);
                            column.MinWidth = col.MinWidth;
                            column.IsReadOnly = true;
                            dataGrid.Columns.Add(column);
                        }
                    }

                    if (dataGrid.Columns.Count == 0)
                        GenerateColumns(dataGrid, string.Empty, columns);
                }
            }
        }

        public static void AnimateUsingAFunction(Func<TimeSpan, object> func, Action<object> action, TimeSpan duration)
        {
            var timer = new Timer(200) { Enabled = true };
            timer.Elapsed += (obj, evArgs) =>
            {
                UIThread.Invoke(() =>
                {
                    action(func(new TimeSpan(evArgs.SignalTime.Ticks)));
                });
                if (new TimeSpan(evArgs.SignalTime.Ticks) >= duration)
                    timer.Stop();
            };

            timer.Start();
        }

        public static Func<TimeSpan, object> CreateDoubleFuntionAnimation(double ini, double fin, TimeSpan duration, TimeSpan interval)
        {
            var offset = fin - ini;
            var step = offset / (duration.Ticks / interval.Ticks);

            return (TimeSpan time) =>
            {
                if (time >= duration)
                    return fin;
                return (time.Ticks / interval.Ticks) * step + ini;
            };
        }

        public static T GetPropertyValue<T>(this object item, string propertyName)
        {
            object value = item;
            try
            {
                if (!string.IsNullOrEmpty(propertyName))
                    foreach (var inProp in propertyName.Split('.'))
                        value = value.GetType().GetProperty(inProp).GetValue(value, null);
                return (T)value;
            }
            catch { return default(T); }
        }
        public static void SetPropertyValue<T>(this object item, string propertyName, T newValue)
        {
            var value = item;
            try
            {
                if (!string.IsNullOrEmpty(propertyName))
                {
                    var list = propertyName.Split('.');
                    int count = list.Count();
                    if (count > 1)
                        foreach (var inProp in list.Take(count - 1))
                            value = value.GetType().GetProperty(inProp).GetValue(value, null);
                    value.GetType().GetProperty(list.Last()).SetValue(value, newValue, null);
                }

            }
            catch { }
        }

        public static T InvokeMethod<T>(this object item, string methodName, params object[] args)
        {
            object value = null;
            value = item.GetType().GetMethod(methodName).Invoke(item, args);
            try
            {
                return (T)value;
            }
            catch { return default(T); }
        }

        public static void InvokeMethod(this object item, string methodName, params object[] args)
        {
            item.GetType().GetMethod(methodName).Invoke(item, args);
        }
         
        public static CollectionView? GetCollectionView(this object collection)
        {
            try
            {
                return (CollectionView)CollectionViewSource.GetDefaultView(collection);
            }
            catch
            {
                return null;
            }
        }
    }

    public class AlarmInfo
    {
        public AlarmInfo() { }
        public AlarmInfo(AlarmAction action, Guid id, DateTime time, string description)
        {
            Action = action;
            Identifier = id;
            Time = time;
            Description = description;
        }

        public AlarmAction Action { get; set; }
        public Guid Identifier { get; set; }
        public DateTime Time { get; set; }
        public string Description { get; set; }
    }


    public class DelayTimer
    {
        Timer timer;
        bool delay = false;
        public event ElapsedEventHandler Elapsed;

        public DelayTimer(int interval = 1000)
        {
            timer = new Timer(interval);
            timer.Enabled = false;
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!delay)
            {
                timer.Enabled = false;
                if (Elapsed != null)
                    Elapsed(sender, e);
            }
            else delay = false;
        }

        public void Start()
        {
            delay = false;
            timer.Enabled = true;
        }

        public void Stop()
        {
            delay = false;
            timer.Enabled = false;
        }

        public void Delay()
        {
            if (timer.Enabled)
                delay = true;
            else Start();
        }

        public bool IsEnabled
        {
            get
            {
                return timer.Enabled;
            }
        }
    }
}