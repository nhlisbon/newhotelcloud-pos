﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace NewHotel.WPF.Common
{
    public static class UIThread
    {
        private static readonly Dispatcher Dispatcher;

		static UIThread()
        {
            // Store a reference to the current Dispatcher once per application
            Dispatcher = Application.Current.Dispatcher;
			GetScheduler(Dispatcher).ContinueWith(t => UIThread.Scheduler = t.Result);
        }

		public static TaskScheduler Scheduler { get; private set; }

		/// <summary>
		///   Invokes the given action on the UI thread - if the current thread is the UI thread this will just invoke the action directly on
		///   the current thread so it can be safely called without the calling method being aware of which thread it is on.
		/// </summary>
		public static void Invoke(Action action)
        {
            if (Dispatcher.CheckAccess()) action.Invoke();
            else Dispatcher.BeginInvoke(action);
        }

		public static void Invoke<T>(Action<T> action, T obj)
		{
			if (Dispatcher.CheckAccess()) action.Invoke(obj);
            else Dispatcher.BeginInvoke(action, obj);
        }

        public static void InvokeAsync(DispatcherPriority priority, Action action)
        {
            Dispatcher.InvokeAsync(action, priority);
        }

        public static void InvokeAsync(Action action)
        {
            InvokeAsync(DispatcherPriority.Normal, action);
        }

        /// <summary>
        ///   Invokes the given action on the UI thread - if the current thread is the UI thread this will just invoke the action directly on
        ///   the current thread so it can be safely called without the calling method being aware of which thread it is on.
        /// </summary>
        public static object InvokeFunc(Func<object> action)
        {
            if (Dispatcher.CheckAccess())
                return action.Invoke();

            var disp = Dispatcher.BeginInvoke(action);
            disp.Wait();
            return disp.Result;
        }

        public static void Wait(int miliseconds, Action afterAction, bool invokeInUI = true)
        {
            new Thread(() =>
            {
                Thread.Sleep(miliseconds);
                if (invokeInUI) Invoke(afterAction);
                else afterAction();
            }).Start();
        }

		public static Task<TaskScheduler> GetScheduler(Dispatcher d)
		{
			var schedulerResult = new TaskCompletionSource<TaskScheduler>();
            d.BeginInvoke(new Action(() => schedulerResult.SetResult(TaskScheduler.FromCurrentSynchronizationContext())));
			return schedulerResult.Task;
		}

        public static void RunInStaThread(this Action action)
        {
            var thread = new Thread(() => action());
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }
    }
}
