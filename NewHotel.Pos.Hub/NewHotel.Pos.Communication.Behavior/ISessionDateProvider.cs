﻿using System;

namespace NewHotel.Pos.Communication.Behavior
{
    public interface ISessionDataProvider
    {
        Guid Token { get; }
        string Version { get; }
        string StandWorkDate { get; }
    }
}