﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using NewHotel.Pos.Hub.Core;
using NewHotel.Pos.IoC;

namespace NewHotel.Pos.Communication.Behavior
{
    public class RequestContextClientBehavior : BehaviorExtensionElement, IClientMessageInspector, IEndpointBehavior
    {
        #region IClientMessageInspector Members

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            string token = HubContextClientBehavior.NEWHOTEL_HUB_SESSION_TOKEN;
            string version = HubContextClientBehavior.NEWHOTEL_HUB_CLIENT_VERSION;
            string workdate = HubContextClientBehavior.NEWHOTEL_HUB_CLIENT_WORKDATE;

            foreach (var headerName in new[] { token, version, workdate })
            {
                int pos = request.Headers.FindHeader(headerName, string.Empty);
                if (pos >= 0)
                    request.Headers.RemoveAt(pos);
            }

            var sessionData = CwFactory.Resolve<ISessionDataProvider>();
            request.Headers.Add(MessageHeader.CreateHeader(token, string.Empty, sessionData.Token));
            request.Headers.Add(MessageHeader.CreateHeader(version, string.Empty, sessionData.Version));
            request.Headers.Add(MessageHeader.CreateHeader(workdate, string.Empty, sessionData.StandWorkDate));

            return null;
        }

        #endregion
        #region IEndpointBehavior Members

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new RequestContextClientBehavior());
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }

        #endregion
        #region BehaviorExtensionElement Implementation

        protected override object CreateBehavior()
        {
            return new RequestContextClientBehavior();
        }

        public override Type BehaviorType => typeof(RequestContextClientBehavior);

        #endregion
    }
}