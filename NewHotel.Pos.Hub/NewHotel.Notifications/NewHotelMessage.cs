﻿using System;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Notifications
{
    public partial class NewHotelMessage
    {
        public NewHotelMessage()
        {
            Id = Guid.NewGuid();
            Fired = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Utc);
        }

        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public string Reply { get; set; }
        [DataMember]
        public DateTime Fired { get; set; }
        [DataMember]
        public Guid InstallationId { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendFormat("Id: {0}, ", Id);
            sb.AppendFormat("Source: {0}, ", Source);
            sb.AppendFormat("Reply: {0}, ", Reply);
            sb.AppendFormat("Fired: {0}", Fired.ToString());
            sb.AppendLine();

            return sb.ToString();
        }
    }
}