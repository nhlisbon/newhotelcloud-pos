﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Notifications
{

    public sealed class ReservationsRange : NewHotelMessage
    {
        [DataMember]
        public long ReservationApplication { get; set; }
        [DataMember]
        public DateTime? ReservationArrivalDate { get; set; }
        [DataMember]
        public DateTime? ReservationDepartureDate { get; set; }
    }
}