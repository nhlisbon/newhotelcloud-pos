﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Notifications
{
    public enum NotifyOperationType : long
    {
        OPTYPE_FILEEXPORT = 1
    };

    public sealed class InterfaceMessage : NewHotelMessage
    {
        [DataMember]
        public NotifyOperationType OperationNotify { get; set; }
        [DataMember]
        public bool Done { get; set; }
        [DataMember]
        public string Extension { get; set; }
        [DataMember]
        public string Reservation { get; set; }
        [DataMember]
        public DateTime? ReservationArrivalDate { get; set; }
        [DataMember]
        public DateTime? ReservationDepartureDate { get; set; }
        [DataMember]
        public string ReservationLanguage { get; set; }
        [DataMember]
        public string HolderFirstName { get; set; }
        [DataMember]
        public string HolderLastName { get; set; }
        [DataMember]
        public string OldRoom { get; set; }
        [DataMember]
        public string NewRoom { get; set; }
        [DataMember]
        public string Room { get; set; }
        [DataMember]
        public int KeyAmount { get; set; }
        [DataMember]
        public string KeyPermissions { get; set; }
        [DataMember]
        public string Workstation { get; set; }
        [DataMember]
        public DateTime? OperationInitialDate { get; set; }
        [DataMember]
        public DateTime? OperationFinalDate { get; set; }
        [DataMember]
        public long OperationDevice { get; set; }
    }
}