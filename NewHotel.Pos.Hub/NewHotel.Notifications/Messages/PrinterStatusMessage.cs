﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Notifications
{
    public partial class NewHotelMessage
    {
        public const string FiscalPrinterStatus = "FiscalPrinter.Status";
    }

    [Flags]
    public enum ESupportedDocument
    {
        None = 0,
        NonFiscal = 1,
        Invoice = 2,
        Ticket = 4,
        CreditNote = 8,
        DebitNote = 16,
        Void = 32,
        All = 63
    }

    public enum EPrinterStatus
    {
        Offline = 0,
        Ready = 1,
        Busy = 2,
        Error = 3
    }

    public sealed class PrinterStatusMessage : NewHotelMessage
    {
        public PrinterStatusMessage()
        {
            Status = EPrinterStatus.Ready;
            SupportedDocuments = ESupportedDocument.NonFiscal;
            LastUpdated = DateTime.UtcNow;
        }

        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public ESupportedDocument SupportedDocuments { get; set; }
        [DataMember]
        public EPrinterStatus Status { get; set; }
        [DataMember]
        public bool MemoryNearFull { get; set; }
        [DataMember]
        public bool PaperNearEnd { get; set; }
        [DataMember]
        public string ErrorMessage { get; set; }
        [DataMember]
        public DateTime LastUpdated { get; set; }
    }
}