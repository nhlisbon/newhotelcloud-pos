﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Notifications
{

    public sealed class BookingMessage : NewHotelMessage
    {
        [DataMember]
        public Guid ReservationId { get; set; }
        [DataMember]
        public string Reservation { get; set; }

        #region Old Values

        [DataMember]
        public DateTime? OldArrivalDate { get; set; }
        [DataMember]
        public DateTime? OldDepartureDate { get; set; }
        [DataMember]
        public Guid? OldReservedRoomTypeId { get; set; }
        [DataMember]
        public Guid? OldOccupiedRoomTypeId { get; set; }
        [DataMember]
        public long? OldReservationState { get; set; }
        [DataMember]
        public long? OldConfirmationState { get; set; }

        #endregion

        #region New Values

        [DataMember]
        public DateTime? NewArrivalDate { get; set; }
        [DataMember]
        public DateTime? NewDepartureDate { get; set; }
        [DataMember]
        public Guid? NewReservedRoomTypeId { get; set; }
        [DataMember]
        public Guid? NewOccupiedRoomTypeId { get; set; }
        [DataMember]
        public long? NewReservationState { get; set; }
        [DataMember]
        public long? NewConfirmationState { get; set; }

        #endregion

        [DataMember]
        public bool GuestsChanged { get; set; }
    }
}