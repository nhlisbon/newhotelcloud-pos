﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Notifications
{
    public abstract partial class NewHotelMessage
    {
        public const string InvoiceCreated = "Invoice.Created";
        public const string TicketCreated = "Ticket.Created";
        public const string CreditNoteCreated = "CreditNote.Created";
        public const string DebitNoteCreated = "DebitNote.Created";
        public const string InvoiceCancelled = "Invoice.Cancelled";
        public const string TicketCancelled = "Ticket.Cancelled";
        public const string CreditNoteCancelled = "CreditNote.Cancelled";
        public const string DebitNoteCancelled = "DebitNote.Cancelled";
        public const string InterfacesNotify = "Interfaces.Notify";
        public const string ReservationsRange = "Reservations.Range";
        public const string BookingCreatedOrUpdated = "Booking.CreatedOrUpdated";
    }

    public abstract class DocumentMessage : NewHotelMessage
    {
        [DataMember]
        public Guid DocumentId { get; set; }
        [DataMember]
        public string Serie { get; set; }
        [DataMember]
        public long Number { get; set; }
        [DataMember]
        public DateTime EmissionDate { get; set; }
        [DataMember]
        public bool Fiscalized { get; set; }
    }

    public sealed class InvoiceCreatedMessage : DocumentMessage
    {
    }

    public sealed class TicketCreatedMessage : DocumentMessage
    {
    }

    public sealed class CreditNoteCreatedMessage : DocumentMessage
    {
        [DataMember]
        public Guid? RefDocumentId { get; set; }
        [DataMember]
        public string RefSerie { get; set; }
        [DataMember]
        public long? RefNumber { get; set; }
    }

    public sealed class DebitNoteCreatedMessage : DocumentMessage
    {
        [DataMember]
        public Guid? RefDocumentId { get; set; }
        [DataMember]
        public string RefSerie { get; set; }
        [DataMember]
        public long? RefNumber { get; set; }
    }

    public sealed class DocumentCancelledMessage : DocumentMessage
    {
        [DataMember]
        public DateTime CancellationDate { get; set; }
        [DataMember]
        public string Reason { get; set; }
    }
}