﻿namespace NewHotel.Pos.Localization
{
    public static class Ext
    {
        public static string Translate(this string text, LocalizedSuffix suffix = LocalizedSuffix.Label, params object[] args)
        {
            string newLine = "";
            var list = text.Split('~');
            bool isKey = list.Length == 1;
            for (int i = 0; i < list.Length; i++)
            {
                if (isKey)
                {
                    string id = list[i];
                    newLine += !string.IsNullOrEmpty(id)
                        ? LocalizationMgr.Translation(id, suffix, args)
                        : id;
                }
                else newLine += list[i];
                isKey = !isKey;
            }
            return newLine;
        }

        public static string TranslateMsg(this string text, params object[] args)
        {
            return text.Translate(LocalizedSuffix.Message, args);
        }

        public static string TranslateCol(this string text, params object[] args)
        {
            return text.Translate(LocalizedSuffix.Column, args);
        }
    }

    public enum LocalizedSuffix
    {
        Label,
        Column,
        Message,
    }
}