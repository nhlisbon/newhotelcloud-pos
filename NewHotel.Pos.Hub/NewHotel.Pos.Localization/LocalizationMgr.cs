﻿using System.Globalization;
using System.Threading;
using NewHotel.Localization;

namespace NewHotel.Pos.Localization
{
    public static class LocalizationMgr
    {
        private static CultureInfo CurrentCulture => Thread.CurrentThread.CurrentUICulture;

        public static string Translation(CultureInfo ci, string id, LocalizedSuffix suffix, params object[] args)
        {
            return new LocalizedText(ci, id, (NewHotel.Localization.LocalizedSuffix) suffix, args);
        }

        public static string Translation(string id, params object[] args)
        {
            return new LocalizedText(CurrentCulture, id, (NewHotel.Localization.LocalizedSuffix) LocalizedSuffix.Label, args);
        }

        public static string Translation(CultureInfo ci, string id)
        {
            return Translation(ci, id, LocalizedSuffix.Label);
        }

        public static string Translation(string text)
        {
            string newLine = "";
            var list = ((string)text).Split('~');
            bool isKey = list.Length == 1;
            for (int i = 0; i < list.Length; i++)
            {
                if (isKey)
                {
                    string id = list[i];
                    if (!string.IsNullOrEmpty(id))
                    {
                        LocalizedSuffix suffix = LocalizedSuffix.Label;
                        newLine += LocalizationMgr.Translation(id, suffix);
                    }
                    else newLine += id;
                }
                else newLine += list[i];
                isKey = !isKey;
            }
            return newLine;
        }

        public static string Translation(string id, LocalizedSuffix suffix)
        {
            return Translation(CurrentCulture, id, suffix);
        }

        public static string Translation(string id, LocalizedSuffix suffix, params object[] args)
        {
            return new LocalizedText(CurrentCulture, id, (NewHotel.Localization.LocalizedSuffix) suffix, args);
        }

        public static string TranslationMessage(string id)
        {
            return Translation(CurrentCulture, id, LocalizedSuffix.Message);
        }

        public static string TranslationMessage(string id, params object[] args)
        {
            return Translation(CurrentCulture, id, LocalizedSuffix.Message, args);
        }

        public static string TranslationMessage(long languageId, string id)
        {
            CultureInfo cultureInfo = CultureInfo.CreateSpecificCulture(GetCulture(languageId));
            return Translation(cultureInfo, id, LocalizedSuffix.Message);
        }

        public static string Translation(long languageId, string id)
        {
            string culture = GetCulture(languageId);
            return Translation(CultureInfo.CreateSpecificCulture(culture), id);
        }

        public static string GetCulture(long languageId)
        {
            string culture;
            switch (languageId)
            {
                case 1033:
                    culture = "en-US";
                    break;
                case 2070:
                    culture = "pt-PT";
                    break;
                case 1046:
                    culture = "pt-BR";
                    break;
                case 3082:
                    culture = "es-ES";
                    break;
                case 1036:
                    culture = "fr-FR";
                    break;
                case 1063:
                    culture = "lt-LT";
                    break;
                case 1049:
                    culture = "ru-RU";
                    break;
                case 1027:
                    culture = "ca-ES";
                    break;
                case 1031:
                    culture = "de-DE";
                    break;
                case 1062:
                    culture = "lv-LV";
                    break;
                case 2052:
                    culture = "zh-CN";
                    break;
                case 1040:
                    culture = "it-IT";
                    break;
                default:
                    culture = "";
                    break;
            };
            return culture;
        }

        public static void SetCultureInfo(long languageId)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(GetCulture(languageId), true);
        }
        
        public static void SetCultureInfo(string code)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(code, true);
        }
    }
}