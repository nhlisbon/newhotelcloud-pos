﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Media;
using System.Xml.Serialization;
using NewHotel.Pos.Spooler.Common;

namespace NewHotel.Pos.Spooler.Settings
{
    public class Communicator
    {
        public static ConfigModelResult GetData()
        {
            try
            {
                var client = new Communication.ServiceHandheldClient();
                var result = new ConfigModel();

                var callAreasResult = client.GetStandAreas();
                var callStandResult = client.GetAllStands();

                if (!callAreasResult.HasErrors)
                {
                    #region Load from HUB Database
                    var areas = callAreasResult.ItemSource;
                    foreach (var record in areas)
                    {
                        var model = new PrinterModel
                        {
                            StandId = record.StandId.ToString("N").ToUpper(),
                            AreaId = record.AreaId.ToString("N").ToUpper(),
                            Stand = record.StandDescription,
                            Area = record.AreaDescription,
                            Color = record.AreaColor.HasValue
                                ? new SolidColorBrush(Color.FromArgb(record.AreaColor.Value.A, record.AreaColor.Value.R, record.AreaColor.Value.G, record.AreaColor.Value.B))
                                : new SolidColorBrush(Colors.Black),
                            NumberOfCopies = record.NumberOfCopies,
                            LineSize = 14,
                            NoteLineSize = 9,
                            ActionLineSize = 20,
                            SaloonLineSize = 14,
                            SeatSeparatorSize = 14
                        };
                        model.Color.Freeze();
                        result.DispatchAreas.Add(model);
                    }
                    #endregion
                }
                else
                {
                    return new ConfigModelResult { Error = callAreasResult.ToString() };
                }

                if (!callStandResult.HasErrors)
                {
                    #region Load from HUB Database
                    var stands = callStandResult.ItemSource;
                    foreach (var record in stands)
                    {
                        var model = new PrinterModel
                        {
                            StandId = record.Id.ToString("N").ToUpper(),
                            Stand = record.Description,
                            Area = "Ticket",
                            Color = new SolidColorBrush(Colors.Black),
                            NumberOfCopies = 1,
                            LineSize = 14,
                            NoteLineSize = 9,
                            ActionLineSize = 20,
                            SaloonLineSize = 14,
                            SeatSeparatorSize = 14
                        };
                        model.Color.Freeze();
                        result.TicketPrinters.Add(model);
                    }
                    #endregion
                }
                else
                {
                    return new ConfigModelResult { Error = callStandResult.ToString() };
                }

                #region Load From File
                var xmlConfigResult = LoadFromConfig();
                if (!string.IsNullOrEmpty(xmlConfigResult.Error)) System.Windows.Forms.MessageBox.Show(xmlConfigResult.Error);

                var xmlConfig = xmlConfigResult.Model;
                foreach (var stand in xmlConfig.TicketPrinters)
                {
                    var hubStand = result.TicketPrinters.FirstOrDefault(x => x.StandId == stand.StandId);
                    if (hubStand == null) continue;
                    hubStand.PrinterDescription = stand.PrinterDescription;
                    hubStand.Server = stand.Server;
                    hubStand.MarginLeft = stand.MarginLeft;
                    hubStand.MarginRight = stand.MarginRight;
                    hubStand.NumberOfCopies = stand.NumberOfCopies;
                    hubStand.LineSize = stand.LineSize;
                    hubStand.NoteLineSize = stand.NoteLineSize;
                    hubStand.ActionLineSize = stand.ActionLineSize;
                    hubStand.SaloonLineSize = stand.SaloonLineSize;
                    hubStand.SeatSeparatorSize = stand.SeatSeparatorSize;
                }
                foreach (var area in xmlConfig.DispatchAreas)
                {
                    var hubArea = result.DispatchAreas.FirstOrDefault(x => x.StandId == area.StandId && x.AreaId == area.AreaId);
                    if (hubArea == null) continue;
                    hubArea.PrinterDescription = area.PrinterDescription;
                    hubArea.Server = area.Server;
                    hubArea.MarginLeft = area.MarginLeft;
                    hubArea.MarginRight = area.MarginRight;
                    hubArea.NumberOfCopies = area.NumberOfCopies;
                    hubArea.LineSize = area.LineSize;
                    hubArea.NoteLineSize = area.NoteLineSize;
                    hubArea.ActionLineSize = area.ActionLineSize;
                    hubArea.SaloonLineSize = area.SaloonLineSize;
                    hubArea.SeatSeparatorSize = area.SeatSeparatorSize;
                }
                #endregion

                return new ConfigModelResult { Error = string.Empty, Model = result };
            }
            catch (Exception ex)
            {
                return new ConfigModelResult { Error = ex.Message + Environment.NewLine + ex.StackTrace };
            }
        }

        private static ConfigModelResult LoadFromConfig()
        {
            try
            {
                var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                var file = Path.Combine(path ?? string.Empty, "printers.xml");
                var serializer = new XmlSerializer(typeof(ConfigModel));
                var reader = new StreamReader(file);
                var result = serializer.Deserialize(reader) as ConfigModel;
                reader.Close();

                return result == null ? new ConfigModelResult { Model = new ConfigModel() } : new ConfigModelResult { Model = result };
            }
            catch(Exception ex)
            {
                return new ConfigModelResult { Error = ex.Message + Environment.NewLine + ex.StackTrace };
            }
        }

        public static void SaveToConfig(ConfigModel model)
        {
            try
            {
                var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                var file = Path.Combine(path ?? string.Empty, "printers.xml");
                var writer = new StreamWriter(file, false);
                var serializer = new XmlSerializer(typeof(ConfigModel));
                serializer.Serialize(writer, model);
                writer.Close();
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }
}