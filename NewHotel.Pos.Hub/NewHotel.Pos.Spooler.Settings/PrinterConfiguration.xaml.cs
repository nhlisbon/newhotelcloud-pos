﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Printing;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using NewHotel.Pos.Spooler.Common;

namespace NewHotel.Pos.Spooler.Settings
{
    /// <summary>
    /// Interaction logic for PrinterConfiguration.xaml
    /// </summary>
    public partial class PrinterConfiguration : UserControl
    {
        public PrinterModel ViewModel { get { return DataContext as PrinterModel; } }

        public PrinterConfiguration()
        {
            InitializeComponent();
            

            DataContextChanged += (s, ev) =>
            {
                if (ViewModel != null)
                {
                    if (!string.IsNullOrEmpty(ViewModel.Server)) RefreshServers();

                    ViewModel.PropertyChanged += (s1, ev1) =>
                    {
                        if (ev1.PropertyName == "Server" && !String.IsNullOrEmpty(ViewModel.Server)) RefreshServers();
                    };
                }
            };
        }
        
        private void btnPrinters_Click(object sender, RoutedEventArgs e)
        {
            RefreshServers();
        }

        public void RefreshServers()
        {
            try
            {
                var server = new PrintServer(ViewModel.Server);
                var printers = new ObservableCollection<string>(
                    server.GetPrintQueues(new EnumeratedPrintQueueTypes[] { EnumeratedPrintQueueTypes.Local }).Select(x => x.Name)
                    .Concat(server.GetPrintQueues(new EnumeratedPrintQueueTypes[] { EnumeratedPrintQueueTypes.Connections }).Select(x => x.Description)));
                ViewModel.Printers = printers;
            }
            catch (Exception ex) { string asd = ex.Message; }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex(@"^[1-9]\d*$");
            var newText = (sender as TextBox).Text.Insert((sender as TextBox).CaretIndex, e.Text);
            e.Handled = !regex.IsMatch(newText);
        }
    }
}