﻿using System;
using System.ComponentModel;
using System.Windows;
using NewHotel.Pos.Spooler.Common;

namespace NewHotel.Pos.Spooler.Settings
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public BackgroundWorker Worker { get; set; }
        public Communicator Proxy { get; set; }
        public ConfigModel ViewModel { get { return DataContext as ConfigModel; } }
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new ConfigModel();
            Proxy = new Communicator();
            Worker = new BackgroundWorker();
            Worker.DoWork += Worker_DoWork;
            Worker.WorkerReportsProgress = true;
            Worker.RunWorkerCompleted += Worker_RunWorkerCompleted;
            Worker.ProgressChanged += Worker_ProgressChanged;
            Loaded += (s, ev) =>
            {
                busyCtrl.Show("Getting information...");
                Worker.RunWorkerAsync();
            };
            Closing += (s, ev) =>
            {
                Communicator.SaveToConfig(ViewModel);
            };
        }
        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            System.Windows.Forms.MessageBox.Show(e.UserState.ToString());
        }
        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                ConfigModelResult result = Communicator.GetData();
                if (!string.IsNullOrEmpty(result.Error))
                {
                    Worker.ReportProgress(0, result.Error);
                }

                ConfigModel xmlConfig = result.Model;
                e.Result = xmlConfig;
            }
            catch (Exception ex)
            {
                string error = ex.Message + Environment.NewLine + ex.StackTrace;
                Worker.ReportProgress(0, error);
            }
        }
        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            busyCtrl.Close();
            ConfigModel xmlConfig = e.Result as ConfigModel;
            DataContext = xmlConfig;

            //ViewModel.TicketPrinter = xmlConfig.TicketPrinter;
            //ViewModel.DispatchAreas.Clear();
            //foreach (PrinterModel printer in xmlConfig.DispatchAreas)
            //{
            //    ViewModel.DispatchAreas.Add(printer);
            //}
        }
    }
}
