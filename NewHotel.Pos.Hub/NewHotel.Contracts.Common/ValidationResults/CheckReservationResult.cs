﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [Flags]
    public enum ReservationCheckFlags { None = 0, Availability = 1, ExtraAllotment = 2 };

    [DataContract]
	[Serializable]
    public class CheckReservationResult : ValidationResult
    {
        [DataMember]
        public ReservationCheckFlags Flags { get; internal set; }
        [DataMember]
        public Guid? AllotmentId { get; internal set; }
        [DataMember]
        public bool AllotmentIncluded { get; internal set; }
        [DataMember]
        public Guid? OccupedRoomTypeId { get; internal set; }
        [DataMember]
        public DateTime CreationDate { get; internal set; }

        public CheckReservationResult(bool checkAvailability, bool checkExtraAllotment,
            Guid? allotmentId, bool allotmentIncluded, Guid? occupedRoomTypeId, DateTime creationDate)
            : base()
        {
            Flags = ReservationCheckFlags.None;
            if (checkAvailability)
                Flags = Flags | ReservationCheckFlags.Availability;
            if (checkExtraAllotment)
                Flags = Flags | ReservationCheckFlags.ExtraAllotment;
            AllotmentId = allotmentId;
            AllotmentIncluded = allotmentIncluded;
            OccupedRoomTypeId = occupedRoomTypeId;
            CreationDate = creationDate;
        }
    }
}
