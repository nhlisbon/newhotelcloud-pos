﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PersistPriceRateResult : ValidationResult
    {
        #region Properties

        [DataMember]
        public Guid PriceRateId { get; internal set; }

        [DataMember]
        public Guid? RoomServiceByDepartmentId { get; set; }
        [DataMember]
        public Guid? ExtraBedServiceByDepartmentId { get; set; }
        [DataMember]
        public Guid? BreakfastServiceByDepartmentId { get; set; }
        [DataMember]
        public Guid? BreakfastDrinkServiceByDepartmentId { get; set; }
        [DataMember]
        public Guid? LunchServiceByDepartmentId { get; set; }
        [DataMember]
        public Guid? LunchDrinkServiceByDepartmentId { get; set; }
        [DataMember]
        public Guid? DinnerServiceByDepartmentId { get; set; }
        [DataMember]
        public Guid? DinnerDrinkServiceByDepartmentId { get; set; }

        [DataMember]
        public decimal FoodBreakfastAdultsPrice { get; set; }
        [DataMember]
        public decimal DrinkBreakfastAdultsPrice { get; set; }
        [DataMember]
        public decimal FoodLunchAdultsPrice { get; set; }
        [DataMember]
        public decimal DrinkLunchAdultsPrice { get; set; }
        [DataMember]
        public decimal FoodDinnerAdultsPrice { get; set; }
        [DataMember]
        public decimal DrinkDinnerAdultsPrice { get; set; }
        [DataMember]
        public decimal AllInclusiveAdultsPrice { get; set; }
        [DataMember]
        public decimal FoodBreakfastChildrenPrice { get; set; }
        [DataMember]
        public decimal DrinkBreakfastChildrenPrice { get; set; }
        [DataMember]
        public decimal FoodLunchChildrenPrice { get; set; }
        [DataMember]
        public decimal DrinkLunchChildrenPrice { get; set; }
        [DataMember]
        public decimal FoodDinnerChildrenPrice { get; set; }
        [DataMember]
        public decimal DrinkDinnerChildrenPrice { get; set; }
        [DataMember]
        public decimal AllInclusiveChildrenPrice { get; set; }

        #region Bed + Breakfast

        [DataMember]
        public decimal FoodBreakfastAdultsBBPrice { get; set; }
        [DataMember]
        public decimal DrinkBreakfastAdultsBBPrice { get; set; }
        [DataMember]
        public decimal FoodBreakfastChildrenBBPrice { get; set; }
        [DataMember]
        public decimal DrinkBreakfastChildrenBBPrice { get; set; }

        #endregion
        #region Full Board

        [DataMember]
        public decimal FoodBreakfastAdultsFBPrice { get; set; }
        [DataMember]
        public decimal DrinkBreakfastAdultsFBPrice { get; set; }
        [DataMember]
        public decimal FoodLunchAdultsFBPrice { get; set; }
        [DataMember]
        public decimal DrinkLunchAdultsFBPrice { get; set; }
        [DataMember]
        public decimal FoodDinnerAdultsFBPrice { get; set; }
        [DataMember]
        public decimal DrinkDinnerAdultsFBPrice { get; set; }
        [DataMember]
        public decimal FoodBreakfastChildrenFBPrice { get; set; }
        [DataMember]
        public decimal DrinkBreakfastChildrenFBPrice { get; set; }
        [DataMember]
        public decimal FoodLunchChildrenFBPrice { get; set; }
        [DataMember]
        public decimal DrinkLunchChildrenFBPrice { get; set; }
        [DataMember]
        public decimal FoodDinnerChildrenFBPrice { get; set; }
        [DataMember]
        public decimal DrinkDinnerChildrenFBPrice { get; set; }

        #endregion
        #region All Inclusive

        [DataMember]
        public decimal FoodBreakfastAdultsAIPrice { get; set; }
        [DataMember]
        public decimal DrinkBreakfastAdultsAIPrice { get; set; }
        [DataMember]
        public decimal FoodLunchAdultsAIPrice { get; set; }
        [DataMember]
        public decimal DrinkLunchAdultsAIPrice { get; set; }
        [DataMember]
        public decimal FoodDinnerAdultsAIPrice { get; set; }
        [DataMember]
        public decimal DrinkDinnerAdultsAIPrice { get; set; }
        [DataMember]
        public decimal FoodBreakfastChildrenAIPrice { get; set; }
        [DataMember]
        public decimal DrinkBreakfastChildrenAIPrice { get; set; }
        [DataMember]
        public decimal FoodLunchChildrenAIPrice { get; set; }
        [DataMember]
        public decimal DrinkLunchChildrenAIPrice { get; set; }
        [DataMember]
        public decimal FoodDinnerChildrenAIPrice { get; set; }
        [DataMember]
        public decimal DrinkDinnerChildrenAIPrice { get; set; }

        #endregion

        [DataMember]
        public long AffectedReservations { get; set; }

        #endregion

        public PersistPriceRateResult(Guid priceRateId)
            : base()
        {
            PriceRateId = priceRateId;
        }
    }
}
