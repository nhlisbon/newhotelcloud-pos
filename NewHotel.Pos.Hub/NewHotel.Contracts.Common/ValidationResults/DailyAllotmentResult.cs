﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class DailyAllotmentResult : ValidationResult
    {
        [DataMember]
        public readonly TypedList<AllotmentInfoContract> DailyValues;

        public DailyAllotmentResult()
            : base() { DailyValues = new TypedList<AllotmentInfoContract>(); }

        public DailyAllotmentResult(AllotmentInfoContract[] dailyValues)
            : base() { DailyValues = new TypedList<AllotmentInfoContract>(dailyValues); }
    }  
}
