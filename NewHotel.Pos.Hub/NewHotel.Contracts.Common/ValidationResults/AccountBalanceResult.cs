﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{

    [DataContract]
	[Serializable]
    public class AccountBalanceResult : ValidationResult
    {
        [DataMember]
        public readonly decimal Balance;

        public AccountBalanceResult(decimal balance)
            : base()
        {
            Balance = balance;
        }
    }

}
