﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ValidationStringResult : ValidationResult
    {
        public ValidationStringResult() { }
        public ValidationStringResult(string description) { Description = description; }
        public ValidationStringResult(Exception ex) : base(ex) { }

        [DataMember]
        public string Description { get; set; }

        public void AddException(Exception ex)
        {
            ValidationResult result = new ValidationResult(ex);
            this.Add(result);
        }
    }
}
