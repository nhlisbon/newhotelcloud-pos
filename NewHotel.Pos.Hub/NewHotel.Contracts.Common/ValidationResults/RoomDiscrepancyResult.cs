﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RoomDiscrepancyResult : ValidationContractResult
    {
        [DataMember]
        public TypedList<RoomDiscrepancyContract> RoomDiscrepancies { get; internal set; }

        public RoomDiscrepancyResult(IEnumerable<RoomDiscrepancyContract> discrepancies)
            : base()
        {
            RoomDiscrepancies = new TypedList<RoomDiscrepancyContract>(discrepancies);
        }

        public RoomDiscrepancyResult()
            : base()
        {
            RoomDiscrepancies = new TypedList<RoomDiscrepancyContract>();
        }
    }
}
