﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
	[DataContract]
	[Serializable]
	public class PersistCreditNoteResult : ValidationContractResult
	{
        [DataMember]
        public Guid? CreditNoteId { get; set; }
        [DataMember]
        public string Signature { get; set; }
        [DataMember]
        public string Serie { get; set; }
        [DataMember]
        public CreditCardPaymentResult[] Refunds { get; set; }

        public string Receipt
        {
            get
            {
                if (Refunds != null && Refunds.Length > 0)
                    return string.Join(Environment.NewLine, Refunds.Select(x => x.Receipt));

                return string.Empty;
            }
        }

		public PersistCreditNoteResult(BaseContract contract)
			: base(contract) { }
	}

    [DataContract]
    [Serializable]
    public class PersistDebitNoteResult : ValidationContractResult
    {
        [DataMember]
        public Guid? DebitNoteId { get; set; }
        [DataMember]
        public string Signature { get; set; }
        [DataMember]
        public string Serie { get; set; }
        [DataMember]
        public CreditCardPaymentResult[] Payments { get; set; }
        //[DataMember]
        //public bool FiscalPrinter { get; set; }
        //[DataMember]
        //public FiscalDeviceRecord[] Printers { get; set; }

        public string Receipt
        {
            get
            {
                if (Payments != null && Payments.Length > 0)
                    return string.Join(Environment.NewLine, Payments.Select(x => x.Receipt));

                return string.Empty;
            }
        }

        public PersistDebitNoteResult(BaseContract contract)
            : base(contract) { }
    }
}
