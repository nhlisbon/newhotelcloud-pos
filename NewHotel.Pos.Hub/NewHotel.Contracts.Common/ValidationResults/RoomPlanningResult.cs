﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RoomPlanningResult : ValidationResult
    {
        #region Parameters

        [DataMember]
        public DateTime WorkDate { get; internal set; }
        [DataMember]
        public DateTime FromDate { get; internal set; }
        [DataMember]
        public DateTime ToDate { get; internal set; }
        [DataMember]
        public DateTime MinDate { get; internal set; }
        [DataMember]
        public DateTime MaxDate { get; internal set; }
        [DataMember]
        public short Period { get; internal set; }
        [DataMember]
        public short? DaysShowedBeforeWorkingDate { get; internal set; }
        [DataMember]
        public ARGBColor BackgroundColorMonday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorTuesday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorWednesday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorThursday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorFriday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorSaturday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorSunday { get; set; }
        [DataMember]
        public ARGBColor ColorWorkingDateInPlannings { get; internal set; }
        [DataMember]
        public ARGBColor ColorAlarmInPlannings { get; internal set; }
        [DataMember]
        public ARGBColor ColorWarningInPlannings { get; internal set; }
        [DataMember]
        public RoomStatusType RoomStatusUsed { get; internal set; }
        [DataMember]
        public IDictionary<DateTime, ValuePair<ARGBColor, string>> SpecialDays { get; internal set; }

        #endregion
        #region Properties

        [DataMember]
        public AssignedRoomReservationRecord[] AssignedRooms { get; set; }
        [DataMember]
        public AssignedRoomReservationRecord[] AssignedVirtualRooms { get; set; }
        [DataMember]
        public UnAssignedRoomReservationRecord[] UnAssignedRooms { get; set; }
        [DataMember]
        public TotalRoomReservationPlanningContract[] TotalRooms { get; set; }
        [DataMember]
        public TotalSummaryRoomPlanningContract[] NewTotalRooms { get; set; }
        [DataMember]
        public RoomInactivityRecord[] Inactivities { get; set; }

        #endregion
        #region Constructors

        private RoomPlanningResult(RoomStatusType roomStatusUsed)
            : base()
        {
            RoomStatusUsed = roomStatusUsed;
            SpecialDays = new Dictionary<DateTime, ValuePair<ARGBColor, string>>();
        }

        public RoomPlanningResult(DateTime workDate,
            DateTime fromDate, DateTime toDate,
            DateTime minDate, DateTime maxDate,
            short period, short? daysShowedBeforeWorkingDate,
            ARGBColor backgroundColorMonday, ARGBColor backgroundColorTuesday, ARGBColor backgroundColorWednesday,
            ARGBColor backgroundColorThursday, ARGBColor backgroundColorFriday, ARGBColor backgroundColorSaturday,
            ARGBColor backgroundColorSunday, 
            ARGBColor colorWorkingDateInPlannings, ARGBColor colorAlarmInPlannings,
            ARGBColor colorWarningInPlannings, RoomStatusType roomStatusUsed)
            : this(roomStatusUsed)
        {
            WorkDate = workDate;
            FromDate = fromDate;
            ToDate = toDate;
            MinDate = minDate;
            MaxDate = maxDate;
            Period = period;
            DaysShowedBeforeWorkingDate = daysShowedBeforeWorkingDate;

            BackgroundColorMonday = backgroundColorMonday;
            BackgroundColorTuesday= backgroundColorTuesday;
            BackgroundColorWednesday = backgroundColorWednesday;
            BackgroundColorThursday = backgroundColorThursday;
            BackgroundColorFriday = backgroundColorFriday;
            BackgroundColorSaturday = backgroundColorSaturday;
            BackgroundColorSunday = backgroundColorSunday;
            
            ColorWorkingDateInPlannings = colorWorkingDateInPlannings;
            ColorAlarmInPlannings = colorAlarmInPlannings;
            ColorWarningInPlannings = colorWarningInPlannings;
        }

        public RoomPlanningResult()
            : this(RoomStatusType.None)
        {
        }

        #endregion
    }
}