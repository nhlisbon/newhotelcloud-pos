﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PersistOccupationLineListResult : ValidationResult
    {
        [DataMember]
        public List<PersistOccupationLineResult> OccupationLineList { get; set; }
        [DataMember]
        public AvailabilityResult AvailabilityResult { get; set; }

        public PersistOccupationLineListResult()
            : base()
        {
            OccupationLineList = new List<PersistOccupationLineResult>();
            AvailabilityResult = new AvailabilityResult();
        }

        public bool HasListErrors
        {
           get { return OccupationLineList.Any(x => x.HasErrors) || AvailabilityResult.HasErrors; }
        }
    }
}
