﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PaymentOrderFileResult : ValidationResult
    {
        [DataMember]
        public readonly byte[] Buffer;

        public PaymentOrderFileResult(ValidationResult result, byte[] buffer)
            : base(result)
        {
            Buffer = buffer;
        }
    }
}
