﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ReservationGroupPlanningResult : ValidationResult
    {
        #region Parameters

        [DataMember]
        public DateTime WorkDate { get; internal set; }
        [DataMember]
        public DateTime FromDate { get; internal set; }
        [DataMember]
        public DateTime ToDate { get; internal set; }
        [DataMember]
        public DateTime MinDate { get; internal set; }
        [DataMember]
        public DateTime MaxDate { get; internal set; }
        [DataMember]
        public short Period { get; internal set; }
        [DataMember]
        public short? DaysShowedBeforeWorkingDate { get; internal set; }
        [DataMember]
        public ARGBColor BackgroundColorMonday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorTuesday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorWednesday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorThursday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorFriday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorSaturday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorSunday { get; set; }
        [DataMember]
        public ARGBColor ColorGroupInPlannings { get; internal set; }
        [DataMember]
        public ARGBColor ColorWorkingDateInPlannings { get; internal set; }
        [DataMember]
        public ARGBColor ColorAlarmInPlannings { get; internal set; }
        [DataMember]
        public ARGBColor ColorWarningInPlannings { get; internal set; }
        [DataMember]
        public IDictionary<DateTime, ValuePair<ARGBColor, string>> SpecialDays { get; internal set; }

        #endregion
        #region Properties

        [DataMember]
        public ReservationsGroupsRecord[] Groups { get; set; }
        [DataMember]
        public TotalRoomReservationPlanningContract[] TotalRooms { get; set; }

        #endregion
        #region Constructors

        public ReservationGroupPlanningResult(DateTime workDate,
            DateTime fromDate, DateTime toDate,
            DateTime minDate, DateTime maxDate,
            short period, short? daysShowedBeforeWorkingDate,
            ARGBColor backgroundColorMonday, ARGBColor backgroundColorTuesday, ARGBColor backgroundColorWednesday,
            ARGBColor backgroundColorThursday, ARGBColor backgroundColorFriday, ARGBColor backgroundColorSaturday,
            ARGBColor backgroundColorSunday, ARGBColor colorGroupInPlannings,
            ARGBColor colorWorkingDateInPlannings, ARGBColor colorAlarmInPlannings, ARGBColor colorWarningInPlannings)
        {
            WorkDate = workDate;
            FromDate = fromDate;
            ToDate = toDate;
            MinDate = minDate;
            MaxDate = maxDate;
            Period = period;
            DaysShowedBeforeWorkingDate = daysShowedBeforeWorkingDate;

            BackgroundColorMonday = backgroundColorMonday;
            BackgroundColorTuesday= backgroundColorTuesday;
            BackgroundColorWednesday = backgroundColorWednesday;
            BackgroundColorThursday = backgroundColorThursday;
            BackgroundColorFriday = backgroundColorFriday;
            BackgroundColorSaturday = backgroundColorSaturday;
            BackgroundColorSunday = backgroundColorSunday;

            ColorGroupInPlannings = colorGroupInPlannings;
            ColorWorkingDateInPlannings = colorWorkingDateInPlannings;
            ColorAlarmInPlannings = colorAlarmInPlannings;
            ColorWarningInPlannings = colorWarningInPlannings;
            SpecialDays = new Dictionary<DateTime, ValuePair<ARGBColor, string>>();
        }

        public ReservationGroupPlanningResult(IEnumerable<Validation> validations)
            : base(validations)
        {
        }

        public ReservationGroupPlanningResult()
            : base()
        {
        }

        #endregion
    }
}