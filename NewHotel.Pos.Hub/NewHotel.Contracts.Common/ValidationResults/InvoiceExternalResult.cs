﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
	[DataContract]
	[Serializable]
	public class InvoiceExternalResult : ValidationResult
	{
		[DataMember]
        public Guid? InvoiceId { get; set; }
        [DataMember]
		public string Invoice { get; set; }
        [DataMember]
        public DateTime EmissionDate { get; set; }
		[DataMember]
		public string Holder { get; set; }
        [DataMember]
		public string Address { get; set; }
        [DataMember]
        public string FiscalNumber { get; set; }
        [DataMember]
		public string Signature { get; set; }
        [DataMember]
        public string FactSerie { get; set; }
        [DataMember]
        public long? FactNumber { get; set; }
        [DataMember]
        public DateTime? FactDate { get; set; }
        [DataMember]
        public DateTime? FactSysDateTime { get; set; }
        [DataMember]
        public string PaymentReceipt { get; set; }
	}
}