﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class CheckExtraAllotmentResult : ValidationResult
    {
        #region Members

        [DataMember]
        public string Reservation { get; internal set; }
        [DataMember]
        public bool AllotmentIncluded { get; set; }

        public bool ErrorOtherThanOutOfRelease
        {
            get
            {
                if (!IsEmpty)
                {
                    foreach (var item in Errors)
                    {
                        if (item.Code != Res.ReservationOutOfRelease)
                            return true;
                    }
                }

                return false;
            }
        }

        #endregion
        #region Constructor

        public CheckExtraAllotmentResult(string reservation, bool allotmentIncluded)
        {
            Reservation = reservation;
            AllotmentIncluded = allotmentIncluded;
        }

        public CheckExtraAllotmentResult(string reservation, bool allotmentIncluded, IEnumerable<Validation> collection)
            : base(collection)
        {
            Reservation = reservation;
            AllotmentIncluded = allotmentIncluded;
        }

        #endregion
    }
}