﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
	[DataContract]
	[Serializable]
	public class CreditCardPaymentResult : ValidationResult
    {
        [DataMember]
        public Guid CreditCardId { get; set; }
        [DataMember]
        public string CreditCardNumber { get; set; }
        [DataMember]
        public Guid? CreditCardTypeId { get; set; }
        [DataMember]
        public string CreditCardTypeDescription { get; set; }

        [DataMember]
        public short? ValidationMonth { get; set; }
        [DataMember]
        public short? ValidationYear { get; set; }
        [DataMember]
        public string ValidationCode { get; set; }
        [DataMember]
        public string HolderName { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string ZipCode { get; set; }

        [DataMember]
		public Guid TransactionId { get; set; }
		[DataMember]
		public string Receipt { get; set; }
        [DataMember]
        public string Reference { get; set; }
        [DataMember]
        public decimal Value { get; set; }
        [DataMember]
        public PaymentServiceProvider Provider { get; set; }
        [DataMember]
        public PaymentOperationStatus Status { get; set; }
        [DataMember]
        public string StatusMessage { get; set; }
        [DataMember]
        public string AuthMessage { get; set; }

        [DataMember]
        public string User { get; set; }

        public CreditCardPaymentResult()
            : base()
        {
            Provider = PaymentServiceProvider.Manual;
            Status = PaymentOperationStatus.Pending;
        }

        public CreditCardPaymentResult(Exception ex)
            : base(ex)
        {
            Provider = PaymentServiceProvider.Manual;
            Status = PaymentOperationStatus.Pending;
        }
    }
}