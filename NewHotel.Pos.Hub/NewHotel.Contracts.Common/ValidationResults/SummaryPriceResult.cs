﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SummarizedPriceResult : ValidationContractResult
    {
        [DataMember]
        public TypedList<MovementPriceSummarizedContract> SummarizedPrices { get; internal set; }

        public SummarizedPriceResult(IEnumerable<MovementPriceSummarizedContract> summarizedPrices)
            : base()
        {
            SummarizedPrices = new TypedList<MovementPriceSummarizedContract>(summarizedPrices);
        }

        public SummarizedPriceResult()
            : base()
        {
            SummarizedPrices = new TypedList<MovementPriceSummarizedContract>();
        }
    }
}