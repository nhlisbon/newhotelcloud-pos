﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class AllocationResult : ValidationResult
    {
        #region Properties

        [DataMember]
        public Guid? RoomServiceByDepartmentId { get; set; }
        [DataMember]
        public Guid? ExtraBedServiceByDepartmentId { get; set; }
        [DataMember]
        public Guid? BreakfastServiceByDepartmentId { get; set; }
        [DataMember]
        public Guid? BreakfastDrinkServiceByDepartmentId { get; set; }
        [DataMember]
        public Guid? LunchServiceByDepartmentId { get; set; }
        [DataMember]
        public Guid? LunchDrinkServiceByDepartmentId { get; set; }
        [DataMember]
        public Guid? DinnerServiceByDepartmentId { get; set; }
        [DataMember]
        public Guid? DinnerDrinkServiceByDepartmentId { get; set; }

        [DataMember]
        public decimal FoodBreakfastAdultsPrice { get; set; }
        [DataMember]
        public decimal DrinkBreakfastAdultsPrice { get; set; }
        [DataMember]
        public decimal FoodLunchAdultsPrice { get; set; }
        [DataMember]
        public decimal DrinkLunchAdultsPrice { get; set; }
        [DataMember]
        public decimal FoodDinnerAdultsPrice { get; set; }
        [DataMember]
        public decimal DrinkDinnerAdultsPrice { get; set; }
        [DataMember]
        public decimal FoodBreakfastChildrenPrice { get; set; }
        [DataMember]
        public decimal DrinkBreakfastChildrenPrice { get; set; }
        [DataMember]
        public decimal FoodLunchChildrenPrice { get; set; }
        [DataMember]
        public decimal DrinkLunchChildrenPrice { get; set; }
        [DataMember]
        public decimal FoodDinnerChildrenPrice { get; set; }
        [DataMember]
        public decimal DrinkDinnerChildrenPrice { get; set; }
        [DataMember]
        public long AffectedReservations { get; set; }

        #endregion

        public AllocationResult()
            : base() { }
    }
}