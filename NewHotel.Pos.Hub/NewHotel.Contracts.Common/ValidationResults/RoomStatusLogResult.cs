﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RoomStatusLogResult : ValidationContractResult
    {
        [DataMember]
        public TypedList<RoomStatusLogContract> RoomStatusLog { get; internal set; }

        public RoomStatusLogResult(IEnumerable<RoomStatusLogContract> roomstatuslog)
            : base()
        {
            RoomStatusLog = new TypedList<RoomStatusLogContract>(roomstatuslog);
        }

        public RoomStatusLogResult()
            : base()
        {
            RoomStatusLog = new TypedList<RoomStatusLogContract>();
        }
    }
}
