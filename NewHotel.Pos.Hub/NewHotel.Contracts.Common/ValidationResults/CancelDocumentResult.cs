﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
	[DataContract]
	[Serializable]
	public class CancelDocumentResult : ValidationResult
	{
		[DataMember]
		public string PaymentReceipt { get; set; }

		public CancelDocumentResult()
			: base() { }
	}
}