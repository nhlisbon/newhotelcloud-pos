﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ValidationObjectResult : ValidationResult
    {
        [DataMember]
        public Guid[] Item { get; set; }

        public ValidationObjectResult() { }

        public ValidationObjectResult(IEnumerable<Validation> validations)
            : base(validations) { }

        public ValidationObjectResult(Exception ex)
            : base(ex) { }
    }
}
