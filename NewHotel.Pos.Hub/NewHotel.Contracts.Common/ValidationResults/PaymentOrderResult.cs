﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PaymentOrderResult : ValidationResult
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public long Number { get; set; }

        public PaymentOrderResult(Guid id)
            : base()
        {
            Id = id;
        }
    }
}
