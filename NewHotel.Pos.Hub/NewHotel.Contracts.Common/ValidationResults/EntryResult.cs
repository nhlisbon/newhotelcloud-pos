﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
	[DataContract]
	[Serializable]
	public class EntryResult : ValidationResult
	{
		[DataMember]
		public string Receipts { get; set; }
        [DataMember]
        public CreditCardPaymentResult Payment { get; set; }

        public string PaymentReceipt
        {
            get
            {
                if (Payment != null)
                    return Payment.Receipt;

                return string.Empty;
            }
        }

        //[DataMember]
        //public string PaymentReceipt { get; set; }

        public EntryResult()
			: base() { }

		public EntryResult(IEnumerable<Validation> collection)
			: base(collection) { }
	}
}