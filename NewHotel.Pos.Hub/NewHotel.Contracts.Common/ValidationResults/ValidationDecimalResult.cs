﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ValidationDecimalResult : ValidationResult
    {
        public ValidationDecimalResult()
            :base()
        {

        }
        public ValidationDecimalResult(decimal amount)
            : base()
        {
            Amount = amount;
        }
        public ValidationDecimalResult(Exception ex)
            : base(ex)
        {

        }

        [DataMember]
        public decimal Amount { get; set; }
    }
}
