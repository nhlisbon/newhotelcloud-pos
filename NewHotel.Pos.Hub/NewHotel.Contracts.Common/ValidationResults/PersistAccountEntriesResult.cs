﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PersistAccountEntriesResult : ValidationResult
    {
        [DataMember]
        public Guid[] MovementsId { get; set; }
        [DataMember]
        public bool AllowMultiSchemaInvoicing { get; set; }
        [DataMember]
        public bool AllowAdvanceDepositInvoice { get; set; }
        [DataMember]
        public bool AllowInternalInvoice { get; set; }
        [DataMember]
        public bool AllowTicket { get; set; } = true;

        public PersistAccountEntriesResult()
            : base() { }

        public PersistAccountEntriesResult(IEnumerable<Validation> validations, Guid[] movementIds)
            : base(validations) { MovementsId = movementIds; }
    }  
}