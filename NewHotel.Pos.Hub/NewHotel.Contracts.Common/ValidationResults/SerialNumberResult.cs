﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SerialNumberResult : ValidationResult
    {
        [DataMember]
        public Guid? SerieId { get; set; }
        [DataMember]
        public long? DocumentNumber { get; set; }
        [DataMember]
        public string DocumentSerie { get; set; }
        [DataMember]
        public bool Fiscalization { get; set; }
        [DataMember]
        public string ValidationCode { get; set; }

        [ReflectionExclude]
        public string SerieName
        {
            get
            {
                var serieName = string.Empty;
                if (DocumentNumber.HasValue)
                    serieName += DocumentNumber.Value.ToString() + "/";
                serieName += DocumentSerie;

                return serieName;
            }
        }
    }
}