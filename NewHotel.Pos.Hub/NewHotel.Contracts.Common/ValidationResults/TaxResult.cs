﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Runtime.Serialization;

//namespace NewHotel.Contracts
//{
//    [DataContract]
//    public class TaxResult : ValidationResult
//    {
//        [DataMember]
//        public bool TaxIncluded { get; internal set; }
//        [DataMember]
//        public TaxSchemaContract TaxSchema { get; internal set; }
//        [DataMember]
//        public TaxContract TaxDetail { get; internal set; }

//        internal TaxResult()
//            : base() { }

//        public TaxResult(bool taxIncluded, TaxSchemaContract taxSchema, TaxContract taxDetail)
//            : base() 
//        {
//            TaxIncluded = taxIncluded;
//            TaxSchema = taxSchema;
//            TaxDetail = taxDetail;
//        }
//    }
//}
