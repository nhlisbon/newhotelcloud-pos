﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class DailyPricesResult : ValidationResult
    {
        [DataMember]
        public readonly TypedList<PriceRateInfoContract> DailyPrices;

        public DailyPricesResult()
            : base() { DailyPrices = new TypedList<PriceRateInfoContract>(); }

        public DailyPricesResult(PriceRateInfoContract[] dailyPrices)
            : base() { DailyPrices = new TypedList<PriceRateInfoContract>(dailyPrices); }
    }  
}
