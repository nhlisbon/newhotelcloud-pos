﻿using System;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class DownloadFileResult : ValidationResult
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public Blob? Content { get; set; }
        [DataMember]
        public string Url { get; set; }

        public DownloadFileResult() { }

        private DownloadFileResult(string name)
            : base()
        {
            Name = name;
        }

        public DownloadFileResult(string name, Blob content)
            : this(name)
        {
            Content = content;
        }

        public DownloadFileResult(string name, Clob content)
            : this(name)
        {
            Content = Encoding.UTF8.GetBytes(content);
        }

        public DownloadFileResult(string name, string url)
            : this(name)
        {
            Url = url;
        }
    }
}