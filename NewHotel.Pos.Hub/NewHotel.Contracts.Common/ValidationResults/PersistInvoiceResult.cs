﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public interface IPaymentResult : IEnumerable<Validation>
    {
        string DocumentSerie { get; }
        long? DocumentNumber { get; }
        CreditCardPaymentResult[] Payments { get; }
        string Receipt { get; }
        string Document { get; }
    }

    [DataContract]
    [Serializable]
    public class InvoicePaymentResult : ValidationResult, IPaymentResult
    {
        [DataMember]
        public Guid DocumentId { get; set; }
        [DataMember]
        public string DocumentSerie { get; set; }
        [DataMember]
        public long? DocumentNumber { get; set; }
        [DataMember]
        public CreditCardPaymentResult[] Payments { get; set; }

        [ReflectionExclude]
        public string Receipt
        {
            get
            {
                if (Payments != null && Payments.Length > 0)
                    return string.Join(Environment.NewLine, Payments.Select(x => x.Receipt));

                return string.Empty;
            }
        }

        [ReflectionExclude]
        public string Document
        {
            get
            {
                if (DocumentNumber.HasValue && !string.IsNullOrEmpty(DocumentSerie))
                    return DocumentNumber.Value.ToString() + "/" + DocumentSerie;

                return string.Empty;
            }
        }

        public InvoicePaymentResult()
            : base() { }
    }

    [DataContract]
	[Serializable]
    public class PersistInvoiceResult : ValidationContractResult, IPaymentResult
	{
        [DataMember]
        public Guid? InvoiceId { get; set; }
        [DataMember]
        public string Signature { get; set; }
        [DataMember]
        public string DocumentSerie { get; set; }
        [DataMember]
        public long? DocumentNumber { get; set; }
        [DataMember]
        public CreditCardPaymentResult[] Payments { get; set; }

        [ReflectionExclude]
        public string Receipt
        {
            get
            {
                if (Payments != null && Payments.Length > 0)
                    return string.Join(Environment.NewLine, Payments.Select(x => x.Receipt));

                return string.Empty;
            }
        }

        [ReflectionExclude]
        public string Document
        {
            get
            {
                if (DocumentNumber.HasValue && !string.IsNullOrEmpty(DocumentSerie))
                    return DocumentNumber.Value.ToString() + "/" + DocumentSerie;

                return string.Empty;
            }
        }

        public PersistInvoiceResult(BaseContract contract)
			: base(contract) { }

        public PersistInvoiceResult()
            : base() { }
	}
}