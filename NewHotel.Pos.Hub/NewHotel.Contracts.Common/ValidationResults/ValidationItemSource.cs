﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ValidationItemSource<T> : ValidationResult, IEnumerable
    {
        [DataMember]
        public List<T> ItemSource { get; set; }

        public ValidationItemSource ()
        {
        }

        public ValidationItemSource(IEnumerable<Validation> validations)
            : base(validations) { }

        public ValidationItemSource(Exception ex)
            : base(ex) { }

        public new IEnumerator GetEnumerator()
        {
            return ItemSource.GetEnumerator();
        }
    }
}