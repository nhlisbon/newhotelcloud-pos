﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class ReportResult : ValidationResult
    {
        [DataMember]
        public byte[] ReportDoc { get; set; }

        [DataMember]
        public Dictionary<string,byte[]> Files { get; set; }

        [DataMember]
        public string ReportName { get; set; }
        [DataMember]
        public string ReportEmail { get; set; }
        [DataMember]
        public DateTime ReportDate { get; set; }
        [DataMember]
        public bool Succced { get; set; }

        #region Constructor

        public ReportResult() { }

        #endregion
    }
}