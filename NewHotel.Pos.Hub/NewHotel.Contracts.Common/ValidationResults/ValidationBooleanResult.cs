﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ValidationBooleanResult : ValidationResult
    {
        [DataMember]
        public bool Value { get; set; }

        public ValidationBooleanResult() { }

        public ValidationBooleanResult(IEnumerable<Validation> validations)
            : base(validations) { }
    }
}
