﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ReservationRoomResult : ValidationContractResult
    {
        [DataMember]
        public TypedList<ReservationRoomContract> ReservationRooms { get; internal set; }

        public ReservationRoomResult(IEnumerable<ReservationRoomContract> reservations)
            : base()
        {
            ReservationRooms = new TypedList<ReservationRoomContract>(reservations);
        }
    }

    [DataContract]
	[Serializable]
    public class ReservationRoomAllocationResult : SequentialExecResult
    {
        [DataMember]
        public List<ReservationRoomContract> ReservationRooms { get; set; }

        public ReservationRoomAllocationResult(IDictionary<string, object> data, object tag)
            : base(data, tag)
        {
        }
    }
}