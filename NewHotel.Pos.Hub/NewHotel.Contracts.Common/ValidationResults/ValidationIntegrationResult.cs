﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ValidationIntegrationResult : ValidationResult
    {
        public ValidationIntegrationResult() : base() { }
        public ValidationIntegrationResult(Exception ex)
            :base(ex)
        {
        }

        [DataMember]
        public int PaymentForms { get; set; }
        [DataMember]
        public int CreditCardTypes { get; set; }
        [DataMember]
        public int Taxes { get; set; }
        [DataMember]
        public int Currencies { get; set; }
        [DataMember]
        public int Services { get; set; }
        [DataMember]
        public int Departments { get; set; }
        [DataMember]
        public int Origins { get; set; }
        [DataMember]
        public int Segments { get; set; }
        [DataMember]
        public int Entities { get; set; }
        [DataMember]
        public int Sellers { get; set; }
        [DataMember]
        public int Supliers { get; set; }
        [DataMember]
        public int Clients { get; set; }
        [DataMember]
        public int EntityContacts { get; set; }
        [DataMember]
        public int EntityAddress { get; set; }
        [DataMember]
        public int Deposits { get; set; }
        [DataMember]
        public int CreditCardPayments { get; set; }
        [DataMember]
        public int Invoices { get; set; }
        [DataMember]
        public int Reservations { get; set; }
        [DataMember]
        public int BlockF100 { get; set; }
        [DataMember]
        public int Users { get; set; }
        [DataMember]
        public int Cashiers { get; set; }
        [DataMember]
        public int ReductionZ { get; set; }
        [DataMember]
        public int Stocks { get; set; }
        [DataMember]
        public int Tickets { get; set; }
        [DataMember]
        public int Danfes { get; set; }
        [DataMember]
        public int Movements { get; set; }
    }
}