﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ValidationIntResult : ValidationResult
    {
        public ValidationIntResult()
            :base()
        {

        }
        public ValidationIntResult(int amount)
            : base()
        {
            Amount = amount;
        }
        public ValidationIntResult(Exception ex)
            : base(ex)
        {

        }

        [DataMember]
        public int Amount { get; set; }
    }
}
