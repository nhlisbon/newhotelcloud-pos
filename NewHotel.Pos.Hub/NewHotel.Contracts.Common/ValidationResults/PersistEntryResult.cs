﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PersistEntryResult : ValidationResult
    {
        [DataMember]
        public Guid? ReceiptId { get; set; }
		[DataMember]
		public string Receipt { get; set; }
        [DataMember]
        public string AccountDescription { get; set; }
        [DataMember]
        public CreditCardPaymentResult Payment { get; set; }

        public string PaymentReceipt
        {
            get
            {
                if (Payment != null)
                    return Payment.Receipt;

                return string.Empty;
            }
        }

        public PersistEntryResult() { }
    }  
}