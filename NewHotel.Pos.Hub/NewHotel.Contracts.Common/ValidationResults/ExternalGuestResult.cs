﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ExternalGuestResult : ValidationResult
    {
        [DataMember]
        public string SerieName { get; set; }
        [DataMember]
        public Guid? OccupationLineId { get; set; }
        [DataMember]
        public Guid? GuestId { get; set; }
        [DataMember]
        public string ExternalGuestId { get; set; }

        public ExternalGuestResult() : base() { }

    }

 
}
