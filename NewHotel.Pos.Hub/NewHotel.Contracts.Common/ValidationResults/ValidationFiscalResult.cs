﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ValidationFiscalResult : ValidationResult
    {
        [DataMember]
        public string Signature { get; set; }
        [DataMember]
        public string Info { get; set; }
    }
}
