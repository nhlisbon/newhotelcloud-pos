﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ValidationDoubleContractResult : ValidationContractResult
    {
        [DataMember]
        public BaseContract OtherContract { get; set; }

        public ValidationDoubleContractResult() 
            : base() 
        { 
        }

        public ValidationDoubleContractResult(bool ignoreWarning) 
            : base(ignoreWarning) 
        { 
        }

        public ValidationDoubleContractResult(long culture) 
            : base(culture) 
        { 
        }

        public ValidationDoubleContractResult(BaseContract contract, BaseContract otherContract)
            : base()
        {
            Contract = contract;
            OtherContract = otherContract;
        }

        public ValidationDoubleContractResult(BaseContract contract, bool ignoreWarning)
            : base(ignoreWarning)
        {
            Contract = contract;
        }

        public ValidationDoubleContractResult(IEnumerable<Validation> collection)
            : base(collection)
        {
        }
    }
}
