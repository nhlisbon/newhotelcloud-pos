﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ValidationWorkerResult : ValidationResult
    {
        public ValidationWorkerResult()
        {
            Status = WorkerStatus.None;
        }

        public ValidationWorkerResult(WorkerStatus status)
        {
            Status = status;
        }

        [DataMember]
        public WorkerStatus Status { get; set; } 
    }  

    public enum WorkerStatus { Starting = 0, Running = 1, Stopping = 2, Stopped = 3, None = 4 }
}
