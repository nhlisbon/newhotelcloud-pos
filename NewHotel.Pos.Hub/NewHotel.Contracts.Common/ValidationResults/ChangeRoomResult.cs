﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ChangeRoomResult : ValidationResult
    {
        [DataMember]
        public DateTime LastModified { get; set; }

        public ChangeRoomResult() : base() { }
    }
}