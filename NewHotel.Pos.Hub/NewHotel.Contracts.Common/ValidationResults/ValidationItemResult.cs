﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ValidationItemResult<T> : ValidationResult
    {
        [DataMember]
        public T Item { get; set; }

        public ValidationItemResult() { }

        public ValidationItemResult(IEnumerable<Validation> validations)
            : base(validations) { }

        public ValidationItemResult(Exception ex)
            : base(ex) { }
    }
}