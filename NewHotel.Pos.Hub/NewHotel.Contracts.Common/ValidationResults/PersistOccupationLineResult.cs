﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PersistOccupationLineResult : ValidationContractResult
    {
        [DataMember]
        public string SerieName { get; set; }
        [DataMember]
        public Guid? OccupationLineId { get; set; }
        [DataMember]
        public Guid? ReceiptId { get; set; }
        [DataMember]
        public string SerieNameCanceled { get; set; }
        [DataMember]
        public string Log { get; set; }

        public PersistOccupationLineResult() : base() { }

        public PersistOccupationLineResult(BaseContract contract)
            : base(contract) { }
    }
}