﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class WebExpressReservationResult : ValidationResult
    {
        [DataMember]
        public List<KeyValuePair<Guid, string>> Series { get; set; }
        [DataMember]
        public List<string> TripAdvisorCodes { get; set; }

        public WebExpressReservationResult() : base() { Series = new List<KeyValuePair<Guid, string>>(); TripAdvisorCodes = new List<string>(); }
    }
}
