﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RegularizationPaymentResult : ValidationResult
    {
        [DataMember]
        public Guid? ReceiptId { get; set; }
		[DataMember]
		public string Receipt { get; set; }
        [DataMember]
        public Guid? RegularizationId { get; set; }
		[DataMember]
		public string Regularization { get; set; }

        public RegularizationPaymentResult() { }

    }  
}
