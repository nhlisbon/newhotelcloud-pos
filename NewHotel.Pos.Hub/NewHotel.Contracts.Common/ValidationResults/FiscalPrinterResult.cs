﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class FiscalPrinterResult : ValidationResult
    {
        [DataMember]
        public FiscalPrinterRecord[] Printers { get; set; }

        public FiscalPrinterResult()
            : base(false) { }
    }
}