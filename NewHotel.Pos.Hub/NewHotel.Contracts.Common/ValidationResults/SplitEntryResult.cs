﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SplitEntryResult : ValidationResult
    {
        [DataMember]
        public TypedList<SplitMovementGridContract> SplitData { get; internal set; }

        public SplitEntryResult()
        {
            SplitData = new TypedList<SplitMovementGridContract>();
        }

        public SplitEntryResult(IEnumerable<Validation> validations)
            : base(validations)
        {
            SplitData = new TypedList<SplitMovementGridContract>();
        }
    }  
}
