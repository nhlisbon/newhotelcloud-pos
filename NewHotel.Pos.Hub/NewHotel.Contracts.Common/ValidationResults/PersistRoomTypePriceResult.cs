﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PersistRoomTypePriceResult : ValidationResult
    {
        #region Members

        [DataMember]
        internal Guid _priceRateId;
        [DataMember]
        internal Guid _roomTypeId;
        [DataMember]
        internal DateTime _finalDate;

        #endregion

        public Guid PriceRateId
        {
            get { return _priceRateId; }
        }

        public Guid RoomTypeId
        {
            get { return _roomTypeId; }
        }

        public DateTime FinalDate
        {
            get { return _finalDate; }
        }

        [DataMember]
        public bool AdultPricesChanged;
        [DataMember]
        public bool ChildPricesChanged;
        [DataMember]
        public long AffectedReservations;

        public PersistRoomTypePriceResult(Guid priceRateId, Guid roomTypeId, DateTime finalDate)
            : base()
        {
            _priceRateId = priceRateId;
            _roomTypeId = roomTypeId;
            _finalDate = finalDate;
        }
    }
}
