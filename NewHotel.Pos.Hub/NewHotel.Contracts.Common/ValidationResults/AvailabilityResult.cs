﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class AvailabilityResult : ValidationResult
    {
        [DataMember]
        public bool ShowAvailability { get; internal set; }
        [DataMember]
        public bool InsertionLock { get; internal set; }
        [DataMember]
        public ResourceOccupationControlType? CriticalControlType { get; internal set; }
        [DataMember]
        public decimal? CriticalControlValue { get; internal set; }
        [DataMember]
        public bool InsertionLockSettings { get; set; }
        [DataMember]
        public TypedList<AvailabilityInfoContract> Availabilities { get; internal set; }

        public AvailabilityResult(TypedList<AvailabilityInfoContract> availabilities,
            bool showAvailability, bool insertionLock,
            ResourceOccupationControlType? criticalControlType,
            decimal? criticalControlValue, bool insertionLockSettings)
            : base()
        {
            Availabilities = availabilities;
            ShowAvailability = showAvailability;
            InsertionLock = insertionLock;
            CriticalControlType = criticalControlType;
            CriticalControlValue = criticalControlValue;
            InsertionLockSettings = insertionLockSettings;
        }

        public AvailabilityResult(TypedList<AvailabilityInfoContract> availabilities)
        {
            Availabilities = availabilities;
        }

        public AvailabilityResult()
            : base()
        {
            Availabilities = new TypedList<AvailabilityInfoContract>();
        }
    }  
}
