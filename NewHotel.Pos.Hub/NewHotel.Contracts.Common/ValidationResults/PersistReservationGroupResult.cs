﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PersistReservationGroupResult : ValidationResult
    {
        [DataMember]
        public string GroupName { get; set; }
        [DataMember]
        public List<string> Series { get; set; }
        [DataMember]
        public List<string> TripAdvisorCodes { get; set; }
        [DataMember]
        public List<Guid> OccupationLinesIds { get; set; }

        public PersistReservationGroupResult() : base()
        {
            Series = new List<string>();
            TripAdvisorCodes = new List<string>();
            OccupationLinesIds = new List<Guid>();
        }
    }
}