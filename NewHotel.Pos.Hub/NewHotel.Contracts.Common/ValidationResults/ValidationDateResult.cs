﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ValidationDateResult : ValidationResult
    {
        public ValidationDateResult() { }
        public ValidationDateResult(DateTime date) { Date = date; }
        public ValidationDateResult(Exception ex) : base(ex) { }

        [DataMember]
        public DateTime? Date { get; set; }
    }
}
