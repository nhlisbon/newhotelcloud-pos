﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHotel.Contracts.Common
{
    public enum IntegrationType
    {
        Stocks = 0,
        Payments = 1
    }
}
