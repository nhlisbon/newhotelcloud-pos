﻿using System;

namespace NewHotel.Contracts
{
    #region InvoiceType

    public enum InvoiceType : long { Invoice = 1, AdvanceDepositInvoice = 2, Ticket = 3, ProformInvoice = 4, InternalInvoice = 5 }

    #endregion
    #region DocType

    public enum SerializedDocType : long
    {
        Invoices = 9,
        CreditInvoice = 10,
        CashInvoice = 11,
        InternalInvoice = 12,
        CancelationInvoice = 13,
        ProformInvoice = 14,
        Offshore = 15,
        CreditNotes = 16,
        CreditCreditNote = 17,
        CashCreditNote = 18,
        InternalCreditNote = 19,
        Receipts = 20,
        CashReceipt = 21,
        DepositReceipt = 22,
        CancelationReceipt = 23,
        CorrectionReceipt = 24,
        Tickets = 25,
        SalesTicket = 26,
        TransferTicket = 27,
        Reservations = 28,
        RoomReservation = 29,
        SpaReservation = 30,
        TableReservation = 31,
        EventsReservation = 32,
        DebitNotes = 36,
        CreditDebitNoteOfInvoice = 37,
        CashDebitNoteOfInvoice = 38,
        InternalDebitNote = 39,
        OwnerStatement = 42,
        CashCreditNoteOfInvoice = 43,
        CashCreditNoteOfTicket = 44,
        CreditCreditNoteOfInvoice = 45,
        CreditCreditNoteOfTicket = 46,
        LookupTable = 48,
        AdvanceDepositInvoice = 53,
        AdvanceDepositCreditNote = 54,
        RPS = 61,
        RPP = 62
    }

    /// <summary>
    /// IdentityDocument, Passport, DriverLicence, ResidenceCertificate
    /// </summary>
    public enum IdentityDocType : long
    {
        IdentityDocument = 2,
        Passport = 3,
        DriverLicence = 4,
        ResidenceCertificate = 5
    }

    public enum PersonalDocType : long
    {
        CreditCard = 1,
        IdentityDocument = 2,
        Passport = 3,
        DriverLicence = 4,
        ResidenceCertificate = 5,
        InsuranceCard = 6,
        Other = 7,
        FiscalNumber = 8
    }

    /// <summary>
    /// CHBS.CHBS_REEX = ReviewExpress (None, PMS,  IBE , All )
    /// </summary>
    public enum ReviewExpressType : long { None = 4, PMS = 4082, IBE = 4081, All = 1 };

    /// <summary>
    /// TNHT_CHBS.CHBS_SPRI (Both, Price Per Night, Total Price)
    /// </summary>
    public enum ShowPriceType : long { Both = 3, PricePerNight = 4201, TotalPrice = 4202 };

    /// <summary>
    /// BHORE.HORE_ESTA = Estado (Reservada, Check-In, Check-Out)
    /// </summary>
    public enum GuestState : long { Reserved = 601, CheckIn = 602, CheckOut = 606 };

    /// <summary>
    /// LIOC.LIOC_CRIT = Tipo Calculo/Precio (Standard, Edad, Orden)
    /// </summary>
    public enum ReservationPriceCalculation : long { Standard = 1171, Age = 1201, Order = 1202 };

    /// <summary>
    /// TCFG_HOTE.PARA_PSPR = Tipo Provider Pago (Manual, Shift4, Authorize.Net, Unicre, ClearOne, SuperPay, Six, SiTef, Braspag)
    /// </summary>
    public enum PaymentServiceProvider : long
    {
        Manual = 511, Shift4 = 3405, AuthorizeDotNet = 3408, Unicre = 3411, ClearOne = 3412, SuperPay = 3413, Six = 3414, SiTef = 3415, Braspag = 3416
    };

    /// <summary>
    /// TNHT_TACR.TACR_ESTA = Estado Operación Pago (Completada, Pendiente, Rechazada, Requiere Autorización, Cancelada)
    /// </summary>
    public enum PaymentOperationStatus : long { Completed = 1471, Pending = 1474, Declined = 1478, AuthRequired = 1479, Cancelled = 1480 };

    /// <summary>
    /// TNHT_CHBS.CHBS_VOUH = BookingVoucherType (None, OriginalCode, PromoCode, OriginalCodeAndPromotion, OriginalInvoiceInformation, ItineraryCode)
    /// </summary>
    public enum BookingVoucherType : long { None = 4, OriginalCode = 4141, PromoCode = 4142, OriginalCodeAndPromotion = 4143, OriginalInvoiceInformation = 4144, ItineraryCode = 4145 };

    #endregion
    #region Contact Type

    public enum ContactType : long
    {
        Address = 1,
        Phone = 2,
        Fax = 3,
        EMail = 4,
        Internet = 5,
        IMAddress = 6
    }

    #endregion
    #region CurrentAccount Grouping

    [Flags]
    public enum CurrentAccountGroupingFlags
    {
        None = 0,
        ByService = 2,
        ByDepartment = 4,
        ByWorkDate = 8,
        ByValueDate = 16,
        ByDaily = 32,
        ByRoomRate = 64,
        ByPhone = 128,
        ByDocNumber = 256,
        ByPaymentsType = 512,
        ByReservation = 1024,
        ByTax = 2048,
        ByPackage = 4096,//2019.08.07
        ByCityTax = 65536,
        ByWorkDateDaily = 40,
        ByWorkDateDailyCityTax = 65576,
        ByDailyRoomRate = 96
    };

    [Flags]
    public enum CondoCurrentAccountGroupingFlags
    {
        None = 0,
        ByDate = 2,
        ByContract = 4,
        ByOwnerCharge = 8,
        ByBooking = 16
    }

    #endregion
    #region Extension Operations
    [Flags]
    public enum ExtensionOper
    {
        None = 0,
        View = 1,
        OpenLine = 2,
        CloseLine = 4,
        OpenPayTV = 8,
        ClosePayTV = 16,
        CreateKey = 32,
        CopyKey = 64,
        CancelKey = 128,
        OpenInternet = 256,
        CloseInternet = 512,
        OpenAC = 1024,
        CloseAC = 2048
    };
    #endregion

    #region AccountMovement Operations

    [Flags]
    public enum AccountMovementOper
    {
        None = 0,
        View = 1,
        Modify = 2,
        Cancel = 4,
        Transfer = 8,
        Overpayment = 16,
        BadDebt = 32,
        PrintOriginal = 64,
        PrintCopies = 128,
        NewPaymentOrder = 256
    };

    #endregion

    #region Export to CRM & XML
    public enum ExportTypeReservations : long
    {
        ToXML = 1,
        ToCRM = 2,
    }

    public enum CRMExportType : long
    {
        ProductLine = 1,
        Reservations = 2,
        Entity = 4,
        Cardex = 8,
    }
    #endregion
    #region LogUser

    public enum LogUserOperations : long
    {
        Cancel = 1,
        Create = 2,
        Solution = 3,
        Readed = 4,
        Locked = 5,
        Closed = 6,
        Emission = 7,
        Confirmed = 8,
        Modified = 9,
        Erased = 10,
        Opened = 11,
        Unlocked = 12,
        CheckIn = 13,
        UndoCheckIn = 14,
        CheckOut = 15,
        UndoCheckOut = 16,
        NoShow = 17,
        UndoNoShow = 18,
        Overbooking = 19,
        UndoOverbooking = 20,
        ChangeRoom = 21,
        UndoCancel = 22,
        ChangeStay = 23,
        LockRoom = 24,
        UnLockRoom = 25,
        PreCheckIn = 26,
        Notification = 27,
        AttemptConversation = 28,
        ChangesPrices = 29,
        ChangeOccupedRoomType = 30,
        ChangeReservedRoomType = 31,
        CreateInactivity = 32,
        EditInactivity = 33,
        ExportExternalFiles = 34,
        ChannelExport = 35,
        ChangeTaxSchema = 36,
        GuestCheckIn = 37,
        GuestCheckOut = 38,
        BookingServiceNotification = 39,
        UserChanged = 40,
        NightAuditor = 41,
        ServiceTaxes = 42,
        ChangeComment = 43,
        CreditLimit = 44,
        PriceRateChanged = 45,
        ExportToRevPro = 49,
        CheckinOnlineNotification = 54,
        FiscalizationChanged = 55
    }

    public enum LogCurrentAccountOperations : long
    {
        Create = 2,
        Locked = 5,
        Closed = 6,
        Opened = 11,
        Unlocked = 12
    }

    public enum LogReservationOperations : long
    {
        Cancel = 1,
        Create = 2,
        Confirmed = 8,
        CheckIn = 13,
        UndoCheckIn = 14,
        CheckOut = 15,
        UndoCheckOut = 16,
        NoShow = 17,
        UndoNoShow = 18,
        Overbooking = 19,
        UndoOverbooking = 20,
        ChangeRoom = 21,
        UndoCancel = 22,
        ChangeStay = 23,
        LockRoom = 24,
        UnLockRoom = 25,
        PreCheckIn = 26,
        Notification = 27,
        FromAttempt = 28,
        ChangePrices = 29,
        ChangeOccupedRoomType = 30,
        ChangeReservedRoomType = 31,
        ChangeTaxSchema = 36,
        GuestCheckIn = 37,
        GuestCheckOut = 38,
        ChangeComment = 43,
        ChangeAllotment = 47,
        ValidationFailed = 48,
        ConfirmationEmail = 50,
        PreCheckinEmail = 51,
        PostCheckoutEmail = 52,
        CheckinOnlineReservationUpdate = 53,
        SendGuestsInformation = 56
    }

    public enum LogPersonalInformationOperations : long
    {
        Create = 2,
        Modified = 9,
        Show = 46
    }

    #endregion
    #region Other Types

    /// <summary>
    /// NO FIELD = Tipo Descuento (Valor, Porciento)
    /// </summary>
    public enum DiscountType : long { Value = 2291, Percent = 2292 };

    /// <summary>
    /// NO FIELD = Tipo Movimiento Caja (In, Out)
    /// </summary>
    public enum CashEntrieType : long { In = 7, Out = 8 };

    /// <summary>
    /// DOFI_STAT = Estado Documento Oficial (Cobrada, Total a contabilidad, Parcialmente a contabilidad, Pendiente, CRM, Cobrada + CRM, Previsualización, Cancelado)
    /// </summary>
    public enum OficialDocumentState : long { Payed = 1471, TotalAccounting = 1472, PartialAccounting = 1473, Pending = 1474, CRMPoints = 1475, PayedPlusCRMPoints = 1476, Preview = 1477, Declined = 1478, AuthRequired = 1479, Cancelled = 1480 };

    /// <summary>
    /// TNHT_DEFA.DEFA_TIPO = Tipo Linea Detalle Documento
    /// </summary>
    public enum DocumentDetail : long { Service = 2401, Payment = 2402 };

    /// <summary>
    /// MOPR.MOVI_SURC = Source Pre-Movimiento
    /// Price Rate, Price Rate Additional, Reservation Additional, Manual Rate, Guest Tax
    /// </summary>
    public enum MovementSource : long { PriceRate = 1, PriceRateAddtional = 2, ReservationAdditional = 3, ManualRate = 4, GuestTax = 5 }

    /// <summary>
    /// MOPR.MOVI_TYPE = Tipo Pre-Movimiento
    /// Room, Breakfast, BreakfastDrink, Lunch, LunchDrink, Dinner, DinnerDrink, AllInclusive, GuestTax, Supplement, Other
    /// </summary>
    public enum MovementType : long { Room = 1, Breakfast = 2, BreakfastDrink = 3, Lunch = 4, LunchDrink = 5, Dinner = 6, DinnerDrink = 7, AllInclusive = 8, GuestTax = 9, Supplement = 10, Other = 99 };

    /// <summary>
    /// Tipo Persona
    /// Adulto, Niño, Adulto (Extra), Niño (Extra), Adulto (S.S), Niño (S.S)
    /// </summary>
    public enum PersonType : long { Adult = 1, Child = 2, ExtraAdult = 3, ExtraChild = 4, SingleAdult = 5, SingleChild = 6 };

    /// <summary>
    /// MOVI.TIRE_PK = Tipo Recibimiento (Payment, Transfer, Points, Cash Advance, Devolution, Discount, Free Title, RoomPlan, Tick. Transfer, HouseUse, Credit)
    /// </summary>
    public enum ReceivableType : long { Payment = 2131, AccountTransfer = 2132, DiscountPoints = 2133, CashAdvance = 2134, Devolution = 2135, UnofficialInvoice = 2136, Discount = 2137, FreeTitle = 2138, RoomPlan = 2139, TicketTransfer = 2140, HouseUse = 1172, Credit = 271, TaxRetention = 2141, InternalAccountSettlement = 2142, RetentionOverNet = 2143, UseAccountDeposit = 2144 };

    public enum RolOptionsType : long
    {
        Hotels = 1, AddHotels = 2, GroupHotels = 3, Users = 4, AddUsers = 5, RolesUsers = 6, Taxes = 7,
        TaxSequence = 8, TaxRegion = 9, SettingsCompany = 10, SettingsPayment = 11, SettingsDistributor = 12,
        Licences = 13, SettingsMail = 14, SettingsTemplate = 15, SettingsServiceExt = 16, RequestProposal = 17, RequestDemo = 18, RequestJoin = 19
    }

    /// <summary>
    /// Types of resources where license limits are apply over
    /// </summary>
    public enum LimitedResources : long { User = 1, Room = 2, Booking = 3, Transaction = 4, Company = 5 };

    public enum NewGesType : long { Line = 1, PayTV = 3, Internet = 6, AC = 9, Key = 12 };

    public enum JobType : long
    {
        JOBTYPE_OPENLINE = 1,
        JOBTYPE_CLOSELINE = 2,
        JOBTYPE_OPENLINEMANUAL = 3,
        JOBTYPE_CLOSELINEMANUAL = 4,
        JOBTYPE_MODIFYDATA = 5,
        JOBTYPE_ROOMCHANGE = 6,
        JOBTYPE_TOGGLEDISTURBMODE = 7,
        JOBTYPE_WAKEUP = 8,
        JOBTYPE_CANCELWAKEUP = 9,
        JOBTYPE_CREATEKEY = 10,
        JOBTYPE_DUPLICATEKEY = 11,
        JOBTYPE_CANCELKEY = 12,
        JOBTYPE_KEYGUESTCHANGE = 13,
        JOBTYPE_OPENPAYTV = 31,
        JOBTYPE_CLOSEPAYTV = 32,
        JOBTYPE_OPENPAYTVMANUAL = 33,
        JOBTYPE_CLOSEPAYTVMANUAL = 34,
        JOBTYPE_PAYTVMODIFYDATA = 35,
        JOBTYPE_PAYTVROOMCHANGE = 36,
        JOBTYPE_PAYTVWAKEUP = 38,
        JOBTYPE_PAYTVCANCELWAKEUP = 39,
        JOBTYPE_SCANDOCUMENT = 51,
        JOBTYPE_OPENINTERNET = 61,
        JOBTYPE_CLOSEINTERNET = 62,
        JOBTYPE_OPENINTERNETMANUAL = 63,
        JOBTYPE_CLOSEINTERNETMANUAL = 64,
        JOBTYPE_INTERNETMODIFYDATA = 65,
        JOBTYPE_INTERNETROOMCHANGE = 66,
        JOBTYPE_INTERNETWAKEUP = 68,
        JOBTYPE_INTERNETCANCELWAKEUP = 69,
        JOBTYPE_TEFPAYMENT = 71,
        JOBTYPE_TEFDEVOLUTION = 72,
        JOBTYPE_TEFPAYANNULAMENT = 73,
        JOBTYPE_TEFDEVANNULAMENT = 74,
        JOBTYPE_OPENAC = 75,
        JOBTYPE_CLOSEAC = 76,
        JOBTYPE_DUMMY = 9999
    };

    public enum ExportDocumentType : long { OfficialDoc = 1, RegistrationCard = 2 };

    public enum TableCode : long { tnht_tial = 1, tnht_tien = 2, tnht_enti = 3, tnht_secc = 4, tnht_grse = 5, tnht_serv = 6, tnht_orme = 7, tnht_seme = 8 };

    public enum ReportViewType : long { Normal = 1, Detail = 2, Summarize = 3 };

    #endregion
    #region Domain Types

    /// <summary>
    /// TNHT_LSPA.LSPA_CRPR (By Service, By Hour)
    /// </summary>
    public enum SpaReservationPriceType : long { ByService = 4491, ByHour = 4494 };

    /// <summary>
    /// BMOAD.MOAD_TICO, INTF.MOVI_TICO, LCRE_TICO, MOVI.MOVI_TICO, SERV_TICO, INTR.MOVI_TICD, INTR.MOVI_TICO = tipo cuenta (master, extra 1 ... extra N)
    /// </summary>
    public enum DailyAccountType : long { Master = 871, Extra1 = 872, Extra2 = 873, Extra3 = 874, Extra4 = 875, Extra5 = 876 };

    public enum FiscalPOSType : long { None = 4600, BrazilBematech = 4630, CuracaoBixolon = 4631, PanamaTally = 4632 };

    /// <summary>
    /// IMSE.TIVA_APL2 = Aplicación Impuesto 2 (Sobre Base, Sobre Base + Taxa1)
    /// </summary>
    public enum ApplyTax2 : long { OverBase = 91, OverBasePlusRate1 = 92 };

    /// <summary>
    /// IMSE.TIVA_APL3 = Aplicación Impuesto 3 (Sobre Base, Sobre Base + Taxa1, Sobre Base + Taxa2 , Sobre Base + Taxa1 + Taxa2)
    /// </summary>
    public enum ApplyTax3 : long { OverBase = 91, OverBasePlusRate1 = 92, OverBasePlusRate2 = 93, OverBasePlusRate1PlusRate2 = 94 };

    /// <summary>
    /// EBFE/EBTU/OPER_APDE, EBFE/EBTU/OPER_CADE, HOPP/LIOC.INDE_CODI, HOPP/LIOC.INDE_CODA = Incidencia descuento (aplicado a...)
    /// </summary>
    public enum DiscountMethod : long { AllServices = 841, Room = 842, Breakfast = 843, Meals = 844, RoomAndBreakfast = 845, RoomAndMeals = 846, BreakfastAndMeals = 847, RoomAndBreakfastAndMeals = 848 };

    /// <summary>
    /// LIOC.LIOC_ESTA = Estado Reserva (Reserved, CheckIn, NoShow, Cancelled, CheckOut)
    /// </summary>
    public enum ReservationState : long { Reserved = 601, CheckIn = 602, NoShow = 604, Cancelled = 605, CheckOut = 606 };

    /// <summary>
    /// HOPP.HOPP_TITA = Tipo Tarifa: Standard, House Use, Complementary, Invitation
    /// </summary>
    public enum ReservationPaymentType : long { Standard = 1171, HouseUse = 1172, Complimentary = 1173, Invitation = 1174 };

    /// <summary>
    /// ALLO.ALLO_TIPO = Tipo Allotment: (Entidad, Directo)
    /// </summary>
    public enum EntityClientType : long { Entity = 211, InDesk = 212 };

    /// <summary>
    /// ALOJ_ROST, DRST_ROAN, DRST_ROST, LRST_ESAN, LRST_NOES = Room Status (Sucio, Limpiando, Verificar, Limpio)
    /// </summary>
    public enum RoomStatus : long { Dirty = 661, Cleaning = 662, Verify = 663, Clean = 664 };

    /// <summary>
    /// TACO.TACO_TICU = Tipo Cuenta (Cliente, Proveedor, Ambos)
    /// </summary>
    public enum AccountType : long { Client = 11, Supplier = 12, Both = 3 };

    /// <summary>
    /// HENTI.DEPO_TYPE = Tipo pre.pago solicitado
    /// </summary>
    public enum AdvancedDepositType : long { None = 4, NotRequired = 2, Nights = 1291, Fixed = 1292, StayPercent = 1293 };

    /// <summary>
    /// MTCO.MTCO_TICA = Tipo Cancelación (Facturas, Movimientos, Reservas, City Tax, Notas de Crédito, Notas de Débito)
    /// </summary>
    ///
    public enum CancellationType : long { All = 1, None = 4, Invoice = 121, Entries = 122, Reservation = 123, CityTax = 4407, CreditNote = 125, DebitNote = 126 };

    /// <summary>
    /// MTCO.MTCO_CATE = Categoría Cancelación (Exclusiones, Excenciones, Otras)
    /// </summary>
    ///
    public enum CancellationCategory : long { Exclusions = 4691, Exemptions = 4692, Others = 4712 };

    /// <summary>
    /// CONT.CONT_CVIL = Estado Civil (Soltero, Casado, Divorciado, Viudo)
    /// </summary>
    public enum CivilState : long { Single = 181, Married = 182, Divorced = 183, Widowed = 184 };

    /// <summary>
    /// TIDE.TIDE_APPL = Descuento Aplicado a (Facturas, Reservas, Movimientos, Precios, Todos)
    /// </summary>
    public enum DiscountApplyTo : long { Invoice = 121, Reservation = 123, Entries = 122, Prices = 124, All = 1 };

    /// <summary>
    /// TIDE.TIDE_TIDE = Descuento aplicado sobre (bruto, neto)
    /// </summary>
    public enum DiscountApplyOver : long { Gross = 451, Net = 452 };

    /// <summary>
    /// SESU.SESU_APOV = Prociento aplicado sobre (Gross, Net)
    /// </summary>
    public enum PercentApplyOver : long { Gross = 451, Net = 452 };

    /// <summary>
    /// ENTI.CATE_PK = Categoria Entidades (Company, Agency)
    /// </summary>
    public enum EntityCategory : long { Company = 2321, Agency = 2322 };

    /// <summary>
    /// TNHT_EXTE.EXTE_PREC = Tipo Tarifa Comunicaciones (Habitacion, Interna, Extra)
    /// </summary>
    public enum PhonePrice : long { Room = 61, Internal = 2732, Extras = 2735 };

    /// <summary>
    /// EXTE.EXTE_TIPO = Tipo Extensión (Telefono, PayTv, Llaves, Internet, Verjas, AC, POS-Externo)
    /// </summary>
    public enum ExtensionType : long { Phone = 1831, PayTv = 1832, Keys = 1833, Internet = 1834, Gates = 1835, AirCondition = 1836, ExternalPOS = 1837 };

    /// <summary>
    /// VUEL.VUEL_STAT = Status (Entrada, Salida)
    /// </summary>
    public enum FlightStatus : long { Arrive = 301, Departure = 302 };

    /// <summary>
    /// VUEL.VUEL_TIPO = Tipo de Vuelo (Regular, Charter)
    /// </summary>
    public enum FlightType : long { Regular = 361, Charter = 362 };

    /// <summary>
    /// CONT.CONT_SEXO = Sexo (Masculino, Femenino)
    /// </summary>
    public enum GenderType : long { Male = 751, Female = 752 };

    /// <summary>
    /// HENTI.ENTI_FAMA, INTF_TIPO = Facturar Master de Reservas a (Cliente, Entidad, Agencia)
    /// </summary>
    public enum InvoiceDestination : long { Client = 11, Entity = 211, Agency = 2322 };

    /// <summary>
    /// PARE.PARE_TYPE = Type de Parentezco/Relación (Cliente, Entidad)
    /// </summary>
    public enum RelationType : long { Client = 11, Entity = 211 };

    /// <summary>
    /// HENTI.ENTI_FOFA = Tipo de Factura (resumida, detallada)
    /// </summary>
    public enum InvoiceModel : long { Summarized = 391, Detailed = 392 };

    /// <summary>
    /// HENTI.ENTI_TIFA = Facturar a: (Info Comercial, Info Fiscal, Agencia Mayorista, Representante)
    /// </summary>
    public enum InvoiceOwner : long { ComercialInfo = 421, FiscalInfo = 422, WholesaleAgency = 423, Representative = 424 };

    /// <summary>
    /// HENTI.ENTI_FCBN = Comision aplicada sobre el valor de la factura (Bruto, Neto)
    /// </summary>
    public enum KindValue : long { Gross = 451, Net = 452 };

    /// <summary>
    /// RELA_FRNA, RNAU_FRNA = Tipo Listado Cierre (Control, Listado Final)
    /// </summary>
    public enum NightAuditorReportType : long { ControlList = 541, FinalList = 542 };

    /// <summary>
    /// PREC.PREC_FOAL = Forma aplicación del precio (Alojamiento, Paxs)
    /// </summary>
    public enum PriceCalculationType : long { Room = 901, Paxs = 902 };

    // CAAL_CATE, TREC_CATE = Categoria recurso (habitación, golf, etc)
    //public enum ResourceCategory : long { Room = 61, Golf = 1441 };

    /// <summary>
    /// TREC.TREC_TIOV = Tipo Control(Máx. Reservas/% Ocupacion Critica)
    /// </summary>
    public enum ResourceOccupationControlType : long { MaxReservation = 1951, CriticalOccupation = 1952 };

    /// <summary>
    /// TREC_MODE = Modo Tipo Recurso (Diaria, Intervalo tiempo)
    /// </summary>
    public enum ResourceUseMode : long { Daily = 481, TimeIntervals = 482 };

    /// <summary>
    /// TIAL.TIAL_TIAL = Tipo (Habitación, Casa)
    /// </summary>
    public enum RoomRentType : long { Room = 61, House = 631 };

    /// <summary>
    /// SEDO.SEDO_ORDE = Orden precedencia prefijo (texto+numerico, numerico+texto)
    /// </summary>
    public enum SerieOrder : long { TextPlusNumber = 691, NumerPlusText = 692 };

    /// <summary>
    /// SEDO.SEDO_SEAC = Estado serie documento (activa, inactiva , futura)
    /// </summary>
    public enum SerieStatus : long { Active = 721, Inactive = 722, Future = 723 };

    /// <summary>
    /// CCCO.CCCO_TIPO = Tipo de Cuenta (Reserva, Entidades, Grupo, Huésped, Control, EventReservation, Protocol, Client, Owner, SpaReservation)
    /// </summary>
    public enum CurrentAccountType : long { Reservation = 241, Entities = 211, Groups = 242, Guests = 244, Control = 245, EventReservation = 246, Protocol = 247, Client = 11, Owner = 961, SpaReservation = 4584 };

    /// <summary>
    /// OPER.OPER_APAP = Aplicar la distribucion (Alojamiento, Pensiones o Ambas)
    /// </summary>
    public enum ApplyDistribution : long { Room = 61, Boards = 62, Both = 3 };

    /// <summary>
    /// NO FIELD = Tipo Contrato (Contrato, Oferta, Early Booking)
    /// </summary>
    public enum ContractType : long { Contract = 2351, Offer = 2352, EarlyBooking = 2353 };

    /// <summary>
    /// OPER.OPER_ENSA = Aplicar desde... (Entrada, Salida)
    /// </summary>
    public enum EntryType : long { Arrival = 301, Departure = 302 };

    /// <summary>
    /// TCFG_HOTE.PARA_MIVA = Modelo impuesto aplicado (x Servicios, Portugues)
    /// </summary>
    public enum TaxModel : long { ByServices = 1921, Portuguese = 1922 };

    /// <summary>
    /// TCFG_RESE.ADIC_PRCM = Adicionales en precio medio
    /// </summary>
    public enum AditionalsInAveragePrice : long { Exclude = 2101, Include = 2102, IncludeOnlyDaily = 2103 };

    /// <summary>
    /// TCFG_RESE.DAYU_RULE = (Nothing, Invoice, Current Account)
    /// </summary>
    public enum DayUseDailyRule : long { Nothing = 2191, Invoice = 2192, CurrentAccount = 2193 };

    /// <summary>
    /// TCFG_RESE.GUEST_AGE = (Nunca, Todos en Reserva, Todos en Check-In, Solo Niños en Reserva, Solo Niños en Check-In)
    /// </summary>
    public enum GuestAgeControl : long { Never = 5, AllGuestsReservation = 1651, AllGuestsCheckIn = 1652, OnlyChildsReservation = 1653, OnlyChildsCheckIn = 1654 };

    /// <summary>
    /// TCFG_RESE.GUEST_CTRL = Permitir Reservas sin Huespedes (Nunca, Reserva, Check-In, Reservation and Check-In, Check-In and Night Auditor)
    /// </summary>
    public enum AssignGuestsByReservationLine : long { Never = 5, Reservation = 241, CheckIn = 602, ReservationAndCheckIn = 1531, CheckInAndNightAuditor = 1532 };

    /// <summary>
    /// LIOC.LIOC_RACI = Autorizo de checkin (no requiere, esta autorizado, no esta autorizado)
    /// </summary>
    public enum CheckInAuthorize : long { NotRequire = 2, Authorized = 1111, NotAuthorized = 1112 };

    /// <summary>
    /// RESE.RESE_TICO = Tipo de confirmación  (Carta, E-Mail, Teléfono, Fax, SMS)
    /// </summary>
    public enum ConfirmationType : long { Letter = 1891, EMail = 1892, Phone = 1893, Fax = 1894, SMS = 1895 };

    /// <summary>
    /// RESE.RESE_ESCO = Estados de confirmación  (No requiere, Lleva pero no ha sido enviado, Lleva y ya fue enviado)
    /// </summary>
    public enum ConfirmationState : long { NotRequired = 2, RequiredNotSended = 1081, RequiredAndSended = 1082 };

    /// <summary>
    /// TCFG_RECU.OCUP_CRIT = Tipo de ocupación crítica (no requiere, general , especifico)
    /// </summary>
    public enum CriticalOccupationType : long { NotRequired = 2, General = 1681, Specific = 1682 };

    /// <summary>
    /// TCFG_RESE.RATE_DUSE = Diaria para day-use = (Check-In, Check-Out)
    /// </summary>
    public enum DayUseDailyMethod : long { CheckIn = 602, CheckOut = 606 };

    /// <summary>
    /// TCFG_RECU.ROOM_DIRT = En en cierre Habitacion = Sucia(Ocupadas, Todas)
    /// </summary>
    public enum DirtyRoomStatusAtClose : long { Occupied = 1981, All = 1982 };

    /// <summary>
    /// TCFG_RESE.DPEN_MODE = Modo de doble pensión (Real -> Invoice, Invoice -> Real)
    /// </summary>
    public enum DoublePensionMode : long { RealToInvoicing = 2041, InvoicingToReal = 2042 };

    /// <summary>
    /// TCFG_FACT.SIGN_TIPO = Tipo de fiscalización
    /// </summary>
    public enum DocumentSign : long
    {
        None = 4,
        FiscalPrinter = 2610,
        FiscalizationPortugal = 2611,
        FiscalizationBrazilNFE = 2612,
        FiscalizationCroatia = 2613,
        FiscalizationChile = 2614,
        FiscalizationBrazilCMFLEX = 2615,
        FiscalizationPeruDFacture = 2616,
        FiscalizationEcuador = 2617,
        FiscalizationColombiaBtw = 2618,
        FiscalizationBrazilProsyst = 2619,
        FiscalizationCuracao = 2620,
        FiscalizationMexicoCfdi = 2621,
        FiscalizationAngola = 2622,
        FiscalizationUruguayExFactura = 2623,
        FiscalizationPanamaHKA = 2625,
        FiscalizationMalta = 2626,
        FiscalizationCaboVerde = 2627,
    };

    /// <summary>
    /// TCFG_RESE.GUEST_IDEN, GUEST_CARDEX = (Nunca, Todos en Reservas, Todos en Check-In, Titular en Reserva, Titular en Check-In)
    /// </summary>
    public enum GuestBehaviorInReservation : long { Never = 5, AllInReservation = 1561, AllInCheckIn = 1562, HolderInReservation = 1563, HolderInCheckIn = 1564 };

    /// <summary>
    /// BHORE.HORE_TIHO = Tipo huesped (Adulto, Niño, Bebé)
    /// </summary>
    public enum GuestType : long { Adult = 31, Child = 1321, Baby = 1322 };

    /// <summary>
    /// HOPP.HOPP_MPAL = Comida en Media Pension (Almuerzo, Cena)
    /// </summary>
    public enum HalfBoardEatMode : long { Lunch = 1262, Dinner = 1263 };

    /// <summary>
    /// HOPP.HOPP_ALDE = Confirmación almuerzo (Día de llegada, Día de salida, Ambos)
    /// </summary>
    public enum LunchConfirmation : long { ArrivalDay = 1411, DepartureDay = 1412, BothDays = 3 };

    /// <summary>
    /// TPDI.TPDI_TIPR = Tipo Precio (Entidad, Directo, Ambas)
    /// </summary>
    public enum PriceUsedFor : long { Company = 211, InDesk = 212, Both = 3 };

    /// <summary>
    /// LIRE.LIRE_ESCO = Estado de confirmación (Waiting List, Confirmado, Tentativa)
    /// </summary>
    public enum ReservationConfirmState : long { WaitingList = 1022, Confirmed = 1023, Attempt = 1050 };

    /// <summary>
    /// LIRE.LIRE_TRES, TCFG_RESE.RESE_TRES = Tipo de Reserva(Directo, Entidad, Time-Shared, Propietario)
    /// </summary>
    public enum ReservationType : long { Company = 211, InDesk = 212, Owner = 961, TimeShare = 962 };

    /// <summary>
    /// NO FIELD = Reglas para Series Documento (Futura, Orden, Autogenerar)
    /// </summary>
    public enum SerieRules : long { Future = 2381, Order = 2382, Autogenerate = 2383 };

    /// <summary>
    /// Formato del nombre del fichero de exportacion a Entidad Oficial (NIFHotelNumeroDAT, HotelNumero)
    /// </summary>
    public enum ExportTypeOfficialEntity : long { NIFHotelNumeroDAT = 2890, HotelNumero = 2891, WebService = 2892 };

    /// <summary>
    /// TCFG_RECU.PARA_ROST = Control Room Status (Ninguno, Sucio/Limpio , Sucio/En Limpieza/Verificación/Limpio)
    /// </summary>
    public enum RoomStatusType : long { None = 4, CleanDirty = 1711, CleanDirtyCleaningVerify = 1712 };

    /// <summary>
    /// BEOT.BEOT_REST = Tipo Restricción (Insersion, Modificacion, Estancia, Stop Reservas, Stop Check-In, Pre-Pago)
    /// </summary>
    public enum Restriction : long { Insert = 1501, Modify = 1502, Stay = 1503, StopReservation = 1504, StopCheckIn = 1505, PrePayment = 1506 };

    /// <summary>
    /// BEOT.BEOT_TIPO = Tipo (Entidad, Directo)
    /// </summary>
    public enum RestrictionType : long { Entity = 211, InDesk = 212 };

    /// <summary>
    /// DEFO.DEFO_TILI = Tipo Linea (Adulto, Adulto Extra, Niño, Niño Extra)
    /// </summary>
    public enum FoodLine : long { Adult = 31, ExtraAdult = 34, Child = 1321, ExtraChild = 2161 };

    /// <summary>
    /// DEAD.DEAD_TILI = Tipo de Linea (Adulto, Adulto Extra, Suplemento Individual)
    /// </summary>
    public enum AdultLine : long { Adult = 31, ExtraAdult = 34, SingleSupplement = 35 };

    /// <summary>
    /// DENI.DENI_TILI = Tipo Linea (Niño, Niño Extra, Suplemento Individual)
    /// </summary>
    public enum ChildLine : long { Child = 1321, ExtraChild = 2161, SingleSupplement = 35 };

    /// <summary>
    /// SETP.SETP_APLX = Forma de Aplicación: (Reservation, Paxs)
    /// </summary>
    public enum ApplyMethod : long { Reservation = 241, Paxs = 902 };

    /// <summary>
    /// SETP.SETP_TIME = Momento de Aplicación: (Entrada, Estadia)
    /// </summary>
    public enum ApplyMoment : long { Arrive = 301, Stay = 1771 };

    /// <summary>
    /// SETP.SETP_OPER = Operación Aplicada: (Adicionar, Descontar de Diaria)
    /// </summary>
    public enum ApplyOperation : long { Add = 1801, DailyDiscount = 1802 };

    /// <summary>
    /// LOOP.LOOP_CRIT = Tipo Evento (Critico, Error, Aviso, Información, Auditoria)
    /// </summary>
    public enum EventType : long { Critical = 331, Error = 332, Warning = 333, Information = 334, Auditing = 335 };

    /// <summary>
    /// MOVI.MOVI_TIMO = Tipo Movimiento (Credito, Debito)
    /// </summary>
    public enum EntrieType : long { Credit = 271, Debit = 272 };

    /// <summary>
    /// OBSE_ORIG = Origen Observacion (Reserva, Housekeeper, POS)
    /// </summary>
    public enum CommentOrigin : long { Reservation = 123, Housekeeper = 1743, POS = 2431 };

    /// <summary>
    /// Room Order (Door, Room Type, Room Block, Floor)
    /// </summary>
    public enum ERoomOrder : long { Door = 2921, RoomType = 2922, RoomBlock = 2923, Floor = 2924 };

    /// <summary>
    /// Check Price Action(Refuse, FixManualPrice)
    /// </summary>
    public enum CheckPriceActionType : long { RefuseReservation = 2950, FixManualPrice = 2951 };

    /// <summary>
    /// TNHT_CHBS.CHBS_DTYP = Tipo Deposito Booking Service (Percent, Fijo, by Nights)
    /// </summary>
    public enum DepositInfoType : long { Percent = 2292, Fijo = 2790, byNights = 2791 };

    /// <summary>
    /// TNHT_CHBS.CHBS_TYPE = Tipo Booking Service (GenericOTA, EGDS, Expedia, GuestCentric, BookAssist, SynXis, Other, Pegasus, Cubilis, WebBooking, CloudOne, TravelClick, SiteMinder, AvailPro, TripAdvisor, Booking, NeoBookings, EnzoKiosk, EGDSInatel, DBK, RateGain, Idiso, Omnibees, ParityRate, Fastbooking, TravelClickGMS, Airbnb, SynxisHTNG, Dingus)
    /// </summary>
    public enum BookingServiceType : long { GenericOTA = 2759, EGDS = 2760, Expedia = 2761, GuestCentric = 2762, BookAssist = 2763, SynXis = 2764, Other = 2765, Pegasus = 2766, Cubilis = 2767, WebBooking = 2768, CloudOne = 2769, TravelClick = 2770, SiteMinder = 2771, AvailPro = 2772, TripAdvisor = 2773, Booking = 2774, NeoBookings = 2775, EnzoKiosk = 2776, EGDSInatel = 2777, DBK = 2778, RateGain = 2779, Idiso = 2780, Omnibees = 2781, ParityRate = 2782, Fastbooking = 2783, TravelClickGMS = 2784, Airbnb = 2785, SynxisHTNG = 2786, Dingus = 2787 };

    /// <summary>
    /// Use Reservation Market Channel (UseDefault , UseMappingDefault, UseOriginal, PMSDefinitions, PMSDefinitionsIfNoExistMappingDefault)
    /// </summary>
    public enum UseReservationMarketChannel : long { UseDefault = 2952, UseMappingDefault = 2953, UseOriginal = 2954, PMSDefinitions = 2955, PMSDefinitionsIfNoExistMappingDefault = 2956 };

    /// <summary>
    /// Invoice Instruction Type (None , Entity, Client)
    /// </summary>
    public enum InvoiceInstructionType : long { None = 4, Entity = 211, Client = 11 };

    /// <summary>
    /// MOAD.MOAD_TRMA = Tipo Comida (Standard, Desayuno, Almuerzo, Cena)
    /// </summary>
    public enum AdditionalEntryType : long { Standard = 1171, Breakfast = 1261, Lunch = 1262, Dinner = 1263 };

    /// <summary>
    /// TNHT_MSUT.MSUT_ESTA = MessageState (Unread, Read, Replied)
    /// </summary>
    public enum MessageStatus : long { Unread = 3011, Read = 3012, Replied = 3013 };

    /// <summary>
    /// TNHT_ACRE.ORIG_TYPE = AccountsType  (GeneralIncome, GeneralPayment, GeneralDiscount, GeneralInAdvance, Incomes, Payments, Taxes, Discounts, Clients, ClientsInAdvance)
    /// </summary>
    public enum AccountsType : long { GeneralIncome = 3071, GeneralPayment = 3072, GeneralDiscount = 3073, GeneralInAdvance = 3074, Incomes = 3075, Payments = 2131, Taxes = 3076, Discounts = 2137, Clients = 3077, ClientsInAdvance = 3078 };

    /// <summary>
    /// TNHT_ACPL.ACPL_TYPE = AccountsSource  (Services, TaxRate, PaymentForms, CreditCardTypes, Clients, DiscountTypes, Entities)
    /// </summary>
    public enum AccountsSource : long { Services = 3431, TaxRate = 3432, CreditCardTypes = 3433, Clients = 3077, DiscountTypes = 3434, Entities = 211, PaymentForms = 3435, External = 3436, Departments = 3430 };

    /// <summary>
    /// TNHT_ACEX.ACEX_SRCE = RelationSource  (ServiceBySegment)
    /// </summary>
    public enum RelationSource : long { ServiceBySegment = 4001 };

    /// <summary>
    /// EMAIL_TYPE = EmailType (Proposal, Demo, Subscription, License Upgrade, Payment, Hotel Creation, Event, OfficeBox)
    /// </summary>
    public enum EmailType : long { Proposal = 3131, Demo = 3132, Subscription = 3133, LicenseUpgrade = 3134, Payment = 2131, HotelCreation = 3135, Event = 3136, OfficeBox = 3137 };

    /// <summary>
    /// TCFG_RESE.EXRT_MODE = Modo de aplicación de la tasa de cambio ( Daily, CheckIn )
    /// </summary>
    public enum ExchangeRateApplyMode : long { Daily = 2551, CheckIn = 602 };

    /// <summary>
    /// TNHT_BRECU.RECU_SEXO = ResourceGender (Masculino, Femenino, Ambos)
    /// </summary>
    public enum ResourceGender : long { Male = 751, Female = 752, Both = 3 };

    /// <summary>
    /// TNHT_CHBS.CHBS_EXPM = External Channel Export Mode (None, Inventory, Availability)
    /// </summary>
    public enum ExternalChannelExportMode : long { None = 4, Inventory = 3251, Availability = 3252 };

    /// <summary>
    /// TNHT_LOTE.LOTE_ESTA = NFSe Lote Status (NotFound, Processing, Error, Successful, Pending, EmailSended, Canceled)
    /// </summary>
    public enum ServiceFiscalNoteLoteStatus : long { NotFound = 3371, Processing = 3372, Error = 3373, Successful = 3374, Pending = 3375, EmailSent = 3376, Canceled = 1862 };

    /// <summary>
    /// TNHT_DOCF.DOCF_FIES = Document Fiscal Status Status (Manual, Processing, Error, Successful, Pending, PrinterPending, PrinterFailed, PrinterTimeout, Canceled)
    /// </summary>
    public enum DocumentFiscalStatus : long { Manual = 511, Processing = 3372, Error = 3373, Successful = 3374, Pending = 3375, PrinterPending = 3378, PrinterFailed = 3379, PrinterTimeout = 3380, Canceled = 1862 };

    /// <summary>
    /// TNHT_DOCF.DOCF_TIDE = ServiceFiscalNote (NFS-e), FiscalNote (NF-e), ConsumerFiscalNote (NFC-e), ModuleFiscal (MF-e), SATFiscal (SAT), FiscalPrinter (PAF-ECF), FiscalNote (Danfe)
    /// </summary>
    [Flags]
    public enum ElectronicDocumentType : long { None = 0, ServiceFiscalNote = 1, FiscalNote = 2, ConsumerFiscalNote = 4, ModuleFiscal = 8, SATFiscal = 16, FiscalPrinter = 32, DanfeFiscalNote = 64 }

    /// <summary>
    /// TNHT_INXX.XXXX_ESTA = Syncronization Status (Insert, Modify, Remove, Error, Pending, Successful)
    /// </summary>
    public enum SyncronizationStatus : long { Insert = 1501, Modify = 1502, Remove = 1507, Error = 3373, Pending = 3375, Successful = 3374 };

    /// <summary>
    /// /// ARTG.ARTG_TIPR = Tipo precio a aplicar (Always Rate, Always Manual, Combined)
    /// </summary>
    public enum ProductPriceType : long { AlwaysRate = 3461, AlwaysManual = 3463, Combined = 3465 };

    /// <summary>
    ///  GroupTypeFailure (Failure Types, Zone, Failure Origin)
    /// </summary>
    public enum GroupTypeFailure : long { FailureTypes = 3521, Zone = 3522, FailureOrigin = 3523 };

    /// <summary>
    /// AVAR.AVAR_PRIO = Prioridad Averia (Low, Medium, High)
    /// ALRT.ALRT_PRIO = Prioridad de la Alerta (Low, Medium, High)
    /// </summary>
    public enum PriorityType : long { Low = 571, Medium = 572, High = 573 };

    /// <summary>
    /// TNHT_ALRC.ALRC_TYPE (Users, Clients, Reservation)
    /// </summary>
    public enum AlertRecipientType : long { Users = 4232, Clients = 4233, Reservation = 4234 };

    /// <summary>
    /// TNHT_ALGR.ALGR_TYPE (Custom, Users, Clients, Reservations)
    /// </summary>
    public enum AlertGroupType : long { Custom = 4231, Users = 4232, Clients = 4233, Reservations = 4234 };

    #endregion
    #region PriceCriterion for Tariffs
    public enum PricesCriterion : long
    {
        Reservation = 241,
        Persons = 3161,
        Hours = 3168,
        PersonsByHours = 3163,
        Daily = 481,
        PersonsPerDay = 3162,
        Units = 3164,
        Times = 3165,
        ReservationTimes = 3167,
        PersonsTimes = 3166
    };
    #endregion

    #region Condo
    /// <summary>
    /// TNHT_ALOJ.AREA_UNIT (Metro Cuadrado, Pies Cuadrado)
    /// </summary>
    public enum MeasurementArea : long { SqMeters = 3661, SqFeets = 3662 };

    /// <summary>
    /// TNHT_ALOCO.ALCO_TYPE (Bedroom, Bathroom, etc.)
    /// </summary>
    public enum FloorDivision : long { Bedroom = 3681, Bathroom = 3682, DiningRoom = 3683, Kitchen = 3684, Den = 3685, LivingRoom = 3686, FamilyRoom = 3687, Library = 3688, Office = 3689, Balcony = 3690, Other = 3691 };

    /// <summary>
    /// TNTH_BUIL.BUIL_TYPE (Condo, Residential, etc.)
    /// </summary>
    public enum BuildingType : long { Condo = 3751, Residential = 3752, MixedResidential = 3753, Commercial = 3754, SocialHousing = 3755, SeniorHousing = 3756, Parking = 3757, Storage = 3758, Association = 3759, Military = 3760, Other = 3761 };

    /// <summary>
    /// TNTH_BUIL.BUIL_CONS (Brick, Wood, etc.)
    /// </summary>
    public enum BuildingConstruction : long { Brick = 3791, Wood = 3792, Block = 3793, Panel = 3794, Other = 3795 };

    /// <summary>
    /// TNTH_BUIL.BUIL_FLOOR (Hardwood, Tile, etc.)
    /// </summary>
    public enum FloorType : long { Hardwood = 3821, Tile = 3822, Laminate = 3823, Carpet = 3824, Mixed = 3825, Other = 3826 };

    /// <summary>
    /// TNTH_BUIL.BUIL_WATER (Municipal, Well, ect.)
    /// </summary>
    public enum WaterSupply : long { Municipal = 3851, PrivateWell = 3852, CommunityWell = 3853, Other = 3854 };

    /// <summary>
    /// TNHT_BUUT.BUIL_UTTY (Gas; Water; Hydro; Cable; Internet)
    /// </summary>
    public enum UtilityType : long { Gas = 3881, Water = 3882, Hydro = 3883, Cable = 3884, Internet = 3885 };

    /// <summary>
    /// TNHT_OMAN.OMAN_COTY (None; Fixed Value; Commission %; etc.)
    /// </summary>
    public enum OwnerManagementCommissionType : long { None = 3901, FixedValue = 3902, CommissionPercent = 3903, RateCommissionPercent = 3904, ServiceRatePercent = 3905 };

    /// <summary>
    /// tnht_omch.omch_stat (Room State Required -> enum (Always, Occuped, Vacant, Check-In Date, Check-Out Date, Predefined))
    /// </summary>
    public enum RoomStateRequired : long { Always = 6, Occupied = 3962, Vacant = 3963, CheckInDate = 3964, CheckOutDate = 3965, Predefined = 3966 };

    /// <summary>
    /// tnht_mcon.mcon_timo (Condo Entrie Type -> enum (+ Owner, - Owner))
    /// </summary>
    public enum CondoEntrieType : long { PlusOwner = 3991, MinusOwner = 3992 };

    /// <summary>
    /// tnht_mcon.mcon_timo (Condo Entrie Type -> enum (+ Owner, - Owner))
    /// </summary>
    public enum CondoCommissionPeriodType : long { Daily = 4000, Weekly = 4001, Monthly = 4002, Yearly = 4003 };

    /// <summary>
    /// TNHT_MCON.MCON_TICO (General, Condominuim, Extras, Historic)
    /// </summary>
    public enum CondoAccountType : long { General = 4641, Condominium = 4644, Extras = 4648, Historic = 4650 };

    /// <summary>
    /// TNHT_BGIT.BGIT_BITY (Title, Charges)
    /// </summary>
    public enum BudgetItemType : long { Title = 4618, Charges = 4660 };

    /// <summary>
    /// tnht_oman.oman_type (Condominium, Exploration, TimeSharing)
    /// </summary>
    public enum CondoOwnershipManagementType : long { Condominium = 4731, Exploration = 4735, TimeSharing = 4740 };

    /// <summary>
    /// tnht_mete.mete_type (Water, Energy, Gas) 
    /// </summary>
    public enum MeterType : long { Water = 4745, Energy = 4750, Gas = 4755 };

    /// <summary>
    /// TNHT_TPTP.TPTP_TITL = Titulo Template EMails (Notification, Circular, Warning)
    /// </summary>
    public enum EMailTemplateTitleCondo : long { Notification = 4980, Circular = 4985, Warning = 4990 };

    /// <summary>
    /// (Monthly, Biannual, Annual)
    /// </summary>
    public enum ChargeYearCriteria : long { Monthly = 5031, Biannual = 5032, Annual = 5033 };

    /// <summary>
    /// (Biannual, Annual)
    /// </summary>
    public enum PenaltyControlMode : long { Biannual = 5032, Annual = 5033 };

    /// <summary>
    /// (First day, Last day)
    /// </summary>
    public enum ChargeDayCriteria : long { FirstDay = 5041, LastDay = 5042 };

    /// <summary>
    /// (First month, Last month)
    /// </summary>
    public enum ChargeMonthCriteria : long { FirstMonth = 5051, LastMonth = 5052 };

    /// <summary>
    /// (Jan..Dec)
    /// </summary>
    public enum ChargeMonthCalendar : long { Jan = 5071, Feb = 5072, Mar = 5073, Apr = 5074, May = 5075, Jun = 5076, Jul = 5077, Aug = 5078, Sep = 5079, Oct = 5080, Nov = 5081, Dec = 5082 };

    /// <summary>
    /// BAR Price Calculation Method
    /// </summary>
    public enum BARCalculationMethod : long { OnTheBooks = 5010, BudgetForecast = 5011, YieldOptimization = 5012, OnTheBookPlusPickup = 5013 };

    #endregion
    #region Housekeeping

    /// <summary>
    /// tnht_hkem.hkem_tipo
    /// </summary>
    public enum HousekeeperType : long { RoomMaid = 4701, Covershifts = 4704, RoomMaidAfternoon = 4708, Others = 4712 };

    #endregion
    #region Account

    /// <summary>
    /// TNHT_MOCO.TIMO_PK (Invoice, CreditNote, DebitNote, Payment, OverPayment, BadDebit, AdjustBaseCurrencyMin, AdjustBaseCurrencyMax, CreditCardInstallment, Reservation)
    /// </summary>
    public enum AccountMovementType : long { Invoice = 5, CreditNote = 9, DebitNote = 10, Payment = 15, OverPayment = 20, BadDebit = 21, AdjustBaseCurrencyMin = 32, AdjustBaseCurrencyMax = 31, CreditCardInstallment = 32, Reservation = 33 };

    /// <summary>
    /// TNHT_BILLET.INFI_PK (SICREDI, ITAU)
    /// </summary>
    public enum FinancialInstitution : long { SICREDI = 5243, ITAU = 5244, SANTANDER = 5245, BRADESCO = 5246 }

    #endregion

    #region PMS Financial Budget

    public enum Indicator : long
    {
        RoomsInventory = 4770
        , RoomsInactive = 4775
        , RoomsOccupied = 4780
        , RoomsCheckedIn = 4785
        , RoomsCheckedOut = 4790
        , RoomsComplimentary = 4795
        , RoomsHouseUse = 4800
        , RoomsDayUse = 4805
        , RoomsNoShows = 4810
        , RoomsCanceled = 4815
        , RoomsWalkIn = 4820
        , RoomsOverbooked = 4825
        , GuestsInHouse = 4830
        , GuestsCheckedIn = 4835
        , GuestsCheckedOut = 4840
        , GuestsComplimentary = 4845
        , GuestsHouseUse = 4850
        , GuestsDayUse = 4855
        , GuestsNoShows = 4860
        , GuestsCanceled = 4865
        , GuestsWalkIn = 4870
        , GuestsOverbooked = 4875
        , UnexpectedDepartures = 4880
        , NrExtraBedsOccupied = 4885
        , NrCotsOccupied = 4890
        , NrBreakfast = 4895
        , NrLunch = 4900
        , NrDinner = 4905
        , OccupancyInventory = 4910
        , OccupancyActiveInventory = 4915
        , AverageRoomRate = 4920
        , TotalIncome = 4925
        , RoomsIncome = 4930
        , MealsIncome = 4935
        , DrinksIncome = 4940
        , OthersIncome = 4945
        , TotalInvoiced = 4950
        , RoomsInvoiced = 4955
        , MealsInvoiced = 4960
        , DrinksInvoiced = 4965
        , OthersInvoiced = 4970
    }

    #endregion

    /// <summary>
    /// TNHT_REAT.REAT_EXCL
    /// </summary>
    public enum ExcludeDay : long { None = 4, EntryDay = 1411, OutDay = 1412, BothDays = 3 };

    /// <summary>
    /// TNHT_REAT.REAT_FROM
    /// </summary>
    public enum DaysFrom : long { Arrive = 301, Departure = 302 };

    /// <summary>
    /// TCFG_RECU.PARA_CHRS (ChangeRoomStatusAt -> enum (NightAuditor, CheckOut, CheckIn, Always, Never, CheckInNightAuditor, CheckOutNightAuditor))
    /// </summary>
    public enum ChangeRoomStatusAt : long { NightAuditor = 1621, CheckOut = 606, CheckIn = 602, Always = 6, Never = 5, CheckInAndNightAuditor = 1532, CheckOutAndNightAuditor = 1533 };

    /// <summary>
    /// TCFG_RECU.CKIN_DRTY ( CheckInWithDirtyRoom -> enum (Allow, Warning Message, Deny))
    /// </summary>
    public enum CheckInWithDirtyRoom : long { Allow = 5201, WarningMessage = 5202, Deny = 5203 };

    /// <summary>
    /// TCFG_HOTE.PKIO_KYEM (KeyEmissionType -> enum (Reservation, Guest))
    /// </summary>
    public enum KeyEmissionType : long { Guest = 244, Reservation = 241 };

    /// <summary>
    /// TCFG_RESE.RESE_ACAN (ReserationAutomaticCancellation-> Never, Pending Credits,Pending Debits,Always)
    /// </summary>
    public enum ReservationAutomaticCancellation : long { Never = 5, PendingCredits = 4022, PendingDebits = 4023, Always = 6 };

    /// <summary>
    /// TCFG_HOTE.HOTE_TYPE (HotelType->  Hotel, Hostel)
    /// </summary>
    public enum HotelType : long { Hotel = 4051, Hostel = 4052 };

    /// <summary>
    /// NO FIELD = Tipo Cobro Factura (Payment, Transfer, Pending,  Free Title)
    /// </summary>
    public enum PaymentMode : long { Payment = 2131, AccountTransfer = 2132, Pending = 1474, FreeTitle = 2138, InternalAccountSettlement = 2142 }

    #region Documents Operations

    [Flags]
    public enum DocumentsOper
    {
        None = 0,
        View = 1,
        Cancel = 2,
        ShowEntries = 4,
        PrintOriginal = 8,
        Print = 16,
        CreateCreditNote = 32,
        CreateDebitNote = 64,
        FinalConsumer = 128,
        SaftPt = 256,
        SendEmail = 512,
        GetFiscalRequest = 1024,
        GetFiscalResponse = 2048,
        SendFiscalDocument = 4096,
        GetFiscalDocument = 8192,
        BrowseFiscalDocument = 16384,
        Export = 32768,
        ApplySecurityDeposit = 65536,
        Regularize = 131072
    };

    #endregion
    #region Entries Operations

    [Flags]
    public enum EntrieOper
    {
        None = 0,
        View = 1,
        Modify = 2,
        Cancel = 4,
        Transfer = 8,
        Split = 16,
        Receipt = 32,
        Refund = 64,
        Statement = 128,
        Penalty = 256
    };

    #endregion
    /// <summary>
    /// TIAL.TIAL_IVIN = Impuesto Incluido (Por defecto, Incluido, No Incluido)
    /// </summary>
    public enum TaxIncludedMode : long { ByDefault = 2011, Included = 2012, NotIncluded = 2013 };

    /// <summary>
    /// TFIO.TFIO_TYOP = ClearOne Operation type (Sales, Sales Annul ,Devolution, Devolution Annul)
    /// </summary>
    public enum TransferCCOperation : long { Sales = 3341, SalesAnnul = 3342, Devolution = 2135, DevolutionAnnul = 3343 };

    /// <summary>
    /// LIOC.LIOC_SEXO = Sexo en multiocupación (Masculino, Femenino, No interesa)
    /// </summary>
    public enum ReservationGender : long { Male = 751, Female = 752, None = 4 };

    /// <summary>
    /// LIOC.LIOC_PRMU = Tipo Precio Multi-Ocupacion (Base, Extra)
    /// </summary>
    public enum ShareRoomPriceType : long { Base = 1231, Extra = 1232 };

    /// <summary>
    /// CTNP_TIER = Tipo Error (Cuenta Cerrada, Error, Ext. inexistente, Hab. inexistente, Hab. Libre)
    /// </summary>
    public enum CallErrorType : long { ClosedAccount = 2521, Error = 2522, ExtNotExist = 2523, RoomNotExist = 2524, VacantRoom = 2525 };

    /// <summary>
    /// NO FIELD = Tipo Movimiento (Crédito-Débito, Credito, Debito)
    /// </summary>
    public enum FilterEntrieType : long { Credit = 271, Debit = 272, CreditDebit = 2261 };

    /// <summary>
    /// DESP.DESP_STAT = Estado (Solicitada, Anulada, Ejecutada, Programada, Sin respuesta, Solicitud de Cancelamiento)
    /// </summary>
    public enum WakeUpCallState : long { Requested = 1861, Cancelled = 1862, Executed = 1863, Scheduled = 1864, NoResponse = 1865, CancelRequested = 1866 };

    /// <summary>
    /// DESP.DESP_DETE = Tipo llamada (Telefono, PayTv, Otra)
    /// </summary>
    public enum WakeUpCallTypeCall : long { Phone = 1831, PayTv = 1832, Other = 13 };

    #region External Channel Price Verification

    public enum ExternalPriceMismatchVerify { Ignore = 0, Error = 1, UseManualPrice = 2 }

    #endregion

    #region Price Calculation

    /// <summary>
    /// Pensiones
    /// AP, EP, MP, PC, TI
    /// </summary>
    public enum PensionBoard { AP = 1, EP = 2, MP = 3, PC = 4, TI = 5 };

    #endregion

    #region Reservation Operations

    [Flags]
    public enum ReservationOper
    {
        None = 0,
        CheckIn = 1,
        UndoCheckIn = 2,
        CheckOut = 4,
        UndoCheckOut = 8,
        Cancel = 16,
        UndoCancel = 32,
        NoShow = 64,
        UndoNoShow = 128,
        Overbooking = 256,
        UndoOverbooking = 512,
        View = 1024,
        CheckInGuest = 2048,
        CheckOutGuest = 4096,
        RegisterGuest = 8192,
        CloseAccount = 16384,
        ReOpenAccount = 32768,
        ViewAccount = 65536,
        Entries = 131072,
        InvoiceOrTicket = 262144,
        WalkIn = 524288,
        Confirm = 1048576,
        WaitingList = 2097152,
        Copy = 4194304,
        KeyPass = 8388608,
        Transfer = 16777216,
        ScheduledChangeRoom = 33554432
    };

    /// <summary>
    /// TCFG_RESE.RESE_APTO = (Both, Reservation, Group)
    /// </summary>
    public enum ReservationRulesApplyTo : long { Both = 3, Reservation = 241, Group = 242 };


    #endregion

    /// <summary>
    /// RECO.RECO_TIPO = Tipo Contacto de la Reserva (Contacto, Vendedor, Asesor)
    /// </summary>
    public enum ReservationContactType : long { Contact = 991, Salesman = 992, Adviser = 993 };

    /// <summary>
    /// PTRA.PTRA_TYPE =  Workstation Types (Keys, Card Reader, Document Scanner)
    /// </summary>
    public enum WorkstationTypes : long { Keys = 3491, CardReader = 3492, DocumentScanner = 3493, PaymentTerminal = 3494 };

    /// <summary>
    /// TNHT_TPTP.TPTP_TITL = Titulo Template eMails (Basic, Pre Check-in, Post Check-out, Post Travel, eInvoice, Notification, Circular, Warning)
    /// </summary>
    public enum EMailTemplateTitle : long { Basic = 2981, PreCheckIn = 2982, PostCheckIn = 2983, PostTravel = 2984, eInvoice = 5012, Notification = 4980, Circular = 4985, Warning = 4990 };

    /// <summary>
    /// TNHT_PKMS.PKMS_TYPE (MessageNewGesType->  Call, Message)
    /// </summary>
    public enum MessageNewGesType : long { Call = 4110, Message = 4111 };

    /// <summary>
    /// ACC (RegularizationType->  Total regularized, Partial regularized, Not regularized, Cancelled)
    /// </summary>
    public enum RegularizationType : long { TotalRegularized = 4171, PartialRegularized = 4172, NotRegularized = 4173, Cancelled = 605 };

    /// <summary>
    /// Type Of scanned Documents
    /// </summary>
    public enum ScanDocType : long { DNI = 0, Passport = 1, DriverLicence = 2 };

    /// <summary>
    /// TNHT_HORE.REGI_PURP (Vacation, Business, Convention, Relatives, Studies, Religion, Health, Shopping, Other)
    /// </summary>
    public enum TravelPurposes : long { Vacation = 4261, Business = 4262, Convention = 4263, Relatives = 4264, Studies = 4265, Religion = 4266, Health = 4267, Shopping = 4268, Other = 4297 };

    /// <summary>
    /// TNHT_HORE.REGI_ARBY (Plane, Car, Bus, Motorcycle, Ship, Train, Other)
    /// </summary>
    public enum ArrivingBy : long { Plane = 4291, Car = 4292, Bus = 4293, Motorcycle = 4294, Ship = 4295, Train = 4296, Other = 4297 };



    /// <summary>
    /// (Yes, No)
    /// </summary>
    public enum YesNo : long { Yes = 17, No = 18 };

    /// <summary>
    /// (Category, Rate, Product, Room Type)
    /// </summary>
    public enum RestrictionLevel : long { Hotel = 4051, Category = 4321, Rate = 4323, Product = 4325, RoomType = 4327 };

    /// <summary>
    /// (Stop, Change Type, Remove Room)
    /// </summary>
    public enum ChangeRoomAction : long { Stop = 4351, ChangeType = 4352, RemoveRoom = 4353 };

    /// <summary>
    /// TCFG_HOTE.GTAX_APTO (City Tax -> Apply To: All Services, Daily Rate, Room Rate, Just Services)
    /// </summary>
    public enum CityTaxApplyTo : long { AllServices = 841, DailyRate = 4382, RoomRate = 4385, JustServices = 4386 };

    /// <summary>
    /// (Account Transfer, Discount, Free Title, Tax Retention, Internal Account Settlement, Tip, City Tax, Supplement, Additional, EntranceFee)
    /// </summary>
    public enum SpecialLineType : long { AccountTransfer = 2132, Discount = 2137, FreeTitle = 2138, TaxRetention = 2141, InternalAccountSettlement = 2142, RetentionOverNet = 2143, Tip = 4403, CityTax = 4407, Supplement = 4408, Additional = 4551, EntranceFee = 4554 };

    /// <summary>
    /// (Operacion de Stocks)
    /// </summary>
    public enum StockOperationType : long { Sales = 3341, Devolution = 2135, Shopping = 4268, AccountTransfer = 2132 }

    /// <summary>
    /// (Dias de la semana)
    /// </summary>
    public enum WholeWeek : long { Sunday = 1, Monday = 2, Thursday = 3, Wednesday = 4, Tuesday = 5, Friday = 7, Saturday = 6 }

    /// <summary>
    /// TNHT_CHBS.CHBS_GRUP
    /// </summary>
    public enum EBookingGroupIntegrationType : long { None = 4, AsComments = 1, AsGroup = 2 };

    /// <summary>
    /// (Operacion de Condo Polling Duplex)
    /// </summary>
    public enum TimeshareOperation : long { Booking = 1, Payments = 2, Charges = 3 }

    /// <summary>
    /// TNHT_CAPD.CAPD_TYPE
    /// </summary>
    public enum CancellationLineType : long { Arrive = 301, Stay = 1503, Release = 4431 };

    /// <summary>
    /// TNHT_CAPD.CAPD_CHRG
    /// </summary>
    public enum CancellationChargeType : long { FirstNight = 4461, NextNight = 4465, FullStay = 4468 };

    /// <summary>
    /// TNHT_EXIO.EXIO_TYPE
    /// </summary>
    public enum ExternalDeviceType : long { PowerYourRoom = 3436, Reservations = 3437, Accounting = 3438, AccountingNC = 3439, Bavel = 3440, Str = 3441, Revinate = 3442 }

    public enum EBookingTravelAgencyIntegrationType : long { None = 4, AsComments = 1, AsTravelAgency = 2 };

    public enum EBookingCompanyIntegrationType : long { None = 4, AsComments = 1, AsEntity = 2 };

    public enum EActionWhenModifyWithoutInsert : long { NotifyError = 2824, InsertAsNew = 1 };

    public enum EConfirmationState : long { Confirmed = 1023, WaitingList = 1022 };

    public enum EAttentionMappingType : long { None = 4, ChannelRoomType = 1, }

    /// <summary>
    /// TNHT_CHBS.CHBS_PTYP = Payment Integration Type (None, Advance Deposit, Invoiced Advance Deposit)
    /// </summary>
    public enum EBookingChannelPaymentsIntegrationType : long { None = 4, AsAdvancedDeposit = 2822, AsInvoiceAdvancedDeposit = 2823 };

    /// <summary>
    /// TNHT_CHBS.CHBS_PRER = Price Error Action (Notify Error, Distribute Difference)
    /// </summary>
    public enum EActionWhenPriceError : long { NotifyError = 2824, DistributeDifference = 2825 };

    /// <summary>
    /// TNHT_CHBS.CHBS_TPER = Total Price Error Action (Notify Error, Roomstay Total, Reservation General Total)
    /// </summary>
    public enum EActionWhenTotalPriceError : long { NotifyError = 2824, RoomStaysTotal = 2826, ReservationGeneralTotal = 2846 };

    /// <summary>
    /// TNHT_CHBS.CHBS_SYTY = Synchronization Type (SyncAtomic, SyncByGroup)
    /// </summary>
    public enum ESynchronizationType : long { SyncAtomic = 2827, SyncByGroup = 2828 };

    /// <summary>
    /// TNHT_CHBS.CHBS_LSTY = Expedia Export LOST Type (Arrival, StayThrough)
    /// </summary>
    public enum EExpediaExportLOSType : long { Arrival = 2829, StayThrough = 2830 };

    /// <summary>
    /// TNHT_CHBS.CHBS_PRTY = Expedia Export Price Type (Occupancy, PerDay, PerGuest, PerRoom)
    /// </summary>
    public enum EExpediaExportPriceType : long { Occupancy = 2831, PerDay = 2832, PerGuest = 2833, PerRoom = 2834 };

    /// <summary>
    /// TNHT_CHBS.CHBS_FETY = Fastbooking Export Type (RoomRateBasedAllotment, RoomBasedAllotment)
    /// </summary>
    public enum EFastbookingExportType : long { RoomRateBasedAllotment = 2835, RoomBasedAllotment = 2836 };

    /// <summary>
    /// TNHT_CHBS.CHBS_ORMM = Origin Mapping Type (None, ChannelOrigin, ChannelMarketOrigin, ChannelRoomType, ChannelRate)
    /// </summary>
    public enum EOriginMappingType : long { None = 4, ChannelOrigin = 2838, ChannelMarketOrigin = 2839, ChannelRoomType = 2841, ChannelRate = 2845 };

    /// <summary>
    /// TNHT_CHBS.CHBS_SEMM = Segment Mapping Type (None, ChannelSegment, ChannelOrigin, ChannelRoomType, ChannelMarketOrigin, ChannelRate)
    /// </summary>
    public enum ESegmentMappingType : long { None = 4, ChannelSegment = 2840, ChannelOrigin = 2838, ChannelRoomType = 2841, ChannelMarketOrigin = 2839, ChannelRate = 2845 };

    /// <summary>
    /// TNHT_CHBS.CHBS_ADDS = Use Additionals (None, AsIncludedServices)
    /// </summary>
    public enum EUseAdditionals : long { None = 4, AsIncludedServices = 2837 };

    /// <summary>
    /// TNHT_CHBS.CHBS_ANPR = Action when no prices (NotifyError, AllowWithoutPrices)
    /// </summary>
    public enum EActionWhenNoPricesInformed : long { NotifyError = 2824, AllowWithoutPrices = 2842 };

    /// <summary>
    /// TNHT_CHBS.CHBS_AITX = Action when no taxes  (NotifyError, UseDefinedDefinitions)
    /// </summary>
    public enum EActionWhenInvalidTaxInformed : long { NotifyError = 2824, UseDefinedDefinitions = 2843 };

    /// <summary>
    /// TNHT_CHBS.CHBS_GEML = Guest email integration (None, AsEmail)
    /// </summary>
    public enum EBookingGuestEmailIntegrationType : long { None = 4, AsEmail = 1892 };

    /// <summary>
    /// TNHT_CHBS.CHBS_AIDV = Action invalid data validation (NotifyError, IntegrateWithWarnings)
    /// </summary>
    public enum EActionWhenPMSDataValidationError : long { NotifyError = 2824, IntegrateWithWarnings = 2844 };

    /// <summary>
    /// TNHT_BIND.BIND_TYPE (Manual, Calculated, Title, Separator)
    /// </summary>
    public enum BudgetLineType : long { Manual = 4611, Calculated = 4614, Title = 4618, Separator = 4623 };

    /// <summary>
    /// TNHT_TPRQ.TPRQ_ESTA (Requested, Readed, FinishedSuccessfully, FinishedFailed, TimeOut)
    /// </summary>
    public enum TEFPaymentState : long { Requested = 4670, Readed = 4671, FinishedSuccessfully = 4672, FinishedFailed = 4673, Timeout = 4674 };

    /// <summary>
    /// TNHT_HOTE.HOTE_TIEM (LegalPerson, NaturalPerson)
    /// </summary>
    public enum FiscalCompanyType : long { LegalPerson = 5251, NaturalPerson = 5252 };

    public enum ClientInformationCheckAt : long { CheckOut = 606, CheckIn = 602, Always = 6, Never = 5 };

    /// <summary>
    /// TNHT_TICK.TICK_TYST = (Gross, Net)
    /// </summary>
    public enum KindSubTotal : long { Gross = 451, Net = 452 };

    /// <summary>
    /// TNHT_LIRE.LIRE_CKIN = (Pending, EmailSent, Successful)
    /// </summary>
    public enum CheckInOnlineStatus : long { Pending = 3375, EmailSent = 3376, Successful = 3374 };

    public enum RegimeEspecialTributacao : long { Microempresa_Municipal = 1, Estimativa = 2, Sociedade_de_profissionais = 3, Cooperativa = 4, Microempresário_Individual_MEI = 5, Microempresário_e_Empresa_de_PequenoPorte_MEEPP = 6 };

    public enum ExigibilidadeIss : long { Exigivel = 1, Nao_incidencia = 2, Isencao = 3, Exportacao = 4, Imunidade = 5, Suspensa_Decisao_Judicial = 6, Suspensa_Preoceso_Administrativo = 7 };

    public enum IntegrationInterface : long { None = 1, TOTVS = 2, CMFlex = 3, CIGAM = 4 };

    public enum DataType : int { LiveData = 1, HistoricalData = 2 };

    /// <summary>
    /// TNHT_BILLET.WALLETTYPE = (Carteira Cobrança Simples = 1, Carteira Cobrança Vinculada = 2, Carteira Cobrança Caucionada = 3, Carteira Cobrança Descontada = 4, Carteira Cobrança Vendor = 5)
    /// </summary>
    public enum WalletType : long { Cobranca_Simples = 1, Cobranca_Vinculada = 2, Cobranca_Caucionada = 3, Cobranca_Descontada = 4, Cobranca_Vendor = 5 };

    /// <summary>
    /// TNHT_BILLET.FILETYPE = (CNAB240 = 0, CNAB400 = 1)
    /// </summary>
    public enum FileType : long { CNAB_240 = 0, CNAB_400 = 1 };

    public enum ExportFileType : long { PdfFile = 0, DeliveryFile = 1, EmailFile = 2 };

    /// <summary>
    /// TNHT_BILLET.REGTYPE = (Com Registro = 1, Sem Registro = 2, Debito Automático = 3)
    /// </summary>
    public enum RegisterType : long { Com_Registro = 1, Sem_Registro = 2, Debito_Automatico = 3 };

    /// <summary>
    /// TNHT_BILLET.PRINTTYPE = (Banco = 1, Empresa = 2, Banco PreEmite Cliente Complementa = 3, Banco Reemite = 4, Banco Não Reemite = 5, Banco Emitente Aberta = 7, Banco Emitente Auto Envolopavel = 8)
    /// </summary>
    public enum PrintType : long { Banco = 1, Empresa = 2, Banco_PreEmite_Cliente_Complementa = 3, Banco_Reemite = 4, Banco_Nao_Reemite = 5, Banco_Emitente_Aberta = 7, Banco_Emitente_AutoEnvolopavel = 8 };

    /// <summary>
    /// TNHT_BILLET.DOCTYPE = (Tradicional = 1, Escritural = 2)
    /// </summary>
    public enum DocumentType : long { Tradicional = 1, Escritural = 2 };

    /// <summary>
    /// TNHT_IPOS.IPOS_TIGR = (SeatsSeparators, SeparatorsSeats)
    /// </summary>
    public enum TicketProductGrouping : long { SeatsSeparators = 5671, SeparatorsSeats = 5672 };

    #region INE

    [Flags]
    public enum Months : long { None = 0, January = 1, February = 2, March = 4, April = 8, May = 16, June = 32, July = 64, August = 128, September = 256, October = 512, November = 1024, December = 2048 };

    #endregion
}