﻿using System;

namespace NewHotel.Contracts
{
    public static class Res
    {
        #region General

        private static readonly int ErrorMask = 0;
        public static int UnhandledException = ErrorMask.GetResId(0);
        public static int ChildRecordFound = ErrorMask.GetResId(99);
        public static int DatabaseStaleData = ErrorMask.GetResId(100);
        public static int DatabaseNoDataFound = ErrorMask.GetResId(101);
        
        public static int ErrorCreateUserDatabase = ErrorMask.GetResId(103);
        public static int SendEmailError = ErrorMask.GetResId(104);
        public static int DontHaveDocuments = ErrorMask.GetResId(105);
        public static int DontHaveGuestToExport = ErrorMask.GetResId(106);
        public static int ErrorInstallationReportFolder = ErrorMask.GetResId(107);
        public static int SchemasReplicationFailed = ErrorMask.GetResId(108);
        public static int DatabaseDuplicatedRecord = ErrorMask.GetResId(109);

        #endregion
        #region  Year opening

        private static readonly int YearOpening = 1;
        public static readonly int YearOpeningError = YearOpening.GetResId(1);

        #endregion
        #region Doc serie

        private static readonly int DocSerie = 2;
        public static readonly int CannotGetSerieNumber = DocSerie.GetResId(1);

        #endregion
        #region Reservation

        private static readonly int Reservation = 3;
        public static readonly int GuaranteeDaysNotMatchEntity = Reservation.GetResId(1);
        public static readonly int EntityTaxSchemaNotMatch = Reservation.GetResId(2);
        public static readonly int InvalidPersonsQuantity = Reservation.GetResId(3);
        public static readonly int MovementsPendingOfInvoice = Reservation.GetResId(4);
        public static readonly int MultioucpationNotAvailable = Reservation.GetResId(5);
        public static readonly int MaxPersonsExceeded = Reservation.GetResId(6);
        public static readonly int TypeLineUndefinedPrice = Reservation.GetResId(7);
        public static readonly int ExchangeRateNotDefined = Reservation.GetResId(8);
        public static readonly int InvalidExchangeRate = Reservation.GetResId(9);
        public static readonly int PeriodPriceNotDefined = Reservation.GetResId(10);
        public static readonly int RoomTypeMaxPersonsExceeded = Reservation.GetResId(11);
        public static readonly int NoPriceInformationAtDate = Reservation.GetResId(12);
        public static readonly int NoAdultsDefinedAtDate = Reservation.GetResId(13);
        public static readonly int InvalidArrivalDate = Reservation.GetResId(14);
        public static readonly int InvalidStatus = Reservation.GetResId(15);
        public static readonly int InvalidGuaranteeDate = Reservation.GetResId(16);
        public static readonly int GuaranteeDaysLessThanMin = Reservation.GetResId(17);
        public static readonly int GuaranteeDaysGreaterThanMax = Reservation.GetResId(18);
        public static readonly int CheckInNotAuthorized = Reservation.GetResId(19);
        public static readonly int NoShowResOutOfGaranted = Reservation.GetResId(20);
        public static readonly int UnassignedRoom = Reservation.GetResId(21);
        public static readonly int OccupiedRoom = Reservation.GetResId(22);
        public static readonly int GuestNumNotMatchRegistered = Reservation.GetResId(23);
        public static readonly int InvalidCheckInDate = Reservation.GetResId(24);
        public static readonly int UnexpectedDepartureTypeNotSpecified = Reservation.GetResId(25);
        public static readonly int ReservationRoomInUse = Reservation.GetResId(26);
        public static readonly int InvalidDepartureDate = Reservation.GetResId(27);
        public static readonly int InvalidOccupationLineStatus = Reservation.GetResId(28);
        public static readonly int UndefinedEntityPrice = Reservation.GetResId(29);
        public static readonly int InvalidContractDefinition = Reservation.GetResId(30);
        public static readonly int InvalidPriceRate = Reservation.GetResId(31);
        public static readonly int InvalidPrice = Reservation.GetResId(32);
        public static readonly int UndefinedPrice = Reservation.GetResId(33);
        public static readonly int InvalidOverbookingInformation = Reservation.GetResId(34);
        public static readonly int InvalidDisscountInformation = Reservation.GetResId(35);
        public static readonly int UndefinedBaseCurrency = Reservation.GetResId(36);
        public static readonly int UndefinedCurrency = Reservation.GetResId(37);
        public static readonly int UndefinedRoomType = Reservation.GetResId(38);
        public static readonly int UndefinedGuests = Reservation.GetResId(39);
        public static readonly int ReservationOutOfRelease = Reservation.GetResId(40);
        public static readonly int ReservationDaysWithoutAllotment = Reservation.GetResId(41);
        public static readonly int RoomTypeNotFoundInAllotment = Reservation.GetResId(42);
        public static readonly int AllotmentAlarmTriggered = Reservation.GetResId(43);
        public static readonly int ReservationRestrictionsForPeriod = Reservation.GetResId(44);
        public static readonly int ReservationStayRestrictionsForPeriod = Reservation.GetResId(45);
        public static readonly int DepartureBeforeArrivalDate = Reservation.GetResId(46);
        public static readonly int ArrivalBeforeWorkingDate = Reservation.GetResId(47);
        public static readonly int DepartureBeforeWorkingDate = Reservation.GetResId(48);
        public static readonly int CannotRemEntrieWithDocAssciated = Reservation.GetResId(49);
        public static readonly int CannotChangeRoom = Reservation.GetResId(50);
        public static readonly int InvalidPersonsQuantitySettings = Reservation.GetResId(51);
        public static readonly int InvalidArrivalDateForDayUse = Reservation.GetResId(52);
        public static readonly int PendingInvoice = Reservation.GetResId(53);
        public static readonly int AssignedRoom = Reservation.GetResId(54);
        public static readonly int InvalidExportOfficialConfiguration = Reservation.GetResId(55);
        public static readonly int UndefinedDocumentGuests = Reservation.GetResId(56);
        public static readonly int UndefinedDocumentHolderGuests = Reservation.GetResId(57);
        public static readonly int ReservationOnExtraAllotment = Reservation.GetResId(58);
        public static readonly int InvalidCurrencyAtDate = Reservation.GetResId(59);
        public static readonly int AmbiguousPriceInformationAtDate = Reservation.GetResId(60);
        public static readonly int MultipleCurrenciesNotAllowed = Reservation.GetResId(61);
        public static readonly int ReservationClosed = Reservation.GetResId(62);
        public static readonly int ReservationClosedToArrival = Reservation.GetResId(63);
        public static readonly int ReservationClosedToDeparture = Reservation.GetResId(64);
        public static readonly int ReservationLOSOutOfRangeToArrival = Reservation.GetResId(65);
        public static readonly int ReservationLOSOutOfRangeThrough = Reservation.GetResId(66);
        public static readonly int ReservationDatesModificationNotAllowed = Reservation.GetResId(67);
        public static readonly int ReservationReservedRoomTypeModificationNotAllowed = Reservation.GetResId(68);
        public static readonly int ReservationOccupiedRoomTypeModificationNotAllowed = Reservation.GetResId(69);
        public static readonly int PriceRateMaxLOSExceeded = Reservation.GetResId(70);
        public static readonly int PriceRateLeadOutOfRange = Reservation.GetResId(71);
        public static readonly int PriceRateOutOfSalesPeriod = Reservation.GetResId(72);
        public static readonly int PriceRateOccupationPercentExceeded = Reservation.GetResId(73);
        public static readonly int PriceRateMealPlanNotAllowed = Reservation.GetResId(74);
        public static readonly int ReservationCancellationFeeMissing = Reservation.GetResId(75);
        public static readonly int ServicePriceMissing = Reservation.GetResId(76);
        public static readonly int ReservationGroupTemplateFailed = Reservation.GetResId(77);
        public static readonly int InvalidRateType = Reservation.GetResId(78);
        public static readonly int ReservationDataValidationError = Reservation.GetResId(79);
        public static readonly int ReservationDataValidationWarning = Reservation.GetResId(80);
        public static readonly int DirtyRoomWarning = Reservation.GetResId(81);
        public static readonly int DuplicateGroupReservationName = Reservation.GetResId(82);
        public static readonly int ReservationDirtyRoomCheckIn = Reservation.GetResId(83);
        public static readonly int ReservationLockedRoom = Reservation.GetResId(84);

        #endregion
        #region Resource

        private static readonly int Resource = 4;
        public static readonly int AvailabilityAlert = Resource.GetResId(1);
        public static readonly int ReservationForbiden = Resource.GetResId(2);
        public static readonly int OccupiedResource = Resource.GetResId(3);
        public static readonly int UndefinedExtensionRoom = Resource.GetResId(4);
        public static readonly int CanNotOpenLine = Resource.GetResId(5);
        public static readonly int CanNotCloseLine = Resource.GetResId(6);
        public static readonly int OutOfRentalResource = Resource.GetResId(7);
        public static readonly int InvalidVirtualRoomComp = Resource.GetResId(8);

        #endregion
        #region Tax

        private static readonly int Tax = 5;
        public static readonly int TaxRulesNotDefined = Tax.GetResId(1);
        public static readonly int InvalidPensionMode = Tax.GetResId(2);
        public static readonly int InvalidPortugueseTaxModelConfiguration = Tax.GetResId(3);
        public static readonly int TaxRateAlreadyExists = Tax.GetResId(4);
        public static readonly int TaxRegionAlreadyRegistered = Tax.GetResId(5);
        public static readonly int TaxSequenceAlreadyRegistered = Tax.GetResId(6);
        public static readonly int MandatoryTaxRate = Tax.GetResId(7);
        public static readonly int UndefinedTaxRateApplyMode = Tax.GetResId(8);

        #endregion
        #region CurrentAccount

        private static readonly int CurrAccount = 6;
        public static readonly int CurrAccountAreadyOpen = CurrAccount.GetResId(1);
        public static readonly int CurrAccountAlreadyClosed = CurrAccount.GetResId(2);
        public static readonly int CurrAccountNotSettled = CurrAccount.GetResId(3);
        public static readonly int CurrAccountUnbalanced = CurrAccount.GetResId(4);

        #endregion
        #region Transaction

        private static readonly int Transaction = 7;
        public static readonly int InvalidCreditEntrie = Transaction.GetResId(1);
        public static readonly int InvalidDebitEntrie = Transaction.GetResId(2);
        public static readonly int CannotCreateDepositInAdvWithoutAccount = Transaction.GetResId(3);
        public static readonly int InvalidEntrie = Transaction.GetResId(4);
        public static readonly int CreditLimitReached = Transaction.GetResId(5);
        public static readonly int BalanceForRefundNotAvailable = Transaction.GetResId(6);
        public static readonly int UnbalanceCashFlow = Transaction.GetResId(7);
        public static readonly int AccClosedOrLocked = Transaction.GetResId(8);
        public static readonly int TransferInstCircularRef = Transaction.GetResId(9);
        public static readonly int TransferInstDefOverlaped = Transaction.GetResId(10);
        public static readonly int InvalidEntrieSplitPercent = Transaction.GetResId(11);
        public static readonly int InvalidEntrieSplitValue = Transaction.GetResId(12);
        public static readonly int CancellationReasonNotDefined = Transaction.GetResId(13);
        public static readonly int EntrieAlreadyCancelledOrCorrected = Transaction.GetResId(14);
        public static readonly int EntrieHasInvoiceOrTicket = Transaction.GetResId(15);
        public static readonly int EntrieAlreadyHasReceipt = Transaction.GetResId(16);
        public static readonly int EntrieMultipleCurrencies = Transaction.GetResId(17);
        public static readonly int EntrieNullServiceByDepartment = Transaction.GetResId(18);
        public static readonly int CannotSplitDebitEntrie = Transaction.GetResId(19);
        public static readonly int CannotSplitCityTaxEntrie = Transaction.GetResId(20);
        public static readonly int InvalidDebitEntrieServiceDepartment = Transaction.GetResId(21);
        public static readonly int InvalidDebitEntrieReceivable = Transaction.GetResId(22);
        public static readonly int InvalidDebitEntrieQuantity = Transaction.GetResId(23);
        public static readonly int InvalidDebitEntrieUndiscountValue = Transaction.GetResId(24);
        public static readonly int InvalidDebitEntrieCommissionValue = Transaction.GetResId(25);
        public static readonly int InvalidDebitEntrieDiscountPercent = Transaction.GetResId(26);
        public static readonly int InvalidDebitEntrieDiscountValue = Transaction.GetResId(27);
        public static readonly int InvalidDebitEntrieCurrency = Transaction.GetResId(28);
        public static readonly int InvalidDebitEntriePayment = Transaction.GetResId(29);
        public static readonly int TicketTransactionReversalNotAllowed = Transaction.GetResId(30);
        public static readonly int TicketNotClosed = Transaction.GetResId(31);
        public static readonly int TicketAlreadyCancelled = Transaction.GetResId(32);
        public static readonly int TicketCancellationNotPossible = Transaction.GetResId(33);

        #endregion
        #region Price

        private static readonly int Price = 8;
        public static readonly int DuplicatedAdultPriceDetail = Price.GetResId(1);
        public static readonly int InvalidAgeRangeType = Price.GetResId(2);
        public static readonly int DuplicatedAgeRange = Price.GetResId(3);
        public static readonly int AgeRangeNotFound = Price.GetResId(4);
        public static readonly int DuplicatedChildPriceDetail = Price.GetResId(5);
        public static readonly int DuplicatedRoomPriceDetail = Price.GetResId(6);
        public static readonly int UndefinedPriceRatePriceAtDate = Price.GetResId(7);
        public static readonly int NotFoundRecordPrice = Price.GetResId(8);
        public static readonly int UnassignedPensionBoard = Price.GetResId(9);
        public static readonly int InvalidDailyMovement = Price.GetResId(10);
        public static readonly int InvalidServiceMovement = Price.GetResId(11);
        public static readonly int NotFoundAllotmentRecordPrice = Price.GetResId(12);
        public static readonly int UndefinedDailyMovementRoomService = Price.GetResId(13);
        public static readonly int UndefinedDailyMovementRoomPrice = Price.GetResId(14);
        public static readonly int DailyMovementFoodPriceGreatherThanRoomPrice = Price.GetResId(15);
        public static readonly int UndefinedDailyMovementRoomSuplPriceOrDiscount = Price.GetResId(16);
        public static readonly int UndefinedDailyMovementLunchService = Price.GetResId(17);
        public static readonly int UndefinedDailyMovementLunchDrinkService = Price.GetResId(18);
        public static readonly int UndefinedDailyMovementLunchPrice = Price.GetResId(19);
        public static readonly int UndefinedDailyMovementLunchDrinkPrice = Price.GetResId(20);
        public static readonly int UndefinedDailyMovementDinnerService = Price.GetResId(21);
        public static readonly int UndefinedDailyMovementDinnerDrinkService = Price.GetResId(22);
        public static readonly int UndefinedDailyMovementDinnerPrice = Price.GetResId(23);
        public static readonly int UndefinedDailyMovementDinnerDrinkPrice = Price.GetResId(24);
        public static readonly int UndefinedDailyMovementBreakfastService = Price.GetResId(25);
        public static readonly int UndefinedDailyMovementBreakfastDrinkService = Price.GetResId(26);
        public static readonly int UndefinedDailyMovementBreakfastPrice = Price.GetResId(27);
        public static readonly int UndefinedDailyMovementBreakfastDrinkPrice = Price.GetResId(28);
        public static readonly int UndefinedPriceAtDate = Price.GetResId(29);
        public static readonly int PriceMismatch = Price.GetResId(30);
        public static readonly int PriceRateAlreadyRegistered = Price.GetResId(31);
        public static readonly int UndefinedDefaultPriceRateCategory = Price.GetResId(32);
        public static readonly int PriceRateDateLocked = Price.GetResId(33);
        public static readonly int UndefinedRoomService = Price.GetResId(34);
        public static readonly int InvalidDailyPrice = Price.GetResId(35);

        #endregion
        #region Guest

        private static readonly int Guest = 9;
        public static readonly int GuestNotClient = Guest.GetResId(1);
        public static readonly int GuestWithoutAge = Guest.GetResId(2);

        #endregion
        #region Department & Service

        private static readonly int DepartmentAndService = 10;
        public static readonly int DuplicatedServiceByDepartment = DepartmentAndService.GetResId(1);
        public static readonly int DuplicatedDepartmentByService = DepartmentAndService.GetResId(2);
        public static readonly int DuplicatedApplicationByDepartment = DepartmentAndService.GetResId(3);
        public static readonly int DuplicatedApplicationByService = DepartmentAndService.GetResId(4);
        public static readonly int UnrelatedServiceAndDepartment = DepartmentAndService.GetResId(5);
        
        public static readonly int DuplicatedApplicationByServiceGrouping = DepartmentAndService.GetResId(7);
        public static readonly int TipServiceOrDepartmentNotDefined = DepartmentAndService.GetResId(8);

        #endregion
        #region Contact

        private static readonly int Conctact = 11;
        public static readonly int DuplicatedContactByBank = Conctact.GetResId(1);

        #endregion
        #region Market

        private static readonly int SegmentAndOrigin = 12;
        public static readonly int DuplicatedOriginBySegment = SegmentAndOrigin.GetResId(1);
        public static readonly int DuplicatedSegmentByOrigin = SegmentAndOrigin.GetResId(2);

        #endregion
        #region Alloment
        private static readonly int Allotment = 13;
        public static readonly int PeriodOverlapped = Allotment.GetResId(1);
        public static readonly int InvalidPeriod = Allotment.GetResId(2);
        #endregion
        #region Documents
        private static readonly int Documents = 14;
        public static readonly int DocumentAmountDueNotZero = Documents.GetResId(1);
        public static readonly int InvoiceDepartmentNotDefined = Documents.GetResId(2);
        public static readonly int ExchangeRateNotFoundForDate = Documents.GetResId(3);
        public static readonly int RecordsNotAddedForInvoiceOrTicket = Documents.GetResId(4);
        public static readonly int InvalidInvoicePendingValue = Documents.GetResId(5);
        public static readonly int ExchangeRateCurrencyNotFound = Documents.GetResId(6);
        public static readonly int PendingInvoiceNotAllowed = Documents.GetResId(7);
        public static readonly int MovementsRegistered = Documents.GetResId(8);
        public static readonly int MovementsDailyClosing = Documents.GetResId(9);
        public static readonly int OfficialDocumentsRegistered = Documents.GetResId(10);
        public static readonly int MandatoryFiscalNumber = Documents.GetResId(11);
        public static readonly int ValueGreaterThanPendingTransactions = Documents.GetResId(12);
        public static readonly int DebitsGreaterThanCredits = Documents.GetResId(13);
        public static readonly int InvalidDocDate = Documents.GetResId(14);
        public static readonly int FiscalInfoWarning = Documents.GetResId(15);
        public static readonly int CertificateNotFound = Documents.GetResId(16);
        public static readonly int FiscalError = Documents.GetResId(17);
        public static readonly int CancelDocInvalidStatus = Documents.GetResId(18);
        public static readonly int CancelDocError = Documents.GetResId(19);
        public static readonly int InvalidDocumentBalance = Documents.GetResId(20);
        public static readonly int InvalidInvoiceTransactions = Documents.GetResId(21);
        public static readonly int MandatoryFiscalCountry = Documents.GetResId(22);
        public static readonly int PreviousDocumentSignNotFound = Documents.GetResId(23);
        public static readonly int CanceledTransactionsNotPermitted = Documents.GetResId(24);
        public static readonly int AccountCreditInvoiceLocked = Documents.GetResId(25);
        public static readonly int ReversalDetailsNotAllowed = Documents.GetResId(26);
        public static readonly int DocumentWithoutTransactions = Documents.GetResId(27);
        public static readonly int InvoiceGatewayPaymentsNotSupported = Documents.GetResId(28);
        public static readonly int InvalidDocumentAmount = Documents.GetResId(29);
        public static readonly int DocumentAlreadySettled = Documents.GetResId(30);
        public static readonly int PaymentsOtherThanDepositsNotAllowed = Documents.GetResId(31);
        public static readonly int MultiInstructionsInvoiceNotAllowed = Documents.GetResId(32);
        public static readonly int InvalidDocumentAccountTransferAmount = Documents.GetResId(33);
        #endregion
        #region Web Services
        private static readonly int WebService = 99;
        public static readonly int InvalidClientDateTime = WebService.GetResId(1);
        public static readonly int InvalidDateTimeFormat = WebService.GetResId(2);
        public static readonly int InvalidNomenclatureID = WebService.GetResId(3);
        public static readonly int InvalidUserPassword = WebService.GetResId(4);
        public static readonly int InvalidPublicKey = WebService.GetResId(5);
        public static readonly int InvalidCulture = WebService.GetResId(6);
        public static readonly int InvalidApplication = WebService.GetResId(7);
        public static readonly int InstallationNotFoundOrInvalid = WebService.GetResId(8);
        public static readonly int ChannelOrSubChannelNotFound = WebService.GetResId(9);
        public static readonly int InvalidDepartment = WebService.GetResId(10);
        public static readonly int UndefinedDepartment = WebService.GetResId(11);
        public static readonly int InvalidServiceByDepartment = WebService.GetResId(12);
        public static readonly int UndefinedServiceByDepartment = WebService.GetResId(13);
        public static readonly int InvalidCurrentAccount = WebService.GetResId(14);
        public static readonly int UndefinedCurrentAccount = WebService.GetResId(15);
        public static readonly int InvalidWithdrawType = WebService.GetResId(16);
        public static readonly int UndefinedWithdrawType = WebService.GetResId(17);
        public static readonly int TooManyDays = WebService.GetResId(18);
        #endregion
        #region Phone Service
        private static readonly int WebPhone = 101;
        public static readonly int InvalidJob = WebPhone.GetResId(1);
        public static readonly int InvalidIdClearOne = WebPhone.GetResId(2);
        public static readonly int DuplicatedWakeUp = WebPhone.GetResId(3);
        #endregion
        #region Pos Service
        private static readonly int WebPos = 102;
        public static readonly int InvalidEntrieCurrency = WebService.GetResId(1);
        public static readonly int InvalidEntrieCreditCardType = WebPos.GetResId(2);
        public static readonly int UndefinedEntrieCreditCardType = WebPos.GetResId(3);
        public static readonly int InvalidService = WebService.GetResId(4);
        public static readonly int UndefinedService = WebService.GetResId(5);
        public static readonly int ServiceByDepartmentNotFound = WebService.GetResId(6);
        public static readonly int ReceiptHolderNotDefined = WebService.GetResId(7);
        public static readonly int InvalidCancelationReason = WebService.GetResId(8);
        public static readonly int EntrieNotFound = WebService.GetResId(9);
        public static readonly int EntrieAlreadyInvoiced = WebService.GetResId(10);
        #endregion
        #region Daily closing
        private static readonly int DailyClosing = 100;
        public static readonly int ReservationPendingOfDeparture = DailyClosing.GetResId(1);
        public static readonly int ReservationPendingOfArrival = DailyClosing.GetResId(2);
        public static readonly int ReservationPendingOfGuestsRegistration = DailyClosing.GetResId(3);
        public static readonly int ReservationPendingOfGuestsCheckIn = DailyClosing.GetResId(4);
        public static readonly int ReservationPendingOfGuestsCheckOut = DailyClosing.GetResId(5);
        public static readonly int ReservationPendingOfPriceRate = DailyClosing.GetResId(6);
        public static readonly int ReservationPendingOfExRate = DailyClosing.GetResId(7);
        public static readonly int ReservationPendingOfCheckIn = DailyClosing.GetResId(8);
        public static readonly int ReservationPendingOfCheckInRetOverBooking = DailyClosing.GetResId(9);
        public static readonly int ClosedUnbalancedAccount = DailyClosing.GetResId(10);
        public static readonly int ReservationsinCheckout = DailyClosing.GetResId(11);
        public static readonly int ReservationsinCheckin = DailyClosing.GetResId(12);
        public static readonly int ClosingInProgress = DailyClosing.GetResId(13);
        public static readonly int ClosingAlreadyDone = DailyClosing.GetResId(14);
        public static readonly int ClosingEntriesNotFound = DailyClosing.GetResId(15);
        public static readonly int ConfirmationInProgress = DailyClosing.GetResId(16);
        public static readonly int OpenCashiers = DailyClosing.GetResId(17);
        public static readonly int ReservationPendingRoomChange = DailyClosing.GetResId(18);
        
        #endregion
        #region Facilities
        private static readonly int Facilities = 110;
        public static readonly int CannotDeleteAuthenticateInstallation = Facilities.GetResId(1);
        #endregion
        #region Booking Service
        private static readonly int WebBooking = 120;
        public static readonly int InvalidBookingDates = WebBooking.GetResId(1);
        public static readonly int InvalidBookingPrice = WebBooking.GetResId(2);
        public static readonly int InvalidBookingRoomType = WebBooking.GetResId(3);
        public static readonly int UndefinedBookingRoomType = WebBooking.GetResId(4);
        public static readonly int InvalidBookingPensionBoard = WebBooking.GetResId(5);
        public static readonly int UndefinedBookingPensionBoard = WebBooking.GetResId(6);
        public static readonly int InvalidBookingMarketOrigin = WebBooking.GetResId(7);
        public static readonly int UndefinedBookingMarketOrigin = WebBooking.GetResId(8);
        public static readonly int InvalidBookingMarketSegment = WebBooking.GetResId(9);
        public static readonly int UndefinedBookingMarketSegment = WebBooking.GetResId(10);
        public static readonly int InvalidBookingGuestFirstName = WebBooking.GetResId(11);
        public static readonly int InvalidBookingGuestLastName = WebBooking.GetResId(12);
        public static readonly int InvalidBookingGuestCountry = WebBooking.GetResId(13);
        public static readonly int UndefinedBookingGuestCountry = WebBooking.GetResId(14);
        public static readonly int InvalidBookingAdultsCount = WebBooking.GetResId(18);
        public static readonly int InvalidBookingChildrenCount = WebBooking.GetResId(19);
        public static readonly int InvalidBookingPaxCount = WebBooking.GetResId(20);
        public static readonly int UndefinedBookingGuestType = WebBooking.GetResId(21);
        public static readonly int UndefinedBookingTaxSchema = WebBooking.GetResId(22);
        public static readonly int InvalidBookingGuestType = WebBooking.GetResId(23);
        public static readonly int InvalidBookingCompany = WebBooking.GetResId(24);
        public static readonly int InvalidBookingContract = WebBooking.GetResId(26);
        public static readonly int InvalidBookingPriceRate = WebBooking.GetResId(27);
        public static readonly int InvalidBookingService = WebBooking.GetResId(28);
        public static readonly int InvalidBookingDepartament = WebBooking.GetResId(29);
        public static readonly int InvalidBookingGuestTitle = WebBooking.GetResId(30);
        public static readonly int InvalidCreditCardType = WebBooking.GetResId(31);
        public static readonly int InvalidBookingChannel = WebBooking.GetResId(32);
        public static readonly int InvalidBookingCreditCardExpireDate = WebBooking.GetResId(33);
        public static readonly int InvalidBookingReservationId = WebBooking.GetResId(34);
        public static readonly int InvalidBookingReservationState = WebBooking.GetResId(35);
        public static readonly int InvalidBookingCreditCardNumber = WebBooking.GetResId(36);
        public static readonly int InvalidBookingGuaranteeTypeId = WebBooking.GetResId(37);
        public static readonly int InvalidBookingCancellationReason = WebBooking.GetResId(38);
        public static readonly int InvalidBookingResourceRequirement = WebBooking.GetResId(39);
        public static readonly int BookingErrors = WebBooking.GetResId(40);
        public static readonly int InvalidBookingCancellation = WebBooking.GetResId(41);
        public static readonly int ErrorUpdateChannelData = WebBooking.GetResId(42);
        public static readonly int MealTypeAdditional = Reservation.GetResId(43);
        public static readonly int InvalidBookingCountryGuest = WebBooking.GetResId(44);
        public static readonly int InvalidBookingAuthentication = WebBooking.GetResId(45);
        public static readonly int InvalidBookingDescription = WebBooking.GetResId(46);
        public static readonly int InvalidBookingClientType = WebBooking.GetResId(47);
        public static readonly int InvalidBookingAccountType = WebBooking.GetResId(48);
        public static readonly int InvalidBookingServiceDate = WebBooking.GetResId(49);
        public static readonly int InvalidBookingAdditionalEntryType = WebBooking.GetResId(50);
        public static readonly int InvalidBookingPriceCero = WebBooking.GetResId(51);
        public static readonly int InvalidBookingRoomRate = WebBooking.GetResId(52);
        public static readonly int UndefinedBookingReservationCountry = WebBooking.GetResId(53);
        public static readonly int InvalidBookingArrivalDate = WebBooking.GetResId(54);
        public static readonly int PersonsCountExceedsRoomTypeMaxCapacity = WebBooking.GetResId(55);
        public static readonly int InvalidBookingTaxSchema = WebBooking.GetResId(56);
        public static readonly int InvalidBookingGuestGender = WebBooking.GetResId(57);
        public static readonly int InvalidBookingReservationPayment = WebBooking.GetResId(58);
        public static readonly int InvalidBookingCreditCardMapping = WebBooking.GetResId(59);
        public static readonly int BookingErrorsResourceRequirement = WebBooking.GetResId(60);
        public static readonly int BookingErrorsGuarantee = WebBooking.GetResId(61);
        public static readonly int BookingErrorsServiceAditionals = WebBooking.GetResId(62);
        public static readonly int BookingErrorsLoadOccupationLine = WebBooking.GetResId(63);
        public static readonly int BookingErrorsGuestPensionPrice = WebBooking.GetResId(64);
        public static readonly int BookingErrorsOccupationGuest = WebBooking.GetResId(65);
        public static readonly int BookingErrorsEntity = WebBooking.GetResId(66);
        public static readonly int BookingErrorsRoomType = WebBooking.GetResId(67);
        public static readonly int BookingErrorsMarket = WebBooking.GetResId(68);
        public static readonly int InvalidBookingPaxCountGender = WebBooking.GetResId(69);
        public static readonly int InvalidBookingAllotment = WebBooking.GetResId(70);
        public static readonly int BookingErrorsDepositPayments = WebBooking.GetResId(71);
        public static readonly int BookingErrorsChannelNotSpecified = WebBooking.GetResId(72);
        public static readonly int InvalidBookingUserCancellation = WebBooking.GetResId(73);
        public static readonly int InvalidBookingUserCreation = WebBooking.GetResId(74);
        public static readonly int InvalidBookingMealType = WebBooking.GetResId(75);
        public static readonly int InvalidBookingExternalReservationId = WebBooking.GetResId(76);
        public static readonly int InvalidBookingExternalReferenceReservationId = WebBooking.GetResId(77);
        public static readonly int InvalidBookingExternalModificationReservationId = WebBooking.GetResId(78);
        public static readonly int GetInfoHotelErrors = WebBooking.GetResId(79);
        public static readonly int InvalidBookingServiceSupplement = WebBooking.GetResId(80);
        public static readonly int InvalidBookingBabiesCount = WebBooking.GetResId(81);
        public static readonly int InvalidBookingAdultsFreeCount = WebBooking.GetResId(82);
        public static readonly int InvalidBookingChildrenFreeCount = WebBooking.GetResId(83);
        public static readonly int InvalidBookingIsDaily = WebBooking.GetResId(84);

        #endregion
        #region External Channel
        private static readonly int ExternalChannel = 130;
        public static readonly int ErrorNotificationExternalChannel = ExternalChannel.GetResId(1);
        public static readonly int ErrorNotificationExternalBookingData = ExternalChannel.GetResId(2);
        public static readonly int ErrorNotificationExternalHotelData = ExternalChannel.GetResId(3);
        public static readonly int ErrorProxyExternalChannel = ExternalChannel.GetResId(4);
        #endregion
        #region Interface
        private static readonly int InterfaceNewGes = 150;
        public static readonly int InvalidConfigurationKeyEmission = InterfaceNewGes.GetResId(1);
        public static readonly int InvalidNotifyOperations = InterfaceNewGes.GetResId(2);
        public static readonly int InvalidExtensionKey = InterfaceNewGes.GetResId(3);
        #endregion
        #region COPE-PT
        private static readonly int CopePT = 700;
        public static readonly int InvalidOfficialCode = CopePT.GetResId(1);
        public static readonly int NothingToExportToCopePt = SaftPt.GetResId(99);
        #endregion
        #region Payments
        private static readonly int Payments = 800;
        public static readonly int PaymentGatewayError = Payments.GetResId(1);
        public static readonly int PaymentConfigNotFound = Payments.GetResId(2);
        public static readonly int PaymentRefundInvalidAmount = Payments.GetResId(3);
        public static readonly int PaymentRefundInvalidCurrency = Payments.GetResId(4);
        public static readonly int PaymentCreditNotAllowed = Payments.GetResId(5);
        public static readonly int PaymentRefundInvalidProvider = Payments.GetResId(6);
        public static readonly int PaymentVoidInvalidProvider = Payments.GetResId(7);
        public static readonly int PaymentExchangeRateNotFound = Payments.GetResId(8);
        public static readonly int PaymentWithoutReceipt = Payments.GetResId(9);
        public static readonly int PaymentInvalidReceivableType = Payments.GetResId(10);
        public static readonly int PaymentInvalidCurrency = Payments.GetResId(11);
        public static readonly int PaymentUndefinedMonthYear = Payments.GetResId(12);
        public static readonly int PaymentInvalidMonthYear = Payments.GetResId(13);
        public static readonly int PaymentInvalidTransactionId = Payments.GetResId(14);
        public static readonly int PaymentAuthAlreadyUsed = Payments.GetResId(15);
        public static readonly int PaymentMultipleVoidNotAllowed = Payments.GetResId(16);
        public static readonly int PaymentUndefinedTerminal = Payments.GetResId(17);
        public static readonly int PaymentUndefinedCreditCardType = Payments.GetResId(18);
        public static readonly int PaymentRequireAuthorization = Payments.GetResId(99);
        #endregion
        #region SAF-T
        private static readonly int SaftPt = 900;
        public static readonly int InvalidCodeForClientOrEntityName = SaftPt.GetResId(1);
        public static readonly int InvalidCodeForProductName = SaftPt.GetResId(2);
        public static readonly int InvalidTaxRegionForRegionName = SaftPt.GetResId(3);
        public static readonly int InvalidExemptionReasonForServiceName = SaftPt.GetResId(4);
        public static readonly int InvalidInstallationFiscalNumber = SaftPt.GetResId(5);
        public static readonly int InvalidFiscalNumber = SaftPt.GetResId(6);
        public static readonly int InvalidTaxTotal = SaftPt.GetResId(7);
        public static readonly int InvalidNetTotal = SaftPt.GetResId(8);
        public static readonly int InvalidExemptionCodeForName = SaftPt.GetResId(9);
        public static readonly int UndefinedVersion = SaftPt.GetResId(95);
        public static readonly int InvalidDatesFiscalYear = SaftPt.GetResId(96);
        public static readonly int InvalidDatesVersioning = SaftPt.GetResId(97);
        public static readonly int InvalidFieldLength = SaftPt.GetResId(98);
        public static readonly int NothingToExport = SaftPt.GetResId(99);
        public static readonly int InvalidClientOrEntity = SaftPt.GetResId(100);
        public static readonly int InvalidFiscalizationType = SaftPt.GetResId(101);
        #endregion
        #region CRMSync
        private static readonly int CRMSync = 1000;
        public static readonly int DonthaveReservationstoExportCRM = CRMSync.GetResId(1);
        public static readonly int ErrorSyncCompanies = CRMSync.GetResId(2);
        public static readonly int ErrorSyncClients = CRMSync.GetResId(3);
        public static readonly int ErrorSyncReservation = CRMSync.GetResId(4);
        public static readonly int ErrorSyncDocuments = CRMSync.GetResId(5);
        public static readonly int ErrorGetHotelSetting = CRMSync.GetResId(6);
        public static readonly int InvalidSyncDateRange = CRMSync.GetResId(7);
        public static readonly int DonthaveDocumentstoExportCRM = CRMSync.GetResId(8);
        #endregion
        #region Communication

        private static readonly int Communication = 1100;
        public static readonly int CommunicationError = Communication.GetResId(1);

        #endregion
    }
}
