﻿using System.Linq;
using System.Text.RegularExpressions;

namespace NewHotel.Contracts
{
    public static class FiscalValidations
    {
        #region Constants

        public const long NationalIdentityDoc = 2;
        public const long PassportDoc = 3;
        public const long ForeignIdentityDoc = 5;
        public const long FiscalForeignDoc = 7;
        public const long FiscalNumDoc = 8;

        public const string FinalCustomerNumberPT = "999999990";
        public const string FinalCustomerNumberAO = "999999999";
        public const string FinalCustomerNumberEC = "9999999999999";
        public const string FinalCustomerNumberCW = "";
        public const string FinalCustomerNumberMX = "XAXX010101000";
        public const string FinalCustomerNumberExMX = "XEXX010101000";

        public const string FinalCustomerName = "Consumidor final";
        public const string FinalCustomerNameCW = "";
        public const string FinalCustomerNameMX = "Ventas Publico General";

        #endregion

        #region Portugal

        public static bool ValidateNIFPortugal(string nif)
        {
            if (!string.IsNullOrEmpty(nif))
            {
                if (nif.StartsWith("PT"))
                    nif = nif.Substring(2);

                if (nif.Length == FinalCustomerNumberPT.Length && !nif.Any(c => !char.IsDigit(c)))
                {
                    // Si el NIF empieza con:
                    //1 a 3: Pessoa singular, o 3 ainda não está atribuido;[2]
                    //45: Pessoa singular.Os algarismos iniciais "45" correspondem aos cidadãos não residentes que apenas obtenham em território português rendimentos sujeitos a retenção na fonte a título definitivo.[2]
                    //5: pessoa coletiva obrigada a registo no Registo Nacional de Pessoas Coletivas;[3]
                    //6: Organismo da Administração Pública Central, Regional ou Local;
                    //70, 74 e 75: Herança Indivisa, em que o autor da sucessão não era empresário individual, ou Herança Indivisa em que o cônjuge sobrevivo tem rendimentos comerciais;
                    //71: Não residentes coletivos sujeitos a retenção na fonte a título definitivo.
                    //72: Fundos de investimento.
                    //77: Atribuição Oficiosa de NIF de sujeito passivo(entidades que não requerem NIF junto do RNPC).
                    //78: Atribuição oficiosa a não residentes abrangidos pelo processo VAT REFUND.
                    //79: Regime excepcional - Expo 98.
                    //8: "empresário em nome individual" (deixou de ser utilizado, já não é válido);
                    //90 e 91: Condomínios, Sociedade Irregulares, Heranças Indivisas cujo autor da sucessão era empresário individual.
                    //98: Não residentes sem estabelecimento estável.
                    //99: Sociedades civis sem personalidade jurídica.

                    var validFirstDigits = new char[] { '1', '2', '3', '4', '5', '6', '7', '8', '9' };
                    if (validFirstDigits.Contains(nif[0]))
                    {
                        var checkDigit = 0;
                        for (int i = 1; i <= 8; i++)
                            checkDigit += int.Parse(nif[i - 1].ToString()) * (10 - i);
                        checkDigit = 11 - (checkDigit % 11);

                        // si es mayor o igual que 10 ponerlo a cero
                        if (checkDigit >= 10) checkDigit = 0;

                        // comparar el digito de verificación con el último digito
                        // del NIF, si son iguales entonces es válido
                        if (nif.EndsWith(checkDigit.ToString()))
                            return true;
                    }
                }
            }

            return false;
        }

        #endregion

        #region Angola

        public static bool ValidateNIFAngola(string nif)
        {
            if (!string.IsNullOrEmpty(nif))
            {
                if (nif == FinalCustomerNumberAO)
                    return true;

                if (nif.Length <= 30 && nif.All(c => char.IsLetterOrDigit(c)))
                    return true;
            }

            return false;
        }

        public static bool ValidateCCAngola(string cc)
        {
            if (!string.IsNullOrEmpty(cc))
            {
                if (cc.Length == 14 && cc.All(c => char.IsLetterOrDigit(c)))
                    return true;
            }

            return false;
        }

        #endregion

        #region Brazil

        public static bool ValidCPF(string cpf)
        {
            if (cpf == null) return false;
            if (cpf.Length != 11) return false;
            var regexObj = new Regex(@"[^\d]");
            cpf = regexObj.Replace(cpf, "");

            if (cpf == "" || cpf.Length != 11 || cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444"
                          || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999")
                return false;

            // Valida 1o digito
            int add = 0;
            for (int i = 0; i < 9; i++)
                add += int.Parse(cpf[i].ToString()) * (10 - i);
            int rev = 11 - (add % 11);
            if (rev == 10 || rev == 11)
                rev = 0;
            if (rev != int.Parse(cpf[9].ToString())) return false;

            // Valida 2o digito
            add = 0;
            for (int i = 0; i < 10; i++)
                add += int.Parse(cpf[i].ToString()) * (11 - i);
            rev = 11 - (add % 11);
            if (rev == 10 || rev == 11)
                rev = 0;
            if (rev != int.Parse(cpf[10].ToString())) return false;

            return true;
        }

        public static bool ValidCNPJ(string cnpj)
        {
            if (cnpj == null) return false;
            if (cnpj.Length != 14) return false;
            var regexObj = new Regex(@"[^\d]");
            cnpj = regexObj.Replace(cnpj, "");

            if (cnpj == "" || cnpj.Length != 14 || cnpj == "00000000000000" || cnpj == "11111111111111" || cnpj == "22222222222222" || cnpj == "33333333333333" || cnpj == "44444444444444"
                           || cnpj == "55555555555555" || cnpj == "66666666666666" || cnpj == "77777777777777" || cnpj == "88888888888888" || cnpj == "99999999999999")
                return false;

            // Valida DVs
            int tamanho = cnpj.Length - 2;
            string numeros = cnpj.Substring(0, tamanho);
            string digitos = cnpj.Substring(tamanho);
            int soma = 0;
            int pos = tamanho - 7;

            for (int i = tamanho; i >= 1; i--)
            {
                soma += int.Parse(numeros[tamanho - i].ToString()) * pos--;
                if (pos < 2)
                    pos = 9;
            }

            int resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != int.Parse(digitos[0].ToString()))
                return false;

            tamanho = tamanho + 1;
            numeros = cnpj.Substring(0, tamanho);
            soma = 0;
            pos = tamanho - 7;
            for (int i = tamanho; i >= 1; i--)
            {
                soma += int.Parse(numeros[tamanho - i].ToString()) * pos--;
                if (pos < 2)
                    pos = 9;
            }

            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != int.Parse(digitos[1].ToString()))
                return false;

            return true;
        }

        #endregion

        #region Croatia

        public static bool ValidateNIFCroatia(string nif)
        {
            if (!string.IsNullOrEmpty(nif))
            {
                if (nif.Length != 11 || (nif.StartsWith("HR") && nif.Length != 13))
                    return false;
            }

            return true;
        }

        #endregion

        #region Peru

        public static bool ValidRUCPeru(string ruc)
        {
            if (!string.IsNullOrEmpty(ruc))
            {
                int rucDocumentLength = ruc.Length;
                if (rucDocumentLength == 11)
                {
                    int addition = 0;
                    int[] hash = { 5, 4, 3, 2, 7, 6, 5, 4, 3, 2 };

                    var rucComponent = ruc.Substring(0, rucDocumentLength - 1);
                    var rucComponentLength = rucComponent.Length;
                    var diff = hash.Length - rucComponentLength;

                    for (int i = rucComponentLength - 1; i >= 0; i--)
                        addition += (rucComponent[i] - '0') * hash[i + diff];

                    addition = 11 - (addition % 11);
                    if (addition > 9)
                        addition -= 10;

                    var last = char.ToUpperInvariant(ruc[rucDocumentLength - 1]);

                    // corresponde a un RUC
                    return addition.Equals(last - '0');
                }
            }

            return false;
        }

        public static bool ValidDNIPeru(string dni)
        {
            if (!string.IsNullOrEmpty(dni))
            {
                int dniDocumentLength = dni.Length;
                if (dniDocumentLength == 9)
                {
                    int addition = 0;
                    int[] hash = { 5, 4, 3, 2, 7, 6, 5, 4, 3, 2 };

                    var dniComponent = dni.Substring(0, dniDocumentLength - 1);
                    var dniComponentLength = dniComponent.Length;
                    var diff = hash.Length - dniComponentLength;

                    for (int i = dniComponentLength - 1; i >= 0; i--)
                        addition += (dniComponent[i] - '0') * hash[i + diff];

                    addition = 11 - (addition % 11);
                    if (addition > 9)
                        addition -= 10;

                    var last = char.ToUpperInvariant(dni[dniDocumentLength - 1]);

                    // corresponde a un DNI
                    if (char.IsDigit(last))
                    {
                        // es un DNI con número como dígito de verificación
                        char[] hashNumbers = { '6', '7', '8', '9', '0', '1', '1', '2', '3', '4', '5' };
                        return last.Equals(hashNumbers[addition]);
                    }
                    else if (char.IsLetter(last))
                    {
                        // es un DNI con letra como dígito de verificación 
                        char[] hashLetters = { 'K', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J' };
                        return last.Equals(hashLetters[addition]);
                    }
                }
                else if (dniDocumentLength == 8)
                    return dni.All(c => char.IsDigit(c));
            }

            return false;
        }

        #endregion

        #region Ecuador

        private static bool CheckDigit(string s, int[] coeficients)
        {
            int acum = 0;
            for (int index = 0; index < coeficients.Length; index++)
                acum += int.Parse(s.Substring(index, 1)) * coeficients[index];

            var dv = 11 - (acum % 11);
            if (dv == 11) dv = 0;

            return s.Substring(coeficients.Length, 1) == dv.ToString();
        }

        private static readonly int[] coeficients6 = new int[] { 3, 2, 7, 6, 5, 4, 3, 2 };
        private static readonly int[] coeficients9 = new int[] { 4, 3, 2, 7, 6, 5, 4, 3, 2 };

        public static bool ValidRUCEcuador(string ruc)
        {
            if (!string.IsNullOrEmpty(ruc))
            {
                if (ruc == FinalCustomerNumberEC)
                    return true;
                else
                {
                    if (ruc.Length != 13)
                        return false;

                    if (ruc.Any(c => !char.IsDigit(c)))
                        return false;

                    // personas naturales
                    if (int.Parse(ruc.Substring(2, 1)) <= 5)
                        return ValidDNIEcuador(ruc.Substring(0, 10)) && ruc.Substring(10, 3) == "001";
                    else
                    {
                        // provincia de expedición
                        var ruc01 = int.Parse(ruc.Substring(0, 2));
                        if (ruc01 < 1 || ruc01 > 24)
                            return false;

                        switch (ruc[2])
                        {
                            // publicos
                            case '6':
                            // jurídicos y extranjeros sin cédula
                            case '9':
                                return ruc.Substring(3, 7).All(c => char.IsDigit(c)) && ruc.Substring(10, 3) == "001";
                        }
                    }
                }
            }

            return false;
        }

        public static bool ValidDNIEcuador(string dni)
        {
            if (!string.IsNullOrEmpty(dni))
            {
                if (dni.Length != 10)
                    return false;

                if (dni.Any(c => !char.IsDigit(c)))
                    return false;

                if (int.Parse(dni.Substring(0, 2)) > 24)
                    return false;

                if (int.Parse(dni.Substring(2, 1)) > 5)
                    return false;

                var coeficients = new int[] { 2, 1, 2, 1, 2, 1, 2, 1, 2 };
                int acum = 0;
                for (int index = 0; index < dni.Length - 1; index++)
                {
                    var v = int.Parse(dni.Substring(index, 1)) * coeficients[index];
                    acum += (v >= 10 ? v - 9 : v);
                }

                var dv = 10 - (acum % 10);
                if (dv == 10) dv = 0;

                return dni.Substring(9, 1) == dv.ToString();
            }

            return false;
        }

        #endregion

        #region Colombia
        /*
        public static bool ValidNITColombia(string nit)
        {
            nit = GetOnlyDigits(nit);
            if (!string.IsNullOrEmpty(nit))
            {
                if (nit.Length >= 7 && nit.Length <= 11)
                {
                    var digits = new byte[nit.Length];
                    for (int i = 0; i < nit.Length; i++)
                    {
                        if (!char.IsDigit(nit[i]))
                            return false;

                        digits[i] = byte.Parse(nit[i].ToString());
                    }

                    var mult = new int[] { 3, 7, 13, 17, 19, 23, 29, 37, 41, 43 };

                    int dv = 0;
                    var length = digits.Length - 1;
                    for (int index = 0; index < length; index++)
                        dv += digits[length - index - 1] * mult[index];

                    dv = dv % 11;
                    if (dv >= 2)
                        dv = 11 - dv;

                    return dv == digits[digits.Length - 1];
                }
            }

            return false;
        }
        */
        public static bool ValidNITColombia(string nit)
        {
            return !string.IsNullOrEmpty(GetDigits(nit));
        }

        public static bool ValidDNIColombia(string dni)
        {
            if (string.IsNullOrEmpty(dni))
                return false;

            return dni.All(c => char.IsDigit(c));
        }

        #endregion

        #region Chile

        public static bool ValidRUTChile(string rut)
        {
            if (!string.IsNullOrEmpty(rut))
            {
                var chars = GetLettersOrDigits(rut);
                return (chars.Length == 8 || chars.Length == 9) &&
                       (chars.All(c => char.IsDigit(c)) ||
                       (chars.Substring(0, chars.Length - 1).All(c => char.IsDigit(c)) && char.IsLetter(chars[chars.Length - 1])));
            }

            return false;
        }

        #endregion

        #region Public Methods

        public static bool HasFinalCustomer(DocumentSign? signatureType)
        {
            if (signatureType.HasValue)
            {
                switch (signatureType.Value)
                {
                    case DocumentSign.FiscalizationPortugal:
                    case DocumentSign.FiscalizationMexicoCfdi:
                    case DocumentSign.FiscalizationEcuador:
                    case DocumentSign.FiscalizationCuracao:
                    case DocumentSign.FiscalizationAngola:
                        return true;
                }
            }

            return false;
        }

        public static bool HasFinalCustomer(string country)
        {
            switch (country)
            {
                case "PT":
                case "MX":
                case "EC":
                case "CW":
                case "AO":
                    return true;
            }

            return false;
        }

        public static string GetFinalCustomerNumber(DocumentSign? signatureType)
        {
            if (signatureType.HasValue)
            {
                switch (signatureType.Value)
                {
                    case DocumentSign.FiscalizationPortugal:
                        return FinalCustomerNumberPT;
                    case DocumentSign.FiscalizationMexicoCfdi:
                        return FinalCustomerNumberMX;
                    case DocumentSign.FiscalizationEcuador:
                        return FinalCustomerNumberEC;
                    case DocumentSign.FiscalizationCuracao:
                        return FinalCustomerNumberCW;
                    case DocumentSign.FiscalizationAngola:
                        return FinalCustomerNumberAO;
                }
            }

            return string.Empty;
        }

        public static string GetFinalCustomerNumber(string country)
        {
            switch (country)
            {
                case "PT":
                    return FinalCustomerNumberPT;
                case "MX":
                    return FinalCustomerNumberMX;
                case "EC":
                    return FinalCustomerNumberEC;
                case "CW":
                    return FinalCustomerNumberCW;
                case "AO":
                    return FinalCustomerNumberAO;
            }

            return string.Empty;
        }

        public static string GetFinalCustomerName(DocumentSign? signatureType)
        {
            if (signatureType.HasValue)
            {
                switch (signatureType.Value)
                {
                    case DocumentSign.FiscalizationEcuador:
                    case DocumentSign.FiscalizationPortugal:
                    case DocumentSign.FiscalizationAngola:
                        return FinalCustomerName;
                    case DocumentSign.FiscalizationMexicoCfdi:
                        return FinalCustomerNameMX;
                    case DocumentSign.FiscalizationCuracao:
                        return FinalCustomerNameCW;
                }
            }

            return string.Empty;
        }

        public static string GetFinalCustomerName(string country)
        {
            switch (country)
            {
                case "EC":
                case "PT":
                case "AO":
                    return FinalCustomerName;
                case "MX":
                    return FinalCustomerNameMX;
                case "CW":
                    return FinalCustomerNameCW;
            }

            return string.Empty;
        }

        public static string GetDigits(string value)
        {
            if (!string.IsNullOrEmpty(value))
                return new string(value.Where(c => char.IsDigit(c)).ToArray());

            return value;
        }

        public static string GetLettersOrDigits(string value)
        {
            if (!string.IsNullOrEmpty(value))
                return new string(value.Where(c => char.IsLetterOrDigit(c)).ToArray());

            return value;
        }

        public static bool IsValid(string fiscalNumber, long? fiscalNumberType, DocumentSign? signType, string country)
        {
            if (signType.HasValue)
            {
                switch (signType.Value)
                {
                    case DocumentSign.FiscalizationPortugal:
                        if (fiscalNumber != FinalCustomerNumberPT)
                            return !string.IsNullOrEmpty(fiscalNumber) && fiscalNumber.Length <= 20;
                        break;
                    case DocumentSign.FiscalizationAngola:
                        if (country == "AO")
                        {
                            if (fiscalNumberType.HasValue)
                            {
                                switch (fiscalNumberType.Value)
                                {
                                    case FiscalNumDoc:
                                        return ValidateNIFAngola(fiscalNumber);
                                    case NationalIdentityDoc:
                                        return ValidateCCAngola(fiscalNumber);
                                }
                            }
                        }
                        return !string.IsNullOrEmpty(fiscalNumber);
                    case DocumentSign.FiscalizationCroatia:
                        return ValidateNIFCroatia(fiscalNumber);
                    case DocumentSign.FiscalizationMexicoCfdi:
                    case DocumentSign.FiscalizationPeruDFacture:
                        return !string.IsNullOrEmpty(fiscalNumber);
                    case DocumentSign.FiscalizationEcuador:
                        if (fiscalNumberType.HasValue)
                        {
                            switch (fiscalNumberType.Value)
                            {
                                case FiscalNumDoc:
                                    return ValidRUCEcuador(fiscalNumber);
                                case NationalIdentityDoc:
                                    return ValidDNIEcuador(fiscalNumber);
                            }
                        }
                        return fiscalNumberType.HasValue && !string.IsNullOrEmpty(fiscalNumber);
                    case DocumentSign.FiscalizationColombiaBtw:
                        if (fiscalNumberType.HasValue)
                        {
                            switch (fiscalNumberType.Value)
                            {
                                case FiscalNumDoc:
                                    return ValidNITColombia(fiscalNumber);
                                case NationalIdentityDoc:
                                    return ValidDNIColombia(fiscalNumber);
                            }
                        }
                        return fiscalNumberType.HasValue && !string.IsNullOrEmpty(fiscalNumber);
                    case DocumentSign.FiscalizationChile:
                        if (!string.IsNullOrEmpty(fiscalNumber))
                        {
                            if (fiscalNumberType.HasValue)
                            {
                                switch (fiscalNumberType.Value)
                                {
                                    case FiscalNumDoc:
                                        return ValidRUTChile(fiscalNumber);
                                }
                            }
                        }
                        break;
                }
            }
            else
            {
                switch (country)
                {
                    case "CL":
                        if (!string.IsNullOrEmpty(fiscalNumber))
                            return ValidRUTChile(fiscalNumber);
                        break;
                }
            }

            return true;
        }

        #endregion
    }
}