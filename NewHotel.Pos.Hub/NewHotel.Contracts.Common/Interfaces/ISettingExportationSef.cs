﻿namespace NewHotel.Contracts
{
    public interface ISettingExportationSef
    {
        short? MinimumCode { get; }
        short? NextCode { get; set; }
        short? MaximumCode { get; }
        string EntityOfficialCode { get; }
        string FiscalNumer { get; }
        string PasswordEntityOfficial { get; }
    }
}