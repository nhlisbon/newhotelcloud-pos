﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ContractContract), "ValidateContactContract")]
    public class ContractContract : BaseContract
    {
        #region Members

        private ContractType _contractType;
        private Guid? _priceRateId;
        private string _priceRateDescription;
        private Guid? _allotmentId;
        private string _allotmentDescription;
        private Guid? _receivableAccountId;
        private string _receivableAccountDescription;
        private Guid? _discountTypeId;
        private decimal? _discountPercent;
        private DiscountMethod? _discountApplyTo;
        private DiscountMethod? _discountCalculateFrom;
        private short? _applicationDays;
        //private short? _applicationOrder;
        private short _freeDays;
        private ApplyDistribution _applyMode;
        private EntryType _applyFrom;
        private bool _useAverageValue;
        private short? _minDaysDuringWeek;
        private short? _maxDaysDuringWeek;
        private short? _minWeekendDays;
        private short? _maxWeekendDays;
        private short? _initialDay;
        private short? _finalDay;
        private short? _elapsedDaysToSpread;
        private bool _onlyForEntryPeriods;
        private DateTime? _lastUpdateDate;
        private DateTime? _lastUpdateInitialDate;
        private DateTime? _lastUpdateFinalDate;
        private short? _pensionForPlannings;
        private string _installationEntityDescription;
        private PriceUsedFor _usedFor;

        [DataMember]
        internal LanguageTranslationContract _abbreviation;
        [DataMember]
        internal LanguageTranslationContract _description;

        #endregion
        #region Properties

        [DataMember]
        public ContractType ContractType { get { return _contractType; } set { Set(ref _contractType, value, "ContractType"); } }
        [DataMember]
        public Guid? PriceRateId { get { return _priceRateId; } set { Set(ref _priceRateId, value, "PriceRateId"); } }
        [DataMember]
        public string PriceRateDescription { get { return _priceRateDescription; } set { Set(ref _priceRateDescription, value, "PriceRateDescription"); } }
        [DataMember]
        public Guid? AllotmentId { get { return _allotmentId; } set { Set(ref _allotmentId, value, "AllotmentId"); } }
        [DataMember]
        public string AllotmentDescription { get { return _allotmentDescription; } set { Set(ref _allotmentDescription, value, "AllotmentDescription"); } }
        [DataMember]
        public Guid? ReceivableAccountId { get { return _receivableAccountId; } set { Set(ref _receivableAccountId, value, "ReceivableAccountId"); } }
        [DataMember]
        public string ReceivableAccountDescription { get { return _receivableAccountDescription; } set { Set(ref _receivableAccountDescription, value, "ReceivableAccountDescription"); } }
        [DataMember]
        public Guid? DiscountTypeId { get { return _discountTypeId; } set { Set(ref _discountTypeId, value, "DiscountTypeId"); } }
        [DataMember]
        public decimal? DiscountPercent { get { return _discountPercent; } set { Set(ref _discountPercent, value, "DiscountPercent"); } }
        [DataMember]
        public DiscountMethod? DiscountApplyTo { get { return _discountApplyTo; } set { Set(ref _discountApplyTo, value, "DiscountApplyTo"); } }
        [DataMember]
        public DiscountMethod? DiscountCalculateFrom { get { return _discountCalculateFrom; } set { Set(ref _discountCalculateFrom, value, "DiscountCalculateFrom"); } }
        [DataMember]
        public short? ApplicationDays { get { return _applicationDays; } set { Set(ref _applicationDays, value, "ApplicationDays"); } }
        [DataMember]
        //public short? ApplicationOrder
        //{
        //    get { return _applicationOrder; }
        //    set
        //    {
        //        if (Set(ref _applicationOrder, value, "ApplicationOrder"))
        //            NotifyPropertyChanged("ApplyRestrictions");
        //    }
        //}
        public short FreeDays
        {
            get { return _freeDays; }
            set
            {
                if (Set(ref _freeDays, value, "FreeDays"))
                    NotifyPropertyChanged("ApplyRestrictions");
            }
        }
        [DataMember]
        public ApplyDistribution ApplyMode { get { return _applyMode; } set { Set(ref _applyMode, value, "ApplyMode"); } }
        [DataMember]
        public EntryType ApplyFrom { get { return _applyFrom; } set { Set(ref _applyFrom, value, "ApplyFrom"); } }
        [DataMember]
        public short? MinDaysDuringWeek
        {
            get { return _minDaysDuringWeek; }
            set
            {
                if (Set(ref _minDaysDuringWeek, value, "MinDaysDuringWeek"))
                    NotifyPropertyChanged("WeekDays");
            }
        }
        [DataMember]
        public short? MaxDaysDuringWeek
        {
            get { return _maxDaysDuringWeek; }
            set
            {
                if (Set(ref _maxDaysDuringWeek, value, "MaxDaysDuringWeek"))
                    NotifyPropertyChanged("WeekDays");
            }
        }
        [DataMember]
        public short? MinWeekendDays
        {
            get { return _minWeekendDays; }
            set
            {
                if (Set(ref _minWeekendDays, value, "MinWeekendDays"))
                    NotifyPropertyChanged("WeekEndDays");
            }
        }
        [DataMember]
        public short? MaxWeekendDays
        {
            get { return _maxWeekendDays; }
            set
            {
                if (Set(ref _maxWeekendDays, value, "MaxWeekendDays"))
                    NotifyPropertyChanged("WeekEndDays");
            }
        }
        [DataMember]
        public short? InitialDay { get { return _initialDay; } set { Set(ref _initialDay, value, "InitialDay"); } }
        [DataMember]
        public short? FinalDay { get { return _finalDay; } set { Set(ref _finalDay, value, "FinalDay"); } }
        [DataMember]
        public short? ElapsedDaysToSpread
        {
            get { return _elapsedDaysToSpread; }
            set
            {
                if (Set(ref _elapsedDaysToSpread, value, "ElapsedDaysToSpread"))
                    NotifyPropertyChanged("ExtendedDailyRate");
            }
        }
        [DataMember]
        public bool UseAverageValue { get { return _useAverageValue; } set { Set(ref _useAverageValue, value, "UseAverageValue"); } }
        [DataMember]
        public bool OnlyForEntryPeriods { get { return _onlyForEntryPeriods; } set { Set(ref _onlyForEntryPeriods, value, "OnlyForEntryPeriods"); } }
        [DataMember]
        public DateTime? LastUpdateDate { get { return _lastUpdateDate; } set { Set(ref _lastUpdateDate, value, "LastUpdateDate"); } }
        [DataMember]
        public DateTime? LastUpdateInitialDate { get { return _lastUpdateInitialDate; } set { Set(ref _lastUpdateInitialDate, value, "LastUpdateInitialDate"); } }
        [DataMember]
        public DateTime? LastUpdateFinalDate { get { return _lastUpdateFinalDate; } set { Set(ref _lastUpdateFinalDate, value, "LastUpdateFinalDate"); } }
        [DataMember]
        public short? PensionForPlannings { get { return _pensionForPlannings; } set { Set(ref _pensionForPlannings, value, "PensionForPlannings"); } }
        [DataMember]
        public string InstallationEntityDescription { get { return _installationEntityDescription; } set { Set(ref _installationEntityDescription, value, "InstallationEntityDescription"); } }
        [DataMember]
        public PriceUsedFor UsedFor
        { 
            get { return _usedFor; }
            set
            {
                if (Set(ref _usedFor, value, "UsedFor"))
                    NotifyPropertyChanged("ShowEntities");
            }
        }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public bool ValidForStays
        {
            get { return InitialDay.HasValue || FinalDay.HasValue; }
        }

        [ReflectionExclude]
        public bool ExtendedDailyRate
        {
            get { return ElapsedDaysToSpread.HasValue; }
        }

        [ReflectionExclude]
        public bool WeekDays
        {
            get { return MinDaysDuringWeek.HasValue || MaxDaysDuringWeek.HasValue; }
        }

        [ReflectionExclude]
        public bool WeekEndDays
        {
            get { return MinWeekendDays.HasValue || MaxWeekendDays.HasValue; }
        }

        [ReflectionExclude]
        public bool ShowEntities
        {
            get { return _usedFor != PriceUsedFor.InDesk; }
        }

        [ReflectionExclude]
        public string ContractAbbreviation
        {
            get { return Abbreviation; }
        }

        [ReflectionExclude]
        public string ContractDescription
        {
            get { return Description; }
        }

		[ReflectionExclude]
		public bool ApplyRestrictions
		{
			get { return FreeDays > 0; }
		}

        #endregion
        #region Contracts

        [DataMember]
        [ReflectionExclude]
        public LanguageTranslationContract Abbreviation
        {
            get { return _abbreviation; }
            set { _abbreviation = value; }
        }

        [DataMember]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [ReflectionExclude]
        public TypedList<ContractEntryPeriodContract> EntryPeriods { get; set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<EntityContractRelContract> Entities { get;  set; }

        #endregion
        #region Constructors

        public ContractContract()
            : base() 
        {
            PriceRateDescription = string.Empty;
            AllotmentDescription = string.Empty;
            ReceivableAccountDescription = string.Empty;
            _applyFrom = EntryType.Arrival;
            _applyMode = ApplyDistribution.Both;
            _abbreviation = new LanguageTranslationContract();
            _description = new LanguageTranslationContract();
            EntryPeriods = new TypedList<ContractEntryPeriodContract>();
            Entities = new TypedList<EntityContractRelContract>();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateContactContract(ContractContract obj)
        {
            if (obj.Abbreviation.IsEmpty)
				return new System.ComponentModel.DataAnnotations.ValidationResult("Abreviation Required");
            if (obj.Description.IsEmpty)
				return new System.ComponentModel.DataAnnotations.ValidationResult("Description Required");
            if (!obj.PriceRateId.HasValue)
				return new System.ComponentModel.DataAnnotations.ValidationResult("Price Rate Required");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}