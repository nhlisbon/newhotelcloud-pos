﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ContractEntryPeriodContract), "ValidateContractEntryPeriod")]
    public class ContractEntryPeriodContract : BaseContract
    {
        #region Members
        private DateTime _initialDate;
        private DateTime _finalDate;
        #endregion

        #region Properties
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Final date required.")]
        public DateTime FinalDate { get { return _finalDate; } set { Set(ref _finalDate, value, "FinalDate"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Initial date required.")]
        public DateTime InitialDate { get { return _initialDate; } set { Set(ref _initialDate, value, "InitialDate"); } }
        #endregion

        #region Constructors

        public ContractEntryPeriodContract()
            : base() { }

        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateContractEntryPeriod(ContractEntryPeriodContract obj)
        {
            if (obj.FinalDate <= obj.InitialDate)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Final date must be greater than initial date.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    } 
}
