﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(EntityContractRelContract), "ValidateEntityContract")]
    public class EntityContractRelContract : BaseContract
    {
        #region Members

        private Guid _entity;
        private Guid _contract;

        #endregion
        #region Properties

        [DataMember]
        public Guid Entity { get { return _entity; } set { Set(ref _entity, value, "Entity"); } }
        [DataMember]
        public Guid Contract { get { return _contract; } set { Set(ref _contract, value, "Contract"); } }

        #endregion
        #region Extended Properties

        [DataMember]
        public string OperDescription { get; set; }
        [DataMember]
        public string EntiDescription { get; set; }

        #endregion
        #region Constructors

        public EntityContractRelContract()
            : base() 
        {
        }

        public EntityContractRelContract(Guid id, Guid entity, Guid contract, string entityDescription, string operDescription)
            : base()
        {
            Id = id;
            Entity = entity;
            Contract = contract;
            EntiDescription = entityDescription;
            OperDescription = operDescription;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateEntityContract(ContractEntryPeriodContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}