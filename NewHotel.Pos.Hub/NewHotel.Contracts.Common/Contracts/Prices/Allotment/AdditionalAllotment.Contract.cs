﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(AdditionalAllotmentContract), "ValidateAdditionalAllotment")]
    public class AdditionalAllotmentContract : BaseContract
    {
        #region Members

        [DataMember]
        internal DateTime _workDate;

        #endregion
        #region Properties

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Final date required.")]
        public DateTime FinalDate { get; set; }
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Initial date required.")]
        public DateTime InitialDate { get; set; }
        [DataMember]
        public Guid AditionalId { get; set; }
        [DataMember]
        public short AditionalQuantity { get; set; }

        public DateTime WorkDate { get { return _workDate; } }

        #endregion

        #region Constructors

        public AdditionalAllotmentContract(DateTime workDate)
            : base() 
        {
            _workDate = workDate;
        }

        #endregion

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateAdditionalAllotment(AdditionalAllotmentContract obj)
        {
            if (obj.InitialDate < obj.WorkDate)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Initial date must be less or equal than working date.");
            if (obj.FinalDate < obj.WorkDate)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Final date must be less or equal than working date.");
            if (obj.InitialDate > obj.FinalDate)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Initial date muest be less or equal than Final date.");
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
    }
}