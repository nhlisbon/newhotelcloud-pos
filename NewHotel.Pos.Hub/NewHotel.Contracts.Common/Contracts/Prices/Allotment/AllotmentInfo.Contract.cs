﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class AllotmentInfoContract : BaseContract
    {
        #region Members

        private long _total;
        private long _used;
        private DateTime _date;
        private ARGBColor _backgroundColor;

        #endregion
        #region Properties

        [DataMember]
        public long Total { get { return _total; } set { Set(ref _total, value, "Total"); } }
        [DataMember]
        public long Used { get { return _used; } set { Set(ref _used, value, "Used"); } }
        [DataMember]
        public DateTime Date { get { return _date; } set { Set(ref _date, value, "Date"); } }
        [DataMember]
        public ARGBColor BackgroundColor { get { return _backgroundColor; } set { Set(ref _backgroundColor, value, "BackgroundColor"); } }

        #endregion
    }
}