﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(AllotmentContract), "ValidateAllotment")]
    public class AllotmentContract : BaseContract
    {
        #region Members

        private string _abbreviation;
        private string _description;
        private bool _allotmentInGuarantee;
        private bool _affectBookingChannels;
        private bool _alarmActivated;
        private decimal? _alarmTriggerPercent;
        private EntityClientType _clientType;

        #endregion
        #region Properties

        [DataMember]
        public string Abbreviation { get { return _abbreviation; } set { Set(ref _abbreviation, value, "Abbreviation"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public bool AlarmActivated { get { return _alarmActivated; } set { Set(ref _alarmActivated, value, "AlarmActivated"); } }
        [DataMember]
        public decimal? AlarmTriggerPercent { get { return _alarmTriggerPercent; } set { Set(ref _alarmTriggerPercent, value, "AlarmTriggerPercent"); } }
        [DataMember]
        public EntityClientType Type { get { return _clientType; } set { Set(ref _clientType, value, "Type"); } }

        [DataMember]
        public bool AllotmentInGuarantee
        {
            get { return _allotmentInGuarantee; }
            set
            {
                if (Set(ref _allotmentInGuarantee, value, "AllotmentInGuarantee"))
                {
                    if (_allotmentInGuarantee)
                        AffectBookingChannels = false;
                    NotifyPropertyChanged("IsEnableAffectChannels");
                }
            }
        }
        [DataMember]
        public bool oldAffectBookingChannels { get; set; }
        [DataMember]
        public bool AffectBookingChannels { get { return _affectBookingChannels; } set { Set(ref _affectBookingChannels, value, "AffectBookingChannels"); } }

        [ReflectionExclude]
        public bool IsEnableGarantee { get { return !IsPersisted; } }

        [ReflectionExclude]
        public bool IsEnableAffectChannels { get { return !AllotmentInGuarantee; } }

        #endregion       
        #region Constructors

        public AllotmentContract(DateTime workDate)
            : base() { }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateAllotment(AllotmentContract obj)
        {
            if (string.IsNullOrEmpty(obj.Abbreviation))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Abbreviation is missing");
            if (string.IsNullOrEmpty(obj.Description))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description is missing");
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class AllotmentEditContract : BaseContract
    {
        #region Constructor

        public AllotmentEditContract(bool allotmentInGuarantee, bool affectBookingChannels)
        {
            Details = new TypedList<AllotmentEditItemContract>();
            _allotmentInGuarantee = allotmentInGuarantee;
            _affectBookingChannels = affectBookingChannels;
        }

        #endregion
        #region Members

        private bool _allotmentInGuarantee;
        private bool _affectBookingChannels;
        private DateTime _from;
        private DateTime _to;

        #endregion
        #region Properties

        [DataMember]
        public bool AllotmentInGuarantee { get { return _allotmentInGuarantee; } set { Set(ref _allotmentInGuarantee, value, "AllotmentInGuarantee"); } }
        [DataMember]
        public bool AffectBookingChannels { get { return _affectBookingChannels; } set { Set(ref _affectBookingChannels, value, "AffectBookingChannels"); } }
        [DataMember]
        public DateTime From { get { return _from; } set { Set(ref _from, value, "From"); } }
        [DataMember]
        public DateTime To { get { return _to; } set { Set(ref _to, value, "To"); } }
        [DataMember]
        public TypedList<AllotmentEditItemContract> Details { get; set; }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class AllotmentEditItemContract : BaseContract
    {
        #region Members

        private bool _allotmentInGuarantee;
        private bool _affectBookingChannels;
        private Guid _roomTypeId;
        private string _roomTypeDescription;
        private short _contractedRoomQuantity;
        private short _releaseDays;

        #endregion
        #region Properties

        [DataMember]
        public bool AllotmentInGuarantee { get { return _allotmentInGuarantee; } set { Set(ref _allotmentInGuarantee, value, "AllotmentInGuarantee"); } }
        [DataMember]
        public bool AffectBookingChannels { get { return _affectBookingChannels; } set { Set(ref _affectBookingChannels, value, "AffectBookingChannels"); } }
        [DataMember]
        public Guid RoomTypeId { get { return _roomTypeId; } set { Set(ref _roomTypeId, value, "RoomTypeId"); } }
        [DataMember]
        public string RoomTypeDescription { get { return _roomTypeDescription; } set { Set(ref _roomTypeDescription, value, "RoomTypeDescription"); } }
        [DataMember]
        public short ContractedRoomQuantity { get { return _contractedRoomQuantity; } set { Set(ref _contractedRoomQuantity, value, "ContractedRoomQuantity"); } }
        [DataMember]
        public short ReleaseDays { get { return _releaseDays; } set { Set(ref _releaseDays, value, "ReleaseDays"); } }

        #endregion
    }
}
