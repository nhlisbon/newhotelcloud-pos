﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(PriceRateContract), "ValidatePriceRate")]
    public class PriceRateContract : BaseContract
    {
        #region Members

        [DataMember]
        internal LanguageTranslationContract _description;
        [DataMember]
        internal LanguageTranslationContract _invoiceDescription;
        [DataMember]
        internal string[] _mealPlanDescriptions;
        [DataMember]
        internal bool? _barPriceRate;

        private string _currency;
        private PriceUsedFor _priceType;
        private Guid? _allocationServiceGroupId;
        private string _allocationServiceGroupDesc;
        private bool _noChildsForSingleSuplement;
        private bool _taxIncluded;
        private bool _cityTaxIncluded;
        private bool _turboPriority;
        private string _comments;
        private bool _isBar;
        private Guid _categoryId;
        private short _showOrder;

        private Guid? _parentPriceRateId;
        private bool _supplementInValue;
        private decimal? _supplementPercentOrValue;
        private bool _excludeChildren;
        private bool _excludeExtraBeds;
        private bool _percentValueOverRoom;

        private bool _disableRoomOnlyPlan;
        private bool _disableBedBreakfastPlan;
        private bool _disableHalfBoardPlan;
        private bool _disableFullBoardPlan;
        private bool _disableAllInclusivePlan;

        private Guid? _marketSegmentId;
        private Guid? _cancellationPolicyId;

        private short? _minLOS;
        private short? _minLeadDays;
        private short? _maxLeadDays;
        private DateTime? _minSellDate;
        private DateTime? _maxSellDate;
        private short? _maxOccupancyPercent;

        private string _abbreviation;
        private bool _hideRatePrices;
        private Guid? _supplementSchemaId;
        private bool _inactive;

        [DataMember]
        internal readonly bool _hasDerivedPriceRates;
        [DataMember]
        internal readonly DateTime _workDate;
        [DataMember]
        internal readonly CurrencyInstallationContract _baseCurrency;
        [DataMember]
        internal readonly ARGBColor _colorMonday;
        [DataMember]
        internal readonly ARGBColor _colorTuesday;
        [DataMember]
        internal readonly ARGBColor _colorWednesday;
        [DataMember]
        internal readonly ARGBColor _colorThursday;
        [DataMember]
        internal readonly ARGBColor _colorFriday;
        [DataMember]
        internal readonly ARGBColor _colorSaturday;
        [DataMember]
        internal readonly ARGBColor _colorSunday;
        [DataMember]
        internal readonly ARGBColor _workDateColor;

        #endregion
        #region Properties

        [DataMember]
        public string Abbreviation { get { return _abbreviation; } set { Set(ref _abbreviation, value, "Abbreviation"); } }
        [DataMember]
        public string Currency { get { return _currency; } set { Set(ref _currency, value, "Currency"); } }
        [DataMember]
        public PriceUsedFor PriceType { get { return _priceType; } set { Set(ref _priceType, value, "PriceType"); } }
        [DataMember]
        public Guid? AllocationServiceGroupId { get { return _allocationServiceGroupId; } set { Set(ref _allocationServiceGroupId, value, "AllocationServiceGroupId"); } }
        [DataMember]
        public string AllocationServiceGroupDesc { get { return _allocationServiceGroupDesc; } set { Set(ref _allocationServiceGroupDesc, value, "AllocationServiceGroupDesc"); } }
        [DataMember]
        public bool NoChildsForSingleSuplement { get { return _noChildsForSingleSuplement; } set { Set(ref _noChildsForSingleSuplement, value, "NoChildsForSingleSuplement"); } }
        [DataMember]
        public bool TaxIncluded { get { return _taxIncluded; } set { Set(ref _taxIncluded, value, "TaxIncluded"); } }
        [DataMember]
        public bool CityTaxIncluded { get { return _cityTaxIncluded; } set { Set(ref _cityTaxIncluded, value, "CityTaxIncluded"); } }
        [DataMember]
        public bool TurboPriority { get { return _turboPriority; } set { Set(ref _turboPriority, value, "TurboPriority"); } }
        [DataMember]
        public string Comments { get { return _comments; } set { Set(ref _comments, value, "Comments"); } }
        [DataMember]
        public bool IsBar
        {
            get { return _isBar; }
            set
            {
                if (Set(ref _isBar, value, "IsBar"))
                {
                    if (_isBar)
                        ParentPriceRateId = null;
                    NotifyPropertyChanged("ManualPriceAvailable", "CannotDerive");
                }
            }
        }
        [DataMember]
        public Guid CategoryId { get { return _categoryId; } set { Set(ref _categoryId, value, "CategoryId"); } }
        [DataMember]
        public short ShowOrder { get { return _showOrder; } set { Set(ref _showOrder, value, "ShowOrder"); } }
        [DataMember]
        public bool IsExportable { get; set; }
        [DataMember]
        public Guid? ParentPriceRateId
        {
            get { return _parentPriceRateId; }
            set
            {
                if (Set(ref _parentPriceRateId, value, "ParentPriceRateId"))
                    NotifyPropertyChanged("IsDerived");
            }
        }
        [DataMember]
        public bool SupplementInValue { get { return _supplementInValue; } set { Set(ref _supplementInValue, value, "SupplementInValue"); } }
        [DataMember]
        public decimal? SupplementPercentOrValue { get { return _supplementPercentOrValue; } set { Set(ref _supplementPercentOrValue, value, "SupplementPercentOrValue"); } }
        [DataMember]
        public bool ExcludeChildren { get { return _excludeChildren; } set { Set(ref _excludeChildren, value, "ExcludeChildren"); } }
        [DataMember]
        public bool ExcludeExtraBeds { get { return _excludeExtraBeds; } set { Set(ref _excludeExtraBeds, value, "ExcludeExtraBeds"); } }
        [DataMember]
        public bool PercentValueOverRoom { get { return _percentValueOverRoom; } set { Set(ref _percentValueOverRoom, value, "PercentValueOverRoom"); } }

        [DataMember]
        public bool DisableRoomOnlyPlan { get { return _disableRoomOnlyPlan; } set { Set(ref _disableRoomOnlyPlan, value, "DisableRoomOnlyPlan"); } }
        [DataMember]
        public bool DisableBedBreakfastPlan { get { return _disableBedBreakfastPlan; } set { Set(ref _disableBedBreakfastPlan, value, "DisableBedBreakfastPlan"); } }
        [DataMember]
        public bool DisableHalfBoardPlan { get { return _disableHalfBoardPlan; } set { Set(ref _disableHalfBoardPlan, value, "DisableHalfBoardPlan"); } }
        [DataMember]
        public bool DisableFullBoardPlan { get { return _disableFullBoardPlan; } set { Set(ref _disableFullBoardPlan, value, "DisableFullBoardPlan"); } }
        [DataMember]
        public bool DisableAllInclusivePlan { get { return _disableAllInclusivePlan; } set { Set(ref _disableAllInclusivePlan, value, "DisableAllInclusivePlan"); } }
        [DataMember]
        public Guid? MarketSegmentId { get { return _marketSegmentId; } set { Set(ref _marketSegmentId, value, "MarketSegmentId"); } }
        [DataMember]
        public Guid? CancellationPolicyId { get { return _cancellationPolicyId; } set { Set(ref _cancellationPolicyId, value, "CancellationPolicyId"); } }

        [DataMember]
        public short? MinLOS { get { return _minLOS; } set { Set(ref _minLOS, value, "MinLOS"); } }
        [DataMember]
        public short? MinLeadDays { get { return _minLeadDays; } set { Set(ref _minLeadDays, value, "MinLeadDays"); } }
        [DataMember]
        public short? MaxLeadDays { get { return _maxLeadDays; } set { Set(ref _maxLeadDays, value, "MaxLeadDays"); } }
        [DataMember]
        public DateTime? MinSellDate { get { return _minSellDate; } set { Set(ref _minSellDate, value, "MinSellDate"); } }
        [DataMember]
        public DateTime? MaxSellDate { get { return _maxSellDate; } set { Set(ref _maxSellDate, value, "MaxSellDate"); } }
        [DataMember]
        public short? MaxOccupancyPercent { get { return _maxOccupancyPercent; } set { Set(ref _maxOccupancyPercent, value, "MaxOccupancyPercent"); } }
        [DataMember]
        public bool HideRatePrices { get { return _hideRatePrices; } set { Set(ref _hideRatePrices, value, "HideRatePrices"); } }
        [DataMember]
        public Guid? SupplementSchemaId { get { return _supplementSchemaId; } set { Set(ref _supplementSchemaId, value, "SupplementSchemaId"); } }
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }

        [ReflectionExclude]
        public bool CannotDerive
        {
            get { return _hasDerivedPriceRates || _isBar; }
        }

        [ReflectionExclude]
        public bool HasDerivedPriceRates
        {
            get { return _hasDerivedPriceRates; }
        }

        [ReflectionExclude]
        public bool ManualPriceAvailable
        {
            get { return IsPersisted && !IsBar; }
        }

        [ReflectionExclude]
        public bool IsDerived
        {
            get { return _parentPriceRateId.HasValue; }
        }

        [ReflectionExclude]
        public string[] MealPlanDescriptions
        {
            get { return _mealPlanDescriptions; }
        }

        [ReflectionExclude]
        public bool ConvertedToBar
        {
            get { return _barPriceRate.HasValue && !_barPriceRate.Value && _isBar; }
        }

        #endregion
        #region Contracts

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        [ReflectionExclude]
        public LanguageTranslationContract InvoiceDescription
        {
            get { return _invoiceDescription; }
            set { _invoiceDescription = value; }
        }

        [ReflectionExclude]
        public TypedList<LockPeriodContract> LockPeriods { get; set; }

        [ReflectionExclude]
        public TypedList<AgeRangeContract> AgeRanges { get; set; }

        [ReflectionExclude]
        public TypedList<AgeRangeContract> GeneralAgeRanges { get; set; }

        [ReflectionExclude]
        public TypedList<ServicePriceContract> ServicePrices { get; set; }

        [ReflectionExclude]
        public TypedList<ServiceUpSellContract> ServiceUpSellItems { get; set; }

        [ReflectionExclude]
        public TypedList<EarlyBookingContract> EarlyBookings { get; set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<PriceRateServiceSupplementContract> ServiceSupplements { get; set; }

        #endregion
        #region Parameters

        public DateTime WorkDate { get { return _workDate; } }
        public CurrencyInstallationContract BaseCurrency { get { return _baseCurrency; } }

        public ARGBColor ColorMonday { get { return _colorMonday; } }
        public ARGBColor ColorTuesday { get { return _colorTuesday; } }
        public ARGBColor ColorWednesday { get { return _colorWednesday; } }
        public ARGBColor ColorThursday { get { return _colorThursday; } }
        public ARGBColor ColorFriday { get { return _colorFriday; } }
        public ARGBColor ColorSaturday { get { return _colorSaturday; } }
        public ARGBColor ColorSunday { get { return _colorSunday; } }       
        public ARGBColor WorkDateColor { get { return _workDateColor; } }

        #endregion
        #region Constructors

        public PriceRateContract(AgeRangeContract[] generalAgeRanges, string[] mealPlanDescriptions,
            DateTime workDate, CurrencyInstallationContract baseCurrency, bool hasDerivedPriceRates,
            ARGBColor mondayColor, ARGBColor tuesdayColor, ARGBColor wednesdayColor, ARGBColor thursdayColor, 
            ARGBColor fridayColor, ARGBColor saturdayColor, ARGBColor sundayColor, ARGBColor workDateColor,
            bool? barPriceRate = null)
            : base() 
        {
            _description = new LanguageTranslationContract();
            _invoiceDescription = new LanguageTranslationContract();
            _mealPlanDescriptions = mealPlanDescriptions;
            LockPeriods = new TypedList<LockPeriodContract>();
            ServicePrices = new TypedList<ServicePriceContract>();
            ServiceUpSellItems = new TypedList<ServiceUpSellContract>();
            AgeRanges = new TypedList<AgeRangeContract>();
            GeneralAgeRanges = new TypedList<AgeRangeContract>(generalAgeRanges);
            EarlyBookings = new TypedList<EarlyBookingContract>();
            ServiceSupplements = new TypedList<PriceRateServiceSupplementContract>();
            Comments = string.Empty;
            _workDate = workDate;
            _baseCurrency = baseCurrency;
            _hasDerivedPriceRates = hasDerivedPriceRates;

            _colorMonday = mondayColor;
            _colorTuesday = tuesdayColor;
            _colorWednesday = wednesdayColor;
            _colorThursday = thursdayColor;
            _colorFriday = fridayColor;
            _colorSaturday = saturdayColor;
            _colorSunday = sundayColor;
            _workDateColor = workDateColor;

            _barPriceRate = barPriceRate;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidatePriceRate(PriceRateContract obj)
        {
            if (obj.Description == string.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description required.");

            if (obj.ParentPriceRateId.HasValue)
            {
                if (!obj.SupplementPercentOrValue.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Percent or Value required.");
            }

            if (!obj.AllocationServiceGroupId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Breakdown required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
