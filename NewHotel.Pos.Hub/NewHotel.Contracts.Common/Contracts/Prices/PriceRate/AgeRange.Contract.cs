﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(AgeRangeContract), "ValidateAgeRange")]
    public class AgeRangeContract : BaseContract
    {
        #region Members
        private short? _initialAge;
        private short? _finalAge;
        #endregion

        #region Properties
        [NonNegativeValue("InitialAge")]
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Initial age required.")]
        public short? InitialAge { get { return _initialAge; } set { Set(ref _initialAge, value, "InitialAge"); } }
        [NonNegativeValue("FinalAge")]
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Final age required.")]
        public short? FinalAge { get { return _finalAge; } set { Set(ref _finalAge, value, "FinalAge"); } }
        [DataMember]
        public Guid? PriceRateId { get; set; }
        #endregion

        #region Constructors

        public AgeRangeContract()
            : base() { }

        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateAgeRange(AgeRangeContract obj)
        {
            if (obj.InitialAge.HasValue && obj.InitialAge.Value <= 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Initial age must be greater than 0.");
            if (obj.FinalAge.HasValue && obj.FinalAge.Value <= 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Final age must be greater than 0.");
            if (obj.FinalAge.HasValue && obj.InitialAge.HasValue && obj.InitialAge.Value >= obj.FinalAge.Value)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Initial age greater than final age.");
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
