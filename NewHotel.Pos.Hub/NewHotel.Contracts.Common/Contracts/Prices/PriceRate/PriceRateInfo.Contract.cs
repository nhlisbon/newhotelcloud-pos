﻿using System;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PriceRateInfoContract : BaseContract
    {
        #region Properties

        [DataMember]
        public DateTime Date { get; internal set; }
        [DataMember]
        public decimal? Price { get; set; }
		[DataMember]
		public string CurrencySymbol { get; internal set; }
        [DataMember]
        public decimal? Exchange { get; set; }
        [DataMember]
        public bool IsBlock { get; set; }
        [DataMember]
        public string Error { get; set; }
        [DataMember]
        public DateTime WorkDate { get; internal set; }

        #endregion
        #region Constructor

		public PriceRateInfoContract(DateTime workDate, DateTime date, string currencySymbol)
        {
            WorkDate = workDate;
            Date = date;
			CurrencySymbol = currencySymbol;
        }

        public PriceRateInfoContract(DateTime workDate, DateTime date, string currencySymbol, decimal? price, decimal? exchange, bool isBlock)
			: this(workDate, date, currencySymbol)
        {
            Price = price;
            Exchange = exchange;
            IsBlock = isBlock;
        }

        public bool HasErrors
        {
            get { return !string.IsNullOrEmpty(Error); }
        }

        #endregion
		#region Public Methods

		public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine(Date.Day.ToString());
            if (Price.HasValue)
            {
                sb.AppendLine(string.Format("$ {0}", Price));
                if (Exchange.HasValue)
                    sb.AppendLine(string.Format("§ {0}", Exchange));
            }

            return sb.ToString();
		}

		#endregion
	}
}
