﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public class ChildPriceChangedEventArgs : PriceChangedEventArgs
    {
        public readonly ChildLine ChildLine;

        public ChildPriceChangedEventArgs(ChildLine childLine, short pensionModeId, short? orderNumber, decimal? price)
            : base(pensionModeId, orderNumber, price)
        {
            ChildLine = childLine;
        }
    }

    public delegate void ChildPriceChangedEventHandler(object sender, ChildPriceChangedEventArgs e);

    public interface IDiscountChildPrice
    {
        decimal? DiscountPercent { get; set; }
    }

    [DataContract]
	[Serializable]
    [KnownType(typeof(RoomChildPriceContract))]
    public abstract class BaseChildPriceContract : BaseContract
    {
        #region Members

        [DataMember]
        internal ChildLine _childLine;
        [DataMember]
        internal short _pensionMode;
        [DataMember]
        internal short? _orderNumber;
        private Guid? _ageRangeId;
        private string _ageRangeDesc;
        private short? _children;
        private short? _adults;

        #endregion
        #region Properties

        public ChildLine ChildLine 
        {
            get { return _childLine; }
        }

        public short PensionMode 
        {
            get { return _pensionMode; }
        }

        [DataMember]
        public Guid? AgeRangeId
        {
            get { return _ageRangeId; }
            set { Set(ref _ageRangeId, value, "AgeRangeId"); }
        }

        [DataMember]
        public string AgeRangeDesc
        {
            get { return _ageRangeDesc; }
            set { Set(ref _ageRangeDesc, value, "AgeRangeDesc"); }
        }

        [DataMember]
        public short? Children
        {
            get { return _children; }
            set { Set(ref _children, value, "Children"); }
        }

        [DataMember]
        public short? Adults
        {
            get { return _adults; }
            set { Set(ref _adults, value, "Adults"); }
        }

        public short? OrderNumber 
        {
            get { return _orderNumber; }
        }

        #endregion
        #region Constructors

        public BaseChildPriceContract(ChildLine childLine, short pensionMode, short? orderNumber)
            : base()
        {
            _childLine = childLine;
            _pensionMode = pensionMode;
            _orderNumber = orderNumber;
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    [KnownType(typeof(DiscountRoomChildPriceContract))]
    [KnownType(typeof(RoomBreakfastChildPriceContract))]
    public class RoomChildPriceContract : BaseChildPriceContract, IRoomPrice
    {
        #region Members

        public bool Modified = false;
        private decimal? _roomPrice;

        [DataMember]
        public bool CalculateRoomPrice = false;

        #endregion
        #region Events

        public event ChildPriceChangedEventHandler PriceChanged;

        #endregion
        #region Properties

        [DataMember]
        public decimal? RoomPrice
        {
            get { return _roomPrice; }
            set
            {
                if (Set(ref _roomPrice, value, "RoomPrice"))
                {
                    Modified = true;
                    if (PriceChanged != null)
                        PriceChanged(this, new ChildPriceChangedEventArgs(ChildLine, PensionMode, OrderNumber, RoomPrice));
                }
            }
        }

        #endregion
        #region Constructors

        public RoomChildPriceContract(ChildLine childLine, short pensionMode, short? orderNumber)
            : base(childLine, pensionMode, orderNumber) { }

        #endregion
        #region Public Methods

        public void SetRoomPrice(decimal price)
        {
            if (price != _roomPrice)
            {
                if (price < 0)
                    _roomPrice = null;
                else
                    _roomPrice = price;
                NotifyPropertyChanged("RoomPrice");
            }
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class DiscountRoomChildPriceContract : RoomChildPriceContract, IDiscountChildPrice
    {
        #region Members

        private decimal? _discountPercent;

        #endregion
        #region Properties

        [DataMember]
        public decimal? DiscountPercent
        {
            get { return _discountPercent; }
            set { Set(ref _discountPercent, value, "DiscountPercent"); }
        }

        #endregion
        #region Constructors

        public DiscountRoomChildPriceContract(ChildLine childLine, short pensionMode, short? orderNumber)
            : base(childLine, pensionMode, orderNumber) { }

        #endregion
    }

    [DataContract]
	[Serializable]
    [KnownType(typeof(DiscountRoomBreakfastChildPriceContract))]
    [KnownType(typeof(ChildPriceContract))]
    public class RoomBreakfastChildPriceContract : RoomChildPriceContract
    {
        #region Members

        private decimal? _breakfastPrice;
        private decimal? _drinkPriceAtBreakfast;

        #endregion
        #region Properties

        [DataMember]
        public decimal? BreakfastPrice
        {
            get { return _breakfastPrice; }
            set { Set(ref _breakfastPrice, value, "BreakfastPrice"); }
        }

        [DataMember]
        public decimal? DrinkPriceAtBreakfast
        {
            get { return _drinkPriceAtBreakfast; }
            set { Set(ref _drinkPriceAtBreakfast, value, "DrinkPriceAtBreakfast"); }
        }

        #endregion
        #region Constructors

        public RoomBreakfastChildPriceContract(ChildLine childLine, short pensionMode, short? orderNumber)
            : base(childLine, pensionMode, orderNumber) { }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class DiscountRoomBreakfastChildPriceContract : RoomBreakfastChildPriceContract, IDiscountChildPrice
    {
        #region Members

        private decimal? _discountPercent;

        #endregion
        #region Properties

        [DataMember]
        public decimal? DiscountPercent
        {
            get { return _discountPercent; }
            set { Set(ref _discountPercent, value, "DiscountPercent"); }
        }

        #endregion
        #region Constructors

        public DiscountRoomBreakfastChildPriceContract(ChildLine childLine, short pensionMode, short? orderNumber)
            : base(childLine, pensionMode, orderNumber) { }

        #endregion
    }

    [DataContract]
	[Serializable]
    [KnownType(typeof(DiscountChildPriceContract))]
    public class ChildPriceContract : RoomBreakfastChildPriceContract
    {
        #region Members

        private decimal? _lunchPrice;
        private decimal? _drinkPriceAtLunch;
        private decimal? _dinnerPrice;
        private decimal? _drinkPriceAtDinner;

        #endregion
        #region Properties

        [DataMember]
        public decimal? LunchPrice
        {
            get { return _lunchPrice; }
            set { Set(ref _lunchPrice, value, "LunchPrice"); }
        }

        [DataMember]
        public decimal? DrinkPriceAtLunch
        {
            get { return _drinkPriceAtLunch; }
            set { Set(ref _drinkPriceAtLunch, value, "DrinkPriceAtLunch"); }
        }

        [DataMember]
        public decimal? DinnerPrice
        {
            get { return _dinnerPrice; }
            set { Set(ref _dinnerPrice, value, "DinnerPrice"); }
        }

        [DataMember]
        public decimal? DrinkPriceAtDinner
        {
            get { return _drinkPriceAtDinner; }
            set { Set(ref _drinkPriceAtDinner, value, "DrinkPriceAtDinner"); }
        }

        #endregion
        #region Constructors

        public ChildPriceContract(ChildLine childLine, short pensionMode, short? orderNumber)
            : base(childLine, pensionMode, orderNumber) { }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class DiscountChildPriceContract : ChildPriceContract, IDiscountChildPrice
    {
        #region Members

        private decimal? _discountPercent;

        #endregion
        #region Properties

        [DataMember]
        public decimal? DiscountPercent
        {
            get { return _discountPercent; }
            set { Set(ref _discountPercent, value, "DiscountPercent"); }
        }

        #endregion
        #region Constructors

        public DiscountChildPriceContract(ChildLine childLine, short pensionMode, short? orderNumber)
            : base(childLine, pensionMode, orderNumber) { }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class ChildPriceByLineTypeContract : BaseContract
    {
        #region Members

        [DataMember]
        internal int _maxPersons;
        [DataMember]
        internal ChildLine _lineType;
        [DataMember]
        internal string _lineTypeDesc;
        [DataMember]
        internal string _roomPensionDesc;
        [DataMember]
        internal string _roomBreakfastPensionDesc;
        [DataMember]
        internal string _roomBreakfast1MealPensionDesc;
        [DataMember]
        internal string _roomBreakfast2MealsPensionDesc;
        [DataMember]
        internal string _allInclusivePensionDesc;

        [DataMember]
        internal TypedList<ChildPriceContract> _allPensionPrices;
        [DataMember]
        internal TypedList<RoomChildPriceContract> _roomPrices;
        [DataMember]
        internal TypedList<RoomBreakfastChildPriceContract> _roomBreakfastPrices;
        [DataMember]
        internal TypedList<ChildPriceContract> _roomBreakfast1MealPrices;
        [DataMember]
        internal TypedList<ChildPriceContract> _roomBreakfast2MealsPrices;
        [DataMember]
        internal TypedList<ChildPriceContract> _allInclusivePrices;

        public IEnumerable<RoomChildPriceContract> Prices
        {
            get
            {
                var prices = new List<RoomChildPriceContract>();
                prices.AddRange(RoomPrices);
                prices.AddRange(RoomBreakfastPrices);
                prices.AddRange(RoomBreakfast1MealPrices);
                prices.AddRange(RoomBreakfast2MealsPrices);
                prices.AddRange(AllInclusivePrices);

                return prices;
            }
        }

        #endregion
        #region Properties

        public int MaxPersons
        {
            get { return _maxPersons; }
        }

        public ChildLine LineType
        {
            get { return _lineType; }
        }

        public string LineTypeDesc
        {
            get { return _lineTypeDesc; }
        }

        public string RoomPensionDesc
        {
            get { return _roomPensionDesc; }
        }

        public string RoomBreakfastPensionDesc
        {
            get { return _roomBreakfastPensionDesc; }
        }

        public string RoomBreakfast1MealPensionDesc
        {
            get { return _roomBreakfast1MealPensionDesc; }
        }

        public string RoomBreakfast2MealsPensionDesc
        {
            get { return _roomBreakfast2MealsPensionDesc; }
        }

        public string AllInclusivePensionDesc
        {
            get { return _allInclusivePensionDesc; }
        }

        #endregion
        #region Contracts

        public TypedList<ChildPriceContract> AllPensionPrices
        {
            get { return _allPensionPrices; }
        }

        public TypedList<RoomChildPriceContract> RoomPrices
        {
            get { return _roomPrices; }
        }

        public TypedList<RoomBreakfastChildPriceContract> RoomBreakfastPrices
        {
            get { return _roomBreakfastPrices; }
        }

        public TypedList<ChildPriceContract> RoomBreakfast1MealPrices
        {
            get { return _roomBreakfast1MealPrices; }
        }

        public TypedList<ChildPriceContract> RoomBreakfast2MealsPrices
        {
            get { return _roomBreakfast2MealsPrices; }
        }

        public TypedList<ChildPriceContract> AllInclusivePrices
        {
            get { return _allInclusivePrices; }
        }

        #endregion
        #region Constructor

        public ChildPriceByLineTypeContract(ChildLine lineType, string lineTypeDesc,
            string roomPensionDesc, string roomBreakfastPensionDesc,
            string roomBreakfast1MealPensionDesc, string roomBreakfast2MealsPensionDesc,
            string allInclusivePensionDesc, short maxPersons)
            : base()
        {
            _maxPersons = maxPersons;
            _lineType = lineType;
            _lineTypeDesc = lineTypeDesc;
            _roomPensionDesc = roomPensionDesc;
            _roomBreakfastPensionDesc = roomBreakfastPensionDesc;
            _roomBreakfast1MealPensionDesc = roomBreakfast1MealPensionDesc;
            _roomBreakfast2MealsPensionDesc = roomBreakfast2MealsPensionDesc;
            _allInclusivePensionDesc = allInclusivePensionDesc;

            _allPensionPrices = new TypedList<ChildPriceContract>();
            _roomPrices = new TypedList<RoomChildPriceContract>();
            _roomBreakfastPrices = new TypedList<RoomBreakfastChildPriceContract>();
            _roomBreakfast1MealPrices = new TypedList<ChildPriceContract>();
            _roomBreakfast2MealsPrices = new TypedList<ChildPriceContract>();
            _allInclusivePrices = new TypedList<ChildPriceContract>();

            _allPensionPrices.Add(new ChildPriceContract(ChildLine.Child, 0, null));
            _roomPrices.Add(new ChildPriceContract(ChildLine.Child, 1, null));
            _roomBreakfastPrices.Add(new ChildPriceContract(ChildLine.Child, 2, null));
            _roomBreakfast1MealPrices.Add(new ChildPriceContract(ChildLine.Child, 3, null));
            _roomBreakfast2MealsPrices.Add(new ChildPriceContract(ChildLine.Child, 4, null));
            _allInclusivePrices.Add(new ChildPriceContract(ChildLine.Child, 5, null));
            for (short i = 1; i <= _maxPersons; i++)
            {
                _allPensionPrices.Add(new ChildPriceContract(ChildLine.Child, 0, i));
                _roomPrices.Add(new ChildPriceContract(ChildLine.Child, 1, i));
                _roomBreakfastPrices.Add(new ChildPriceContract(ChildLine.Child, 2, i));
                _roomBreakfast1MealPrices.Add(new ChildPriceContract(ChildLine.Child, 3, i));
                _roomBreakfast2MealsPrices.Add(new ChildPriceContract(ChildLine.Child, 4, i));
                _allInclusivePrices.Add(new ChildPriceContract(ChildLine.Child, 5, i));
            }
        }

        #endregion
        #region Methods

        public RoomChildPriceContract GetAnyOrderChildPriceForPension(short pensionModeId)
        {
            IEnumerable<RoomChildPriceContract> childPrices = null;
            switch (pensionModeId)
            {
                case 0:
                    childPrices = AllPensionPrices.Cast<RoomChildPriceContract>();
                    break;
                case 1:
                    childPrices = RoomPrices;
                    break;
                case 2:
                    childPrices = RoomBreakfastPrices.Cast<RoomChildPriceContract>();
                    break;
                case 3:
                    childPrices = RoomBreakfast1MealPrices.Cast<RoomChildPriceContract>();
                    break;
                case 4:
                    childPrices = RoomBreakfast2MealsPrices.Cast<RoomChildPriceContract>();
                    break;
                case 5:
                    childPrices = AllInclusivePrices.Cast<RoomChildPriceContract>();
                    break;
            }

            return childPrices.FirstOrDefault(x => x.PensionMode.Equals(pensionModeId) &&
                !x.Adults.HasValue && !x.Children.HasValue && !x.OrderNumber.HasValue && !x.AgeRangeId.HasValue);
        }

        #endregion
    }
}
