﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class EarlyBookingDateContract : BaseEarlyBookingContract
    {
        #region Properties

        [Required("InitialCreationDate")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Initial creation date required.")]
        [DataMember]
        public DateTime? InitialCreationDate { get; set; }
        [DataMember]
        public DateTime? FinalCreationDate { get; set; }
        [DataMember]
        public DateTime? InitialArrivalDate { get; set; }
        [DataMember]
        public DateTime? FinalArrivalDate { get; set; }

        #endregion
        #region Constructors

        public EarlyBookingDateContract(EarlyBookingContract earlyBookingContract)
            : base(earlyBookingContract) { }

        #endregion
    }
}
