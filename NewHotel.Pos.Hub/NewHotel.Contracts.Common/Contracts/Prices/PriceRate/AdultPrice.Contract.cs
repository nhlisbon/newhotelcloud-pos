﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public class AdultPriceChangedEventArgs : PriceChangedEventArgs
    {
        public readonly AdultLine AdultLine;

        public AdultPriceChangedEventArgs(AdultLine adultLine, short pensionModeId, short? orderNumber, decimal? price)
            : base(pensionModeId, orderNumber, price)
        {
            AdultLine = adultLine;
        }
    }

    public interface IRoomPrice
    {
        decimal? RoomPrice { get; set; }
    }

    public delegate void AdultPriceChangedEventHandler(object sender, AdultPriceChangedEventArgs e);

    [DataContract]
	[Serializable]
    public class RoomPriceContract : BaseContract, IRoomPrice
    {
        #region Members

		public bool Modified = false;
        private decimal? _roomPrice;

        [DataMember]
        internal readonly AdultLine _adultLine;
        [DataMember]
        internal readonly short _pensionModeId;
        [DataMember]
        public bool CalculateRoomPrice = false;

        #endregion
        #region Properties

        [DataMember]
        public decimal? RoomPrice 
        { 
            get { return _roomPrice; } 
            set 
            {
                if (Set(ref _roomPrice, value, "RoomPrice"))
                    Modified = true;
            }
        }

        public AdultLine AdultLine
        {
            get { return _adultLine; }
        }

        public short PensionModeId
        {
            get { return _pensionModeId; }
        }

        #endregion
        #region Constructor

        public RoomPriceContract()
        {
        }

        public RoomPriceContract(AdultLine adultLine, short pensionModeId)
            : this()
        {
            _adultLine = adultLine;
            _pensionModeId = pensionModeId;
        }

        #endregion
        #region Public Methods

        public void SetRoomPrice(decimal? roomPrice, decimal mealsPrice)
        {
            decimal? price = null;
            if (roomPrice.HasValue)
            {
                price = roomPrice.Value + mealsPrice;
                if (price.Value < decimal.Zero)
                    price = decimal.Zero;
            }

            if (price != _roomPrice)
            {
                _roomPrice = price;
                NotifyPropertyChanged("RoomPrice");
            }
        }

        #endregion
    }

    // For Adult type line
    [DataContract]
	[Serializable]
    [KnownType(typeof(DiscountAdultPriceContract))]
    public class AdultPriceContract : BaseContract
    {
        #region Members

        [DataMember]
        internal short? _orderNumber;
        private RoomPriceContract _anyPensionPrice;
        private RoomPriceContract _roomPensionPrice;
        private RoomPriceContract _roomBreakfastPensionPrice;
        private RoomPriceContract _roomBreakfast1MealPensionPrice;
        private RoomPriceContract _roomBreakfast2MealsPensionPrice;
        private RoomPriceContract _allInclusivePensionPrice;

        #endregion
        #region Events

        public event AdultPriceChangedEventHandler PriceChanged;

        #endregion
        #region Properties

        public short? OrderNumber { get { return _orderNumber; } }
        public RoomPriceContract AnyPensionPrice { get { return _anyPensionPrice; } }
        public RoomPriceContract RoomPensionPrice { get { return _roomPensionPrice; } }
        public RoomPriceContract RoomBreakfastPensionPrice { get { return _roomBreakfastPensionPrice; } }
        public RoomPriceContract RoomBreakfast1MealPensionPrice { get { return _roomBreakfast1MealPensionPrice; } }
        public RoomPriceContract RoomBreakfast2MealsPensionPrice { get { return _roomBreakfast2MealsPensionPrice; } }
        public RoomPriceContract AllInclusivePensionPrice { get { return _allInclusivePensionPrice; } }

        [DataMember]
        internal RoomPriceContract anyPensionPrice
        {
            get { return _anyPensionPrice; }
            set
            {
                _anyPensionPrice = value;
                _anyPensionPrice.PropertyChanged += new PropertyChangedEventHandler(OnPropertyChanged);
            }
        }

        [DataMember]
        internal RoomPriceContract roomPensionPrice
        {
            get { return _roomPensionPrice; }
            set
            {
                _roomPensionPrice = value;
                _roomPensionPrice.PropertyChanged += new PropertyChangedEventHandler(OnPropertyChanged);
            }
        }

        [DataMember]
        internal RoomPriceContract roomBreakfastPensionPrice
        {
            get { return _roomBreakfastPensionPrice; }
            set
            {
                _roomBreakfastPensionPrice = value;
                _roomBreakfastPensionPrice.PropertyChanged += new PropertyChangedEventHandler(OnPropertyChanged);
            }
        }

        [DataMember]
        internal RoomPriceContract roomBreakfast1MealPensionPrice
        {
            get { return _roomBreakfast1MealPensionPrice; }
            set
            {
                _roomBreakfast1MealPensionPrice = value;
                _roomBreakfast1MealPensionPrice.PropertyChanged += new PropertyChangedEventHandler(OnPropertyChanged);
            }
        }

        [DataMember]
        internal RoomPriceContract roomBreakfast2MealsPensionPrice
        {
            get { return _roomBreakfast2MealsPensionPrice; }
            set
            {
                _roomBreakfast2MealsPensionPrice = value;
                _roomBreakfast2MealsPensionPrice.PropertyChanged += new PropertyChangedEventHandler(OnPropertyChanged);
            }
        }

        [DataMember]
        internal RoomPriceContract allInclusivePensionPrice
        {
            get { return _allInclusivePensionPrice; }
            set
            {
                _allInclusivePensionPrice = value;
                _allInclusivePensionPrice.PropertyChanged += new PropertyChangedEventHandler(OnPropertyChanged);
            }
        }

        #endregion
        #region Constructors

        public AdultPriceContract(AdultLine adultLine)
            : base() 
        {
            anyPensionPrice = new RoomPriceContract(adultLine, 0);
            roomPensionPrice = new RoomPriceContract(adultLine, 1);
            roomBreakfastPensionPrice = new RoomPriceContract(adultLine, 2);
            roomBreakfast1MealPensionPrice = new RoomPriceContract(adultLine, 3);
            roomBreakfast2MealsPensionPrice = new RoomPriceContract(adultLine, 4);
            allInclusivePensionPrice = new RoomPriceContract(adultLine, 5);
        }

        public AdultPriceContract(AdultLine adultLine, short orderNumber)
            : this(adultLine) 
        {
            _orderNumber = orderNumber;
        }

        #endregion
        #region Private Methods

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "RoomPrice")
            {
                if (PriceChanged != null)
                {
                    var contract = (RoomPriceContract)sender;
                    PriceChanged(this, new AdultPriceChangedEventArgs(contract.AdultLine, contract.PensionModeId, _orderNumber, contract.RoomPrice));
                }
            }
        }

        #endregion
    }

    // For ExtraAdult and Single Suplement
    [DataContract]
	[Serializable]
    public class DiscountAdultPriceContract : AdultPriceContract
    {
        #region Members

        private bool _isDiscountPercent;

        #endregion
        #region Properties

        [DataMember]
        public bool IsDiscountPercent { get { return _isDiscountPercent; } set { Set<bool>(ref _isDiscountPercent, value, "IsDiscountPercent"); } }

        #endregion
        #region Constructors

        public DiscountAdultPriceContract(AdultLine adultLine)
            : base(adultLine) { }

        public DiscountAdultPriceContract(AdultLine adultLine, short orderNumber)
            : base(adultLine, orderNumber) { }

        #endregion
    }

    [DataContract]
	[Serializable]
    [KnownType(typeof(RoomAdultPriceByLineTypeContract))]
    [KnownType(typeof(PaxsAdultPriceByLineTypeContract))]
    public abstract class AdultPriceByLineTypeContract : BaseContract
    {
        #region Constants

        public const short BehondMaxOrder = 99;

        #endregion
        #region Members

        private bool _isDiscountPercentPrice;
        [DataMember]
        internal AdultLine _lineType;
        [DataMember]
        internal string _lineTypeDesc;
        [DataMember]
        internal string _roomPensionDesc;
        [DataMember]
        internal string _roomBreakfastPensionDesc;
        [DataMember]
        internal string _roomBreakfast1MealPensionDesc;
        [DataMember]
        internal string _roomBreakfast2MealsPensionDesc;
        [DataMember]
        internal string _allInclusivePensionDesc;
        [DataMember]
        internal bool _paxsApplicationMode;
        [DataMember]
        internal TypedList<AdultPriceContract> _prices;

        private bool _autoCalcRoomPension = true;
		private bool _autoCalcRoomBreakfastPension = true;
		private bool _autoCalcRoomBreakfast1MealPension = true;
		private bool _autoCalcRoomBreakfast2MealPension = true;
		private bool _autoCalcAllInclusivePension = true;

        #endregion
        #region Properties

        public AdultLine LineType { get { return _lineType; } }
        public string LineTypeDesc { get { return _lineTypeDesc; } }
        public bool PaxsApplicationMode { get { return _paxsApplicationMode; } }

        [DataMember]
        public bool IsDiscountPercentPrice { get { return _isDiscountPercentPrice; } set { Set(ref _isDiscountPercentPrice, value, "IsDiscountPercentPrice"); } }
		public bool IsValuePrice { get { return !_isDiscountPercentPrice; } set { Set(ref _isDiscountPercentPrice, !value, "IsValuePrice"); } }

		[DataMember]
		public bool AutoCalcRoomPension { get { return _autoCalcRoomPension; } set { Set(ref _autoCalcRoomPension, value, "AutoCalcRoomPension"); } }
		[DataMember]
		public bool AutoCalcRoomBreakfastPension { get { return _autoCalcRoomBreakfastPension; } set { Set(ref _autoCalcRoomBreakfastPension, value, "AutoCalcRoomBreakfastPension"); } }
		[DataMember]
		public bool AutoCalcRoomBreakfast1MealPension { get { return _autoCalcRoomBreakfast1MealPension; } set { Set(ref _autoCalcRoomBreakfast1MealPension, value, "AutoCalcRoomBreakfast1MealPension"); } }
		[DataMember]
		public bool AutoCalcRoomBreakfast2MealPension { get { return _autoCalcRoomBreakfast2MealPension; } set { Set(ref _autoCalcRoomBreakfast2MealPension, value, "AutoCalcRoomBreakfast2MealPension"); } }
		[DataMember]
		public bool AutoCalcAllInclusivePension { get { return _autoCalcAllInclusivePension; } set { Set(ref _autoCalcAllInclusivePension, value, "AutoCalcAllInclusivePension"); } }

        #endregion
        #region Contracts

        public string RoomPensionDesc { get { return _roomPensionDesc; } }
        public string RoomBreakfastPensionDesc { get { return _roomBreakfastPensionDesc; } }
        public string RoomBreakfast1MealPensionDesc { get { return _roomBreakfast1MealPensionDesc; } }
        public string RoomBreakfast2MealsPensionDesc { get { return _roomBreakfast2MealsPensionDesc; } }
        public string AllInclusivePensionDesc { get { return _allInclusivePensionDesc; } }

        public TypedList<AdultPriceContract> Prices { get { return _prices; } }

        #endregion
        #region Constructors

        public AdultPriceByLineTypeContract()
            : base()
        {
            _prices = new TypedList<AdultPriceContract>();
        }

        public AdultPriceByLineTypeContract(AdultLine lineType, string lineTypeDesc,
            string roomPensionDesc, string roomBreakfastPensionDesc,
            string roomBreakfast1MealPensionDesc, string roomBreakfast2MealsPensionDesc,
            string allInclusivePensionDesc, bool paxsApplicationMode)
            : base()
        {
            _lineType = lineType;
            _lineTypeDesc = lineTypeDesc;
            _roomPensionDesc = roomPensionDesc;
            _roomBreakfastPensionDesc = roomBreakfastPensionDesc;
            _roomBreakfast1MealPensionDesc = roomBreakfast1MealPensionDesc;
            _roomBreakfast2MealsPensionDesc = roomBreakfast2MealsPensionDesc;
            _allInclusivePensionDesc = allInclusivePensionDesc;
            _paxsApplicationMode = paxsApplicationMode;
        }

        #endregion
        #region Methods

        public IEnumerable<RoomPriceContract> GetAdultPricesForPension(short pensionModeId)
        {
            IEnumerable<RoomPriceContract> contracts;
            var prices = Prices.Where(x => x.OrderNumber.HasValue);
            switch (pensionModeId)
            {
                case 1:
                    contracts = prices.Select(x => x.RoomPensionPrice);
                    break;
                case 2:
                    contracts = prices.Select(x => x.RoomBreakfastPensionPrice);
                    break;
                case 3:
                    contracts = prices.Select(x => x.RoomBreakfast1MealPensionPrice);
                    break;
                case 4:
                    contracts = prices.Select(x => x.RoomBreakfast2MealsPensionPrice);
                    break;
                case 5:
                    contracts = prices.Select(x => x.AllInclusivePensionPrice);
                    break;
                default:
                    contracts = prices.Select(x => x.AnyPensionPrice);
                    break;
            }

            return contracts;
        }

        #endregion
    }

    // Prices for room
    [DataContract]
	[Serializable]
    public class RoomAdultPriceByLineTypeContract : AdultPriceByLineTypeContract
    {
        #region Constructors

        public RoomAdultPriceByLineTypeContract()
            : base() { }

        public RoomAdultPriceByLineTypeContract(AdultLine lineType, string lineTypeDesc,
            string roomPensionDesc, string roomBreakfastPensionDesc,
            string roomBreakfast1MealPensionDesc, string roomBreakfast2MealsPensionDesc,
            string allInclusivePensionDesc, short minPersons, short maxPersons)
            : base(lineType, lineTypeDesc, roomPensionDesc, roomBreakfastPensionDesc,
            roomBreakfast1MealPensionDesc, roomBreakfast2MealsPensionDesc, allInclusivePensionDesc,
            false)
        {
            var prices = new TypedList<AdultPriceContract>();
            if (!lineType.Equals(AdultLine.SingleSupplement))
            {
                if (lineType.Equals(AdultLine.Adult))
                {
                    for (short i = minPersons; i <= maxPersons; i++)
                        prices.Add(lineType.Equals(AdultLine.Adult)
                            ? new AdultPriceContract(lineType, i)
                            : new DiscountAdultPriceContract(lineType, i));
                }
                else
                {
                    prices.Add(new DiscountAdultPriceContract(lineType));
                    prices.Add(new DiscountAdultPriceContract(lineType, 1));
                    prices.Add(new DiscountAdultPriceContract(lineType, 2));
                    prices.Add(new DiscountAdultPriceContract(lineType, BehondMaxOrder));
                }
            }

            _prices = new TypedList<AdultPriceContract>(prices.ToArray());
        }

        #endregion
    }

    // Prices for pax
    [DataContract]
	[Serializable]
    public class PaxsAdultPriceByLineTypeContract : AdultPriceByLineTypeContract
    {
        #region Members

        [DataMember]
        internal short _maxOrder;

        #endregion
        #region Properties

        public short MaxOrder { get { return _maxOrder; } }

        #endregion
        #region Constructors

        public PaxsAdultPriceByLineTypeContract()
            : base() { }

        public PaxsAdultPriceByLineTypeContract(AdultLine lineType, string lineTypeDesc,
            string roomPensionDesc, string roomBreakfastPensionDesc,
            string roomBreakfast1MealPensionDesc, string roomBreakfast2MealsPensionDesc,
            string allInclusivePensionDesc, short maxOrder)
            : base(lineType, lineTypeDesc, roomPensionDesc, roomBreakfastPensionDesc,
            roomBreakfast1MealPensionDesc, roomBreakfast2MealsPensionDesc, allInclusivePensionDesc,
            true)
        {
            _maxOrder = maxOrder;
            var prices = new TypedList<AdultPriceContract>();
            if (lineType.Equals(AdultLine.SingleSupplement))
                prices.Add(new DiscountAdultPriceContract(lineType));
            else
            {
                if (lineType.Equals(AdultLine.Adult))
                {
                    prices.Add(new AdultPriceContract(lineType));
                    for (short i = 1; i <= maxOrder; i++)
                        prices.Add(new AdultPriceContract(lineType, i));
                }
                else
                {
                    prices.Add(new DiscountAdultPriceContract(lineType));
                    prices.Add(new DiscountAdultPriceContract(lineType, 1));
                    prices.Add(new DiscountAdultPriceContract(lineType, 2));
                    prices.Add(new DiscountAdultPriceContract(lineType, BehondMaxOrder));
                }
            }

            _prices = new TypedList<AdultPriceContract>(prices.ToArray());
        }

        #endregion
    }
}
