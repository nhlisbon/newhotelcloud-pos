﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class NewRoomTypePriceContract : BaseContract
    {
        #region Members

        private Guid _priceRateId;
        private string _priceRateDesc;
        [DataMember]
        internal string _currency;
        [DataMember]
        internal bool _taxIncluded;
        [DataMember]
        internal string _comments;
        private Guid? _roomTypeId;
        private PriceCalculationType _applicationMode;
        private DateTime? _fromDate;
        private DateTime? _toDate;
        private bool _includeSunday;
        private bool _includeMonday;
        private bool _includeTuesday;
        private bool _includeWednesday;
        private bool _includeThursday;
        private bool _includeFriday;
        private bool _includeSaturday;
        private decimal? _fixedExchangeRate;
        [DataMember]
        internal bool _allowPriceForZeroPaxs;
        [DataMember]
        internal short _pensionModeId;
        [DataMember]
        internal short? _persons;

        #endregion
        #region Properties

        [DataMember]
        public Guid PriceRateId { get { return _priceRateId; } set { Set(ref _priceRateId, value, "PriceRateId"); } }
        [DataMember]
        public string PriceRateDesc { get { return _priceRateDesc; } set { Set(ref _priceRateDesc, value, "PriceRateDesc"); } }
        [DataMember]
        [Required("RoomTypeId")]
        public Guid? RoomTypeId { get { return _roomTypeId; } set { Set(ref _roomTypeId, value, "RoomTypeId"); } }
        [Required("PensionModeId")]
        public short PensionModeId { get { return _pensionModeId; } set { Set(ref _pensionModeId, value, "PensionModeId"); } }
        public short? Persons { get { return _persons; } set { Set(ref _persons, value, "Persons"); } }
        [DataMember]
        public PriceCalculationType ApplicationMode { get { return _applicationMode; } set { Set(ref _applicationMode, value, "ApplicationMode"); } }
        [DataMember]
        [Required("FromDate")]
        public DateTime? FromDate { get { return _fromDate; } set { Set(ref _fromDate, value, "FromDate"); } }
        [DataMember]
        [Required("ToDate")]
        public DateTime? ToDate { get { return _toDate; } set { Set(ref _toDate, value, "ToDate"); } }
        public string Currency { get { return _currency; } }
        public bool TaxIncluded { get { return _taxIncluded; } }
        public string Comments { get { return _comments; } }
        [DataMember]
        public bool IncludeSunday { get { return _includeSunday; } set { Set(ref _includeSunday, value, "IncludeSunday"); } }
        [DataMember]
        public bool IncludeMonday { get { return _includeMonday; } set { Set(ref _includeMonday, value, "IncludeMonday"); } }
        [DataMember]
        public bool IncludeTuesday { get { return _includeTuesday; } set { Set(ref _includeTuesday, value, "IncludeTuesday"); } }
        [DataMember]
        public bool IncludeWednesday { get { return _includeWednesday; } set { Set(ref _includeWednesday, value, "IncludeWednesday"); } }
        [DataMember]
        public bool IncludeThursday { get { return _includeThursday; } set { Set(ref _includeThursday, value, "IncludeThursday"); } }
        [DataMember]
        public bool IncludeFriday { get { return _includeFriday; } set { Set(ref _includeFriday, value, "IncludeFriday"); } }
        [DataMember]
        public bool IncludeSaturday { get { return _includeSaturday; } set { Set(ref _includeSaturday, value, "IncludeSaturday"); } }
        [DataMember]
        public short? UndoLevel { get; set; }
        [DataMember]
        public decimal? FixedExchangeRate { get { return _fixedExchangeRate; } set { Set(ref _fixedExchangeRate, value, "FixedExchangeRate"); } }

        #endregion
        #region Parameters

        [ReflectionExclude]
        public bool AllowPriceForZeroPaxs 
        { 
            get { return _allowPriceForZeroPaxs; } 
        }

        #endregion
        #region Constructors

        public NewRoomTypePriceContract(Guid priceRateId, string priceRateDesc,
            string currency, bool taxIncluded, string comments, bool allowPriceForZeroPaxs,
            short pensionModeId, short? persons)
            : base()
        {
            _priceRateId = priceRateId;
            _priceRateDesc = priceRateDesc;
            _currency = currency;
            _taxIncluded = taxIncluded;
            _comments = comments;
            ApplicationMode = PriceCalculationType.Room;
            IncludeSunday = true;
            IncludeMonday = true;
            IncludeTuesday = true;
            IncludeWednesday = true;
            IncludeThursday = true;
            IncludeFriday = true;
            IncludeSaturday = true;
            _taxIncluded = true;
            _allowPriceForZeroPaxs = allowPriceForZeroPaxs;
            _pensionModeId = pensionModeId;
            _persons = persons;
        }

        public NewRoomTypePriceContract(Guid priceRateId, string priceRateDesc,
            string currency, bool taxIncluded, string comments, bool allowPriceForZeroPaxs,
            short pensionModeId)
            : this(priceRateId, priceRateDesc, currency, taxIncluded, comments, allowPriceForZeroPaxs,
            pensionModeId, null)
        {
        }

        #endregion
    }
}
