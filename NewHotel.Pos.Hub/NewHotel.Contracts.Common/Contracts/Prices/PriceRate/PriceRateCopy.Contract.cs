﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(PriceRateContract), "ValidatePriceRateCopy")]
    public class PriceRateCopyContract : BaseContract
    {
        #region Private Members
        private Guid _originalPriceRateId;
        private string _abbreviationRecord;
        private string _descriptionRecord;
        private string _priceTypeRecord;
        private string _currencyRecord;
        private DateTime _initialDate;
        private DateTime _finalDate;
        private bool _onlyRoomModifications;
        private decimal _value;
        private int _valueSign;
        private int _valueType;

        #endregion
        #region Constructor
        public PriceRateCopyContract()
        {
            ValueSignItems = new List<KeyDescRecord>();
            ValueTypeItems = new List<KeyDescRecord>();
        }
        #endregion
        #region Public Properties
        [DataMember]
        public Guid OriginalPriceRateId { get { return _originalPriceRateId; } set { Set(ref _originalPriceRateId, value, "OriginalPriceRateId"); } }
        [DataMember]
        public string AbbreviationRecord { get { return _abbreviationRecord; } set { Set(ref _abbreviationRecord, value, "AbbreviationRecord"); } }
        [DataMember]
        public string DescriptionRecord { get { return _descriptionRecord; } set { Set(ref _descriptionRecord, value, "DescriptionRecord"); } }
        [DataMember]
        public string PriceTypeRecord { get { return _priceTypeRecord; } set { Set(ref _priceTypeRecord, value, "PriceTypeRecord"); } }
        [DataMember]
        public string CurrencyRecord { get { return _currencyRecord; } set { Set(ref _currencyRecord, value, "CurrencyRecord"); } }
        [DataMember]
        public DateTime InitialDate { get { return _initialDate; } set { Set(ref _initialDate, value, "InitialDate"); } }
        [DataMember]
        public DateTime FinalDate { get { return _finalDate; } set { Set(ref _finalDate, value, "FinalDate"); } }
        [DataMember]
        public bool OnlyRoomModifications { get { return _onlyRoomModifications; } set { Set(ref _onlyRoomModifications, value, "OnlyRoomModifications"); } }
        [DataMember]
        public int ValueSign { get { return _valueSign; } set { Set(ref _valueSign, value, "ValueSign"); } }
        [DataMember]
        public int ValueType { get { return _valueType; } set { Set(ref _valueType, value, "ValueType"); } }
        [DataMember]
        public decimal Value { get { return _value; } set { Set(ref _value, value, "Value"); } }

        [DataMember]
        public PriceRateContract PriceRate { get; set; }
        [DataMember]
        public List<KeyDescRecord> ValueSignItems { get; set; }
        [DataMember]
        public List<KeyDescRecord> ValueTypeItems { get; set; }
        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidatePriceRateCopy(PriceRateCopyContract obj)
        {
            if (obj.InitialDate > obj.FinalDate) return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid Dates.");
            if (obj.PriceRate.Description.IsEmpty) return new System.ComponentModel.DataAnnotations.ValidationResult("Missing new PriceRate Description.");
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
