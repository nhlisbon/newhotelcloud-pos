﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class AllocationRoomTypeGroupContract : BaseContract
    {
        #region Members

        [DataMember]
        internal Guid _roomTypeId;
        [DataMember]
        internal string _description;

        private bool _allocationSupplementInValue;
        private decimal? _allocationSupplementValue;
        private bool _percentValueOverRoom;
        private bool _excludeExtraBeds;
        private bool _excludeChildren;
        private bool _isEditable;

        #endregion
        #region Properties
        
        [DataMember(Order = 0)]
        public bool AllocationSupplementInValue
        {
            get { return _allocationSupplementInValue; }
            set
            {
                if (Set(ref _allocationSupplementInValue, value, "AllocationSupplementInValue"))
                {
                    AllocationSupplementValue = null;
                    NotifyPropertyChanged("MinAllocationSupplementValue", "MaxAllocationSupplementValue", "ShowPercentValueOverRoom");
                }
            }
        }
        [DataMember(Order = 1)]
        public decimal? AllocationSupplementValue 
        { 
            get { return _allocationSupplementValue; }
            set
            {
                if (Set(ref _allocationSupplementValue, value, "AllocationSupplementValue"))
                    NotifyPropertyChanged("NullTranslationText");
            }
        }
        [DataMember]
        public bool PercentValueOverRoom
        {
            get { return _percentValueOverRoom; }
            set { Set(ref _percentValueOverRoom, value, "PercentValueOverRoom"); }
        }
        [DataMember]
        public bool ExcludeExtraBeds 
        {
            get { return _excludeExtraBeds; }
            set { Set(ref _excludeExtraBeds, value, "ExcludeExtraBeds"); }
        }
        [DataMember]
        public bool ExcludeChildren
        {
            get { return _excludeChildren; }
            set { Set(ref _excludeChildren, value, "ExcludeChildren"); }
        }
        [DataMember]
        public bool IsEditable
        {
            get { return _isEditable; }
            set
            {
                if (Set(ref _isEditable, value, "IsEditable"))
                    NotifyPropertyChanged("NullTranslationText");
            }
        }

        public Guid RoomTypeId
        {
            get { return _roomTypeId; }
        }

        public string Description
        {
            get { return _description; }
        }

        [ReflectionExclude]
        public bool ShowPercentValueOverRoom
        {
            get { return !_allocationSupplementInValue; }
        }

        [ReflectionExclude]
        public string NullTranslationText
        {
            get { return !_isEditable ? "BaseRoomType" : (!_allocationSupplementValue.HasValue ? "DoNotCompute" : string.Empty); }
        }

        [ReflectionExclude]
        public decimal? MinAllocationSupplementValue
        {
            get
            {
                if (_allocationSupplementInValue)
                    return null;

                return -100M;
            }
        }

        [ReflectionExclude]
        public decimal? MaxAllocationSupplementValue
        {
            get
            {
                if (_allocationSupplementInValue)
                    return null;

                return 100M;
            }
        }

        #endregion
        #region Constructors

        public AllocationRoomTypeGroupContract(Guid roomTypeId, string description)
            : base() 
        {
            _roomTypeId = roomTypeId;
            _description = description;
        }

        #endregion
    }
}