﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class EarlyBookingContract : BaseContract
    {
        #region Members

        private DateTime _initialApplicationDate;
        private DateTime _finalApplicationDate;

        [DataMember]
        internal DateTime _workDate;

        #endregion
        #region Properties

        [DataMember]
        public DateTime InitialApplicationDate { get { return _initialApplicationDate; } set { Set(ref _initialApplicationDate, value, "InitialApplicationDate"); } }
        [DataMember]
        public DateTime FinalApplicationDate { get { return _finalApplicationDate; } set { Set(ref _finalApplicationDate, value, "FinalApplicationDate"); } }

        public DateTime WorkDate { get { return _workDate; } }

        #endregion
        #region Contracts

        [ReflectionExclude]
        public TypedList<EarlyBookingDaysInAdvanceContract> DaysInAdvance { get; set; }

        [ReflectionExclude]
        public TypedList<EarlyBookingDateContract> ByDates { get; set; }

        #endregion
        #region Constructors

        public EarlyBookingContract(DateTime initialAppDate, DateTime finalAppDate, DateTime workDate)
            : base() 
        {
            _initialApplicationDate = initialAppDate;
            _finalApplicationDate = finalAppDate;
            _workDate = workDate;
            DaysInAdvance = new TypedList<EarlyBookingDaysInAdvanceContract>();
            ByDates = new TypedList<EarlyBookingDateContract>(); 
        }

        #endregion
    }
}