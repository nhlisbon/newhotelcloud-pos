﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(EarlyBookingDaysInAdvanceContract), "ValidateEarlyBookingDaysInAdvance")]
    public class EarlyBookingDaysInAdvanceContract : BaseEarlyBookingContract
    {
        #region Properties

        [NewHotel.Contracts.Required("InitialDayNumber")]
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Initial day number required.")]
        public short? InitialDayNumber { get; set; }
        [DataMember]
        public short? FinalDayNumber { get; set; }

        #endregion
        #region Constructors

        public EarlyBookingDaysInAdvanceContract(EarlyBookingContract earlyBookingContract)
            : base(earlyBookingContract) { }

        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateEarlyBookingDaysInAdvance(EarlyBookingDaysInAdvanceContract obj)
        {
            if (obj.InitialDayNumber.HasValue && obj.InitialDayNumber.Value <= 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Initial day must be greater than 0.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}