﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public interface IPriceRatePrices
    {
        string Description { get; }
        PriceCalculationType ApplicationMode { get; }
        bool EditableAdultPrice { get; }
        TypedList<AdultPriceByLineTypeContract> AdultPrices { get; }
        bool EditableChildPrice { get; }
        TypedList<ChildPriceByLineTypeContract> ChildPrices { get; }
        bool EditableFoodPrice { get; }
    }

    [DataContract]
	[Serializable]
    public class RoomTypePriceContract : BaseContract, IPriceRatePrices
	{
		#region Constants

		public const short RoomOnly = 1;
		public const short BedBreakfast = 2;
		public const short HalfBoard = 3;
		public const short FullBoard = 4;
		public const short AllInclusive = 5;

		#endregion
		#region Members

		private bool _recalculating = false;

        [DataMember]
        internal string _description;
        [DataMember]
        internal Guid _priceRateId;
        [DataMember]
        internal Guid _roomTypeId;
        [DataMember]
        internal string _currency;
        [DataMember]
        internal bool _taxIncluded;
        [DataMember]
        internal DateTime _fromDate;
        [DataMember]
        internal DateTime _toDate;
        [DataMember]
        internal bool _includeSunday;
        [DataMember]
        internal bool _includeMonday;
        [DataMember]
        internal bool _includeTuesday;
        [DataMember]
        internal bool _includeWednesday;
        [DataMember]
        internal bool _includeThursday;
        [DataMember]
        internal bool _includeFriday;
        [DataMember]
        internal bool _includeSaturday;
        [DataMember]
        internal short _pensionModeId;
        [DataMember]
        internal string _pensionModeDesc;
        [DataMember]
        internal string _roomTypeDesc;
        [DataMember]
        internal short _minPersons;
        [DataMember]
        internal short _maxPersons;
        [DataMember]
        internal short _basePersons;
        [DataMember]
        internal short _persons;
        [DataMember]
        internal PriceCalculationType _applicationMode;
        [DataMember]
        internal string _applicationModeDesc;
        [DataMember]
        internal decimal? _fixedExchangeRate;
        [DataMember]
        internal bool _excludeExtraBeds;
        [DataMember]
        internal bool _excludeChildren;
        [DataMember]
        internal HalfBoardEatMode _halfBoardEatMode;
        [DataMember]
        internal IDictionary<short, string> _pensionModes;

        [DataMember]
        internal bool[] _defaultBreakfastAdultsPriceInPercent;
		[DataMember]
		internal decimal?[] _defaultFoodBreakfastAdultsPrices;
        [DataMember]
        internal decimal?[] _defaultDrinkBreakfastAdultsPrices;

        [DataMember]
        internal bool[] _defaultLunchAdultsPriceInPercent;
		[DataMember]
		internal decimal?[] _defaultFoodLunchAdultsPrices;
        [DataMember]
        internal decimal?[] _defaultDrinkLunchAdultsPrices;

        [DataMember]
        internal bool[] _defaultDinnerAdultsPriceInPercent;
		[DataMember]
		internal decimal?[] _defaultFoodDinnerAdultsPrices;
        [DataMember]
        internal decimal?[] _defaultDrinkDinnerAdultsPrices;

        [DataMember]
        internal bool _defaultAllInclusiveAdultsPriceInPercent;
        [DataMember]
        internal decimal? _defaultAllInclusiveAdultsPrice;

        [DataMember]
        internal bool[] _defaultBreakfastChildrenPriceInPercent;
		[DataMember]
		internal decimal?[] _defaultFoodBreakfastChildrenPrices;
        [DataMember]
        internal decimal?[] _defaultDrinkBreakfastChildrenPrices;

        [DataMember]
        internal bool[] _defaultLunchChildrenPriceInPercent;
        [DataMember]
		internal decimal?[] _defaultFoodLunchChildrenPrices;
        [DataMember]
        internal decimal?[] _defaultDrinkLunchChildrenPrices;

        [DataMember]
        internal bool[] _defaultDinnerChildrenPriceInPercent;
        [DataMember]
        internal decimal?[] _defaultFoodDinnerChildrenPrices;
		[DataMember]
		internal decimal?[] _defaultDrinkDinnerChildrenPrices;

        [DataMember]
        internal bool _defaultAllInclusiveChildrenPriceInPercent;
        [DataMember]
        internal decimal? _defaultAllInclusiveChildrenPrice;

        [DataMember]
        internal AdultLine[] _adultAutocalculateLines;
        [DataMember]
        internal ChildLine[] _childAutocalculateLines;

        [DataMember]
        internal bool _editableAdultPrice;
        [DataMember]
        internal bool _editableChildPrice;
        [DataMember]
        internal bool _editableFoodPrice;

		private TypedList<AdultPriceByLineTypeContract> _adultPrices;
        private TypedList<ChildPriceByLineTypeContract> _childPrices;
		private TypedList<FoodPriceByLineTypeContract> _foodPrices;
        private TypedList<DerivedPriceRateContract> _derivedPriceRates;

        [DataMember(Order = 0)]
        internal TypedList<AdultPriceByLineTypeContract> adultPrices
        {
            get { return _adultPrices; }
			set
			{
				_adultPrices = value;
				foreach (var adultPrice in _adultPrices)
					foreach (var price in adultPrice.Prices)
						price.PriceChanged += new AdultPriceChangedEventHandler(AdultPriceChanged);
			}
        }

        [DataMember(Order = 0)]
        internal TypedList<ChildPriceByLineTypeContract> childPrices
        {
            get { return _childPrices; }
            set
            {
                _childPrices = value;
                foreach (var childPrice in _childPrices)
                {
                    for (short pensionModeId = 1; pensionModeId <= 5; pensionModeId++)
                    {
                        var prices = GetRoomChildPrices(childPrice, pensionModeId);
                        foreach (var price in prices)
                            price.PriceChanged += new ChildPriceChangedEventHandler(ChildPriceChanged);
                    }
                }
            }
        }

        [DataMember(Order = 0)]
        internal TypedList<FoodPriceByLineTypeContract> foodPrices
		{
			get { return _foodPrices; }
			set
			{
				_foodPrices = value;
				foreach (var foodPrice in _foodPrices)
					foreach (var price in foodPrice.Prices)
						price.PriceChanged += new PriceChangedEventHandler(FoodPriceChanged);
			}
		}

        [DataMember(Order = 1)]
        public TypedList<DerivedPriceRateContract> DerivedPriceRates
        {
            get { return _derivedPriceRates; }
            set { _derivedPriceRates = value; }
        }

        #endregion
        #region Properties

        public string Description { get { return _description; } }
        public Guid PriceRateId { get { return _priceRateId; } }
        public Guid RoomTypeId { get { return _roomTypeId; } }
        public short PensionModeId { get { return _pensionModeId; } }
        public string PensionModeDesc { get { return _pensionModeDesc; } }
        public string RoomTypeDesc { get { return _roomTypeDesc; } }
        public short MinPersons { get { return _minPersons; } }
        public short MaxPersons { get { return _maxPersons; } }
        public short BasePersons { get { return _basePersons; } }
        public short Persons { get { return _persons; } }
        public PriceCalculationType ApplicationMode { get { return _applicationMode; } }
        public string ApplicationModeDesc { get { return _applicationModeDesc; } }
        public string Currency { get { return _currency; } }
        public DateTime FromDate { get { return _fromDate; } }
        public DateTime ToDate { get { return _toDate; } }
        public bool TaxIncluded { get { return _taxIncluded; } }
        public bool IncludeSunday { get { return _includeSunday; } }
        public bool IncludeMonday { get { return _includeMonday; } }
        public bool IncludeTuesday { get { return _includeTuesday; } }
        public bool IncludeWednesday { get { return _includeWednesday; } }
        public bool IncludeThursday { get { return _includeThursday; } }
        public bool IncludeFriday { get { return _includeFriday; } }
        public bool IncludeSaturday { get { return _includeSaturday; } }
        public IDictionary<short, string> PensionModes { get { return _pensionModes; } }

        [DataMember]
        public short PriceType { get; set; }
        [DataMember]
        public decimal? FixedExchangeRate { get { return _fixedExchangeRate; } set { Set<decimal?>(ref _fixedExchangeRate, value, "FixedExchangeRate"); } }

        [ReflectionExclude]
        public TypedList<AdultPriceByLineTypeContract> AdultPrices { get { return _adultPrices; } }

        [ReflectionExclude]
        public TypedList<ChildPriceByLineTypeContract> ChildPrices { get { return _childPrices; } }

        [ReflectionExclude]
        public TypedList<FoodPriceByLineTypeContract> FoodPrices { get { return _foodPrices; } }

        [ReflectionExclude]
        public short? UndoLevel { get; private set; }

        [ReflectionExclude]
        public bool BreakfastAdultsInPercent
        {
            get { return _defaultBreakfastAdultsPriceInPercent[_pensionModeId - 1]; }
        }

        [ReflectionExclude]
        public decimal? FoodBreakfastAdultsPrice
        {
			get { return _defaultFoodBreakfastAdultsPrices[_pensionModeId - 1]; }
        }

        [ReflectionExclude]
        public decimal? DrinkBreakfastAdultsPrice
        {
			get { return _defaultDrinkBreakfastAdultsPrices[_pensionModeId - 1]; }
        }

        [ReflectionExclude]
        public decimal? BreakfastAdultsPrice
        {
            get
			{
				if (!FoodBreakfastAdultsPrice.HasValue && !DrinkBreakfastAdultsPrice.HasValue)
					return null;

				return (FoodBreakfastAdultsPrice ?? decimal.Zero) + (DrinkBreakfastAdultsPrice ?? decimal.Zero);
			}
        }

        [ReflectionExclude]
        public decimal? FoodBreakfastChildrenPrice
        {
            get { return _defaultFoodBreakfastChildrenPrices[_pensionModeId - 1]; }
        }

        [ReflectionExclude]
        public decimal? DrinkBreakfastChildrenPrice
        {
            get { return _defaultDrinkBreakfastChildrenPrices[_pensionModeId - 1]; }
        }

        [ReflectionExclude]
        public bool BreakfastChildrenInPercent
        {
            get { return _defaultBreakfastChildrenPriceInPercent[_pensionModeId - 1]; }
        }

        [ReflectionExclude]
        public decimal? BreakfastChildrenPrice
        {
			get
			{
				if (!FoodBreakfastChildrenPrice.HasValue && !DrinkBreakfastChildrenPrice.HasValue)
                    return null;

				return (FoodBreakfastChildrenPrice ?? decimal.Zero) + (DrinkBreakfastChildrenPrice ?? decimal.Zero);
			}
        }

        [ReflectionExclude]
        public bool LunchAdultsInPercent
        {
            get { return _defaultLunchAdultsPriceInPercent[_pensionModeId - 1]; }
        }

        [ReflectionExclude]
        public decimal? FoodLunchAdultsPrice
        {
            get { return _defaultFoodLunchAdultsPrices[_pensionModeId - 1]; }
        }

        [ReflectionExclude]
        public decimal? DrinkLunchAdultsPrice
        {
            get { return _defaultDrinkLunchAdultsPrices[_pensionModeId - 1]; }
        }

        [ReflectionExclude]
        public decimal? LunchAdultsPrice
        {
            get
			{
				if (!FoodLunchAdultsPrice.HasValue && !DrinkLunchAdultsPrice.HasValue)
					return null;

				return (FoodLunchAdultsPrice ?? decimal.Zero) + (DrinkLunchAdultsPrice ?? decimal.Zero);
			}
        }

        [ReflectionExclude]
        public bool LunchChildrenInPercent
        {
            get { return _defaultLunchChildrenPriceInPercent[_pensionModeId - 1]; }
        }

        [ReflectionExclude]
        public decimal? FoodLunchChildrenPrice
        {
            get { return _defaultFoodLunchChildrenPrices[_pensionModeId - 1]; }
        }

        [ReflectionExclude]
        public decimal? DrinkLunchChildrenPrice
        {
            get { return _defaultDrinkLunchChildrenPrices[_pensionModeId - 1]; }
        }

        [ReflectionExclude]
        public decimal? LunchChildrenPrice
        {
			get
			{
				if (!FoodLunchChildrenPrice.HasValue && !DrinkLunchChildrenPrice.HasValue)
					return null;

				return (FoodLunchChildrenPrice ?? decimal.Zero) + (DrinkLunchChildrenPrice ?? decimal.Zero);
			}
        }

        [ReflectionExclude]
        public bool DinnerAdultsInPercent
        {
            get { return _defaultDinnerAdultsPriceInPercent[_pensionModeId - 1]; }
        }

        [ReflectionExclude]
        public decimal? FoodDinnerAdultsPrice
        {
            get { return _defaultFoodDinnerAdultsPrices[_pensionModeId - 1]; }
        }

        [ReflectionExclude]
        public decimal? DrinkDinnerAdultsPrice
        {
            get { return _defaultDrinkDinnerAdultsPrices[_pensionModeId - 1]; }
        }

        [ReflectionExclude]
        public decimal? DinnerAdultsPrice
        {
            get
			{
				if (!FoodDinnerAdultsPrice.HasValue && !DrinkDinnerAdultsPrice.HasValue)
					return null;

				return (FoodDinnerAdultsPrice ?? decimal.Zero) + (DrinkDinnerAdultsPrice ?? decimal.Zero);
			}
        }

        [ReflectionExclude]
        public bool DinnerChildrenInPercent
        {
            get { return _defaultDinnerChildrenPriceInPercent[_pensionModeId - 1]; }
        }

        [ReflectionExclude]
        public decimal? FoodDinnerChildrenPrice
        {
            get { return _defaultFoodDinnerChildrenPrices[_pensionModeId - 1]; }
        }

        [ReflectionExclude]
        public decimal? DrinkDinnerChildrenPrice
        {
            get { return _defaultDrinkDinnerChildrenPrices[_pensionModeId - 1]; }
        }

        [ReflectionExclude]
        public decimal? DinnerChildrenPrice
        {
			get
			{
				if (!FoodDinnerChildrenPrice.HasValue && !DrinkDinnerChildrenPrice.HasValue)
					return null;

				return (FoodDinnerChildrenPrice ?? decimal.Zero) + (DrinkDinnerChildrenPrice ?? decimal.Zero);
			}
        }

        [ReflectionExclude]
        public bool AllInclusiveAdultsInPercent
        {
            get { return _defaultAllInclusiveAdultsPriceInPercent; }
        }

        [ReflectionExclude]
        public decimal? AllInclusiveAdultsPrice
        {
            get
            {
                if (_pensionModeId == AllInclusive)
                    return _defaultAllInclusiveAdultsPrice;

                return null;
            }
        }

        [ReflectionExclude]
        public bool AllInclusiveChildrenInPercent
        {
            get { return _defaultAllInclusiveChildrenPriceInPercent; }
        }

        [ReflectionExclude]
        public decimal? AllInclusiveChildrenPrice
        {
            get
            {
                if (_pensionModeId == AllInclusive)
                    return _defaultAllInclusiveChildrenPrice;

                return null;
            }
        }

        [ReflectionExclude]
        public bool EditableAdultPrice
        {
            get { return _editableAdultPrice; }
        }

        [ReflectionExclude]
        public bool EditableChildPrice
        {
            get { return _editableChildPrice; }
        }

        [ReflectionExclude]
        public bool EditableFoodPrice
        {
            get { return _editableFoodPrice; }
        }

        [ReflectionExclude]
        private IEnumerable<AdultPriceByLineTypeContract> AutoCalcAdultPrices
        {
            get { return _adultPrices.Where(x => x.LineType.Equals(AdultLine.Adult) || x.LineType.Equals(AdultLine.ExtraAdult)); }
        }

        [ReflectionExclude]
        private IEnumerable<ChildPriceByLineTypeContract> AutoCalcChildPrices
        {
            get { return _childPrices.Where(x => x.LineType.Equals(ChildLine.Child)); }
        }

        [ReflectionExclude]
        public bool AutoCalcRoomPension
        {
            get
            {
                var autoCalcRoomPension = AutoCalcAdultPrices.Select(x => x.AutoCalcRoomPension).Distinct();
                return autoCalcRoomPension.Count() == 1 ? autoCalcRoomPension.First() : false;
            }
            set
            {
                foreach (var adultPrice in AutoCalcAdultPrices)
                    adultPrice.AutoCalcRoomPension = value;
            }
        }

        [ReflectionExclude]
        public bool AutoCalcRoomBreakfastPension
        {
            get
            {
                var autoCalcRoomBreakfastPension = AutoCalcAdultPrices.Select(x => x.AutoCalcRoomBreakfastPension).Distinct();
                return autoCalcRoomBreakfastPension.Count() == 1 ? autoCalcRoomBreakfastPension.First() : false;
            }
            set
            {
                foreach (var adultPrice in AutoCalcAdultPrices)
                    adultPrice.AutoCalcRoomBreakfastPension = value;
            }
        }

        [ReflectionExclude]
        public bool AutoCalcRoomBreakfast1MealPension
        {
            get
            {
                var autoCalcRoomBreakfast1MealPension = AutoCalcAdultPrices.Select(x => x.AutoCalcRoomBreakfast1MealPension).Distinct();
                return autoCalcRoomBreakfast1MealPension.Count() == 1 ? autoCalcRoomBreakfast1MealPension.First() : false;
            }
            set
            {
                foreach (var adultPrice in AutoCalcAdultPrices)
                    adultPrice.AutoCalcRoomBreakfast1MealPension = value;
            }
        }

        [ReflectionExclude]
        public bool AutoCalcRoomBreakfast2MealPension
        {
            get
            {
                var autoCalcRoomBreakfast2MealPension = AutoCalcAdultPrices.Select(x => x.AutoCalcRoomBreakfast2MealPension).Distinct();
                return autoCalcRoomBreakfast2MealPension.Count() == 1 ? autoCalcRoomBreakfast2MealPension.First() : false;
            }
            set
            {
                foreach (var adultPrice in AutoCalcAdultPrices)
                    adultPrice.AutoCalcRoomBreakfast2MealPension = value;
            }
        }

        [ReflectionExclude]
        public bool AutoCalcAllInclusivePension
        {
            get
            {
                var autoCalcAllInclusivePension = AutoCalcAdultPrices.Select(x => x.AutoCalcAllInclusivePension).Distinct();
                return autoCalcAllInclusivePension.Count() == 1 ? autoCalcAllInclusivePension.First() : false;
            }
            set
            {
                foreach (var adultPrice in AutoCalcAdultPrices)
                    adultPrice.AutoCalcAllInclusivePension = value;
            }
        }

        #endregion
        #region Private Methods

        private static IEnumerable<RoomChildPriceContract> GetRoomChildPrices(ChildPriceByLineTypeContract childPrice, short pensionModeId)
        {
            switch (pensionModeId)
            {
                case RoomOnly: return childPrice.RoomPrices.Cast<RoomChildPriceContract>();
                case BedBreakfast: return childPrice.RoomBreakfastPrices.Cast<RoomChildPriceContract>();
                case HalfBoard: return childPrice.RoomBreakfast1MealPrices.Cast<RoomChildPriceContract>();
                case FullBoard: return childPrice.RoomBreakfast2MealsPrices.Cast<RoomChildPriceContract>();
                case AllInclusive: return childPrice.AllInclusivePrices.Cast<RoomChildPriceContract>();
            }

            return null;
        }

        private static void RecalculateChildRoomPrices(ChildPriceByLineTypeContract childPrice,
            short pensionModeId, decimal? roomPrice, decimal mealsPrice)
        {
            var prices = GetRoomChildPrices(childPrice, pensionModeId);
            foreach (var price in prices)
            {
                if (price.CalculateRoomPrice)
                    price.SetRoomPrice((roomPrice ?? decimal.Zero) + mealsPrice);
            }
        }

		// Only Room
		private void RecalculateRoomPrices(short orderNumber)
		{
            var foodPrice = GetFoodPriceLine(FoodLine.Adult, orderNumber);
            foreach (var adultPrice in AutoCalcAdultPrices)
            {
                var lineType = (FoodLine)adultPrice.LineType;
                foreach (var price in adultPrice.Prices.Where(x => x.OrderNumber.Equals(orderNumber)))
				{
                    if (price.RoomPensionPrice.RoomPrice.HasValue)
                    {
                        var roomOnlyPrice = price.RoomPensionPrice.RoomPrice.Value;

                        if (price.RoomBreakfastPensionPrice.CalculateRoomPrice)
                            price.RoomBreakfastPensionPrice.SetRoomPrice(roomOnlyPrice,
                                GetFoodBreakfastPrice(BedBreakfast, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkBreakfastPrice(BedBreakfast, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult));

                        if (price.RoomBreakfast1MealPensionPrice.CalculateRoomPrice)
                            price.RoomBreakfast1MealPensionPrice.SetRoomPrice(roomOnlyPrice,
                                GetFoodBreakfastPrice(HalfBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkBreakfastPrice(HalfBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                Get1MealPrice(HalfBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult));

                        if (price.RoomBreakfast2MealsPensionPrice.CalculateRoomPrice)
                            price.RoomBreakfast2MealsPensionPrice.SetRoomPrice(roomOnlyPrice,
                                GetFoodBreakfastPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkBreakfastPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetFoodLunchPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkLunchPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetFoodDinnerPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkDinnerPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult));

                        if (price.AllInclusivePensionPrice.CalculateRoomPrice)
                            price.AllInclusivePensionPrice.SetRoomPrice(roomOnlyPrice,
                                GetFoodBreakfastPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkBreakfastPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetFoodLunchPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkLunchPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetFoodDinnerPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkDinnerPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetAllInclusiveAdultsPrice(orderNumber, roomOnlyPrice));
                    }
				}
			}

            //foreach (var childPrice in AutoCalcChildPrices)
            //{
            //    var price = GetRoomChildPrice(childPrice, 1);
            //    RecalculateChildRoomPrices(childPrice, 2, price.RoomPrice, breakfastChildPrice);
            //    RecalculateChildRoomPrices(childPrice, 3, price.RoomPrice, breakfastChildPrice + dinnerChildPrice);
            //    RecalculateChildRoomPrices(childPrice, 4, price.RoomPrice, breakfastChildPrice + lunchChildPrice + dinnerChildPrice);
            //    RecalculateChildRoomPrices(childPrice, 5, price.RoomPrice, breakfastChildPrice + lunchChildPrice + dinnerChildPrice + allInclusiveChildPrice);
            //}
		}

		// Bed + Breakfast
        private void RecalculateRoomBreakfastPrices(short orderNumber)
		{
            var foodPrice = GetFoodPriceLine(FoodLine.Adult, orderNumber);
            foreach (var adultPrice in AutoCalcAdultPrices)
			{
				foreach (var price in adultPrice.Prices.Where(x => x.OrderNumber.Equals(orderNumber)))
				{
                    if (price.RoomBreakfastPensionPrice.RoomPrice.HasValue)
                    {
                        var roomOnlyPrice = GetRoomAdultsPrice(BedBreakfast, orderNumber,
                            price.RoomBreakfastPensionPrice.RoomPrice.Value);

                        if (price.RoomPensionPrice.CalculateRoomPrice)
                            price.RoomPensionPrice.SetRoomPrice(roomOnlyPrice, decimal.Zero);

                        if (price.RoomBreakfast1MealPensionPrice.CalculateRoomPrice)
                            price.RoomBreakfast1MealPensionPrice.SetRoomPrice(roomOnlyPrice,
                                GetFoodBreakfastPrice(HalfBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkBreakfastPrice(HalfBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                Get1MealPrice(HalfBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult));

                        if (price.RoomBreakfast2MealsPensionPrice.CalculateRoomPrice)
                            price.RoomBreakfast2MealsPensionPrice.SetRoomPrice(roomOnlyPrice,
                                GetFoodBreakfastPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkBreakfastPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetFoodLunchPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkLunchPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetFoodDinnerPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkDinnerPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult));

                        if (price.AllInclusivePensionPrice.CalculateRoomPrice)
                            price.AllInclusivePensionPrice.SetRoomPrice(roomOnlyPrice,
                                GetFoodBreakfastPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkBreakfastPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetFoodLunchPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkLunchPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetFoodDinnerPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkDinnerPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetAllInclusiveAdultsPrice(orderNumber, roomOnlyPrice));
                    }
				}
			}

            //foreach (var childPrice in AutoCalcChildPrices)
            //{
            //    var price = GetRoomChildPrice(childPrice, 2);
            //    RecalculateChildRoomPrices(childPrice, 1, price.RoomPrice, -breakfastChildPrice);
            //    RecalculateChildRoomPrices(childPrice, 3, price.RoomPrice, dinnerChildPrice);
            //    RecalculateChildRoomPrices(childPrice, 4, price.RoomPrice, lunchChildPrice + dinnerChildPrice);
            //    RecalculateChildRoomPrices(childPrice, 5, price.RoomPrice, lunchChildPrice + dinnerChildPrice + allInclusiveChildPrice);
            //}
		}

		// Bed + Breakfast + 1 Meal
        private void RecalculateRoomBreakfast1MealPrices(short orderNumber)
		{
            var foodPrice = GetFoodPriceLine(FoodLine.Adult, orderNumber);
            foreach (var adultPrice in AutoCalcAdultPrices)
			{
				foreach (var price in adultPrice.Prices.Where(x => x.OrderNumber.Equals(orderNumber)))
				{
                    if (price.RoomBreakfast1MealPensionPrice.RoomPrice.HasValue)
                    {
                        var roomOnlyPrice = GetRoomAdultsPrice(HalfBoard, orderNumber,
                            price.RoomBreakfast1MealPensionPrice.RoomPrice.Value);

                        if (price.RoomPensionPrice.CalculateRoomPrice)
                            price.RoomPensionPrice.SetRoomPrice(roomOnlyPrice, decimal.Zero);

                        if (price.RoomBreakfastPensionPrice.CalculateRoomPrice)
                            price.RoomBreakfastPensionPrice.SetRoomPrice(roomOnlyPrice,
                                GetFoodBreakfastPrice(BedBreakfast, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkBreakfastPrice(BedBreakfast, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult));

                        if (price.RoomBreakfast2MealsPensionPrice.CalculateRoomPrice)
                            price.RoomBreakfast2MealsPensionPrice.SetRoomPrice(roomOnlyPrice,
                                GetFoodBreakfastPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkBreakfastPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetFoodLunchPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkLunchPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetFoodDinnerPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkDinnerPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult));

                        if (price.AllInclusivePensionPrice.CalculateRoomPrice)
                            price.AllInclusivePensionPrice.SetRoomPrice(roomOnlyPrice,
                                GetFoodBreakfastPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkBreakfastPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetFoodLunchPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkLunchPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetFoodDinnerPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkDinnerPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetAllInclusiveAdultsPrice(orderNumber, roomOnlyPrice));
                    }
				}
			}

            //foreach (var childPrice in AutoCalcChildPrices)
            //{
            //    var price = GetRoomChildPrice(childPrice, 3);
            //    RecalculateChildRoomPrices(childPrice, 1, price.RoomPrice, -(breakfastChildPrice + dinnerChildPrice));
            //    RecalculateChildRoomPrices(childPrice, 2, price.RoomPrice, -dinnerChildPrice);
            //    RecalculateChildRoomPrices(childPrice, 4, price.RoomPrice, lunchChildPrice);
            //    RecalculateChildRoomPrices(childPrice, 5, price.RoomPrice, lunchChildPrice + allInclusiveChildPrice);
            //}
		}

		// Bed + Breakfast + 2 Meals
		private void RecalculateRoomBreakfast2MealsPrices(short orderNumber)
		{
            var foodPrice = GetFoodPriceLine(FoodLine.Adult, orderNumber);
            foreach (var adultPrice in AutoCalcAdultPrices)
			{
				foreach (var price in adultPrice.Prices.Where(x => x.OrderNumber.Equals(orderNumber)))
				{
                    if (price.RoomBreakfast2MealsPensionPrice.RoomPrice.HasValue)
                    {
                        var roomOnlyPrice = GetRoomAdultsPrice(FullBoard, orderNumber,
                            price.RoomBreakfast2MealsPensionPrice.RoomPrice.Value);

                        if (price.RoomPensionPrice.CalculateRoomPrice)
                            price.RoomPensionPrice.SetRoomPrice(roomOnlyPrice, decimal.Zero);

                        if (price.RoomBreakfastPensionPrice.CalculateRoomPrice)
                            price.RoomBreakfastPensionPrice.SetRoomPrice(roomOnlyPrice,
                                GetFoodBreakfastPrice(BedBreakfast, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkBreakfastPrice(BedBreakfast, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult));

                        if (price.RoomBreakfast1MealPensionPrice.CalculateRoomPrice)
                            price.RoomBreakfast1MealPensionPrice.SetRoomPrice(roomOnlyPrice,
                                GetFoodBreakfastPrice(HalfBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkBreakfastPrice(HalfBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                Get1MealPrice(HalfBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult));

                        if (price.AllInclusivePensionPrice.CalculateRoomPrice)
                            price.AllInclusivePensionPrice.SetRoomPrice(roomOnlyPrice,
                                GetFoodBreakfastPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkBreakfastPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetFoodLunchPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkLunchPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetFoodDinnerPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkDinnerPrice(AllInclusive, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetAllInclusiveAdultsPrice(orderNumber, roomOnlyPrice));
                    }
				}
			}

            //foreach (var childPrice in AutoCalcChildPrices)
            //{
            //    var price = GetRoomChildPrice(childPrice, 4);
            //    RecalculateChildRoomPrices(childPrice, 1, price.RoomPrice, -(breakfastChildPrice + lunchChildPrice + dinnerChildPrice));
            //    RecalculateChildRoomPrices(childPrice, 2, price.RoomPrice, -(breakfastChildPrice + lunchChildPrice));
            //    RecalculateChildRoomPrices(childPrice, 3, price.RoomPrice, -lunchChildPrice);
            //    RecalculateChildRoomPrices(childPrice, 5, price.RoomPrice, allInclusiveChildPrice);
            //}
		}

		// All Inclusive
		private void RecalculateAllInclusivePrices(short orderNumber)
		{
            var foodPrice = GetFoodPriceLine(FoodLine.Adult, orderNumber);
            foreach (var adultPrice in AutoCalcAdultPrices)
			{
				foreach (var price in adultPrice.Prices.Where(x => x.OrderNumber.Equals(orderNumber)))
				{
                    if (price.AllInclusivePensionPrice.RoomPrice.HasValue)
                    {
                        var roomOnlyPrice = GetRoomAdultsPrice(AllInclusive, orderNumber,
                            price.AllInclusivePensionPrice.RoomPrice.Value);

                        if (price.RoomPensionPrice.CalculateRoomPrice)
                            price.RoomPensionPrice.SetRoomPrice(roomOnlyPrice, decimal.Zero);

                        if (price.RoomBreakfastPensionPrice.CalculateRoomPrice)
                            price.RoomBreakfastPensionPrice.SetRoomPrice(roomOnlyPrice,
                                GetFoodBreakfastPrice(BedBreakfast, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkBreakfastPrice(BedBreakfast, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult));

                        if (price.RoomBreakfast1MealPensionPrice.CalculateRoomPrice)
                            price.RoomBreakfast1MealPensionPrice.SetRoomPrice(roomOnlyPrice,
                                GetFoodBreakfastPrice(HalfBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkBreakfastPrice(HalfBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                Get1MealPrice(HalfBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult));

                        if (price.RoomBreakfast2MealsPensionPrice.CalculateRoomPrice)
                            price.RoomBreakfast2MealsPensionPrice.SetRoomPrice(roomOnlyPrice,
                                GetFoodBreakfastPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkBreakfastPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetFoodLunchPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkLunchPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetFoodDinnerPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult) +
                                GetDrinkDinnerPrice(FullBoard, foodPrice, orderNumber, roomOnlyPrice, FoodLine.Adult));
                    }
				}
			}

            //foreach (var childPrice in AutoCalcChildPrices)
            //{
            //    var price = GetRoomChildPrice(childPrice, 5);
            //    RecalculateChildRoomPrices(childPrice, 1, price.RoomPrice, -(breakfastChildPrice + lunchChildPrice + dinnerChildPrice + allInclusiveChildPrice));
            //    RecalculateChildRoomPrices(childPrice, 2, price.RoomPrice, -(breakfastChildPrice + lunchChildPrice + allInclusiveChildPrice));
            //    RecalculateChildRoomPrices(childPrice, 3, price.RoomPrice, -(lunchChildPrice + allInclusiveChildPrice));
            //    RecalculateChildRoomPrices(childPrice, 4, price.RoomPrice, -allInclusiveChildPrice);
            //}
		}

		private void RecalculateAdultPrices(short pensionModeId, short orderNumber)
		{
			if (!_recalculating && _foodPrices != null && _adultPrices != null)
			{
				_recalculating = true;
				try
				{
					switch (pensionModeId)
					{
						case RoomOnly:
                            RecalculateRoomPrices(orderNumber);
							break;
						case BedBreakfast:
                            RecalculateRoomBreakfastPrices(orderNumber);
							break;
						case HalfBoard:
                            RecalculateRoomBreakfast1MealPrices(orderNumber);
							break;
						case FullBoard:
                            RecalculateRoomBreakfast2MealsPrices(orderNumber);
							break;
						case AllInclusive:
                            RecalculateAllInclusivePrices(orderNumber);
							break;
					}
				}
				finally
				{
					_recalculating = false;
				}
			}
		}

        private void RecalculateChildPrices(short pensionModeId, short orderNumber)
        {
        }

        private decimal GetFoodPrice(short pensionModeId, FoodLine lineType, short orderNumber, decimal roomOnlyPrice)
        {
            var price = decimal.Zero;

            if (pensionModeId != RoomOnly)
            {
                var foodPrice = GetFoodPriceLine(lineType, orderNumber);
                switch (pensionModeId)
                {
                    case BedBreakfast:
                        price += GetFoodBreakfastPrice(pensionModeId, foodPrice, orderNumber, roomOnlyPrice, lineType);
                        price += GetDrinkBreakfastPrice(pensionModeId, foodPrice, orderNumber, roomOnlyPrice, lineType);
                        break;
                    case HalfBoard:
                        price += GetFoodBreakfastPrice(pensionModeId, foodPrice, orderNumber, roomOnlyPrice, lineType);
                        price += GetDrinkBreakfastPrice(pensionModeId, foodPrice, orderNumber, roomOnlyPrice, lineType);
                        price += Get1MealPrice(pensionModeId, foodPrice, orderNumber, roomOnlyPrice, lineType);
                        break;
                    case FullBoard:
                        price += GetFoodBreakfastPrice(pensionModeId, foodPrice, orderNumber, roomOnlyPrice, lineType);
                        price += GetDrinkBreakfastPrice(pensionModeId, foodPrice, orderNumber, roomOnlyPrice, lineType);
                        price += GetFoodLunchPrice(pensionModeId, foodPrice, orderNumber, roomOnlyPrice, lineType);
                        price += GetDrinkLunchPrice(pensionModeId, foodPrice, orderNumber, roomOnlyPrice, lineType);
                        price += GetFoodDinnerPrice(pensionModeId, foodPrice, orderNumber, roomOnlyPrice, lineType);
                        price += GetDrinkDinnerPrice(pensionModeId, foodPrice, orderNumber, roomOnlyPrice, lineType);
                        break;
                    case AllInclusive:
                        price += GetFoodBreakfastPrice(pensionModeId, foodPrice, orderNumber, roomOnlyPrice, lineType);
                        price += GetDrinkBreakfastPrice(pensionModeId, foodPrice, orderNumber, roomOnlyPrice, lineType);
                        price += GetFoodLunchPrice(pensionModeId, foodPrice, orderNumber, roomOnlyPrice, lineType);
                        price += GetDrinkLunchPrice(pensionModeId, foodPrice, orderNumber, roomOnlyPrice, lineType);
                        price += GetFoodDinnerPrice(pensionModeId, foodPrice, orderNumber, roomOnlyPrice, lineType);
                        price += GetDrinkDinnerPrice(pensionModeId, foodPrice, orderNumber, roomOnlyPrice, lineType);
                        price += GetAllInclusiveAdultsPrice(pensionModeId, roomOnlyPrice);
                        break;
                }

                //price += GetFoodPrice(_defaultFoodBreakfastAdultsPrices, pensionModeId);
                //price += GetFoodPrice(_defaultDrinkBreakfastAdultsPrices, pensionModeId);
                //price += GetFoodPrice(_defaultFoodLunchAdultsPrices, pensionModeId);
                //price += GetFoodPrice(_defaultDrinkLunchAdultsPrices, pensionModeId);
                //price += GetFoodPrice(_defaultFoodDinnerAdultsPrices, pensionModeId);
                //price += GetFoodPrice(_defaultDrinkDinnerAdultsPrices, pensionModeId);
                //if (pensionModeId == AllInclusive)
                //    price += _defaultAllInclusiveAdultsPrice ?? decimal.Zero;

                //if (ApplicationMode == PriceCalculationType.Room &&
                //    orderNumber > 0 && orderNumber < short.MaxValue)
                //    return price * orderNumber;
            }

            return price;
        }

        private void RecalculateDerivedAdultPrices(AdultLine adultLine,
            short pensionModeId, short? orderNumber, decimal? price, bool excludeExtraBeds)
        {
            foreach (var derivedPriceRate in DerivedPriceRates)
            {
                var foodPrice = decimal.Zero;
                if (!derivedPriceRate.SupplementInValue && derivedPriceRate.PercentValueOverRoom)
                    foodPrice = GetFoodPrice(pensionModeId, FoodLine.Adult, orderNumber ?? 1, price ?? decimal.Zero);

                derivedPriceRate.CalculateAdultPrices(adultLine,
                    pensionModeId, orderNumber, price, foodPrice, excludeExtraBeds);
            }
        }

        private void RecalculateDerivedChildPrices(ChildLine childLine,
            short pensionModeId, short? orderNumber, decimal? price, bool excludeChildren)
        {
            foreach (var derivedPriceRate in DerivedPriceRates)
            {
                var foodPrice = decimal.Zero;
                if (!derivedPriceRate.SupplementInValue && derivedPriceRate.PercentValueOverRoom)
                    foodPrice = GetFoodPrice(pensionModeId, FoodLine.Child, orderNumber ?? 1, price ?? decimal.Zero);

                derivedPriceRate.CalculateChildPrices(childLine,
                    pensionModeId, orderNumber, price, foodPrice, excludeChildren);
            }
        }

		private void AdultPriceChanged(object sender, AdultPriceChangedEventArgs e)
		{
            if (e.OrderNumber.HasValue && _adultAutocalculateLines.Contains(e.AdultLine))
                RecalculateAdultPrices(e.PensionModeId, e.OrderNumber.Value);
                
            if (DerivedPriceRates != null && DerivedPriceRates.Count > 0)
                RecalculateDerivedAdultPrices(e.AdultLine, e.PensionModeId,
                    e.OrderNumber, e.Price, _excludeExtraBeds);
		}

        private void ChildPriceChanged(object sender, ChildPriceChangedEventArgs e)
        {
            if (e.OrderNumber.HasValue && _childAutocalculateLines.Contains(e.ChildLine))
                RecalculateChildPrices(e.PensionModeId, e.OrderNumber.Value);

            if (DerivedPriceRates != null && DerivedPriceRates.Count > 0)
            {
                RecalculateDerivedChildPrices(e.ChildLine, e.PensionModeId,
                    e.OrderNumber, e.Price, _excludeChildren);
            }
        }

		private void FoodPriceChanged(object sender, PriceChangedEventArgs e)
		{
			if (e.OrderNumber.HasValue)
				RecalculateAdultPrices(e.PensionModeId, e.OrderNumber.Value);
			else
			{
				for (short orderNumber = 1; orderNumber <= _maxPersons; orderNumber++)
					RecalculateAdultPrices(e.PensionModeId, orderNumber);
			}
		}

        private FoodPriceContract GetFoodPriceLine(FoodLine lineType, short? persons)
        {
            FoodPriceContract price = null;

            var adultLine = FoodPrices.FirstOrDefault(x => x.LineType.Equals(lineType));
            if (adultLine != null)
            {
                if (persons.HasValue && !persons.Value.Equals(short.MaxValue))
                    price = adultLine.Prices.FirstOrDefault(x => persons.Value.Equals(x.OrderNumber));

                if (price == null)
                    price = adultLine.Prices.FirstOrDefault(x => !x.OrderNumber.HasValue);
            }

            return price;
        }

        #region Default Adult Prices

        private decimal? GetDefaultFoodBreakfastAdultsPrice(short pensionModeId, short? persons, decimal roomOnlyPrice)
        {
            if (_defaultBreakfastAdultsPriceInPercent[pensionModeId - 1])
                return roomOnlyPrice * _defaultFoodBreakfastAdultsPrices[pensionModeId - 1] / 100;
            else if (persons.HasValue && persons.Value > 0 && persons.Value < short.MaxValue)
            {
                var price = _defaultFoodBreakfastAdultsPrices[pensionModeId - 1];
                if (_applicationMode == PriceCalculationType.Room && price.HasValue)
                    price *= persons.Value;

                return price;
            }

            return null;
        }

        private decimal? GetDefaultDrinkBreakfastAdultsPrice(short pensionModeId, short? persons, decimal roomOnlyPrice)
        {
            if (_defaultBreakfastAdultsPriceInPercent[pensionModeId - 1])
                return roomOnlyPrice * _defaultDrinkBreakfastAdultsPrices[pensionModeId - 1] / 100;
            else if (persons.HasValue && persons.Value > 0 && persons.Value < short.MaxValue)
            {
                var price = _defaultDrinkBreakfastAdultsPrices[pensionModeId - 1];
                if (_applicationMode == PriceCalculationType.Room && price.HasValue)
                    price *= persons.Value;

                return price;
            }

            return null;
        }

        private decimal? GetDefaultFoodLunchAdultsPrice(short pensionModeId, short? persons, decimal roomOnlyPrice)
        {
            if (_defaultLunchAdultsPriceInPercent[pensionModeId - 1])
                return roomOnlyPrice * _defaultFoodLunchAdultsPrices[pensionModeId - 1] / 100;
            else if (persons.HasValue && persons.Value > 0 && persons.Value < short.MaxValue)
            {
                var price = _defaultFoodLunchAdultsPrices[pensionModeId - 1];
                if (_applicationMode == PriceCalculationType.Room && price.HasValue)
                    price *= persons.Value;

                return price;
            }

            return null;
        }

        private decimal? GetDefaultDrinkLunchAdultsPrice(short pensionModeId, short? persons, decimal roomOnlyPrice)
        {
            if (_defaultLunchAdultsPriceInPercent[pensionModeId - 1])
                return roomOnlyPrice * _defaultDrinkLunchAdultsPrices[pensionModeId - 1] / 100;
            else if (persons.HasValue && persons.Value > 0 && persons.Value < short.MaxValue)
            {
                var price = _defaultDrinkLunchAdultsPrices[pensionModeId - 1];
                if (_applicationMode == PriceCalculationType.Room && price.HasValue)
                    price *= persons.Value;

                return price;
            }

            return null;
        }

        private decimal? GetDefaultFoodDinnerAdultsPrice(short pensionModeId, short? persons, decimal roomOnlyPrice)
        {
            if (_defaultDinnerAdultsPriceInPercent[pensionModeId - 1])
                return roomOnlyPrice * _defaultFoodDinnerAdultsPrices[pensionModeId - 1] / 100;
            else if (persons.HasValue && persons.Value > 0 && persons.Value < short.MaxValue)
            {
                var price = _defaultFoodDinnerAdultsPrices[pensionModeId - 1];
                if (_applicationMode == PriceCalculationType.Room && price.HasValue)
                    price *= persons.Value;

                return price;
            }

            return null;
        }

        private decimal? GetDefaultDrinkDinnerAdultsPrice(short pensionModeId, short? persons, decimal roomOnlyPrice)
        {
            if (_defaultDinnerAdultsPriceInPercent[pensionModeId - 1])
                return roomOnlyPrice * _defaultDrinkDinnerAdultsPrices[pensionModeId - 1] / 100;
            else if (persons.HasValue && persons.Value > 0 && persons.Value < short.MaxValue)
            {
                var price = _defaultDrinkDinnerAdultsPrices[pensionModeId - 1];
                if (_applicationMode == PriceCalculationType.Room && price.HasValue)
                    price *= persons.Value;

                return price;
            }

            return null;
        }

        private decimal GetAllInclusiveAdultsPrice(short? persons, decimal roomOnlyPrice)
        {
            if (_defaultAllInclusiveAdultsPrice.HasValue)
            {
                if (_defaultAllInclusiveAdultsPriceInPercent)
                    return roomOnlyPrice * _defaultAllInclusiveAdultsPrice.Value / 100;
                else if (persons.HasValue && persons.Value > 0 && persons.Value < short.MaxValue)
                {
                    var price = _defaultAllInclusiveAdultsPrice.Value;
                    if (_applicationMode == PriceCalculationType.Room)
                        price *= persons.Value;

                    return price;
                }
            }

            return decimal.Zero;
        }

        #endregion

        #region Default Child Prices

        private decimal? GetDefaultFoodBreakfastChildrenPrice(short pensionModeId, short? persons, decimal roomOnlyPrice)
        {
            if (!_defaultBreakfastChildrenPriceInPercent[pensionModeId - 1] &&
                persons.HasValue && persons.Value > 0 && persons.Value < short.MaxValue)
            {
                var price = _defaultFoodBreakfastChildrenPrices[pensionModeId - 1];
                if (_applicationMode == PriceCalculationType.Room && price.HasValue)
                    price *= persons.Value;

                return price;
            }

            return null;
        }

        private decimal? GetDefaultDrinkBreakfastChildrenPrice(short pensionModeId, short? persons, decimal roomOnlyPrice)
        {
            if (!_defaultBreakfastChildrenPriceInPercent[pensionModeId - 1] &&
                persons.HasValue && persons.Value > 0 && persons.Value < short.MaxValue)
            {
                var price = _defaultDrinkBreakfastChildrenPrices[pensionModeId - 1];
                if (_applicationMode == PriceCalculationType.Room && price.HasValue)
                    price *= persons.Value;

                return price;
            }

            return null;
        }

        private decimal? GetDefaultFoodLunchChildrenPrice(short pensionModeId, short? persons, decimal roomOnlyPrice)
        {
            if (!_defaultLunchChildrenPriceInPercent[pensionModeId - 1] &&
                persons.HasValue && persons.Value > 0 && persons.Value < short.MaxValue)
            {
                var price = _defaultFoodLunchChildrenPrices[pensionModeId - 1];
                if (_applicationMode == PriceCalculationType.Room && price.HasValue)
                    price *= persons.Value;

                return price;
            }

            return null;
        }

        private decimal? GetDefaultDrinkLunchChildrenPrice(short pensionModeId, short? persons, decimal roomOnlyPrice)
        {
            if (!_defaultLunchChildrenPriceInPercent[pensionModeId - 1] &&
                persons.HasValue && persons.Value > 0 && persons.Value < short.MaxValue)
            {
                var price = _defaultDrinkLunchChildrenPrices[pensionModeId - 1];
                if (_applicationMode == PriceCalculationType.Room && price.HasValue)
                    price *= persons.Value;

                return price;
            }

            return null;
        }

        private decimal? GetDefaultFoodDinnerChildrenPrice(short pensionModeId, short? persons, decimal roomOnlyPrice)
        {
            if (!_defaultDinnerChildrenPriceInPercent[pensionModeId - 1] &&
                persons.HasValue && persons.Value > 0 && persons.Value < short.MaxValue)
            {
                var price = _defaultFoodDinnerChildrenPrices[pensionModeId - 1];
                if (_applicationMode == PriceCalculationType.Room && price.HasValue)
                    price *= persons.Value;

                return price;
            }

            return null;
        }

        private decimal? GetDefaultDrinkDinnerChildrenPrice(short pensionModeId, short? persons, decimal roomOnlyPrice)
        {
            if (!_defaultDinnerChildrenPriceInPercent[pensionModeId - 1] &&
                persons.HasValue && persons.Value > 0 && persons.Value < short.MaxValue)
            {
                var price = _defaultDrinkDinnerChildrenPrices[pensionModeId - 1];
                if (_applicationMode == PriceCalculationType.Room && price.HasValue)
                    price *= persons.Value;

                return price;
            }

            return null;
        }

        private decimal? GetDefaultAllInclusiveChildrenPrice(short? persons, decimal roomOnlyPrice)
        {
            if (!_defaultAllInclusiveChildrenPriceInPercent &&
                persons.HasValue && persons.Value > 0 && persons.Value < short.MaxValue)
            {
                var price = _defaultAllInclusiveChildrenPrice;
                if (_applicationMode == PriceCalculationType.Room && price.HasValue)
                    price *= persons.Value;

                return price;
            }

            return null;
        }

        #endregion

        private static decimal? GetMealPrice(decimal? foodPrice, decimal? drinkPrice)
        {
            if (foodPrice.HasValue || drinkPrice.HasValue)
                return (foodPrice ?? decimal.Zero) + (drinkPrice ?? decimal.Zero);

            return null;
        }

        private decimal GetRoomAdultsPrice(short pensionModeId, short? persons, decimal pensionPrice)
        {
            var roomPrice = pensionPrice;
            if (pensionModeId > RoomOnly)
            {
                decimal? breakfastPrice = null;
                decimal? lunchPrice = null;
                decimal? dinnerPrice = null;
                decimal? allInclusivePrice = null;

                var mealsPrice = decimal.Zero;
                var percent = decimal.Zero;

                var adultPrice = GetFoodPriceLine(FoodLine.Adult, persons);
                if (adultPrice != null)
                {
                    switch (pensionModeId)
                    {
                        case BedBreakfast:
                            breakfastPrice = GetMealPrice(adultPrice.RoomBreakfastPensionPrice.BreakfastPrice,
                                adultPrice.RoomBreakfastPensionPrice.DrinkPriceAtBreakfast);
                            break;
                        case HalfBoard:
                            breakfastPrice = GetMealPrice(adultPrice.RoomBreakfast1MealPensionPrice.BreakfastPrice,
                                adultPrice.RoomBreakfast1MealPensionPrice.DrinkPriceAtBreakfast);
                            lunchPrice = GetMealPrice(adultPrice.RoomBreakfast1MealPensionPrice.LunchPrice,
                                adultPrice.RoomBreakfast1MealPensionPrice.DrinkPriceAtLunch);
                            dinnerPrice = GetMealPrice(adultPrice.RoomBreakfast1MealPensionPrice.DinnerPrice,
                                adultPrice.RoomBreakfast1MealPensionPrice.DrinkPriceAtDinner);
                            break;
                        case FullBoard:
                            breakfastPrice = GetMealPrice(adultPrice.RoomBreakfast2MealsPensionPrice.BreakfastPrice,
                                adultPrice.RoomBreakfast2MealsPensionPrice.DrinkPriceAtBreakfast);
                            lunchPrice = GetMealPrice(adultPrice.RoomBreakfast2MealsPensionPrice.LunchPrice,
                                adultPrice.RoomBreakfast2MealsPensionPrice.DrinkPriceAtLunch);
                            dinnerPrice = GetMealPrice(adultPrice.RoomBreakfast2MealsPensionPrice.DinnerPrice,
                                adultPrice.RoomBreakfast2MealsPensionPrice.DrinkPriceAtDinner);
                            break;
                        case AllInclusive:
                            breakfastPrice = GetMealPrice(adultPrice.AllInclusivePensionPrice.BreakfastPrice,
                                adultPrice.AllInclusivePensionPrice.DrinkPriceAtBreakfast);
                            lunchPrice = GetMealPrice(adultPrice.AllInclusivePensionPrice.LunchPrice,
                                adultPrice.AllInclusivePensionPrice.DrinkPriceAtLunch);
                            dinnerPrice = GetMealPrice(adultPrice.AllInclusivePensionPrice.DinnerPrice,
                                adultPrice.AllInclusivePensionPrice.DrinkPriceAtDinner);
                            break;
                    }

                    if (!breakfastPrice.HasValue)
                        breakfastPrice = GetMealPrice(adultPrice.AnyPensionPrice.BreakfastPrice,
                            adultPrice.AnyPensionPrice.DrinkPriceAtBreakfast);

                    if (pensionModeId > BedBreakfast)
                    {
                        if (!lunchPrice.HasValue)
                            lunchPrice = GetMealPrice(adultPrice.AnyPensionPrice.LunchPrice,
                                adultPrice.AnyPensionPrice.DrinkPriceAtLunch);
                        if (!dinnerPrice.HasValue)
                            dinnerPrice = GetMealPrice(adultPrice.AnyPensionPrice.DinnerPrice,
                                adultPrice.AnyPensionPrice.DrinkPriceAtDinner);
                    }

                    if (_applicationMode != PriceCalculationType.Room && persons.HasValue)
                    {
                        if (breakfastPrice.HasValue)
                            breakfastPrice = breakfastPrice.Value * persons.Value;
                        if (lunchPrice.HasValue)
                            lunchPrice = lunchPrice.Value * persons.Value;
                        if (dinnerPrice.HasValue)
                            dinnerPrice = dinnerPrice.Value * persons.Value;
                    }
                }

                if (pensionModeId >= BedBreakfast)
                {
                    var inPercent = false;
                    if (!breakfastPrice.HasValue)
                    {
                        inPercent = _defaultBreakfastAdultsPriceInPercent[pensionModeId - 1];
                        if (!inPercent)
                        {
                            breakfastPrice = GetMealPrice(_defaultFoodBreakfastAdultsPrices[pensionModeId - 1],
                                                          _defaultDrinkBreakfastAdultsPrices[pensionModeId - 1]);

                            if (persons.HasValue && breakfastPrice.HasValue)
                                breakfastPrice = breakfastPrice.Value * persons.Value;
                        }
                    }

                    if (!breakfastPrice.HasValue && inPercent)
                    {
                        percent += _defaultFoodBreakfastAdultsPrices[pensionModeId - 1] ?? decimal.Zero;
                        percent += _defaultDrinkBreakfastAdultsPrices[pensionModeId - 1] ?? decimal.Zero;
                    }

                    if (breakfastPrice.HasValue)
                        mealsPrice += breakfastPrice.Value;
                }

                if ((pensionModeId == HalfBoard && _halfBoardEatMode == HalfBoardEatMode.Lunch) ||
                    pensionModeId >= FullBoard)
                {
                    var inPercent = false;
                    if (!lunchPrice.HasValue)
                    {
                        inPercent = _defaultLunchAdultsPriceInPercent[pensionModeId - 1];
                        if (!inPercent)
                        {
                            lunchPrice = GetMealPrice(_defaultFoodLunchAdultsPrices[pensionModeId - 1],
                                                      _defaultDrinkLunchAdultsPrices[pensionModeId - 1]);

                            if (persons.HasValue && lunchPrice.HasValue)
                                lunchPrice = lunchPrice.Value * persons.Value;
                        }
                    }

                    if (!lunchPrice.HasValue && inPercent)
                    {
                        percent += _defaultFoodLunchAdultsPrices[pensionModeId - 1] ?? decimal.Zero;
                        percent += _defaultDrinkLunchAdultsPrices[pensionModeId - 1] ?? decimal.Zero;
                    }

                    if (lunchPrice.HasValue)
                        mealsPrice += lunchPrice.Value;
                }

                if ((pensionModeId == HalfBoard && _halfBoardEatMode == HalfBoardEatMode.Dinner) ||
                    pensionModeId >= FullBoard)
                {
                    var inPercent = false;
                    if (!dinnerPrice.HasValue)
                    {
                        inPercent = _defaultDinnerAdultsPriceInPercent[pensionModeId - 1];
                        if (!inPercent)
                        {
                            dinnerPrice = GetMealPrice(_defaultFoodDinnerAdultsPrices[pensionModeId - 1],
                                                       _defaultDrinkDinnerAdultsPrices[pensionModeId - 1]);

                            if (persons.HasValue && dinnerPrice.HasValue)
                                dinnerPrice = dinnerPrice.Value * persons.Value;
                        }
                    }

                    if (!dinnerPrice.HasValue && inPercent)
                    {
                        percent += _defaultFoodDinnerAdultsPrices[pensionModeId - 1] ?? decimal.Zero;
                        percent += _defaultDrinkDinnerAdultsPrices[pensionModeId - 1] ?? decimal.Zero;
                    }

                    if (dinnerPrice.HasValue)
                        mealsPrice += dinnerPrice.Value;
                }

                if (pensionModeId == AllInclusive)
                {
                    var inPercent = false;
                    if (!allInclusivePrice.HasValue)
                    {
                        inPercent = _defaultAllInclusiveAdultsPriceInPercent;
                        if (!inPercent)
                        {
                            allInclusivePrice = _defaultAllInclusiveAdultsPrice;

                            if (persons.HasValue && allInclusivePrice.HasValue)
                                allInclusivePrice = allInclusivePrice.Value * persons.Value;
                        }

                        if (!allInclusivePrice.HasValue && inPercent)
                            percent += _defaultAllInclusiveAdultsPrice ?? decimal.Zero;

                        if (allInclusivePrice.HasValue)
                            mealsPrice += allInclusivePrice.Value;                        
                    }
                }

                if (percent > decimal.Zero)
                    mealsPrice += roomPrice - (roomPrice / (decimal.One + percent / 100));

                roomPrice -= mealsPrice;
            }

            return roomPrice;
        }

        #endregion
        #region Constructors

        public RoomTypePriceContract()
            : base()
        {
            PriceType = 0;
            _applicationMode = PriceCalculationType.Room;
            _adultPrices = new TypedList<AdultPriceByLineTypeContract>();
            _childPrices = new TypedList<ChildPriceByLineTypeContract>();
            _foodPrices = new TypedList<FoodPriceByLineTypeContract>();
        }

        public RoomTypePriceContract(
            Guid priceRateId, string description,
            Guid roomTypeId, string roomTypeDesc, string applicationModeDesc,
            short pensionModeId, string pensionModeDesc, IDictionary<short, string> pensionModes,
            short minPersons, short maxPersons, short basePersons, short? persons, HalfBoardEatMode halfBoardEatMode,
            bool[] defaultBreakfastAdultsPriceInPercent, decimal?[] defaultFoodBreakfastAdultsPrices, decimal?[] defaultDrinkBreakfastAdultsPrices,
            bool[] defaultLunchAdultsPriceInPercent, decimal?[] defaultFoodLunchAdultsPrices, decimal?[] defaultDrinkLunchAdultsPrices,
            bool[] defaultDinnerAdultsPriceInPercent, decimal?[] defaultFoodDinnerAdultsPrices, decimal?[] defaultDrinkDinnerAdultsPrices,
            bool[] defaultBreakfastChildrenPriceInPercent, decimal?[] defaultFoodBreakfastChildrenPrices, decimal?[] defaultDrinkBreakfastChildrenPrices,
            bool[] defaultLunchChildrenPriceInPercent, decimal?[] defaultFoodLunchChildrenPrices, decimal?[] defaultDrinkLunchChildrenPrices,
            bool[] defaultDinnerChildrenPriceInPercent, decimal?[] defaultFoodDinnerChildrenPrices, decimal?[] defaultDrinkDinnerChildrenPrices,
            bool defaultAllInclusiveAdultsPriceInPercent, decimal? defaultAllInclusiveAdultsPrice,
            bool defaultAllInclusiveChildrenPriceInPercent, decimal? defaultAllInclusiveChildrenPrice,
            AdultLine[] adultAutocalculateLines, ChildLine[] childAutocalculateLines,
            bool editableAdultPrice, bool editableChildPrice, bool editableFoodPrice,
            AdultPriceByLineTypeContract[] adultPrices,
            ChildPriceByLineTypeContract[] childPrices,
            FoodPriceByLineTypeContract[] foodPrices,
            PriceCalculationType applicationMode,
            DateTime fromDate, DateTime toDate,
            string currency,
            bool taxIncluded,
            bool includeSunday,
            bool includeMonday,
            bool includeTuesday,
            bool includeWednesday,
            bool includeThursday,
            bool includeFriday,
            bool includeSaturday,
            short? undoLevel,
            decimal? fixedExchangeRate,
            bool excludeExtraBeds,
            bool excludeChildren,
            DerivedPriceRateContract[] derivedPriceRates)
            : base()
        {
            _priceRateId = priceRateId;
            _description = description;
            _roomTypeId = roomTypeId;
            _roomTypeDesc = roomTypeDesc;
            _pensionModeId = pensionModeId;
            _pensionModeDesc = pensionModeDesc;
            _pensionModes = pensionModes;
            _minPersons = minPersons;
            _maxPersons = maxPersons;
            _basePersons = basePersons;
            _persons = persons ?? basePersons;
            _applicationMode = applicationMode;
            _applicationModeDesc = applicationModeDesc;
            _currency = currency;
            _taxIncluded = taxIncluded;
            _fromDate = fromDate;
            _toDate = toDate;
            _includeSunday = includeSunday;
            _includeMonday = includeMonday;
            _includeTuesday = includeTuesday;
            _includeWednesday = includeWednesday;
            _includeThursday = includeThursday;
            _includeFriday = includeFriday;
            _includeSaturday = includeSaturday;
            _fixedExchangeRate = fixedExchangeRate;
            _halfBoardEatMode = halfBoardEatMode;
            _defaultBreakfastAdultsPriceInPercent = defaultBreakfastAdultsPriceInPercent;
            _defaultFoodBreakfastAdultsPrices = defaultFoodBreakfastAdultsPrices;
            _defaultDrinkBreakfastAdultsPrices = defaultDrinkBreakfastAdultsPrices;
            _defaultLunchAdultsPriceInPercent = defaultLunchAdultsPriceInPercent;
            _defaultFoodLunchAdultsPrices = defaultFoodLunchAdultsPrices;
            _defaultDrinkLunchAdultsPrices = defaultDrinkLunchAdultsPrices;
            _defaultDinnerAdultsPriceInPercent = defaultDinnerAdultsPriceInPercent;
            _defaultFoodDinnerAdultsPrices = defaultFoodDinnerAdultsPrices;
            _defaultDrinkDinnerAdultsPrices = defaultDrinkDinnerAdultsPrices;
            _defaultBreakfastChildrenPriceInPercent = defaultBreakfastChildrenPriceInPercent;
            _defaultFoodBreakfastChildrenPrices = defaultFoodBreakfastChildrenPrices;
            _defaultDrinkBreakfastChildrenPrices = defaultDrinkBreakfastChildrenPrices;
            _defaultLunchChildrenPriceInPercent = defaultLunchChildrenPriceInPercent;
            _defaultFoodLunchChildrenPrices = defaultFoodLunchChildrenPrices;
            _defaultDrinkLunchChildrenPrices = defaultDrinkLunchChildrenPrices;
            _defaultDinnerChildrenPriceInPercent = defaultDinnerChildrenPriceInPercent;
            _defaultFoodDinnerChildrenPrices = defaultFoodDinnerChildrenPrices;
            _defaultDrinkDinnerChildrenPrices = defaultDrinkDinnerChildrenPrices;
            _defaultAllInclusiveAdultsPriceInPercent = defaultAllInclusiveAdultsPriceInPercent;
            _defaultAllInclusiveAdultsPrice = defaultAllInclusiveAdultsPrice;
            _defaultAllInclusiveChildrenPriceInPercent = defaultAllInclusiveChildrenPriceInPercent;
            _defaultAllInclusiveChildrenPrice = defaultAllInclusiveChildrenPrice;
            _adultAutocalculateLines = adultAutocalculateLines;
            _childAutocalculateLines = childAutocalculateLines;
            _editableAdultPrice = editableAdultPrice;
            _editableChildPrice = editableChildPrice;
            _editableFoodPrice = editableFoodPrice;
            UndoLevel = undoLevel;
            PriceType = 0;

            _excludeExtraBeds = excludeExtraBeds;
            _excludeChildren = excludeChildren;

            this.adultPrices = new TypedList<AdultPriceByLineTypeContract>(adultPrices);
            this.childPrices = new TypedList<ChildPriceByLineTypeContract>(childPrices);
            this.foodPrices = new TypedList<FoodPriceByLineTypeContract>(foodPrices);
            _derivedPriceRates = new TypedList<DerivedPriceRateContract>(derivedPriceRates);
        }

        #endregion
        #region Public Methods

        #region Food & Beverage prices

        public decimal GetFoodBreakfastPrice(short pensionModeId, FoodPriceContract contract,
            short? persons, decimal roomOnlyPrice, FoodLine lineType)
        {
            decimal? price = null;
            if (contract != null)
            {
                switch (pensionModeId)
                {
                    case BedBreakfast:
                        price = contract.RoomBreakfastPensionPrice.BreakfastPrice;
                        break;
                    case HalfBoard:
                        price = contract.RoomBreakfast1MealPensionPrice.BreakfastPrice;
                        break;
					case FullBoard:
                        price = contract.RoomBreakfast2MealsPensionPrice.BreakfastPrice;
                        break;
                    case AllInclusive:
                        price = contract.AllInclusivePensionPrice.BreakfastPrice;
                        break;
                    default:
                        return decimal.Zero;
                }

                if (!price.HasValue)
                {
                    price = contract.AnyPensionPrice.BreakfastPrice;
                    if (_applicationMode != PriceCalculationType.Room &&
                        price.HasValue && persons.HasValue &&
                        persons.Value > 0 && persons.Value < short.MaxValue)
                        price = price.Value * persons.Value;
                }
            }

            if (!price.HasValue)
            {
                switch (lineType)
                {
                    case FoodLine.Adult:
                    case FoodLine.ExtraAdult:
                        price = GetDefaultFoodBreakfastAdultsPrice(pensionModeId, persons, roomOnlyPrice);
                        break;
                    case FoodLine.Child:
                    case FoodLine.ExtraChild:
                        price = GetDefaultFoodBreakfastChildrenPrice(pensionModeId, persons, roomOnlyPrice);
                        break;
                }
            }

            return price ?? decimal.Zero;
        }

        public decimal GetDrinkBreakfastPrice(short pensionModeId, FoodPriceContract contract,
            short? persons, decimal roomOnlyPrice, FoodLine lineType)
        {
            decimal? price = null;
            if (contract != null)
            {
                switch (pensionModeId)
                {
					case BedBreakfast:
                        price = contract.RoomBreakfastPensionPrice.DrinkPriceAtBreakfast;
                        break;
					case HalfBoard:
                        price = contract.RoomBreakfast1MealPensionPrice.DrinkPriceAtBreakfast;
                        break;
					case FullBoard:
                        price = contract.RoomBreakfast2MealsPensionPrice.DrinkPriceAtBreakfast;
                        break;
					case AllInclusive:
                        price = contract.AllInclusivePensionPrice.DrinkPriceAtBreakfast;
                        break;
                    default:
						return decimal.Zero;
                }

                if (!price.HasValue)
                {
                    price = contract.AnyPensionPrice.DrinkPriceAtBreakfast;
                    if (_applicationMode != PriceCalculationType.Room &&
                        price.HasValue && persons.HasValue &&
                        persons.Value > 0 && persons.Value < short.MaxValue)
                        price = price.Value * persons.Value;
                }
            }

            if (!price.HasValue)
            {
                switch (lineType)
                {
                    case FoodLine.Adult:
                    case FoodLine.ExtraAdult:
                        price = GetDefaultDrinkBreakfastAdultsPrice(pensionModeId, persons, roomOnlyPrice);
                        break;
                    case FoodLine.Child:
                    case FoodLine.ExtraChild:
                        price = GetDefaultDrinkBreakfastChildrenPrice(pensionModeId, persons, roomOnlyPrice);
                        break;
                }
            }

            return price ?? decimal.Zero;
        }

        public decimal GetFoodLunchPrice(short pensionModeId, FoodPriceContract contract,
            short? persons, decimal roomOnlyPrice, FoodLine lineType)
        {
            decimal? price = null;
            if (contract != null)
            {
                switch (pensionModeId)
                {
					case HalfBoard:
                        price = contract.RoomBreakfast1MealPensionPrice.LunchPrice;
                        break;
					case FullBoard:
                        price = contract.RoomBreakfast2MealsPensionPrice.LunchPrice;
                        break;
					case AllInclusive:
                        price = contract.AllInclusivePensionPrice.LunchPrice;
                        break;
                    default:
						return decimal.Zero;
                }

                if (!price.HasValue)
                {
                    price = contract.AnyPensionPrice.DinnerPrice ?? contract.AnyPensionPrice.LunchPrice;
                    if (_applicationMode != PriceCalculationType.Room &&
                        price.HasValue && persons.HasValue &&
                        persons.Value > 0 && persons.Value < short.MaxValue)
                        price = price.Value * persons.Value;
                }
            }

            if (!price.HasValue)
            {
                switch (lineType)
                {
                    case FoodLine.Adult:
                    case FoodLine.ExtraAdult:
                        price = GetDefaultFoodLunchAdultsPrice(pensionModeId, persons, roomOnlyPrice);
                        break;
                    case FoodLine.Child:
                    case FoodLine.ExtraChild:
                        price = GetDefaultFoodLunchChildrenPrice(pensionModeId, persons, roomOnlyPrice);
                        break;
                }
            }

            return price ?? decimal.Zero;
        }

        public decimal GetDrinkLunchPrice(short pensionModeId, FoodPriceContract contract,
            short? persons, decimal roomOnlyPrice, FoodLine lineType)
        {
            decimal? price = null;
            if (contract != null)
            {
                switch (pensionModeId)
                {
					case HalfBoard:
                        price = contract.RoomBreakfast1MealPensionPrice.DrinkPriceAtDinner;
                        break;
					case FullBoard:
                        price = contract.RoomBreakfast2MealsPensionPrice.DrinkPriceAtDinner;
                        break;
					case AllInclusive:
                        price = contract.AllInclusivePensionPrice.DrinkPriceAtDinner;
                        break;
                    default:
						return decimal.Zero;
                }

                if (!price.HasValue)
                {
                    price = contract.AnyPensionPrice.DrinkPriceAtDinner ?? contract.AnyPensionPrice.DrinkPriceAtLunch;
                    if (_applicationMode != PriceCalculationType.Room &&
                        price.HasValue && persons.HasValue &&
                        persons.Value > 0 && persons.Value < short.MaxValue)
                        price = price.Value * persons.Value;
                }
            }

            if (!price.HasValue)
            {
                switch (lineType)
                {
                    case FoodLine.Adult:
                    case FoodLine.ExtraAdult:
                        price = GetDefaultDrinkLunchAdultsPrice(pensionModeId, persons, roomOnlyPrice);
                        break;
                    case FoodLine.Child:
                    case FoodLine.ExtraChild:
                        price = GetDefaultDrinkLunchChildrenPrice(pensionModeId, persons, roomOnlyPrice);
                        break;
                }
            }

            return price ?? decimal.Zero;
        }

        public decimal GetFoodDinnerPrice(short pensionModeId, FoodPriceContract contract,
            short? persons, decimal roomOnlyPrice, FoodLine lineType)
        {
            decimal? price = null;
            if (contract != null)
            {
                switch (pensionModeId)
                {
					case HalfBoard:
                        price = contract.RoomBreakfast1MealPensionPrice.DinnerPrice;
                        break;
					case FullBoard:
                        price = contract.RoomBreakfast2MealsPensionPrice.DinnerPrice;
                        break;
					case AllInclusive:
                        price = contract.AllInclusivePensionPrice.DinnerPrice;
                        break;
                    default:
						return decimal.Zero;
                }

                if (!price.HasValue)
                {
                    price = contract.AnyPensionPrice.DinnerPrice;
                    if (_applicationMode != PriceCalculationType.Room &&
                        price.HasValue && persons.HasValue &&
                        persons.Value > 0 && persons.Value < short.MaxValue)
                        price = price.Value * persons.Value;
                }
            }

            if (!price.HasValue)
            {
                switch (lineType)
                {
                    case FoodLine.Adult:
                    case FoodLine.ExtraAdult:
                        price = GetDefaultFoodDinnerAdultsPrice(pensionModeId, persons, roomOnlyPrice);
                        break;
                    case FoodLine.Child:
                    case FoodLine.ExtraChild:
                        price = GetDefaultFoodDinnerChildrenPrice(pensionModeId, persons, roomOnlyPrice);
                        break;
                }
            }

            return price ?? decimal.Zero;
        }

        public decimal GetDrinkDinnerPrice(short pensionModeId, FoodPriceContract contract,
            short? persons, decimal roomOnlyPrice, FoodLine lineType)
        {
            decimal? price = null;
            if (contract != null)
            {
                switch (pensionModeId)
                {
                    case HalfBoard:
                        price = contract.RoomBreakfast1MealPensionPrice.DrinkPriceAtDinner;
                        break;
                    case FullBoard:
                        price = contract.RoomBreakfast2MealsPensionPrice.DrinkPriceAtDinner;
                        break;
                    case AllInclusive:
                        price = contract.AllInclusivePensionPrice.DrinkPriceAtDinner;
                        break;
                    default:
                        return decimal.Zero;
                }

                if (!price.HasValue)
                {
                    price = contract.AnyPensionPrice.DrinkPriceAtDinner;
                    if (_applicationMode != PriceCalculationType.Room &&
                        price.HasValue && persons.HasValue &&
                        persons.Value > 0 && persons.Value < short.MaxValue)
                        price = price.Value * persons.Value;
                }
            }

            if (!price.HasValue)
            {
                switch (lineType)
                {
                    case FoodLine.Adult:
                    case FoodLine.ExtraAdult:
                        price = GetDefaultDrinkDinnerAdultsPrice(pensionModeId, persons, roomOnlyPrice);
                        break;
                    case FoodLine.Child:
                    case FoodLine.ExtraChild:
                        price = GetDefaultDrinkDinnerChildrenPrice(pensionModeId, persons, roomOnlyPrice);
                        break;
                }
            }

            return price ?? decimal.Zero;
        }

        public decimal Get1MealPrice(short pensionModeId, FoodPriceContract contract,
            short? persons, decimal roomOnlyPrice, FoodLine lineType)
        {
            var price = decimal.Zero;
            switch (_halfBoardEatMode)
            {
                case HalfBoardEatMode.Lunch:
                    price += GetFoodLunchPrice(pensionModeId, contract, persons, roomOnlyPrice, lineType);
                    price += GetDrinkLunchPrice(pensionModeId, contract, persons, roomOnlyPrice, lineType);
                    break;
                case HalfBoardEatMode.Dinner:
                    price += GetFoodDinnerPrice(pensionModeId, contract, persons, roomOnlyPrice, lineType);
                    price += GetDrinkDinnerPrice(pensionModeId, contract, persons, roomOnlyPrice, lineType);
                    break;
            }

            return price;
        }

        #endregion

        public bool AdultPricesModified
        {
            get
            {
                foreach (var adultPrice in AutoCalcAdultPrices)
                {
                    foreach (var price in adultPrice.Prices.Where(x => x.OrderNumber.HasValue))
                    {
                        if (price.AnyPensionPrice.Modified)
                            return true;
                        if (price.RoomPensionPrice.Modified)
                            return true;
                        if (price.RoomBreakfastPensionPrice.Modified)
                            return true;
                        if (price.RoomBreakfast1MealPensionPrice.Modified)
                            return true;
                        if (price.RoomBreakfast2MealsPensionPrice.Modified)
                            return true;
                        if (price.AllInclusivePensionPrice.Modified)
                            return true;
                    }
                }

                return false;
            }
            set
            {
                foreach (var adultPrice in AutoCalcAdultPrices)
                {
                    foreach (var price in adultPrice.Prices.Where(x => x.OrderNumber.HasValue))
                    {
                        price.AnyPensionPrice.Modified = value;
                        price.RoomPensionPrice.Modified = value;
                        price.RoomBreakfastPensionPrice.Modified = value;
                        price.RoomBreakfast1MealPensionPrice.Modified = value;
                        price.RoomBreakfast2MealsPensionPrice.Modified = value;
                        price.AllInclusivePensionPrice.Modified = value;
                    }
                }
            }
        }

        public bool ChildPricesModified
        {
            get
            {
                foreach (var childPrice in AutoCalcChildPrices)
                {
                    for (short pensionModeId = RoomOnly; pensionModeId <= AllInclusive; pensionModeId++)
                    {
                        var prices = GetRoomChildPrices(childPrice, pensionModeId);
                        foreach (var price in prices)
                            if (price != null && price.Modified)
                                return true;
                    }
                }

                return false;
            }
            set
            {
                foreach (var childPrice in AutoCalcChildPrices)
                {
                    for (short pensionModeId = RoomOnly; pensionModeId <= AllInclusive; pensionModeId++)
                    {
                        var prices = GetRoomChildPrices(childPrice, pensionModeId);
                        foreach (var price in prices)
                            price.Modified = value;
                    }
                }
            }
        }

        public void AdultPriceCalculation(AdultLine lineType, short? pensionModeId, bool enabled, bool recalculate)
        {
            foreach (var adultPrice in AutoCalcAdultPrices)
            {
                foreach (var price in adultPrice.Prices.Where(x => x.OrderNumber.HasValue))
                {
					if (!pensionModeId.HasValue || pensionModeId.Value == 1)
						price.RoomPensionPrice.CalculateRoomPrice = enabled;
					if (!pensionModeId.HasValue || pensionModeId.Value == 2)
						price.RoomBreakfastPensionPrice.CalculateRoomPrice = enabled;
					if (!pensionModeId.HasValue || pensionModeId.Value == 3)
						price.RoomBreakfast1MealPensionPrice.CalculateRoomPrice = enabled;
					if (!pensionModeId.HasValue || pensionModeId.Value == 4)
						price.RoomBreakfast2MealsPensionPrice.CalculateRoomPrice = enabled;
					if (!pensionModeId.HasValue || pensionModeId.Value == 5)
						price.AllInclusivePensionPrice.CalculateRoomPrice = enabled;

					if (enabled && recalculate)
					{
						if (pensionModeId.HasValue)
							RecalculateAdultPrices(pensionModeId.Value, price.OrderNumber.Value);
						else
						{
							foreach (var pm in PensionModes.Keys)
								RecalculateAdultPrices(pm, price.OrderNumber.Value);
						}
					}
                }
            }
        }

        public void ChildPriceCalculation(ChildLine lineType, short? pensionModeId, bool enabled, bool recalculate)
        {
        }

        public void UpdateEmptyPrices(short pensionModeId)
        {
            var adultPriceByLineType = AdultPrices.FirstOrDefault(x => x.LineType.Equals(AdultLine.Adult));
            var adultPrices = adultPriceByLineType.GetAdultPricesForPension(pensionModeId);
            var roomPrice = adultPrices.Where(x => x.RoomPrice.HasValue).Min(x => x.RoomPrice ?? decimal.Zero);
            if (roomPrice > decimal.Zero)
            {
                foreach (var adultPrice in adultPrices.Where(x => !x.RoomPrice.HasValue))
                    adultPrice.RoomPrice = roomPrice;
            }

            var childPriceByLineType = ChildPrices.FirstOrDefault(x => x.LineType.Equals(ChildLine.Child));
            var childPrice = childPriceByLineType.GetAnyOrderChildPriceForPension(pensionModeId);
            if (childPrice != null && !childPrice.RoomPrice.HasValue)
                childPrice.RoomPrice = decimal.Zero;
        }

        #region Adult Breakdown Prices

        public decimal? GetBreakfastAdultPrice(short pensionModeId)
        {
            var foodPrice = _defaultFoodBreakfastAdultsPrices[pensionModeId - 1];
            var drinkPrice = _defaultDrinkBreakfastAdultsPrices[pensionModeId - 1];

            if (!foodPrice.HasValue && !drinkPrice.HasValue)
                return null;

            return (foodPrice ?? decimal.Zero) + (drinkPrice ?? decimal.Zero);
        }

        public bool IsBreakfastAdultPriceInPercent(short pensionModeId)
        {
            return _defaultBreakfastAdultsPriceInPercent[pensionModeId - 1];
        }

        public decimal? GetLunchAdultPrice(short pensionModeId)
        {
            var foodPrice = _defaultFoodLunchAdultsPrices[pensionModeId - 1];
            var drinkPrice = _defaultDrinkLunchAdultsPrices[pensionModeId - 1];

            if (!foodPrice.HasValue && !drinkPrice.HasValue)
                return null;

            return (foodPrice ?? decimal.Zero) + (drinkPrice ?? decimal.Zero);
        }

        public bool IsLunchAdultPriceInPercent(short pensionModeId)
        {
            return _defaultLunchAdultsPriceInPercent[pensionModeId - 1];
        }

        public decimal? GetDinnerAdultPrice(short pensionModeId)
        {
            var foodPrice = _defaultFoodDinnerAdultsPrices[pensionModeId - 1];
            var drinkPrice = _defaultDrinkDinnerAdultsPrices[pensionModeId - 1];

            if (!foodPrice.HasValue && !drinkPrice.HasValue)
                return null;

            return (foodPrice ?? decimal.Zero) + (drinkPrice ?? decimal.Zero);
        }

        public bool IsDinnerAdultPriceInPercent(short pensionModeId)
        {
            return _defaultDinnerAdultsPriceInPercent[pensionModeId - 1];
        }

        public decimal? GetAllInclusiveAdultPrice()
        {
            return _defaultAllInclusiveAdultsPrice;
        }

        public bool IsAllInclusiveAdultPriceInPercent()
        {
            return _defaultAllInclusiveAdultsPriceInPercent;
        }

        #endregion
        #region Child Breakdown Prices

        public decimal? GetBreakfastChildPrice(short pensionModeId)
        {
            var foodPrice = _defaultFoodBreakfastChildrenPrices[pensionModeId - 1];
            var drinkPrice = _defaultDrinkBreakfastChildrenPrices[pensionModeId - 1];

            if (!foodPrice.HasValue && !drinkPrice.HasValue)
                return null;

            return (foodPrice ?? decimal.Zero) + (drinkPrice ?? decimal.Zero);
        }

        public bool IsBreakfastChildPriceInPercent(short pensionModeId)
        {
            return _defaultBreakfastChildrenPriceInPercent[pensionModeId - 1];
        }

        public decimal? GetLunchChildPrice(short pensionModeId)
        {
            var foodPrice = _defaultFoodLunchChildrenPrices[pensionModeId - 1];
            var drinkPrice = _defaultDrinkLunchChildrenPrices[pensionModeId - 1];

            if (!foodPrice.HasValue && !drinkPrice.HasValue)
                return null;

            return (foodPrice ?? decimal.Zero) + (drinkPrice ?? decimal.Zero);
        }

        public bool IsLunchChildPriceInPercent(short pensionModeId)
        {
            return _defaultLunchChildrenPriceInPercent[pensionModeId - 1];
        }

        public decimal? GetDinnerChildPrice(short pensionModeId)
        {
            var foodPrice = _defaultFoodDinnerChildrenPrices[pensionModeId - 1];
            var drinkPrice = _defaultDrinkDinnerChildrenPrices[pensionModeId - 1];

            if (!foodPrice.HasValue && !drinkPrice.HasValue)
                return null;

            return (foodPrice ?? decimal.Zero) + (drinkPrice ?? decimal.Zero);
        }

        public bool IsDinnerChildPriceInPercent(short pensionModeId)
        {
            return _defaultDinnerChildrenPriceInPercent[pensionModeId - 1];
        }

        public decimal? GetAllInclusiveChildPrice()
        {
            return _defaultAllInclusiveChildrenPrice;
        }

        public bool IsAllInclusiveChildPriceInPercent()
        {
            return _defaultAllInclusiveChildrenPriceInPercent;
        }

        #endregion

        #endregion
    }

    [DataContract]
	[Serializable]
    public class DerivedPriceRateContract : BaseContract, IPriceRatePrices
    {
        #region Constants

        public const short AnyPension = 0;
        public const short RoomPension = 1;
        public const short RoomBreakfastPension = 2;
        public const short RoomBreakfast1MealPension = 3;
        public const short RoomBreakfast2MealsPension = 4;
        public const short AllInclusivePension = 5;

        #endregion
        #region Members

        [DataMember]
        internal string _description;
        [DataMember]
        internal PriceCalculationType _applicationMode;
        [DataMember(Order = 0)]
        internal bool _supplementInValue;
        [DataMember(Order = 1)]
        internal decimal? _supplementPercentOrValue;
        [DataMember(Order = 1)]
        internal bool _percentValueOverRoom;
        [DataMember(Order = 2)]
        internal TypedList<AdultPriceByLineTypeContract> _adultPrices;
        [DataMember(Order = 2)]
        internal TypedList<ChildPriceByLineTypeContract> _childPrices;

        #endregion
        #region Properties

        public string Description { get { return _description; } }
        public PriceCalculationType ApplicationMode { get { return _applicationMode; } }
        public bool SupplementInValue { get { return _supplementInValue; } }
        public decimal? SupplementPercentOrValue { get { return _supplementPercentOrValue; } }
        public bool PercentValueOverRoom { get { return _percentValueOverRoom; } }

        public bool EditableAdultPrice { get { return false; } }
        public bool EditableChildPrice { get { return false; } }
        public bool EditableFoodPrice { get { return false; } }

        public TypedList<AdultPriceByLineTypeContract> AdultPrices
        {
            get { return _adultPrices; }
        }

        public TypedList<ChildPriceByLineTypeContract> ChildPrices
        {
            get { return _childPrices; }
        }

        #endregion
        #region Contructor

        public DerivedPriceRateContract(Guid id, string description, PriceCalculationType applicationMode,
            bool supplementInValue, decimal? supplementPercentOrValue, bool percentValueOverRoom,
            IEnumerable<AdultPriceByLineTypeContract> adultPrices,
            IEnumerable<ChildPriceByLineTypeContract> childPrices)
            : base()
        {
            Id = id;
            _description = description;
            _applicationMode = applicationMode;
            _supplementInValue = supplementInValue;
            _supplementPercentOrValue = supplementPercentOrValue;
            _percentValueOverRoom = percentValueOverRoom;
            _adultPrices = new TypedList<AdultPriceByLineTypeContract>(adultPrices);
            _childPrices = new TypedList<ChildPriceByLineTypeContract>(childPrices);
        }

        #endregion
        #region Private Methods

        private decimal? GetRoomPrice(decimal? price, decimal foodPrice, decimal value)
        {
            if (price.HasValue)
            {
                var roomPrice = price.Value;
                if (SupplementInValue)
                    roomPrice += value;
                else
                    roomPrice += (roomPrice - foodPrice) * value;

                return roomPrice;
            }

            return null;
        }

        private static RoomPriceContract GetAdultRoomPrice(AdultPriceByLineTypeContract adultPrice,
            short pensionModeId, short? orderNumber)
        {
            var orderPrice = adultPrice.Prices
                .FirstOrDefault(x => (x.OrderNumber ?? -1) == (orderNumber ?? -1));

            if (orderPrice != null)
            {
                switch (pensionModeId)
                {
                    case AnyPension: return orderPrice.AnyPensionPrice;
                    case RoomPension: return orderPrice.RoomPensionPrice;
                    case RoomBreakfastPension: return orderPrice.RoomBreakfastPensionPrice;
                    case RoomBreakfast1MealPension: return orderPrice.RoomBreakfast1MealPensionPrice;
                    case RoomBreakfast2MealsPension: return orderPrice.RoomBreakfast2MealsPensionPrice;
                    case AllInclusivePension: return orderPrice.AllInclusivePensionPrice;
                }
            }

            return null;
        }

        private static RoomChildPriceContract GetChildRoomPrice(ChildPriceByLineTypeContract childPrice,
            short pensionModeId, short? orderNumber)
        {
            switch (pensionModeId)
            {
                case RoomPension: return childPrice.RoomPrices.FirstOrDefault(x => (x.OrderNumber ?? -1) == (orderNumber ?? -1));
                case RoomBreakfastPension: return childPrice.RoomBreakfastPrices.FirstOrDefault(x => (x.OrderNumber ?? -1) == (orderNumber ?? -1));
                case RoomBreakfast1MealPension: return childPrice.RoomBreakfast1MealPrices.FirstOrDefault(x => (x.OrderNumber ?? -1) == (orderNumber ?? -1));
                case RoomBreakfast2MealsPension: return childPrice.RoomBreakfast2MealsPrices.FirstOrDefault(x => (x.OrderNumber ?? -1) == (orderNumber ?? -1));
                case AllInclusivePension: return childPrice.AllInclusivePrices.FirstOrDefault(x => (x.OrderNumber ?? -1) == (orderNumber ?? -1));
            }

            return null;
        }

        #endregion
        #region Public Methods

        public void CalculateAdultPrices(AdultLine lineType, short pensionModeId,
            short? orderNumber, decimal? price, decimal foodPrice, bool excludeExtraBeds)
        {
            var value = decimal.Zero;
            if (!excludeExtraBeds || lineType != AdultLine.ExtraAdult)
            {
                if (SupplementPercentOrValue.HasValue)
                {
                    value += SupplementPercentOrValue.Value;
                    if (!SupplementInValue)
                        value /= 100;
                }
            }

            foreach (var adultPrice in _adultPrices.Where(x => x.LineType == lineType))
            {
                var roomPrice = GetAdultRoomPrice(adultPrice, pensionModeId, orderNumber);
                if (roomPrice != null)
                    roomPrice.RoomPrice = GetRoomPrice(price, foodPrice, value);
            }
        }

        public void CalculateChildPrices(ChildLine lineType, short pensionModeId,
            short? orderNumber, decimal? price, decimal foodPrice, bool excludeChildren)
        {
            var value = decimal.Zero;
            if (!excludeChildren)
            {
                if (SupplementPercentOrValue.HasValue)
                {
                    value += SupplementPercentOrValue.Value;
                    if (!SupplementInValue)
                        value /= 100;
                }
            }

            foreach (var childPrice in _childPrices.Where(x => x.LineType == lineType))
            {
                var orderPrice = GetChildRoomPrice(childPrice, pensionModeId, orderNumber);
                if (orderPrice != null)
                    orderPrice.RoomPrice = GetRoomPrice(price, foodPrice, value);               
            }
        }

        #endregion
    }
}