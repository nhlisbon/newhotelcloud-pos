﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class PriceRateServiceSupplementContract : BaseContract
    {
        #region Members

        private bool _added;

        #endregion
        #region Properties

        [DataMember]
        public Guid ServiceId { get; set; }
        [DataMember]
        public string ServiceDescription { get; set; }
        [DataMember]
        public bool Added { get { return _added; } set { Set<bool>(ref _added, value, "Added"); } }

        #endregion
        #region Constructors

        public PriceRateServiceSupplementContract()
                : base()
        {
        }

        #endregion
    }
}

