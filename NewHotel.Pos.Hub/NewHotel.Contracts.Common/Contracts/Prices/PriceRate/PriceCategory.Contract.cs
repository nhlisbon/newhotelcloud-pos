﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PriceCategoryContract : BaseContract
    {
        #region Members

        private bool _showInPrices;
        private long? _applicationId;

        [DataMember]
        internal LanguageTranslationContract _description;

        #endregion
        #region Properties

        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }
        [DataMember]
        public long? ApplicationId { get { return _applicationId; } set { Set(ref _applicationId, value, "ApplicationId"); } }
        [DataMember]
        public bool ShowInPrices { get { return _showInPrices; } set { Set(ref _showInPrices, value, "ShowInPrices"); } }

        #endregion
        #region Constructor

        public PriceCategoryContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
    }
}