﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class DeleteRoomTypePriceContract : BaseContract
    {
        #region Members

        [DataMember]
        internal Guid _priceRateId;
		[DataMember]
		internal DateTime _workDate;
        private short? _pensionModeId;
        private Guid? _roomTypeId;
        private DateTime _fromDate;
        private DateTime? _toDate;
        private bool _includeSunday;
        private bool _includeMonday;
        private bool _includeTuesday;
        private bool _includeWednesday;
        private bool _includeThursday;
        private bool _includeFriday;
        private bool _includeSaturday;

        #endregion
        #region Properties

        public Guid PriceRateId { get { return _priceRateId; } }
		public DateTime WorkDate { get { return _workDate; } }

        [DataMember]
        [Required("RoomTypeId")]
        public Guid? RoomTypeId { get { return _roomTypeId; } set { Set(ref _roomTypeId, value, "RoomTypeId"); } }
		[DataMember]
        [Required("PensionModeId")]
        public short? PensionModeId { get { return _pensionModeId; } set { Set(ref _pensionModeId, value, "PensionModeId"); } }
        [DataMember]
        [Required("FromDate")]
        public DateTime FromDate { get { return _fromDate; } set { Set(ref _fromDate, value, "FromDate"); } }
        [DataMember]
        [Required("ToDate")]
        public DateTime? ToDate { get { return _toDate; } set { Set(ref _toDate, value, "ToDate"); } }
        [DataMember]
        public bool IncludeSunday { get { return _includeSunday; } set { Set(ref _includeSunday, value, "IncludeSunday"); } }
        [DataMember]
        public bool IncludeMonday { get { return _includeMonday; } set { Set(ref _includeMonday, value, "IncludeMonday"); } }
        [DataMember]
        public bool IncludeTuesday { get { return _includeTuesday; } set { Set(ref _includeTuesday, value, "IncludeTuesday"); } }
        [DataMember]
        public bool IncludeWednesday { get { return _includeWednesday; } set { Set(ref _includeWednesday, value, "IncludeWednesday"); } }
        [DataMember]
        public bool IncludeThursday { get { return _includeThursday; } set { Set(ref _includeThursday, value, "IncludeThursday"); } }
        [DataMember]
        public bool IncludeFriday { get { return _includeFriday; } set { Set(ref _includeFriday, value, "IncludeFriday"); } }
        [DataMember]
        public bool IncludeSaturday { get { return _includeSaturday; } set { Set(ref _includeSaturday, value, "IncludeSaturday"); } }

        #endregion
        #region Constructors

        public DeleteRoomTypePriceContract(Guid priceRateId, Guid? roomTypeId, DateTime workDate)
            : base()
        {
            _priceRateId = priceRateId;
			_roomTypeId = roomTypeId;
			_workDate = workDate;
			_fromDate = _workDate;
			_includeSunday = true;
			_includeMonday = true;
			_includeTuesday = true;
			_includeWednesday = true;
			_includeThursday = true;
			_includeFriday = true;
			_includeSaturday = true;
        }

        #endregion
    }
}
