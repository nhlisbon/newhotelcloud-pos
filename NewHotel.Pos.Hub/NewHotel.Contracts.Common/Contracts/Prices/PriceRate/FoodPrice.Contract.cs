﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace NewHotel.Contracts
{
    public class PriceChangedEventArgs
    {
        public readonly short PensionModeId;
        public readonly short? OrderNumber;
        public readonly decimal? Price;

        public PriceChangedEventArgs(short pensionModeId, short? orderNumber, decimal? price)
        {
            PensionModeId = pensionModeId;
            OrderNumber = orderNumber;
            Price = price;
        }
    }

    public delegate void PriceChangedEventHandler(object sender, PriceChangedEventArgs e);

    // For RoomBreakfast pension
    [DataContract]
	[Serializable]
    [KnownType(typeof(MealsPriceContract))]
    public class RoomBreakfastFoodPriceContract : BaseContract
    {
        #region Properties

		private decimal? _breakfastPrice;
		private decimal? _drinkPriceAtBreakfast;

		[DataMember]
		public decimal? BreakfastPrice
		{
			get { return _breakfastPrice; }
			set { Set(ref _breakfastPrice, value, "BreakfastPrice"); }
		}

        [DataMember]
        public decimal? DrinkPriceAtBreakfast
		{
			get { return _drinkPriceAtBreakfast; }
			set { Set(ref _drinkPriceAtBreakfast, value, "DrinkPriceAtBreakfast"); }
		}

        #endregion
    }

    // For all other pensions except Room
    [DataContract]
	[Serializable]
    public class MealsPriceContract : RoomBreakfastFoodPriceContract
    {
        #region Properties

		private decimal? _lunchPrice;
		private decimal? _drinkPriceAtLunch;
		private decimal? _dinnerPrice;
		private decimal? _drinkPriceAtDinner;

        [DataMember]
        public decimal? LunchPrice
		{
			get { return _lunchPrice; }
			set { Set(ref _lunchPrice, value, "LunchPrice"); }
		}

        [DataMember]
        public decimal? DrinkPriceAtLunch
		{
			get { return _drinkPriceAtLunch; }
			set { Set(ref _drinkPriceAtLunch, value, "DrinkPriceAtLunch"); }
		}

        [DataMember]
        public decimal? DinnerPrice
		{
			get { return _dinnerPrice; }
			set { Set(ref _dinnerPrice, value, "DinnerPrice"); }
		}

        [DataMember]
        public decimal? DrinkPriceAtDinner
		{
			get { return _drinkPriceAtDinner; }
			set { Set(ref _drinkPriceAtDinner, value, "DrinkPriceAtDinner"); }
		}

        #endregion
    }

    // For all orders
    [DataContract]
	[Serializable]
    public class FoodPriceContract : BaseContract
    {
        #region Members

        [DataMember]
        internal short? _orderNumber;
        private MealsPriceContract _anyPensionPrice;
        private RoomBreakfastFoodPriceContract _roomBreakfastPensionPrice;
        private MealsPriceContract _roomBreakfast1MealPensionPrice;
        private MealsPriceContract _roomBreakfast2MealsPensionPrice;
        private MealsPriceContract _allInclusivePensionPrice;

        #endregion
		#region Events

		public event PriceChangedEventHandler PriceChanged;

		#endregion
        #region Properties

        public short? OrderNumber { get { return _orderNumber; } set { _orderNumber = value; } }
        public MealsPriceContract AnyPensionPrice { get { return _anyPensionPrice; } }
        public RoomBreakfastFoodPriceContract RoomBreakfastPensionPrice { get { return _roomBreakfastPensionPrice; } }
        public MealsPriceContract RoomBreakfast1MealPensionPrice { get { return _roomBreakfast1MealPensionPrice; } }
        public MealsPriceContract RoomBreakfast2MealsPensionPrice { get { return _roomBreakfast2MealsPensionPrice; } }
        public MealsPriceContract AllInclusivePensionPrice { get { return _allInclusivePensionPrice; } }

		[DataMember]
		internal MealsPriceContract anyPensionPrice
		{
			get { return _anyPensionPrice; }
			set
			{
				_anyPensionPrice = value;
				_anyPensionPrice.PropertyChanged += new PropertyChangedEventHandler(OnPropertyChanged);
			}
		}

		[DataMember]
		internal RoomBreakfastFoodPriceContract roomBreakfastPensionPrice
		{
			get { return _roomBreakfastPensionPrice; }
			set
			{
				_roomBreakfastPensionPrice = value;
				_roomBreakfastPensionPrice.PropertyChanged += new PropertyChangedEventHandler(OnPropertyChanged);
			}
		}

		[DataMember]
		internal MealsPriceContract roomBreakfast1MealPensionPrice
		{
			get { return _roomBreakfast1MealPensionPrice; }
			set
			{
				_roomBreakfast1MealPensionPrice = value;
				_roomBreakfast1MealPensionPrice.PropertyChanged += new PropertyChangedEventHandler(OnPropertyChanged);
			}
		}

		[DataMember]
		internal MealsPriceContract roomBreakfast2MealsPensionPrice
		{
			get { return _roomBreakfast2MealsPensionPrice; }
			set
			{
				_roomBreakfast2MealsPensionPrice = value;
				_roomBreakfast2MealsPensionPrice.PropertyChanged += new PropertyChangedEventHandler(OnPropertyChanged);
			}
		}

		[DataMember]
		internal MealsPriceContract allInclusivePensionPrice
		{
			get { return _allInclusivePensionPrice; }
			set
			{
				_allInclusivePensionPrice = value;
				_allInclusivePensionPrice.PropertyChanged += new PropertyChangedEventHandler(OnPropertyChanged);
			}
		}

        #endregion
        #region Constructors

        public FoodPriceContract()
            : base()
        {
            anyPensionPrice = new MealsPriceContract();
            roomBreakfastPensionPrice = new RoomBreakfastFoodPriceContract();
            roomBreakfast1MealPensionPrice = new MealsPriceContract();
            roomBreakfast2MealsPensionPrice = new MealsPriceContract();
            allInclusivePensionPrice = new MealsPriceContract();
        }

        #endregion
		#region Private Methods

		private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (PriceChanged != null)
			{
				var roomBreakfastFoodPriceContract = sender as RoomBreakfastFoodPriceContract;
				if (roomBreakfastFoodPriceContract != null)
				{
					switch (e.PropertyName)
					{
						case "BreakfastPrice":
							PriceChanged(this, new PriceChangedEventArgs(1, _orderNumber, roomBreakfastFoodPriceContract.BreakfastPrice));
							break;
						case "DrinkPriceAtBreakfast":
							PriceChanged(this, new PriceChangedEventArgs(1, _orderNumber, roomBreakfastFoodPriceContract.DrinkPriceAtBreakfast));
							break;
					}
				}

				var mealsPriceContract = sender as MealsPriceContract;
				if (mealsPriceContract != null)
				{
					switch (e.PropertyName)
					{
						case "LunchPrice":
							PriceChanged(this, new PriceChangedEventArgs(1, _orderNumber, mealsPriceContract.LunchPrice));
							break;
						case "DrinkPriceAtLunch":
							PriceChanged(this, new PriceChangedEventArgs(1, _orderNumber, mealsPriceContract.DrinkPriceAtLunch));
							break;

						case "DinnerPrice":
							PriceChanged(this, new PriceChangedEventArgs(1, _orderNumber, mealsPriceContract.DinnerPrice));
							break;
						case "DrinkPriceAtDinner":
							PriceChanged(this, new PriceChangedEventArgs(1, _orderNumber, mealsPriceContract.DrinkPriceAtDinner));
							break;
					}
				}
			}
		}

		#endregion
    }

    // For all type line
    [DataContract]
	[Serializable]
    public class FoodPriceByLineTypeContract : BaseContract
    {
        #region Members

        [DataMember]
        internal FoodLine _lineType;
        [DataMember]
        internal string _lineTypeDesc;
        [DataMember]
        internal string _roomBreakfastPensionDesc;
        [DataMember]
        internal string _roomBreakfast1MealsPensionDesc;
        [DataMember]
        internal string _roomBreakfast2MealsPensionDesc;
        [DataMember]
        internal string _allInclusivePensionDesc;
        [DataMember]
        internal TypedList<FoodPriceContract> _prices;

        #endregion
        #region Properties

        public FoodLine LineType { get { return _lineType; } }
        public string LineTypeDesc { get { return _lineTypeDesc; } }
        public string RoomBreakfastPensionDesc { get { return _roomBreakfastPensionDesc; } }
        public string RoomBreakfast1MealsPensionDesc { get { return _roomBreakfast1MealsPensionDesc; } }
        public string RoomBreakfast2MealsPensionDesc { get { return _roomBreakfast2MealsPensionDesc; } }
        public string AllInclusivePensionDesc { get { return _allInclusivePensionDesc; } }

        #endregion
        #region Contracts

        public TypedList<FoodPriceContract> Prices { get { return _prices; } }

        #endregion
        #region Constructors

        public FoodPriceByLineTypeContract()
            : base()
        {
            TypedList<FoodPriceContract> prices = new TypedList<FoodPriceContract>();
            prices.Add(new FoodPriceContract());
            _prices = new TypedList<FoodPriceContract>(prices.ToArray());
        }

        public FoodPriceByLineTypeContract(FoodLine lineType, string lineTypeDesc,
            string roomBreakfastPensionDesc, string roomBreakfast1MealsPensionDesc,
            string roomBreakfast2MealsPensionDesc, string allInclusivePensionDesc)
            : this()
        {
            _lineType = lineType;
            _lineTypeDesc = lineTypeDesc;
            _roomBreakfastPensionDesc = roomBreakfastPensionDesc;
            _roomBreakfast1MealsPensionDesc = roomBreakfast1MealsPensionDesc;
            _roomBreakfast2MealsPensionDesc = roomBreakfast2MealsPensionDesc;
            _allInclusivePensionDesc = allInclusivePensionDesc;
        }

        #endregion
    }
}