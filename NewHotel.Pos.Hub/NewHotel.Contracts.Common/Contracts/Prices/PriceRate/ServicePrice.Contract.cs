﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ServicePriceContract), "ValidateServicePrice")]
    public class ServicePriceContract : BaseContract
    {
        #region Members
        private Guid? _departmentId;
        private Guid? _serviceByDepartmentId;
        #endregion

        #region Properties
        [DataMember]
        public DateTime InitialApplicationDate { get; set; }
        [DataMember]
        public DateTime FinalApplicationDate { get; set; }
        //[DiscreteValues("PensionModeId", new IComparable[] {1,2,3,4,5})]
        [DataMember]
        public short? PensionModeId { get; set; }
        [DataMember]
        public string PensionModeDesc { get; set; }
        [DataMember]
        [NewHotel.Contracts.Required("ServiceByDepartmentId")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Service required.")]
        public Guid? ServiceByDepartmentId { get { return _serviceByDepartmentId; } set { Set<Guid?>(ref _serviceByDepartmentId, value, "ServiceByDepartmentId"); } }
        [DataMember]
        public string ServiceDesc { get; set; }
        [DataMember]
        public string DepartmentDesc { get; set; }
        [DataMember]
        public Guid? DepartmentId { get { return _departmentId; } set { Set<Guid?>(ref _departmentId, value, "DepartmentId"); } }
        [DataMember]
        public ApplyMethod AppliedMode { get; set; }
        [DataMember]
        public string AppliedModeDesc { get; set; }
        [DataMember]
        public decimal? AdultPrice { get; set; }
        [DataMember]
        public decimal? ChildPrice { get; set; }
        [DataMember]
        public decimal? FixedPrice { get; set; }
        [DataMember]
        public ApplyMoment ApplyMoment { get; set; }
        [DataMember]
        public string ApplyMomentDesc { get; set; }
        [DataMember]
        public ApplyOperation AppliedOperation { get; set; }
        [DataMember]
        public string AppliedOperationDesc { get; set; }
        #endregion
        #region Constructors

        public ServicePriceContract()
            : base() { }

        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateServicePrice(ServicePriceContract obj)
        {
            if (obj.FinalApplicationDate < obj.InitialApplicationDate)
                return new System.ComponentModel.DataAnnotations.ValidationResult("FinalApplicationDate date must be greater than InitialApplicationDate.");
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
