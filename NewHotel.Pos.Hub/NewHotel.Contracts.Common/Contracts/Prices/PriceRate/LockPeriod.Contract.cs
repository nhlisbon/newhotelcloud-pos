﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(LockPeriodContract), "ValidateLockPeriod")]
    public class LockPeriodContract : BaseContract
    {
        #region Members
        private DateTime? _initialDate;
        private DateTime? _finalDate;
        #endregion

        #region Properties
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Initial date required.")]
        public DateTime? InitialDate { get { return _initialDate; } set { Set<DateTime?>(ref _initialDate, value, "InitialDate"); } }
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Final date required.")]
        public DateTime? FinalDate { get { return _finalDate; } set { Set<DateTime?>(ref _finalDate, value, "FinalDate"); } }
        #endregion

        #region Constructors
        public LockPeriodContract()
            : base()
        {
        }
        #endregion

        #region Validations
        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateLockPeriod(LockPeriodContract obj)
        {
            if (obj.InitialDate.HasValue && obj.FinalDate.HasValue && obj.InitialDate.Value >= obj.FinalDate.Value)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Initial date greater than final date.");
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
        #endregion
    }
}
