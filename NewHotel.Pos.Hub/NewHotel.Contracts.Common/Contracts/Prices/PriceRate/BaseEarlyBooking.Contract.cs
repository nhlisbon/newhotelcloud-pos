﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class BaseEarlyBookingContract : BaseContract
    {
        #region Members

        private EarlyBookingContract EarlyBooking { get; set; }

        #endregion
        #region Properties

        [DataMember]
        public Guid? DiscountTypeId { get; set; }
        [DataMember]
        public string DiscountTypeDesc { get; set; }
        [DataMember]
        public decimal DiscountPercent { get; set; }
        [DataMember]
        public DiscountMethod DiscountCalculateFrom { get; set; }
        [DataMember]
        public string DiscountCalculateFromDesc { get; set; }
        [DataMember]
        public DiscountMethod DiscountApplyTo { get; set; }
        [DataMember]
        public string DiscountApplyToDesc { get; set; }

        #endregion
        #region Properties Extension

        [ReflectionExclude]
        public Guid EarlyBookingId
        {
            get { return (Guid)EarlyBooking.Id; }
        }

        [ReflectionExclude]
        public DateTime InitialApplicationDate
        {
            get { return EarlyBooking.InitialApplicationDate; }
        }

        [ReflectionExclude]
        public DateTime FinalApplicationDate
        {
            get { return EarlyBooking.FinalApplicationDate; }
        }

        #endregion
        #region Constructors

        public BaseEarlyBookingContract(EarlyBookingContract earlyBookingContract)
            : base()
        {
            DiscountTypeDesc = string.Empty;
            EarlyBooking = earlyBookingContract;
        }

        #endregion
    }
}