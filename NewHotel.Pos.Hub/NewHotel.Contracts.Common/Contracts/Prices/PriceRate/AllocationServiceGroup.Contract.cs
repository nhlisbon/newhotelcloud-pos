﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    public interface IAllocationServiceGroup
    {
        decimal? GetFoodBreakfastAdultsPrice(short pensionModeId);
        decimal? GetDrinkBreakfastAdultsPrice(short pensionModeId);
        decimal? GetFoodLunchAdultsPrice(short pensionModeId);
        decimal? GetDrinkLunchAdultsPrice(short pensionModeId);
        decimal? GetFoodDinnerAdultsPrice(short pensionModeId);
        decimal? GetDrinkDinnerAdultsPrice(short pensionModeId);
        decimal? AllInclusiveAdultsPrice { get; }

        decimal? GetFoodBreakfastChildrenPrice(short pensionModeId);
        decimal? GetDrinkBreakfastChildrenPrice(short pensionModeId);
        decimal? GetFoodLunchChildrenPrice(short pensionModeId);
        decimal? GetDrinkLunchChildrenPrice(short pensionModeId);
        decimal? GetFoodDinnerChildrenPrice(short pensionModeId);
        decimal? GetDrinkDinnerChildrenPrice(short pensionModeId);
        decimal? AllInclusiveChildrenPrice { get; }

        decimal? GetFoodBreakfastPrice(short pensionModeId);
        decimal? GetDrinkBreakfastPrice(short pensionModeId);
        decimal? GetFoodLunchPrice(short pensionModeId);
        decimal? GetDrinkLunchPrice(short pensionModeId);
        decimal? GetFoodDinnerPrice(short pensionModeId);
        decimal? GetDrinkDinnerPrice(short pensionModeId);
        decimal? AllInclusivePrice { get; }
    }

    [DataContract]
	[Serializable]
    [CustomValidation(typeof(AllocationServiceGroupContract), "ValidateAllocationServiceGroupContract")]
    public class AllocationServiceGroupContract : BaseContract, IAllocationServiceGroup
    {
        #region Constants

        private const short Room = 1;
        private const short RoomBreakfast = 2;
        private const short RoomBreakfast1Meal = 3;
        private const short RoomBreakfast2Meals = 4;
        private const short AllInclusive = 5;

        #endregion
        #region Members

        [DataMember]
        internal LanguageTranslationContract _description;
        [DataMember]
        internal string[] _boards;
        
        private Guid? _roomDepartmentId;
        private Guid? _roomServiceByDepartmentId;
        
        private Guid? _extraBedDepartmentId;
        private Guid? _extraBedServiceByDepartmentId;
        
        private Guid? _breakfastDepartmentId;
        private Guid? _breakfastServiceByDepartmentId;
        private Guid? _breakfastDrinkDepartmentId;
        private Guid? _breakfastDrinkServiceByDepartmentId;
        
        private Guid? _lunchDepartmentId;
        private Guid? _lunchServiceByDepartmentId;
        private Guid? _lunchDrinkDepartmentId;
        private Guid? _lunchDrinkServiceByDepartmentId;
        
        private Guid? _dinnerDepartmentId;
        private Guid? _dinnerServiceByDepartmentId;
        private Guid? _dinnerDrinkDepartmentId;
        private Guid? _dinnerDrinkServiceByDepartmentId;

        private Guid? _telephoneDepartmentId;
        private Guid? _telephoneServiceByDepartmentId;
        private Guid? _payTvDepartmentId;
        private Guid? _payTvServiceByDepartmentId;
        private Guid? _internetDepartmentId;
        private Guid? _internetServiceByDepartmentId;
        private Guid? _allInclusiveDepartmentId;
        private Guid? _allInclusiveServiceByDepartmentId;

        private bool _inPercent;
        private Guid? _roomTypeId;

        private decimal? _foodBreakfastPrice;
        private decimal? _drinkBreakfastPrice;
        private decimal? _foodLunchPrice;
        private decimal? _drinkLunchPrice;
        private decimal? _foodDinnerPrice;
        private decimal? _drinkDinnerPrice;
        private decimal? _allInclusivePrice;

        private decimal? _foodBreakfastAdultsPrice;
        private decimal? _drinkBreakfastAdultsPrice;
        private decimal? _foodLunchAdultsPrice;
        private decimal? _drinkLunchAdultsPrice;
        private decimal? _foodDinnerAdultsPrice;
        private decimal? _drinkDinnerAdultsPrice;
        private decimal? _allInclusiveAdultsPrice;

        private decimal? _foodBreakfastChildrenPrice;
        private decimal? _drinkBreakfastChildrenPrice;
        private decimal? _foodLunchChildrenPrice;
        private decimal? _drinkLunchChildrenPrice;
        private decimal? _foodDinnerChildrenPrice;
        private decimal? _drinkDinnerChildrenPrice;
        private decimal? _allInclusiveChildrenPrice;

        #region Bed + Breakfast

        private decimal? _foodBreakfastBBPrice;
        private decimal? _drinkBreakfastBBPrice;

        private decimal? _foodBreakfastAdultsBBPrice;
        private decimal? _drinkBreakfastAdultsBBPrice;

        private decimal? _foodBreakfastChildrenBBPrice;
        private decimal? _drinkBreakfastChildrenBBPrice;

        #endregion
        #region Half Board

        private decimal? _foodBreakfastHBPrice;
        private decimal? _drinkBreakfastHBPrice;
        private decimal? _foodLunchHBPrice;
        private decimal? _drinkLunchHBPrice;
        private decimal? _foodDinnerHBPrice;
        private decimal? _drinkDinnerHBPrice;

        private decimal? _foodBreakfastAdultsHBPrice;
        private decimal? _drinkBreakfastAdultsHBPrice;
        private decimal? _foodLunchAdultsHBPrice;
        private decimal? _drinkLunchAdultsHBPrice;
        private decimal? _foodDinnerAdultsHBPrice;
        private decimal? _drinkDinnerAdultsHBPrice;

        private decimal? _foodBreakfastChildrenHBPrice;
        private decimal? _drinkBreakfastChildrenHBPrice;
        private decimal? _foodLunchChildrenHBPrice;
        private decimal? _drinkLunchChildrenHBPrice;
        private decimal? _foodDinnerChildrenHBPrice;
        private decimal? _drinkDinnerChildrenHBPrice;

        #endregion
        #region Full Board

        private decimal? _foodBreakfastFBPrice;
        private decimal? _drinkBreakfastFBPrice;
        private decimal? _foodLunchFBPrice;
        private decimal? _drinkLunchFBPrice;
        private decimal? _foodDinnerFBPrice;
        private decimal? _drinkDinnerFBPrice;

        private decimal? _foodBreakfastAdultsFBPrice;
        private decimal? _drinkBreakfastAdultsFBPrice;
        private decimal? _foodLunchAdultsFBPrice;
        private decimal? _drinkLunchAdultsFBPrice;
        private decimal? _foodDinnerAdultsFBPrice;
        private decimal? _drinkDinnerAdultsFBPrice;

        private decimal? _foodBreakfastChildrenFBPrice;
        private decimal? _drinkBreakfastChildrenFBPrice;
        private decimal? _foodLunchChildrenFBPrice;
        private decimal? _drinkLunchChildrenFBPrice;
        private decimal? _foodDinnerChildrenFBPrice;
        private decimal? _drinkDinnerChildrenFBPrice;

        #endregion
        #region All Inclusive

        private decimal? _foodBreakfastAIPrice;
        private decimal? _drinkBreakfastAIPrice;
        private decimal? _foodLunchAIPrice;
        private decimal? _drinkLunchAIPrice;
        private decimal? _foodDinnerAIPrice;
        private decimal? _drinkDinnerAIPrice;

        private decimal? _foodBreakfastAdultsAIPrice;
        private decimal? _drinkBreakfastAdultsAIPrice;
        private decimal? _foodLunchAdultsAIPrice;
        private decimal? _drinkLunchAdultsAIPrice;
        private decimal? _foodDinnerAdultsAIPrice;
        private decimal? _drinkDinnerAdultsAIPrice;

        private decimal? _foodBreakfastChildrenAIPrice;
        private decimal? _drinkBreakfastChildrenAIPrice;
        private decimal? _foodLunchChildrenAIPrice;
        private decimal? _drinkLunchChildrenAIPrice;
        private decimal? _foodDinnerChildrenAIPrice;
        private decimal? _drinkDinnerChildrenAIPrice;

        #endregion

        [DataMember]
        internal CurrencyInstallationContract _baseCurrency;
        [DataMember(Order = 0)]
        internal TypedList<AllocationRoomTypeGroupContract> _allocationRoomTypeGroups;

        #endregion
        #region Properties

        [DataMember]
        public Guid? RoomDepartmentId 
        { 
            get { return _roomDepartmentId; } 
            set { Set(ref _roomDepartmentId, value, "RoomDepartmentId"); } 
        }

        [DataMember]
        public Guid? RoomServiceByDepartmentId 
        { 
            get { return _roomServiceByDepartmentId; } 
            set { Set(ref _roomServiceByDepartmentId, value, "RoomServiceByDepartmentId"); } 
        }

        [DataMember]
        public Guid? ExtraBedDepartmentId 
        { 
            get { return _extraBedDepartmentId; } 
            set { Set(ref _extraBedDepartmentId, value, "ExtraBedDepartmentId"); } 
        }

        [DataMember]
        public Guid? ExtraBedServiceByDepartmentId 
        { 
            get { return _extraBedServiceByDepartmentId; } 
            set { Set(ref _extraBedServiceByDepartmentId, value, "ExtraBedServiceByDepartmentId"); } 
        }

        [DataMember]
        public Guid? BreakfastDepartmentId 
        { 
            get { return _breakfastDepartmentId; } 
            set { Set(ref _breakfastDepartmentId, value, "BreakfastDepartmentId"); } 
        }

        [DataMember]
        public Guid? BreakfastServiceByDepartmentId 
        { 
            get { return _breakfastServiceByDepartmentId; } 
            set { Set(ref _breakfastServiceByDepartmentId, value, "BreakfastServiceByDepartmentId"); } 
        }

        [DataMember]
        public Guid? BreakfastDrinkDepartmentId 
        { 
            get { return _breakfastDrinkDepartmentId; } 
            set { Set(ref _breakfastDrinkDepartmentId, value, "BreakfastDrinkDepartmentId"); } 
        }

        [DataMember]
        public Guid? BreakfastDrinkServiceByDepartmentId 
        { 
            get { return _breakfastDrinkServiceByDepartmentId; } 
            set { Set(ref _breakfastDrinkServiceByDepartmentId, value, "BreakfastDrinkServiceByDepartmentId"); } 
        }

        [DataMember]
        public Guid? LunchDepartmentId 
        { 
            get { return _lunchDepartmentId; } 
            set { Set(ref _lunchDepartmentId, value, "LunchDepartmentId"); } 
        }

        [DataMember]
        public Guid? LunchServiceByDepartmentId 
        { 
            get { return _lunchServiceByDepartmentId; } 
            set { Set(ref _lunchServiceByDepartmentId, value, "LunchServiceByDepartmentId"); } 
        }

        [DataMember]
        public Guid? LunchDrinkDepartmentId 
        { 
            get { return _lunchDrinkDepartmentId; } 
            set { Set(ref _lunchDrinkDepartmentId, value, "LunchDrinkDepartmentId"); } 
        }

        [DataMember]
        public Guid? LunchDrinkServiceByDepartmentId 
        {
            get { return _lunchDrinkServiceByDepartmentId; } 
            set { Set(ref _lunchDrinkServiceByDepartmentId, value, "LunchDrinkServiceByDepartmentId"); } 
        }

        [DataMember]
        public Guid? DinnerDepartmentId 
        { 
            get { return _dinnerDepartmentId; } 
            set { Set(ref _dinnerDepartmentId, value, "DinnerDepartmentId"); } 
        }

        [DataMember]
        public Guid? DinnerServiceByDepartmentId 
        { 
            get { return _dinnerServiceByDepartmentId; } 
            set { Set(ref _dinnerServiceByDepartmentId, value, "DinnerServiceByDepartmentId"); } 
        }

        [DataMember]
        public Guid? DinnerDrinkDepartmentId 
        { 
            get { return _dinnerDrinkDepartmentId; } 
            set { Set(ref _dinnerDrinkDepartmentId, value, "DinnerDrinkDepartmentId"); } 
        }

        [DataMember]
        public Guid? DinnerDrinkServiceByDepartmentId 
        { 
            get { return _dinnerDrinkServiceByDepartmentId; } 
            set { Set(ref _dinnerDrinkServiceByDepartmentId, value, "DinnerDrinkServiceByDepartmentId"); } 
        }

        [DataMember]
        public Guid? TelephoneDepartmentId
        {
            get { return _telephoneDepartmentId; }
            set { Set(ref _telephoneDepartmentId, value, "TelephoneDepartmentId"); }
        }

        [DataMember]
        public Guid? TelephoneServiceByDepartmentId
        {
            get { return _telephoneServiceByDepartmentId; }
            set { Set(ref _telephoneServiceByDepartmentId, value, "TelephoneServiceByDepartmentId"); } 
        }

        [DataMember]
        public Guid? PayTvDepartmentId
        {
            get { return _payTvDepartmentId; }
            set { Set(ref _payTvDepartmentId, value, "PayTvDepartmentId"); }
        }

        [DataMember]
        public Guid? PayTvServiceByDepartmentId
        {
            get { return _payTvServiceByDepartmentId; }
            set { Set(ref _payTvServiceByDepartmentId, value, "PayTvServiceByDepartmentId"); }
        }

        [DataMember]
        public Guid? InternetDepartmentId
        {
            get { return _internetDepartmentId; }
            set { Set(ref _internetDepartmentId, value, "InternetDepartmentId"); }
        }

        [DataMember]
        public Guid? InternetServiceByDepartmentId
        {
            get { return _internetServiceByDepartmentId; }
            set { Set(ref _internetServiceByDepartmentId, value, "InternetServiceByDepartmentId"); }
        }

        [DataMember]
        public Guid? AllInclusiveDepartmentId
        {
            get { return _allInclusiveDepartmentId; }
            set { Set(ref _allInclusiveDepartmentId, value, "AllInclusiveDepartmentId"); }
        }

        [DataMember]
        public Guid? AllInclusiveServiceByDepartmentId
        {
            get { return _allInclusiveServiceByDepartmentId; }
            set { Set(ref _allInclusiveServiceByDepartmentId, value, "AllInclusiveServiceByDepartmentId"); }
        }

        [DataMember(Order = 0)]
        public bool InPercent
        {
            get { return _inPercent; }
            set
            {
                if (Set(ref _inPercent, value, "InPercent"))
                {
                    if (_inPercent)
                    {
                        if (FoodBreakfastAdultsPrice.HasValue && FoodBreakfastAdultsPrice.Value > 100M)
                            FoodBreakfastAdultsPrice = null;
                        if (DrinkBreakfastAdultsPrice.HasValue && DrinkBreakfastAdultsPrice.Value > 100M)
                            DrinkBreakfastAdultsPrice = null;
                        if (FoodLunchAdultsPrice.HasValue && FoodLunchAdultsPrice.Value > 100M)
                            FoodLunchAdultsPrice = null;
                        if (DrinkLunchAdultsPrice.HasValue && DrinkLunchAdultsPrice.Value > 100M)
                            DrinkLunchAdultsPrice = null;
                        if (FoodDinnerAdultsPrice.HasValue && FoodDinnerAdultsPrice.Value > 100M)
                            FoodDinnerAdultsPrice = null;
                        if (DrinkDinnerAdultsPrice.HasValue && DrinkDinnerAdultsPrice.Value > 100M)
                            DrinkDinnerAdultsPrice = null;
                        if (AllInclusiveAdultsPrice.HasValue && AllInclusiveAdultsPrice.Value > 100M)
                            AllInclusiveAdultsPrice = null;

                        if (FoodBreakfastChildrenPrice.HasValue && FoodBreakfastChildrenPrice.Value > 100M)
                            FoodBreakfastChildrenPrice = null;
                        if (DrinkBreakfastChildrenPrice.HasValue && DrinkBreakfastChildrenPrice.Value > 100M)
                            DrinkBreakfastChildrenPrice = null;
                        if (FoodLunchChildrenPrice.HasValue && FoodLunchChildrenPrice.Value > 100M)
                            FoodLunchChildrenPrice = null;
                        if (DrinkLunchChildrenPrice.HasValue && DrinkLunchChildrenPrice.Value > 100M)
                            DrinkLunchChildrenPrice = null;
                        if (FoodDinnerChildrenPrice.HasValue && FoodDinnerChildrenPrice.Value > 100M)
                            FoodDinnerChildrenPrice = null;
                        if (DrinkDinnerChildrenPrice.HasValue && DrinkDinnerChildrenPrice.Value > 100M)
                            DrinkDinnerChildrenPrice = null;
                        if (AllInclusiveChildrenPrice.HasValue && AllInclusiveChildrenPrice.Value > 100M)
                            AllInclusiveChildrenPrice = null;

                        if (FoodBreakfastPrice.HasValue && FoodBreakfastPrice.Value > 100M)
                            FoodBreakfastPrice = null;
                        if (DrinkBreakfastPrice.HasValue && DrinkBreakfastPrice.Value > 100M)
                            DrinkBreakfastPrice = null;
                        if (FoodLunchPrice.HasValue && FoodLunchPrice.Value > 100M)
                            FoodLunchPrice = null;
                        if (DrinkLunchPrice.HasValue && DrinkLunchPrice.Value > 100M)
                            DrinkLunchPrice = null;
                        if (FoodDinnerPrice.HasValue && FoodDinnerPrice.Value > 100M)
                            FoodDinnerPrice = null;
                        if (DrinkDinnerPrice.HasValue && DrinkDinnerPrice.Value > 100M)
                            DrinkDinnerPrice = null;
                        if (AllInclusivePrice.HasValue && AllInclusivePrice.Value > 100M)
                            AllInclusivePrice = null;

                        if (FoodBreakfastAdultsBBPrice.HasValue && FoodBreakfastAdultsBBPrice.Value > 100M)
                            FoodBreakfastAdultsBBPrice = null;
                        if (DrinkBreakfastAdultsBBPrice.HasValue && DrinkBreakfastAdultsBBPrice.Value > 100M)
                            DrinkBreakfastAdultsBBPrice = null;

                        if (FoodBreakfastChildrenBBPrice.HasValue && FoodBreakfastChildrenBBPrice.Value > 100M)
                            FoodBreakfastChildrenBBPrice = null;
                        if (DrinkBreakfastChildrenBBPrice.HasValue && DrinkBreakfastChildrenBBPrice.Value > 100M)
                            DrinkBreakfastChildrenBBPrice = null;

                        if (FoodBreakfastBBPrice.HasValue && FoodBreakfastBBPrice.Value > 100M)
                            FoodBreakfastBBPrice = null;
                        if (DrinkBreakfastBBPrice.HasValue && DrinkBreakfastBBPrice.Value > 100M)
                            DrinkBreakfastBBPrice = null;

                        if (FoodBreakfastAdultsHBPrice.HasValue && FoodBreakfastAdultsHBPrice.Value > 100M)
                            FoodBreakfastAdultsHBPrice = null;
                        if (DrinkBreakfastAdultsHBPrice.HasValue && DrinkBreakfastAdultsHBPrice.Value > 100M)
                            DrinkBreakfastAdultsHBPrice = null;
                        if (FoodLunchAdultsHBPrice.HasValue && FoodLunchAdultsHBPrice.Value > 100M)
                            FoodLunchAdultsHBPrice = null;
                        if (DrinkLunchAdultsHBPrice.HasValue && DrinkLunchAdultsHBPrice.Value > 100M)
                            DrinkLunchAdultsHBPrice = null;
                        if (FoodDinnerAdultsHBPrice.HasValue && FoodDinnerAdultsHBPrice.Value > 100M)
                            FoodDinnerAdultsHBPrice = null;
                        if (DrinkDinnerAdultsHBPrice.HasValue && DrinkDinnerAdultsHBPrice.Value > 100M)
                            DrinkDinnerAdultsHBPrice = null;

                        if (FoodBreakfastChildrenHBPrice.HasValue && FoodBreakfastChildrenHBPrice.Value > 100M)
                            FoodBreakfastChildrenHBPrice = null;
                        if (DrinkBreakfastChildrenHBPrice.HasValue && DrinkBreakfastChildrenHBPrice.Value > 100M)
                            DrinkBreakfastChildrenHBPrice = null;
                        if (FoodLunchChildrenHBPrice.HasValue && FoodLunchChildrenHBPrice.Value > 100M)
                            FoodLunchChildrenHBPrice = null;
                        if (DrinkLunchChildrenHBPrice.HasValue && DrinkLunchChildrenHBPrice.Value > 100M)
                            DrinkLunchChildrenHBPrice = null;
                        if (FoodDinnerChildrenHBPrice.HasValue && FoodDinnerChildrenHBPrice.Value > 100M)
                            FoodDinnerChildrenHBPrice = null;
                        if (DrinkDinnerChildrenHBPrice.HasValue && DrinkDinnerChildrenHBPrice.Value > 100M)
                            DrinkDinnerChildrenHBPrice = null;

                        if (FoodBreakfastHBPrice.HasValue && FoodBreakfastHBPrice.Value > 100M)
                            FoodBreakfastHBPrice = null;
                        if (DrinkBreakfastHBPrice.HasValue && DrinkBreakfastHBPrice.Value > 100M)
                            DrinkBreakfastHBPrice = null;
                        if (FoodLunchHBPrice.HasValue && FoodLunchHBPrice.Value > 100M)
                            FoodLunchHBPrice = null;
                        if (DrinkLunchHBPrice.HasValue && DrinkLunchHBPrice.Value > 100M)
                            DrinkLunchHBPrice = null;
                        if (FoodDinnerHBPrice.HasValue && FoodDinnerHBPrice.Value > 100M)
                            FoodDinnerHBPrice = null;
                        if (DrinkDinnerHBPrice.HasValue && DrinkDinnerHBPrice.Value > 100M)
                            DrinkDinnerHBPrice = null;

                        if (FoodBreakfastAdultsFBPrice.HasValue && FoodBreakfastAdultsFBPrice.Value > 100M)
                            FoodBreakfastAdultsFBPrice = null;
                        if (DrinkBreakfastAdultsFBPrice.HasValue && DrinkBreakfastAdultsFBPrice.Value > 100M)
                            DrinkBreakfastAdultsFBPrice = null;
                        if (FoodLunchAdultsFBPrice.HasValue && FoodLunchAdultsFBPrice.Value > 100M)
                            FoodLunchAdultsFBPrice = null;
                        if (DrinkLunchAdultsFBPrice.HasValue && DrinkLunchAdultsFBPrice.Value > 100M)
                            DrinkLunchAdultsFBPrice = null;
                        if (FoodDinnerAdultsFBPrice.HasValue && FoodDinnerAdultsFBPrice.Value > 100M)
                            FoodDinnerAdultsFBPrice = null;
                        if (DrinkDinnerAdultsFBPrice.HasValue && DrinkDinnerAdultsFBPrice.Value > 100M)
                            DrinkDinnerAdultsFBPrice = null;

                        if (FoodBreakfastChildrenFBPrice.HasValue && FoodBreakfastChildrenFBPrice.Value > 100M)
                            FoodBreakfastChildrenFBPrice = null;
                        if (DrinkBreakfastChildrenFBPrice.HasValue && DrinkBreakfastChildrenFBPrice.Value > 100M)
                            DrinkBreakfastChildrenFBPrice = null;
                        if (FoodLunchChildrenFBPrice.HasValue && FoodLunchChildrenFBPrice.Value > 100M)
                            FoodLunchChildrenFBPrice = null;
                        if (DrinkLunchChildrenFBPrice.HasValue && DrinkLunchChildrenFBPrice.Value > 100M)
                            DrinkLunchChildrenFBPrice = null;
                        if (FoodDinnerChildrenFBPrice.HasValue && FoodDinnerChildrenFBPrice.Value > 100M)
                            FoodDinnerChildrenFBPrice = null;
                        if (DrinkDinnerChildrenFBPrice.HasValue && DrinkDinnerChildrenFBPrice.Value > 100M)
                            DrinkDinnerChildrenFBPrice = null;

                        if (FoodBreakfastFBPrice.HasValue && FoodBreakfastFBPrice.Value > 100M)
                            FoodBreakfastFBPrice = null;
                        if (DrinkBreakfastFBPrice.HasValue && DrinkBreakfastFBPrice.Value > 100M)
                            DrinkBreakfastFBPrice = null;
                        if (FoodLunchFBPrice.HasValue && FoodLunchFBPrice.Value > 100M)
                            FoodLunchFBPrice = null;
                        if (DrinkLunchFBPrice.HasValue && DrinkLunchFBPrice.Value > 100M)
                            DrinkLunchFBPrice = null;
                        if (FoodDinnerFBPrice.HasValue && FoodDinnerFBPrice.Value > 100M)
                            FoodDinnerFBPrice = null;
                        if (DrinkDinnerFBPrice.HasValue && DrinkDinnerFBPrice.Value > 100M)
                            DrinkDinnerFBPrice = null;

                        if (FoodBreakfastAdultsAIPrice.HasValue && FoodBreakfastAdultsAIPrice.Value > 100M)
                            FoodBreakfastAdultsAIPrice = null;
                        if (DrinkBreakfastAdultsAIPrice.HasValue && DrinkBreakfastAdultsAIPrice.Value > 100M)
                            DrinkBreakfastAdultsAIPrice = null;
                        if (FoodLunchAdultsAIPrice.HasValue && FoodLunchAdultsAIPrice.Value > 100M)
                            FoodLunchAdultsAIPrice = null;
                        if (DrinkLunchAdultsAIPrice.HasValue && DrinkLunchAdultsAIPrice.Value > 100M)
                            DrinkLunchAdultsAIPrice = null;
                        if (FoodDinnerAdultsAIPrice.HasValue && FoodDinnerAdultsAIPrice.Value > 100M)
                            FoodDinnerAdultsAIPrice = null;
                        if (DrinkDinnerAdultsAIPrice.HasValue && DrinkDinnerAdultsAIPrice.Value > 100M)
                            DrinkDinnerAdultsAIPrice = null;

                        if (FoodBreakfastChildrenAIPrice.HasValue && FoodBreakfastChildrenAIPrice.Value > 100M)
                            FoodBreakfastChildrenAIPrice = null;
                        if (DrinkBreakfastChildrenAIPrice.HasValue && DrinkBreakfastChildrenAIPrice.Value > 100M)
                            DrinkBreakfastChildrenAIPrice = null;
                        if (FoodLunchChildrenAIPrice.HasValue && FoodLunchChildrenAIPrice.Value > 100M)
                            FoodLunchChildrenAIPrice = null;
                        if (DrinkLunchChildrenAIPrice.HasValue && DrinkLunchChildrenAIPrice.Value > 100M)
                            DrinkLunchChildrenAIPrice = null;
                        if (FoodDinnerChildrenAIPrice.HasValue && FoodDinnerChildrenAIPrice.Value > 100M)
                            FoodDinnerChildrenAIPrice = null;
                        if (DrinkDinnerChildrenAIPrice.HasValue && DrinkDinnerChildrenAIPrice.Value > 100M)
                            DrinkDinnerChildrenAIPrice = null;

                        if (FoodBreakfastAIPrice.HasValue && FoodBreakfastAIPrice.Value > 100M)
                            FoodBreakfastAIPrice = null;
                        if (DrinkBreakfastAIPrice.HasValue && DrinkBreakfastAIPrice.Value > 100M)
                            DrinkBreakfastAIPrice = null;
                        if (FoodLunchAIPrice.HasValue && FoodLunchAIPrice.Value > 100M)
                            FoodLunchAIPrice = null;
                        if (DrinkLunchAIPrice.HasValue && DrinkLunchAIPrice.Value > 100M)
                            DrinkLunchAIPrice = null;
                        if (FoodDinnerAIPrice.HasValue && FoodDinnerAIPrice.Value > 100M)
                            FoodDinnerAIPrice = null;
                        if (DrinkDinnerAIPrice.HasValue && DrinkDinnerAIPrice.Value > 100M)
                            DrinkDinnerAIPrice = null;
                    }

                    NotifyPropertyChanged("PriceMaxValue");
                }
            }
        }

        [ReflectionExclude]
        public decimal? PriceMaxValue
        {
            get
            {
                if (_inPercent)
                    return 100M;

                return null;
            }
        }

        [DataMember(Order = 1)]
        public decimal? FoodBreakfastPrice
        {
            get { return _foodBreakfastPrice; }
            set { Set(ref _foodBreakfastPrice, value, "FoodBreakfastPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkBreakfastPrice
        {
            get { return _drinkBreakfastPrice; }
            set { Set(ref _drinkBreakfastPrice, value, "DrinkBreakfastPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodLunchPrice
        {
            get { return _foodLunchPrice; }
            set { Set(ref _foodLunchPrice, value, "FoodLunchPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkLunchPrice
        {
            get { return _drinkLunchPrice; }
            set { Set(ref _drinkLunchPrice, value, "DrinkLunchPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodDinnerPrice
        {
            get { return _foodDinnerPrice; }
            set { Set(ref _foodDinnerPrice, value, "FoodDinnerPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkDinnerPrice
        {
            get { return _drinkDinnerPrice; }
            set { Set(ref _drinkDinnerPrice, value, "DrinkDinnerPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? AllInclusivePrice
        {
            get { return _allInclusivePrice; }
            set { Set(ref _allInclusivePrice, value, "AllInclusivePrice"); }
        }


        [DataMember(Order = 1)]
        public decimal? FoodBreakfastAdultsPrice
        {
            get { return _foodBreakfastAdultsPrice; }
            set { Set(ref _foodBreakfastAdultsPrice, value, "FoodBreakfastAdultsPrice"); } 
        }

        [DataMember(Order = 1)]
        public decimal? DrinkBreakfastAdultsPrice
        {
            get { return _drinkBreakfastAdultsPrice; }
            set { Set(ref _drinkBreakfastAdultsPrice, value, "DrinkBreakfastAdultsPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodLunchAdultsPrice
        {
            get { return _foodLunchAdultsPrice; }
            set { Set(ref _foodLunchAdultsPrice, value, "FoodLunchAdultsPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkLunchAdultsPrice
        {
            get { return _drinkLunchAdultsPrice; }
            set { Set(ref _drinkLunchAdultsPrice, value, "DrinkLunchAdultsPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodDinnerAdultsPrice
        {
            get { return _foodDinnerAdultsPrice; }
            set { Set(ref _foodDinnerAdultsPrice, value, "FoodDinnerAdultsPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkDinnerAdultsPrice
        {
            get { return _drinkDinnerAdultsPrice; }
            set { Set(ref _drinkDinnerAdultsPrice, value, "DrinkDinnerAdultsPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? AllInclusiveAdultsPrice
        {
            get { return _allInclusiveAdultsPrice; }
            set { Set(ref _allInclusiveAdultsPrice, value, "AllInclusiveAdultsPrice"); }
        }


        [DataMember(Order = 1)]
        public decimal? FoodBreakfastChildrenPrice
        {
            get { return _foodBreakfastChildrenPrice; }
            set { Set(ref _foodBreakfastChildrenPrice, value, "FoodBreakfastChildrenPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkBreakfastChildrenPrice
        {
            get { return _drinkBreakfastChildrenPrice; }
            set { Set(ref _drinkBreakfastChildrenPrice, value, "DrinkBreakfastChildrenPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodLunchChildrenPrice
        {
            get { return _foodLunchChildrenPrice; }
            set { Set(ref _foodLunchChildrenPrice, value, "FoodLunchChildrenPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkLunchChildrenPrice
        {
            get { return _drinkLunchChildrenPrice; }
            set { Set(ref _drinkLunchChildrenPrice, value, "DrinkLunchChildrenPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodDinnerChildrenPrice
        {
            get { return _foodDinnerChildrenPrice; }
            set { Set(ref _foodDinnerChildrenPrice, value, "FoodDinnerChildrenPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkDinnerChildrenPrice
        {
            get { return _drinkDinnerChildrenPrice; }
            set { Set(ref _drinkDinnerChildrenPrice, value, "DrinkDinnerChildrenPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? AllInclusiveChildrenPrice
        {
            get { return _allInclusiveChildrenPrice; }
            set { Set(ref _allInclusiveChildrenPrice, value, "AllInclusiveChildrenPrice"); }
        }

        #region Bed and Breakfast

        public string BedBreakfastDescription
        {
            get { return _boards[0]; }
        }


        [DataMember(Order = 1)]
        public decimal? FoodBreakfastBBPrice
        {
            get { return _foodBreakfastBBPrice; }
            set { Set(ref _foodBreakfastBBPrice, value, "FoodBreakfastBBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkBreakfastBBPrice
        {
            get { return _drinkBreakfastBBPrice; }
            set { Set(ref _drinkBreakfastBBPrice, value, "DrinkBreakfastBBPrice"); }
        }


        [DataMember(Order = 1)]
        public decimal? FoodBreakfastAdultsBBPrice
        {
            get { return _foodBreakfastAdultsBBPrice; }
            set { Set(ref _foodBreakfastAdultsBBPrice, value, "FoodBreakfastAdultsBBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkBreakfastAdultsBBPrice
        {
            get { return _drinkBreakfastAdultsBBPrice; }
            set { Set(ref _drinkBreakfastAdultsBBPrice, value, "DrinkBreakfastAdultsBBPrice"); }
        }


        [DataMember(Order = 1)]
        public decimal? FoodBreakfastChildrenBBPrice
        {
            get { return _foodBreakfastChildrenBBPrice; }
            set { Set(ref _foodBreakfastChildrenBBPrice, value, "FoodBreakfastChildrenBBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkBreakfastChildrenBBPrice
        {
            get { return _drinkBreakfastChildrenBBPrice; }
            set { Set(ref _drinkBreakfastChildrenBBPrice, value, "DrinkBreakfastChildrenBBPrice"); }
        }

        #endregion
        #region Half Board

        public string HalfBoardDescription
        {
            get { return _boards[1]; }
        }


        [DataMember(Order = 1)]
        public decimal? FoodBreakfastHBPrice
        {
            get { return _foodBreakfastHBPrice; }
            set { Set(ref _foodBreakfastHBPrice, value, "FoodBreakfastHBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkBreakfastHBPrice
        {
            get { return _drinkBreakfastHBPrice; }
            set { Set(ref _drinkBreakfastHBPrice, value, "DrinkBreakfastHBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodLunchHBPrice
        {
            get { return _foodLunchHBPrice; }
            set { Set(ref _foodLunchHBPrice, value, "FoodLunchHBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkLunchHBPrice
        {
            get { return _drinkLunchHBPrice; }
            set { Set(ref _drinkLunchHBPrice, value, "DrinkLunchHBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodDinnerHBPrice
        {
            get { return _foodDinnerHBPrice; }
            set { Set(ref _foodDinnerHBPrice, value, "FoodDinnerHBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkDinnerHBPrice
        {
            get { return _drinkDinnerHBPrice; }
            set { Set(ref _drinkDinnerHBPrice, value, "DrinkDinnerHBPrice"); }
        }


        [DataMember(Order = 1)]
        public decimal? FoodBreakfastAdultsHBPrice
        {
            get { return _foodBreakfastAdultsHBPrice; }
            set { Set(ref _foodBreakfastAdultsHBPrice, value, "FoodBreakfastAdultsHBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkBreakfastAdultsHBPrice
        {
            get { return _drinkBreakfastAdultsHBPrice; }
            set { Set(ref _drinkBreakfastAdultsHBPrice, value, "DrinkBreakfastAdultsHBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodLunchAdultsHBPrice
        {
            get { return _foodLunchAdultsHBPrice; }
            set { Set(ref _foodLunchAdultsHBPrice, value, "FoodLunchAdultsHBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkLunchAdultsHBPrice
        {
            get { return _drinkLunchAdultsHBPrice; }
            set { Set(ref _drinkLunchAdultsHBPrice, value, "DrinkLunchAdultsHBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodDinnerAdultsHBPrice
        {
            get { return _foodDinnerAdultsHBPrice; }
            set { Set(ref _foodDinnerAdultsHBPrice, value, "FoodDinnerAdultsHBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkDinnerAdultsHBPrice
        {
            get { return _drinkDinnerAdultsHBPrice; }
            set { Set(ref _drinkDinnerAdultsHBPrice, value, "DrinkDinnerAdultsHBPrice"); }
        }


        [DataMember(Order = 1)]
        public decimal? FoodBreakfastChildrenHBPrice
        {
            get { return _foodBreakfastChildrenHBPrice; }
            set { Set(ref _foodBreakfastChildrenHBPrice, value, "FoodBreakfastChildrenHBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkBreakfastChildrenHBPrice
        {
            get { return _drinkBreakfastChildrenHBPrice; }
            set { Set(ref _drinkBreakfastChildrenHBPrice, value, "DrinkBreakfastChildrenHBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodLunchChildrenHBPrice
        {
            get { return _foodLunchChildrenHBPrice; }
            set { Set(ref _foodLunchChildrenHBPrice, value, "FoodLunchChildrenHBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkLunchChildrenHBPrice
        {
            get { return _drinkLunchChildrenHBPrice; }
            set { Set(ref _drinkLunchChildrenHBPrice, value, "DrinkLunchChildrenHBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodDinnerChildrenHBPrice
        {
            get { return _foodDinnerChildrenHBPrice; }
            set { Set(ref _foodDinnerChildrenHBPrice, value, "FoodDinnerChildrenHBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkDinnerChildrenHBPrice
        {
            get { return _drinkDinnerChildrenHBPrice; }
            set { Set(ref _drinkDinnerChildrenHBPrice, value, "DrinkDinnerChildrenHBPrice"); }
        }

        #endregion
        #region Full Board

        public string FullBoardDescription
        {
            get { return _boards[2]; }
        }


        [DataMember(Order = 1)]
        public decimal? FoodBreakfastFBPrice
        {
            get { return _foodBreakfastFBPrice; }
            set { Set(ref _foodBreakfastFBPrice, value, "FoodBreakfastFBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkBreakfastFBPrice
        {
            get { return _drinkBreakfastFBPrice; }
            set { Set(ref _drinkBreakfastFBPrice, value, "DrinkBreakfastFBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodLunchFBPrice
        {
            get { return _foodLunchFBPrice; }
            set { Set(ref _foodLunchFBPrice, value, "FoodLunchFBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkLunchFBPrice
        {
            get { return _drinkLunchFBPrice; }
            set { Set(ref _drinkLunchFBPrice, value, "DrinkLunchFBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodDinnerFBPrice
        {
            get { return _foodDinnerFBPrice; }
            set { Set(ref _foodDinnerFBPrice, value, "FoodDinnerFBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkDinnerFBPrice
        {
            get { return _drinkDinnerFBPrice; }
            set { Set(ref _drinkDinnerFBPrice, value, "DrinkDinnerFBPrice"); }
        }


        [DataMember(Order = 1)]
        public decimal? FoodBreakfastAdultsFBPrice
        {
            get { return _foodBreakfastAdultsFBPrice; }
            set { Set(ref _foodBreakfastAdultsFBPrice, value, "FoodBreakfastAdultsFBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkBreakfastAdultsFBPrice
        {
            get { return _drinkBreakfastAdultsFBPrice; }
            set { Set(ref _drinkBreakfastAdultsFBPrice, value, "DrinkBreakfastAdultsFBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodLunchAdultsFBPrice
        {
            get { return _foodLunchAdultsFBPrice; }
            set { Set(ref _foodLunchAdultsFBPrice, value, "FoodLunchAdultsFBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkLunchAdultsFBPrice
        {
            get { return _drinkLunchAdultsFBPrice; }
            set { Set(ref _drinkLunchAdultsFBPrice, value, "DrinkLunchAdultsFBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodDinnerAdultsFBPrice
        {
            get { return _foodDinnerAdultsFBPrice; }
            set { Set(ref _foodDinnerAdultsFBPrice, value, "FoodDinnerAdultsFBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkDinnerAdultsFBPrice
        {
            get { return _drinkDinnerAdultsFBPrice; }
            set { Set(ref _drinkDinnerAdultsFBPrice, value, "DrinkDinnerAdultsFBPrice"); }
        }


        [DataMember(Order = 1)]
        public decimal? FoodBreakfastChildrenFBPrice
        {
            get { return _foodBreakfastChildrenFBPrice; }
            set { Set(ref _foodBreakfastChildrenFBPrice, value, "FoodBreakfastChildrenFBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkBreakfastChildrenFBPrice
        {
            get { return _drinkBreakfastChildrenFBPrice; }
            set { Set(ref _drinkBreakfastChildrenFBPrice, value, "DrinkBreakfastChildrenFBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodLunchChildrenFBPrice
        {
            get { return _foodLunchChildrenFBPrice; }
            set { Set(ref _foodLunchChildrenFBPrice, value, "FoodLunchChildrenFBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkLunchChildrenFBPrice
        {
            get { return _drinkLunchChildrenFBPrice; }
            set { Set(ref _drinkLunchChildrenFBPrice, value, "DrinkLunchChildrenFBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodDinnerChildrenFBPrice
        {
            get { return _foodDinnerChildrenFBPrice; }
            set { Set(ref _foodDinnerChildrenFBPrice, value, "FoodDinnerChildrenFBPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkDinnerChildrenFBPrice
        {
            get { return _drinkDinnerChildrenFBPrice; }
            set { Set(ref _drinkDinnerChildrenFBPrice, value, "DrinkDinnerChildrenFBPrice"); }
        }

        #endregion
        #region All Inclusive

        public string AllInclusiveDescription
        {
            get { return _boards[3]; }
        }


        [DataMember(Order = 1)]
        public decimal? FoodBreakfastAIPrice
        {
            get { return _foodBreakfastAIPrice; }
            set { Set(ref _foodBreakfastAIPrice, value, "FoodBreakfastAIPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkBreakfastAIPrice
        {
            get { return _drinkBreakfastAIPrice; }
            set { Set(ref _drinkBreakfastAIPrice, value, "DrinkBreakfastAIPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodLunchAIPrice
        {
            get { return _foodLunchAIPrice; }
            set { Set(ref _foodLunchAIPrice, value, "FoodLunchAIPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkLunchAIPrice
        {
            get { return _drinkLunchAIPrice; }
            set { Set(ref _drinkLunchAIPrice, value, "DrinkLunchAIPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodDinnerAIPrice
        {
            get { return _foodDinnerAIPrice; }
            set { Set(ref _foodDinnerAIPrice, value, "FoodDinnerAIPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkDinnerAIPrice
        {
            get { return _drinkDinnerAIPrice; }
            set { Set(ref _drinkDinnerAIPrice, value, "DrinkDinnerAIPrice"); }
        }


        [DataMember(Order = 1)]
        public decimal? FoodBreakfastAdultsAIPrice
        {
            get { return _foodBreakfastAdultsAIPrice; }
            set { Set(ref _foodBreakfastAdultsAIPrice, value, "FoodBreakfastAdultsAIPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkBreakfastAdultsAIPrice
        {
            get { return _drinkBreakfastAdultsAIPrice; }
            set { Set(ref _drinkBreakfastAdultsAIPrice, value, "DrinkBreakfastAdultsAIPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodLunchAdultsAIPrice
        {
            get { return _foodLunchAdultsAIPrice; }
            set { Set(ref _foodLunchAdultsAIPrice, value, "FoodLunchAdultsAIPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkLunchAdultsAIPrice
        {
            get { return _drinkLunchAdultsAIPrice; }
            set { Set(ref _drinkLunchAdultsAIPrice, value, "DrinkLunchAdultsAIPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodDinnerAdultsAIPrice
        {
            get { return _foodDinnerAdultsAIPrice; }
            set { Set(ref _foodDinnerAdultsAIPrice, value, "FoodDinnerAdultsAIPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkDinnerAdultsAIPrice
        {
            get { return _drinkDinnerAdultsAIPrice; }
            set { Set(ref _drinkDinnerAdultsAIPrice, value, "DrinkDinnerAdultsAIPrice"); }
        }


        [DataMember(Order = 1)]
        public decimal? FoodBreakfastChildrenAIPrice
        {
            get { return _foodBreakfastChildrenAIPrice; }
            set { Set(ref _foodBreakfastChildrenAIPrice, value, "FoodBreakfastChildrenAIPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkBreakfastChildrenAIPrice
        {
            get { return _drinkBreakfastChildrenAIPrice; }
            set { Set(ref _drinkBreakfastChildrenAIPrice, value, "DrinkBreakfastChildrenAIPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodLunchChildrenAIPrice
        {
            get { return _foodLunchChildrenAIPrice; }
            set { Set(ref _foodLunchChildrenAIPrice, value, "FoodLunchChildrenAIPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkLunchChildrenAIPrice
        {
            get { return _drinkLunchChildrenAIPrice; }
            set { Set(ref _drinkLunchChildrenAIPrice, value, "DrinkLunchChildrenAIPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? FoodDinnerChildrenAIPrice
        {
            get { return _foodDinnerChildrenAIPrice; }
            set { Set(ref _foodDinnerChildrenAIPrice, value, "FoodDinnerChildrenAIPrice"); }
        }

        [DataMember(Order = 1)]
        public decimal? DrinkDinnerChildrenAIPrice
        {
            get { return _drinkDinnerChildrenAIPrice; }
            set { Set(ref _drinkDinnerChildrenAIPrice, value, "DrinkDinnerChildrenAIPrice"); }
        }

        #endregion

        [DataMember(Order = 1)]
        public Guid? RoomTypeId
        {
            get { return _roomTypeId; }
            set
            {
                if (Set(ref _roomTypeId, value, "RoomTypeId") && _allocationRoomTypeGroups != null)
                {
                    foreach (var allocationRoomTypeGroup in _allocationRoomTypeGroups)
                        allocationRoomTypeGroup.IsEditable = _roomTypeId.HasValue && !allocationRoomTypeGroup.RoomTypeId.Equals(_roomTypeId.Value);
                }
            }
        }

        #endregion
        #region Public Methods

        public decimal? GetFoodBreakfastAdultsPrice(short pensionModeId)
        {
            decimal? price = null;
            switch (pensionModeId)
            {
                case Room:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast:
                    price = FoodBreakfastAdultsBBPrice;
                    break;
                case RoomBreakfast1Meal:
                    price = FoodBreakfastAdultsHBPrice;
                    break;
                case RoomBreakfast2Meals:
                    price = FoodBreakfastAdultsFBPrice;
                    break;
                case AllInclusive:
                    price = FoodBreakfastAdultsAIPrice;
                    break;
            }

            return price ?? FoodBreakfastAdultsPrice;
        }

        public decimal? GetDrinkBreakfastAdultsPrice(short pensionModeId)
        {
            decimal? price = null;
            switch (pensionModeId)
            {
                case Room:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast:
                    price = DrinkBreakfastAdultsBBPrice;
                    break;
                case RoomBreakfast1Meal:
                    price = DrinkBreakfastAdultsHBPrice;
                    break;
                case RoomBreakfast2Meals:
                    price = DrinkBreakfastAdultsFBPrice;
                    break;
                case AllInclusive:
                    price = DrinkBreakfastAdultsAIPrice;
                    break;
            }

            return price ?? DrinkBreakfastAdultsPrice;
        }

        public decimal? GetFoodLunchAdultsPrice(short pensionModeId)
        {
            decimal? price = null;
            switch (pensionModeId)
            {
                case Room:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast1Meal:
                    price = FoodLunchAdultsHBPrice;
                    break;
                case RoomBreakfast2Meals:
                    price = FoodLunchAdultsFBPrice;
                    break;
                case AllInclusive:
                    price = FoodLunchAdultsAIPrice;
                    break;
            }

            return price ?? FoodLunchAdultsPrice;
        }

        public decimal? GetDrinkLunchAdultsPrice(short pensionModeId)
        {
            decimal? price = null;
            switch (pensionModeId)
            {
                case Room:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast1Meal:
                    price = DrinkLunchAdultsHBPrice;
                    break;
                case RoomBreakfast2Meals:
                    price = DrinkLunchAdultsFBPrice;
                    break;
                case AllInclusive:
                    price = DrinkLunchAdultsAIPrice;
                    break;
            }

            return price ?? DrinkLunchAdultsPrice;
        }

        public decimal? GetFoodDinnerAdultsPrice(short pensionModeId)
        {
            decimal? price = null;
            switch (pensionModeId)
            {
                case Room:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast1Meal:
                    price = FoodDinnerAdultsHBPrice;
                    break;
                case RoomBreakfast2Meals:
                    price = FoodDinnerAdultsFBPrice;
                    break;
                case AllInclusive:
                    price = FoodDinnerAdultsAIPrice;
                    break;
            }

            return price ?? FoodDinnerAdultsPrice;
        }

        public decimal? GetDrinkDinnerAdultsPrice(short pensionModeId)
        {
            decimal? price = null;
            switch (pensionModeId)
            {
                case Room:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast1Meal:
                    price = DrinkDinnerAdultsHBPrice;
                    break;
                case RoomBreakfast2Meals:
                    price = DrinkDinnerAdultsFBPrice;
                    break;
                case AllInclusive:
                    price = DrinkDinnerAdultsAIPrice;
                    break;
            }

            return price ?? DrinkDinnerAdultsPrice;
        }

        public decimal? GetFoodBreakfastChildrenPrice(short pensionModeId)
        {
            decimal? price = null;
            switch (pensionModeId)
            {
                case Room:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast:
                    price = FoodBreakfastChildrenBBPrice;
                    break;
                case RoomBreakfast1Meal:
                    price = FoodBreakfastChildrenHBPrice;
                    break;
                case RoomBreakfast2Meals:
                    price = FoodBreakfastChildrenFBPrice;
                    break;
                case AllInclusive:
                    price = FoodBreakfastChildrenAIPrice;
                    break;
            }

            return price ?? FoodBreakfastChildrenPrice;
        }

        public decimal? GetDrinkBreakfastChildrenPrice(short pensionModeId)
        {
            decimal? price = null;
            switch (pensionModeId)
            {
                case Room:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast:
                    price = DrinkBreakfastChildrenBBPrice;
                    break;
                case RoomBreakfast1Meal:
                    price = DrinkBreakfastChildrenHBPrice;
                    break;
                case RoomBreakfast2Meals:
                    price = DrinkBreakfastChildrenFBPrice;
                    break;
                case AllInclusive:
                    price = DrinkBreakfastChildrenAIPrice;
                    break;
            }

            return price ?? DrinkBreakfastChildrenPrice;
        }

        public decimal? GetFoodLunchChildrenPrice(short pensionModeId)
        {
            decimal? price = null;
            switch (pensionModeId)
            {
                case Room:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast1Meal:
                    price = FoodLunchChildrenHBPrice;
                    break;
                case RoomBreakfast2Meals:
                    price = FoodLunchChildrenFBPrice;
                    break;
                case AllInclusive:
                    price = FoodLunchChildrenAIPrice;
                    break;
            }

            return price ?? FoodLunchChildrenPrice;
        }

        public decimal? GetDrinkLunchChildrenPrice(short pensionModeId)
        {
            decimal? price = null;
            switch (pensionModeId)
            {
                case Room:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast1Meal:
                    price = DrinkLunchChildrenHBPrice;
                    break;
                case RoomBreakfast2Meals:
                    price = DrinkLunchChildrenFBPrice;
                    break;
                case AllInclusive:
                    price = DrinkLunchChildrenAIPrice;
                    break;
            }

            return price ?? DrinkLunchChildrenPrice;
        }

        public decimal? GetFoodDinnerChildrenPrice(short pensionModeId)
        {
            decimal? price = null;
            switch (pensionModeId)
            {
                case Room:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast1Meal:
                    price = FoodDinnerChildrenHBPrice;
                    break;
                case RoomBreakfast2Meals:
                    price = FoodDinnerChildrenFBPrice;
                    break;
                case AllInclusive:
                    price = FoodDinnerChildrenAIPrice;
                    break;
            }

            return price ?? FoodDinnerChildrenPrice;
        }

        public decimal? GetDrinkDinnerChildrenPrice(short pensionModeId)
        {
            decimal? price = null;
            switch (pensionModeId)
            {
                case Room:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast1Meal:
                    price = DrinkDinnerChildrenHBPrice;
                    break;
                case RoomBreakfast2Meals:
                    price = DrinkDinnerChildrenFBPrice;
                    break;
                case AllInclusive:
                    price = DrinkDinnerChildrenAIPrice;
                    break;
            }

            return price ?? DrinkDinnerChildrenPrice;
        }


        public decimal? GetFoodBreakfastPrice(short pensionModeId)
        {
            decimal? price = null;
            switch (pensionModeId)
            {
                case Room:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast:
                    price = FoodBreakfastBBPrice;
                    break;
                case RoomBreakfast1Meal:
                    price = FoodBreakfastHBPrice;
                    break;
                case RoomBreakfast2Meals:
                    price = FoodBreakfastFBPrice;
                    break;
                case AllInclusive:
                    price = FoodBreakfastAIPrice;
                    break;
            }

            return price ?? FoodBreakfastPrice;
        }

        public decimal? GetDrinkBreakfastPrice(short pensionModeId)
        {
            decimal? price = null;
            switch (pensionModeId)
            {
                case Room:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast:
                    price = DrinkBreakfastBBPrice;
                    break;
                case RoomBreakfast1Meal:
                    price = DrinkBreakfastHBPrice;
                    break;
                case RoomBreakfast2Meals:
                    price = DrinkBreakfastFBPrice;
                    break;
                case AllInclusive:
                    price = DrinkBreakfastAIPrice;
                    break;
            }

            return price ?? DrinkBreakfastPrice;
        }

        public decimal? GetFoodLunchPrice(short pensionModeId)
        {
            decimal? price = null;
            switch (pensionModeId)
            {
                case Room:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast1Meal:
                    price = FoodLunchHBPrice;
                    break;
                case RoomBreakfast2Meals:
                    price = FoodLunchFBPrice;
                    break;
                case AllInclusive:
                    price = FoodLunchAIPrice;
                    break;
            }

            return price ?? FoodLunchPrice;
        }

        public decimal? GetDrinkLunchPrice(short pensionModeId)
        {
            decimal? price = null;
            switch (pensionModeId)
            {
                case Room:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast1Meal:
                    price = DrinkLunchHBPrice;
                    break;
                case RoomBreakfast2Meals:
                    price = DrinkLunchFBPrice;
                    break;
                case AllInclusive:
                    price = DrinkLunchAIPrice;
                    break;
            }

            return price ?? DrinkLunchPrice;
        }

        public decimal? GetFoodDinnerPrice(short pensionModeId)
        {
            decimal? price = null;
            switch (pensionModeId)
            {
                case Room:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast1Meal:
                    price = FoodDinnerHBPrice;
                    break;
                case RoomBreakfast2Meals:
                    price = FoodDinnerFBPrice;
                    break;
                case AllInclusive:
                    price = FoodDinnerAIPrice;
                    break;
            }

            return price ?? FoodDinnerPrice;
        }

        public decimal? GetDrinkDinnerPrice(short pensionModeId)
        {
            decimal? price = null;
            switch (pensionModeId)
            {
                case Room:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast:
                    price = decimal.Zero;
                    break;
                case RoomBreakfast1Meal:
                    price = DrinkDinnerHBPrice;
                    break;
                case RoomBreakfast2Meals:
                    price = DrinkDinnerFBPrice;
                    break;
                case AllInclusive:
                    price = DrinkDinnerAIPrice;
                    break;
            }

            return price ?? DrinkDinnerPrice;
        }

        #endregion
        #region Parameters

        public CurrencyInstallationContract BaseCurrency { get { return _baseCurrency; } }
        
        #endregion
        #region Contracts

        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [ReflectionExclude]
        public TypedList<AllocationRoomTypeGroupContract> AllocationRoomTypeGroups { get { return _allocationRoomTypeGroups; } }

        #endregion
        #region Constructors

        public AllocationServiceGroupContract(CurrencyInstallationContract baseCurrency,
            string[] boards, AllocationRoomTypeGroupContract[] allocationRoomTypeGroups)
            : base() 
        {
            _description = new LanguageTranslationContract();
            _baseCurrency = baseCurrency;
            _boards = boards;
            _allocationRoomTypeGroups = new TypedList<AllocationRoomTypeGroupContract>(allocationRoomTypeGroups);
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateAllocationServiceGroupContract(AllocationServiceGroupContract obj)
        {
            if (obj.Description.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description cannot  be empty.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}