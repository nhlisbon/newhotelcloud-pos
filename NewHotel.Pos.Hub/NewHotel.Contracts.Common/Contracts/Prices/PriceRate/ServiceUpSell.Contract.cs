﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ServiceUpSellContract), "ValidateServiceUpSell")]
    public class ServiceUpSellContract : BaseContract
    {
        #region Members

        private Guid? _serviceByDepartmentId;
        private bool _perStay;
        private bool _perRoom;
        private decimal? _value;
        private decimal? _valueChild;
        private decimal? _valueInfant;
        private Blob? _image;

        #endregion
        #region Properties

        [DataMember]
        public DateTime InitialApplicationDate { get; set; }
        [DataMember]
        public DateTime FinalApplicationDate { get; set; }
        [DataMember]
        [NewHotel.Contracts.Required("ServiceByDepartmentId")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Service required.")]
        public Guid? ServiceByDepartmentId { get { return _serviceByDepartmentId; } set { Set(ref _serviceByDepartmentId, value, "ServiceByDepartmentId"); } }
        [DataMember]
        public bool Persisted { get; set; }
        [DataMember]
        public string ServiceDesc { get; set; }
        [DataMember]
        public bool VATIncluded { get; set; }
        [DataMember]
        public DailyAccountType AccountType { get; set; }
        [DataMember]
        [NewHotel.Contracts.Required("Value")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Value required.")]
        public decimal? Value { get { return _value; } set { Set(ref _value, value, "Value"); } }
        [DataMember]
        public decimal? ValueChild { get { return _valueChild; } set { Set(ref _valueChild, value, "ValueChild"); } }
        [DataMember]
        public decimal? ValueInfant { get { return _valueInfant; } set { Set(ref _valueInfant, value, "ValueInfant"); } }
        [DataMember]
        public Guid? DepartmentId { get; set; }
        [DataMember]
        public Blob? Image { get { return _image; } set { Set(ref _image, value, "Image"); } }
        [DataMember]
        public bool Selected { get; set; }
        [DataMember]
        public bool PerRoom { get { return _perRoom; } set { Set(ref _perRoom, value, "PerRoom"); } }
        [DataMember]
        public bool PerStay { get { return _perStay; }  set { Set(ref _perStay, value, "PerStay"); } }

        #endregion
        #region Constructors

        public ServiceUpSellContract()
            : base() { }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateServiceUpSell(ServiceUpSellContract obj)
        {
            if (obj.FinalApplicationDate < obj.InitialApplicationDate)
                return new System.ComponentModel.DataAnnotations.ValidationResult("FinalApplicationDate date must be greater than InitialApplicationDate.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}