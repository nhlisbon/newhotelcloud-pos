﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class CurrentAccountPrepaymentsContract : BaseContract
    {
        #region Members
        private Guid _currentAccountId;
        private short _paymentQuote;
        private DateTime _paymentDate;
        private decimal _paymentValue;
        #endregion
        #region Properties

        [GetValueExcludeProperty]
        [DataMember]
        public Guid CurrentAccountId { get { return _currentAccountId; } set { Set<Guid>(ref _currentAccountId, value, "CurrentAccountId"); } }
        [DataMember]
        public short PaymentQuote { get { return _paymentQuote; } set { Set<short>(ref _paymentQuote, value, "PaymentQuote"); } }
        [DataMember]
        public DateTime PaymentDate { get { return _paymentDate; } set { Set<DateTime>(ref _paymentDate, value, "PaymentDate"); } }
        [DataMember]
        public decimal PaymentValue { get { return _paymentValue; } set { Set<decimal>(ref _paymentValue, value, "PaymentValue"); } }

        public bool clavao { get; set; }
        #endregion
        #region Constructors

        public CurrentAccountPrepaymentsContract()
            : base()
        {
        }

        #endregion
    }
}
