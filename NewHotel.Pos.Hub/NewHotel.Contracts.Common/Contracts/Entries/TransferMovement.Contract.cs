﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TransferMovementContract : BaseContract
    {
        #region Members

        private DailyAccountType? _folder;
        private Guid? _currentAccountId;

        #endregion
        #region Persistent Properties

        [DataMember]
        public List<Guid> EntrieIds { get; set; }
        [DataMember]
        public DailyAccountType? Folder { get { return _folder; } set { Set(ref _folder, value, "Folder"); } }
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Current account required.")]
        [DataMember]
        public Guid? CurrentAccountId { get { return _currentAccountId; } set { Set(ref _currentAccountId, value, "CurrentAccountId"); } }

        #endregion
        #region Contracts

        [DataMember]
        public CancellationControlContract CancellationControl { get; internal set; }

        #endregion
        #region Constructors

        public TransferMovementContract(CancellationControlContract cancellationControl)
            : base()
        {
            EntrieIds = new List<Guid>();
            CancellationControl = cancellationControl;
        }

        #endregion
    }  
}