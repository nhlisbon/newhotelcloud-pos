﻿using System;

namespace NewHotel.Contracts
{
    public interface IMovementPrice
    {
        EntrieType Type { get; set; }
        bool IsAuto { get; set; }
        bool IsDaily { get; set; }
        bool InAdvance { get; set; }
        DateTime WorkDate { get; set; }
        short WorkShift { get; set; }
        short? PensionMode { get; set; }
        Guid? PriceRateId { get; set; }
        Guid? ServiceByDepartmentId { get; set; }
        decimal? UndiscountValue { get; set; }
        decimal? DiscountPercent { get; set; }
        decimal? DiscountValue { get; set; }
        decimal InformedValue { get; set; }
        decimal GrossValue { get; set; }
        decimal NetValue { get; set; }
        decimal CityTaxPercent { get; set; }
        decimal CityTaxValue { get; set; }
        DiscountApplyOver CityTaxAppliedOver { get; set; }
        string Currency { get; set; }
        string ForeignCurrency { get; set; }
        decimal? ForeignCurrencyValue { get; set; }
        bool ExchangeRateMult { get; set; }
        decimal? ExchangeRateValue { get; set; }
        Guid? TaxSchemaId { get; set; }
        bool? TaxIncluded { get; set; }

        decimal Discount { get; }
        short? AdultNumber { get; }
        short? ChildNumber { get; }
        Guid? GuestId { get; }

        void SetCurrentAccountId(Guid? currentAccountId);
    }
}