﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    public class BulkTransactionElementContract :BaseContract
    {

        [DataMember]
        public Guid ServiceId { get; set; }
        [DataMember]
        public string ServiceAbbreviation { get; set; }
        [DataMember]
        public string ServiceDescription { get; set; }
        [DataMember]
        public Guid DepartmentId { get; set; }
        [DataMember]
        public string DepartmentAbbreviation { get; set; }
        [DataMember]
        public string DepartmentDescription { get; set; }
        [DataMember]
        public DailyAccountType? AccountFolder { get; set; }
        [DataMember]
        public decimal? BaseCurrencyValue { get; set; }
        [DataMember]
        public string AllowedValue { get; set; }
        [DataMember]
        public int Quantity { get; set; }

        public BulkTransactionElementContract(ServiceDepartmentRecord data)
        {
            this.ServiceId = data.ServiceId;
            this.ServiceAbbreviation = data.ServiceAbbreviation;
            this.ServiceDescription = data.ServiceDescription;
            this.DepartmentId = data.DepartmentId;
            this.DepartmentAbbreviation = data.DepartmentAbbreviation;
            this.DepartmentDescription = data.DepartmentDescription;
            this.AccountFolder = data.AccountFolder;
            this.BaseCurrencyValue = data.BaseCurrencyValue;
            this.AllowedValue = data.AllowedValue;
            this.Quantity = data.Quantity;
            this.Id = data.Id;
        
        }
        public BulkTransactionElementContract() { Id = new Guid(); }
    }
}
