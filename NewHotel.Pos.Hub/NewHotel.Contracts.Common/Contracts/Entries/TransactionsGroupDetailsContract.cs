﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TransactionsGroupDetailsContract : BaseContract
    {
        #region private members

        private Guid _serviceDepartmentId;
        private string _serviceAbbreviation;
        private string _serviceDescription;
        private Guid _departmentId;
        private string _departmentAbbreviation;
        private string _departmentDescription;
        private DailyAccountType? _accountFolder;
        private decimal? _serviceValue;
        private string _allowedValue;
        private short _serviceQty;
        private Guid _serviceID;
        
        #endregion
        #region public properties

        [DataMember]
        public Guid ServiceDepartmentId { get { return _serviceDepartmentId; } set { Set(ref _serviceDepartmentId, value, "ServiceDepartmentId"); } }
        [DataMember]
        public string ServiceAbbreviation { get { return _serviceAbbreviation; } set { Set(ref _serviceAbbreviation, value, "ServiceAbbreviation"); } }
        [DataMember]
        public string ServiceDescription { get { return _serviceDescription; } set { Set(ref _serviceDescription, value, "ServiceDescription"); } }
        [DataMember]
        public Guid DepartmentId { get { return _departmentId; } set { Set(ref _departmentId, value, "DepartmentId"); } }
        [DataMember]
        public string DepartmentAbbreviation { get { return _departmentAbbreviation; } set { Set(ref _departmentAbbreviation, value, "DepartmentAbbreviation"); } }
        [DataMember]
        public string DepartmentDescription { get { return _departmentDescription; } set { Set(ref _departmentDescription, value, "DepartmentDescription"); } }
        [DataMember]
        public DailyAccountType? AccountFolder { get { return _accountFolder; } set { Set(ref _accountFolder, value, "AccountFolder"); } }
        [DataMember]
        public decimal? ServiceValue { get { return _serviceValue; } set { Set(ref _serviceValue, value, "ServiceValue"); } }
        [DataMember]
        public string AllowedValue { get { return _allowedValue; } set { Set(ref _allowedValue, value, "AllowedValue"); } }
        [DataMember]
        public short ServiceQty { get { return _serviceQty; } set { Set(ref _serviceQty, value, "ServiceQty"); } }
        [DataMember]
        public Guid ServiceID { get { return _serviceID; } set { Set(ref _serviceID, value, "ServiceID"); } }

        #endregion
        #region Constructor

        public TransactionsGroupDetailsContract(ServiceDepartmentRecord data)
            : base()
        {
            this.ServiceDepartmentId = (Guid)data.Id;
            this.ServiceAbbreviation = data.ServiceAbbreviation;
            this.ServiceDescription = data.ServiceDescription;
            this.DepartmentId = data.DepartmentId;
            this.DepartmentAbbreviation = data.DepartmentAbbreviation;
            this.DepartmentDescription = data.DepartmentDescription;
            this.AccountFolder = data.AccountFolder;
            this.ServiceValue = data.BaseCurrencyValue;
            this.AllowedValue = data.AllowedValue;
            this.ServiceQty = data.Quantity;
            this.ServiceID = data.ServiceId;
        }

        public TransactionsGroupDetailsContract() : base() { }

        #endregion
    }



}
