﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class MovementCashFlowContract : BaseContract
    {
        #region Members
        private Guid _installation;
        #endregion
        #region Persistent Properties

        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Currency { get; set; }
        [DataMember]
        public decimal Value { get; set; }
        [DataMember]
        public CashEntrieType EntrieType { get; set; }
        [DataMember]
        public DateTime WorkDate { get; set; }
        [DataMember]
        public short WorkShift { get; set; }
        [DataMember]
        public DateTime RegistrationDateTime { get; set; }

        public decimal? Debit
        {
            get { return EntrieType == CashEntrieType.Out ? new Nullable<decimal>(Value) : null; }
            set
            {
                if (value != null)
                {
                    EntrieType = CashEntrieType.Out;
                    Value = value.Value;
                }
            }
        }
        public decimal? Credit
        {
            get { return EntrieType == CashEntrieType.In ? new Nullable<decimal>(Value) : null; }
            set
            {
                if (value != null)
                {
                    EntrieType = CashEntrieType.In;
                    Value = value.Value;
                }
            }
        }
        [DataMember]
        public Guid Installation { get { return _installation; } set { Set(ref _installation, value, "Installation"); } }
        #endregion
        #region Constructors

        public MovementCashFlowContract()
            : base() { }

        #endregion
    }
}
