﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.Contracts;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class MoviResultsContract : BaseContract
    {
        #region Members

        #endregion

        #region Constructor
        public MoviResultsContract()
            : base()
        {

        }

        [ReflectionExclude]
        [DataMember]
        public TypedList<Guid> Movements { get; set; }

        #endregion



    }
}
