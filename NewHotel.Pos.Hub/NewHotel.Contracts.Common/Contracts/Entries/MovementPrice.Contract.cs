﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class MovementPriceContract : MovementBaseContract, IMovementPrice
    {
        #region Properties

        [DataMember]
        public short? PersonNumber { get; set; }
        [DataMember]
        public MovementSource MovementSource { get; set; }
        [DataMember]
        public PersonType? TypeLine { get; set; }
        [DataMember]
        public MovementType MovementType { get; set; }
        [DataMember]
        public Guid? RoomTypeId { get; set; }
        [DataMember]
        public HalfBoardEatMode? HalfPensionMode { get; set; }

        [DataMember]
        public Guid? CityTaxDepartmentId { get; set; }
        [DataMember]
        public Guid? CityTaxServiceByDepartmentId { get; set; }
        [DataMember]
        public DailyAccountType? CityTaxAccountFolder { get; set; }
        [DataMember]
        public Guid? CityTaxSchemaId { get; set; }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public bool IsBoard
        {
            get
            {
                switch (MovementType)
                {
                    case MovementType.Breakfast:
                    case MovementType.BreakfastDrink:
                    case MovementType.Lunch:
                    case MovementType.LunchDrink:
                    case MovementType.Dinner:
                    case MovementType.DinnerDrink:
                        return PensionMode.HasValue;
                }

                return false;
            }
        }

        [ReflectionExclude]
        public decimal Discount
        {
            get
            {
                if (UndiscountValue.HasValue)
                    return UndiscountValue.Value - GrossValue;

                return decimal.Zero;
            }
        }

        [ReflectionExclude]
        public decimal? ForeignCurrencyDiscount
        {
            get
            {
                if (!string.IsNullOrEmpty(ForeignCurrency))
                {
                    return ExchangeRateMult
                        ? Discount / (ExchangeRateValue ?? decimal.One)
                        : Discount * (ExchangeRateValue ?? decimal.One);
                }

                return null;
            }
        }

        #endregion
        #region Constructors

        public MovementPriceContract(CurrencyInstallationContract baseCurrency)
            : base(baseCurrency)
        {
            MovementSource = MovementSource.ManualRate;
            MovementType = MovementType.Other;
        }

        public MovementPriceContract(DateTime workDate, DateTime valueDate, Guid? departmentId, Guid? serviceByDepartmentId,
            MovementType movementType, MovementSource movementSource, DailyAccountType accountType,
            short? pensionMode, Guid? priceRate, short? quantity, string movementCurrency,
            decimal informedValue, decimal? exchangeRateValue, bool exchangeRateMult, decimal? commissionValue,
            decimal? disscountPercent, decimal? discountValue, PersonType? typeLine, Guid? roomTypeId,
            HalfBoardEatMode? halfPensionMode, CurrencyInstallationContract baseCurrency, short? personNumber = null)
            : base(workDate, valueDate, serviceByDepartmentId, accountType, pensionMode, priceRate, quantity,
            movementCurrency, exchangeRateValue, exchangeRateMult, commissionValue, disscountPercent, discountValue, baseCurrency)
        {
            Type = EntrieType.Credit;
            DepartmentId = departmentId;
            PersonNumber = personNumber;
            MovementType = movementType;
            MovementSource = movementSource;
            InformedValue = informedValue;
            TypeLine = typeLine;
            RoomTypeId = roomTypeId;
            HalfPensionMode = halfPensionMode;

            var undiscountValue = informedValue;
            var currencyId = baseCurrency.CurrencyId;
            if (movementCurrency != currencyId)
            {
                Currency = currencyId;
                ForeignCurrency = movementCurrency;
                ForeignCurrencyValue = informedValue;
                undiscountValue = exchangeRateMult
                    ? informedValue * (exchangeRateValue ?? decimal.One)
                    : informedValue / (exchangeRateValue ?? decimal.One);
            }

            UndiscountValue = undiscountValue;
            GrossValue = undiscountValue;
            NetValue = undiscountValue;
        }

        public MovementPriceContract(DateTime workDate, DateTime valueDate, Guid? departmentId, Guid? serviceByDepartmentId,
            MovementType movementType, MovementSource movementSource, DailyAccountType accountType,
            short? pensionMode, Guid? priceRate, short? quantity, string movementCurrency,
            decimal informedValue, decimal? exchangeRateValue, bool exchangeRateMult, decimal? commissionValue,
            decimal? disscountPercent, decimal? discountValue, PersonType? typeLine,
            Guid? roomTypeId, HalfBoardEatMode? halfPensionMode,
            CurrencyInstallationContract baseCurrency, bool taxIncluded, short? personNumber = null)
            : this(workDate, valueDate, departmentId, serviceByDepartmentId, movementType, movementSource,
                  accountType, pensionMode, priceRate, quantity, movementCurrency, informedValue,
                  exchangeRateValue, exchangeRateMult, commissionValue, disscountPercent, discountValue,
                  typeLine, roomTypeId, halfPensionMode, baseCurrency, personNumber)
        {
            TaxIncluded = taxIncluded;
        }

        #endregion
        #region Public Methods

        public void SetCurrentAccountId(Guid? currentAccountId)
        {
            CurrentAccountId = currentAccountId;
        }

        #endregion
    }
}