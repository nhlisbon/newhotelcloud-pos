﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TipServiceEntriesContract : BaseContract
    {
        #region Members
        private Guid _currentAccountId;
        private CurrentAccountType _currentAccountType;
        private DailyAccountType _accountFolder;
        private bool _taxIncluded;
        private Guid? _taxSchemaId;
        #endregion
        #region Constructor
        public TipServiceEntriesContract()
        {
            AccountFolder = DailyAccountType.Master;
            Services = new TypedList<TipServiceEntryContract>();
        }
        #endregion
        #region Properties
        [DataMember]
        public Guid CurrentAccountId { get { return _currentAccountId; } set { Set(ref _currentAccountId, value, "CurrentAccountId"); } }
        [DataMember]
        public CurrentAccountType CurrentAccountType { get { return _currentAccountType; } set { Set(ref _currentAccountType, value, "CurrentAccountType"); } }
        [DataMember]
        public DailyAccountType AccountFolder { get { return _accountFolder; } set { Set(ref _accountFolder, value, "AccountFolder"); } }
        [DataMember]
        public bool TaxIncluded { get { return _taxIncluded; } set { Set(ref _taxIncluded, value, "TaxIncluded"); } }
        [DataMember]
        public Guid? TaxSchemaId { get { return _taxSchemaId; } set { Set(ref _taxSchemaId, value, "TaxSchemaId"); } }
        [DataMember]
        public TypedList<TipServiceEntryContract> Services { get; set; }
        #endregion
    }

    [DataContract]
	[Serializable]
    public class TipServiceEntryContract : BaseContract
    {
        #region Members
        private bool _isSelected;
        private string _serviceDescription;
        private short _quantity;
        private decimal _price;
        private Guid _departmentId;
        #endregion
        #region Properties
        [DataMember]
        public bool IsSelected { get { return _isSelected; } set { Set(ref _isSelected, value, "IsSelected"); } }
        [DataMember]
        public string ServiceDescription { get { return _serviceDescription; } set { Set(ref _serviceDescription, value, "ServiceDescription"); } }
        [DataMember]
        public short Quantity { get { return _quantity; } set { Set(ref _quantity, value, "Quantity"); } }
        [DataMember]
        public decimal Price { get { return _price; } set { Set(ref _price, value, "Price"); } }
        [DataMember]
        public Guid DepartmentId { get { return _departmentId; } set { Set(ref _departmentId, value, "DepartmentId"); } }
        #endregion
    }
}
