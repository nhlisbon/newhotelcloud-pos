﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [KnownType(typeof(CurrentAccountReservationProperties))]
    [KnownType(typeof(CurrentAccountClientProperties))]
    [KnownType(typeof(CurrentAccountCompanyProperties))]
    [KnownType(typeof(CurrentAccountGroupProperties))]
    [KnownType(typeof(CurrentAccountGuestProperties))]
    [KnownType(typeof(CurrentAccountControlAccountProperties))]
    [KnownType(typeof(CurrentAccountProtocolAccountProperties))]
    public abstract class CurrentAccountProperties : BaseContract
    {
        [GetValueExcludeProperty]
        [DataMember]
        public Guid AccountId { get; set; }
        [DataMember]
        public CurrentAccountType AccountType { get; set; }
        [DataMember]
        public decimal MasterAmount { get; set; }
        [DataMember]
        public decimal Extra1Amount { get; set; }
        [DataMember]
        public decimal Extra2Amount { get; set; }
        [DataMember]
        public decimal Extra3Amount { get; set; }
        [DataMember]
        public decimal Extra4Amount { get; set; }
        [DataMember]
        public decimal Extra5Amount { get; set; }
        [DataMember]
        public decimal TotalAmount { get; set; }
        [DataMember]
        public decimal TotalInvoiced { get; set; }
        [DataMember]
        public decimal TotalDeposit { get; set; }
        [DataMember]
        public decimal PendingInvoices { get; set; }
        
        public bool ReservationAccountType
        {
            get { return GetType() == typeof(CurrentAccountReservationProperties); }
        }

        public CurrentAccountProperties(Guid accountId)
            : base()
        {
            AccountId = accountId;
        }
    }

    [DataContract]
	[Serializable]
    public class CurrentAccountReservationProperties : CurrentAccountProperties
    {
        [DataMember]
        public string ReservationNumber { get; set; }
        [DataMember]
        public DateTime ArrivalDate { get; set; }
        [DataMember]
        public DateTime DepartureDate { get; set; }
        [DataMember]
        public string RoomType { get; set; }
        [DataMember]
        public string Room { get; set; }
        [DataMember]
        public string Term { get; set; }
        [DataMember]
        public string Paxs { get; set; }
        [DataMember]
        public string ReservationCompany { get; set; }
        [DataMember]
        public string ReservationGroup { get; set; }
        [DataMember]
        public string ReservationHolder { get; set; }
        [DataMember]
        public string Remarks { get; set; }
        [DataMember]
        public InvoiceDestination Master { get; set; }
        [DataMember]
        public InvoiceDestination Extra1 { get; set; }
        [DataMember]
        public InvoiceDestination Extra2 { get; set; }
        [DataMember]
        public InvoiceDestination Extra3 { get; set; }
        [DataMember]
        public InvoiceDestination Extra4 { get; set; }
        [DataMember]
        public InvoiceDestination Extra5 { get; set; }

        [ReflectionExclude]
        public string AlojRoom
        {
            get
            {
                string alojroom = string.Empty;
                if (!string.IsNullOrEmpty(Room))
                    alojroom += RoomType + " - ";
                alojroom += Room;

                return alojroom;
            }
        }

        public decimal ClientTotalValue
        {
            get
            {
                return ((Master == InvoiceDestination.Client) ? MasterAmount : 0) +
                       ((Extra1 == InvoiceDestination.Client) ? Extra1Amount : 0) +
                       ((Extra2 == InvoiceDestination.Client) ? Extra2Amount : 0) +
                       ((Extra3 == InvoiceDestination.Client) ? Extra3Amount : 0) +
                       ((Extra4 == InvoiceDestination.Client) ? Extra4Amount : 0) +
                       ((Extra5 == InvoiceDestination.Client) ? Extra5Amount : 0);
            }
        }

        public decimal EntityTotalValue
        {
            get
            {
                return ((Master == InvoiceDestination.Entity) ? MasterAmount : 0) +
                       ((Extra1 == InvoiceDestination.Entity) ? Extra1Amount : 0) +
                       ((Extra2 == InvoiceDestination.Entity) ? Extra2Amount : 0) +
                       ((Extra3 == InvoiceDestination.Entity) ? Extra3Amount : 0) +
                       ((Extra4 == InvoiceDestination.Entity) ? Extra4Amount : 0) +
                       ((Extra5 == InvoiceDestination.Entity) ? Extra5Amount : 0);
            }
        }

        public decimal AgencyTotalValue
        {
            get
            {
                return ((Master == InvoiceDestination.Agency) ? MasterAmount : 0) +
                       ((Extra1 == InvoiceDestination.Agency) ? Extra1Amount : 0) +
                       ((Extra2 == InvoiceDestination.Agency) ? Extra2Amount : 0) +
                       ((Extra3 == InvoiceDestination.Agency) ? Extra3Amount : 0) +
                       ((Extra4 == InvoiceDestination.Agency) ? Extra4Amount : 0) +
                       ((Extra5 == InvoiceDestination.Agency) ? Extra5Amount : 0);
            }
        }

        public CurrentAccountReservationProperties(Guid currentAccountId)
            : base(currentAccountId) { }
    }

    [DataContract]
	[Serializable]
    public class CurrentAccountClientProperties : CurrentAccountProperties
    {
        [DataMember]
        public string ClientName { get; set; }
        [DataMember]
        public string ClientType { get; set; }

        public CurrentAccountClientProperties(Guid currentAccountId)
            : base(currentAccountId) { }
    }

    [DataContract]
	[Serializable]
    public class CurrentAccountCompanyProperties : CurrentAccountProperties
    {
        [DataMember]
        public string CompanyName { get; set; }
        [DataMember]
        public string CompanyType { get; set; }

        public CurrentAccountCompanyProperties(Guid currentAccountId)
            : base(currentAccountId) { }
    }

    [DataContract]
	[Serializable]
    public class CurrentAccountGroupProperties : CurrentAccountProperties
    {
        [DataMember]
        public string GroupName { get; set; }
        [DataMember]
        public string GroupType { get; set; }
        [DataMember]
        public short? GroupQtdRoom { get; set; }
        [DataMember]
        public DateTime? ArrivalDate { get; set; }
        [DataMember]
        public DateTime? DepartureDate { get; set; }

        public CurrentAccountGroupProperties(Guid currentAccountId)
            : base(currentAccountId) { }
    }

    [DataContract]
	[Serializable]
    public class CurrentAccountGuestProperties : CurrentAccountProperties
    {
        [DataMember]
        public string GuestName { get; set; }
        [DataMember]
        public string GuestType { get; set; }
        [DataMember]
        public string GuestReservation { get; set; }

        public CurrentAccountGuestProperties(Guid currentAccountId)
            : base(currentAccountId) { }
    }

    [DataContract]
	[Serializable]
    public class CurrentAccountControlAccountProperties : CurrentAccountProperties
    {
        [DataMember]
        public string AccountName { get; set; }

        public CurrentAccountControlAccountProperties(Guid currentAccountId)
            : base(currentAccountId) { }    
    }

    [DataContract]
    [Serializable]
    public class CurrentAccountProtocolAccountProperties : CurrentAccountProperties
    {
        [DataMember]
        public string AccountName { get; set; }

        public CurrentAccountProtocolAccountProperties(Guid currentAccountId)
            : base(currentAccountId) { }
    }

    [DataContract]
	[Serializable]
    public class CurrentAccountContract : BaseContract
    {
        #region Members

        private string _accountDescription;
        [DataMember]
        internal TypedList<CurrentAccountLimitContract> _limits;
        [DataMember]
        internal TypedList<CurrentAccountTransferInstructionContract> _transferInstructionsToThisAccount;
        [DataMember]
        internal TypedList<CurrentAccountTransferInstructionContract> _transferInstructions;
        [DataMember]
        internal TypedList<CurrentAccountPrepaymentsContract> _prepayments;
        [DataMember]
        internal DateTime _workDate;

        private CurrentAccountType _currentAccountType;
        private bool _isLocked;
        private decimal _prepaymentQuoteValue;
        private decimal? _foreignExchangeRate;
        private string _foreignExchangeRateCurrency;
        private Guid? _supplementSchemaId;

        #endregion
        #region Properties

        [DataMember]
        public string AccountDescription
        {
            get { return _accountDescription; } 
            internal set { _accountDescription = value; }  
        }

        [DataMember]
        public CurrentAccountType CurrentAccountType 
        { 
            get { return _currentAccountType; } 
            set { Set<CurrentAccountType>(ref _currentAccountType, value, "CurrentAccountType"); } 
        }
        [DataMember]
        public bool PmsReservationAccount { get; set; }

        [DataMember]
        public string CurrentAccountDescription { get; set; }
        [DataMember]
        public bool IsLocked 
        { 
            get { return _isLocked; } 
            set { Set<bool>(ref _isLocked, value, "IsLocked"); NotifyPropertyChanged("IsUnlocked"); } 
        }

        public bool IsUnlocked { get { return !_isLocked; } }

        [DataMember]
        public DateTime? DateClosedAccount { get; set; }

        [DataMember]
        public decimal? ForeignExchangeRate { get { return _foreignExchangeRate; } set { Set(ref _foreignExchangeRate, value, "ForeignExchangeRate"); } }
        [DataMember]
        public string ForeignExchangeRateCurrency { get { return _foreignExchangeRateCurrency; } set { Set(ref _foreignExchangeRateCurrency, value, "ForeignExchangeRateCurrency"); } }
        [DataMember]
        public decimal PrepaymentQuoteValue { get { return _prepaymentQuoteValue; } set { Set(ref _prepaymentQuoteValue, value, "PrepaymentQuoteValue"); } }

        [DataMember]
        public short MaxQuotasAllowed { get; set; }

        public bool QuotasEnabled { get { return MaxQuotasAllowed > 1; } }

        [DataMember]
        public Guid? SupplementSchemaId { get { return _supplementSchemaId; } set { Set<Guid?>(ref _supplementSchemaId, value, "SupplementSchemaId"); } }
        [DataMember]
        public DateTime? SupplementSchemaDate { get; set; }

        #endregion
        #region Contracts

        [DataMember]
        public CreditCardContract PaymentCreditCard { get; set; }

        public TypedList<CurrentAccountLimitContract> Limits { get { return _limits; } }

        [ReflectionExclude]
        public TypedList<CurrentAccountTransferInstructionContract> TransferInstructions { get { return _transferInstructions; }  }

        [ReflectionExclude]
        public TypedList<CurrentAccountTransferInstructionContract> TransferInstructionsToThisAccount { get { return _transferInstructionsToThisAccount; } }

        [ReflectionExclude]
        public TypedList<CurrentAccountPrepaymentsContract> Prepayments { get { return _prepayments; } }

        #endregion
        #region Parameters

        public DateTime WorkDate { get { return _workDate; } }

        #endregion
        #region Constructors

        public CurrentAccountContract(CurrentAccountLimitContract[] limits, 
            CurrentAccountTransferInstructionContract[] instructions, CurrentAccountPrepaymentsContract[] prepayments, string accountDescription, DateTime workDate)
            : base()
        {
            _accountDescription = accountDescription;
            _limits = new TypedList<CurrentAccountLimitContract>(limits);
            _transferInstructionsToThisAccount = new TypedList<CurrentAccountTransferInstructionContract>(instructions);
            _workDate = workDate;
            _transferInstructions = new TypedList<CurrentAccountTransferInstructionContract>();
            _prepayments = new TypedList<CurrentAccountPrepaymentsContract>(prepayments);
        }

        public CurrentAccountContract(CurrentAccountLimitContract[] limits, CurrentAccountPrepaymentsContract[] prepayments, string accountDescription, DateTime workDate)
            : this(limits, new CurrentAccountTransferInstructionContract[0], prepayments, accountDescription, workDate)
        {
        }

        #endregion
    }
}