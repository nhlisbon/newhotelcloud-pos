﻿using System;

namespace NewHotel.Contracts
{
    public class PostMovementContract : BaseContract
    {
        #region Properties

        public DailyAccountType? AccountFolder { get;  set; }
        public DateTime? InitialDate { get;  set; }
        public DateTime? FinalDate { get;  set; }
        public string DocNumber { get; set; }
        public short? PensionModeId { get; set; }
        public decimal? MovementValue { get; set; }
        public Guid? PriceRateId { get; set; }

        #endregion
 
        #region Constructors

        public PostMovementContract() 
            : base() { }

        public PostMovementContract(DailyAccountType? accountFolder, DateTime? initialDate, DateTime? finalDate, string docNumber, short? pensionModeId, decimal? movementValue, Guid? priceRateId)
            : base() 
        {
            AccountFolder = accountFolder;
            InitialDate = initialDate;
            FinalDate = finalDate;
            DocNumber = docNumber;
            PensionModeId = pensionModeId;
            MovementValue = movementValue;
            PriceRateId = priceRateId;
        }
        #endregion                 
    }
}
