﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(MovementContract), "ValidateMovement")]
    public class MovementContract : MovementBaseContract
    {
        #region Members

        private decimal? _movementValue;
        private decimal? _movementBaseValue;
        private string _movementCurrency;
        private bool _applyGuestTax;
        private bool _guestTaxIncluded;
        private decimal? _movementMinValue;

        #endregion
        #region Constructors

        public MovementContract(CancellationControlContract cancellationControl, CurrencyInstallationContract baseCurrency)
            : base(baseCurrency)
        {
            CancellationControl = cancellationControl;
            GreatherThanZero = true;
            EqualZero = true;
            LessThanZero = true;           
        }

        public MovementContract(CurrencyInstallationContract baseCurrency)
            : this(null, baseCurrency) { GreatherThanZero = true; EqualZero = true; LessThanZero = true; }

        #endregion
        #region Persistent Properties

        [DataMember]
        public bool AvoidRecalculation { get; set; }
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Currency required.")]
        public string MovementCurrency
        {
            get { return _movementCurrency; }
            set { Set(ref _movementCurrency, value, "MovementCurrency"); }
        }
        [DataMember]
        public decimal? MovementValue
        {
            get { return _movementValue; }
            set { Set(ref _movementValue, value, "MovementValue"); }
        }
        [DataMember]
        public decimal? MovementBaseValue
        {
            get { return _movementBaseValue; }
            set { Set(ref _movementBaseValue, value, "MovementBaseValue"); }
        }
        [DataMember]
        public Guid? UserId { get; set; }
        [DataMember]
        public string UserLogin { get; set; }

        #endregion

        [DataMember]
        public bool LessThanZero { get; set; }
        [DataMember]
        public bool GreatherThanZero { get; set; }
        [DataMember]
        public bool EqualZero { get; set; }
        [ReflectionExclude]
        [DataMember]
        public CancellationControlContract CancellationControl { get; set; }
        [DataMember]
        public Guid CurrentAccountOriginId { get; set; }
        [DataMember]
        public string TransferedDescription { get; set; }
        [DataMember]
        public bool ForceInsert { get; set; }
        [DataMember]
        public PaymentServiceProvider PaymentProvider { get; set; }
        [DataMember]
        public Guid? PaymentMethodDefaultId { get; set; }
		[DataMember]
		public bool InAdvanceInvoice { get; set; }
        [DataMember]
        public Guid? RefundReferenceId { get; set; }
        [DataMember]
        public decimal? MovementMinValue
        {
            get { return _movementMinValue; }
            set { Set(ref _movementMinValue, value, "MovementMinValue"); }
        }
        [DataMember]
        public decimal? MovementMaxValue { get; set; }
        [DataMember]
        public Guid? AuthorizationId { get; set; }
        [DataMember]
        public bool ApplyGuestTax
        {
            get { return _applyGuestTax; }
            set { Set(ref _applyGuestTax, value, "ApplyGuestTax"); }
        }
        [DataMember]
        public bool ApplyGuestTaxEnabled { get; set; }
        [DataMember]
        public bool GuestTaxIncluded
        {
            get { return _guestTaxIncluded; }
            set { Set(ref _guestTaxIncluded, value, "GuestTaxIncluded"); }
        }
        [DataMember]
        public Guid? BankId { get; set; }
        [DataMember]
        public Guid? PaymentTransaction { get; set; }
        [DataMember]
        public decimal? CreditCardCommission { get; set; }
        [DataMember]
        public DateTime? RegularizedOn { get; set; }
        [DataMember]
        public Guid? RegularizedBy { get; set; }
        [DataMember]
        public string RegularizationReference { get; set; }
        [DataMember]
        public decimal? ChargebackValue { get; set; }

        [ReflectionExclude]
        public bool IsCreditCardManualPayment
        {
            get { return CreditCard != null && PaymentProvider == PaymentServiceProvider.Manual; }
        }

        [ReflectionExclude]
        public Guid ServiceId { get; set; }

        [ReflectionExclude]
        public string ServiceDesc { get; set; }

        [ReflectionExclude]
        public bool ShowGuestTaxIncluded
        {
            get { return Type != EntrieType.Debit && ApplyGuestTax; }
        }

        [ReflectionExclude]
        public bool IsTransfered
        {
            get { return (CurrentAccountId.HasValue && !Cancelled && !CurrentAccountId.Value.Equals(CurrentAccountOriginId)); }
        }

        [ReflectionExclude]
        public bool CloseWindow
        {
            get { return RefundReferenceId.HasValue; }
        }

        [ReflectionExclude]
        public string CreditCardUniqueId
        {
            get
            {
                if (CreditCard != null && PaymentProvider != PaymentServiceProvider.Manual)
                    return PaymentProvider.ToString();

                return string.Empty;
            }
        } 

        [DataMember]
        public List<MovementTicketProductContract> TicketProducts { get; set; }
        [DataMember]
        public List<MovementTicketPaymentContract> TicketPayments { get; set; }

        public bool TicketInformation
        {
            get
            {
                if (TicketPayments != null && TicketPayments.Count > 0)
                    return true;
                if (TicketProducts != null && TicketProducts.Count > 0)
                    return true;

                return false;
            }
        } 

        #region Protected Methods

        protected override void OnNotifyPropertyChanged(HashSet<string> propertyNames)
        {
            if (propertyNames.Intersect(new string[] { "Type", "ApplyGuestTax" }).Any())
                propertyNames.Add("ShowGuestTaxIncluded");

            base.OnNotifyPropertyChanged(propertyNames);
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateMovement(MovementContract obj)
        {
            //if(obj.MovementCurrency != obj.BaseCurrency.CurrencyId && !obj.ExchangeRateValue.HasValue)
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Foreing Currency Exchange Missing");

            if (obj.MovementValue.HasValue && obj.MovementValue.Value > 0 && !obj.GreatherThanZero) return new System.ComponentModel.DataAnnotations.ValidationResult("Value not allowed");
            if (obj.MovementValue.HasValue && obj.MovementValue.Value < 0 && !obj.LessThanZero) return new System.ComponentModel.DataAnnotations.ValidationResult("Value not allowed");
            if (obj.MovementValue.HasValue && obj.MovementValue.Value == 0 && !obj.EqualZero) return new System.ComponentModel.DataAnnotations.ValidationResult("Value not allowed");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ReservationDepositContract), "ValidateMovementReservationContract")]
    public class ReservationDepositContract : MovementContract
    {
        private bool _selectedAdvancedDeposit;
        private bool _selectedGroup;
        private bool _selectedReservations;
        private bool _isGroupAdvanced;

        public ReservationDepositContract(CurrencyInstallationContract baseCurrency)
            : base(baseCurrency)
        {
            AccountFolder = DailyAccountType.Master;
            Type = EntrieType.Debit;
            Receivable = ReceivableType.Payment;
            PaymentProvider = PaymentServiceProvider.Manual;
            MovementValue = decimal.Zero;
        }

        [ReflectionExclude]
        [DataMember]
        public ReservationHolderInfoContract ReservationHolderInfoContract { get; set; }

        [DataMember]
        public bool SelectedAdvancedDeposit { get { return _selectedAdvancedDeposit; } set { Set(ref _selectedAdvancedDeposit, value, "SelectedAdvancedDeposit"); } }
        [DataMember]
        public bool SelectedGroup { get { return _selectedGroup; } set { Set(ref _selectedGroup, value, "SelectedGroup"); } }
        [DataMember]
        public bool SelectedReservations { get { return _selectedReservations; } set { Set(ref _selectedReservations, value, "SelectedReservations"); } }
        [DataMember]
        public bool IsGroupAdvanced { get { return _isGroupAdvanced; } set { Set(ref _isGroupAdvanced, value, "IsGroupAdvanced"); } }

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateMovementReservationContract(ReservationDepositContract obj)
        {
            if (obj.SelectedAdvancedDeposit &&  !obj.SelectedGroup && !obj.SelectedReservations)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Select advanced deposit type: Group or n-Reservations");
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }

    [DataContract]
    [Serializable]
    public class MovementTicketProductContract : BaseContract
    {
        [DataMember]
        public Guid TicketId { get; set; }
        [DataMember]
        public Guid ProductLineId { get; set; }
        [DataMember]
        public Guid MovementId { get; set; }
        [DataMember]
        public string TicketNumber { get; set; }
        [DataMember]
        public string TicketSerie { get; set; }
        [DataMember]
        public string ProductDescription { get; set; }
        [DataMember]
        public decimal ProductLineValue { get; set; }
        [DataMember]
        public decimal ProductLineQty { get; set; }
    }

    [DataContract]
    [Serializable]
    public class MovementTicketPaymentContract : BaseContract
    {
        [DataMember]
        public Guid TicketId { get; set; }
        [DataMember]
        public Guid PaymentLineId { get; set; }
        [DataMember]
        public Guid MovementId { get; set; }
        [DataMember]
        public string TicketNumber { get; set; }
        [DataMember]
        public string TicketSerie { get; set; }
        [DataMember]
        public string PaymentDescription { get; set; }
        [DataMember]
        public decimal PaymentLineValue { get; set; }
    }
}
