﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class EntriesPreviewContract : BaseContract
    {
        #region Members

        private Guid _taxSchemaId;
        private string _taxSchemaDescription;
        private QueryRequest _request;

        #endregion
        #region Properties

        [DataMember]
        public Guid TaxSchemaId
        {
            get { return _taxSchemaId; }
            set { Set(ref _taxSchemaId, value, "TaxSchemaId"); }
        }

        [DataMember]
        public string TaxSchemaDescription
        {
            get { return _taxSchemaDescription; }
            set { Set(ref _taxSchemaDescription, value, "TaxSchemaDescription"); }
        }

        [DataMember]
        public QueryRequest Request
        {
            get { return _request; }
            set { _request = value; }
        }

        #endregion
    }
}
