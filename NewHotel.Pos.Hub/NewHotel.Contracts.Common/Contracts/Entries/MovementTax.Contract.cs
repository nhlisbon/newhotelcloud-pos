﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class MovementTaxDetailContract : BaseContract
    {
        #region Members

        [DataMember]
        internal decimal _taxValue;
        [DataMember]
        internal decimal _retentionPercent;

        #endregion
        #region Properties

        [DataMember]
        public Guid? TaxRateId { get; set; }
        [DataMember]
        public string TaxRateDescription { get; set; }
        [DataMember]
        public decimal Percent { get; set; }
        [DataMember]
        public string TaxAuxCode { get; set; }
        [DataMember]
        public short TaxLevel { get; set; }
        [DataMember]
        public decimal TaxBase { get; set; }
        [DataMember]
        public decimal TaxIncidence { get; set; }
        [DataMember]
        public decimal RetentionValue { get; set; }

        public decimal RetentionPercent
        {
            get { return _retentionPercent; }
            set
            {
                _retentionPercent = value;
                RetentionValue = _taxValue * _retentionPercent / 100;
            }
        }

        public decimal TaxValue
        {
            get { return _taxValue; }
            set
            {
                _taxValue = value;
                RetentionValue = _taxValue * _retentionPercent / 100;
            }
        }
        

        #endregion
        #region Contructor

        public MovementTaxDetailContract()
            : base() { }

        public MovementTaxDetailContract(Guid taxRateId, decimal percent, short taxLevel, decimal retentionPercent = decimal.Zero)
            : this()
        {
            TaxRateId = taxRateId;
            Percent = percent;
            TaxLevel = taxLevel;
            RetentionPercent = retentionPercent;
        }

        #endregion
    }
}