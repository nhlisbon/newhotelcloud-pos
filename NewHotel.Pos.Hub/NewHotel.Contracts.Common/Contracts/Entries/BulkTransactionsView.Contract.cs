﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class BulkTransactionsViewContract : BaseContract
    {
        #region Private Members
        private TransactionsGroupContract _group;
        private TypedList<TransactionAccountsContract> _currentAccounts;
        private string _description;
        #endregion
        #region Constructor
        public BulkTransactionsViewContract()
            : base()
        {
            _group = new TransactionsGroupContract();
            CurrentAccounts = new TypedList<TransactionAccountsContract>();
        }
        #endregion
        #region Public Properties
        [DataMember]
        [ReflectionExclude]
        public TransactionsGroupContract Group
        {
            get
            {
                return _group;
            }
            set
            {
                if (value != null)
                {
                    this.Description = value.Description;
                    this.Id = value.Id;
                }
                _group = value;
            }
        }
        [DataMember]
        [ReflectionExclude]
        public TypedList<TransactionsGroupDetailsContract> Services 
        {
            get
            {
                return _group.Services;
            }
            set
            {
                if (_group == null)
                    _group = new TransactionsGroupContract();
                _group.Services = value;
 
            }
        }
        [DataMember]
        [ReflectionExclude]
        public TypedList<TransactionAccountsContract> CurrentAccounts 
        {
            get 
            {
                return _currentAccounts;
            }
            set
            {
                _currentAccounts = value;
            }
        }
        [DataMember]
        [ReflectionExclude]
        public string Description 
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }
        #endregion
        #region Data Operations
        public void InsertServiceDepartmentRecord(ServiceDepartmentRecord data) 
        {
            _group.InsertServiceDepartmentRecord(data);
        }
        public void InsertOccupationLineRecord(OccupationLineRecord data) 
        {
            CurrentAccounts.Add(new TransactionAccountsContract(data));
        }
        #endregion
    }
}
