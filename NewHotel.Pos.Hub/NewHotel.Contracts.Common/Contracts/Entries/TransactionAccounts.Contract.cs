﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TransactionAccountsContract : BaseContract
    {
        #region private Members
        private bool _ok;
        private string _comments;
        private string _reservation;
        private string _guest;
        private string _countryCode;
        private string _arrival;
        private string _departure;
        private string _stateDescription;
        private Guid _accountId;
        #endregion
        #region public properties
        [DataMember]
        [ReflectionExclude]
        public bool Ok { get { return _ok; } set { Set (ref _ok, value, "Ok"); } }
        [DataMember]
        [ReflectionExclude]
        public string Comments { get { return _comments; } set { Set(ref _comments, value, "Comments"); } }
        [DataMember]
        [ReflectionExclude]
        public string Reservation { get { return _reservation; } set { Set(ref _reservation, value, "Reservation"); } }
        [DataMember]
        [ReflectionExclude]
        public string Guest { get { return _guest; } set { Set(ref _guest, value, "Guest"); } }
        [DataMember]
        [ReflectionExclude]
        public string CountryCode { get { return _countryCode; } set { Set(ref _countryCode, value, "CountryCode"); } }
        [DataMember]
        [ReflectionExclude]
        public string Arrival { get { return _arrival; } set { Set(ref _arrival, value, "Arrival"); } }
        [DataMember]
        [ReflectionExclude]
        public string Departure { get { return _departure; } set { Set(ref _departure, value, "Departure"); } }
        [DataMember]
        [ReflectionExclude]
        public string StateDescription { get { return _stateDescription; } set { Set(ref _stateDescription, value, "StateDescription"); } }
        [DataMember]
        [ReflectionExclude]
        public Guid AccountId { get { return _accountId; } set { Set(ref _accountId, value, "AccountId"); } }
        #endregion
        #region Constructor
        public TransactionAccountsContract(OccupationLineRecord data)
            : base()
        {
            this.Id = data.Id;
            this.Reservation = data.Reservation;
            this.Guest = data.Guest;
            this.CountryCode = data.CountryCode;
            this.Arrival = data.Arrival;
            this.Departure = data.Departure;
            this.StateDescription = data.StateDescription;
            this.AccountId = (data.AccountId.HasValue) ? data.AccountId.Value : Guid.NewGuid();
        }
        public TransactionAccountsContract()
            : base()
        { }
        #endregion

    }
}
