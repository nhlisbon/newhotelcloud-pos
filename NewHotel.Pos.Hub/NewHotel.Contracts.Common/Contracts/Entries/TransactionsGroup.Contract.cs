﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TransactionsGroupContract : BaseContract
    {

        #region Constructor
        public TransactionsGroupContract()
            : base()
        {
            Services = new TypedList<TransactionsGroupDetailsContract>();
        }
        #endregion
        #region private members
        private TypedList<TransactionsGroupDetailsContract> _services;
        private string _description;

        #endregion
        #region Public Properties
        [DataMember]
        [ReflectionExclude]
        public TypedList<TransactionsGroupDetailsContract> Services 
        {
            get
            {
                return _services;
            }
            set 
            {
                _services = value;
            }
        }
        [DataMember]
        [ReflectionExclude]
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }
        #endregion
        #region dataOperations
        public void InsertServiceDepartmentRecord(ServiceDepartmentRecord data) 
        {
            _services.Add(new TransactionsGroupDetailsContract(data));
        }
        #endregion
    }
}
