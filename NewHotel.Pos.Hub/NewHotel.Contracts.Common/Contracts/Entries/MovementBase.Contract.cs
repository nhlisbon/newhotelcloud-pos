﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class MovementBaseContract : BaseContract
    {
        #region Members

        private Guid? _departmentId;
        private Guid? _serviceByDepartmentId;
        private ReceivableType? _receivableType;
        private short? _quantity;
        private string _currency;
        private DailyAccountType _accountFolder;
        private Guid? _currentAccountId;
        private string _currentAccountDescription;
        private CurrentAccountType? _currentAccountType;
        private decimal? _discountPercent;
        private decimal? _discountValue;
        private Guid? _withdrawType;
        private Guid? _entityId;
        private EntrieType _type;
        private SpecialLineType? _lineType;
        private Guid? _taxSchemaId;
        private bool? _taxIncluded;
        private CreditCardContract _creditCard;
        private CreditCardContract _paymentCreditCard;
        private CreditCardContract _refundCreditCard;
        private bool _cancellationFee;
        private Guid? _guestId;

        [DataMember]
        internal CurrencyInstallationContract _baseCurrency;

        #endregion
        #region Persistent Properties

        [DataMember]
        public DateTime RegistrationDateTime { get; set; }
        [DataMember]
        public DateTime WorkDate { get; set; }
        [DataMember]
        public short WorkShift { get; set; }
        [DataMember]
        public short? PosWorkShift { get; set; }
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Value date required.")]
        public DateTime ValueDate { get; set; }
        [DataMember]
        public EntrieType Type
        {
            get { return _type; }
            set { Set(ref _type, value, "Type"); }
        }
        [DataMember]
        public SpecialLineType? LineType
        {
            get { return _lineType; }
            set { Set(ref _lineType, value, "LineType"); }
        }
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Department required.")]
        public Guid? DepartmentId
        {
            get { return _departmentId; }
            set { Set(ref _departmentId, value, "DepartmentId"); }
        }
        [DataMember]
        public Guid? ServiceByDepartmentId
        {
            get { return _serviceByDepartmentId; }
            set { Set(ref _serviceByDepartmentId, value, "ServiceByDepartmentId"); }
        }
        [DataMember]
        public short? Quantity
        {
            get { return _quantity; }
            set { Set(ref _quantity, value, "Quantity"); }
        }
        [DataMember]
        public string Currency
        {
            get { return _currency; }
            set { Set(ref _currency, value, "Currency"); }
        }
        [DataMember]
        public decimal InformedValue { get; set; }
        [DataMember]
        public decimal NetValue { get; set; }
        [DataMember]
        public decimal GrossValue { get; set; }
        [DataMember]
        public decimal? ForeignCurrencyValue { get; set; }
        [DataMember]
        public decimal? ExchangeRateValue { get; set; }
        [DataMember]
        public bool ExchangeRateMult { get; set; }
        [DataMember]
        public string ForeignCurrency { get; set; }
        [DataMember]
        public decimal? UndiscountValue { get; set; }
        [DataMember]
        public decimal IrsRetentionPercent { get; set; }
        [DataMember]
        public decimal IrsRetentionValue { get; set; }
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Current account required.")]
        public Guid? CurrentAccountId
        {
            get { return _currentAccountId; }
            set { Set(ref _currentAccountId, value, "CurrentAccountId"); }
        }
        [DataMember]
        public CurrentAccountType? CurrentAccountType
        {
            get { return _currentAccountType; }
            set { Set(ref _currentAccountType, value, "CurrentAccountType"); }
        }
        [DataMember]
        public string CurrentAccountDescription
        {
            get { return _currentAccountDescription; }
            set { Set(ref _currentAccountDescription, value, "CurrentAccountDescription"); }
        }
        [DataMember]
        public DailyAccountType AccountFolder
        {
            get { return _accountFolder; }
            set { Set(ref _accountFolder, value, "AccountFolder"); }
        }
        [DataMember]
        public bool Cancelled { get; set; }
        [DataMember]
        public bool Corrected { get; set; }
        [DataMember]
        public bool IsAuto { get; set; }
        [DataMember]
        public bool IsDaily { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string DocNumber { get; set; }
        [DataMember]
        public string PhoneNumber { get; set; }
        [DataMember]
        public short? PensionMode { get; set; }
        [DataMember]
        public Guid? PriceRateId { get; set; }
        [DataMember]
        public ReceivableType? Receivable
        {
            get { return _receivableType; }
            set { Set(ref _receivableType, value, "Receivable"); }
        }
        [DataMember]
        public bool InAdvance { get; set; }
        [DataMember]
        public Guid? WithdrawType
        {
            get { return _withdrawType; }
            set { Set(ref _withdrawType, value, "WithdrawType"); }
        }
        [DataMember]
        public Guid? ReceiptDocumentId { get; set; }

        [DataMember]
        public Guid? OfficialDocumentId { get; set; }

        [DataMember]
        public OfficialDocContract OfficialDocument { get; set; }

        [DataMember]
        public decimal? DiscountPercent
        {
            get { return _discountPercent; }
            set { Set(ref _discountPercent, value, "DiscountPercent"); }
        }

        [DataMember]
        public decimal? DiscountValue
        {
            get { return _discountValue; }
            set { Set(ref _discountValue, value, "DiscountValue"); }
        }

        [DataMember]
        public Guid? EntityId
        {
            get { return _entityId; }
            set { Set(ref _entityId, value, "EntityId"); }
        }

        [DataMember]
        public Guid? TaxSchemaId
        {
            get { return _taxSchemaId; }
            set { Set(ref _taxSchemaId, value, "TaxSchemaId"); }
        }

        [DataMember]
        public bool? TaxIncluded
        {
            get { return _taxIncluded; }
            set { Set(ref _taxIncluded, value, "TaxIncluded"); }
        }

        [DataMember]
        public decimal? CommissionValue { get; set; }
        [DataMember]
        public Guid? CityTaxId { get; set; }
        [DataMember]
        public decimal CityTaxPercent { get; set; }
        [DataMember]
        public DiscountApplyOver CityTaxAppliedOver { get; set; }
        [DataMember]
        public decimal CityTaxValue { get; set; }
        [DataMember]
        public short? AdultNumber { get; set; }
        [DataMember]
        public short? ChildNumber { get; set; }
        [DataMember]
        public long ApplicationId { get; set; }
        [DataMember]
        public bool CancellationFee { get { return _cancellationFee; } set { Set(ref _cancellationFee, value, "CancellationFee"); } }
        [DataMember]
        public Guid? GuestId { get { return _guestId; } set { Set(ref _guestId, value, "GuestId"); } }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public CurrencyInstallationContract BaseCurrency
        {
            get { return _baseCurrency; }
        }

        [ReflectionExclude]
        public DiscountType DiscountType
        {
            get { return _discountPercent.HasValue ? DiscountType.Percent : DiscountType.Value; }
        }

        [ReflectionExclude]
        public bool IsCityTax
        {
            get { return LineType.HasValue && LineType.Value == SpecialLineType.CityTax; }
        }

        [ReflectionExclude]
        public bool IsCityTaxByPercent
        {
            get { return IsCityTax && !AdultNumber.HasValue && !ChildNumber.HasValue; }
        }

        [ReflectionExclude]
        public bool IsTip
        {
            get { return LineType.HasValue && LineType.Value == SpecialLineType.Tip; }
        }

        [ReflectionExclude]
        public bool ExchangeRateChanged { get; set; }

        #endregion
        #region Parameters

        [DataMember]
        public bool PaymentMethodVisibility { get; set; }

        #endregion
        #region Contracts

        [ReflectionExclude]
        [DataMember]
        public TypedList<MovementTaxDetailContract> TaxDetailMovements { get; set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<MovementCashFlowContract> CashFlows { get; set; }

        [ReflectionExclude]
        [DataMember]
        public CreditCardContract CreditCard
        {
            get { return _creditCard; }
            set { Set(ref _creditCard, value, "CreditCard"); }
        }

        [ReflectionExclude]
        public bool ShowInstallmentDetails
        {
            get { return CreditCard != null; }
        }
            
        [ReflectionExclude]
        [DataMember]
        public CreditCardContract PaymentCreditCard
        {
            get { return _paymentCreditCard; }
            set { Set(ref _paymentCreditCard, value, "PaymentCreditCard"); }
        }

        [ReflectionExclude]
        [DataMember]
        public CreditCardContract RefundCreditCard
        {
            get { return _refundCreditCard; }
            set { Set(ref _refundCreditCard, value, "RefundCreditCard"); }
        }

        #endregion
        #region Public Methods

        public override object Clone()
        {
            var contract = (BaseContract)Activator.CreateInstance(GetType(), BaseCurrency);
            this.AssignTo(contract);
            return contract;
        }

        public virtual void Reverse()
        {
            if (Quantity.HasValue)
                Quantity = (short)-Quantity.Value;
            InformedValue = -InformedValue;
            GrossValue = -GrossValue;
            NetValue = -NetValue;
            if (UndiscountValue.HasValue)
                UndiscountValue = -UndiscountValue.Value;
            if (ForeignCurrencyValue.HasValue)
                ForeignCurrencyValue = -ForeignCurrencyValue.Value;
            CityTaxValue = -CityTaxValue;
        }

        /// <summary>
        /// Retorna el Valor Bruto en la moneda del movimiento (extranjera o base)
        /// </summary>
        public decimal ComputedGrossValue
        {
            get
            {
                if (ForeignCurrencyValue.HasValue && ExchangeRateValue.HasValue)
                {
                    if (ExchangeRateValue.Value == decimal.Zero)
                        return decimal.Zero;

                    return ExchangeRateMult
                        ? GrossValue / ExchangeRateValue.Value
                        : GrossValue * ExchangeRateValue.Value;
                }

                return GrossValue;
            }
        }

        /// <summary>
        /// Retorna el Valor Neto en la moneda del movimiento (extranjera o base)
        /// </summary>
        public decimal ComputedNetValue
        {
            get
            {
                if (ForeignCurrencyValue.HasValue && ExchangeRateValue.HasValue)
                {
                    if (ExchangeRateValue.Value == decimal.Zero)
                        return decimal.Zero;

                    return ExchangeRateMult
                        ? NetValue / ExchangeRateValue.Value
                        : NetValue * ExchangeRateValue.Value;
                }

                return NetValue;
            }
        }

        #endregion
        #region Constructors

        public MovementBaseContract(CurrencyInstallationContract baseCurrency,
            DailyAccountType accountFolder = DailyAccountType.Master,
            DiscountApplyOver cityTaxAppliedOver = DiscountApplyOver.Net)
            : base()
        {
            _baseCurrency = baseCurrency;
            AccountFolder = accountFolder;
            CityTaxAppliedOver = cityTaxAppliedOver;
            TaxDetailMovements = new TypedList<MovementTaxDetailContract>();
            CashFlows = new TypedList<MovementCashFlowContract>();
        }

        public MovementBaseContract(DateTime workDate, DateTime valueDate, Guid? serviceByDepartmentId,
            DailyAccountType accountFolder, short? pensionMode, Guid? priceRate, short? quantity,
            string movementCurrency, decimal? exchangeRateValue, bool exchangeRateMult,
            decimal? commissionValue, decimal? disscountPercent, decimal? discountValue,
            CurrencyInstallationContract baseCurrency)
            : this(baseCurrency, accountFolder)
        {
            WorkDate = workDate;
            ValueDate = valueDate;
            ServiceByDepartmentId = serviceByDepartmentId;
            PensionMode = pensionMode;
            PriceRateId = priceRate;
            Quantity = quantity;
            Currency = movementCurrency;
            ExchangeRateValue = exchangeRateValue;
            ExchangeRateMult = exchangeRateMult;
            CommissionValue = commissionValue;
            DiscountPercent = disscountPercent;
            DiscountValue = discountValue;
        }

        #endregion
    }
}