﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class ProductStandContract : BaseContract
    {
        private ProductStandContract(Guid productId, string productDescription, Guid? serviceId, decimal quantity, decimal value)
        {
            ProductId = productId;
            ProductDescription = productDescription;
            ServiceId = serviceId;
            Value = value;
            Quantity = quantity;
        }

        public ProductStandContract(Guid productId, string productDescription, Guid? serviceId, decimal quantity, decimal value,
            IEnumerable<ProductStandTaxContract> taxes)
            : this(productId, productDescription, serviceId, quantity, value)
        {
            Taxes = new TypedList<ProductStandTaxContract>(taxes);
        }

        public ProductStandContract(Guid productId, string productDescription, Guid? serviceId, decimal quantity, decimal value,
            Guid taxRateId1, decimal percent1, decimal taxBase1, decimal taxValue1,
            Guid? taxRateId2, decimal? percent2, decimal taxBase2, decimal taxValue2,
            Guid? taxRateId3, decimal? percent3, decimal taxBase3, decimal taxValue3)
            : this(productId, productDescription, serviceId, quantity, value)
        {
            Taxes = new TypedList<ProductStandTaxContract>();
            Taxes.Add(new ProductStandTaxContract(taxRateId1, percent1, taxBase1, taxValue1));
            if (taxRateId2.HasValue && percent2.HasValue)
                Taxes.Add(new ProductStandTaxContract(taxRateId2.Value, percent2.Value, taxBase2, taxValue2));
            if (taxRateId3.HasValue && percent3.HasValue)
                Taxes.Add(new ProductStandTaxContract(taxRateId3.Value, percent3.Value, taxBase3, taxValue3));
        }

        [DataMember]
        public decimal Quantity { get; set; }
        [DataMember]
        public decimal Value { get; set; }
        [DataMember]
        public Guid ProductId { get; set; }
        [DataMember]
        public string ProductDescription { get; set; }
        [DataMember]
        public Guid? ServiceId { get; set; }

        [DataMember]
        public TypedList<ProductStandTaxContract> Taxes { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ProductStandTaxContract : BaseContract
    {
        public ProductStandTaxContract(Guid taxRateId, decimal percent, bool retention)
        {
            TaxRateId = taxRateId;
            Percent = percent;
            Retention = retention;
        }

        public ProductStandTaxContract(Guid taxRateId, decimal percent, decimal taxBase, decimal taxValue)
            : this(taxRateId, percent, false)
        {
            TaxBase = taxBase;
            TaxIncidence = TaxBase;
            TaxValue = taxValue;
        }

        [DataMember]
        public Guid TaxRateId { get; set; }
        [DataMember]
        public decimal Percent { get; set; }
        [DataMember]
        public decimal TaxIncidence { get; set; }
        [DataMember]
        public decimal TaxBase { get; set; }
        [DataMember]
        public decimal TaxValue { get; set; }
        [DataMember]
        public bool Retention { get; set; }
    }
}