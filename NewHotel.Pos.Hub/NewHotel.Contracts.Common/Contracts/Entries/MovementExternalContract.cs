﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(MovementExternalContract), "ValidateMovementPos")]
    public class MovementExternalContract : BaseContract
    {
        #region Members

        private string _document;
        private string _description;
        private Guid _department;
        private CurrentAccountType? _accountType;
        private Guid? _accountId;
        private string _currency;
        private DateTime? _workDate;
        private DateTime? _valueDate;
        private decimal _valueDebit;
        private Guid? _fore;
        private CreditCardContract _creditCard = null;
        private bool? _taxIncluded;
        private Guid? _taxSchemaId;
        private short? _posWorkShift;

        #endregion
        #region Constructor

        public MovementExternalContract()
            : base()
        {
            Initialize();
        }

        public MovementExternalContract(Guid id, string document, string description, Guid department, Guid servicesByDepartment,
            CurrentAccountType? accountType, Guid? accountId, string currency, DateTime? workDate,
            TypedList<ProductStandContract> products, DateTime? valueDate = null)
            : base()
        {
            Id = id;
            Document = document;
            Description = description;
            Department = department;
            AccountType = accountType;
            AccountId = accountId;
            Currency = currency;
            WorkDate = workDate;
            ValueDate = valueDate;
            Products = products;
            Initialize();
        }

        private void Initialize()
        {
            TicketLineIds = new TypedList<Guid>();
            MovementIds = new TypedList<Guid>();
        }

        #endregion
        #region Properties

        [DataMember]
        public string Document { get { return _document; } set { Set(ref _document, value, "Document"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public Guid Department { get { return _department; } set { Set(ref _department, value, "Department"); } }
        [DataMember]
        public CurrentAccountType? AccountType { get { return _accountType; } set { Set(ref _accountType, value, "AccountType"); } }
        [DataMember]
        public Guid? AccountId { get { return _accountId; } set { Set(ref _accountId, value, "AccountId"); } }
        [DataMember]
        public string Currency { get { return _currency; } set { Set(ref _currency, value, "Currency"); } }
        [DataMember]
        public DateTime? WorkDate { get { return _workDate; } set { Set(ref _workDate, value, "WorkDate"); } }
        [DataMember]
        public DateTime? ValueDate { get { return _valueDate; } set { Set(ref _valueDate, value, "ValueDate"); } }
        [DataMember]
        public decimal ValueDebit { get { return _valueDebit; } set { Set(ref _valueDebit, value, "ValueDebit"); } }
        [DataMember]
        public Guid? Fore { get { return _fore; } set { Set(ref _fore, value, "Fore"); } }
        [DataMember]
        public CreditCardContract CreditCard { get { return _creditCard; } set { Set(ref _creditCard, value, "CreditCard"); } }
        [DataMember]
        public bool? TaxIncluded { get { return _taxIncluded; } set { Set(ref _taxIncluded, value, "TaxIncluded"); } }
        [DataMember]
        public Guid? TaxSchemaId { get { return _taxSchemaId; } set { Set(ref _taxSchemaId, value, "TaxSchemaId"); } }
        [DataMember]
        public short? PosWorkShift { get { return _posWorkShift; } set { Set(ref _posWorkShift, value, "PosWorkShift"); } }

        [DataMember]
        public TypedList<ProductStandContract> Products { get; set; }
        #endregion
        #region External Lines vs Movements
        [DataMember]
        public Dictionary<Guid, Guid[]> LinesPerMovement;
        [DataMember]
        public Dictionary<Guid, List<Guid>> GroupedProductLines;
        [DataMember]
        public TypedList<Guid> TicketLineIds;
        [DataMember]
        public TypedList<Guid> MovementIds;

        //Add all lines that generate the PMS credit or debit movement
        public void AddMovement(Guid movementId, IEnumerable<Guid> lines)
        {
            if (LinesPerMovement == null) LinesPerMovement = new Dictionary<Guid, Guid[]>();
            LinesPerMovement.Add(movementId, lines.ToArray());
        }

        //Add all original lines grouped by product on BuildContainer
        public void AddProductLine(Guid groupId, Guid productLine)
        {
            if (GroupedProductLines == null) GroupedProductLines = new Dictionary<Guid, List<Guid>>();
            if (GroupedProductLines.ContainsKey(groupId)) GroupedProductLines[groupId].Add(productLine);
            else GroupedProductLines.Add(groupId, new List<Guid>() { productLine });
        }

        public void AddLineId(Guid lineId)
        {
            if (!TicketLineIds.Contains(lineId))
                TicketLineIds.Add(lineId);
        }

        public void AddMovementId(Guid movementId)
        {
            if (!MovementIds.Contains(movementId))
                MovementIds.Add(movementId);
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateMovementPos(MovementExternalContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
