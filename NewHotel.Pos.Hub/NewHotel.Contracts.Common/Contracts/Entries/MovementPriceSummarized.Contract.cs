﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class MovementPriceSummarizedContract : BaseContract, IComparable<MovementPriceSummarizedContract>
    {
        #region Members

        private decimal _movementValue;
        private decimal _movementNetValue;
        private decimal _movementGuestTaxValue;
        private Guid? _movementGuestTaxParentId;
        private Guid? _movementGuestTaxDepartmentId;
        private Guid? _movementGuestTaxServiceByDepartmentId;
        private Guid? _movementGuestTaxSchemaId;
        private DailyAccountType? _movementGuestTaxAccountFolder;
        private bool _taxIncluded;

        [DataMember]
        internal MovementType _movementType;
        [DataMember]
        internal MovementSource _movementSource;
        [DataMember]
        internal bool _invalidPrice;
        [DataMember]
        internal bool _isForecast;

        #endregion
        #region Properties

        [DataMember]
        public DailyAccountType AccountFolder { get; internal set; }
        [DataMember]
        public string AccountFolderDesc { get; internal set; }
        [DataMember]
        public DateTime MovementDate { get; internal set; }
        [DataMember]
        public Guid? DepartmentId { get; internal set; }
        [DataMember]
        public string DepartmentDesc { get; internal set; }
        [DataMember]
        public Guid? ServiceId { get; internal set; }
        [DataMember]
        public Guid? ServiceByDepartmentId { get; internal set; }
        [DataMember]
        public string ServiceDesc { get; internal set; }
        [DataMember]
        public decimal? DiscountPercent { get; internal set; }
        [DataMember]
        public decimal Discount { get; internal set; }
        [DataMember]
        public string MovementCurrency { get; internal set; }
        [DataMember]
        public decimal MovementValue
        {
            get { return _movementValue; }
            set { Set(ref _movementValue, value, "MovementValue"); }
        }
        [DataMember]
        public decimal MovementNetValue
        {
            get { return _movementNetValue; }
            set { Set(ref _movementNetValue, value, "MovementNetValue"); }
        }
        [DataMember]
        public decimal MovementGuestTaxValue
        {
            get { return _movementGuestTaxValue; }
            set { Set(ref _movementGuestTaxValue, value, "MovementGuestTaxValue"); }
        }
        [DataMember]
        public Guid? MovementGuestTaxParentId
        {
            get { return _movementGuestTaxParentId; }
            set { Set(ref _movementGuestTaxParentId, value, "MovementGuestTaxParentId"); }
        }
        [DataMember]
        public Guid? MovementGuestTaxDepartmentId
        {
            get { return _movementGuestTaxDepartmentId; }
            set { Set(ref _movementGuestTaxDepartmentId, value, "MovementGuestTaxDepartmentId"); }
        }
        [DataMember]
        public Guid? MovementGuestTaxServiceByDepartmentId
        {
            get { return _movementGuestTaxServiceByDepartmentId; }
            set { Set(ref _movementGuestTaxServiceByDepartmentId, value, "MovementGuestTaxServiceByDepartmentId"); }
        }
        [DataMember]
        public DailyAccountType? MovementGuestTaxAccountFolder
        {
            get { return _movementGuestTaxAccountFolder; }
            set { Set(ref _movementGuestTaxAccountFolder, value, "MovementGuestTaxAccountFolder"); }
        }
        [DataMember]
        public bool TaxIncluded
        {
            get { return _taxIncluded; }
            set { Set(ref _taxIncluded, value, "TaxIncluded"); }
        }

        [ReflectionExclude]
        public MovementType MovementType
        {
            get { return _movementType; }
        }

        [ReflectionExclude]
        public MovementSource MovementSource
        {
            get { return _movementSource; }
        }

        [ReflectionExclude]
        public long MovementSourceOrder
        {
            get
            {
                switch (_movementSource)
                {
                    case MovementSource.PriceRate:
                        return 1;
                    case MovementSource.ManualRate:
                        return 2;
                    case MovementSource.PriceRateAddtional:
                        return 3;
                    case MovementSource.ReservationAdditional:
                        return 4;
                }

                return long.MaxValue;
            }
        }

        [ReflectionExclude]
        public bool InvalidPrice
        {
            get { return _invalidPrice; }
        }

        [ReflectionExclude]
        public bool IsForecast
        {
            get { return _isForecast; }
        }

        #endregion
        #region IComparable<MovementPriceSummarizedContract>

        int IComparable<MovementPriceSummarizedContract>.CompareTo(MovementPriceSummarizedContract other)
        {
            var comparison = DateTime.Compare(MovementDate, other.MovementDate);
            if (comparison != 0)
                return comparison;

            comparison = decimal.Compare(MovementSourceOrder, other.MovementSourceOrder);
            if (comparison != 0)
                return comparison;

            comparison = MovementType.CompareTo(other.MovementType);
            if (comparison != 0)
                return comparison;

            if (MovementGuestTaxParentId.HasValue && MovementGuestTaxParentId.Value.Equals((Guid)other.Id))
                comparison = -1;
            if (other.MovementGuestTaxParentId.HasValue && other.MovementGuestTaxParentId.Value.Equals((Guid)Id))
                comparison = 1;
            if (comparison != 0)
                return comparison;

            comparison = AccountFolder.CompareTo(other.AccountFolder);

            return comparison;
        }

        #endregion
        #region Constructors

        public MovementPriceSummarizedContract(DateTime movementDate,
            DailyAccountType accountFolder, string accountFolderDesc,
            Guid? departmentId, string departmentDesc, Guid? serviceId, string serviceDesc,
            decimal? discountPerc, decimal discount,
            decimal movementValue, decimal movementNetValue, decimal movementGuestTaxValue,
            Guid? movementGuestTaxDepartmentId, Guid? movementGuestTaxServiceByDepartmentId,
            Guid? movementGuestTaxSchemaId, DailyAccountType? movementGuestTaxAccountFolder,
            bool invalidPrice, Guid? servicebyDepartament, string currency,
            MovementType movementType, MovementSource movementSource, bool isForecast)
            : base() 
        {
            MovementDate = movementDate;
            AccountFolder = accountFolder;
            AccountFolderDesc = accountFolderDesc;
            DepartmentId = departmentId;
            DepartmentDesc = departmentDesc;
            ServiceId = serviceId;
            ServiceDesc = serviceDesc;
            DiscountPercent = discountPerc;
            Discount = discount;
            _movementValue = movementValue;
            _movementNetValue = movementNetValue;
            _movementGuestTaxValue = movementGuestTaxValue;
            _movementGuestTaxDepartmentId = movementGuestTaxDepartmentId;
            _movementGuestTaxServiceByDepartmentId = movementGuestTaxServiceByDepartmentId;
            _movementGuestTaxSchemaId = movementGuestTaxSchemaId;
            _movementGuestTaxAccountFolder = movementGuestTaxAccountFolder;
            _invalidPrice = invalidPrice;
            ServiceByDepartmentId = servicebyDepartament;
            MovementCurrency = currency;
            _movementType = movementType;
            _movementSource = movementSource;
            _isForecast = isForecast;
        }

        #endregion
    }
}
