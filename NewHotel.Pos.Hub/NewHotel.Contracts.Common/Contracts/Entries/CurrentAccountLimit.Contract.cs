﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class CurrentAccountLimitContract : BaseContract
    {
        #region Members

        private DailyAccountType _accountFolder;
        private string _currencyId;
        private bool _unlimited;
        private decimal? _limitValue;
        private bool _prePaymentRequired;

        #endregion
        #region Properties

        [DataMember]
        public DailyAccountType AccountFolder { get { return _accountFolder; } set { Set(ref _accountFolder, value, "AccountFolder"); } }
        [DataMember]
        public string CurrencyId { get { return _currencyId; } set { Set(ref _currencyId, value, "CurrencyId"); } }
        [DataMember]
        public bool Unlimited { get { return _unlimited; } set { Set(ref _unlimited, value, "Unlimited"); } }
        [DataMember]
        public decimal? LimitValue { get { return _limitValue; } set { Set(ref _limitValue, value, "LimitValue"); } }
        [DataMember]
        public bool PrePaymentRequired { get { return _prePaymentRequired; } set { Set(ref _prePaymentRequired, value, "PrePaymentRequired"); } }

        #endregion
        #region Constructors

        public CurrentAccountLimitContract()
            : base()
        {
        }

        #endregion
    }
}