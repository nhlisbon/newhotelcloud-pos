﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SplitMovementContract : BaseContract
    {
        #region Members

        private DailyAccountType _folder;
        private bool _percentCriteria;
        private decimal _valueCriteria;

        #endregion
        #region Persistent Properties
        
        [DataMember]
        public MovementContract MovementContract { get;  set; }

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Folder required.")]
        public DailyAccountType Folder
        {
            get { return _folder; }
            set { Set(ref _folder, value, "Folder"); }
        }

        [DataMember]
        public bool PercentCriteria
        {
            get { return _percentCriteria; }
            set { Set(ref _percentCriteria, value, "PercentCriteria"); }
        }

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Value required.")]
        public decimal ValueCriteria
        {
            get { return _valueCriteria; }
            set { Set(ref _valueCriteria, value, "ValueCriteria"); }
        }

        #endregion
        #region Constructors

        public SplitMovementContract()
            : base() 
        {
            Folder = DailyAccountType.Master;
        }

        #endregion
    }
}