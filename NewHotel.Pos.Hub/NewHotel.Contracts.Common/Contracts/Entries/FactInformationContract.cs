﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class FactInformationContract : BaseContract
    {
        public FactInformationContract(string holder, string address, string nif, Guid? baseEntityId)
        {
            Holder = holder;
            Address = address;
            NIF = nif;
            Benti = baseEntityId;
        }

        public FactInformationContract(string holder, string address, string nif, Guid? baseEntityId, DateTime? docDate, DateTime? docSysDateTime)
            : this(holder, address, nif, baseEntityId)
        {
            DocDate = docDate;
            DocSysDateTime = docSysDateTime;
        }

        public FactInformationContract()
        {
        }

        [DataMember]
        public DateTime? DocDate { get;  set; }
        [DataMember]
        public DateTime? DocSysDateTime { get;  set; }
        [DataMember]
        public string Holder { get; set; }
        [DataMember]
        public string Nationality { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string NIF { get; set; }
        [DataMember]
        public Guid? Anul { get; set; }
        [DataMember]
        public Guid? Benti { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public string FiscalSignature { get; set; }
        [DataMember]
        public string FactSerie { get; set; }
        [DataMember]
        public long? FactNumber { get; set; }
        [DataMember]
        public decimal? Value { get; set; }
        [DataMember]
        public bool AllowInsertfromExternal { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public bool? Fiscalization { get; set; }
        [DataMember]
        public long Application { get; set; }
        [DataMember]
        public bool IsBallot { get; set; }
    }
}
