﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RoomStatusLogContract : BaseContract
    {
        #region Properties

        [DataMember]
        public Guid RoomId { get; set; }
        [DataMember]
        public string RoomNum { get; internal set; }
        [DataMember]
        public DateTime LogDate { get; internal set; }
        [DataMember]
        public DateTime LogTime { get; internal set; }
        [DataMember]
        public RoomStatus? OldRoomStatus { get; set; }
        [DataMember]
        public string OldRoomStatusDesc { get; set; }
        [DataMember]
        public RoomStatus? NewRoomStatus { get; set; }
        [DataMember]
        public string NewRoomStatusDesc { get; set; }

        #endregion

        #region Constructors

        public RoomStatusLogContract()
            : base() { }


        public RoomStatusLogContract(Guid? roomStatusLogId,
            DateTime? logDate, Guid roomId, string roomNum,
            RoomStatus? oldRoomStatus, string oldRoomStatusDesc,
            RoomStatus? newRoomStatus, string newRoomStatusDesc,
            DateTime lastModified, DateTime date)
            : base() 
        {
            Id = roomStatusLogId;
            LogDate = logDate ?? date;
            RoomId = roomId;
            RoomNum = roomNum;
            OldRoomStatus = oldRoomStatus;
            OldRoomStatusDesc = oldRoomStatusDesc;
            NewRoomStatus = newRoomStatus ?? oldRoomStatus;
            NewRoomStatusDesc = newRoomStatusDesc ?? oldRoomStatusDesc;
            LastModified = lastModified;
        }

        #endregion
    }
}
