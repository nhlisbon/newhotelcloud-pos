﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(SupervisorsContract), "ValidateLineContract")]
    public class SupervisorsContract : BaseContract
    {
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public string Description { get; set; }

        #region Constructor

        public SupervisorsContract()
            : base()
        {
        }
        
        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateLineContract(SupervisorsContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
