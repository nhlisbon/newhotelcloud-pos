﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(HousekeeperDaysFreeContract), "ValidateLineContract")]
    public class HousekeeperDaysFreeContract : BaseContract
    {
        [DataMember]
        public Guid HousekeeperId { get; set; }
        [DataMember]
        public DateTime SelectedDate { get; set; }
        [DataMember]
        public bool DayFree { get; set; }

        #region Constructor

        public HousekeeperDaysFreeContract()
            : base()
        {
        }
        
        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateLineContract(HousekeeperDaysFreeContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
