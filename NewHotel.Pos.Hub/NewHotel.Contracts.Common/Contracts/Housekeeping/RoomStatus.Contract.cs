﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RoomStatusContract : BaseContract
    {
        #region Properties

        [DataMember]
        public Guid RoomId { get; set; }
        [DataMember]
        public string RoomNum { get;  set; }

        [DataMember]
        public RoomStatus? Status { get; set; }
        [DataMember]
        public int Column { get; set; }

        #endregion
        #region Constructors

        public RoomStatusContract()
            : base() { }

        public RoomStatusContract(Guid? roomStatusLogId,
            Guid roomId, string roomNum,
            RoomStatus? status )
            : base() 
        {
            Id = roomStatusLogId;
            RoomId = roomId;
            RoomNum = roomNum;
            Status = status;
        }

        #endregion
    }


    [DataContract]
	[Serializable]
    public class RoomStatusContractDetailed : BaseRecord
    {
        [DataMember]
        public RoomStatusRecord Col0 { get; set; }
        [DataMember]
        public RoomStatusRecord Col1 { get; set; }
        [DataMember]
        public RoomStatusRecord Col2 { get; set; }
        [DataMember]
        public RoomStatusRecord Col3 { get; set; }
        [DataMember]
        public RoomStatusRecord Col4 { get; set; }
        [DataMember]
        public RoomStatusRecord Col5 { get; set; }
        [DataMember]
        public RoomStatusRecord Col6 { get; set; }
        [DataMember]
        public RoomStatusRecord Col7 { get; set; }
        [DataMember]
        public RoomStatusRecord Col8 { get; set; }
        [DataMember]
        public RoomStatusRecord Col9 { get; set; }
        [DataMember]
        public RoomStatusRecord Col10 { get; set; }
        [DataMember]
        public RoomStatusRecord Col11 { get; set; }
        [DataMember]
        public RoomStatusRecord Col12 { get; set; }
        [DataMember]
        public RoomStatusRecord Col13 { get; set; }

        public int Rooms
        {
            get
            {
                int result = 0;
                if (Col0 != null) result++;
                if (Col1 != null) result++;
                if (Col2 != null) result++;
                if (Col3 != null) result++;
                if (Col4 != null) result++;
                if (Col5 != null) result++;
                if (Col6 != null) result++;
                if (Col7 != null) result++;
                if (Col8 != null) result++;
                if (Col9 != null) result++;
                if (Col10 != null) result++;
                if (Col11 != null) result++;
                if (Col12 != null) result++;
                if (Col13 != null) result++;

                return result;
            }
        }

        public int Clean
        {
            get
            {
                int result = 0;
                if (Col0 != null && Col0.Status == RoomStatus.Clean) result++;
                if (Col1 != null && Col1.Status == RoomStatus.Clean) result++;
                if (Col2 != null && Col2.Status == RoomStatus.Clean) result++;
                if (Col3 != null && Col3.Status == RoomStatus.Clean) result++;
                if (Col4 != null && Col4.Status == RoomStatus.Clean) result++;
                if (Col5 != null && Col5.Status == RoomStatus.Clean) result++;
                if (Col6 != null && Col6.Status == RoomStatus.Clean) result++;
                if (Col7 != null && Col7.Status == RoomStatus.Clean) result++;
                if (Col8 != null && Col8.Status == RoomStatus.Clean) result++;
                if (Col9 != null && Col9.Status == RoomStatus.Clean) result++;
                if (Col10 != null && Col10.Status == RoomStatus.Clean) result++;
                if (Col11 != null && Col11.Status == RoomStatus.Clean) result++;
                if (Col12 != null && Col12.Status == RoomStatus.Clean) result++;
                if (Col13 != null && Col13.Status == RoomStatus.Clean) result++;

                return result;
            }
        }

        public int Dirty
        {
            get
            {
                int result = 0;
                if (Col0 != null && Col0.Status == RoomStatus.Dirty) result++;
                if (Col1 != null && Col1.Status == RoomStatus.Dirty) result++;
                if (Col2 != null && Col2.Status == RoomStatus.Dirty) result++;
                if (Col3 != null && Col3.Status == RoomStatus.Dirty) result++;
                if (Col4 != null && Col4.Status == RoomStatus.Dirty) result++;
                if (Col5 != null && Col5.Status == RoomStatus.Dirty) result++;
                if (Col6 != null && Col6.Status == RoomStatus.Dirty) result++;
                if (Col7 != null && Col7.Status == RoomStatus.Dirty) result++;
                if (Col8 != null && Col8.Status == RoomStatus.Dirty) result++;
                if (Col9 != null && Col9.Status == RoomStatus.Dirty) result++;
                if (Col10 != null && Col10.Status == RoomStatus.Dirty) result++;
                if (Col11 != null && Col11.Status == RoomStatus.Dirty) result++;
                if (Col12 != null && Col12.Status == RoomStatus.Dirty) result++;
                if (Col13 != null && Col13.Status == RoomStatus.Dirty) result++;

                return result;
            }
        }
    }
}
