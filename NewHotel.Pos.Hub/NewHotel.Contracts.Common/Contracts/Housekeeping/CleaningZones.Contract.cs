﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class CleaningZonesContract : BaseContract
    {
        [DataMember]
        internal LanguageTranslationContract _description;

        [Display(Name = "Description")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Cleaning Zone required.")]
        public Guid CleaningZonesGroupId { get; set; }

        #region Constructor

        public CleaningZonesContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }
        
        #endregion
    }
}
