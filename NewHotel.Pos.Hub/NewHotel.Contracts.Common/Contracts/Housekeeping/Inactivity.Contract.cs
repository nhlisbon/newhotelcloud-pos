﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(InactivityContract), "ValidateInactivity")]
    public class InactivityContract : BaseContract
    {
        #region Members

        [DataMember]
        internal DateTime _workDate;
        private DateTime _initialDate;
        private DateTime? _finalDate;
        private Guid _inactivityTypeId;
        private bool _affectBooking;
        private bool _unlimited;

        #endregion
        #region Properties

        public DateTime WorkDate { get { return _workDate; } }
        [DataMember]
        public Guid ResourceId { get; set; }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Initial date required.")]
        public DateTime InitialDate
        {
            get { return _initialDate; }
            set
            {
                if (Set(ref _initialDate, value, "InitialDate"))
                    NotifyPropertyChanged("IsInitialDateEnabled");
            }
        }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Final date required.")]
        public DateTime? FinalDate
        { 
            get { return _finalDate; }
            set
            { 
                if (Set(ref _finalDate, value, "FinalDate"))
                    NotifyPropertyChanged("IsFinalDateEnabled");
            }
        }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Inactivity type required.")]
        public Guid InactivityTypeId
        {
            get { return _inactivityTypeId; }
            set { Set(ref _inactivityTypeId, value, "InactivityTypeId"); }
        }
        [DataMember]
        public string InactivityTranslation { get; set; }
        [DataMember]
        public bool AffectBooking { get { return _affectBooking; } set { Set(ref _affectBooking, value, "AffectBooking"); } }
        [DataMember]
        public bool Unlimited { get { return _unlimited; } set { Set(ref _unlimited, value, "Unlimited"); NotifyPropertyChanged("IsFinalDateEnabled"); } }
        [DataMember]
        public string Comments { get; set; }

        #endregion
        #region Extension Properties

        public bool IsInitialDateEnabled
        {
            get { return InitialDate.Date >= WorkDate.Date; } 
        }

        public bool IsFinalDateEnabled
        {
            get { return !Unlimited && (!FinalDate.HasValue || FinalDate.Value.Date >= WorkDate.Date); }
        }

        public DateTime InitialMinDate
        {
            get { return InitialDate.Date >= WorkDate.Date ? WorkDate.Date : InitialDate.Date; }
        }

        public DateTime FinalMinDate
        {
            get { return FinalDate.HasValue ? (FinalDate.Value.Date >= WorkDate.Date ? WorkDate.Date.AddDays(-1) : FinalDate.Value.Date) : WorkDate.Date; }
        }

        #endregion
        #region Constructors

        public InactivityContract(DateTime workDate)
            : base()
        {
            _workDate = workDate;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateInactivity(InactivityContract obj)
        {
            if (obj.ResourceId == Guid.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Resource required.");
            if (obj.InitialDate == DateTime.MinValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Initial date required.");
            if (!obj.Unlimited && !obj.FinalDate.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Final date required.");
            if (obj.FinalDate < obj.InitialDate)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Initial date greater than final Date.");
            if (obj.InactivityTypeId == Guid.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Inactivity type required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
