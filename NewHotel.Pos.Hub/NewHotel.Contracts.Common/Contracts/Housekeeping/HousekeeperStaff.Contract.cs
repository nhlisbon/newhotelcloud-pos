﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(HousekeeperStaffContract), "ValidateLineContract")]
    public class HousekeeperStaffContract : BaseContract
    {
        private bool _sunday;
        private bool _monday;
        private bool _tuesday;
        private bool _wednesday;
        private bool _thurday;
        private bool _friday;
        private bool _saturday;

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public string Description { get; set; }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Supervisor required.")]
        public Guid SupervisorId { get; set; }
        [DataMember]
        public HousekeeperType HousekeeperType { get; set; }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Cleaning Zone required.")]
        public Guid CleaningZoneId { get; set; }
        [DataMember]
        public bool Sunday { get { return _sunday; } set { Set(ref _sunday, value, "Sunday"); } }
        [DataMember]
        public bool Monday { get { return _monday; } set { Set(ref _monday, value, "Monday"); } }
        [DataMember]
        public bool Tuesday { get { return _tuesday; } set { Set(ref _tuesday, value, "Tuesday"); } }
        [DataMember]
        public bool Wednesday { get { return _wednesday; } set { Set(ref _wednesday, value, "Wednesday"); } }
        [DataMember]
        public bool Thurday { get { return _thurday; } set { Set(ref _thurday, value, "Thurday"); } }
        [DataMember]
        public bool Friday { get { return _friday; } set { Set(ref _friday, value, "Friday"); } }
        [DataMember]
        public bool Saturday { get { return _saturday; } set { Set(ref _saturday, value, "Saturday"); } }

        [DataMember]
        public bool Inactive { get; set; }

        #region Constructor

        public HousekeeperStaffContract()
            : base()
        {
            Inactive = false;
            Sunday = Monday = Tuesday = Wednesday = Thurday = Friday = Saturday = false;
            HousekeeperType = HousekeeperType.RoomMaid;
        }
        
        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateLineContract(HousekeeperStaffContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}