﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class OnlinePaymentTerminalContract : BaseContract
    {
        #region Private Members

        private string _des;
        private string _ExtraData;
        private TerminalInfoContract _Info;
        private Guid _OriginId;
        private string _TerminalName;
        #endregion
        #region Constructor
        public OnlinePaymentTerminalContract()
        {
            this.Info = new TerminalInfoContract();
        }
        #endregion
        #region Public Properties
        [DataMember]
        public string Description { get { return _des; } set { Set(ref _des, value, "Description"); } }

        [DataMember]
        public string ExtraData { get { return _ExtraData; } set { Set(ref _ExtraData, value, "ExtraData"); } }
        [DataMember]
        public TerminalInfoContract Info { get { return _Info; } set { Set(ref _Info, value, "Info"); } }
        [DataMember]
        public Guid OriginId { get { return _OriginId; } set { Set(ref _OriginId, value, "OriginId"); } }
        [DataMember]
        public string TerminalName { get { return _TerminalName; } set { Set(ref _TerminalName, value, "TerminalName"); } }

        #endregion

    }
    [DataContract]
    [Serializable]
    public class TerminalInfoContract : BaseContract
    {
        private string _ExtraInfo;
        private string _TerminalId;
        public TerminalInfoContract()
        { }
        [DataMember]
        public string ExtraInfo { get { return _ExtraInfo; } set { Set(ref _ExtraInfo, value, "ExtraInfo"); } }
        [DataMember]
        public string TerminalId { get { return _TerminalId; } set { Set(ref _TerminalId, value, "TerminalId"); } }
    }
}
