﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RoomPriceRatePlanningContract : PlanningContract
    {
        public RoomPriceRatePlanningContract(string totalRowTitle)
            : base(totalRowTitle)
        {
        }

        public override void BuildPlanning()
        {
            Rows.Clear();
            BuildPlanning(Records.Cast<RoomPriceRatePlanningRecord>());
            SortPlanning();
            AddTotalRow(Rows);
        }

        private void BuildPlanning(IEnumerable<RoomPriceRatePlanningRecord> records)
        {
            //Default Mode
            if (DetailColumns == null || DetailColumns.Count == 0)
                BuildPlanningInTotalGrouping(records);
            //Price Rate & Group
            else if (DetailColumns.Count == 1 && ContainsColumn("catp_pk"))
                BuildPlanningInGroupGrouping(records);
            //Price Rate & Room
            else if (DetailColumns.Count == 1 && ContainsColumn("trec_pk"))
                BuildPlanningInRoomGrouping(records);
            //Price Rate, Group & Room
            else if (DetailColumns.Count == 2 && ContainsColumn("catp_pk") && ContainsColumn("trec_pk"))
                BuildPlanningInFullDetails(records);
        }

        private void BuildPlanningInTotalGrouping(IEnumerable<RoomPriceRatePlanningRecord> records)
        {
            //Rows = new List<PlanningRowContract>();
            foreach (var record in records)
            {
                #region Hotel

                var key = record.HotelId.ToString();

                if (ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key;
                    rowContract.Title = record.HotelDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }

                #endregion
                #region Price Rate

                var key_price = key + "&" + record.PriceRateId.ToString();

                if (ContainsRow(key_price))
                {
                    var rowContract = GetRowByKey(key_price);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_price;
                    rowContract.Title = record.PriceRateDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.PriceRateDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = null;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
            }
        }

        private void BuildPlanningInGroupGrouping(IEnumerable<RoomPriceRatePlanningRecord> records)
        {
            //Rows = new List<PlanningRowContract>();
            foreach (var record in records)
            {
                #region Hotel

                var key = record.HotelId.ToString();

                if (ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key;
                    rowContract.Title = record.HotelDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(string.Empty);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }
                #endregion
                #region Category

                var key_category = key + "&" + record.CategoryId.ToString();

                if (ContainsRow(key_category))
                {
                    var rowContract = GetRowByKey(key_category);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_category;
                    rowContract.Title = record.CategoryDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.CategoryDescription);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.FontBold = true;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238); ;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
                #region Price Rate

                var key_pricerate = key_category + "&" + record.PriceRateId.ToString();

                if (ContainsRow(key_pricerate))
                {
                    var rowContract = GetRowByKey(key_pricerate);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_pricerate;
                    rowContract.Title = record.PriceRateDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.CategoryDescription);
                    rowContract.SortKeys.Add(record.PriceRateDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 10;
                    rowContract.CustomBackground = null;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key_category);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
            }
        }

        private void BuildPlanningInRoomGrouping(IEnumerable<RoomPriceRatePlanningRecord> records)
        {
            //Rows = new List<PlanningRowContract>();
            foreach (var record in records)
            {
                #region Hotel

                var key = record.HotelId.ToString();

                if (ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key;
                    rowContract.Title = record.HotelDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(string.Empty);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }

                #endregion
                #region Price Rate

                var key_pricerate = key + "&" + record.PriceRateId.ToString();

                if (ContainsRow(key_pricerate))
                {
                    var rowContract = GetRowByKey(key_pricerate);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_pricerate;
                    rowContract.Title = record.PriceRateDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.PriceRateDescription);
                    rowContract.SortKeys.Add(String.Empty);

                    //Styles
                    rowContract.FontBold = true;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238); ;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
                #region Room Type

                var key_roomtype =  key_pricerate + "&" + record.RoomTypeId.ToString();

                if (ContainsRow(key_roomtype))
                {
                    var rowContract = GetRowByKey(key_roomtype);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_roomtype;
                    rowContract.RoomTypeRow = true;
                    rowContract.Title = record.RoomTypeDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.PriceRateDescription);
                    rowContract.SortKeys.Add(record.RoomTypeDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 10;
                    rowContract.CustomBackground = null;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key_pricerate);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
            }
        }

        private void BuildPlanningInFullDetails(IEnumerable<RoomPriceRatePlanningRecord> records)
        {
            //Rows = new List<PlanningRowContract>();
            foreach (var record in records)
            {
                #region Hotel

                var key = record.HotelId.ToString();

                if (ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key;
                    rowContract.Title = record.HotelDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(string.Empty);
                    rowContract.SortKeys.Add(string.Empty);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }

                #endregion
                #region Category

                var key_category = key + "&" + record.CategoryId.ToString();

                if (ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key;
                    rowContract.Title = record.CategoryDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.CategoryDescription);
                    rowContract.SortKeys.Add(string.Empty);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.FontBold = true;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.LightGray;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                    
                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
                #region Price Rate

                var key_pricerate = key_category + "&" + record.PriceRateId.ToString();

                if (ContainsRow(key_pricerate))
                {
                    var rowContract = GetRowByKey(key_pricerate);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_pricerate;
                    rowContract.Title = record.PriceRateDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.CategoryDescription);
                    rowContract.SortKeys.Add(record.PriceRateDescription);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = true;
                    rowContract.LeftMargin = 10;
                    rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key_category);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
                #region Room Type

                var key_roomtype = key_pricerate + "&" + record.RoomTypeId.ToString();

                if (ContainsRow(key_roomtype))
                {
                    var rowContract = GetRowByKey(key_roomtype);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_roomtype;
                    rowContract.RoomTypeRow = true;
                    rowContract.Title = record.RoomTypeDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.CategoryDescription);
                    rowContract.SortKeys.Add(record.PriceRateDescription);
                    rowContract.SortKeys.Add(record.RoomTypeDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 15;
                    rowContract.CustomBackground = null;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key_pricerate);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
            }
        }
    }
}
