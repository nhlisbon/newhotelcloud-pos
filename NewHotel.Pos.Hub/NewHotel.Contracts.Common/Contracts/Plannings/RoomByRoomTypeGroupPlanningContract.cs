﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RoomByRoomTypeGroupPlanningContract : PlanningContract
    {
        public RoomByRoomTypeGroupPlanningContract(string totalRowTitle)
            : base(totalRowTitle)
        {
        }

        public override void BuildPlanning()
        {
            Rows.Clear();
            BuildPlanning(Records.Cast<RoomByRoomGroupPlanningRecord>());
            SortPlanning();
            AddTotalRow(Rows);
        }

        private void BuildPlanning(IEnumerable<RoomByRoomGroupPlanningRecord> records)
        {
            //Default Mode
            if (DetailColumns == null || DetailColumns.Count == 0)
                BuildPlanningInTotalGrouping(records);
            //Country & Room
            else if (DetailColumns.Count == 1 && ContainsColumn("trec_pk"))
                BuildPlanningInRoomGrouping(records);
        }
        
        private void BuildPlanningInTotalGrouping(IEnumerable<RoomByRoomGroupPlanningRecord> records)
        {
            //Rows = new List<PlanningRowContract>();
            foreach (var record in records)
            {
                #region Hotel

                var key = record.HotelId.ToString();

                if (ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key;
                    rowContract.Title = record.HotelDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }

                #endregion
                #region Room Group

                var key_roomgroup = key + "&" + record.RoomGroupId.ToString();

                if (ContainsRow(key_roomgroup))
                {
                    var rowContract = GetRowByKey(key_roomgroup);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_roomgroup;
                    rowContract.Title = record.RoomTypeGroupDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.RoomTypeGroupDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = null;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
            }
        }

        private void BuildPlanningInRoomGrouping(IEnumerable<RoomByRoomGroupPlanningRecord> records)
        {
            //Rows = new List<PlanningRowContract>();
            foreach (var record in records)
            {
                #region Hotel

                var key = record.HotelId.ToString();

                if (ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key;
                    rowContract.Title = record.HotelDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(string.Empty);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }

                #endregion
                #region Room Group

                var key_roomgroup = key + "&" + record.RoomGroupId.ToString();

                if (ContainsRow(key_roomgroup))
                {
                    var rowContract = GetRowByKey(key_roomgroup);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_roomgroup;
                    rowContract.Title = record.RoomTypeGroupDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.RoomTypeGroupDescription);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.FontBold = true;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238); ;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
                #region Room Type

                var key_roomtype = key_roomgroup + "&" + record.RoomTypeId.ToString();

                if (ContainsRow(key_roomtype))
                {
                    var rowContract = GetRowByKey(key_roomtype);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_roomtype;
                    rowContract.RoomTypeRow = true;
                    rowContract.Title = record.RoomTypeDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.RoomTypeGroupDescription);
                    rowContract.SortKeys.Add(record.RoomTypeDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 10;
                    rowContract.CustomBackground = null;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key_roomgroup);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
            }
        }
    }
}