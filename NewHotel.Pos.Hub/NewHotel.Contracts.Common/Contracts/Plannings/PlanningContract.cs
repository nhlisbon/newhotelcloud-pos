﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class NHPlanningSeparator
    {
        public Guid HotelId { get; set; }
        public int RowPosition { get; set; }
        public ARGBColor Color { get; set; }
    }

    [DataContract]
    [Serializable]
    public abstract class PlanningContract : BaseContract
    {
        public class RowsCollection : KeyedCollection<object, PlanningRowContract>
        {
            protected override object GetKeyForItem(PlanningRowContract item)
            {
                return item.Id;
            }
        }

        #region Constants

        private const int RecordCount = 50;

        #endregion
        #region Members

        private List<string> _detailsColumns;
        private string _totalRowTitle;

        [DataMember]
        internal readonly List<PlanningRowContract> _rows;
        // TODO
        //internal readonly RowsCollection _rows;

        #endregion
        #region Constructor

        public PlanningContract(string totalRowTitle)
        {
            _totalRowTitle = totalRowTitle;
            _rows = new List<PlanningRowContract>();
            // TODO
            //_rows = new RowsCollection();

            // Set Colors
            BackgroundColorFriday = ARGBColor.White;
            BackgroundColorMonday = ARGBColor.White;
            BackgroundColorSaturday = ARGBColor.Yellow;
            BackgroundColorSunday = ARGBColor.Yellow;
            BackgroundColorThursday = ARGBColor.White;
            BackgroundColorTuesday = ARGBColor.White;
            BackgroundColorWednesday = ARGBColor.White;

            WorkDateColor = ARGBColor.Cyan;
            NegativeColor = ARGBColor.LightPink;
            PositiveColor = ARGBColor.LightGreen;
            TotalColor = ARGBColor.LightGreen;

            SpecialDays = new Dictionary<DateTime, ValuePair<ARGBColor, string>>();
        }

        #endregion
        #region Local Properties

        public TypedList<PlanningBaseRecord> Records { get; set; }

        public List<string> DetailColumns
        {
            get
            {
                if (_detailsColumns == null)
                    _detailsColumns = new List<string>();

                return _detailsColumns;
            }
        }

        #endregion
        #region Public Properties

        [DataMember]
        public IDictionary<DateTime, ValuePair<ARGBColor, string>> SpecialDays { get; set; }

        [DataMember]
        public List<NHPlanningSeparator> Separators { get; set; }
        [DataMember]
        public short? DaysShowedBeforeWorkingDateInPlannings { get; set; }
        [DataMember]
        public DateTime From { get; set; }
        [DataMember]
        public DateTime To { get; set; }
        [DataMember]
        public DateTime WorkDate { get; set; }
        [DataMember]
        public int CurrentPage { get; set; }
        [DataMember]
        public int TotalPages { get; set; }
        [DataMember]
        public bool MultiProperty { get; set; }

        // Colors
        [DataMember]
        public ARGBColor BackgroundColorMonday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorTuesday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorWednesday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorThursday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorFriday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorSaturday { get; set; }
        [DataMember]
        public ARGBColor BackgroundColorSunday { get; set; }
        [DataMember]
        public ARGBColor WorkDateColor { get; set; }
        [DataMember]
        public ARGBColor NegativeColor { get; set; }
        [DataMember]
        public ARGBColor PositiveColor { get; set; }
        [DataMember]
        public ARGBColor TotalColor { get; set; }

        public List<PlanningRowContract> Rows { get { return _rows; } }
        // TODO
        //public IList<PlanningRowContract> Rows
        //{
        //    get { return _rows; }
        //}

        #endregion
        #region Protected Methods

        protected virtual PlanningRowContract CreateRow(bool isVirtual = false)
        {           
            return new PlanningRowContract(isVirtual);
        }

        protected virtual void RemoveRows()
        {
            Rows.RemoveAll(r => r.IsEmpty && !r.IsGroup);
        }

        protected virtual void SortRows()
        {
            Rows.Sort();
        }

        #endregion
        #region Public Methods

        public abstract void BuildPlanning();

        public void SortPlanning()
        {
            RemoveRows();
            SortRows();
        }

        public void AddTotalRow(IEnumerable<PlanningRowContract> rows)
        {
            if (rows.Any())
            {
                var totalRow = CreateRow(false);
                totalRow.TotalRow = true;
                totalRow.InitializeList((To - From).Days + 1, "0");

                // Details
                totalRow.Id = Guid.NewGuid().ToString();
                totalRow.Title = _totalRowTitle;

                // Styles
                totalRow.FontBold = true;
                totalRow.FontItalic = false;
                totalRow.LeftMargin = 5;
                totalRow.CustomBackground = null;

                var content = new int[rows.First().Content.Length];

                // Values
                foreach (var row in rows)
                {
                    if (row.TitleRow)
                        continue;

                    for (int i = 0; i < content.Length; i++)
                    {
                        if (!row.FontBold && !row.FontItalic && !row.HotelDescriptionRow)
                           content[i] = content[i] + int.Parse(row.Content[i]);
                    }
                }

                for (int i = 0; i < content.Length; i++)
                    totalRow.Content[i] = content[i].ToString();

                Rows.Add(totalRow);
            }
        }

        public bool ContainsColumn(string column)
        {
            foreach (var detailColumn in DetailColumns)
                if (string.Compare(detailColumn, column, StringComparison.InvariantCultureIgnoreCase) == 0)
                    return true;

            return false;
        }

        public bool ContainsRow(object id)
        {
            foreach (var rowContract in Rows)
            {
                if (rowContract.Id.Equals(id))
                    return true;
            }

            return false;
            // TODO
            //return _rows.Contains(id);
        }

        public PlanningRowContract GetRowByKey(object id)
        {
            foreach (var rowContract in Rows)
            {
                if (rowContract.Id.Equals(id))
                    return rowContract;
            }

            return null;
            // TODO
            //try
            //{
            //    return _rows[id];
            //}
            //catch (KeyNotFoundException)
            //{
            //    return null;
            //}
        }

        public int GetRowIndexByKey(object id)
        {
            for (int i = 0; i < Rows.Count; i++)
            {
                if (Rows[i].Id.Equals(id))
                    return i;
            }
            // TODO
            //var row = GetRowByKey(id);
            //if (row != null)
            //    return _rows.IndexOf(row);

            return -1;
        }

        public int GetLastRowIndexByParentKey(object id)
        {
            var firstMatch = false;
            for (int i = 0; i < Rows.Count; i++)
            {
                if (Rows[i].Id.ToString().Contains(id.ToString()))
                    firstMatch = true;

                if (firstMatch && !Rows[i].Id.ToString().Contains(id.ToString()))
                    return i - 1;
            }

            if (firstMatch)
                return Rows.Count - 1;

            return -1;
        }

        #endregion
        #region Paging Methods

        //Gets the number of groups
        private int GetGroupNumber()
        {
            int result = 0;
            foreach (var row in Rows)
            {
                if (row.IsGroup)
                    result++;
            }

            return result;
        }

        //Get the average size of all groups
        private int GetGroupSize()
        {
            var groups = new List<int>();
            var firstItem = true;
            int groupCount = 0;

            foreach (var row in Rows)
            {
                if (row.IsGroup)
                {
                    if (firstItem)
                        firstItem = false;
                    else
                    {
                        groups.Add(groupCount);
                        groupCount = 0;
                    }
                }
                else
                    groupCount++;
            }

            int sum = 0;
            foreach (int value in groups) { sum += value; }
            if (sum == 0) return 0;
            double average = sum / groups.Count;
            int result = int.Parse(Math.Round(average, 0).ToString());

            return result;
        }

        //Gets the number of groups per page, with an average of 50 items per request
        private int GetGroupPerPage()
        {
            int groupSize = GetGroupSize();
            if (groupSize == 0) return RecordCount;
            double groupAmount = RecordCount / groupSize;
            int result = int.Parse(Math.Round(groupAmount, 0).ToString());
            if (result == 0) return Rows.Count;

            return result;
        }

        //Gets the number of pages
        public int PageNumber()
        {
            var groupNumber = GetGroupNumber();
            var groupPerPage = GetGroupPerPage();
            if (groupNumber == 0 || groupNumber < groupPerPage)
                return 1;

            if (groupNumber % groupPerPage == 0)
                return (int)(groupNumber / groupPerPage);

            decimal division = groupNumber / groupPerPage;
            return decimal.ToInt32(Math.Round(division, 0)) + 1;
        }

        // Leave in rows the selected groups, according to provided page
        public void CropRows()
        {
            var groupPerPage = GetGroupPerPage();
            var page = CurrentPage;

            //index of first group to look for
            int groupIndex = (page - 1) * groupPerPage;

            //if index is greather than available groups return
            if (groupIndex > GetGroupNumber())
                return;

            //move looking for first group
            int currentIndex = -1;
            int groupsInserted = 0;
            var insertMode = false;

            var resultRows = new List<PlanningRowContract>();
            foreach (var row in Rows)
            {
                //increment index if is group
                if (row.IsGroup) currentIndex++;

                //if reach the goal index, start inserting
                if (currentIndex == groupIndex) insertMode = true;
                if (insertMode)
                {
                    //increment number of groups inserted
                    if (row.IsGroup) groupsInserted++;
                    //if current group index is greather than page, end
                    if (groupsInserted > groupPerPage) break;
                    //else add row
                    else resultRows.Add(row);
                }
            }

            Rows.Clear();
            foreach (var row in resultRows)
                Rows.Add(row);
        }

        #endregion
    }
}