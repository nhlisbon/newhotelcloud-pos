﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class AvailabilityPlanningContract : PlanningContract
    {
        #region Properties

        public string RoomsAvailablesAffectChannelTranslation { get; set; }
        public string RoomsAvailablesTranslation { get; set; }
        public string ExtraBedsTranslation { get; set; }
        public string CotsTranslation { get; set; }
        public string ReservedTranslation { get; set; }
        public string InWaitingListTranslation { get; set; }
        public string CancelledTranslation { get; set; }
        public string NoShowTranslation { get; set; }
        public string OverbookedTranslation { get; set; }
        public string AttemptsTranslation { get; set; }
        public string AllotmentsTranslation { get; set; }
        public string OccupancyTranslation { get; set; }
        public string UsedTranslation { get; set; }
        public string RoomInventoryTranslation { get; set; }
        public string ActiveTranslation { get; set; }
        public string OutOfOrderTranslation { get; set; }
        public string OwnerReservationTranslation { get; set; }
        public string OutOfRentalTranslation { get; set; }
        public string AllotmentNotGuaranteedOnRequestTranslation { get; set; }
        public string AllotmentNotGuaranteedAffectChannelTranslation { get; set; }
        public string AllotmentGuaranteedTranslation { get; set; }
        public string VirtualTranslation { get; set; }

        public bool Allotments { get; set; }
        public bool AllotmentsByRoomType { get; set; }
        public bool AllotmentsAsOccupied { get; set; }
        public bool Attempts { get; set; }
        public bool Summary { get; set; }
        public bool ExtendedSummary { get; set; }
        public bool WaitingList { get; set; }
        public bool Extras { get; set; }
        public bool Reservations { get; set; }
        public bool AvailabilityPerChannel { get; set; }
        public bool IsMultiHotel { get; set; }
        public bool PaintOwnerReservations { get; set; }

        public List<AvailabilityRoomTypeRecord> ListRoomType { get; set; }
        public List<AvailabilityAllotmenByRoomTypeRecord> ListAllotmentByRoomType { get; set; }
        public List<AvailabilityExtrasRecord> ListExtras { get; set; }
        public List<AvailabilityReservationsRecord> ListReservations { get; set; }
        public List<AvailabilityInventoryRecord> ListInventory { get; set; }
        public List<AvailabilityInventoryByRoomTypeRecord> ListInventoryExtended { get; set; }
        public List<OccupancyPercentRecord> ListOccupancy { get; set; }

        #endregion
        #region Constructor

        public AvailabilityPlanningContract(string totalRowTitle)
            : base(totalRowTitle)
        {
            ListRoomType = new List<AvailabilityRoomTypeRecord>();
            ListAllotmentByRoomType = new List<AvailabilityAllotmenByRoomTypeRecord>();
            ListExtras = new List<AvailabilityExtrasRecord>();
            ListReservations = new List<AvailabilityReservationsRecord>();
            ListInventory = new List<AvailabilityInventoryRecord>();
            ListInventoryExtended = new List<AvailabilityInventoryByRoomTypeRecord>();
            ListOccupancy = new List<OccupancyPercentRecord>();
        }

        #endregion
        #region Protected Methods

        protected override void RemoveRows()
        {
        }

        #endregion
        #region Public Methods

        public override void BuildPlanning()
        {
            Rows.Clear();

            var extraBedFirstItemKey = string.Empty;

            #region Extras

            if (Extras && ListExtras.Count > 0)
            {
                foreach (var record in ListExtras)
                {
                    var key = record.HotelId.ToString();
                    var date = (DateTime)record.Id;

                    #region Hotel

                    var rowContract = GetRowByKey(key);
                    if (ContainsRow(key))
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.TotalIncidental ?? 0).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.InitializeList((To - From).Days + 1, "0");

                        rowContract.Id = key;
                        rowContract.HotelId = record.HotelId;
                        rowContract.Title = record.HotelDescription;
                        rowContract.SortKeys.Add(record.HotelDescription);

                        rowContract.HotelDescriptionRow = true;

                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.TotalIncidental ?? 0).ToString();
                        Rows.Add(rowContract);
                    }

                    #endregion

                    #region Extras

                    var key_extraType = record.HotelId.ToString() + "&" + record.IncidentalId.ToString();

                    rowContract = GetRowByKey(key_extraType);
                    if (rowContract != null)
                    {                        
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.TotalIncidental ?? 0).ToString();
                    }
                    else
                    {
                        extraBedFirstItemKey = key_extraType;

                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.InitializeList((To - From).Days + 1, "0");

                        rowContract.Id = key_extraType;
                        rowContract.HotelId = record.HotelId;
                        rowContract.Title = record.IncidentalDescription;
                        rowContract.SortKeys.Add(record.HotelDescription);

                        rowContract.FontBold = false;
                        rowContract.FontItalic = false;
                        rowContract.LeftMargin = 10;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);

                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.TotalIncidental ?? 0).ToString();

                        var parentIndex = GetRowIndexByKey(key);
                        if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(parentIndex + 1, rowContract);
                    }

                    #endregion
                }
            }

            #endregion

            #region Availability by RoomTypes

            #region Hotel Availability

            foreach (var record in ListRoomType)
            {
                var key = record.HotelId.ToString();
                var date = (DateTime)record.Id;

                #region Hotel

                var rowContract = GetRowByKey(key);
                if (rowContract != null)
                {
                    var indexInContext = (date - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.AvailableRooms.HasValue) ? record.AvailableRooms.Value : 0)).ToString();
                }
                else
                {
                    rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    rowContract.Id = key;
                    rowContract.HotelId = record.HotelId;
                    rowContract.Title = record.HotelDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);

                    rowContract.HotelDescriptionRow = true;

                    var indexInContext = (date - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.AvailableRooms.HasValue) ? record.AvailableRooms.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }

                #endregion

                #region RoomType Total

                var key_roomTotal = record.HotelId.ToString() + "&" + RoomsAvailablesTranslation;

                rowContract = GetRowByKey(key_roomTotal);
                if (rowContract!= null)
                {
                    var indexInContext = (date - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.AvailableRooms.HasValue) ? record.AvailableRooms.Value : 0)).ToString();
                }
                else
                {
                    rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    rowContract.Id = key_roomTotal;
                    rowContract.HotelId = record.HotelId;
                    rowContract.Title = RoomsAvailablesTranslation;
                    rowContract.TotalRoomsRow = true;
                    rowContract.SortKeys.Add(record.HotelDescription);

                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);

                    var indexInContext = (date - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.AvailableRooms.HasValue) ? record.AvailableRooms.Value : 0)).ToString();

                    var parentIndex = GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion

                #region RoomType

                var key_roomType = key_roomTotal + "&" + record.RoomTypeId.ToString();

                rowContract = GetRowByKey(key_roomType);
                if (rowContract != null)
                {                
                    var indexInContext = (date - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.AvailableRooms.HasValue) ? record.AvailableRooms.Value : 0)).ToString();
                }
                else
                {
                    rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    rowContract.Id = key_roomType;
                    rowContract.RoomTypeRow = true;
                    rowContract.HotelId = record.HotelId;
                    rowContract.Title = record.RoomTypeDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.RoomTypeRow = true;

                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 10;
                    rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);

                    var indexInContext = (date - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.AvailableRooms.HasValue) ? record.AvailableRooms.Value : 0)).ToString();

                    var parentIndex = GetRowIndexByKey(key_roomTotal);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
            }

            #endregion

            #region Booking Channels Availability

            if (AvailabilityPerChannel)
            {
                foreach (var record in ListRoomType)
                {
                    var key = record.HotelId.ToString();
                    var date = (DateTime)record.Id;

                    #region Hotel

                    var rowContract = GetRowByKey(key);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + (record.AvailableRoomForChannels ?? 0)).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.InitializeList((To - From).Days + 1, "0");

                        rowContract.Id = key;
                        rowContract.HotelId = record.HotelId;
                        rowContract.Title = record.HotelDescription;
                        rowContract.SortKeys.Add(record.HotelDescription);

                        rowContract.HotelDescriptionRow = true;

                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + (record.AvailableRoomForChannels ?? 0)).ToString();
                        Rows.Add(rowContract);
                    }

                    #endregion

                    #region RoomType Total

                    var key_roomTotal = record.HotelId.ToString() + "&" + RoomsAvailablesAffectChannelTranslation;

                    rowContract = GetRowByKey(key_roomTotal);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + (record.AvailableRoomForChannels ?? 0)).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.InitializeList((To - From).Days + 1, "0");

                        rowContract.HotelId = record.HotelId;
                        rowContract.Title = RoomsAvailablesAffectChannelTranslation;
                        rowContract.TotalRoomsRow = true;
                        rowContract.Id = key_roomTotal;
                        rowContract.SortKeys.Add(record.HotelDescription);

                        rowContract.FontBold = false;
                        rowContract.FontItalic = false;
                        rowContract.LeftMargin = 5;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);

                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + (record.AvailableRoomForChannels ?? 0)).ToString();

                        var parentIndex = GetRowIndexByKey(key);
                        if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(parentIndex + 1, rowContract);
                    }

                    #endregion

                    #region RoomType

                    var key_roomType = key_roomTotal + "&" + record.RoomTypeId.ToString();

                    rowContract = GetRowByKey(key_roomType);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + (record.AvailableRoomForChannels ?? 0)).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.InitializeList((To - From).Days + 1, "0");

                        rowContract.Id = key_roomType;
                        rowContract.RoomTypeRow = true;
                        rowContract.HotelId = record.HotelId;
                        rowContract.Title = record.RoomTypeDescription;
                        rowContract.SortKeys.Add(record.HotelDescription);
                        rowContract.RoomTypeRow = true;

                        rowContract.FontBold = false;
                        rowContract.FontItalic = false;
                        rowContract.LeftMargin = 10;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);

                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + (record.AvailableRoomForChannels ?? 0)).ToString();

                        var parentIndex = GetRowIndexByKey(key_roomTotal);
                        if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(parentIndex + 1, rowContract);
                    }

                    #endregion
                }
            }

            #endregion

            #region Take Allotments from Availability Directly

            if (AllotmentsAsOccupied)
            {
                foreach (var record in ListAllotmentByRoomType)
                {
                    var key = record.HotelId.ToString();
                    var key_roomTotal = key + "&" + RoomsAvailablesTranslation;
                    var key_roomType = key_roomTotal + "&" + record.RoomTypeId.ToString();
                    var key_roomTotal_channel = key + "&" + RoomsAvailablesAffectChannelTranslation;
                    var key_roomType_channel = key_roomTotal_channel + "&" + record.RoomTypeId.ToString();
                    var date = (DateTime)record.Id;

                    var onrequest = record.Contrated + record.ContractedUsed; 
                    var onrequestChannels = record.ContratedAffectChannel + record.ContractedAffectChannelUsed;
                    var releaseDays = record.OutOfReleaseOnRequest;
                    var releaseDaysChannels = record.OutOfReleaseAffectChannel;

                    #region RoomType Total Adjustments

                    var rowContract = GetRowByKey(key_roomTotal);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) - onrequest - onrequestChannels + releaseDays + releaseDaysChannels).ToString();
                    }

                    rowContract = GetRowByKey(key_roomTotal_channel);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) - onrequest + releaseDays).ToString();
                    }

                    #endregion

                    #region RoomType Group Adjustments

                    rowContract = GetRowByKey(key_roomType);
                    if (rowContract!= null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) - onrequest - onrequestChannels + releaseDays + releaseDaysChannels).ToString();
                    }

                    rowContract = GetRowByKey(key_roomType_channel);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) - onrequest + releaseDays).ToString();
                    }

                    #endregion
                }
            }
             
            #endregion

            #endregion

            #region Reservations

            if (Reservations && ListReservations.Count > 0)
            {
                foreach (var record in ListReservations)
                {
                    var key = record.HotelId.ToString();
                    var key_rese = key + "&" + ReservedTranslation;
                    var key_canc = key + "&" + CancelledTranslation;
                    var key_nshw = key + "&" + NoShowTranslation;
                    var key_over = key + "&" + OverbookedTranslation;
                    var date = (DateTime)record.Id;

                    #region Reservations

                    var rowContract = GetRowByKey(key_rese);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Reservations).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.Id = key_rese;
                        rowContract.Title = ReservedTranslation;
                        rowContract.FontBold = false;
                        rowContract.FontItalic = false;
                        rowContract.LeftMargin = 5;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                        rowContract.HotelId = record.HotelId;

                        rowContract.InitializeList((To - From).Days + 1, "0");
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Reservations).ToString();

                        var index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }

                    #endregion

                    #region WaitingList

                    if (WaitingList)
                    {
                        var key_wait = key + "&" + InWaitingListTranslation;

                        rowContract = GetRowByKey(key_wait);
                        if (rowContract!= null)
                        {
                            var indexInContext = (date - From).Days;
                            rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.WaitingList).ToString();
                        }
                        else
                        {
                            rowContract = CreateRow(record.IsVirtual);
                            rowContract.Id = key_wait;
                            rowContract.Title = InWaitingListTranslation;
                            rowContract.FontBold = false;
                            rowContract.FontItalic = false;
                            rowContract.LeftMargin = 10;
                            rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                            rowContract.HotelId = record.HotelId;

                            rowContract.InitializeList((To - From).Days + 1, "0");
                            var indexInContext = (date - From).Days;
                            rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.WaitingList).ToString();

                            var index = GetLastRowIndexByParentKey(key);
                            if (index + 1 == Rows.Count) Rows.Add(rowContract);
                            else Rows.Insert(index + 1, rowContract);
                        }
                    }

                    #endregion

                    #region Cancelled

                    rowContract = GetRowByKey(key_canc);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Cancelled).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.Id = key_canc;
                        rowContract.Title = CancelledTranslation;
                        rowContract.FontBold = false;
                        rowContract.FontItalic = false;
                        rowContract.LeftMargin = 5;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                        rowContract.HotelId = record.HotelId;

                        rowContract.InitializeList((To - From).Days + 1, "0");
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Cancelled).ToString();

                        var index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }

                    #endregion

                    #region NoShow

                    rowContract = GetRowByKey(key_nshw);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.NoShows).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.Id = key_nshw;
                        rowContract.Title = NoShowTranslation;
                        rowContract.FontBold = false;
                        rowContract.FontItalic = false;
                        rowContract.LeftMargin = 5;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                        rowContract.HotelId = record.HotelId;

                        rowContract.InitializeList((To - From).Days + 1, "0");
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.NoShows).ToString();

                        var index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }

                    #endregion

                    #region Overbooked

                    rowContract = GetRowByKey(key_over);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Overbooked).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.Id = key_over;
                        rowContract.Title = OverbookedTranslation;
                        rowContract.FontBold = false;
                        rowContract.FontItalic = false;
                        rowContract.LeftMargin = 5;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                        rowContract.HotelId = record.HotelId;

                        rowContract.InitializeList((To - From).Days + 1, "0");
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Overbooked).ToString();

                        var index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }

                    #endregion

                    #region Owner Reservations

                    if (PaintOwnerReservations)
                    {
                        var key_owner = key + "&" + OwnerReservationTranslation;

                        rowContract = GetRowByKey(key_owner);
                        if (rowContract != null)
                        {
                            var indexInContext = (date - From).Days;
                            rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.OwnerReservations).ToString();
                        }
                        else
                        {
                            rowContract = CreateRow(record.IsVirtual);
                            rowContract.Id = key_owner;
                            rowContract.Title = OwnerReservationTranslation;
                            rowContract.FontBold = false;
                            rowContract.FontItalic = false;
                            rowContract.LeftMargin = 5;
                            rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                            rowContract.HotelId = record.HotelId;

                            rowContract.InitializeList((To - From).Days + 1, "0");
                            var indexInContext = (date - From).Days;
                            rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.OwnerReservations).ToString();

                            var index = GetLastRowIndexByParentKey(key);
                            if (index + 1 == Rows.Count) Rows.Add(rowContract);
                            else Rows.Insert(index + 1, rowContract);
                        }
                    }

                    #endregion

                    #region Attempts

                    if (Attempts)
                    {
                        var key_attempt = key + "&" + AttemptsTranslation;

                        rowContract = GetRowByKey(key_attempt);
                        if (rowContract != null)
                        {
                            var indexInContext = (date - From).Days;
                            rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Attempts).ToString();
                        }
                        else
                        {
                            rowContract = CreateRow(record.IsVirtual);
                            rowContract.Id = key_attempt;
                            rowContract.Title = AttemptsTranslation;
                            rowContract.FontBold = false;
                            rowContract.FontItalic = false;
                            rowContract.LeftMargin = 5;
                            rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                            rowContract.HotelId = record.HotelId;

                            rowContract.InitializeList((To - From).Days + 1, "0");
                            var indexInContext = (date - From).Days;
                            rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Attempts).ToString();

                            var index = GetLastRowIndexByParentKey(key);
                            if (index + 1 == Rows.Count) Rows.Add(rowContract);
                            else Rows.Insert(index + 1, rowContract);
                        }
                    }

                    #endregion
                }
            }

            #endregion

            #region Allotment

            if (Allotments)
            {
                foreach (var record in ListAllotmentByRoomType)
                {
                    var key = record.HotelId.ToString();
                    var date = (DateTime)record.Id;

                    #region Contracted, Used

                    var key_cont = key + "&" + AllotmentNotGuaranteedOnRequestTranslation;
                    var key_cuse = key_cont + UsedTranslation;

                    var rowContract = GetRowByKey(key_cont);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Contrated).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.Id = key_cont;
                        rowContract.Title = AllotmentNotGuaranteedOnRequestTranslation;
                        rowContract.FontBold = false;
                        rowContract.FontItalic = false;
                        rowContract.LeftMargin = 5;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                        rowContract.HotelId = record.HotelId;

                        rowContract.InitializeList((To - From).Days + 1, "0");
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Contrated).ToString();

                        var index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }

                    rowContract = GetRowByKey(key_cuse);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.ContractedUsed).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.Id = key_cuse;
                        rowContract.Title = UsedTranslation;
                        rowContract.FontBold = false;
                        rowContract.FontItalic = true;
                        rowContract.LeftMargin = 10;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                        rowContract.HotelId = record.HotelId;

                        rowContract.InitializeList((To - From).Days + 1, "0");
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.ContractedUsed).ToString();

                        var index = GetLastRowIndexByParentKey(key_cont);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }

                    #endregion

                    #region AffectChannel, Used

                    var key_afch = key + "&" + AllotmentNotGuaranteedAffectChannelTranslation;
                    var key_ause = key_afch + UsedTranslation;

                    rowContract = GetRowByKey(key_afch);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.ContratedAffectChannel).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.Id = key_afch;
                        rowContract.Title = AllotmentNotGuaranteedAffectChannelTranslation;
                        rowContract.FontBold = false;
                        rowContract.FontItalic = false;
                        rowContract.LeftMargin = 5;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                        rowContract.HotelId = record.HotelId;

                        rowContract.InitializeList((To - From).Days + 1, "0");
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.ContratedAffectChannel).ToString();

                        var index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }

                    rowContract = GetRowByKey(key_ause);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.ContractedAffectChannelUsed).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.Id = key_ause;
                        rowContract.Title = UsedTranslation;
                        rowContract.FontBold = false;
                        rowContract.FontItalic = true;
                        rowContract.LeftMargin = 10;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                        rowContract.HotelId = record.HotelId;

                        rowContract.InitializeList((To - From).Days + 1, "0");
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.ContractedAffectChannelUsed).ToString();

                        var index = GetLastRowIndexByParentKey(key_afch);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }

                    #endregion

                    #region Garanteed, Used

                    var key_gara = key + "&" + AllotmentGuaranteedTranslation;
                    var key_guse = key_gara + UsedTranslation;

                    rowContract = GetRowByKey(key_gara);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Garanteed).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.Id = key_gara;
                        rowContract.Title = AllotmentGuaranteedTranslation;
                        rowContract.FontBold = false;
                        rowContract.FontItalic = false;
                        rowContract.LeftMargin = 5;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                        rowContract.HotelId = record.HotelId;

                        rowContract.InitializeList((To - From).Days + 1, "0");
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Garanteed).ToString();

                        var index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }

                    rowContract = GetRowByKey(key_guse);
                    if (rowContract != null)
                    {                       
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.GaranteedUsed).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.Id = key_guse;
                        rowContract.Title = UsedTranslation;
                        rowContract.FontBold = false;
                        rowContract.FontItalic = true;
                        rowContract.LeftMargin = 10;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                        rowContract.HotelId = record.HotelId;

                        rowContract.InitializeList((To - From).Days + 1, "0");
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.GaranteedUsed).ToString();

                        var index = GetLastRowIndexByParentKey(key_gara);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }

                    #endregion
                }
            }

            if (AllotmentsByRoomType)
            {
                foreach (var record in ListAllotmentByRoomType)
                {
                    var key = record.HotelId.ToString();
                    var date = (DateTime)record.Id;

                    #region Contracted by room type

                    var key_cont = key + "&" + AllotmentNotGuaranteedOnRequestTranslation;
                    var key_cont_roomType = key_cont + "&" + record.RoomTypeDescription;

                    var rowContract = GetRowByKey(key_cont_roomType);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Contrated - record.ContractedUsed).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.InitializeList((To - From).Days + 1, "0");

                        rowContract.Id = key_cont_roomType;
                        rowContract.RoomTypeRow = true;
                        rowContract.HotelId = record.HotelId;
                        rowContract.Title = record.RoomTypeDescription;
                        rowContract.SortKeys.Add(record.HotelDescription);
                        rowContract.RoomTypeRow = true;

                        rowContract.FontBold = false;
                        rowContract.FontItalic = true;
                        rowContract.LeftMargin = 15;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);

                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Contrated - record.ContractedUsed).ToString();

                        var parentIndex = GetRowIndexByKey(key_cont);
                        if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(parentIndex + 1, rowContract);
                    }

                    #endregion

                    #region Affect Channel by room type

                    var key_afch = key + "&" + AllotmentNotGuaranteedAffectChannelTranslation;
                    var key_afch_roomType = key_afch + "&" + record.RoomTypeDescription;

                    rowContract = GetRowByKey(key_afch_roomType);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.ContratedAffectChannel - record.ContractedAffectChannelUsed).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.InitializeList((To - From).Days + 1, "0");

                        rowContract.Id = key_afch_roomType;
                        rowContract.RoomTypeRow = true;
                        rowContract.HotelId = record.HotelId;
                        rowContract.Title = record.RoomTypeDescription;
                        rowContract.SortKeys.Add(record.HotelDescription);
                        rowContract.RoomTypeRow = true;

                        rowContract.FontBold = false;
                        rowContract.FontItalic = true;
                        rowContract.LeftMargin = 15;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);

                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.ContratedAffectChannel - record.ContractedAffectChannelUsed).ToString();

                        var parentIndex = GetRowIndexByKey(key_afch);
                        if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(parentIndex + 1, rowContract);
                    }

                    #endregion

                    #region Garanteed by room type

                    var key_gara = key + "&" + AllotmentGuaranteedTranslation;
                    var key_gara_roomType = key_gara + "&" + record.RoomTypeDescription;

                    rowContract = GetRowByKey(key_gara_roomType);
                    if (rowContract != null)
                    {                        
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Garanteed - record.GaranteedUsed).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.InitializeList((To - From).Days + 1, "0");

                        rowContract.Id = key_gara_roomType;
                        rowContract.RoomTypeRow = true;
                        rowContract.HotelId = record.HotelId;
                        rowContract.Title = record.RoomTypeDescription;
                        rowContract.SortKeys.Add(record.HotelDescription);
                        rowContract.RoomTypeRow = true;

                        rowContract.FontBold = false;
                        rowContract.FontItalic = true;
                        rowContract.LeftMargin = 15;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);

                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Garanteed - record.GaranteedUsed).ToString();

                        var parentIndex = GetRowIndexByKey(key_gara);
                        if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(parentIndex + 1, rowContract);
                    }

                    #endregion
                }
            }

            #endregion

            #region Allotments Release Days

            if (Allotments)
            {
                foreach (var record in ListAllotmentByRoomType)
                {
                    var key = record.HotelId.ToString();
                    var date = (DateTime)record.Id;

                    if (record.OutOfReleaseAffectChannel > 0)
                    {
                        var indexInContext = (date - From).Days;
                        var key_afch = key + "&" + AllotmentNotGuaranteedAffectChannelTranslation;

                        var rowContract = GetRowByKey(key_afch);
                        if (rowContract != null)
                            rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) - record.OutOfReleaseAffectChannel).ToString();

                        if (AllotmentsByRoomType)
                        {
                            var key_afch_roomType = key_afch + "&" + record.RoomTypeDescription;

                            rowContract = GetRowByKey(key_afch_roomType);
                            if (rowContract != null)
                                rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) - record.OutOfReleaseAffectChannel).ToString();
                        }
                    }

                    if  (record.OutOfReleaseOnRequest > 0)
                    {
                        var indexInContext = (date - From).Days;
                        var key_onre = key + "&" + AllotmentNotGuaranteedOnRequestTranslation;

                        var rowContract = GetRowByKey(key_onre);
                        if (rowContract != null)
                            rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) - record.OutOfReleaseOnRequest).ToString();

                        if (AllotmentsByRoomType)
                        {
                            var key_onre_roomType = key_onre + "&" + record.RoomTypeDescription;

                            rowContract = GetRowByKey(key_onre_roomType);
                            if (rowContract != null)
                                rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) - record.OutOfReleaseOnRequest).ToString();
                        }
                    }
                }
            }

            #endregion

            #region Inventory

            if (Summary && ListInventory.Count > 0)
            {
                foreach (var record in ListInventory.OrderBy(r => r.IsVirtual ? "1" : "0"))
                {
                    var key = record.HotelId.ToString();
                    var key_virt = record.IsVirtual.ToString();
                    var key_inve = key + "&" + RoomInventoryTranslation + "&" + key_virt;
                    var key_avai = key + "&" + ActiveTranslation + "&" + key_virt;
                    var key_inac = key + "&" + OutOfOrderTranslation + "&" + key_virt;
                    var key_rent = key + "&" + OutOfRentalTranslation + "&" + key_virt;
                    var date = (DateTime)record.Id;

                    #region Inventory

                    var rowContract = GetRowByKey(key_inve);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Inventory).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.Id = key_inve;
                        var title = RoomInventoryTranslation;
                        if (record.IsVirtual)
                            title += " " + VirtualTranslation;
                        rowContract.Title = title;
                        rowContract.FontBold = false;
                        rowContract.FontItalic = false;
                        rowContract.LeftMargin = 5;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                        rowContract.HotelId = record.HotelId;
                        
                        rowContract.InitializeList((To - From).Days + 1, "0");
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Inventory).ToString();

                        var index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }

                    #endregion

                    #region Available

                    rowContract = GetRowByKey(key_avai);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Active).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.Id = key_avai;
                        rowContract.Title = ActiveTranslation;
                        rowContract.FontBold = false;
                        rowContract.FontItalic = false;
                        rowContract.LeftMargin = 10;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                        rowContract.HotelId = record.HotelId;

                        rowContract.InitializeList((To - From).Days + 1, "0");
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Active).ToString();

                        var index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }

                    #endregion

                    #region Out of Order

                    rowContract = GetRowByKey(key_inac);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Inactive).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.Id = key_inac;
                        rowContract.Title = OutOfOrderTranslation;
                        rowContract.FontBold = false;
                        rowContract.FontItalic = false;
                        rowContract.LeftMargin = 10;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                        rowContract.HotelId = record.HotelId;

                        rowContract.InitializeList((To - From).Days + 1, "0");
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Inactive).ToString();

                        var index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }

                    #endregion

                    #region Out of Rental

                    if (PaintOwnerReservations)
                    {
                        rowContract = GetRowByKey(key_rent);
                        if (rowContract != null)
                        {
                            var indexInContext = (date - From).Days;
                            rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.OutOfRental).ToString();
                        }
                        else
                        {
                            rowContract = CreateRow(record.IsVirtual);
                            rowContract.Id = key_rent;
                            rowContract.Title = OutOfRentalTranslation;
                            rowContract.FontBold = false;
                            rowContract.FontItalic = false;
                            rowContract.LeftMargin = 10;
                            rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                            rowContract.HotelId = record.HotelId;

                            rowContract.InitializeList((To - From).Days + 1, "0");
                            var indexInContext = (date - From).Days;
                            rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.OutOfRental).ToString();

                            var index = GetLastRowIndexByParentKey(key);
                            if (index + 1 == Rows.Count) Rows.Add(rowContract);
                            else Rows.Insert(index + 1, rowContract);
                        }
                    }

                    #endregion
                }
            }

            #endregion

            #region Extended Summary

            if (ExtendedSummary && ListInventoryExtended.Count > 0)
            {
                foreach (var record in ListInventoryExtended)
                {
                    var key = record.HotelId.ToString();
                    var key_virt = record.IsVirtual.ToString();
                    var key_avai = key + "&" + ActiveTranslation + "&" + key_virt;
                    var key_tial = key_avai + "&" + record.RoomTypeId.ToString();
                    var date = (DateTime)record.Id;

                    var rowContract = GetRowByKey(key_tial);
                    if (ContainsRow(key_tial))
                    {                        
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Active).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.Id = key_tial;
                        rowContract.RoomTypeRow = true;
                        rowContract.Title = record.RoomTypeDescription;
                        rowContract.FontBold = false;
                        rowContract.FontItalic = true;
                        rowContract.LeftMargin = 15;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                        rowContract.HotelId = record.HotelId;

                        rowContract.SortKeys.Add(record.HotelDescription);
                        rowContract.InitializeList((To - From).Days + 1, "0");
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Active).ToString();

                        var index = GetLastRowIndexByParentKey(key_avai);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }
                }
            }

            #endregion

            #region Separators

            Separators = new List<NHPlanningSeparator>();

            for (int i = 0; i < Rows.Count; i++)
            {
                var rowContract = Rows[i];
                if (rowContract.Id.ToString().IndexOf(ReservedTranslation) != -1) 
                    Separators.Add(new NHPlanningSeparator() { HotelId = rowContract.HotelId, RowPosition = (IsMultiHotel) ? i + 6 : i - 1, Color = ARGBColor.Black });
                if (rowContract.Id.ToString().IndexOf(AttemptsTranslation) != -1) 
                    Separators.Add(new NHPlanningSeparator() { HotelId = rowContract.HotelId, RowPosition = (IsMultiHotel) ? i + 6 : i - 1, Color = ARGBColor.Black });
                if (rowContract.Id.ToString().IndexOf(AllotmentNotGuaranteedOnRequestTranslation) != -1 && !rowContract.FontItalic) 
                    Separators.Add(new NHPlanningSeparator() { HotelId = rowContract.HotelId, RowPosition = (IsMultiHotel) ? i + 6 : i - 1, Color = ARGBColor.Black });
                if (rowContract.Id.ToString().IndexOf(RoomInventoryTranslation) != -1) 
                    Separators.Add(new NHPlanningSeparator() { HotelId = rowContract.HotelId, RowPosition = (IsMultiHotel) ? i + 6 : i - 1, Color = ARGBColor.Black });
            }

            if (ListRoomType.Count > 0 && !string.IsNullOrEmpty(extraBedFirstItemKey))
            {
                var index = GetRowIndexByKey(extraBedFirstItemKey);
                var rowContract = GetRowByKey(extraBedFirstItemKey);
                Separators.Add(new NHPlanningSeparator()
                {
                    HotelId = rowContract.HotelId,
                    RowPosition = IsMultiHotel ? index + 6 : index - 1,
                    Color = ARGBColor.Black
                });
            }

            #endregion

            #region Occupancy

            if (ListOccupancy.Count > 0)
            {
                const decimal precision = 100M;

                var rows = new List<PlanningRowContract>();

                foreach (var record in ListOccupancy)
                {
                    var key = record.HotelId.ToString();
                    var key_data = key + "&" + OccupancyTranslation + "0";
                    var date = (DateTime)record.Id;

                    var rowContract = GetRowByKey(key_data);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + (int)(record.Occupancy * precision)).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.Id = key_data;
                        rowContract.Title = OccupancyTranslation;
                        rowContract.FontBold = false;
                        rowContract.FontItalic = false;
                        rowContract.LeftMargin = 5;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                        rowContract.HotelId = record.HotelId;

                        rowContract.InitializeList((To - From).Days + 1, "0");
                        var indexInContext = (date - From).Days;

                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + (int)(record.Occupancy * precision)).ToString();

                        var index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);

                        rows.Add(rowContract);
                    }
                }

                foreach (var record in ListOccupancy)
                {
                    var key = record.HotelId.ToString();
                    var key_data = key + "&" + OccupancyTranslation + "1";
                    var date = (DateTime)record.Id;

                    var rowContract = GetRowByKey(key_data);
                    if (rowContract != null)
                    {
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + (int)(record.OccupancyReal * precision)).ToString();
                    }
                    else
                    {
                        rowContract = CreateRow(record.IsVirtual);
                        rowContract.Id = key_data;
                        rowContract.Title = OccupancyTranslation + " (R)";
                        rowContract.FontBold = false;
                        rowContract.FontItalic = false;
                        rowContract.LeftMargin = 5;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                        rowContract.HotelId = record.HotelId;

                        rowContract.InitializeList((To - From).Days + 1, "0");
                        var indexInContext = (date - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + (int)(record.OccupancyReal * precision)).ToString();

                        var index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);

                        rows.Add(rowContract);
                    }
                }

                foreach (var row in rows)
                {
                    for (int index = 0; index < row.Content.Length; index++)
                        row.Content[index] = Math.Round(int.Parse(row.Content[index]) / precision, 1).ToString() + "%";
                }
            }

            #endregion

            #region Multi Hotel

            if (IsMultiHotel)
            {
                #region Inventory

                var rowInventory = new PlanningRowContract() { Title = RoomInventoryTranslation, Id = Guid.Empty.ToString(), LeftMargin = 5 };
                var rowActive = new PlanningRowContract() { Title = ActiveTranslation, Id = Guid.Empty.ToString(), LeftMargin = 10 };
                var rowOutOfOrder = new PlanningRowContract() { Title = OutOfOrderTranslation, Id = Guid.Empty.ToString(), LeftMargin = 10 };
                var rowOutOfRental = new PlanningRowContract() { Title = OutOfRentalTranslation, Id = Guid.Empty.ToString(), LeftMargin = 10 };

                var length = (To - From).Days + 1;
                rowInventory.InitializeList(length, "0");
                rowActive.InitializeList(length, "0");
                rowOutOfOrder.InitializeList(length, "0");
                rowOutOfRental.InitializeList(length, "0");

                foreach (var record in ListInventory)
                {
                    var date = (DateTime)record.Id;

                    var indexInContext = (date - From).Days;
                    rowInventory.Content[indexInContext] = (int.Parse(rowInventory.Content[indexInContext]) + record.Inventory).ToString();
                    rowActive.Content[indexInContext] = (int.Parse(rowActive.Content[indexInContext]) + record.Active).ToString();
                    rowOutOfOrder.Content[indexInContext] = (int.Parse(rowOutOfOrder.Content[indexInContext]) + record.Inactive).ToString();
                    rowOutOfRental.Content[indexInContext] = (int.Parse(rowOutOfRental.Content[indexInContext]) + record.OutOfRental).ToString();
                }

                Rows.Insert(0, rowOutOfOrder);
                Rows.Insert(0, rowOutOfRental);
                Rows.Insert(0, rowActive);
                Rows.Insert(0, rowInventory);

                #endregion

                #region Reservations

                var rowReservations = new PlanningRowContract() { Title = ReservedTranslation, Id = Guid.Empty.ToString(), LeftMargin = 5 };
                rowReservations.InitializeList((To - From).Days + 1, "0");
                foreach (var record in ListReservations)
                {
                    var date = (DateTime)record.Id;

                    var indexInContext = (date - From).Days;
                    rowReservations.Content[indexInContext] = (int.Parse(rowReservations.Content[indexInContext]) + record.Reservations).ToString();
                }

                Rows.Insert(0, rowReservations);

                #endregion

                #region Rooms Available

                var rowRooms = new PlanningRowContract() { Title = RoomsAvailablesTranslation, Id = Guid.Empty.ToString(), LeftMargin = 5 };
                rowRooms.InitializeList((To - From).Days + 1, "0");
                foreach (var record in ListRoomType)
                {
                    var date = (DateTime)record.Id;

                    var indexInContext = (date - From).Days;
                    rowRooms.Content[indexInContext] = (int.Parse(rowRooms.Content[indexInContext]) + (record.AvailableRooms ?? 0)).ToString();
                }

                Rows.Insert(0, rowRooms);

                #endregion

                var rowHeader = new PlanningRowContract() { Title = "Totals", Id = Guid.Empty.ToString(), HotelDescriptionRow = true };
                Rows.Insert(0, rowHeader);
            }

            #endregion
        }

        #endregion
    }
}