﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PaxsMealsPlanningContract : PlanningContract
    {
        public string BreakfastTraslate { get; set; }
        public string LunchTraslate { get; set; }
        public string DinnerTraslate { get; set; }

        public PaxsMealsPlanningContract(string totalRowTitle)
            : base(totalRowTitle)
        {
        }

        public override void BuildPlanning()
        {
            Rows.Clear();
            BuildPlanning(Records.Cast<PaxsMealsPlanningRecord>());
        }

        private void BuildPlanning(IEnumerable<PaxsMealsPlanningRecord> records)
        {
            //Default Mode
            if (DetailColumns == null || DetailColumns.Count == 0)
                BuildPlanningInTotalGrouping(records);
        }

        private void BuildPlanningInTotalGrouping(IEnumerable<PaxsMealsPlanningRecord> records)
        {
            //Rows = new List<PlanningRowContract>();
            foreach (var record in records)
            {
                if (ContainsRow("Breakfast"))
                {
                    var rowContractB = GetRowByKey("Breakfast");
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContractB.Id = "Breakfast";
                    rowContractB.Title = BreakfastTraslate;
                    rowContractB.Content[indexInContext] = (int.Parse(rowContractB.Content[indexInContext]) + ((record.Breakfast.HasValue) ? record.Breakfast.Value : 0)).ToString();
                }

                if (ContainsRow("Lunch"))
                {
                    var rowContractL = GetRowByKey("Lunch");
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContractL.Id = "Lunch";
                    rowContractL.Title = LunchTraslate;
                    rowContractL.Content[indexInContext] = (int.Parse(rowContractL.Content[indexInContext]) + ((record.Lunch.HasValue) ? record.Lunch.Value : 0)).ToString();
                }

                if (ContainsRow("Dinner"))
                {
                    var rowContractD = GetRowByKey("Dinner");
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContractD.Id = "Dinner";
                    rowContractD.Title = DinnerTraslate;
                    rowContractD.Content[indexInContext] = (int.Parse(rowContractD.Content[indexInContext]) + ((record.Dinner.HasValue) ? record.Dinner.Value : 0)).ToString();
                }
                else
                {
                    var rowContractB = CreateRow(record.IsVirtual);
                    rowContractB.InitializeList((To - From).Days + 1, "0");
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContractB.Id = "Breakfast";
                    rowContractB.Content[indexInContext] = (int.Parse(rowContractB.Content[indexInContext]) + ((record.Breakfast.HasValue) ? record.Breakfast.Value : 0)).ToString();

                    var rowContractL = CreateRow(record.IsVirtual);
                    rowContractL.InitializeList((To - From).Days + 1, "0");
                    var indexInContextL = ((DateTime)record.Id - From).Days;
                    rowContractL.Id = "Lunch";
                    rowContractL.Content[indexInContextL] = (int.Parse(rowContractL.Content[indexInContextL]) + ((record.Lunch.HasValue) ? record.Lunch.Value : 0)).ToString();

                    var rowContractD = CreateRow(record.IsVirtual);
                    rowContractD.InitializeList((To - From).Days + 1, "0");
                    var indexInContextD = ((DateTime)record.Id - From).Days;
                    rowContractD.Id = "Dinner";
                    rowContractD.Content[indexInContextD] = (int.Parse(rowContractD.Content[indexInContextD]) + ((record.Dinner.HasValue) ? record.Dinner.Value : 0)).ToString();

                    //Styles
                    rowContractB.FontBold = rowContractL.FontBold = rowContractD.FontBold = false;
                    rowContractB.FontItalic = rowContractL.FontItalic = rowContractD.FontItalic = false;
                    rowContractB.LeftMargin = rowContractL.LeftMargin = rowContractD.LeftMargin = 5;
                    rowContractB.CustomBackground = rowContractL.CustomBackground = rowContractD.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238); ;

                    //Values
                    indexInContext = ((DateTime)record.Id - From).Days;
                    rowContractB.Content[indexInContext] = (int.Parse(rowContractB.Content[indexInContext]) + ((record.Breakfast.HasValue) ? record.Breakfast.Value : 0)).ToString();
                    Rows.Add(rowContractB);
                    rowContractL.Content[indexInContext] = (int.Parse(rowContractL.Content[indexInContext]) + ((record.Lunch.HasValue) ? record.Lunch.Value : 0)).ToString();
                    Rows.Add(rowContractL);
                    rowContractD.Content[indexInContext] = (int.Parse(rowContractD.Content[indexInContext]) + ((record.Dinner.HasValue) ? record.Dinner.Value : 0)).ToString();
                    Rows.Add(rowContractD);
                }               
            }
        }
    }
}