﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RoomMarketSegmentPlanningContract : PlanningContract
    {
        public RoomMarketSegmentPlanningContract(string totalRowTitle)
            : base(totalRowTitle)
        {
        }

        public override void BuildPlanning()
        {
            Rows.Clear();
            BuildPlanning(Records.Cast<RoomMarketSegmentPlanningRecord>());
            SortPlanning();
            AddTotalRow(Rows);
        }

        private void BuildPlanning(IEnumerable<RoomMarketSegmentPlanningRecord> records)
        {
            //Default Mode
            if (DetailColumns == null || DetailColumns.Count == 0)
                BuildPlanningInTotalGrouping(records);
            //Segment & Group
            else if (DetailColumns.Count == 1 && ContainsColumn("grme_pk"))
                BuildPlanningInGroupGrouping(records);
            //Segment & Room
            else if (DetailColumns.Count == 1 && ContainsColumn("trec_pk"))
                BuildPlanningInRoomGrouping(records);
            //Segment, Group & Room
            else if (DetailColumns.Count == 2 && ContainsColumn("grme_pk") && ContainsColumn("trec_pk"))
                BuildPlanningInFullDetails(records);
        }

        private void BuildPlanningInTotalGrouping(IEnumerable<RoomMarketSegmentPlanningRecord> records)
        {
            //Rows = new List<PlanningRowContract>();
            foreach (var record in records)
            {
                #region Hotel

                var key = record.HotelId.ToString();

                if (ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key;
                    rowContract.Title = record.HotelDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }

                #endregion
                #region Segment

                var key_segment = key + "&" + record.SegmentId.ToString();

                if (ContainsRow(key_segment))
                {
                    var rowContract = GetRowByKey(key_segment);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_segment;
                    rowContract.Title = record.SegmentDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.SegmentDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = null;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);

                }

                #endregion
            }
        }

        private void BuildPlanningInGroupGrouping(IEnumerable<RoomMarketSegmentPlanningRecord> records)
        {
            //Rows = new List<PlanningRowContract>();
            foreach (var record in records)
            {
                #region Hotel Group

                var key = record.HotelId.ToString();

                if (ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key;
                    rowContract.Title = record.HotelDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(string.Empty);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }

                #endregion
                #region Group

                var key_group = key + "&" + record.GroupId.ToString();

                if (ContainsRow(key_group))
                {
                    var rowContract = GetRowByKey(key_group);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_group;
                    rowContract.Title = record.GroupDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.GroupDescription);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.FontBold = true;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238); ;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
                #region Segment

                var key_segment = key_group + "&" + record.SegmentId.ToString();

                if (ContainsRow(key_segment))
                {
                    var rowContract = GetRowByKey(key_segment);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_segment;
                    rowContract.Title = record.SegmentDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.GroupDescription);
                    rowContract.SortKeys.Add(record.SegmentDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 10;
                    rowContract.CustomBackground = null;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key_group);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
            }
        }

        private void BuildPlanningInRoomGrouping(IEnumerable<RoomMarketSegmentPlanningRecord> records)
        {
            //Rows = new List<PlanningRowContract>();
            foreach (var record in records)
            {
                #region Hotel Group

                var key = record.HotelId.ToString();

                if (ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key;
                    rowContract.Title = record.HotelDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(string.Empty);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }

                #endregion
                #region Segment

                var key_segment = key + "&" + record.SegmentId.ToString();

                if (ContainsRow(key_segment))
                {
                    var rowContract = GetRowByKey(key_segment);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_segment;
                    rowContract.Title = record.SegmentDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.SegmentDescription);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.FontBold = true;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238); ;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
                #region Room Type

                var key_roomtype =  key_segment + "&" + record.RoomTypeId.ToString();

                if (ContainsRow(key_roomtype))
                {
                    var rowContract = GetRowByKey(key_roomtype);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_roomtype;
                    rowContract.RoomTypeRow = true;
                    rowContract.Title = record.RoomTypeDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.SegmentDescription);
                    rowContract.SortKeys.Add(record.RoomTypeDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 10;
                    rowContract.CustomBackground = null;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key_segment);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
            }
        }

        private void BuildPlanningInFullDetails(IEnumerable<RoomMarketSegmentPlanningRecord> records)
        {
            //Rows = new List<PlanningRowContract>();
            foreach (var record in records)
            {
                #region Hotel Group

                var key = record.HotelId.ToString();

                if (ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key;
                    rowContract.Title = record.HotelDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(string.Empty);
                    rowContract.SortKeys.Add(string.Empty);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }

                #endregion
                #region Group

                var key_group = key + "&" + record.GroupId.ToString();

                if (ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key;
                    rowContract.Title = record.GroupDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.GroupDescription);
                    rowContract.SortKeys.Add(string.Empty);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.FontBold = true;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.LightGray;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                    
                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
                #region Segment

                var key_segment = key_group + "&" + record.SegmentId.ToString();

                if (ContainsRow(key_segment))
                {
                    var rowContract = GetRowByKey(key_segment);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_segment;
                    rowContract.Title = record.SegmentDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.GroupDescription);
                    rowContract.SortKeys.Add(record.SegmentDescription);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = true;
                    rowContract.LeftMargin = 10;
                    rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key_group);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
                #region Room Type

                var key_roomtype = key_segment + "&" + record.RoomTypeId.ToString();

                if (ContainsRow(key_roomtype))
                {
                    var rowContract = GetRowByKey(key_roomtype);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_roomtype;
                    rowContract.RoomTypeRow = true;
                    rowContract.Title = record.RoomTypeDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.GroupDescription);
                    rowContract.SortKeys.Add(record.SegmentDescription);
                    rowContract.SortKeys.Add(record.RoomTypeDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 15;
                    rowContract.CustomBackground = null;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key_segment);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
            }
        }
    }
}