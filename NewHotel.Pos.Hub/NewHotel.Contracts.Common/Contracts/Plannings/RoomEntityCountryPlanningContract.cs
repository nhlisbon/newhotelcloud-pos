﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RoomEntityCountryPlanningContract : PlanningContract
    {
        public RoomEntityCountryPlanningContract(string totalRowTitle)
            : base(totalRowTitle)
        {
        }

        public override void BuildPlanning()
        {
            Rows.Clear();
            BuildPlanning(Records.Cast<RoomEntityCountryPlanningRecord>());
            SortPlanning();
            AddTotalRow(Rows);
        }

        private void BuildPlanning(IEnumerable<RoomEntityCountryPlanningRecord> records)
        {
            //Default Mode
            if (DetailColumns == null || DetailColumns.Count == 0)
                BuildPlanningInTotalGrouping(records);
            //Entity & Room
            else if (DetailColumns.Count == 1 && ContainsColumn("trec_pk"))
                BuildPlanningInRoomGrouping(records);
        }

        private void BuildPlanningInTotalGrouping(IEnumerable<RoomEntityCountryPlanningRecord> records)
        {
            //Rows = new List<PlanningRowContract>();
            foreach (var record in records)
            {
                #region Hotel

                var key = record.HotelId.ToString();

                if (ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Title = record.HotelDescription;
                    rowContract.Id = key;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(string.Empty);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }

                #endregion
                #region Country

                var key_country = key + "&" + record.CountryId.ToString();

                if (ContainsRow(key_country))
                {
                    var rowContract = GetRowByKey(key_country);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_country;
                    rowContract.Title = record.CountryDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.CountryDescription);
                    rowContract.SortKeys.Add(String.Empty);

                    //Styles
                    rowContract.FontBold = true;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238); ;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                } 

                #endregion
                #region Entity

                var key_entity = key_country + "&" + record.EntityId.ToString();

                if (ContainsRow(key_entity))
                {
                    var rowContract = GetRowByKey(key_entity);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_entity;
                    rowContract.Title = record.EntityDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.CountryDescription);
                    rowContract.SortKeys.Add(record.EntityDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 10;
                    rowContract.CustomBackground = null;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key_country);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
            }
        }

        private void BuildPlanningInRoomGrouping(IEnumerable<RoomEntityCountryPlanningRecord> records)
        {
            //Rows = new List<PlanningRowContract>();
            foreach (var record in records)
            {
                #region Hotel

                var key = record.HotelId.ToString();

                if (ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key;
                    rowContract.Title = record.HotelDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(String.Empty);
                    rowContract.SortKeys.Add(String.Empty);
                    rowContract.SortKeys.Add(String.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }
                #endregion
                #region Country

                var key_country = key + "&" + record.CountryId.ToString();

                if (ContainsRow(key_country))
                {
                    var rowContract = GetRowByKey(key_country);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_country;
                    rowContract.Title = record.CountryDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.CountryDescription);
                    rowContract.SortKeys.Add(string.Empty);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.FontBold = true;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.LightGray;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
                #region Entity

                var key_entity = key_country + "&" + record.EntityId.ToString();

                if (ContainsRow(key_entity))
                {
                    var rowContract = GetRowByKey(key_entity);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_entity;
                    rowContract.Title = record.EntityDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.CountryDescription);
                    rowContract.SortKeys.Add(record.EntityDescription);
                    rowContract.SortKeys.Add(String.Empty);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = true;
                    rowContract.LeftMargin = 10;
                    rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = this.GetRowIndexByKey(key_country);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
                #region Room Type

                var key_roomtype = key_entity + "&" + record.RoomTypeId.ToString();

                if (ContainsRow(key_roomtype))
                {
                    var rowContract = GetRowByKey(key_roomtype);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_roomtype;
                    rowContract.RoomTypeRow = true;
                    rowContract.Title = record.RoomTypeDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.CountryDescription);
                    rowContract.SortKeys.Add(record.EntityDescription);
                    rowContract.SortKeys.Add(record.RoomTypeDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 15;
                    rowContract.CustomBackground = null;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.TotalRooms.HasValue) ? record.TotalRooms.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key_entity);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
            }
        }
    }
}