﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PlanningListContract : BaseContract
    {
        [DataMember]
        public List<PlanningContract> Plannings { get; set; }

        public PlanningListContract()
        {
            Plannings = new List<PlanningContract>();
        }
    }
}