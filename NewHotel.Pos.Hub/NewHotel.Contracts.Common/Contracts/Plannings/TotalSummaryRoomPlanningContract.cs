﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class TotalSummaryRoomPlanningContract : BaseContract
    {
        [DataMember]
        public DateTime Date { get; set; }

        #region Real

        [DataMember]
        public long RealInventory { get; set; }
        [DataMember]
        public long RealOutOfOrder { get; set; }
        [DataMember]
        public long RealBooking { get; set; }
        [DataMember]
        public long RealGuarantee { get; set; }
        [DataMember]
        public long RealUsedGuarantee { get; set; }
        [DataMember]
        public long RealAvailability { get; set; }

        public long RealActive
        {
            get { return RealInventory - RealOutOfOrder; }
        }

        public long RealAvailableGuarantee
        {
            get { return RealGuarantee - RealUsedGuarantee; }
        }

        #endregion

        #region Virtual

        [DataMember]
        public long VirtualInventory { get; set; }
        [DataMember]
        public long VirtualOutOfOrder { get; set; }
        [DataMember]
        public long VirtualBooking { get; set; }
        [DataMember]
        public long VirtualGuarantee { get; set; }
        [DataMember]
        public long VirtualUsedGuarantee { get; set; }
        [DataMember]
        public long VirtualAvailability { get; set; }

        #endregion

        #region Total

        public long TotalInventory
        {
            get { return RealInventory + VirtualInventory; }
        }

        public long TotalOutOfOrder
        {
            get { return RealOutOfOrder + VirtualOutOfOrder; }
        }

        public long TotalActive
        {
            get { return TotalInventory - TotalOutOfOrder; }
        }

        public long TotalGuarantee
        {
            get { return RealGuarantee + VirtualGuarantee; }
        }

        public long TotalUsedGuarantee
        {
            get { return RealUsedGuarantee + VirtualUsedGuarantee; }
        }

        public long TotalAvailableGuarantee
        {
            get { return TotalGuarantee - TotalUsedGuarantee; }
        }

        public long TotalBookings
        {
            get { return RealBooking + VirtualBooking; }
        }

        #endregion

        // no tiene en cuenta los cuartos fuera de orden
        public decimal Occupancy
        {
            get
            {
                if (RealActive > decimal.Zero)
                    return (RealBooking + RealAvailableGuarantee) * 100M / RealActive;

                return decimal.Zero;
            }
        }
    }
}