﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RoomPensionModePlanningContract : PlanningContract
    {
        public RoomPensionModePlanningContract(string totalRowTitle)
            : base(totalRowTitle)
        {
        }

        public string BreakfastTraslate { get; set; }
        public string LunchTraslate { get; set; }
        public string DinnerTraslate { get; set; }

        public TypedList<PlanningBaseRecord> RecordsDetails { get; set; }

        public override void BuildPlanning()
        {
            Rows.Clear();
            BuildPlanning(Records.Cast<RoomPensionModePlanningRecord>().OrderBy(r => r.PensionModeId));
            AddTotalRow(Rows);
            AddPaxsMeals(RecordsDetails.Cast<PaxsMealsPlanningRecord>());
        }

        private void BuildPlanning(IEnumerable<RoomPensionModePlanningRecord> records)
        {
            //Default Mode
            if (DetailColumns == null || DetailColumns.Count == 0)
                BuildPlanningInTotalGrouping(records);
        }

        public void AddPaxsMeals (IEnumerable<PaxsMealsPlanningRecord> records)
        {
            var hotels = new List<string>();

            foreach (var record in records)
            {
                var key = record.HotelId.ToString();
                var key_breakfast = record.HotelId.ToString() + "&" + "Breakfast";
                var key_lunch = record.HotelId.ToString() + "&" + "Lunch";
                var key_dinner = record.HotelId.ToString() + "&" + "Dinner";

                if (!hotels.Contains(key)) hotels.Add(key);

                #region Breakfast

                if (ContainsRow(key_breakfast))
                {
                    var rowContract = GetRowByKey(key_breakfast);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Breakfast.HasValue) ? record.Breakfast.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");
                    rowContract.Id = key_breakfast;
                    rowContract.Title = BreakfastTraslate;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Breakfast.HasValue) ? record.Breakfast.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetLastRowIndexByParentKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                } 

                #endregion
                #region Lunch

                if (ContainsRow(key_lunch))
                {
                    var rowContract = GetRowByKey(key_lunch);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Lunch.HasValue) ? record.Lunch.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");
                    rowContract.Id = key_lunch;
                    rowContract.Title = LunchTraslate;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Lunch.HasValue) ? record.Lunch.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetLastRowIndexByParentKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
                #region Dinner

                if (ContainsRow(key_dinner))
                {
                    var rowContract = GetRowByKey(key_dinner);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Dinner.HasValue) ? record.Dinner.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");
                    rowContract.Id = key_dinner;
                    rowContract.Title = DinnerTraslate;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Dinner.HasValue) ? record.Dinner.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetLastRowIndexByParentKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion
            }

            #region Separators

            Separators = new List<NHPlanningSeparator>();

            if (hotels.Count > 1)
            {
                foreach (var hotel_key in hotels)
                {
                    var index = GetLastRowIndexByParentKey(hotel_key);
                    Separators.Add(new NHPlanningSeparator() { RowPosition = index - 2, Color = ARGBColor.Black });
                }
            }

            #endregion
        }

        private void BuildPlanningInTotalGrouping(IEnumerable<RoomPensionModePlanningRecord> records)
        {
            //Rows = new List<PlanningRowContract>();
            foreach (var record in records)
            {
                #region Hotel

                var key = record.HotelId.ToString();

                if (ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Reservations.HasValue) ? record.Reservations.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key;
                    rowContract.Title = record.HotelDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Reservations.HasValue) ? record.Reservations.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }

                #endregion
                #region Pension

                var key_pension = key + "&" + record.PensionModeId.ToString();

                if (ContainsRow(key_pension))
                {
                    var rowContract = GetRowByKey(key_pension);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Reservations.HasValue) ? record.Reservations.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_pension;
                    rowContract.Title = record.PensionModeDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.PensionModeDescription);

                    //Styles
                    rowContract.FontBold = false;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238); ;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Reservations.HasValue) ? record.Reservations.Value : 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);
                }

                #endregion 
            }
        }
    }
}