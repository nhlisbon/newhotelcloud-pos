﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class PlanningRowContent
    {
        [DataMember]
        internal string[] _content;

        public string this[int index]
        {
            get { return _content[index]; }
            set { _content[index] = value; }
        }

        public int Length
        {
            get { return _content.Length; }
        }

        public PlanningRowContent(int length)
        {
            _content = new string[length];
        }
    }

    [DataContract]
	[Serializable]
    public class PlanningRowContract : BaseContract<string>, IComparable<PlanningRowContract>
    {
        #region Members

        [DataMember]
        internal List<string> _sortKeys;
        [DataMember]
        internal bool _isVirtual;
        [DataMember]
        internal string _title;

        #endregion
        #region Properties

        [DataMember]
        public Guid HotelId { get; set; }

        [DataMember]
        public bool FontBold { get; set; }
        [DataMember]
        public bool FontItalic { get; set; }
        [DataMember]
        public int LeftMargin { get; set; }
        [DataMember]
        public ARGBColor? CustomBackground { get; set; }
        [DataMember]
        public bool HotelDataExpanded { get; set; }

        [DataMember]
        public bool HotelDescriptionRow { get; set; }
        [DataMember]
        public bool RoomTypeRow { get; set; }
        [DataMember]
        public bool TotalRoomsRow { get; set; }
        [DataMember]
        public bool TotalRow { get; set; }
        [DataMember]
        public bool TitleRow { get; set; } = false;

        [DataMember]
        public PlanningRowContent Content { get; set; }

        public string Title
        {
            set { _title = value; }
        }

        public string FormattedTitle
        {
            get
            {
                if (IsVirtual)
                {
                    if (RoomTypeRow)
                        return _title + "(*)";
                }

                return _title;
            }
        }

        public bool EmptyTitle
        {
            get { return string.IsNullOrEmpty(_title); }
        }

        public bool IsEmpty
        {
            get
            {
                for (int index = 0; index < Content.Length; index++)
                {
                    if (Content[index] != "0")
                        return false;
                }

                return true;
            }
        }

        public bool IsGroup
        {
            get { return !Id.ToString().Contains("&"); }
        }

        public bool IsVirtual { get { return _isVirtual; } }

        public List<string> SortKeys { get { return _sortKeys; } }

        #endregion
        #region Public Methods

        public void InitializeList(int length, string value)
        {
            Content = new PlanningRowContent(length);
            for (int i = 0; i < Content.Length; i++)
                Content[i] = TitleRow ? string.Empty : value;
        }

        public int Total()
        {
            int result = 0;
            for (int index = 0; index < Content.Length; index++)
            {
                int value;
                if (int.TryParse(Content[index], out value))
                    result += value;
            }

            return result;
        }

        public int Average()
        {
            int result = 0;
            for (int index = 0; index < Content.Length; index++)
            {
                int value;
                if (int.TryParse(Content[index], out value))
                    result += value;
            }

            if (Content.Length > 0)
                return result / Content.Length;

            return 0;
        }

        public PlanningRowContract CloneRow()
        {
            var row = new PlanningRowContract(_isVirtual);

            row.Id = Id;
            row.Title = _title;
            row.InitializeList(Content.Length, "0");
            for (int i = 0; i < Content.Length; i++)
                row.Content[i] = Content[i];
            row.CustomBackground = CustomBackground;
            row.FontBold = FontBold;
            row.FontItalic = FontItalic;
            row.HotelDataExpanded = HotelDataExpanded;
            row.HotelDescriptionRow = HotelDescriptionRow;
            row.HotelId = HotelId;
            row.LeftMargin = LeftMargin;
            row.RoomTypeRow = RoomTypeRow;
            foreach (var sortKeys in SortKeys)
                row.SortKeys.Add(sortKeys);
            row.TotalRoomsRow = TotalRoomsRow;
            row.TotalRow = TotalRow;

            return row;
        }

        public int CompareTo(PlanningRowContract other)
        {
            var length = Math.Min(SortKeys.Count, other.SortKeys.Count);
            for (int index = 0; index < length; index++)
            {
                var result = string.Compare(SortKeys[index], other.SortKeys[index]);
                if (result != 0)
                    return result;
            }

            if (SortKeys.Count > other.SortKeys.Count)
                return 1;
            else if (SortKeys.Count < other.SortKeys.Count)
                return -1;

            return 0;
        }

        #endregion
        #region Constructor

        public PlanningRowContract(bool isVirtual = false)
        {
            _isVirtual = isVirtual;
            _sortKeys = new List<string>();
        }

        #endregion
    }
}