﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class AllotmentPlanningContract : PlanningContract
    {
        #region Constants

        private const string GuaranteeKey = "4F5B9DE8-13D9-4923-A26A-398F939AFEC6";
        private const string AffectChannelKey = "1334355E-0CA6-4F39-AD42-154BFECE2B4C";
        private const string OnRequestKey = "A415AEC0-B2A2-4C0B-9E98-45F56E98D135";

        #endregion
        #region Properties

        private List<string> AllotmentRoomTypePairs { get; set; }

        public string DirectTranslation;
        public string RoomInventoryTranslation { get; set; }
        public string ActiveTranslation { get; set; }
        public string OutOfOrderTranslation { get; set; }
        public bool Summary { get; set; }
        public List<AvailabilityInventoryRecord> ListInventory { get; set; }
        public List<PlanningBaseRecord> RecordsDetails { get; set; }

        #endregion
        #region Constructor

        public AllotmentPlanningContract(string totalRowTitle)
            : base(totalRowTitle) { }

        #endregion
        #region Public Methods

        public override void BuildPlanning()
        {
            AllotmentRoomTypePairs = new List<string>();

            var records = Records.Cast<AllotmentByEntityRecord>();

            //Look for Summary and take it out
            if (DetailColumns.Contains("Summary"))
            {
                Summary = true;
                DetailColumns.Remove("Summary");
            }

            //Default Mode
            Rows.Clear();
            if (DetailColumns == null || DetailColumns.Count == 0)
                BuildPlanningInTotalGrouping(records);
            //Origin & Room
            else if (DetailColumns.Count == 1 && ContainsColumn("tial_pk"))
                BuildPlanningInRoomGrouping(records);

            SortPlanning();
            BuildInventory();
        }

        #endregion
        #region Protected Methods

        protected override void RemoveRows()
        {
            Rows.RemoveAll(r => r.EmptyTitle);
        }

        #endregion
        #region Private Methods

        private void BuildPlanningInTotalGrouping(IEnumerable<AllotmentByEntityRecord> records)
        {
            //Rows = new List<PlanningRowContract>();
            var allotmentRoomTypePairs = new List<string>();

            foreach (var record in records)
            {
                #region Hotel

                var key = record.HotelId.ToString();

                if (ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = (record.Id - From).Days;
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key;
                    rowContract.Title = record.HotelDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(string.Empty);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    Rows.Add(rowContract);
                }

                #endregion
                #region Grouping

                var key_grouping = key + "&" + (record.IsGuarantee ? GuaranteeKey : (record.AffectBookingChannel ? AffectChannelKey : OnRequestKey));

                if (ContainsRow(key_grouping))
                {
                    var rowContract = GetRowByKey(key_grouping);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Contracted.HasValue) ? record.Contracted.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.TitleRow = false;
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_grouping;
                    rowContract.Title = (record.IsGuarantee ? "Guaranteed" : (record.AffectBookingChannel ? "Not Guaranteed (Affect Channels)" : "Not Guaranteed (On Request)"));
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add((record.IsGuarantee ? "3" : (record.AffectBookingChannel ? "2" : "1")));
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.FontBold = true;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.LightGray;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Contracted.HasValue) ? record.Contracted.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }

                #endregion
                #region Allotment

                var key_allotment = key_grouping + "&" + record.AllotmentId.ToString();

                if (ContainsRow(key_allotment))
                    AllotmentUpdate(record, key_allotment);
                else
                    AllotmentInsert(record, key, key_allotment);

                #endregion
            }

            #region Release Dates

            foreach (var record in records)
            {
                var key = record.HotelId.ToString();
                var key_grouping = key + "&" + (record.IsGuarantee ? GuaranteeKey : (record.AffectBookingChannel ? AffectChannelKey : OnRequestKey));
                var key_allotment = key_grouping + "&" + record.AllotmentId.ToString();

                //Release Day Channel
                if (record.AffectBookingChannel && record.OverReleaseChannel.HasValue && record.OverReleaseChannel.Value > 0)
                {
                    var date = (DateTime)record.Id;
                    var indexInContext = (date - From).Days;

                    var rowContract = GetRowByKey(key_allotment);
                    if (rowContract != null)
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) - record.OverReleaseChannel.Value + (record.DirectReservations ?? 0) + (record.EntityReservations ?? 0)).ToString();

                    rowContract = GetRowByKey(key_grouping);
                    if (rowContract != null)
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) - record.OverReleaseChannel.Value + (record.DirectReservations ?? 0) + (record.EntityReservations ?? 0)).ToString();
                }

                //Release Day Standard
                if (!record.IsGuarantee && !record.AffectBookingChannel && record.OverRelease.HasValue && record.OverRelease.Value > 0)
                {
                    var date = (DateTime)record.Id;
                    var indexInContext = (date - From).Days;

                    var rowContract = GetRowByKey(key_allotment);
                    if (rowContract != null)
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) - record.OverRelease.Value + (record.DirectReservations ?? 0) + (record.EntityReservations ?? 0)).ToString();

                    rowContract = GetRowByKey(key_grouping);
                    if (rowContract != null)
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) - record.OverRelease.Value + (record.DirectReservations ?? 0) + (record.EntityReservations ?? 0)).ToString();
                }
            }

            #endregion
        }

        private void BuildPlanningInRoomGrouping(IEnumerable<AllotmentByEntityRecord> records)
        {
            //Rows = new List<PlanningRowContract>();
            foreach (var record in records)
            {
                #region Hotel

                var key = record.HotelId.ToString();

                if (ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    int indexInContext = ((DateTime)record.Id - From).Days;
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key;
                    rowContract.Title = record.HotelDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(string.Empty);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    Rows.Add(rowContract);
                }

                #endregion
                #region Grouping

                var key_grouping = key + "&" + (record.IsGuarantee ? GuaranteeKey : (record.AffectBookingChannel ? AffectChannelKey : OnRequestKey));

                if (ContainsRow(key_grouping))
                {
                    var rowContract = GetRowByKey(key_grouping);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Contracted.HasValue) ? record.Contracted.Value : 0)).ToString();
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.TitleRow = false;
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_grouping;
                    rowContract.Title = (record.IsGuarantee ? "Guarantee" : (record.AffectBookingChannel ? "Not Guaranteed (Affect Channels)" : "Not Guaranteed (On Request)"));
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add((record.IsGuarantee ? "3" : (record.AffectBookingChannel ? "2" : "1")));
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.FontBold = true;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.LightGray;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + ((record.Contracted.HasValue) ? record.Contracted.Value : 0)).ToString();
                    Rows.Add(rowContract);
                }

                #endregion
                #region Allotment

                var key_allotment = key_grouping + "&" + record.AllotmentId.ToString();

                if (ContainsRow(key_allotment))
                    AllotmentUpdate(record, key_allotment);
                else
                    AllotmentInsert(record, key, key_allotment);

                #endregion
                #region Room Type

                var key_roomtype = key_allotment + "&" + record.RoomTypeId.ToString();

                if (ContainsRow(key_roomtype))
                    RoomTypeUpdate(record, key_roomtype, true, false);
                else
                    RoomTypeInsert(record, key_allotment, key_roomtype, true, false);

                #endregion
                #region Room Type Used

                var key_roomtypeused = key_roomtype + "R";

                if (ContainsRow(key_roomtypeused))
                    RoomTypeUpdate(record, key_roomtype, false, true);
                else
                    RoomTypeInsert(record, key_allotment, key_roomtype, false, true);

                #endregion
            }

            #region Release Dates

            foreach (var record in records)
            {
                var key = record.HotelId.ToString();
                var key_grouping = key + "&" + (record.IsGuarantee ? GuaranteeKey : (record.AffectBookingChannel ? AffectChannelKey : OnRequestKey));
                var key_allotment = key_grouping + "&" + record.AllotmentId.ToString();
                var key_roomtype = key_allotment + "&" + record.RoomTypeId.ToString();

                //Release Day Channel
                if (record.AffectBookingChannel && record.OverReleaseChannel.HasValue && record.OverReleaseChannel.Value > 0)
                {
                    var date = (DateTime)record.Id;
                    var indexInContext = (date - From).Days;

                    var rowContract = GetRowByKey(key_allotment);
                    if (rowContract != null)
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) - record.OverReleaseChannel.Value + (record.DirectReservations ?? 0) + (record.EntityReservations ?? 0)).ToString();

                    rowContract = GetRowByKey(key_roomtype);
                    if (rowContract != null)
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) - record.OverReleaseChannel.Value + (record.DirectReservations ?? 0) + (record.EntityReservations ?? 0)).ToString();

                    rowContract = GetRowByKey(key_grouping);
                    if (rowContract != null)
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) - record.OverReleaseChannel.Value + (record.DirectReservations ?? 0) + (record.EntityReservations ?? 0)).ToString();
                }

                //Release Day Standard
                if (!record.IsGuarantee && !record.AffectBookingChannel && record.OverRelease.HasValue && record.OverRelease.Value > 0)
                {
                    var date = (DateTime)record.Id;
                    var indexInContext = (date - From).Days;

                    var rowContract = GetRowByKey(key_allotment);
                    if (rowContract != null)
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) - record.OverRelease.Value + (record.DirectReservations ?? 0) + (record.EntityReservations ?? 0)).ToString();

                    rowContract = GetRowByKey(key_roomtype);
                    if (rowContract != null)
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) - record.OverRelease.Value + (record.DirectReservations ?? 0) + (record.EntityReservations ?? 0)).ToString();

                    rowContract = GetRowByKey(key_grouping);
                    if (rowContract != null)
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) - record.OverRelease.Value + (record.DirectReservations ?? 0) + (record.EntityReservations ?? 0)).ToString();
                }
            }

            #endregion
        }

        private void BuildPlanningInEntityGrouping(IEnumerable<AllotmentByEntityRecord> records)
        {
            //Rows = new List<PlanningRowContract>();
            var allotmentRoomTypePairs = new List<string>();

            foreach (var record in records)
            {
                #region Hotel

                var key = record.HotelId.ToString();

                if (ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key;
                    rowContract.Title = record.HotelDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(string.Empty);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    Rows.Add(rowContract);
                }

                #endregion
                #region Grouping

                var key_grouping = key + "&" + (record.IsGuarantee ? GuaranteeKey : (record.AffectBookingChannel ? AffectChannelKey : OnRequestKey));

                if (ContainsRow(key_grouping))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.TitleRow = true;
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_grouping;
                    rowContract.Title = (record.IsGuarantee ? "Guarantee" : (record.AffectBookingChannel ? "Not Guaranteed (Affect Channels)" : "Not Guaranteed (On Request)"));
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add((record.IsGuarantee ? "3" : (record.AffectBookingChannel ? "2" : "1")));
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.FontBold = true;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.LightGray;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    Rows.Add(rowContract);
                }

                #endregion
                #region Allotment

                var key_allotment = key_grouping + "&" + record.AllotmentId.ToString();

                if (ContainsRow(key_allotment))
                    AllotmentUpdate(record, key_allotment);
                else
                    AllotmentInsert(record, key, key_allotment);

                #endregion
                #region Entity

                if (record.IsUsed)
                {
                    var key_entity = key_allotment+ "&" + (record.EntityId.HasValue ? record.EntityId.Value : Guid.Empty).ToString();

                    if (ContainsRow(key_entity))
                        EntityUpdate(record, key_entity);
                    else
                        EntityInsert(record, key_allotment, key_entity);
                }

                #endregion
            }
        }

        private void BuildPlanningInFullDetails(IEnumerable<AllotmentByEntityRecord> records)
        {
            //Rows = new List<PlanningRowContract>();
            var allotmentRoomTypePairs = new List<string>();

            foreach (var record in records)
            {
                #region Hotel

                var key = record.HotelId.ToString();

                if (ContainsRow(key))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key;
                    rowContract.Title = record.HotelDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(string.Empty);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.HotelDescriptionRow = true;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    Rows.Add(rowContract);
                }

                #endregion
                #region Grouping

                var key_grouping = key + "&" + (record.IsGuarantee ? GuaranteeKey : (record.AffectBookingChannel ? AffectChannelKey : OnRequestKey));

                if (ContainsRow(key_grouping))
                {
                    var rowContract = GetRowByKey(key);
                    var indexInContext = ((DateTime)record.Id - From).Days;
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.TitleRow = true;
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_grouping;
                    rowContract.Title = (record.IsGuarantee ? "Guarantee" : (record.AffectBookingChannel ? "Not Guaranteed (Affect Channels)" : "Not Guaranteed (On Request)"));
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add((record.IsGuarantee ? "3" : (record.AffectBookingChannel ? "2" : "1")));
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.FontBold = true;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.LightGray;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    Rows.Add(rowContract);
                }

                #endregion
                #region Allotment

                var key_allotment = key_grouping + "&" + record.AllotmentId.ToString();

                if (ContainsRow(key_allotment))
                {
                    var rowContract = GetRowByKey(key_allotment);
                    var indexInContext = ((DateTime)record.Id - From).Days;

                    //Add value only if allo_pk + tial_pk hasn't being used yet
                    var key_allotment_roomType = key_allotment + record.RoomTypeId.ToString() + record.Id.ToString();

                    if (!allotmentRoomTypePairs.Contains(key_allotment_roomType))
                    {
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + (record.Contracted ?? 0)).ToString();
                        allotmentRoomTypePairs.Add(key_allotment_roomType);
                    }

                    #region Summary

                    if (Summary)
                    {
                        var usedValue = (int)((record.EntityReservations ?? 0) + (record.DirectReservations ?? 0));
                        var contracted = int.Parse(rowContract.Content[indexInContext]);

                        var used = GetRowByKey(key_allotment + "&Used");
                        used.Content[indexInContext] = (int.Parse(used.Content[indexInContext]) + usedValue).ToString();

                        var available = GetRowByKey(key_allotment + "&Available");
                        available.Content[indexInContext] = (contracted - int.Parse(used.Content[indexInContext])).ToString();

                        var percent = GetRowByKey(key_allotment + "&Percent");
                        percent.Content[indexInContext] = ((contracted == 0) ? 0 : (int.Parse(used.Content[indexInContext])) * 100 / contracted).ToString();
                    }

                    #endregion
                }
                else
                {
                    var rowContract = CreateRow(record.IsVirtual);
                    rowContract.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContract.Id = key_allotment;
                    rowContract.Title = record.AllotmentDescription;
                    rowContract.SortKeys.Add(record.HotelDescription);
                    rowContract.SortKeys.Add(record.AllotmentDescription);
                    rowContract.SortKeys.Add(string.Empty);

                    //Styles
                    rowContract.FontBold = true;
                    rowContract.FontItalic = false;
                    rowContract.LeftMargin = 5;
                    rowContract.CustomBackground = ARGBColor.LightGray;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContract.Content[indexInContext] = (record.Contracted ?? 0).ToString();
                    allotmentRoomTypePairs.Add(key_allotment + record.RoomTypeId.ToString());

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                    else Rows.Insert(parentIndex + 1, rowContract);

                    #region Summary

                    if (Summary)
                    {
                        var available = rowContract.CloneRow();
                        available.InitializeList((To - From).Days + 1, "0");
                        available.Id = key_allotment + "&Available";
                        available.Title = "Available";
                        available.SortKeys.Add("1");
                        available.Content[indexInContext] = (record.Contracted - ((record.DirectReservations ?? 0) + (record.EntityReservations ?? 0))).ToString();
                        Rows.Add(available);

                        var used = rowContract.CloneRow();
                        used.InitializeList((To - From).Days + 1, "0");
                        used.Id = key_allotment + "&Used";
                        used.Title = "Used";
                        used.SortKeys.Add("2");
                        used.Content[indexInContext] = ((record.DirectReservations ?? 0) + (record.EntityReservations ?? 0)).ToString();
                        Rows.Add(used);

                        var percent = rowContract.CloneRow();
                        percent.InitializeList((To - From).Days + 1, "0");
                        percent.Id = key_allotment + "&Percent";
                        percent.Title = "Percent";
                        percent.SortKeys.Add("3");
                        percent.Content[indexInContext] = (((record.Contracted ?? 0) == 0) ? 0 : ((record.DirectReservations ?? 0) + (record.EntityReservations ?? 0)) * 100 / record.Contracted.Value).ToString();
                        Rows.Add(percent);
                    }

                    #endregion
                }

                #endregion
                #region Room Type

                var key_roomtype = key_allotment + "&" + record.RoomTypeId.ToString();

                if (ContainsRow(key_roomtype))
                    RoomTypeUpdate(record, key_roomtype, true, false);
                else
                    RoomTypeInsert(record, key_allotment, key_roomtype, true, false);

                #endregion
                #region Entity

                var key_entity = key_allotment + "&" + (record.EntityId.HasValue ? record.EntityId.Value : Guid.Empty).ToString();

                if (record.IsUsed)
                {
                    if (ContainsRow(key_entity))
                        EntityUpdate(record, key_entity);
                    else
                        EntityInsert(record, key_allotment, key_entity);
                }

                #endregion
                #region Room Type Used

                if (record.IsUsed)
                {
                    var key_entity_roomType = record.HotelId.ToString() + "&" + record.AllotmentId.ToString() + "&" +
                        (record.EntityId.HasValue ? record.EntityId.Value : Guid.Empty).ToString() + "&" + record.RoomTypeId.ToString();

                    if (ContainsRow(key_entity_roomType + "R"))
                        RoomTypeUpdate(record, key_entity_roomType, false, true);
                    else
                        RoomTypeInsert(record, key_entity, key_entity_roomType, false, true, (record.EntityId.HasValue ? "5" + record.EntityDescription : "4"), true, 15);
                }

                #endregion
            }
        }

        private void BuildInventory()
        {
            if (ListInventory.Count > 0)
            {
                foreach (var record in ListInventory)
                {
                    var key = record.HotelId.ToString();
                    var key_inventory = key + "&" + RoomInventoryTranslation;
                    var key_available = key + "&" + ActiveTranslation;
                    var key_inactive = key + "&" + OutOfOrderTranslation;

                    #region Contracted

                    if (ContainsRow(key_inventory))
                    {
                        var rowContract = GetRowByKey(key_inventory);
                        var indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Inventory).ToString();
                    }
                    else
                    {
                        var rowContract = CreateRow(record.IsVirtual);
                        rowContract.Id = key + "&" + RoomInventoryTranslation;
                        rowContract.Title = RoomInventoryTranslation;
                        rowContract.FontBold = false;
                        rowContract.FontItalic = false;
                        rowContract.LeftMargin = 5;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                        rowContract.HotelId = record.HotelId;

                        rowContract.InitializeList((To - From).Days + 1, "0");
                        var indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Inventory).ToString();

                        var index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }

                    #endregion
                    #region Used

                    if (ContainsRow(key_available))
                    {
                        var rowContract = GetRowByKey(key_available);
                        var indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Active).ToString();
                    }
                    else
                    {
                        var rowContract = CreateRow(record.IsVirtual);
                        rowContract.Id = key + "&" + ActiveTranslation;
                        rowContract.Title = ActiveTranslation;
                        rowContract.FontBold = false;
                        rowContract.FontItalic = false;
                        rowContract.LeftMargin = 10;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                        rowContract.HotelId = record.HotelId;

                        rowContract.InitializeList((To - From).Days + 1, "0");
                        var indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Active).ToString();

                        var index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }

                    #endregion
                    #region Inactive

                    if (ContainsRow(key_inactive))
                    {
                        var rowContract = GetRowByKey(key_inactive);
                        var indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Inactive).ToString();
                    }
                    else
                    {
                        var rowContract = CreateRow(record.IsVirtual);
                        rowContract.Id = key + "&" + OutOfOrderTranslation;
                        rowContract.Title = OutOfOrderTranslation;
                        rowContract.FontBold = false;
                        rowContract.FontItalic = false;
                        rowContract.LeftMargin = 10;
                        rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);
                        rowContract.HotelId = record.HotelId;

                        rowContract.InitializeList((To - From).Days + 1, "0");
                        var indexInContext = ((DateTime)record.Id - From).Days;
                        rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + record.Inactive).ToString();

                        var index = GetLastRowIndexByParentKey(key);
                        if (index + 1 == Rows.Count) Rows.Add(rowContract);
                        else Rows.Insert(index + 1, rowContract);
                    }

                    #endregion
                    #region Separators

                    var last_index = GetRowIndexByKey(key_inventory);
                    Separators = new List<NHPlanningSeparator>();
                    Separators.Add(new NHPlanningSeparator() { RowPosition = last_index - 1, Color = ARGBColor.Black });

                    #endregion
                }
            }
        }

        private void AllotmentInsert(AllotmentByEntityRecord record, string key, string key_allotment)
        {
            var rowContract = CreateRow(record.IsVirtual);
            rowContract.InitializeList((To - From).Days + 1, "0");

            //Details
            rowContract.Id = key_allotment;
            rowContract.Title = record.AllotmentDescription;
            rowContract.SortKeys.Add(record.HotelDescription);
            rowContract.SortKeys.Add((record.IsGuarantee ? "3" : (record.AffectBookingChannel ? "2" : "1")));
            rowContract.SortKeys.Add(record.AllotmentDescription);

            //Styles
            rowContract.FontBold = false;
            rowContract.FontItalic = false;
            rowContract.LeftMargin = 5;
            rowContract.CustomBackground = ARGBColor.NewARGBColor(255, 238, 238, 238);

            //Values
            var indexInContext = ((DateTime)record.Id - From).Days;
            rowContract.Content[indexInContext] = (record.Contracted ?? 0).ToString();
            AllotmentRoomTypePairs.Add(key_allotment + record.RoomTypeId.ToString() + record.Id.ToString());

            //Insert in correct place
            var parentIndex = GetRowIndexByKey(key);
            if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
            else Rows.Insert(parentIndex + 1, rowContract);

            #region Summary

            if (Summary)
            {
                var used = rowContract.CloneRow();
                used.FontItalic = true;
                used.LeftMargin = 10;
                used.InitializeList((To - From).Days + 1, "0");
                used.Id = key_allotment + "&Used";
                used.Title = "Used";
                used.SortKeys.Add("1");
                used.Content[indexInContext] = ((record.DirectReservations ?? 0) + (record.EntityReservations ?? 0)).ToString();
                Rows.Add(used);

                var percent = rowContract.CloneRow();
                percent.FontItalic = true;
                percent.LeftMargin = 10;
                percent.InitializeList((To - From).Days + 1, "0");
                percent.Id = key_allotment + "&Percent";
                percent.Title = "Percent";

                percent.SortKeys.Add("2");
                percent.Content[indexInContext] = (((record.Contracted ?? 0) == 0) ? 0 : ((record.DirectReservations ?? 0) + (record.EntityReservations ?? 0)) * 100 / record.Contracted.Value).ToString();
                Rows.Add(percent);
            }

            #endregion
        }

        private void AllotmentUpdate(AllotmentByEntityRecord record, string key_allotment)
        {
            var rowContract = GetRowByKey(key_allotment);
            var indexInContext = ((DateTime)record.Id - From).Days;

            //Add value only if allo_pk + tial_pk hasn't being used yet
            var key_allotment_roomType = key_allotment + record.RoomTypeId.ToString() + record.Id.ToString();

            if (!AllotmentRoomTypePairs.Contains(key_allotment_roomType))
            {
                rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + (record.Contracted ?? 0)).ToString();
                AllotmentRoomTypePairs.Add(key_allotment_roomType);
            }

            #region Summary

            if (Summary)
            {
                var usedValue = (int)((record.EntityReservations ?? 0) + (record.DirectReservations ?? 0));
                var contracted = int.Parse(rowContract.Content[indexInContext]);

                var used = GetRowByKey(key_allotment + "&Used");
                used.Content[indexInContext] = (int.Parse(used.Content[indexInContext]) + usedValue).ToString();

                var percent = GetRowByKey(key_allotment + "&Percent");
                percent.Content[indexInContext] = ((contracted == 0) ? 0 : (int.Parse(used.Content[indexInContext])) * 100 / contracted).ToString();
            }

            #endregion
        }

        private void RoomTypeInsert(AllotmentByEntityRecord record, string key_parent, string key_roomtype, bool contracted = true, bool used = true, string sortKeyPrefix = "", bool italic = false, int margin = 10)
        {
            if (contracted)
            {
                #region Contracted

                var rowContract = CreateRow(record.IsVirtual);
                rowContract.InitializeList((To - From).Days + 1, "0");

                //Details
                rowContract.Id = key_roomtype;
                rowContract.RoomTypeRow = true;
                rowContract.Title = record.RoomTypeDescription;
                rowContract.SortKeys.Add(record.HotelDescription);
                rowContract.SortKeys.Add((record.IsGuarantee ? "3" : (record.AffectBookingChannel ? "2" : "1")));
                rowContract.SortKeys.Add(record.AllotmentDescription);
                rowContract.SortKeys.Add("3" + record.RoomTypeDescription);

                //Styles
                rowContract.FontBold = false;
                rowContract.FontItalic = false;
                rowContract.LeftMargin = 10;
                rowContract.CustomBackground = null;

                //Values
                var indexInContext = ((DateTime)record.Id - From).Days;
                rowContract.Content[indexInContext] = (record.Contracted ?? 0).ToString();

                //Insert in correct place
                var parentIndex = GetRowIndexByKey(key_parent);
                if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
                else Rows.Insert(parentIndex + 1, rowContract);

                #endregion
            }
            if (used)
            {
                #region Used

                if (record.IsUsed)
                {
                    var rowContractUsed = CreateRow(record.IsVirtual);
                    rowContractUsed.InitializeList((To - From).Days + 1, "0");

                    //Details
                    rowContractUsed.Id = key_roomtype + "R";
                    rowContractUsed.Title = "Reserved " + record.RoomTypeDescription;
                    rowContractUsed.SortKeys.Add(record.HotelDescription);
                    rowContractUsed.SortKeys.Add((record.IsGuarantee ? "3" : (record.AffectBookingChannel ? "2" : "1")));
                    rowContractUsed.SortKeys.Add(record.AllotmentDescription);
                    rowContractUsed.SortKeys.Add(sortKeyPrefix + "6" + record.RoomTypeDescription);

                    //Styles
                    rowContractUsed.FontBold = false;
                    rowContractUsed.FontItalic = italic;
                    rowContractUsed.LeftMargin = margin;
                    rowContractUsed.CustomBackground = null;

                    //Values
                    var indexInContext = ((DateTime)record.Id - From).Days;
                    rowContractUsed.Content[indexInContext] = ((record.EntityReservations ?? 0) + (record.DirectReservations ?? 0)).ToString();

                    //Insert in correct place
                    var parentIndex = GetRowIndexByKey(key_parent);
                    if (parentIndex + 1 == Rows.Count) Rows.Add(rowContractUsed);
                    else Rows.Insert(parentIndex + 1, rowContractUsed);
                }

                #endregion
            }
        }

        private void RoomTypeUpdate(AllotmentByEntityRecord record, string key_roomtype, bool contracted = true, bool used = true)
        {
            var rowContract = GetRowByKey(key_roomtype);
            var usedRowContract = GetRowByKey(key_roomtype + "R");
            var indexInContext = ((DateTime)record.Id - From).Days;

            if (contracted)
            {
                //Only update the first time the contracted
                if (rowContract.Content[indexInContext] == "0")
                    rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + (record.Contracted ?? 0)).ToString();
            }

            if (used)
            {
                //Only update the used record
                if (record.IsUsed)
                    usedRowContract.Content[indexInContext] = (int.Parse(usedRowContract.Content[indexInContext]) + ((record.EntityReservations ?? 0) + (record.DirectReservations ?? 0))).ToString();
            }
        }

        private void EntityInsert(AllotmentByEntityRecord record, string key_allotment, string key_entity)
        {
            var rowContract = CreateRow(record.IsVirtual);
            rowContract.InitializeList((To - From).Days + 1, "0");

            //Details
            rowContract.Id = key_entity;
            rowContract.Title = record.EntityId.HasValue ? record.EntityDescription : DirectTranslation;
            rowContract.SortKeys.Add(record.HotelDescription);
            rowContract.SortKeys.Add((record.IsGuarantee ? "3" : (record.AffectBookingChannel ? "2" : "1")));
            rowContract.SortKeys.Add(record.AllotmentDescription);
            rowContract.SortKeys.Add(record.EntityId.HasValue ? "5" + record.EntityDescription : "4");

            //Styles
            rowContract.FontBold = false;
            rowContract.FontItalic = false;
            rowContract.LeftMargin = 10;
            rowContract.CustomBackground = null;

            //Values
            var indexInContext = ((DateTime)record.Id - From).Days;
            rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + (record.EntityId.HasValue ? (record.EntityReservations ?? 0) : (record.DirectReservations ?? 0))).ToString();

            //Insert in correct place
            var parentIndex = GetRowIndexByKey(key_allotment);
            if (parentIndex + 1 == Rows.Count) Rows.Add(rowContract);
            else Rows.Insert(parentIndex + 1, rowContract);
        }

        private void EntityUpdate(AllotmentByEntityRecord record, string key_entity)
        {
            var rowContract = GetRowByKey(key_entity);
            var indexInContext = ((DateTime)record.Id - From).Days;
            rowContract.Content[indexInContext] = (int.Parse(rowContract.Content[indexInContext]) + (record.EntityId.HasValue ? (record.EntityReservations ?? 0) : (record.DirectReservations ?? 0))).ToString();
        }
    }

    #endregion
}