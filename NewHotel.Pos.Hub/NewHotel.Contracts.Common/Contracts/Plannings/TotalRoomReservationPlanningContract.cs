﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TotalRoomReservationPlanningContract : BaseContract
    {
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public long RoomInventory { get; set; }
        [DataMember]
        public long Availability { get; set; }
        [DataMember]
        public long Guaranties { get; set; }
    }
}