﻿using System;
using System.Linq;
using System.Collections.Generic;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class Error
    {
        public short Code;
        public short SubCode;

        public Error() { }

        private Error(int value)
        {
            Code = (short)(value & 0x0000FFFF);
            SubCode = (short)(value >> 16);
        }

        public static implicit operator Error(int value)
        {
            return new Error(value);
        }

        public override string ToString()
        {
            return string.Format("{0}.{1}", Code, SubCode);
        }
    }

    public static class ErrorResponseCollection
    {
        public static void Add(this List<ErrorResponse> errors, int id, string message)
        {
            errors.Add(new ErrorResponse() { ID = id, Message = message });
        }

        public static void Add(this List<ErrorResponse> errors, ValidationResult result)
        {
            errors.AddRange(result.Select(x => new ErrorResponse() { ID = x.Code, Message = x.Message }));
        }
    }
}