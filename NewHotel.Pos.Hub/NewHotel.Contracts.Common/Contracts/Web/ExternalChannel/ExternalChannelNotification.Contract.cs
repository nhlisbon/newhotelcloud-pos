﻿using System;

namespace NewHotel.Contracts
{
    public class ExternalChannelNotificationContract : BaseContract
    {
        public Guid? EntityId { get; set; }
        public Guid? AgencyId { get; set; }
        public Guid? ContractId { get; set; }
        public Guid? AllotmentId { get; set; }
        public Guid? PriceRateId { get; set; }
        public Guid? RoomTypeId { get; set; }
        public short? RatePlanId { get; set; }
    }
}
