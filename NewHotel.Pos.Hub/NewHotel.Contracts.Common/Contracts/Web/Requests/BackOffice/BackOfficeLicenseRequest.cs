﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Web.Services;

namespace NewHotel.Contracts
{
    public class BackOfficeLicenseRequest : BaseRequest
    {
        public string License;
    }
}
