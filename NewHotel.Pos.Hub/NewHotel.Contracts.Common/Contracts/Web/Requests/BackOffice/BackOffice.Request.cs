﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewHotel.Web.Services;

namespace NewHotel.Contracts
{
    public class BackOfficeRequest : BaseRequest
    {
        public string HotelCode;
        public string Date;
        public int DaysAlert;
        public int DaysSuspend;
    }
}
