﻿using System;

namespace NewHotel.Web.Services
{
    public class QueryFilterRequest : BaseRequest
    {
        public FilterArgumentRequest[] Arguments;
        public int MaxCount;
    }
}
