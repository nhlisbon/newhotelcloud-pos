﻿using System;
using System.Collections.Generic;
using NewHotel.Contracts;

namespace NewHotel.Web.Services
{
    public class BankAccountRequest
    {
        public string BankId { get; set; }
        public string BankAccountDescription { get; set; }
        public string BankAccountCode { get; set; }
        public string BankAccountNib { get; set; }
        public string BankAccountIban { get; set; }
        public string BankAccountFreeCode { get; set; }
    }

    public class ExternalAccountRequest : BaseRequest
    {
        public string Abbreviation { get; set; }
        public string Description { get; set; }
        public AccountType Type { get; set; }
        public long? Language { get; set; }
        public string CurrencyId { get; set; }
        public string CountryId { get; set; }
        public int? ColorForPlanning { get; set; }
        public string FreeCode { get; set; }

        public string HomeAddress1 { get; set; }
        public string HomeAddress2 { get; set; }
        public string HomeCountry { get; set; }
        public string HomeDoor { get; set; }
        public string HomeLocation { get; set; }
        public string HomePostalCode { get; set; }
        public string HomeDistrictId { get; set; }

        public string FiscalAddress1 { get; set; }
        public string FiscalAddress2 { get; set; }
        public string FiscalCountry { get; set; }
        public string FiscalDoor { get; set; }
        public string FiscalLocation { get; set; }
        public string FiscalPostalCode { get; set; }
        public string FiscalDistrictId { get; set; }

        public string FiscalNumber { get; set; }
        public string FiscalRegister { get; set; }
        public string EmailAddress { get; set; }
        public string PersonalHomePage { get; set; }
        public string SkypeName { get; set; }
        public string BusinessPhone { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string FaxNumber { get; set; }
        public string Comment { get; set; }

        public decimal? AuthorizedMaxRisk { get; set; }
        public bool UnderSupervision { get; set; }
        public short? DaysOfExpired { get; set; }
        public decimal? CommissionValue { get; set; }
        public string AccountGroupId { get; set; }
        public string FinancialCategoryPurchasesId { get; set; }
        public string FinancialCategorySalesId { get; set; }

        public List<BankAccountRequest> BankAccounts { get; private set; }

        public ExternalAccountRequest()
        {
            BankAccounts = new List<BankAccountRequest>();
        }
    }
}