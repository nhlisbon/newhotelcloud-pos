﻿using NewHotel.Contracts;
using System;

namespace NewHotel.Web.Services
{
    public class AlterRoomStatusRequest : BaseRequest
    {
        public Guid RoomId { get; set; }
        public RoomStatus Status { get; set; }
    }
}