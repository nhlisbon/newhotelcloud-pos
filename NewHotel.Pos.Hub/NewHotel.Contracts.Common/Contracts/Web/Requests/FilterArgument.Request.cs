﻿using NewHotel.Core;
using System.Xml.Serialization;

namespace NewHotel.Web.Services
{
    public class FilterArgumentRequest
    {
        public string FilterName;
        public FilterTypes FilterType;
        [XmlElement(Type = typeof(string))]
        public object FilterValue;
    }
}