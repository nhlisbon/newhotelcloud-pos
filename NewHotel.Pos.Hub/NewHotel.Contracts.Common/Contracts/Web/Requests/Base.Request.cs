﻿using System;

namespace NewHotel.Web.Services
{
    public class BaseRequest
    {
        public string RequestID;
        public string UserID;
        public string Password;
        public string PublicKey;
        public string Culture;
        public DateTime ClientDateTime;
        public string HotelID;
    }
}