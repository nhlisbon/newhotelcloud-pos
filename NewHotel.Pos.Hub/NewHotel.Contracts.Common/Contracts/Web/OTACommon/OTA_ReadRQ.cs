﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Contracts.OTA
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05", IsNullable = false)]
    public partial class OTA_ReadRQ
    {

        private SourceType[] pOSField;

        private UniqueID_Type uniqueIDField;

        private OTA_ReadRQReadRequests readRequestsField;

        private string echoTokenField;

        private System.DateTime timeStampField;

        private bool timeStampFieldSpecified;

        private MessageAcknowledgementTypeTarget targetField;

        private bool targetFieldSpecified;

        private string targetNameField;

        private decimal versionField;

        private string transactionIdentifierField;

        private string sequenceNmbrField;

        private MessageAcknowledgementTypeTransactionStatusCode transactionStatusCodeField;

        private bool transactionStatusCodeFieldSpecified;

        private bool retransmissionIndicatorField;

        private bool retransmissionIndicatorFieldSpecified;

        private string correlationIDField;

        private string primaryLangIDField;

        private string altLangIDField;

        private string reqRespVersionField;

        private string reservationTypeField;

        private bool returnListIndicatorField;

        private bool returnListIndicatorFieldSpecified;

        private bool moreIndicatorField;

        private bool moreIndicatorFieldSpecified;

        private string moreDataEchoTokenField;

        private string maxResponsesField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Source", IsNullable = false)]
        public SourceType[] POS
        {
            get
            {
                return this.pOSField;
            }
            set
            {
                this.pOSField = value;
            }
        }

        /// <remarks/>
        public UniqueID_Type UniqueID
        {
            get
            {
                return this.uniqueIDField;
            }
            set
            {
                this.uniqueIDField = value;
            }
        }

        /// <remarks/>
        public OTA_ReadRQReadRequests ReadRequests
        {
            get
            {
                return this.readRequestsField;
            }
            set
            {
                this.readRequestsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string EchoToken
        {
            get
            {
                return this.echoTokenField;
            }
            set
            {
                this.echoTokenField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime TimeStamp
        {
            get
            {
                return this.timeStampField;
            }
            set
            {
                this.timeStampField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TimeStampSpecified
        {
            get
            {
                return this.timeStampFieldSpecified;
            }
            set
            {
                this.timeStampFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public MessageAcknowledgementTypeTarget Target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TargetSpecified
        {
            get
            {
                return this.targetFieldSpecified;
            }
            set
            {
                this.targetFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TargetName
        {
            get
            {
                return this.targetNameField;
            }
            set
            {
                this.targetNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TransactionIdentifier
        {
            get
            {
                return this.transactionIdentifierField;
            }
            set
            {
                this.transactionIdentifierField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string SequenceNmbr
        {
            get
            {
                return this.sequenceNmbrField;
            }
            set
            {
                this.sequenceNmbrField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public MessageAcknowledgementTypeTransactionStatusCode TransactionStatusCode
        {
            get
            {
                return this.transactionStatusCodeField;
            }
            set
            {
                this.transactionStatusCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TransactionStatusCodeSpecified
        {
            get
            {
                return this.transactionStatusCodeFieldSpecified;
            }
            set
            {
                this.transactionStatusCodeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool RetransmissionIndicator
        {
            get
            {
                return this.retransmissionIndicatorField;
            }
            set
            {
                this.retransmissionIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RetransmissionIndicatorSpecified
        {
            get
            {
                return this.retransmissionIndicatorFieldSpecified;
            }
            set
            {
                this.retransmissionIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CorrelationID
        {
            get
            {
                return this.correlationIDField;
            }
            set
            {
                this.correlationIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "language")]
        public string PrimaryLangID
        {
            get
            {
                return this.primaryLangIDField;
            }
            set
            {
                this.primaryLangIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "language")]
        public string AltLangID
        {
            get
            {
                return this.altLangIDField;
            }
            set
            {
                this.altLangIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ReqRespVersion
        {
            get
            {
                return this.reqRespVersionField;
            }
            set
            {
                this.reqRespVersionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ReservationType
        {
            get
            {
                return this.reservationTypeField;
            }
            set
            {
                this.reservationTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool ReturnListIndicator
        {
            get
            {
                return this.returnListIndicatorField;
            }
            set
            {
                this.returnListIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ReturnListIndicatorSpecified
        {
            get
            {
                return this.returnListIndicatorFieldSpecified;
            }
            set
            {
                this.returnListIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool MoreIndicator
        {
            get
            {
                return this.moreIndicatorField;
            }
            set
            {
                this.moreIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MoreIndicatorSpecified
        {
            get
            {
                return this.moreIndicatorFieldSpecified;
            }
            set
            {
                this.moreIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MoreDataEchoToken
        {
            get
            {
                return this.moreDataEchoTokenField;
            }
            set
            {
                this.moreDataEchoTokenField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "positiveInteger")]
        public string MaxResponses
        {
            get
            {
                return this.maxResponsesField;
            }
            set
            {
                this.maxResponsesField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequests
    {

        private object[] itemsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AirReadRequest", typeof(OTA_ReadRQReadRequestsAirReadRequest))]
        [System.Xml.Serialization.XmlElementAttribute("CruiseReadRequest", typeof(OTA_ReadRQReadRequestsCruiseReadRequest))]
        [System.Xml.Serialization.XmlElementAttribute("GlobalReservationReadRequest", typeof(OTA_ReadRQReadRequestsGlobalReservationReadRequest))]
        [System.Xml.Serialization.XmlElementAttribute("GolfReadRequest", typeof(OTA_ReadRQReadRequestsGolfReadRequest))]
        [System.Xml.Serialization.XmlElementAttribute("HotelReadRequest", typeof(OTA_ReadRQReadRequestsHotelReadRequest))]
        [System.Xml.Serialization.XmlElementAttribute("PkgReadRequest", typeof(OTA_ReadRQReadRequestsPkgReadRequest))]
        [System.Xml.Serialization.XmlElementAttribute("ProfileReadRequest", typeof(OTA_ReadRQReadRequestsProfileReadRequest))]
        [System.Xml.Serialization.XmlElementAttribute("RailReadRequest", typeof(OTA_ReadRQReadRequestsRailReadRequest))]
        [System.Xml.Serialization.XmlElementAttribute("ReadRequest", typeof(OTA_ReadRQReadRequestsReadRequest))]
        [System.Xml.Serialization.XmlElementAttribute("VehicleReadRequest", typeof(OTA_ReadRQReadRequestsVehicleReadRequest))]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsAirReadRequest
    {

        private SourceType[] pOSField;

        private CompanyNameType airlineField;

        private string flightNumberField;

        private LocationType departureAirportField;

        private System.DateTime departureDateField;

        private bool departureDateFieldSpecified;

        private PersonNameType nameField;

        private OTA_ReadRQReadRequestsAirReadRequestTelephone telephoneField;

        private OTA_ReadRQReadRequestsAirReadRequestCustLoyalty custLoyaltyField;

        private PaymentCardType creditCardInfoField;

        private TicketingInfoRS_Type ticketNumberField;

        private OTA_ReadRQReadRequestsAirReadRequestQueueInfo queueInfoField;

        private OTA_ReadRQReadRequestsAirReadRequestDate dateField;

        private TPA_ExtensionsType tPA_ExtensionsField;

        private string seatNumberField;

        private bool includeFF_EquivPartnerLevField;

        private bool includeFF_EquivPartnerLevFieldSpecified;

        private bool returnFF_NumberField;

        private bool returnFF_NumberFieldSpecified;

        private bool returnDownlineSegField;

        private bool returnDownlineSegFieldSpecified;

        private string infoToReturnField;

        private OTA_ReadRQReadRequestsAirReadRequestFF_RequestCriteria fF_RequestCriteriaField;

        private bool fF_RequestCriteriaFieldSpecified;

        private bool no_SSR_IndField;

        private bool no_SSR_IndFieldSpecified;

        private string startField;

        private string durationField;

        private string endField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Source", IsNullable = false)]
        public SourceType[] POS
        {
            get
            {
                return this.pOSField;
            }
            set
            {
                this.pOSField = value;
            }
        }

        /// <remarks/>
        public CompanyNameType Airline
        {
            get
            {
                return this.airlineField;
            }
            set
            {
                this.airlineField = value;
            }
        }

        /// <remarks/>
        public string FlightNumber
        {
            get
            {
                return this.flightNumberField;
            }
            set
            {
                this.flightNumberField = value;
            }
        }

        /// <remarks/>
        public LocationType DepartureAirport
        {
            get
            {
                return this.departureAirportField;
            }
            set
            {
                this.departureAirportField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime DepartureDate
        {
            get
            {
                return this.departureDateField;
            }
            set
            {
                this.departureDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DepartureDateSpecified
        {
            get
            {
                return this.departureDateFieldSpecified;
            }
            set
            {
                this.departureDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public PersonNameType Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public OTA_ReadRQReadRequestsAirReadRequestTelephone Telephone
        {
            get
            {
                return this.telephoneField;
            }
            set
            {
                this.telephoneField = value;
            }
        }

        /// <remarks/>
        public OTA_ReadRQReadRequestsAirReadRequestCustLoyalty CustLoyalty
        {
            get
            {
                return this.custLoyaltyField;
            }
            set
            {
                this.custLoyaltyField = value;
            }
        }

        /// <remarks/>
        public PaymentCardType CreditCardInfo
        {
            get
            {
                return this.creditCardInfoField;
            }
            set
            {
                this.creditCardInfoField = value;
            }
        }

        /// <remarks/>
        public TicketingInfoRS_Type TicketNumber
        {
            get
            {
                return this.ticketNumberField;
            }
            set
            {
                this.ticketNumberField = value;
            }
        }

        /// <remarks/>
        public OTA_ReadRQReadRequestsAirReadRequestQueueInfo QueueInfo
        {
            get
            {
                return this.queueInfoField;
            }
            set
            {
                this.queueInfoField = value;
            }
        }

        /// <remarks/>
        public OTA_ReadRQReadRequestsAirReadRequestDate Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        public TPA_ExtensionsType TPA_Extensions
        {
            get
            {
                return this.tPA_ExtensionsField;
            }
            set
            {
                this.tPA_ExtensionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SeatNumber
        {
            get
            {
                return this.seatNumberField;
            }
            set
            {
                this.seatNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool IncludeFF_EquivPartnerLev
        {
            get
            {
                return this.includeFF_EquivPartnerLevField;
            }
            set
            {
                this.includeFF_EquivPartnerLevField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IncludeFF_EquivPartnerLevSpecified
        {
            get
            {
                return this.includeFF_EquivPartnerLevFieldSpecified;
            }
            set
            {
                this.includeFF_EquivPartnerLevFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool ReturnFF_Number
        {
            get
            {
                return this.returnFF_NumberField;
            }
            set
            {
                this.returnFF_NumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ReturnFF_NumberSpecified
        {
            get
            {
                return this.returnFF_NumberFieldSpecified;
            }
            set
            {
                this.returnFF_NumberFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool ReturnDownlineSeg
        {
            get
            {
                return this.returnDownlineSegField;
            }
            set
            {
                this.returnDownlineSegField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ReturnDownlineSegSpecified
        {
            get
            {
                return this.returnDownlineSegFieldSpecified;
            }
            set
            {
                this.returnDownlineSegFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string InfoToReturn
        {
            get
            {
                return this.infoToReturnField;
            }
            set
            {
                this.infoToReturnField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public OTA_ReadRQReadRequestsAirReadRequestFF_RequestCriteria FF_RequestCriteria
        {
            get
            {
                return this.fF_RequestCriteriaField;
            }
            set
            {
                this.fF_RequestCriteriaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FF_RequestCriteriaSpecified
        {
            get
            {
                return this.fF_RequestCriteriaFieldSpecified;
            }
            set
            {
                this.fF_RequestCriteriaFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool No_SSR_Ind
        {
            get
            {
                return this.no_SSR_IndField;
            }
            set
            {
                this.no_SSR_IndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool No_SSR_IndSpecified
        {
            get
            {
                return this.no_SSR_IndFieldSpecified;
            }
            set
            {
                this.no_SSR_IndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string End
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsAirReadRequestCustLoyalty
    {

        private PaymentCardTypeCustLoyaltyShareSynchInd shareSynchIndField;

        private bool shareSynchIndFieldSpecified;

        private PaymentCardTypeCustLoyaltyShareMarketInd shareMarketIndField;

        private bool shareMarketIndFieldSpecified;

        private string programIDField;

        private string membershipIDField;

        private string travelSectorField;

        private string[] vendorCodeField;

        private bool primaryLoyaltyIndicatorField;

        private bool primaryLoyaltyIndicatorFieldSpecified;

        private string allianceLoyaltyLevelNameField;

        private string customerTypeField;

        private string customerValueField;

        private string passwordField;

        private string loyalLevelField;

        private string loyalLevelCodeField;

        private PaymentCardTypeCustLoyaltySingleVendorInd singleVendorIndField;

        private bool singleVendorIndFieldSpecified;

        private System.DateTime signupDateField;

        private bool signupDateFieldSpecified;

        private System.DateTime effectiveDateField;

        private bool effectiveDateFieldSpecified;

        private System.DateTime expireDateField;

        private bool expireDateFieldSpecified;

        private bool expireDateExclusiveIndicatorField;

        private bool expireDateExclusiveIndicatorFieldSpecified;

        private string rPHField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PaymentCardTypeCustLoyaltyShareSynchInd ShareSynchInd
        {
            get
            {
                return this.shareSynchIndField;
            }
            set
            {
                this.shareSynchIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ShareSynchIndSpecified
        {
            get
            {
                return this.shareSynchIndFieldSpecified;
            }
            set
            {
                this.shareSynchIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PaymentCardTypeCustLoyaltyShareMarketInd ShareMarketInd
        {
            get
            {
                return this.shareMarketIndField;
            }
            set
            {
                this.shareMarketIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ShareMarketIndSpecified
        {
            get
            {
                return this.shareMarketIndFieldSpecified;
            }
            set
            {
                this.shareMarketIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ProgramID
        {
            get
            {
                return this.programIDField;
            }
            set
            {
                this.programIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MembershipID
        {
            get
            {
                return this.membershipIDField;
            }
            set
            {
                this.membershipIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TravelSector
        {
            get
            {
                return this.travelSectorField;
            }
            set
            {
                this.travelSectorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] VendorCode
        {
            get
            {
                return this.vendorCodeField;
            }
            set
            {
                this.vendorCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool PrimaryLoyaltyIndicator
        {
            get
            {
                return this.primaryLoyaltyIndicatorField;
            }
            set
            {
                this.primaryLoyaltyIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrimaryLoyaltyIndicatorSpecified
        {
            get
            {
                return this.primaryLoyaltyIndicatorFieldSpecified;
            }
            set
            {
                this.primaryLoyaltyIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AllianceLoyaltyLevelName
        {
            get
            {
                return this.allianceLoyaltyLevelNameField;
            }
            set
            {
                this.allianceLoyaltyLevelNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CustomerType
        {
            get
            {
                return this.customerTypeField;
            }
            set
            {
                this.customerTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CustomerValue
        {
            get
            {
                return this.customerValueField;
            }
            set
            {
                this.customerValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LoyalLevel
        {
            get
            {
                return this.loyalLevelField;
            }
            set
            {
                this.loyalLevelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string LoyalLevelCode
        {
            get
            {
                return this.loyalLevelCodeField;
            }
            set
            {
                this.loyalLevelCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PaymentCardTypeCustLoyaltySingleVendorInd SingleVendorInd
        {
            get
            {
                return this.singleVendorIndField;
            }
            set
            {
                this.singleVendorIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SingleVendorIndSpecified
        {
            get
            {
                return this.singleVendorIndFieldSpecified;
            }
            set
            {
                this.singleVendorIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime SignupDate
        {
            get
            {
                return this.signupDateField;
            }
            set
            {
                this.signupDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SignupDateSpecified
        {
            get
            {
                return this.signupDateFieldSpecified;
            }
            set
            {
                this.signupDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime EffectiveDate
        {
            get
            {
                return this.effectiveDateField;
            }
            set
            {
                this.effectiveDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EffectiveDateSpecified
        {
            get
            {
                return this.effectiveDateFieldSpecified;
            }
            set
            {
                this.effectiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime ExpireDate
        {
            get
            {
                return this.expireDateField;
            }
            set
            {
                this.expireDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExpireDateSpecified
        {
            get
            {
                return this.expireDateFieldSpecified;
            }
            set
            {
                this.expireDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool ExpireDateExclusiveIndicator
        {
            get
            {
                return this.expireDateExclusiveIndicatorField;
            }
            set
            {
                this.expireDateExclusiveIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExpireDateExclusiveIndicatorSpecified
        {
            get
            {
                return this.expireDateExclusiveIndicatorFieldSpecified;
            }
            set
            {
                this.expireDateExclusiveIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RPH
        {
            get
            {
                return this.rPHField;
            }
            set
            {
                this.rPHField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsAirReadRequestDate
    {

        private string startField;

        private string durationField;

        private string endField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string End
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsAirReadRequestQueueInfo
    {

        private OTA_ReadRQReadRequestsAirReadRequestQueueInfoQueue[] queueField;

        private bool firstItemOnlyIndField;

        private bool firstItemOnlyIndFieldSpecified;

        private bool removeFromQueueIndField;

        private bool removeFromQueueIndFieldSpecified;

        private bool fullDataIndField;

        private bool fullDataIndFieldSpecified;

        private string startDateField;

        private string endDateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Queue")]
        public OTA_ReadRQReadRequestsAirReadRequestQueueInfoQueue[] Queue
        {
            get
            {
                return this.queueField;
            }
            set
            {
                this.queueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool FirstItemOnlyInd
        {
            get
            {
                return this.firstItemOnlyIndField;
            }
            set
            {
                this.firstItemOnlyIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FirstItemOnlyIndSpecified
        {
            get
            {
                return this.firstItemOnlyIndFieldSpecified;
            }
            set
            {
                this.firstItemOnlyIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool RemoveFromQueueInd
        {
            get
            {
                return this.removeFromQueueIndField;
            }
            set
            {
                this.removeFromQueueIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RemoveFromQueueIndSpecified
        {
            get
            {
                return this.removeFromQueueIndFieldSpecified;
            }
            set
            {
                this.removeFromQueueIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool FullDataInd
        {
            get
            {
                return this.fullDataIndField;
            }
            set
            {
                this.fullDataIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FullDataIndSpecified
        {
            get
            {
                return this.fullDataIndFieldSpecified;
            }
            set
            {
                this.fullDataIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string StartDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string EndDate
        {
            get
            {
                return this.endDateField;
            }
            set
            {
                this.endDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsAirReadRequestQueueInfoQueue
    {

        private string pseudoCityCodeField;

        private string queueNumberField;

        private string queueCategoryField;

        private string systemCodeField;

        private string queueIDField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PseudoCityCode
        {
            get
            {
                return this.pseudoCityCodeField;
            }
            set
            {
                this.pseudoCityCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string QueueNumber
        {
            get
            {
                return this.queueNumberField;
            }
            set
            {
                this.queueNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string QueueCategory
        {
            get
            {
                return this.queueCategoryField;
            }
            set
            {
                this.queueCategoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SystemCode
        {
            get
            {
                return this.systemCodeField;
            }
            set
            {
                this.systemCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string QueueID
        {
            get
            {
                return this.queueIDField;
            }
            set
            {
                this.queueIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsAirReadRequestTelephone
    {

        private ItemSearchCriterionTypeTelephoneShareSynchInd shareSynchIndField;

        private bool shareSynchIndFieldSpecified;

        private ItemSearchCriterionTypeTelephoneShareMarketInd shareMarketIndField;

        private bool shareMarketIndFieldSpecified;

        private string phoneLocationTypeField;

        private string phoneTechTypeField;

        private string phoneUseTypeField;

        private string countryAccessCodeField;

        private string areaCityCodeField;

        private string phoneNumberField;

        private string extensionField;

        private string pINField;

        private string remarkField;

        private bool formattedIndField;

        private bool formattedIndFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ItemSearchCriterionTypeTelephoneShareSynchInd ShareSynchInd
        {
            get
            {
                return this.shareSynchIndField;
            }
            set
            {
                this.shareSynchIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ShareSynchIndSpecified
        {
            get
            {
                return this.shareSynchIndFieldSpecified;
            }
            set
            {
                this.shareSynchIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ItemSearchCriterionTypeTelephoneShareMarketInd ShareMarketInd
        {
            get
            {
                return this.shareMarketIndField;
            }
            set
            {
                this.shareMarketIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ShareMarketIndSpecified
        {
            get
            {
                return this.shareMarketIndFieldSpecified;
            }
            set
            {
                this.shareMarketIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PhoneLocationType
        {
            get
            {
                return this.phoneLocationTypeField;
            }
            set
            {
                this.phoneLocationTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PhoneTechType
        {
            get
            {
                return this.phoneTechTypeField;
            }
            set
            {
                this.phoneTechTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PhoneUseType
        {
            get
            {
                return this.phoneUseTypeField;
            }
            set
            {
                this.phoneUseTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CountryAccessCode
        {
            get
            {
                return this.countryAccessCodeField;
            }
            set
            {
                this.countryAccessCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AreaCityCode
        {
            get
            {
                return this.areaCityCodeField;
            }
            set
            {
                this.areaCityCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PhoneNumber
        {
            get
            {
                return this.phoneNumberField;
            }
            set
            {
                this.phoneNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Extension
        {
            get
            {
                return this.extensionField;
            }
            set
            {
                this.extensionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PIN
        {
            get
            {
                return this.pINField;
            }
            set
            {
                this.pINField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Remark
        {
            get
            {
                return this.remarkField;
            }
            set
            {
                this.remarkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool FormattedInd
        {
            get
            {
                return this.formattedIndField;
            }
            set
            {
                this.formattedIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FormattedIndSpecified
        {
            get
            {
                return this.formattedIndFieldSpecified;
            }
            set
            {
                this.formattedIndFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsCruiseReadRequest
    {

        private OTA_ReadRQReadRequestsCruiseReadRequestSelectedSailing selectedSailingField;

        private PersonNameType guestInfoField;

        private bool historyRequestedIndField;

        private bool historyRequestedIndFieldSpecified;

        /// <remarks/>
        public OTA_ReadRQReadRequestsCruiseReadRequestSelectedSailing SelectedSailing
        {
            get
            {
                return this.selectedSailingField;
            }
            set
            {
                this.selectedSailingField = value;
            }
        }

        /// <remarks/>
        public PersonNameType GuestInfo
        {
            get
            {
                return this.guestInfoField;
            }
            set
            {
                this.guestInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool HistoryRequestedInd
        {
            get
            {
                return this.historyRequestedIndField;
            }
            set
            {
                this.historyRequestedIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool HistoryRequestedIndSpecified
        {
            get
            {
                return this.historyRequestedIndFieldSpecified;
            }
            set
            {
                this.historyRequestedIndFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsCruiseReadRequestSelectedSailing
    {

        private string voyageIDField;

        private string startField;

        private string durationField;

        private string endField;

        private string vendorCodeField;

        private string vendorNameField;

        private string shipCodeField;

        private string shipNameField;

        private string vendorCodeContextField;

        private string statusField;

        private string groupCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string VoyageID
        {
            get
            {
                return this.voyageIDField;
            }
            set
            {
                this.voyageIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string End
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string VendorCode
        {
            get
            {
                return this.vendorCodeField;
            }
            set
            {
                this.vendorCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string VendorName
        {
            get
            {
                return this.vendorNameField;
            }
            set
            {
                this.vendorNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ShipCode
        {
            get
            {
                return this.shipCodeField;
            }
            set
            {
                this.shipCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ShipName
        {
            get
            {
                return this.shipNameField;
            }
            set
            {
                this.shipNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string VendorCodeContext
        {
            get
            {
                return this.vendorCodeContextField;
            }
            set
            {
                this.vendorCodeContextField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string GroupCode
        {
            get
            {
                return this.groupCodeField;
            }
            set
            {
                this.groupCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsGlobalReservationReadRequest
    {

        private PersonNameType travelerNameField;

        private string startField;

        private string durationField;

        private string endField;

        /// <remarks/>
        public PersonNameType TravelerName
        {
            get
            {
                return this.travelerNameField;
            }
            set
            {
                this.travelerNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string End
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsGolfReadRequest
    {

        private OTA_ReadRQReadRequestsGolfReadRequestMembership[] membershipField;

        private PersonNameType nameField;

        private string idField;

        private string playDateTimeField;

        private string packageIDField;

        private string roundIDField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Membership")]
        public OTA_ReadRQReadRequestsGolfReadRequestMembership[] Membership
        {
            get
            {
                return this.membershipField;
            }
            set
            {
                this.membershipField = value;
            }
        }

        /// <remarks/>
        public PersonNameType Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PlayDateTime
        {
            get
            {
                return this.playDateTimeField;
            }
            set
            {
                this.playDateTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PackageID
        {
            get
            {
                return this.packageIDField;
            }
            set
            {
                this.packageIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "positiveInteger")]
        public string RoundID
        {
            get
            {
                return this.roundIDField;
            }
            set
            {
                this.roundIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsGolfReadRequestMembership
    {

        private PaymentCardTypeCustLoyaltyShareSynchInd shareSynchIndField;

        private bool shareSynchIndFieldSpecified;

        private PaymentCardTypeCustLoyaltyShareMarketInd shareMarketIndField;

        private bool shareMarketIndFieldSpecified;

        private string programIDField;

        private string membershipIDField;

        private string travelSectorField;

        private string[] vendorCodeField;

        private bool primaryLoyaltyIndicatorField;

        private bool primaryLoyaltyIndicatorFieldSpecified;

        private string allianceLoyaltyLevelNameField;

        private string customerTypeField;

        private string customerValueField;

        private string passwordField;

        private string loyalLevelField;

        private string loyalLevelCodeField;

        private PaymentCardTypeCustLoyaltySingleVendorInd singleVendorIndField;

        private bool singleVendorIndFieldSpecified;

        private System.DateTime signupDateField;

        private bool signupDateFieldSpecified;

        private System.DateTime effectiveDateField;

        private bool effectiveDateFieldSpecified;

        private System.DateTime expireDateField;

        private bool expireDateFieldSpecified;

        private bool expireDateExclusiveIndicatorField;

        private bool expireDateExclusiveIndicatorFieldSpecified;

        private string rPHField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PaymentCardTypeCustLoyaltyShareSynchInd ShareSynchInd
        {
            get
            {
                return this.shareSynchIndField;
            }
            set
            {
                this.shareSynchIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ShareSynchIndSpecified
        {
            get
            {
                return this.shareSynchIndFieldSpecified;
            }
            set
            {
                this.shareSynchIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PaymentCardTypeCustLoyaltyShareMarketInd ShareMarketInd
        {
            get
            {
                return this.shareMarketIndField;
            }
            set
            {
                this.shareMarketIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ShareMarketIndSpecified
        {
            get
            {
                return this.shareMarketIndFieldSpecified;
            }
            set
            {
                this.shareMarketIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ProgramID
        {
            get
            {
                return this.programIDField;
            }
            set
            {
                this.programIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MembershipID
        {
            get
            {
                return this.membershipIDField;
            }
            set
            {
                this.membershipIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TravelSector
        {
            get
            {
                return this.travelSectorField;
            }
            set
            {
                this.travelSectorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] VendorCode
        {
            get
            {
                return this.vendorCodeField;
            }
            set
            {
                this.vendorCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool PrimaryLoyaltyIndicator
        {
            get
            {
                return this.primaryLoyaltyIndicatorField;
            }
            set
            {
                this.primaryLoyaltyIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrimaryLoyaltyIndicatorSpecified
        {
            get
            {
                return this.primaryLoyaltyIndicatorFieldSpecified;
            }
            set
            {
                this.primaryLoyaltyIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AllianceLoyaltyLevelName
        {
            get
            {
                return this.allianceLoyaltyLevelNameField;
            }
            set
            {
                this.allianceLoyaltyLevelNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CustomerType
        {
            get
            {
                return this.customerTypeField;
            }
            set
            {
                this.customerTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CustomerValue
        {
            get
            {
                return this.customerValueField;
            }
            set
            {
                this.customerValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LoyalLevel
        {
            get
            {
                return this.loyalLevelField;
            }
            set
            {
                this.loyalLevelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string LoyalLevelCode
        {
            get
            {
                return this.loyalLevelCodeField;
            }
            set
            {
                this.loyalLevelCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PaymentCardTypeCustLoyaltySingleVendorInd SingleVendorInd
        {
            get
            {
                return this.singleVendorIndField;
            }
            set
            {
                this.singleVendorIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SingleVendorIndSpecified
        {
            get
            {
                return this.singleVendorIndFieldSpecified;
            }
            set
            {
                this.singleVendorIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime SignupDate
        {
            get
            {
                return this.signupDateField;
            }
            set
            {
                this.signupDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SignupDateSpecified
        {
            get
            {
                return this.signupDateFieldSpecified;
            }
            set
            {
                this.signupDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime EffectiveDate
        {
            get
            {
                return this.effectiveDateField;
            }
            set
            {
                this.effectiveDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EffectiveDateSpecified
        {
            get
            {
                return this.effectiveDateFieldSpecified;
            }
            set
            {
                this.effectiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime ExpireDate
        {
            get
            {
                return this.expireDateField;
            }
            set
            {
                this.expireDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExpireDateSpecified
        {
            get
            {
                return this.expireDateFieldSpecified;
            }
            set
            {
                this.expireDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool ExpireDateExclusiveIndicator
        {
            get
            {
                return this.expireDateExclusiveIndicatorField;
            }
            set
            {
                this.expireDateExclusiveIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExpireDateExclusiveIndicatorSpecified
        {
            get
            {
                return this.expireDateExclusiveIndicatorFieldSpecified;
            }
            set
            {
                this.expireDateExclusiveIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RPH
        {
            get
            {
                return this.rPHField;
            }
            set
            {
                this.rPHField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsHotelReadRequest
    {

        private string cityNameField;

        private OTA_ReadRQReadRequestsHotelReadRequestAirport airportField;

        private OTA_ReadRQReadRequestsHotelReadRequestUserID userIDField;

        private VerificationType verificationField;

        private OTA_ReadRQReadRequestsHotelReadRequestSelectionCriteria selectionCriteriaField;

        private TPA_ExtensionsType tPA_ExtensionsField;

        private string chainCodeField;

        private string brandCodeField;

        private string hotelCodeField;

        private string hotelCityCodeField;

        private string hotelNameField;

        private string hotelCodeContextField;

        private string chainNameField;

        private string brandNameField;

        private string areaIDField;

        private string tTIcodeField;

        /// <remarks/>
        public string CityName
        {
            get
            {
                return this.cityNameField;
            }
            set
            {
                this.cityNameField = value;
            }
        }

        /// <remarks/>
        public OTA_ReadRQReadRequestsHotelReadRequestAirport Airport
        {
            get
            {
                return this.airportField;
            }
            set
            {
                this.airportField = value;
            }
        }

        /// <remarks/>
        public OTA_ReadRQReadRequestsHotelReadRequestUserID UserID
        {
            get
            {
                return this.userIDField;
            }
            set
            {
                this.userIDField = value;
            }
        }

        /// <remarks/>
        public VerificationType Verification
        {
            get
            {
                return this.verificationField;
            }
            set
            {
                this.verificationField = value;
            }
        }

        /// <remarks/>
        public OTA_ReadRQReadRequestsHotelReadRequestSelectionCriteria SelectionCriteria
        {
            get
            {
                return this.selectionCriteriaField;
            }
            set
            {
                this.selectionCriteriaField = value;
            }
        }

        /// <remarks/>
        public TPA_ExtensionsType TPA_Extensions
        {
            get
            {
                return this.tPA_ExtensionsField;
            }
            set
            {
                this.tPA_ExtensionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ChainCode
        {
            get
            {
                return this.chainCodeField;
            }
            set
            {
                this.chainCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string BrandCode
        {
            get
            {
                return this.brandCodeField;
            }
            set
            {
                this.brandCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string HotelCode
        {
            get
            {
                return this.hotelCodeField;
            }
            set
            {
                this.hotelCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string HotelCityCode
        {
            get
            {
                return this.hotelCityCodeField;
            }
            set
            {
                this.hotelCityCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string HotelName
        {
            get
            {
                return this.hotelNameField;
            }
            set
            {
                this.hotelNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string HotelCodeContext
        {
            get
            {
                return this.hotelCodeContextField;
            }
            set
            {
                this.hotelCodeContextField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ChainName
        {
            get
            {
                return this.chainNameField;
            }
            set
            {
                this.chainNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string BrandName
        {
            get
            {
                return this.brandNameField;
            }
            set
            {
                this.brandNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AreaID
        {
            get
            {
                return this.areaIDField;
            }
            set
            {
                this.areaIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "positiveInteger")]
        public string TTIcode
        {
            get
            {
                return this.tTIcodeField;
            }
            set
            {
                this.tTIcodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsHotelReadRequestAirport
    {

        private string locationCodeField;

        private string codeContextField;

        private string airportNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LocationCode
        {
            get
            {
                return this.locationCodeField;
            }
            set
            {
                this.locationCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CodeContext
        {
            get
            {
                return this.codeContextField;
            }
            set
            {
                this.codeContextField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AirportName
        {
            get
            {
                return this.airportNameField;
            }
            set
            {
                this.airportNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsHotelReadRequestSelectionCriteria
    {

        private string startField;

        private string durationField;

        private string endField;

        private OTA_ReadRQReadRequestsHotelReadRequestSelectionCriteriaDateType dateTypeField;

        private bool dateTypeFieldSpecified;

        private OTA_ReadRQReadRequestsHotelReadRequestSelectionCriteriaSelectionType selectionTypeField;

        private bool selectionTypeFieldSpecified;

        private string groupCodeField;

        private string resStatusField;

        private string originalDeliveryMethodCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string End
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public OTA_ReadRQReadRequestsHotelReadRequestSelectionCriteriaDateType DateType
        {
            get
            {
                return this.dateTypeField;
            }
            set
            {
                this.dateTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DateTypeSpecified
        {
            get
            {
                return this.dateTypeFieldSpecified;
            }
            set
            {
                this.dateTypeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public OTA_ReadRQReadRequestsHotelReadRequestSelectionCriteriaSelectionType SelectionType
        {
            get
            {
                return this.selectionTypeField;
            }
            set
            {
                this.selectionTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SelectionTypeSpecified
        {
            get
            {
                return this.selectionTypeFieldSpecified;
            }
            set
            {
                this.selectionTypeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string GroupCode
        {
            get
            {
                return this.groupCodeField;
            }
            set
            {
                this.groupCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ResStatus
        {
            get
            {
                return this.resStatusField;
            }
            set
            {
                this.resStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string OriginalDeliveryMethodCode
        {
            get
            {
                return this.originalDeliveryMethodCodeField;
            }
            set
            {
                this.originalDeliveryMethodCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsHotelReadRequestUserID : UniqueID_Type
    {

        private string pinNumberField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PinNumber
        {
            get
            {
                return this.pinNumberField;
            }
            set
            {
                this.pinNumberField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsPkgReadRequest
    {

        private PersonNameType nameField;

        private LocationType arrivalLocationField;

        private LocationType departureLocationField;

        private string travelCodeField;

        private string tourCodeField;

        private string packageIDField;

        private string startField;

        private string durationField;

        private string endField;

        /// <remarks/>
        public PersonNameType Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public LocationType ArrivalLocation
        {
            get
            {
                return this.arrivalLocationField;
            }
            set
            {
                this.arrivalLocationField = value;
            }
        }

        /// <remarks/>
        public LocationType DepartureLocation
        {
            get
            {
                return this.departureLocationField;
            }
            set
            {
                this.departureLocationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TravelCode
        {
            get
            {
                return this.travelCodeField;
            }
            set
            {
                this.travelCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TourCode
        {
            get
            {
                return this.tourCodeField;
            }
            set
            {
                this.tourCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PackageID
        {
            get
            {
                return this.packageIDField;
            }
            set
            {
                this.packageIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string End
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsProfileReadRequest
    {

        private OTA_ReadRQReadRequestsProfileReadRequestUniqueID[] uniqueIDField;

        private ContactPersonType companyField;

        private CustomerType customerField;

        private string startField;

        private string durationField;

        private string endField;

        private OTA_ReadRQReadRequestsProfileReadRequestDateType dateTypeField;

        private bool dateTypeFieldSpecified;

        private string statusCodeField;

        private string profileTypeCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("UniqueID")]
        public OTA_ReadRQReadRequestsProfileReadRequestUniqueID[] UniqueID
        {
            get
            {
                return this.uniqueIDField;
            }
            set
            {
                this.uniqueIDField = value;
            }
        }

        /// <remarks/>
        public ContactPersonType Company
        {
            get
            {
                return this.companyField;
            }
            set
            {
                this.companyField = value;
            }
        }

        /// <remarks/>
        public CustomerType Customer
        {
            get
            {
                return this.customerField;
            }
            set
            {
                this.customerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string End
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public OTA_ReadRQReadRequestsProfileReadRequestDateType DateType
        {
            get
            {
                return this.dateTypeField;
            }
            set
            {
                this.dateTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DateTypeSpecified
        {
            get
            {
                return this.dateTypeFieldSpecified;
            }
            set
            {
                this.dateTypeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string StatusCode
        {
            get
            {
                return this.statusCodeField;
            }
            set
            {
                this.statusCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ProfileTypeCode
        {
            get
            {
                return this.profileTypeCodeField;
            }
            set
            {
                this.profileTypeCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsProfileReadRequestUniqueID : UniqueID_Type
    {

        private string pinNumberField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PinNumber
        {
            get
            {
                return this.pinNumberField;
            }
            set
            {
                this.pinNumberField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsRailReadRequest
    {

        private SourceType[] pOSField;

        private TrainQueryType trainField;

        private PersonNameType travelerField;

        private OTA_ReadRQReadRequestsRailReadRequestBookingDateTime bookingDateTimeField;

        private OTA_ReadRQReadRequestsRailReadRequestDepartureDateTime departureDateTimeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Source", IsNullable = false)]
        public SourceType[] POS
        {
            get
            {
                return this.pOSField;
            }
            set
            {
                this.pOSField = value;
            }
        }

        /// <remarks/>
        public TrainQueryType Train
        {
            get
            {
                return this.trainField;
            }
            set
            {
                this.trainField = value;
            }
        }

        /// <remarks/>
        public PersonNameType Traveler
        {
            get
            {
                return this.travelerField;
            }
            set
            {
                this.travelerField = value;
            }
        }

        /// <remarks/>
        public OTA_ReadRQReadRequestsRailReadRequestBookingDateTime BookingDateTime
        {
            get
            {
                return this.bookingDateTimeField;
            }
            set
            {
                this.bookingDateTimeField = value;
            }
        }

        /// <remarks/>
        public OTA_ReadRQReadRequestsRailReadRequestDepartureDateTime DepartureDateTime
        {
            get
            {
                return this.departureDateTimeField;
            }
            set
            {
                this.departureDateTimeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsRailReadRequestBookingDateTime
    {

        private string startField;

        private string durationField;

        private string endField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string End
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsRailReadRequestDepartureDateTime
    {

        private string startField;

        private string durationField;

        private string endField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string End
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsReadRequest
    {

        private UniqueID_Type uniqueIDField;

        private VerificationType verificationField;

        private bool historyRequestedIndField;

        private bool historyRequestedIndFieldSpecified;

        /// <remarks/>
        public UniqueID_Type UniqueID
        {
            get
            {
                return this.uniqueIDField;
            }
            set
            {
                this.uniqueIDField = value;
            }
        }

        /// <remarks/>
        public VerificationType Verification
        {
            get
            {
                return this.verificationField;
            }
            set
            {
                this.verificationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool HistoryRequestedInd
        {
            get
            {
                return this.historyRequestedIndField;
            }
            set
            {
                this.historyRequestedIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool HistoryRequestedIndSpecified
        {
            get
            {
                return this.historyRequestedIndFieldSpecified;
            }
            set
            {
                this.historyRequestedIndFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ReadRQReadRequestsVehicleReadRequest : VehicleRetrieveResRQCoreType
    {

        private VehicleRetrieveResRQAdditionalInfoType vehRetResRQInfoField;

        /// <remarks/>
        public VehicleRetrieveResRQAdditionalInfoType VehRetResRQInfo
        {
            get
            {
                return this.vehRetResRQInfoField;
            }
            set
            {
                this.vehRetResRQInfoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class GuestTransportationType
    {

        private LocationType guestCityField;

        private LocationType gatewayCityField;

        private string modeField;

        private string statusField;

        /// <remarks/>
        public LocationType GuestCity
        {
            get
            {
                return this.guestCityField;
            }
            set
            {
                this.guestCityField = value;
            }
        }

        /// <remarks/>
        public LocationType GatewayCity
        {
            get
            {
                return this.gatewayCityField;
            }
            set
            {
                this.gatewayCityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Mode
        {
            get
            {
                return this.modeField;
            }
            set
            {
                this.modeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }
}