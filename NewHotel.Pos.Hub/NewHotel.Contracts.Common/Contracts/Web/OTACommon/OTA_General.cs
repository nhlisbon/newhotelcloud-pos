﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.5448
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Xml.Serialization;

// 
// This source code was auto-generated by xsd, Version=2.0.50727.3038.
// 
namespace NewHotel.Contracts.OTA
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum OTA_HotelResNotifRQTarget
    {

        /// <remarks/>
        Test,

        /// <remarks/>
        Production,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum OTA_HotelResNotifRQTransactionStatusCode
    {

        /// <remarks/>
        Start,

        /// <remarks/>
        End,

        /// <remarks/>
        Rollback,

        /// <remarks/>
        InSeries,

        /// <remarks/>
        Continuation,

        /// <remarks/>
        Subsequent,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum RoomTypeTypeRoomGender
    {

        /// <remarks/>
        Male,

        /// <remarks/>
        Female,

        /// <remarks/>
        MaleAndFemale,

        /// <remarks/>
        Unknown,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum GuaranteeTypeDeadlineOffsetDropTime
    {

        /// <remarks/>
        BeforeArrival,

        /// <remarks/>
        AfterBooking,

        /// <remarks/>
        AfterConfirmation,

        /// <remarks/>
        AfterArrival,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum TravelArrangerTypeShareSynchInd
    {

        /// <remarks/>
        Yes,

        /// <remarks/>
        No,

        /// <remarks/>
        Inherit,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum TravelArrangerTypeShareMarketInd
    {

        /// <remarks/>
        Yes,

        /// <remarks/>
        No,

        /// <remarks/>
        Inherit,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum OTA_HotelResNotifRSTarget
    {

        /// <remarks/>
        Test,

        /// <remarks/>
        Production,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum OTA_HotelResNotifRSTransactionStatusCode
    {

        /// <remarks/>
        Start,

        /// <remarks/>
        End,

        /// <remarks/>
        Rollback,

        /// <remarks/>
        InSeries,

        /// <remarks/>
        Continuation,

        /// <remarks/>
        Subsequent,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum CommissionInfoTypeShareMarketInd
    {

        /// <remarks/>
        Yes,

        /// <remarks/>
        No,

        /// <remarks/>
        Inherit,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum CommissionInfoTypeShareSynchInd
    {

        /// <remarks/>
        Yes,

        /// <remarks/>
        No,

        /// <remarks/>
        Inherit,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum MessageTypeOriginalPayloadStdAttributesTarget
    {

        /// <remarks/>
        Test,

        /// <remarks/>
        Production,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum MessageTypeOriginalPayloadStdAttributesTransactionStatusCode
    {

        /// <remarks/>
        Start,

        /// <remarks/>
        End,

        /// <remarks/>
        Rollback,

        /// <remarks/>
        InSeries,

        /// <remarks/>
        Continuation,

        /// <remarks/>
        Subsequent,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum HotelResModifyRequestTypeTarget
    {

        /// <remarks/>
        Test,

        /// <remarks/>
        Production,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum HotelResModifyRequestTypeTransactionStatusCode
    {

        /// <remarks/>
        Start,

        /// <remarks/>
        End,

        /// <remarks/>
        Rollback,

        /// <remarks/>
        InSeries,

        /// <remarks/>
        Continuation,

        /// <remarks/>
        Subsequent,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum HotelResModifyResponseTypeTarget
    {

        /// <remarks/>
        Test,

        /// <remarks/>
        Production,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum HotelResModifyResponseTypeTransactionStatusCode
    {

        /// <remarks/>
        Start,

        /// <remarks/>
        End,

        /// <remarks/>
        Rollback,

        /// <remarks/>
        InSeries,

        /// <remarks/>
        Continuation,

        /// <remarks/>
        Subsequent,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_HotelGetMsgRSMessages
    {

        private MessageType[] messageField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Message")]
        public MessageType[] Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_CancelRQUniqueID : UniqueID_Type
    {

        private string reasonField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Reason
        {
            get
            {
                return this.reasonField;
            }
            set
            {
                this.reasonField = value;
            }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_CancelRQVerification : VerificationType
    {
    }
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_CancelRQBasicPropertyInfo
    {
        private string hotelCode;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string HotelCode
        {
            get { return hotelCode; }
            set { hotelCode = value; }
        }

        private string chainCode;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ChainCode
        {
            get { return chainCode; }
            set { chainCode = value; }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_CancelRQSegment
    {

        private string itinSegNbrField;

        private string firstItinSegNbrField;

        private string lastItinSegNbrField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string ItinSegNbr
        {
            get
            {
                return this.itinSegNbrField;
            }
            set
            {
                this.itinSegNbrField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string FirstItinSegNbr
        {
            get
            {
                return this.firstItinSegNbrField;
            }
            set
            {
                this.firstItinSegNbrField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string LastItinSegNbr
        {
            get
            {
                return this.lastItinSegNbrField;
            }
            set
            {
                this.lastItinSegNbrField = value;
            }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_CancelRQOriginAndDestinationSegment
    {

        private LocationType originLocationField;

        private LocationType destinationLocationField;

        private OTA_CancelRQOriginAndDestinationSegmentTraveler[] travelerField;

        private OTA_CancelRQOriginAndDestinationSegmentSegment[] segmentField;

        /// <remarks/>
        public LocationType OriginLocation
        {
            get
            {
                return this.originLocationField;
            }
            set
            {
                this.originLocationField = value;
            }
        }

        /// <remarks/>
        public LocationType DestinationLocation
        {
            get
            {
                return this.destinationLocationField;
            }
            set
            {
                this.destinationLocationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Traveler")]
        public OTA_CancelRQOriginAndDestinationSegmentTraveler[] Traveler
        {
            get
            {
                return this.travelerField;
            }
            set
            {
                this.travelerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Segment")]
        public OTA_CancelRQOriginAndDestinationSegmentSegment[] Segment
        {
            get
            {
                return this.segmentField;
            }
            set
            {
                this.segmentField = value;
            }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_CancelRQCancellationOverrides
    {

        private CancelRuleType cancellationOverrideField;

        /// <remarks/>
        public CancelRuleType CancellationOverride
        {
            get
            {
                return this.cancellationOverrideField;
            }
            set
            {
                this.cancellationOverrideField = value;
            }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_CancelRQReason : FreeTextType
    {

        private string typeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum OTA_CancelRQTarget
    {

        /// <remarks/>
        Test,

        /// <remarks/>
        Production,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum OTA_CancelRQTransactionStatusCode
    {

        /// <remarks/>
        Start,

        /// <remarks/>
        End,

        /// <remarks/>
        Rollback,

        /// <remarks/>
        InSeries,

        /// <remarks/>
        Continuation,

        /// <remarks/>
        Subsequent,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_CancelRQOriginAndDestinationSegmentTraveler : PersonNameType
    {

        private string docIDField;

        private string docTypeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DocID
        {
            get
            {
                return this.docIDField;
            }
            set
            {
                this.docIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DocType
        {
            get
            {
                return this.docTypeField;
            }
            set
            {
                this.docTypeField = value;
            }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_CancelRQOriginAndDestinationSegmentSegment
    {

        private LocationType departureStationField;

        private LocationType arrivalStationField;

        /// <remarks/>
        public LocationType DepartureStation
        {
            get
            {
                return this.departureStationField;
            }
            set
            {
                this.departureStationField = value;
            }
        }

        /// <remarks/>
        public LocationType ArrivalStation
        {
            get
            {
                return this.arrivalStationField;
            }
            set
            {
                this.arrivalStationField = value;
            }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum OTA_CancelRSTarget
    {

        /// <remarks/>
        Test,

        /// <remarks/>
        Production,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum OTA_CancelRSTransactionStatusCode
    {

        /// <remarks/>
        Start,

        /// <remarks/>
        End,

        /// <remarks/>
        Rollback,

        /// <remarks/>
        InSeries,

        /// <remarks/>
        Continuation,

        /// <remarks/>
        Subsequent,
    }
    ///// <remarks/>
    //[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    //[System.SerializableAttribute()]
    //[System.Diagnostics.DebuggerStepThroughAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    //public partial class CustomerTypeTelephone
    //{

    //    private System.DateTime effectiveDateField;

    //    private bool effectiveDateFieldSpecified;

    //    private System.DateTime expireDateField;

    //    private bool expireDateFieldSpecified;

    //    private bool expireDateExclusiveIndicatorField;

    //    private bool expireDateExclusiveIndicatorFieldSpecified;

    //    private string rPHField;

    //    private TransferActionType transferActionField;

    //    private bool transferActionFieldSpecified;

    //    private string parentCompanyRefField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
    //    public System.DateTime EffectiveDate
    //    {
    //        get
    //        {
    //            return this.effectiveDateField;
    //        }
    //        set
    //        {
    //            this.effectiveDateField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlIgnoreAttribute()]
    //    public bool EffectiveDateSpecified
    //    {
    //        get
    //        {
    //            return this.effectiveDateFieldSpecified;
    //        }
    //        set
    //        {
    //            this.effectiveDateFieldSpecified = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
    //    public System.DateTime ExpireDate
    //    {
    //        get
    //        {
    //            return this.expireDateField;
    //        }
    //        set
    //        {
    //            this.expireDateField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlIgnoreAttribute()]
    //    public bool ExpireDateSpecified
    //    {
    //        get
    //        {
    //            return this.expireDateFieldSpecified;
    //        }
    //        set
    //        {
    //            this.expireDateFieldSpecified = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public bool ExpireDateExclusiveIndicator
    //    {
    //        get
    //        {
    //            return this.expireDateExclusiveIndicatorField;
    //        }
    //        set
    //        {
    //            this.expireDateExclusiveIndicatorField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlIgnoreAttribute()]
    //    public bool ExpireDateExclusiveIndicatorSpecified
    //    {
    //        get
    //        {
    //            return this.expireDateExclusiveIndicatorFieldSpecified;
    //        }
    //        set
    //        {
    //            this.expireDateExclusiveIndicatorFieldSpecified = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string RPH
    //    {
    //        get
    //        {
    //            return this.rPHField;
    //        }
    //        set
    //        {
    //            this.rPHField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public TransferActionType TransferAction
    //    {
    //        get
    //        {
    //            return this.transferActionField;
    //        }
    //        set
    //        {
    //            this.transferActionField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlIgnoreAttribute()]
    //    public bool TransferActionSpecified
    //    {
    //        get
    //        {
    //            return this.transferActionFieldSpecified;
    //        }
    //        set
    //        {
    //            this.transferActionFieldSpecified = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string ParentCompanyRef
    //    {
    //        get
    //        {
    //            return this.parentCompanyRefField;
    //        }
    //        set
    //        {
    //            this.parentCompanyRefField = value;
    //        }
    //    }
    //}


    //public enum OTA_UniqueIdType
    //{
    //    Customer = 1,
    //    CustomerReservationOffice = 2,
    //    CorporationRepresentative = 3,

    //}
}