﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Contracts.OTA
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05", IsNullable = false)]
    public partial class OTA_ResRetrieveRS
    {

        private object[] itemsField;

        private string echoTokenField;

        private System.DateTime timeStampField;

        private bool timeStampFieldSpecified;

        private MessageAcknowledgementTypeTarget targetField;

        private bool targetFieldSpecified;

        private string targetNameField;

        private decimal versionField;

        private string transactionIdentifierField;

        private string sequenceNmbrField;

        private MessageAcknowledgementTypeTransactionStatusCode transactionStatusCodeField;

        private bool transactionStatusCodeFieldSpecified;

        private bool retransmissionIndicatorField;

        private bool retransmissionIndicatorFieldSpecified;

        private string correlationIDField;

        private string primaryLangIDField;

        private string altLangIDField;

        private bool moreIndicatorField;

        private bool moreIndicatorFieldSpecified;

        private string moreDataEchoTokenField;

        private string maxResponsesField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Errors", typeof(ErrorsType))]
        [System.Xml.Serialization.XmlElementAttribute("ReservationsList", typeof(OTA_ResRetrieveRSReservationsList))]
        [System.Xml.Serialization.XmlElementAttribute("Success", typeof(SuccessType))]
        [System.Xml.Serialization.XmlElementAttribute("Warnings", typeof(WarningsType))]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string EchoToken
        {
            get
            {
                return this.echoTokenField;
            }
            set
            {
                this.echoTokenField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime TimeStamp
        {
            get
            {
                return this.timeStampField;
            }
            set
            {
                this.timeStampField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TimeStampSpecified
        {
            get
            {
                return this.timeStampFieldSpecified;
            }
            set
            {
                this.timeStampFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public MessageAcknowledgementTypeTarget Target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TargetSpecified
        {
            get
            {
                return this.targetFieldSpecified;
            }
            set
            {
                this.targetFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TargetName
        {
            get
            {
                return this.targetNameField;
            }
            set
            {
                this.targetNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TransactionIdentifier
        {
            get
            {
                return this.transactionIdentifierField;
            }
            set
            {
                this.transactionIdentifierField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string SequenceNmbr
        {
            get
            {
                return this.sequenceNmbrField;
            }
            set
            {
                this.sequenceNmbrField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public MessageAcknowledgementTypeTransactionStatusCode TransactionStatusCode
        {
            get
            {
                return this.transactionStatusCodeField;
            }
            set
            {
                this.transactionStatusCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TransactionStatusCodeSpecified
        {
            get
            {
                return this.transactionStatusCodeFieldSpecified;
            }
            set
            {
                this.transactionStatusCodeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool RetransmissionIndicator
        {
            get
            {
                return this.retransmissionIndicatorField;
            }
            set
            {
                this.retransmissionIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RetransmissionIndicatorSpecified
        {
            get
            {
                return this.retransmissionIndicatorFieldSpecified;
            }
            set
            {
                this.retransmissionIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CorrelationID
        {
            get
            {
                return this.correlationIDField;
            }
            set
            {
                this.correlationIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "language")]
        public string PrimaryLangID
        {
            get
            {
                return this.primaryLangIDField;
            }
            set
            {
                this.primaryLangIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "language")]
        public string AltLangID
        {
            get
            {
                return this.altLangIDField;
            }
            set
            {
                this.altLangIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool MoreIndicator
        {
            get
            {
                return this.moreIndicatorField;
            }
            set
            {
                this.moreIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MoreIndicatorSpecified
        {
            get
            {
                return this.moreIndicatorFieldSpecified;
            }
            set
            {
                this.moreIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MoreDataEchoToken
        {
            get
            {
                return this.moreDataEchoTokenField;
            }
            set
            {
                this.moreDataEchoTokenField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "positiveInteger")]
        public string MaxResponses
        {
            get
            {
                return this.maxResponsesField;
            }
            set
            {
                this.maxResponsesField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ResRetrieveRSReservationsList
    {

        private object[] itemsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AirReservation", typeof(OTA_ResRetrieveRSReservationsListAirReservation))]
        [System.Xml.Serialization.XmlElementAttribute("CruiseReservation", typeof(CruiseReservationType))]
        [System.Xml.Serialization.XmlElementAttribute("GlobalReservation", typeof(OTA_ResRetrieveRSReservationsListGlobalReservation))]
        [System.Xml.Serialization.XmlElementAttribute("GolfReservation", typeof(OTA_ResRetrieveRSReservationsListGolfReservation))]
        [System.Xml.Serialization.XmlElementAttribute("HotelReservation", typeof(HotelReservationType))]
        [System.Xml.Serialization.XmlElementAttribute("PackageReservation", typeof(OTA_ResRetrieveRSReservationsListPackageReservation))]
        [System.Xml.Serialization.XmlElementAttribute("RailReservation", typeof(RailReservationSummaryType))]
        [System.Xml.Serialization.XmlElementAttribute("VehicleReservation", typeof(OTA_ResRetrieveRSReservationsListVehicleReservation))]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CruiseReservationType
    {

        private SailingCategoryInfoType sailingInfoField;

        private CruiseProfileType[] sailingProfileField;

        private CruiseGuestInfoType reservationInfoField;

        private CruiseReservationTypePaymentDue[] paymentsDueField;

        private ParagraphType[] informationField;

        /// <remarks/>
        public SailingCategoryInfoType SailingInfo
        {
            get
            {
                return this.sailingInfoField;
            }
            set
            {
                this.sailingInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SailingProfile")]
        public CruiseProfileType[] SailingProfile
        {
            get
            {
                return this.sailingProfileField;
            }
            set
            {
                this.sailingProfileField = value;
            }
        }

        /// <remarks/>
        public CruiseGuestInfoType ReservationInfo
        {
            get
            {
                return this.reservationInfoField;
            }
            set
            {
                this.reservationInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("PaymentDue", IsNullable = false)]
        public CruiseReservationTypePaymentDue[] PaymentsDue
        {
            get
            {
                return this.paymentsDueField;
            }
            set
            {
                this.paymentsDueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Information")]
        public ParagraphType[] Information
        {
            get
            {
                return this.informationField;
            }
            set
            {
                this.informationField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class SailingCategoryInfoType : SailingInfoType
    {

        private SailingCategoryInfoTypeSelectedCategory[] selectedCategoryField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SelectedCategory")]
        public SailingCategoryInfoTypeSelectedCategory[] SelectedCategory
        {
            get
            {
                return this.selectedCategoryField;
            }
            set
            {
                this.selectedCategoryField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(SailingType))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(SailingCategoryInfoType))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class SailingInfoType
    {

        private SailingInfoTypeSelectedSailing selectedSailingField;

        private SailingInfoTypeInclusivePackageOption inclusivePackageOptionField;

        private SailingInfoTypeCurrency currencyField;

        /// <remarks/>
        public SailingInfoTypeSelectedSailing SelectedSailing
        {
            get
            {
                return this.selectedSailingField;
            }
            set
            {
                this.selectedSailingField = value;
            }
        }

        /// <remarks/>
        public SailingInfoTypeInclusivePackageOption InclusivePackageOption
        {
            get
            {
                return this.inclusivePackageOptionField;
            }
            set
            {
                this.inclusivePackageOptionField = value;
            }
        }

        /// <remarks/>
        public SailingInfoTypeCurrency Currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class SailingInfoTypeSelectedSailing : SailingBaseType
    {

        private string voyageIDField;

        private string startField;

        private string durationField;

        private string endField;

        private string statusField;

        private string portsOfCallQuantityField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string VoyageID
        {
            get
            {
                return this.voyageIDField;
            }
            set
            {
                this.voyageIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string End
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string PortsOfCallQuantity
        {
            get
            {
                return this.portsOfCallQuantityField;
            }
            set
            {
                this.portsOfCallQuantityField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class SailingBaseType
    {

        private SailingBaseTypeCruiseLine cruiseLineField;

        private SailingBaseTypeRegion regionField;

        private SailingBaseTypeDeparturePort departurePortField;

        private SailingBaseTypeArrivalPort arrivalPortField;

        private string[] listOfSailingDescriptionCodeField;

        /// <remarks/>
        public SailingBaseTypeCruiseLine CruiseLine
        {
            get
            {
                return this.cruiseLineField;
            }
            set
            {
                this.cruiseLineField = value;
            }
        }

        /// <remarks/>
        public SailingBaseTypeRegion Region
        {
            get
            {
                return this.regionField;
            }
            set
            {
                this.regionField = value;
            }
        }

        /// <remarks/>
        public SailingBaseTypeDeparturePort DeparturePort
        {
            get
            {
                return this.departurePortField;
            }
            set
            {
                this.departurePortField = value;
            }
        }

        /// <remarks/>
        public SailingBaseTypeArrivalPort ArrivalPort
        {
            get
            {
                return this.arrivalPortField;
            }
            set
            {
                this.arrivalPortField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] ListOfSailingDescriptionCode
        {
            get
            {
                return this.listOfSailingDescriptionCodeField;
            }
            set
            {
                this.listOfSailingDescriptionCodeField = value;
            }
        }
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class SailingBaseTypeCruiseLine
    {

        private string vendorCodeField;

        private string vendorNameField;

        private string shipCodeField;

        private string shipNameField;

        private string vendorCodeContextField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string VendorCode
        {
            get
            {
                return this.vendorCodeField;
            }
            set
            {
                this.vendorCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string VendorName
        {
            get
            {
                return this.vendorNameField;
            }
            set
            {
                this.vendorNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ShipCode
        {
            get
            {
                return this.shipCodeField;
            }
            set
            {
                this.shipCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ShipName
        {
            get
            {
                return this.shipNameField;
            }
            set
            {
                this.shipNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string VendorCodeContext
        {
            get
            {
                return this.vendorCodeContextField;
            }
            set
            {
                this.vendorCodeContextField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class SailingBaseTypeRegion
    {

        private string regionCodeField;

        private string regionNameField;

        private string subRegionCodeField;

        private string subRegionNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RegionCode
        {
            get
            {
                return this.regionCodeField;
            }
            set
            {
                this.regionCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RegionName
        {
            get
            {
                return this.regionNameField;
            }
            set
            {
                this.regionNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SubRegionCode
        {
            get
            {
                return this.subRegionCodeField;
            }
            set
            {
                this.subRegionCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SubRegionName
        {
            get
            {
                return this.subRegionNameField;
            }
            set
            {
                this.subRegionNameField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class SailingBaseTypeDeparturePort : LocationType
    {

        private System.DateTime embarkationTimeField;

        private bool embarkationTimeFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime EmbarkationTime
        {
            get
            {
                return this.embarkationTimeField;
            }
            set
            {
                this.embarkationTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EmbarkationTimeSpecified
        {
            get
            {
                return this.embarkationTimeFieldSpecified;
            }
            set
            {
                this.embarkationTimeFieldSpecified = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class SailingInfoTypeInclusivePackageOption
    {

        private string cruisePackageCodeField;

        private bool inclusiveIndicatorField;

        private bool inclusiveIndicatorFieldSpecified;

        private string startField;

        private string durationField;

        private string endField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CruisePackageCode
        {
            get
            {
                return this.cruisePackageCodeField;
            }
            set
            {
                this.cruisePackageCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool InclusiveIndicator
        {
            get
            {
                return this.inclusiveIndicatorField;
            }
            set
            {
                this.inclusiveIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool InclusiveIndicatorSpecified
        {
            get
            {
                return this.inclusiveIndicatorFieldSpecified;
            }
            set
            {
                this.inclusiveIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string End
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class SailingInfoTypeCurrency
    {

        private string currencyCodeField;

        private string decimalPlacesField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CurrencyCode
        {
            get
            {
                return this.currencyCodeField;
            }
            set
            {
                this.currencyCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string DecimalPlaces
        {
            get
            {
                return this.decimalPlacesField;
            }
            set
            {
                this.decimalPlacesField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class SailingCategoryInfoTypeSelectedCategory
    {

        private SailingCategoryInfoTypeSelectedCategoryCabinAttribute[] cabinAttributesField;

        private SailingCategoryInfoTypeSelectedCategorySelectedCabin[] selectedCabinField;

        private string berthedCategoryCodeField;

        private string pricedCategoryCodeField;

        private string deckNumberField;

        private string deckNameField;

        private bool waitlistIndicatorField;

        private bool waitlistIndicatorFieldSpecified;

        private string fareCodeField;

        private string groupCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("CabinAttribute", IsNullable = false)]
        public SailingCategoryInfoTypeSelectedCategoryCabinAttribute[] CabinAttributes
        {
            get
            {
                return this.cabinAttributesField;
            }
            set
            {
                this.cabinAttributesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SelectedCabin")]
        public SailingCategoryInfoTypeSelectedCategorySelectedCabin[] SelectedCabin
        {
            get
            {
                return this.selectedCabinField;
            }
            set
            {
                this.selectedCabinField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string BerthedCategoryCode
        {
            get
            {
                return this.berthedCategoryCodeField;
            }
            set
            {
                this.berthedCategoryCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PricedCategoryCode
        {
            get
            {
                return this.pricedCategoryCodeField;
            }
            set
            {
                this.pricedCategoryCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DeckNumber
        {
            get
            {
                return this.deckNumberField;
            }
            set
            {
                this.deckNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DeckName
        {
            get
            {
                return this.deckNameField;
            }
            set
            {
                this.deckNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool WaitlistIndicator
        {
            get
            {
                return this.waitlistIndicatorField;
            }
            set
            {
                this.waitlistIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool WaitlistIndicatorSpecified
        {
            get
            {
                return this.waitlistIndicatorFieldSpecified;
            }
            set
            {
                this.waitlistIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string FareCode
        {
            get
            {
                return this.fareCodeField;
            }
            set
            {
                this.fareCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string GroupCode
        {
            get
            {
                return this.groupCodeField;
            }
            set
            {
                this.groupCodeField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class SailingCategoryInfoTypeSelectedCategoryCabinAttribute
    {

        private string cabinAttributeCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CabinAttributeCode
        {
            get
            {
                return this.cabinAttributeCodeField;
            }
            set
            {
                this.cabinAttributeCodeField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class SailingCategoryInfoTypeSelectedCategorySelectedCabin : CabinOptionType
    {

        private SailingCategoryInfoTypeSelectedCategorySelectedCabinCabinAttribute[] cabinAttributesField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("CabinAttribute", IsNullable = false)]
        public SailingCategoryInfoTypeSelectedCategorySelectedCabinCabinAttribute[] CabinAttributes
        {
            get
            {
                return this.cabinAttributesField;
            }
            set
            {
                this.cabinAttributesField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class SailingCategoryInfoTypeSelectedCategorySelectedCabinCabinAttribute
    {

        private string cabinAttributeCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CabinAttributeCode
        {
            get
            {
                return this.cabinAttributeCodeField;
            }
            set
            {
                this.cabinAttributeCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CabinOptionType
    {

        private CabinOptionTypeCabinConfiguration[] cabinConfigurationField;

        private CabinOptionTypeMeasurementInfo[] measurementInfoField;

        private FreeTextType remarkField;

        private string statusField;

        private CategoryLocationType categoryLocationField;

        private bool categoryLocationFieldSpecified;

        private CabinOptionTypeShipSide shipSideField;

        private bool shipSideFieldSpecified;

        private CabinOptionTypePositionInShip positionInShipField;

        private bool positionInShipFieldSpecified;

        private string bedTypeField;

        private string cabinNumberField;

        private string maxOccupancyField;

        private bool declineIndicatorField;

        private bool declineIndicatorFieldSpecified;

        private bool heldIndicatorField;

        private bool heldIndicatorFieldSpecified;

        private System.DateTime releaseDateTimeField;

        private bool releaseDateTimeFieldSpecified;

        private string deckNumberField;

        private string deckNameField;

        private string cabinCategoryStatusCodeField;

        private string cabinCategoryCodeField;

        private string cabinRankingField;

        private bool connectingCabinIndicatorField;

        private bool connectingCabinIndicatorFieldSpecified;

        private string connectingCabinNumberField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CabinConfiguration")]
        public CabinOptionTypeCabinConfiguration[] CabinConfiguration
        {
            get
            {
                return this.cabinConfigurationField;
            }
            set
            {
                this.cabinConfigurationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("MeasurementInfo")]
        public CabinOptionTypeMeasurementInfo[] MeasurementInfo
        {
            get
            {
                return this.measurementInfoField;
            }
            set
            {
                this.measurementInfoField = value;
            }
        }

        /// <remarks/>
        public FreeTextType Remark
        {
            get
            {
                return this.remarkField;
            }
            set
            {
                this.remarkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public CategoryLocationType CategoryLocation
        {
            get
            {
                return this.categoryLocationField;
            }
            set
            {
                this.categoryLocationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CategoryLocationSpecified
        {
            get
            {
                return this.categoryLocationFieldSpecified;
            }
            set
            {
                this.categoryLocationFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public CabinOptionTypeShipSide ShipSide
        {
            get
            {
                return this.shipSideField;
            }
            set
            {
                this.shipSideField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ShipSideSpecified
        {
            get
            {
                return this.shipSideFieldSpecified;
            }
            set
            {
                this.shipSideFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public CabinOptionTypePositionInShip PositionInShip
        {
            get
            {
                return this.positionInShipField;
            }
            set
            {
                this.positionInShipField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PositionInShipSpecified
        {
            get
            {
                return this.positionInShipFieldSpecified;
            }
            set
            {
                this.positionInShipFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string BedType
        {
            get
            {
                return this.bedTypeField;
            }
            set
            {
                this.bedTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CabinNumber
        {
            get
            {
                return this.cabinNumberField;
            }
            set
            {
                this.cabinNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string MaxOccupancy
        {
            get
            {
                return this.maxOccupancyField;
            }
            set
            {
                this.maxOccupancyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool DeclineIndicator
        {
            get
            {
                return this.declineIndicatorField;
            }
            set
            {
                this.declineIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DeclineIndicatorSpecified
        {
            get
            {
                return this.declineIndicatorFieldSpecified;
            }
            set
            {
                this.declineIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool HeldIndicator
        {
            get
            {
                return this.heldIndicatorField;
            }
            set
            {
                this.heldIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool HeldIndicatorSpecified
        {
            get
            {
                return this.heldIndicatorFieldSpecified;
            }
            set
            {
                this.heldIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime ReleaseDateTime
        {
            get
            {
                return this.releaseDateTimeField;
            }
            set
            {
                this.releaseDateTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ReleaseDateTimeSpecified
        {
            get
            {
                return this.releaseDateTimeFieldSpecified;
            }
            set
            {
                this.releaseDateTimeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DeckNumber
        {
            get
            {
                return this.deckNumberField;
            }
            set
            {
                this.deckNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DeckName
        {
            get
            {
                return this.deckNameField;
            }
            set
            {
                this.deckNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CabinCategoryStatusCode
        {
            get
            {
                return this.cabinCategoryStatusCodeField;
            }
            set
            {
                this.cabinCategoryStatusCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CabinCategoryCode
        {
            get
            {
                return this.cabinCategoryCodeField;
            }
            set
            {
                this.cabinCategoryCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string CabinRanking
        {
            get
            {
                return this.cabinRankingField;
            }
            set
            {
                this.cabinRankingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool ConnectingCabinIndicator
        {
            get
            {
                return this.connectingCabinIndicatorField;
            }
            set
            {
                this.connectingCabinIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ConnectingCabinIndicatorSpecified
        {
            get
            {
                return this.connectingCabinIndicatorFieldSpecified;
            }
            set
            {
                this.connectingCabinIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ConnectingCabinNumber
        {
            get
            {
                return this.connectingCabinNumberField;
            }
            set
            {
                this.connectingCabinNumberField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CabinOptionTypeCabinConfiguration
    {

        private string bedConfigurationCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string BedConfigurationCode
        {
            get
            {
                return this.bedConfigurationCodeField;
            }
            set
            {
                this.bedConfigurationCodeField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CabinOptionTypeMeasurementInfo
    {

        private decimal unitOfMeasureQuantityField;

        private bool unitOfMeasureQuantityFieldSpecified;

        private string unitOfMeasureField;

        private string unitOfMeasureCodeField;

        private string codeField;

        private string nameField;

        private string dimensionInfoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal UnitOfMeasureQuantity
        {
            get
            {
                return this.unitOfMeasureQuantityField;
            }
            set
            {
                this.unitOfMeasureQuantityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool UnitOfMeasureQuantitySpecified
        {
            get
            {
                return this.unitOfMeasureQuantityFieldSpecified;
            }
            set
            {
                this.unitOfMeasureQuantityFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string UnitOfMeasure
        {
            get
            {
                return this.unitOfMeasureField;
            }
            set
            {
                this.unitOfMeasureField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string UnitOfMeasureCode
        {
            get
            {
                return this.unitOfMeasureCodeField;
            }
            set
            {
                this.unitOfMeasureCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DimensionInfo
        {
            get
            {
                return this.dimensionInfoField;
            }
            set
            {
                this.dimensionInfoField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class SailingBaseTypeArrivalPort : LocationType
    {

        private System.DateTime debarkationDateTimeField;

        private bool debarkationDateTimeFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime DebarkationDateTime
        {
            get
            {
                return this.debarkationDateTimeField;
            }
            set
            {
                this.debarkationDateTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DebarkationDateTimeSpecified
        {
            get
            {
                return this.debarkationDateTimeFieldSpecified;
            }
            set
            {
                this.debarkationDateTimeFieldSpecified = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CruiseProfileType
    {

        private CruiseProfileTypeCruiseProfile[] cruiseProfileField;

        private CruiseProfileTypeProfileTypeIdentifier profileTypeIdentifierField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CruiseProfile")]
        public CruiseProfileTypeCruiseProfile[] CruiseProfile
        {
            get
            {
                return this.cruiseProfileField;
            }
            set
            {
                this.cruiseProfileField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public CruiseProfileTypeProfileTypeIdentifier ProfileTypeIdentifier
        {
            get
            {
                return this.profileTypeIdentifierField;
            }
            set
            {
                this.profileTypeIdentifierField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CruiseProfileTypeCruiseProfile
    {

        private string codeField;

        private string maxQuantityField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MaxQuantity
        {
            get
            {
                return this.maxQuantityField;
            }
            set
            {
                this.maxQuantityField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CruiseGuestInfoType
    {

        private ReservationID_Type[] reservationIDField;

        private CruiseGuestDetailType[] guestDetailsField;

        private CruiseGuestInfoTypeLinkedBooking[] linkedBookingsField;

        private CruiseGuestInfoTypePaymentOption[] paymentOptionsField;

        private CruiseGuestInfoTypeCancellationPenalty cancellationPenaltyField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ReservationID")]
        public ReservationID_Type[] ReservationID
        {
            get
            {
                return this.reservationIDField;
            }
            set
            {
                this.reservationIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("GuestDetail", IsNullable = false)]
        public CruiseGuestDetailType[] GuestDetails
        {
            get
            {
                return this.guestDetailsField;
            }
            set
            {
                this.guestDetailsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("LinkedBooking", IsNullable = false)]
        public CruiseGuestInfoTypeLinkedBooking[] LinkedBookings
        {
            get
            {
                return this.linkedBookingsField;
            }
            set
            {
                this.linkedBookingsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("PaymentOption", IsNullable = false)]
        public CruiseGuestInfoTypePaymentOption[] PaymentOptions
        {
            get
            {
                return this.paymentOptionsField;
            }
            set
            {
                this.paymentOptionsField = value;
            }
        }

        /// <remarks/>
        public CruiseGuestInfoTypeCancellationPenalty CancellationPenalty
        {
            get
            {
                return this.cancellationPenaltyField;
            }
            set
            {
                this.cancellationPenaltyField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class ReservationID_Type : UniqueID_Type
    {

        private string statusCodeField;

        private System.DateTime lastModifyDateTimeField;

        private bool lastModifyDateTimeFieldSpecified;

        private string bookedDateField;

        private string offerDateField;

        private System.DateTime syncDateTimeField;

        private bool syncDateTimeFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string StatusCode
        {
            get
            {
                return this.statusCodeField;
            }
            set
            {
                this.statusCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime LastModifyDateTime
        {
            get
            {
                return this.lastModifyDateTimeField;
            }
            set
            {
                this.lastModifyDateTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool LastModifyDateTimeSpecified
        {
            get
            {
                return this.lastModifyDateTimeFieldSpecified;
            }
            set
            {
                this.lastModifyDateTimeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string BookedDate
        {
            get
            {
                return this.bookedDateField;
            }
            set
            {
                this.bookedDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string OfferDate
        {
            get
            {
                return this.offerDateField;
            }
            set
            {
                this.offerDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime SyncDateTime
        {
            get
            {
                return this.syncDateTimeField;
            }
            set
            {
                this.syncDateTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SyncDateTimeSpecified
        {
            get
            {
                return this.syncDateTimeFieldSpecified;
            }
            set
            {
                this.syncDateTimeFieldSpecified = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CruiseGuestDetailType
    {

        private CruiseGuestDetailTypeSelectedFareCode[] selectedFareCodeField;

        private CruiseGuestDetailTypeContactInfo[] contactInfoField;

        private GuestTransportationType[] guestTransportationField;

        private CruiseGuestDetailTypeLoyaltyInfo[] loyaltyInfoField;

        private CruiseGuestDetailTypeLinkedTraveler[] linkedTravelerField;

        private DocumentType[] travelDocumentField;

        private CruiseGuestDetailTypeSelectedDining[] selectedDiningField;

        private CruiseGuestDetailTypeSelectedInsurance[] selectedInsuranceField;

        private AmenityOptionType[] selectedOptionsField;

        private CruiseGuestDetailTypeSelectedPackage[] selectedPackagesField;

        private SpecialServiceType[] selectedSpecialServicesField;

        private CruiseGuestDetailTypeAirAccommodation[] airAccommodationsField;

        private DocumentHandlingType[] cruiseDocumentField;

        private CruiseProfileType[] profilesField;

        private bool guestExistsIndicatorField;

        private bool guestExistsIndicatorFieldSpecified;

        private bool repeatGuestIndicatorField;

        private bool repeatGuestIndicatorFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SelectedFareCode")]
        public CruiseGuestDetailTypeSelectedFareCode[] SelectedFareCode
        {
            get
            {
                return this.selectedFareCodeField;
            }
            set
            {
                this.selectedFareCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ContactInfo")]
        public CruiseGuestDetailTypeContactInfo[] ContactInfo
        {
            get
            {
                return this.contactInfoField;
            }
            set
            {
                this.contactInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("GuestTransportation")]
        public GuestTransportationType[] GuestTransportation
        {
            get
            {
                return this.guestTransportationField;
            }
            set
            {
                this.guestTransportationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("LoyaltyInfo")]
        public CruiseGuestDetailTypeLoyaltyInfo[] LoyaltyInfo
        {
            get
            {
                return this.loyaltyInfoField;
            }
            set
            {
                this.loyaltyInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("LinkedTraveler")]
        public CruiseGuestDetailTypeLinkedTraveler[] LinkedTraveler
        {
            get
            {
                return this.linkedTravelerField;
            }
            set
            {
                this.linkedTravelerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("TravelDocument")]
        public DocumentType[] TravelDocument
        {
            get
            {
                return this.travelDocumentField;
            }
            set
            {
                this.travelDocumentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SelectedDining")]
        public CruiseGuestDetailTypeSelectedDining[] SelectedDining
        {
            get
            {
                return this.selectedDiningField;
            }
            set
            {
                this.selectedDiningField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SelectedInsurance")]
        public CruiseGuestDetailTypeSelectedInsurance[] SelectedInsurance
        {
            get
            {
                return this.selectedInsuranceField;
            }
            set
            {
                this.selectedInsuranceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SelectedOptions")]
        public AmenityOptionType[] SelectedOptions
        {
            get
            {
                return this.selectedOptionsField;
            }
            set
            {
                this.selectedOptionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("SelectedPackage", IsNullable = false)]
        public CruiseGuestDetailTypeSelectedPackage[] SelectedPackages
        {
            get
            {
                return this.selectedPackagesField;
            }
            set
            {
                this.selectedPackagesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("SelectedSpecialService", IsNullable = false)]
        public SpecialServiceType[] SelectedSpecialServices
        {
            get
            {
                return this.selectedSpecialServicesField;
            }
            set
            {
                this.selectedSpecialServicesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("AirAccommodation", IsNullable = false)]
        public CruiseGuestDetailTypeAirAccommodation[] AirAccommodations
        {
            get
            {
                return this.airAccommodationsField;
            }
            set
            {
                this.airAccommodationsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CruiseDocument")]
        public DocumentHandlingType[] CruiseDocument
        {
            get
            {
                return this.cruiseDocumentField;
            }
            set
            {
                this.cruiseDocumentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Profiles")]
        public CruiseProfileType[] Profiles
        {
            get
            {
                return this.profilesField;
            }
            set
            {
                this.profilesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool GuestExistsIndicator
        {
            get
            {
                return this.guestExistsIndicatorField;
            }
            set
            {
                this.guestExistsIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool GuestExistsIndicatorSpecified
        {
            get
            {
                return this.guestExistsIndicatorFieldSpecified;
            }
            set
            {
                this.guestExistsIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool RepeatGuestIndicator
        {
            get
            {
                return this.repeatGuestIndicatorField;
            }
            set
            {
                this.repeatGuestIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RepeatGuestIndicatorSpecified
        {
            get
            {
                return this.repeatGuestIndicatorFieldSpecified;
            }
            set
            {
                this.repeatGuestIndicatorFieldSpecified = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CruiseGuestDetailTypeAirAccommodation : AirInfoType
    {

        private string commentField;

        private string airAccommodationTypeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AirAccommodationType
        {
            get
            {
                return this.airAccommodationTypeField;
            }
            set
            {
                this.airAccommodationTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class AirInfoType
    {

        private LocationType departureCityField;

        private LocationType arrivalCityField;

        private OperatingAirlineType airlineField;

        private string departureDateTimeField;

        private string arrivalDateTimeField;

        private string airlineCabinClassField;

        /// <remarks/>
        public LocationType DepartureCity
        {
            get
            {
                return this.departureCityField;
            }
            set
            {
                this.departureCityField = value;
            }
        }

        /// <remarks/>
        public LocationType ArrivalCity
        {
            get
            {
                return this.arrivalCityField;
            }
            set
            {
                this.arrivalCityField = value;
            }
        }

        /// <remarks/>
        public OperatingAirlineType Airline
        {
            get
            {
                return this.airlineField;
            }
            set
            {
                this.airlineField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DepartureDateTime
        {
            get
            {
                return this.departureDateTimeField;
            }
            set
            {
                this.departureDateTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ArrivalDateTime
        {
            get
            {
                return this.arrivalDateTimeField;
            }
            set
            {
                this.arrivalDateTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AirlineCabinClass
        {
            get
            {
                return this.airlineCabinClassField;
            }
            set
            {
                this.airlineCabinClassField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class DocumentHandlingType
    {

        private DocumentHandlingTypeVendorOption[] vendorOptionField;

        private string documentTypeCodeField;

        private string deliveryMethodCodeField;

        private string documentDestinationField;

        private bool selectedOptionIndicatorField;

        private bool selectedOptionIndicatorFieldSpecified;

        private bool defaultIndicatorField;

        private bool defaultIndicatorFieldSpecified;

        private bool addressRequiredIndicatorField;

        private bool addressRequiredIndicatorFieldSpecified;

        private string addressRPHField;

        private string emailRPHField;

        private string telephoneRPHField;

        private string documentLanguageField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("VendorOption")]
        public DocumentHandlingTypeVendorOption[] VendorOption
        {
            get
            {
                return this.vendorOptionField;
            }
            set
            {
                this.vendorOptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DocumentTypeCode
        {
            get
            {
                return this.documentTypeCodeField;
            }
            set
            {
                this.documentTypeCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DeliveryMethodCode
        {
            get
            {
                return this.deliveryMethodCodeField;
            }
            set
            {
                this.deliveryMethodCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DocumentDestination
        {
            get
            {
                return this.documentDestinationField;
            }
            set
            {
                this.documentDestinationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool SelectedOptionIndicator
        {
            get
            {
                return this.selectedOptionIndicatorField;
            }
            set
            {
                this.selectedOptionIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SelectedOptionIndicatorSpecified
        {
            get
            {
                return this.selectedOptionIndicatorFieldSpecified;
            }
            set
            {
                this.selectedOptionIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool DefaultIndicator
        {
            get
            {
                return this.defaultIndicatorField;
            }
            set
            {
                this.defaultIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DefaultIndicatorSpecified
        {
            get
            {
                return this.defaultIndicatorFieldSpecified;
            }
            set
            {
                this.defaultIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool AddressRequiredIndicator
        {
            get
            {
                return this.addressRequiredIndicatorField;
            }
            set
            {
                this.addressRequiredIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AddressRequiredIndicatorSpecified
        {
            get
            {
                return this.addressRequiredIndicatorFieldSpecified;
            }
            set
            {
                this.addressRequiredIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AddressRPH
        {
            get
            {
                return this.addressRPHField;
            }
            set
            {
                this.addressRPHField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string EmailRPH
        {
            get
            {
                return this.emailRPHField;
            }
            set
            {
                this.emailRPHField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TelephoneRPH
        {
            get
            {
                return this.telephoneRPHField;
            }
            set
            {
                this.telephoneRPHField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "language")]
        public string DocumentLanguage
        {
            get
            {
                return this.documentLanguageField;
            }
            set
            {
                this.documentLanguageField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class DocumentHandlingTypeVendorOption
    {

        private string vendorNameField;

        private string currencyCodeField;

        private string decimalPlacesField;

        private decimal amountField;

        private bool amountFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string VendorName
        {
            get
            {
                return this.vendorNameField;
            }
            set
            {
                this.vendorNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CurrencyCode
        {
            get
            {
                return this.currencyCodeField;
            }
            set
            {
                this.currencyCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string DecimalPlaces
        {
            get
            {
                return this.decimalPlacesField;
            }
            set
            {
                this.decimalPlacesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AmountSpecified
        {
            get
            {
                return this.amountFieldSpecified;
            }
            set
            {
                this.amountFieldSpecified = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class SpecialServiceType
    {

        private ParagraphType commentField;

        private string typeField;

        private string codeField;

        private string codeNameField;

        private string codeDetailField;

        private SpecialServiceTypeAssociationType associationTypeField;

        private bool associationTypeFieldSpecified;

        private string dateField;

        private string nbrOfYearsField;

        /// <remarks/>
        public ParagraphType Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CodeName
        {
            get
            {
                return this.codeNameField;
            }
            set
            {
                this.codeNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CodeDetail
        {
            get
            {
                return this.codeDetailField;
            }
            set
            {
                this.codeDetailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public SpecialServiceTypeAssociationType AssociationType
        {
            get
            {
                return this.associationTypeField;
            }
            set
            {
                this.associationTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AssociationTypeSpecified
        {
            get
            {
                return this.associationTypeFieldSpecified;
            }
            set
            {
                this.associationTypeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "positiveInteger")]
        public string NbrOfYears
        {
            get
            {
                return this.nbrOfYearsField;
            }
            set
            {
                this.nbrOfYearsField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CruiseGuestDetailTypeSelectedPackage : CruisePackageType
    {

        private AirInfoType airInfoField;

        /// <remarks/>
        public AirInfoType AirInfo
        {
            get
            {
                return this.airInfoField;
            }
            set
            {
                this.airInfoField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CruisePackageType
    {

        private CruisePackageTypeLocation[] locationField;

        private string packageTypeCodeField;

        private string cruisePackageCodeField;

        private bool inclusiveIndicatorField;

        private bool inclusiveIndicatorFieldSpecified;

        private string startField;

        private string durationField;

        private string endField;

        private string statusField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Location")]
        public CruisePackageTypeLocation[] Location
        {
            get
            {
                return this.locationField;
            }
            set
            {
                this.locationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PackageTypeCode
        {
            get
            {
                return this.packageTypeCodeField;
            }
            set
            {
                this.packageTypeCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CruisePackageCode
        {
            get
            {
                return this.cruisePackageCodeField;
            }
            set
            {
                this.cruisePackageCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool InclusiveIndicator
        {
            get
            {
                return this.inclusiveIndicatorField;
            }
            set
            {
                this.inclusiveIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool InclusiveIndicatorSpecified
        {
            get
            {
                return this.inclusiveIndicatorFieldSpecified;
            }
            set
            {
                this.inclusiveIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string End
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CruisePackageTypeLocation : LocationGeneralType
    {

        private ParagraphType informationField;

        private string locationCodeField;

        private string codeContextField;

        private string locationNameField;

        private string startField;

        private string durationField;

        private string endField;

        /// <remarks/>
        public ParagraphType Information
        {
            get
            {
                return this.informationField;
            }
            set
            {
                this.informationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LocationCode
        {
            get
            {
                return this.locationCodeField;
            }
            set
            {
                this.locationCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CodeContext
        {
            get
            {
                return this.codeContextField;
            }
            set
            {
                this.codeContextField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LocationName
        {
            get
            {
                return this.locationNameField;
            }
            set
            {
                this.locationNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string End
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class AmenityOptionType
    {

        private PersonNameType originatorField;

        private ParagraphType messageField;

        private string optionCodeField;

        private string quantityField;

        private string deliveryDateField;

        private AmenityOptionTypeDeliveryLocation deliveryLocationField;

        private bool deliveryLocationFieldSpecified;

        private bool modifiableIndicatorField;

        private bool modifiableIndicatorFieldSpecified;

        /// <remarks/>
        public PersonNameType Originator
        {
            get
            {
                return this.originatorField;
            }
            set
            {
                this.originatorField = value;
            }
        }

        /// <remarks/>
        public ParagraphType Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string OptionCode
        {
            get
            {
                return this.optionCodeField;
            }
            set
            {
                this.optionCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string Quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DeliveryDate
        {
            get
            {
                return this.deliveryDateField;
            }
            set
            {
                this.deliveryDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public AmenityOptionTypeDeliveryLocation DeliveryLocation
        {
            get
            {
                return this.deliveryLocationField;
            }
            set
            {
                this.deliveryLocationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DeliveryLocationSpecified
        {
            get
            {
                return this.deliveryLocationFieldSpecified;
            }
            set
            {
                this.deliveryLocationFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool ModifiableIndicator
        {
            get
            {
                return this.modifiableIndicatorField;
            }
            set
            {
                this.modifiableIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ModifiableIndicatorSpecified
        {
            get
            {
                return this.modifiableIndicatorFieldSpecified;
            }
            set
            {
                this.modifiableIndicatorFieldSpecified = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CruiseGuestDetailTypeSelectedInsurance
    {

        private string insuranceCodeField;

        private bool selectedOptionIndicatorField;

        private bool selectedOptionIndicatorFieldSpecified;

        private bool defaultIndicatorField;

        private bool defaultIndicatorFieldSpecified;

        private string statusField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string InsuranceCode
        {
            get
            {
                return this.insuranceCodeField;
            }
            set
            {
                this.insuranceCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool SelectedOptionIndicator
        {
            get
            {
                return this.selectedOptionIndicatorField;
            }
            set
            {
                this.selectedOptionIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SelectedOptionIndicatorSpecified
        {
            get
            {
                return this.selectedOptionIndicatorFieldSpecified;
            }
            set
            {
                this.selectedOptionIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool DefaultIndicator
        {
            get
            {
                return this.defaultIndicatorField;
            }
            set
            {
                this.defaultIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DefaultIndicatorSpecified
        {
            get
            {
                return this.defaultIndicatorFieldSpecified;
            }
            set
            {
                this.defaultIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CruiseGuestDetailTypeSelectedDining
    {

        private string smokingCodeField;

        private string diningRoomField;

        private string tableSizeField;

        private string ageCodeField;

        private string languageField;

        private string sittingField;

        private string statusField;

        private PreferLevelType preferenceField;

        private bool preferenceFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SmokingCode
        {
            get
            {
                return this.smokingCodeField;
            }
            set
            {
                this.smokingCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DiningRoom
        {
            get
            {
                return this.diningRoomField;
            }
            set
            {
                this.diningRoomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TableSize
        {
            get
            {
                return this.tableSizeField;
            }
            set
            {
                this.tableSizeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AgeCode
        {
            get
            {
                return this.ageCodeField;
            }
            set
            {
                this.ageCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "language")]
        public string Language
        {
            get
            {
                return this.languageField;
            }
            set
            {
                this.languageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Sitting
        {
            get
            {
                return this.sittingField;
            }
            set
            {
                this.sittingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PreferLevelType Preference
        {
            get
            {
                return this.preferenceField;
            }
            set
            {
                this.preferenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PreferenceSpecified
        {
            get
            {
                return this.preferenceFieldSpecified;
            }
            set
            {
                this.preferenceFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CruiseGuestDetailTypeLinkedTraveler : RelatedTravelerType
    {

        private string linkTypeCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LinkTypeCode
        {
            get
            {
                return this.linkTypeCodeField;
            }
            set
            {
                this.linkTypeCodeField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CruiseGuestDetailTypeContactInfo : ContactPersonType
    {

        private string guestRefNumberField;

        private string ageField;

        private string nationalityField;

        private string guestOccupationField;

        private System.DateTime personBirthDateField;

        private bool personBirthDateFieldSpecified;

        private GuestTypeGender genderField;

        private bool genderFieldSpecified;

        private string loyaltyMembershipIDField;

        private string loyalLevelField;

        private string loyalLevelCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string GuestRefNumber
        {
            get
            {
                return this.guestRefNumberField;
            }
            set
            {
                this.guestRefNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string Age
        {
            get
            {
                return this.ageField;
            }
            set
            {
                this.ageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Nationality
        {
            get
            {
                return this.nationalityField;
            }
            set
            {
                this.nationalityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string GuestOccupation
        {
            get
            {
                return this.guestOccupationField;
            }
            set
            {
                this.guestOccupationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime PersonBirthDate
        {
            get
            {
                return this.personBirthDateField;
            }
            set
            {
                this.personBirthDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PersonBirthDateSpecified
        {
            get
            {
                return this.personBirthDateFieldSpecified;
            }
            set
            {
                this.personBirthDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public GuestTypeGender Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool GenderSpecified
        {
            get
            {
                return this.genderFieldSpecified;
            }
            set
            {
                this.genderFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LoyaltyMembershipID
        {
            get
            {
                return this.loyaltyMembershipIDField;
            }
            set
            {
                this.loyaltyMembershipIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LoyalLevel
        {
            get
            {
                return this.loyalLevelField;
            }
            set
            {
                this.loyalLevelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string LoyalLevelCode
        {
            get
            {
                return this.loyalLevelCodeField;
            }
            set
            {
                this.loyalLevelCodeField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CruiseGuestDetailTypeLoyaltyInfo
    {

        private PaymentCardTypeCustLoyaltyShareSynchInd shareSynchIndField;

        private bool shareSynchIndFieldSpecified;

        private PaymentCardTypeCustLoyaltyShareMarketInd shareMarketIndField;

        private bool shareMarketIndFieldSpecified;

        private string programIDField;

        private string membershipIDField;

        private string travelSectorField;

        private string[] vendorCodeField;

        private bool primaryLoyaltyIndicatorField;

        private bool primaryLoyaltyIndicatorFieldSpecified;

        private string allianceLoyaltyLevelNameField;

        private string customerTypeField;

        private string customerValueField;

        private string passwordField;

        private string loyalLevelField;

        private string loyalLevelCodeField;

        private PaymentCardTypeCustLoyaltySingleVendorInd singleVendorIndField;

        private bool singleVendorIndFieldSpecified;

        private System.DateTime signupDateField;

        private bool signupDateFieldSpecified;

        private System.DateTime effectiveDateField;

        private bool effectiveDateFieldSpecified;

        private System.DateTime expireDateField;

        private bool expireDateFieldSpecified;

        private bool expireDateExclusiveIndicatorField;

        private bool expireDateExclusiveIndicatorFieldSpecified;

        private string rPHField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PaymentCardTypeCustLoyaltyShareSynchInd ShareSynchInd
        {
            get
            {
                return this.shareSynchIndField;
            }
            set
            {
                this.shareSynchIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ShareSynchIndSpecified
        {
            get
            {
                return this.shareSynchIndFieldSpecified;
            }
            set
            {
                this.shareSynchIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PaymentCardTypeCustLoyaltyShareMarketInd ShareMarketInd
        {
            get
            {
                return this.shareMarketIndField;
            }
            set
            {
                this.shareMarketIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ShareMarketIndSpecified
        {
            get
            {
                return this.shareMarketIndFieldSpecified;
            }
            set
            {
                this.shareMarketIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ProgramID
        {
            get
            {
                return this.programIDField;
            }
            set
            {
                this.programIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MembershipID
        {
            get
            {
                return this.membershipIDField;
            }
            set
            {
                this.membershipIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TravelSector
        {
            get
            {
                return this.travelSectorField;
            }
            set
            {
                this.travelSectorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] VendorCode
        {
            get
            {
                return this.vendorCodeField;
            }
            set
            {
                this.vendorCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool PrimaryLoyaltyIndicator
        {
            get
            {
                return this.primaryLoyaltyIndicatorField;
            }
            set
            {
                this.primaryLoyaltyIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrimaryLoyaltyIndicatorSpecified
        {
            get
            {
                return this.primaryLoyaltyIndicatorFieldSpecified;
            }
            set
            {
                this.primaryLoyaltyIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AllianceLoyaltyLevelName
        {
            get
            {
                return this.allianceLoyaltyLevelNameField;
            }
            set
            {
                this.allianceLoyaltyLevelNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CustomerType
        {
            get
            {
                return this.customerTypeField;
            }
            set
            {
                this.customerTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CustomerValue
        {
            get
            {
                return this.customerValueField;
            }
            set
            {
                this.customerValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LoyalLevel
        {
            get
            {
                return this.loyalLevelField;
            }
            set
            {
                this.loyalLevelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string LoyalLevelCode
        {
            get
            {
                return this.loyalLevelCodeField;
            }
            set
            {
                this.loyalLevelCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PaymentCardTypeCustLoyaltySingleVendorInd SingleVendorInd
        {
            get
            {
                return this.singleVendorIndField;
            }
            set
            {
                this.singleVendorIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SingleVendorIndSpecified
        {
            get
            {
                return this.singleVendorIndFieldSpecified;
            }
            set
            {
                this.singleVendorIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime SignupDate
        {
            get
            {
                return this.signupDateField;
            }
            set
            {
                this.signupDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SignupDateSpecified
        {
            get
            {
                return this.signupDateFieldSpecified;
            }
            set
            {
                this.signupDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime EffectiveDate
        {
            get
            {
                return this.effectiveDateField;
            }
            set
            {
                this.effectiveDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EffectiveDateSpecified
        {
            get
            {
                return this.effectiveDateFieldSpecified;
            }
            set
            {
                this.effectiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime ExpireDate
        {
            get
            {
                return this.expireDateField;
            }
            set
            {
                this.expireDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExpireDateSpecified
        {
            get
            {
                return this.expireDateFieldSpecified;
            }
            set
            {
                this.expireDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool ExpireDateExclusiveIndicator
        {
            get
            {
                return this.expireDateExclusiveIndicatorField;
            }
            set
            {
                this.expireDateExclusiveIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExpireDateExclusiveIndicatorSpecified
        {
            get
            {
                return this.expireDateExclusiveIndicatorFieldSpecified;
            }
            set
            {
                this.expireDateExclusiveIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RPH
        {
            get
            {
                return this.rPHField;
            }
            set
            {
                this.rPHField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CruiseGuestDetailTypeSelectedFareCode
    {

        private string fareCodeField;

        private string groupCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string FareCode
        {
            get
            {
                return this.fareCodeField;
            }
            set
            {
                this.fareCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string GroupCode
        {
            get
            {
                return this.groupCodeField;
            }
            set
            {
                this.groupCodeField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CruiseGuestInfoTypeLinkedBooking : RelatedTravelerType
    {

        private string[] linkTypeCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] LinkTypeCode
        {
            get
            {
                return this.linkTypeCodeField;
            }
            set
            {
                this.linkTypeCodeField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CruiseGuestInfoTypeCancellationPenalty
    {

        private decimal amountField;

        private bool amountFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AmountSpecified
        {
            get
            {
                return this.amountFieldSpecified;
            }
            set
            {
                this.amountFieldSpecified = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CruiseGuestInfoTypePaymentOption : PaymentDetailType
    {

        private bool extendedIndicatorField;

        private bool extendedIndicatorFieldSpecified;

        private CruiseGuestInfoTypePaymentOptionPaymentPurpose paymentPurposeField;

        private bool paymentPurposeFieldSpecified;

        private string extendedDepositDateField;

        private string referenceNumberField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool ExtendedIndicator
        {
            get
            {
                return this.extendedIndicatorField;
            }
            set
            {
                this.extendedIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExtendedIndicatorSpecified
        {
            get
            {
                return this.extendedIndicatorFieldSpecified;
            }
            set
            {
                this.extendedIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public CruiseGuestInfoTypePaymentOptionPaymentPurpose PaymentPurpose
        {
            get
            {
                return this.paymentPurposeField;
            }
            set
            {
                this.paymentPurposeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PaymentPurposeSpecified
        {
            get
            {
                return this.paymentPurposeFieldSpecified;
            }
            set
            {
                this.paymentPurposeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ExtendedDepositDate
        {
            get
            {
                return this.extendedDepositDateField;
            }
            set
            {
                this.extendedDepositDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ReferenceNumber
        {
            get
            {
                return this.referenceNumberField;
            }
            set
            {
                this.referenceNumberField = value;
            }
        }
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class CruiseReservationTypePaymentDue
    {

        private decimal amountField;

        private bool amountFieldSpecified;

        private string paymentNumberField;

        private string dueDateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AmountSpecified
        {
            get
            {
                return this.amountFieldSpecified;
            }
            set
            {
                this.amountFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string PaymentNumber
        {
            get
            {
                return this.paymentNumberField;
            }
            set
            {
                this.paymentNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DueDate
        {
            get
            {
                return this.dueDateField;
            }
            set
            {
                this.dueDateField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ResRetrieveRSReservationsListGlobalReservation
    {

        private UniqueID_Type uniqueIDField;

        private PersonNameType travelerNameField;

        private string itineraryNameField;

        private string startField;

        /// <remarks/>
        public UniqueID_Type UniqueID
        {
            get
            {
                return this.uniqueIDField;
            }
            set
            {
                this.uniqueIDField = value;
            }
        }

        /// <remarks/>
        public PersonNameType TravelerName
        {
            get
            {
                return this.travelerNameField;
            }
            set
            {
                this.travelerNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItineraryName
        {
            get
            {
                return this.itineraryNameField;
            }
            set
            {
                this.itineraryNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ResRetrieveRSReservationsListGolfReservation
    {

        private OTA_ResRetrieveRSReservationsListGolfReservationMembership membershipField;

        private PersonNameType nameField;

        private string idField;

        private string roundIDField;

        private string playDateTimeField;

        private string packageIDField;

        private string requestorResIDField;

        private string responderResConfIDField;

        /// <remarks/>
        public OTA_ResRetrieveRSReservationsListGolfReservationMembership Membership
        {
            get
            {
                return this.membershipField;
            }
            set
            {
                this.membershipField = value;
            }
        }

        /// <remarks/>
        public PersonNameType Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "positiveInteger")]
        public string RoundID
        {
            get
            {
                return this.roundIDField;
            }
            set
            {
                this.roundIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PlayDateTime
        {
            get
            {
                return this.playDateTimeField;
            }
            set
            {
                this.playDateTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PackageID
        {
            get
            {
                return this.packageIDField;
            }
            set
            {
                this.packageIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RequestorResID
        {
            get
            {
                return this.requestorResIDField;
            }
            set
            {
                this.requestorResIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ResponderResConfID
        {
            get
            {
                return this.responderResConfIDField;
            }
            set
            {
                this.responderResConfIDField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ResRetrieveRSReservationsListGolfReservationMembership
    {

        private PaymentCardTypeCustLoyaltyShareSynchInd shareSynchIndField;

        private bool shareSynchIndFieldSpecified;

        private PaymentCardTypeCustLoyaltyShareMarketInd shareMarketIndField;

        private bool shareMarketIndFieldSpecified;

        private string programIDField;

        private string membershipIDField;

        private string travelSectorField;

        private string[] vendorCodeField;

        private bool primaryLoyaltyIndicatorField;

        private bool primaryLoyaltyIndicatorFieldSpecified;

        private string allianceLoyaltyLevelNameField;

        private string customerTypeField;

        private string customerValueField;

        private string passwordField;

        private string loyalLevelField;

        private string loyalLevelCodeField;

        private PaymentCardTypeCustLoyaltySingleVendorInd singleVendorIndField;

        private bool singleVendorIndFieldSpecified;

        private System.DateTime signupDateField;

        private bool signupDateFieldSpecified;

        private System.DateTime effectiveDateField;

        private bool effectiveDateFieldSpecified;

        private System.DateTime expireDateField;

        private bool expireDateFieldSpecified;

        private bool expireDateExclusiveIndicatorField;

        private bool expireDateExclusiveIndicatorFieldSpecified;

        private string rPHField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PaymentCardTypeCustLoyaltyShareSynchInd ShareSynchInd
        {
            get
            {
                return this.shareSynchIndField;
            }
            set
            {
                this.shareSynchIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ShareSynchIndSpecified
        {
            get
            {
                return this.shareSynchIndFieldSpecified;
            }
            set
            {
                this.shareSynchIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PaymentCardTypeCustLoyaltyShareMarketInd ShareMarketInd
        {
            get
            {
                return this.shareMarketIndField;
            }
            set
            {
                this.shareMarketIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ShareMarketIndSpecified
        {
            get
            {
                return this.shareMarketIndFieldSpecified;
            }
            set
            {
                this.shareMarketIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ProgramID
        {
            get
            {
                return this.programIDField;
            }
            set
            {
                this.programIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MembershipID
        {
            get
            {
                return this.membershipIDField;
            }
            set
            {
                this.membershipIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TravelSector
        {
            get
            {
                return this.travelSectorField;
            }
            set
            {
                this.travelSectorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] VendorCode
        {
            get
            {
                return this.vendorCodeField;
            }
            set
            {
                this.vendorCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool PrimaryLoyaltyIndicator
        {
            get
            {
                return this.primaryLoyaltyIndicatorField;
            }
            set
            {
                this.primaryLoyaltyIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrimaryLoyaltyIndicatorSpecified
        {
            get
            {
                return this.primaryLoyaltyIndicatorFieldSpecified;
            }
            set
            {
                this.primaryLoyaltyIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AllianceLoyaltyLevelName
        {
            get
            {
                return this.allianceLoyaltyLevelNameField;
            }
            set
            {
                this.allianceLoyaltyLevelNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CustomerType
        {
            get
            {
                return this.customerTypeField;
            }
            set
            {
                this.customerTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CustomerValue
        {
            get
            {
                return this.customerValueField;
            }
            set
            {
                this.customerValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LoyalLevel
        {
            get
            {
                return this.loyalLevelField;
            }
            set
            {
                this.loyalLevelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string LoyalLevelCode
        {
            get
            {
                return this.loyalLevelCodeField;
            }
            set
            {
                this.loyalLevelCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PaymentCardTypeCustLoyaltySingleVendorInd SingleVendorInd
        {
            get
            {
                return this.singleVendorIndField;
            }
            set
            {
                this.singleVendorIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SingleVendorIndSpecified
        {
            get
            {
                return this.singleVendorIndFieldSpecified;
            }
            set
            {
                this.singleVendorIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime SignupDate
        {
            get
            {
                return this.signupDateField;
            }
            set
            {
                this.signupDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SignupDateSpecified
        {
            get
            {
                return this.signupDateFieldSpecified;
            }
            set
            {
                this.signupDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime EffectiveDate
        {
            get
            {
                return this.effectiveDateField;
            }
            set
            {
                this.effectiveDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EffectiveDateSpecified
        {
            get
            {
                return this.effectiveDateFieldSpecified;
            }
            set
            {
                this.effectiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime ExpireDate
        {
            get
            {
                return this.expireDateField;
            }
            set
            {
                this.expireDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExpireDateSpecified
        {
            get
            {
                return this.expireDateFieldSpecified;
            }
            set
            {
                this.expireDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool ExpireDateExclusiveIndicator
        {
            get
            {
                return this.expireDateExclusiveIndicatorField;
            }
            set
            {
                this.expireDateExclusiveIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExpireDateExclusiveIndicatorSpecified
        {
            get
            {
                return this.expireDateExclusiveIndicatorFieldSpecified;
            }
            set
            {
                this.expireDateExclusiveIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RPH
        {
            get
            {
                return this.rPHField;
            }
            set
            {
                this.rPHField = value;
            }
        }
    }



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ResRetrieveRSReservationsListPackageReservation
    {

        private UniqueID_Type uniqueIDField;

        private PersonNameType nameField;

        private LocationType arrivalLocationField;

        private LocationType departureLocationField;

        private string travelCodeField;

        private string tourCodeField;

        private string packageIDField;

        private string startField;

        private string durationField;

        private string endField;

        private string quantityField;

        private string reservationStatusCodeField;

        private InventoryStatusType reservationStatusField;

        private bool reservationStatusFieldSpecified;

        /// <remarks/>
        public UniqueID_Type UniqueID
        {
            get
            {
                return this.uniqueIDField;
            }
            set
            {
                this.uniqueIDField = value;
            }
        }

        /// <remarks/>
        public PersonNameType Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public LocationType ArrivalLocation
        {
            get
            {
                return this.arrivalLocationField;
            }
            set
            {
                this.arrivalLocationField = value;
            }
        }

        /// <remarks/>
        public LocationType DepartureLocation
        {
            get
            {
                return this.departureLocationField;
            }
            set
            {
                this.departureLocationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TravelCode
        {
            get
            {
                return this.travelCodeField;
            }
            set
            {
                this.travelCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TourCode
        {
            get
            {
                return this.tourCodeField;
            }
            set
            {
                this.tourCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PackageID
        {
            get
            {
                return this.packageIDField;
            }
            set
            {
                this.packageIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string End
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string Quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ReservationStatusCode
        {
            get
            {
                return this.reservationStatusCodeField;
            }
            set
            {
                this.reservationStatusCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public InventoryStatusType ReservationStatus
        {
            get
            {
                return this.reservationStatusField;
            }
            set
            {
                this.reservationStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ReservationStatusSpecified
        {
            get
            {
                return this.reservationStatusFieldSpecified;
            }
            set
            {
                this.reservationStatusFieldSpecified = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ResRetrieveRSReservationsListVehicleReservation
    {

        private VehicleReservationSummaryType[] vehResSummaryField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("VehResSummary")]
        public VehicleReservationSummaryType[] VehResSummary
        {
            get
            {
                return this.vehResSummaryField;
            }
            set
            {
                this.vehResSummaryField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_ResRetrieveRSReservationsListAirReservation
    {

        private FlightSegmentType flightSegmentField;

        private PersonNameType[] travelerNameField;

        private TPA_ExtensionsType tPA_ExtensionsField;

        private string bookingReferenceIDField;

        private System.DateTime dateBookedField;

        private bool dateBookedFieldSpecified;

        private string itineraryNameField;

        private TransactionStatusType statusField;

        private bool statusFieldSpecified;

        private string[] supplierBookingInfoListField;

        /// <remarks/>
        public FlightSegmentType FlightSegment
        {
            get
            {
                return this.flightSegmentField;
            }
            set
            {
                this.flightSegmentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("TravelerName")]
        public PersonNameType[] TravelerName
        {
            get
            {
                return this.travelerNameField;
            }
            set
            {
                this.travelerNameField = value;
            }
        }

        /// <remarks/>
        public TPA_ExtensionsType TPA_Extensions
        {
            get
            {
                return this.tPA_ExtensionsField;
            }
            set
            {
                this.tPA_ExtensionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string BookingReferenceID
        {
            get
            {
                return this.bookingReferenceIDField;
            }
            set
            {
                this.bookingReferenceIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime DateBooked
        {
            get
            {
                return this.dateBookedField;
            }
            set
            {
                this.dateBookedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DateBookedSpecified
        {
            get
            {
                return this.dateBookedFieldSpecified;
            }
            set
            {
                this.dateBookedFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ItineraryName
        {
            get
            {
                return this.itineraryNameField;
            }
            set
            {
                this.itineraryNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public TransactionStatusType Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StatusSpecified
        {
            get
            {
                return this.statusFieldSpecified;
            }
            set
            {
                this.statusFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] SupplierBookingInfoList
        {
            get
            {
                return this.supplierBookingInfoListField;
            }
            set
            {
                this.supplierBookingInfoListField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class OTA_VehRetResRQVehRetResRQCore : VehicleRetrieveResRQCoreType
    {

        private string contractStatusField;

        private string reservationStatusField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ContractStatus
        {
            get
            {
                return this.contractStatusField;
            }
            set
            {
                this.contractStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ReservationStatus
        {
            get
            {
                return this.reservationStatusField;
            }
            set
            {
                this.reservationStatusField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class SailingType : SailingInfoType
    {

        private SailingTypeDining[] diningField;

        private GuestTransportationType[] transportationField;

        private ParagraphType[] informationField;

        private string maxCabinOccupancyField;

        private CategoryLocationType categoryLocationField;

        private bool categoryLocationFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Dining")]
        public SailingTypeDining[] Dining
        {
            get
            {
                return this.diningField;
            }
            set
            {
                this.diningField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Transportation")]
        public GuestTransportationType[] Transportation
        {
            get
            {
                return this.transportationField;
            }
            set
            {
                this.transportationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Information")]
        public ParagraphType[] Information
        {
            get
            {
                return this.informationField;
            }
            set
            {
                this.informationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string MaxCabinOccupancy
        {
            get
            {
                return this.maxCabinOccupancyField;
            }
            set
            {
                this.maxCabinOccupancyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public CategoryLocationType CategoryLocation
        {
            get
            {
                return this.categoryLocationField;
            }
            set
            {
                this.categoryLocationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CategoryLocationSpecified
        {
            get
            {
                return this.categoryLocationFieldSpecified;
            }
            set
            {
                this.categoryLocationFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class SailingTypeDining
    {

        private string sittingField;

        private string statusField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Sitting
        {
            get
            {
                return this.sittingField;
            }
            set
            {
                this.sittingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class VehicleRetrieveResRQAdditionalInfoType
    {

        private LocationType pickUpLocationField;

        private LocationType returnLocationField;

        private VehicleRetrieveResRQAdditionalInfoTypeTelephone telephoneField;

        private CompanyNameType vendorField;

        private VehiclePrefType vehPrefField;

        private EmailType emailField;

        private ParagraphType[] remarkField;

        private VehicleRetrieveResRQAdditionalInfoTypeSearchDateRange[] searchDateRangeField;

        private TPA_ExtensionsType tPA_ExtensionsField;

        private System.DateTime pickUpDateTimeField;

        private bool pickUpDateTimeFieldSpecified;

        /// <remarks/>
        public LocationType PickUpLocation
        {
            get
            {
                return this.pickUpLocationField;
            }
            set
            {
                this.pickUpLocationField = value;
            }
        }

        /// <remarks/>
        public LocationType ReturnLocation
        {
            get
            {
                return this.returnLocationField;
            }
            set
            {
                this.returnLocationField = value;
            }
        }

        /// <remarks/>
        public VehicleRetrieveResRQAdditionalInfoTypeTelephone Telephone
        {
            get
            {
                return this.telephoneField;
            }
            set
            {
                this.telephoneField = value;
            }
        }

        /// <remarks/>
        public CompanyNameType Vendor
        {
            get
            {
                return this.vendorField;
            }
            set
            {
                this.vendorField = value;
            }
        }

        /// <remarks/>
        public VehiclePrefType VehPref
        {
            get
            {
                return this.vehPrefField;
            }
            set
            {
                this.vehPrefField = value;
            }
        }

        /// <remarks/>
        public EmailType Email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Remark")]
        public ParagraphType[] Remark
        {
            get
            {
                return this.remarkField;
            }
            set
            {
                this.remarkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SearchDateRange")]
        public VehicleRetrieveResRQAdditionalInfoTypeSearchDateRange[] SearchDateRange
        {
            get
            {
                return this.searchDateRangeField;
            }
            set
            {
                this.searchDateRangeField = value;
            }
        }

        /// <remarks/>
        public TPA_ExtensionsType TPA_Extensions
        {
            get
            {
                return this.tPA_ExtensionsField;
            }
            set
            {
                this.tPA_ExtensionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime PickUpDateTime
        {
            get
            {
                return this.pickUpDateTimeField;
            }
            set
            {
                this.pickUpDateTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PickUpDateTimeSpecified
        {
            get
            {
                return this.pickUpDateTimeFieldSpecified;
            }
            set
            {
                this.pickUpDateTimeFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class VehicleRetrieveResRQAdditionalInfoTypeSearchDateRange
    {

        private string startField;

        private string durationField;

        private string endField;

        private VehicleRetrieveResRQAdditionalInfoTypeSearchDateRangeSearchQualifier searchQualifierField;

        private bool searchQualifierFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string End
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public VehicleRetrieveResRQAdditionalInfoTypeSearchDateRangeSearchQualifier SearchQualifier
        {
            get
            {
                return this.searchQualifierField;
            }
            set
            {
                this.searchQualifierField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SearchQualifierSpecified
        {
            get
            {
                return this.searchQualifierFieldSpecified;
            }
            set
            {
                this.searchQualifierFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class VehicleRetrieveResRQAdditionalInfoTypeTelephone
    {

        private ItemSearchCriterionTypeTelephoneShareSynchInd shareSynchIndField;

        private bool shareSynchIndFieldSpecified;

        private ItemSearchCriterionTypeTelephoneShareMarketInd shareMarketIndField;

        private bool shareMarketIndFieldSpecified;

        private string phoneLocationTypeField;

        private string phoneTechTypeField;

        private string phoneUseTypeField;

        private string countryAccessCodeField;

        private string areaCityCodeField;

        private string phoneNumberField;

        private string extensionField;

        private string pINField;

        private string remarkField;

        private bool formattedIndField;

        private bool formattedIndFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ItemSearchCriterionTypeTelephoneShareSynchInd ShareSynchInd
        {
            get
            {
                return this.shareSynchIndField;
            }
            set
            {
                this.shareSynchIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ShareSynchIndSpecified
        {
            get
            {
                return this.shareSynchIndFieldSpecified;
            }
            set
            {
                this.shareSynchIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ItemSearchCriterionTypeTelephoneShareMarketInd ShareMarketInd
        {
            get
            {
                return this.shareMarketIndField;
            }
            set
            {
                this.shareMarketIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ShareMarketIndSpecified
        {
            get
            {
                return this.shareMarketIndFieldSpecified;
            }
            set
            {
                this.shareMarketIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PhoneLocationType
        {
            get
            {
                return this.phoneLocationTypeField;
            }
            set
            {
                this.phoneLocationTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PhoneTechType
        {
            get
            {
                return this.phoneTechTypeField;
            }
            set
            {
                this.phoneTechTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PhoneUseType
        {
            get
            {
                return this.phoneUseTypeField;
            }
            set
            {
                this.phoneUseTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CountryAccessCode
        {
            get
            {
                return this.countryAccessCodeField;
            }
            set
            {
                this.countryAccessCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AreaCityCode
        {
            get
            {
                return this.areaCityCodeField;
            }
            set
            {
                this.areaCityCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PhoneNumber
        {
            get
            {
                return this.phoneNumberField;
            }
            set
            {
                this.phoneNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Extension
        {
            get
            {
                return this.extensionField;
            }
            set
            {
                this.extensionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PIN
        {
            get
            {
                return this.pINField;
            }
            set
            {
                this.pINField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Remark
        {
            get
            {
                return this.remarkField;
            }
            set
            {
                this.remarkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool FormattedInd
        {
            get
            {
                return this.formattedIndField;
            }
            set
            {
                this.formattedIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FormattedIndSpecified
        {
            get
            {
                return this.formattedIndFieldSpecified;
            }
            set
            {
                this.formattedIndFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class VehicleRetrieveResRQCoreType
    {

        private UniqueID_Type[] uniqueIDField;

        private PersonNameType personNameField;

        private VehicleRetrieveResRQCoreTypeCustLoyalty custLoyaltyField;

        private TPA_ExtensionsType tPA_ExtensionsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("UniqueID")]
        public UniqueID_Type[] UniqueID
        {
            get
            {
                return this.uniqueIDField;
            }
            set
            {
                this.uniqueIDField = value;
            }
        }

        /// <remarks/>
        public PersonNameType PersonName
        {
            get
            {
                return this.personNameField;
            }
            set
            {
                this.personNameField = value;
            }
        }

        /// <remarks/>
        public VehicleRetrieveResRQCoreTypeCustLoyalty CustLoyalty
        {
            get
            {
                return this.custLoyaltyField;
            }
            set
            {
                this.custLoyaltyField = value;
            }
        }

        /// <remarks/>
        public TPA_ExtensionsType TPA_Extensions
        {
            get
            {
                return this.tPA_ExtensionsField;
            }
            set
            {
                this.tPA_ExtensionsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class VehicleRetrieveResRQCoreTypeCustLoyalty
    {

        private PaymentCardTypeCustLoyaltyShareSynchInd shareSynchIndField;

        private bool shareSynchIndFieldSpecified;

        private PaymentCardTypeCustLoyaltyShareMarketInd shareMarketIndField;

        private bool shareMarketIndFieldSpecified;

        private string programIDField;

        private string membershipIDField;

        private string travelSectorField;

        private string[] vendorCodeField;

        private bool primaryLoyaltyIndicatorField;

        private bool primaryLoyaltyIndicatorFieldSpecified;

        private string allianceLoyaltyLevelNameField;

        private string customerTypeField;

        private string customerValueField;

        private string passwordField;

        private string loyalLevelField;

        private string loyalLevelCodeField;

        private PaymentCardTypeCustLoyaltySingleVendorInd singleVendorIndField;

        private bool singleVendorIndFieldSpecified;

        private System.DateTime signupDateField;

        private bool signupDateFieldSpecified;

        private System.DateTime effectiveDateField;

        private bool effectiveDateFieldSpecified;

        private System.DateTime expireDateField;

        private bool expireDateFieldSpecified;

        private bool expireDateExclusiveIndicatorField;

        private bool expireDateExclusiveIndicatorFieldSpecified;

        private string rPHField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PaymentCardTypeCustLoyaltyShareSynchInd ShareSynchInd
        {
            get
            {
                return this.shareSynchIndField;
            }
            set
            {
                this.shareSynchIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ShareSynchIndSpecified
        {
            get
            {
                return this.shareSynchIndFieldSpecified;
            }
            set
            {
                this.shareSynchIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PaymentCardTypeCustLoyaltyShareMarketInd ShareMarketInd
        {
            get
            {
                return this.shareMarketIndField;
            }
            set
            {
                this.shareMarketIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ShareMarketIndSpecified
        {
            get
            {
                return this.shareMarketIndFieldSpecified;
            }
            set
            {
                this.shareMarketIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ProgramID
        {
            get
            {
                return this.programIDField;
            }
            set
            {
                this.programIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MembershipID
        {
            get
            {
                return this.membershipIDField;
            }
            set
            {
                this.membershipIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TravelSector
        {
            get
            {
                return this.travelSectorField;
            }
            set
            {
                this.travelSectorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] VendorCode
        {
            get
            {
                return this.vendorCodeField;
            }
            set
            {
                this.vendorCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool PrimaryLoyaltyIndicator
        {
            get
            {
                return this.primaryLoyaltyIndicatorField;
            }
            set
            {
                this.primaryLoyaltyIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrimaryLoyaltyIndicatorSpecified
        {
            get
            {
                return this.primaryLoyaltyIndicatorFieldSpecified;
            }
            set
            {
                this.primaryLoyaltyIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AllianceLoyaltyLevelName
        {
            get
            {
                return this.allianceLoyaltyLevelNameField;
            }
            set
            {
                this.allianceLoyaltyLevelNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CustomerType
        {
            get
            {
                return this.customerTypeField;
            }
            set
            {
                this.customerTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CustomerValue
        {
            get
            {
                return this.customerValueField;
            }
            set
            {
                this.customerValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LoyalLevel
        {
            get
            {
                return this.loyalLevelField;
            }
            set
            {
                this.loyalLevelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string LoyalLevelCode
        {
            get
            {
                return this.loyalLevelCodeField;
            }
            set
            {
                this.loyalLevelCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PaymentCardTypeCustLoyaltySingleVendorInd SingleVendorInd
        {
            get
            {
                return this.singleVendorIndField;
            }
            set
            {
                this.singleVendorIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SingleVendorIndSpecified
        {
            get
            {
                return this.singleVendorIndFieldSpecified;
            }
            set
            {
                this.singleVendorIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime SignupDate
        {
            get
            {
                return this.signupDateField;
            }
            set
            {
                this.signupDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SignupDateSpecified
        {
            get
            {
                return this.signupDateFieldSpecified;
            }
            set
            {
                this.signupDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime EffectiveDate
        {
            get
            {
                return this.effectiveDateField;
            }
            set
            {
                this.effectiveDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EffectiveDateSpecified
        {
            get
            {
                return this.effectiveDateFieldSpecified;
            }
            set
            {
                this.effectiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime ExpireDate
        {
            get
            {
                return this.expireDateField;
            }
            set
            {
                this.expireDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExpireDateSpecified
        {
            get
            {
                return this.expireDateFieldSpecified;
            }
            set
            {
                this.expireDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool ExpireDateExclusiveIndicator
        {
            get
            {
                return this.expireDateExclusiveIndicatorField;
            }
            set
            {
                this.expireDateExclusiveIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExpireDateExclusiveIndicatorSpecified
        {
            get
            {
                return this.expireDateExclusiveIndicatorFieldSpecified;
            }
            set
            {
                this.expireDateExclusiveIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RPH
        {
            get
            {
                return this.rPHField;
            }
            set
            {
                this.rPHField = value;
            }
        }
    }
}
