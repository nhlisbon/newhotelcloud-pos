﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Contracts.OTA
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum RoomStayCandidateTypeRoomGender
    {

        /// <remarks/>
        Male,

        /// <remarks/>
        Female,

        /// <remarks/>
        MaleAndFemale,

        /// <remarks/>
        Unknown,
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public enum RatePlanCandidatesTypeRatePlanCandidatePrepaidQualifier
    {

        /// <remarks/>
        IncludePrepaid,

        /// <remarks/>
        PrepaidOnly,

        /// <remarks/>
        ExcludePrepaid,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class DestinationSystemCodesTypeDestinationSystemCode
    {

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class RateUploadType
    {

        private RateUploadTypeBaseByGuestAmt[] baseByGuestAmtsField;

        private RateUploadTypeAdditionalGuestAmount[] additionalGuestAmountsField;

        private FeeType[] feesField;

        private GuaranteeType[] guaranteePoliciesField;

        private CancelPenaltiesType cancelPoliciesField;

        private RequiredPaymentsTypeGuaranteePayment[] paymentPoliciesField;

        private ParagraphType rateDescriptionField;

        private UniqueID_Type uniqueIDField;

        private RateUploadTypeMealsIncluded mealsIncludedField;

        private HotelAdditionalChargesType additionalChargesField;

        private string numberOfUnitsField;

        private TimeUnitType rateTimeUnitField;

        private bool rateTimeUnitFieldSpecified;

        private string unitMultiplierField;

        private string minGuestApplicableField;

        private string maxGuestApplicableField;

        private string ageQualifyingCodeField;

        private string minAgeField;

        private string maxAgeField;

        private TimeUnitType ageTimeUnitField;

        private bool ageTimeUnitFieldSpecified;

        private string ageBucketField;

        private string minLOSField;

        private string maxLOSField;

        private DayOfWeekType stayOverDateField;

        private bool stayOverDateFieldSpecified;

        private string startField;

        private string durationField;

        private string endField;

        private bool monField;

        private bool monFieldSpecified;

        private bool tueField;

        private bool tueFieldSpecified;

        private bool wedsField;

        private bool wedsFieldSpecified;

        private bool thurField;

        private bool thurFieldSpecified;

        private bool friField;

        private bool friFieldSpecified;

        private bool satField;

        private bool satFieldSpecified;

        private bool sunField;

        private bool sunFieldSpecified;

        private string currencyCodeField;

        private string decimalPlacesField;

        private string rateTierField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("BaseByGuestAmt", IsNullable = false)]
        public RateUploadTypeBaseByGuestAmt[] BaseByGuestAmts
        {
            get
            {
                return this.baseByGuestAmtsField;
            }
            set
            {
                this.baseByGuestAmtsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("AdditionalGuestAmount", IsNullable = false)]
        public RateUploadTypeAdditionalGuestAmount[] AdditionalGuestAmounts
        {
            get
            {
                return this.additionalGuestAmountsField;
            }
            set
            {
                this.additionalGuestAmountsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Fee", IsNullable = false)]
        public FeeType[] Fees
        {
            get
            {
                return this.feesField;
            }
            set
            {
                this.feesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("GuaranteePolicy", IsNullable = false)]
        public GuaranteeType[] GuaranteePolicies
        {
            get
            {
                return this.guaranteePoliciesField;
            }
            set
            {
                this.guaranteePoliciesField = value;
            }
        }

        /// <remarks/>
        public CancelPenaltiesType CancelPolicies
        {
            get
            {
                return this.cancelPoliciesField;
            }
            set
            {
                this.cancelPoliciesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("GuaranteePayment", IsNullable = false)]
        public RequiredPaymentsTypeGuaranteePayment[] PaymentPolicies
        {
            get
            {
                return this.paymentPoliciesField;
            }
            set
            {
                this.paymentPoliciesField = value;
            }
        }

        /// <remarks/>
        public ParagraphType RateDescription
        {
            get
            {
                return this.rateDescriptionField;
            }
            set
            {
                this.rateDescriptionField = value;
            }
        }

        /// <remarks/>
        public UniqueID_Type UniqueID
        {
            get
            {
                return this.uniqueIDField;
            }
            set
            {
                this.uniqueIDField = value;
            }
        }

        /// <remarks/>
        public RateUploadTypeMealsIncluded MealsIncluded
        {
            get
            {
                return this.mealsIncludedField;
            }
            set
            {
                this.mealsIncludedField = value;
            }
        }

        /// <remarks/>
        public HotelAdditionalChargesType AdditionalCharges
        {
            get
            {
                return this.additionalChargesField;
            }
            set
            {
                this.additionalChargesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string NumberOfUnits
        {
            get
            {
                return this.numberOfUnitsField;
            }
            set
            {
                this.numberOfUnitsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public TimeUnitType RateTimeUnit
        {
            get
            {
                return this.rateTimeUnitField;
            }
            set
            {
                this.rateTimeUnitField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RateTimeUnitSpecified
        {
            get
            {
                return this.rateTimeUnitFieldSpecified;
            }
            set
            {
                this.rateTimeUnitFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string UnitMultiplier
        {
            get
            {
                return this.unitMultiplierField;
            }
            set
            {
                this.unitMultiplierField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string MinGuestApplicable
        {
            get
            {
                return this.minGuestApplicableField;
            }
            set
            {
                this.minGuestApplicableField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string MaxGuestApplicable
        {
            get
            {
                return this.maxGuestApplicableField;
            }
            set
            {
                this.maxGuestApplicableField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AgeQualifyingCode
        {
            get
            {
                return this.ageQualifyingCodeField;
            }
            set
            {
                this.ageQualifyingCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string MinAge
        {
            get
            {
                return this.minAgeField;
            }
            set
            {
                this.minAgeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string MaxAge
        {
            get
            {
                return this.maxAgeField;
            }
            set
            {
                this.maxAgeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public TimeUnitType AgeTimeUnit
        {
            get
            {
                return this.ageTimeUnitField;
            }
            set
            {
                this.ageTimeUnitField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AgeTimeUnitSpecified
        {
            get
            {
                return this.ageTimeUnitFieldSpecified;
            }
            set
            {
                this.ageTimeUnitFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AgeBucket
        {
            get
            {
                return this.ageBucketField;
            }
            set
            {
                this.ageBucketField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MinLOS
        {
            get
            {
                return this.minLOSField;
            }
            set
            {
                this.minLOSField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MaxLOS
        {
            get
            {
                return this.maxLOSField;
            }
            set
            {
                this.maxLOSField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public DayOfWeekType StayOverDate
        {
            get
            {
                return this.stayOverDateField;
            }
            set
            {
                this.stayOverDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StayOverDateSpecified
        {
            get
            {
                return this.stayOverDateFieldSpecified;
            }
            set
            {
                this.stayOverDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string End
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Mon
        {
            get
            {
                return this.monField;
            }
            set
            {
                this.monField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MonSpecified
        {
            get
            {
                return this.monFieldSpecified;
            }
            set
            {
                this.monFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Tue
        {
            get
            {
                return this.tueField;
            }
            set
            {
                this.tueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TueSpecified
        {
            get
            {
                return this.tueFieldSpecified;
            }
            set
            {
                this.tueFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Weds
        {
            get
            {
                return this.wedsField;
            }
            set
            {
                this.wedsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool WedsSpecified
        {
            get
            {
                return this.wedsFieldSpecified;
            }
            set
            {
                this.wedsFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Thur
        {
            get
            {
                return this.thurField;
            }
            set
            {
                this.thurField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ThurSpecified
        {
            get
            {
                return this.thurFieldSpecified;
            }
            set
            {
                this.thurFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Fri
        {
            get
            {
                return this.friField;
            }
            set
            {
                this.friField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FriSpecified
        {
            get
            {
                return this.friFieldSpecified;
            }
            set
            {
                this.friFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Sat
        {
            get
            {
                return this.satField;
            }
            set
            {
                this.satField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SatSpecified
        {
            get
            {
                return this.satFieldSpecified;
            }
            set
            {
                this.satFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Sun
        {
            get
            {
                return this.sunField;
            }
            set
            {
                this.sunField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SunSpecified
        {
            get
            {
                return this.sunFieldSpecified;
            }
            set
            {
                this.sunFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CurrencyCode
        {
            get
            {
                return this.currencyCodeField;
            }
            set
            {
                this.currencyCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string DecimalPlaces
        {
            get
            {
                return this.decimalPlacesField;
            }
            set
            {
                this.decimalPlacesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RateTier
        {
            get
            {
                return this.rateTierField;
            }
            set
            {
                this.rateTierField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class RateUploadTypeBaseByGuestAmt : TotalType
    {

        private ParagraphType[] numberOfGuestsDescriptionField;

        private string codeField;

        private string numberOfGuestsField;

        private string ageQualifyingCodeField;

        private string minAgeField;

        private string maxAgeField;

        private TimeUnitType ageTimeUnitField;

        private bool ageTimeUnitFieldSpecified;

        private string ageBucketField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("NumberOfGuestsDescription")]
        public ParagraphType[] NumberOfGuestsDescription
        {
            get
            {
                return this.numberOfGuestsDescriptionField;
            }
            set
            {
                this.numberOfGuestsDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string NumberOfGuests
        {
            get
            {
                return this.numberOfGuestsField;
            }
            set
            {
                this.numberOfGuestsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AgeQualifyingCode
        {
            get
            {
                return this.ageQualifyingCodeField;
            }
            set
            {
                this.ageQualifyingCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string MinAge
        {
            get
            {
                return this.minAgeField;
            }
            set
            {
                this.minAgeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string MaxAge
        {
            get
            {
                return this.maxAgeField;
            }
            set
            {
                this.maxAgeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public TimeUnitType AgeTimeUnit
        {
            get
            {
                return this.ageTimeUnitField;
            }
            set
            {
                this.ageTimeUnitField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AgeTimeUnitSpecified
        {
            get
            {
                return this.ageTimeUnitFieldSpecified;
            }
            set
            {
                this.ageTimeUnitFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AgeBucket
        {
            get
            {
                return this.ageBucketField;
            }
            set
            {
                this.ageBucketField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class RateUploadTypeAdditionalGuestAmount
    {

        private TaxesType taxesField;

        private ParagraphType[] addlGuestAmtDescriptionField;

        private bool taxInclusiveField;

        private bool taxInclusiveFieldSpecified;

        private string maxAdditionalGuestsField;

        private string ageQualifyingCodeField;

        private string minAgeField;

        private string maxAgeField;

        private TimeUnitType ageTimeUnitField;

        private bool ageTimeUnitFieldSpecified;

        private string ageBucketField;

        private AmountDeterminationType typeField;

        private bool typeFieldSpecified;

        private string codeField;

        private decimal amountField;

        private bool amountFieldSpecified;

        private string currencyCodeField;

        private string decimalPlacesField;

        private decimal percentField;

        private bool percentFieldSpecified;

        /// <remarks/>
        public TaxesType Taxes
        {
            get
            {
                return this.taxesField;
            }
            set
            {
                this.taxesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AddlGuestAmtDescription")]
        public ParagraphType[] AddlGuestAmtDescription
        {
            get
            {
                return this.addlGuestAmtDescriptionField;
            }
            set
            {
                this.addlGuestAmtDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool TaxInclusive
        {
            get
            {
                return this.taxInclusiveField;
            }
            set
            {
                this.taxInclusiveField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TaxInclusiveSpecified
        {
            get
            {
                return this.taxInclusiveFieldSpecified;
            }
            set
            {
                this.taxInclusiveFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string MaxAdditionalGuests
        {
            get
            {
                return this.maxAdditionalGuestsField;
            }
            set
            {
                this.maxAdditionalGuestsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AgeQualifyingCode
        {
            get
            {
                return this.ageQualifyingCodeField;
            }
            set
            {
                this.ageQualifyingCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string MinAge
        {
            get
            {
                return this.minAgeField;
            }
            set
            {
                this.minAgeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string MaxAge
        {
            get
            {
                return this.maxAgeField;
            }
            set
            {
                this.maxAgeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public TimeUnitType AgeTimeUnit
        {
            get
            {
                return this.ageTimeUnitField;
            }
            set
            {
                this.ageTimeUnitField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AgeTimeUnitSpecified
        {
            get
            {
                return this.ageTimeUnitFieldSpecified;
            }
            set
            {
                this.ageTimeUnitFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AgeBucket
        {
            get
            {
                return this.ageBucketField;
            }
            set
            {
                this.ageBucketField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public AmountDeterminationType Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TypeSpecified
        {
            get
            {
                return this.typeFieldSpecified;
            }
            set
            {
                this.typeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AmountSpecified
        {
            get
            {
                return this.amountFieldSpecified;
            }
            set
            {
                this.amountFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CurrencyCode
        {
            get
            {
                return this.currencyCodeField;
            }
            set
            {
                this.currencyCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string DecimalPlaces
        {
            get
            {
                return this.decimalPlacesField;
            }
            set
            {
                this.decimalPlacesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal Percent
        {
            get
            {
                return this.percentField;
            }
            set
            {
                this.percentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PercentSpecified
        {
            get
            {
                return this.percentFieldSpecified;
            }
            set
            {
                this.percentFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class RateUploadTypeMealsIncluded
    {

        private bool breakfastField;

        private bool breakfastFieldSpecified;

        private bool lunchField;

        private bool lunchFieldSpecified;

        private bool dinnerField;

        private bool dinnerFieldSpecified;

        private bool mealPlanIndicatorField;

        private bool mealPlanIndicatorFieldSpecified;

        private string[] mealPlanCodesField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Breakfast
        {
            get
            {
                return this.breakfastField;
            }
            set
            {
                this.breakfastField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BreakfastSpecified
        {
            get
            {
                return this.breakfastFieldSpecified;
            }
            set
            {
                this.breakfastFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Lunch
        {
            get
            {
                return this.lunchField;
            }
            set
            {
                this.lunchField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool LunchSpecified
        {
            get
            {
                return this.lunchFieldSpecified;
            }
            set
            {
                this.lunchFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Dinner
        {
            get
            {
                return this.dinnerField;
            }
            set
            {
                this.dinnerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DinnerSpecified
        {
            get
            {
                return this.dinnerFieldSpecified;
            }
            set
            {
                this.dinnerFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool MealPlanIndicator
        {
            get
            {
                return this.mealPlanIndicatorField;
            }
            set
            {
                this.mealPlanIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MealPlanIndicatorSpecified
        {
            get
            {
                return this.mealPlanIndicatorFieldSpecified;
            }
            set
            {
                this.mealPlanIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] MealPlanCodes
        {
            get
            {
                return this.mealPlanCodesField;
            }
            set
            {
                this.mealPlanCodesField = value;
            }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class MeetingRoomsType
    {

        private MeetingRoomsTypeMeetingRoom[] meetingRoomField;

        private string meetingRoomCountField;

        private string smallestRoomSpaceField;

        private string largestRoomSpaceField;

        private decimal unitOfMeasureQuantityField;

        private bool unitOfMeasureQuantityFieldSpecified;

        private string unitOfMeasureField;

        private string unitOfMeasureCodeField;

        private string totalRoomSpaceField;

        private string largestSeatingCapacityField;

        private string secondLargestSeatingCapacityField;

        private string smallestSeatingCapacityField;

        private string totalRoomSeatingCapacityField;

        private string largestRoomHeightField;

        private string totalExhibitSpaceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("MeetingRoom")]
        public MeetingRoomsTypeMeetingRoom[] MeetingRoom
        {
            get
            {
                return this.meetingRoomField;
            }
            set
            {
                this.meetingRoomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string MeetingRoomCount
        {
            get
            {
                return this.meetingRoomCountField;
            }
            set
            {
                this.meetingRoomCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string SmallestRoomSpace
        {
            get
            {
                return this.smallestRoomSpaceField;
            }
            set
            {
                this.smallestRoomSpaceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string LargestRoomSpace
        {
            get
            {
                return this.largestRoomSpaceField;
            }
            set
            {
                this.largestRoomSpaceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal UnitOfMeasureQuantity
        {
            get
            {
                return this.unitOfMeasureQuantityField;
            }
            set
            {
                this.unitOfMeasureQuantityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool UnitOfMeasureQuantitySpecified
        {
            get
            {
                return this.unitOfMeasureQuantityFieldSpecified;
            }
            set
            {
                this.unitOfMeasureQuantityFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string UnitOfMeasure
        {
            get
            {
                return this.unitOfMeasureField;
            }
            set
            {
                this.unitOfMeasureField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string UnitOfMeasureCode
        {
            get
            {
                return this.unitOfMeasureCodeField;
            }
            set
            {
                this.unitOfMeasureCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string TotalRoomSpace
        {
            get
            {
                return this.totalRoomSpaceField;
            }
            set
            {
                this.totalRoomSpaceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string LargestSeatingCapacity
        {
            get
            {
                return this.largestSeatingCapacityField;
            }
            set
            {
                this.largestSeatingCapacityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string SecondLargestSeatingCapacity
        {
            get
            {
                return this.secondLargestSeatingCapacityField;
            }
            set
            {
                this.secondLargestSeatingCapacityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string SmallestSeatingCapacity
        {
            get
            {
                return this.smallestSeatingCapacityField;
            }
            set
            {
                this.smallestSeatingCapacityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string TotalRoomSeatingCapacity
        {
            get
            {
                return this.totalRoomSeatingCapacityField;
            }
            set
            {
                this.totalRoomSeatingCapacityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string LargestRoomHeight
        {
            get
            {
                return this.largestRoomHeightField;
            }
            set
            {
                this.largestRoomHeightField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string TotalExhibitSpace
        {
            get
            {
                return this.totalExhibitSpaceField;
            }
            set
            {
                this.totalExhibitSpaceField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class MeetingRoomsTypeMeetingRoom
    {

        private MeetingRoomCodeType[] codesField;

        private MeetingRoomsTypeMeetingRoomDimension dimensionField;

        private MeetingRoomCapacityType[] availableCapacitiesField;

        private FeaturesTypeFeature[] featuresField;

        private MultimediaDescriptionsType multimediaDescriptionsField;

        private bool irregularField;

        private bool irregularFieldSpecified;

        private string propertySystemNameField;

        private string roomNameField;

        private string sortField;

        private string meetingRoomCapacityField;

        private bool removalField;

        private bool removalFieldSpecified;

        private string idField;

        private string accessField;

        private string meetingRoomTypeCodeField;

        private string meetingRoomLevelField;

        private bool dedicatedIndicatorField;

        private bool dedicatedIndicatorFieldSpecified;

        private bool featuredIndField;

        private bool featuredIndFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Code", IsNullable = false)]
        public MeetingRoomCodeType[] Codes
        {
            get
            {
                return this.codesField;
            }
            set
            {
                this.codesField = value;
            }
        }

        /// <remarks/>
        public MeetingRoomsTypeMeetingRoomDimension Dimension
        {
            get
            {
                return this.dimensionField;
            }
            set
            {
                this.dimensionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("MeetingRoomCapacity", IsNullable = false)]
        public MeetingRoomCapacityType[] AvailableCapacities
        {
            get
            {
                return this.availableCapacitiesField;
            }
            set
            {
                this.availableCapacitiesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Feature", IsNullable = false)]
        public FeaturesTypeFeature[] Features
        {
            get
            {
                return this.featuresField;
            }
            set
            {
                this.featuresField = value;
            }
        }

        /// <remarks/>
        public MultimediaDescriptionsType MultimediaDescriptions
        {
            get
            {
                return this.multimediaDescriptionsField;
            }
            set
            {
                this.multimediaDescriptionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Irregular
        {
            get
            {
                return this.irregularField;
            }
            set
            {
                this.irregularField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IrregularSpecified
        {
            get
            {
                return this.irregularFieldSpecified;
            }
            set
            {
                this.irregularFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PropertySystemName
        {
            get
            {
                return this.propertySystemNameField;
            }
            set
            {
                this.propertySystemNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RoomName
        {
            get
            {
                return this.roomNameField;
            }
            set
            {
                this.roomNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string Sort
        {
            get
            {
                return this.sortField;
            }
            set
            {
                this.sortField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string MeetingRoomCapacity
        {
            get
            {
                return this.meetingRoomCapacityField;
            }
            set
            {
                this.meetingRoomCapacityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Removal
        {
            get
            {
                return this.removalField;
            }
            set
            {
                this.removalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RemovalSpecified
        {
            get
            {
                return this.removalFieldSpecified;
            }
            set
            {
                this.removalFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Access
        {
            get
            {
                return this.accessField;
            }
            set
            {
                this.accessField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MeetingRoomTypeCode
        {
            get
            {
                return this.meetingRoomTypeCodeField;
            }
            set
            {
                this.meetingRoomTypeCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MeetingRoomLevel
        {
            get
            {
                return this.meetingRoomLevelField;
            }
            set
            {
                this.meetingRoomLevelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool DedicatedIndicator
        {
            get
            {
                return this.dedicatedIndicatorField;
            }
            set
            {
                this.dedicatedIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DedicatedIndicatorSpecified
        {
            get
            {
                return this.dedicatedIndicatorFieldSpecified;
            }
            set
            {
                this.dedicatedIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool FeaturedInd
        {
            get
            {
                return this.featuredIndField;
            }
            set
            {
                this.featuredIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FeaturedIndSpecified
        {
            get
            {
                return this.featuredIndFieldSpecified;
            }
            set
            {
                this.featuredIndFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class MeetingRoomCodeType
    {

        private FeeType chargeField;

        private MultimediaDescriptionsType multimediaDescriptionsField;

        private string codeField;

        private string existsCodeField;

        private bool removalField;

        private bool removalFieldSpecified;

        private string codeDetailField;

        private string quantityField;

        private string discountsAvailableCodeField;

        private string idField;

        /// <remarks/>
        public FeeType Charge
        {
            get
            {
                return this.chargeField;
            }
            set
            {
                this.chargeField = value;
            }
        }

        /// <remarks/>
        public MultimediaDescriptionsType MultimediaDescriptions
        {
            get
            {
                return this.multimediaDescriptionsField;
            }
            set
            {
                this.multimediaDescriptionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ExistsCode
        {
            get
            {
                return this.existsCodeField;
            }
            set
            {
                this.existsCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Removal
        {
            get
            {
                return this.removalField;
            }
            set
            {
                this.removalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RemovalSpecified
        {
            get
            {
                return this.removalFieldSpecified;
            }
            set
            {
                this.removalFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CodeDetail
        {
            get
            {
                return this.codeDetailField;
            }
            set
            {
                this.codeDetailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string Quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DiscountsAvailableCode
        {
            get
            {
                return this.discountsAvailableCodeField;
            }
            set
            {
                this.discountsAvailableCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class MeetingRoomCapacityType
    {

        private MeetingRoomCapacityTypeOccupancy occupancyField;

        private string meetingRoomFormatCodeField;

        /// <remarks/>
        public MeetingRoomCapacityTypeOccupancy Occupancy
        {
            get
            {
                return this.occupancyField;
            }
            set
            {
                this.occupancyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MeetingRoomFormatCode
        {
            get
            {
                return this.meetingRoomFormatCodeField;
            }
            set
            {
                this.meetingRoomFormatCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class MeetingRoomCapacityTypeOccupancy
    {

        private FeeType minRoomChargeField;

        private string minOccupancyField;

        private string maxOccupancyField;

        private string standardOccupancyField;

        /// <remarks/>
        public FeeType MinRoomCharge
        {
            get
            {
                return this.minRoomChargeField;
            }
            set
            {
                this.minRoomChargeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string MinOccupancy
        {
            get
            {
                return this.minOccupancyField;
            }
            set
            {
                this.minOccupancyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string MaxOccupancy
        {
            get
            {
                return this.maxOccupancyField;
            }
            set
            {
                this.maxOccupancyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string StandardOccupancy
        {
            get
            {
                return this.standardOccupancyField;
            }
            set
            {
                this.standardOccupancyField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class FeaturesTypeFeature
    {

        private FeaturesTypeFeatureCharge chargeField;

        private MultimediaDescriptionsType multimediaDescriptionsField;

        private string descriptiveTextField;

        private bool removalField;

        private bool removalFieldSpecified;

        private string codeDetailField;

        private string accessibleCodeField;

        private string securityCodeField;

        private string existsCodeField;

        private string proximityCodeField;

        private string idField;

        private decimal unitOfMeasureQuantityField;

        private bool unitOfMeasureQuantityFieldSpecified;

        private string unitOfMeasureField;

        private string unitOfMeasureCodeField;

        /// <remarks/>
        public FeaturesTypeFeatureCharge Charge
        {
            get
            {
                return this.chargeField;
            }
            set
            {
                this.chargeField = value;
            }
        }

        /// <remarks/>
        public MultimediaDescriptionsType MultimediaDescriptions
        {
            get
            {
                return this.multimediaDescriptionsField;
            }
            set
            {
                this.multimediaDescriptionsField = value;
            }
        }

        /// <remarks/>
        public string DescriptiveText
        {
            get
            {
                return this.descriptiveTextField;
            }
            set
            {
                this.descriptiveTextField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Removal
        {
            get
            {
                return this.removalField;
            }
            set
            {
                this.removalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RemovalSpecified
        {
            get
            {
                return this.removalFieldSpecified;
            }
            set
            {
                this.removalFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CodeDetail
        {
            get
            {
                return this.codeDetailField;
            }
            set
            {
                this.codeDetailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AccessibleCode
        {
            get
            {
                return this.accessibleCodeField;
            }
            set
            {
                this.accessibleCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SecurityCode
        {
            get
            {
                return this.securityCodeField;
            }
            set
            {
                this.securityCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ExistsCode
        {
            get
            {
                return this.existsCodeField;
            }
            set
            {
                this.existsCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ProximityCode
        {
            get
            {
                return this.proximityCodeField;
            }
            set
            {
                this.proximityCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal UnitOfMeasureQuantity
        {
            get
            {
                return this.unitOfMeasureQuantityField;
            }
            set
            {
                this.unitOfMeasureQuantityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool UnitOfMeasureQuantitySpecified
        {
            get
            {
                return this.unitOfMeasureQuantityFieldSpecified;
            }
            set
            {
                this.unitOfMeasureQuantityFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string UnitOfMeasure
        {
            get
            {
                return this.unitOfMeasureField;
            }
            set
            {
                this.unitOfMeasureField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string UnitOfMeasureCode
        {
            get
            {
                return this.unitOfMeasureCodeField;
            }
            set
            {
                this.unitOfMeasureCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class FeaturesTypeFeatureCharge
    {

        private string currencyCodeField;

        private string decimalPlacesField;

        private decimal amountField;

        private bool amountFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CurrencyCode
        {
            get
            {
                return this.currencyCodeField;
            }
            set
            {
                this.currencyCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string DecimalPlaces
        {
            get
            {
                return this.decimalPlacesField;
            }
            set
            {
                this.decimalPlacesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AmountSpecified
        {
            get
            {
                return this.amountFieldSpecified;
            }
            set
            {
                this.amountFieldSpecified = value;
            }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class RatePlanCandidatesTypeRatePlanCandidate
    {

        private RatePlanCandidatesTypeRatePlanCandidateHotelRef[] hotelRefsField;

        private RatePlanCandidatesTypeRatePlanCandidateMealsIncluded mealsIncludedField;

        private RatePlanCandidatesTypeRatePlanCandidateArrivalPolicy arrivalPolicyField;

        private RatePlanCandidatesTypeRatePlanCandidateRatePlanCommission ratePlanCommissionField;

        private string ratePlanTypeField;

        private string ratePlanCodeField;

        private string ratePlanIDField;

        private bool ratePlanQualifierField;

        private bool ratePlanQualifierFieldSpecified;

        private string promotionCodeField;

        private string[] promotionVendorCodeField;

        private string ratePlanCategoryField;

        private string rPHField;

        private RatePlanCandidatesTypeRatePlanCandidatePrepaidQualifier prepaidQualifierField;

        private bool prepaidQualifierFieldSpecified;

        private bool availRatesOnlyIndField;

        private bool availRatesOnlyIndFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("HotelRef", IsNullable = false)]
        public RatePlanCandidatesTypeRatePlanCandidateHotelRef[] HotelRefs
        {
            get
            {
                return this.hotelRefsField;
            }
            set
            {
                this.hotelRefsField = value;
            }
        }

        /// <remarks/>
        public RatePlanCandidatesTypeRatePlanCandidateMealsIncluded MealsIncluded
        {
            get
            {
                return this.mealsIncludedField;
            }
            set
            {
                this.mealsIncludedField = value;
            }
        }

        /// <remarks/>
        public RatePlanCandidatesTypeRatePlanCandidateArrivalPolicy ArrivalPolicy
        {
            get
            {
                return this.arrivalPolicyField;
            }
            set
            {
                this.arrivalPolicyField = value;
            }
        }

        /// <remarks/>
        public RatePlanCandidatesTypeRatePlanCandidateRatePlanCommission RatePlanCommission
        {
            get
            {
                return this.ratePlanCommissionField;
            }
            set
            {
                this.ratePlanCommissionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RatePlanType
        {
            get
            {
                return this.ratePlanTypeField;
            }
            set
            {
                this.ratePlanTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RatePlanCode
        {
            get
            {
                return this.ratePlanCodeField;
            }
            set
            {
                this.ratePlanCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RatePlanID
        {
            get
            {
                return this.ratePlanIDField;
            }
            set
            {
                this.ratePlanIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool RatePlanQualifier
        {
            get
            {
                return this.ratePlanQualifierField;
            }
            set
            {
                this.ratePlanQualifierField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RatePlanQualifierSpecified
        {
            get
            {
                return this.ratePlanQualifierFieldSpecified;
            }
            set
            {
                this.ratePlanQualifierFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PromotionCode
        {
            get
            {
                return this.promotionCodeField;
            }
            set
            {
                this.promotionCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] PromotionVendorCode
        {
            get
            {
                return this.promotionVendorCodeField;
            }
            set
            {
                this.promotionVendorCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RatePlanCategory
        {
            get
            {
                return this.ratePlanCategoryField;
            }
            set
            {
                this.ratePlanCategoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RPH
        {
            get
            {
                return this.rPHField;
            }
            set
            {
                this.rPHField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public RatePlanCandidatesTypeRatePlanCandidatePrepaidQualifier PrepaidQualifier
        {
            get
            {
                return this.prepaidQualifierField;
            }
            set
            {
                this.prepaidQualifierField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrepaidQualifierSpecified
        {
            get
            {
                return this.prepaidQualifierFieldSpecified;
            }
            set
            {
                this.prepaidQualifierFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool AvailRatesOnlyInd
        {
            get
            {
                return this.availRatesOnlyIndField;
            }
            set
            {
                this.availRatesOnlyIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvailRatesOnlyIndSpecified
        {
            get
            {
                return this.availRatesOnlyIndFieldSpecified;
            }
            set
            {
                this.availRatesOnlyIndFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class RatePlanCandidatesTypeRatePlanCandidateHotelRef
    {

        private string chainCodeField;

        private string brandCodeField;

        private string hotelCodeField;

        private string hotelCityCodeField;

        private string hotelNameField;

        private string hotelCodeContextField;

        private string chainNameField;

        private string brandNameField;

        private string areaIDField;

        private string tTIcodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ChainCode
        {
            get
            {
                return this.chainCodeField;
            }
            set
            {
                this.chainCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string BrandCode
        {
            get
            {
                return this.brandCodeField;
            }
            set
            {
                this.brandCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string HotelCode
        {
            get
            {
                return this.hotelCodeField;
            }
            set
            {
                this.hotelCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string HotelCityCode
        {
            get
            {
                return this.hotelCityCodeField;
            }
            set
            {
                this.hotelCityCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string HotelName
        {
            get
            {
                return this.hotelNameField;
            }
            set
            {
                this.hotelNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string HotelCodeContext
        {
            get
            {
                return this.hotelCodeContextField;
            }
            set
            {
                this.hotelCodeContextField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ChainName
        {
            get
            {
                return this.chainNameField;
            }
            set
            {
                this.chainNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string BrandName
        {
            get
            {
                return this.brandNameField;
            }
            set
            {
                this.brandNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AreaID
        {
            get
            {
                return this.areaIDField;
            }
            set
            {
                this.areaIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "positiveInteger")]
        public string TTIcode
        {
            get
            {
                return this.tTIcodeField;
            }
            set
            {
                this.tTIcodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class RatePlanCandidatesTypeRatePlanCandidateMealsIncluded
    {

        private bool breakfastField;

        private bool breakfastFieldSpecified;

        private bool lunchField;

        private bool lunchFieldSpecified;

        private bool dinnerField;

        private bool dinnerFieldSpecified;

        private bool mealPlanIndicatorField;

        private bool mealPlanIndicatorFieldSpecified;

        private string[] mealPlanCodesField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Breakfast
        {
            get
            {
                return this.breakfastField;
            }
            set
            {
                this.breakfastField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BreakfastSpecified
        {
            get
            {
                return this.breakfastFieldSpecified;
            }
            set
            {
                this.breakfastFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Lunch
        {
            get
            {
                return this.lunchField;
            }
            set
            {
                this.lunchField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool LunchSpecified
        {
            get
            {
                return this.lunchFieldSpecified;
            }
            set
            {
                this.lunchFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Dinner
        {
            get
            {
                return this.dinnerField;
            }
            set
            {
                this.dinnerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DinnerSpecified
        {
            get
            {
                return this.dinnerFieldSpecified;
            }
            set
            {
                this.dinnerFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool MealPlanIndicator
        {
            get
            {
                return this.mealPlanIndicatorField;
            }
            set
            {
                this.mealPlanIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MealPlanIndicatorSpecified
        {
            get
            {
                return this.mealPlanIndicatorFieldSpecified;
            }
            set
            {
                this.mealPlanIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] MealPlanCodes
        {
            get
            {
                return this.mealPlanCodesField;
            }
            set
            {
                this.mealPlanCodesField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class RatePlanCandidatesTypeRatePlanCandidateArrivalPolicy
    {

        private bool guaranteePolicyIndicatorField;

        private bool guaranteePolicyIndicatorFieldSpecified;

        private bool depositPolicyIndicatorField;

        private bool depositPolicyIndicatorFieldSpecified;

        private bool holdTimePolicyIndicatorField;

        private bool holdTimePolicyIndicatorFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool GuaranteePolicyIndicator
        {
            get
            {
                return this.guaranteePolicyIndicatorField;
            }
            set
            {
                this.guaranteePolicyIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool GuaranteePolicyIndicatorSpecified
        {
            get
            {
                return this.guaranteePolicyIndicatorFieldSpecified;
            }
            set
            {
                this.guaranteePolicyIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool DepositPolicyIndicator
        {
            get
            {
                return this.depositPolicyIndicatorField;
            }
            set
            {
                this.depositPolicyIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DepositPolicyIndicatorSpecified
        {
            get
            {
                return this.depositPolicyIndicatorFieldSpecified;
            }
            set
            {
                this.depositPolicyIndicatorFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool HoldTimePolicyIndicator
        {
            get
            {
                return this.holdTimePolicyIndicatorField;
            }
            set
            {
                this.holdTimePolicyIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool HoldTimePolicyIndicatorSpecified
        {
            get
            {
                return this.holdTimePolicyIndicatorFieldSpecified;
            }
            set
            {
                this.holdTimePolicyIndicatorFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class RatePlanCandidatesTypeRatePlanCandidateRatePlanCommission
    {

        private decimal maxCommissionPercentageField;

        private bool maxCommissionPercentageFieldSpecified;

        private decimal minCommissionPercentageField;

        private bool minCommissionPercentageFieldSpecified;

        private bool commissionableIndicatorField;

        private bool commissionableIndicatorFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal MaxCommissionPercentage
        {
            get
            {
                return this.maxCommissionPercentageField;
            }
            set
            {
                this.maxCommissionPercentageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MaxCommissionPercentageSpecified
        {
            get
            {
                return this.maxCommissionPercentageFieldSpecified;
            }
            set
            {
                this.maxCommissionPercentageFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal MinCommissionPercentage
        {
            get
            {
                return this.minCommissionPercentageField;
            }
            set
            {
                this.minCommissionPercentageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MinCommissionPercentageSpecified
        {
            get
            {
                return this.minCommissionPercentageFieldSpecified;
            }
            set
            {
                this.minCommissionPercentageFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool CommissionableIndicator
        {
            get
            {
                return this.commissionableIndicatorField;
            }
            set
            {
                this.commissionableIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CommissionableIndicatorSpecified
        {
            get
            {
                return this.commissionableIndicatorFieldSpecified;
            }
            set
            {
                this.commissionableIndicatorFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class RoomStayCandidateType
    {

        private GuestCountType guestCountsField;

        private RoomAmenityPrefType[] roomAmenityField;

        private string roomTypeField;

        private string roomTypeCodeField;

        private string roomCategoryField;

        private string roomIDField;

        private string floorField;

        private string invBlockCodeField;

        private string promotionCodeField;

        private string[] promotionVendorCodeField;

        private string roomLocationCodeField;

        private string roomViewCodeField;

        private string[] bedTypeCodeField;

        private bool nonSmokingField;

        private bool nonSmokingFieldSpecified;

        private string configurationField;

        private string sizeMeasurementField;

        private string quantityField;

        private bool compositeField;

        private bool compositeFieldSpecified;

        private string roomClassificationCodeField;

        private string roomArchitectureCodeField;

        private RoomStayCandidateTypeRoomGender roomGenderField;

        private bool roomGenderFieldSpecified;

        private bool sharedRoomIndField;

        private bool sharedRoomIndFieldSpecified;

        private string rPHField;

        private string ratePlanCandidateRPHField;

        private string bookingCodeField;

        private System.DateTime effectiveDateField;

        private bool effectiveDateFieldSpecified;

        private System.DateTime expireDateField;

        private bool expireDateFieldSpecified;

        private bool expireDateExclusiveIndField;

        private bool expireDateExclusiveIndFieldSpecified;

        /// <remarks/>
        public GuestCountType GuestCounts
        {
            get
            {
                return this.guestCountsField;
            }
            set
            {
                this.guestCountsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("RoomAmenity")]
        public RoomAmenityPrefType[] RoomAmenity
        {
            get
            {
                return this.roomAmenityField;
            }
            set
            {
                this.roomAmenityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RoomType
        {
            get
            {
                return this.roomTypeField;
            }
            set
            {
                this.roomTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RoomTypeCode
        {
            get
            {
                return this.roomTypeCodeField;
            }
            set
            {
                this.roomTypeCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RoomCategory
        {
            get
            {
                return this.roomCategoryField;
            }
            set
            {
                this.roomCategoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RoomID
        {
            get
            {
                return this.roomIDField;
            }
            set
            {
                this.roomIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string Floor
        {
            get
            {
                return this.floorField;
            }
            set
            {
                this.floorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string InvBlockCode
        {
            get
            {
                return this.invBlockCodeField;
            }
            set
            {
                this.invBlockCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PromotionCode
        {
            get
            {
                return this.promotionCodeField;
            }
            set
            {
                this.promotionCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] PromotionVendorCode
        {
            get
            {
                return this.promotionVendorCodeField;
            }
            set
            {
                this.promotionVendorCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RoomLocationCode
        {
            get
            {
                return this.roomLocationCodeField;
            }
            set
            {
                this.roomLocationCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RoomViewCode
        {
            get
            {
                return this.roomViewCodeField;
            }
            set
            {
                this.roomViewCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] BedTypeCode
        {
            get
            {
                return this.bedTypeCodeField;
            }
            set
            {
                this.bedTypeCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool NonSmoking
        {
            get
            {
                return this.nonSmokingField;
            }
            set
            {
                this.nonSmokingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NonSmokingSpecified
        {
            get
            {
                return this.nonSmokingFieldSpecified;
            }
            set
            {
                this.nonSmokingFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Configuration
        {
            get
            {
                return this.configurationField;
            }
            set
            {
                this.configurationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SizeMeasurement
        {
            get
            {
                return this.sizeMeasurementField;
            }
            set
            {
                this.sizeMeasurementField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string Quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Composite
        {
            get
            {
                return this.compositeField;
            }
            set
            {
                this.compositeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CompositeSpecified
        {
            get
            {
                return this.compositeFieldSpecified;
            }
            set
            {
                this.compositeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RoomClassificationCode
        {
            get
            {
                return this.roomClassificationCodeField;
            }
            set
            {
                this.roomClassificationCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RoomArchitectureCode
        {
            get
            {
                return this.roomArchitectureCodeField;
            }
            set
            {
                this.roomArchitectureCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public RoomStayCandidateTypeRoomGender RoomGender
        {
            get
            {
                return this.roomGenderField;
            }
            set
            {
                this.roomGenderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RoomGenderSpecified
        {
            get
            {
                return this.roomGenderFieldSpecified;
            }
            set
            {
                this.roomGenderFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool SharedRoomInd
        {
            get
            {
                return this.sharedRoomIndField;
            }
            set
            {
                this.sharedRoomIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SharedRoomIndSpecified
        {
            get
            {
                return this.sharedRoomIndFieldSpecified;
            }
            set
            {
                this.sharedRoomIndFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RPH
        {
            get
            {
                return this.rPHField;
            }
            set
            {
                this.rPHField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RatePlanCandidateRPH
        {
            get
            {
                return this.ratePlanCandidateRPHField;
            }
            set
            {
                this.ratePlanCandidateRPHField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string BookingCode
        {
            get
            {
                return this.bookingCodeField;
            }
            set
            {
                this.bookingCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime EffectiveDate
        {
            get
            {
                return this.effectiveDateField;
            }
            set
            {
                this.effectiveDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EffectiveDateSpecified
        {
            get
            {
                return this.effectiveDateFieldSpecified;
            }
            set
            {
                this.effectiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime ExpireDate
        {
            get
            {
                return this.expireDateField;
            }
            set
            {
                this.expireDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExpireDateSpecified
        {
            get
            {
                return this.expireDateFieldSpecified;
            }
            set
            {
                this.expireDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool ExpireDateExclusiveInd
        {
            get
            {
                return this.expireDateExclusiveIndField;
            }
            set
            {
                this.expireDateExclusiveIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExpireDateExclusiveIndSpecified
        {
            get
            {
                return this.expireDateExclusiveIndFieldSpecified;
            }
            set
            {
                this.expireDateExclusiveIndFieldSpecified = value;
            }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class GuestRoomType
    {

        private GuestRoomTypeQuantities quantitiesField;

        private GuestRoomTypeOccupancy[] occupancyField;

        private GuestRoomTypeRoom roomField;

        private GuestRoomTypeAmenity[] amenitiesField;

        private GuestRoomTypeRoomLevelFees roomLevelFeesField;

        private AdditionalGuestAmountType[] additionalGuestAmountField;

        private ParagraphType descriptionField;

        /// <remarks/>
        public GuestRoomTypeQuantities Quantities
        {
            get
            {
                return this.quantitiesField;
            }
            set
            {
                this.quantitiesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Occupancy")]
        public GuestRoomTypeOccupancy[] Occupancy
        {
            get
            {
                return this.occupancyField;
            }
            set
            {
                this.occupancyField = value;
            }
        }

        /// <remarks/>
        public GuestRoomTypeRoom Room
        {
            get
            {
                return this.roomField;
            }
            set
            {
                this.roomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Amenity", IsNullable = false)]
        public GuestRoomTypeAmenity[] Amenities
        {
            get
            {
                return this.amenitiesField;
            }
            set
            {
                this.amenitiesField = value;
            }
        }

        /// <remarks/>
        public GuestRoomTypeRoomLevelFees RoomLevelFees
        {
            get
            {
                return this.roomLevelFeesField;
            }
            set
            {
                this.roomLevelFeesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AdditionalGuestAmount")]
        public AdditionalGuestAmountType[] AdditionalGuestAmount
        {
            get
            {
                return this.additionalGuestAmountField;
            }
            set
            {
                this.additionalGuestAmountField = value;
            }
        }

        /// <remarks/>
        public ParagraphType Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class GuestRoomTypeQuantities
    {

        private string maxRollawaysField;

        private string standardNumBedsField;

        private string maximumAdditionalGuestsField;

        private string minBillableGuestsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string MaxRollaways
        {
            get
            {
                return this.maxRollawaysField;
            }
            set
            {
                this.maxRollawaysField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string StandardNumBeds
        {
            get
            {
                return this.standardNumBedsField;
            }
            set
            {
                this.standardNumBedsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string MaximumAdditionalGuests
        {
            get
            {
                return this.maximumAdditionalGuestsField;
            }
            set
            {
                this.maximumAdditionalGuestsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string MinBillableGuests
        {
            get
            {
                return this.minBillableGuestsField;
            }
            set
            {
                this.minBillableGuestsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class GuestRoomTypeOccupancy
    {

        private string minOccupancyField;

        private string maxOccupancyField;

        private string ageQualifyingCodeField;

        private string minAgeField;

        private string maxAgeField;

        private TimeUnitType ageTimeUnitField;

        private bool ageTimeUnitFieldSpecified;

        private string ageBucketField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string MinOccupancy
        {
            get
            {
                return this.minOccupancyField;
            }
            set
            {
                this.minOccupancyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string MaxOccupancy
        {
            get
            {
                return this.maxOccupancyField;
            }
            set
            {
                this.maxOccupancyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AgeQualifyingCode
        {
            get
            {
                return this.ageQualifyingCodeField;
            }
            set
            {
                this.ageQualifyingCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string MinAge
        {
            get
            {
                return this.minAgeField;
            }
            set
            {
                this.minAgeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string MaxAge
        {
            get
            {
                return this.maxAgeField;
            }
            set
            {
                this.maxAgeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public TimeUnitType AgeTimeUnit
        {
            get
            {
                return this.ageTimeUnitField;
            }
            set
            {
                this.ageTimeUnitField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AgeTimeUnitSpecified
        {
            get
            {
                return this.ageTimeUnitFieldSpecified;
            }
            set
            {
                this.ageTimeUnitFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AgeBucket
        {
            get
            {
                return this.ageBucketField;
            }
            set
            {
                this.ageBucketField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class GuestRoomTypeRoom
    {

        private string roomTypeField;

        private string roomTypeCodeField;

        private string roomCategoryField;

        private string roomIDField;

        private string floorField;

        private string invBlockCodeField;

        private string promotionCodeField;

        private string[] promotionVendorCodeField;

        private string roomLocationCodeField;

        private string roomViewCodeField;

        private string[] bedTypeCodeField;

        private bool nonSmokingField;

        private bool nonSmokingFieldSpecified;

        private string configurationField;

        private string sizeMeasurementField;

        private string quantityField;

        private bool compositeField;

        private bool compositeFieldSpecified;

        private string roomClassificationCodeField;

        private string roomArchitectureCodeField;

        private RoomStayCandidateTypeRoomGender roomGenderField;

        private bool roomGenderFieldSpecified;

        private bool sharedRoomIndField;

        private bool sharedRoomIndFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RoomType
        {
            get
            {
                return this.roomTypeField;
            }
            set
            {
                this.roomTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RoomTypeCode
        {
            get
            {
                return this.roomTypeCodeField;
            }
            set
            {
                this.roomTypeCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RoomCategory
        {
            get
            {
                return this.roomCategoryField;
            }
            set
            {
                this.roomCategoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RoomID
        {
            get
            {
                return this.roomIDField;
            }
            set
            {
                this.roomIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string Floor
        {
            get
            {
                return this.floorField;
            }
            set
            {
                this.floorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string InvBlockCode
        {
            get
            {
                return this.invBlockCodeField;
            }
            set
            {
                this.invBlockCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PromotionCode
        {
            get
            {
                return this.promotionCodeField;
            }
            set
            {
                this.promotionCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] PromotionVendorCode
        {
            get
            {
                return this.promotionVendorCodeField;
            }
            set
            {
                this.promotionVendorCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RoomLocationCode
        {
            get
            {
                return this.roomLocationCodeField;
            }
            set
            {
                this.roomLocationCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RoomViewCode
        {
            get
            {
                return this.roomViewCodeField;
            }
            set
            {
                this.roomViewCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string[] BedTypeCode
        {
            get
            {
                return this.bedTypeCodeField;
            }
            set
            {
                this.bedTypeCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool NonSmoking
        {
            get
            {
                return this.nonSmokingField;
            }
            set
            {
                this.nonSmokingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NonSmokingSpecified
        {
            get
            {
                return this.nonSmokingFieldSpecified;
            }
            set
            {
                this.nonSmokingFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Configuration
        {
            get
            {
                return this.configurationField;
            }
            set
            {
                this.configurationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SizeMeasurement
        {
            get
            {
                return this.sizeMeasurementField;
            }
            set
            {
                this.sizeMeasurementField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string Quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool Composite
        {
            get
            {
                return this.compositeField;
            }
            set
            {
                this.compositeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CompositeSpecified
        {
            get
            {
                return this.compositeFieldSpecified;
            }
            set
            {
                this.compositeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RoomClassificationCode
        {
            get
            {
                return this.roomClassificationCodeField;
            }
            set
            {
                this.roomClassificationCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RoomArchitectureCode
        {
            get
            {
                return this.roomArchitectureCodeField;
            }
            set
            {
                this.roomArchitectureCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public RoomStayCandidateTypeRoomGender RoomGender
        {
            get
            {
                return this.roomGenderField;
            }
            set
            {
                this.roomGenderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RoomGenderSpecified
        {
            get
            {
                return this.roomGenderFieldSpecified;
            }
            set
            {
                this.roomGenderFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool SharedRoomInd
        {
            get
            {
                return this.sharedRoomIndField;
            }
            set
            {
                this.sharedRoomIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SharedRoomIndSpecified
        {
            get
            {
                return this.sharedRoomIndFieldSpecified;
            }
            set
            {
                this.sharedRoomIndFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class GuestRoomTypeAmenity
    {

        private string amenityCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AmenityCode
        {
            get
            {
                return this.amenityCodeField;
            }
            set
            {
                this.amenityCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class GuestRoomTypeRoomLevelFees : FeesType
    {

        private string codeField;

        private string codeContextField;

        private string quantityField;

        private string uRIField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CodeContext
        {
            get
            {
                return this.codeContextField;
            }
            set
            {
                this.codeContextField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "nonNegativeInteger")]
        public string Quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "anyURI")]
        public string URI
        {
            get
            {
                return this.uRIField;
            }
            set
            {
                this.uRIField = value;
            }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.opentravel.org/OTA/2003/05")]
    public partial class FeesType
    {

        private FeeType[] feeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Fee")]
        public FeeType[] Fee
        {
            get
            {
                return this.feeField;
            }
            set
            {
                this.feeField = value;
            }
        }
    }

}
