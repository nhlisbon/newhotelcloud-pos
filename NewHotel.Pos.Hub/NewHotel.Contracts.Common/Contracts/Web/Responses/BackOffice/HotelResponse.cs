﻿using System;

namespace NewHotel.Web.Services
{
    public class HotelResponse
    {
        public Guid Id;

        public string HotelCode;
        public string Description;
        public string ServiceName;
        public string ExternalCode;
        public string Country;

        #region License Info

        public string LicenseType;
        public string LicenseCode;
        public string ExpireDate;
        public string StartDate;
        public int DaysAlert;
        public int DaysSuspend;

        #endregion

        #region Hotel Contact

        public string ContactName;
        public string ContactLastName;
        public string ContactEmail;
        public string ContactPhone;
        public string ContactCountry;
        public string ContactLocation;
        public string ContactAddress;

        #endregion

        #region Statistics

        public DateTime? PmsNightAuditor;
        public long PmsGlobalBookings;
        public long PmsTotalBookings;
        public long PmsLastMonthBookings;
        public DateTime? PmsLastBookingDate;
        public long Rooms;

        #endregion
    }
}