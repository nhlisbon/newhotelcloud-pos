﻿using System;

namespace NewHotel.Web.Services
{
    public class NotificationMailResponse : BaseResponse
    {
        public string RequestID;
        public ErrorResponse[] Errors;
        public NotificationMailItem[] Emails;
    }

    public class NotificationMailItem
    {
        public string Hotel { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool AdminCreated { get; set; }
    }
}