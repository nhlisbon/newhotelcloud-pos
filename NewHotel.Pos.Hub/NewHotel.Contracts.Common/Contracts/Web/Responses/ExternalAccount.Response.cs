﻿using System;

namespace NewHotel.Web.Services
{
    public class ExternalAccountResponse : BaseResponse
    {
        public string AccountId;
    }
}