﻿using System;

namespace NewHotel.Web.Services
{
    public class NomenclatureCollectionResponse
    {
        public int ID;
        public KeyValueResponse<int>[] KeyValues;
    }
}