﻿using System;

namespace NewHotel.Web.Services
{
    public class QueryDataRowResponse<T>
    {
        public object[] Values;
        public QueryDataReponse<T>[] Children;

        public QueryDataRowResponse(params object[] values)
        {
            Values = values;
        }

        public QueryDataRowResponse() { }
    }
}
