﻿using System;

namespace NewHotel.Web.Services
{
    public class KeyValueResponse<T>
        where T : struct
    {
        public string Key;
		public string Code;
        public string Value;
        public string Comment;
        public NameValueResponse[] NameValues;
        public KeyValueIDResponse<T>[] Data;

        public KeyValueIDCollectionResponse<T>[] Children;
    }

    public class KeyValueIDResponse<T> : KeyValueResponse<T>
        where T : struct
    {
        public T ID;
    }

    public class KeyValueIDCollectionResponse<T>
        where T : struct
    {
        public T ID;
        public KeyValueResponse<T>[] Items;
    }

    public class NameValueResponse
    {
        public string Name;
        public object Value;
    }

    public class KeyValueResponse : KeyValueResponse<int>
    {
    }

    public class KeyValueIDResponse : KeyValueResponse<int>
    {
    }

    public class KeyValueIDCollectionResponse : KeyValueIDCollectionResponse<int>
    {
    }
}