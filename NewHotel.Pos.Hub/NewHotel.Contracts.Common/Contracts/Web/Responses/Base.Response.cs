﻿using System;

namespace NewHotel.Web.Services
{
    public class BaseResponse
    {
        public string RequestID;
        public ErrorResponse[] Errors;
    }
}