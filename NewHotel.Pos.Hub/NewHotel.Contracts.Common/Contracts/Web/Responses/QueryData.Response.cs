﻿using System;

namespace NewHotel.Web.Services
{
    public class QueryDataReponse<T> : BaseResponse
    {
        public T ID;
        public string[] Columns;
        public QueryDataRowResponse<T>[] Rows;
    }
}
