﻿using System;

namespace NewHotel.Web.Services
{
    public class ErrorResponse
    {
        public Error ID;
        public string Message;
    }
}