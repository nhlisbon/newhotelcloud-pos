﻿using System;

namespace NewHotel.Web.Services
{
    public class BaseStringResponse : BaseResponse
    {
        public string RequestID;
        public ErrorResponse[] Errors;
        public string Response;
    }
}