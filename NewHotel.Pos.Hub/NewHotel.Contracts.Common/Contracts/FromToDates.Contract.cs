﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(FromToDatesContract), "ValidateFromToDates")]
    public class FromToDatesContract : BaseContract
    {
        [DataMember]
        public DateTime? FromDate { get; set; }
        [DataMember]
        public DateTime? ToDate { get; set; }

        public FromToDatesContract() { }

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateFromToDates(FromToDatesContract obj)
        {
            if (!obj.FromDate.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid 'From' Date");
            if (!obj.ToDate.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid 'To' Date");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
    }

    [DataContract]
    [Serializable]
    public class NameContract : BaseContract
    {
        [DataMember]
        public string Description { get; set; }

        public NameContract() { }
    }
}
