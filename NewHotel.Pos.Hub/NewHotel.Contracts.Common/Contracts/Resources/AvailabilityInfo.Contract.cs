﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class AvailabilityInfoContract : BaseContract
    {
        #region Properties

        [DataMember]
        public DateTime AvailabilityDate { get; set; }
        [DataMember]
        public Guid ResourceTypeId { get; set; }
        [DataMember]
        public string ResourceTypeAbrev { get; set; }
        [DataMember]
        public long TotalRooms { get; set; }
        [DataMember]
        public long InactiveRooms { get; set; }
        [DataMember]
        public long OccupedRooms { get; set; }
        [DataMember]
        public long GuaranteedAllotmentRooms { get; set; }
        [DataMember]
        public ResourceOccupationControlType CriticalOcupationType { get; set; }
        [DataMember]
        public decimal CriticalOcupation { get; set; }
        [DataMember]
        public bool InsertionLock { get; set; }

        [ReflectionExclude]
        public string CriticalOcupationTypeTranslation { get; set; }

        [ReflectionExclude]
        public long ActiveRooms
        {
            get { return TotalRooms - InactiveRooms; }
        }

        [ReflectionExclude]
        public long AvailableRooms
        {
            get { return ActiveRooms - OccupedRooms; }
        }

        [ReflectionExclude]
        public decimal OccupationPercent
        {
            get
            {
                switch (CriticalOcupationType)
                {
                    case ResourceOccupationControlType.MaxReservation:
                        if (CriticalOcupationRooms.HasValue && CriticalOcupationRooms.Value > 0)
                            return Math.Round(OccupedRooms * 100.0M / CriticalOcupationRooms.Value, 2);
                        break;
                    case ResourceOccupationControlType.CriticalOccupation:
                        if (ActiveRooms > 0)
                            return Math.Round(OccupedRooms * 100.0M / ActiveRooms, 2);
                        else if (TotalRooms > 0)
                            return Math.Round(OccupedRooms * 100.0M / TotalRooms, 2);
                        break;
                }

                return 100M;
            }
        }

        [ReflectionExclude]
        public long? CriticalOcupationRooms
        {
            get
            {
                if (CriticalOcupationType == ResourceOccupationControlType.MaxReservation)
                    return decimal.ToInt64(CriticalOcupation);

                return null;
            }
        }

        [ReflectionExclude]
        public decimal? CriticalOcupationPercent
        {
            get
            {
                if (CriticalOcupationType == ResourceOccupationControlType.CriticalOccupation)
                    return CriticalOcupation;

                return null;
            }
        }

        [ReflectionExclude]
        public bool HasCriticalOccupation
        {
            get
            {
                if (ActiveRooms == 0)
                    return true;
                else
                {
                    switch (CriticalOcupationType)
                    {
                        case ResourceOccupationControlType.CriticalOccupation:
                            if (OccupationPercent > CriticalOcupationPercent)
                                return true;
                            break;
                        case ResourceOccupationControlType.MaxReservation:
                            if (OccupedRooms > CriticalOcupationRooms)
                                return true;
                            break;
                    }
                }

                return false;
            }
        }

        #endregion
        #region Constructors

        public AvailabilityInfoContract()
            : base()
        {
            ResourceTypeAbrev = string.Empty;
            CriticalOcupationType = ResourceOccupationControlType.CriticalOccupation;
        }

        #endregion
    }
}