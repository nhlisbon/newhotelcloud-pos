﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class DiscountTypeGroupContract : BaseContract
    {
        #region Members
        [DataMember]
        internal LanguageTranslationContract _description;
        #endregion

        #region Contract properties
        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }
        #endregion

        #region Constructor
        public DiscountTypeGroupContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }
        #endregion
    }
}
