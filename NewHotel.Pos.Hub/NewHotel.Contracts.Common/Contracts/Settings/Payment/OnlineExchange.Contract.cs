﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class OnlineExchangeContract : BaseContract
    {
        #region Members

        private string _currencyId;
        [DataMember]
        public string CurrencyId { get { return _currencyId; } set { Set(ref _currencyId, value, "CurrencyId"); } }

        private DateTime? _currentDate;
        [DataMember]
        public DateTime? CurrentDate { get { return _currentDate; } set { Set(ref _currentDate, value, "CurrentDate"); } }

        private DateTime? _applyFrom;
        [DataMember]
        public DateTime? ApplyFrom { get { return _applyFrom; } set { Set(ref _applyFrom, value, "ApplyFrom"); } }

        private DateTime? _applyTo;
        [DataMember]
        public DateTime? ApplyTo { get { return _applyTo; } set { Set(ref _applyTo, value, "ApplyTo"); } }

        private List<string> _currencies;
        [DataMember]
        public List<string> Currencies { get { return _currencies; } set { Set(ref _currencies, value, "Currencies"); } }

        [DataMember]
        public bool LiveData { get; set; } = true;
        [DataMember]
        public bool HistoricalData { get; set; } = false;
        [DataMember]
        public List<OnlineExchangeRecord> OnlineRates { get; set; }

        #endregion
        #region Properties


        #endregion
        #region Constructor

        public OnlineExchangeContract()
            : base()
        {
            Currencies = new List<string>();
            OnlineRates = new List<OnlineExchangeRecord>();
        }

        #endregion
    }
}
