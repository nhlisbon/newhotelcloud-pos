﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(PaymentFormContract), "ValidadePaymentForm")]
    public class PaymentFormContract : BaseContract
    {
        #region Members

        [DataMember]
        internal LanguageTranslationContract _abbreviation;
        [DataMember]
        internal LanguageTranslationContract _description;

        private string _currency;
        private bool _creditCard;
        private bool _paymentGatewayExcluded;
        private bool _inactive;
        private bool _cash;
        private bool _affectBank;
        private bool _cashFlowReportInclude;
        private short _order;
        private decimal? _crmOnePointValue;
        private bool _fiscalPrinted;
        private string _tenderValue;
        private string _evaluatedValue;
        private bool _isPayAndGoPayment;
        private string _AuxiliarCode;
        private bool _allowOnlyGreatherThanZero;
        private bool _allowCreditCard;
        private bool _allowDebitCard;
        private bool _mandatoryNSUAuth;
        private bool _externalInvoice;

        #endregion
        #region Properties

        [DataMember]
        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Abbreviation required.")]
        public LanguageTranslationContract Abbreviation
        {
            get { return _abbreviation; }
            set { value = _abbreviation; }
        }
        [DataMember]
        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { value = _description; }
        }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Currency required.")]
        public string Unmo { get { return _currency; } set { Set(ref _currency, value, "Unmo"); } }
        [DataMember(Order = 1)]
        public bool CreditCard
        {
            get { return _creditCard; }
            set
            {
                if (Set(ref _creditCard, value, "CreditCard"))
                {
                    if (!_creditCard)
                        PaymentGatewayExcluded = false;
                }
            }
        }
        [DataMember(Order = 0)]
        public bool PaymentGatewayExcluded { get { return _paymentGatewayExcluded; } set { Set(ref _paymentGatewayExcluded, value, "PaymentGatewayExcluded"); } }
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }
        [DataMember]
        public bool AffectBank { get { return _affectBank; } set { Set(ref _affectBank, value, "AffectBank"); } }
        [DataMember]
        public bool CashFlowReportInclude { get { return _cashFlowReportInclude; } set { Set(ref _cashFlowReportInclude, value, "CashFlowReportInclude"); } }
        [DataMember]
        public short Order { get { return _order; } set { Set(ref _order, value, "Order"); } }
        [DataMember]
        public decimal? CrmOnePointValue { get { return _crmOnePointValue; } set { Set(ref _crmOnePointValue, value, "CrmOnePointValue"); } }
        [DataMember]
        public bool FiscalPrinted { get { return _fiscalPrinted; } set { Set(ref _fiscalPrinted, value, "FiscalPrinted"); } }
        [DataMember]
        public string TenderValue { get { return _tenderValue; } set { Set(ref _tenderValue, value, "TenderValue"); } }
        [DataMember]
        public string EvaluatedValue { get { return _evaluatedValue; } set { Set(ref _evaluatedValue, value, "EvaluatedValue"); } }
        [DataMember]
        public bool IsPayAndGoPayment { get { return _isPayAndGoPayment; } set { Set(ref _isPayAndGoPayment, value, "IsPayAndGoPayment"); } }
        [DataMember]
        public string AuxiliarCode { get { return _AuxiliarCode; } set { Set(ref _AuxiliarCode, value, "AuxiliarCode"); } }
        [DataMember]
        public bool AllowOnlyGreatherThanZero { get { return _allowOnlyGreatherThanZero; } set { Set(ref _allowOnlyGreatherThanZero, value, "AllowOnlyGreatherThanZero"); } }
        [DataMember]
        public bool Cash
        {
            get { return _cash; }
            set
            {
                if (Set(ref _cash, value, "Cash"))
                {
                    if (_cash)
                    {
                        _allowCreditCard = false;
                        _allowDebitCard = false;
                        NotifyPropertyChanged("AllowCreditCard", "AllowDebitCard");
                        CreditCard = _allowCreditCard || _allowDebitCard;
                    }
                }
            }
        }
        [DataMember]
        public bool AllowCreditCard
        {
            get { return _allowCreditCard; }
            set
            {
                if (Set(ref _allowCreditCard, value, "AllowCreditCard"))
                {
                    CreditCard = _allowCreditCard || _allowDebitCard;
                    if (CreditCard)
                    {
                        Cash = false;
                        MandatoryNSUAuth = false;
                    }
                }
            }
        }
        [DataMember]
        public bool AllowDebitCard
        {
            get { return _allowDebitCard; }
            set
            {
                if (Set(ref _allowDebitCard, value, "AllowDebitCard"))
                {
                    CreditCard = _allowCreditCard || _allowDebitCard;
                    if (CreditCard)
                    {
                        Cash = false;
                        MandatoryNSUAuth = false;
                    }
                }
            }
        }
        [DataMember]
        public bool MandatoryNSUAuth { get { return _mandatoryNSUAuth; } set { Set(ref _mandatoryNSUAuth, value, "MandatoryNSUAuth"); } }
        [DataMember]
        public bool ExternalInvoice { get { return _externalInvoice; } set { Set(ref _externalInvoice, value, "ExternalInvoice"); } }

        #endregion
        #region Lists

        public TypedList<ApplicationContract> Applications { get; set; }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public long[] ApplicationIds { get { return Applications.Select(x => x.ApplicationId).ToArray(); } }

        #endregion
        #region Constructor

        public PaymentFormContract()
            : base()
        {
            _abbreviation = new LanguageTranslationContract();
            _description = new LanguageTranslationContract();
            Applications = new TypedList<ApplicationContract>();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidadePaymentForm(PaymentFormContract obj)
        {
            if (obj.CreditCard && !obj.AllowCreditCard && !obj.AllowDebitCard)
                return new System.ComponentModel.DataAnnotations.ValidationResult("At least one option (Debit or Credit) needs to be checked");
            
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}