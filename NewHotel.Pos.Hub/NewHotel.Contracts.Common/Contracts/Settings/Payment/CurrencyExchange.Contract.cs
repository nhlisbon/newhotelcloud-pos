﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class CurrencyExchangeContract : BaseContract
    {
        #region Members

        private string _currencyId;
        private DateTime _date;
        private DateTime _toDate;
        private decimal _cashExchange;
        private decimal _invoiceExchange;
        private decimal _accountingExchange;
        private Guid _currencyHotelId;
        private decimal? _onlineExchange;

        #endregion
        #region Properties

		[DataMember]
		public DateTime WorkDate { get; internal set; }
        [DataMember]
        public string CurrencyId { get { return _currencyId; } set { Set(ref _currencyId, value, "CurrencyId"); } }
        [DataMember]
        public DateTime Date { get { return _date; } set { Set(ref _date, value, "Date"); } }
        [DataMember]
        public DateTime ToDate { get { return _toDate; } set { Set(ref _toDate, value, "ToDate"); } }
        [DataMember]
        public decimal CashExchange { get { return _cashExchange; } set { Set(ref _cashExchange, value, "CashExchange"); } }
        [DataMember]
        public decimal InvoiceExchange { get { return _invoiceExchange; } set { Set(ref _invoiceExchange, value, "InvoiceExchange"); } }
        [DataMember]
        public decimal AccountingExchange { get { return _accountingExchange; } set { Set(ref _accountingExchange, value, "AccountingExchange"); } }
        [DataMember]
        public decimal? OnlineExchange { get { return _onlineExchange; } set { Set(ref _onlineExchange, value, "OnlineExchange"); } }
        [DataMember]
        public Guid CurrencyHotelId { get { return _currencyHotelId; } set { Set(ref _currencyHotelId, value, "CurrencyHotelId"); } }

        #endregion
        #region Constructor

        public CurrencyExchangeContract(DateTime workDate)
            : base()
        {
			WorkDate = workDate;
		}

        #endregion
    }
}