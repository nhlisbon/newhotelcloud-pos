﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class PaymentGatewaySettingsContract : BaseContract
    {
        #region Private Methods

        private static void AddPaymentProviderConfig(IList<NameValueContract> configNames, string id, string name)
        {
            configNames.Add(new NameValueContract(id, name));
        }

        #endregion
        #region Public Methods

        public IEnumerable<NameValueContract> GetPaymentPrividerConfigs(PaymentServiceProvider paymentProvider)
        {
            var configNames = new List<NameValueContract>();
            switch (paymentProvider)
            {
                case PaymentServiceProvider.Unicre:
                    AddPaymentProviderConfig(configNames, "MERCHANT_ID", "Merchant ID");
                    AddPaymentProviderConfig(configNames, "ACCESS_KEY", "Access key");
                    AddPaymentProviderConfig(configNames, "CONTRACT_NUMBER", "Contract number");
                    break;
                case PaymentServiceProvider.Shift4:
                    AddPaymentProviderConfig(configNames, "MerchantID", "Merchant ID");
                    AddPaymentProviderConfig(configNames, "API_PASSWORD", "API password");
                    AddPaymentProviderConfig(configNames, "API_SERIAL_NUMBER", "API serial number");
                    AddPaymentProviderConfig(configNames, "UTG_URL", "UTG url");
                    AddPaymentProviderConfig(configNames, "INST_ZIP", "Installation zip code");
                    AddPaymentProviderConfig(configNames, "POS_WIDTH", "POS receipt cols");
                    AddPaymentProviderConfig(configNames, "S4_AVS", "S4 AVS");
                    AddPaymentProviderConfig(configNames, "SITE_URL", "Site url");
                    AddPaymentProviderConfig(configNames, "ACCOUNT_ID", "Account ID");
                    AddPaymentProviderConfig(configNames, "SITE_ID", "Site ID");
                    break;
                case PaymentServiceProvider.AuthorizeDotNet:
                    AddPaymentProviderConfig(configNames, "API_LOGIN_ID", "API login ID");
                    AddPaymentProviderConfig(configNames, "TRANSACTION_KEY", "Transaction key");
                    break;
                case PaymentServiceProvider.SuperPay:
                    AddPaymentProviderConfig(configNames, "BUSINESS_ID", "Business ID");
                    AddPaymentProviderConfig(configNames, "WS_USER", "User");
                    AddPaymentProviderConfig(configNames, "WS_PASSWORD", "Password");
                    AddPaymentProviderConfig(configNames, "WS_URL", "Url");
                    AddPaymentProviderConfig(configNames, "TECNOLOGY_TYPE", "Technology Type");
                    break;
                case PaymentServiceProvider.Six:
                    AddPaymentProviderConfig(configNames, "SIX_SYS_URL", "URL");
                    break;
                case PaymentServiceProvider.SiTef:
                    AddPaymentProviderConfig(configNames, "GLOBAL_OPER_TIMEOUT", "TimeOut (seg)");
                    break;
                case PaymentServiceProvider.Braspag:
                    AddPaymentProviderConfig(configNames, "MERCHANT_ID", "Merchant ID");
                    AddPaymentProviderConfig(configNames, "MERCHANT_KEY", "Access key");
                    AddPaymentProviderConfig(configNames, "BRASPAG_URL", "Url");
                    AddPaymentProviderConfig(configNames, "PAYMENT_PROVIDER", "Payment Provider");
                    break;
            }

            return configNames;
        }

        #endregion
        #region Constructor

        public PaymentGatewaySettingsContract() { }

        #endregion
    }
}