﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class CurrencyInstallationContract : BaseContract
    {
        #region Members

        private string _currencyId;
        private bool _usedInCashFlow;
        private bool _usedInRefunds;
        private short? _getDecimalsInPercents;
        private short? _setDecimalsInPercents;
        private short? _getDecimalsInPrices;
        private short? _setDecimalsInPrices;
        private short? _getDecimalsInExchange;
        private short? _setDecimalsInExchange;
        private bool _multOperatorForExchange;
        private string _currencySymbol;
        private Guid _installationId;
        private decimal? _localCondominiumExchange;

        #endregion
        #region Properties

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Currency required.")]
        public string CurrencyId { get { return _currencyId; } set { Set(ref _currencyId, value, "CurrencyId"); } }
        [DataMember]
        public bool UsedInCashFlow { get { return _usedInCashFlow; } set { Set(ref _usedInCashFlow, value, "UsedInCashFlow"); } }
        [DataMember]
        public bool UsedInRefunds { get { return _usedInRefunds; } set { Set(ref _usedInRefunds, value, "UsedInRefunds"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Decimal in percent required.")]
        public short? GetDecimalsInPercents { get { return _getDecimalsInPercents; } set { Set(ref _getDecimalsInPercents, value, "GetDecimalsInPercents"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Decimal in percent required.")]
        public short? SetDecimalsInPercents { get { return _setDecimalsInPercents; } set { Set(ref _setDecimalsInPercents, value, "SetDecimalsInPercents"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Decimal in price required.")]
        public short? GetDecimalsInPrices { get { return _getDecimalsInPrices; } set { Set(ref _getDecimalsInPrices, value, "GetDecimalsInPrices"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Decimal in price required.")]
        public short? SetDecimalsInPrices { get { return _setDecimalsInPrices; } set { Set(ref _setDecimalsInPrices, value, "SetDecimalsInPrices"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Decimal in exchange required.")]
        public short? GetDecimalsInExchange { get { return _getDecimalsInExchange; } set { Set(ref _getDecimalsInExchange, value, "GetDecimalsInExchange"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Decimal in exchange required.")]
        public short? SetDecimalsInExchange { get { return _setDecimalsInExchange; } set { Set(ref _setDecimalsInExchange, value, "SetDecimalsInExchange"); } }
        [DataMember]
        public bool MultOperatorForExchange { get { return _multOperatorForExchange; } set { Set(ref _multOperatorForExchange, value, "MultOperatorForExchange"); } }
        [DataMember]
        public string CurrencySymbol { get { return _currencySymbol; } set { Set(ref _currencySymbol, value, "CurrencySymbol"); } }
        [DataMember]
        public Guid InstallationId { get { return _installationId; } set { Set(ref _installationId, value, "InstallationId"); } }
        [DataMember]
        public decimal? LocalCondominiumExchange { get { return _localCondominiumExchange; } set { Set(ref _localCondominiumExchange, value, "LocalCondominiumExchange"); } }
        [DataMember]
        public string TextColumn { get; set; }

        #endregion
        #region Constructor

        public CurrencyInstallationContract()
            : base() { }

        #endregion
    }
}