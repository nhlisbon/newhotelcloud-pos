﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(CreditCardTypeContract), "ValidateCreditCardType")]
    public class CreditCardTypeContract : BaseContract
    {
        #region Members

        private long? _validationTypeId;
        private decimal? _commission;
        private short? _paymentdays;
        private bool _isCreditCard;
        private bool _isDebitCard;
        private string _PaymentGatewayCode;
        private Guid? _accountId;
        private string _accountDescription;
        private bool _inactive;
        private bool _externalInvoice;

        private long? _installments;
        private decimal? _installment01;
        private decimal? _installment02;
        private decimal? _installment03;
        private decimal? _installment04;
        private decimal? _installment05;
        private decimal? _installment06;
        private decimal? _installment07;
        private decimal? _installment08;
        private decimal? _installment09;
        private decimal? _installment10;
        private decimal? _installment11;
        private decimal? _installment12;

        [DataMember]
        internal LanguageTranslationContract _description;

        #endregion
        #region Constructor

        public CreditCardTypeContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion        
        #region Persistent Properties

        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        [ReflectionExclude]
        public LanguageTranslationContract Description { get { return _description; } set { value = _description; } }

        [DataMember]
        public long? ValidationTypeId { get { return _validationTypeId; } set { Set(ref _validationTypeId, value, "ValidationTypeId"); } }
        [DataMember]
        public decimal? Commission { get { return _commission; } set { Set(ref _commission, value, "Commission"); } }
        [DataMember]
        public short? PaymentDays { get { return _paymentdays; } set { Set(ref _paymentdays, value, "PaymentDays"); } }
        [DataMember]
        public bool IsCreditCard { get { return _isCreditCard; } set { Set(ref _isCreditCard, value, "IsCreditCard"); } }
        [DataMember]
        public bool IsDebitCard { get { return _isDebitCard; } set { Set(ref _isDebitCard, value, "IsDebitCard"); } }
        [DataMember]
        public string PaymentGatewayCode { get { return _PaymentGatewayCode; } set { Set(ref _PaymentGatewayCode, value, "PaymentGatewayCode"); } }
        [DataMember]
        public Guid? AccountId { get { return _accountId; } set { Set(ref _accountId, value, "AccountId"); } }
        [DataMember]
        public string AccountDescription { get { return _accountDescription; } set { Set(ref _accountDescription, value, "AccountDescription"); } }
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }
        [DataMember]
        public bool ExternalInvoice { get { return _externalInvoice; } set { Set(ref _externalInvoice, value, "ExternalInvoice"); } }

        [DataMember]
        public long? Installments { get { return _installments; } set { Set(ref _installments, value, "Installments"); } }
        [DataMember]
        public decimal? Installment01 { get { return _installment01; } set { Set(ref _installment01, value, "Installment01"); } }
        [DataMember]
        public decimal? Installment02 { get { return _installment02; } set { Set(ref _installment02, value, "Installment02"); } }
        [DataMember]
        public decimal? Installment03 { get { return _installment03; } set { Set(ref _installment03, value, "Installment03"); } }
        [DataMember]
        public decimal? Installment04 { get { return _installment04; } set { Set(ref _installment04, value, "Installment04"); } }
        [DataMember]
        public decimal? Installment05 { get { return _installment05; } set { Set(ref _installment05, value, "Installment05"); } }
        [DataMember]
        public decimal? Installment06 { get { return _installment06; } set { Set(ref _installment06, value, "Installment06"); } }
        [DataMember]
        public decimal? Installment07 { get { return _installment07; } set { Set(ref _installment07, value, "Installment07"); } }
        [DataMember]
        public decimal? Installment08 { get { return _installment08; } set { Set(ref _installment08, value, "Installment08"); } }
        [DataMember]
        public decimal? Installment09 { get { return _installment09; } set { Set(ref _installment09, value, "Installment09"); } }
        [DataMember]
        public decimal? Installment10 { get { return _installment10; } set { Set(ref _installment10, value, "Installment10"); } }
        [DataMember]
        public decimal? Installment11 { get { return _installment11; } set { Set(ref _installment11, value, "Installment11"); } }
        [DataMember]
        public decimal? Installment12 { get { return _installment12; } set { Set(ref _installment12, value, "Installment12"); } }
        
        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateCreditCardType(CreditCardTypeContract obj)
        {
            if (!obj.IsCreditCard && !obj.IsDebitCard)
                return new System.ComponentModel.DataAnnotations.ValidationResult("At least one option (Debit, Credit) needs to be checked");
            if (obj.Installments > 0 && !obj.AccountId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Account cannot be empty with installments defined");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}