﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class PaymentAuthContract : BaseContract
    {
        #region Members

        private Guid _currentAccountId;
        private Guid _creditCardId;
        private decimal _value;
        private DateTime _workDate;
        private PaymentServiceProvider _paymentProvider;
        private Guid? _paymentIdentifier;
        private Guid _userId;
        private string _currencyId;
        private bool _used;
        private bool _void;
        private Guid _paymentFormId;
        private Guid? _withdrawTypeId;
        private CreditCardContract _creditCard;

        #endregion
        #region Properties

        [DataMember]
        public Guid CurrentAccountId { get { return _currentAccountId; } set { Set(ref _currentAccountId, value, "CurrentAccountId"); } }
        [DataMember]
        public Guid CreditCardId { get { return _creditCardId; } set { Set(ref _creditCardId, value, "CreditCardId"); } }
        [DataMember]
        public decimal Value { get { return _value; } set { Set(ref _value, value, "Value"); } }
        [DataMember]
        public DateTime WorkDate { get { return _workDate; } set { Set(ref _workDate, value, "WorkDate"); } }
        [DataMember]
        public PaymentServiceProvider PaymentProvider { get { return _paymentProvider; } set { Set(ref _paymentProvider, value, "PaymentProvider"); } }
        [DataMember]
        public Guid? PaymentIdentifier { get { return _paymentIdentifier; } set { Set(ref _paymentIdentifier, value, "PaymentIdentifier"); } }
        [DataMember]
        public Guid UserId { get { return _userId; } set { Set(ref _userId, value, "UserId"); } }
        [DataMember]
        public string CurrencyId { get { return _currencyId; } set { Set(ref _currencyId, value, "CurrencyId"); } }
        [DataMember]
        public bool Used { get { return _used; } set { Set(ref _used, value, "Used"); } }
        [DataMember]
        public bool Void { get { return _void; } set { Set(ref _void, value, "Void"); } }
        [DataMember]
        public Guid PaymentFormId { get { return _paymentFormId; } set { Set(ref _paymentFormId, value, "PaymentFormId"); } }
        [DataMember]
        public Guid? WithdrawTypeId { get { return _withdrawTypeId; } set { Set(ref _withdrawTypeId, value, "WithdrawTypeId"); } }

        #endregion
        #region Visual Properties

        [DataMember]
        public PaymentServiceProvider SettingsPaymentProvider { get; set; }
        [DataMember]
        public CreditCardContract CreditCard
        {
            get { return _creditCard; }
            set
            {
                if (value != _creditCard)
                {
                    _creditCard = value;
                    NotifyPropertyChanged("CreditCardDescription");
                }
            }
        }
        [DataMember]
        public string UserDescription { get; set; }

        [ReflectionExclude]
        public string CreditCardDescription
        {
            get { return _creditCard != null ? CreditCardContract.FormatCreditCard(_creditCard.IdentNumber) : string.Empty;  }
        }

        #endregion
        #region Constructor

        public PaymentAuthContract()
            : base()
        {
            SettingsPaymentProvider = PaymentServiceProvider.Manual;
        }

        #endregion
    }
}