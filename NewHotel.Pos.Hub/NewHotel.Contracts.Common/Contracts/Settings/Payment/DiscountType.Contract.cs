﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class DiscountTypeContract : BaseContract
    {
        #region Members

        [DataMember]
        internal LanguageTranslationContract _description;

        private long? _application;
        private DiscountApplyOver _appliedOver;
        private DiscountApplyTo _appliedTo;
        private bool _fixed;
        private decimal? _minValue;
        private decimal? _maxValue;
        private Guid? _discountTypeGroupId;
        private string _freeCode;

        #endregion
        #region Properties
        [LanguageTranslationValidation(ErrorMessage = "Description required.")]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        public long? Application { get { return _application; } set { Set(ref _application, value, "Application"); } }
        [DataMember]
        public DiscountApplyOver AppliedOver { get { return _appliedOver; } set { Set(ref _appliedOver, value, "AppliedOver"); } }
        [DataMember]
        public DiscountApplyTo AppliedTo { get { return _appliedTo; } set { Set(ref _appliedTo, value, "AppliedTo"); } }
        [DataMember]
        public bool Fixed { get { return _fixed; } set { Set(ref _fixed, value, "Fixed"); } }
        [DataMember]
        public decimal? MinValue { get { return _minValue; } set { Set(ref _minValue, value, "MinValue"); } }
        [DataMember]
        public decimal? MaxValue { get { return _maxValue; } set { Set(ref _maxValue, value, "MaxValue"); } }
        [DataMember]
        public Guid? DiscountTypeGroupId { get { return _discountTypeGroupId; } set { Set(ref _discountTypeGroupId, value, "DiscountTypeGroupId"); } }
        [DataMember]
        public string FreeCode { get { return _freeCode; } set { Set(ref _freeCode, value, "FreeCode"); } }
        
        #endregion
        #region Constructor

        public DiscountTypeContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
    }
}