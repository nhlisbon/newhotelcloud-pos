﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ZoneTypeContract : BaseContract
    {
        #region Contract properties
        [DataMember]
        internal LanguageTranslationContract _description;

        [DataMember]
        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        [ReflectionExclude()]
        public LanguageTranslationContract DescriptionId
        {
            get { return _description; }
            set { _description = value; }
        }

        #endregion

        #region Constructor

        public ZoneTypeContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
    }
}