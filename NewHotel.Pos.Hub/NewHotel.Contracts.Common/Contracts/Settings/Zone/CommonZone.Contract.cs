﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class CommonZoneContract : ZoneContract
    {
        public CommonZoneContract()
            : base() 
        {
            AreaUnit = MeasurementArea.SqMeters;
        }

        private decimal? _area;
        private MeasurementArea _areaUnit;
        private Guid? _cleaningZoneId;

        [DataMember]
        public MeasurementArea AreaUnit { get { return _areaUnit; } set { Set(ref _areaUnit, value, "AreaUnit"); } }
        [DataMember]
        public decimal? Area { get { return _area; } set { Set(ref _area, value, "Area"); } }
        [DataMember]
        public Guid? CleaningZoneId { get { return _cleaningZoneId; } set { Set(ref _cleaningZoneId, value, "CleaningZoneId"); } }
    }
}