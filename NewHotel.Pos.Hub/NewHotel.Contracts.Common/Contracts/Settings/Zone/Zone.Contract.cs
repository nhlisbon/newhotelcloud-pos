﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [KnownType(typeof(CommonZoneContract))]
    [KnownType(typeof(BlockContract))]
    public class ZoneContract : BaseContract
    {
        private Guid? _zoneTypeId;

        [DataMember]
        internal LanguageTranslationContract _description;

        [DataMember]
        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Zone type required.")]
        [Display(Name = "ZoneTypeId")]
        public Guid? ZoneTypeId
        {
            get { return _zoneTypeId; }
            set { Set(ref _zoneTypeId, value, "ZoneTypeId"); }
        }

        public ZoneContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }
    }
}
