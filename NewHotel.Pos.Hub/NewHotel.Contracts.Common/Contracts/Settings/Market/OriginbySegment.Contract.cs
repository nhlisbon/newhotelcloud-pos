﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public class OriginbySegmentContract : BaseContract
    {
      #region Contract properties

        public Guid OriginId { get; set; }
        public string Description { get; set; }

        #endregion

        public OriginbySegmentContract(Guid id, string description) 
            : base()
        {
            OriginId = id;
            Description = description;
        }
    }
}
