﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SegmentbyOriginContract : BaseContract
    {
        #region Properties

        [DataMember]
        public Guid OriginId { get; set; }
        [DataMember]
        public Guid SegmentId { get; set; }
        [DataMember]
        public string Origin { get; set; }
        [DataMember]
        public string Segment { get; set; }

        #endregion
        #region Constructors

        public SegmentbyOriginContract()
            : base() { }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class SegmentOriginRelationContract : BaseContract
    {
        #region Members

        private Guid _segmentOrOriginId;
        private string _description;

        #endregion

        #region Properties

        [DataMember]
        public Guid SegmentOrOriginId { get { return _segmentOrOriginId; } set { Set(ref _segmentOrOriginId, value, "SegmentOrOriginId"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }

        #endregion

        #region Constructors

        public SegmentOriginRelationContract() : base() { }

        public SegmentOriginRelationContract(Guid id, Guid segmentOrOriginId, string description) 
            : base()
        {
            Id = id;
            SegmentOrOriginId = segmentOrOriginId;
            Description = description;
        }

        #endregion
    }
}
