﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(OriginContract), "ValidateOrigin")]
    public class OriginContract : BaseContract
    {
        #region Members

        private Guid _marketOriginGrouping;
        internal TypedList<SegmentOriginRelationContract> _segments;

        #endregion
        #region Contract properties

        [DataMember]
        internal LanguageTranslationContract _description;

        [Display(Name = "Description")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        public Guid MarketOriginGrouping { get { return _marketOriginGrouping; } set { Set(ref _marketOriginGrouping, value, "MarketOriginGrouping"); } }

        [ReflectionExclude]
        public TypedList<SegmentOriginRelationContract> Segments { get { return _segments; } set { _segments = value; } }

        #endregion
        #region Constructor

        public OriginContract()
            : base()
        {
            _description = new LanguageTranslationContract();
            _segments = new TypedList<SegmentOriginRelationContract>();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateOrigin(OriginContract obj)
        {
            if (obj.MarketOriginGrouping == Guid.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Group required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
