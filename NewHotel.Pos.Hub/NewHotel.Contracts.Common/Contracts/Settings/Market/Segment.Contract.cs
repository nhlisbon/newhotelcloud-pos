﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(SegmentContract), "ValidateSegment")]
    public class SegmentContract : BaseContract
    {
        #region Members

        private Guid _marketGrouping;
        long? _applicationId;
        internal TypedList<SegmentOriginRelationContract> _origins;


        #endregion
        #region Contract Properties

        [DataMember]
        internal LanguageTranslationContract _description;
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public string DescriptionTranslated { get; set; }

        [DataMember]
        public long? ApplicationId
        {
            get { return _applicationId; }
            set { Set(ref _applicationId, value, "ApplicationId"); }
        }

        [DataMember]
        public Guid MarketGrouping { get { return _marketGrouping; } set { Set<Guid>(ref _marketGrouping, value, "MarketGrouping"); } }
        
        [ReflectionExclude]
        public TypedList<SegmentOriginRelationContract> Origins { get { return _origins; } set { _origins = value; } }

        #endregion

        #region Constructor
        public SegmentContract()
                : base()
        {
            _description = new LanguageTranslationContract();
            _origins = new TypedList<SegmentOriginRelationContract>();
        }
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateSegment(SegmentContract obj)
        {
            if (obj.Description.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description Translated required.");
            if (obj.MarketGrouping == Guid.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Group required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
