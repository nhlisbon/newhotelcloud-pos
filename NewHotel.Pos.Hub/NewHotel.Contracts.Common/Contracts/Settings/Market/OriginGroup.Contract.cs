﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(OriginGroupContract), "ValidateOriginGroup")]
    public class OriginGroupContract : BaseContract
    {
        #region Members
        private Guid _installation;
        #endregion
        #region Contract properties

        [DataMember]
        internal LanguageTranslationContract _description;

        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }
        [DataMember]
        public Guid Installation { get { return _installation; } set { Set(ref _installation, value, "Installation"); } }
        #endregion
        #region Constructor

        public OriginGroupContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateOriginGroup(OriginGroupContract obj)
        {
            if (obj.Description.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description Translated required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
