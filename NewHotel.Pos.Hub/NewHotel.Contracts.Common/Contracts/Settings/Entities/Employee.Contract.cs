﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(EmployeeContract), "ValidateEmployee")]
    public class EmployeeContract : ContactContract 
    {
        public EmployeeContract(DateTime today)
            : base(DocumentSign.None, null, today) { }

        #region Members
        private long? _role;
        private bool _inactive;
        #endregion

        #region Properties
        [DataMember]
        public long? Role { get { return _role; } set { Set(ref _role, value, "Role"); } }
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }

        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateEmployee(EmployeeContract obj)
        {
            if (!obj.Role.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Role required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}