﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(BusinessLineContract), "ValidateBusinessLine")]
    public class BusinessLineContract : BaseContract
    {
        #region Contract Properties
        [DataMember]
        internal LanguageTranslationContract _description;

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public LanguageTranslationContract Description
        {
            get { return _description; }
        }

        #endregion

        #region Constructor

        public BusinessLineContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion

        #region Validations
        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateBusinessLine(BusinessLineContract obj)
        {
            if (string.IsNullOrEmpty(obj.Description))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
        #endregion
    }
}
