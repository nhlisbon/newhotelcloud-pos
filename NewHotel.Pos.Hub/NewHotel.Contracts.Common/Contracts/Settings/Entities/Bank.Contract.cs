﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class BankContract : EntityBaseContract
    {
        #region Members

        private string _bicSwift;
        private string _bankCode;
        private string _bankAgency;
        private string _bankAccount;
        private string _bankOurNumber;
        private string _bankAssignorCode;
        private string _bankFixedText;

        #endregion
        #region Constructor

        public BankContract()
            : base()
        {
            _localBankAccounts = new TypedList<BankAccountContract>();
        }

        #endregion
        #region Properties

        [DataMember]
        public string BicSwift { get { return _bicSwift; } set { Set(ref _bicSwift, value, "BicSwift"); } }
        [DataMember]
        public string BankCode { get { return _bankCode; } set { Set(ref _bankCode, value, "BankCode"); } }
        [DataMember]
        public string BankAgency { get { return _bankAgency; } set { Set(ref _bankAgency, value, "BankAgency"); } }
        [DataMember]
        public string BankAccount { get { return _bankAccount; } set { Set(ref _bankAccount, value, "BankAccount"); } }
        [DataMember]
        public string BankOurNumber { get { return _bankOurNumber; } set { Set(ref _bankOurNumber, value, "BankOurNumber"); } }
        [DataMember]
        public string BankAssignorCode { get { return _bankAssignorCode; } set { Set(ref _bankAssignorCode, value, "BankAssignorCode"); } }
        [DataMember]
        public string BankFixedText { get { return _bankFixedText; } set { Set(ref _bankFixedText, value, "BankFixedText"); } }

        #endregion
        #region Lists

        internal TypedList<BankAccountContract> _localBankAccounts;
        [ReflectionExclude]
        public TypedList<BankAccountContract> LocalBankAccounts
        {
            get { return _localBankAccounts; }
            set { _localBankAccounts = value; }
        }

        #endregion
    }
}
