﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class EntityContactContract : BaseContract
    {
        #region Members

        private Guid _baseEntityId;
        private Guid _contactId;
        private string _entityDescription;
        private string _contactName;
        private string _contactLastName;

        #endregion
        #region Properties

        [DataMember]
        public Guid BaseEntityId { get { return _baseEntityId; } set { Set(ref _baseEntityId, value, "BaseEntityId"); } }
        [DataMember]
        public Guid ContactId { get { return _contactId; } set { Set(ref _contactId, value, "ContactId"); } }
        [DataMember]
        public string EntityDescription { get { return _entityDescription; } set { Set(ref _entityDescription, value, "EntityDescription"); } }
        [DataMember]
        public string ContactName
        {
            get { return _contactName; }
            set
            {
                if (Set(ref _contactName, value, "ContactName"))
                    NotifyPropertyChanged("ContactFullName");
            }
        }
        [DataMember]
        public string ContactLastName
        {
            get { return _contactLastName; }
            set
            {
                if (Set(ref _contactLastName, value, "ContactLastName"))
                    NotifyPropertyChanged("ContactFullName");
            }
        }

        public string ContactFullName
        {
            get { return ((ContactLastName ?? string.Empty) + ", " + (ContactName ?? string.Empty)).Trim(); }
        }

        #endregion
    }
}