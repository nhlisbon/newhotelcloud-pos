﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class EntityMergeContract : BaseContract
    {
        #region Private Members
        private Guid _leftClientId;
        private string _leftClientDescription;

        private Guid _rightClientId;
        private string _rightClientDescription;

        //True Indicates Use Left Client Information
        private bool _personal;
        private bool _contract;
        private bool _homeAddress;
        private bool _fiscalAddress;
        private bool _documents;
        private bool _properties;
        private bool _accountProperties;
        private bool _currentAccount;

        private bool _marketSegment;
        private bool _attentions;
        private bool _relatives;
        private bool _cards;
        private bool _preferences;

        private bool _mergeMarketSegment;
        private bool _mergeAttentions;
        private bool _mergeRelatives;
        private bool _mergeCards;
        private bool _mergePreferences;
        #endregion
        #region Public Properties
        [DataMember]
        public Guid LeftEntityId { get { return _leftClientId; } set { Set(ref _leftClientId, value, "LeftEntityId"); } }
        [DataMember]
        public string LeftClientDescription { get { return _leftClientDescription; } set { Set(ref _leftClientDescription, value, "LeftClientDescription"); } }
        [DataMember]
        public Guid RightEntityId { get { return _rightClientId; } set { Set(ref _rightClientId, value, "RightEntityId"); } }
        [DataMember]
        public string RightClientDescription { get { return _rightClientDescription; } set { Set(ref _rightClientDescription, value, "RightClientDescription"); } }

        [DataMember]
        public bool Names { get { return _personal; } set { Set(ref _personal, value, "Names"); } }
        [DataMember]
        public bool Contract { get { return _contract; } set { Set(ref _contract, value, "Contract"); } }
        [DataMember]
        public bool HomeAddress { get { return _homeAddress; } set { Set(ref _homeAddress, value, "HomeAddress"); } }
        [DataMember]
        public bool FiscalAddress { get { return _fiscalAddress; } set { Set(ref _fiscalAddress, value, "FiscalAddress"); } }
        [DataMember]
        public bool Documents { get { return _documents; } set { Set(ref _documents, value, "Documents"); } }
        [DataMember]
        public bool Properties { get { return _properties; } set { Set(ref _properties, value, "Properties"); } }
        [DataMember]
        public bool AccountProperties { get { return _accountProperties; } set { Set(ref _accountProperties, value, "AccountProperties"); } }
        [DataMember]
        public bool CurrentAccount { get { return _currentAccount; } set { Set(ref _currentAccount, value, "CurrentAccount"); } }
        [DataMember]
        public bool MarketSegment { get { return _marketSegment; } set { Set(ref _marketSegment, value, "MarketSegment"); } }
        [DataMember]
        public bool Attentions { get { return _attentions; } set { Set(ref _attentions, value, "Attentions"); } }
        [DataMember]
        public bool Relations { get { return _relatives; } set { Set(ref _relatives, value, "Relations"); } }
        [DataMember]
        public bool Cards { get { return _cards; } set { Set(ref _cards, value, "Cards"); } }
        [DataMember]
        public bool Preferences { get { return _preferences; } set { Set(ref _preferences, value, "Preferences"); } }
        [DataMember]
        public bool MergeMarketSegments { get { return _mergeMarketSegment; } set { Set(ref _mergeMarketSegment, value, "MergeMarketSegments"); } }
        [DataMember]
        public bool MergeAttentions { get { return _mergeAttentions; } set { Set(ref _mergeAttentions, value, "MergeAttentions"); } }
        [DataMember]
        public bool MergeRelations { get { return _mergeRelatives; } set { Set(ref _mergeRelatives, value, "MergeRelations"); } }
        [DataMember]
        public bool MergeCards { get { return _mergeCards; } set { Set(ref _mergeCards, value, "MergeCards"); } }
        [DataMember]
        public bool MergePreferences { get { return _mergePreferences; } set { Set(ref _mergePreferences, value, "MergePreferences"); } }
        #endregion
        #region Constructor
        public EntityMergeContract()
        {
            //Normal Properties use Left by Default
            _marketSegment = true;
            _accountProperties = true;
            _contract = true;
            _currentAccount = true;
            _documents = true;
            _fiscalAddress = true;
            _personal = true;
            _properties = true;
            _homeAddress = true;
            //List Properties use Merge by Default
            _attentions = false;
            _cards = false;
            
            _preferences = false;
            _relatives = false;
            _mergeAttentions = true;
            _mergeCards = true;
            _mergeMarketSegment = true;
            _mergePreferences = true;
            _mergeRelatives = true;
            
        }
        #endregion
    }
}
