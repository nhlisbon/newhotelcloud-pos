﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class EntityExtAccountContract : BaseContract
    {
        #region Members

        private Guid _extAccountId;
        private string _extAccountDescription;
        private string _extAccountAbbreviation;

        #endregion
        #region Properties

        [DataMember]
        public Guid ExtAccountId { get { return _extAccountId; } set { Set(ref _extAccountId, value, "ExtAccountId"); } }
        [DataMember]
        public string ExtAccountAbbreviation { get { return _extAccountAbbreviation; } set { Set(ref _extAccountAbbreviation, value, "ExtAccountAbbreviation"); } }
        [DataMember]
        public string ExtAccountDescription { get { return _extAccountDescription; } set { Set(ref _extAccountDescription, value, "ExtAccountDescription"); } }

        #endregion
    }
}