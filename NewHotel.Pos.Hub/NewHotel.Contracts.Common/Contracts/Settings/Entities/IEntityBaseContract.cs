﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public enum TypeAddress
    {
        HomeTypeAddress,
        FiscalTypeAddress
    }

    public interface IEntityBaseContract
    {
        bool IsInstallation { get; }

        TypeAddressContract FiscalAddressContract { get; set; }
        TypeAddressContract HomeAddressContract { get; set; }

        string HomePhone { get; set; }
        string MobilePhone { get; set; }
        string FaxNumber { get; set; }
        string EmailAddress { get; set; }
        string PersonalHomePage { get; set; }
        string SkypeName { get; set; }
        string FiscalNumber { get; set; }
        string FiscalRegister { get; set; }
        string Comment { get; set; }

        void CopyAddress(bool home);
    }

    public interface IInstallationEntityBaseContract : IEntityBaseContract
    {
        bool InVATSystem { get; set; }
        DocumentSign DocumentSignatureType { get; }
    }

    [DataContract]
	[Serializable]
    public class TypeAddressContract : BaseContract
    {
        private string _name;
        private string _address1;
        private string _address2;
        private string _doorNumber;
        private string _country;
        private string _postalCode;
        private string _location;
        private Guid? _districtId;
        [DataMember]
        internal bool _showName;

        [DataMember]
        public string Name { get { return _name; } set { Set(ref _name, value, "Name"); } }
        [DataMember]
        public string Address1 { get { return _address1; } set { Set(ref _address1, value, "Address1"); } }
        [DataMember]
        public string Address2 { get { return _address2; } set { Set(ref _address2, value, "Address2"); } }
        [DataMember]
        public string DoorNumber { get { return _doorNumber; } set { Set(ref _doorNumber, value, "DoorNumber"); } }
        [DataMember]
        public string Country { get { return _country; } set { Set(ref _country, value, "Country"); } }
        [DataMember]
        public Guid? DistrictId { get { return _districtId; } set { Set<Guid?>(ref _districtId, value, "DistrictId"); } }
        [DataMember]
        public string Location { get { return _location; } set { Set(ref _location, value, "Location"); } }
        [DataMember]
        public string PostalCode { get { return _postalCode; } set { Set(ref _postalCode, value, "PostalCode"); } }

        public TypeAddressContract(bool showName)
        {
            _showName = showName;
        }

        public TypeAddressContract()
            : this(false)
        {
        }

        protected TypeAddressContract(string name, bool showName, string address1, string address2, string doorNumber, string country, string postalCode, string location, Guid? districtId)
            : this(showName)
        {
            Name = name;
            Address1 = address1;
            Address2 = address2;
            DoorNumber = doorNumber;
            Country = country;
            PostalCode = postalCode;
            Location = location;
            DistrictId = districtId;
        }

        public string Address
        {
            get
            {
                var address = string.Empty;
                if (!string.IsNullOrEmpty(Address1))
                {
                    address += Address1;
                    if (!string.IsNullOrEmpty(Address2))
                        address += Environment.NewLine + Address2;
                }
                else
                    address += Address2;

                return address;
            }
        }

        public bool ShowName
        {
            get { return _showName; }
        }
    }

    [DataContract]
	[Serializable]
    public class HomeAddress : TypeAddressContract
    {
        public HomeAddress(string name, string address1, string address2, string doorNumber, string country, string postalCode, string location, Guid? districtId)
            : base(name, true, address1, address2, doorNumber, country, postalCode, location, districtId)
        {
        }

        public HomeAddress(string address1, string address2, string doorNumber, string country, string postalCode, string location, Guid? districtId)
            : base(string.Empty, false, address1, address2, doorNumber, country, postalCode, location, districtId)
        {
        }

        public HomeAddress()
            : base()
        {
        }
    }

    [DataContract]
	[Serializable]
    public class FiscalAddress : TypeAddressContract
    {
        public FiscalAddress(string name, string address1, string address2, string doorNumber, string country, string postalCode, string location, Guid? districtId)
            : base(name, true, address1, address2, doorNumber, country, postalCode, location, districtId)
        {
        }

        public FiscalAddress(string address1, string address2, string doorNumber, string country, string postalCode, string location, Guid? districtId)
            : base(string.Empty, false, address1, address2, doorNumber, country, postalCode, location, districtId)
        {
        }

        public FiscalAddress()
            : base()
        {
        }
    }
}