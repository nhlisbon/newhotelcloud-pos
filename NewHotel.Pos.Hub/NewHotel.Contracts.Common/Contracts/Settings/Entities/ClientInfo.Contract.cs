﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class ClientInfoContract : BaseContract<Guid>
    {
        #region Lists

        [DataMember]
        public TypedList<ClientPreferenceRecord> Preferences { get; set; }
        [DataMember]
        public TypedList<ClientDietRecord> Diets { get; set; }
        [DataMember]
        public TypedList<ClientAttentionRecord> Attentions { get; set; }
        [DataMember]
        public TypedList<ClientAllergyRecord> Allergies { get; set; }

        #endregion
        #region Constructor

        public ClientInfoContract()
            : base()
        {
            Preferences = new TypedList<ClientPreferenceRecord>();
            Diets = new TypedList<ClientDietRecord>();
            Attentions = new TypedList<ClientAttentionRecord>();
            Allergies = new TypedList<ClientAllergyRecord>();
        }

        #endregion
    }
}