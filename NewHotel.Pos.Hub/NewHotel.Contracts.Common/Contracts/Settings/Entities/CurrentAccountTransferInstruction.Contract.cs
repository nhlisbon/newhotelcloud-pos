﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class CurrentAccountTransferInstructionContract : BaseContract
    {
        #region Members

        private DailyAccountType? _fromAccountType;
        private DailyAccountType? _toAccountType;
        private Guid? _departmentId;
        private Guid? _serviceId;
        private Guid? _toAccountId;
        private string _fromAccountTypeDescription;
        private string _toAccountTypeDescription;
        private string _departmentDescription;
        private string _serviceDescription;
        private string _fromAccountDescription;
        private string _toAccountDescription;
        private bool _transferOnlyDaily;
        [DataMember]
        internal string _toSameAccountDescription;

        #endregion
        #region CurrentAccountTransferInstruction properties

        [DataMember]
        public DailyAccountType? FromAccountType { get { return _fromAccountType; } set { Set(ref _fromAccountType, value, "FromAccountType"); } }
        [DataMember]
        public DailyAccountType? ToAccountType { get { return _toAccountType; } set { Set(ref _toAccountType, value, "ToAccountType"); } }
        [DataMember]
        public Guid? DepartmentId { get { return _departmentId; } set { Set(ref _departmentId, value, "DepartmentId"); } }
        [DataMember]
        public Guid? ServiceId { get { return _serviceId; } set { Set(ref _serviceId, value, "ServiceId"); } }
        [DataMember]
        public Guid? ToAccountId
        { 
            get { return _toAccountId; }
            set
            {
                if (Set(ref _toAccountId, value, "ToAccountId"))
                    NotifyPropertyChanged("ToAccountDescription");                
            }
        }
        [DataMember]
        public string FromAccountTypeDescription { get { return _fromAccountTypeDescription; } set { Set(ref _fromAccountTypeDescription, value, "FromAccountTypeDescription"); } }
        [DataMember]
        public string ToAccountTypeDescription { get { return _toAccountTypeDescription; } set { Set(ref _toAccountTypeDescription, value, "ToAccountTypeDescription"); } }
        [DataMember]
        public string DepartmentDescription { get { return _departmentDescription; } set { Set(ref _departmentDescription, value, "DepartmentDescription"); } }
        [DataMember]
        public string ServiceDescription { get { return _serviceDescription; } set { Set(ref _serviceDescription, value, "ServiceDescription"); } }
        [DataMember]
        public string FromAccountDescription { get { return _fromAccountDescription; } set { Set(ref _fromAccountDescription, value, "FromAccountDescription"); } }
        [DataMember]
        public string ToAccountDescription
        { 
            get { return _toAccountId.HasValue ? _toAccountDescription : _toSameAccountDescription; }
            set { Set(ref _toAccountDescription, value, "ToAccountDescription"); }
        }
        [DataMember]
        public bool TransferOnlyDaily { get { return _transferOnlyDaily; } set { Set(ref _transferOnlyDaily, value, "TransferOnlyDaily"); } }

        #endregion
        [DataMember]
        public List<Guid> ServicesIds { get; set; }

        #region Constructor

        public CurrentAccountTransferInstructionContract()
            : base()
        {
            ServicesIds = new List<Guid>();
        }

        public CurrentAccountTransferInstructionContract(string toSameAccountDescription)
            : this()
        {
            _toSameAccountDescription = toSameAccountDescription;
        }

        #endregion
    }
}
