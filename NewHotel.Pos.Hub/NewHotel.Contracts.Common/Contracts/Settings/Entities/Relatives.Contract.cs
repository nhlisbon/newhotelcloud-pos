﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RelativeContract : BaseContract
    {
        #region Members

        private RelationType _relationType;
        private Guid _installation;

        [DataMember]
        internal LanguageTranslationContract _description;

        #endregion
        #region Properties

        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        public RelationType RelationType { get { return _relationType; } set { Set(ref _relationType, value, "RelationType"); } }
        [DataMember]
        public Guid Installation { get { return _installation; } set { Set(ref _installation, value, "Installation"); } }

        #endregion
        #region Constructor

        public RelativeContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
    }
}
