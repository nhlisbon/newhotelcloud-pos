﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(EntityRelationContract), "ValidateEntityRelation")]
    public class EntityRelationContract : BaseContract
    {
        #region Members

        private Guid? _entityRelationId;
        private string _abbreviation;
        private string _companyName;
        private string _fiscalName;
        private string _fiscalNumber;
        private Guid? _relationTypeId;
        private string _relationTypeDesc;

        #endregion
        #region Properties

        [DataMember]
        public Guid? EntityRelationId { get { return _entityRelationId; } set { Set(ref _entityRelationId, value, "EntityRelationId"); } }
        [DataMember]
        public string Abbreviation { get { return _abbreviation; } set { Set(ref _abbreviation, value, "Abbreviation"); } }
        [DataMember]
        public string CompanyName { get { return _companyName; } set { Set(ref _companyName, value, "CompanyName"); } }
        [DataMember]
        public string FiscalName { get { return _fiscalName; } set { Set(ref _fiscalName, value, "FiscalName"); } }
        [DataMember]
        public string FiscalNumber { get { return _fiscalNumber; } set { Set(ref _fiscalNumber, value, "FiscalNumber"); } }
        [DataMember]
        public Guid? RelationTypeId { get { return _relationTypeId; } set { Set(ref _relationTypeId, value, "RelationTypeId"); } }
        [DataMember]
        public string RelationTypeDesc { get { return _relationTypeDesc; } set { Set(ref _relationTypeDesc, value, "RelationTypeDesc"); } }

        #endregion
        #region Constructors

        public EntityRelationContract()
            : base()
        {
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateEntityRelation(EntityRelationContract obj)
        {
            if (!obj._entityRelationId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Entity cannot be empty.");
            if (!obj.RelationTypeId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Relation type cannot be empty.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
