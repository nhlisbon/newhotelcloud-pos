﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [KnownType(typeof(TechnicianContract))]
    [CustomValidation(typeof(EmployeeInstallationContract), "ValidateEmployeeInstallation")]
    public class EmployeeInstallationContract : BaseContract
    {
        #region Members
        private Guid? _role;
        #endregion

        [DataMember]
        public Guid? Role { get { return _role; } set { Set(ref _role, value, "Role"); } }

        [ReflectionExclude]
        [DataMember]
        public EmployeeContract Employee { get; set; }

        public EmployeeInstallationContract(DateTime today)
            : base()
        {
            Employee = new EmployeeContract(today);
        }

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateEmployeeInstallation(EmployeeInstallationContract obj)
        {
            if (!obj.Role.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Role required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}