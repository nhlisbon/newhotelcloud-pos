﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(GuestCategoryContract), "ValidateGuestCategory")]
    public class GuestCategoryContract : BaseContract
    {
        #region Members

        private int? _applicationId;
        private Guid? _priceRateId;
        private string _priceRateDescription;
        private string _externalCode;

        #endregion
        #region Contract Properties

        [DataMember]
        public LanguageTranslationContract Description { get; set; }
        [DataMember]
        public int? ApplicationId { get { return _applicationId; } set { Set(ref _applicationId, value, "ApplicationId"); } }
        [DataMember]
        public Guid? PriceRateId { get { return _priceRateId; } set { Set(ref _priceRateId, value, "PriceRateId"); } }
        [DataMember]
        public string PriceRateDescription { get { return _priceRateDescription; } set { Set(ref _priceRateDescription, value, "PriceRateDescription"); } }
        [DataMember]
        public string ExternalCode { get { return _externalCode; } set { Set(ref _externalCode, value, "ExternalCode"); } }

        #endregion

        #region Constructor

        public GuestCategoryContract()
            : base() 
        {
            Description = new LanguageTranslationContract();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateGuestCategory(GuestCategoryContract obj)
        {
            if (obj.Description.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}