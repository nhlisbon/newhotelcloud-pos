﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ClientAttentionContract : BaseContract
    {
        [DataMember]
        public Guid ClientId { get; set; }
        [DataMember]
        public Guid AttentionType { get; set; }
        [DataMember]
        public string AttentionTypeDesc { get; set; }

        public ClientAttentionContract() : base() { }

        public ClientAttentionContract(Guid id, Guid clientId, Guid attentionType, string attentionTypeDesc) : base()
        {
            Id = id;
            ClientId = clientId;
            AttentionType = attentionType;
            AttentionTypeDesc = attentionTypeDesc;
        }
    }
}