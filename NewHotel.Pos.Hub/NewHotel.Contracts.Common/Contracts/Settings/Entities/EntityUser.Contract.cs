﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewHotel.Contracts
{
    public class EntityUserContract : BaseContract
    {
        #region Extended Properties

        [ReflectionExclude]
        public string UserLogin
        {
            get { return User.Login; }
        }

        [ReflectionExclude]
        public Guid UserId
        {
            get { return (Guid)User.Id; }
        }

        #endregion
        #region Constructors

        //[ReflectionExclude]
        public UserContract User { get; private set; }

        #endregion
        #region Constructors

        public EntityUserContract()
            : base()
        {
            User = new UserContract();
        }

        #endregion
    }
}
