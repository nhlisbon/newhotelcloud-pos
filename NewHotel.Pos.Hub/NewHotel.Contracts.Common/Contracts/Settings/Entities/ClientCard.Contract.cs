﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ClientCardContract : BaseContract
    {
        #region Members
        private Guid _clientId;
        private string _cardNumber;
        #endregion

        #region Properties
        [DataMember]
        public Guid ClientId { get { return _clientId; } set { Set(ref _clientId, value, "ClientId"); } }

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Card number required.")]
        public string CardNumber { get { return _cardNumber; } set { Set(ref _cardNumber, value, "CardNumber"); } }
        #endregion

        public ClientCardContract() : base() { }

        public ClientCardContract(Guid id, Guid clientId, string cardNumber) : base() 
        {
            Id = id;
            ClientId = clientId;
            CardNumber = cardNumber;
        }
    }
}
