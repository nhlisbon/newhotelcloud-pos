﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class BankAccountContract : BaseContract
    {
        #region Properties

        private Guid? _bankId;
        [DataMember]
        public Guid? BankId { get { return _bankId; } set { Set(ref _bankId, value, "BankId"); } }
        private string _bankName;
        [DataMember]
        public string BankName { get { return _bankName; } set { Set(ref _bankName, value, "BankName"); } }
        [DataMember]
        public bool BankReadOnly { get; set; }
        private string _bankAccountNumber;
        [DataMember]
        public string BankAccountNumber { get { return _bankAccountNumber; } set { Set(ref _bankAccountNumber, value, "BankAccountNumber"); } }
        private bool _mainBankAccount = false;
        [DataMember]
        public bool MainBankAccount { get { return _mainBankAccount; } set { Set(ref _mainBankAccount, value, "MainBankAccount"); } }
        [DataMember]
        public bool OldMainBankAccount { get; set; }
        private DateTime _initialBalanceDate;
        [DataMember]
        public DateTime InitialBalanceDate { get { return _initialBalanceDate; } set { Set(ref _initialBalanceDate, value, "InitialBalanceDate"); } }
        private decimal _initialBalance;
        [DataMember]
        public decimal InitialBalance { get { return _initialBalance; } set { Set(ref _initialBalance, value, "InitialBalance"); } }

        #endregion
        #region Constructors

        public BankAccountContract() : base() { }
        public BankAccountContract(Guid id, Guid bankId, string bankName, string bankAccountNumber) : this()
        {
            Id = id;
            BankId = bankId;
            BankName = bankName;
            InitialBalanceDate = DateTime.Now;
            BankAccountNumber = bankAccountNumber;
        }

        #endregion
    }
}
