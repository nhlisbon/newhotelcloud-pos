﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(EntityDepositRequestContract), "ValidateEntityDepositRequest")]
    public class EntityDepositRequestContract : BaseContract
    {
        #region Members
        public Guid? _archivedDocId;
        public string _archivedDocDesc;
        public decimal? _depositValue;
        public string _currencyId;
        public bool _confirmed;
        #endregion

        #region Properties
        [DataMember]
        public Guid? ArchivedDocId { get { return _archivedDocId; } set { Set(ref _archivedDocId, value, "ArchivedDocId"); } }
        [DataMember]
        public string ArchivedDocDesc { get { return _archivedDocDesc; } set { Set(ref _archivedDocDesc, value, "ArchivedDocDesc"); } }
        [DataMember]
        [PositiveValue("Deposit")]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Deposit value cannot be empty or less than zero.")]
        public decimal? DepositValue { get { return _depositValue; } set { Set(ref _depositValue, value, "DepositValue"); } }
        [DataMember]
        [NewHotel.Contracts.Required("Currency")]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Currency required.")]
        public string CurrencyId { get { return _currencyId; } set { Set(ref _currencyId, value, "CurrencyId"); } }
        [DataMember]
        public string RegisterUser { get; internal set; }
        [DataMember]
        public DateTime RegisterDate { get; internal set; }
        [DataMember]
        public string ConfirmUser { get; internal set; }
        [DataMember]
        public DateTime? ConfirmDate { get; internal set; }
        public bool Confirmed { get { return _confirmed; } set { Set(ref _confirmed, value, "Confirmed"); } }
        #endregion

        #region Constructors

        public EntityDepositRequestContract(string registerUser, DateTime registerDate,
            string confirmUser, DateTime confirmDate)
            : base()
        {
            ArchivedDocDesc = string.Empty;
            RegisterUser = registerUser;
            RegisterDate = registerDate;
            ConfirmUser = confirmUser;
            ConfirmDate = confirmDate;
        }

        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateEntityDepositRequest(EntityDepositRequestContract obj)
        {
            if (obj.DepositValue.HasValue && obj.DepositValue <= 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Deposit value cannot be empty or less than zero.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
