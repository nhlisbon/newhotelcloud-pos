﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class EntityCommissionContract : BaseContract
    {
        #region Members

        private decimal _commissionPercent;
        private bool _isDefault;

        #endregion
        #region Constructor

        public EntityCommissionContract()
            : base()
        {
        }

        #endregion
        #region Properties

        [DataMember]
        public decimal CommissionPercent { get { return _commissionPercent; } set { Set(ref _commissionPercent, value, "CommissionPercent"); } }
        [DataMember]
        public bool IsDefault
        {
            get { return _isDefault; }
            set { Set(ref _isDefault, value, "IsDefault"); }
        }

        #endregion
    }
}