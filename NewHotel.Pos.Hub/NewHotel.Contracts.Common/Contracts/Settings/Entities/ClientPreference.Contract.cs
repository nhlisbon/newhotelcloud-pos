﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ClientPreferenceContract : BaseContract
    {
        [DataMember]
        public Guid ClientId { get; set; }

        [DataMember]
        public Guid PreferenceId { get; set; }

        [DataMember]
        public string PreferenceDesc { get; set; }

        public ClientPreferenceContract() : base() { }

        public ClientPreferenceContract(Guid id, Guid clientId, Guid preferenceId, string preferenceDesc) : base() 
        {
            Id = id;
            ClientId = clientId;
            PreferenceId = preferenceId;
            PreferenceDesc = preferenceDesc;
        }
    }
}