﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SalesmanEntitiesContract : BaseContract
    {
        [DataMember]
        public Guid SalesmanInstallationId { get; set; }
        [DataMember]
        public Guid EntityInstallationId { get; set; }
        [DataMember]
        public string EntityCompanyName { get; set; }
       

        public SalesmanEntitiesContract() { }

        public SalesmanEntitiesContract(Guid id, Guid salesmanInstallationId, Guid entityInstallationId,  string description)
        {
            this.Id = id;
            this.SalesmanInstallationId = salesmanInstallationId;
            this.EntityInstallationId = entityInstallationId;
            this.EntityCompanyName = description;
        }
    }
}
