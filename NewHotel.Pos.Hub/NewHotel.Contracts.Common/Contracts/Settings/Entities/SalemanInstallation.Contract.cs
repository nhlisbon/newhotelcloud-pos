﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SalemanInstallationContract : BaseContract
    {
        [DataMember]
        public bool ExternalSalesman { get; set; }

        [ReflectionExclude]
        [DataMember]
        public SalemanContract Salesman { get; set; }

        public SalemanInstallationContract(DateTime today)
            : base()
        {
            Salesman = new SalemanContract(today);
            SalesmanEntities = new TypedList<SalesmanEntitiesContract>();
        }

        [ReflectionExclude]
        public TypedList<SalesmanEntitiesContract> SalesmanEntities { get; private set; }

        [IgnoreDataMember]
        [ReflectionExclude]
        public Guid[] SalesmanEntitiesIds
        {
            get { return SalesmanEntities.Select(x => x.SalesmanInstallationId).ToArray(); }
        }
    }
}