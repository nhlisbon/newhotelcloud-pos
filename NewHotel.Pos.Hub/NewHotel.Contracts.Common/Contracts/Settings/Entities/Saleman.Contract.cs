﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SalemanContract : ContactContract
    {
        #region Properties
        [DataMember]
        public bool IsExternal { get; set; }

        [DataMember]

        public decimal? CommissionPercent { get; set; }
         
        #endregion

        [ReflectionExclude]
        public TypedList<SalesmanEntitiesContract> SalesmanEntities { get; private set; }
        [IgnoreDataMember]
        [ReflectionExclude]
        public Guid[] SalesmanEntitiesIds
        {
            get { return SalesmanEntities.Select(x => x.SalesmanInstallationId).ToArray(); }
        }

        #region Constructor

        public SalemanContract(DateTime today)
            : base(DocumentSign.None, null, today) 
        {
            SalesmanEntities = new TypedList<SalesmanEntitiesContract>(); 
        }

        #endregion
    }
}