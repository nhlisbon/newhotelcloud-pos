﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TitleContract : BaseContract
    {
        #region Members
        private Guid _installation;
        #endregion
        #region Contract Properties
        [DataMember]
        internal LanguageTranslationContract _titleMasc;
        [DataMember]
        internal LanguageTranslationContract _titleFeme;

        [Display(Name = "TitleMasc")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract TitleMasc
        {
            get { return _titleMasc; }
            set { _titleMasc = value; }
        }
        [Display(Name = "TitleFeme")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract TitleFeme
        {
            get { return _titleFeme; }
            set { _titleFeme = value; }
        }

        [DataMember]
        public Guid Installation { get { return _installation; } set { Set(ref _installation, value, "Installation"); } }
        #endregion
        #region Constructor

        public TitleContract()
            : base()
        {
            TitleMasc = new LanguageTranslationContract();
            TitleFeme = new LanguageTranslationContract();
        }

        #endregion
    }
}
