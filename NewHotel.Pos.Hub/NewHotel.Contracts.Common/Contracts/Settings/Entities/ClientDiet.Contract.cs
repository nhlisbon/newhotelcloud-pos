﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class ClientDietContract : BaseContract
    {
        [DataMember]
        public Guid ClientId { get; set; }

        [DataMember]
        public Guid DietId { get; set; }

        [DataMember]
        public string DietDescription { get; set; }

        public ClientDietContract() : base() { }

        public ClientDietContract(Guid id, Guid clientId, Guid dietId, string dietDesc) : base()
        {
            Id = id;
            ClientId = clientId;
            DietId = dietId;
            DietDescription = dietDesc;
        }
    }
}