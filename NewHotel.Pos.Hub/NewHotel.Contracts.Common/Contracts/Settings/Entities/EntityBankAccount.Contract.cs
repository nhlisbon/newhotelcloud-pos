﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class EntityBankAccountContract : BaseContract
    {
        #region Members

        public string _bankDesc;
        public Guid? _bankId;
        public string _description;
        public string _iban;
        public string _code;
        public string _nib;

        #endregion

        #region Properties

        [DataMember]
        public string BankDesc { get { return _bankDesc; } set { Set(ref _bankDesc, value, "BankDesc"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Bank required.")]
        public Guid? BankId { get { return _bankId; } set { Set(ref _bankId, value, "BankId"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } } // Para quitar
        [DataMember]
        public string Iban { get { return _iban; } set { Set(ref _iban, value, "Iban"); } }
        [DataMember]
        public string Code { get { return _code; } set { Set(ref _code, value, "Code"); } }
        [DataMember]
        public string Nib { get { return _nib; } set { Set(ref _nib, value, "Nib"); } }

        #endregion
        #region Constructors

        public EntityBankAccountContract()
            : base()
        {
        }

        #endregion
    }
}
