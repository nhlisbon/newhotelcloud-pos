﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(InstallationContract), "ValidateInstallation")]
    public class InstallationContract : BaseContract, IInstallationEntityBaseContract
    {
        #region Constants

        public const int Bradesco = 1;
        public const int AgencySize = 5;
        public const int WalletSize = 2;
        public const int AccountSize = 8;

        #endregion
        #region Members

        [DataMember]
        internal DocumentSign _documentSignatureType;

        private string _description;
        private string _company;
        private string _abbreviation;
        private Blob? _logo;
        private string _customReportPath;
        private long _languages;
        private string _freeCode;
        private decimal _rating;
        private short _stars;
        private string _fiscalCertificate;
        private Blob? _fiscalCertificateKey;
        private string _fiscalCertificatePassword;
        private bool _inVATSystem;
        private string _colombiaTourismRegNumber;
        private string _homePhone;
        private string _mobilePhone;
        private string _faxNumber;
        private string _emailAddress;
        private string _personalHomePage;
        private string _skypeName;
        private string _comment;
        private string _fiscalName;
        private FiscalCompanyType _fiscalCompanyType;
        private string _fiscalNumber;
        private string _fiscalRegister;
        private string _fiscalRegimeTypeCode;
        private string _fiscalResponsabilityCode;
        private decimal? _longitude;
        private decimal? _latitude;
        private decimal? _tripAdvidorRate;
        private Guid? _cityId;

        private long? _bank;
        private string _agency;
        private string _account;
        private string _wallet;
        private string _place;
        private string _instructions;

        #endregion
        #region Properties

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public virtual string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public string Company { get { return _company; } set { Set(ref _company, value, "Company"); } }
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Abbreviation Entity required.")]
        public string Abbreviation { get { return _abbreviation; } set { Set(ref _abbreviation, value, "Abbreviation"); } }

        [DataMember]
        public Blob? Logo { get { return _logo; } set { Set(ref _logo, value, "Logo"); } }
        [DataMember]
        public string CustomReportPath { get { return _customReportPath; } set { Set(ref _customReportPath, value, "CustomReportPath"); } }
        [DataMember]
        public long Languages { get { return _languages; } set { Set(ref _languages, value, "Languages"); } }
        [DataMember]
        public string FreeCode { get { return _freeCode; } set { Set(ref _freeCode, value, "FreeCode"); } }
        [DataMember]
        public decimal Rating { get { return _rating; } set { Set(ref _rating, value, "Rating"); } }
        [DataMember]
        public short Stars { get { return _stars; } set { Set(ref _stars, value, "Stars"); } }
        [DataMember]
        public TypeAddressContract FiscalAddressContract { get; set; }
        [DataMember]
        public TypeAddressContract HomeAddressContract { get; set; }

        public void GetAddress(TypeAddress tipo, string name, string address1, string address2, string doorNumber, string country, string postalCode, string location, Guid? districtId)
        {
            if (tipo == TypeAddress.HomeTypeAddress)
                HomeAddressContract = new HomeAddress(name, address1, address2, doorNumber, country, postalCode, location, districtId);
            else
                FiscalAddressContract = new FiscalAddress(name, address1, address2, doorNumber, country, postalCode, location, districtId);
        }

        [DataMember]
        public string HomePhone { get { return _homePhone; } set { Set(ref _homePhone, value, "HomePhone"); } }
        [DataMember]
        public string MobilePhone { get { return _mobilePhone; } set { Set(ref _mobilePhone, value, "MobilePhone"); } }
        [DataMember]
        public string FaxNumber { get { return _faxNumber; } set { Set(ref _faxNumber, value, "FaxNumber"); } }
        [DataMember]
        public string EmailAddress  { get { return _emailAddress; } set { Set(ref _emailAddress, value, "EmailAddress"); } }
        [DataMember]
        public string PersonalHomePage { get { return _personalHomePage; } set { Set(ref _personalHomePage, value, "PersonalHomePage"); } }
        [DataMember]
        public string SkypeName { get { return _skypeName; } set { Set(ref _skypeName, value, "SkypeName"); } }
        [DataMember]
        public string Comment { get { return _comment; } set { Set(ref _comment, value, "Comment"); } }
        [DataMember]
        public string FiscalName { get { return _fiscalName; } set { Set(ref _fiscalName, value, "FiscalName"); } }
        [DataMember]
        public FiscalCompanyType FiscalCompanyType { get { return _fiscalCompanyType; } set { Set(ref _fiscalCompanyType, value, "FiscalCompanyType"); } }
        [DataMember]
        public string FiscalNumber { get { return _fiscalNumber; } set { Set(ref _fiscalNumber, value, "FiscalNumber"); } }
        [DataMember]
        public string FiscalRegister { get { return _fiscalRegister; } set { Set(ref _fiscalRegister, value, "FiscalRegister"); } }
        [DataMember]
        public string FiscalRegimeTypeCode { get { return _fiscalRegimeTypeCode; } set { Set(ref _fiscalRegimeTypeCode, value, "FiscalRegimeTypeCode"); } }
        [DataMember]
        public string FiscalResponsabilityCode { get { return _fiscalResponsabilityCode; } set { Set(ref _fiscalResponsabilityCode, value, "FiscalResponsabilityCode"); } }
        [DataMember]
        public string FiscalCertificate { get { return _fiscalCertificate; } set { Set(ref _fiscalCertificate, value, "FiscalCertificate"); } }
        [DataMember]
        public bool InVATSystem { get { return _inVATSystem; } set { Set(ref _inVATSystem, value, "InVATSystem"); } }
        [DataMember]
        public string ColombiaTourismRegNumber { get { return _colombiaTourismRegNumber; } set { Set(ref _colombiaTourismRegNumber, value, "ColombiaTourismRegNumber"); } }
        [DataMember]
        public decimal? Longitude { get { return _longitude; } set { Set(ref _longitude, value, "Longitude"); } }
        [DataMember]
        public decimal? Latitude { get { return _latitude; } set { Set(ref _latitude, value, "Latitude"); } }
        [DataMember]
        public decimal? TripAdvisorRate { get { return _tripAdvidorRate; } set { Set(ref _tripAdvidorRate, value, "TripAdvisorRate"); } }
        [DataMember]
        public Guid? CityId { get { return _cityId; } set { Set(ref _cityId, value, "CityId"); } }
        [DataMember]
        public long? Bank { get { return _bank; } set { Set(ref _bank, value, "Bank"); } }
        [DataMember]
        public string Agency { get { return _agency; } set { Set(ref _agency, value, "Agency"); } }
        [DataMember]
        public string Account { get { return _account; } set { Set(ref _account, value, "Account"); } }
        [DataMember]
        public string Wallet { get { return _wallet; } set { Set(ref _wallet, value, "Wallet"); } }
        [DataMember]
        public string Place { get { return _place; } set { Set(ref _place, value, "Place"); } }
        [DataMember]
        public string Instructions { get { return _instructions; } set { Set(ref _instructions, value, "Instructions"); } }

        [ReflectionExclude]
        public DocumentSign DocumentSignatureType
        {
            get { return _documentSignatureType; }
            set { Set(ref _documentSignatureType, value, "DocumentSignatureType"); }
        }

        [ReflectionExclude]
        public bool IsInstallation { get { return true; } }

        #endregion
        #region Dummy Properties

        [ReflectionExclude]
        public bool NewInstallation { get; set; }

        #endregion
        #region Contracts
        
        [DataMember]
        public SettingHotelContract SettingHotelContract { get; set; }
        [DataMember]
        public SettingReservationContract SettingReservationContract { get; set; }
        [DataMember]
        public SettingPriceContract SettingPriceContract { get; set; }
        [DataMember]
        public SettingInvoiceContract SettingInvoiceContract { get; set; }
        [DataMember]
        public SettingResourceContract SettingResourceContract { get; set; }
        [DataMember]
        public SettingTemplateContract SettingTemplateContract { get; set; }
        [DataMember]
        public TypedList<EntityAddressContract> Addresses { get; set; }
        [DataMember]
        public TypedList<EntityDocumentsContract> EntityDocs { get; set; }

        #endregion

        public InstallationContract()
            : base()
        {
            _documentSignatureType = DocumentSign.None;
            _fiscalCompanyType = FiscalCompanyType.LegalPerson;
            Addresses = new TypedList<EntityAddressContract>();
            EntityDocs = new TypedList<EntityDocumentsContract>();
            FiscalAddressContract = new TypeAddressContract(true);
            HomeAddressContract = new TypeAddressContract(true);
        }

        public void CopyAddress(bool home)
        {
            if (home)
            {
                FiscalAddressContract.Name = HomeAddressContract.Name;
                FiscalAddressContract.Address1 = HomeAddressContract.Address1;
                FiscalAddressContract.Address2 = HomeAddressContract.Address2;
                FiscalAddressContract.DoorNumber = HomeAddressContract.DoorNumber;
                FiscalAddressContract.Country = HomeAddressContract.Country;
                FiscalAddressContract.DistrictId = HomeAddressContract.DistrictId;
                FiscalAddressContract.Location = HomeAddressContract.Location;
                FiscalAddressContract.PostalCode = HomeAddressContract.PostalCode;
            }
            else
            {
                HomeAddressContract.Name = FiscalAddressContract.Name;
                HomeAddressContract.Address1 = FiscalAddressContract.Address1;
                HomeAddressContract.Address2 = FiscalAddressContract.Address2;
                HomeAddressContract.DoorNumber = FiscalAddressContract.DoorNumber;
                HomeAddressContract.Country = FiscalAddressContract.Country;
                HomeAddressContract.DistrictId = FiscalAddressContract.DistrictId;
                HomeAddressContract.Location = FiscalAddressContract.Location;
                HomeAddressContract.PostalCode = FiscalAddressContract.PostalCode;
            }
        }

        #region Validations

        private static char ValidationDigitMod11(string str)
        {
            var factor = 2;
            var auxSum = 0;
            for (var i = str.Length - 1; i >= 0; i--)
            {
                if (factor > 7)
                    factor = 2;

                auxSum += factor * int.Parse(str[i].ToString());
                factor++;
            }

            var validationDigit = auxSum % 11;
            validationDigit = 11 - validationDigit;
            if (validationDigit == 10)
                return '1';
            else if (validationDigit == 11)
                return '0';

            return validationDigit.ToString("0")[0];
        }

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateInstallation(InstallationContract obj)
        {
            if (obj.SettingHotelContract != null)
            {
                var result = SettingHotelContract.ValidateSettingHotel(obj.SettingHotelContract);
                if (result != System.ComponentModel.DataAnnotations.ValidationResult.Success)
                    return result;
            }

            if (obj.SettingTemplateContract != null)
            {
                var result = SettingTemplateContract.ValidateSettingTemplate(obj.SettingTemplateContract);
                if (result != System.ComponentModel.DataAnnotations.ValidationResult.Success)
                    return result;
            }

            if (obj.SettingInvoiceContract != null)
                obj._documentSignatureType = obj.SettingInvoiceContract.SignType ?? DocumentSign.None;

            if (obj._documentSignatureType == DocumentSign.FiscalizationCroatia)
            {
                if (string.IsNullOrEmpty(obj._description))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid description");
                if (string.IsNullOrEmpty(obj._fiscalNumber) || (obj._fiscalNumber.Length != 11 || (obj._fiscalNumber.Length == 13 && !obj._fiscalNumber.StartsWith("HR"))))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid fiscal number");
            }

            if (obj.Bank.HasValue && obj.Bank.Value == Bradesco)
            {
                if (string.IsNullOrEmpty(obj.Agency) ||
                    obj.Agency.Length != AgencySize || obj.Agency.Any(x => !char.IsDigit(x)) ||
                    ValidationDigitMod11(obj.Agency.Substring(0, AgencySize - 1)) != obj.Agency[obj.Agency.Length - 1])
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Agência não é valida");

                if (string.IsNullOrEmpty(obj.Wallet) ||
                    obj.Wallet.Length != WalletSize || obj.Wallet.Any(x => !char.IsDigit(x)))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Carteira não é valida");

                if (string.IsNullOrEmpty(obj.Account) ||
                    obj.Account.Length != AccountSize || obj.Account.Any(x => !char.IsDigit(x)) ||
                    ValidationDigitMod11(obj.Account.Substring(0, AccountSize - 1)) != obj.Account[obj.Account.Length - 1])
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Conta não é valida");
            }

            //decimal latitude = 0;
            //if (!String.IsNullOrEmpty(obj.LatitudeDesc) && !decimal.TryParse(obj.LatitudeDesc, out latitude))
            //{
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid Latitude");
            //}
            //else obj.Latitude = latitude;

            //decimal longitude = 0;
            //if (!String.IsNullOrEmpty(obj.LongitudeDesc) && !decimal.TryParse(obj.LongitudeDesc, out longitude))
            //{
            //    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid Longitude");
            //}
            //else obj.Longitude = longitude;

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion 
    }
}
