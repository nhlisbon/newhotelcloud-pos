﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ClientInstallationContract), "ValidateClientInstallation")]
    public class ClientInstallationContract : BaseContract
    {
        #region Members

        private bool _usual;
        private DateTime _registrationDate;
        private bool _unwanted;
        private bool _isOwner;
        private Guid? _clientTypeId;
        //private Guid? _currentAccountId;
        private Guid? _payableAccountId;
        private string _payableAccountDescription;
        private Guid? _receivableAccountId;
        private string _receivableAccountDescription;
        private DateTime? _operationClosingDate;
        private bool _active;
        private short? _mandatoryDepositDays;
        private AdvancedDepositType _depositType;
        private short? _depositValue;
        private DateTime? _lastDayModificationLocked;
        private DateTime? _firstDayModificationLocked;
        private DateTime? _lastDayInsertionLocked;
        private DateTime? _firstDayInsertionLocked;
        private bool _modificationLockActive;
        private bool _insertionLockActive;
        private short? _colorForPlanning;
        private short? _commissionPercent;
        private short? _daysForDeadLine;
        private bool _unproduceCommission;
        private short? _daysGuaranteedForNoShow;
        private Guid? _representativeEntityId;
        private bool _isWholesale;
        private bool _isRepresentative;
        private InvoiceDestination _invoiceReservationMasterTo;
        private KindValue _invoiceCommissionType;
        private bool _fixPricesAtReservation;
        private InvoiceModel _invoiceType;
        private Guid? _wholesalerEntityId;
        private bool _requiresCheckinAuthorization;
        private InvoiceOwner _invoiceTo;
        private bool _useSerieContadoForCreditInvoice;
        private bool _voucherIsMandatory;
        private Guid? _taxSchemaId;
        private Guid? _marketOriginId;
        private Guid? _marketSegmentId;
        private Guid? _taxRateId;
        #endregion
        #region ClientInstallation Properties

        [DataMember]
        public bool Usual { get { return _usual; } set { Set(ref _usual, value, "Usual"); } }
        [DataMember]
        public DateTime RegistrationDate { get { return _registrationDate; } set { Set(ref _registrationDate, value, "RegistrationDate"); } }
        [DataMember]
        public bool Unwanted { get { return _unwanted; } set { Set(ref _unwanted, value, "Unwanted"); } }
        [DataMember]
        public bool IsOwner { get { return _isOwner; } set { Set(ref _isOwner, value, "IsOwner"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Client type required")]
        public Guid? ClientTypeId { get { return _clientTypeId; } set { Set(ref _clientTypeId, value, "ClientTypeId"); } }

        #endregion
        #region EntityComercialInstallation Properties
        [DataMember]
        public Guid? PayableAccountId { get { return _payableAccountId; } set { Set(ref _payableAccountId, value, "PayableAccountId"); } }
        [DataMember]
        public string PayableAccountDescription { get { return _payableAccountDescription; } set { Set(ref _payableAccountDescription, value, "PayableAccountDescription"); } }
        [DataMember]
        public Guid? ReceivableAccountId { get { return _receivableAccountId; } set { Set(ref _receivableAccountId, value, "ReceivableAccountId"); } }
        [DataMember]
        public string ReceivableAccountDescription { get { return _receivableAccountDescription; } set { Set(ref _receivableAccountDescription, value, "ReceivableAccountDescription"); } }
        [DataMember]
        public DateTime? OperationClosingDate { get { return _operationClosingDate; } set { Set(ref _operationClosingDate, value, "OperationClosingDate"); } }
        [DataMember]
        public bool Active { get { return _active; } set { Set(ref _active, value, "Active"); } }

        #endregion
        #region EntityInstallation Properties
        [DataMember]
        public short? MandatoryDepositDays { get { return _mandatoryDepositDays; } set { Set(ref _mandatoryDepositDays, value, "MandatoryDepositDays"); } }
        [DataMember]
        public AdvancedDepositType DepositType { get { return _depositType; } set { Set(ref _depositType, value, "DepositType"); } }
        [DataMember]
        public short? DepositValue { get { return _depositValue; } set { Set(ref _depositValue, value, "DepositValue"); } }
        [DataMember]
        public DateTime? LastDayModificationLocked { get { return _lastDayModificationLocked; } set { Set(ref _lastDayModificationLocked, value, "LastDayModificationLocked"); } }
        [DataMember]
        public DateTime? FirstDayModificationLocked { get { return _firstDayModificationLocked; } set { Set(ref _firstDayModificationLocked, value, "FirstDayModificationLocked"); } }
        [DataMember]
        public DateTime? LastDayInsertionLocked { get { return _lastDayInsertionLocked; } set { Set(ref _lastDayInsertionLocked, value, "LastDayInsertionLocked"); } }
        [DataMember]
        public DateTime? FirstDayInsertionLocked { get { return _firstDayInsertionLocked; } set { Set(ref _firstDayInsertionLocked, value, "FirstDayInsertionLocked"); } }
        [DataMember]
        public bool ModificationLockActive { get { return _modificationLockActive; } set { Set(ref _modificationLockActive, value, "ModificationLockActive"); } }
        [DataMember]
        public bool InsertionLockActive { get { return _insertionLockActive; } set { Set(ref _insertionLockActive, value, "InsertionLockActive"); } }
        [DataMember]
        public short? ColorForPlanning { get { return _colorForPlanning; } set { Set(ref _colorForPlanning, value, "ColorForPlanning"); } }
        [DataMember]
        public short? CommissionPercent { get { return _commissionPercent; } set { Set(ref _commissionPercent, value, "CommissionPercent"); } }
        [DataMember]
        public short? DaysForDeadLine { get { return _daysForDeadLine; } set { Set(ref _daysForDeadLine, value, "DaysForDeadLine"); } }
        [DataMember]
        public bool UnproduceCommission { get { return _unproduceCommission; } set { Set(ref _unproduceCommission, value, "UnproduceCommission"); } }
        [DataMember]
        public short? DaysGuaranteedForNoShow { get { return _daysGuaranteedForNoShow; } set { Set(ref _daysGuaranteedForNoShow, value, "DaysGuaranteedForNoShow"); } }
        [DataMember]
        public Guid? RepresentativeEntityId { get { return _representativeEntityId; } set { Set(ref _representativeEntityId, value, "RepresentativeEntityId"); } }
        [DataMember]
        public bool IsWholesale { get { return _isWholesale; } set { Set(ref _isWholesale, value, "IsWholesale"); } }
        [DataMember]
        public bool IsRepresentative { get { return _isRepresentative; } set { Set(ref _isRepresentative, value, "IsRepresentative"); } }
        [DataMember]
        public InvoiceDestination InvoiceReservationMasterTo { get { return _invoiceReservationMasterTo; } set { Set(ref _invoiceReservationMasterTo, value, "InvoiceReservationMasterTo"); } }
        [DataMember]
        public KindValue InvoiceCommissionType { get { return _invoiceCommissionType; } set { Set(ref _invoiceCommissionType, value, "InvoiceCommissionType"); } }
        [DataMember]
        public bool FixPricesAtReservation { get { return _fixPricesAtReservation; } set { Set(ref _fixPricesAtReservation, value, "FixPricesAtReservation"); } }
        [DataMember]
        public InvoiceModel InvoiceType { get { return _invoiceType; } set { Set(ref _invoiceType, value, "InvoiceType"); } }
        [DataMember]
        public Guid? WholesalerEntityId { get { return _wholesalerEntityId; } set { Set(ref _wholesalerEntityId, value, "WholesalerEntityId"); } }
        [DataMember]
        public bool RequiresCheckinAuthorization { get { return _requiresCheckinAuthorization; } set { Set(ref _requiresCheckinAuthorization, value, "RequiresCheckinAuthorization"); } }
        [DataMember]
        public InvoiceOwner InvoiceTo { get { return _invoiceTo; } set { Set(ref _invoiceTo, value, "InvoiceTo"); } }
        [DataMember]
        public bool UseSerieContadoForCreditInvoice { get { return _useSerieContadoForCreditInvoice; } set { Set(ref _useSerieContadoForCreditInvoice, value, "UseSerieContadoForCreditInvoice"); } }
        [DataMember]
        public bool VoucherIsMandatory { get { return _voucherIsMandatory; } set { Set(ref _voucherIsMandatory, value, "VoucherIsMandatory"); } }
        [DataMember]
        public Guid? TaxSchemaId { get { return _taxSchemaId; } set { Set(ref _taxSchemaId, value, "TaxSchemaId"); } }
        [DataMember]
        public Guid? MarketOriginId { get { return _marketOriginId; } set { Set(ref _marketOriginId, value, "MarketOriginId"); } }
        [DataMember]
        public Guid? MarketSegmentId { get { return _marketSegmentId; } set { Set(ref _marketSegmentId, value, "MarketSegmentId"); } }
        [DataMember]
        public Guid? TaxRateId { get { return _taxRateId; } set { Set(ref _taxRateId, value, "TaxRateId"); } }

        #endregion
        #region Contracts

        private ClientContract _client;
        [ReflectionExclude]
        [DataMember]
        public ClientContract Client { get { return _client; } set { Set(ref _client, value, "Client"); } }

        [ReflectionExclude]
        [DataMember]
        public CurrentAccountContract CurrentAccount { get; set; }

        #endregion

        #region Lists

        private TypedList<ClientAttentionContract> _clientAttentions;
        [ReflectionExclude()]
        public TypedList<ClientAttentionContract> ClientAttentions
        {
            get
            {
                return _clientAttentions;
            }
        }
        [IgnoreDataMember]
        [ReflectionExclude]
        public Guid[] ClientAttentionsIds
        {
            get { return ClientAttentions.Select(x => x.AttentionType).ToArray(); }
        }

        private TypedList<ClientCardContract> _clientCards;
        [ReflectionExclude()]
        public TypedList<ClientCardContract> ClientCards
        {
            get
            {
                return _clientCards;
            }
        }
        [IgnoreDataMember]
        [ReflectionExclude]
        public string[] ClientCardIds
        {
            get { return ClientCards.Select(x => x.CardNumber).ToArray(); }
        }

        private TypedList<ClientMarketSegmentContract> _clientMarketSegments;
        [ReflectionExclude()]
        public TypedList<ClientMarketSegmentContract> ClientMarketSegments
        {
            get
            {
                return _clientMarketSegments;
            }
        }
        [IgnoreDataMember]
        [ReflectionExclude]
        public Guid[] ClientMarketSegmentsIds
        {
            get { return ClientMarketSegments.Select(x => x.SegmentId).ToArray(); }
        }

        private TypedList<ClientPreferenceContract> _clientPreferences;
        [ReflectionExclude()]
        public TypedList<ClientPreferenceContract> ClientPreferences
        {
            get
            {
                return _clientPreferences;
            }
        }
        [IgnoreDataMember]
        [ReflectionExclude]
        public Guid[] ClientPreferencesIds
        {
            get { return ClientPreferences.Select(x => x.PreferenceId).ToArray(); }
        }

        private TypedList<ClientRelativesContract> _clientRelatives;
        [ReflectionExclude()]
        public TypedList<ClientRelativesContract> ClientRelatives
        {
            get
            {
                return _clientRelatives;
            }
        }
        [IgnoreDataMember]
        [ReflectionExclude]
        public Guid[] ClientRelativesIds
        {
            get { return ClientRelatives.Select(x => x.RelativeClientId).ToArray(); }
        }

        #endregion

        #region Constructors

        public ClientInstallationContract(DocumentSign documentSignatureType, string fullNameOrder, DateTime today)
            : base()
        {
            Client = new ClientContract(documentSignatureType, fullNameOrder, today);
            DepositType = AdvancedDepositType.None;
            InvoiceReservationMasterTo = InvoiceDestination.Client;
            InvoiceType = InvoiceModel.Detailed;
            InvoiceTo = InvoiceOwner.ComercialInfo;
            InvoiceCommissionType = KindValue.Gross;

            _clientAttentions = new TypedList<ClientAttentionContract>();
            _clientCards = new TypedList<ClientCardContract>();
            _clientMarketSegments = new TypedList<ClientMarketSegmentContract>();
            _clientPreferences = new TypedList<ClientPreferenceContract>();
            _clientRelatives = new TypedList<ClientRelativesContract>();
        }

        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateClientInstallation(ClientInstallationContract obj)
        {
            if (obj.Client != null)
            {
                if (string.IsNullOrEmpty(obj.Client.FirstName))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("First name required.");
                if (string.IsNullOrEmpty(obj.Client.LastName))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Last name required.");
                if (!obj.Client.Gender.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Gender required.");
                if (string.IsNullOrEmpty(obj.Client.CountryId))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Country required.");
                if (!obj.Client.CivilStatus.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Civil status required.");
                if (!obj.Client.ClientTypeId.HasValue)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Client type required.");
                //if (obj.Client.Addresses!= null && obj.Client.Addresses.Count == 0)
                //    return new System.ComponentModel.DataAnnotations.ValidationResult("One Address is Required.");
            }
            
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}