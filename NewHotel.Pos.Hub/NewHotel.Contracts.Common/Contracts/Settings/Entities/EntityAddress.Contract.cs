﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
/*
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(EntityAddressContract), "ValidateEntityAddresss")]
    public class EntityAddressContract : BaseContract
    {
        #region Members
        [DataMember]
        internal BaseAddressContract _address;
        #endregion

        #region Properties
        [DataMember]
        public bool IsPublic { get; set; }
        [DataMember]
        public bool ByDefault 
        {
            get
            {
                if (_address is AddressContract) return ((AddressContract)_address).IsDefault;
                else return false;
            }
            set
            {
                if (_address is AddressContract) ((AddressContract)_address).IsDefault = value;
            }
        }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public long ContactTypeId
        {
            get { return Address.ContactTypeId; }
        }

        [ReflectionExclude]
        public string ContactTypeDetailDesc
        {
            get { return Address.ContactTypeDetailDesc; }
        }

        [ReflectionExclude]
        //[DataMember]
        public string ContactDescription
        {
            get { return Address.ContactDescription; }
        }

        #endregion
        #region Contracts

        [ReflectionExclude]
        public BaseAddressContract Address { get { return _address; } }


        #endregion
        #region Constructors

        public EntityAddressContract(BaseAddressContract address)
            : base()
        {
            _address = address;
        }

        #endregion

        #region Validations
        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateEntityAddresss(EntityAddressContract obj)
        {
            if (obj.Address is OtherAddressContract)
            {
                System.ComponentModel.DataAnnotations.ValidationResult res = OtherAddressContract.ValidateOtherAddress((OtherAddressContract)obj.Address);
                if (res != System.ComponentModel.DataAnnotations.ValidationResult.Success)
                    return res;
            }

            if (obj.Address is AddressContract)
            {
                System.ComponentModel.DataAnnotations.ValidationResult res = AddressContract.ValidateAddress((AddressContract)obj.Address);
                if (res != System.ComponentModel.DataAnnotations.ValidationResult.Success)
                    return res;
            }

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
        #endregion
    }
*/
}
