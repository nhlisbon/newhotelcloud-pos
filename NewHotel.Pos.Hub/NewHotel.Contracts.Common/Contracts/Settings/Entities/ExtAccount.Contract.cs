﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ExtAccountContract), "ValidateExtAccount")]
    public class ExtAccountContract : EntityBaseContract
    {
        #region Members

        private AccountType _type;
        private string _countryId;
        private string _accountGroup;
        private Guid? _accountGroupId;
        private Guid? _financialCategoryPurchasesId;
        private Guid? _financialCategorySalesId;

        #endregion
        #region Properties

        [DataMember]
        public AccountType Type { get { return _type; } set { Set(ref _type, value, "Type"); } }
        [DataMember]
        public string CountryId { get { return _countryId; } set { Set(ref _countryId, value, "CountryId"); } }
        [DataMember]
        public decimal? AuthorizedMaxRisk { get; set; }
        [DataMember]
        public bool UnderSupervision { get; set; }
        [DataMember]
        public short DaysOfExpired { get; set; }
        [DataMember]
        public decimal? CommissionValue { get; set; }
        [DataMember]
        public string CurrencyId { get; set; }
        [DataMember]
        public bool ClosingDate { get; set; }
        [DataMember]
        public Guid? AccountGroupId { get { return _accountGroupId; } set { Set(ref _accountGroupId, value, "AccountGroupId"); } }
        [DataMember]
        public string AccountGroup { get { return _accountGroup; } set { Set(ref _accountGroup, value, "AccountGroup"); } }
        [DataMember]
        public Guid? FinancialCategoryPurchasesId { get { return _financialCategoryPurchasesId; } set { Set(ref _financialCategoryPurchasesId, value, "FinancialCategoryPurchasesId"); } }
        [DataMember]
        public Guid? FinancialCategorySalesId { get { return _financialCategorySalesId; } set { Set(ref _financialCategorySalesId, value, "FinancialCategorySalesId"); } }

        #endregion
        #region Lists

        [ReflectionExclude]
        [DataMember]
        public TypedList<ExtAccountProtocolContract> Protocols { get; internal set; }

        #endregion
        #region Constructor

        public ExtAccountContract()
            : base()
        {
            Protocols = new TypedList<ExtAccountProtocolContract>();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateExtAccount(ExtAccountContract obj)
        {
            if (string.IsNullOrEmpty(obj.Abbreviation))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Abbreviation required.");
            if (string.IsNullOrEmpty(obj.Description))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description required.");
            if (string.IsNullOrEmpty(obj.CountryId))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Country required.");

            if (obj.Protocols != null && obj.Protocols.Count > 0)
            {
                foreach (var protocol in obj.Protocols)
                {
                    var vres = ExtAccountProtocolContract.ValidateExtAccountProtocol(protocol);
                    if (vres != System.ComponentModel.DataAnnotations.ValidationResult.Success)
                        return vres;
                }
            }

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class ExtAccountBalanceContract : ExtAccountContract
    {
        #region Properties

        [DataMember]
        public TypedList<ExtAccountOfficialDocContract> ExtAccountOfficialDocs { get;  set; }
        [DataMember]
        public TypedList<UpdateExtAccountOfficialContract> UdpateExtAccountOfficialDocs { get; set; }
        [DataMember]
        public TypedList<UpdateExtAccountOfficialContract> UndoUdpateExtAccountOfficialDocs { get; set; }

        #endregion
        #region Constructor

        public ExtAccountBalanceContract()
            : base()
        {
            ExtAccountOfficialDocs = new TypedList<ExtAccountOfficialDocContract>();
            UdpateExtAccountOfficialDocs = new TypedList<UpdateExtAccountOfficialContract>();
            UndoUdpateExtAccountOfficialDocs = new TypedList<UpdateExtAccountOfficialContract>();
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class UpdateExtAccountOfficialContract : BaseContract
    {
        #region Members

        private bool _regularizationExtAccount;

        #endregion
        #region Properties

        [DataMember]
        public Guid DocumentId { get; set; }
        [DataMember]
        public bool RegularizationExtAccount { get { return _regularizationExtAccount; } set { Set(ref _regularizationExtAccount, value, "RegularizationExtAccount"); } }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class ExtAccountOfficialDocContract : BaseContract
    {
        #region Members

        private bool _regularizationExtAccount;

        #endregion
        #region Properties

        [DataMember]
        public Guid DocumentId { get; set; }
        [DataMember]
        public Guid? ExtAccountId { get; set; }
        [DataMember]
        public string DocumentTypeDescription { get; set; }
        [DataMember]
        public string DocumentFullSerie { get; set; }
        [DataMember]
        public string EmissionDate { get; set; }
        [DataMember]
        public string GuestName { get; set; }
        [DataMember]
        public string ReseNumber { get; set; }
        [DataMember]
        public decimal? Total { get; set; }
        [DataMember]
        public DateTime PaymentDate { get; set; }
        [DataMember]
        public string Voucher { get; set; }
        [DataMember]
        public bool RegularizationExtAccount { get { return _regularizationExtAccount; } set { Set(ref _regularizationExtAccount, value, "RegularizationExtAccount"); } }
        [DataMember]
        public DateTime? RegularizationDateExtAccount { get; set; }
        [DataMember]
        public string UserDescription { get; set; }

        #endregion
        #region Extended Properties

        public bool DisableRegularization
        {
            get { return RegularizationExtAccount && !string.IsNullOrEmpty(UserDescription); }
        }

        #endregion
    }
}