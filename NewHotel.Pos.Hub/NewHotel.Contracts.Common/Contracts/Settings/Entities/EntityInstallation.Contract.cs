﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(EntityInstallationContract), "ValidateEntityInstallation")]
    public class EntityInstallationContract : BaseContract
    {
        #region Members

        private Guid? _payableAccountId;
        private string _payableAccountDescription;
        private Guid? _receivableAccountId;
        private string _receivableAccountDescription;
        private bool _active;
        private short? _mandatoryDepositDays;
        private AdvancedDepositType _depositType;
        private decimal? _depositValue;
        private ARGBColor? _colorForPlanning;
        private short? _daysForDeadLine;
        private bool _unproduceCommission;
        private short? _daysGuaranteedForNoShow;
        private InvoiceDestination _invoiceReservationMasterTo;
        private Guid? _taxRateId;
        private KindValue? _invoiceCommissionType;
        private bool _fixPricesAtReservation;
        private InvoiceModel _invoiceType;
        private bool _requiresCheckinAuthorization;
        private InvoiceOwner _invoiceTo;
        private bool _useSerieContadoForCreditInvoice;
        private bool _voucherIsMandatory;
        private Guid? _taxSchemaId;
        private Guid? _marketOriginId;
        private Guid? _marketSegmentId;
        private Guid? _entityTypeId;
        private string _entityTypeDesc;
        private EntityCategory _category;

        #endregion
        #region EntityComercialInstallation Properties

        [DataMember]
        public Guid? PayableAccountId { get { return _payableAccountId; } set { Set(ref _payableAccountId, value, "PayableAccountId"); } }
        [DataMember]
        public string PayableAccountDescription { get { return _payableAccountDescription; } set { Set(ref _payableAccountDescription, value, "PayableAccountDescription"); } }
        [DataMember]
        public Guid? ReceivableAccountId { get { return _receivableAccountId; } set { Set(ref _receivableAccountId, value, "ReceivableAccountId"); } }
        [DataMember]
        public string ReceivableAccountDescription { get { return _receivableAccountDescription; } set { Set(ref _receivableAccountDescription, value, "ReceivableAccountDescription"); } }
        [DataMember]
        public bool Active { get { return _active; } set { Set(ref _active, value, "Active"); } }

        #endregion
        #region EntityInstallation Properties

        [DataMember]
        public short? MandatoryDepositDays { get { return _mandatoryDepositDays; } set { Set(ref _mandatoryDepositDays, value, "MandatoryDepositDays"); } }
        [DataMember]
        public AdvancedDepositType DepositType { get { return _depositType; } set { Set(ref _depositType, value, "DepositType"); } }
        [DataMember]
        public decimal? DepositValue { get { return _depositValue; } set { Set(ref _depositValue, value, "DepositValue"); } }
        [DataMember]
        public ARGBColor? ColorForPlanning { get { return _colorForPlanning; } set { Set(ref _colorForPlanning, value, "ColorForPlanning"); } }
        [DataMember]
        public short? DaysForDeadLine { get { return _daysForDeadLine; } set { Set(ref _daysForDeadLine, value, "DaysForDeadLine"); } }
        [DataMember]
        public bool UnproduceCommission { get { return _unproduceCommission; } set { Set(ref _unproduceCommission, value, "UnproduceCommission"); } }
        [DataMember]
        public short? DaysGuaranteedForNoShow { get { return _daysGuaranteedForNoShow; } set { Set(ref _daysGuaranteedForNoShow, value, "DaysGuaranteedForNoShow"); } }
        [DataMember]
        public InvoiceDestination InvoiceReservationMasterTo { get { return _invoiceReservationMasterTo; } set { Set(ref _invoiceReservationMasterTo, value, "InvoiceReservationMasterTo"); } }
        [DataMember]
        public Guid? TaxRateId { get { return _taxRateId; } set { Set(ref _taxRateId, value, "TaxRateId"); } }
        [DataMember]
        public KindValue? InvoiceCommissionType { get { return _invoiceCommissionType; } set { Set(ref _invoiceCommissionType, value, "InvoiceCommissionType"); } }
        [DataMember]
        public bool FixPricesAtReservation { get { return _fixPricesAtReservation; } set { Set(ref _fixPricesAtReservation, value, "FixPricesAtReservation"); } }
        [DataMember]
        public InvoiceModel InvoiceType { get { return _invoiceType; } set { Set(ref _invoiceType, value, "InvoiceType"); } }
        [DataMember]
        public bool RequiresCheckinAuthorization { get { return _requiresCheckinAuthorization; } set { Set(ref _requiresCheckinAuthorization, value, "RequiresCheckinAuthorization"); } }
        [DataMember]
        public InvoiceOwner InvoiceTo { get { return _invoiceTo; } set { Set(ref _invoiceTo, value, "InvoiceTo"); } }
        [DataMember]
        public bool UseSerieContadoForCreditInvoice { get { return _useSerieContadoForCreditInvoice; } set { Set(ref _useSerieContadoForCreditInvoice, value, "UseSerieContadoForCreditInvoice"); } }
        [DataMember]
        public bool VoucherIsMandatory { get { return _voucherIsMandatory; } set { Set(ref _voucherIsMandatory, value, "VoucherIsMandatory"); } }
        [DataMember]
        public Guid? TaxSchemaId { get { return _taxSchemaId; } set { Set(ref _taxSchemaId, value, "TaxSchemaId"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage="Market origin required.")]
        public Guid? MarketOriginId { get { return _marketOriginId; } set { Set(ref _marketOriginId, value, "MarketOriginId"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage="Market segment required.")]
        public Guid? MarketSegmentId { get { return _marketSegmentId; } set { Set(ref _marketSegmentId, value, "MarketSegmentId"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage="Entity type required.")]
        public Guid? EntityTypeId { get { return _entityTypeId; } set { Set(ref _entityTypeId, value, "EntityTypeId"); } }
        [DataMember]
        public string EntityTypeDesc { get { return _entityTypeDesc; } set { Set(ref _entityTypeDesc, value, "EntityTypeDesc"); } }
        [DataMember]
        public EntityCategory Category { get { return _category; } set { Set(ref _category, value, "Category"); } }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public string ComercialName
        {
            get
            {
                if (Entity != null)
                    return Entity.ComercialName;

                return string.Empty;
            }
        }

        #endregion
        #region Contracts

        [DataMember]
        public EntityContract Entity { get; internal set; }
        
        [ReflectionExclude]
        [DataMember]
        public TypedList<EntityContractRelContract> Contracts { get; set; }

        [ReflectionExclude]
        public TypedList<EntityDepositRequestContract> DepositRequests { get; private set; }

        private CurrentAccountContract _currentAccount;
        [ReflectionExclude]
        [DataMember]
        public CurrentAccountContract CurrentAccount { get { return _currentAccount; } set { Set(ref _currentAccount, value, "CurrentAccount"); } }

        [ReflectionExclude]
        [DataMember]
        public TypedList<EntityExtAccountContract> ExtAccounts { get; private set; }

        #endregion
        #region Constructors

        public EntityInstallationContract(DocumentSign documentSignatureType)
            : base()
        {
			Entity = new EntityContract(documentSignatureType);
            Contracts = new TypedList<EntityContractRelContract>();
            DepositRequests = new TypedList<EntityDepositRequestContract>();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateEntityInstallation(EntityInstallationContract obj)
        {
            if (obj.Entity != null)
            {
				var validations = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
                if (!obj.Entity.ValidateObject(validations))
                    return validations.FirstOrDefault();

                if (string.IsNullOrEmpty(obj.Entity.FiscalAddressContract.Address) &&
					string.IsNullOrEmpty(obj.Entity.HomeAddressContract.Address))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("One Address is Required.");
            }

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
