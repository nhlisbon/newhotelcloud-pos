﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class OwnerContract : ContactContract
    {
        private bool _isClient;
        private bool _disableClient;

        #region Constructor

        public OwnerContract(DateTime today)
            : base(DocumentSign.None, null, today) { }

        #endregion
        #region Public Properties

        [DataMember]
        public Guid? CurrentAccountId { get; set; }
        [ReflectionExclude]
        [DataMember]
        public CurrentAccountContract CurrentAccount { get; set; }

        [DataMember]
        public Guid? CurrentOwnerGroupId { get; set; }
        
        [DataMember]
        public bool IsClient { get { return _isClient; } set { Set(ref _isClient, value, "IsClient"); } }
        [DataMember]
        public Guid ClientTypeId { get; set; }
        [DataMember]
        public bool DisableClient
        {
            get
            {
                  return _disableClient; 
            } 
            set { Set(ref _disableClient, value, "DisableClient"); }
        }

        #endregion
    }
}