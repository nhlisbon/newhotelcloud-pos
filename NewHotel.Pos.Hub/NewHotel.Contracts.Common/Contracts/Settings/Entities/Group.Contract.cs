﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class GroupContract : BaseContract
    {
        #region Members

        private string _description;

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }

        #endregion
        #region Constructor

        public GroupContract()
            : base() { }

        #endregion
    }
}
