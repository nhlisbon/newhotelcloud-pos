﻿using System;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ContactContract), "ValidateContact")]
    [KnownType(typeof(ClientContract))]
    [KnownType(typeof(EmployeeContract))]
    [KnownType(typeof(SalemanContract))]
    [KnownType(typeof(OwnerContract))]
    public class ContactContract : EntityBaseContract
    {
        #region Constants

        private const string defaultFullNameOrder = "15234";

        #endregion
        #region Members

        private Guid? _title;
        private string _middleName;
        private string _lastName;
        private GenderType? _gender;
        private string _countryId;
        private string _countryDescription;
        private Blob? _image;
        private Blob? _documentImageScan;
        private CivilState? _civilStatus;
        private string _enterpriseName;
        private Guid? _professionId;
        private Guid? _jobtitleId;
        private short? _children;
        private DateTime? _youngerChildBirthdate;
        private DateTime? _birthdate;
        private string _fathersName;
        private string _mothersName;
        private Guid? _clientInstallationId;

        [DataMember]
        internal DateTime _today;
        [DataMember]
        internal string _fullNameOrder;

        #endregion
        #region Properties

        [DataMember]
        public Guid? Title { get { return _title; } set { Set(ref _title, value, "Title"); } }
        [DataMember]
        public string MiddleName 
        { 
            get { return _middleName; } 
            set 
            {
                if (Set(ref _middleName, value, "MiddleName"))
                    NotifyPropertyChanged("FullName");
            } 
        }
        [DataMember]
        public string LastName 
        {
            get { return _lastName; } 
            set 
            {
                if (Set(ref _lastName, value, "LastName"))
                    NotifyPropertyChanged("FullName");
            }
        }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Gender required.")]
        public GenderType? Gender
        {
            get { return _gender; }
            set
            {
                if (Set(ref _gender, value, "Gender"))
                    NotifyPropertyChanged("TitleGender");
            }
        }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Civil status required.")]
        public CivilState? CivilStatus { get { return _civilStatus; } set { Set(ref _civilStatus, value, "CivilStatus"); } }
        [DataMember]
        public DateTime? Birthdate
        {
            get { return _birthdate; } 
            set 
            {
                if (Set(ref _birthdate, value, "Birthdate"))
                    NotifyPropertyChanged("Age");
            } 
        }        
        [DataMember]
        public string EnterpriseName { get { return _enterpriseName; } set { Set(ref _enterpriseName, value, "EnterpriseName"); } }
        [DataMember]
        public Guid? ProfessionId { get { return _professionId; } set { Set(ref _professionId, value, "ProfessionId"); } }
        [DataMember]
        public Guid? JobTitleId { get { return _jobtitleId; } set { Set(ref _jobtitleId, value, "JobTitleId"); } }
        [DataMember]
        public short? Children { get { return _children; } set { Set(ref _children, value, "Children"); } }
        [DataMember]
        public DateTime? YoungerChildBirthdate { get { return _youngerChildBirthdate; } set { Set(ref _youngerChildBirthdate, value, "YoungerChildBirthdate"); } }
        [DataMember]
        public string FathersName { get { return _fathersName; } set { Set(ref _fathersName, value, "FathersName"); } }
        [DataMember]
        public string MothersName { get { return _mothersName; } set { Set(ref _mothersName, value, "MothersName"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Country required.")]
        public string CountryId { get { return _countryId; } set { Set(ref _countryId, value, "CountryId"); } }
        [DataMember]
        public string CountryDescription { get { return _countryDescription; } set { Set(ref _countryDescription, value, "CountryDescription"); } }
        [DataMember]
        public Blob? Image { get { return _image; } set { Set(ref _image, value, "Image"); } }
        [DataMember]
        public Blob? DocumentImageScan { get { return _documentImageScan; } set { Set(ref _documentImageScan, value, "DocumentImageScan"); } }
        [DataMember]
        public Guid? ClientInstallationId { get { return _clientInstallationId; } set { Set(ref _clientInstallationId, value, "ClientInstallationId"); } }

        [DataMember]
        public override string Description
        {
            get { return base.Description; } 
            set 
            {
                var changed = base.Description != value;
                base.Description = value;
                if (changed)
					NotifyPropertyChanged("FullName");
            }
        }

        [DataMember]
        public bool SwipeUSLicense { get; set; }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public string TitleGender
        {
            get
            {
                if (Gender.HasValue)
                {
                    switch (Gender.Value)
                    {
                        case GenderType.Male:
                            return "TitleMasc";
                        case GenderType.Female:
                            return "TitleFeme";
                    }
                }

                return "Title";
            }
        }

        [ReflectionExclude]
        public string FirstName
        {
            get { return Description; }
            set { Description = value; }
        }

        [ReflectionExclude]
        public string Names
        {
            get
            {
                var firstName = string.IsNullOrEmpty(FirstName) ? string.Empty : FirstName.Trim();

                if (firstName == string.Empty)
                    return "[Empty]";

                return firstName;
            }
        }

        [ReflectionExclude]
        public string FullName
        {
            get
            {
                var fullName = new StringBuilder();

                var fullNameOrder = _fullNameOrder ?? defaultFullNameOrder;
                for (int i = 1; i <= 5; i++)
                {
                    switch (fullNameOrder.IndexOf(i.ToString()[0]))
                    {
                        case 0:
                            break;
                        case 1:
                            if (!string.IsNullOrEmpty(FirstName))
                            {
                                fullName.Append(" ");
                                fullName.Append(FirstName.Trim());
                            }
                            break;
                        case 2:
                            if (!string.IsNullOrEmpty(LastName))
                            {
                                fullName.Append(" ");
                                fullName.Append(LastName.Trim());
                            }
                            break;
                        case 3:
                            if (!string.IsNullOrEmpty(MiddleName))
                            {
                                fullName.Append(" ");
                                fullName.Append(MiddleName.Trim());
                            }
                            break;
                        case 4:
                            fullName.Append(",");
                            break;
                    }
                }

                if (fullName.Length > 0)
                    fullName = fullName.Remove(0, 1);

                if (fullName.Length > 0)
                    return fullName.ToString();

                return string.Empty;
            }
        }

        [ReflectionExclude]
        public string SurNames
        {
            get
            {
                var surNames = new StringBuilder();

                var fullNameOrder = _fullNameOrder ?? defaultFullNameOrder;
                for (int i = 1; i <= 5; i++)
                {
                    switch (fullNameOrder.IndexOf(i.ToString()[0]))
                    {
                        case 0:
                        case 1:
                        case 4:
                            break;
                        case 2:
                            if (!string.IsNullOrEmpty(LastName))
                            {
                                surNames.Append(" ");
                                surNames.Append(LastName.Trim());
                            }
                            break;
                        case 3:
                            if (!string.IsNullOrEmpty(MiddleName))
                            {
                                surNames.Append(" ");
                                surNames.Append(MiddleName.Trim());
                            }
                            break;
                    }
                }

                if (surNames.Length > 0)
                    return surNames.Remove(0, 1).ToString();

                return "[Empty]";
            }
        }

        [ReflectionExclude]
        public int? Age
        {
            get
            {
                if (_birthdate.HasValue)
                {
                    var years = _today.Year - _birthdate.Value.Year;
                    if ((_today.Month < _birthdate.Value.Month) || ((_today.Month == _birthdate.Value.Month) && (_today.Day < _birthdate.Value.Day)))
                        years--;

                    return years;
                }

                return null;
            }
        }

        [ReflectionExclude]
        public DateTime MinBirthDate
        {
            get { return _today.AddYears(-120); }
        }

        [ReflectionExclude]
        public DateTime MaxBirthDate
        {
            get { return _today; }
        }

        #endregion
        #region Constructors

		public ContactContract(DocumentSign documentSignatureType, string fullNameOrder, DateTime today)
			: base(documentSignatureType)
        {
            _fullNameOrder = fullNameOrder;
            _today = today.Date;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateContact(ContactContract obj)
        {
            if (string.IsNullOrEmpty(obj.Description))
                return new System.ComponentModel.DataAnnotations.ValidationResult("First name required.");
            if (string.IsNullOrEmpty(obj.LastName))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Last name required.");
            if (!obj.Gender.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Gender required.");
            if (string.IsNullOrEmpty(obj.CountryId))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Country required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
        #region Swipe Card

        public void LicenseSwipe(string cardData)
        {
        }

        #endregion
    }
}