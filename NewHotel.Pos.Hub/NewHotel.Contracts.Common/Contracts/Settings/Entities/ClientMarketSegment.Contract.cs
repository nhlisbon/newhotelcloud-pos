﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ClientMarketSegmentContract : BaseContract
    {
        [DataMember]
        public Guid ClientId { get; set; }

        [DataMember]
        public Guid SegmentId { get; set; }

        [DataMember]
        public string SegmentDesc { get; set; }

        public ClientMarketSegmentContract() : base() { }

        public ClientMarketSegmentContract(Guid id, Guid clientId, Guid segmentId, string segmentDesc) : base() 
        {
            Id = id;
            ClientId = clientId;
            SegmentId = segmentId;
            SegmentDesc = segmentDesc;
        }
    }
}
