﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class AttentionContract : BaseContract
    {
        #region Contract Properties
        [DataMember]
        internal LanguageTranslationContract _description;

        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        [ReflectionExclude()]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public string DescriptionTranslated { get; set; }

        #endregion
        #region Constructor

        public AttentionContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
    }
}
