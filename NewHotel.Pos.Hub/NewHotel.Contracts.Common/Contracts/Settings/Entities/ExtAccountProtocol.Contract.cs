﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ExtAccountContract), "ValidateExtAccountProtocol")]
    public class ExtAccountProtocolContract : BaseContract
    {
        #region Members

        private string _description;
        private string _comment;
        private bool _inactive;

        #endregion
        #region Properties

        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public string Comment { get { return _comment; } set { Set(ref _comment, value, "Comment"); } }
        [DataMember]
        public Guid CurrentAccountId { get; set; }
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }

        #endregion
        #region Constructor

        public ExtAccountProtocolContract()
            : base() { }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateExtAccountProtocol(ExtAccountProtocolContract obj)
        {
            if (string.IsNullOrEmpty(obj.Description))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}