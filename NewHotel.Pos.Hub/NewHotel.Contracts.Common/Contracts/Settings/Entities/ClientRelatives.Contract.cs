﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ClientRelativesContract : BaseContract
    {
        #region Members
        private Guid _clientId;
        private Guid _relativeClientId;
        private Guid _relativeTypeId;
        private string _relationTypeDesc;
        private string _relativeClientDesc;
        #endregion

        #region Properties
        [DataMember]
        public Guid ClientId { get { return _clientId; } set { Set(ref _clientId, value, "ClientId"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Client required.")]
        public Guid RelativeClientId { get { return _relativeClientId; } set { Set(ref _relativeClientId, value, "RelativeClientId"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Relation required.")]
        public Guid RelativeTypeId { get { return _relativeTypeId; } set { Set(ref _relativeTypeId, value, "RelativeTypeId"); } }
        [DataMember]
        public string RelationTypeDesc { get { return _relationTypeDesc; } set { Set(ref _relationTypeDesc, value, "RelationTypeDesc"); } }
        [DataMember]
        public string RelativeClientDesc { get { return _relativeClientDesc; } set { Set(ref _relativeClientDesc, value, "RelativeClientDesc"); } }
        #endregion

        public ClientRelativesContract() : base() { }

        public ClientRelativesContract(Guid id, Guid clientId, Guid relativeClientId, Guid relativeTypeId, string relationTypeDesc, string relativeClientDesc) : base() 
        {
            Id = id;
            ClientId = clientId;
            RelativeClientId = relativeClientId;
            RelativeTypeId = relativeTypeId;
            RelationTypeDesc = relationTypeDesc;
            RelativeClientDesc = relativeClientDesc;
        }
    }
}
