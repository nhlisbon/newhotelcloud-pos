﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ProfContract:BaseContract
    {
        #region Members
        private Guid _installation;
        #endregion
        #region Contract Properties

        [DataMember]
        internal LanguageTranslationContract _description;

        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        [ReflectionExclude()]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }
        [DataMember]
        public Guid Installation { get { return _installation; } set { Set(ref _installation, value, "Installation"); } }

        #endregion
        #region Constructor

        public ProfContract()
            : base() 
        {
            _description = new LanguageTranslationContract();
        } 

        #endregion
    }
}
