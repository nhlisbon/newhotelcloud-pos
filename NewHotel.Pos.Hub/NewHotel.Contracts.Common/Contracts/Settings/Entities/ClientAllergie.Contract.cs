﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class ClientAllergieContract : BaseContract
    {
        [DataMember]
        public Guid ClientId { get; set; }

        [DataMember]
        public Guid AllergyId { get; set; }

        [DataMember]
        public string AllergyDescription { get; set; }

        public ClientAllergieContract() : base() { }

        public ClientAllergieContract(Guid id, Guid clientId, Guid allergyId, string allergyDesc) : base()
        {
            Id = id;
            ClientId = clientId;
            AllergyId = allergyId;
            AllergyDescription = allergyDesc;
        }
    }
}