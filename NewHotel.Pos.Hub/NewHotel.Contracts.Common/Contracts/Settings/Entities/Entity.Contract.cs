﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.Collections.Specialized;
using System.ComponentModel;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(EntityContract), "ValidateEntity")]
    public class EntityContract : EntityBaseContract
    {
        #region Members

        private string _comercialName;
        private Guid? _payableAccountId;
        private string _payableAccountDescription;
        private Guid? _receivableAccountId;
        private string _receivableAccountDescription;
        private bool _active;
        private short? _mandatoryDepositDays;
        private AdvancedDepositType _depositType;
        private decimal? _depositValue;
        private short? _daysForDeadLine;
        public bool _inactive;
        private bool _unproduceCommission;
        private short? _daysGuaranteedForNoShow;
        private bool _isWholesale;
        private Guid? _wholesalerEntityId;
        private string _wholesalerEntityDescription;
        private bool _isRepresentative;
        private Guid? _representativeEntityId;
        private string _representativeEntityDescription;
        private InvoiceDestination _invoiceReservationMasterTo;
        private InvoiceDestination _invoiceReservationExtra1To;
        private InvoiceDestination _invoiceReservationExtra2To;
        private InvoiceDestination _invoiceReservationExtra3To;
        private InvoiceDestination _invoiceReservationExtra4To;
        private InvoiceDestination _invoiceReservationExtra5To;
        private Guid? _taxRateId;
        private KindValue _invoiceCommissionType;
        private bool _fixPricesAtReservation;
        private InvoiceModel _invoiceType;
        private bool _requiresCheckinAuthorization;
        private InvoiceOwner _invoiceTo;
        private bool _useSerieContadoForCreditInvoice;
        private bool _voucherIsMandatory;
        private Guid? _taxSchemaId;
        private Guid? _marketOriginId;
        private Guid? _marketSegmentId;
        private Guid? _entityTypeId;
        private string _entityTypeDesc;
        private EntityCategory _category;
        private string _iATANumber;
        private bool _publicEntity;
        private bool _cityTaxApplicavel;
        private Guid? _supplementSchemaId;
        private Guid? _salesmanId;
        private Guid? _ownerGroupId;
        private string _icmsRegistrationCode;
        private string _issRetentionCode;

        private bool _checkLimitOnCreate;
        private bool _checkLimitOnCheckIn;
        private bool _checkLimitOnCheckOut;
        private bool _checkLimitOnInvoice;
        private bool _checkLimitOverflowAllowed;
        private bool _checkLimitReservationAcc;
        private bool _underSupervision;
        private string _checkLimitNotificationMail;

        private Guid? _taxRegion;
        private string _countryTax;

        private bool _commissionIncludeRoom;
        private bool _commissionIncludeMeal;
        private bool _commissionIncludeServiceRate;
        private bool _commissionIncludeIncidentalCharge;

        private CurrentAccountContract _currentAccount;

        private TypedList<EntityCommissionContract> _commissions;

        #endregion
        #region Properties

        [DataMember]
        [DataAnnotations.Required(ErrorMessage="Commercial name required.")]
        public string ComercialName { get { return _comercialName; } set { Set(ref _comercialName, value, "ComercialName"); } }
        [DataMember]
        public string CountryId { get; set; }
        [DataMember]
        public string CountryDescription { get; set; }
        [DataMember]
        public short? MandatoryDepositDays { get { return _mandatoryDepositDays; } set { Set(ref _mandatoryDepositDays, value, "MandatoryDepositDays"); } }
        [DataMember]
        public AdvancedDepositType DepositType { get { return _depositType; } set { Set(ref _depositType, value, "DepositType"); } }
        [DataMember]
        public decimal? DepositValue { get { return _depositValue; } set { Set(ref _depositValue, value, "DepositValue"); } }       
        [DataMember]
        public short? DaysForDeadLine { get { return _daysForDeadLine; } set { Set(ref _daysForDeadLine, value, "DaysForDeadLine"); } }       
        [DataMember]
        public bool UnproduceCommission { get { return _unproduceCommission; } set { Set(ref _unproduceCommission, value, "UnproduceCommission"); } }
        [DataMember]
        public short? DaysGuaranteedForNoShow { get { return _daysGuaranteedForNoShow; } set { Set(ref _daysGuaranteedForNoShow, value, "DaysGuaranteedForNoShow"); } }
        [DataMember]
        public bool IsWholesale { get { return _isWholesale; } set { Set(ref _isWholesale, value, "IsWholesale"); } }
        [DataMember]
        public Guid? WholesalerEntityId { get { return _wholesalerEntityId; } set { Set(ref _wholesalerEntityId, value, "WholesalerEntityId"); } }
        [DataMember]
        public string WholesalerEntityDescription { get { return _wholesalerEntityDescription; } set { Set(ref _wholesalerEntityDescription, value, "WholesalerEntityDescription"); } }
        [DataMember]
        public bool IsRepresentative { get { return _isRepresentative; } set { Set(ref _isRepresentative, value, "IsRepresentative"); } }
        [DataMember]
        public Guid? RepresentativeEntityId { get { return _representativeEntityId; } set { Set(ref _representativeEntityId, value, "RepresentativeEntityId"); } }
        [DataMember]
        public string RepresentativeEntityDescription { get { return _representativeEntityDescription; } set { Set(ref _representativeEntityDescription, value, "RepresentativeEntityDescription"); } }
        [DataMember]
        public InvoiceDestination InvoiceReservationMasterTo { get { return _invoiceReservationMasterTo; } set { Set(ref _invoiceReservationMasterTo, value, "InvoiceReservationMasterTo"); } }
        [DataMember]
        public InvoiceDestination InvoiceReservationExtra1To { get { return _invoiceReservationExtra1To; } set { Set(ref _invoiceReservationExtra1To, value, "InvoiceReservationExtra1To"); } }
        [DataMember]
        public InvoiceDestination InvoiceReservationExtra2To { get { return _invoiceReservationExtra2To; } set { Set(ref _invoiceReservationExtra2To, value, "InvoiceReservationExtra2To"); } }
        [DataMember]
        public InvoiceDestination InvoiceReservationExtra3To { get { return _invoiceReservationExtra3To; } set { Set(ref _invoiceReservationExtra3To, value, "InvoiceReservationExtra3To"); } }
        [DataMember]
        public InvoiceDestination InvoiceReservationExtra4To { get { return _invoiceReservationExtra4To; } set { Set(ref _invoiceReservationExtra4To, value, "InvoiceReservationExtra4To"); } }
        [DataMember]
        public InvoiceDestination InvoiceReservationExtra5To { get { return _invoiceReservationExtra5To; } set { Set(ref _invoiceReservationExtra5To, value, "InvoiceReservationExtra5To"); } }
        [DataMember]
        public Guid? TaxRateId { get { return _taxRateId; } set { Set(ref _taxRateId, value, "TaxRateId"); } }
        [DataMember]
        public KindValue InvoiceCommissionType { get { return _invoiceCommissionType; } set { Set(ref _invoiceCommissionType, value, "InvoiceCommissionType"); } }
        [DataMember]
        public bool FixPricesAtReservation { get { return _fixPricesAtReservation; } set { Set(ref _fixPricesAtReservation, value, "FixPricesAtReservation"); } }
        [DataMember]
        public InvoiceModel InvoiceType { get { return _invoiceType; } set { Set(ref _invoiceType, value, "InvoiceType"); } }
        [DataMember]
        public bool RequiresCheckinAuthorization { get { return _requiresCheckinAuthorization; } set { Set(ref _requiresCheckinAuthorization, value, "RequiresCheckinAuthorization"); } }
        [DataMember]
        public InvoiceOwner InvoiceTo { get { return _invoiceTo; } set { Set(ref _invoiceTo, value, "InvoiceTo"); } }
        [DataMember]
        public bool UseSerieContadoForCreditInvoice { get { return _useSerieContadoForCreditInvoice; } set { Set(ref _useSerieContadoForCreditInvoice, value, "UseSerieContadoForCreditInvoice"); } }
        [DataMember]
        public bool VoucherIsMandatory { get { return _voucherIsMandatory; } set { Set(ref _voucherIsMandatory, value, "VoucherIsMandatory"); } }
        [DataMember]
        public Guid? TaxSchemaId { get { return _taxSchemaId; } set { Set(ref _taxSchemaId, value, "TaxSchemaId"); } }
        [DataMember]
        [DataAnnotations.Required(ErrorMessage = "Market origin required.")]
        public Guid? MarketOriginId { get { return _marketOriginId; } set { Set(ref _marketOriginId, value, "MarketOriginId"); } }
        [DataMember]
        [DataAnnotations.Required(ErrorMessage = "Market segment required.")]
        public Guid? MarketSegmentId { get { return _marketSegmentId; } set { Set(ref _marketSegmentId, value, "MarketSegmentId"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Entity type required.")]
        public Guid? EntityTypeId { get { return _entityTypeId; } set { Set(ref _entityTypeId, value, "EntityTypeId"); } }
        [DataMember]
        public string EntityTypeDesc { get { return _entityTypeDesc; } set { Set(ref _entityTypeDesc, value, "EntityTypeDesc"); } }
        [DataMember]
        public EntityCategory Category { get { return _category; } set { Set(ref _category, value, "Category"); } }

        [DataMember]
        public bool CommissionIncludeRoom  { get { return _commissionIncludeRoom; } set { Set(ref _commissionIncludeRoom, value, "CommissionIncludeRoom"); } }
        [DataMember]
        public bool CommissionIncludeMeal  { get { return _commissionIncludeMeal; } set { Set(ref _commissionIncludeMeal, value, "CommissionIncludeMeal"); } }
        [DataMember]
        public bool CommissionIncludeServiceRate  { get { return _commissionIncludeServiceRate; } set { Set(ref _commissionIncludeServiceRate, value, "CommissionIncludeServiceRate"); } }
        [DataMember]
        public bool CommissionIncludeIncidentalCharge  { get { return _commissionIncludeIncidentalCharge; } set { Set(ref _commissionIncludeIncidentalCharge, value, "CommissionIncludeIncidentalCharge"); } }

        [DataMember]
        public string IATANumber { get { return _iATANumber; } set { Set(ref _iATANumber, value, "IATANumber"); } }
        [DataMember]
        public bool PublicEntity { get { return _publicEntity; } set { Set(ref _publicEntity, value, "PublicEntity"); } }
        [DataMember]
        public bool CityTaxApplicavel { get { return _cityTaxApplicavel; } set { Set(ref _cityTaxApplicavel, value, "CityTaxApplicavel"); } }
        [DataMember]
        public Guid? SupplementSchemaId { get { return _supplementSchemaId; } set { Set(ref _supplementSchemaId, value, "SupplementSchemaId"); } }
        [DataMember]
        public Guid? SalesmanId { get { return _salesmanId; } set { Set(ref _salesmanId, value, "SalesmanId"); } }
        [DataMember]
        public Guid? OwnerGroupId { get { return _ownerGroupId; } set { Set(ref _ownerGroupId, value, "OwnerGroupId"); } }

        [DataMember]
        public string IcmsRegistrationCode { get { return _icmsRegistrationCode; } set { Set(ref _icmsRegistrationCode, value, "IcmsRegistrationCode"); } }
        [DataMember]
        public string IssRetentionCode { get { return _issRetentionCode; } set { Set(ref _issRetentionCode, value, "IssRetentionCode"); } }

        #region Credit Limit Control

        [DataMember]
        public bool CheckLimitOnCreate { get { return _checkLimitOnCreate; } set { Set(ref _checkLimitOnCreate, value, "CheckLimitOnCreate"); } }
        [DataMember]
        public bool CheckLimitOnCheckIn { get { return _checkLimitOnCheckIn; } set { Set(ref _checkLimitOnCheckIn, value, "CheckLimitOnCheckIn"); } }
        [DataMember]
        public bool CheckLimitOnCheckOut { get { return _checkLimitOnCheckOut; } set { Set(ref _checkLimitOnCheckOut, value, "CheckLimitOnCheckOut"); } }
        [DataMember]
        public bool CheckLimitOnInvoice { get { return _checkLimitOnInvoice; } set { Set(ref _checkLimitOnInvoice, value, "CheckLimitOnInvoice"); } }
        [DataMember]
        public bool CheckLimitOverflowAllowed { get { return _checkLimitOverflowAllowed; } set { Set(ref _checkLimitOverflowAllowed, value, "CheckLimitOverflowAllowed"); } }
        [DataMember]
        public bool CheckLimitReservationAcc { get { return _checkLimitReservationAcc; } set { Set(ref _checkLimitReservationAcc, value, "CheckLimitReservationAcc"); } }
        [DataMember]
        public bool UnderSupervision { get { return _underSupervision; } set { Set(ref _underSupervision, value, "UnderSupervision"); } }
        [DataMember]
        public string CheckLimitNotificationMail { get { return _checkLimitNotificationMail; } set { Set(ref _checkLimitNotificationMail, value, "CheckLimitNotificationMail"); } }

        #endregion
        #region Credit Limit Visual Info

        [DataMember]
        public decimal CreditServices { get; set; }
        [DataMember]
        public decimal CreditPayments { get; set; }
        [DataMember]
        public decimal CreditTransfers { get; set; }
        [DataMember]
        public decimal CreditExpired { get; set; }

        #endregion

        [ReflectionExclude]
        [DataMember]
        public CurrentAccountContract CurrentAccount { get { return _currentAccount; } set { Set(ref _currentAccount, value, "CurrentAccount"); } }

        #endregion
        #region EntityComercialInstallation Properties

        [DataMember]
        public Guid? PayableAccountId { get { return _payableAccountId; } set { Set(ref _payableAccountId, value, "PayableAccountId"); } }
        [DataMember]
        public string PayableAccountDescription { get { return _payableAccountDescription; } set { Set(ref _payableAccountDescription, value, "PayableAccountDescription"); } }
        [DataMember]
        public Guid? ReceivableAccountId { get { return _receivableAccountId; } set { Set(ref _receivableAccountId, value, "ReceivableAccountId"); } }
        [DataMember]
        public string ReceivableAccountDescription { get { return _receivableAccountDescription; } set { Set(ref _receivableAccountDescription, value, "ReceivableAccountDescription"); } }
        [DataMember]
        public bool Active { get { return _active; } set { Set(ref _active, value, "Active"); } }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public decimal CreditCurrentRisk
        {
            get { return CreditServices - CreditPayments - CreditExpired; }
        }

        [ReflectionExclude]
        public bool HighRisk
        {
            get
            {
                if (CreditLimit.HasValue)
                    return CreditCurrentRisk > CreditLimit.Value;

                return false;
            }
        }

        [ReflectionExclude]
        public int DescriptionMaxLength
        {
            get
            {
                switch (DocumentSignatureType)
                {
                    case DocumentSign.FiscalizationBrazilCMFLEX:
                        return 60;
                }

                return 255;
            }
        }

        #endregion
        #region Settings

        [DataMember]
        public string CountryTax { get { return _countryTax; } set { Set(ref _countryTax, value, "CountryTax"); } }
        [DataMember]
        public Guid? TaxRegion { get { return _taxRegion; } set { Set(ref _taxRegion, value, "TaxRegion"); } }

        #endregion
        #region Lists

        [ReflectionExclude]
        public TypedList<EntityDepositRequestContract> DepositRequests { get; set; }

        [ReflectionExclude]
        public TypedList<EntityRelationContract> Relations { get; set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<EntityContractRelContract> Contracts { get; set; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<EntityCommissionContract> Commissions
        {
            get { return _commissions; }
            set
            {
                _commissions = value;
                foreach (var commission in _commissions)
                    commission.PropertyChanged += OnCommissionIsDefaultChanged;

                _commissions.CollectionChanged += (s, e) =>
                {
                    if (e.Action != NotifyCollectionChangedAction.Remove)
                    {
                        foreach (var commission in e.NewItems.Cast<EntityCommissionContract>())
                            commission.PropertyChanged += OnCommissionIsDefaultChanged;
                    }
                };
            }
        }

        [ReflectionExclude]
        [DataMember]
        public TypedList<EntityExtAccountContract> ExtAccounts { get; set; }

        #endregion
        #region Constructors

        public EntityContract(DocumentSign documentSignatureType)
            : base(documentSignatureType)
        {
            _commissionIncludeRoom = true;
            _commissionIncludeMeal = true;
            _commissionIncludeServiceRate = true;
            InvoiceCommissionType = KindValue.Gross;
            DepositRequests = new TypedList<EntityDepositRequestContract>();
            Relations = new TypedList<EntityRelationContract>();
            Contracts = new TypedList<EntityContractRelContract>();
            Commissions = new TypedList<EntityCommissionContract>();
            ExtAccounts = new TypedList<EntityExtAccountContract>();
        }

        #endregion
        #region Private Methods

        private void OnCommissionIsDefaultChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsDefault")
            {
                var contract = (EntityCommissionContract)sender;
                if (contract.IsDefault)
                {
                    foreach (var commission in _commissions.Where(c => !c.Id.Equals(contract.Id)))
                        commission.IsDefault = false;
                }
            }
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateEntity(EntityContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}