﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(EmployeeRoleContract), "ValidateEmployeeRole")]
    public class EmployeeRoleContract : BaseContract<long>
    {
        #region Contract Properties

        [ReflectionExclude]
        [DataMember]
        public LanguageTranslationContract Description { get; set; }

        [ReflectionExclude]
        [DataMember]
        public LanguageTranslationContract DepartamentDescription { get; set; }
        

        #endregion
        #region Constructor

        public EmployeeRoleContract()
            : base()
        {
            Description = new LanguageTranslationContract();
            DepartamentDescription = new LanguageTranslationContract();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateEmployeeRole(EmployeeRoleContract obj)
        {
            if (obj.Description.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description required.");
            if (obj.DepartamentDescription.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
