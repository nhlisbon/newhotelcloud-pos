﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class EntityDocumentsContract : BaseContract
    {
        #region Members
        private Guid? _installationId;
        #endregion

        #region Extended Properties

        [ReflectionExclude]
        public long? DocumentTypeId
        {
            get { return Document.DocTypeId; }
        }

        [ReflectionExclude]
        public string DocumentTypeDesc
        {
            get { return Document.DocTypeDescription; }
        }

        #endregion
        #region Contracts

        [ReflectionExclude]
        public DocumentContract Document { get; private set; }

        [DataMember]
        public Guid? InstallationId { get { return _installationId; } set { Set(ref _installationId, value, "InstallationId"); } }

        #endregion
        #region Constructors

        public EntityDocumentsContract(DocumentContract document)
            : base()
        {
            Document = document;
        }

        #endregion
    }
}
