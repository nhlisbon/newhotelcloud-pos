﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
	[DataContract]
	[Serializable]
	[KnownType(typeof(EntityContract))]
	[KnownType(typeof(BankContract))]
    [KnownType(typeof(ExtAccountContract))]
    [CustomValidation(typeof(EntityBaseContract), "ValidateBaseEntity")]
	public class EntityBaseContract : BaseContract, IEntityBaseContract
	{
		#region Members

		[DataMember]
		internal string[] _docTypeTitles;
		[DataMember]
		internal DocumentSign _documentSignatureType;

		private string _description;
		private string _abbreviation;
		private long? _allLanguages;
		private string _freeCode;
		private Guid _installationId;
		private string _homePhone;
		private string _mobilePhone;
		private string _faxNumber;
		private string _emailAddress;
		private string _personalHomePage;
		private string _skypeName;
        private string _comment;
		private string _fiscalNumber;
        private string _fiscalRegister;
        private string _fiscalRegimeTypeCode;
        private string _fiscalResponsabilityCode;
        private string _economicActivity;
		private ARGBColor? _colorForPlanning;
		private bool _inactive;
        private bool _mailAutorization;
        private bool _isOwner;
        private decimal? _creditLimit;
        private bool _creditBlocked;
        private bool _fiscalNotePreffered;
        private bool _invoiceXML;
        private Guid? _retentionSchemaId;

        #endregion
        #region Properties

        [DataMember]
		[System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.")]
		public virtual string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
		[DataMember]
		public string Abbreviation { get { return _abbreviation; } set { Set(ref _abbreviation, value, "Abbreviation"); } }
		[DataMember]
		public long? AllLanguages { get { return _allLanguages; } set { Set(ref _allLanguages, value, "AllLanguages"); } }
		[DataMember]
		public string FreeCode { get { return _freeCode; } set { Set(ref _freeCode, value, "FreeCode"); } }
		[DataMember]
		public Guid InstallationId { get { return _installationId; } set { Set(ref _installationId, value, "InstallationId"); } }

		public void GetAddress(TypeAddress tipo, string address1, string address2, string doorNumber, string country, string postalCode, string location, Guid? districtId)
		{
			if (tipo == TypeAddress.HomeTypeAddress)
				HomeAddressContract = new HomeAddress(address1, address2, doorNumber, country, postalCode, location, districtId);
			else
				FiscalAddressContract = new FiscalAddress(address1, address2, doorNumber, country, postalCode, location, districtId);
		}
		[DataMember]
		public string HomePhone { get { return _homePhone; } set { Set(ref _homePhone, value, "HomePhone"); } }
		[DataMember]
		public string MobilePhone { get { return _mobilePhone; } set { Set(ref _mobilePhone, value, "MobilePhone"); } }
		[DataMember]
		public string FaxNumber { get { return _faxNumber; } set { Set(ref _faxNumber, value, "FaxNumber"); } }
		[DataMember]
		public string EmailAddress { get { return _emailAddress; } set { Set(ref _emailAddress, value, "EmailAddress"); } }
		[DataMember]
		public string PersonalHomePage { get { return _personalHomePage; } set { Set(ref _personalHomePage, value, "PersonalHomePage"); } }
		[DataMember]
		public string SkypeName { get { return _skypeName; } set { Set(ref _skypeName, value, "SkypeName"); } }
        [DataMember]
        public string Comment { get { return _comment; } set { Set(ref _comment, value, "Comment"); } }
		[DataMember]
        public string FiscalNumber { get { return _fiscalNumber; } set { Set(ref _fiscalNumber, value != null ? value.Trim() : value, "FiscalNumber"); } }
        [DataMember]
        public string FiscalRegister { get { return _fiscalRegister; } set { Set(ref _fiscalRegister, value != null ? value.Trim() : value, "FiscalRegister"); } }
        [DataMember]
        public string FiscalRegimeTypeCode { get { return _fiscalRegimeTypeCode; } set { Set(ref _fiscalRegimeTypeCode, value, "FiscalRegimeTypeCode"); } }
        [DataMember]
        public string FiscalResponsabilityCode { get { return _fiscalResponsabilityCode; } set { Set(ref _fiscalResponsabilityCode, value, "FiscalResponsabilityCode"); } }
		[DataMember]
		public string InsuranceDoc { get; set; }
		[DataMember]
		public string EconomicActivity { get { return _economicActivity; } set { Set(ref _economicActivity, value, "EconomicActivity"); } }
		[DataMember]
		public ARGBColor? ColorForPlanning { get { return _colorForPlanning; } set { Set(ref _colorForPlanning, value, "ColorForPlanning"); } }
		[DataMember]
		public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }
        [DataMember]
        public bool MailAutorization { get { return _mailAutorization; } set { Set(ref _mailAutorization, value, "MailAutorization"); } }
        [DataMember]
        public bool IsOwner { get { return _isOwner; } set { Set(ref _isOwner, value, "IsOwner"); } }
        [DataMember]
        public decimal? CreditLimit { get { return _creditLimit; } set { Set(ref _creditLimit, value, "CreditLimit"); } }
        [DataMember]
        public bool CreditBlocked { get { return _creditBlocked; } set { Set(ref _creditBlocked, value, "CreditBlocked"); } }
        [DataMember]
        public bool FiscalNotePreffered { get { return _fiscalNotePreffered; } set { Set(ref _fiscalNotePreffered, value, "FiscalNotePreffered"); } }
        [DataMember]
        public bool InvoiceXML { get { return _invoiceXML; } set { Set(ref _invoiceXML, value, "InvoiceXML"); } }
        [DataMember]
        public bool AllowFreeCodeEdit { get; set; }
        [DataMember]
        public bool DisableFiscalNumberEditing { get; set; }
        [DataMember]
        public bool EnableNifApiRequest { get; set; }
        [DataMember]
        public Guid? RetentionSchemaId { get { return _retentionSchemaId; } set { Set(ref _retentionSchemaId, value, "RetentionSchemaId"); } }
        [DataMember]
        public bool ApplyTaxRetention { get; set; }
        [DataMember]
        public DateTime LastAccess { get; set; }

        [ReflectionExclude]
		public DocumentSign DocumentSignatureType
		{
			get { return _documentSignatureType; }
            set { Set(ref _documentSignatureType, value, "DocumentSignatureType"); }
		}

        [ReflectionExclude]
        public bool IsInstallation { get { return false; } }

        #endregion
        #region Contracts

        [ReflectionExclude]
		public string[] DocTypeTitles { get { return _docTypeTitles; } }
        [ReflectionExclude]
		public TypedList<EntityUserContract> Users { get; private set; }

        [DataMember]
		public TypedList<EntityBankAccountContract> BankAccounts { get; set; }
        [DataMember]
        public TypedList<EntityContactContract> EntityContacts { get; set; }

        [DataMember]
		public IdentityDocContract IdentityDoc { get; set; }
		[DataMember]
		public IdentityDocContract PassportDoc { get; set; }
		[DataMember]
		public IdentityDocContract DriverLicenceDoc { get; set; }
        [DataMember]
		public IdentityDocContract ResidenceCertificateDoc { get; set; }
		[DataMember]
		public PersonalDocContract OtherDoc { get; set; }

        [DataMember]
        public TypeAddressContract HomeAddressContract { get; set; }
        [DataMember]
		public TypeAddressContract FiscalAddressContract { get; set; }

        [ReflectionExclude]
        public TypedList<CreditCardContract> CreditCards { get; private set; }

        #endregion
        #region Constructors

        public EntityBaseContract(DocumentSign documentSignatureType)
			: base()
		{
			_documentSignatureType = documentSignatureType;
            BankAccounts = new TypedList<EntityBankAccountContract>();
            EntityContacts = new TypedList<EntityContactContract>();
			Users = new TypedList<EntityUserContract>();
			_docTypeTitles = new string[8];
			IdentityDoc = new IdentityDocContract((long)IdentityDocType.IdentityDocument);
			PassportDoc = new IdentityDocContract((long)IdentityDocType.Passport);
			DriverLicenceDoc = new IdentityDocContract((long)IdentityDocType.DriverLicence);
			ResidenceCertificateDoc = new IdentityDocContract((long)IdentityDocType.ResidenceCertificate);
			OtherDoc = new PersonalDocContract((long)PersonalDocType.Other);
            HomeAddressContract = new TypeAddressContract();
            FiscalAddressContract = new TypeAddressContract();
            CreditCards = new TypedList<CreditCardContract>();
        }

		public EntityBaseContract()
			: this(DocumentSign.None) { }

        #endregion
        #region Public Methods

        public void CopyAddress(bool home)
		{
			if (home)
			{
				FiscalAddressContract.Address1 = HomeAddressContract.Address1;
				FiscalAddressContract.Address2 = HomeAddressContract.Address2;
                FiscalAddressContract.DoorNumber = HomeAddressContract.DoorNumber;
				FiscalAddressContract.Country = HomeAddressContract.Country;
				FiscalAddressContract.DistrictId = HomeAddressContract.DistrictId;
				FiscalAddressContract.Location = HomeAddressContract.Location;
				FiscalAddressContract.PostalCode = HomeAddressContract.PostalCode;
			}
			else
			{
				HomeAddressContract.Address1 = FiscalAddressContract.Address1;
				HomeAddressContract.Address2 = FiscalAddressContract.Address2;
                HomeAddressContract.DoorNumber = FiscalAddressContract.DoorNumber;
				HomeAddressContract.Country = FiscalAddressContract.Country;
				HomeAddressContract.DistrictId = FiscalAddressContract.DistrictId;
				HomeAddressContract.Location = FiscalAddressContract.Location;
				HomeAddressContract.PostalCode = FiscalAddressContract.PostalCode;
			}
		}

		#endregion
		#region Validations

		public static System.ComponentModel.DataAnnotations.ValidationResult ValidateBaseEntity(EntityBaseContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
		}

		#endregion
	}
}