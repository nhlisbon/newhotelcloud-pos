﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ClientTypeContract : BaseContract
    {
        #region Members

        private string _externalCode;

        [DataMember]
        internal LanguageTranslationContract _description;

        #endregion
        #region Properties

        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        public LanguageTranslationContract Description { get { return _description; } set { _description = value; } }
        [DataMember]
        public string ExternalCode { get { return _externalCode; } set { Set(ref _externalCode, value, "ExternalCode"); } }

        #endregion
        #region Constructor

        public ClientTypeContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
    }
}