﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    /// <summary>
    /// Base Resource Type Contract
    /// </summary>
    [DataContract]
	[Serializable]
    [KnownType(typeof(AdditionalResourceTypeContract))]
    [KnownType(typeof(RoomTypeContract))]
    public class ResourceTypeContract : BaseContract
    {
        #region Members

        private string _abbreviation;
        private long _applicationId;
        private bool _inactive;
        private bool _unisex;
        private ResourceUseMode _usedMode;
        private ResourceOccupationControlType _overBookingControlMode;
        private decimal? _criticalOccupationPercent;
        private short? _maxReservation;
        private short? _maxPax;
        private bool _insertionLock;
        private bool _allowMultiocupation;
        private ARGBColor? _planningColor;

        [DataMember]
        internal LanguageTranslationContract _description;
        [DataMember]
        internal LanguageTranslationContract _observations;

        #endregion
        #region Properties

        /// <summary>
        /// Abbreviation
        /// </summary>
        [DataMember]
        public string Abbreviation
        {
            get { return _abbreviation; }
            set { Set(ref _abbreviation, value, "Abbreviation"); }
        }

        /// <summary>
        /// Application ID (1.PMS, 2.POS, ect.)
        /// </summary>
        [DataMember]
        public long ApplicationId
        {
            get { return _applicationId; }
            set { Set(ref _applicationId, value, "ApplicationId"); }
        }
        /// <summary>
        /// flag: Inactive
        /// </summary>
        [DataMember]
        public bool Inactive
        {
            get { return _inactive; }
            set { Set(ref _inactive, value, "Inactive"); }
        }
        /// <summary>
        /// flag: Resource Unisex
        /// </summary>
        [DataMember]
        public bool Unisex
        {
            get { return _unisex; }
            set { Set(ref _unisex, value, "Unisex"); }
        }
        /// <summary>
        /// Mode (Daily; Hour)
        /// </summary>
        [DataMember]
        public ResourceUseMode UsedMode
        {
            get { return _usedMode; }
            set { Set(ref _usedMode, value, "UsedMode"); }
        }
        /// <summary>
        /// Overbooking Control (Occupancy % or Nº Max. Reservation)
        /// </summary>
        [DataMember]
        public ResourceOccupationControlType OverbookingControlMode
        {
            get { return _overBookingControlMode; }
            set { Set(ref _overBookingControlMode, value, "OverbookingControlMode"); }
        }
        /// <summary>
        /// Critical Occupancy %
        /// </summary>
        [DataMember]
        public decimal? CriticalOccupationPercent
        {
            get { return _criticalOccupationPercent; }
            set { Set(ref _criticalOccupationPercent, value, "CriticalOccupationPercent"); }
        }
        /// <summary>
        /// Nº Max. Reservation
        /// </summary>
        [DataMember]
        public short? MaxReservation
        {
            get { return _maxReservation; }
            set { Set(ref _maxReservation, value, "MaxReservation"); }
        }
        /// <summary>
        /// flag: Stop Reservations
        /// </summary>
        [DataMember]
        public bool InsertionLock
        {
            get { return _insertionLock; }
            set { Set(ref _insertionLock, value, "InsertionLock"); }
        }
        /// <summary>
        /// flag: Allow Shared Room
        /// </summary>
        [DataMember]
        public bool AllowMultiocupation
        {
            get { return _allowMultiocupation; }
            set { Set(ref _allowMultiocupation, value, "AllowMultiocupation"); }
        }
        /// <summary>
        /// Color in Planning
        /// </summary>
        [DataMember]
        public ARGBColor? PlanningColor
        {
            get { return _planningColor; }
            set { Set(ref _planningColor, value, "PlanningColor"); }
        }
        /// <summary>
        /// Nº Max. Paxs
        /// </summary>
        [DataMember]
        public short? MaxPax
        {
            get { return _maxPax; }
            set { Set(ref _maxPax, value, "MaxPax"); }
        }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public string ResaourceTypeDesc
        {
            get { return _description; }
        }

        #endregion
        #region Contracts

        /// <summary>
        /// Description Translation
        /// </summary>
        /// <seealso cref="NewHotel.Contracts.LanguageTranslationContract"/>
        [ReflectionExclude]
        public LanguageTranslationContract Description { get { return _description; } }

        /// <summary>
        /// Remarks Translation
        /// </summary>
        /// <seealso cref="NewHotel.Contracts.LanguageTranslationContract"/>
        [ReflectionExclude]
        public LanguageTranslationContract Observations { get { return _observations; } }

        #endregion
        #region Constructors

        public ResourceTypeContract()
            : base()
        {
            _description = new LanguageTranslationContract();
            _observations = new LanguageTranslationContract();
        }

        #endregion
    }
}