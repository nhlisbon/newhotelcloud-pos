﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class AdditionalFixedContract : ResourceBaseContract
    {
        #region Members
        private Guid _resourceId;
        private Guid _aditionalId;
        private  short _quantity;
        #endregion

        #region Properties
        [DataMember]
        public Guid ResourceId
        {
            get { return _resourceId; }
            set { Set(ref _resourceId, value, "ResourceId"); }
        }

        [DataMember]
        public Guid AditionalId
        {
            get { return _aditionalId; }
            set { Set(ref _aditionalId, value, "AditionalId"); }
        }

        [DataMember]
        public short Quantity
        {
            get { return _quantity; }
            set { Set(ref _quantity, value, "Quantity"); }
        }

        #endregion
        #region Constructors

        public AdditionalFixedContract()
            : base()
        {}

        #endregion
    }
}
