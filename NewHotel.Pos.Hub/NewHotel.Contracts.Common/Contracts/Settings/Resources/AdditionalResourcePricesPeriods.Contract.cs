﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(AdditionalResourcePricesPeriodsContract), "ValidateAdditionalResourcePricesPeriods")]

    public class AdditionalResourcePricesPeriodsContract : BaseContract
    {
        #region Members
        private Guid _additionalId;
        private DateTime _initialDate;
        private DateTime _finalDate;
        #endregion
        #region Properties
        [DataMember]
        public Guid AdditionalId
        {
            get { return _additionalId; }
            set { Set(ref _additionalId, value, "AdditionalId"); }
        }
        [DataMember]
        public DateTime InitialDate
        {
            get { return _initialDate; }
            set { Set(ref _initialDate, value, "InitialDate"); }
        }
        [DataMember]
        public DateTime FinalDate
        {
            get { return _finalDate; }
            set { Set(ref _finalDate, value, "FinalDate"); }
        }
        #endregion
        #region Dummy Properties

        public string FirstDate { get { return InitialDate.ToString("dd-MM-yy"); } }
        public string LastDate { get { return FinalDate.ToString("dd-MM-yy"); } }

        #endregion

        #region Lists
        private TypedList<AdditionalResourcePricesContract> _prices;
        [ReflectionExclude]
        public TypedList<AdditionalResourcePricesContract> Prices
        {
            get { return _prices; }
        }
        #endregion
        #region Constructors

        public AdditionalResourcePricesPeriodsContract()
            : base()
        {
            _prices = new TypedList<AdditionalResourcePricesContract>();
        }

        public AdditionalResourcePricesPeriodsContract(Guid id, Guid additionalId, 
                                                       DateTime initialDate, DateTime finalDate)
            : base()
        {
            Id = id;
            _additionalId = additionalId;
            _initialDate = initialDate;
            _finalDate = finalDate;
            _prices = new TypedList<AdditionalResourcePricesContract>();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateAdditionalResourcePricesPeriods(AdditionalResourcePricesPeriodsContract obj)
        {
            if (obj.InitialDate > obj.FinalDate) return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid Period");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
