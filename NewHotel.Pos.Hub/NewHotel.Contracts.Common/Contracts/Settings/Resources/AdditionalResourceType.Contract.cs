﻿using System;
using System.Runtime.Serialization;
    
namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class AdditionalResourceTypeContract : ResourceTypeContract
    {
        #region Constructor

        public AdditionalResourceTypeContract()
            : base() { }

        #endregion
    }
}