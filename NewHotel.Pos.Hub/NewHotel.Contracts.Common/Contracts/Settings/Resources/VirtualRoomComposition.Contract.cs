﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class VirtualRoomCompositionContract : BaseContract
    {
        #region Members

        private Guid _virtualRoomId;
        private Guid _roomId;
        private string _roomDoor;
        private string _roomType;

        #endregion
        #region Properties

        [DataMember]
        public Guid VirtualRoomId { get { return _virtualRoomId; } internal set { Set(ref _virtualRoomId, value, "VirtualRoomId"); } }
        [DataMember]
        public Guid RoomId { get { return _roomId; } internal set { Set(ref _roomId, value, "RoomId"); } }
        [DataMember]
        public string RoomDoor { get { return _roomDoor; } internal set { Set(ref _roomDoor, value, "RoomDoor"); } }
        [DataMember]
        public string RoomType { get { return _roomType; } internal set { Set(ref _roomType, value, "RoomType"); } }

        #endregion
        #region Constructor

        public VirtualRoomCompositionContract(Guid roomId, Guid virtualRoomId, string roomDoor, string roomType)
        {
            RoomId = roomId;
            VirtualRoomId = virtualRoomId;
            RoomDoor = roomDoor;
            RoomType = roomType;
        }

        public VirtualRoomCompositionContract(Guid id, Guid roomId, Guid virtualRoomId, string roomDoor, string roomType)
            : this(roomId, virtualRoomId, roomDoor, roomType)
        {
            Id = id;
        }

        #endregion
    }
}