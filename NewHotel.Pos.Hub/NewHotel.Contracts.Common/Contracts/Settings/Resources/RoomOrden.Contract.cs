﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RoomOrderContract : ResourceBaseContract
    {
        #region Members

        private TypedList<KeyDescRecord> _columns;
        private TypedList<KeyDescRecord> _filters;
        private KeyDescRecord _selectedFilter;

        #endregion
        #region Constructor

        public RoomOrderContract()
        {
            _columns = new TypedList<KeyDescRecord>();
            _filters = new TypedList<KeyDescRecord>();
        }

        #endregion
        #region Public Properties

        [DataMember]
        public TypedList<KeyDescRecord> Columns { get { return _columns; } set { Set(ref _columns, value, "Columns"); } }
        [DataMember]
        public TypedList<KeyDescRecord> Filters { get { return _filters; } set { Set(ref _filters, value, "Filters"); } }
        [DataMember]
        public KeyDescRecord SelectedFilter { get { return _selectedFilter; } set { Set(ref _selectedFilter, value, "SelectedFilter"); } }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class RoomOrdenManualContract : BaseContract
    {
        #region Properties

        [DataMember]
        public RoomRecord[] Records { get; set; }

        #endregion
    }
}