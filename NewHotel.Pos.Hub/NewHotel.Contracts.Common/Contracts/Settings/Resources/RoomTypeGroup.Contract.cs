﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(RoomTypeGroupContract), "ValidateRoomTypeGroup")]
    public class RoomTypeGroupContract : BaseContract, ISettingExportationSef
    {
        #region Members

        private string _entityOfficialCode;
        private string _passwordEntityOfficial;
        private string _fiscalNumer;
        private short? _nextCode;
        private short? _minimumCode;
        private short? _maximumCode;
        private bool _exportInDailyClosing;

        [DataMember]
        internal LanguageTranslationContract _description;

        #endregion
        #region Contracts

        [DataMember]
        public bool ExportInDailyClosing { get { return _exportInDailyClosing; } set { Set(ref _exportInDailyClosing, value, nameof(ExportInDailyClosing)); } }
        [DataMember]
        public string EntityOfficialCode { get { return _entityOfficialCode; } set { Set(ref _entityOfficialCode, value, nameof(EntityOfficialCode)); } }
        [DataMember]
        public string PasswordEntityOfficial { get { return _passwordEntityOfficial; } set { Set(ref _passwordEntityOfficial, value, nameof(PasswordEntityOfficial)); } }
        [DataMember]
        public string FiscalNumer { get { return _fiscalNumer; } set { Set(ref _fiscalNumer, value, nameof(FiscalNumer)); } }
        [DataMember]
        public short? NextCode { get { return _nextCode; } set { Set(ref _nextCode, value, nameof(NextCode)); } }
        [DataMember]
        public short? MinimumCode { get { return _minimumCode; } set { Set(ref _minimumCode, value, nameof(MinimumCode)); } }
        [DataMember]
        public short? MaximumCode { get { return _maximumCode; } set { Set(ref _maximumCode, value, nameof(MaximumCode)); } }

        public LanguageTranslationContract Description { get { return _description; } }

        #endregion
        #region Constructor

        public RoomTypeGroupContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateRoomTypeGroup(RoomTypeGroupContract obj)
        {
            if (obj.Description.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description Translated required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}