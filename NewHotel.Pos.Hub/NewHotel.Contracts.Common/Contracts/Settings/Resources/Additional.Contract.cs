﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class AdditionalGlobalContract : AdditionalResourceContract
    {
        #region Members

        private bool _stockControl;
        private bool _availabilityControl;
        private long _quantity;
        private Guid? _serviceId;
        private Guid? _departmentId;

        #endregion
        #region Properties

        [DataMember]
        public bool StockControl
        {
            get { return _stockControl; }
            set { Set(ref _stockControl, value, "StockControl"); }
        }
        [DataMember]
        public bool AvailabilityControl
        {
            get { return _availabilityControl; }
            set { Set(ref _availabilityControl, value, "AvailabilityControl"); }
        }
        [DataMember]
        public long Quantity
        {
            get { return _quantity; }
            set { Set(ref _quantity, value, "Quantity"); }
        }
        [DataMember]
        public Guid? ServiceId
        {
            get { return _serviceId; }
            set { Set(ref _serviceId, value, "ServiceId"); }
        }
        [DataMember]
        public Guid? DepartmentId
        {
            get { return _departmentId; }
            set { Set(ref _departmentId, value, "DepartmentId"); }
        }

        #endregion
        #region Lists

        private TypedList<AdditionalResourceNotesContract> _notes;
        [ReflectionExclude]
        public TypedList<AdditionalResourceNotesContract> Notes
        {
            get { return _notes; }
        }

        private TypedList<AdditionalResourcePricesPeriodsContract> _periods;
        [ReflectionExclude]
        public TypedList<AdditionalResourcePricesPeriodsContract> Periods
        {
            get { return _periods; }
        }

        private TypedList<AdditionalResourcePricesContract> _prices;
        [ReflectionExclude]
        public TypedList<AdditionalResourcePricesContract> Prices
        {
            get { return _prices; }
        }

        #endregion
        #region Constructors

        public AdditionalGlobalContract()
            : base() 
        {
            _notes = new TypedList<AdditionalResourceNotesContract>();
            _periods = new TypedList<AdditionalResourcePricesPeriodsContract>();
            _prices = new TypedList<AdditionalResourcePricesContract>();
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class AdditionalPriceResultContract : BaseContract
    {
        #region Properties
        [DataMember]
        public decimal TotalPrice { get; set; }

        [DataMember]
        public decimal? ReservationPrice { get; set; }

        [DataMember]
        public decimal? AdultPrice { get; set; }

        [DataMember]
        public decimal? ChildrenPrice { get; set; }

        [DataMember]
        public decimal? UnitPrice { get; set; }
        #endregion
        #region Constructor

        public AdditionalPriceResultContract()
            : base()
        {
        }

        #endregion
    }
}
