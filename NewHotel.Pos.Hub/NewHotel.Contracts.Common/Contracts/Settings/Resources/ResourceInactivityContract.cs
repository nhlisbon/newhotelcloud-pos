﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ResourceInactivityContract : BaseContract
    {
        #region Properties

        private Guid _resourceid;
        [DataMember]
        public Guid ResourceId { get { return _resourceid; } set { Set(ref _resourceid, value, "ResourceId"); } }

        private string _observations;
        [DataMember]
        public string Observations { get { return _observations; } set { Set(ref _observations, value, "Observations"); } }

        private ARGBColor _color;
        [DataMember]
        public ARGBColor Color { get { return _color; } set { Set(ref _color, value, "Color"); } }

        private DateTime _firstDate;
        private DateTime _lastDate;
        private DateTime _initialHour;
        private DateTime _finalHour;
        [DataMember]
        public DateTime FirstDate { get { return _firstDate; } set { Set(ref _firstDate, value, "FirstDate"); } }
        [DataMember]
        public DateTime LastDate { get { return _lastDate; } set { Set(ref _lastDate, value, "LastDate"); } }
        [DataMember]
        public DateTime InitialHour { get { return _initialHour; } set { Set(ref _initialHour, value, "InitialHour"); } }
        [DataMember]
        public DateTime FinalHour { get { return _finalHour; } set { Set(ref _finalHour, value, "FinalHour"); } }
        
        #endregion;

        #region Dummy Properties

        public string FirstDateHour { get { return FirstDate.ToString("dd-MM-yy") + " " + InitialHour.ToString("HH:mm"); } }
        public string LastDateHour { get { return LastDate.ToString("dd-MM-yy") + " " + FinalHour.ToString("HH:mm"); } }

        #endregion

        #region Constructors

        public ResourceInactivityContract()
            : base()
        {
            FirstDate = DateTime.Now.Date;
            LastDate = DateTime.Now.Date;
        }

        public ResourceInactivityContract(Guid id, Guid resourceId, string observations,
                     DateTime startDate, DateTime finalDate, DateTime initialhour, DateTime finalhour, ARGBColor color)
            : base()
        {
            Id = id;
            ResourceId = resourceId;
            Observations = observations;
            FirstDate = startDate;
            LastDate = finalDate;
            InitialHour = initialhour;
            FinalHour = finalhour;
            Color = color;
        }


        #endregion;
    }
}
