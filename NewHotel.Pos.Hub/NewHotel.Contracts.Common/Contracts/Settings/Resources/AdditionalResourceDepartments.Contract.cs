﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class AdditionalResourceDepartmentsContract : BaseContract
    {
        #region Members

        private Guid _additionalId;
        private Guid _departmentId;
        private long _quantity;
        private string _departmentDescription;
        private string _serviceDescription;
        private Guid _depId;
        private Guid _servId;

        #endregion
        #region Properties

        [DataMember]
        public Guid AdditionalId
        {
            get { return _additionalId; }
            set { Set(ref _additionalId, value, "AdditionalId"); }
        }
        [DataMember]
        public Guid DepartmentId
        {
            get { return _departmentId; }
            set { Set(ref _departmentId, value, "DepartmentId"); }
        }
        [DataMember]
        public long Quantity
        {
            get { return _quantity; }
            set { Set(ref _quantity, value, "Quantity"); }
        }
        [DataMember]
        public string DepartmentDescription
        {
            get { return _departmentDescription; }
            set { Set(ref _departmentDescription, value, "DepartmentDescription"); }
        }
        [DataMember]
        public string ServiceDescription
        {
            get { return _serviceDescription; }
            set { Set(ref _serviceDescription, value, "ServiceDescription"); }
        }
        [DataMember]
        public Guid DepId
        {
            get { return _depId; }
            set { Set(ref _depId, value, "DepId"); }
        }
        [DataMember]
        public Guid ServId
        {
            get { return _servId; }
            set { Set(ref _servId, value, "ServId"); }
        }

        #endregion
        #region Lists

        private TypedList<AdditionalResourcePricesContract> _prices;
        [ReflectionExclude]
        public TypedList<AdditionalResourcePricesContract> Prices
        {
            get { return _prices; }
        }

        #endregion
        #region Constructors

        public AdditionalResourceDepartmentsContract()
            : base()
        {
            _prices = new TypedList<AdditionalResourcePricesContract>();
        }

        public AdditionalResourceDepartmentsContract(Guid id, Guid additionaId, Guid departmentId, long quantity)
            : base()
        {
            Id = id;
            _additionalId = additionaId;
            _departmentId = departmentId;
            _quantity = quantity;
            _prices = new TypedList<AdditionalResourcePricesContract>();
        }

        #endregion
    }
}