﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ResourceDistributionContract), "ValidateResourceDistribution")]
    public class ResourceDistributionContract : BaseContract
    {
        #region Members
        private Guid? _resourceId;
        private Guid? _resourceGroupId;
        private Guid _distributionId;
        private short _paxs;
        private string _description;
        #endregion

        #region Properties
        [DataMember]
        public Guid? ResourceId { get { return _resourceId; } set { Set(ref _resourceId, value, "ResourceId"); } }
        [DataMember]
        public Guid? ResourceGroupId { get { return _resourceGroupId; } set { Set(ref _resourceGroupId, value, "ResourceGroupId"); } }
        [DataMember]
        public Guid DistributionId { get { return _distributionId; } set { Set(ref _distributionId, value, "DistributionId"); } }
        [DataMember]
        public short Paxs { get { return _paxs; } set { Set(ref _paxs, value, "Paxs"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        #endregion

        #region Constructor

        public ResourceDistributionContract()
            : base() 
        {
        }

        public ResourceDistributionContract(Guid id, Guid? resourceId, Guid? resourceGroupId, 
                                            Guid distributionId, short paxs, string description)
            : base()
        {
            Id = id;
            ResourceId = resourceId;
            ResourceGroupId = resourceGroupId;
            DistributionId = distributionId;
            Paxs = paxs;
            Description = description;
        }

        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateResourceDistribution(ResourceDistributionContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
