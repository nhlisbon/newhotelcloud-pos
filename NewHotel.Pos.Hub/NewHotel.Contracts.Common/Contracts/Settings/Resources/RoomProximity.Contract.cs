﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RoomProximityContract : BaseContract
    {
        #region Members

        private Guid _nextRoomId;
        private string _roomDoor;
        private string _nextRoomDoor;
        private bool _areConnected;

        #endregion
        #region Properties

        [DataMember]
        public Guid NextRoomId { get { return _nextRoomId; } internal set { Set(ref _nextRoomId, value, "NextRoomId"); } }
        [DataMember]
        public string RoomDoor { get { return _roomDoor; } internal set { Set(ref _roomDoor, value, "RoomDoor"); } }
        [DataMember]
        public string NextRoomDoor { get { return _nextRoomDoor; } internal set { Set(ref _nextRoomDoor, value, "NextRoomDoor"); } }
        [DataMember]
        public bool AreConnected { get { return _areConnected; } set { Set(ref _areConnected, value, "AreConnected"); } }

        #endregion
        #region Constructors

        public RoomProximityContract(Guid nextRoomId, string roomDoor, string nextRoomDoor, bool areConnected = false)
        {
            NextRoomId = nextRoomId;
            RoomDoor = roomDoor;
            NextRoomDoor = nextRoomDoor;
            AreConnected = areConnected;
        }

        public RoomProximityContract(Guid id, Guid nextRoomId, string roomDoor, string nextRoomDoor, bool areConnected = false)
            : this(nextRoomId, roomDoor, nextRoomDoor, areConnected)
        {
            Id = id;
        }

        #endregion
    }
}