﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ResourceCharacteristicContract : BaseContract
    {
        #region Members

        private Guid _resourceCharacteristicsTypeId;
        private string _abbreviation;
        private string _description;

        #endregion
        #region Properties

        [DataMember]
        public Guid ResourceCharacteristicTypeId { get { return _resourceCharacteristicsTypeId; } set { Set(ref _resourceCharacteristicsTypeId, value, "ResourceCharacteristicTypeId"); } }
        [DataMember]
        public string Abbreviation { get { return _abbreviation; } set { Set(ref _abbreviation, value, "Abbreviation"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }

        #endregion
        #region Constructors

        public ResourceCharacteristicContract(Guid resourceCharacteristicTypeId,
            string abbreviation, string description)
        {
            ResourceCharacteristicTypeId = resourceCharacteristicTypeId;
            Abbreviation = abbreviation;
            Description = description;
        }

        public ResourceCharacteristicContract(Guid id, Guid resourceCharacteristicTypeId,
            string abbreviation, string description)
            : this(resourceCharacteristicTypeId, abbreviation, description)
        {
            Id = id;
        }

        #endregion
    }
}