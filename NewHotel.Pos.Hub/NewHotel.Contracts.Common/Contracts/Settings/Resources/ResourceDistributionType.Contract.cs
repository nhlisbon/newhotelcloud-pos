﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ResourceDistributionTypeContract), "ValidateResourceDistributionType")]
    public class ResourceDistributionTypeContract : BaseContract
    {
        #region Members
        internal LanguageTranslationContract _description;
        #endregion

        #region Properties

        [DataMember]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        #endregion

        #region Constructor

        public ResourceDistributionTypeContract()
            : base() 
        {
            _description = new LanguageTranslationContract();
        }

        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateResourceDistributionType(ResourceDistributionTypeContract obj)
        {
            if (obj._description.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

    }
}
