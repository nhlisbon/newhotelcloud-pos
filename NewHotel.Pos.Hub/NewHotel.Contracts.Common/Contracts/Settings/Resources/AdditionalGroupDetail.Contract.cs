﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class AdditionalGroupDetailContract : BaseContract
    {
        #region Properties

        private Guid _additionalGroupId;
        [DataMember]
        public Guid AdditionalGroupId
        {
            get { return _additionalGroupId; }
            set { Set(ref _additionalGroupId, value, "AdditionalGroupId"); } 
        }

        private Guid _additionalId;
        [DataMember]
        public Guid AdditionalId
        {
            get { return _additionalId; }
            set { Set(ref _additionalId, value, "AdditionalId"); }
        }
        [DataMember]
        public string AdditionalDescription { get; set; }

        private decimal? _percentValue;
        private decimal? _value;
        [DataMember]
        public decimal? PercentValue
        {
            get { return _percentValue; }
            set { Set(ref _percentValue, value, "PercentValue"); }
        }
        [DataMember]
        public decimal? Value
        {
            get { return _value; }
            set { Set(ref _value, value, "Value"); }
        }

        [ReflectionExclude]
        public DiscountType DistributionType => ((PercentValue.HasValue) ? DiscountType.Percent : DiscountType.Value);

        private bool _launchMovement = true;
        [DataMember]
        public bool LaunchMovement
        {
            get { return _launchMovement; }
            set { Set(ref _launchMovement, value, "LaunchMovement"); }
        }

        #endregion
        #region Constructors

        public AdditionalGroupDetailContract() : base() { }

        #endregion
    }
}