﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [KnownType(typeof(AdditionalResourceContract))]
    public class ResourceBaseContract : BaseContract
    {
        #region Members

        private Guid? _resourceTypeId;
        private ResourceGender _resourceGenderType;
        private Blob? _image;
        private bool _inactive;

        #endregion
        #region Properties

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Resource type required.")]
        public Guid? ResourceTypeId { get { return _resourceTypeId; } set { Set(ref _resourceTypeId, value, "ResourceTypeId"); } }
        [DataMember]
        public ResourceGender ResourceGenderType { get { return _resourceGenderType; } set { Set(ref _resourceGenderType, value, "ResourceGenderType"); } }
        [DataMember]
        public Blob? Image { get { return _image; } set { Set(ref _image, value, "Image"); } }
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }

        #endregion
        #region Extension Properties

        [ReflectionExclude]
        public string ResourceDesc
        {
            get { return Description; }
        }

        #endregion
        #region Contracts

        [ReflectionExclude]
        [DataMember]
        public LanguageTranslationContract Description { get; set; }

        #endregion
        #region Constructors

        public ResourceBaseContract()
            : base()
        {
            Description = new LanguageTranslationContract();
            ResourceGenderType = ResourceGender.Both;
        }

        #endregion
    }
}