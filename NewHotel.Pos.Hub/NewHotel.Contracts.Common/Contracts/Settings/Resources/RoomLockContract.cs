﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RoomLockContract : BaseContract
    {
        #region Persistent Properties
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Room id required.")]
        public Guid ResourceId { get; set; }

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Initial date required.")]
        public DateTime InitialDate { get; set; }

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Final date required.")]
        public DateTime FinalDate { get; set; }

        #endregion
        #region Constructor

        public RoomLockContract()
            : base() { }

        #endregion
    }
}