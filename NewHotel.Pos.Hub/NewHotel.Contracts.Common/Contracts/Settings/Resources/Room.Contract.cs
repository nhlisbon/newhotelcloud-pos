﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(RoomContract), "ValidateRoom")]
    public class RoomContract : ResourceBaseContract
    {
        #region Members

        [DataMember]
        internal DateTime _workDate;
        [DataMember]
        internal TypedList<RoomProximityContract> _roomsNextTo;
        [DataMember]
        internal TypedList<ResourceCharacteristicContract> _characteristics;
        [DataMember]
        internal TypedList<VirtualRoomCompositionContract> _rooms;

        private string _door;
        private string _doorTo;
        private string _doorInterval;
        private RoomStatus _status;
        private Guid? _roomBlockId;
        private short _order;
        private bool _inUse;
        private short _floor;
        private ARGBColor? _color;
        private short? _evelatorDistance;
        private short? _stairDistance;
        private short? _houseKeepingDistance;
        private int? _step;
        private int? _roomFrom;
        private int? _roomTo;
        private bool _multiple;
        private bool _newContract;
        private bool _enableAdvancedDetails;
        private short _beds;
        private short _bathrooms;
        private decimal? _area;
        private MeasurementArea _areaUnit;
        private string _wifiSSID;
        private string _wifiPassword;
        private bool _ownershipStandardUse;
        private Guid? _cleaningZoneId;
        private bool _virtualRoom;

        #endregion
        #region Properties

        [DataMember]
        public string Door { get { return _door; } set { Set(ref _door, value, "Door"); } }
        [DataMember]
        public string DoorTo { get { return _doorTo; } set { Set(ref _doorTo, value, "DoorTo"); } }
        [DataMember]
        public string DoorInterval { get { return _doorInterval; } set { Set(ref _doorInterval, value, "DoorInterval"); } }
        [DataMember]
        public RoomStatus Status { get { return _status; } set { Set(ref _status, value, "Status"); } }
        [DataMember]
        public Guid? RoomBlockId { get { return _roomBlockId; } set { Set(ref _roomBlockId, value, "RoomBlockId"); } }
        [DataMember]
        public short Order { get { return _order; } set { Set(ref _order, value, "Order"); } }
        [DataMember]
        public bool InUse { get { return _inUse; } set { Set(ref _inUse, value, "InUse"); } }
        [DataMember]
        public short Floor { get { return _floor; } set { Set(ref _floor, value, "Floor"); } }
        [DataMember]
        public ARGBColor? Color { get { return _color; } set { Set(ref _color, value, "Color"); } }
        [DataMember]
        public short? ElevatorDistance { get { return _evelatorDistance; } set { Set(ref _evelatorDistance, value, "ElevatorDistance"); } }
        [DataMember]
        public short? StairDistance { get { return _stairDistance; } set { Set(ref _stairDistance, value, "StairDistance"); } }
        [DataMember]
        public short? HouseKeeperDistance { get { return _houseKeepingDistance; } set { Set(ref _houseKeepingDistance, value, "HouseKeeperDistance"); } }
        [DataMember]
        public int? Step { get { return _step; } set { Set(ref _step, value, "Step"); } }
        [DataMember]
        public int? RoomFrom { get { return _roomFrom; } set { Set(ref _roomFrom, value, "RoomFrom"); } }
        [DataMember]
        public int? RoomTo { get { return _roomTo; } set { Set(ref _roomTo, value, "RoomTo"); } }
        [DataMember]
        public bool Multiple { get { return _multiple; } set { Set(ref _multiple, value, "Multiple"); } }
        [DataMember]
        public bool NewContract { get { return _newContract; } set { Set(ref _newContract, value, "NewContract"); } }
        [DataMember]
        public short Beds { get { return _beds; } set { Set(ref _beds, value, "Beds"); } }
        [DataMember]
        public short Bathrooms { get { return _bathrooms; } set { Set(ref _bathrooms, value, "Bathrooms"); } }
        [DataMember]
        public MeasurementArea AreaUnit { get { return _areaUnit; } set { Set(ref _areaUnit, value, "AreaUnit"); } }
        [DataMember]
        public decimal? Area { get { return _area; } set { Set(ref _area, value, "Area"); } }
        [DataMember]
        public string WifiSSID { get { return _wifiSSID; } set { Set(ref _wifiSSID, value, "WifiSSID"); } }
        [DataMember]
        public string WifiPassword { get { return _wifiPassword; } set { Set(ref _wifiPassword, value, "WifiPassword"); } }
        [DataMember]
        public bool OwnershipStandardUse { get { return _ownershipStandardUse; } set { Set(ref _ownershipStandardUse, value, "OwnershipStandardUse"); } }
        [DataMember]
        public bool EnableAdvancedDetails { get { return _enableAdvancedDetails; } set { Set(ref _enableAdvancedDetails, value, "EnableAdvancedDetails"); } }
        [DataMember]
        public Guid? CleaningZoneId { get { return _cleaningZoneId; } set { Set(ref _cleaningZoneId, value, "CleaningZoneId"); } }
        [DataMember]
        public bool VirtualRoom { get { return _virtualRoom; } set { Set(ref _virtualRoom, value, "VirtualRoom"); } }

        [ReflectionExclude]
        public TypedList<RoomProximityContract> RoomsNextTo { get { return _roomsNextTo; } }

        [ReflectionExclude]
        public TypedList<ResourceCharacteristicContract> Characteristics { get { return _characteristics; } }

        [ReflectionExclude]
        public TypedList<VirtualRoomCompositionContract> Rooms { get { return _rooms; } }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public Guid [] RoomsIds
        {
            get { return Rooms.Select(x => x.RoomId).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] RoomsNextToIds
        {
            get { return RoomsNextTo.Select(x => x.NextRoomId).ToArray(); }
        }

        [ReflectionExclude]
        public Guid[] CharacteristicsIds
        {
            get { return Characteristics.Select(x => x.ResourceCharacteristicTypeId).ToArray(); }
        }

        #endregion
        #region Parameters

        [ReflectionExclude]
        public DateTime WorkDate { get { return _workDate; } }

        #endregion
        #region Constructors

        public RoomContract(DateTime workDate)
            : base()
        {
            _workDate = workDate;
            _roomsNextTo = new TypedList<RoomProximityContract>();
            _characteristics = new TypedList<ResourceCharacteristicContract>();
            _rooms = new TypedList<VirtualRoomCompositionContract>();
            AreaUnit = MeasurementArea.SqMeters;
            VirtualRoom = false;
        }

        #endregion
        #region Methods

        public int Amount
        {
            get 
            {
                if (Multiple)
                {
                    var roomRange = new List<int>();
                    int currentValue = RoomFrom.Value;
                    do
                    {
                        roomRange.Add(currentValue);
                        currentValue += Step.Value;
                    }
                    while (currentValue <= RoomTo.Value);

                    return roomRange.Count;
                }
                else return 1;
            }
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateRoom(RoomContract contract)
        {
            if (string.IsNullOrEmpty(contract.Door))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Room number cannot be empty");

            if (!contract.ResourceTypeId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Room type missing");

            if (!contract.RoomBlockId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Room block missing");

            if (!string.IsNullOrEmpty(contract.DoorTo))
            {
                if (string.IsNullOrEmpty(contract.DoorInterval))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Step number is invalid");

                int step = 0;
                int initial = 0;
                int final = 0;

                int.TryParse(contract.Door, out initial);
                int.TryParse(contract.DoorTo, out final);
                int.TryParse(contract.DoorInterval, out step);

                if (initial == 0 || final == 0)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Initial or final values are not numeric, therefore, no range can be made");

                if (step == 0)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Step value is incorrect");

                contract.RoomFrom = new int?(initial);
                contract.RoomTo = new int?(final);
                contract.Step = new int?(step);
                contract.Multiple = true;
            }
            else contract.Multiple = false;

            if (contract.VirtualRoom && (contract.Rooms == null || contract.Rooms.Count < 2))
                return new System.ComponentModel.DataAnnotations.ValidationResult("At least 2 rooms are required to create virtual type");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}