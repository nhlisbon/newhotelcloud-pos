﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    /// <summary>
    /// Represents the Room Type DataContract. Its use to transport information between business layers
    /// </summary>
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(RoomTypeContract), "ValidateRoomType")]
    public class RoomTypeContract : ResourceTypeContract
    {
        #region Members

        private RoomRentType _rentType;
        private short? _roomsCount;
        private short _minPersonQuantity;
        private short _maxPersonQuantity;
        private short _basePersonQuantity;
        private short _statisticsBasePersonQuantity;
        private decimal _averagePersonQuantityInMultiOccupation;
        private short _maxPersonForWarning;
        public PriceCalculationType _priceMode;
        private short _order;
        private Blob? _image;
        private string _imageName;
        private ARGBColor? _backgroundColor;
        private Guid? _roomTypeGroupId;
        private short? _interchangeDay;
        private bool _isVirtual;

        [DataMember]
        internal bool _enabledVirtual;
        
        #endregion
        #region Properties

        /// <summary>
        /// Type (Room, House)
        /// </summary>
        [DataMember]
        public RoomRentType RentType { get { return _rentType; } set { Set(ref _rentType, value, "RentType"); } }

        /// <summary>
        /// Nº Rooms (used only when TIAL_TIAL == House)
        /// </summary>
        [DataMember]
        public short? RoomsCount { get { return _roomsCount; } set { Set(ref _roomsCount, value, "RoomsCount"); } }

        /// <summary>
        /// Nº Paxs (Min)
        /// </summary>
        [DataMember]
        public short MinPersonQuantity { get { return _minPersonQuantity; } set { Set(ref _minPersonQuantity, value, "MinPersonQuantity"); } }

        /// <summary>
        /// Nº Paxs (Max)
        /// </summary>
        [DataMember]
        public short MaxPersonQuantity { get { return _maxPersonQuantity; } set { Set(ref _maxPersonQuantity, value, "MaxPersonQuantity"); } }

        /// <summary>
        /// Nº Paxs (Base)
        /// </summary>
        [DataMember]
        public short BasePersonQuantity { get { return _basePersonQuantity; } set { Set(ref _basePersonQuantity, value, "BasePersonQuantity"); } }

        /// <summary>
        /// Nº Paxs (Base Statistics)
        /// </summary>
        [DataMember]
        public short StatisticsBasePersonQuantity { get { return _statisticsBasePersonQuantity; } set { Set(ref _statisticsBasePersonQuantity, value, "StatisticsBasePersonQuantity"); } }

        /// <summary>
        /// Avg Reservation in Shared Room mode
        /// </summary>
        [DataMember]
        public decimal AveragePersonQuantityInMultiOccupation { get { return _averagePersonQuantityInMultiOccupation; } set { Set(ref _averagePersonQuantityInMultiOccupation, value, "AveragePersonQuantityInMultiOccupation"); } }

        /// <summary>
        /// Nº Paxs (Max Validation)
        /// </summary>
        [DataMember]
        public short MaxPersonForWarning { get { return _maxPersonForWarning; } set { Set(ref _maxPersonForWarning, value, "MaxPersonForWarning"); } }

        /// <summary>
        /// Price Mode: (By Room; By Paxs)
        /// </summary>
        [DataMember]
        public PriceCalculationType PriceMode { get { return _priceMode; } set { Set(ref _priceMode, value, "PriceMode"); } }

        /// <summary>
        /// Room Type Picture
        /// </summary>
        [DataMember]
        public Blob? Image { get { return _image; } set { Set(ref _image, value, "Image"); } }

        /// <summary>
        /// Room Type Picture Name
        /// </summary>
        [DataMember]
        public string ImageName { get { return _imageName; } set { Set(ref _imageName, value, "ImageName"); } }

        /// <summary>
        /// Nº Order
        /// </summary>
        [DataMember]
        public short Order { get { return _order; } set { Set(ref _order, value, "Order"); } }

        /// Room Type Group
        /// </summary>
        [DataMember]
        public Guid? RoomTypeGroupId{ get { return _roomTypeGroupId; } set { Set(ref _roomTypeGroupId, value, "RoomTypeGroupId"); } }

        /// <summary>
        /// Nº de días a partir de la entrada para cambio de ropas
        /// </summary>
        [DataMember]
        public short? InterchangeDay { get { return _interchangeDay; } set { Set(ref _interchangeDay, value, "InterchangeDay"); } }

        /// <summary>
        /// Background Color
        /// </summary>
        [DataMember]
        public ARGBColor? BackgroundColor { get { return _backgroundColor; } set { Set(ref _backgroundColor, value, "BackgroundColor"); } }

        /// <summary>
        /// Is virtual
        /// </summary>
        [DataMember]
        public bool IsVirtual
        {
            get { return _isVirtual; }
            set
            {
                if (Set(ref _isVirtual, value, "IsVirtual"))
                    NotifyPropertyChanged("EnabledChangeComposition");
            }
        }

        #endregion
        #region Contracts

        [DataMember]
        public TypedList<VirtualRoomTypeCompositionContract> Composition { get; internal set; }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public bool EnabledChangeVirtual
        {
            get { return _enabledVirtual && Composition.Count == 0; }
        }

        [ReflectionExclude]
        public bool EnabledChangeComposition
        {
            get { return _enabledVirtual && IsVirtual; }
        }

        #endregion
        #region Constructor

        public RoomTypeContract(bool enabledVirtual = true)
            : base()
        {
            Composition = new TypedList<VirtualRoomTypeCompositionContract>();
            _enabledVirtual = enabledVirtual;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateRoomType(RoomTypeContract contract)
        {
            if (!contract.RoomTypeGroupId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Room type group cannot be empty");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}