﻿using System;
using System.Linq;
using NewHotel.DataAnnotations;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ResourceContract), "ValidateResource")]
    public class ResourceContract : ResourceBaseContract
    {
        #region Members

        private string _initialHour;
        private string _finalHour;
        private string _restPeriod;
        private Guid? _serviceByDepartmentId;
        private Guid? _departmentId;
        private int _resourceOccupation;

        private TypedList<ResourceCharacteristicContract> _characteristics;
        private TypedList<ResourceDistributionContract> _distributions;
        private TypedList<BaseContract> _services;
        private TypedList<BaseContract> _resourceInactivities;

        private decimal? _resourceLength;
        private decimal? _resourceWidth;
        private decimal? _resourceHeight;
        private string _resourceLongitudeUnit;
        private decimal? _resourceArea;
        private string _resourceAreaUnit;

        #endregion
        #region Properties

        [DataMember]
        public string InitialHour { get { return _initialHour; } set { Set(ref _initialHour, value, "InitialHour"); } }
        [DataMember]
        public string FinalHour { get { return _finalHour; } set { Set(ref _finalHour, value, "FinalHour"); } }
        [DataMember]
        public string RestPeriod { get { return _restPeriod; } set { Set(ref _restPeriod, value, "RestPeriod"); } }
        [DataMember]
        public Guid? ServiceByDepartmentId { get { return _serviceByDepartmentId; } set { Set(ref _serviceByDepartmentId, value, "ServiceByDepartmentId"); } }
        [DataMember]
        public Guid? DepartmentId { get { return _departmentId; } set { Set(ref _departmentId, value, "DepartmentId"); } }
        [DataMember]
        public bool GenderChanged { get; set; }
        
        [DataMember]
        public decimal? ResourceLength { get { return _resourceLength; } set { Set(ref _resourceLength, value, "ResourceLength"); } }
        [DataMember]
        public decimal? ResourceWidth { get { return _resourceWidth; } set { Set(ref _resourceWidth, value, "ResourceWidth"); } }
        [DataMember]
        public decimal? ResourceHeight { get { return _resourceHeight; } set { Set(ref _resourceHeight, value, "ResourceHeight"); } }
        [DataMember]
        public string ResourceLongitudeUnit { get { return _resourceLongitudeUnit; } set { Set(ref _resourceLongitudeUnit, value, "ResourceLongitudeUnit"); } }
        [DataMember]
        public decimal? ResourceArea { get { return _resourceArea; } set { Set(ref _resourceArea, value, "ResourceArea"); } }
        [DataMember]
        public string ResourceAreaUnit { get { return _resourceAreaUnit; } set { Set(ref _resourceAreaUnit, value, "ResourceAreaUnit"); } }

        [ReflectionExclude]
        public TypedList<ResourceCharacteristicContract> Characteristics
        {
            get { return _characteristics; }
        }

        [ReflectionExclude]
        public TypedList<ResourceDistributionContract> Distributions
        {
            get { return _distributions; }
        }

        [ReflectionExclude]
        public TypedList<BaseContract> Services
        {
            get { return _services; }
        }

        [ReflectionExclude]
        public TypedList<BaseContract> ResourceInactivities
        {
            get { return _resourceInactivities; }
        }

        [ReflectionExclude]
        public Guid[] CharacteristicsIds
        {
            get { return Characteristics.Select(x => x.ResourceCharacteristicTypeId).ToArray(); }
        }

        #endregion
        #region Constructors

        public ResourceContract()
            : base()
        {
            _characteristics = new TypedList<ResourceCharacteristicContract>();
            _distributions = new TypedList<ResourceDistributionContract>();
            _services = new TypedList<BaseContract>();
            _resourceInactivities = new TypedList<BaseContract>();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateResource(ResourceContract contract)
        {
            if (string.IsNullOrEmpty(contract.InitialHour))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Initial Hour cannot be empty");

            if (string.IsNullOrEmpty(contract.FinalHour))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Final Hour cannot be empty");

            int initial = (Convert.ToInt32(contract.InitialHour.Substring(0, 2)) * 60) + Convert.ToInt32(contract.InitialHour.Substring(3, 2));
            int final = (Convert.ToInt32(contract.FinalHour.Substring(0, 2)) * 60) + Convert.ToInt32(contract.FinalHour.Substring(3, 2));
            if (initial >= final)
                return new System.ComponentModel.DataAnnotations.ValidationResult("The final hour must be greater than the initial hour");

            //Verificar si las horas de inicio y fin son múltiplos de 5
            int hour = Convert.ToInt32(contract.InitialHour.Substring(0, 2));
            int min = Convert.ToInt32(contract.InitialHour.Substring(3, 2));
            int tmin = (hour * 60) + min;
            if ((tmin % 5) != 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("The initial hour must be a multiple of 5");

            if (contract.FinalHour != "23:59")
            {
                hour = Convert.ToInt32(contract.FinalHour.Substring(0, 2));
                min = Convert.ToInt32(contract.FinalHour.Substring(3, 2));
                tmin = (hour * 60) + min;
                if ((tmin % 5) != 0)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("The final hour must be a multiple of 5");
            }

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
