﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ResourceGroupContract), "ValidateResourceGroup")]
    public class ResourceGroupContract : BaseContract
    {
        #region Constructor

        public ResourceGroupContract()
            : base()
        {
            _description = new LanguageTranslationContract();
            _inactive = false;
            Resources = new TypedList<ResourceGroupItemContract>();
            Distributions = new TypedList<ResourceDistributionContract>();
        }

        #endregion
        #region Members

        internal LanguageTranslationContract _description;
        private Guid _installationId;
        private bool _inactive;
        private Guid? _serviceByDepartmentId;
        private Guid? _departmentId;

        private decimal? _resourceGroupLength;
        private decimal? _resourceGroupWidth;
        private decimal? _resourceGroupHeight;
        private string _resourceGroupLongitudeUnit;
        private decimal? _resourceGroupArea;
        private string _resourceGroupAreaUnit;

        #endregion
        #region Properties

        [DataMember]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        public Guid InstallationId { get { return _installationId; } set { Set(ref _installationId, value, "InstallationId"); } }

        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }

        [DataMember]
        public Guid? DepartmentId { get { return _departmentId; } set { Set(ref _departmentId, value, "DepartmentId"); } }

        [DataMember]
        public Guid? ServiceByDepartmentId { get { return _serviceByDepartmentId; } set { Set(ref _serviceByDepartmentId, value, "ServiceByDepartmentId"); } }

        [DataMember]
        public decimal? ResourceGroupLength { get { return _resourceGroupLength; } set { Set(ref _resourceGroupLength, value, "ResourceGroupLength"); } }
        [DataMember]
        public decimal? ResourceGroupWidth { get { return _resourceGroupWidth; } set { Set(ref _resourceGroupWidth, value, "ResourceGroupWidth"); } }
        [DataMember]
        public decimal? ResourceGroupHeight { get { return _resourceGroupHeight; } set { Set(ref _resourceGroupHeight, value, "ResourceGroupHeight"); } }
        [DataMember]
        public string ResourceGroupLongitudeUnit { get { return _resourceGroupLongitudeUnit; } set { Set(ref _resourceGroupLongitudeUnit, value, "ResourceGroupLongitudeUnit"); } }
        [DataMember]
        public decimal? ResourceGroupArea { get { return _resourceGroupArea; } set { Set(ref _resourceGroupArea, value, "ResourceGroupArea"); } }
        [DataMember]
        public string ResourceGroupAreaUnit { get { return _resourceGroupAreaUnit; } set { Set(ref _resourceGroupAreaUnit, value, "ResourceGroupAreaUnit"); } }

        #endregion
        #region Lists

        [ReflectionExclude]
        public TypedList<ResourceGroupItemContract> Resources { get; set; }
        [ReflectionExclude]
        public Guid[] ResourceIds
        {
            get { return Resources.Select(x => x.ResourceId).ToArray(); }
        }

        [ReflectionExclude()]
        public TypedList<ResourceDistributionContract> Distributions { get; set; }
        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateResourceGroup(ResourceGroupContract obj)
        {
            if (obj._description.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
