﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(AdditionalGroupNotesContract), "ValidateAdditionalGroupNotes")]
    public class AdditionalGroupNotesContract : BaseContract
    {
        #region Members

        private long _languageId;
        private string _notes;
        private string _internalNotes;
        private string _languageDescription;
        private Guid _additionalGroupId;

        #endregion
        #region Properties

        [DataMember]
        public Guid AdditionalGroupId
        {
            get { return _additionalGroupId; }
            set { Set(ref _additionalGroupId, value, "AdditionalGroupId"); }
        }
        [DataMember]
        public long LanguageId
        {
            get { return _languageId; }
            set { Set(ref _languageId, value, "LanguageId"); }
        }
        [DataMember]
        public string Notes
        {
            get { return _notes; }
            set { Set(ref _notes, value, "Notes"); }
        }
        [DataMember]
        public string InternalNotes
        {
            get { return _internalNotes; }
            set { Set(ref _internalNotes, value, "InternalNotes"); }
        }
        [DataMember]
        public string LanguageDescription
        {
            get { return _languageDescription; }
            set { Set(ref _languageDescription, value, "LanguageDescription"); }
        }

        #endregion
        #region Constructors

        public AdditionalGroupNotesContract()
            : base()
        {
        }

        public AdditionalGroupNotesContract(Guid id, Guid additionalGroupId, long languageId, string notes, string internalnotes)
            : base()
        {
            Id = id;
            _additionalGroupId = additionalGroupId;
            _languageId = languageId;
            _notes = notes;
            _internalNotes = internalnotes;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateAdditionalGroupNotes(AdditionalGroupNotesContract obj)
        {
            if (obj.Notes == "")
                return new System.ComponentModel.DataAnnotations.ValidationResult("Notes can not is empty.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}