﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ResourceGroupItemContract), "ValidateResourceGroupItem")]
    public class ResourceGroupItemContract : BaseContract
    {
        #region Members
        private Guid _resourceId;
        private Guid _resourceGroupId;
        private string _initialHour;
        private string _finalHour;
        private string _description;
        #endregion

        #region Properties
        [DataMember]
        public Guid ResourceId { get { return _resourceId; } set { Set(ref _resourceId, value, "ResourceId"); } }
        [DataMember]
        public Guid ResourceGroupId { get { return _resourceGroupId; } set { Set(ref _resourceGroupId, value, "ResourceGroupId"); } }
        [DataMember]
        public string InitialHour { get { return _initialHour; } set { Set(ref _initialHour, value, "InitialHour"); } }
        [DataMember]
        public string FinalHour { get { return _finalHour; } set { Set(ref _finalHour, value, "FinalHour"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        #endregion

        #region Constructor
        public ResourceGroupItemContract()
            : base() 
        {
        }

        public ResourceGroupItemContract(Guid id, Guid resourceId, Guid resourceGroupId,
                                         string initialHour, string finalHour, string description)
            : base()
        {
            Id = id;
            ResourceId = resourceId;
            ResourceGroupId = resourceGroupId;
            InitialHour = initialHour;
            FinalHour = finalHour;
            Description = description;
        }
        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateResourceGroupItem(ResourceGroupItemContract obj)
        {
            if (obj.ResourceId == null)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Resource required.");
            if (obj.ResourceGroupId == null)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Group required.");
            if (obj.InitialHour == string.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Initial Hour required.");
            if (obj.FinalHour == string.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Final Hour required.");

            int inic = (Convert.ToInt32(obj.InitialHour.Substring(0, 2)) * 60) + Convert.ToInt32(obj.InitialHour.Substring(3, 2));
            int fina = (Convert.ToInt32(obj.FinalHour.Substring(0, 2)) * 60) + Convert.ToInt32(obj.FinalHour.Substring(3, 2));
            if (inic >= fina)
                return new System.ComponentModel.DataAnnotations.ValidationResult("The final hour must be greater than the initial hour");

            //Verificar si las horas de inicio y fin son múltiplos de 5
            int hour = Convert.ToInt32(obj.InitialHour.Substring(0, 2));
            int min = Convert.ToInt32(obj.InitialHour.Substring(3, 2));
            int tmin = (hour * 60) + min;
            if ((tmin % 5) != 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("The initial hour must be a multiple of 5");

            if (obj.FinalHour != "23:59")
            {
                hour = Convert.ToInt32(obj.FinalHour.Substring(0, 2));
                min = Convert.ToInt32(obj.FinalHour.Substring(3, 2));
                tmin = (hour * 60) + min;
                if ((tmin % 5) != 0)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("The final hour must be a multiple of 5");
            }

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion


    }
}
