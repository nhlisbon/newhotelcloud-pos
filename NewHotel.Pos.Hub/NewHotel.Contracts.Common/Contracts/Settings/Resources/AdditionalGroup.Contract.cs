﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    #region AdditionalGroupContract
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(AdditionalGroupContract), "ValidateAdditionalGroup")]
    public class AdditionalGroupContract : BaseContract
    {
        #region Properties

        [DataMember]
        internal LanguageTranslationContract _description;

        [Display(Name = "Description")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private Guid _additionalTypeId;
        [DataMember]
        public Guid AdditionalTypeId
        {
            get { return _additionalTypeId; }
            set { Set(ref _additionalTypeId, value, "AdditionalTypeId"); }
        }

        private Guid? _serviceId;
        private Guid? _departmentId;
        [DataMember]
        public Guid? ServiceId
        {
            get { return _serviceId; }
            set { Set(ref _serviceId, value, "ServiceId"); }
        }
        [DataMember]
        public Guid? DepartmentId
        {
            get { return _departmentId; }
            set { Set(ref _departmentId, value, "DepartmentId"); }
        }

        private decimal _basePrice;
        [DataMember]
        public decimal BasePrice
        {
            get { return _basePrice; }
            set { Set(ref _basePrice, value, "BasePrice"); }
        }

        private long _applicationId;//2019.07.22
        [DataMember]
        public long ApplicationId
        {
            get { return _applicationId; }
            set { Set(ref _applicationId, value, "ApplicationId"); }
        }


        internal TypedList<AdditionalGroupDetailContract> _additionalGroupDetails;
        [ReflectionExclude]
        [DataMember]
        public TypedList<AdditionalGroupDetailContract> AdditionalGroupDetails { get { return _additionalGroupDetails; } set { _additionalGroupDetails = value; } }

        private TypedList<AdditionalGroupNotesContract> _notes;
        [ReflectionExclude]
        //2019.10.25 Comentariado ACF causa de error [DataMember]
        public TypedList<AdditionalGroupNotesContract> Notes
        {
            get { return _notes; }
        }

        #endregion
        #region Constructors

        public AdditionalGroupContract() : base()
        {
            _description = new LanguageTranslationContract();
            _additionalGroupDetails = new TypedList<AdditionalGroupDetailContract>();
            _notes = new TypedList<AdditionalGroupNotesContract>();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateAdditionalGroup(AdditionalGroupContract obj)
        {
            /* lista obligatoria */
            if (obj.AdditionalGroupDetails.Count == 0)
                return new System.ComponentModel.DataAnnotations.ValidationResult("The group should contain at least one detail line");

            /* solo una posibilidad de ratio */
            if (!obj.DepartmentId.HasValue || !obj.ServiceId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Department and Service cannot be empty");

            if ((obj.AdditionalGroupDetails.Sum(x => x.PercentValue ?? 0) != 100) &&
               (obj.AdditionalGroupDetails.Sum(x => x.Value ?? 0) != obj.BasePrice))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Total Value or percent not valids for details");

            if (!(((obj.AdditionalGroupDetails.Sum(x => x.PercentValue ?? 0) > 0) && 
                 (obj.AdditionalGroupDetails.Sum(x => x.Value ?? 0) == 0)) || 
                 (obj.AdditionalGroupDetails.Sum(x => x.PercentValue ?? 0) == 0) &&
                 (obj.AdditionalGroupDetails.Sum(x => x.Value ?? 0) > 0)))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Use only one ratio, value or percent");
            /* no permitir lineas sin cantidades */
            bool found = false;
            foreach (var item in obj.AdditionalGroupDetails)
            {
                if ((!item.Value.HasValue || item.Value.Value == 0) && (!item.PercentValue.HasValue || item.PercentValue.Value == 0))
                {
                    found = true;
                    break;
                }
            }

            if (found)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Cannot accept details without percent or value");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
    #endregion

    
}