﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(AdditionalResourceNotesContract), "ValidateAdditionalResourceNotes")]

    public class AdditionalResourceNotesContract : BaseContract
    {
        #region Members
        private Guid _additionalId;
        private long _languageId;
        private string _notes;
        private string _internalNotes;
        private string _languageDescription;
        #endregion

        #region Properties
        [DataMember]
        public Guid AdditionalId
        {
            get { return _additionalId; }
            set { Set(ref _additionalId, value, "AdditionalId"); }
        }
        [DataMember]
        public long LanguageId
        {
            get { return _languageId; }
            set { Set(ref _languageId, value, "LanguageId"); }
        }
        [DataMember]
        public string Notes
        {
            get { return _notes; }
            set { Set(ref _notes, value, "Notes"); }
        }
        [DataMember]
        public string InternalNotes
        {
            get { return _internalNotes; }
            set { Set(ref _internalNotes, value, "InternalNotes"); }
        }
        [DataMember]
        public string LanguageDescription
        {
            get { return _languageDescription; }
            set { Set(ref _languageDescription, value, "LanguageDescription"); }
        }
        #endregion

        #region Constructor

        public AdditionalResourceNotesContract()
            : base()
        {
        }

        public AdditionalResourceNotesContract(Guid id, Guid additionalId, long languageId, string notes, string internalnotes)
            : base()
        {
            Id = id;
            _additionalId = additionalId;
            _languageId = languageId;
            _notes = notes;
            _internalNotes = internalnotes;
        }

        #endregion

        #region Validations
        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateAdditionalResourceNotes(AdditionalResourceNotesContract obj)
        {
            if (obj.Notes == "")
                return new System.ComponentModel.DataAnnotations.ValidationResult("Notes can not is empty.");
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
        #endregion
    }
}
