﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public class VirtualRoomTypeCompositionContract : BaseContract
    {
        #region Members

        private Guid _realRoomTypeId;
        private long _count;

        #endregion
        #region Properties

        [DataMember]
        public Guid RealRoomTypeId { get { return _realRoomTypeId; } set { Set(ref _realRoomTypeId, value, "RealRoomTypeId"); } }
        [DataMember]
        public long Count { get { return _count; } set { Set(ref _count, value, "Count"); } }

        #endregion
        #region Extended Properties

        public string RealRoomTypeAbbreviation { get; set; }
        public string RealRoomTypeDescription { get; set; }

        #endregion
        #region Constructor

        public VirtualRoomTypeCompositionContract()
        {
        }

        #endregion
    }
}