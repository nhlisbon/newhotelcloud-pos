﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class AdditionalResourceContract : ResourceBaseContract
    {
        #region Members

        private bool _extendPeriod;
        private decimal _basePrice;
        private bool _LaunchMovement;

        #endregion
        #region Properties

        [DataMember]
        public bool ExtendPeriod
        {
            get { return _extendPeriod; }
            set { Set(ref _extendPeriod, value, "ExtendPeriod"); }
        }

        [DataMember]
        public decimal BasePrice
        {
            get { return _basePrice; }
            set { Set(ref _basePrice, value, "BasePrice"); }
        }

        [DataMember]
        public bool LaunchMovement
        {
            get { return _LaunchMovement; }
            set { Set(ref _LaunchMovement, value, "LaunchMovement"); }
        }

        #endregion
        #region Constructors

        public AdditionalResourceContract()
            : base() { }

        #endregion
    }
}
