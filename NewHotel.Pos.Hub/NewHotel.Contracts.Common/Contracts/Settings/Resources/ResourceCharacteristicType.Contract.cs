﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ResourceCharacteristicTypeContract : BaseContract
    {
        #region Members

        private long _applicationId;

        [DataMember]
        internal LanguageTranslationContract _abbreviation;
        [DataMember]
        internal LanguageTranslationContract _description;

        #endregion
        #region Properties

        [DataMember]
        public long ApplicationId
        {
            get { return _applicationId; }
            set { Set(ref _applicationId, value, "ApplicationId"); }
        }

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Abbreviation required.")]
        [ReflectionExclude()]
        public LanguageTranslationContract Abbreviation
        {
            get { return _abbreviation; }
            set { _abbreviation = value; }
        }

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        #endregion
        #region Constructor

        public ResourceCharacteristicTypeContract()
            : base()
        {
            _abbreviation = new LanguageTranslationContract();
            _description = new LanguageTranslationContract();
        }

        #endregion
    }
}