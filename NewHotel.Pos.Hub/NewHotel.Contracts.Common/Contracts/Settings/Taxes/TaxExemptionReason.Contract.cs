﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class TaxExemptionReasonContract : BaseContract<string>
    {
        #region Members

        [DataMember]
        internal string _description;
        [DataMember]
        internal string _countryId;

        #endregion
        #region Constructor

        public TaxExemptionReasonContract(string id, string description, string countryId)
            : base()
        {
            Id = id;
            _description = description;
            _countryId = countryId;
        }

        #endregion
        #region Properties
      
        public string Description { get { return _description; } }
        public string CountryId { get { return _countryId; } }

        #endregion
    }
}