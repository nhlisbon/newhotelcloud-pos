﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TaxesSequenceContract : BaseContract
    {
        #region Members

        private string _abbreviation;

        [DataMember]
        internal LanguageTranslationContract _description;

        #endregion
        #region Properties

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Abbreviation required.")]
        public string Abbreviation
        {
            get { return _abbreviation; }
            set { Set(ref _abbreviation, value, "Abbreviation"); }
        }
        [DataMember]
        public string Country { get; set; }

        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
        }

        #endregion
        #region Constructor

        public TaxesSequenceContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
    }
}