﻿using System;

namespace NewHotel.Contracts
{
    public class TaxCalculation
    {
        public decimal GrossValue { get; set; }
        public decimal NetValue { get; set; }
        public decimal? CityTaxValue { get; set; }

        public decimal? TaxValue1 { get; set; }
        public decimal? TaxBase1 { get; set; }
        public decimal? TaxIncidence1 { get; set; }

        public decimal? TaxValue2 { get; set; }
        public decimal? TaxBase2 { get; set; }
        public decimal? TaxIncidence2 { get; set; }

        public decimal? TaxValue3 { get; set; }
        public decimal? TaxBase3 { get; set; }
        public decimal? TaxIncidence3 { get; set; }
        public decimal RetentionPercent1 { get; set; }
        public decimal RetentionPercent2 { get; set; }
        public decimal RetentionPercent3 { get; set; }

        public RoundMode? RoundMode { get; set; }
        public short Decimals { get; set; }

        public readonly string Country;

        public readonly Guid? TaxRateId1;
        public readonly decimal? Percent1;

        public readonly Guid? TaxRateId2;
        public readonly decimal? Percent2;
        public readonly ApplyTax2? ApplyMode2;

        public readonly Guid? TaxRateId3;
        public readonly decimal? Percent3;
        public readonly ApplyTax3? ApplyMode3;

        public readonly decimal CityTaxPercent;
        public readonly DiscountApplyOver CityTaxAppliedOver;
        public readonly bool CityTaxIncluded;

        public decimal TaxTotal
        {
            get { return (TaxValue1 ?? decimal.Zero) + (TaxValue2 ?? decimal.Zero) + (TaxValue3 ?? decimal.Zero); }
        }

        public TaxCalculation(string country,
            Guid? taxRateId1, decimal? percent1,
            Guid? taxRateId2, decimal? percent2, ApplyTax2? applyMode2,
            Guid? taxRateId3, decimal? percent3, ApplyTax3? applyMode3,
            decimal cityTaxPercent = decimal.Zero,
            DiscountApplyOver cityTaxAppliedOver = DiscountApplyOver.Net)
        {
            Country = country;

            TaxRateId1 = taxRateId1;
            Percent1 = percent1;

            TaxRateId2 = taxRateId2;
            Percent2 = percent2;
            ApplyMode2 = applyMode2;

            TaxRateId3 = taxRateId3;
            Percent3 = percent3;
            ApplyMode3 = applyMode3;

            CityTaxPercent = Math.Abs(cityTaxPercent);
            CityTaxAppliedOver = cityTaxAppliedOver;
            CityTaxIncluded = cityTaxPercent < decimal.Zero;
        }

        private decimal RoundTo(decimal value, short decimals)
        {
            if (!RoundMode.HasValue)
                return value;
            switch (RoundMode.Value)
            {
                case Contracts.RoundMode.Close:
                    return decimal.Round(value, decimals, MidpointRounding.AwayFromZero);
                case Contracts.RoundMode.Truncate:
                    return Math.Truncate(((decimal)Math.Pow(10, decimals) * value)) / (decimal)Math.Pow(10, decimals);
            }
            return 0;
        }

        private void CalculateIncidence(decimal value, short decimals)
        {
            var totalTax = TaxTotal;
            if (totalTax > decimal.Zero)
            {
                if (TaxValue1.HasValue)
                {
                    TaxIncidence1 = (TaxValue1 / totalTax * value) - TaxValue1;
                    if (RoundMode.HasValue)
                        TaxIncidence1 = RoundTo(TaxIncidence1.Value, Decimals);
                }

                if (TaxValue2.HasValue)
                {
                    TaxIncidence2 = (TaxValue2 / totalTax * value) - TaxValue2;
                    if (RoundMode.HasValue)
                        TaxIncidence2 = RoundTo(TaxIncidence2.Value, Decimals);
                }

                if (TaxValue3.HasValue)
                {
                    TaxIncidence3 = (TaxValue3 / totalTax * value) - TaxValue3;
                    if (RoundMode.HasValue)
                        TaxIncidence3 = RoundTo(TaxIncidence3.Value, Decimals);
                }

                var taxIncidence =
                    (TaxIncidence1 ?? decimal.Zero) +
                    (TaxIncidence2 ?? decimal.Zero) +
                    (TaxIncidence3 ?? decimal.Zero);

                var taxIncidenceDiff = value - taxIncidence;
                if (taxIncidenceDiff != decimal.Zero)
                {
                    if (TaxIncidence3.HasValue)
                        TaxIncidence3 = TaxIncidence3.Value + taxIncidenceDiff;
                    else if (TaxIncidence2.HasValue)
                        TaxIncidence2 = TaxIncidence2.Value + taxIncidenceDiff;
                    else if (TaxIncidence1.HasValue)
                        TaxIncidence1 = TaxIncidence1.Value + taxIncidenceDiff;
                }
            }
        }

        public void DeduceTax(decimal value)
        {
            GrossValue = RoundTo(value, Decimals);

            switch (Country)
            {
                // caso específico de Brasil
                case "BR":
                    {
                        // city tax incluido sobre neto o bruto
                        if (CityTaxIncluded)
                        {
                            if (CityTaxPercent != decimal.Zero)
                            {
                                switch (CityTaxAppliedOver)
                                {
                                    case DiscountApplyOver.Net:
                                        var netValue = GrossValue * (decimal.One
                                            - (Percent1 ?? decimal.Zero) / 100
                                            - (Percent2 ?? decimal.Zero) / 100
                                            - (Percent3 ?? decimal.Zero) / 100) /
                                            (decimal.One + CityTaxPercent / 100);
                                        CityTaxValue = RoundTo(netValue * CityTaxPercent / 100, Decimals);
                                        break;
                                    case DiscountApplyOver.Gross:
                                        CityTaxValue = GrossValue - RoundTo(GrossValue / (decimal.One + CityTaxPercent / 100), Decimals);
                                        break;
                                }
                            }

                            GrossValue -= (CityTaxValue ?? decimal.Zero);
                        }

                        // primer impuesto definido
                        if (TaxRateId1.HasValue)
                        {
                            TaxValue1 = GrossValue * (Percent1 ?? decimal.Zero) / 100;
                            if (RoundMode.HasValue)
                                TaxValue1 = RoundTo(TaxValue1.Value, Decimals);
                            TaxBase1 = GrossValue;
                        }

                        // segundo impuesto definido
                        if (TaxRateId2.HasValue)
                        {
                            TaxValue2 = GrossValue * (Percent2 ?? decimal.Zero) / 100;
                            if (RoundMode.HasValue)
                                TaxValue2 = RoundTo(TaxValue2.Value, Decimals);
                            TaxBase2 = GrossValue;
                        }

                        // tercer impuesto definido
                        if (TaxRateId3.HasValue)
                        {
                            TaxValue3 = GrossValue * (Percent3 ?? decimal.Zero) / 100;
                            if (RoundMode.HasValue)
                                TaxValue3 = RoundTo(TaxValue3.Value, Decimals);
                            TaxBase3 = GrossValue;
                        }

                        // ajuste final del valor neto
                        NetValue = GrossValue;
                        if (TaxTotal > decimal.Zero)
                            NetValue -= TaxTotal;

                        // city tax adicionado sobre neto o bruto
                        if (!CityTaxIncluded)
                        {
                            if (CityTaxPercent != decimal.Zero)
                            {
                                switch (CityTaxAppliedOver)
                                {
                                    case DiscountApplyOver.Net:
                                        CityTaxValue = RoundTo(NetValue * CityTaxPercent / 100, Decimals);
                                        break;
                                    case DiscountApplyOver.Gross:
                                        CityTaxValue = RoundTo(GrossValue * CityTaxPercent / 100, Decimals);
                                        break;
                                }
                            }
                        }
                    }
                    break;
                // caso general
                default:
                    {
                        var factor1 = decimal.Zero;
                        var factor2 = decimal.Zero;
                        var base2 = decimal.One;
                        var factor3 = decimal.Zero;
                        var base3 = decimal.One;

                        // primer impuesto definido
                        if (TaxRateId1.HasValue)
                            factor1 = (Percent1 ?? decimal.Zero) / 100;

                        // segundo impuesto definido
                        if (TaxRateId2.HasValue)
                        {
                            if (ApplyMode2.HasValue)
                            {
                                // modos de aplicación para el 2do impuesto
                                switch (ApplyMode2.Value)
                                {
                                    case ApplyTax2.OverBase:
                                        base2 = decimal.One;
                                        break;
                                    case ApplyTax2.OverBasePlusRate1:
                                        base2 = decimal.One + (Percent1 ?? decimal.Zero) / 100;
                                        break;
                                }

                                factor2 = base2 * (Percent2 ?? decimal.Zero) / 100;
                            }
                        }

                        // tercer impuesto definido
                        if (TaxRateId3.HasValue)
                        {
                            if (ApplyMode3.HasValue)
                            {
                                switch (ApplyMode3.Value)
                                {
                                    case ApplyTax3.OverBase:
                                        base3 = decimal.One;
                                        break;
                                    case ApplyTax3.OverBasePlusRate1:
                                        base3 = decimal.One + (Percent1 ?? decimal.Zero) / 100;
                                        break;
                                    case ApplyTax3.OverBasePlusRate2:
                                        base3 = decimal.One + (Percent2 ?? decimal.Zero) / 100;
                                        break;
                                    case ApplyTax3.OverBasePlusRate1PlusRate2:
                                        base3 = decimal.One + (Percent1 ?? decimal.Zero) / 100 + (Percent2 ?? decimal.Zero) / 100;
                                        break;
                                }

                                factor3 = base3 * (Percent3 ?? decimal.Zero) / 100;
                            }
                        }

                        // city tax incluido
                        var factorCityTax = decimal.Zero;
                        if (CityTaxIncluded)
                        {
                            if (CityTaxPercent != decimal.Zero)
                            {
                                switch (CityTaxAppliedOver)
                                {
                                    // sobre valor neto
                                    case DiscountApplyOver.Net:
                                        factorCityTax = CityTaxPercent / 100;
                                        break;
                                    // sobre valor bruto
                                    case DiscountApplyOver.Gross:
                                        CityTaxValue = GrossValue - RoundTo(GrossValue / (decimal.One + CityTaxPercent / 100), Decimals);
                                        GrossValue -= CityTaxValue.Value;
                                        break;
                                }
                            }
                        }

                        // calculo valor neto
                        NetValue = GrossValue / (decimal.One + factor1 + factor2 + factor3 + factorCityTax);

                        // calcular city tax incluido sobre valor neto y decrementar el bruto
                        if (factorCityTax > decimal.Zero)
                        {
                            CityTaxValue = RoundTo(NetValue * factorCityTax, Decimals);
                            GrossValue -= CityTaxValue.Value;
                        }
                        // calcular city tax para otros casos
                        else if (CityTaxPercent != decimal.Zero)
                        {
                            switch (CityTaxAppliedOver)
                            {
                                case DiscountApplyOver.Net:
                                    CityTaxValue = RoundTo(NetValue * CityTaxPercent / 100, Decimals);
                                    break;
                                case DiscountApplyOver.Gross:
                                    if (!CityTaxIncluded)
                                        CityTaxValue = RoundTo(GrossValue * CityTaxPercent / 100, Decimals);
                                    break;
                            }
                        }

                        // calculo primer impuesto
                        if (TaxRateId1.HasValue)
                        {
                            TaxValue1 = NetValue * factor1;
                            TaxBase1 = NetValue;

                            if (RoundMode.HasValue)
                            {
                                TaxValue1 = RoundTo(TaxValue1.Value, Decimals);
                                TaxBase1 = RoundTo(TaxBase1.Value, Decimals);
                            }
                        }

                        // calculo segundo impuesto
                        if (TaxRateId2.HasValue)
                        {
                            TaxValue2 = NetValue * factor2;
                            TaxBase2 = NetValue * base2;

                            if (RoundMode.HasValue)
                            {
                                TaxValue2 = RoundTo(TaxValue2.Value, Decimals);
                                TaxBase2 = RoundTo(TaxBase2.Value, Decimals);
                            }
                        }

                        // calculo tercer impuesto
                        if (TaxRateId3.HasValue)
                        {
                            TaxValue3 = NetValue * factor3;
                            TaxBase3 = NetValue * base3;

                            if (RoundMode.HasValue)
                            {
                                TaxValue3 = RoundTo(TaxValue3.Value, Decimals);
                                TaxBase3 = RoundTo(TaxBase3.Value, Decimals);
                            }
                        }

                        // ajuste final del valor neto
                        NetValue = GrossValue;
                        if (TaxTotal > decimal.Zero)
                            NetValue -= TaxTotal;
                    }
                    break;
            }

            // calculo de las incidencias
            CalculateIncidence(GrossValue, Decimals);
        }

        public void AddTax(decimal value)
        {
            NetValue = RoundTo(value, Decimals);

            var factor1 = decimal.Zero;
            var factor2 = decimal.Zero;
            var base2 = decimal.One;
            var factor3 = decimal.Zero;
            var base3 = decimal.One;

            // primer impuesto definido
            if (TaxRateId1.HasValue)
                factor1 = (Percent1 ?? decimal.Zero) / 100;

            // segundo impuesto definido
            if (TaxRateId2.HasValue)
            {
                if (ApplyMode2.HasValue)
                {
                    // modos de aplicación para el 2do impuesto
                    switch (ApplyMode2.Value)
                    {
                        case ApplyTax2.OverBase:
                            base2 = decimal.One;
                            break;
                        case ApplyTax2.OverBasePlusRate1:
                            base2 = decimal.One + (Percent1 ?? decimal.Zero) / 100;
                            break;
                    }

                    factor2 = base2 * (Percent2 ?? decimal.Zero) / 100;
                }
            }

            // tercer impuesto definido
            if (TaxRateId3.HasValue)
            {
                if (ApplyMode3.HasValue)
                {
                    switch (ApplyMode3.Value)
                    {
                        case ApplyTax3.OverBase:
                            base3 = decimal.One;
                            break;
                        case ApplyTax3.OverBasePlusRate1:
                            base3 = decimal.One + (Percent1 ?? decimal.Zero) / 100;
                            break;
                        case ApplyTax3.OverBasePlusRate2:
                            base3 = decimal.One + (Percent2 ?? decimal.Zero) / 100;
                            break;
                        case ApplyTax3.OverBasePlusRate1PlusRate2:
                            base3 = decimal.One + (Percent1 ?? decimal.Zero) / 100 + (Percent2 ?? decimal.Zero) / 100;
                            break;
                    }

                    factor3 = base3 * (Percent3 ?? decimal.Zero) / 100;
                }
            }

            // city tax solo para incluido
            if (CityTaxIncluded)
            {
                if (CityTaxPercent != decimal.Zero)
                {
                    switch (CityTaxAppliedOver)
                    {
                        case DiscountApplyOver.Net:
                            CityTaxValue = NetValue - RoundTo(NetValue / (decimal.One + CityTaxPercent / 100), Decimals);
                            break;
                        case DiscountApplyOver.Gross:
                            CityTaxValue = NetValue - RoundTo(NetValue / (decimal.One + (decimal.One + factor1 + factor2 + factor3) * CityTaxPercent / 100), Decimals);
                            break;
                    }

                    // retirar city tax del valor neto
                    NetValue -= CityTaxValue.Value;
                }
            }

            // calculo primer impuesto
            if (TaxRateId1.HasValue)
            {
                var taxValue1 = NetValue * factor1;
                var taxBase1 = NetValue;

                if (RoundMode.HasValue)
                {
                    taxValue1 = RoundTo(taxValue1, Decimals);
                    taxBase1 = RoundTo(taxBase1, Decimals);
                }

                TaxValue1 = taxValue1;
                TaxBase1 = taxBase1;
            }

            // calculo segundo impuesto
            if (TaxRateId2.HasValue)
            {
                var taxValue2 = NetValue * factor2;
                var taxBase2 = NetValue * base2;

                if (RoundMode.HasValue)
                {
                    taxValue2 = RoundTo(taxValue2, Decimals);
                    taxBase2 = RoundTo(taxBase2, Decimals);
                }

                TaxValue2 = taxValue2;
                TaxBase2 = taxBase2;
            }

            // calculo tercer impuesto
            if (TaxRateId3.HasValue)
            {
                var taxValue3 = NetValue * factor3;
                var taxBase3 = NetValue * base3;

                if (RoundMode.HasValue)
                {
                    taxValue3 = RoundTo(taxValue3, Decimals);
                    taxBase3 = RoundTo(taxBase3, Decimals);
                }

                TaxValue3 = taxValue3;
                TaxBase3 = taxBase3;
            }

            // calcular el valor bruto
            GrossValue = NetValue + TaxTotal;

            // calcular el city tax sobre neto o bruto
            if (!CityTaxIncluded)
            {
                if (CityTaxPercent != decimal.Zero)
                {
                    switch (CityTaxAppliedOver)
                    {
                        // sobre valor neto
                        case DiscountApplyOver.Net:
                            CityTaxValue = RoundTo(NetValue * CityTaxPercent / 100, Decimals);
                            break;
                        // sobre valor bruto
                        case DiscountApplyOver.Gross:
                            CityTaxValue = RoundTo(GrossValue * CityTaxPercent / 100, Decimals);
                            break;
                    }
                }
            }
        }
    }

    public enum RoundMode
    {
        Truncate, Close
    }

}