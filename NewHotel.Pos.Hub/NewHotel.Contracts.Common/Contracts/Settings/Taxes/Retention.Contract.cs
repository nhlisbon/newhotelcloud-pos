﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RetentionContract : BaseContract
    {
        #region Members

        private string _abbreviation;
        private decimal _retentionPercent;
        private string _freeCode;
        private bool _inactive;

        [DataMember]
        internal LanguageTranslationContract _description;

        #endregion
        #region Properties

        [ReflectionExclude]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        public string Abbreviation { get { return _abbreviation; } set { Set(ref _abbreviation, value, "Abbreviation"); } }
        [DataMember]
        public decimal RetentionPercent { get { return _retentionPercent; } set { Set(ref _retentionPercent, value, "RetentionPercent"); } }
        [DataMember]
        public string FreeCode { get { return _freeCode; } set { Set(ref _freeCode, value, "FreeCode"); } }
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }

        #endregion
        #region Constructor

        public RetentionContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
    }
}