﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TaxesRegionContract : BaseContract
    {
        #region Members

        private string _country;
        private string _freeCode;

        [DataMember]
        internal LanguageTranslationContract _description;

        #endregion
        #region Properties

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Country required.")]
        public string Country
        {
            get { return _country; }
            set { Set(ref _country, value, "Country"); }
        }

        [DataMember]
        public string FreeCode
        {
            get { return _freeCode; }
            set { Set(ref _freeCode, value, "FreeCode"); }
        }

        #endregion
        #region Constructor

        public TaxesRegionContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
    }
}