﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(TaxSchemaContract), "ValidateTaxSchema")]
    public class TaxSchemaContract : BaseContract
    {
        #region Members

        private bool _isDefault;
        private string _country;
        private Guid? _taxRegion;
        private DateTime? _expiredDate;
        private Guid? _schemaLink;
        private Guid? _taxRateId1;
        private string _taxRateDesc1;
        private decimal _retentionPercent1;
        private Guid? _taxRateId2;
        private string _taxRateDesc2;
        private ApplyTax2? _mode2;
        private decimal _retentionPercent2;
        private Guid? _taxRateId3;
        private string _taxRateDesc3;
        private ApplyTax3? _mode3;
        private decimal _retentionPercent3;

        [DataMember]
        internal LanguageTranslationContract _description;

        #endregion
        #region Properties

        [DataMember]
        public bool IsDefault { get { return _isDefault; } set { Set(ref _isDefault, value, "IsDefault"); } }
        [DataMember]
        public string Country { get { return _country; } set { Set(ref _country, value, "Country"); } }
        [DataMember]
        public Guid? TaxRegion { get { return _taxRegion; } set { Set(ref _taxRegion, value, "TaxRegion"); } }
        [DataMember]
        public DateTime? ExpiredDate { get { return _expiredDate; } set { Set(ref _expiredDate, value, "ExpiredDate"); } }
        [DataMember]
        public Guid? SchemaLink { get { return _schemaLink; } set { Set(ref _schemaLink, value, "SchemaLink"); } }
        [DataMember]
        public Guid? TaxRateId1 { get { return _taxRateId1; } set { Set(ref _taxRateId1, value, "TaxRateId1"); } }
        [DataMember]
        public string TaxRateDesc1 { get { return _taxRateDesc1; } set { Set(ref _taxRateDesc1, value, "TaxRateDesc1"); } }
        [DataMember]
        public decimal RetentionPercent1 { get { return _retentionPercent1; } set { Set(ref _retentionPercent1, value, "RetentionPercent1"); } }
        [DataMember(Order = 0)]
        public Guid? TaxRateId2
        {
            get { return _taxRateId2; }
            set
            {
                if (Set(ref _taxRateId2, value, "TaxRateId2"))
                {
                    if (_taxRateId2.HasValue)
                    {
                        if (!_mode2.HasValue)
                            Mode2 = ApplyTax2.OverBase;
                    }
                    else
                        Mode2 = null;
                }
            }
        }
        [DataMember]
        public string TaxRateDesc2 { get { return _taxRateDesc2; } set { Set(ref _taxRateDesc2, value, "TaxRateDesc2"); } }
        [DataMember(Order = 1)]
        public ApplyTax2? Mode2
        {
            get { return _mode2; }
            set { Set(ref _mode2, _taxRateId2.HasValue ? value : null, "Mode2"); }
        }
        [DataMember]
        public decimal RetentionPercent2 { get { return _retentionPercent2; } set { Set(ref _retentionPercent2, value, "RetentionPercent2"); } }
        [DataMember(Order = 0)]
        public Guid? TaxRateId3
        {
            get { return _taxRateId3; }
            set
            {
                if (Set(ref _taxRateId3, value, "TaxRateId3"))
                {
                    if (_taxRateId3.HasValue)
                    {
                        if (!_mode3.HasValue)
                            Mode3 = ApplyTax3.OverBase;
                    }
                    else
                        Mode3 = null;
                }
            }
        }
        [DataMember]
        public string TaxRateDesc3 { get { return _taxRateDesc3; } set { Set(ref _taxRateDesc3, value, "TaxRateDesc3"); } }
        [DataMember(Order = 1)]
        public ApplyTax3? Mode3
        {
            get { return _mode3; }
            set { Set(ref _mode3, _taxRateId3.HasValue ? value : null, "Mode3"); }
        }
        [DataMember]
        public decimal RetentionPercent3 { get { return _retentionPercent3; } set { Set(ref _retentionPercent3, value, "RetentionPercent3"); } }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public bool ApplyModeEnabled
        {
            get { return _country != "BR"; }
        }

        #endregion
        #region Contracts

        [ReflectionExclude]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        #endregion
        #region Constructor

        public TaxSchemaContract()
            : base()
        {
            Description = new LanguageTranslationContract();
        }

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateTaxSchema(TaxSchemaContract obj)
        {
            if (!obj.TaxRateId1.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Tax Rate 1 can't be empty.");
            if (obj.TaxRateId2.HasValue && !obj.Mode2.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Tax Rate Apply 2 can't be empty.");
            if (obj.TaxRateId3.HasValue && !obj.Mode3.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Tax Rate Apply 3 can't be empty.");

            if (obj.RetentionPercent1 < decimal.Zero || obj.RetentionPercent1 > 100M)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Retention 1 must be between 0 and 100");
            if (obj.RetentionPercent2 < decimal.Zero || obj.RetentionPercent2 > 100M)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Retention 2 must be between 0 and 100");
            if (obj.RetentionPercent3 < decimal.Zero || obj.RetentionPercent3 > 100M)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Retention 3 must be between 0 and 100");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}