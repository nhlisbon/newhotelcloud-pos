﻿using System;
using System.Linq;

namespace NewHotel.Contracts
{
    public class TaxContract : BaseContract
    {
        #region Constructor

        public TaxContract()
            : base() 
        {
            TaxIncluded = true;
            Taxes = new TypedList<MovementTaxDetailContract>();
        }

        public TaxContract(Guid? taxSchemaId, bool taxIncluded, decimal informedPrice)
            : this()
        {
            TaxSchemaId = taxSchemaId;
            TaxIncluded = taxIncluded;
            InformedPrice = informedPrice;
        }
        
        #endregion
        #region Properties

        public Guid? TaxSchemaId { get; private set; }
        public bool TaxIncluded { get; private set; }
        public decimal InformedPrice { get; private set; }
        public decimal GrossValue { get; set; }
        public decimal NetValue { get; set; }
        public decimal CityTaxValue { get; set; }

        public decimal TotalTaxes 
        { 
            get { return Taxes.Sum(x => x.TaxValue); }
        }

        #endregion
        #region Contracts

        [ReflectionExclude]
        public TypedList<MovementTaxDetailContract> Taxes { get; private set; }

        #endregion
    }
}