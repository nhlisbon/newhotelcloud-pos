﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TaxesRateContract : BaseContract
    {
        #region Members

        [DataMember]
        internal LanguageTranslationContract _description;

        private Guid? _taxRegion;
        private Guid? _taxSequence;
        private decimal? _percent;
        private string _freeCode;
        private bool _inactive;

        #endregion
        #region Properties

        [DataMember]
        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Tax region required.")]
        public Guid? TaxRegion
        {
            get { return _taxRegion; }
            set { Set(ref _taxRegion, value, "TaxRegion"); }
        }

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Sequence required.")]
        public Guid? TaxSequence
        {
            get { return _taxSequence; }
            set { Set(ref _taxSequence, value, "TaxSequence"); }
        }

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Percent required.")]
        public decimal? Percent
        {
            get { return _percent; }
            set { Set(ref _percent, value, "Percent"); }
        }

        [DataMember]
        public string FreeCode
        {
            get { return _freeCode; }
            set { Set(ref _freeCode, value, "FreeCode"); }
        }

        [DataMember]
        public bool Inactive
        {
            get { return _inactive; }
            set { Set(ref _inactive, value, "Inactive"); }
        }

        [DataMember]
        public string Country { get; set; }
        #endregion
        #region Constructor

        public TaxesRateContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class TaxesRateReplacementContract : BaseContract
    {
        #region Members

        [DataMember]
        public Guid FromTaxRateId { get; internal set; }
        [DataMember]
        public string FromTaxRateDescription { get; internal set; } 
        [DataMember]
        public Guid? ToTaxRateId { get; set; } 

        #endregion
        #region Constructor

        public TaxesRateReplacementContract(Guid fromTaxRateId, string fromTaxRateDescription)
        {
            FromTaxRateId = fromTaxRateId;
            FromTaxRateDescription = fromTaxRateDescription;
        }

        #endregion
    }
}