﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RetentionSchemaDetailContract : BaseContract
    {
        #region Members

        private Guid _retentionId;
        private string _retentionDescription;

        #endregion
        #region Properties

        [DataMember]
        public Guid RetentionId { get { return _retentionId; } set { Set(ref _retentionId, value, "RetentionId"); } }
        [DataMember]
        public string RetentionDescription { get { return _retentionDescription; } set { Set(ref _retentionDescription, value, "RetentionDescription"); } }

        #endregion
        #region Constructor

        public RetentionSchemaDetailContract()
            : base()
        {
        }

        #endregion
    }
}