﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class RetentionSchemaContract : BaseContract
    {
        #region Members

        private bool _inactive;

        [DataMember]
        internal LanguageTranslationContract _description;

        #endregion
        #region Properties

        [ReflectionExclude]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }

        #endregion
        #region Lists

        [DataMember]
        [ReflectionExclude]
        public TypedList<RetentionSchemaDetailContract> Details { get; internal set; }

        #endregion
        #region Constructor

        public RetentionSchemaContract()
            : base()
        {
            _description = new LanguageTranslationContract();
            Details = new TypedList<RetentionSchemaDetailContract>();
        }

        #endregion
    }
}