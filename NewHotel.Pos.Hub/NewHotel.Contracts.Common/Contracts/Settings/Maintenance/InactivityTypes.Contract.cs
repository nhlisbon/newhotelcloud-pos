﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class InactivityTypeContract : BaseContract
    {
        #region Members

        private ARGBColor _color;
        private bool _usedForOutOfRental;

        [DataMember]
        internal LanguageTranslationContract _description;

        #endregion
        #region Properties

        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        public LanguageTranslationContract Description
        {
            get { return _description; }
        }

        [DataMember]
        public ARGBColor Color { get { return _color; } set { Set(ref _color, value, "Color"); } }
        [DataMember]
        public bool UsedForOutOfRental { get { return _usedForOutOfRental; } set { Set(ref _usedForOutOfRental, value, "UsedForOutOfRental"); } }

        #endregion
        #region Constructor

        public InactivityTypeContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
    }
}