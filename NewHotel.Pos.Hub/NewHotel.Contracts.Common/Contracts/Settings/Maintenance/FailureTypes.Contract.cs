﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class FailureTypeContract : BaseContract
    {
        #region Members

        [DataMember]
        internal LanguageTranslationContract _description;

        #endregion
        #region Constructors

        public FailureTypeContract()
            : base()
        {
            _description = new LanguageTranslationContract();
            SubTypes = new TypedList<FailureSubTypesContract>();
        }

        #endregion
        #region Properties

        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        public LanguageTranslationContract Description
        {
            get { return _description; }
        }

        #endregion
        #region Lists

        [DataMember]
        [ReflectionExclude]
        public TypedList<FailureSubTypesContract> SubTypes { get; internal set; }

        #endregion     
    }
}