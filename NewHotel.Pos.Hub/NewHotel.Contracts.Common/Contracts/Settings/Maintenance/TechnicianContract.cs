﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TechnicianContract : EmployeeContract
    {
        public TechnicianContract(DateTime today)
            : base(today)
        {
        }
    }
}