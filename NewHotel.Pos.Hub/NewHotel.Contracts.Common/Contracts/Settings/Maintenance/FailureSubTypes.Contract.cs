﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class FailureSubTypesContract : BaseContract
    {
        #region Properties

        [ReflectionExclude]
        [DataMember]
        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        public LanguageTranslationContract Description { get; internal set; }

        #endregion
        #region Constructor

        public FailureSubTypesContract()
            : base()
        {
            Description = new LanguageTranslationContract();       
        }
       
        #endregion
    }
}