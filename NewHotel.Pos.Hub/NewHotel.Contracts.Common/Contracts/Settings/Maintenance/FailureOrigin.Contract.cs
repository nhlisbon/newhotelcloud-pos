﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class FailureOriginContract : BaseContract
    {
        #region Members

        [DataMember]
        internal LanguageTranslationContract _description;

        #endregion
        #region Properties

        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        public LanguageTranslationContract Description
        {
            get { return _description; }
        }

        #endregion
        #region Constructor

        public FailureOriginContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
    }
}