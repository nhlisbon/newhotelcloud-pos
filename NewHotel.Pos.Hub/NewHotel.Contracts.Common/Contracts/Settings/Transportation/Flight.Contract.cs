﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class FlightContract : BaseContract
    {
        #region Members
        private string _description;
        private FlightType _type;
        private FlightStatus _status;
        private DateTime _time;
        private bool _inactive;
        private bool _sunday;
        private bool _monday;
        private bool _tuesday;
        private bool _wednesday;
        private bool _thurday;
        private bool _friday;
        private bool _saturday;
        #endregion

        #region Properties
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public FlightType Type { get { return _type; } set { Set(ref _type, value, "Type"); } }
        [DataMember]
        public FlightStatus Status { get { return _status; } set { Set(ref _status, value, "Status"); } }
        [DataMember]
        public DateTime Time { get { return _time; } set { Set(ref _time, value, "Time"); } }
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }
        [DataMember]
        public bool Sunday { get { return _sunday; } set { Set(ref _sunday, value, "Sunday"); } }
        [DataMember]
        public bool Monday { get { return _monday; } set { Set(ref _monday, value, "Monday"); } }
        [DataMember]
        public bool Tuesday { get { return _tuesday; } set { Set(ref _tuesday, value, "Tuesday"); } }
        [DataMember]
        public bool Wednesday { get { return _wednesday; } set { Set(ref _wednesday, value, "Wednesday"); } }
        [DataMember]
        public bool Thurday { get { return _thurday; } set { Set(ref _thurday, value, "Thurday"); } }
        [DataMember]
        public bool Friday { get { return _friday; } set { Set(ref _friday, value, "Friday"); } }
        [DataMember]
        public bool Saturday { get { return _saturday; } set { Set(ref _saturday, value, "Saturday"); } }
        #endregion
    }
}
