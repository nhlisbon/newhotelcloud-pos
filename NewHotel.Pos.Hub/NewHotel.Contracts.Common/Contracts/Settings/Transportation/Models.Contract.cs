﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ModelsContract : BaseContract
    {
        #region Members
        private string _description;
        private Guid _marketBrand;
        #endregion

        #region Contract Properties
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }

        [DataMember]
        [Required("MarketBrand")]
        public Guid MarketBrand { get { return _marketBrand; } set { Set(ref _marketBrand, value, "MarketBrand"); } }
        #endregion

        #region Constructor

        public ModelsContract(Guid id, Guid marketBrand, string description)
            : base()
        {
            this.Id = id;
            this.MarketBrand = marketBrand;
            this.Description = description;
        }

        public ModelsContract()
            : base() { }

        #endregion
    }
}
