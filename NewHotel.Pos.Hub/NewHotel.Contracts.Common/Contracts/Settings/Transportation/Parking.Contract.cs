﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ParkingContract : BaseContract
    {
        #region Members
        private string _description;
        private Guid? _parkingZone;
        private short? _capacity;
        #endregion

        #region Properties
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public string Description { get { return _description; } set { Set<string>(ref _description, value, "Description"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Parking zone required.")]
        public Guid? ParkingZone { get { return _parkingZone; } set { Set<Guid?>(ref _parkingZone, value, "ParkingZone"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Capacity required.")]
        public short? Capacity { get { return _capacity; } set { Set<short?>(ref _capacity, value, "Capacity"); } }
        #endregion
    }
}
