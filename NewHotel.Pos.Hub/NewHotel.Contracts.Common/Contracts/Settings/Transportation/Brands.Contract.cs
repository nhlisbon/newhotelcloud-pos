﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class BrandsContract : BaseContract
    {
        #region Members
        private string _description;
        private TypedList<ModelsContract> _modelsInBrand;
        private Guid _installation;
        #endregion

        #region Contract Properties
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }

        [ReflectionExclude]
        public TypedList<ModelsContract> ModelsInBrand { get { return _modelsInBrand; } }
        [DataMember]
        public Guid Installation { get { return _installation; } set { Set(ref _installation, value, "Installation"); } }
        #endregion

        #region Constructor
        public BrandsContract()
            : base()
        {
            _modelsInBrand = new TypedList<ModelsContract>();
        }

        [IgnoreDataMember]
        [ReflectionExclude]
        public Guid[] ModelsIds
        {
            get { return ModelsInBrand.Select(x => x.MarketBrand).ToArray(); }
        }
        #endregion
    }
}
