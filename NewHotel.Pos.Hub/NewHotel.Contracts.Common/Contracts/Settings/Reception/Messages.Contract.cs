﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(MessagesContract), "ValidateMessage")]
    public class MessagesContract : BaseContract
    {
        #region Members
        private MessageStatus _status;
        private Guid _from;
        private Guid[] _users;
        private string _subject;
        private Clob _body;
        private DateTime _sentDate;
        private DateTime? _readDate;
        private DateTime? _replyDate;
        #endregion
        #region Properties
        [DataMember]
        public MessageStatus Status { get { return _status; } set { Set(ref _status, value, "Status"); } }
        [DataMember]
        public Guid From { get { return _from; } set { Set(ref _from, value, "From"); } }
        [DataMember]
        public Guid[] Users { get { return _users; } set { Set(ref _users, value, "Users"); } }
        [DataMember]
        public string Subject { get { return _subject; } set { Set(ref _subject, value, "Subject"); } }
        [DataMember]
        public Clob Body { get { return _body; } set { Set(ref _body, value, "Body"); } }
        [DataMember]
        public DateTime SentDate { get { return _sentDate; } set { Set(ref _sentDate, value, "SentDate"); } }
        [DataMember]
        public DateTime? ReadDate { get { return _readDate; } set { Set(ref _readDate, value, "ReadDate"); } }
        [DataMember]
        public DateTime? ReplyDate { get { return _replyDate; } set { Set(ref _replyDate, value, "ReplyDate"); } }
        [DataMember]
        public string FromDesc { get; set; }
        [DataMember]
        public string ToDesc { get; set; }
        #endregion
        #region Constructor
        public MessagesContract()
            : base() { }
        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateMessage(MessagesContract obj)
        {
            if (obj.Users == null || obj.Users.Length == 0)
            {
                return new System.ComponentModel.DataAnnotations.ValidationResult("To field cannot be empty");
            }

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
        #endregion
    }
}
