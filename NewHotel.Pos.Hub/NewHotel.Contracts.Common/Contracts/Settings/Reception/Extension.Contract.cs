﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ExtensionContract), "ValidateExtension")]
    public class ExtensionContract : BaseContract
    {
        #region Members

        private string _description;
        private ExtensionType? _type;
        private Guid? _room;
        private string _group;
        private Guid? _currentAccountId;
        private PhonePrice? _extensionPrice;

        #endregion
        #region Properties

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public string Description
        {
            get { return _description; }
            set { Set(ref _description, value, "Description"); }
        }

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Type required.")]
        public ExtensionType? Type
        {
            get { return _type; }
            set { Set(ref _type, value, "Type"); }
        }

        [DataMember]
        public Guid? Room
        {
            get { return _room; }
            set { Set(ref _room, value, "Room"); }
        }

        [DataMember]
        public string Group
        {
            get { return _group; }
            set { Set(ref _group, value, "Group"); }
        }

        [DataMember]
        public Guid? CurrentAccountId
        {
            get { return _currentAccountId; }
            set { Set(ref _currentAccountId, value, "CurrentAccountId"); }
        }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Rate required.")]
        public PhonePrice? ExtensionPrice
        {
            get { return _extensionPrice; }
            set { Set(ref _extensionPrice, value, "ExtensionPrice"); }
        }
     
        #endregion
        #region Constructor

        public ExtensionContract()
            : base() { }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateExtension(ExtensionContract obj)
        {
            if (!obj.Room.HasValue && String.IsNullOrEmpty(obj.Group) && !obj.CurrentAccountId.HasValue)
            {
                return new System.ComponentModel.DataAnnotations.ValidationResult("Room, Group and CurrentAccount are all empty.");
            }

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
        #endregion
    }
}
