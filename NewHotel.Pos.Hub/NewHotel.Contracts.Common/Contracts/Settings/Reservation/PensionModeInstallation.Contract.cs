﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(PensionModeInstallationContract), "ValidatePensionMode")]
    public class PensionModeInstallationContract : BaseContract
    {
        #region Members

        private Guid _installationId;
        private short? _pensionbModeId;
        private string _abbreviation;
        private string _description;

        #endregion
        #region Constructor

        public PensionModeInstallationContract()
        {
            DailyDescription = new LanguageTranslationContract();
        }

        #endregion
        #region Properties

        [DataMember]
        public Guid InstallationId { get { return _installationId; } set { Set(ref _installationId, value, "InstallationId"); } }
        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Pension mode required")]
        public short? PensionModeId { get { return _pensionbModeId; } set { Set(ref _pensionbModeId, value, "PensionModeId"); } }
        [DataMember]
        public string Abbreviation { get { return _abbreviation; } set { Set(ref _abbreviation, value, "Abbreviation"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }

        [DataMember]
        public LanguageTranslationContract DailyDescription { get; set; }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidatePensionMode(PensionModeInstallationContract obj)
        {
            if (!obj.PensionModeId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Meal Plan required.");

            if (obj.DailyDescription.IsEmpty) return new System.ComponentModel.DataAnnotations.ValidationResult("Description required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
