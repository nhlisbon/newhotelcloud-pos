﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(UnexpectedDepartureTypeContract), "ValidateUnexpectedDepartureType")]
    public class UnexpectedDepartureTypeContract : BaseContract
    {
        #region Contract properties
        [DataMember]
        internal LanguageTranslationContract _description;

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        #endregion
        #region Constructor

        public UnexpectedDepartureTypeContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateUnexpectedDepartureType(UnexpectedDepartureTypeContract obj)
        {
            if (obj.Description.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
