﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(WarrantyTypeContract), "ValidateWarrantyType")]
    public class WarrantyTypeContract : BaseContract
    {
        #region Contract Properties
        [DataMember]
        internal LanguageTranslationContract _description;

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        #endregion
        #region Constructor

        public WarrantyTypeContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateWarrantyType(WarrantyTypeContract obj)
        {
            if (obj.Description.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
