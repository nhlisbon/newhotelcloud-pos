﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(RegionsContract), "ValidateRegions")]
    public class RegionsContract : BaseContract
    {
        #region Members

        private string _countryId;
        private string _description;
        private Guid? _countryZoneId;
        private string _countryZone;
        private string _freeCode;

        #endregion
        #region Constructor

        public RegionsContract()
            : base()
        {
            _postalCodes = new TypedList<PostalCodeContract>();
        }

        public RegionsContract(Guid id, string countryId, string description, Guid? countryZoneId, DateTime lastModified, string countryZone = null)
            : base()
        {
            Id = id;
            CountryId = countryId;
            Description = description;
            CountryZoneId = countryZoneId;
            CountryZone = countryZone;
            LastModified = lastModified;
            _postalCodes = new TypedList<PostalCodeContract>();
        }

        #endregion
        #region Public Properties

        [DataMember]
        public string CountryId { get { return _countryId; } set { Set(ref _countryId, value, "CountryId"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public Guid? CountryZoneId { get { return _countryZoneId; } set { Set(ref _countryZoneId, value, "CountryZoneId"); } }
        [DataMember]
        public string FreeCode { get { return _freeCode; } set { Set(ref _freeCode, value, "FreeCode"); } }

        #endregion
        #region Dummy Properties

        [DataMember]
        public string CountryZone { get { return _countryZone; } set { Set(ref _countryZone, value, "CountryZone"); } }

        #endregion
        #region Persistent Lists

        internal TypedList<PostalCodeContract> _postalCodes;

        [ReflectionExclude()]
        public TypedList<PostalCodeContract> PostalCodes
        {
            get { return _postalCodes; }
            set { _postalCodes = value; }
        }

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateRegions(RegionsContract obj)
        {
            if (string.IsNullOrEmpty(obj.Description))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}