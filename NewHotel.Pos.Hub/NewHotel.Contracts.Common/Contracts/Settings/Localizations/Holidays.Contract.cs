﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class HolidaysContract : BaseContract
    {
        #region Members

        private string _countryId;
        private DateTime _holidayDate;
        private string _holidayReason;
        private ARGBColor? _planningColor;

        #endregion

        #region properties

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Country Id required.")]
        public string CountryId
        {
            get { return _countryId; }
            set { Set<string>(ref _countryId, value, "CountryId"); }
        }

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Holiday Date required.")]
        public DateTime HolidayDate
        {
            get { return _holidayDate; }
            set { Set<DateTime>(ref _holidayDate, value, "HolidayDate"); }
        }

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Holiday Reason required.")]
        public string HolidayReason
        {
            get { return _holidayReason; }
            set { Set<string>(ref _holidayReason, value, "HolidayReason"); }
        }

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Planning Color required.")]
        public ARGBColor? PlanningColor
        {
            get { return _planningColor; }
            set { Set<ARGBColor?>(ref _planningColor, value, "PlanningColor"); }
        }

        public string HolidayDateF { get { return HolidayDate.ToMonthAndDay(); } }

        #endregion

        public HolidaysContract() : base() { }
        public HolidaysContract(Guid id, string countryId, DateTime holidayDate, string holidayReason, ARGBColor planningColor)
            : base()
        {
            this.Id = id;
            this._countryId = countryId;
            this._holidayDate = holidayDate;
            this._holidayReason = holidayReason;
            this.PlanningColor = planningColor;
        }
    }
}