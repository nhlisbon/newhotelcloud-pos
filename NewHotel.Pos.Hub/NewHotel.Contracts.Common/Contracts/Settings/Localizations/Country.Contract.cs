﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class CountryContract : BaseContract<string>
    {
        #region Constructor

        public CountryContract()
            : base()
        {
            _description = new LanguageTranslationContract();
            _countryZones = new TypedList<CountryZonesContract>();
            _regions = new TypedList<RegionsContract>();
            _holidays = new TypedList<HolidaysContract>();
            _borderPoints = new TypedList<BorderPointsContract>();
        }

        #endregion
        #region Members

        [DataMember]

        internal LanguageTranslationContract _description;
        internal TypedList<CountryZonesContract> _countryZones;
        internal TypedList<RegionsContract> _regions;
        internal TypedList<HolidaysContract> _holidays;
        internal TypedList<BorderPointsContract> _borderPoints;

        private long _worldRegionId;
        private long _languageId;
        private string _callingCode;
        private string _iso3166;
        private string _ptSEF;

        #endregion
        #region Public Properties

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        /// <summary>
        /// REMU_PK
        /// </summary>
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "World Region Id required.")]
        public long WorldRegionId { get { return _worldRegionId; } set { Set<long>(ref _worldRegionId, value, "WorldRegionId"); } }

        /// <summary>
        /// LILC_PK
        /// </summary>
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Language Id required.")]
        public long LanguageId { get { return _languageId; } set { Set<long>(ref _languageId, value, "LanguageId"); } }

        /// <summary>
        /// NACI_CACO
        /// </summary>
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Calling Code required.")]
        public string CallingCode { get { return _callingCode; } set { Set<string>(ref _callingCode, value, "CallingCode"); } }

        /// <summary>
        /// ISO_3166
        /// </summary>
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "ISO-3166 required.")]
        public string Iso3166 { get { return _iso3166; } set { Set<string>(ref _iso3166, value, "Iso3166"); } }

        /// <summary>
        /// PT_SEF
        /// </summary>
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "PT-SEF required.")]
        public string PtSEF { get { return _ptSEF; } set { Set<string>(ref _ptSEF, value, "PtSEF"); } }

        #endregion
        #region Persistent Lists

        [ReflectionExclude]
        public TypedList<CountryZonesContract> CountryZones
        {
            get { return _countryZones; }
            set { _countryZones = value; }
        }

        [ReflectionExclude]
        public TypedList<RegionsContract> Regions
        {
            get { return _regions; }
            set { _regions = value; }
        }

        [ReflectionExclude]
        public TypedList<HolidaysContract> Holidays
        {
            get { return _holidays; }
            set { _holidays = value; }
        }

        [ReflectionExclude]
        public TypedList<BorderPointsContract> BorderPoints
        {
            get { return _borderPoints; }
            set { _borderPoints = value; }
        }

        #endregion
    }
}