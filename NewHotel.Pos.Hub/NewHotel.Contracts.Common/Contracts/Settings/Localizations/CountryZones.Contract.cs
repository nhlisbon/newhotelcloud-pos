﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(CountryZonesContract), "ValidateCountryZones")]
    public class CountryZonesContract : BaseContract
    {
        #region Members

        private string _countryId;
        private string _description;
        private string _freeCode;

        #endregion
        #region Properties

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Country Id required.")]
        public string CountryId
        {
            get { return _countryId; }
            set { Set<string>(ref _countryId, value, "CountryId"); }
        }
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public string Description
        {
            get { return _description; }
            set { Set<string>(ref _description, value, "Description"); }
        }
        [DataMember]
        public string FreeCode
        {
            get { return _freeCode; }
            set { Set(ref _freeCode, value, "FreeCode"); }
        }

        #endregion
        #region Constructor

        public CountryZonesContract() : base() { }

        public CountryZonesContract(Guid id, string countryId, string description, DateTime lastModified)
            : base()
        {
            Id = id;
            CountryId = countryId;
            Description = description;
            LastModified = lastModified;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateCountryZones(CountryZonesContract obj)
        {
            if (obj.Description == String.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}