﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(BorderPointsContract), "ValidateBorderPoints")]
    public class BorderPointsContract : BaseContract
    {
        #region Members
        private string _countryId;
        [DataMember]
        internal LanguageTranslationContract _description;
        private string _descriptionTranslated;
        #endregion

        #region Properties

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Country Id required.")]
        public string CountryId
        {
            get { return _countryId; }
            set { Set<string>(ref _countryId, value, "CountryId"); }
        }
        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { Set<LanguageTranslationContract>(ref _description, value, "Description"); }
        }

        [DataMember]
        public string DescriptionTranslated
        {
            get { return _descriptionTranslated; }
            set { Set<string>(ref _descriptionTranslated, value, "DescriptionTranslated"); }
        }
        #endregion

        #region Constructor

        public BorderPointsContract()
            : base()
        {
            Description = new LanguageTranslationContract();
        }

        #endregion

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateBorderPoints(BorderPointsContract obj)
        {
            if (obj.Description == String.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description Translated required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
