﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class PostalCodeContract : BaseContract
    {
        #region members

        private Guid _regionId;
        private string _initialCode;
        private string _finalCode;
        private string _location;

        #endregion

        #region properties

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Region id required.")]
        public Guid RegionId
        {
            get { return _regionId; }
            set { Set<Guid>(ref _regionId, value, "RegionId"); }
        }

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Initial code required.")]
        public string InitialCode
        {
            get { return _initialCode; }
            set { Set<string>(ref _initialCode, value, "InitialCode"); }
        }

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Final code required.")]
        public string FinalCode
        {
            get { return _finalCode; }
            set { Set<string>(ref _finalCode, value, "FinalCode"); }
        }

        [DataMember]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "Location required.")]
        public string Location
        {
            get { return _location; }
            set { Set<string>(ref _location, value, "Location"); }
        }

        #endregion

        #region Constructor

        public PostalCodeContract() : base() { }
        public PostalCodeContract(Guid id, Guid RegionId, string initialCode, string finalCode, string location)
            : base()
        {
            Id = id;
            this.RegionId = RegionId;
            this.InitialCode = initialCode;
            this.FinalCode = finalCode;
            this.Location = location;
        }

        #endregion
    }
}
