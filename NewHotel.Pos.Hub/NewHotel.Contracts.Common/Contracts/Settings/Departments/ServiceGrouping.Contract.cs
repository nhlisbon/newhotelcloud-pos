﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ServiceGroupingContract : BaseContract
    {
        #region Members

        private ARGBColor? _colorGrouping;
        private string _abbreviation;

        #endregion
        #region Properties

        [DataMember]
        internal LanguageTranslationContract _description;

        [DataMember]
        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        [ReflectionExclude()]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }
        [DataMember]
        public string DescriptionTranslated { get; set; }
        [DataMember]
        public ARGBColor? ColorGrouping { get { return _colorGrouping; } set { Set(ref _colorGrouping, value, "ColorGrouping"); } }
        [DataMember]
        public string Abbreviation { get { return _abbreviation; } set { Set(ref _abbreviation, value, "Abbreviation"); } }

        [DataMember]
        public bool DisableEditing { get; set; }

        internal TypedList<ApplicationContract> _applications;
        // [DataMember]
        [ReflectionExclude]
        public TypedList<ApplicationContract> Applications { get { return _applications; } set { _applications = value; } }
        
        [ReflectionExclude]
        public long[] ApplicationIds
        {
            get { return Applications.Select(x => x.ApplicationId).ToArray(); }
        } 
                
        #endregion
        #region Constructor

        public ServiceGroupingContract()
            : base()
        {
            _description = new LanguageTranslationContract();
            _applications = new TypedList<ApplicationContract>();
        }

        #endregion
    }
}