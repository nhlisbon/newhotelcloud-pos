﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ApplicationContract : BaseContract
    {
        #region Members

        private long _applicationId;
        private string _applicationName;

        #endregion
        #region Proprties

        [DataMember]
        public long ApplicationId { get { return _applicationId; } set { Set(ref _applicationId, value, "ApplicationId"); } }
        [DataMember]
        public string ApplicationName { get { return _applicationName; } set { Set(ref _applicationName, value, "ApplicationName"); } }

        #endregion
        #region Constructor

        public ApplicationContract() { }
        public ApplicationContract(Guid id, long applicationId, string name)
        {
            Id = id;
            ApplicationId = applicationId;
            ApplicationName = name;
        }

        #endregion
    }
}
