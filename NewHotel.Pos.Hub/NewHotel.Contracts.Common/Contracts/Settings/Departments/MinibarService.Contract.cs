﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(MinibarServiceContract), "ValidateMinibarService")]
    public class MinibarServiceContract : BaseContract
    {
        #region Members
        private Guid _serviceDepartmentId;
        private decimal? _defaultValue;
        private string _departmentServiceDescription;
        #endregion
        #region Properties
        [DataMember]
        public Guid ServiceDepartmentId { get { return _serviceDepartmentId; } set { Set(ref _serviceDepartmentId, value, "ServiceDepartmentId"); } }
        [DataMember]
        public decimal? DefaultValue { get { return _defaultValue; } set { Set(ref _defaultValue, value, "DefaultValue"); } }
        [DataMember]
        public string DepartmentServiceDescription { get { return _departmentServiceDescription; } set { Set(ref _departmentServiceDescription, value, "DepartmentServiceDescription"); } }
        #endregion
        #region Constructor
        public MinibarServiceContract()
            : base()
        {
            
        }
        #endregion
        #region Validations
        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateMinibarService(MinibarServiceContract contract)
        {
            if (contract.ServiceDepartmentId == Guid.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Service cannot be empty.");
            if(!contract.DefaultValue.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Value cannot be empty.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
        #endregion
    }
}
