﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ServiceByDepartmentContract : BaseContract
    {
        #region Properties

        [DataMember]
        public Guid DepartmentId { get; set; }
        [DataMember]
        public Guid ServiceId { get; set; }
        [DataMember]
        public bool Inactive { get; set; }
        [DataMember]
        public string Department { get; set; }
        [DataMember]
        public string Service { get; set; }

        #endregion
        #region Constructors

        public ServiceByDepartmentContract()
            : base() { }

        #endregion
    }

    [DataContract]
	[Serializable]
    public class ServiceDepartmentRelationContract : BaseContract
    {
        #region Members

        private Guid _serviceOrDepartmentId;
        private string _abbreviation;
        private string _description;
        private bool _inactive;

        #endregion

        #region Properties

        [DataMember]
        public Guid ServiceOrDepartmentId { get { return _serviceOrDepartmentId; } set { Set(ref _serviceOrDepartmentId, value, "ServiceOrDepartmentId"); } }
        [DataMember]
        public string Abbreviation { get { return _abbreviation; } set { Set(ref _abbreviation, value, "Abbreviation"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }

        #endregion
        #region Constructors

        public ServiceDepartmentRelationContract() : base() { }
        public ServiceDepartmentRelationContract(Guid id, Guid serviceorDepartmentId,
            string abbreviation, string description, bool inactive) 
            : base()
        {
            Id = id;
            ServiceOrDepartmentId = serviceorDepartmentId;
            Abbreviation = abbreviation;
            Description = description;
            Inactive = inactive;
        }

        #endregion
    }
}
