﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(ServiceTaxContract), "ValidateTaxContract")]
    public class ServiceTaxContract : BaseContract
    {
        #region Members

        private Guid? _taxSchemaId;
        private string _taxSchemaName;
        private Guid? _serviceId;
        private Guid? _taxRateId1;
        private string _taxRateDesc1;
        private decimal? _percent1;
        private decimal _retentionPercent1;
        private Guid? _taxRateId2;
        private string _taxRateDesc2;
        private decimal? _percent2;
        private ApplyTax2? _mode2;
        private decimal _retentionPercent2;
        private Guid? _taxRateId3;
        private string _taxRateDesc3;
        private decimal? _percent3;
        private ApplyTax3? _mode3;
        private decimal _retentionPercent3;
        private Guid? _retentionSchemaId;
        private string _taxRateAuxCode3;
        private string _taxRateAuxCode2;
        private string _taxRateAuxCode1;

        [DataMember]
        internal string _country;
        [DataMember]
        internal DocumentSign _signType;
 
        #endregion
        #region Properties

        [DataMember]
        public Guid? TaxSchemaId { get { return _taxSchemaId; } set { Set(ref _taxSchemaId, value, nameof(TaxSchemaId)); } }
        [DataMember]
        public string TaxSchemaName { get { return _taxSchemaName; } set { Set(ref _taxSchemaName, value, nameof(TaxSchemaName)); } }
        [DataMember]
        public Guid? ServiceId { get { return _serviceId; } set { Set(ref _serviceId, value, nameof(ServiceId)); } }
        [DataMember]
        public Guid? TaxRateId1 { get { return _taxRateId1; } set { Set(ref _taxRateId1, value, nameof(TaxRateId1)); } }
        [DataMember]
        public string TaxRateAuxCode1 { get { return _taxRateAuxCode1; } set { Set(ref _taxRateAuxCode1, value, nameof(TaxRateAuxCode1)); } }
        [DataMember]
        public string TaxRateDesc1 { get { return _taxRateDesc1; } set { Set(ref _taxRateDesc1, value, nameof(TaxRateDesc1)); } }
        [DataMember]
        public decimal? Percent1 { get { return _percent1; } set { Set(ref _percent1, value, nameof(Percent1)); } }
        [DataMember]
        public decimal RetentionPercent1 { get { return _retentionPercent1; } set { Set(ref _retentionPercent1, value, nameof(RetentionPercent1)); } }
        [DataMember(Order = 0)]
        public Guid? TaxRateId2
        {
            get { return _taxRateId2; }
            set
            {
                if (Set(ref _taxRateId2, value, "TaxRateId2"))
                {
                    if (_taxRateId2.HasValue)
                    {
                        if (!_mode2.HasValue)
                            Mode2 = ApplyTax2.OverBase;
                    }
                    else
                        Mode2 = null;
                }
            }
        }
        [DataMember]
        public string TaxRateAuxCode2 { get { return _taxRateAuxCode2; } set { Set(ref _taxRateAuxCode2, value, nameof(TaxRateAuxCode2)); } }
        [DataMember]
        public string TaxRateDesc2 { get { return _taxRateDesc2; } set { Set(ref _taxRateDesc2, value, nameof(TaxRateDesc2)); } }
        [DataMember]
        public decimal? Percent2 { get { return _percent2; } set { Set(ref _percent2, value, nameof(Percent2)); } }
        [DataMember(Order = 1)]
        public ApplyTax2? Mode2
        {
            get { return _mode2; }
            set { Set(ref _mode2, _taxRateId2.HasValue ? value : null, nameof(Mode2)); }
        }
        [DataMember]
        public decimal RetentionPercent2 { get { return _retentionPercent2; } set { Set(ref _retentionPercent2, value, nameof(RetentionPercent2)); } }
        [DataMember(Order = 0)]
        public Guid? TaxRateId3
        {
            get { return _taxRateId3; }
            set
            {
                if (Set(ref _taxRateId3, value, "TaxRateId3"))
                {
                    if (_taxRateId3.HasValue)
                    {
                        if (!_mode3.HasValue)
                            Mode3 = ApplyTax3.OverBase;
                    }
                    else
                        Mode3 = null;
                }
            }
        }
        [DataMember]
        public string TaxRateAuxCode3 { get { return _taxRateAuxCode3; } set { Set(ref _taxRateAuxCode3, value, nameof(TaxRateAuxCode3)); } }
        [DataMember]
        public string TaxRateDesc3 { get { return _taxRateDesc3; } set { Set(ref _taxRateDesc3, value, nameof(TaxRateDesc3)); } }
        [DataMember]
        public decimal? Percent3 { get { return _percent3; } set { Set(ref _percent3, value, nameof(Percent3)); } }
        [DataMember(Order = 1)]
        public ApplyTax3? Mode3
        {
            get { return _mode3; }
            set { Set(ref _mode3, _taxRateId3.HasValue ? value : null, nameof(Mode3)); }
        }
        [DataMember]
        public decimal RetentionPercent3 { get { return _retentionPercent3; } set { Set(ref _retentionPercent3, value, nameof(RetentionPercent3)); } }
        [DataMember]
        public Guid? RetentionSchemaId { get { return _retentionSchemaId; } set { Set(ref _retentionSchemaId, value, nameof(RetentionSchemaId)); } }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public bool ApplyModeEnabled
        {
            get { return _country != "BR"; }
        }

        [ReflectionExclude]
        public bool TaxLevel2Enabled
        {
            get
            {
                switch (_signType)
                {
                    case DocumentSign.FiscalizationPortugal:
                    case DocumentSign.FiscalizationAngola:
                        return false;
                }

                return true;
            }
        }

        [ReflectionExclude]
        public bool TaxLevel3Enabled
        {
            get
            {
                switch (_signType)
                {
                    case DocumentSign.FiscalizationPortugal:
                    case DocumentSign.FiscalizationAngola:
                        return false;
                }

                return true;
            }
        }

        [ReflectionExclude]
        public bool MandatoryExemptionReason
        {
            get
            {
                switch (_signType)
                {
                    case DocumentSign.FiscalizationPortugal:
                    case DocumentSign.FiscalizationAngola:
                        return Percent1.HasValue && Percent1.Value == decimal.Zero;
                }

                return false;
            }
        }

        #endregion
        #region Constructor

        public ServiceTaxContract(DocumentSign signType, string country = "")
            : base()
        {
            _signType = signType;
            _country = country;
        }

        public ServiceTaxContract()
            : this(DocumentSign.None, string.Empty)
        {
        }

        public ServiceTaxContract(Guid id, Guid taxSchemaId, string taxSchemaName, Guid? serviceId,
            Guid taxRateId1, Guid? taxRateId2, Guid? taxRateId3, ApplyTax2? mode2, ApplyTax3? mode3,
            string taxRateDesc1, string taxRateDesc2, string taxRateDesc3, Guid? retentionSchemaId,
            DocumentSign signType, string country = "")
            : this(signType, country)
        {
            Id = id;
            TaxSchemaId = taxSchemaId;
            TaxSchemaName = taxSchemaName;
            ServiceId = serviceId;
            TaxRateId1 = taxRateId1;
            TaxRateDesc1 = taxRateDesc1;
            TaxRateId2 = taxRateId2;
            Mode2 = mode2;
            TaxRateDesc2 = taxRateDesc2;
            TaxRateId3 = taxRateId3;
            Mode3 = mode3;
            TaxRateDesc3 = taxRateDesc3;
            RetentionSchemaId = retentionSchemaId;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateTaxContract(ServiceTaxContract obj)
        {
            if (!obj.TaxRateId1.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Tax Rate 1 can't be empty.");
            if (obj.TaxRateId2.HasValue && !obj.Mode2.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Must have application formula.");
            if (obj.TaxRateId3.HasValue && !obj.Mode3.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Must have application formula.");

            if (obj.RetentionPercent1 < decimal.Zero || obj.RetentionPercent1 > 100M)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Retention 1 must be between 0 and 100");
            if (obj.RetentionPercent2 < decimal.Zero || obj.RetentionPercent2 > 100M)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Retention 2 must be between 0 and 100");
            if (obj.RetentionPercent3 < decimal.Zero || obj.RetentionPercent3 > 100M)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Retention 3 must be between 0 and 100");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}