﻿using System;

namespace NewHotel.Contracts
{
    public class DepartmentByServiceContract : BaseContract
    {
        #region Properties

        public Guid DepartmentId { get; set; }
        public string Abbreviation { get; set; }
        public string Description { get; set; }

        #endregion
        #region Constructor

        public DepartmentByServiceContract(Guid id, string abbreviation, string description) 
            : base()
        {
            DepartmentId = id;
            Abbreviation = abbreviation;
            Description = description;
        }

        #endregion
    }
}
