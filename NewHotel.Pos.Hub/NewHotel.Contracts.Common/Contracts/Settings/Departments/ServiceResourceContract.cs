﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class ServiceResourceContract : BaseContract
    {
        #region Properties

        [DataMember]
        public Guid ServiceSpaId { get; set; }

        [DataMember]
        public Guid ResourceId { get; set; }

        [DataMember]
        public short Priority { get; set; }

        [DataMember]
        public short Capacity { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string ServiceAbbreviation { get; set; }

        [DataMember]
        public string ServiceDescription { get; set; }

        #endregion;

        #region Constructors

        public ServiceResourceContract() : base() { }

        public ServiceResourceContract(Guid id, Guid serviceSpaId, Guid resourceId, 
                                          short priority, short capacity, string description, string abbreviation, string servDescription)
            : base()
        {
            Id = id;
            ServiceSpaId = serviceSpaId;
            ResourceId = resourceId;
            Priority = priority;
            Capacity = capacity;
            Description = description;
            ServiceAbbreviation = abbreviation;
            ServiceDescription = servDescription;
        }

        #endregion;
    }
}
