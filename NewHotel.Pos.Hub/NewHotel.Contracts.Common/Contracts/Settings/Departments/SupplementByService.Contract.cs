﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(SupplementByServiceContract), "ValidateSupplementByService")]
    public class SupplementByServiceContract : BaseContract
    {
        #region Members

        private Guid? _supplementSchemaId;
        private Guid? _departmentId;
        private Guid? _serviceByDepartmentId;
        private string _department;
        private string _service;
        private Guid _serviceId;
        private decimal _percent;
        private PercentApplyOver _appliedOver;
        private DailyAccountType? _accountFolder;
        private bool _isDaily;
        private bool _allowExclusion;
        private bool _allowDecreaseValue;
        private bool _forecast;

        #endregion
        #region Properties

        [DataMember]
        public Guid? SupplementSchemaId { get { return _supplementSchemaId; } set { Set(ref _supplementSchemaId, value, "SupplementSchemaId"); } }
        [DataMember]
        public Guid? DepartmentId { get { return _departmentId; } set { Set(ref _departmentId, value, "DepartmentId"); } }
        [DataMember]
        public Guid? ServiceByDepartmentId { get { return _serviceByDepartmentId; } set { Set(ref _serviceByDepartmentId, value, "ServiceByDepartmentId"); } }
        [DataMember]
        public string Department
        {
            get { return _department; }
            set
            {
                if (Set(ref _department, value, "Department"))
                    NotifyPropertyChanged("Description");
            }
        }
        [DataMember]
        public Guid ServiceId { get { return _serviceId; } set { Set(ref _serviceId, value, "ServiceId"); } }
        [DataMember]
        public string Service
        { 
            get { return _service; }
            set
            {
                if (Set(ref _service, value, "Service"))
                    NotifyPropertyChanged("Description");
            }
        }
        [DataMember]
        public decimal Percent { get { return _percent; } set { Set(ref _percent, value, "Percent"); } }
        [DataMember]
        public PercentApplyOver AppliedOver { get { return _appliedOver; } set { Set(ref _appliedOver, value, "AppliedOver"); } }
        [DataMember]
        public DailyAccountType? AccountFolder { get { return _accountFolder; } set { Set(ref _accountFolder, value, "AccountFolder"); } }
        [DataMember]
        public bool IsDaily { get { return _isDaily; } set { Set(ref _isDaily, value, "IsDaily"); } }
        [DataMember]
        public bool AllowExclusion { get { return _allowExclusion; } set { Set(ref _allowExclusion, value, "AllowExclusion"); } }
        [DataMember]
        public bool AllowDecreaseValue { get { return _allowDecreaseValue; } set { Set(ref _allowDecreaseValue, value, "AllowDecreaseValue"); } }
        [DataMember]
        public bool Forecast { get { return _forecast; } set { Set(ref _forecast, value, "Forecast"); } }

        [ReflectionExclude]
        public string Description
        {
            get
            { 
                var description = string.Empty;
                if (!string.IsNullOrEmpty(Department))
                    description += Department;
                if (!string.IsNullOrEmpty(Service))
                    description += "/" + Service;

                return description;
            }
        }

        #endregion
        #region Constructor

        public SupplementByServiceContract()
            : base() { }

        public SupplementByServiceContract(Guid id, Guid departmentId, Guid serviceByDepartmentId, Guid serviceId)
            : base()
        {
            Id = id;
            DepartmentId = departmentId;
            ServiceByDepartmentId = serviceByDepartmentId;
            ServiceId = serviceId;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateSupplementByService(SupplementByServiceContract contract)
        {
            if (!contract.SupplementSchemaId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Supplement Schema cannot be empty.");

            if (!contract.ServiceByDepartmentId.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Service by Department cannot be empty.");

            if (contract.Percent <= decimal.Zero)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Percent must be a positive value.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}