﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(ServiceContract), "ValidateService")]
    public class ServiceContract : ServiceBaseContract
    {
        #region Members

        private Guid? _marketSegment;
        private DailyAccountType _defaultAccountType;
        private bool _byPoints;
        private decimal? _detractionPercent;
        private string _documentFilter;
        private string _fiscalPrinter;
        private bool _externalService;
        private bool _externalInvoice;
        private decimal _irsRetentionPercent;

        private TypedList<ServiceDepartmentRelationContract> _departments;
        private TypedList<ServiceTaxContract> _taxes;
        private TypedList<SupplementByServiceContract> _supplements;
        private TypedList<ApplicationContract> _applications;

        #endregion
        #region Properties

        [DataMember]
        public Guid? MarketSegment { get { return _marketSegment; } set { Set(ref _marketSegment, value, "MarketSegment"); } }
        [DataMember]
        public DailyAccountType DefaultAccountType { get { return _defaultAccountType; } set { Set(ref _defaultAccountType, value, "DefaultAccountType"); } }
        [DataMember]
        public bool ByPoints { get { return _byPoints; } set { Set(ref _byPoints, value, "ByPoints"); } }
        [DataMember]
        public decimal? DetractionPercent { get { return _detractionPercent; } set { Set(ref _detractionPercent, value, "DetractionPercent"); } }
        [DataMember]
        public string DocumentFilter { get { return _documentFilter; } set { Set(ref _documentFilter, value, "DocumentFilter"); } }
        [DataMember]
        public string FiscalPrinter { get { return _fiscalPrinter; } set { Set(ref _fiscalPrinter, value, "FiscalPrinter"); } }
        [DataMember]
        public bool ExternalService { get { return _externalService; } set { Set(ref _externalService, value, "ExternalService"); } }
        [DataMember]
        public bool ExternalInvoice { get { return _externalInvoice; } set { Set(ref _externalInvoice, value, "ExternalInvoice"); } }
        [DataMember]
        public decimal IrsRetentionPercent { get { return _irsRetentionPercent; } set { Set(ref _irsRetentionPercent, value, "IrsRetentionPercent"); } }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public Guid[] DepartmentIds
        {
            get { return Departments.Select(x => x.ServiceOrDepartmentId).ToArray(); }
        }

        [ReflectionExclude]
        public long[] ApplicationIds
        {
            get { return Applications.Select(x => x.ApplicationId).ToArray(); }
        }

        #endregion
        #region Contracts

        //[DataMember]
        [ReflectionExclude]
        public TypedList<ServiceDepartmentRelationContract> Departments { get { return _departments; } set { _departments = value; } }
        //[DataMember]
        [ReflectionExclude]
        public TypedList<ServiceTaxContract> Taxes { get { return _taxes; } set { _taxes = value; } }
        //[DataMember]
        [ReflectionExclude]
        public TypedList<SupplementByServiceContract> Supplements { get { return _supplements; } set { _supplements = value; } }
        //[DataMember]
        [ReflectionExclude]
        public TypedList<ApplicationContract> Applications { get { return _applications; } set { _applications = value; } }

        #endregion
        #region Constructor

        public ServiceContract()
            : base()
        {
            _departments = new TypedList<ServiceDepartmentRelationContract>();
            _taxes = new TypedList<ServiceTaxContract>();
            _supplements = new TypedList<SupplementByServiceContract>();
            _applications = new TypedList<ApplicationContract>();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateService(ServiceContract contract)
        {
            if (!contract.ServiceCategory.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("ServiceCategory cannot be empty.");

            if (!contract.ServiceGrouping.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("ServiceGroup cannot be empty.");

            if (contract.Taxes != null)
            {
                if (contract.Taxes.IsEmpty)
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Service must contain at least one tax specification.");
                else if (contract.Taxes.Any(t => t.MandatoryExemptionReason))
                {
                    if (string.IsNullOrEmpty(contract.CodeTaxZero))
                        return new System.ComponentModel.DataAnnotations.ValidationResult("Tax 0 reason code cannot be empty.");

                    if (string.IsNullOrEmpty(contract.DescriptionTaxZero))
                        return new System.ComponentModel.DataAnnotations.ValidationResult("Tax 0 reason description cannot be empty.");
                }
            }

            if (!contract.AllowedValueGreaterThanZero && !contract.AllowedValueEqualToZero && !contract.AllowedValueLessThanZero)
                return new System.ComponentModel.DataAnnotations.ValidationResult("At least one allowed value option must be selected.");

            if (contract.UseInUpsell && string.IsNullOrEmpty(contract.DescriptionUpSell))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Upsell Description Empty");

            if (contract.IrsRetentionPercent < decimal.Zero || contract.IrsRetentionPercent > 100)
                return new System.ComponentModel.DataAnnotations.ValidationResult("IRS retention must be between 0 and 100.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}