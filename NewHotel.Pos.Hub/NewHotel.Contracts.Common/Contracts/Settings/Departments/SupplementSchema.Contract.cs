﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SupplementSchemaContract : BaseContract
    {
        #region Private Members

        private Guid _installationId;
        private bool _isDefault;

        #endregion
        #region Public Properties

        [DataMember]
        internal LanguageTranslationContract _description;

        [DataMember]
        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        public Guid InstallationId { get { return _installationId; } set { Set(ref _installationId, value, "InstallationId"); } }

        [DataMember]
        public bool IsDefault { get { return _isDefault; } set { Set(ref _isDefault, value, "IsDefault"); } } 

        #endregion
        #region Constructor

        public SupplementSchemaContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        } 

        #endregion
    }
}
