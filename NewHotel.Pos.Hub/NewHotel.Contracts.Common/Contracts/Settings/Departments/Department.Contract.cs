﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(DepartmentContract), "ValidateDepartment")]
    public class DepartmentContract : BaseContract
    {
        #region Members

        private bool _allowManualInsertions;
        private bool _agruppedAutomatic;
        private bool _agruppedDocumentCode;
        private bool _inactive;
        private string _freeCode;
        private string _fiscalPrinter;
        private bool _Stockable;

        #endregion
        #region Properties
        
        [DataMember]
        public bool AllowManualInsertions { get { return _allowManualInsertions; } set { Set(ref _allowManualInsertions, value, "AllowManualInsertions"); } }
        [DataMember]
        public bool AgruppedAutomatic { get { return _agruppedAutomatic; } set { Set(ref _agruppedAutomatic, value, "AgruppedAutomatic"); } }
        [DataMember]
        public bool AgruppedDocumentCode { get { return _agruppedDocumentCode; } set { Set(ref _agruppedDocumentCode, value, "AgruppedDocumentCode"); } }
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }
        [DataMember]
        public string FiscalPrinter { get { return _fiscalPrinter; } set { Set(ref _fiscalPrinter, value, "FiscalPrinter"); } }

        [DataMember]
        public bool Stockable { get { return _Stockable; } set { Set(ref _Stockable, value, "Stockable"); } }
        [DataMember]
        public string FreeCode { get { return _freeCode; } set { Set(ref _freeCode, value, "FreeCode"); } }

        private Guid? _costCenterId;
        [DataMember]
        public Guid? CostCenterId { get { return _costCenterId; } set { Set(ref _costCenterId, value, "CostCenterId"); } }
      
        #endregion
        #region Contracts

        [DataMember]
        internal LanguageTranslationContract _abbreviation;
        [DataMember]
        internal LanguageTranslationContract _description;

        [DataMember]
        [ReflectionExclude]
        public LanguageTranslationContract Abbreviation
        {
            get { return _abbreviation; }
            set { _abbreviation = value; }
        }

        [DataMember]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        internal TypedList<ServiceDepartmentRelationContract> _services;
        [ReflectionExclude]
        public TypedList<ServiceDepartmentRelationContract> Services { get { return _services; } set { _services = value; } }

        internal TypedList<ApplicationContract> _applications;
        [ReflectionExclude]
        public TypedList<ApplicationContract> Applications { get { return _applications; } set { _applications = value; } }

        #endregion
        #region Constructor

        public DepartmentContract()
            : base()
        {
            _abbreviation = new LanguageTranslationContract();
            _description = new LanguageTranslationContract();
            _services = new TypedList<ServiceDepartmentRelationContract>();
            _applications = new TypedList<ApplicationContract>();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateDepartment(DepartmentContract contract)
        {
            if (contract.Abbreviation.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Abbreviation cannot be empty.");

            if (contract.Description.IsEmpty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description cannot be empty.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion

        [ReflectionExclude]
        public Guid[] ServiceIds
        {
            get { return Services.Select(x => x.ServiceOrDepartmentId).ToArray(); }
        }
          
        [ReflectionExclude]
        public long[] ApplicationIds
        {
            get { return Applications.Select(x => x.ApplicationId).ToArray(); }
        }         
   }
}