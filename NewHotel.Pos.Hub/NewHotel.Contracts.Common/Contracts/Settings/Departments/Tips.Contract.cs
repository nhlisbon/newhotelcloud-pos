﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TipsContract : BaseContract
    {
        #region Private Members

        private Guid _installation;
        private decimal? _percent;

        #endregion
        #region Public Properties

        [DataMember]
        internal LanguageTranslationContract _description;

        [DataMember]
        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        [ReflectionExclude]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        public Guid Installation { get { return _installation; } set { Set(ref _installation, value, "Installation"); } }

        [DataMember]
        public decimal? Percent { get { return _percent; } set { Set(ref _percent, value, "Percent"); } } 

        #endregion
        #region Constructor

        public TipsContract()
            : base()
        {
            _description = new LanguageTranslationContract();
        } 

        #endregion
    }
}
