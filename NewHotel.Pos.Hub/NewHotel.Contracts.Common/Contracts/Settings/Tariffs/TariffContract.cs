﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TariffContract : BaseContract
    {
        #region Members
        private Guid _tariffTypeId;
        private PricesCriterion _defaultPriceCriterion;
        private bool _inactive;
        #endregion

        #region Properties

        [DataMember]
        internal LanguageTranslationContract _abbreviation;
        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Abbreviation required.")]
        [ReflectionExclude()]
        public LanguageTranslationContract Abbreviation
        {
            get { return _abbreviation; }
        }

        [DataMember]
        internal LanguageTranslationContract _description;
        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        [ReflectionExclude()]
        public LanguageTranslationContract Description
        {
            get { return _description; }
        }

        [DataMember]
        public Guid TariffTypeId { get { return _tariffTypeId; } set { Set(ref _tariffTypeId, value, "TariffTypeId"); } }

        [DataMember]
        public PricesCriterion DefaultPriceCriterion { get { return _defaultPriceCriterion; } set { Set(ref _defaultPriceCriterion, value, "DefaultPriceCriterion"); } }

        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }

        #endregion

        #region Lists

        private TypedList<TariffPricesContract> _prices;
        [ReflectionExclude]
        public TypedList<TariffPricesContract> Prices
        {
            get { return _prices; }
        }

        private TypedList<TariffPeriodContract> _periods;
        [ReflectionExclude]
        public TypedList<TariffPeriodContract> Periods
        {
            get { return _periods; }
        }

        #endregion


        #region Constructor

        public TariffContract()
            : base() 
        {
            _abbreviation = new LanguageTranslationContract();
            _description = new LanguageTranslationContract();
            _defaultPriceCriterion = PricesCriterion.Reservation;
            _prices = new TypedList<TariffPricesContract>();
            _periods = new TypedList<TariffPeriodContract>();
            _inactive = false;
        }

        #endregion

    }

    [DataContract]
	[Serializable]
    public class TariffPriceResultContract : BaseContract
    {
        #region Properties
        [DataMember]
        public decimal TotalPrice { get; set; }

        [DataMember]
        public decimal? ReservationPrice { get; set; }

        [DataMember]
        public decimal? AdultPrice { get; set; }

        [DataMember]
        public decimal? ChildrenPrice { get; set; }

        [DataMember]
        public decimal? UnitPrice { get; set; }
        #endregion
        #region Constructor

        public TariffPriceResultContract()
            : base() 
        {
        }

        #endregion
    }
}
