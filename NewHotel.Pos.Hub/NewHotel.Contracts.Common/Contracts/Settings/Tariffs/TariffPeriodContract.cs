﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]

    public class TariffPeriodContract : BaseContract
    {
        #region Members
        private Guid _tariffId;
        private DateTime _initialDate;
        private DateTime _finalDate;
        #endregion

        #region Properties

        [DataMember]
        public Guid TariffId { get { return _tariffId; } set { Set(ref _tariffId, value, "TariffId"); } }

        [DataMember]
        public DateTime InitialDate { get { return _initialDate; } set { Set(ref _initialDate, value, "InitialDate"); } }

        [DataMember]
        public DateTime FinalDate { get { return _finalDate; } set { Set(ref _finalDate, value, "FinalDate"); } }

        #endregion

        #region Dummy Properties

        public string FirstDate { get { return InitialDate.ToString("dd-MM-yy"); } }
        public string LastDate { get { return FinalDate.ToString("dd-MM-yy"); } }

        #endregion

        #region Lists

        private TypedList<TariffPricesContract> _prices;
        [ReflectionExclude]
        public TypedList<TariffPricesContract> Prices
        {
            get { return _prices; }
        }

        #endregion

        #region Constructor

        public TariffPeriodContract()
            : base() 
        {
            _prices = new TypedList<TariffPricesContract>();
        }

        public TariffPeriodContract(Guid id, Guid tariffId, DateTime initialDate, DateTime finalDate)
            : base()
        {
            Id = id;
            _tariffId = tariffId;
            _initialDate = initialDate;
            _finalDate = finalDate;
            _prices = new TypedList<TariffPricesContract>();
        }

        #endregion

    }
}
