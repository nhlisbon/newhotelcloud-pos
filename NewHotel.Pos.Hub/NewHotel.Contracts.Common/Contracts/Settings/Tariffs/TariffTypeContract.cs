﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TariffTypeContract : BaseContract
    {
        #region Properties

        [DataMember]
        internal LanguageTranslationContract _description;
        [NewHotel.DataAnnotations.LanguageTranslationValidation(ErrorMessage = "Description required.")]
        [ReflectionExclude()]
        public LanguageTranslationContract Description
        {
            get { return _description; }
        }

        private Guid _installationId;
        [DataMember]
        public Guid InstallationId { get { return _installationId; } set { Set(ref _installationId, value, "InstallationId"); } }

        private long _applicationId;
        [DataMember]
        public long ApplicationId { get { return _applicationId; } set { Set(ref _applicationId, value, "ApplicationId"); } }

        #endregion

        #region Constructor

        public TariffTypeContract()
            : base() 
        {
            _description = new LanguageTranslationContract();
        }

        #endregion

    }
}
