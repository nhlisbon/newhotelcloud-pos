﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(TariffPricesContract), "ValidateTariffPrices")]

    public class TariffPricesContract : BaseContract
    {
        #region Members
        private Guid _tariffId;
        private Guid? _tariffPeriodId;
        private PricesCriterion _priceCriterion;
        private string _priceCriterionDescription;
        private decimal? _reservationPrice;
        private decimal? _adultPrice;
        private decimal? _childPrice;
        private string _initialHour;
        private string _finalHour;
        private string _unmoId;
        #endregion

        #region Properties

        [DataMember]
        public Guid TariffId { get { return _tariffId; } set { Set(ref _tariffId, value, "TariffId"); } }

        [DataMember]
        public Guid? TariffPeriodId { get { return _tariffPeriodId; } set { Set(ref _tariffPeriodId, value, "TariffPeriodId"); } }

        [DataMember]
        public PricesCriterion PriceCriterion { get { return _priceCriterion; } set { Set(ref _priceCriterion, value, "PriceCriterion"); } }

        [DataMember]
        public string PriceCriterionDescription { get { return _priceCriterionDescription; } set { Set(ref _priceCriterionDescription, value, "PriceCriterionDescription"); } }

        [DataMember]
        public decimal? ReservationPrice { get { return _reservationPrice; } set { Set(ref _reservationPrice, value, "ReservationPrice"); } }

        [DataMember]
        public decimal? AdultPrice { get { return _adultPrice; } set { Set(ref _adultPrice, value, "AdultPrice"); } }

        [DataMember]
        public decimal? ChildPrice { get { return _childPrice; } set { Set(ref _childPrice, value, "ChildPrice"); } }

        [DataMember]
        public string InitialHour { get { return _initialHour; } set { Set(ref _initialHour, value, "InitialHour"); } }

        [DataMember]
        public string FinalHour { get { return _finalHour; } set { Set(ref _finalHour, value, "FinalHour"); } }

        [DataMember]
        public string UnmoId { get { return _unmoId; } set { Set(ref _unmoId, value, "UnmoId"); } }

        #endregion

        #region Constructor

        public TariffPricesContract()
            : base() 
        {
            _priceCriterion = PricesCriterion.Reservation;
            _reservationPrice = 0;
        }

        public TariffPricesContract(Guid id, Guid tariffId, Guid? tariffPeriodId, PricesCriterion pricecriterion,
                                    decimal? reservationprice, decimal? adultPrice, decimal? childPrice,
                                    string initialHour, string finalHour, string unmoId, string pricecriterionDescription)
            : base()
        {
            Id = id;
            _tariffId = tariffId;
            _tariffPeriodId = tariffPeriodId;
            _priceCriterion = pricecriterion;
            _reservationPrice = reservationprice;
            _adultPrice = adultPrice;
            _childPrice = childPrice;
            _initialHour = initialHour;
            _finalHour = finalHour;
            _unmoId = unmoId;
            _priceCriterionDescription = pricecriterionDescription;
        }

        #endregion

        #region Validations

        public static bool MultipleOfFive(string sTime)
        {
            if (sTime.Length != 5) return false;
            int hour = Convert.ToInt32(sTime.Substring(0, 2));
            int min = Convert.ToInt32(sTime.Substring(3, 2));
            int tmin = (hour * 60) + min;
            return ((tmin % 5) == 0);
        }

        public static bool VerifyTimes(TariffPricesContract obj)
        {
            DateTime dt = DateTime.Now.Date;
            int hour = Convert.ToInt32(obj.InitialHour.Substring(0, 2));
            int min = Convert.ToInt32(obj.InitialHour.Substring(3, 2));
            TimeSpan t = new TimeSpan(hour, min, 0);
            DateTime dI = dt.Add(t);
            hour = Convert.ToInt32(obj.FinalHour.Substring(0, 2));
            min = Convert.ToInt32(obj.FinalHour.Substring(3, 2));
            t = new TimeSpan(hour, min, 0);
            DateTime dF = dt.Add(t);
            return (dF > dI);           
        }

        public static bool VerifyPriceCriterion(TariffPricesContract obj)
        {
            bool ret = false;
            switch (obj.PriceCriterion)
            {
                case PricesCriterion.Daily:
                    ret = obj.ReservationPrice.HasValue;
                    break;
                case PricesCriterion.Hours:
                    ret = obj.ReservationPrice.HasValue;
                    break;
                case PricesCriterion.Persons:
                    ret = (obj.AdultPrice.HasValue && obj.ChildPrice.HasValue);
                    break;
                case PricesCriterion.PersonsByHours:
                    ret = (obj.AdultPrice.HasValue && obj.ChildPrice.HasValue);
                    break;
                case PricesCriterion.PersonsPerDay:
                    ret = (obj.AdultPrice.HasValue && obj.ChildPrice.HasValue);
                    break;
                case PricesCriterion.PersonsTimes:
                    ret = (obj.AdultPrice.HasValue && obj.ChildPrice.HasValue);
                    break;
                case PricesCriterion.Reservation:
                    ret = obj.ReservationPrice.HasValue;
                    break;
                case PricesCriterion.ReservationTimes:
                    ret = obj.ReservationPrice.HasValue;
                    break;
                case PricesCriterion.Times:
                    ret = obj.ReservationPrice.HasValue;
                    break;
                case PricesCriterion.Units:
                    ret = obj.ReservationPrice.HasValue;
                    break;
            }
            return ret;
        }

        public static bool IsTimeCriterion(PricesCriterion crit)
        {
            return (crit == PricesCriterion.PersonsTimes ||
                    crit == PricesCriterion.ReservationTimes ||
                    crit == PricesCriterion.Times);
        }

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateTariffPrices(TariffPricesContract obj)
        {
            if (obj.InitialHour.Length != 5 && (IsTimeCriterion(obj.PriceCriterion)))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Initial Hour is not valid.");
            if (obj.FinalHour.Length != 5 && (IsTimeCriterion(obj.PriceCriterion)))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Final Hour is not valid.");
            if (!MultipleOfFive(obj.InitialHour) && (IsTimeCriterion(obj.PriceCriterion)))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Initial Hour must be a multiple of 5");
            if (!MultipleOfFive(obj.FinalHour) && (IsTimeCriterion(obj.PriceCriterion)))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Final Hour must be a multiple of 5");
            if (obj.UnmoId == string.Empty)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Currency cannot be empty.");
            if (!VerifyPriceCriterion(obj))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Prices not valid.");
            if (IsTimeCriterion(obj.PriceCriterion))
            {
                if (!VerifyTimes(obj))
                    return new System.ComponentModel.DataAnnotations.ValidationResult("Invalids Times.");
            }
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion


    }
}
