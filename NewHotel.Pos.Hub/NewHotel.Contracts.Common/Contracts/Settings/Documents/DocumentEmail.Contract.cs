﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(DocumentEmailContract), "ValidateDocumentEmail")]
    public class DocumentEmailContract : BaseContract
    {
        #region Members
        private long? _docTypeId;
        private string _emailAddress;
        private long? _language;
        private Guid? _documentId;
        private Guid? _ownerId;
        #endregion
        #region Properties
        [DataMember]
        public long? DocTypeId { get { return _docTypeId; } set { Set(ref _docTypeId, value, "DocTypeId"); } }
        [DataMember]
        public string EmailAddress { get { return _emailAddress; } set { Set(ref _emailAddress, value, "EmailAddress"); } }
        [DataMember]
        public long? Language { get { return _language; } set { Set(ref _language, value, "Language"); } }
        [DataMember]
        public Guid? DocumentId { get { return _documentId; } set { Set(ref _documentId, value, "DocumentId"); } }
        [DataMember]
        public Guid? OwnerId { get { return _ownerId; } set { Set(ref _ownerId, value, "OwnerId"); } }

        #endregion
        #region Constructors
        public DocumentEmailContract()
            : base() { }
        #endregion
        #region Validations
        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateDocumentEmail(DocumentEmailContract obj)
        {
            if (!obj.DocTypeId.HasValue) return new System.ComponentModel.DataAnnotations.ValidationResult("DocType missing.");
            if (String.IsNullOrEmpty(obj.EmailAddress)) return new System.ComponentModel.DataAnnotations.ValidationResult("Email address missing.");
            if (!obj.Language.HasValue) return new System.ComponentModel.DataAnnotations.ValidationResult("Language missing.");
            if (!obj.DocumentId.HasValue) return new System.ComponentModel.DataAnnotations.ValidationResult("Document Id missing.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
        #endregion
    }
}
