﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(DocumentTemplateContract), "ValidateDocumentTemplate")]
    public class DocumentTemplateContract : BaseContract
    {
        #region Members
        private long _languageId;
        private string _description;
        private string _subject;
        private Clob _template;
        private long _docTypeId;
        private bool _isHtml;
        private long? _applicationId; 
        #endregion
        #region Properties
        [DataMember]
        public long LanguageId { get { return _languageId; } set { Set(ref _languageId, value, "LanguageId"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public string Subject { get { return _subject; } set { Set(ref _subject, value, "Subject"); } }
        [DataMember]
        public Clob Template { get { return _template; } set { Set(ref _template, value, "Template"); } }
        [DataMember]
        public long DocTypeId { get { return _docTypeId; } set { Set(ref _docTypeId, value, "DocTypeId"); } }
        [DataMember]
        public bool IsHtml { get { return _isHtml; } set { Set(ref _isHtml, value, "IsHtml"); } }
        [DataMember]
        public long? ApplicationId { get { return _applicationId; } set { Set(ref _applicationId, value, "ApplicationId"); } }
        #endregion
        #region Constructor
        public DocumentTemplateContract() 
            : base() 
        {
            Template = new Clob();
        }
        #endregion
        #region Validations
        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateDocumentTemplate(DocumentTemplateContract obj)
        {
            if (obj.LanguageId == 0) return new System.ComponentModel.DataAnnotations.ValidationResult("Language is required");
            if (obj.DocTypeId == 0) return new System.ComponentModel.DataAnnotations.ValidationResult("Document Type is required");
            //if (obj.Template == null) return new System.ComponentModel.DataAnnotations.ValidationResult("Template Text is required");
            if (String.IsNullOrEmpty(obj.Description)) return new System.ComponentModel.DataAnnotations.ValidationResult("Description is required");
            if (String.IsNullOrEmpty(obj.Subject)) return new System.ComponentModel.DataAnnotations.ValidationResult("Subject is required");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
        #endregion
    }
}
