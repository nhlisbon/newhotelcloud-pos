﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(DocumentSerieContract), "ValidateDocumentSerie")]
    public class DocumentSerieContract : BaseContract
    {
        #region Constants

        private const long Invoices = 9;
        private const long CreditInvoice = 10;
        private const long CashInvoice = 11;
        private const long Tickets = 25;

        #endregion
        #region Members

        private long? _docType;
        private string _prefixText;
        private long? _prefixNumber;
        private SerieOrder _prefixOrder;
        private long _nextNumber;
        private long? _endNumber;
        private DateTime? _endDate;
        private SerieStatus _serieStatus;
        private string _signature;
        private Guid _installationId;
        private bool _fiscalization;

        [DataMember]
        internal DateTime _workDate;
        [DataMember]
        internal DocumentSign _signType;
        private bool _isContingency;

        #endregion
        #region Properties

        [DataMember]
        public long? DocType { get { return _docType; } internal set { _docType = value; } }
        [DataMember]
        public string PrefixText { get { return _prefixText; } set { Set(ref _prefixText, value, "PrefixText"); } }
        [DataMember]
        public long? PrefixNumber { get { return _prefixNumber; } set { Set(ref _prefixNumber, value, "PrefixNumber"); } }
        [DataMember]
        public SerieOrder PrefixOrder { get { return _prefixOrder; } set { Set(ref _prefixOrder, value, "PrefixOrder"); } }
        [DataMember]
        public long NextNumber { get { return _nextNumber; } set { Set(ref _nextNumber, value, "NextNumber"); } }
        [DataMember]
        public long? EndNumber
        { 
            get { return _endNumber; }
            set 
            {
                if (Set(ref _endNumber, value, "EndNumber") && value.HasValue)
                    EndDate = null;
            }
        }
        [DataMember]
        public DateTime? EndDate
        { 
            get { return _endDate; }
            set 
            {
                if (Set(ref _endDate, value, "EndDate") && value.HasValue)
                    EndNumber = null;
            }
        }
        [DataMember]
        public SerieStatus SerieStatus { get { return _serieStatus; } set { Set(ref _serieStatus, value, "SerieStatus"); } }
        [DataMember]
        public Guid InstallationId { get { return _installationId; } set { Set(ref _installationId, value, "InstallationId"); } }
        [DataMember]
        public string Signature { get { return _signature; } set { Set(ref _signature, value, "Signature"); } }
        [DataMember]
        public bool Fiscalization { get { return _fiscalization; } set { Set(ref _fiscalization, value, "Fiscalization"); } }
        [DataMember]
        public bool IsContingency { get { return _isContingency; } set { Set(ref _isContingency, value, nameof(IsContingency)); } }
        [DataMember]
        public long Application { get; set; }
        [DataMember]
        public int PrefixMaxLength { get; set; }
        [DataMember]
        public bool PrefixNumberDisabled { get; set; }
        [DataMember]
        public bool EndNumberHidden { get; set; }
        [DataMember]
        public bool EndDateHidden { get; set; }
        [DataMember]
        public string ValidationCode { get; set; }

        [ReflectionExclude]
        public int ValidationCodeMaxLength
        {
            get { return 255; }
        }

        [ReflectionExclude]
        public DateTime WorkDate
        { 
            get { return _workDate; } 
        }

        [ReflectionExclude]
        public string SerieName
        {
            get
            {
                if (PrefixOrder == SerieOrder.TextPlusNumber)
                    return PrefixText + (PrefixNumber.HasValue ? PrefixNumber.ToString() : string.Empty);
                else
                    return (PrefixNumber.HasValue ? PrefixNumber.ToString() : string.Empty) + PrefixText;
            }
        }

        [ReflectionExclude]
        public long? MinEndNumber
        {
            get
            {
                if (IsReadOnly && !EndDate.HasValue)
                    return NextNumber;
                
                return 0;
            }
        }

        [ReflectionExclude]
        public bool IsReadOnly
        {
            get { return SerieStatus != SerieStatus.Future && Application != 2; }
        }

        [ReflectionExclude]
        public bool ActiveInactive { get { return (SerieStatus == SerieStatus.Active || SerieStatus == SerieStatus.Inactive); } }

        [ReflectionExclude]
        public bool Inactive { get { return (SerieStatus == SerieStatus.Inactive); } }

        #endregion
        #region Constructor

        public DocumentSerieContract(DateTime workDate, DocumentSign signType, long application, long? docType = null) 
            : base() 
        {
            _docType = docType;
            _workDate = workDate;
            _prefixOrder = SerieOrder.TextPlusNumber;
            _serieStatus = SerieStatus.Future;
            _endNumber = 0;
            _signType = signType;
            Application = application;
            PrefixMaxLength = 50;
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateSeriePrefix(DocumentSerieContract obj)
        {
            if (!string.IsNullOrEmpty(obj.PrefixText))
            {
                var pattern = string.Empty;
                switch (obj._signType)
                {
                    case DocumentSign.FiscalizationPortugal:
                        pattern = @"^[a-zA-Z0-9]+$";
                        break;
                    case DocumentSign.FiscalizationEcuador:
                        pattern = @"^[a-zA-Z0-9\-]+$";
                        break;
                    case DocumentSign.FiscalizationPeruDFacture:
                        if (obj.DocType.HasValue)
                        {
                            switch (obj.DocType.Value)
                            {
                                case Invoices:
                                case CashInvoice:
                                case CreditInvoice:
                                    pattern = @"^F[[a-zA-Z0-9]{3}$";
                                    break;
                                case Tickets:
                                    pattern = @"^B[[a-zA-Z0-9]{3}$";
                                    break;
                                default:
                                    pattern = @"^F[[a-zA-Z0-9]{3}|B[[a-zA-Z0-9]{3}$";
                                    break;
                            }
                        }
                        break;
                    default:
                        if (!string.IsNullOrEmpty(obj.PrefixText))
                            pattern = @"^[a-zA-Z0-9]+$";
                        break;
                }

                if (!string.IsNullOrEmpty(pattern) && !Regex.IsMatch(obj.PrefixText, pattern))
                    return new System.ComponentModel.DataAnnotations.ValidationResult(string.Format("Invalid Prefix: {0}", obj.PrefixText));
            }

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateDocumentSerie(DocumentSerieContract obj)
        {
            if (!obj._prefixNumber.HasValue && string.IsNullOrEmpty(obj._prefixText))
                return new System.ComponentModel.DataAnnotations.ValidationResult("One Prefix is Required");

            if (!obj._endNumber.HasValue && !obj._endDate.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("One End Condition is Required");

            if (obj._endNumber.HasValue && obj._endNumber.Value < obj._nextNumber)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid End Number");

            if (obj._endDate.HasValue && obj._endDate.Value < obj._workDate)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid End Date");

            return ValidateSeriePrefix(obj);
        }

        #endregion
    }
}