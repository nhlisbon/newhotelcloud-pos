﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    public class DocumentSerieControlContract : BaseContract
    {
        #region Members

        private long _documentType;
        private string _documentTypeDescription;
        private Guid? _caja;
        private Guid? _stand;
        private Guid? _serieId;
        private string _serieDescription;
        private bool _defaultSerie;

        #endregion
        #region Properties

        [DataMember]
        public long DocumentType { get { return _documentType; } set { Set(ref _documentType, value, "DocumentType"); } }
        [DataMember]
        public string DocumentTypeDescription { get { return _documentTypeDescription; } set { Set(ref _documentTypeDescription, value, "DocumentTypeDescription"); } }
        [DataMember]
        public Guid? Caja { get { return _caja; } set { Set(ref _caja, value, "Caja"); } }
        [DataMember]
        public Guid? Stand { get { return _stand; } set { Set(ref _stand, value, "Stand"); } }
        [DataMember]
        public Guid? SerieId { get { return _serieId; } set { Set(ref _serieId, value, "SerieId"); } }
        [DataMember]
        public string SerieDescription { get { return _serieDescription; } set { Set(ref _serieDescription, value, "SerieDescription"); } }
        [DataMember]
        public bool DefaultSerie { get { return _defaultSerie; } set { Set(ref _defaultSerie, value, "DefaultSerie"); } }

        #endregion
        #region Constructor

        public DocumentSerieControlContract() : base() { } 

        #endregion
    }
}