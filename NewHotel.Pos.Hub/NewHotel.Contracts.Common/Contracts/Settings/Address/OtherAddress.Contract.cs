﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(OtherAddressContract), "ValidateOtherAddress")]
    public class OtherAddressContract : BaseAddressContract
    {
        #region Members

        private string _description;

        #endregion
        #region Properties

        [DataMember]
        [NewHotel.DataAnnotations.Required(ErrorMessage = "Description required.")]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }

        public override string ContactDescription
        {
            get { return Description; }
        }

        #endregion
        #region Constructors

        public OtherAddressContract(Guid id, long contactTypeId,
            long contactTypeDetail, string contactTypeDetailDesc, DateTime lastModified)
            : base(id, contactTypeId, contactTypeDetail, contactTypeDetailDesc,lastModified)
        {
        }

        public OtherAddressContract(Guid id, long contactTypeId,
            long contactTypeDetail, string contactTypeDetailDesc)
            : base(id, contactTypeId, contactTypeDetail, contactTypeDetailDesc)
        {
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateOtherAddress(OtherAddressContract obj)
        {
            if (string.IsNullOrEmpty(obj.Description))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description required.");
            if ((ContactType)obj.ContactTypeId == ContactType.EMail && !Regex.IsMatch(obj.Description, @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid E-mail address.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}