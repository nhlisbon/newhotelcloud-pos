﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [KnownType(typeof(AddressContract))]
    [KnownType(typeof(OtherAddressContract))]
    public abstract class BaseAddressContract : BaseContract
    {
        #region Members
        [DataMember]
        internal long _contactTypeId;
        #endregion

        #region Properties
        public long ContactTypeId { get { return _contactTypeId; } }
        [DataMember]
        public long ContactTypeDetailId { get; set; }
        public abstract string ContactDescription { get; }
        #endregion

        #region Extended Properties

        //[ReflectionExclude]
        [DataMember]
        public string ContactTypeDetailDesc { get; set; }

        #endregion
        #region Constructors

        public BaseAddressContract(Guid id, long contactTypeId,
            long contactTypeDetailId, string contactTypeDetailDesc, DateTime lastModified)
            : base()
        {
            Id = id;
            _contactTypeId = contactTypeId;
            ContactTypeDetailId = contactTypeDetailId;
            ContactTypeDetailDesc = contactTypeDetailDesc;
            LastModified = lastModified;

        }
        public BaseAddressContract(Guid id, long contactTypeId,
          long contactTypeDetailId, string contactTypeDetailDesc)
            : base()
        {
            Id = id;
            _contactTypeId = contactTypeId;
            ContactTypeDetailId = contactTypeDetailId;
            ContactTypeDetailDesc = contactTypeDetailDesc;

        }
        #endregion
    }
}
