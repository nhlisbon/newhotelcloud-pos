﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(AddressContract), "ValidateAddress")]
    public class AddressContract : BaseAddressContract
    {
        #region Members

        private string _country;
        private string _street;
        private string _location;
        private string _postalCode;
        private Guid? _districtId;
        private bool _isDefault;

        #endregion
        #region Properties
        [DataMember]
        public string Street { get { return _street; } set { Set(ref _street, value, "Street"); } }
        [DataMember]
        public string PostalCode { get { return _postalCode; } set { Set(ref _postalCode, value, "PostalCode"); } }
        [DataMember]
        public Guid? DistrictId { get { return _districtId; } set { Set<Guid?>(ref _districtId, value, "DistrictId"); } }
        [DataMember]
        public string Location { get { return _location; } set { Set(ref _location, value, "Location"); } }
        [DataMember]
        public string Country { get { return _country; } set { Set(ref _country, value, "Country"); } }
        [DataMember]
        public bool IsDefault { get { return _isDefault; } set { Set(ref _isDefault, value, "IsDefault"); } }
      

        public override string ContactDescription 
        {
            get { return (Street ?? string.Empty).Trim() + " " + (PostalCode ?? string.Empty).Trim() + " " + (Location ?? string.Empty).Trim(); }
        }

        #endregion
        #region Constructors

        public AddressContract(Guid id, long contactTypeId,
            long contactTypeDetail, string contactTypeDetailDesc, DateTime lastModified)
            : base(id, contactTypeId, contactTypeDetail, contactTypeDetailDesc, lastModified)
        {
        }

        public AddressContract(Guid id, long contactTypeId,
            long contactTypeDetail, string contactTypeDetailDesc)
            : base(id, contactTypeId, contactTypeDetail, contactTypeDetailDesc)
        {
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateAddress(AddressContract obj)
        {
            if (string.IsNullOrEmpty(obj.Street))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Street required.");
            if (string.IsNullOrEmpty(obj.Country))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Country required.");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}
