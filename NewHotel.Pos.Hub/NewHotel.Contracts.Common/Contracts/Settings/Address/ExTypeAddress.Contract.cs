﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class ExTypeAddressContract : TypeAddressContract
    {
        [DataMember]
        public string HomePhone { get; set; }
        [DataMember]
        public string MobilePhone { get; set; }
        [DataMember]
        public string FaxNumber { get; set; }
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public string PersonalHomePage { get; set; }
        [DataMember]
        public string SkypeName { get; set; }
    }
}