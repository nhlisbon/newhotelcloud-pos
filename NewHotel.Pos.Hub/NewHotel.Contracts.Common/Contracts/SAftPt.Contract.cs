﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SaftPtContract : BaseContract
    {
        [DataMember]
        public string InstallationName { get; internal set; }
        [DataMember]
        public DateTime Date { get; internal set; }
        [DataMember]
        public DateTime? MinDate { get; internal set; }
        [DataMember]
        public DateTime? MaxDate { get; internal set; }
        [DataMember]
        public TypedList<EntityRecord> FiscalEntities { get; set; }

        public SaftPtContract(string installationName, DateTime date, DateTime? minDate, DateTime? maxDate)
        {
            InstallationName = installationName;
            Date = date;
            MinDate = minDate;
            MaxDate = maxDate;
            FiscalEntities = new TypedList<EntityRecord>();
        }
    }
}