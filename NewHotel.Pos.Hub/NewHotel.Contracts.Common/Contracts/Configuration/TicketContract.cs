﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(TicketContract), "ValidateTicket")]
    public class TicketContract : BaseContract
    {
        #region Members
        private bool _printTaxDetails;
        private bool _printOpeningTime;
        private bool _printCloseDateTime;

        private bool _showLogo;
        private bool _columnCaptions;
        private bool _supressDiscount;
        private bool _supressRecharge;
        private bool _paymentInReceipts;
        private bool _paymentDetailed;
        private bool _printTips;
        private bool _showCommentsProducts;
        private bool _showCommentsTicket;
        private bool _showCancelledProducts;

        private bool _printSeparators;
        private string _printMessage1;
        private string _printMessage2;
        private bool _printSignature;
        private short _headerLinesEmpty;
        private short? _minLines;
        private Blob? _logo;
        private short? _logoAlign;
        private bool _isLarge;
        private bool _tipSuggestion;
        private bool _printTaxDetailsPerLine;
        private bool _printSubtotal;
        private KindSubTotal _subTotalToPrint;
        private short _copies;

        #endregion
        #region Properties

        [DataMember]
        internal LanguageTranslationContract _description;

        [Display(Name = "Description")]
        [LanguageTranslationValidation]
        public LanguageTranslationContract Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        public bool PrintTaxDetails { get { return _printTaxDetails; } set { Set(ref _printTaxDetails, value, "PrintTaxDetails"); } }
        [DataMember]
        public bool PrintOpeningTime { get { return _printOpeningTime; } set { Set(ref _printOpeningTime, value, "PrintOpeningTime"); } }
        [DataMember]
        public bool PrintCloseDateTime { get { return _printCloseDateTime; } set { Set(ref _printCloseDateTime, value, "PrintCloseDateTime"); } }
        [DataMember]
        public bool PrintSeparators { get { return _printSeparators; } set { Set(ref _printSeparators, value, "PrintSeparators"); } }
        [DataMember]
        public bool ShowLogo { get { return _showLogo; } set { Set(ref _showLogo, value, "ShowLogo"); } }
        [DataMember]
        public bool ColumnCaptions { get { return _columnCaptions; } set { Set(ref _columnCaptions, value, "ColumnCaptions"); } }
        [DataMember]
        public bool SupressDiscount { get { return _supressDiscount; } set { Set(ref _supressDiscount, value, "SupressDiscount"); } }
        [DataMember]
        public bool SupressRecharge { get { return _supressRecharge; } set { Set(ref _supressRecharge, value, "SupressRecharge"); } }
        [DataMember]
        public bool PaymentInReceipts { get { return _paymentInReceipts; } set { Set(ref _paymentInReceipts, value, "PaymentInReceipts"); } }
        [DataMember]
        public bool PaymentDetailed { get { return _paymentDetailed; } set { Set(ref _paymentDetailed, value, "PaymentDetailed"); } }
        [DataMember]
        public bool PrintTips { get { return _printTips; } set { Set(ref _printTips, value, "PrintTips"); } }
        [DataMember]
        public bool ShowCommentsProducts { get { return _showCommentsProducts; } set { Set(ref _showCommentsProducts, value, "ShowCommentsProducts"); } }
        [DataMember]
        public bool ShowCommentsTicket { get { return _showCommentsTicket; } set { Set(ref _showCommentsTicket, value, "ShowCommentsTicket"); } }
        [DataMember]
        public bool ShowCancelledProducts { get { return _showCancelledProducts; } set { Set(ref _showCancelledProducts, value, "ShowCancelledProducts"); } }
        [DataMember]
        public string PrintMessage1 { get { return _printMessage1; } set { Set(ref _printMessage1, value, "PrintMessage1"); } }
        [DataMember]
        public string PrintMessage2 { get { return _printMessage2; } set { Set(ref _printMessage2, value, "PrintMessage2"); } }
        [DataMember]
        public bool PrintSignature { get { return _printSignature; } set { Set(ref _printSignature, value, "PrintSignature"); } }
        [DataMember]
        public short HeaderLinesEmpty { get { return _headerLinesEmpty; } set { Set(ref _headerLinesEmpty, value, "HeaderLinesEmpty"); } }
        [DataMember]
        public short? MinLines { get { return _minLines; } set { Set(ref _minLines, value, "MinLines"); } }
        [DataMember]
        public Blob? Logo { get { return _logo; } set { Set(ref _logo, value, "Logo"); } }
        [DataMember]
        public short? LogoAlign { get { return _logoAlign; } set { Set(ref _logoAlign, value, "LogoAlign"); } }
        [DataMember]
        public bool IsLarge { get { return _isLarge; } set { Set(ref _isLarge, value, "IsLarge"); } }
        [DataMember]
        public bool TipSuggestion { get { return _tipSuggestion; } set { Set(ref _tipSuggestion, value, "TipSuggestion"); } }
        [DataMember]
        public bool PrintTaxDetailsPerLine { get { return _printTaxDetailsPerLine; } set { Set(ref _printTaxDetailsPerLine, value, "PrintTaxDetailsPerLine"); } }
        [DataMember]
        public bool PrintSubtotal { get { return _printSubtotal; } set { Set(ref _printSubtotal, value, "PrintSubtotal"); } }
        [DataMember]
        public KindSubTotal SubTotalToPrint { get { return _subTotalToPrint; } set { Set(ref _subTotalToPrint, value, "SubTotalToPrint"); } }
        [DataMember]
        public short Copies { get { return _copies; } set { Set(ref _copies, value, "Copies"); } }

        [ReflectionExclude]
        [DataMember]
        public TypedList<TipSuggestionContract> Suggestions { get; set; }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateTicket(TicketContract obj)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
        #region Constructor

        public TicketContract()
            : base()
        {
            SubTotalToPrint = KindSubTotal.Gross;
            Description = new LanguageTranslationContract();
            Suggestions = new TypedList<TipSuggestionContract>();
        }

        #endregion
    }
}