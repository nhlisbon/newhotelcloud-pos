﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TipSuggestionContract : BaseContract
    {
        #region Members
        private Guid _ticketConfiguration;
        private string _description;
        private decimal _percent;
        private bool _autogenerate;
        private bool _overNet;
        #endregion
        #region Properties

        [DataMember]
        public Guid TicketConfiguration { get { return _ticketConfiguration; } set { Set(ref _ticketConfiguration, value, "TicketConfiguration"); } }
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }
        [DataMember]
        public decimal Percent { get { return _percent; } set { Set(ref _percent, value, "Percent"); } }
        [DataMember]
        public bool Autogenerate { get { return _autogenerate; } set { Set(ref _autogenerate, value, "Autogenerate"); } }
        [DataMember]
        public bool OverNet { get { return _overNet; } set { Set(ref _overNet, value, "OverNet"); } }

        #endregion
        #region Visual Properties
        [DataMember]
        public decimal Proposal { get; set; }
        #endregion
        #region Constructor
        public TipSuggestionContract()
            : base()
        {
            
        }
        #endregion
    }
}
