﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TEFPaymentContract : BaseContract
    {
        #region Private Members
    
        private string _des;
        private Guid _user;
        private decimal _value;
        private TEFPaymentState _estado;
        private DateTime _fechaT;
        private DateTime _fecha;
        private string _token;
        private DateTime? _timeAttemtpTreated;
        private bool _AttemtpTreated;
        private DateTime? _dateAttemtpTreated;
        private long _attempts;
        private string _tefresponse;
        #endregion
        #region Constructor
        public TEFPaymentContract()
            : base() 
        {
            
        }
        #endregion
        #region Public Properties
        [DataMember]
        public string TerminalDescription { get { return _des; } set { Set(ref _des, value, "TerminalDescription"); } }
        [DataMember]
        public decimal Value { get { return _value; } set { Set(ref _value, value, "Value"); } }
        [DataMember]
        public TEFPaymentState Estado { get { return _estado; } set { Set(ref _estado, value, "Estado"); } }
        [DataMember]
        public DateTime WorkDate { get { return _fechaT; } set { Set(ref _fechaT, value, "WorkDate"); } }
        [DataMember]
        public DateTime Date { get { return _fecha; } set { Set(ref _fecha, value, "Date"); } }
        [DataMember]
        public Guid User { get { return _user; } set { Set(ref _user, value, "User"); } }
        [DataMember]
        public string Token { get { return _token; } set { Set(ref _token, value, "Token"); } }
        [DataMember]
        public long Attempts { get { return _attempts; } set {Set(ref _attempts, value, "Attempts"); } }
        [DataMember]
        public bool AttemtpTreated { get { return _AttemtpTreated; } set { Set(ref _AttemtpTreated, value, "AttemtpTreated"); } }
        [DataMember]
        public DateTime? DateAttemtpTreated { get { return _dateAttemtpTreated; } set { Set(ref _dateAttemtpTreated, value, "DateAttemtpTreated"); } }
        [DataMember]
        public DateTime? TimeAttemtpTreated { get { return _timeAttemtpTreated; } set { Set(ref _timeAttemtpTreated, value, "TimeAttemtpTreated"); } }
        [DataMember]
        public string TEFResponse { get { return _tefresponse; } set { Set(ref _tefresponse, value, "TEFResponse"); } }
        #endregion
       
    }
}
