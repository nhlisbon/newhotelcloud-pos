﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TEFTerminalContract : BaseContract
    {
        #region Private Members
    
        private string _des;
        #endregion
        #region Constructor
        public TEFTerminalContract()
        {
            
        }
        #endregion
        #region Public Properties
        [DataMember]
        public string Description { get { return _des; } set { Set(ref _des, value, "Description"); } }
      
        #endregion
       
    }
}
