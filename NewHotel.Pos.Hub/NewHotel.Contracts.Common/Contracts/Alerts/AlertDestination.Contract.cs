﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class AlertDestinationContract : BaseContract
    {
        #region Members

        [DataMember]
        private Guid _recipientId;
        [DataMember]
        private string _recipientDescription;
        [DataMember]
        private string _recipientTypeDescription;
        private DateTime? _readed;
        private DateTime? _replied;
        private DateTime? _notified;
                
        #endregion
        #region Properties

        public Guid RecipientId { get { return _recipientId; } set { _recipientId = value; } }
        public string RecipientDescription { get { return _recipientDescription; } set { _recipientDescription = value; } }
        public string RecipientTypeDescription { get { return _recipientTypeDescription; } set { _recipientTypeDescription = value; } }

        [DataMember]
        public DateTime? Readed { get { return _readed; } set { Set(ref _readed, value, "Readed"); } }
        [DataMember]
        public DateTime? Replied { get { return _replied; } set { Set(ref _replied, value, "Replied"); } }
        [DataMember]
        public DateTime? Notified { get { return _notified; } set { Set(ref _notified, value, "Notified"); } }

        #endregion
        #region Constructor

        public AlertDestinationContract(Guid recipientId, string recipientDescription, string recipientTypeDescription)
        {
            _recipientId = recipientId;
            _recipientDescription = recipientDescription;
            _recipientTypeDescription = recipientTypeDescription;
        }

        public AlertDestinationContract()
        {
        }

        #endregion
    }
}
