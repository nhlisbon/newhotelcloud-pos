﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class AlertContract : BaseContract
    {
        #region Members

        private DateTime _date;
        private Guid? _from;
        private string _subject;
        private PriorityType _priority;
        private string _body;
        private MessageStatus _status;
        private DateTime? _created;

        [DataMember]
        internal IDictionary<AlertRecipientType, string> _alertTypeDescriptions;
                
        #endregion
        #region Properties

        [DataMember]
        public DateTime Date { get { return _date; } set { Set(ref _date, value, "Date"); } }
        [DataMember]
        public Guid? From { get { return _from; } set { Set(ref _from, value, "From"); } }
        [DataMember]
        public string Subject { get { return _subject; } set { Set(ref _subject, value, "Subject"); } }
        [DataMember]
        public PriorityType Priority { get { return _priority; } set { Set(ref _priority, value, "Priority"); } }
        [DataMember]
        public string Body { get { return _body; } set { Set(ref _body, value, "Body"); } }
        [DataMember]
        public MessageStatus Status { get { return _status; } set { Set(ref _status, value, "Status"); } }
        [DataMember]
        public DateTime? Created { get { return _created; } set { Set(ref _created, value, "Created"); } }

        #endregion
        #region Constructor

        public AlertContract(DateTime date, IDictionary<AlertRecipientType, string> alertTypeDescriptions)
        {
            Date = date;
            Priority = PriorityType.Low;
            Status = MessageStatus.Unread;
            Destinations = new TypedList<AlertDestinationContract>();
            _alertTypeDescriptions = alertTypeDescriptions;
        }

        #endregion
        #region Lists

        [DataMember]
        public TypedList<AlertDestinationContract> Destinations { get; internal set; }

        #endregion
        #region Methods

        public void AddDestination(Guid recipientId, AlertRecipientType recipientType, string recipientDescription)
        {
            string recipientTypeDescription;
            if (_alertTypeDescriptions.TryGetValue(recipientType, out recipientTypeDescription))
                Destinations.Add(new AlertDestinationContract(recipientId, recipientDescription, recipientTypeDescription));
        }

        public void RemoveDestination(HashSet<Guid> ids)
        {
            Destinations.Remove(x => ids.Contains((Guid)x.Id));
        }

        #endregion
    }
}
