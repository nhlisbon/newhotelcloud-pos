﻿using System;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    public class AlertGroupContract : BaseContract
    {
        #region Members

        [DataMember]
        internal AlertGroupType _type;
        private bool _inactive;
                
        #endregion
        #region Properties

        [DataMember]
        public AlertGroupType Type { get { return _type; } set { Set(ref _type, value, "Type"); } }

        #endregion
        #region Contracts

        [ReflectionExclude]
        [DataMember]
        public LanguageTranslationContract Description { internal set; get; }

        [ReflectionExclude]
        [DataMember]
        public TypedList<Guid> Items { internal set; get; }

        #endregion
        #region Constructor

        public AlertGroupContract(AlertGroupType type)
        {
            _type = type;
            Description = new LanguageTranslationContract();
            Items = new TypedList<Guid>();
        }

        #endregion
    }
}
