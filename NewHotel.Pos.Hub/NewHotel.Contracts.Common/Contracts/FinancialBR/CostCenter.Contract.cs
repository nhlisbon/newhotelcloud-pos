﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(CostCenterContract), "ValidateCostCenter")]
    public class CostCenterContract : BaseContract
    {
        #region Public Properties

        public string _description;
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }

        public bool _inactive = false;
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }

        #endregion
        #region List

        [DataMember]
        [ReflectionExclude]
        public TypedList<NameContract> Departments { get; set; }

        [DataMember]
        [ReflectionExclude]
        public TypedList<Guid> DepartmentIds { get; set; }

        #endregion
        #region Constructor

        public CostCenterContract() : base()
        {
            Departments = new TypedList<NameContract>();
            DepartmentIds = new TypedList<Guid>();
        }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateCostCenter(CostCenterContract obj)
        {
            if (string.IsNullOrEmpty(obj.Description))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description can not be empry");
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}