﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using NewHotel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    [CustomValidation(typeof(BilletsContract), "ValidateBillets")]
    public class BilletsContract : BaseContract
    {
        #region Public Properties

        private FinancialInstitution _financialInstitutionAlgorithm;
        [DataMember]
        public FinancialInstitution FinancialInstitutionAlgorithm { get { return _financialInstitutionAlgorithm; } set { Set(ref _financialInstitutionAlgorithm, value, nameof(FinancialInstitutionAlgorithm)); } }

        public Guid? _baacPk;
        [DataMember]
        public Guid? BaacPk { get { return _baacPk; } set { Set(ref _baacPk, value, "BaacPk"); } }

        [DataMember]
        public bool BankReadOnly { get; set; }

        public string _walletCode;
        [DataMember]
        public string WalletCode { get { return _walletCode; } set { Set(ref _walletCode, value, "WalletCode"); } }

        public WalletType? _walletType;
        [DataMember]
        public WalletType? WalletType { get { return _walletType; } set { Set(ref _walletType, value, "WalletType"); } }

        public FileType? _fileType;
        [DataMember]
        public FileType? FileType { get { return _fileType; } set { Set(ref _fileType, value, "FileType"); } }

        public int? _fineDays;
        [DataMember]
        public int? FineDays { get { return _fineDays; } set { Set(ref _fineDays, value, "FineDays"); } }

        public decimal? _finePercent;
        [DataMember]
        public decimal? FinePercent { get { return _finePercent; } set { Set(ref _finePercent, value, "FinePercent"); } }

        public int? _interestDays;
        [DataMember]
        public int? InterestDays { get { return _interestDays; } set { Set(ref _interestDays, value, "InterestDays"); } }

        public decimal? _interestPercent;
        [DataMember]
        public decimal? InterestPercent { get { return _interestPercent; } set { Set(ref _interestPercent, value, "InterestPercent"); } }

        public RegisterType? _registerType;
        [DataMember]
        public RegisterType? RegisterType { get { return _registerType; } set { Set(ref _registerType, value, "RegisterType"); } }

        public PrintType? _printType;
        [DataMember]
        public PrintType? PrintType { get { return _printType; } set { Set(ref _printType, value, "PrintType"); } }

        public DocumentType? _documentType;
        [DataMember]
        public DocumentType? DocumentType { get { return _documentType; } set { Set(ref _documentType, value, "DocumentType"); } }

        public string _cedenteCode;
        [DataMember]
        public string CedenteCode { get { return _cedenteCode; } set { Set(ref _cedenteCode, value, "CedenteCode"); } }

        public string _fileNumber;
        [DataMember]
        public string FileNumber { get { return _fileNumber; } set { Set(ref _fileNumber, value, "FileNumber"); } }

        private string _operationNumber;
        [DataMember]
        public string OperationNumber { get { return _operationNumber; } set { Set(ref _operationNumber, value, "OperationNumber"); } }

        private string _transmitionCode;
        [DataMember]
        public string TransmitionCode { get { return _transmitionCode; } set { Set(ref _transmitionCode, value, "TransmitionCode"); } }

        private long? _minNumber;
        [DataMember]
        public long? MinNumber { get { return _minNumber; } set { Set(ref _minNumber, value, nameof(MinNumber)); } }

        private long? _maxNumber;
        [DataMember]
        public long? MaxNumber { get { return _maxNumber; } set { Set(ref _maxNumber, value, nameof(MaxNumber)); } }

        private long? _lastNumberUsed;
        [DataMember]
        public long? LastNumberUsed { get { return _lastNumberUsed; } set { Set(ref _lastNumberUsed, value, "LastNumberUsed"); } }

        private string _emailSubject;
        [DataMember]
        public string EmailSubject { get { return _emailSubject; } set { Set(ref _emailSubject, value, "EmailSubject"); } }

        private Clob? _emailBody;
        [DataMember]
        public Clob? EmailBody { get { return _emailBody; } set { Set(ref _emailBody, value, "EmailBody"); } }

        #endregion
        #region Constructor

        public BilletsContract() : base() { }

        #endregion
        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateBillets(BilletsContract obj)
        {
            if (Guid.Empty == obj.BaacPk)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Bank can not be empry");
            if (!obj.WalletType.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Wallet Type can not be empry");
            if (!obj.FileType.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("File Type can not be empry");
            if (string.IsNullOrEmpty(obj.CedenteCode))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Cedente can not be empry");
            if (!obj.DocumentType.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Document Type can not be empry");
            if (string.IsNullOrEmpty(obj.FileNumber))
                return new System.ComponentModel.DataAnnotations.ValidationResult("File Number can not be empry");
            if (!obj.PrintType.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Print Type can not be empry");
            if (!obj.RegisterType.HasValue)
                return new System.ComponentModel.DataAnnotations.ValidationResult("Register Type can not be empry");
            if (string.IsNullOrEmpty(obj.WalletCode))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Wallet Code can not be empry");

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }
}