﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
    [Serializable]
    public class FinancialCategoryCostCenterContract : BaseContract
    {
        public FinancialCategoryCostCenterContract() : base() { }

        private Guid _financialCategoryId;
        [DataMember]
        public Guid FinancialCategoryId { get { return _financialCategoryId; } set { Set(ref _financialCategoryId, value, "FinancialCategoryId"); } }

        private string _financialCategoryName;
        [DataMember]
        public string FinancialCategoryName { get; set; }

        private Guid _costCenterId;
        [DataMember]
        public Guid CostCenterId { get { return _costCenterId; } set { Set(ref _costCenterId, value, "CostCenterId"); } }

        [DataMember]
        public string CostCenterName { get; set; }

        private decimal? _percentRatio;
        [DataMember]
        public decimal? PercentRatio { get { return _percentRatio; } set { Set(ref _percentRatio, value, "PercentRatio"); } }
    }
}