﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(FinancialCategoryContract), "ValidateFinancialCategory")]
    public class FinancialCategoryContract : BaseContract
    {
        #region Properties

        private short _level = 1;
        [DataMember]
        public short Level { get { return _level; } set { Set(ref _level, value, "Level"); } }

        private string _description;
        [DataMember]
        public string Description { get { return _description; } set { Set(ref _description, value, "Description"); } }

        private bool _analytic = true;
        private bool _syntetic = false;
        [DataMember]
        public bool Analytic { get { return _analytic; } set { Set(ref _analytic, value, "Analytic"); } }
        [DataMember]
        public bool Syntetic { get { return _syntetic; } set { Set(ref _syntetic, value, "Syntetic"); } }

        private bool _credit = true;
        private bool _debit = false;
        [DataMember]
        public bool Credit { get { return _credit; } set { Set(ref _credit, value, "Credit"); } }
        [DataMember]
        public bool Debit { get { return _debit; } set { Set(ref _debit, value, "Debit"); } }

        private string _creditMask;
        private string _debitMask;
        [DataMember]
        public string CreditMask { get { return _creditMask; } set { Set(ref _creditMask, value, "CreditMask"); } }
        [DataMember]
        public string DebitMask { get { return _debitMask; } set { Set(ref _debitMask, value, "DebitMask"); } }

        private bool _showInDREx = false;
        [DataMember]
        public bool ShowInDREx { get { return _showInDREx; } set { Set(ref _showInDREx, value, "ShowInDREx"); } }

        private string _sequence = "1";
        [DataMember]
        public string Sequence { get { return _sequence; } set { Set(ref _sequence, value, "Sequence"); } }

        private Guid? _parent;
        [DataMember]
        public Guid? Parent { get { return _parent; } set { Set(ref _parent, value, "Parent"); } }

        private bool _inactive = false;
        [DataMember]
        public bool Inactive { get { return _inactive; } set { Set(ref _inactive, value, "Inactive"); } }

        #endregion

        internal TypedList<FinancialCategoryCostCenterContract> _defaultCostCenters;
        [DataMember]
        [ReflectionExclude]
        public TypedList<FinancialCategoryCostCenterContract> DefaultCostCenters { get { return _defaultCostCenters; } set { _defaultCostCenters = value; } }

        #region List

        [DataMember]
        [ReflectionExclude]
        public TypedList<NameContract> Services { get; set; }

        [DataMember]
        [ReflectionExclude]
        public TypedList<Guid> ServiceIds { get; set; }

        #endregion

        public FinancialCategoryContract() : base()
        {
            _defaultCostCenters = new TypedList<FinancialCategoryCostCenterContract>();
            Services = new TypedList<NameContract>();
            ServiceIds = new TypedList<Guid>();
        }

        #region Validations

        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateFinancialCategory(FinancialCategoryContract obj)
        {
            if (string.IsNullOrEmpty(obj.Description))
                return new System.ComponentModel.DataAnnotations.ValidationResult("Description cannot ne empty");
            if (obj.DefaultCostCenters.Count > 0)
            {
                var total = (from item in obj.DefaultCostCenters select (item.PercentRatio ?? 0)).Sum(); 
                if (total != 100)
                    return new System.ComponentModel.DataAnnotations.ValidationResult($"Total of percent ratio shoud be 100, currently is: {total}");
            }

            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }

        #endregion
    }     
}