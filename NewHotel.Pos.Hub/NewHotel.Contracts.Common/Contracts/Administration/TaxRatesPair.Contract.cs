﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class TaxRatesPairContract
    {
        [DataMember]
        public Guid taxRateOrig { get; set; }
        [DataMember] 
        public Guid taxRateNew { get; set; }
    }
}
