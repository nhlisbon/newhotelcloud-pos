﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SettingGeneralContract : BaseContract<long>
    {
        #region Properties

        [DataMember]
        public bool MultiInstallation { get; set; }
        [DataMember]
        public string DatabaseVersion { get; set; }

        #endregion
        #region Constructors

        public SettingGeneralContract()
            : base() { }

        #endregion
    }
}
