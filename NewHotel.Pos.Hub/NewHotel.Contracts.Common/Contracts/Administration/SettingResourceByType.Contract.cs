﻿using System;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SettingResourceByTypeContract : BaseContract
    {
        #region Members

        private ResourceOccupationControlType _overbookingControlMode;
        private decimal? _criticalOccupationPercent;
        private short? _maxReservation;
        private bool _insertionLock;

        #endregion
        #region Properties

        [ReflectionExclude]
        [DataMember]
        public string Abbreviation { get; internal set; }
        [ReflectionExclude]
        [DataMember]
        public string Description { get; internal set; }
        [DataMember]
        public ResourceOccupationControlType OverbookingControlMode
        {
            get { return _overbookingControlMode; }
            set
            {
                if (Set(ref _overbookingControlMode, value, "OverbookingControlMode"))
                {
                    if (_overbookingControlMode == ResourceOccupationControlType.CriticalOccupation)
                    {
                        if (!CriticalOccupationPercent.HasValue)
                            CriticalOccupationPercent = 100;
                    }
                    else
                    {
                        if (!MaxReservation.HasValue)
                            MaxReservation = 0;
                    }
                }
            }
        }
        [DataMember]
        public string ResourceOccupationControlTypeDesc { get; set; }
        [DataMember]
        public decimal? CriticalOccupationPercent
        {
            get { return _criticalOccupationPercent; }
            set
            {
                if (Set(ref _criticalOccupationPercent, value, "CriticalOccupationPercent"))
                {
                    if (CriticalOccupationPercent.HasValue)
                        MaxReservation = null;
                    NotifyPropertyChanged("OccupationPercent");
                }
            }
        }
        [DataMember]
        public short? MaxReservation
        {
            get { return _maxReservation; }
            set
            {
                if (Set(ref _maxReservation, value, "MaxReservation"))
                {
                    if (MaxReservation.HasValue)
                        CriticalOccupationPercent = null;
                }
            }
        }
        [DataMember]
        public bool InsertionLock
        {
            get { return _insertionLock; }
            set { Set(ref _insertionLock, value, "InsertionLock"); }
        }

        public string OccupationPercent
        {
            get
            {
                if (CriticalOccupationPercent.HasValue)
                    return CriticalOccupationPercent.Value.ToString() + " %";

                return string.Empty;
            }
        }

        #endregion
        #region Constructors

        public SettingResourceByTypeContract(string abbreviation, string description, string controlTypeDesc)
            : base() 
        {
            Abbreviation = abbreviation;
            Description = description;
            ResourceOccupationControlTypeDesc = controlTypeDesc;
        }

        public SettingResourceByTypeContract()
            : base() { }

        #endregion
    }
}