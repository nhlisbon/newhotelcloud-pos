﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    public class SettingInvoiceContract : BaseContract
    {
        #region Private Members

        private Guid? _tipDepartmentId;
        private Guid? _tipServiceByDepartmentId;
        private short? _daysToCancelInvoice;
        private bool _restrictedInvoiceGrouping;
        private bool _invoiceReversalDetails;
        private bool _invoicePaymentCancel;
        private bool _movementsOnCheckinEvents;
        private bool _cashierTaxMode;
        private string _activityCode;
        private bool _usePurchaseOrders;
        private bool _multiHotelMode;
        private bool _signTestMode;
        private bool _signPointOfSale;
        private long? _signVersion;
        private DocumentSign? _signType;
        private TypedList<KeyDescRecord> _signTypeVersions;
        private bool _multipleSerieAllowed;
        private bool _multiSchemaInvoiceAllowed;
        private bool _advanceDepositInvoiceAllowed;
        private bool _internalInvoiceAllowed;
        private bool _fiscalValidationEnabled;
        private bool _validateInvoiceInstructions;
        private bool _allowCancelPOSTransaction;
        private bool _disablePaymentTransfer;
        private bool _disablePaymentSplit;
        private string _freeText1;
        private string _freeText2;
        private string _freeText3;
        private string _freeText4;

        #endregion
        #region Properties

        [DataMember]
        public string BaseCurrencyId { get; set; }
        [DataMember]
        public Guid? InvoiceDepartmentId { get; set; }
        [DataMember]
        public bool ModifyAccountLimitsAfterInvoice { get; set; }
        [DataMember]
        public SerieRules DocumentRule { get; set; }
        [DataMember]
        public short? DaysToExpireDocument { get; set; }
        [DataMember]
        public bool CloseCurrentAccount { get; set; }
        [DataMember]
        public bool ConfirmDocumentType { get; set; }
        [DataMember]
        public bool ExcludeInvoiceInstructionContract { get; set; }
        [DataMember]
        public bool GroupByWorkDate { get; set; }
        [DataMember]
        public bool GroupByValueDate { get; set; }
        [DataMember]
        public bool GroupByDepartment { get; set; }
        [DataMember]
        public bool GroupByService { get; set; }
        [DataMember]
        public bool GroupByDaily { get; set; }
        [DataMember]
        public bool GroupByRoomRate { get; set; }
        [DataMember]
        public bool GroupByPhone { get; set; }
        [DataMember]
        public bool GroupByDocNumber { get; set; }
        [DataMember]
        public bool GroupByPaymentType { get; set; }
        [DataMember]
        public bool GroupByReservation { get; set; }
        [DataMember]
        public bool GroupByTax { get; set; }
        [DataMember]
        public bool GroupByCityTax { get; set; }
        [DataMember]
        public bool GroupByPackage { get; set; }
        [DataMember]
        public bool HideInvoiceHeader { get; set; }
        [DataMember]
        public string MandatoryFiscalNumberInInvoices { get; set; }
        [DataMember]
        public bool MandatoryFiscalNumberInReceipts { get; set; }
        [DataMember]
        public bool AllowPendingInvoices { get; set; }
        [DataMember]
        public bool CollectPendingInvoiceBeforeNightAuditor { get; set; }
        [DataMember]
        public bool CancelInvoicesOnlyEmissionDate { get; set; }
        [DataMember]
        public bool CancelInvoicesOnlyEmissionMonth { get; set; }
        [DataMember]
        public bool InsertAdvancedInvoiceDifferences { get; set; }
        [DataMember]
        public bool InvoiceDifferencesInCheckOut { get; set; }
        [DataMember]
        public short InvoiceBaseNoCopies { get; set; }
        [DataMember]
        public short InvoiceForeignNoCopies { get; set; }
        [DataMember]
        public short CreditNoteBaseNoCopies { get; set; }
        [DataMember]
        public short CreditNoteForeignNoCopies { get; set; }
        [DataMember]
        public short DebitNoteBaseNoCopies { get; set; }
        [DataMember]
        public short DebitNoteForeignNoCopies { get; set; }
        [DataMember]
        public short ReceiptBaseNoCopies { get; set; }
        [DataMember]
        public short ReceiptForeignNoCopies { get; set; }
        [DataMember]
        public Guid? CancellationReasonIdEntrie { get; set; }
        [DataMember]
        public short IntervalOneCredits { get; set; }
        [DataMember]
        public short IntervalTwoCredits { get; set; }
        [DataMember]
        public short IntervalThreeCredits { get; set; }
        [DataMember]
        public bool ShowGuestName { get; set; }
        [DataMember]
        public bool ShowPaxs { get; set; }
        [DataMember]
        public bool EnableInvoiceAdvancedDeposit { get; set; }
        [DataMember]
        public Guid? TipDepartmentId { get { return _tipDepartmentId; } set { Set(ref _tipDepartmentId, value, "TipDepartmentId"); } }
        [DataMember]
        public Guid? TipServiceByDepartmentId { get { return _tipServiceByDepartmentId; } set { Set(ref _tipServiceByDepartmentId, value, "TipServiceByDepartmentId"); } }
        [DataMember]
        public bool MovementsOnCheckinEvents { get { return _movementsOnCheckinEvents; } set { Set(ref _movementsOnCheckinEvents, value, "MovementsOnCheckinEvents"); } }
        [DataMember]
        public short? DaysToCancelInvoice { get { return _daysToCancelInvoice; } set { Set(ref _daysToCancelInvoice, value, "DaysToCancelInvoice"); } }
        [DataMember]
        public bool CashierTaxMode { get { return _cashierTaxMode; } set { Set(ref _cashierTaxMode, value, "CashierTaxMode"); } }
        [DataMember]
        public bool RestrictedInvoiceGrouping { get { return _restrictedInvoiceGrouping; } set { Set(ref _restrictedInvoiceGrouping, value, "RestrictedInvoiceGrouping"); } }
        [DataMember]
        public bool InvoiceReversalDetails { get { return _invoiceReversalDetails; } set { Set(ref _invoiceReversalDetails, value, "InvoiceReversalDetails"); } }
        [DataMember]
        public bool InvoicePaymentCancel { get { return _invoicePaymentCancel; } set { Set(ref _invoicePaymentCancel, value, "InvoicePaymentCancel"); } }
        [DataMember]
        public string ActivityCode { get { return _activityCode; } set { Set(ref _activityCode, value, "ActivityCode"); } }
        [DataMember]
        public bool UsePurchaseOrders { get { return _usePurchaseOrders; } set { Set(ref _usePurchaseOrders, value, "UsePurchaseOrders"); } }
        [DataMember]
        public string FreeText1 { get { return _freeText1; } set { Set(ref _freeText1, value, "FreeText1"); } }
        [DataMember]
        public string FreeText2 { get { return _freeText2; } set { Set(ref _freeText2, value, "FreeText2"); } }
        [DataMember]
        public string FreeText3 { get { return _freeText3; } set { Set(ref _freeText3, value, "FreeText3"); } }
        [DataMember]
        public string FreeText4 { get { return _freeText4; } set { Set(ref _freeText4, value, "FreeText4"); } }
        [DataMember]
        public bool AllowProtocolInvoicing { get; set; }
        [DataMember]
        public Guid? SettlementControlAccountId { get; set; }
        [DataMember]
        public Guid? SettlementDepartmentId { get; set; }

        [DataMember]
        public decimal RetentionInvoicePercent { get; set; }
        [DataMember]
        public decimal RetentionInvoiceMinValue { get; set; }
        [DataMember]
        public string RetentionInvoiceCurrency { get; set; }

        [DataMember]
        public string FiscalServicePassword { get; set; }
        [DataMember]
        public string FiscalServiceUser { get; set; }
        [DataMember]
        public short? FiscalServiceUploadDay { get; set; }

        [DataMember]
        public string FiscalPdfFormat { get; set; }
        [DataMember]
        public string FiscalTestUrl { get; set; }
        [DataMember]
        public string FiscalProductionUrl { get; set; }

        [DataMember]
        public bool MultiHotelMode { get { return _multiHotelMode; } set { Set(ref _multiHotelMode, value, "MultiHotelMode"); } }
        [DataMember]
        public bool SignTestMode { get { return _signTestMode; } set { Set(ref _signTestMode, value, "SignTestMode"); } }
        [DataMember]
        public bool SignPointOfSale { get { return _signPointOfSale; } set { Set(ref _signPointOfSale, value, "SignPointOfSale"); } }
        [DataMember]
        public long? SignVersion { get { return _signVersion; } set { Set(ref _signVersion, value, "SignVersion"); } }
        [DataMember]
        public DocumentSign? SignType
        {
            get { return _signType; }
            set
            {
                if (Set(ref _signType, value, "SignType"))
                {
                    PopulateVersions(_signType, SignTypeVersions);
                    NotifyPropertyChanged("SignTypeVersions", "ShowFiscalServiceCredentials", "ShowFiscalServiceUploadDay", "ShowFiscalUrl");

                    if (SignVersion.HasValue && !SignTypeVersions.Any(v => v.Id.Equals(SignVersion.Value)))
                        SignVersion = null;
                }
            }
        }

        [DataMember]
        public bool MultipleSerieAllowed { get { return _multipleSerieAllowed; } set { Set(ref _multipleSerieAllowed, value, "MultipleSerieAllowed"); } }
        [DataMember]
        public bool MultiSchemaInvoiceAllowed { get { return _multiSchemaInvoiceAllowed; } set { Set(ref _multiSchemaInvoiceAllowed, value, "MultiSchemaInvoiceAllowed"); } }
        [DataMember]
        public bool AdvanceDepositInvoiceAllowed { get { return _advanceDepositInvoiceAllowed; } set { Set(ref _advanceDepositInvoiceAllowed, value, "AdvanceDepositInvoiceAllowed"); } }
        [DataMember]
        public bool InternalInvoiceAllowed { get { return _internalInvoiceAllowed; } set { Set(ref _internalInvoiceAllowed, value, "InternalInvoiceAllowed"); } }
        [DataMember]
        public bool FiscalValidationEnabled { get { return _fiscalValidationEnabled; } set { Set(ref _fiscalValidationEnabled, value, "FiscalValidationEnabled"); } }
        [DataMember]
        public bool ValidateInvoiceInstructions { get { return _validateInvoiceInstructions; } set { Set(ref _validateInvoiceInstructions, value, "ValidateInvoiceInstructions"); } }
        [DataMember]
        public bool AllowCancelPOSTransaction { get { return _allowCancelPOSTransaction; } set { Set(ref _allowCancelPOSTransaction, value, "AllowCancelPOSTransaction"); } }
        [DataMember]
        public bool DisablePaymentTransfer { get { return _disablePaymentTransfer; } set { Set(ref _disablePaymentTransfer, value, "DisablePaymentTransfer"); } }
        [DataMember]
        public bool DisablePaymentSplit { get { return _disablePaymentSplit; } set { Set(ref _disablePaymentSplit, value, "DisablePaymentSplit"); } }
        [DataMember]
        public bool IsInternalUser { get; set; }

        #endregion
        #region Extended Properties

        [ReflectionExclude]
        public bool SignInvoices { get { return SignType.HasValue && SignType.Value != DocumentSign.None; } }

        [ReflectionExclude]
        public TypedList<KeyDescRecord> SignTypeVersions
        {
            get
            {
                if (_signTypeVersions == null)
                    _signTypeVersions = new TypedList<KeyDescRecord>();

                return _signTypeVersions;
            }
        }

        [ReflectionExclude]
        public bool ShowFiscalServiceCredentials
        {
            get
            {
                if (SignType.HasValue)
                {
                    switch (SignType.Value)
                    {
                        case DocumentSign.FiscalizationPortugal:
                        case DocumentSign.FiscalizationBrazilNFE:
                        case DocumentSign.FiscalizationColombiaBtw:
                        case DocumentSign.FiscalizationPeruDFacture:
                        case DocumentSign.FiscalizationMexicoCfdi:
                            return true;
                    }                   
                }

                return false;
            }
        }

        [ReflectionExclude]
        public bool ShowFiscalServiceUpdloadDay
        {
            get
            {
                if (SignType.HasValue)
                {
                    switch (SignType.Value)
                    {
                        case DocumentSign.FiscalizationPortugal:
                            return true;
                    }
                }

                return false;
            }
        }

        [ReflectionExclude]
        public bool ShowFiscalUrl
        {
            get
            {
                if (SignType.HasValue)
                {
                    switch (SignType.Value)
                    {
                        case DocumentSign.FiscalizationColombiaBtw:
                            return true;
                    }
                }

                return false;
            }
        }

        [ReflectionExclude]
        public bool VerifyFiscalNumber
        {
            get
            {
                if (!string.IsNullOrEmpty(MandatoryFiscalNumberInInvoices))
                {
                    //nunca = 100, todas = 010
                    if (MandatoryFiscalNumberInInvoices.Equals("010"))
                        return true;
                }

                return false;
            }
        }

        [ReflectionExclude]
        public CurrentAccountGroupingFlags CurrentAccountGrouping
        {
            get
            {
                var currentAccountGrouping = CurrentAccountGroupingFlags.None;

                if (GroupByWorkDate)
                    currentAccountGrouping = currentAccountGrouping | CurrentAccountGroupingFlags.ByWorkDate;
                if (GroupByValueDate)
                    currentAccountGrouping = currentAccountGrouping | CurrentAccountGroupingFlags.ByValueDate;
                if (GroupByDepartment)
                    currentAccountGrouping = currentAccountGrouping | CurrentAccountGroupingFlags.ByDepartment;
                if (GroupByService)
                    currentAccountGrouping = currentAccountGrouping | CurrentAccountGroupingFlags.ByService;
                if (GroupByDaily)
                    currentAccountGrouping = currentAccountGrouping | CurrentAccountGroupingFlags.ByDaily;
                if (GroupByRoomRate)
                    currentAccountGrouping = currentAccountGrouping | CurrentAccountGroupingFlags.ByRoomRate;
                if (GroupByPhone)
                    currentAccountGrouping = currentAccountGrouping | CurrentAccountGroupingFlags.ByPhone;
                if (GroupByDocNumber)
                    currentAccountGrouping = currentAccountGrouping | CurrentAccountGroupingFlags.ByDocNumber;
                if (GroupByPaymentType)
                    currentAccountGrouping = currentAccountGrouping | CurrentAccountGroupingFlags.ByPaymentsType;
                if (GroupByReservation)
                    currentAccountGrouping = currentAccountGrouping | CurrentAccountGroupingFlags.ByReservation;
                if (GroupByTax)
                    currentAccountGrouping = currentAccountGrouping | CurrentAccountGroupingFlags.ByTax;
                if (GroupByCityTax)
                    currentAccountGrouping = currentAccountGrouping | CurrentAccountGroupingFlags.ByCityTax;
                if (GroupByPackage)
                    currentAccountGrouping = currentAccountGrouping | CurrentAccountGroupingFlags.ByPackage;

                return currentAccountGrouping;
            }
            set
            {
                GroupByWorkDate = value.HasFlag(CurrentAccountGroupingFlags.ByWorkDate);
                GroupByValueDate = value.HasFlag(CurrentAccountGroupingFlags.ByValueDate);
                GroupByDepartment = value.HasFlag(CurrentAccountGroupingFlags.ByDepartment);
                GroupByService = value.HasFlag(CurrentAccountGroupingFlags.ByService);
                GroupByDaily = value.HasFlag(CurrentAccountGroupingFlags.ByDaily);
                GroupByRoomRate = value.HasFlag(CurrentAccountGroupingFlags.ByRoomRate);
                GroupByPhone = value.HasFlag(CurrentAccountGroupingFlags.ByPhone);
                GroupByDocNumber = value.HasFlag(CurrentAccountGroupingFlags.ByDocNumber);
                GroupByPaymentType = value.HasFlag(CurrentAccountGroupingFlags.ByPaymentsType);
                GroupByReservation = value.HasFlag(CurrentAccountGroupingFlags.ByReservation);
                GroupByTax = value.HasFlag(CurrentAccountGroupingFlags.ByTax);
                GroupByCityTax = value.HasFlag(CurrentAccountGroupingFlags.ByCityTax);
                GroupByPackage = value.HasFlag(CurrentAccountGroupingFlags.ByPackage);
            }
        }

        [ReflectionExclude]
        public bool HasFiscalServiceCredentials
        {
            get { return !string.IsNullOrEmpty(FiscalServiceUser) && !string.IsNullOrEmpty(FiscalServicePassword); }
        }

        #endregion
        #region Public Methods

        public static void PopulateVersions(DocumentSign? signType, IList<KeyDescRecord> records)
        {
            records.Clear();
            if (signType.HasValue)
            {
                switch (signType.Value)
                {
                    case DocumentSign.FiscalizationPortugal:
                        records.Add(new KeyDescRecord() { Id = 103L, Description = "1.03_01 274/2013 21/08" });
                        records.Add(new KeyDescRecord() { Id = 104L, Description = "1.04_01 302/2016 02/12" });
                        break;
                    case DocumentSign.FiscalizationAngola:
                        records.Add(new KeyDescRecord() { Id = 104L, Description = "1.04_01" });
                        break;
                    case DocumentSign.FiscalizationPeruDFacture:
                        records.Add(new KeyDescRecord() { Id = 21L, Description = "UBL 2.1" });
                        break;
                    case DocumentSign.FiscalizationColombiaBtw:
                        records.Add(new KeyDescRecord() { Id = 10L, Description = "UBL 1.0" });
                        records.Add(new KeyDescRecord() { Id = 200L, Description = "UBL 2.0 sync" });
                        records.Add(new KeyDescRecord() { Id = 201L, Description = "UBL 2.0 async" });
                        break;
                    case DocumentSign.FiscalizationMexicoCfdi:
                        records.Add(new KeyDescRecord() { Id = 133L, Description = "3.3 WFactura" });
                        records.Add(new KeyDescRecord() { Id = 233L, Description = "3.3 EDICOM" });
                        break;
                }
            }
        }

        public static string GetVersion(DocumentSign? signType, long? signVersion)
        {
            var versions = new List<KeyDescRecord>();
            SettingInvoiceContract.PopulateVersions(signType, versions);
            var version = versions.FirstOrDefault(v => v.Id.Equals(signVersion));
            if (version != null)
                return version.Description;

            return string.Empty;
        }

        #endregion
        #region Constructor

        public SettingInvoiceContract()
            : base() 
        {
            CurrentAccountGrouping = CurrentAccountGroupingFlags.None;
        }

        #endregion
    }
}