﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using NewHotel.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace NewHotel.Contracts
{
    [DataContract]
	[Serializable]
    [CustomValidation(typeof(SettingEventsContract), "ValidateSettingEvents")]
    public class SettingEventsContract : BaseContract
    {
        #region Members
        private ARGBColor _inactivityColor;
        private ARGBColor _invoicedColor;
        private ARGBColor _notinvoicedColor;
        private Guid? _departmentId;
        private Guid? _serviceId;
        private bool _AllowReservations;
        private Guid? _CancellationReasonId;
        private bool _AllowFacturation;
        private bool _multiOccupation;
        private bool _individualInvoiceSeries;
        #endregion

        #region Properties

        [DataMember]
        public ARGBColor InactivityColor { get { return _inactivityColor; } set { Set(ref _inactivityColor, value, "InactivityColor"); } }
        [DataMember]
        public ARGBColor InvoicedColor { get { return _invoicedColor; } set { Set(ref _invoicedColor, value, "InvoicedColor"); } }
        [DataMember]
        public ARGBColor NotInvoicedColor { get { return _notinvoicedColor; } set { Set(ref _notinvoicedColor, value, "NotInvoicedColor"); } }
        [DataMember]
        public Guid? DepartmentId { get { return _departmentId; } set { Set(ref _departmentId, value, "DepartmentId"); } }
        [DataMember]
        public Guid? ServiceId { get { return _serviceId; } set { Set(ref _serviceId, value, "ServiceId"); } }
        [DataMember]
        public bool AllowReservations { get { return _AllowReservations; } set { Set(ref _AllowReservations, value, "AllowReservations"); } }
        [DataMember]
        public bool AllowFacturation { get { return _AllowFacturation; } set { Set(ref _AllowFacturation, value, "AllowFacturation"); } }
        [DataMember]
        public Guid? CancellationReasonId { get { return _CancellationReasonId; } set { Set(ref _CancellationReasonId, value, "CancellationReasonId"); } }
        [DataMember]
        public bool MultiOccupation { get { return _multiOccupation; } set { Set(ref _multiOccupation, value, "MultiOccupation"); } }
        [DataMember]
        public bool IndividualInvoiceSeries { get { return _individualInvoiceSeries; } set { Set(ref _individualInvoiceSeries, value, "IndividualInvoiceSeries"); } }
        #endregion
        #region Constructors

        public SettingEventsContract()
            : base()
        {
        }

        #endregion

        #region Validations
        public static System.ComponentModel.DataAnnotations.ValidationResult ValidateSettingEvents(SettingEventsContract contract)
        {
            return System.ComponentModel.DataAnnotations.ValidationResult.Success;
        }
        #endregion
    }
}
